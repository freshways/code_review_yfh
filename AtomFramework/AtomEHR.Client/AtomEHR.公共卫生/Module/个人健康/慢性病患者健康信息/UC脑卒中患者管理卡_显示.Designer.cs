﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC脑卒中患者管理卡_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC脑卒中患者管理卡_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt特殊治疗 = new DevExpress.XtraEditors.TextEdit();
            this.txt危险因素 = new DevExpress.XtraEditors.TextEdit();
            this.txt目前症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt家族史 = new DevExpress.XtraEditors.TextEdit();
            this.txt病例来源 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt管理卡编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt终止管理日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt体检日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt低密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt高密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txtBMI = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txtMRI检查 = new DevExpress.XtraEditors.TextEdit();
            this.txtCT检查 = new DevExpress.XtraEditors.TextEdit();
            this.txt诊断医院 = new DevExpress.XtraEditors.TextEdit();
            this.txt发病时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt管理组别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtmRS评分 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑卒中分类 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑卒中部位 = new DevExpress.XtraEditors.TextEdit();
            this.txt非药物治疗措施 = new DevExpress.XtraEditors.MemoEdit();
            this.txt用药情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.TextEdit();
            this.txt终止管理 = new DevExpress.XtraEditors.TextEdit();
            this.txt终止理由 = new DevExpress.XtraEditors.TextEdit();
            this.txt生活自理能力 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt特殊治疗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病例来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMRI检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCT检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt诊断医院.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发病时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发病时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmRS评分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt非药物治疗措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止理由.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(3, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(84, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 1;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(165, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt特殊治疗);
            this.layoutControl1.Controls.Add(this.txt危险因素);
            this.layoutControl1.Controls.Add(this.txt目前症状);
            this.layoutControl1.Controls.Add(this.txt家族史);
            this.layoutControl1.Controls.Add(this.txt病例来源);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt管理卡编号);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab最近更新时间);
            this.layoutControl1.Controls.Add(this.lab创建时间);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt终止管理日期);
            this.layoutControl1.Controls.Add(this.txt体检日期);
            this.layoutControl1.Controls.Add(this.txt胆固醇);
            this.layoutControl1.Controls.Add(this.txt低密度蛋白);
            this.layoutControl1.Controls.Add(this.txt高密度蛋白);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt血压);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.txtBMI);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.txtMRI检查);
            this.layoutControl1.Controls.Add(this.txtCT检查);
            this.layoutControl1.Controls.Add(this.txt诊断医院);
            this.layoutControl1.Controls.Add(this.txt发病时间);
            this.layoutControl1.Controls.Add(this.txt管理组别);
            this.layoutControl1.Controls.Add(this.txtmRS评分);
            this.layoutControl1.Controls.Add(this.txt脑卒中分类);
            this.layoutControl1.Controls.Add(this.txt脑卒中部位);
            this.layoutControl1.Controls.Add(this.txt非药物治疗措施);
            this.layoutControl1.Controls.Add(this.txt用药情况);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Controls.Add(this.txt终止管理);
            this.layoutControl1.Controls.Add(this.txt终止理由);
            this.layoutControl1.Controls.Add(this.txt生活自理能力);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(618, 324, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt特殊治疗
            // 
            this.txt特殊治疗.Location = new System.Drawing.Point(405, 630);
            this.txt特殊治疗.Name = "txt特殊治疗";
            this.txt特殊治疗.Size = new System.Drawing.Size(320, 20);
            this.txt特殊治疗.StyleController = this.layoutControl1;
            this.txt特殊治疗.TabIndex = 124;
            // 
            // txt危险因素
            // 
            this.txt危险因素.Location = new System.Drawing.Point(98, 289);
            this.txt危险因素.Name = "txt危险因素";
            this.txt危险因素.Size = new System.Drawing.Size(630, 20);
            this.txt危险因素.StyleController = this.layoutControl1;
            this.txt危险因素.TabIndex = 123;
            // 
            // txt目前症状
            // 
            this.txt目前症状.Location = new System.Drawing.Point(98, 240);
            this.txt目前症状.Name = "txt目前症状";
            this.txt目前症状.Size = new System.Drawing.Size(630, 20);
            this.txt目前症状.StyleController = this.layoutControl1;
            this.txt目前症状.TabIndex = 122;
            // 
            // txt家族史
            // 
            this.txt家族史.Location = new System.Drawing.Point(98, 216);
            this.txt家族史.Name = "txt家族史";
            this.txt家族史.Size = new System.Drawing.Size(630, 20);
            this.txt家族史.StyleController = this.layoutControl1;
            this.txt家族史.TabIndex = 121;
            // 
            // txt病例来源
            // 
            this.txt病例来源.Location = new System.Drawing.Point(98, 192);
            this.txt病例来源.Name = "txt病例来源";
            this.txt病例来源.Size = new System.Drawing.Size(630, 20);
            this.txt病例来源.StyleController = this.layoutControl1;
            this.txt病例来源.TabIndex = 120;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(449, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(279, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 119;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(449, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(279, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 118;
            // 
            // txt管理卡编号
            // 
            this.txt管理卡编号.Location = new System.Drawing.Point(98, 144);
            this.txt管理卡编号.Name = "txt管理卡编号";
            this.txt管理卡编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt管理卡编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt管理卡编号.Properties.ReadOnly = true;
            this.txt管理卡编号.Size = new System.Drawing.Size(252, 20);
            this.txt管理卡编号.StyleController = this.layoutControl1;
            this.txt管理卡编号.TabIndex = 117;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(98, 120);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(252, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 116;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(98, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(252, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 115;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(98, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(252, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 114;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(449, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(279, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 113;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(449, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(279, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 112;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(6, 555);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(719, 71);
            this.gcDetail.TabIndex = 93;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 332;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 369;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(98, 48);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(252, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 53;
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, 30);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(725, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 52;
            this.lab考核项.Text = "考核项：25     缺项：17 完整度：0% ";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(101, 788);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(624, 20);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 51;
            this.lab当前所属机构.Text = "labelControl16";
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(624, 764);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(101, 20);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 50;
            this.lab创建机构.Text = "labelControl15";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(405, 764);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(120, 20);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 49;
            this.lab最近修改人.Text = "labelControl14";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(101, 764);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(205, 20);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 48;
            this.lab创建人.Text = "labelControl13";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近更新时间.Location = new System.Drawing.Point(624, 740);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(101, 14);
            this.lab最近更新时间.StyleController = this.layoutControl1;
            this.lab最近更新时间.TabIndex = 47;
            this.lab最近更新时间.Text = "labelControl12";
            // 
            // lab创建时间
            // 
            this.lab创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建时间.Location = new System.Drawing.Point(405, 740);
            this.lab创建时间.Name = "lab创建时间";
            this.lab创建时间.Size = new System.Drawing.Size(120, 20);
            this.lab创建时间.StyleController = this.layoutControl1;
            this.lab创建时间.TabIndex = 46;
            this.lab创建时间.Text = "labelControl11";
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(101, 740);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(205, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 45;
            // 
            // txt终止管理日期
            // 
            this.txt终止管理日期.EditValue = null;
            this.txt终止管理日期.Location = new System.Drawing.Point(101, 715);
            this.txt终止管理日期.Name = "txt终止管理日期";
            this.txt终止管理日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt终止管理日期.Size = new System.Drawing.Size(205, 20);
            this.txt终止管理日期.StyleController = this.layoutControl1;
            this.txt终止管理日期.TabIndex = 43;
            // 
            // txt体检日期
            // 
            this.txt体检日期.EditValue = null;
            this.txt体检日期.Location = new System.Drawing.Point(452, 506);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt体检日期.Size = new System.Drawing.Size(139, 20);
            this.txt体检日期.StyleController = this.layoutControl1;
            this.txt体检日期.TabIndex = 36;
            // 
            // txt胆固醇
            // 
            this.txt胆固醇.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt胆固醇.Lbl1Text = "mmol/L";
            this.txt胆固醇.Location = new System.Drawing.Point(101, 506);
            this.txt胆固醇.Name = "txt胆固醇";
            this.txt胆固醇.Size = new System.Drawing.Size(252, 20);
            this.txt胆固醇.TabIndex = 35;
            this.txt胆固醇.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt低密度蛋白
            // 
            this.txt低密度蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt低密度蛋白.Lbl1Text = "mmol/L";
            this.txt低密度蛋白.Location = new System.Drawing.Point(452, 482);
            this.txt低密度蛋白.Name = "txt低密度蛋白";
            this.txt低密度蛋白.Size = new System.Drawing.Size(273, 20);
            this.txt低密度蛋白.TabIndex = 34;
            this.txt低密度蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt高密度蛋白
            // 
            this.txt高密度蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt高密度蛋白.Lbl1Text = "mmol/L";
            this.txt高密度蛋白.Location = new System.Drawing.Point(101, 482);
            this.txt高密度蛋白.Name = "txt高密度蛋白";
            this.txt高密度蛋白.Size = new System.Drawing.Size(252, 20);
            this.txt高密度蛋白.TabIndex = 33;
            this.txt高密度蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(452, 458);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(273, 20);
            this.txt空腹血糖.TabIndex = 32;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血压
            // 
            this.txt血压.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压.Lbl1Text = "/";
            this.txt血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt血压.Lbl2Text = "mmHg";
            this.txt血压.Location = new System.Drawing.Point(101, 458);
            this.txt血压.Name = "txt血压";
            this.txt血压.Size = new System.Drawing.Size(252, 20);
            this.txt血压.TabIndex = 31;
            this.txt血压.Txt1EditValue = null;
            this.txt血压.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt血压.Txt2EditValue = null;
            this.txt血压.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt腰围.Lbl1Text = "CM";
            this.txt腰围.Location = new System.Drawing.Point(452, 434);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(273, 20);
            this.txt腰围.TabIndex = 30;
            this.txt腰围.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txtBMI
            // 
            this.txtBMI.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txtBMI.Lbl1Text = "kg/㎡";
            this.txtBMI.Location = new System.Drawing.Point(101, 434);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Size = new System.Drawing.Size(252, 20);
            this.txtBMI.TabIndex = 29;
            this.txtBMI.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt体重.Lbl1Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(452, 410);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(273, 20);
            this.txt体重.TabIndex = 28;
            this.txt体重.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(101, 410);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(252, 20);
            this.txt身高.TabIndex = 27;
            this.txt身高.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txtMRI检查
            // 
            this.txtMRI检查.Location = new System.Drawing.Point(449, 313);
            this.txtMRI检查.Name = "txtMRI检查";
            this.txtMRI检查.Size = new System.Drawing.Size(279, 20);
            this.txtMRI检查.StyleController = this.layoutControl1;
            this.txtMRI检查.TabIndex = 23;
            // 
            // txtCT检查
            // 
            this.txtCT检查.Location = new System.Drawing.Point(98, 313);
            this.txtCT检查.Name = "txtCT检查";
            this.txtCT检查.Size = new System.Drawing.Size(252, 20);
            this.txtCT检查.StyleController = this.layoutControl1;
            this.txtCT检查.TabIndex = 22;
            // 
            // txt诊断医院
            // 
            this.txt诊断医院.Location = new System.Drawing.Point(449, 168);
            this.txt诊断医院.Name = "txt诊断医院";
            this.txt诊断医院.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt诊断医院.Properties.Appearance.Options.UseBackColor = true;
            this.txt诊断医院.Properties.ReadOnly = true;
            this.txt诊断医院.Size = new System.Drawing.Size(279, 20);
            this.txt诊断医院.StyleController = this.layoutControl1;
            this.txt诊断医院.TabIndex = 16;
            // 
            // txt发病时间
            // 
            this.txt发病时间.EditValue = null;
            this.txt发病时间.Location = new System.Drawing.Point(98, 168);
            this.txt发病时间.Name = "txt发病时间";
            this.txt发病时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发病时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发病时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发病时间.Size = new System.Drawing.Size(252, 20);
            this.txt发病时间.StyleController = this.layoutControl1;
            this.txt发病时间.TabIndex = 15;
            // 
            // txt管理组别
            // 
            this.txt管理组别.Location = new System.Drawing.Point(449, 144);
            this.txt管理组别.Name = "txt管理组别";
            this.txt管理组别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt管理组别.Properties.Appearance.Options.UseBackColor = true;
            this.txt管理组别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt管理组别.Properties.ReadOnly = true;
            this.txt管理组别.Size = new System.Drawing.Size(279, 20);
            this.txt管理组别.StyleController = this.layoutControl1;
            this.txt管理组别.TabIndex = 14;
            // 
            // txtmRS评分
            // 
            this.txtmRS评分.Location = new System.Drawing.Point(98, 264);
            this.txtmRS评分.Name = "txtmRS评分";
            this.txtmRS评分.Size = new System.Drawing.Size(630, 20);
            this.txtmRS评分.StyleController = this.layoutControl1;
            this.txtmRS评分.TabIndex = 20;
            // 
            // txt脑卒中分类
            // 
            this.txt脑卒中分类.Location = new System.Drawing.Point(98, 337);
            this.txt脑卒中分类.Name = "txt脑卒中分类";
            this.txt脑卒中分类.Size = new System.Drawing.Size(252, 20);
            this.txt脑卒中分类.StyleController = this.layoutControl1;
            this.txt脑卒中分类.TabIndex = 24;
            // 
            // txt脑卒中部位
            // 
            this.txt脑卒中部位.Location = new System.Drawing.Point(449, 337);
            this.txt脑卒中部位.Name = "txt脑卒中部位";
            this.txt脑卒中部位.Size = new System.Drawing.Size(279, 20);
            this.txt脑卒中部位.StyleController = this.layoutControl1;
            this.txt脑卒中部位.TabIndex = 25;
            // 
            // txt非药物治疗措施
            // 
            this.txt非药物治疗措施.Location = new System.Drawing.Point(101, 655);
            this.txt非药物治疗措施.Name = "txt非药物治疗措施";
            this.txt非药物治疗措施.Size = new System.Drawing.Size(624, 31);
            this.txt非药物治疗措施.StyleController = this.layoutControl1;
            this.txt非药物治疗措施.TabIndex = 125;
            this.txt非药物治疗措施.UseOptimizedRendering = true;
            // 
            // txt用药情况
            // 
            this.txt用药情况.Location = new System.Drawing.Point(101, 530);
            this.txt用药情况.Name = "txt用药情况";
            this.txt用药情况.Properties.AutoHeight = false;
            this.txt用药情况.Size = new System.Drawing.Size(252, 21);
            this.txt用药情况.StyleController = this.layoutControl1;
            this.txt用药情况.TabIndex = 37;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(101, 630);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Size = new System.Drawing.Size(205, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 39;
            // 
            // txt终止管理
            // 
            this.txt终止管理.Location = new System.Drawing.Point(101, 690);
            this.txt终止管理.Name = "txt终止管理";
            this.txt终止管理.Properties.AutoHeight = false;
            this.txt终止管理.Size = new System.Drawing.Size(205, 21);
            this.txt终止管理.StyleController = this.layoutControl1;
            this.txt终止管理.TabIndex = 42;
            // 
            // txt终止理由
            // 
            this.txt终止理由.Location = new System.Drawing.Point(405, 715);
            this.txt终止理由.Name = "txt终止理由";
            this.txt终止理由.Properties.AutoHeight = false;
            this.txt终止理由.Size = new System.Drawing.Size(320, 21);
            this.txt终止理由.StyleController = this.layoutControl1;
            this.txt终止理由.TabIndex = 44;
            // 
            // txt生活自理能力
            // 
            this.txt生活自理能力.Location = new System.Drawing.Point(98, 362);
            this.txt生活自理能力.Name = "txt生活自理能力";
            this.txt生活自理能力.Properties.AutoHeight = false;
            this.txt生活自理能力.Size = new System.Drawing.Size(630, 21);
            this.txt生活自理能力.StyleController = this.layoutControl1;
            this.txt生活自理能力.TabIndex = 26;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "脑卒中患者管理卡";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem49});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 824);
            this.layoutControlGroup1.Text = "脑卒中患者管理卡";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23,
            this.layoutControlItem21,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem17,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem11,
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem35,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 18);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 777);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt生活自理能力;
            this.layoutControlItem23.CustomizationFormText = "生活自理能力";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(729, 25);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "生活自理能力";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt脑卒中分类;
            this.layoutControlItem21.CustomizationFormText = "脑卒中分类";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 289);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(351, 25);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "脑卒中分类";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txtCT检查;
            this.layoutControlItem19.CustomizationFormText = "CT检查结果";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 265);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "CT检查结果";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txtMRI检查;
            this.layoutControlItem20.CustomizationFormText = "MRI检查结果";
            this.layoutControlItem20.Location = new System.Drawing.Point(351, 265);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "MRI检查结果";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt脑卒中部位;
            this.layoutControlItem22.CustomizationFormText = "脑卒中部位";
            this.layoutControlItem22.Location = new System.Drawing.Point(351, 289);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(378, 25);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Tag = "check";
            this.layoutControlItem22.Text = "脑卒中部位";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txtmRS评分;
            this.layoutControlItem17.CustomizationFormText = "mRS评分";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(729, 25);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Tag = "check";
            this.layoutControlItem17.Text = "mRS评分";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt发病时间;
            this.layoutControlItem12.CustomizationFormText = "发病时间";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "发病时间";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt诊断医院;
            this.layoutControlItem13.CustomizationFormText = "诊断医院";
            this.layoutControlItem13.Location = new System.Drawing.Point(351, 120);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "诊断医院";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt管理组别;
            this.layoutControlItem11.CustomizationFormText = "管理组别";
            this.layoutControlItem11.Location = new System.Drawing.Point(351, 96);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "管理组别";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "体检结果";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem36,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.emptySpaceItem1,
            this.layout药物列表,
            this.emptySpaceItem3,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.emptySpaceItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 339);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 428);
            this.layoutControlGroup3.Text = "体检结果";
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem27.Control = this.txt腰围;
            this.layoutControlItem27.CustomizationFormText = "腰围";
            this.layoutControlItem27.Location = new System.Drawing.Point(351, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "腰围";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.txt体重;
            this.layoutControlItem25.CustomizationFormText = "体重";
            this.layoutControlItem25.Location = new System.Drawing.Point(351, 0);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "体重";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem24.Control = this.txt身高;
            this.layoutControlItem24.CustomizationFormText = "身高";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "身高";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem26.Control = this.txtBMI;
            this.layoutControlItem26.CustomizationFormText = "BMI";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Tag = "check";
            this.layoutControlItem26.Text = "BMI";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.txt血压;
            this.layoutControlItem28.CustomizationFormText = "血压值";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "血压值";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt空腹血糖;
            this.layoutControlItem29.CustomizationFormText = "空腹血糖";
            this.layoutControlItem29.Location = new System.Drawing.Point(351, 48);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Tag = "check";
            this.layoutControlItem29.Text = "空腹血糖";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem30.Control = this.txt高密度蛋白;
            this.layoutControlItem30.CustomizationFormText = "高密度脂蛋白";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "高密度脂蛋白";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.txt低密度蛋白;
            this.layoutControlItem31.CustomizationFormText = "低密度脂蛋白";
            this.layoutControlItem31.Location = new System.Drawing.Point(351, 72);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "低密度脂蛋白";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem32.Control = this.txt胆固醇;
            this.layoutControlItem32.CustomizationFormText = "胆固醇";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Tag = "check";
            this.layoutControlItem32.Text = "胆固醇";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem33.Control = this.txt体检日期;
            this.layoutControlItem33.CustomizationFormText = "体检时间";
            this.layoutControlItem33.Location = new System.Drawing.Point(351, 96);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "体检时间";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem34.Control = this.txt用药情况;
            this.layoutControlItem34.CustomizationFormText = "用药情况";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(351, 25);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "用药情况";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.txt服药依从性;
            this.layoutControlItem36.CustomizationFormText = "服药依从性";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 220);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Tag = "check";
            this.layoutControlItem36.Text = "服药依从性";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.txt终止管理;
            this.layoutControlItem39.CustomizationFormText = "是否终止管理";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 280);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "是否终止管理";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.txt终止管理日期;
            this.layoutControlItem40.CustomizationFormText = "终止管理日期";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 305);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "终止管理日期";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt终止理由;
            this.layoutControlItem41.CustomizationFormText = "终止理由";
            this.layoutControlItem41.Location = new System.Drawing.Point(304, 305);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(419, 25);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "终止理由";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt发生时间;
            this.layoutControlItem42.CustomizationFormText = "发生时间";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 330);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "发生时间";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lab创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(304, 330);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lab最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(523, 330);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 18);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lab创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 354);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "录入人";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab最近修改人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(304, 354);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近更新人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lab创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(523, 354);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lab当前所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 378);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(351, 120);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(372, 25);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = " ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 145);
            this.layout药物列表.MinSize = new System.Drawing.Size(111, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(723, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = " ";
            this.layout药物列表.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(304, 280);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(419, 25);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt特殊治疗;
            this.layoutControlItem9.CustomizationFormText = "特殊治疗";
            this.layoutControlItem9.Location = new System.Drawing.Point(304, 220);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(419, 25);
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "特殊治疗";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem10.Control = this.txt非药物治疗措施;
            this.layoutControlItem10.CustomizationFormText = "非药物治疗措施";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 245);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(50, 35);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(723, 35);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "非药物治疗措施";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(589, 96);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(134, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt个人档案号;
            this.layoutControlItem1.CustomizationFormText = "个人档案号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem1.Text = "个人档案号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 767);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(729, 10);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt出生日期;
            this.layoutControlItem2.CustomizationFormText = "出生日期";
            this.layoutControlItem2.Location = new System.Drawing.Point(351, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem2.Text = "出生日期";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt姓名;
            this.layoutControlItem5.CustomizationFormText = "姓名";
            this.layoutControlItem5.Location = new System.Drawing.Point(351, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem5.Text = "姓名";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt性别;
            this.layoutControlItem3.CustomizationFormText = "性别";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem3.Text = "性别";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt身份证号;
            this.layoutControlItem35.CustomizationFormText = "身份证号";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem35.Text = "身份证号";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt联系电话;
            this.layoutControlItem50.CustomizationFormText = "联系电话";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem50.Text = "联系电话";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt管理卡编号;
            this.layoutControlItem51.CustomizationFormText = "管理卡编号";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem51.Text = "管理卡编号";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt居住地址;
            this.layoutControlItem52.CustomizationFormText = "居住地址";
            this.layoutControlItem52.Location = new System.Drawing.Point(351, 72);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem52.Text = "居住地址";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.txt职业;
            this.layoutControlItem53.CustomizationFormText = "职业";
            this.layoutControlItem53.Location = new System.Drawing.Point(351, 48);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem53.Text = "职业";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt病例来源;
            this.layoutControlItem4.CustomizationFormText = "病例来源";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem4.Tag = "check";
            this.layoutControlItem4.Text = "病例来源";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt家族史;
            this.layoutControlItem6.CustomizationFormText = "家族史";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "家族史";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt目前症状;
            this.layoutControlItem7.CustomizationFormText = "就诊时症状";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem7.Tag = "check";
            this.layoutControlItem7.Text = "就诊时症状";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt危险因素;
            this.layoutControlItem8.CustomizationFormText = "脑卒中危险因素";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 241);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "脑卒中危险因素";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.lab考核项;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(729, 18);
            this.layoutControlItem49.Text = "layoutControlItem49";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextToControlDistance = 0;
            this.layoutControlItem49.TextVisible = false;
            // 
            // UC脑卒中患者管理卡_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC脑卒中患者管理卡_显示";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC脑卒中患者管理卡_显示_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt特殊治疗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病例来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMRI检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCT检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt诊断医院.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发病时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发病时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtmRS评分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt非药物治疗措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止理由.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt管理卡编号;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建时间;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.DateEdit txt终止管理日期;
        private DevExpress.XtraEditors.DateEdit txt体检日期;
        private Library.UserControls.UCTxtLbl txt胆固醇;
        private Library.UserControls.UCTxtLbl txt低密度蛋白;
        private Library.UserControls.UCTxtLbl txt高密度蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLblTxtLbl txt血压;
        private Library.UserControls.UCTxtLbl txt腰围;
        private Library.UserControls.UCTxtLbl txtBMI;
        private Library.UserControls.UCTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt身高;
        private DevExpress.XtraEditors.TextEdit txtMRI检查;
        private DevExpress.XtraEditors.TextEdit txtCT检查;
        private DevExpress.XtraEditors.TextEdit txt诊断医院;
        private DevExpress.XtraEditors.DateEdit txt发病时间;
        private DevExpress.XtraEditors.ComboBoxEdit txt管理组别;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraEditors.TextEdit txt特殊治疗;
        private DevExpress.XtraEditors.TextEdit txt危险因素;
        private DevExpress.XtraEditors.TextEdit txt目前症状;
        private DevExpress.XtraEditors.TextEdit txt家族史;
        private DevExpress.XtraEditors.TextEdit txt病例来源;
        private DevExpress.XtraEditors.TextEdit txtmRS评分;
        private DevExpress.XtraEditors.TextEdit txt脑卒中分类;
        private DevExpress.XtraEditors.TextEdit txt脑卒中部位;
        private DevExpress.XtraEditors.MemoEdit txt非药物治疗措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txt用药情况;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit txt服药依从性;
        private DevExpress.XtraEditors.TextEdit txt终止管理;
        private DevExpress.XtraEditors.TextEdit txt终止理由;
        private DevExpress.XtraEditors.TextEdit txt生活自理能力;
    }
}
