﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC糖尿病患者管理卡_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC糖尿病患者管理卡_显示));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.Layout1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt尿微量蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txt终止理由 = new DevExpress.XtraEditors.TextEdit();
            this.txt终止管理 = new DevExpress.XtraEditors.TextEdit();
            this.txt生活自理能力 = new DevExpress.XtraEditors.TextEdit();
            this.txt体育锻炼 = new DevExpress.XtraEditors.TextEdit();
            this.txt饮酒情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt吸烟情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt降糖药 = new DevExpress.XtraEditors.TextEdit();
            this.txt胰岛素 = new DevExpress.XtraEditors.TextEdit();
            this.txt目前症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt家族史 = new DevExpress.XtraEditors.TextEdit();
            this.txt病例来源 = new DevExpress.XtraEditors.TextEdit();
            this.txt管理卡编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt管理组别 = new DevExpress.XtraEditors.TextEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.txt胰岛素用量 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt餐后血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt终止管理日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt甘油三脂 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt低密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt高密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txtBMI = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt确诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt确诊单位 = new DevExpress.XtraEditors.TextEdit();
            this.txt糖化血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lab录入时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt并发症情况 = new DevExpress.XtraEditors.MemoEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt糖尿病类型 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.UCTxtLblitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).BeginInit();
            this.Layout1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿微量蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止理由.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体育锻炼.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt降糖药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病例来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖尿病类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UCTxtLblitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(748, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(3, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(55, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(64, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(55, 23);
            this.btn删除.TabIndex = 1;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(125, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(55, 23);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Visible = false;
            // 
            // Layout1
            // 
            this.Layout1.Controls.Add(this.txt联系电话);
            this.Layout1.Controls.Add(this.txt出生日期);
            this.Layout1.Controls.Add(this.txt身份证号);
            this.Layout1.Controls.Add(this.txt职业);
            this.Layout1.Controls.Add(this.txt性别);
            this.Layout1.Controls.Add(this.txt姓名);
            this.Layout1.Controls.Add(this.txt个人档案号);
            this.Layout1.Controls.Add(this.txt尿微量蛋白);
            this.Layout1.Controls.Add(this.txt终止理由);
            this.Layout1.Controls.Add(this.txt终止管理);
            this.Layout1.Controls.Add(this.txt生活自理能力);
            this.Layout1.Controls.Add(this.txt体育锻炼);
            this.Layout1.Controls.Add(this.txt饮酒情况);
            this.Layout1.Controls.Add(this.txt吸烟情况);
            this.Layout1.Controls.Add(this.txt降糖药);
            this.Layout1.Controls.Add(this.txt胰岛素);
            this.Layout1.Controls.Add(this.txt目前症状);
            this.Layout1.Controls.Add(this.txt家族史);
            this.Layout1.Controls.Add(this.txt病例来源);
            this.Layout1.Controls.Add(this.txt管理卡编号);
            this.Layout1.Controls.Add(this.txt管理组别);
            this.Layout1.Controls.Add(this.gcDetail);
            this.Layout1.Controls.Add(this.lab创建人);
            this.Layout1.Controls.Add(this.lab当前所属机构);
            this.Layout1.Controls.Add(this.txt胰岛素用量);
            this.Layout1.Controls.Add(this.txt餐后血糖);
            this.Layout1.Controls.Add(this.txt发生时间);
            this.Layout1.Controls.Add(this.txt终止管理日期);
            this.Layout1.Controls.Add(this.txt胆固醇);
            this.Layout1.Controls.Add(this.txt甘油三脂);
            this.Layout1.Controls.Add(this.txt低密度蛋白);
            this.Layout1.Controls.Add(this.txt高密度蛋白);
            this.Layout1.Controls.Add(this.txt空腹血糖);
            this.Layout1.Controls.Add(this.txt腰围);
            this.Layout1.Controls.Add(this.txtBMI);
            this.Layout1.Controls.Add(this.txt体重);
            this.Layout1.Controls.Add(this.txt身高);
            this.Layout1.Controls.Add(this.txt血压值);
            this.Layout1.Controls.Add(this.txt确诊时间);
            this.Layout1.Controls.Add(this.txt确诊单位);
            this.Layout1.Controls.Add(this.txt糖化血红蛋白);
            this.Layout1.Controls.Add(this.lab创建机构);
            this.Layout1.Controls.Add(this.lab最近修改人);
            this.Layout1.Controls.Add(this.lab最近更新时间);
            this.Layout1.Controls.Add(this.labelControl2);
            this.Layout1.Controls.Add(this.lab录入时间);
            this.Layout1.Controls.Add(this.txt并发症情况);
            this.Layout1.Controls.Add(this.txt居住地址);
            this.Layout1.Controls.Add(this.txt糖尿病类型);
            this.Layout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Layout1.Location = new System.Drawing.Point(0, 28);
            this.Layout1.Name = "Layout1";
            this.Layout1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(618, 208, 250, 350);
            this.Layout1.Root = this.layoutControlGroup1;
            this.Layout1.Size = new System.Drawing.Size(748, 470);
            this.Layout1.TabIndex = 2;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(87, 128);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Size = new System.Drawing.Size(260, 20);
            this.txt联系电话.StyleController = this.Layout1;
            this.txt联系电话.TabIndex = 131;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(432, 73);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Size = new System.Drawing.Size(293, 20);
            this.txt出生日期.StyleController = this.Layout1;
            this.txt出生日期.TabIndex = 130;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(87, 97);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(260, 20);
            this.txt身份证号.StyleController = this.Layout1;
            this.txt身份证号.TabIndex = 129;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(432, 97);
            this.txt职业.Name = "txt职业";
            this.txt职业.Size = new System.Drawing.Size(293, 20);
            this.txt职业.StyleController = this.Layout1;
            this.txt职业.TabIndex = 128;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(87, 73);
            this.txt性别.Name = "txt性别";
            this.txt性别.Size = new System.Drawing.Size(260, 20);
            this.txt性别.StyleController = this.Layout1;
            this.txt性别.TabIndex = 127;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(432, 49);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(293, 20);
            this.txt姓名.StyleController = this.Layout1;
            this.txt姓名.TabIndex = 126;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(87, 49);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Size = new System.Drawing.Size(260, 20);
            this.txt个人档案号.StyleController = this.Layout1;
            this.txt个人档案号.TabIndex = 125;
            // 
            // txt尿微量蛋白
            // 
            this.txt尿微量蛋白.Location = new System.Drawing.Point(309, 660);
            this.txt尿微量蛋白.Name = "txt尿微量蛋白";
            this.txt尿微量蛋白.Size = new System.Drawing.Size(194, 20);
            this.txt尿微量蛋白.StyleController = this.Layout1;
            this.txt尿微量蛋白.TabIndex = 124;
            // 
            // txt终止理由
            // 
            this.txt终止理由.Location = new System.Drawing.Point(347, 735);
            this.txt终止理由.Name = "txt终止理由";
            this.txt终止理由.Size = new System.Drawing.Size(378, 20);
            this.txt终止理由.StyleController = this.Layout1;
            this.txt终止理由.TabIndex = 123;
            // 
            // txt终止管理
            // 
            this.txt终止管理.Location = new System.Drawing.Point(87, 711);
            this.txt终止管理.Name = "txt终止管理";
            this.txt终止管理.Size = new System.Drawing.Size(638, 20);
            this.txt终止管理.StyleController = this.Layout1;
            this.txt终止管理.TabIndex = 122;
            // 
            // txt生活自理能力
            // 
            this.txt生活自理能力.Location = new System.Drawing.Point(90, 539);
            this.txt生活自理能力.Name = "txt生活自理能力";
            this.txt生活自理能力.Size = new System.Drawing.Size(632, 20);
            this.txt生活自理能力.StyleController = this.Layout1;
            this.txt生活自理能力.TabIndex = 121;
            // 
            // txt体育锻炼
            // 
            this.txt体育锻炼.Location = new System.Drawing.Point(90, 515);
            this.txt体育锻炼.Name = "txt体育锻炼";
            this.txt体育锻炼.Size = new System.Drawing.Size(632, 20);
            this.txt体育锻炼.StyleController = this.Layout1;
            this.txt体育锻炼.TabIndex = 120;
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Location = new System.Drawing.Point(90, 491);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(632, 20);
            this.txt饮酒情况.StyleController = this.Layout1;
            this.txt饮酒情况.TabIndex = 119;
            // 
            // txt吸烟情况
            // 
            this.txt吸烟情况.Location = new System.Drawing.Point(90, 467);
            this.txt吸烟情况.Name = "txt吸烟情况";
            this.txt吸烟情况.Size = new System.Drawing.Size(632, 20);
            this.txt吸烟情况.StyleController = this.Layout1;
            this.txt吸烟情况.TabIndex = 118;
            // 
            // txt降糖药
            // 
            this.txt降糖药.Location = new System.Drawing.Point(90, 368);
            this.txt降糖药.Name = "txt降糖药";
            this.txt降糖药.Size = new System.Drawing.Size(632, 20);
            this.txt降糖药.StyleController = this.Layout1;
            this.txt降糖药.TabIndex = 117;
            // 
            // txt胰岛素
            // 
            this.txt胰岛素.Location = new System.Drawing.Point(90, 344);
            this.txt胰岛素.Name = "txt胰岛素";
            this.txt胰岛素.Size = new System.Drawing.Size(260, 20);
            this.txt胰岛素.StyleController = this.Layout1;
            this.txt胰岛素.TabIndex = 116;
            // 
            // txt目前症状
            // 
            this.txt目前症状.Location = new System.Drawing.Point(87, 234);
            this.txt目前症状.Name = "txt目前症状";
            this.txt目前症状.Size = new System.Drawing.Size(638, 20);
            this.txt目前症状.StyleController = this.Layout1;
            this.txt目前症状.TabIndex = 114;
            // 
            // txt家族史
            // 
            this.txt家族史.Location = new System.Drawing.Point(87, 210);
            this.txt家族史.Name = "txt家族史";
            this.txt家族史.Size = new System.Drawing.Size(638, 20);
            this.txt家族史.StyleController = this.Layout1;
            this.txt家族史.TabIndex = 113;
            // 
            // txt病例来源
            // 
            this.txt病例来源.Location = new System.Drawing.Point(87, 186);
            this.txt病例来源.Name = "txt病例来源";
            this.txt病例来源.Size = new System.Drawing.Size(638, 20);
            this.txt病例来源.StyleController = this.Layout1;
            this.txt病例来源.TabIndex = 112;
            // 
            // txt管理卡编号
            // 
            this.txt管理卡编号.Location = new System.Drawing.Point(87, 162);
            this.txt管理卡编号.Name = "txt管理卡编号";
            this.txt管理卡编号.Size = new System.Drawing.Size(260, 20);
            this.txt管理卡编号.StyleController = this.Layout1;
            this.txt管理卡编号.TabIndex = 111;
            // 
            // txt管理组别
            // 
            this.txt管理组别.Location = new System.Drawing.Point(432, 162);
            this.txt管理组别.Name = "txt管理组别";
            this.txt管理组别.Size = new System.Drawing.Size(293, 20);
            this.txt管理组别.StyleController = this.Layout1;
            this.txt管理组别.TabIndex = 110;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(9, 392);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(713, 71);
            this.gcDetail.TabIndex = 92;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 326;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 369;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(87, 783);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(175, 14);
            this.lab创建人.StyleController = this.Layout1;
            this.lab创建人.TabIndex = 107;
            this.lab创建人.Text = "录入人";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.Location = new System.Drawing.Point(87, 801);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(48, 14);
            this.lab当前所属机构.StyleController = this.Layout1;
            this.lab当前所属机构.TabIndex = 106;
            this.lab当前所属机构.Text = "所属机构";
            // 
            // txt胰岛素用量
            // 
            this.txt胰岛素用量.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt胰岛素用量.Lbl1Text = "pmol/L";
            this.txt胰岛素用量.Location = new System.Drawing.Point(435, 344);
            this.txt胰岛素用量.Margin = new System.Windows.Forms.Padding(0);
            this.txt胰岛素用量.Name = "txt胰岛素用量";
            this.txt胰岛素用量.Size = new System.Drawing.Size(287, 20);
            this.txt胰岛素用量.TabIndex = 104;
            this.txt胰岛素用量.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt餐后血糖
            // 
            this.txt餐后血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt餐后血糖.Lbl1Text = "mmol/L";
            this.txt餐后血糖.Location = new System.Drawing.Point(588, 612);
            this.txt餐后血糖.Margin = new System.Windows.Forms.Padding(0);
            this.txt餐后血糖.Name = "txt餐后血糖";
            this.txt餐后血糖.Size = new System.Drawing.Size(134, 20);
            this.txt餐后血糖.TabIndex = 98;
            this.txt餐后血糖.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(87, 759);
            this.txt发生时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt发生时间.Size = new System.Drawing.Size(175, 20);
            this.txt发生时间.StyleController = this.Layout1;
            this.txt发生时间.TabIndex = 76;
            // 
            // txt终止管理日期
            // 
            this.txt终止管理日期.EditValue = null;
            this.txt终止管理日期.Location = new System.Drawing.Point(87, 735);
            this.txt终止管理日期.Margin = new System.Windows.Forms.Padding(0);
            this.txt终止管理日期.Name = "txt终止管理日期";
            this.txt终止管理日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt终止管理日期.Size = new System.Drawing.Size(175, 20);
            this.txt终止管理日期.StyleController = this.Layout1;
            this.txt终止管理日期.TabIndex = 72;
            // 
            // txt胆固醇
            // 
            this.txt胆固醇.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt胆固醇.Lbl1Text = "mmol/L";
            this.txt胆固醇.Location = new System.Drawing.Point(90, 660);
            this.txt胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt胆固醇.Name = "txt胆固醇";
            this.txt胆固醇.Size = new System.Drawing.Size(134, 20);
            this.txt胆固醇.TabIndex = 63;
            this.txt胆固醇.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt甘油三脂
            // 
            this.txt甘油三脂.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt甘油三脂.Lbl1Text = "mmol/L";
            this.txt甘油三脂.Location = new System.Drawing.Point(588, 636);
            this.txt甘油三脂.Margin = new System.Windows.Forms.Padding(0);
            this.txt甘油三脂.Name = "txt甘油三脂";
            this.txt甘油三脂.Size = new System.Drawing.Size(134, 20);
            this.txt甘油三脂.TabIndex = 61;
            this.txt甘油三脂.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt低密度蛋白
            // 
            this.txt低密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt低密度蛋白.Lbl1Text = "mmol/L";
            this.txt低密度蛋白.Location = new System.Drawing.Point(309, 636);
            this.txt低密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt低密度蛋白.Name = "txt低密度蛋白";
            this.txt低密度蛋白.Size = new System.Drawing.Size(194, 20);
            this.txt低密度蛋白.TabIndex = 59;
            this.txt低密度蛋白.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt高密度蛋白
            // 
            this.txt高密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt高密度蛋白.Lbl1Text = "mmol/L";
            this.txt高密度蛋白.Location = new System.Drawing.Point(90, 636);
            this.txt高密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt高密度蛋白.Name = "txt高密度蛋白";
            this.txt高密度蛋白.Size = new System.Drawing.Size(134, 20);
            this.txt高密度蛋白.TabIndex = 57;
            this.txt高密度蛋白.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(309, 612);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(0);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(194, 20);
            this.txt空腹血糖.TabIndex = 55;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt腰围.Lbl1Text = "CM";
            this.txt腰围.Location = new System.Drawing.Point(588, 588);
            this.txt腰围.Margin = new System.Windows.Forms.Padding(0);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(134, 20);
            this.txt腰围.TabIndex = 51;
            this.txt腰围.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txtBMI
            // 
            this.txtBMI.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txtBMI.Lbl1Text = "kg/㎡";
            this.txtBMI.Location = new System.Drawing.Point(90, 684);
            this.txtBMI.Margin = new System.Windows.Forms.Padding(0);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Size = new System.Drawing.Size(632, 20);
            this.txtBMI.TabIndex = 49;
            this.txtBMI.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt体重.Lbl1Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(309, 588);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(194, 20);
            this.txt体重.TabIndex = 47;
            this.txt体重.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "CM";
            this.txt身高.Location = new System.Drawing.Point(90, 588);
            this.txt身高.Margin = new System.Windows.Forms.Padding(0);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(134, 20);
            this.txt身高.TabIndex = 45;
            this.txt身高.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(90, 612);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(0);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(134, 20);
            this.txt血压值.TabIndex = 53;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt确诊时间
            // 
            this.txt确诊时间.EditValue = null;
            this.txt确诊时间.Location = new System.Drawing.Point(262, 258);
            this.txt确诊时间.Name = "txt确诊时间";
            this.txt确诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt确诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt确诊时间.Size = new System.Drawing.Size(88, 20);
            this.txt确诊时间.StyleController = this.Layout1;
            this.txt确诊时间.TabIndex = 91;
            // 
            // txt确诊单位
            // 
            this.txt确诊单位.Location = new System.Drawing.Point(435, 258);
            this.txt确诊单位.Name = "txt确诊单位";
            this.txt确诊单位.Size = new System.Drawing.Size(290, 20);
            this.txt确诊单位.StyleController = this.Layout1;
            this.txt确诊单位.TabIndex = 93;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt糖化血红蛋白.Lbl1Text = "%";
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(588, 660);
            this.txt糖化血红蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(134, 20);
            this.txt糖化血红蛋白.TabIndex = 99;
            this.txt糖化血红蛋白.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(577, 783);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(48, 14);
            this.lab创建机构.StyleController = this.Layout1;
            this.lab创建机构.TabIndex = 86;
            this.lab创建机构.Text = "创建机构";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.Location = new System.Drawing.Point(347, 783);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(145, 14);
            this.lab最近修改人.StyleController = this.Layout1;
            this.lab最近修改人.TabIndex = 84;
            this.lab最近修改人.Text = "修改人 ";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.Location = new System.Drawing.Point(577, 759);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(122, 14);
            this.lab最近更新时间.StyleController = this.Layout1;
            this.lab最近更新时间.TabIndex = 80;
            this.lab最近更新时间.Text = "2015-07-26 10:14:18 ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(6, 31);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(719, 14);
            this.labelControl2.StyleController = this.Layout1;
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "考核项：25     缺项：25 完整度：0% ";
            // 
            // lab录入时间
            // 
            this.lab录入时间.Location = new System.Drawing.Point(347, 759);
            this.lab录入时间.Name = "lab录入时间";
            this.lab录入时间.Size = new System.Drawing.Size(145, 20);
            this.lab录入时间.StyleController = this.Layout1;
            this.lab录入时间.TabIndex = 78;
            this.lab录入时间.Text = "2015-07-26 10:14:18 ";
            // 
            // txt并发症情况
            // 
            this.txt并发症情况.Location = new System.Drawing.Point(87, 282);
            this.txt并发症情况.Name = "txt并发症情况";
            this.txt并发症情况.Size = new System.Drawing.Size(638, 36);
            this.txt并发症情况.StyleController = this.Layout1;
            this.txt并发症情况.TabIndex = 115;
            this.txt并发症情况.UseOptimizedRendering = true;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(432, 121);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Size = new System.Drawing.Size(293, 37);
            this.txt居住地址.StyleController = this.Layout1;
            this.txt居住地址.TabIndex = 132;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt糖尿病类型
            // 
            this.txt糖尿病类型.Location = new System.Drawing.Point(87, 258);
            this.txt糖尿病类型.Name = "txt糖尿病类型";
            this.txt糖尿病类型.Size = new System.Drawing.Size(90, 20);
            this.txt糖尿病类型.StyleController = this.Layout1;
            this.txt糖尿病类型.TabIndex = 89;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "糖尿病患者管理卡";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem58,
            this.layoutControlItem31,
            this.layoutControlItem6,
            this.layoutControlItem25,
            this.layoutControlItem43,
            this.layoutControlItem37,
            this.layoutControlItem36,
            this.layoutControlItem39,
            this.layoutControlItem41,
            this.layoutControlItem22,
            this.layoutControlItem42,
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutControlItem20,
            this.layoutControlGroup2,
            this.layoutControlGroup4,
            this.layoutControlItem24,
            this.layoutControlItem7,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem38,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem44,
            this.layoutControlItem40});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 821);
            this.layoutControlGroup1.Text = "糖尿病患者管理卡";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt发生时间;
            this.layoutControlItem4.CustomizationFormText = "发生时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 728);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(260, 29);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(153, 24);
            this.layoutControlItem4.Name = "dateEdit2item";
            this.layoutControlItem4.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "发生时间";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt糖尿病类型;
            this.layoutControlItem28.CustomizationFormText = "糖尿病类型";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 227);
            this.layoutControlItem28.Name = "comboBoxEdit1item";
            this.layoutControlItem28.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "糖尿病类型";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt确诊时间;
            this.layoutControlItem29.CustomizationFormText = "确诊时间";
            this.layoutControlItem29.Location = new System.Drawing.Point(175, 227);
            this.layoutControlItem29.Name = "comboBoxEdit2item";
            this.layoutControlItem29.Size = new System.Drawing.Size(173, 24);
            this.layoutControlItem29.Tag = "check";
            this.layoutControlItem29.Text = "确诊时间";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt确诊单位;
            this.layoutControlItem30.CustomizationFormText = "确诊单位";
            this.layoutControlItem30.Location = new System.Drawing.Point(348, 227);
            this.layoutControlItem30.Name = "dateEdit3item";
            this.layoutControlItem30.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "确诊单位";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.labelControl2;
            this.layoutControlItem58.CustomizationFormText = "labelControl2item";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem58.Name = "labelControl2item";
            this.layoutControlItem58.Size = new System.Drawing.Size(723, 18);
            this.layoutControlItem58.Text = "labelControl2item";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem58.TextToControlDistance = 0;
            this.layoutControlItem58.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.lab录入时间;
            this.layoutControlItem31.CustomizationFormText = "录入时间:";
            this.layoutControlItem31.Location = new System.Drawing.Point(260, 728);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(230, 29);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(225, 18);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "录入时间:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt终止管理日期;
            this.layoutControlItem6.CustomizationFormText = "终止管理日期";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 704);
            this.layoutControlItem6.Name = "dateEdit1item";
            this.layoutControlItem6.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem6.Text = "终止管理日期";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.lab当前所属机构;
            this.layoutControlItem25.CustomizationFormText = "所属机构";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 770);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(723, 18);
            this.layoutControlItem25.Text = "所属机构";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lab最近更新时间;
            this.layoutControlItem43.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem43.Location = new System.Drawing.Point(490, 728);
            this.layoutControlItem43.Name = "labelControl54item";
            this.layoutControlItem43.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem43.Text = "最近更新时间:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.lab创建机构;
            this.layoutControlItem37.CustomizationFormText = "创建机构:";
            this.layoutControlItem37.Location = new System.Drawing.Point(490, 752);
            this.layoutControlItem37.Name = "labelControl60item";
            this.layoutControlItem37.Size = new System.Drawing.Size(233, 18);
            this.layoutControlItem37.Text = "创建机构:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.lab创建人;
            this.layoutControlItem36.CustomizationFormText = "录入人";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 752);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(139, 18);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(260, 18);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "录入人";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.lab最近修改人;
            this.layoutControlItem39.CustomizationFormText = "修改人:";
            this.layoutControlItem39.Location = new System.Drawing.Point(260, 752);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(230, 29);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(143, 18);
            this.layoutControlItem39.Name = "labelControl58item";
            this.layoutControlItem39.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "修改人:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt管理组别;
            this.layoutControlItem41.CustomizationFormText = "管理组别";
            this.layoutControlItem41.Location = new System.Drawing.Point(345, 131);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem41.Tag = "check";
            this.layoutControlItem41.Text = "管理组别";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt管理卡编号;
            this.layoutControlItem22.CustomizationFormText = "管理卡编号";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(345, 24);
            this.layoutControlItem22.Text = "管理卡编号";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt病例来源;
            this.layoutControlItem42.CustomizationFormText = "病历来源";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem42.Tag = "check";
            this.layoutControlItem42.Text = "病历来源";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt家族史;
            this.layoutControlItem21.CustomizationFormText = "家族史";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 179);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "家族史";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt目前症状;
            this.layoutControlItem23.CustomizationFormText = "目前症状";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 203);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "目前症状";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem20.Control = this.txt并发症情况;
            this.layoutControlItem20.CustomizationFormText = "糖尿病并发症情况";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 251);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(50, 40);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(723, 40);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "糖尿病并发症情况";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(76, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "近期药物治疗情况";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.UCTxtLblitem,
            this.layout药物列表,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 291);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(723, 244);
            this.layoutControlGroup2.Text = "近期药物治疗情况";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt胰岛素;
            this.layoutControlItem1.CustomizationFormText = "胰岛素";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(345, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "胰岛素";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt降糖药;
            this.layoutControlItem2.CustomizationFormText = "口服降糖药";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "口服降糖药";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(78, 14);
            // 
            // UCTxtLblitem
            // 
            this.UCTxtLblitem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.UCTxtLblitem.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UCTxtLblitem.Control = this.txt胰岛素用量;
            this.UCTxtLblitem.CustomizationFormText = "胰岛素用量";
            this.UCTxtLblitem.Location = new System.Drawing.Point(345, 0);
            this.UCTxtLblitem.Name = "UCTxtLblitem";
            this.UCTxtLblitem.Size = new System.Drawing.Size(372, 24);
            this.UCTxtLblitem.Text = "胰岛素用量";
            this.UCTxtLblitem.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = " ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 48);
            this.layout药物列表.MaxSize = new System.Drawing.Size(0, 75);
            this.layout药物列表.MinSize = new System.Drawing.Size(219, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(717, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = " ";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt吸烟情况;
            this.layoutControlItem16.CustomizationFormText = "吸烟情况";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "吸烟情况";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt饮酒情况;
            this.layoutControlItem17.CustomizationFormText = "饮酒情况";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem17.Tag = "check";
            this.layoutControlItem17.Text = "饮酒情况";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt体育锻炼;
            this.layoutControlItem18.CustomizationFormText = "体育锻炼";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "体育锻炼";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt生活自理能力;
            this.layoutControlItem19.CustomizationFormText = "生活自理能力";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 195);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "生活自理能力";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "建档时的体检结果";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem15,
            this.layoutControlItem13,
            this.layoutControlItem27,
            this.layoutControlItem12,
            this.layoutControlItem3,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem14,
            this.layoutControlItem34,
            this.layoutControlItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 535);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(723, 145);
            this.layoutControlGroup4.Text = "建档时的体检结果";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt身高;
            this.layoutControlItem26.CustomizationFormText = "身高";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "item7";
            this.layoutControlItem26.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem26.Tag = "check";
            this.layoutControlItem26.Text = "身高";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt体重;
            this.layoutControlItem15.CustomizationFormText = "体重";
            this.layoutControlItem15.Location = new System.Drawing.Point(219, 0);
            this.layoutControlItem15.Name = "item6";
            this.layoutControlItem15.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "体重";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt腰围;
            this.layoutControlItem13.CustomizationFormText = "腰围";
            this.layoutControlItem13.Location = new System.Drawing.Point(498, 0);
            this.layoutControlItem13.Name = "item5";
            this.layoutControlItem13.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "腰围";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt血压值;
            this.layoutControlItem27.CustomizationFormText = "血压";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem27.Name = "UCTxtLblTxtLblitem";
            this.layoutControlItem27.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "血压";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt空腹血糖;
            this.layoutControlItem12.CustomizationFormText = "空腹血糖";
            this.layoutControlItem12.Location = new System.Drawing.Point(219, 24);
            this.layoutControlItem12.Name = "item4";
            this.layoutControlItem12.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "空腹血糖";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt餐后血糖;
            this.layoutControlItem3.CustomizationFormText = "餐后2h血糖";
            this.layoutControlItem3.Location = new System.Drawing.Point(498, 24);
            this.layoutControlItem3.Name = "item0";
            this.layoutControlItem3.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "餐后2h血糖";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt高密度蛋白;
            this.layoutControlItem11.CustomizationFormText = "高密度脂蛋白";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.Name = "item3";
            this.layoutControlItem11.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "高密度脂蛋白";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt低密度蛋白;
            this.layoutControlItem10.CustomizationFormText = "低密度脂蛋白";
            this.layoutControlItem10.Location = new System.Drawing.Point(219, 48);
            this.layoutControlItem10.Name = "item2";
            this.layoutControlItem10.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "低密度脂蛋白";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt甘油三脂;
            this.layoutControlItem9.CustomizationFormText = "甘油三脂";
            this.layoutControlItem9.Location = new System.Drawing.Point(498, 48);
            this.layoutControlItem9.Name = "item1";
            this.layoutControlItem9.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "甘油三脂";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt胆固醇;
            this.layoutControlItem8.CustomizationFormText = "胆固醇";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem8.Name = "item8";
            this.layoutControlItem8.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "胆固醇";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txtBMI;
            this.layoutControlItem14.CustomizationFormText = "BMI";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem14.Name = "item10";
            this.layoutControlItem14.Size = new System.Drawing.Size(717, 24);
            this.layoutControlItem14.Text = "BMI";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt糖化血红蛋白;
            this.layoutControlItem34.CustomizationFormText = "糖化血红蛋白";
            this.layoutControlItem34.Location = new System.Drawing.Point(498, 72);
            this.layoutControlItem34.Name = "item9";
            this.layoutControlItem34.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "糖化血红蛋白";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt尿微量蛋白;
            this.layoutControlItem5.CustomizationFormText = "尿微量蛋白";
            this.layoutControlItem5.Location = new System.Drawing.Point(219, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "尿微量蛋白";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txt终止管理;
            this.layoutControlItem24.CustomizationFormText = "是否终止管理";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 680);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(723, 24);
            this.layoutControlItem24.Text = "是否终止管理";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt终止理由;
            this.layoutControlItem7.CustomizationFormText = "终止理由";
            this.layoutControlItem7.Location = new System.Drawing.Point(260, 704);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(463, 24);
            this.layoutControlItem7.Text = "终止理由";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt个人档案号;
            this.layoutControlItem32.CustomizationFormText = "个人档案号";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(345, 24);
            this.layoutControlItem32.Text = "个人档案号";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.txt姓名;
            this.layoutControlItem33.CustomizationFormText = "姓名";
            this.layoutControlItem33.Location = new System.Drawing.Point(345, 18);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem33.Text = "姓名";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt性别;
            this.layoutControlItem35.CustomizationFormText = "性别";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(345, 24);
            this.layoutControlItem35.Text = "性别";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt职业;
            this.layoutControlItem38.CustomizationFormText = "职业";
            this.layoutControlItem38.Location = new System.Drawing.Point(345, 66);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem38.Text = "职业";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.txt联系电话;
            this.layoutControlItem45.CustomizationFormText = "联系电话";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 9, 2);
            this.layoutControlItem45.Size = new System.Drawing.Size(345, 41);
            this.layoutControlItem45.Text = "联系电话";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.txt居住地址;
            this.layoutControlItem46.CustomizationFormText = "居住地址";
            this.layoutControlItem46.Location = new System.Drawing.Point(345, 90);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(95, 41);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(378, 41);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "居住地址";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.txt出生日期;
            this.layoutControlItem44.CustomizationFormText = "出生日期";
            this.layoutControlItem44.Location = new System.Drawing.Point(345, 42);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem44.Text = "出生日期";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.txt身份证号;
            this.layoutControlItem40.CustomizationFormText = "身份证号";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(345, 24);
            this.layoutControlItem40.Text = "身份证号";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(78, 14);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Layout1);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 498);
            this.panel1.TabIndex = 3;
            // 
            // UC糖尿病患者管理卡_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Name = "UC糖尿病患者管理卡_显示";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC糖尿病患者管理卡_显示_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).EndInit();
            this.Layout1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿微量蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止理由.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体育锻炼.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt降糖药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt病例来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理组别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖尿病类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UCTxtLblitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl Layout1;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private Library.UserControls.UCTxtLbl txt胰岛素用量;
        private Library.UserControls.UCTxtLbl txt餐后血糖;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.DateEdit txt终止管理日期;
        private Library.UserControls.UCTxtLbl txt胆固醇;
        private Library.UserControls.UCTxtLbl txt甘油三脂;
        private Library.UserControls.UCTxtLbl txt低密度蛋白;
        private Library.UserControls.UCTxtLbl txt高密度蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLbl txt腰围;
        private Library.UserControls.UCTxtLbl txtBMI;
        private Library.UserControls.UCTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraEditors.DateEdit txt确诊时间;
        private DevExpress.XtraEditors.TextEdit txt确诊单位;
        private Library.UserControls.UCTxtLbl txt糖化血红蛋白;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lab录入时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.TextEdit txt生活自理能力;
        private DevExpress.XtraEditors.TextEdit txt体育锻炼;
        private DevExpress.XtraEditors.TextEdit txt饮酒情况;
        private DevExpress.XtraEditors.TextEdit txt吸烟情况;
        private DevExpress.XtraEditors.TextEdit txt降糖药;
        private DevExpress.XtraEditors.TextEdit txt胰岛素;
        private DevExpress.XtraEditors.TextEdit txt目前症状;
        private DevExpress.XtraEditors.TextEdit txt家族史;
        private DevExpress.XtraEditors.TextEdit txt病例来源;
        private DevExpress.XtraEditors.TextEdit txt管理卡编号;
        private DevExpress.XtraEditors.TextEdit txt管理组别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem UCTxtLblitem;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.MemoEdit txt并发症情况;
        private DevExpress.XtraEditors.TextEdit txt终止理由;
        private DevExpress.XtraEditors.TextEdit txt终止管理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit txt尿微量蛋白;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt糖尿病类型;
    }
}
