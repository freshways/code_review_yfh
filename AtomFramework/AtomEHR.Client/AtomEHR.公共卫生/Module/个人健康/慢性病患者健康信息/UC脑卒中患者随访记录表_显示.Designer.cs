﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC脑卒中患者随访记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC脑卒中患者随访记录表_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn新增 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt康复治疗方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访分类 = new DevExpress.XtraEditors.TextEdit();
            this.txt新发卒中症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑卒中并发症情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人病史 = new DevExpress.XtraEditors.TextEdit();
            this.txt目前症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt每次持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.txt辅助检查 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt新发卒中症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.txt随访方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑卒中类型 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑卒中部位 = new DevExpress.XtraEditors.TextEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.TextEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.TextEdit();
            this.txt生活自理能力 = new DevExpress.XtraEditors.TextEdit();
            this.txt遵医行为 = new DevExpress.XtraEditors.TextEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.TextEdit();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.TextEdit();
            this.txt不良反应 = new DevExpress.XtraEditors.TextEdit();
            this.txt用药情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt肢体功能恢复情况 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem83 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem86 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem87 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem89 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem90 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem91 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem92 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem93 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem94 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem95 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem96 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem97 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem85 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt康复治疗方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中并发症情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人病史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肢体功能恢复情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(103, 498);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(103, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(622, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn新增);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(618, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn新增
            // 
            this.btn新增.Image = ((System.Drawing.Image)(resources.GetObject("btn新增.Image")));
            this.btn新增.Location = new System.Drawing.Point(3, 3);
            this.btn新增.Name = "btn新增";
            this.btn新增.Size = new System.Drawing.Size(75, 23);
            this.btn新增.TabIndex = 4;
            this.btn新增.Text = "添加";
            this.btn新增.Click += new System.EventHandler(this.btn新增_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 5;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 6;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // sbtnExport
            // 
            this.sbtnExport.Location = new System.Drawing.Point(246, 3);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport.TabIndex = 7;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt康复治疗方式);
            this.layoutControl1.Controls.Add(this.txt随访分类);
            this.layoutControl1.Controls.Add(this.txt新发卒中症状);
            this.layoutControl1.Controls.Add(this.txt脑卒中并发症情况);
            this.layoutControl1.Controls.Add(this.txt个人病史);
            this.layoutControl1.Controls.Add(this.txt目前症状);
            this.layoutControl1.Controls.Add(this.txt每次持续时间);
            this.layoutControl1.Controls.Add(this.txt运动频率);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt体质指数);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.txt转诊科别);
            this.layoutControl1.Controls.Add(this.txt药物副作用详述);
            this.layoutControl1.Controls.Add(this.txt辅助检查);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.txt新发卒中症状其他);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab最近更新时间);
            this.layoutControl1.Controls.Add(this.lab创建时间);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt医生建议);
            this.layoutControl1.Controls.Add(this.txt随访方式);
            this.layoutControl1.Controls.Add(this.txt脑卒中类型);
            this.layoutControl1.Controls.Add(this.txt脑卒中部位);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt生活自理能力);
            this.layoutControl1.Controls.Add(this.txt遵医行为);
            this.layoutControl1.Controls.Add(this.txt摄盐情况1);
            this.layoutControl1.Controls.Add(this.txt摄盐情况2);
            this.layoutControl1.Controls.Add(this.txt不良反应);
            this.layoutControl1.Controls.Add(this.txt用药情况);
            this.layoutControl1.Controls.Add(this.txt转诊情况);
            this.layoutControl1.Controls.Add(this.txt肢体功能恢复情况);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem29});
            this.layoutControl1.Location = new System.Drawing.Point(103, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(618, 275, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(622, 466);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl2";
            // 
            // txt康复治疗方式
            // 
            this.txt康复治疗方式.Location = new System.Drawing.Point(108, 593);
            this.txt康复治疗方式.Margin = new System.Windows.Forms.Padding(0);
            this.txt康复治疗方式.Name = "txt康复治疗方式";
            this.txt康复治疗方式.Size = new System.Drawing.Size(494, 20);
            this.txt康复治疗方式.StyleController = this.layoutControl1;
            this.txt康复治疗方式.TabIndex = 6;
            // 
            // txt随访分类
            // 
            this.txt随访分类.Location = new System.Drawing.Point(108, 569);
            this.txt随访分类.Margin = new System.Windows.Forms.Padding(0);
            this.txt随访分类.Name = "txt随访分类";
            this.txt随访分类.Size = new System.Drawing.Size(494, 20);
            this.txt随访分类.StyleController = this.layoutControl1;
            this.txt随访分类.TabIndex = 20;
            // 
            // txt新发卒中症状
            // 
            this.txt新发卒中症状.Location = new System.Drawing.Point(108, 252);
            this.txt新发卒中症状.Name = "txt新发卒中症状";
            this.txt新发卒中症状.Size = new System.Drawing.Size(494, 20);
            this.txt新发卒中症状.StyleController = this.layoutControl1;
            this.txt新发卒中症状.TabIndex = 103;
            // 
            // txt脑卒中并发症情况
            // 
            this.txt脑卒中并发症情况.Location = new System.Drawing.Point(108, 252);
            this.txt脑卒中并发症情况.Name = "txt脑卒中并发症情况";
            this.txt脑卒中并发症情况.Size = new System.Drawing.Size(494, 20);
            this.txt脑卒中并发症情况.StyleController = this.layoutControl1;
            this.txt脑卒中并发症情况.TabIndex = 102;
            // 
            // txt个人病史
            // 
            this.txt个人病史.Location = new System.Drawing.Point(108, 228);
            this.txt个人病史.Name = "txt个人病史";
            this.txt个人病史.Size = new System.Drawing.Size(494, 20);
            this.txt个人病史.StyleController = this.layoutControl1;
            this.txt个人病史.TabIndex = 101;
            // 
            // txt目前症状
            // 
            this.txt目前症状.Location = new System.Drawing.Point(108, 204);
            this.txt目前症状.Margin = new System.Windows.Forms.Padding(0);
            this.txt目前症状.Name = "txt目前症状";
            this.txt目前症状.Size = new System.Drawing.Size(494, 20);
            this.txt目前症状.StyleController = this.layoutControl1;
            this.txt目前症状.TabIndex = 7;
            // 
            // txt每次持续时间
            // 
            this.txt每次持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt每次持续时间.Lbl1Text = "/";
            this.txt每次持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt每次持续时间.Lbl2Text = "(分钟/次)";
            this.txt每次持续时间.Location = new System.Drawing.Point(402, 422);
            this.txt每次持续时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt每次持续时间.Name = "txt每次持续时间";
            this.txt每次持续时间.Size = new System.Drawing.Size(197, 20);
            this.txt每次持续时间.TabIndex = 98;
            this.txt每次持续时间.Txt1EditValue = null;
            this.txt每次持续时间.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt每次持续时间.Txt2EditValue = null;
            this.txt每次持续时间.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(109, 422);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(0);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(184, 20);
            this.txt运动频率.TabIndex = 96;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(402, 398);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(197, 20);
            this.txt饮酒情况.TabIndex = 95;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(109, 398);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(184, 20);
            this.txt日吸烟量.TabIndex = 94;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(402, 299);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(197, 20);
            this.txt体质指数.TabIndex = 131;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(20, 14);
            this.txt体重.Lbl2Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(109, 299);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(184, 20);
            this.txt体重.TabIndex = 130;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Enabled = false;
            this.txt转诊原因.Location = new System.Drawing.Point(448, 717);
            this.txt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(154, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 129;
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Enabled = false;
            this.txt转诊科别.Location = new System.Drawing.Point(281, 717);
            this.txt转诊科别.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(108, 20);
            this.txt转诊科别.StyleController = this.layoutControl1;
            this.txt转诊科别.TabIndex = 117;
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Enabled = false;
            this.txt药物副作用详述.Location = new System.Drawing.Point(393, 545);
            this.txt药物副作用详述.Margin = new System.Windows.Forms.Padding(0);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(209, 20);
            this.txt药物副作用详述.StyleController = this.layoutControl1;
            this.txt药物副作用详述.TabIndex = 110;
            // 
            // txt辅助检查
            // 
            this.txt辅助检查.Location = new System.Drawing.Point(108, 497);
            this.txt辅助检查.Margin = new System.Windows.Forms.Padding(0);
            this.txt辅助检查.Name = "txt辅助检查";
            this.txt辅助检查.Size = new System.Drawing.Size(494, 20);
            this.txt辅助检查.StyleController = this.layoutControl1;
            this.txt辅助检查.TabIndex = 106;
            this.txt辅助检查.ToolTip = "123";
            this.txt辅助检查.ToolTipTitle = "1";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(3, 30);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(599, 14);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "考核项：31     缺项：0 完整度：100% ";
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(3, 642);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(599, 71);
            this.gcDetail.TabIndex = 94;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "药物名称";
            this.gridColumn5.FieldName = "药物名称";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 337;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "用法";
            this.gridColumn6.FieldName = "用法";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 370;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "个人档案编号";
            this.gridColumn7.FieldName = "个人档案编号";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "创建时间";
            this.gridColumn8.FieldName = "创建时间";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(395, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(207, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 73;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(108, 120);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(178, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 72;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(395, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(207, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 71;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(108, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(178, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 70;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(395, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(207, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 69;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(108, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(178, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 68;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(395, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(207, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 67;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(108, 48);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(178, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 66;
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(109, 275);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(184, 20);
            this.txt血压值.TabIndex = 64;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt腰围.Lbl1Text = "cm";
            this.txt腰围.Location = new System.Drawing.Point(402, 323);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(197, 20);
            this.txt腰围.TabIndex = 63;
            this.txt腰围.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // txt新发卒中症状其他
            // 
            this.txt新发卒中症状其他.Location = new System.Drawing.Point(108, 252);
            this.txt新发卒中症状其他.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.txt新发卒中症状其他.Name = "txt新发卒中症状其他";
            this.txt新发卒中症状其他.Properties.AutoHeight = false;
            this.txt新发卒中症状其他.Size = new System.Drawing.Size(494, 36);
            this.txt新发卒中症状其他.StyleController = this.layoutControl1;
            this.txt新发卒中症状其他.TabIndex = 56;
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(108, 849);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(185, 18);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 51;
            this.lab创建机构.Text = "labelControl16";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(402, 827);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(200, 18);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 50;
            this.lab最近修改人.Text = "labelControl15";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(108, 827);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(185, 18);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 49;
            this.lab创建人.Text = "labelControl14";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(402, 849);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(200, 18);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 48;
            this.lab当前所属机构.Text = "labelControl13";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近更新时间.Location = new System.Drawing.Point(402, 805);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(200, 18);
            this.lab最近更新时间.StyleController = this.layoutControl1;
            this.lab最近更新时间.TabIndex = 47;
            this.lab最近更新时间.Text = "labelControl12";
            // 
            // lab创建时间
            // 
            this.lab创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建时间.Location = new System.Drawing.Point(108, 805);
            this.lab创建时间.Name = "lab创建时间";
            this.lab创建时间.Size = new System.Drawing.Size(185, 18);
            this.lab创建时间.StyleController = this.layoutControl1;
            this.lab创建时间.TabIndex = 46;
            this.lab创建时间.Text = "labelControl11";
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(312, 781);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(290, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 45;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(108, 781);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Size = new System.Drawing.Size(105, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 44;
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(109, 323);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(184, 20);
            this.txt空腹血糖.TabIndex = 28;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(130, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(402, 275);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(197, 20);
            this.txt身高.TabIndex = 17;
            this.txt身高.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(108, 144);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Size = new System.Drawing.Size(178, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 12;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(108, 741);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(494, 36);
            this.txt医生建议.StyleController = this.layoutControl1;
            this.txt医生建议.TabIndex = 38;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // txt随访方式
            // 
            this.txt随访方式.Location = new System.Drawing.Point(395, 144);
            this.txt随访方式.Margin = new System.Windows.Forms.Padding(0);
            this.txt随访方式.Name = "txt随访方式";
            this.txt随访方式.Properties.AutoHeight = false;
            this.txt随访方式.Size = new System.Drawing.Size(207, 21);
            this.txt随访方式.StyleController = this.layoutControl1;
            this.txt随访方式.TabIndex = 13;
            // 
            // txt脑卒中类型
            // 
            this.txt脑卒中类型.EditValue = "";
            this.txt脑卒中类型.Location = new System.Drawing.Point(108, 169);
            this.txt脑卒中类型.Margin = new System.Windows.Forms.Padding(0);
            this.txt脑卒中类型.Name = "txt脑卒中类型";
            this.txt脑卒中类型.Properties.AutoHeight = false;
            this.txt脑卒中类型.Size = new System.Drawing.Size(494, 21);
            this.txt脑卒中类型.StyleController = this.layoutControl1;
            this.txt脑卒中类型.TabIndex = 99;
            // 
            // txt脑卒中部位
            // 
            this.txt脑卒中部位.Location = new System.Drawing.Point(108, 194);
            this.txt脑卒中部位.Name = "txt脑卒中部位";
            this.txt脑卒中部位.Size = new System.Drawing.Size(177, 20);
            this.txt脑卒中部位.StyleController = this.layoutControl1;
            this.txt脑卒中部位.TabIndex = 26;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(108, 521);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Size = new System.Drawing.Size(186, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 33;
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "";
            this.txt心理调整.Location = new System.Drawing.Point(402, 446);
            this.txt心理调整.Margin = new System.Windows.Forms.Padding(0);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Size = new System.Drawing.Size(197, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 128;
            // 
            // txt生活自理能力
            // 
            this.txt生活自理能力.Location = new System.Drawing.Point(393, 521);
            this.txt生活自理能力.Name = "txt生活自理能力";
            this.txt生活自理能力.Size = new System.Drawing.Size(209, 20);
            this.txt生活自理能力.StyleController = this.layoutControl1;
            this.txt生活自理能力.TabIndex = 36;
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.EditValue = "";
            this.txt遵医行为.Location = new System.Drawing.Point(109, 470);
            this.txt遵医行为.Margin = new System.Windows.Forms.Padding(0);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Properties.NullText = "请选择";
            this.txt遵医行为.Size = new System.Drawing.Size(184, 20);
            this.txt遵医行为.StyleController = this.layoutControl1;
            this.txt遵医行为.TabIndex = 104;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.EditValue = "";
            this.txt摄盐情况1.Location = new System.Drawing.Point(109, 446);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(73, 20);
            this.txt摄盐情况1.StyleController = this.layoutControl1;
            this.txt摄盐情况1.TabIndex = 126;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.EditValue = "";
            this.txt摄盐情况2.Location = new System.Drawing.Point(200, 446);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(93, 20);
            this.txt摄盐情况2.StyleController = this.layoutControl1;
            this.txt摄盐情况2.TabIndex = 127;
            // 
            // txt不良反应
            // 
            this.txt不良反应.EditValue = "";
            this.txt不良反应.Location = new System.Drawing.Point(108, 545);
            this.txt不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.txt不良反应.Name = "txt不良反应";
            this.txt不良反应.Properties.AutoHeight = false;
            this.txt不良反应.Size = new System.Drawing.Size(186, 20);
            this.txt不良反应.StyleController = this.layoutControl1;
            this.txt不良反应.TabIndex = 108;
            // 
            // txt用药情况
            // 
            this.txt用药情况.Location = new System.Drawing.Point(108, 617);
            this.txt用药情况.Name = "txt用药情况";
            this.txt用药情况.Properties.AutoHeight = false;
            this.txt用药情况.Size = new System.Drawing.Size(186, 21);
            this.txt用药情况.StyleController = this.layoutControl1;
            this.txt用药情况.TabIndex = 39;
            // 
            // txt转诊情况
            // 
            this.txt转诊情况.EditValue = "";
            this.txt转诊情况.Location = new System.Drawing.Point(108, 717);
            this.txt转诊情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊情况.Name = "txt转诊情况";
            this.txt转诊情况.Properties.AutoHeight = false;
            this.txt转诊情况.Size = new System.Drawing.Size(104, 20);
            this.txt转诊情况.StyleController = this.layoutControl1;
            this.txt转诊情况.TabIndex = 116;
            // 
            // txt肢体功能恢复情况
            // 
            this.txt肢体功能恢复情况.Location = new System.Drawing.Point(109, 347);
            this.txt肢体功能恢复情况.Name = "txt肢体功能恢复情况";
            this.txt肢体功能恢复情况.Size = new System.Drawing.Size(184, 20);
            this.txt肢体功能恢复情况.StyleController = this.layoutControl1;
            this.txt肢体功能恢复情况.TabIndex = 65;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt脑卒中部位;
            this.layoutControlItem28.CustomizationFormText = "心理调整";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 164);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem28.Name = "layoutControlItem23";
            this.layoutControlItem28.Size = new System.Drawing.Size(286, 25);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "脑卒中部位";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt脑卒中并发症情况;
            this.layoutControlItem50.CustomizationFormText = "脑卒中并发症情况";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem50.Tag = "check";
            this.layoutControlItem50.Text = "脑卒中并发症情况";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem51.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt新发卒中症状;
            this.layoutControlItem51.CustomizationFormText = "新发卒中症状";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem51.Tag = "check";
            this.layoutControlItem51.Text = "新发卒中症状";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt新发卒中症状其他;
            this.layoutControlItem29.CustomizationFormText = "其他新发卒中症状";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(161, 40);
            this.layoutControlItem29.Name = "layoutControlItem53";
            this.layoutControlItem29.Size = new System.Drawing.Size(603, 40);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Tag = "";
            this.layoutControlItem29.Text = "其他新发卒中症状";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem27,
            this.layoutControlGroup6,
            this.emptySpaceItem4,
            this.layoutControlItem31,
            this.emptySpaceItem2,
            this.layout药物列表,
            this.layoutControlItem34,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlGroup8,
            this.layoutControlItem74,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem77,
            this.layoutControlItem78,
            this.layoutControlItem79,
            this.layoutControlItem80,
            this.layoutControlItem81,
            this.layoutControlItem82,
            this.layoutControlItem83,
            this.layoutControlItem84,
            this.layoutControlItem86,
            this.layoutControlItem87,
            this.layoutControlItem89,
            this.layoutControlItem90,
            this.layoutControlItem91,
            this.layoutControlItem92,
            this.layoutControlItem93,
            this.layoutControlItem94,
            this.layoutControlItem95,
            this.layoutControlItem96,
            this.layoutControlItem97,
            this.layoutControlGroup7,
            this.layoutControlItem2,
            this.layoutControlItem49,
            this.layoutControlItem52,
            this.layoutControlItem85});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(605, 871);
            this.layoutControlGroup2.Text = "脑卒中患者随访记录表";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt发生时间;
            this.layoutControlItem11.CustomizationFormText = "随访日期";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(169, 22);
            this.layoutControlItem11.Name = "layoutControlItem9";
            this.layoutControlItem11.Size = new System.Drawing.Size(287, 25);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "随访日期";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt随访方式;
            this.layoutControlItem27.CustomizationFormText = "随访方式";
            this.layoutControlItem27.Location = new System.Drawing.Point(287, 114);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem27.Name = "layoutControlItem10";
            this.layoutControlItem27.Size = new System.Drawing.Size(316, 25);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "随访方式";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 841);
            this.layoutControlGroup6.Name = "layoutControlGroup5";
            this.layoutControlGroup6.Size = new System.Drawing.Size(603, 1);
            this.layoutControlGroup6.Text = "layoutControlGroup5";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 164);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(603, 10);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.txt用药情况;
            this.layoutControlItem31.CustomizationFormText = "用药情况";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 587);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem31.Name = "layoutControlItem36";
            this.layoutControlItem31.Size = new System.Drawing.Size(295, 25);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "用药情况";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem2.Location = new System.Drawing.Point(295, 587);
            this.emptySpaceItem2.Name = "emptySpaceItem3";
            this.emptySpaceItem2.Size = new System.Drawing.Size(308, 25);
            this.emptySpaceItem2.Text = "emptySpaceItem3";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = "layout药物列表";
            this.layout药物列表.Location = new System.Drawing.Point(0, 612);
            this.layout药物列表.MinSize = new System.Drawing.Size(212, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(603, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = "layout药物列表";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.labelControl1;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem34.Name = "layoutControlItem1";
            this.layoutControlItem34.Size = new System.Drawing.Size(603, 18);
            this.layoutControlItem34.Text = "layoutControlItem1";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.txt脑卒中类型;
            this.layoutControlItem37.CustomizationFormText = "脑卒中类型";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 139);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem37.Name = "layoutControlItem3";
            this.layoutControlItem37.Size = new System.Drawing.Size(603, 25);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Tag = "check";
            this.layoutControlItem37.Text = "卒中诊断";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Control = this.txt下次随访时间;
            this.layoutControlItem38.CustomizationFormText = "下次随访时间";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 751);
            this.layoutControlItem38.Name = "layoutControlItem41";
            this.layoutControlItem38.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem38.Tag = "check";
            this.layoutControlItem38.Text = "下次随访时间";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem39.Control = this.txt医生签名;
            this.layoutControlItem39.CustomizationFormText = "随访医生签名";
            this.layoutControlItem39.Location = new System.Drawing.Point(214, 751);
            this.layoutControlItem39.Name = "layoutControlItem42";
            this.layoutControlItem39.Size = new System.Drawing.Size(389, 24);
            this.layoutControlItem39.Tag = "check";
            this.layoutControlItem39.Text = "随访医生签名";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem40.Control = this.txt医生建议;
            this.layoutControlItem40.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 711);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(109, 40);
            this.layoutControlItem40.Name = "layoutControlItem35";
            this.layoutControlItem40.Size = new System.Drawing.Size(603, 40);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Tag = "check";
            this.layoutControlItem40.Text = "此次随访医生建议";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup8.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup8.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup8.CustomizationFormText = "生活行为";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem66,
            this.layoutControlItem67,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.emptySpaceItem6,
            this.layoutControlItem70,
            this.layoutControlItem71,
            this.layoutControlItem72,
            this.layoutControlItem73});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 345);
            this.layoutControlGroup8.Name = "layoutControlGroup3";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Size = new System.Drawing.Size(603, 122);
            this.layoutControlGroup8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutControlGroup8.Text = "生活方式指导";
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem66.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem66.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem66.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem66.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem66.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem66.Control = this.txt摄盐情况1;
            this.layoutControlItem66.CustomizationFormText = "摄盐情况";
            this.layoutControlItem66.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem66.Name = "layoutControlItem2";
            this.layoutControlItem66.Size = new System.Drawing.Size(182, 24);
            this.layoutControlItem66.Tag = "check";
            this.layoutControlItem66.Text = "摄盐情况";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.txt摄盐情况2;
            this.layoutControlItem67.CustomizationFormText = "~";
            this.layoutControlItem67.Location = new System.Drawing.Point(182, 48);
            this.layoutControlItem67.Name = "layoutControlItem4";
            this.layoutControlItem67.Size = new System.Drawing.Size(111, 24);
            this.layoutControlItem67.Text = "~";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem67.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem68.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem68.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem68.Control = this.txt心理调整;
            this.layoutControlItem68.CustomizationFormText = "心理调整";
            this.layoutControlItem68.Location = new System.Drawing.Point(293, 48);
            this.layoutControlItem68.Name = "layoutControlItem5";
            this.layoutControlItem68.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem68.Tag = "check";
            this.layoutControlItem68.Text = "心理调整";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem68.TextToControlDistance = 5;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem69.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem69.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem69.Control = this.txt遵医行为;
            this.layoutControlItem69.CustomizationFormText = "遵医行为";
            this.layoutControlItem69.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem69.Name = "layoutControlItem6";
            this.layoutControlItem69.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem69.Tag = "check";
            this.layoutControlItem69.Text = "遵医行为";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem69.TextToControlDistance = 5;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem6.Location = new System.Drawing.Point(293, 72);
            this.emptySpaceItem6.Name = "emptySpaceItem2";
            this.emptySpaceItem6.Size = new System.Drawing.Size(306, 24);
            this.emptySpaceItem6.Text = "emptySpaceItem2";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem70.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem70.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem70.Control = this.txt运动频率;
            this.layoutControlItem70.CustomizationFormText = "运动频率";
            this.layoutControlItem70.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem70.Name = "layoutControlItem37";
            this.layoutControlItem70.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem70.Tag = "check";
            this.layoutControlItem70.Text = "运动频率";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem70.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem71.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem71.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem71.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem71.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem71.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem71.Control = this.txt每次持续时间;
            this.layoutControlItem71.CustomizationFormText = "每次持续时间";
            this.layoutControlItem71.Location = new System.Drawing.Point(293, 24);
            this.layoutControlItem71.Name = "layoutControlItem38";
            this.layoutControlItem71.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem71.Tag = "check";
            this.layoutControlItem71.Text = "每次持续时间";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem71.TextToControlDistance = 5;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem72.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem72.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem72.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem72.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem72.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem72.Control = this.txt日吸烟量;
            this.layoutControlItem72.CustomizationFormText = "吸烟情况";
            this.layoutControlItem72.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem72.Name = "layoutControlItem12";
            this.layoutControlItem72.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem72.Tag = "check";
            this.layoutControlItem72.Text = "吸烟情况";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem73.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem73.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem73.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem73.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem73.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem73.Control = this.txt饮酒情况;
            this.layoutControlItem73.CustomizationFormText = "饮酒情况";
            this.layoutControlItem73.Location = new System.Drawing.Point(293, 0);
            this.layoutControlItem73.Name = "layoutControlItem32";
            this.layoutControlItem73.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem73.Tag = "check";
            this.layoutControlItem73.Text = "饮酒情况";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem74.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem74.Control = this.lab创建时间;
            this.layoutControlItem74.CustomizationFormText = "layoutControlItem43";
            this.layoutControlItem74.Location = new System.Drawing.Point(0, 775);
            this.layoutControlItem74.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem74.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem74.Name = "layoutControlItem43";
            this.layoutControlItem74.Size = new System.Drawing.Size(294, 22);
            this.layoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem74.Text = "录入时间";
            this.layoutControlItem74.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem74.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem74.TextToControlDistance = 0;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem75.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem75.Control = this.lab最近更新时间;
            this.layoutControlItem75.CustomizationFormText = "layoutControlItem44";
            this.layoutControlItem75.Location = new System.Drawing.Point(294, 775);
            this.layoutControlItem75.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem75.Name = "layoutControlItem44";
            this.layoutControlItem75.Size = new System.Drawing.Size(309, 22);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Text = "最近更新时间";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem75.TextToControlDistance = 0;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem76.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem76.Control = this.lab创建人;
            this.layoutControlItem76.CustomizationFormText = "layoutControlItem46";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 797);
            this.layoutControlItem76.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem76.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem76.Name = "layoutControlItem46";
            this.layoutControlItem76.Size = new System.Drawing.Size(294, 22);
            this.layoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem76.Text = "录入人";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem76.TextToControlDistance = 0;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem77.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem77.Control = this.lab最近修改人;
            this.layoutControlItem77.CustomizationFormText = "layoutControlItem47";
            this.layoutControlItem77.Location = new System.Drawing.Point(294, 797);
            this.layoutControlItem77.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem77.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem77.Name = "layoutControlItem47";
            this.layoutControlItem77.Size = new System.Drawing.Size(309, 22);
            this.layoutControlItem77.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem77.Text = "最近更新人";
            this.layoutControlItem77.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem77.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem77.TextToControlDistance = 0;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem78.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem78.Control = this.lab创建机构;
            this.layoutControlItem78.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem78.Location = new System.Drawing.Point(0, 819);
            this.layoutControlItem78.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem78.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem78.Name = "layoutControlItem48";
            this.layoutControlItem78.Size = new System.Drawing.Size(294, 22);
            this.layoutControlItem78.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem78.Text = "创建机构";
            this.layoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem78.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem78.TextToControlDistance = 0;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem79.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem79.Control = this.lab当前所属机构;
            this.layoutControlItem79.CustomizationFormText = "layoutControlItem45";
            this.layoutControlItem79.Location = new System.Drawing.Point(294, 819);
            this.layoutControlItem79.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem79.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem79.Name = "layoutControlItem45";
            this.layoutControlItem79.Size = new System.Drawing.Size(309, 22);
            this.layoutControlItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem79.Text = "当前所属机构";
            this.layoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem79.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem79.TextToControlDistance = 0;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem80.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem80.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem80.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem80.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem80.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem80.Control = this.txt辅助检查;
            this.layoutControlItem80.CustomizationFormText = "辅助检查*";
            this.layoutControlItem80.Location = new System.Drawing.Point(0, 467);
            this.layoutControlItem80.Name = "layoutControlItem7";
            this.layoutControlItem80.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem80.Tag = "check";
            this.layoutControlItem80.Text = "辅助检查*";
            this.layoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem80.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem80.TextToControlDistance = 5;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem81.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem81.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem81.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem81.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem81.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem81.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem81.Control = this.txt服药依从性;
            this.layoutControlItem81.CustomizationFormText = "服药依从性";
            this.layoutControlItem81.Location = new System.Drawing.Point(0, 491);
            this.layoutControlItem81.Name = "layoutControlItem30";
            this.layoutControlItem81.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem81.Tag = "check";
            this.layoutControlItem81.Text = "用药依从性";
            this.layoutControlItem81.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem81.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem81.TextToControlDistance = 5;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem82.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem82.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem82.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem82.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem82.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem82.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem82.Control = this.txt生活自理能力;
            this.layoutControlItem82.CustomizationFormText = "生活能否自理能力";
            this.layoutControlItem82.Location = new System.Drawing.Point(295, 491);
            this.layoutControlItem82.Name = "layoutControlItem33";
            this.layoutControlItem82.Size = new System.Drawing.Size(308, 24);
            this.layoutControlItem82.Tag = "check";
            this.layoutControlItem82.Text = "生活能否自理能力";
            this.layoutControlItem82.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem82.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem82.TextToControlDistance = 5;
            // 
            // layoutControlItem83
            // 
            this.layoutControlItem83.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem83.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem83.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem83.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem83.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem83.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem83.Control = this.txt不良反应;
            this.layoutControlItem83.CustomizationFormText = "药物不良反应";
            this.layoutControlItem83.Location = new System.Drawing.Point(0, 515);
            this.layoutControlItem83.Name = "layoutControlItem8";
            this.layoutControlItem83.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem83.Tag = "check";
            this.layoutControlItem83.Text = "药物不良反应";
            this.layoutControlItem83.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem83.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem83.TextToControlDistance = 5;
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem84.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem84.Control = this.txt药物副作用详述;
            this.layoutControlItem84.CustomizationFormText = "副作用详述";
            this.layoutControlItem84.Location = new System.Drawing.Point(295, 515);
            this.layoutControlItem84.Name = "layoutControlItem11";
            this.layoutControlItem84.Size = new System.Drawing.Size(308, 24);
            this.layoutControlItem84.Text = "副作用详述";
            this.layoutControlItem84.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem84.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem84.TextToControlDistance = 5;
            // 
            // layoutControlItem86
            // 
            this.layoutControlItem86.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem86.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem86.Control = this.txt转诊科别;
            this.layoutControlItem86.CustomizationFormText = "机构及科别";
            this.layoutControlItem86.Location = new System.Drawing.Point(213, 687);
            this.layoutControlItem86.Name = "layoutControlItem27";
            this.layoutControlItem86.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem86.Text = "机构及科别";
            this.layoutControlItem86.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem86.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem86.TextToControlDistance = 5;
            // 
            // layoutControlItem87
            // 
            this.layoutControlItem87.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem87.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem87.Control = this.txt转诊原因;
            this.layoutControlItem87.CustomizationFormText = "原因";
            this.layoutControlItem87.Location = new System.Drawing.Point(390, 687);
            this.layoutControlItem87.Name = "layoutControlItem28";
            this.layoutControlItem87.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem87.Text = "原因";
            this.layoutControlItem87.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem87.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem87.TextToControlDistance = 5;
            // 
            // layoutControlItem89
            // 
            this.layoutControlItem89.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem89.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem89.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem89.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem89.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem89.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem89.Control = this.txt转诊情况;
            this.layoutControlItem89.CustomizationFormText = "转诊情况";
            this.layoutControlItem89.Location = new System.Drawing.Point(0, 687);
            this.layoutControlItem89.Name = "layoutControlItem39";
            this.layoutControlItem89.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem89.Tag = "check";
            this.layoutControlItem89.Text = "转诊情况";
            this.layoutControlItem89.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem89.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem89.TextToControlDistance = 5;
            // 
            // layoutControlItem90
            // 
            this.layoutControlItem90.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem90.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem90.Control = this.txt性别;
            this.layoutControlItem90.CustomizationFormText = "性别";
            this.layoutControlItem90.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem90.Name = "layoutControlItem19";
            this.layoutControlItem90.Size = new System.Drawing.Size(287, 24);
            this.layoutControlItem90.Text = "性别";
            this.layoutControlItem90.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem90.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem90.TextToControlDistance = 5;
            // 
            // layoutControlItem91
            // 
            this.layoutControlItem91.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem91.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem91.Control = this.txt出生日期;
            this.layoutControlItem91.CustomizationFormText = "出生日期";
            this.layoutControlItem91.Location = new System.Drawing.Point(287, 42);
            this.layoutControlItem91.Name = "layoutControlItem20";
            this.layoutControlItem91.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItem91.Text = "出生日期";
            this.layoutControlItem91.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem91.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem91.TextToControlDistance = 5;
            // 
            // layoutControlItem92
            // 
            this.layoutControlItem92.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem92.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem92.Control = this.txt联系电话;
            this.layoutControlItem92.CustomizationFormText = "联系电话";
            this.layoutControlItem92.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem92.Name = "layoutControlItem24";
            this.layoutControlItem92.Size = new System.Drawing.Size(287, 24);
            this.layoutControlItem92.Text = "联系电话";
            this.layoutControlItem92.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem92.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem92.TextToControlDistance = 5;
            // 
            // layoutControlItem93
            // 
            this.layoutControlItem93.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem93.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem93.Control = this.txt居住地址;
            this.layoutControlItem93.CustomizationFormText = "居住地址";
            this.layoutControlItem93.Location = new System.Drawing.Point(287, 90);
            this.layoutControlItem93.Name = "layoutControlItem26";
            this.layoutControlItem93.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItem93.Text = "居住地址";
            this.layoutControlItem93.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem93.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem93.TextToControlDistance = 5;
            // 
            // layoutControlItem94
            // 
            this.layoutControlItem94.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem94.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem94.Control = this.txt身份证号;
            this.layoutControlItem94.CustomizationFormText = "身份证号";
            this.layoutControlItem94.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem94.Name = "layoutControlItem21";
            this.layoutControlItem94.Size = new System.Drawing.Size(287, 24);
            this.layoutControlItem94.Text = "身份证号";
            this.layoutControlItem94.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem94.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem94.TextToControlDistance = 5;
            // 
            // layoutControlItem95
            // 
            this.layoutControlItem95.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem95.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem95.Control = this.txt职业;
            this.layoutControlItem95.CustomizationFormText = "职业";
            this.layoutControlItem95.Location = new System.Drawing.Point(287, 66);
            this.layoutControlItem95.Name = "layoutControlItem22";
            this.layoutControlItem95.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItem95.Text = "职业";
            this.layoutControlItem95.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem95.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem95.TextToControlDistance = 5;
            // 
            // layoutControlItem96
            // 
            this.layoutControlItem96.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem96.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem96.Control = this.txt个人档案号;
            this.layoutControlItem96.CustomizationFormText = "个人档案号";
            this.layoutControlItem96.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem96.Name = "layoutControlItem17";
            this.layoutControlItem96.Size = new System.Drawing.Size(287, 24);
            this.layoutControlItem96.Text = "个人档案号";
            this.layoutControlItem96.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem96.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem96.TextToControlDistance = 5;
            // 
            // layoutControlItem97
            // 
            this.layoutControlItem97.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem97.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem97.Control = this.txt姓名;
            this.layoutControlItem97.CustomizationFormText = "姓名";
            this.layoutControlItem97.Location = new System.Drawing.Point(287, 18);
            this.layoutControlItem97.Name = "layoutControlItem18";
            this.layoutControlItem97.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItem97.Text = "姓名";
            this.layoutControlItem97.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem97.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem97.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "体  征";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem54,
            this.emptySpaceItem5,
            this.layoutControlItem60,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem63,
            this.layoutControlItem64,
            this.layoutControlItem65});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 222);
            this.layoutControlGroup7.Name = "layoutControlGroup2";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(603, 123);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutControlGroup7.Text = "体  征";
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem54.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.txt肢体功能恢复情况;
            this.layoutControlItem54.CustomizationFormText = "肢体功能恢复情况";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem54.Name = "layoutControlItem16";
            this.layoutControlItem54.Size = new System.Drawing.Size(293, 25);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Tag = "check";
            this.layoutControlItem54.Text = "肢体功能恢复情况";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem5.Location = new System.Drawing.Point(293, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem1";
            this.emptySpaceItem5.Size = new System.Drawing.Size(306, 25);
            this.emptySpaceItem5.Text = "emptySpaceItem1";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem60.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.txt血压值;
            this.layoutControlItem60.CustomizationFormText = "血压值";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(203, 24);
            this.layoutControlItem60.Name = "layoutControlItem15";
            this.layoutControlItem60.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Tag = "check";
            this.layoutControlItem60.Text = "血压值";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.txt身高;
            this.layoutControlItem61.CustomizationFormText = "身高";
            this.layoutControlItem61.Location = new System.Drawing.Point(293, 0);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem61.Name = "layoutControlItem14";
            this.layoutControlItem61.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Tag = "";
            this.layoutControlItem61.Text = "身高";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem62.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.txt体重;
            this.layoutControlItem62.CustomizationFormText = "体重";
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem62.Name = "layoutControlItem29";
            this.layoutControlItem62.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem62.Tag = "check";
            this.layoutControlItem62.Text = "体重";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.txt体质指数;
            this.layoutControlItem63.CustomizationFormText = "体质指数";
            this.layoutControlItem63.Location = new System.Drawing.Point(293, 24);
            this.layoutControlItem63.Name = "layoutControlItem31";
            this.layoutControlItem63.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem63.Text = "体质指数";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem64.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem64.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem64.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem64.Control = this.txt空腹血糖;
            this.layoutControlItem64.CustomizationFormText = "空腹血糖";
            this.layoutControlItem64.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem64.Name = "layoutControlItem25";
            this.layoutControlItem64.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem64.Tag = "";
            this.layoutControlItem64.Text = "空腹血糖";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem64.TextToControlDistance = 5;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem65.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem65.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem65.Control = this.txt腰围;
            this.layoutControlItem65.CustomizationFormText = "腰围";
            this.layoutControlItem65.Location = new System.Drawing.Point(293, 48);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(203, 24);
            this.layoutControlItem65.Name = "layoutControlItem13";
            this.layoutControlItem65.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Tag = "";
            this.layoutControlItem65.Text = "腰围";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem65.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt目前症状;
            this.layoutControlItem2.CustomizationFormText = "目前症状";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "目前症状";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.txt个人病史;
            this.layoutControlItem49.CustomizationFormText = "个人病史";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 198);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem49.Tag = "check";
            this.layoutControlItem49.Text = "个人病史";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem52.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt随访分类;
            this.layoutControlItem52.CustomizationFormText = "此次随访分类";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 539);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem52.Tag = "check";
            this.layoutControlItem52.Text = "此次随访分类";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem85
            // 
            this.layoutControlItem85.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem85.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem85.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem85.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem85.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem85.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem85.Control = this.txt康复治疗方式;
            this.layoutControlItem85.CustomizationFormText = "康复治疗的方式";
            this.layoutControlItem85.Location = new System.Drawing.Point(0, 563);
            this.layoutControlItem85.Name = "layoutControlItem85";
            this.layoutControlItem85.Size = new System.Drawing.Size(603, 24);
            this.layoutControlItem85.Tag = "check";
            this.layoutControlItem85.Text = "康复治疗的方式";
            this.layoutControlItem85.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem85.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem85.TextToControlDistance = 5;
            // 
            // UC脑卒中患者随访记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC脑卒中患者随访记录表_显示";
            this.Size = new System.Drawing.Size(725, 498);
            this.Load += new System.EventHandler(this.UC脑卒中患者随访记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt康复治疗方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中并发症情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人病史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肢体功能恢复情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn新增;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt目前症状;
        private Library.UserControls.UCTxtLblTxtLbl txt每次持续时间;
        private Library.UserControls.UCTxtLblTxtLbl txt运动频率;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private DevExpress.XtraEditors.TextEdit txt辅助检查;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private Library.UserControls.UCTxtLbl txt腰围;
        private DevExpress.XtraEditors.TextEdit txt新发卒中症状其他;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建时间;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLbl txt身高;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraEditors.TextEdit txt随访方式;
        private DevExpress.XtraEditors.TextEdit txt脑卒中类型;
        private DevExpress.XtraEditors.TextEdit txt脑卒中部位;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem83;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem86;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem87;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem89;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem90;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem91;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem92;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem93;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem94;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem95;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem96;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem97;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txt新发卒中症状;
        private DevExpress.XtraEditors.TextEdit txt脑卒中并发症情况;
        private DevExpress.XtraEditors.TextEdit txt个人病史;
        private DevExpress.XtraEditors.TextEdit txt服药依从性;
        private DevExpress.XtraEditors.TextEdit txt心理调整;
        private DevExpress.XtraEditors.TextEdit txt生活自理能力;
        private DevExpress.XtraEditors.TextEdit txt遵医行为;
        private DevExpress.XtraEditors.TextEdit txt摄盐情况1;
        private DevExpress.XtraEditors.TextEdit txt摄盐情况2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraEditors.TextEdit txt康复治疗方式;
        private DevExpress.XtraEditors.TextEdit txt随访分类;
        private DevExpress.XtraEditors.TextEdit txt不良反应;
        private DevExpress.XtraEditors.TextEdit txt用药情况;
        private DevExpress.XtraEditors.TextEdit txt转诊情况;
        private DevExpress.XtraEditors.TextEdit txt肢体功能恢复情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem85;
        private DevExpress.XtraEditors.SimpleButton sbtnExport;
    }
}
