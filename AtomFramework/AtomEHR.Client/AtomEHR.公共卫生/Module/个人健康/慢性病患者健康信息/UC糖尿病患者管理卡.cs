﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC糖尿病患者管理卡 : UserControlBase
    {
        DataRow[] _dr个人信息 = null;
        public UC糖尿病患者管理卡()
        {
            InitializeComponent();
        }

        public UC糖尿病患者管理卡(DataRow[] dr, UpdateType _UpdateType)
        {
            base._UpdateType = _UpdateType;
            _dr个人信息 = dr;
            _BLL = new bllMXB糖尿病管理卡();
            InitializeComponent();

            //默认绑定
            txt个人档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();        
        }

        private void UC糖尿病患者管理卡_Load(object sender, EventArgs e)
        {
            DataTable dt = _BLL.GetBusinessByKey(txt个人档案编号.Text, true).Tables[tb_MXB糖尿病管理卡.__TableName];

            if (dt != null && dt.Rows.Count > 0)
            {
                DoBindingSummaryEditor(dt);
            }

            //绑定最新体检数据
            if (_dr个人信息 != null)
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.身高] = Return(_dr个人信息[0][tb_健康体检.身高].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.体重] = Return(_dr个人信息[0][tb_健康体检.体重].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.BMI] = Return(_dr个人信息[0][tb_健康体检.体重指数].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.腰围] = Return(_dr个人信息[0][tb_健康体检.腰围].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.收缩压] = Return(_dr个人信息[0][tb_健康体检.血压左侧1].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.舒张压] = Return(_dr个人信息[0][tb_健康体检.血压左侧2].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.空腹血糖] = Return(_dr个人信息[0][tb_健康体检.空腹血糖].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.高密度蛋白] = Return(_dr个人信息[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.低密度蛋白] = Return(_dr个人信息[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.甘油三脂] = Return(_dr个人信息[0][tb_健康体检.甘油三酯].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.总胆固醇] = Return(_dr个人信息[0][tb_健康体检.总胆固醇].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.体检日期] = Return(_dr个人信息[0][tb_健康体检.体检日期].ToString());
                string str尿蛋白 = _dr个人信息[0][tb_健康体检.尿微量白蛋白].ToString();
                if (!string.IsNullOrEmpty(str尿蛋白) && str尿蛋白 != "0" && str尿蛋白 != "0.00")
                {
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.尿微量蛋白] = '1'; //有
                }
                else
                {
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.尿微量蛋白] = "2";//无
                }
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.糖化血红蛋白] = Return(_dr个人信息[0][tb_健康体检.糖化血红蛋白].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.吸烟情况] = Return(_dr个人信息[0][tb_健康体检.吸烟状况].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.饮酒情况] = Return(_dr个人信息[0][tb_健康体检.饮酒频率].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.锻炼频率] = Return(_dr个人信息[0][tb_健康体检.锻炼频率].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.生活自理能力] = Return(_dr个人信息[0][tb_健康体检.老年人自理评估].ToString());
            }

            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡_用药情况.__TableName];

            DataBinder.BindingLookupEditDataSource(txt糖尿病类型, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='tnbfx' ")), "P_DESC", "P_CODE");

            this.txt身高.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt1.Leave += 身高_Leave;
            this.txtBMI.Txt1.Enter += BMI_Enter;
        }

        object Return(string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                    return "0";
                else
                    return value;
            }
            catch
            {
                return "0";
            }
        }

        #region MBI

        void 身高_Leave(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txtBMI.Txt1);
        }

        void BMI_Enter(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txtBMI.Txt1);
        } 
        #endregion

        private void btn修改保存_Click(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.None) return;


            Set考核项颜色_new(this.Layout1, labelControl2);

            _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.家族史] = GetFlowLayoutResult(this.fl家族史);
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.目前症状] = GetFlowLayoutResult(this.fl症状);

            _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.完整度] = _base完整度;

            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = _base缺项 + "," + _base完整度;
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0].AcceptChanges();
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0].SetModified();

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                this._UpdateType = UpdateType.None;
                Msg.ShowInformation("保存成功!");
                //保存成功后跳转到显示页面
                UC糖尿病患者管理卡_显示 uc = new UC糖尿病患者管理卡_显示(_dr个人信息);
                ShowControl(uc);
            }
            else
                Msg.Warning("保存失败!");
        }

        #region 保存检查
        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt管理卡编号.Text)))
            //{
            //    Msg.Warning("管理卡编号不能为空!");
            //    txt管理卡编号.Focus();
            //    return false;
            //}

            #region 是否纳入高血压管理
            int gxyzc1 = this.txt血压值.Txt1.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt血压值.Txt1.Text);
            int gxyzc2 = this.txt血压值.Txt2.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt血压值.Txt2.Text);
            if (gxyzc1 >= 140 || gxyzc2 >= 90)//符合高血压的特征
            {
                if (_BLL.CurrentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)//并且不存在高血压管理卡
                {
                    if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))//点击确定
                    {
                        DataRow rowGXYGLK = _BLL.CurrentBusiness.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                        AddNewGXYGLK(rowGXYGLK);
                    }
                }
            }
            #endregion

            return true;
        }

        private void AddNewGXYGLK(DataRow rowGXYGLK)
        {
            rowGXYGLK[tb_MXB高血压管理卡.管理卡编号] = "";
            rowGXYGLK[tb_MXB高血压管理卡.个人档案编号] = this.txt个人档案编号.Text.ToString();
            rowGXYGLK[tb_MXB高血压管理卡.所属机构] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.所属机构];
            rowGXYGLK[tb_MXB高血压管理卡.创建机构] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.创建机构];
            rowGXYGLK[tb_MXB高血压管理卡.发生时间] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.发生时间];
            rowGXYGLK[tb_MXB高血压管理卡.创建时间] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.创建时间];
            rowGXYGLK[tb_MXB高血压管理卡.创建人] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.创建人];
            rowGXYGLK[tb_MXB高血压管理卡.修改人] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.修改人];
            rowGXYGLK[tb_MXB高血压管理卡.修改时间] = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.修改时间];
            //TODO:     tb_MXB高血压管理卡   缺项
            rowGXYGLK[tb_MXB高血压管理卡.缺项] = "20";
            rowGXYGLK[tb_MXB高血压管理卡.完整度] = "0";

            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "20,0";
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";

            DataRow row = _BLL.CurrentBusiness.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            row[tb_健康档案_既往病史.个人档案编号] = this.txt个人档案编号.Text.ToString();
            row[tb_健康档案_既往病史.疾病类型] = "疾病";
            row[tb_健康档案_既往病史.疾病名称] = "2";
            row[tb_健康档案_既往病史.D_JBBM] = "高血压";
            row[tb_健康档案_既往病史.日期] = txt发生时间.Text == "" ? DateTime.Now.ToString("yyyy-MM") : txt发生时间.DateTime.ToString("yyyy-MM");
        } 
        #endregion

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            //dataSource.Rows[0][tb_MXB糖尿病管理卡.个人档案编号] = lab个人档案号.Text;
            //dataSource.Rows[0][tb_MXB糖尿病管理卡.所属机构] = lab当前所属机构.Text;

            ////TextEdit
            DataBinder.BindingTextEdit(txt管理卡编号, dataSource, tb_MXB糖尿病管理卡.管理卡编号);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB糖尿病管理卡.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB糖尿病管理卡.体重);
            DataBinder.BindingTextEdit(txtBMI.Txt1, dataSource, tb_MXB糖尿病管理卡.BMI);
            DataBinder.BindingTextEdit(txt腰围.Txt1, dataSource, tb_MXB糖尿病管理卡.腰围);
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB糖尿病管理卡.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB糖尿病管理卡.舒张压);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB糖尿病管理卡.空腹血糖);
            DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB糖尿病管理卡.高密度蛋白);
            DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB糖尿病管理卡.低密度蛋白);
            DataBinder.BindingTextEdit(txt甘油三脂.Txt1, dataSource, tb_MXB糖尿病管理卡.甘油三脂);
            DataBinder.BindingTextEdit(txt胆固醇.Txt1, dataSource, tb_MXB糖尿病管理卡.总胆固醇);
            DataBinder.BindingTextEdit(txt餐后血糖.Txt1, dataSource, tb_MXB糖尿病管理卡.餐后血糖);
            DataBinder.BindingTextEdit(txt糖化血红蛋白.Txt1, dataSource, tb_MXB糖尿病管理卡.糖化血红蛋白);
            DataBinder.BindingTextEdit(txtBMI.Txt1, dataSource, tb_MXB糖尿病管理卡.BMI);
            DataBinder.BindingTextEdit(txt终止管理日期, dataSource, tb_MXB糖尿病管理卡.终止管理日期);
            DataBinder.BindingTextEdit(txt发生时间, dataSource, tb_MXB糖尿病管理卡.发生时间);
            DataBinder.BindingTextEdit(txt目前症状其他, dataSource, tb_MXB糖尿病管理卡.目前症状其他);
            DataBinder.BindingTextEdit(txt糖尿病类型, dataSource, tb_MXB糖尿病管理卡.糖尿病类型);
            DataBinder.BindingTextEdit(txt确诊时间, dataSource, tb_MXB糖尿病管理卡.确诊时间);
            DataBinder.BindingTextEdit(txt确诊单位, dataSource, tb_MXB糖尿病管理卡.确诊单位);
            DataBinder.BindingTextEdit(txt肾脏.Txt1, dataSource, tb_MXB糖尿病管理卡.肾脏病变);
            DataBinder.BindingTextEdit(txt神经.Txt1, dataSource, tb_MXB糖尿病管理卡.神经病变);
            DataBinder.BindingTextEdit(txt心脏.Txt1, dataSource, tb_MXB糖尿病管理卡.心脏病变);
            DataBinder.BindingTextEdit(txt视网膜.Txt1, dataSource, tb_MXB糖尿病管理卡.视网膜病变);
            DataBinder.BindingTextEdit(txt足部.Txt1, dataSource, tb_MXB糖尿病管理卡.足部病变);
            DataBinder.BindingTextEdit(txt脑血管.Txt1, dataSource, tb_MXB糖尿病管理卡.脑血管病变);
            DataBinder.BindingTextEdit(txt胰岛素用量.Txt1, dataSource, tb_MXB糖尿病管理卡.胰岛素用量);

            if ((dataSource.Rows[0][tb_MXB糖尿病管理卡.肾脏病变]!= null && dataSource.Rows[0][tb_MXB糖尿病管理卡.肾脏病变].ToString() != "0")
                || (dataSource.Rows[0][tb_MXB糖尿病管理卡.神经病变] != null && dataSource.Rows[0][tb_MXB糖尿病管理卡.神经病变].ToString() != "0")
                || (dataSource.Rows[0][tb_MXB糖尿病管理卡.心脏病变] != null && dataSource.Rows[0][tb_MXB糖尿病管理卡.心脏病变].ToString() != "0")
                || (dataSource.Rows[0][tb_MXB糖尿病管理卡.视网膜病变] != null && dataSource.Rows[0][tb_MXB糖尿病管理卡.视网膜病变].ToString() != "0")
                || (dataSource.Rows[0][tb_MXB糖尿病管理卡.足部病变] != null && dataSource.Rows[0][tb_MXB糖尿病管理卡.足部病变].ToString() != "0")
                || (dataSource.Rows[0][tb_MXB糖尿病管理卡.脑血管病变] != null && dataSource.Rows[0][tb_MXB糖尿病管理卡.脑血管病变].ToString() != "0"))
                checkEdit并发症情况.Checked = false;

            ////RadioEdit
            DataBinder.BindingRadioEdit(radio管理组别, dataSource, tb_MXB糖尿病管理卡.管理组别);
            DataBinder.BindingRadioEdit(radio病例来源, dataSource, tb_MXB糖尿病管理卡.病例来源);
            DataBinder.BindingRadioEdit(radio胰岛素, dataSource, tb_MXB糖尿病管理卡.胰岛素);
            DataBinder.BindingRadioEdit(radio降糖药, dataSource, tb_MXB糖尿病管理卡.口服降糖药);
            DataBinder.BindingRadioEdit(radio吸烟情况, dataSource, tb_MXB糖尿病管理卡.吸烟情况);
            DataBinder.BindingRadioEdit(radio饮酒情况, dataSource, tb_MXB糖尿病管理卡.饮酒情况);
            DataBinder.BindingRadioEdit(radio体育锻炼, dataSource, tb_MXB糖尿病管理卡.锻炼频率);
            DataBinder.BindingRadioEdit(radio生活自理能力, dataSource, tb_MXB糖尿病管理卡.生活自理能力);
            DataBinder.BindingRadioEdit(radio尿微量蛋白, dataSource, tb_MXB糖尿病管理卡.尿微量蛋白);
            DataBinder.BindingRadioEdit(radio终止管理, dataSource, tb_MXB糖尿病管理卡.终止管理);
            DataBinder.BindingRadioEdit(radio终止理由, dataSource, tb_MXB糖尿病管理卡.终止理由);

            //flowLayoutPanel2
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病管理卡.家族史].ToString(), fl家族史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病管理卡.目前症状].ToString(), fl症状);

            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.所属机构].ToString()); 
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.修改时间].ToString();

            //设置控件颜色
            Set考核项颜色_new(this.Layout1, labelControl2);
        }

        #region 使用药物-子表

        private void btn添加药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物.Tag, gcDetail);
        }

        private void btn删除药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物.Tag, gcDetail);
        }

        protected override void CreateOneDetail(GridView gridView)
        {
            gvDetail.MoveLast();

            DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡_用药情况.__TableName].NewRow();
            //添加用法说明默认值
            row[tb_MXB糖尿病管理卡_用药情况.用法] = "每日  次，每次  mg";
            row[tb_MXB糖尿病管理卡_用药情况.个人档案编号] = this.txt个人档案编号.Text;
            row[tb_MXB糖尿病管理卡_用药情况.创建时间] = lab创建时间.Text;

            _BLL.CurrentBusiness.Tables[tb_MXB糖尿病管理卡_用药情况.__TableName].Rows.Add(row); //增加一条明细记录

            gcDetail.RefreshDataSource();
            gvDetail.FocusedRowHandle = gvDetail.RowCount - 1;

            gvDetail.FocusedColumn = gvDetail.VisibleColumns[0];
        }
        #endregion

        #region 页面控制

        private void checkEdit并发症情况_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkEdit并发症情况.Checked)
                layout糖尿病并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            else
                layout糖尿病并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        private void jzs_ck以上都无_CheckedChanged(object sender, EventArgs e)
        {
            string 点击名称 = ((DevExpress.XtraEditors.CheckEdit)sender).Text;

            #region 以上都无
            if (jzs_ck以上都无.Text == 点击名称)
            {
                if (jzs_ck以上都无.Checked)
                {
                    jzs_ck拒答.Checked = false;
                    jzs_ck拒答.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;
                    return;
                }
                else
                {
                    jzs_ck拒答.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    return;
                }
            }
            #endregion

            #region 不详
            if (jzs_ck不详.Text == 点击名称)
            {
                if (jzs_ck不详.Checked)
                {
                    jzs_ck拒答.Checked = false;
                    jzs_ck拒答.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;

                    jzs_ck以上都无.Checked = false;
                    jzs_ck以上都无.Enabled = false;
                    return;
                }
                else
                {
                    jzs_ck拒答.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    jzs_ck以上都无.Enabled = true;
                    return;
                }
            }
            #endregion

            #region 拒答

            if (jzs_ck拒答.Text == 点击名称)
            {
                if (jzs_ck拒答.Checked)
                {
                    jzs_ck拒答.Checked = false;
                    jzs_ck拒答.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;

                    jzs_ck以上都无.Checked = false;
                    jzs_ck以上都无.Enabled = false;

                    jzs_ck不详.Checked = false;
                    jzs_ck不详.Enabled = false;
                    return;
                }
                else
                {
                    jzs_ck拒答.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    jzs_ck以上都无.Enabled = true;
                    jzs_ck不详.Enabled = true;
                    return;
                }
            }
            #endregion

        }

        private void ck无症状_CheckedChanged(object sender, EventArgs e)
        {
            if (ck无症状.Checked)
                SetFlCheckEnabled(fl症状, false, ck无症状.Text);
            else
                SetFlCheckEnabled(fl症状, true, ck无症状.Text);
        }
        private void SetFlCheckEnabled(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Enabled = value;
                }
            }
        }

        private void radio降糖药_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.radio降糖药.EditValue !=null && this.radio降糖药.EditValue.ToString() == "1")
            {
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void radio终止管理_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.radio终止管理.EditValue!=null && this.radio终止管理.EditValue.ToString() == "1")
            {
                layout终止管理.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout终止理由.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                layout终止管理.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout终止理由.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void ck症状其他_CheckedChanged(object sender, EventArgs e)
        {
            if (ck症状其他.Checked)
                this.txt目前症状其他.Visible = false;
            else
            {
                this.txt目前症状其他.Visible = true;
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病管理卡.目前症状其他] = "";
            }
        }


        private void radio胰岛素_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio胰岛素.EditValue != null && radio胰岛素.EditValue.ToString() == "1")
                this.txt胰岛素用量.Enabled = true;
            else
            {
                this.txt胰岛素用量.Enabled = false;
                this.txt胰岛素用量.Txt1.Text = "";
            }
        }       

        #endregion


    }
}
