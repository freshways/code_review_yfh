﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC糖尿病患者随访记录表 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        DataRow _dr高血压 = null;
        string _ID = "";
        public UC糖尿病患者随访记录表()
        {
            InitializeComponent();
        }

        public UC糖尿病患者随访记录表(DataRow[] dr, UpdateType _UpdateType, object ID, DataRow dr高血压信息)
        {
            base._UpdateType = _UpdateType;
            _dr个人档案信息 = dr;
            _dr高血压 = dr高血压信息;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bllMXB糖尿病随访表();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txt体征其他.Text = "无";//体征其他默认“无”
            this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            //this.txt身高.Txt1.Text = dr[0][tb_健康体检.身高].ToString();
        }

        private void UC糖尿病患者随访记录表_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.个人档案编号] = _dr个人档案信息[0][tb_健康档案.个人档案编号].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.姓名] = util.DESEncrypt.DES解密(_dr个人档案信息[0][tb_健康档案.姓名].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.性别] = _dr个人档案信息[0][tb_健康档案.性别].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.身份证号] = _dr个人档案信息[0][tb_健康档案.身份证号].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.出生日期] = _dr个人档案信息[0][tb_健康档案.出生日期].ToString();
                //绑定联系电话
                string str联系电话 = _dr个人档案信息[0][tb_健康档案.本人电话].ToString();
                if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
                {
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.联系电话] = _dr个人档案信息[0][tb_健康档案.本人电话].ToString();
                }
                else
                {
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.联系电话] = _dr个人档案信息[0][tb_健康档案.联系人电话].ToString();
                }
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.省] = _dr个人档案信息[0][tb_健康档案.省].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.市] = _dr个人档案信息[0][tb_健康档案.市].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.区] = _dr个人档案信息[0][tb_健康档案.区].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.街道] = _dr个人档案信息[0][tb_健康档案.街道].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.居委会] = _dr个人档案信息[0][tb_健康档案.居委会].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.居住地址] = _dr个人档案信息[0][tb_健康档案.居住地址].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.职业] = _dr个人档案信息[0][tb_健康档案.职业].ToString();

                //判断绑定的身高是否为空(当体检表中的身高为空时报错)
                if (_UpdateType == UpdateType.Add)
                {
                    if (string.IsNullOrEmpty(_dr个人档案信息[0][tb_健康体检.身高].ToString()))
                    {
                        _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.身高] = 0.00;
                    }
                    else
                    {
                        _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.身高] = _dr个人档案信息[0][tb_健康体检.身高].ToString();
                    }

                    //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.身高] = _dr个人档案信息[0][tb_健康体检.身高].ToString();
                }

                #region 从高血压随访带入信息
                if (_dr高血压 != null)
                {
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.发生时间] = _dr高血压[tb_MXB高血压随访表.发生时间];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.收缩压] = _dr高血压[tb_MXB高血压随访表.收缩压];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.舒张压] = _dr高血压[tb_MXB高血压随访表.舒张压];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.体重] = _dr高血压[tb_MXB高血压随访表.体重];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.体重2] = _dr高血压[tb_MXB高血压随访表.体重2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.身高] = _dr高血压[tb_MXB高血压随访表.身高];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.BMI] = _dr高血压[tb_MXB高血压随访表.BMI];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.BMI2] = _dr高血压[tb_MXB高血压随访表.BMI2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.吸烟数量] = _dr高血压[tb_MXB高血压随访表.吸烟数量];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.吸烟数量2] = _dr高血压[tb_MXB高血压随访表.吸烟数量2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.饮酒数量] = _dr高血压[tb_MXB高血压随访表.饮酒数量];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.饮酒数量2] = _dr高血压[tb_MXB高血压随访表.饮酒数量2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.运动频率] = _dr高血压[tb_MXB高血压随访表.运动频率];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.运动频率2] = _dr高血压[tb_MXB高血压随访表.运动频率2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.运动持续时间] = _dr高血压[tb_MXB高血压随访表.运动持续时间];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.运动持续时间2] = _dr高血压[tb_MXB高血压随访表.运动持续时间2];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.本次随访分类] = _dr高血压[tb_MXB高血压随访表.本次随访分类];
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.下次随访时间] = _dr高血压[tb_MXB高血压随访表.下次随访时间];
                } 
                #endregion
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                if (_ID != null && _ID != "")
                {
                    DataTable dt = ((bllMXB糖尿病随访表)_BLL).GetBusinessByKeyEdit(_ID, true).Tables[tb_MXB糖尿病随访表.__TableName];
                }
                else return;
            }

            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表.__TableName]);
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName];
            gc药物调整.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName];

            //初始化
            Init();
            //设置颜色
            if (_UpdateType == UpdateType.Modify)
                Set考核项颜色_new(layoutControl1, lab考核项);

            if (_UpdateType == UpdateType.Add)
            {
                checkEdit多饮.CheckedChanged += checkEdit症状_CheckedChanged;
                checkEdit多尿.CheckedChanged += checkEdit症状_CheckedChanged;
                checkEdit12.CheckedChanged += checkEdit症状_CheckedChanged;
                txt症状其他.EditValueChanged += txt症状其他_EditValueChanged;
                txt血压值.Leave += txt血压值_Leave;
                txt空腹血糖.Leave += txt空腹血糖_Leave;
                txt备注.TextChanged += txt备注_TextChanged;

                checkEdit控制满意.CheckedChanged += ck随访分类_CheckedChanged;
                checkEdit控制不满意.CheckedChanged += ck随访分类_CheckedChanged;
                checkEdit不良反应.CheckedChanged += ck随访分类_CheckedChanged;
                ck随访并发症.CheckedChanged += ck随访分类_CheckedChanged;
                radio管理措施.EditValueChanged+=radio管理措施_EditValueChanged;
                txt发生时间.EditValueChanged += txt发生时间_EditValueChanged;
            }


            is孕产妇 = new bll健康档案().Is孕产妇(_dr个人档案信息[0][tb_健康档案.个人档案编号].ToString());
            m_dt下一步管理措施 = ((bllMXB糖尿病随访表)_BLL).Get全部随访下次管理措施(txt个人档案号.Text);
            if (m_dt下一步管理措施 != null)
            {
                m_dt下一步管理措施.Columns.Add("下一步管理措施名称");
            }

            for (int index = 0; index < m_dt下一步管理措施.Rows.Count; index++)
            {
                string stritemvalue = m_dt下一步管理措施.Rows[index][tb_MXB糖尿病随访表.下一步管理措施].ToString();
                if (!string.IsNullOrWhiteSpace(stritemvalue))
                {
                    m_dt下一步管理措施.Rows[index]["下一步管理措施名称"] = radio管理措施.Properties.Items.GetItemByValue(stritemvalue).Description;
                }
            }
        }

        

        #region 初始化

        void Init()//初始化
        {
            DataBinder.BindingLookupEditDataSource(txt足背动脉搏动, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='cjwcj' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt心理调整, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='lhybjc' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt遵医行为, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='lhybjc' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt服药依从性, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='fyycx-mb' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt低血糖反应, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='mb_dxtfy' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt转诊结果, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='sfdw' ")), "P_DESC", "P_CODE");

            btn添加药物.Click += btn添加药物_Click;
            btn删除药物.Click += btn删除药物_Click;

            btn添加药物调整.Click += btn添加药物调整_Click;
            btn删除药物调整.Click += btn删除药物调整_Click;

            this.txt身高.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt2.Leave += 身高_Leave;
            this.txt体质指数.Txt1.Enter += BMI_Enter;
            this.txt体质指数.Txt2.Enter += BMI_Enter;
        }
        void 身高_Leave(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        void BMI_Enter(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        #endregion

        #region 保存相关
        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (!Msg.AskQuestion("信息保存后，‘随访日期’将不允许修改，确认保存信息？")) return;
            if (_UpdateType == UpdateType.None) return;

            if (!ValidatingSummaryData()) return; //检查主表数据合法性
            //设置颜色
            Set考核项颜色_new(layoutControl1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.发生时间] = this.txt发生时间.Text;
            //根据用户要求，下次随访时间默认“发生时间”的3个月以后
            if (string.IsNullOrEmpty(txt下次随访时间.Text))
            {
                string str发生时间 = this.txt发生时间.Text;
                this.txt下次随访时间.Text = Convert.ToDateTime(str发生时间).AddMonths(3).ToShortDateString();
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.下次随访时间] = this.txt下次随访时间.Text;
            }
            else
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.下次随访时间] = this.txt下次随访时间.Text;
            }
            //_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.下次随访时间] = this.txt下次随访时间.Text;

            //绑定检查日期
            if (string.IsNullOrEmpty(txt检查日期.Text))
            {
                this.txt检查日期.Text = this.txt发生时间.Text;
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.辅助检查日期] = this.txt检查日期.Text;
            }
            else
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.辅助检查日期] = this.txt检查日期.Text;
            }

            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.目前症状] = GetFlowLayoutResult(this.fl症状);
            //判断空腹血糖值，自动绑定分类类型
            //string str空腹血糖 = this.txt空腹血糖.Txt1.Text;
            //if (Convert.ToDecimal(str空腹血糖)>=7)
            //{
            //    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.本次随访分类] = "2";//控制不满意
            //}
            //else//Convert.ToDecimal(str空腹血糖)<7
            //{
            //    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.本次随访分类] = "1";//控制满意
            //}
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.本次随访分类] = GetFlowLayoutResult(this.fl随访分类);
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.慢病并发症] = GetFlowLayoutResult(this.fl随访分类并发症);
            //添加随访方式为其他的填空项
            if (_BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.随访方式].ToString() == "4")
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.随访方式其他] = this.txt随访方式其他.Text;
            }
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.完整度] = _base完整度;

            _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.访视情况] = GetFlowLayoutResult(this.fl访视情况);
            if (chk失访.Checked)
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.失访原因] = GetFlowLayoutResult(this.fl失访原因);
            }
            else if (chk死亡.Checked)
            {
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.死亡日期] = dte死亡日期.Text;
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.死亡原因] = txt死亡原因.Text;
            }

            //更新手签 ▽
            if (b手签更新)
            {
                using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                {
                    this.txt居民签名.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.居民签名] = Convert.ToBase64String(arr1);
                }
                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人或家属签字] = this.textEdit本人或家属签字.Text.Trim();
                //_BLL.DataBinder.Rows[0][tb_MXB高血压随访表.] = _BLL.ServiceDateTime;
            }
            //更新手签 △

            
            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人
                ((bllMXB糖尿病随访表)BLL).Set个人健康特征(txt个人档案号.Text);
                
                Msg.ShowInformation("保存成功!");
                if (_UpdateType == UpdateType.Add) //新增随访时添加判断
                {
                    if (((bllMXB糖尿病随访表)BLL).Check高血压随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为高血压患者，若录入高血压随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC高血压患者随访记录表 ctl高血压 = new UC高血压患者随访记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl高血压, DockStyle.Fill);
                            return;
                        }
                    }
                    if (((bllMXB糖尿病随访表)BLL).Check脑卒中随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为脑卒中患者，若录入脑卒中随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC脑卒中患者随访记录表 ctl脑卒中 = new UC脑卒中患者随访记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl脑卒中, DockStyle.Fill);
                            return;
                        }
                    }
                    if (((bllMXB糖尿病随访表)BLL).Check冠心病随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为冠心病患者，若录入冠心病随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC冠心病患者随访服务记录表 ctl冠心病 = new UC冠心病患者随访服务记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl冠心病, DockStyle.Fill);
                            return;
                        }
                    }
                }
                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面
                UC糖尿病患者随访记录表_显示 control = new UC糖尿病患者随访记录表_显示(_dr个人档案信息, this.lab创建时间.Text);
                ShowControl(control, DockStyle.Fill);

                try
                {
                    if (Convert.ToDecimal(txt空腹血糖.Txt1.Text.Trim()) >= 10)
                    {
                        DataSet ds = ((bllMXB糖尿病随访表)_BLL).Get签约医生(Loginer.CurrentUser.所属机构);
                        DataTable dt = ds.Tables[0];
                        string rows = "";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string 身份证号 = dt.Rows[i]["签约医生身份证号"].ToString();
                            string 体检日期 = this.txt发生时间.Text;
                            string 个人档案编号 = this.txt个人档案号.Text;
                            string 姓名 = txt姓名.Text;
                            string[] keyWord = new string[2];
                            keyWord[0] = "血糖偏高，已划入三级管理。血糖值[" + txt空腹血糖.Txt1.Text + "]";
                            keyWord[1] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            if (!string.IsNullOrEmpty(身份证号))
                                ((bllMXB糖尿病随访表)_BLL).SendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc"
                                , 身份证号, "sfgl", "姓名[" + 姓名 + "],电话[" + txt联系电话.Text + "],地址[" + txt居住地址.Text + "]！", "请及时联系该居民进行指导", 个人档案编号, keyWord);//点击详情查看
                            rows = (i + 1).ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Msg.Warning("此人血糖偏高，微信推送给大夫失败，请及时推荐病人到医院就诊!");
                }
            }
            else
                Msg.Warning("保存失败!");
        }
        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {

            if (string.IsNullOrEmpty(ConvertEx.ToString(txt发生时间.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                txt发生时间.Focus();
                return false;
            }

            if(chk失访.Checked || chk死亡.Checked)
            {
                return true;
            }

            StringBuilder sbNotity = new StringBuilder();

            #region 允许修改下次随访时间(设置默认值)
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt下次随访时间.Text)))
            //{
            //    Msg.Warning("下次随访日期不能为空!");
            //    txt下次随访时间.Focus();
            //    return false;
            //}
            #endregion

            if (this.txt发生时间.DateTime > Convert.ToDateTime(lab创建时间.Text))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                txt发生时间.Focus();
                return false;
            }

            //判断“空腹血糖”是否已填
            if (string.IsNullOrEmpty(txt空腹血糖.Txt1.Text.Trim()))
            {
                Msg.Warning("请填写空腹血糖值！");
                txt空腹血糖.Focus();
                return false;
            }
            else if (Convert.ToDecimal(txt空腹血糖.Txt1.Text) < 3.9m || Convert.ToDecimal(txt空腹血糖.Txt1.Text) > 16.7m)
            {
                sbNotity.Append("空腹血糖："+txt空腹血糖.Txt1.Text +" 不在3.9--16.7mmol/L 范围之内\n");
            }
            else
            { }

            if (radio随访方式.EditValue == null
               || string.IsNullOrWhiteSpace(radio随访方式.EditValue.ToString())
               || (radio随访方式.EditValue.ToString() == "4" && string.IsNullOrWhiteSpace(txt随访方式其他.Text))
               )
            {
                Msg.Warning("随访方式不能为空!");
                radio随访方式.Focus();
                return false;
            }

            if (string.IsNullOrWhiteSpace(GetFlowLayoutResult(this.fl症状).ToString()))
            {
                Msg.Warning("症状不能为空!");
                fl症状.Focus();
                return false;
            }


            //判断"血压值"是否填写完整
            if (string.IsNullOrEmpty(txt血压值.Txt1.Text.Trim()) || string.IsNullOrEmpty(txt血压值.Txt2.Text.Trim()))
            {
                Msg.Warning("请完整填写血压值!");
                txt血压值.Focus();
                return false;
            }
            else
            {
                if (( int.Parse(txt血压值.Txt1.Text) > 140 || int.Parse(txt血压值.Txt1.Text) < 90))
                {
                    sbNotity.Append("收缩压：" + txt血压值.Txt1.Text + "，不在90--140之间\n");
                }

                if ( (int.Parse(txt血压值.Txt2.Text) > 90 || int.Parse(txt血压值.Txt2.Text) < 60))
                {
                    sbNotity.Append("舒张压：" + txt血压值.Txt2.Text + "，不在60--90之间\n");
                }
            }

            if (string.IsNullOrWhiteSpace(txt体重.Txt1.Text) || string.IsNullOrWhiteSpace(txt体重.Txt2.Text))
            {
            }
            else
            {
                if (Convert.ToDecimal(txt体重.Txt1.Text) > 100m || Convert.ToDecimal(txt体重.Txt1.Text) < 40m
                    || (Convert.ToDecimal(txt体重.Txt2.Text) > 100m || Convert.ToDecimal(txt体重.Txt2.Text) < 40m))
                {
                    sbNotity.Append("体重：" + txt体重.Txt1.Text + "/" + txt体重.Txt2.Text + "，不在40--100之间\n");
                }
            }

            if (!string.IsNullOrWhiteSpace(txt体质指数.Txt1.Text) && !string.IsNullOrWhiteSpace(txt体质指数.Txt2.Text))
            {
                if (Convert.ToDecimal(txt体质指数.Txt1.Text) > 28m || Convert.ToDecimal(txt体质指数.Txt1.Text) < 18.5m
                    || Convert.ToDecimal(txt体质指数.Txt2.Text) > 28m || Convert.ToDecimal(txt体质指数.Txt2.Text) < 18.5m
                   )
                {
                    sbNotity.Append("体质指数：" + txt体质指数.Txt1.Text + "/" + txt体质指数.Txt2.Text + "，不在18.5--28之间\n");
                }
            }

            //足背动脉搏动
            if (txt足背动脉搏动.EditValue==null || string.IsNullOrWhiteSpace(txt足背动脉搏动.EditValue.ToString()))
            {
                Msg.Warning("请填写足背动脉搏动!");
                txt足背动脉搏动.Focus();
                return false;
            }

            //生活指导
            //生活方式指导 ▼
            if ((string.IsNullOrWhiteSpace(txt日吸烟量.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt日吸烟量.Txt2.Text))
                 )
            {
                Msg.Warning("日吸烟量不能为空!");
                txt日吸烟量.Focus();
                return false;
            }
            else
            {
                if (int.Parse(txt日吸烟量.Txt1.Text) < 0 || int.Parse(txt日吸烟量.Txt1.Text) > 20
                    || int.Parse(txt日吸烟量.Txt2.Text) < 0 || int.Parse(txt日吸烟量.Txt2.Text) > 20)
                {
                    sbNotity.Append("日吸烟量：" + txt日吸烟量.Txt1.Text + "/" + txt日吸烟量.Txt2.Text + "，不在0--20之间\n");
                }
            }

            if ((string.IsNullOrWhiteSpace(txt饮酒情况.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt饮酒情况.Txt2.Text))
                 )
            {
                Msg.Warning("饮酒情况不能为空!");
                txt饮酒情况.Focus();
                return false;
            }
            else
            {
                if (Convert.ToDecimal(txt饮酒情况.Txt1.Text) < 0m || Convert.ToDecimal(txt饮酒情况.Txt1.Text) > 10m
                    || Convert.ToDecimal(txt饮酒情况.Txt2.Text) < 0m || Convert.ToDecimal(txt饮酒情况.Txt2.Text) > 10m)
                {
                    sbNotity.Append("日饮酒量：" + txt饮酒情况.Txt1.Text + "/" + txt饮酒情况.Txt2.Text + "，不在0--10之间\n");
                }
            }


            if ((string.IsNullOrWhiteSpace(txt运动频率.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt运动频率.Txt2.Text))
                )
            {
                Msg.Warning("运动频率不能为空!");
                txt运动频率.Focus();
                return false;
            }


            if ((string.IsNullOrWhiteSpace(txt持续时间.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt持续时间.Txt2.Text)))
            {
                Msg.Warning("持续时间不能为空!");
                txt持续时间.Focus();
                return false;
            }
            else
            {
                if (Convert.ToDecimal(txt持续时间.Txt1.Text) < 10m || Convert.ToDecimal(txt持续时间.Txt1.Text) > 120m
                    || Convert.ToDecimal(txt持续时间.Txt2.Text) < 10m || Convert.ToDecimal(txt持续时间.Txt2.Text) > 120m)
                {
                    sbNotity.Append("运动持续时间：" + txt持续时间.Txt1.Text + "/" + txt持续时间.Txt2.Text + "，不在10--120之间\n");
                }
            }

            if (string.IsNullOrWhiteSpace(txt主食.Txt1.Text) || string.IsNullOrWhiteSpace(txt主食.Txt2.Text))
            {
                Msg.Warning("主食情况不能为空!");
                txt主食.Txt1.Focus();
                return false;
            }
            else if (Convert.ToDecimal(txt主食.Txt1.Text) < 250m || Convert.ToDecimal(txt主食.Txt1.Text) > 400m
                    || Convert.ToDecimal(txt主食.Txt2.Text) < 250m || Convert.ToDecimal(txt主食.Txt2.Text) > 400m)
            {
                sbNotity.Append("主食：" + txt主食.Txt1.Text + "/" + txt主食.Txt2.Text + "，不在250--400g之间\n");
            }

            if (txt心理调整.EditValue == null
                || string.IsNullOrWhiteSpace(txt心理调整.EditValue.ToString())
               )
            {
                Msg.Warning("心理调整不能为空!");
                txt心理调整.Focus();
                return false;
            }

            if (txt遵医行为.EditValue == null || string.IsNullOrWhiteSpace(txt遵医行为.EditValue.ToString()))
            {
                Msg.Warning("遵医行为不能为空!");
                txt遵医行为.Focus();
                return false;
            }
            //生活方式指导 ▲
            //生活指导

            if (txt服药依从性.EditValue == null || string.IsNullOrWhiteSpace(txt服药依从性.EditValue.ToString())
                 )
            {
                Msg.Warning("服药依从性不能为空!");
                txt服药依从性.Focus();
                return false;
            }

            if (string.IsNullOrWhiteSpace(GetFlowLayoutResult(this.fl随访分类).ToString()))
            {
                Msg.Warning("请确认随访分类!");
                fl随访分类.Focus();
                return false;
            }

            if (radio用药情况.EditValue == null || string.IsNullOrWhiteSpace(radio用药情况.EditValue.ToString())
            || (radio用药情况.EditValue.ToString() == "1" && gvDetail.DataRowCount == 0))
            {
                Msg.Warning("请填写用药情况!");
                radio用药情况.Focus();
                return false;
            }
            else
            {
                for (int index = 0; index < gvDetail.DataRowCount; index++)
                {
                    //添加用法说明默认值
                    DataRow row = gvDetail.GetDataRow(index);
                    if (row[tb_MXB糖尿病随访表_用药情况.用法].ToString().Contains("每日  次")
                        || row[tb_MXB糖尿病随访表_用药情况.用法].ToString().Contains("每次  mg")
                        || string.IsNullOrWhiteSpace(row[tb_MXB糖尿病随访表_用药情况.药物名称].ToString()))
                    {
                        Msg.Warning("请检查用药情况!");
                        gvDetail.Focus();
                        return false;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(txt医生签名.Text))
            {
                Msg.Warning("请填写医生签名!");
                txt医生签名.Focus();
                return false;
            }

            if (sbNotity.Length > 0)
            {
                return Msg.AskQuestion("请确认下方数据：\r\n" + sbNotity.ToString() + "确定要保存吗？");
            }

            if (txt备注.Text.Contains("拒绝服药") || txt备注.Text.Contains("拒绝转诊"))
            {
                bool isFirstRemarkInSeason = true;

                DateTime dt发生时间 = txt发生时间.DateTime;

                //发生时间所属季度的第一天
                DateTime dt随访所属季度第一天 = dt发生时间.AddDays(1 - dt发生时间.Day).AddMonths(0 - (dt发生时间.Month - 1) % 3);

                //m_dt下一步管理措施 中的数据是按照 随访时间 倒叙排列的
                for (int index = 0; index < m_dt下一步管理措施.Rows.Count; index++)
                {
                    string strTemp = m_dt下一步管理措施.Rows[index]["随访时间"].ToString();
                    if (strTemp.StartsWith(dt随访所属季度第一天.ToString("yyyy-MM"))
                        || strTemp.StartsWith(dt随访所属季度第一天.AddMonths(1).ToString("yyyy-MM"))
                        || strTemp.StartsWith(dt随访所属季度第一天.AddMonths(2).ToString("yyyy-MM")))
                    {
                        if (m_dt下一步管理措施.Rows[index]["备注"].ToString().Contains("拒绝服药")
                            || (m_dt下一步管理措施.Rows[index]["备注"].ToString().Contains("拒绝转诊")))
                        {
                            isFirstRemarkInSeason = false;
                        }
                    }
                }

                if (isFirstRemarkInSeason && txt转诊结果.EditValue != "2")
                {
                    bool askResult = Msg.AskQuestion(@"经程序自动判断，本次随访所在季度中，您第一次将“备注”标注为“拒绝服药”或“拒绝转诊”。
每季度第一次备注为“拒绝服药或拒绝转诊”的，需满足以下条件：
  ①转诊后2周随访，转诊结果为未到位，
  ②且再次劝说转诊无效，
  ③本人在纸质随访表上签字确认。
条件②③无法通过程序自动判断，需人工判断。针对条件①，本次随访填写的“转诊结果”不是“未到位”，您确定要保存此次随访数据吗？");

                    return askResult;
                }
            }

            return true;
        }


        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            dataSource.Rows[0][tb_MXB糖尿病随访表.个人档案编号] = txt个人档案号.Text;
            //dataSource.Rows[0][tb_MXB糖尿病随访表.所属机构] = lab当前所属机构.Text;

            DataBinder.BindingTextEditDateTime(txt发生时间, dataSource, tb_MXB糖尿病随访表.发生时间);
            DataBinder.BindingTextEdit(txt症状其他, dataSource, tb_MXB糖尿病随访表.目前症状其他);
            //TextEdit - 体征
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB糖尿病随访表.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB糖尿病随访表.舒张压);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB糖尿病随访表.体重);
            DataBinder.BindingTextEdit(txt体重.Txt2, dataSource, tb_MXB糖尿病随访表.体重2);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB糖尿病随访表.身高);
            DataBinder.BindingTextEdit(txt体质指数.Txt1, dataSource, tb_MXB糖尿病随访表.BMI);
            DataBinder.BindingTextEdit(txt体质指数.Txt2, dataSource, tb_MXB糖尿病随访表.BMI2);
            DataBinder.BindingTextEdit(txt足背动脉搏动, dataSource, tb_MXB糖尿病随访表.足背动脉搏动);
            DataBinder.BindingTextEdit(txt体征其他, dataSource, tb_MXB糖尿病随访表.体征其他);
            //TextEdit - 生活方式指导
            DataBinder.BindingTextEdit(txt日吸烟量.Txt1, dataSource, tb_MXB糖尿病随访表.吸烟数量);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt2, dataSource, tb_MXB糖尿病随访表.吸烟数量2);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt1, dataSource, tb_MXB糖尿病随访表.饮酒数量);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt2, dataSource, tb_MXB糖尿病随访表.饮酒数量2);
            DataBinder.BindingTextEdit(txt运动频率.Txt1, dataSource, tb_MXB糖尿病随访表.运动频率);
            DataBinder.BindingTextEdit(txt运动频率.Txt2, dataSource, tb_MXB糖尿病随访表.运动频率2);
            DataBinder.BindingTextEdit(txt持续时间.Txt1, dataSource, tb_MXB糖尿病随访表.运动持续时间);
            DataBinder.BindingTextEdit(txt持续时间.Txt2, dataSource, tb_MXB糖尿病随访表.运动持续时间2);
            DataBinder.BindingTextEdit(txt主食.Txt1, dataSource, tb_MXB糖尿病随访表.适合主食);
            DataBinder.BindingTextEdit(txt主食.Txt2, dataSource, tb_MXB糖尿病随访表.适合主食2);
            DataBinder.BindingTextEdit(txt心理调整, dataSource, tb_MXB糖尿病随访表.心理调整);
            DataBinder.BindingTextEdit(txt遵医行为, dataSource, tb_MXB糖尿病随访表.遵医行为);
            //辅助检查
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB糖尿病随访表.空腹血糖);
            DataBinder.BindingTextEdit(txt糖化血红蛋白.Txt1, dataSource, tb_MXB糖尿病随访表.糖化血红蛋白);
            //this.txt糖化血红蛋白.Txt1.Text = dataSource.Rows[0][tb_MXB糖尿病随访表.糖化血红蛋白].ToString();
            //DataBinder.BindingTextEdit(txt检查日期, dataSource, tb_MXB糖尿病随访表.辅助检查日期);
            DataBinder.BindingTextEdit(txt随机血糖.Txt1, dataSource, tb_MXB糖尿病随访表.随机血糖);
            DataBinder.BindingTextEdit(txt餐后2h血糖.Txt1, dataSource, tb_MXB糖尿病随访表.餐后2h血糖);
            DataBinder.BindingTextEdit(txt服药依从性, dataSource, tb_MXB糖尿病随访表.服药依从性);
            DataBinder.BindingTextEdit(txt药物副作用详述, dataSource, tb_MXB糖尿病随访表.副作用详述);
            DataBinder.BindingTextEdit(txt低血糖反应, dataSource, tb_MXB糖尿病随访表.低血糖反应);

            DataBinder.BindingTextEdit(txt医生建议, dataSource, tb_MXB糖尿病随访表.随访医生建议);
            DataBinder.BindingTextEdit(txt转诊科别, dataSource, tb_MXB糖尿病随访表.转诊科别);
            DataBinder.BindingTextEdit(txt转诊原因, dataSource, tb_MXB糖尿病随访表.转诊原因);
            DataBinder.BindingTextEditDateTime(txt下次随访时间, dataSource, tb_MXB糖尿病随访表.下次随访时间);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_MXB糖尿病随访表.随访医生);

            //RadioEdit
            DataBinder.BindingRadioEdit(radio随访方式, dataSource, tb_MXB糖尿病随访表.随访方式);
            DataBinder.BindingRadioEdit(radio不良反应, dataSource, tb_MXB糖尿病随访表.药物副作用);
            DataBinder.BindingRadioEdit(radio用药情况, dataSource, tb_MXB糖尿病随访表.降压药);
            DataBinder.BindingRadioEdit(radio转诊情况, dataSource, tb_MXB糖尿病随访表.转诊情况);

            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病随访表.访视情况].ToString(), fl访视情况);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病随访表.失访原因].ToString(), fl失访原因);
            dte死亡日期.Text = dataSource.Rows[0][tb_MXB糖尿病随访表.死亡日期].ToString();
            txt死亡原因.Text = dataSource.Rows[0][tb_MXB糖尿病随访表.死亡原因].ToString();

            //flowLayoutPanel
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病随访表.目前症状].ToString(), fl症状);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病随访表.本次随访分类].ToString(), fl随访分类);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB糖尿病随访表.慢病并发症].ToString(), fl随访分类并发症);

            #region  新版本添加
            DataBinder.BindingTextEdit(txt胰岛素种类, dataSource, tb_MXB糖尿病随访表.胰岛素种类);
            DataBinder.BindingTextEdit(txt胰岛素用法用量, dataSource, tb_MXB糖尿病随访表.胰岛素用法用量);
            DataBinder.BindingRadioEdit(radio管理措施, dataSource, tb_MXB糖尿病随访表.下一步管理措施);
            DataBinder.BindingRadioEdit(radio药物调整, dataSource, tb_MXB糖尿病随访表.用药调整意见);
            DataBinder.BindingTextEdit(txt调整胰岛素种类, dataSource, tb_MXB糖尿病随访表.调整胰岛素种类);
            DataBinder.BindingTextEdit(txt调整胰岛素用法用量, dataSource, tb_MXB糖尿病随访表.调整胰岛素用法用量);
            DataBinder.BindingTextEdit(txt转诊联系人, dataSource, tb_MXB糖尿病随访表.转诊联系人);
            DataBinder.BindingTextEdit(txt转诊联系电话, dataSource, tb_MXB糖尿病随访表.转诊联系电话);
            DataBinder.BindingTextEdit(txt转诊结果, dataSource, tb_MXB糖尿病随访表.转诊结果);
            //DataBinder.BindingTextEdit(txt居民签名, dataSource, tb_MXB糖尿病随访表.居民签名);

            //指纹 ▽
            string SQ = dataSource.Rows[0][tb_MXB糖尿病随访表.居民签名].ToString();
            if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
            {
                Byte[] bitmapData = new Byte[SQ.Length];
                bitmapData = Convert.FromBase64String(SQ);
                using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData))
                {
                    this.txt居民签名.Image = Image.FromStream(streamBitmap);
                    streamBitmap.Close();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(SQ))
                    SQ = "暂无电子签名";
                Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                Font font = new Font("宋体", 9);
                SizeF sizeF = g.MeasureString(SQ, font); //测量出字体的高度和宽度  
                Brush brush; //笔刷，颜色  
                brush = Brushes.Black;
                PointF pf = new PointF(0, 0);
                Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                g = Graphics.FromImage(img);
                g.DrawString(SQ, font, brush, pf);
                this.txt居民签名.Image = img;
            }
            //指纹 △

            DataBinder.BindingTextEdit(txt备注, dataSource, tb_MXB糖尿病随访表.备注);
            #endregion

            //非编辑项
            //this.txt检查日期.Text = this.txt发生时间.Text;//绑定检查日期
            DataBinder.BindingTextEdit(txt检查日期, dataSource, tb_MXB糖尿病随访表.辅助检查日期);
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.所属机构].ToString()); 
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.修改时间].ToString();

        } 
        #endregion

        #region 使用药物-子表

        private void btn添加药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物.Tag, gcDetail);
        }

        private void btn删除药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物.Tag, gcDetail);
        }

        protected override void CreateOneDetail(GridView gridView)
        {
            if (gridView==gvDetail)
            {
            gvDetail.MoveLast();

            DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName].NewRow();
            //添加用法说明默认值
            row[tb_MXB糖尿病随访表_用药情况.用法] = "每日   次，每次   mg  （口服）";
            row[tb_MXB糖尿病随访表_用药情况.个人档案编号] = this.txt个人档案号.Text;
            row[tb_MXB糖尿病随访表_用药情况.创建时间] = lab创建时间.Text;

            _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName].Rows.Add(row); //增加一条明细记录

            gcDetail.RefreshDataSource();
            gvDetail.FocusedRowHandle = gvDetail.RowCount - 1;

            gvDetail.FocusedColumn = gvDetail.VisibleColumns[0];
            }

            if (gridView == gv药物调整)
            {
                gv药物调整.MoveLast();

                DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName].NewRow();
                //添加用法说明默认值
                row[tb_MXB糖尿病随访表_用药调整.用法] = "每日   次，每次   mg  （口服）";
                row[tb_MXB糖尿病随访表_用药调整.个人档案编号] = this.txt个人档案号.Text;
                row[tb_MXB糖尿病随访表_用药调整.创建时间] = lab创建时间.Text;

                _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName].Rows.Add(row); //增加一条明细记录

                gc药物调整.RefreshDataSource();
                gv药物调整.FocusedRowHandle = gv药物调整.RowCount - 1;

                gv药物调整.FocusedColumn = gv药物调整.VisibleColumns[0];
            }
        }
        #endregion

        #region 页面控制

        private void radio用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio用药情况.EditValue!=null && radio用药情况.EditValue.ToString() == "1")
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                if (_BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName].Rows.Count <= 0)
                    btn添加药物.PerformClick();
            }
            else
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                
            }

        }

        private void radio不良反应_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio不良反应.EditValue!=null && radio不良反应.EditValue.ToString() == "2")
                this.txt药物副作用详述.Enabled = true;
            else
                this.txt药物副作用详述.Enabled = false ;
        }

        private void check无症状_CheckedChanged(object sender, EventArgs e)
        {
            if (check无症状.Checked)
                SetFlCheckEnabled(fl症状, false, check无症状.Text);
            else
                SetFlCheckEnabled(fl症状, true, check无症状.Text);
        }

        private void SetFlCheckEnabled(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Enabled = value;
                }
            }
        }

        private void ck随访并发症_CheckedChanged(object sender, EventArgs e)
        {
            if (ck随访并发症.Checked)
                layout随访分类并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            else
            {
                SetFlChecked(fl随访分类并发症, false, ck随访并发症.Text);
                layout随访分类并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void SetFlChecked(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Checked = value;
                }
            }
        }

        private void txt服药依从性_EditValueChanged(object sender, EventArgs e)
        {
            if (txt服药依从性.EditValue.ToString() != "3")
                radio用药情况.EditValue = "1";
            else
                radio用药情况.EditValue = "2";
        }

        private void ch症状其他_CheckedChanged(object sender, EventArgs e)
        {
            if (ch症状其他.Checked)
            {
                sbtn便捷录入症状.Visible = true;
                this.txt症状其他.Visible = true;
            }
            else
            {
                sbtn便捷录入症状.Visible = false;
                this.txt症状其他.Visible = false;
                _BLL.DataBinder.Rows[0][tb_MXB糖尿病随访表.目前症状其他] = "";
            }
        }

        //private void txt发生时间_EditValueChanged(object sender, EventArgs e)
        //{
        //    this.txt检查日期.EditValue = txt发生时间.EditValue.ToString();
        //}
        #endregion        
        
        #region 用药调整
        private void btn添加药物调整_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物调整.Tag, gc药物调整);
        }

        private void btn删除药物调整_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物调整.Tag, gc药物调整);
        }

        #endregion

        private void radio药物调整_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio药物调整.EditValue != null && radio药物调整.EditValue.ToString() == "1")
            {
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                if (_BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName].Rows.Count <= 0)
                    btn添加药物调整.PerformClick();
            }
            else
            {
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void radio转诊情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio转诊情况.EditValue != null && radio转诊情况.EditValue.ToString() == "1")
            {
                this.txt转诊原因.Enabled = this.txt转诊科别.Enabled = this.txt转诊联系人.Enabled = this.txt转诊联系电话.Enabled = this.txt转诊结果.Enabled = true;
            }
            else
            {
                this.txt转诊原因.Enabled = this.txt转诊科别.Enabled = this.txt转诊联系人.Enabled = this.txt转诊联系电话.Enabled = this.txt转诊结果.Enabled = false;
                this.txt转诊原因.Text = this.txt转诊科别.Text = this.txt转诊联系人.Text = this.txt转诊联系电话.Text =this.txt转诊结果.Text= "";
            }
        }

        bool b手签更新 = false;
        FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
        private void sbtnFingerPrint_Click(object sender, EventArgs e)
        {
            DialogResult result = frm.ShowDialog();
            if (result == DialogResult.OK)
            {
                b手签更新 = true;
                txt居民签名.Image = frm.bmp;
            }
        }
		
        //Begin WXF 2018-11-28 | 12:06
        //如果检查日期为空则检查日期默认为随访日期 
        //根据随访日期和下一步管理措施自动推算下次随访时间  2018-11-28 | 15:18
        private void txt发生时间_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt检查日期.Text) && !string.IsNullOrEmpty(txt发生时间.Text))
            {
                txt检查日期.Text = txt发生时间.Text;
            }

            radio管理措施_EditValueChanged(null, null);
            //if (string.IsNullOrEmpty(txt下次随访时间.Text) && radio管理措施.EditValue.ToString().Equals("1") && !string.IsNullOrEmpty(txt发生时间.Text))
            //{
            //    txt下次随访时间.Text = Convert.ToDateTime(txt发生时间.Text).AddMonths(+3).ToString("yyyy-MM-dd");
            //}
        }

        private void radio管理措施_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt发生时间.Text))
            {
                XtraMessageBox.Show("无随访日期无法推算下次随访时间!");
                return;
            }

            DateTime date = Convert.ToDateTime(txt发生时间.Text);
            if (!radio管理措施.EditValue.ToString().Equals("1"))
            {
                date = date.AddDays(+13);
            }
            else
            {
                date = date.AddMonths(+3);
            }

            txt下次随访时间.Text = date.ToString("yyyy-MM-dd");
        }
        //End
        private void chk失访_CheckedChanged(object sender, EventArgs e)
        {
            if (chk失访.Checked)
            {
                chk死亡.Checked = false;

                fl失访原因.Enabled = true;
                fl死亡信息.Enabled = false;
            }
            else
            {
                fl失访原因.Enabled = false;
            }
        }

        private void chk死亡_CheckedChanged(object sender, EventArgs e)
        {
            if (chk死亡.Checked)
            {
                chk失访.Checked = false;

                fl失访原因.Enabled = false;
                fl死亡信息.Enabled = true;
            }
            else
            {
                fl死亡信息.Enabled = false;
            }
        }

        private void checkEdit症状_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = sender as CheckEdit;
            if (chk.Checked)
            {
                radio管理措施.EditValue = "4";
                checkEdit控制不满意.Checked = true;
            }
        }

        private void txt症状其他_EditValueChanged(object sender, EventArgs e)
        {
            if (txt症状其他.Text.Contains("意识改变") ||
                txt症状其他.Text.Contains("行为改变") ||
                txt症状其他.Text.Contains("呼气有烂苹果样丙酮味") ||
                txt症状其他.Text.Contains("心悸") ||
                txt症状其他.Text.Contains("出汗") ||
                txt症状其他.Text.Contains("食欲减退") ||
                txt症状其他.Text.Contains("恶心") ||
                txt症状其他.Text.Contains("呕吐") ||
                txt症状其他.Text.Contains("腹痛") ||
                txt症状其他.Text.Contains("深大呼吸") ||
                txt症状其他.Text.Contains("皮肤潮红") ||
                txt症状其他.Text.Contains("视力下降")
               )
            {
                radio管理措施.EditValue = "4";
                checkEdit控制不满意.Checked = true;
            }
        }


        private void txt血压值_Leave(object sender, EventArgs e)
        {
            if ((!string.IsNullOrWhiteSpace(txt血压值.Txt1.Text) && Convert.ToDecimal(this.txt血压值.Txt1.Text) >= 180m)
                 || (!string.IsNullOrWhiteSpace(txt血压值.Txt2.Text) && Convert.ToDecimal(this.txt血压值.Txt2.Text) >= 110m))
            {
                radio管理措施.EditValue = "4";
                checkEdit控制不满意.Checked = true;
            }
        }

        bool is孕产妇=false;
        private void txt空腹血糖_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt空腹血糖.Txt1.Text) 
                && (Convert.ToDecimal(this.txt空腹血糖.Txt1.Text) >= 16.7m || Convert.ToDecimal(this.txt空腹血糖.Txt1.Text)<=3.9m))
            {
                radio管理措施.EditValue = "4";
                checkEdit控制不满意.Checked = true;
            }
            else if (!string.IsNullOrWhiteSpace(txt空腹血糖.Txt1.Text) && (Convert.ToDecimal(this.txt空腹血糖.Txt1.Text) >= 7m) && is孕产妇)
            {
                radio管理措施.EditValue = "4";
                checkEdit控制不满意.Checked = true;
                
            }
            else if (!string.IsNullOrWhiteSpace(txt空腹血糖.Txt1.Text) 
                && (Convert.ToDecimal(this.txt空腹血糖.Txt1.Text) < 16.7m && Convert.ToDecimal(this.txt空腹血糖.Txt1.Text)>=7m))
            {
                checkEdit控制不满意.Checked = true;
            }
            else if (!string.IsNullOrWhiteSpace(txt空腹血糖.Txt1.Text) && (Convert.ToDecimal(this.txt空腹血糖.Txt1.Text)<7m))
            {
                checkEdit控制满意.Checked = true;
            }
            
        }

        private DataTable m_dt下一步管理措施 = null;
        private void sbtn查看近期下一步_Click(object sender, EventArgs e)
        {
            Frm近期下一步管理措施 frm = new Frm近期下一步管理措施(m_dt下一步管理措施);
            frm.ShowDialog();
        }

        private void ck随访分类_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = sender as CheckEdit;
            if (chk.Checked)
            {
                CheckEdit[] chks = { checkEdit控制满意, checkEdit控制不满意, checkEdit不良反应, ck随访并发症 };
                for (int index = 0; index < chks.Length; index++)
                {
                    if (chk.Name != chks[index].Name)
                    {
                        chks[index].Checked = false;
                    }
                }

                if (radio管理措施.EditValue == "4")//在已选择"紧急措施"的情况下，不再进行其他的关联设置。
                {
                    return;
                }

                if (chk.Name == checkEdit控制满意.Name)//控制满意的对应关系：此次随访分类为控制满意，下一步管理措施中逻辑性选择常规随访。
                {
                    radio管理措施.EditValue = "1";
                }
                //此次随访分类：并发症、无备注的，下一步管理措施中逻辑性选择紧急转诊。 ②此次随访分类：并发症、备注为拒绝服药或拒绝转诊的，下一步管理措施中逻辑性紧急转诊
                else if (chk.Name == ck随访并发症.Name)
                {
                    radio管理措施.EditValue = "4";
                }
                //此次随访分类：并发症、无备注的，下一步管理措施中逻辑性选择紧急转诊。 ②此次随访分类：并发症、备注为拒绝服药或拒绝转诊的，下一步管理措施中逻辑性紧急转诊
                else if (chk.Name == checkEdit不良反应.Name || chk.Name == checkEdit控制不满意.Name)
                {
                    if (string.IsNullOrWhiteSpace(txt备注.Text))
                    {
                        string str = Get上次随访的下一步管理措施();
                        if (str == "1")
                        {
                            radio管理措施.EditValue = "2";
                        }
                        else if (str == "2")
                        {
                            radio管理措施.EditValue = "3";
                        }
                    }
                    else if (txt备注.Text.Contains("拒绝服药") || txt备注.Text.Contains("拒绝转诊"))
                    {
                        radio管理措施.EditValue = "1";
                    }
                }
            }
        }

        private string Get上次随访的下一步管理措施()
        {
            string ret = "";
            if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
            {
                for (int index = 0; index < m_dt下一步管理措施.Rows.Count; index++)
                {
                    //此处的比较是以日期全是yyyy-MM-dd为前提的。
                    if (m_dt下一步管理措施.Rows[index][tb_MXB糖尿病随访表.下一步管理措施].ToString().CompareTo(txt发生时间.Text) < 0)
                    {
                        ret = m_dt下一步管理措施.Rows[index][tb_MXB糖尿病随访表.下一步管理措施].ToString();
                        break;
                    }
                }
            }

            return ret;
        }

        private void radio管理措施_EditValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
            {
                switch (radio管理措施.EditValue.ToString())
                {
                    case "1":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                        break;
                    case "2":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        break;
                    case "3":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        break;
                    case "4":
                        if (txt备注.Text.Contains("拒绝服药") || txt备注.Text.Contains("拒绝转诊"))
                        {
                            txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        }
                        break;
                }
            }
        }

        private void txt备注_TextChanged(object sender, EventArgs e)
        {
            if (txt备注.Text.Contains("拒绝转诊") || txt备注.Text.Contains("拒绝服药"))
            {
                if ((checkEdit控制不满意.Checked || checkEdit不良反应.Checked) && radio管理措施.EditValue != "4")
                {
                    radio管理措施.EditValue = "1";
                }
                else if (radio管理措施.EditValue == "4")
                {
                    if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
                    {
                        txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                    }
                }
            }
            else
            {
                if ((checkEdit控制不满意.Checked || checkEdit不良反应.Checked) && radio管理措施.EditValue != "4")
                {
                    if (string.IsNullOrWhiteSpace(txt备注.Text))
                    {
                        string str = Get上次随访的下一步管理措施();
                        if (str == "1")
                        {
                            radio管理措施.EditValue = "2";
                        }
                        else if (str == "2")
                        {
                            radio管理措施.EditValue = "3";
                        }
                    }
                }
                else if (radio管理措施.EditValue == "4")
                {
                    if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
                    {
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                    }
                }
            }
        }


        private void sbtn便捷录入症状_Click(object sender, EventArgs e)
        {
            ShowLittleForm(new string[] {
            "有意识或行为改变",
"呼气有烂苹果样丙酮味",
"心悸",
"出汗",
"食欲减退",
"恶心",
"呕吐",
"多饮",
"多尿",
"腹痛",
"有深大呼吸",
"皮肤潮红",
"视力突然下降"}, txt症状其他);
        }

        private void sbtn便捷录入备注_Click(object sender, EventArgs e)
        {
            ShowLittleForm(new string[] {
            "拒绝服药",
            "拒绝转诊"}, txt备注);
        }

        private void ShowLittleForm(string[] arrlist, TextEdit edit)
        {
            Form frm = new Form();
            frm.ClientSize = new System.Drawing.Size(390, 176);
            frm.MaximizeBox = false;
            frm.MinimizeBox = false;
            frm.Text = "双击选择，选择完后关闭此窗口即可";
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

            CheckedListBox list = new CheckedListBox();
            list.FormattingEnabled = true;
            list.Items.AddRange(arrlist);
            list.Location = new System.Drawing.Point(12, 12);
            list.MultiColumn = true;
            list.Name = "checkedListBox便捷录入备注";
            list.Size = new System.Drawing.Size(359, 148);
            list.TabIndex = 0;
            frm.Controls.Add(list);

            frm.ShowDialog();

            string strTemp = "";
            for (int index = 0; index < list.CheckedItems.Count; index++)
            {
                if (!edit.Text.Contains(list.CheckedItems[index].ToString()))
                {
                    strTemp += "," + list.CheckedItems[index].ToString(); ;
                }
            }

            if (string.IsNullOrWhiteSpace(edit.Text))
            {
                edit.Text = strTemp.Trim(',');
            }
            else
            {
                edit.Text += strTemp;
            }

            list.Dispose();
            frm.Dispose();
        }

        private void txt备注_EditValueChanged(object sender, EventArgs e)
        {

        }

    }
}
