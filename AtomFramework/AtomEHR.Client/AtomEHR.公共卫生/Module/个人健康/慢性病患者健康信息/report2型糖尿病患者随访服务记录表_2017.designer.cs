﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class report2型糖尿病患者随访服务记录表_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel_编号5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_年_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_月_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_日_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_年_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_月_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_日_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_年_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_月_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_日_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_年_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_月_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访日期_日_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_随访方式_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_随访方式_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_随访方式_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_随访方式_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_1_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_2_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_3_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状_4_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状其他_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_血压_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_血压_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_血压_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_血压_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体质指数_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_心率_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_心率_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_心率_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_心率_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱双侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱左侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱右侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱双侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱左侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱右侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱双侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱左侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱右侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱双侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱左侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动减弱右侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失双侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失左侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失右侧_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失双侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失左侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失右侧_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失双侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失左侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失右侧_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失双侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失左侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动消失右侧_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体征其他_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体征其他_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体征其他_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体征其他_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日吸烟量_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_日饮酒量_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动频率_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_运动时长_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_摄盐情况_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_摄盐情况_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_摄盐情况_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_摄盐情况_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_主食_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心理调整_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心理调整_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心理调整_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心理调整_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_遵医行为_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_遵医行为_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_遵医行为_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_遵医行为_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_空腹血糖_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_空腹血糖_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_空腹血糖_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_空腹血糖_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_糖化血红蛋白_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_糖化血红蛋白_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_糖化血红蛋白_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_糖化血红蛋白_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期月_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期日_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期月_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期日_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期月_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期日_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期月_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_检查日期日_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_其他检查其他_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_其他检查其他_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_其他检查其他_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_其他检查其他_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称目前3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日目前3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次目前3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类目前_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类目前_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类目前_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类目前_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量目前_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量目前_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量目前_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量目前_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_服药依从性_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_服药依从性_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_服药依从性_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_服药依从性_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物不良反应有_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_药物不良反应_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物不良反应有_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_药物不良反应_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物不良反应有_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_药物不良反应_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物不良反应有_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_药物不良反应_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_低血糖反应_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell411 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_低血糖反应_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_低血糖反应_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_低血糖反应_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_此次随访分类_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_此次随访分类_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_此次随访分类_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_此次随访分类_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下一步管理措施_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下一步管理措施_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下一步管理措施_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下一步管理措施_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称调整3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每日调整3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用法用量每次调整3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell429 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类调整_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类调整_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell440 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类调整_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell445 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素种类调整_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell451 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell452 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量调整_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell457 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量调整_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell462 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量调整_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell467 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_胰岛素用法用量调整_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊原因_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊原因_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊原因_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊原因_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊机构_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊机构_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊机构_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊机构_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊联系人_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊联系人_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊联系人_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_转诊联系人_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_转诊结果_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_转诊结果_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_转诊结果_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_转诊结果_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_下次随访日期_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_下次随访日期_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_下次随访日期_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_下次随访日期_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访医生签字_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访医生签字_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访医生签字_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_随访医生签字_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_居民签字_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_居民签字_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_居民签字_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_居民签字_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_备注_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_备注_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_备注_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_备注_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel_编号1,
            this.xrLabel_编号3,
            this.xrLabel_编号2,
            this.xrLabel_编号4,
            this.xrLine1,
            this.xrLabel_编号5,
            this.xrLabel_编号6,
            this.xrLabel_编号8,
            this.xrLabel_编号7,
            this.xrLabel_姓名,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 2970F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1211.821F, 83.41998F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(129.1166F, 58.42F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "编号：";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel_编号1
            // 
            this.xrLabel_编号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号1.BorderWidth = 2F;
            this.xrLabel_编号1.CanGrow = false;
            this.xrLabel_编号1.Dpi = 254F;
            this.xrLabel_编号1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号1.LocationFloat = new DevExpress.Utils.PointFloat(1340.938F, 90F);
            this.xrLabel_编号1.Name = "xrLabel_编号1";
            this.xrLabel_编号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号1.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号1.StylePriority.UseBorders = false;
            this.xrLabel_编号1.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号1.StylePriority.UseFont = false;
            // 
            // xrLabel_编号3
            // 
            this.xrLabel_编号3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号3.BorderWidth = 2F;
            this.xrLabel_编号3.CanGrow = false;
            this.xrLabel_编号3.Dpi = 254F;
            this.xrLabel_编号3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号3.LocationFloat = new DevExpress.Utils.PointFloat(1462.246F, 90F);
            this.xrLabel_编号3.Name = "xrLabel_编号3";
            this.xrLabel_编号3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号3.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号3.StylePriority.UseBorders = false;
            this.xrLabel_编号3.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号3.StylePriority.UseFont = false;
            // 
            // xrLabel_编号2
            // 
            this.xrLabel_编号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号2.BorderWidth = 2F;
            this.xrLabel_编号2.CanGrow = false;
            this.xrLabel_编号2.Dpi = 254F;
            this.xrLabel_编号2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号2.LocationFloat = new DevExpress.Utils.PointFloat(1400.863F, 90F);
            this.xrLabel_编号2.Name = "xrLabel_编号2";
            this.xrLabel_编号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号2.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号2.StylePriority.UseBorders = false;
            this.xrLabel_编号2.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号2.StylePriority.UseFont = false;
            // 
            // xrLabel_编号4
            // 
            this.xrLabel_编号4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号4.BorderWidth = 2F;
            this.xrLabel_编号4.CanGrow = false;
            this.xrLabel_编号4.Dpi = 254F;
            this.xrLabel_编号4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号4.LocationFloat = new DevExpress.Utils.PointFloat(1570.196F, 90F);
            this.xrLabel_编号4.Name = "xrLabel_编号4";
            this.xrLabel_编号4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号4.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号4.StylePriority.UseBorders = false;
            this.xrLabel_编号4.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号4.StylePriority.UseFont = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 5;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(1521.513F, 90F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(35F, 45F);
            // 
            // xrLabel_编号5
            // 
            this.xrLabel_编号5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号5.BorderWidth = 2F;
            this.xrLabel_编号5.CanGrow = false;
            this.xrLabel_编号5.Dpi = 254F;
            this.xrLabel_编号5.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号5.LocationFloat = new DevExpress.Utils.PointFloat(1633.696F, 90F);
            this.xrLabel_编号5.Name = "xrLabel_编号5";
            this.xrLabel_编号5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号5.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号5.StylePriority.UseBorders = false;
            this.xrLabel_编号5.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号5.StylePriority.UseFont = false;
            // 
            // xrLabel_编号6
            // 
            this.xrLabel_编号6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号6.BorderWidth = 2F;
            this.xrLabel_编号6.CanGrow = false;
            this.xrLabel_编号6.Dpi = 254F;
            this.xrLabel_编号6.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号6.LocationFloat = new DevExpress.Utils.PointFloat(1697.196F, 90F);
            this.xrLabel_编号6.Name = "xrLabel_编号6";
            this.xrLabel_编号6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号6.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号6.StylePriority.UseBorders = false;
            this.xrLabel_编号6.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号6.StylePriority.UseFont = false;
            // 
            // xrLabel_编号8
            // 
            this.xrLabel_编号8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号8.BorderWidth = 2F;
            this.xrLabel_编号8.CanGrow = false;
            this.xrLabel_编号8.Dpi = 254F;
            this.xrLabel_编号8.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号8.LocationFloat = new DevExpress.Utils.PointFloat(1822.079F, 90F);
            this.xrLabel_编号8.Name = "xrLabel_编号8";
            this.xrLabel_编号8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号8.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号8.StylePriority.UseBorders = false;
            this.xrLabel_编号8.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号8.StylePriority.UseFont = false;
            // 
            // xrLabel_编号7
            // 
            this.xrLabel_编号7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号7.BorderWidth = 2F;
            this.xrLabel_编号7.CanGrow = false;
            this.xrLabel_编号7.Dpi = 254F;
            this.xrLabel_编号7.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_编号7.LocationFloat = new DevExpress.Utils.PointFloat(1760.696F, 90F);
            this.xrLabel_编号7.Name = "xrLabel_编号7";
            this.xrLabel_编号7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号7.SizeF = new System.Drawing.SizeF(40F, 45F);
            this.xrLabel_编号7.StylePriority.UseBorders = false;
            this.xrLabel_编号7.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号7.StylePriority.UseFont = false;
            // 
            // xrLabel_姓名
            // 
            this.xrLabel_姓名.CanGrow = false;
            this.xrLabel_姓名.Dpi = 254F;
            this.xrLabel_姓名.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_姓名.LocationFloat = new DevExpress.Utils.PointFloat(340.7834F, 83.41998F);
            this.xrLabel_姓名.Name = "xrLabel_姓名";
            this.xrLabel_姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_姓名.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel_姓名.StylePriority.UseFont = false;
            this.xrLabel_姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel_姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(211.6633F, 83.41998F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(129.12F, 58.42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(260.8993F, 24.99999F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1437.217F, 58.41999F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "国家基本公共卫生服务项目2型糖尿病患者随访服务记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 141.84F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow7,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow14,
            this.xrTableRow13,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow47,
            this.xrTableRow18,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow25,
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2034F, 2780F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTable_随访日期_年_1,
            this.xrTableCell43,
            this.xrTable_随访日期_月_1,
            this.xrTableCell44,
            this.xrTable_随访日期_日_1,
            this.xrTableCell2,
            this.xrTable_随访日期_年_2,
            this.xrTableCell46,
            this.xrTable_随访日期_月_2,
            this.xrTableCell51,
            this.xrTable_随访日期_日_2,
            this.xrTableCell37,
            this.xrTable_随访日期_年_3,
            this.xrTableCell52,
            this.xrTable_随访日期_月_3,
            this.xrTableCell54,
            this.xrTable_随访日期_日_3,
            this.xrTableCell38,
            this.xrTable_随访日期_年_4,
            this.xrTableCell58,
            this.xrTable_随访日期_月_4,
            this.xrTableCell60,
            this.xrTable_随访日期_日_4,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "随访日期";
            this.xrTableCell1.Weight = 1.3416664464267349D;
            // 
            // xrTable_随访日期_年_1
            // 
            this.xrTable_随访日期_年_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_年_1.CanGrow = false;
            this.xrTable_随访日期_年_1.Dpi = 254F;
            this.xrTable_随访日期_年_1.Name = "xrTable_随访日期_年_1";
            this.xrTable_随访日期_年_1.StylePriority.UseBorders = false;
            this.xrTable_随访日期_年_1.Weight = 0.46089192201442114D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Text = "年";
            this.xrTableCell43.Weight = 0.24422628377030498D;
            // 
            // xrTable_随访日期_月_1
            // 
            this.xrTable_随访日期_月_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_月_1.CanGrow = false;
            this.xrTable_随访日期_月_1.Dpi = 254F;
            this.xrTable_随访日期_月_1.Name = "xrTable_随访日期_月_1";
            this.xrTable_随访日期_月_1.StylePriority.UseBorders = false;
            this.xrTable_随访日期_月_1.Weight = 0.21988250918937427D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Text = "月";
            this.xrTableCell44.Weight = 0.22690202437599671D;
            // 
            // xrTable_随访日期_日_1
            // 
            this.xrTable_随访日期_日_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_日_1.CanGrow = false;
            this.xrTable_随访日期_日_1.Dpi = 254F;
            this.xrTable_随访日期_日_1.Name = "xrTable_随访日期_日_1";
            this.xrTable_随访日期_日_1.StylePriority.UseBorders = false;
            this.xrTable_随访日期_日_1.Weight = 0.23589247732317481D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "日";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.27875983506151064D;
            // 
            // xrTable_随访日期_年_2
            // 
            this.xrTable_随访日期_年_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_年_2.CanGrow = false;
            this.xrTable_随访日期_年_2.Dpi = 254F;
            this.xrTable_随访日期_年_2.Name = "xrTable_随访日期_年_2";
            this.xrTable_随访日期_年_2.StylePriority.UseBorders = false;
            this.xrTable_随访日期_年_2.Weight = 0.46090549169156547D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Text = "年";
            this.xrTableCell46.Weight = 0.2442125869053037D;
            // 
            // xrTable_随访日期_月_2
            // 
            this.xrTable_随访日期_月_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_月_2.CanGrow = false;
            this.xrTable_随访日期_月_2.Dpi = 254F;
            this.xrTable_随访日期_月_2.Name = "xrTable_随访日期_月_2";
            this.xrTable_随访日期_月_2.StylePriority.UseBorders = false;
            this.xrTable_随访日期_月_2.Weight = 0.21988187841269663D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.Text = "月";
            this.xrTableCell51.Weight = 0.22688974945715568D;
            // 
            // xrTable_随访日期_日_2
            // 
            this.xrTable_随访日期_日_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_日_2.CanGrow = false;
            this.xrTable_随访日期_日_2.Dpi = 254F;
            this.xrTable_随访日期_日_2.Name = "xrTable_随访日期_日_2";
            this.xrTable_随访日期_日_2.StylePriority.UseBorders = false;
            this.xrTable_随访日期_日_2.Weight = 0.23590548556683655D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "日";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell37.Weight = 0.27875971139361289D;
            // 
            // xrTable_随访日期_年_3
            // 
            this.xrTable_随访日期_年_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_年_3.CanGrow = false;
            this.xrTable_随访日期_年_3.Dpi = 254F;
            this.xrTable_随访日期_年_3.Name = "xrTable_随访日期_年_3";
            this.xrTable_随访日期_年_3.StylePriority.UseBorders = false;
            this.xrTable_随访日期_年_3.Weight = 0.46090547890238065D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Text = "年";
            this.xrTableCell52.Weight = 0.24421257411611885D;
            // 
            // xrTable_随访日期_月_3
            // 
            this.xrTable_随访日期_月_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_月_3.CanGrow = false;
            this.xrTable_随访日期_月_3.Dpi = 254F;
            this.xrTable_随访日期_月_3.Name = "xrTable_随访日期_月_3";
            this.xrTable_随访日期_月_3.StylePriority.UseBorders = false;
            this.xrTable_随访日期_月_3.Weight = 0.21988187489273756D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Text = "月";
            this.xrTableCell54.Weight = 0.22688975872638145D;
            // 
            // xrTable_随访日期_日_3
            // 
            this.xrTable_随访日期_日_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_日_3.CanGrow = false;
            this.xrTable_随访日期_日_3.Dpi = 254F;
            this.xrTable_随访日期_日_3.Name = "xrTable_随访日期_日_3";
            this.xrTable_随访日期_日_3.StylePriority.UseBorders = false;
            this.xrTable_随访日期_日_3.Weight = 0.23590550058532889D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "日";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell38.Weight = 0.27874005793642487D;
            // 
            // xrTable_随访日期_年_4
            // 
            this.xrTable_随访日期_年_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_年_4.CanGrow = false;
            this.xrTable_随访日期_年_4.Dpi = 254F;
            this.xrTable_随访日期_年_4.Name = "xrTable_随访日期_年_4";
            this.xrTable_随访日期_年_4.StylePriority.UseBorders = false;
            this.xrTable_随访日期_年_4.Weight = 0.46090547890238054D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Text = "年";
            this.xrTableCell58.Weight = 0.24421257411611885D;
            // 
            // xrTable_随访日期_月_4
            // 
            this.xrTable_随访日期_月_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_月_4.CanGrow = false;
            this.xrTable_随访日期_月_4.Dpi = 254F;
            this.xrTable_随访日期_月_4.Name = "xrTable_随访日期_月_4";
            this.xrTable_随访日期_月_4.StylePriority.UseBorders = false;
            this.xrTable_随访日期_月_4.Weight = 0.21988187489273753D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Text = "月";
            this.xrTableCell60.Weight = 0.22688975696640185D;
            // 
            // xrTable_随访日期_日_4
            // 
            this.xrTable_随访日期_日_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访日期_日_4.CanGrow = false;
            this.xrTable_随访日期_日_4.Dpi = 254F;
            this.xrTable_随访日期_日_4.Name = "xrTable_随访日期_日_4";
            this.xrTable_随访日期_日_4.StylePriority.UseBorders = false;
            this.xrTable_随访日期_日_4.Weight = 0.23590549307608272D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "日";
            this.xrTableCell3.Weight = 0.27876674004802149D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell61,
            this.xrTableCell5,
            this.xrTableCell62,
            this.xrTableCell39,
            this.xrTableCell63,
            this.xrTableCell40,
            this.xrTableCell64,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = "随访方式";
            this.xrTableCell4.Weight = 1.3416663262787962D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.Text = "1 门诊 2 家庭 3 电话";
            this.xrTableCell61.Weight = 1.387795225590502D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_随访方式_1});
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Weight = 0.27876006644015794D;
            // 
            // xrLabel_随访方式_1
            // 
            this.xrLabel_随访方式_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_随访方式_1.CanGrow = false;
            this.xrLabel_随访方式_1.Dpi = 254F;
            this.xrLabel_随访方式_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_随访方式_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_随访方式_1.Name = "xrLabel_随访方式_1";
            this.xrLabel_随访方式_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_随访方式_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_随访方式_1.StylePriority.UseBorders = false;
            this.xrLabel_随访方式_1.StylePriority.UseFont = false;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.Text = "1 门诊 2 家庭 3 电话";
            this.xrTableCell62.Weight = 1.3877951814736806D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_随访方式_2});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.27875966187952095D;
            // 
            // xrLabel_随访方式_2
            // 
            this.xrLabel_随访方式_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_随访方式_2.CanGrow = false;
            this.xrLabel_随访方式_2.Dpi = 254F;
            this.xrLabel_随访方式_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_随访方式_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_随访方式_2.Name = "xrLabel_随访方式_2";
            this.xrLabel_随访方式_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_随访方式_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_随访方式_2.StylePriority.UseBorders = false;
            this.xrLabel_随访方式_2.StylePriority.UseFont = false;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.Text = "1 门诊 2 家庭 3 电话";
            this.xrTableCell63.Weight = 1.3877952194892396D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_随访方式_3});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 0.27873939489345512D;
            // 
            // xrLabel_随访方式_3
            // 
            this.xrLabel_随访方式_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_随访方式_3.CanGrow = false;
            this.xrLabel_随访方式_3.Dpi = 254F;
            this.xrLabel_随访方式_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_随访方式_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_随访方式_3.Name = "xrLabel_随访方式_3";
            this.xrLabel_随访方式_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_随访方式_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_随访方式_3.StylePriority.UseBorders = false;
            this.xrLabel_随访方式_3.StylePriority.UseFont = false;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.Text = "1 门诊 2 家庭 3 电话";
            this.xrTableCell64.Weight = 1.3877952194892393D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_随访方式_4});
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Weight = 0.278767269215212D;
            // 
            // xrLabel_随访方式_4
            // 
            this.xrLabel_随访方式_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_随访方式_4.CanGrow = false;
            this.xrLabel_随访方式_4.Dpi = 254F;
            this.xrLabel_随访方式_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_随访方式_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_随访方式_4.Name = "xrLabel_随访方式_4";
            this.xrLabel_随访方式_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_随访方式_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_随访方式_4.StylePriority.UseBorders = false;
            this.xrLabel_随访方式_4.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell67,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell9});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1.2437297156350191D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "症";
            this.xrTableCell7.Weight = 0.31666666034638469D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = " 1 无症状   2 多饮  3 多\r\n 食  4 多尿   5 视力模糊";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 1.0249996021038188D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel_症状_1_1,
            this.xrLabel_症状_1_2,
            this.xrLabel11,
            this.xrLabel13,
            this.xrLabel_症状_1_3,
            this.xrLabel15,
            this.xrLabel_症状_1_4,
            this.xrLabel_症状_1_5,
            this.xrLabel18,
            this.xrLabel_症状_1_6,
            this.xrLabel20,
            this.xrLabel_症状_1_7,
            this.xrLabel22,
            this.xrLabel_症状_1_8});
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Weight = 1.6665518255749001D;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.CanGrow = false;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(46F, 12F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.Text = "/";
            // 
            // xrLabel_症状_1_1
            // 
            this.xrLabel_症状_1_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_1.CanGrow = false;
            this.xrLabel_症状_1_1.Dpi = 254F;
            this.xrLabel_症状_1_1.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_1.LocationFloat = new DevExpress.Utils.PointFloat(14F, 16F);
            this.xrLabel_症状_1_1.Name = "xrLabel_症状_1_1";
            this.xrLabel_症状_1_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_1.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_1.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_1.StylePriority.UseFont = false;
            this.xrLabel_症状_1_1.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_1_2
            // 
            this.xrLabel_症状_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_2.CanGrow = false;
            this.xrLabel_症状_1_2.Dpi = 254F;
            this.xrLabel_症状_1_2.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_2.LocationFloat = new DevExpress.Utils.PointFloat(66F, 16F);
            this.xrLabel_症状_1_2.Name = "xrLabel_症状_1_2";
            this.xrLabel_症状_1_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_2.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_2.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_2.StylePriority.UseFont = false;
            this.xrLabel_症状_1_2.StylePriority.UsePadding = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.CanGrow = false;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(98F, 12F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.Text = "/";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.CanGrow = false;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(150F, 12F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.Text = "/";
            // 
            // xrLabel_症状_1_3
            // 
            this.xrLabel_症状_1_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_3.CanGrow = false;
            this.xrLabel_症状_1_3.Dpi = 254F;
            this.xrLabel_症状_1_3.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_3.LocationFloat = new DevExpress.Utils.PointFloat(118F, 16F);
            this.xrLabel_症状_1_3.Name = "xrLabel_症状_1_3";
            this.xrLabel_症状_1_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_3.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_3.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_3.StylePriority.UseFont = false;
            this.xrLabel_症状_1_3.StylePriority.UsePadding = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(202F, 12F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UsePadding = false;
            this.xrLabel15.Text = "/";
            // 
            // xrLabel_症状_1_4
            // 
            this.xrLabel_症状_1_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_4.CanGrow = false;
            this.xrLabel_症状_1_4.Dpi = 254F;
            this.xrLabel_症状_1_4.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_4.LocationFloat = new DevExpress.Utils.PointFloat(170F, 16F);
            this.xrLabel_症状_1_4.Name = "xrLabel_症状_1_4";
            this.xrLabel_症状_1_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_4.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_4.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_4.StylePriority.UseFont = false;
            this.xrLabel_症状_1_4.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_1_5
            // 
            this.xrLabel_症状_1_5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_5.CanGrow = false;
            this.xrLabel_症状_1_5.Dpi = 254F;
            this.xrLabel_症状_1_5.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_5.LocationFloat = new DevExpress.Utils.PointFloat(222.0001F, 16F);
            this.xrLabel_症状_1_5.Name = "xrLabel_症状_1_5";
            this.xrLabel_症状_1_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_5.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_5.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_5.StylePriority.UseFont = false;
            this.xrLabel_症状_1_5.StylePriority.UsePadding = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(254.0001F, 12F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UsePadding = false;
            this.xrLabel18.Text = "/";
            // 
            // xrLabel_症状_1_6
            // 
            this.xrLabel_症状_1_6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_6.CanGrow = false;
            this.xrLabel_症状_1_6.Dpi = 254F;
            this.xrLabel_症状_1_6.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_6.LocationFloat = new DevExpress.Utils.PointFloat(274.0001F, 16F);
            this.xrLabel_症状_1_6.Name = "xrLabel_症状_1_6";
            this.xrLabel_症状_1_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_6.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_6.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_6.StylePriority.UseFont = false;
            this.xrLabel_症状_1_6.StylePriority.UsePadding = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.CanGrow = false;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(306.0001F, 12F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UsePadding = false;
            this.xrLabel20.Text = "/";
            // 
            // xrLabel_症状_1_7
            // 
            this.xrLabel_症状_1_7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_7.CanGrow = false;
            this.xrLabel_症状_1_7.Dpi = 254F;
            this.xrLabel_症状_1_7.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_7.LocationFloat = new DevExpress.Utils.PointFloat(326.0001F, 16F);
            this.xrLabel_症状_1_7.Name = "xrLabel_症状_1_7";
            this.xrLabel_症状_1_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_7.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_7.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_7.StylePriority.UseFont = false;
            this.xrLabel_症状_1_7.StylePriority.UsePadding = false;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.CanGrow = false;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(358.0001F, 12F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UsePadding = false;
            this.xrLabel22.Text = "/";
            // 
            // xrLabel_症状_1_8
            // 
            this.xrLabel_症状_1_8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_1_8.CanGrow = false;
            this.xrLabel_症状_1_8.Dpi = 254F;
            this.xrLabel_症状_1_8.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_1_8.LocationFloat = new DevExpress.Utils.PointFloat(378.0001F, 16F);
            this.xrLabel_症状_1_8.Name = "xrLabel_症状_1_8";
            this.xrLabel_症状_1_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_1_8.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_1_8.StylePriority.UseBorders = false;
            this.xrLabel_症状_1_8.StylePriority.UseFont = false;
            this.xrLabel_症状_1_8.StylePriority.UsePadding = false;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel25,
            this.xrLabel_症状_2_1,
            this.xrLabel_症状_2_2,
            this.xrLabel28,
            this.xrLabel29,
            this.xrLabel_症状_2_3,
            this.xrLabel31,
            this.xrLabel_症状_2_4,
            this.xrLabel_症状_2_5,
            this.xrLabel34,
            this.xrLabel_症状_2_6,
            this.xrLabel36,
            this.xrLabel_症状_2_7,
            this.xrLabel38,
            this.xrLabel_症状_2_8});
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Weight = 1.6665518255749001D;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(45.65207F, 12.75F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.Text = "/";
            // 
            // xrLabel_症状_2_1
            // 
            this.xrLabel_症状_2_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_1.CanGrow = false;
            this.xrLabel_症状_2_1.Dpi = 254F;
            this.xrLabel_症状_2_1.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_1.LocationFloat = new DevExpress.Utils.PointFloat(13.65207F, 16.75F);
            this.xrLabel_症状_2_1.Name = "xrLabel_症状_2_1";
            this.xrLabel_症状_2_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_1.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_1.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_1.StylePriority.UseFont = false;
            this.xrLabel_症状_2_1.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_2_2
            // 
            this.xrLabel_症状_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_2.CanGrow = false;
            this.xrLabel_症状_2_2.Dpi = 254F;
            this.xrLabel_症状_2_2.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_2.LocationFloat = new DevExpress.Utils.PointFloat(65.65207F, 16.75F);
            this.xrLabel_症状_2_2.Name = "xrLabel_症状_2_2";
            this.xrLabel_症状_2_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_2.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_2.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_2.StylePriority.UseFont = false;
            this.xrLabel_症状_2_2.StylePriority.UsePadding = false;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.CanGrow = false;
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(97.65207F, 12.75F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UsePadding = false;
            this.xrLabel28.Text = "/";
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.CanGrow = false;
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(149.6521F, 12.75F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UsePadding = false;
            this.xrLabel29.Text = "/";
            // 
            // xrLabel_症状_2_3
            // 
            this.xrLabel_症状_2_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_3.CanGrow = false;
            this.xrLabel_症状_2_3.Dpi = 254F;
            this.xrLabel_症状_2_3.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_3.LocationFloat = new DevExpress.Utils.PointFloat(117.6521F, 16.75F);
            this.xrLabel_症状_2_3.Name = "xrLabel_症状_2_3";
            this.xrLabel_症状_2_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_3.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_3.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_3.StylePriority.UseFont = false;
            this.xrLabel_症状_2_3.StylePriority.UsePadding = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.CanGrow = false;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(201.6521F, 12.75F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UsePadding = false;
            this.xrLabel31.Text = "/";
            // 
            // xrLabel_症状_2_4
            // 
            this.xrLabel_症状_2_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_4.CanGrow = false;
            this.xrLabel_症状_2_4.Dpi = 254F;
            this.xrLabel_症状_2_4.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_4.LocationFloat = new DevExpress.Utils.PointFloat(169.6521F, 16.75F);
            this.xrLabel_症状_2_4.Name = "xrLabel_症状_2_4";
            this.xrLabel_症状_2_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_4.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_4.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_4.StylePriority.UseFont = false;
            this.xrLabel_症状_2_4.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_2_5
            // 
            this.xrLabel_症状_2_5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_5.CanGrow = false;
            this.xrLabel_症状_2_5.Dpi = 254F;
            this.xrLabel_症状_2_5.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_5.LocationFloat = new DevExpress.Utils.PointFloat(221.6521F, 16.75F);
            this.xrLabel_症状_2_5.Name = "xrLabel_症状_2_5";
            this.xrLabel_症状_2_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_5.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_5.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_5.StylePriority.UseFont = false;
            this.xrLabel_症状_2_5.StylePriority.UsePadding = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.CanGrow = false;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(253.6521F, 12.75F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UsePadding = false;
            this.xrLabel34.Text = "/";
            // 
            // xrLabel_症状_2_6
            // 
            this.xrLabel_症状_2_6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_6.CanGrow = false;
            this.xrLabel_症状_2_6.Dpi = 254F;
            this.xrLabel_症状_2_6.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_6.LocationFloat = new DevExpress.Utils.PointFloat(273.6521F, 16.75F);
            this.xrLabel_症状_2_6.Name = "xrLabel_症状_2_6";
            this.xrLabel_症状_2_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_6.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_6.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_6.StylePriority.UseFont = false;
            this.xrLabel_症状_2_6.StylePriority.UsePadding = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.CanGrow = false;
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(305.6521F, 12.75F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UsePadding = false;
            this.xrLabel36.Text = "/";
            // 
            // xrLabel_症状_2_7
            // 
            this.xrLabel_症状_2_7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_7.CanGrow = false;
            this.xrLabel_症状_2_7.Dpi = 254F;
            this.xrLabel_症状_2_7.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_7.LocationFloat = new DevExpress.Utils.PointFloat(325.6521F, 16.75F);
            this.xrLabel_症状_2_7.Name = "xrLabel_症状_2_7";
            this.xrLabel_症状_2_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_7.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_7.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_7.StylePriority.UseFont = false;
            this.xrLabel_症状_2_7.StylePriority.UsePadding = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.CanGrow = false;
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(357.6521F, 12.75F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UsePadding = false;
            this.xrLabel38.Text = "/";
            // 
            // xrLabel_症状_2_8
            // 
            this.xrLabel_症状_2_8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_2_8.CanGrow = false;
            this.xrLabel_症状_2_8.Dpi = 254F;
            this.xrLabel_症状_2_8.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_2_8.LocationFloat = new DevExpress.Utils.PointFloat(377.6521F, 16.75F);
            this.xrLabel_症状_2_8.Name = "xrLabel_症状_2_8";
            this.xrLabel_症状_2_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_2_8.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_2_8.StylePriority.UseBorders = false;
            this.xrLabel_症状_2_8.StylePriority.UseFont = false;
            this.xrLabel_症状_2_8.StylePriority.UsePadding = false;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40,
            this.xrLabel_症状_3_1,
            this.xrLabel_症状_3_2,
            this.xrLabel43,
            this.xrLabel44,
            this.xrLabel_症状_3_3,
            this.xrLabel46,
            this.xrLabel_症状_3_4,
            this.xrLabel_症状_3_5,
            this.xrLabel49,
            this.xrLabel_症状_3_6,
            this.xrLabel51,
            this.xrLabel_症状_3_7,
            this.xrLabel53,
            this.xrLabel_症状_3_8});
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Weight = 1.6665412525563026D;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.CanGrow = false;
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(45.65073F, 12.75F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UsePadding = false;
            this.xrLabel40.Text = "/";
            // 
            // xrLabel_症状_3_1
            // 
            this.xrLabel_症状_3_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_1.CanGrow = false;
            this.xrLabel_症状_3_1.Dpi = 254F;
            this.xrLabel_症状_3_1.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_1.LocationFloat = new DevExpress.Utils.PointFloat(13.65073F, 16.75F);
            this.xrLabel_症状_3_1.Name = "xrLabel_症状_3_1";
            this.xrLabel_症状_3_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_1.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_1.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_1.StylePriority.UseFont = false;
            this.xrLabel_症状_3_1.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_3_2
            // 
            this.xrLabel_症状_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_2.CanGrow = false;
            this.xrLabel_症状_3_2.Dpi = 254F;
            this.xrLabel_症状_3_2.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_2.LocationFloat = new DevExpress.Utils.PointFloat(65.65073F, 16.75F);
            this.xrLabel_症状_3_2.Name = "xrLabel_症状_3_2";
            this.xrLabel_症状_3_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_2.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_2.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_2.StylePriority.UseFont = false;
            this.xrLabel_症状_3_2.StylePriority.UsePadding = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(97.65073F, 12.75F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.Text = "/";
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.CanGrow = false;
            this.xrLabel44.Dpi = 254F;
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(149.6507F, 12.75F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UsePadding = false;
            this.xrLabel44.Text = "/";
            // 
            // xrLabel_症状_3_3
            // 
            this.xrLabel_症状_3_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_3.CanGrow = false;
            this.xrLabel_症状_3_3.Dpi = 254F;
            this.xrLabel_症状_3_3.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_3.LocationFloat = new DevExpress.Utils.PointFloat(117.6507F, 16.75F);
            this.xrLabel_症状_3_3.Name = "xrLabel_症状_3_3";
            this.xrLabel_症状_3_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_3.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_3.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_3.StylePriority.UseFont = false;
            this.xrLabel_症状_3_3.StylePriority.UsePadding = false;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.CanGrow = false;
            this.xrLabel46.Dpi = 254F;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(201.6507F, 12.75F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UsePadding = false;
            this.xrLabel46.Text = "/";
            // 
            // xrLabel_症状_3_4
            // 
            this.xrLabel_症状_3_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_4.CanGrow = false;
            this.xrLabel_症状_3_4.Dpi = 254F;
            this.xrLabel_症状_3_4.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_4.LocationFloat = new DevExpress.Utils.PointFloat(169.6507F, 16.75F);
            this.xrLabel_症状_3_4.Name = "xrLabel_症状_3_4";
            this.xrLabel_症状_3_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_4.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_4.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_4.StylePriority.UseFont = false;
            this.xrLabel_症状_3_4.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_3_5
            // 
            this.xrLabel_症状_3_5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_5.CanGrow = false;
            this.xrLabel_症状_3_5.Dpi = 254F;
            this.xrLabel_症状_3_5.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_5.LocationFloat = new DevExpress.Utils.PointFloat(221.6508F, 16.75F);
            this.xrLabel_症状_3_5.Name = "xrLabel_症状_3_5";
            this.xrLabel_症状_3_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_5.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_5.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_5.StylePriority.UseFont = false;
            this.xrLabel_症状_3_5.StylePriority.UsePadding = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.CanGrow = false;
            this.xrLabel49.Dpi = 254F;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(253.6508F, 12.75F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UsePadding = false;
            this.xrLabel49.Text = "/";
            // 
            // xrLabel_症状_3_6
            // 
            this.xrLabel_症状_3_6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_6.CanGrow = false;
            this.xrLabel_症状_3_6.Dpi = 254F;
            this.xrLabel_症状_3_6.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_6.LocationFloat = new DevExpress.Utils.PointFloat(273.6508F, 16.75F);
            this.xrLabel_症状_3_6.Name = "xrLabel_症状_3_6";
            this.xrLabel_症状_3_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_6.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_6.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_6.StylePriority.UseFont = false;
            this.xrLabel_症状_3_6.StylePriority.UsePadding = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.CanGrow = false;
            this.xrLabel51.Dpi = 254F;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(305.6508F, 12.75F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UsePadding = false;
            this.xrLabel51.Text = "/";
            // 
            // xrLabel_症状_3_7
            // 
            this.xrLabel_症状_3_7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_7.CanGrow = false;
            this.xrLabel_症状_3_7.Dpi = 254F;
            this.xrLabel_症状_3_7.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_7.LocationFloat = new DevExpress.Utils.PointFloat(325.6508F, 16.75F);
            this.xrLabel_症状_3_7.Name = "xrLabel_症状_3_7";
            this.xrLabel_症状_3_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_7.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_7.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_7.StylePriority.UseFont = false;
            this.xrLabel_症状_3_7.StylePriority.UsePadding = false;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.CanGrow = false;
            this.xrLabel53.Dpi = 254F;
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(357.6508F, 12.75F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UsePadding = false;
            this.xrLabel53.Text = "/";
            // 
            // xrLabel_症状_3_8
            // 
            this.xrLabel_症状_3_8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_3_8.CanGrow = false;
            this.xrLabel_症状_3_8.Dpi = 254F;
            this.xrLabel_症状_3_8.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_3_8.LocationFloat = new DevExpress.Utils.PointFloat(377.6508F, 16.75F);
            this.xrLabel_症状_3_8.Name = "xrLabel_症状_3_8";
            this.xrLabel_症状_3_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_3_8.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_3_8.StylePriority.UseBorders = false;
            this.xrLabel_症状_3_8.StylePriority.UseFont = false;
            this.xrLabel_症状_3_8.StylePriority.UsePadding = false;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel55,
            this.xrLabel_症状_4_1,
            this.xrLabel_症状_4_2,
            this.xrLabel58,
            this.xrLabel59,
            this.xrLabel_症状_4_3,
            this.xrLabel61,
            this.xrLabel_症状_4_4,
            this.xrLabel_症状_4_5,
            this.xrLabel64,
            this.xrLabel_症状_4_6,
            this.xrLabel66,
            this.xrLabel_症状_4_7,
            this.xrLabel68,
            this.xrLabel_症状_4_8});
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Weight = 1.6665623985934976D;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.CanGrow = false;
            this.xrLabel55.Dpi = 254F;
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(45.65341F, 12.75F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UsePadding = false;
            this.xrLabel55.Text = "/";
            // 
            // xrLabel_症状_4_1
            // 
            this.xrLabel_症状_4_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_1.CanGrow = false;
            this.xrLabel_症状_4_1.Dpi = 254F;
            this.xrLabel_症状_4_1.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_1.LocationFloat = new DevExpress.Utils.PointFloat(13.65341F, 16.75F);
            this.xrLabel_症状_4_1.Name = "xrLabel_症状_4_1";
            this.xrLabel_症状_4_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_1.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_1.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_1.StylePriority.UseFont = false;
            this.xrLabel_症状_4_1.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_4_2
            // 
            this.xrLabel_症状_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_2.CanGrow = false;
            this.xrLabel_症状_4_2.Dpi = 254F;
            this.xrLabel_症状_4_2.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_2.LocationFloat = new DevExpress.Utils.PointFloat(65.65341F, 16.75F);
            this.xrLabel_症状_4_2.Name = "xrLabel_症状_4_2";
            this.xrLabel_症状_4_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_2.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_2.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_2.StylePriority.UseFont = false;
            this.xrLabel_症状_4_2.StylePriority.UsePadding = false;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.CanGrow = false;
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(97.65341F, 12.75F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UsePadding = false;
            this.xrLabel58.Text = "/";
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.CanGrow = false;
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(149.6534F, 12.75F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UsePadding = false;
            this.xrLabel59.Text = "/";
            // 
            // xrLabel_症状_4_3
            // 
            this.xrLabel_症状_4_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_3.CanGrow = false;
            this.xrLabel_症状_4_3.Dpi = 254F;
            this.xrLabel_症状_4_3.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_3.LocationFloat = new DevExpress.Utils.PointFloat(117.6534F, 16.75F);
            this.xrLabel_症状_4_3.Name = "xrLabel_症状_4_3";
            this.xrLabel_症状_4_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_3.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_3.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_3.StylePriority.UseFont = false;
            this.xrLabel_症状_4_3.StylePriority.UsePadding = false;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.CanGrow = false;
            this.xrLabel61.Dpi = 254F;
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(201.6534F, 12.75F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UsePadding = false;
            this.xrLabel61.Text = "/";
            // 
            // xrLabel_症状_4_4
            // 
            this.xrLabel_症状_4_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_4.CanGrow = false;
            this.xrLabel_症状_4_4.Dpi = 254F;
            this.xrLabel_症状_4_4.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_4.LocationFloat = new DevExpress.Utils.PointFloat(169.6534F, 16.75F);
            this.xrLabel_症状_4_4.Name = "xrLabel_症状_4_4";
            this.xrLabel_症状_4_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_4.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_4.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_4.StylePriority.UseFont = false;
            this.xrLabel_症状_4_4.StylePriority.UsePadding = false;
            // 
            // xrLabel_症状_4_5
            // 
            this.xrLabel_症状_4_5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_5.CanGrow = false;
            this.xrLabel_症状_4_5.Dpi = 254F;
            this.xrLabel_症状_4_5.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_5.LocationFloat = new DevExpress.Utils.PointFloat(221.6535F, 16.75F);
            this.xrLabel_症状_4_5.Name = "xrLabel_症状_4_5";
            this.xrLabel_症状_4_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_5.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_5.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_5.StylePriority.UseFont = false;
            this.xrLabel_症状_4_5.StylePriority.UsePadding = false;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.CanGrow = false;
            this.xrLabel64.Dpi = 254F;
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(253.6535F, 12.75F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UsePadding = false;
            this.xrLabel64.Text = "/";
            // 
            // xrLabel_症状_4_6
            // 
            this.xrLabel_症状_4_6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_6.CanGrow = false;
            this.xrLabel_症状_4_6.Dpi = 254F;
            this.xrLabel_症状_4_6.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_6.LocationFloat = new DevExpress.Utils.PointFloat(273.6535F, 16.75F);
            this.xrLabel_症状_4_6.Name = "xrLabel_症状_4_6";
            this.xrLabel_症状_4_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_6.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_6.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_6.StylePriority.UseFont = false;
            this.xrLabel_症状_4_6.StylePriority.UsePadding = false;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.CanGrow = false;
            this.xrLabel66.Dpi = 254F;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(305.6535F, 12.75F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UsePadding = false;
            this.xrLabel66.Text = "/";
            // 
            // xrLabel_症状_4_7
            // 
            this.xrLabel_症状_4_7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_7.CanGrow = false;
            this.xrLabel_症状_4_7.Dpi = 254F;
            this.xrLabel_症状_4_7.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_7.LocationFloat = new DevExpress.Utils.PointFloat(325.6535F, 16.75F);
            this.xrLabel_症状_4_7.Name = "xrLabel_症状_4_7";
            this.xrLabel_症状_4_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_7.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_7.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_7.StylePriority.UseFont = false;
            this.xrLabel_症状_4_7.StylePriority.UsePadding = false;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.CanGrow = false;
            this.xrLabel68.Dpi = 254F;
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(357.6535F, 12.75F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(20F, 34F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UsePadding = false;
            this.xrLabel68.Text = "/";
            // 
            // xrLabel_症状_4_8
            // 
            this.xrLabel_症状_4_8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状_4_8.CanGrow = false;
            this.xrLabel_症状_4_8.Dpi = 254F;
            this.xrLabel_症状_4_8.Font = new System.Drawing.Font("微软雅黑", 5.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_症状_4_8.LocationFloat = new DevExpress.Utils.PointFloat(377.6535F, 16.75F);
            this.xrLabel_症状_4_8.Name = "xrLabel_症状_4_8";
            this.xrLabel_症状_4_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_症状_4_8.SizeF = new System.Drawing.SizeF(32F, 34F);
            this.xrLabel_症状_4_8.StylePriority.UseBorders = false;
            this.xrLabel_症状_4_8.StylePriority.UseFont = false;
            this.xrLabel_症状_4_8.StylePriority.UsePadding = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell74,
            this.xrTable_症状其他_1_1,
            this.xrTableCell75,
            this.xrTable_症状其他_2_1,
            this.xrTableCell76,
            this.xrTable_症状其他_3_1,
            this.xrTableCell77,
            this.xrTable_症状其他_4_1});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1.2637898417539972D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 0.31666672042035415D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = " 6感染  7 手脚麻木  8下\r\n 肢浮肿    9 体重明显下";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 1.0249996021038188D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.Text = "其他：";
            this.xrTableCell74.Weight = 0.41732281067299237D;
            // 
            // xrTable_症状其他_1_1
            // 
            this.xrTable_症状其他_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_症状其他_1_1.CanGrow = false;
            this.xrTable_症状其他_1_1.Dpi = 254F;
            this.xrTable_症状其他_1_1.Name = "xrTable_症状其他_1_1";
            this.xrTable_症状其他_1_1.StylePriority.UseBorders = false;
            this.xrTable_症状其他_1_1.Weight = 1.2492289998834152D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.Text = "其他：";
            this.xrTableCell75.Weight = 0.417322840709977D;
            // 
            // xrTable_症状其他_2_1
            // 
            this.xrTable_症状其他_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_症状其他_2_1.CanGrow = false;
            this.xrTable_症状其他_2_1.Dpi = 254F;
            this.xrTable_症状其他_2_1.Name = "xrTable_症状其他_2_1";
            this.xrTable_症状其他_2_1.StylePriority.UseBorders = false;
            this.xrTable_症状其他_2_1.Weight = 1.2492289698464307D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.Text = "其他：";
            this.xrTableCell76.Weight = 0.417322840709977D;
            // 
            // xrTable_症状其他_3_1
            // 
            this.xrTable_症状其他_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_症状其他_3_1.CanGrow = false;
            this.xrTable_症状其他_3_1.Dpi = 254F;
            this.xrTable_症状其他_3_1.Name = "xrTable_症状其他_3_1";
            this.xrTable_症状其他_3_1.StylePriority.UseBorders = false;
            this.xrTable_症状其他_3_1.Weight = 1.2492185169757719D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.Text = "其他：";
            this.xrTableCell77.Weight = 0.417322840709977D;
            // 
            // xrTable_症状其他_4_1
            // 
            this.xrTable_症状其他_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_症状其他_4_1.CanGrow = false;
            this.xrTable_症状其他_4_1.Dpi = 254F;
            this.xrTable_症状其他_4_1.Name = "xrTable_症状其他_4_1";
            this.xrTable_症状其他_4_1.StylePriority.UseBorders = false;
            this.xrTable_症状其他_4_1.Weight = 1.2492394227170895D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTable_症状其他_1_2,
            this.xrTable_症状其他_2_2,
            this.xrTable_症状其他_3_2,
            this.xrTable_症状其他_4_2});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.2437296633543185D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "状";
            this.xrTableCell13.Weight = 0.31666648012447696D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.BorderWidth = 1F;
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseBorderWidth = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = " 降";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell14.Weight = 1.0249997222517577D;
            // 
            // xrTable_症状其他_1_2
            // 
            this.xrTable_症状其他_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_症状其他_1_2.CanGrow = false;
            this.xrTable_症状其他_1_2.Dpi = 254F;
            this.xrTable_症状其他_1_2.Name = "xrTable_症状其他_1_2";
            this.xrTable_症状其他_1_2.StylePriority.UseBorders = false;
            this.xrTable_症状其他_1_2.Weight = 1.6665518405933921D;
            // 
            // xrTable_症状其他_2_2
            // 
            this.xrTable_症状其他_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_症状其他_2_2.CanGrow = false;
            this.xrTable_症状其他_2_2.Dpi = 254F;
            this.xrTable_症状其他_2_2.Name = "xrTable_症状其他_2_2";
            this.xrTable_症状其他_2_2.StylePriority.UseBorders = false;
            this.xrTable_症状其他_2_2.Weight = 1.6665518405933921D;
            // 
            // xrTable_症状其他_3_2
            // 
            this.xrTable_症状其他_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_症状其他_3_2.CanGrow = false;
            this.xrTable_症状其他_3_2.Dpi = 254F;
            this.xrTable_症状其他_3_2.Name = "xrTable_症状其他_3_2";
            this.xrTable_症状其他_3_2.StylePriority.UseBorders = false;
            this.xrTable_症状其他_3_2.Weight = 1.6665412675747946D;
            // 
            // xrTable_症状其他_4_2
            // 
            this.xrTable_症状其他_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_症状其他_4_2.CanGrow = false;
            this.xrTable_症状其他_4_2.Dpi = 254F;
            this.xrTable_症状其他_4_2.Name = "xrTable_症状其他_4_2";
            this.xrTable_症状其他_4_2.StylePriority.UseBorders = false;
            this.xrTable_症状其他_4_2.Weight = 1.6665624136119897D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTable_血压_1,
            this.xrTable_血压_2,
            this.xrTable_血压_3,
            this.xrTable_血压_4});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.0159612278256041D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.31666660027241544D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Text = "血压（mmHg）";
            this.xrTableCell17.Weight = 1.0250001427695428D;
            // 
            // xrTable_血压_1
            // 
            this.xrTable_血压_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_血压_1.CanGrow = false;
            this.xrTable_血压_1.Dpi = 254F;
            this.xrTable_血压_1.Name = "xrTable_血压_1";
            this.xrTable_血压_1.StylePriority.UseBorders = false;
            this.xrTable_血压_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_血压_2
            // 
            this.xrTable_血压_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_血压_2.CanGrow = false;
            this.xrTable_血压_2.Dpi = 254F;
            this.xrTable_血压_2.Name = "xrTable_血压_2";
            this.xrTable_血压_2.StylePriority.UseBorders = false;
            this.xrTable_血压_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_血压_3
            // 
            this.xrTable_血压_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_血压_3.CanGrow = false;
            this.xrTable_血压_3.Dpi = 254F;
            this.xrTable_血压_3.Name = "xrTable_血压_3";
            this.xrTable_血压_3.StylePriority.UseBorders = false;
            this.xrTable_血压_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_血压_4
            // 
            this.xrTable_血压_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_血压_4.CanGrow = false;
            this.xrTable_血压_4.Dpi = 254F;
            this.xrTable_血压_4.Name = "xrTable_血压_4";
            this.xrTable_血压_4.StylePriority.UseBorders = false;
            this.xrTable_血压_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTable_体重_1_1,
            this.xrTableCell12,
            this.xrTable_体重_1_2,
            this.xrTable_体重_2_1,
            this.xrTableCell18,
            this.xrTable_体重_2_2,
            this.xrTable_体重_3_1,
            this.xrTableCell48,
            this.xrTable_体重_3_2,
            this.xrTable_体重_4_1,
            this.xrTableCell50,
            this.xrTable_体重_4_2});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.0000240005493164D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Weight = 0.31666660027241544D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.Text = "体重（kg）";
            this.xrTableCell23.Weight = 1.0250001427695428D;
            // 
            // xrTable_体重_1_1
            // 
            this.xrTable_体重_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重_1_1.CanGrow = false;
            this.xrTable_体重_1_1.Dpi = 254F;
            this.xrTable_体重_1_1.Name = "xrTable_体重_1_1";
            this.xrTable_体重_1_1.StylePriority.UseBorders = false;
            this.xrTable_体重_1_1.Weight = 0.79173228144276164D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = "/";
            this.xrTableCell12.Weight = 0.083070866485043734D;
            // 
            // xrTable_体重_1_2
            // 
            this.xrTable_体重_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体重_1_2.CanGrow = false;
            this.xrTable_体重_1_2.Dpi = 254F;
            this.xrTable_体重_1_2.Name = "xrTable_体重_1_2";
            this.xrTable_体重_1_2.StylePriority.UseBorders = false;
            this.xrTable_体重_1_2.Weight = 0.791748557499156D;
            // 
            // xrTable_体重_2_1
            // 
            this.xrTable_体重_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重_2_1.CanGrow = false;
            this.xrTable_体重_2_1.Dpi = 254F;
            this.xrTable_体重_2_1.Name = "xrTable_体重_2_1";
            this.xrTable_体重_2_1.StylePriority.UseBorders = false;
            this.xrTable_体重_2_1.Weight = 0.791732251405777D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Text = "/";
            this.xrTableCell18.Weight = 0.083070862730420719D;
            // 
            // xrTable_体重_2_2
            // 
            this.xrTable_体重_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体重_2_2.CanGrow = false;
            this.xrTable_体重_2_2.Dpi = 254F;
            this.xrTable_体重_2_2.Name = "xrTable_体重_2_2";
            this.xrTable_体重_2_2.StylePriority.UseBorders = false;
            this.xrTable_体重_2_2.Weight = 0.79174859129076369D;
            // 
            // xrTable_体重_3_1
            // 
            this.xrTable_体重_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重_3_1.CanGrow = false;
            this.xrTable_体重_3_1.Dpi = 254F;
            this.xrTable_体重_3_1.Name = "xrTable_体重_3_1";
            this.xrTable_体重_3_1.StylePriority.UseBorders = false;
            this.xrTable_体重_3_1.Weight = 0.791732251405777D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Text = "/";
            this.xrTableCell48.Weight = 0.083070862730420719D;
            // 
            // xrTable_体重_3_2
            // 
            this.xrTable_体重_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体重_3_2.CanGrow = false;
            this.xrTable_体重_3_2.Dpi = 254F;
            this.xrTable_体重_3_2.Name = "xrTable_体重_3_2";
            this.xrTable_体重_3_2.StylePriority.UseBorders = false;
            this.xrTable_体重_3_2.Weight = 0.79174859129076369D;
            // 
            // xrTable_体重_4_1
            // 
            this.xrTable_体重_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重_4_1.CanGrow = false;
            this.xrTable_体重_4_1.Dpi = 254F;
            this.xrTable_体重_4_1.Name = "xrTable_体重_4_1";
            this.xrTable_体重_4_1.StylePriority.UseBorders = false;
            this.xrTable_体重_4_1.Weight = 0.791732251405777D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Text = "/";
            this.xrTableCell50.Weight = 0.083070862730420719D;
            // 
            // xrTable_体重_4_2
            // 
            this.xrTable_体重_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体重_4_2.CanGrow = false;
            this.xrTable_体重_4_2.Dpi = 254F;
            this.xrTable_体重_4_2.Name = "xrTable_体重_4_2";
            this.xrTable_体重_4_2.StylePriority.UseBorders = false;
            this.xrTable_体重_4_2.Weight = 0.79174859129076369D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTable_体质指数_1_1,
            this.xrTableCell68,
            this.xrTable_体质指数_1_2,
            this.xrTable_体质指数_2_1,
            this.xrTableCell69,
            this.xrTable_体质指数_2_2,
            this.xrTable_体质指数_3_1,
            this.xrTableCell71,
            this.xrTable_体质指数_3_2,
            this.xrTable_体质指数_4_1,
            this.xrTableCell73,
            this.xrTable_体质指数_4_2});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Text = "体";
            this.xrTableCell25.Weight = 0.31666635997653825D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "体质指数（kg/㎡）";
            this.xrTableCell26.Weight = 1.0250003830654202D;
            // 
            // xrTable_体质指数_1_1
            // 
            this.xrTable_体质指数_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体质指数_1_1.CanGrow = false;
            this.xrTable_体质指数_1_1.Dpi = 254F;
            this.xrTable_体质指数_1_1.Name = "xrTable_体质指数_1_1";
            this.xrTable_体质指数_1_1.StylePriority.UseBorders = false;
            this.xrTable_体质指数_1_1.Weight = 0.79173199961136664D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Text = "/";
            this.xrTableCell68.Weight = 0.083070994142228571D;
            // 
            // xrTable_体质指数_1_2
            // 
            this.xrTable_体质指数_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体质指数_1_2.CanGrow = false;
            this.xrTable_体质指数_1_2.Dpi = 254F;
            this.xrTable_体质指数_1_2.Name = "xrTable_体质指数_1_2";
            this.xrTable_体质指数_1_2.StylePriority.UseBorders = false;
            this.xrTable_体质指数_1_2.Weight = 0.79174871167336625D;
            // 
            // xrTable_体质指数_2_1
            // 
            this.xrTable_体质指数_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体质指数_2_1.CanGrow = false;
            this.xrTable_体质指数_2_1.Dpi = 254F;
            this.xrTable_体质指数_2_1.Name = "xrTable_体质指数_2_1";
            this.xrTable_体质指数_2_1.StylePriority.UseBorders = false;
            this.xrTable_体质指数_2_1.Weight = 0.79390533713438372D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Text = "/";
            this.xrTableCell69.Weight = 0.0808977582286986D;
            // 
            // xrTable_体质指数_2_2
            // 
            this.xrTable_体质指数_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体质指数_2_2.CanGrow = false;
            this.xrTable_体质指数_2_2.Dpi = 254F;
            this.xrTable_体质指数_2_2.Name = "xrTable_体质指数_2_2";
            this.xrTable_体质指数_2_2.StylePriority.UseBorders = false;
            this.xrTable_体质指数_2_2.Weight = 0.79174861006387909D;
            // 
            // xrTable_体质指数_3_1
            // 
            this.xrTable_体质指数_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体质指数_3_1.CanGrow = false;
            this.xrTable_体质指数_3_1.Dpi = 254F;
            this.xrTable_体质指数_3_1.Name = "xrTable_体质指数_3_1";
            this.xrTable_体质指数_3_1.StylePriority.UseBorders = false;
            this.xrTable_体质指数_3_1.Weight = 0.793899960514131D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Text = "/";
            this.xrTableCell71.Weight = 0.078739660955433166D;
            // 
            // xrTable_体质指数_3_2
            // 
            this.xrTable_体质指数_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体质指数_3_2.CanGrow = false;
            this.xrTable_体质指数_3_2.Dpi = 254F;
            this.xrTable_体质指数_3_2.Name = "xrTable_体质指数_3_2";
            this.xrTable_体质指数_3_2.StylePriority.UseBorders = false;
            this.xrTable_体质指数_3_2.Weight = 0.79391208395739721D;
            // 
            // xrTable_体质指数_4_1
            // 
            this.xrTable_体质指数_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体质指数_4_1.CanGrow = false;
            this.xrTable_体质指数_4_1.Dpi = 254F;
            this.xrTable_体质指数_4_1.Name = "xrTable_体质指数_4_1";
            this.xrTable_体质指数_4_1.StylePriority.UseBorders = false;
            this.xrTable_体质指数_4_1.Weight = 0.79390002058810039D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Text = "/";
            this.xrTableCell73.Weight = 0.080901843258611217D;
            // 
            // xrTable_体质指数_4_2
            // 
            this.xrTable_体质指数_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体质指数_4_2.CanGrow = false;
            this.xrTable_体质指数_4_2.Dpi = 254F;
            this.xrTable_体质指数_4_2.Name = "xrTable_体质指数_4_2";
            this.xrTable_体质指数_4_2.StylePriority.UseBorders = false;
            this.xrTable_体质指数_4_2.Weight = 0.79174984158024975D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell228,
            this.xrTable_心率_1,
            this.xrTableCell229,
            this.xrTable_心率_2,
            this.xrTableCell232,
            this.xrTable_心率_3,
            this.xrTableCell233,
            this.xrTable_心率_4});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Weight = 0.31666635997653825D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Weight = 1.0250003830654202D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Dpi = 254F;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseTextAlignment = false;
            this.xrTableCell228.Text = " 1 触及正常";
            this.xrTableCell228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell228.Weight = 1.3877956550255168D;
            // 
            // xrTable_心率_1
            // 
            this.xrTable_心率_1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_心率_1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动_1});
            this.xrTable_心率_1.Dpi = 254F;
            this.xrTable_心率_1.Name = "xrTable_心率_1";
            this.xrTable_心率_1.StylePriority.UseBorders = false;
            this.xrTable_心率_1.Weight = 0.27875605040144458D;
            // 
            // xrLabel_足背动脉搏动_1
            // 
            this.xrLabel_足背动脉搏动_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动_1.CanGrow = false;
            this.xrLabel_足背动脉搏动_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 10F);
            this.xrLabel_足背动脉搏动_1.Name = "xrLabel_足背动脉搏动_1";
            this.xrLabel_足背动脉搏动_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_足背动脉搏动_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_足背动脉搏动_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动_1.StylePriority.UseFont = false;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Dpi = 254F;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseTextAlignment = false;
            this.xrTableCell229.Text = " 1 触及正常";
            this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell229.Weight = 1.3877951744337624D;
            // 
            // xrTable_心率_2
            // 
            this.xrTable_心率_2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_心率_2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动_2});
            this.xrTable_心率_2.Dpi = 254F;
            this.xrTable_心率_2.Name = "xrTable_心率_2";
            this.xrTable_心率_2.StylePriority.UseBorders = false;
            this.xrTable_心率_2.Weight = 0.27875653099319897D;
            // 
            // xrLabel_足背动脉搏动_2
            // 
            this.xrLabel_足背动脉搏动_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动_2.CanGrow = false;
            this.xrLabel_足背动脉搏动_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 10F);
            this.xrLabel_足背动脉搏动_2.Name = "xrLabel_足背动脉搏动_2";
            this.xrLabel_足背动脉搏动_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_足背动脉搏动_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_足背动脉搏动_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动_2.StylePriority.UseFont = false;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Dpi = 254F;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseTextAlignment = false;
            this.xrTableCell232.Text = " 1 触及正常";
            this.xrTableCell232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell232.Weight = 1.3877951744337624D;
            // 
            // xrTable_心率_3
            // 
            this.xrTable_心率_3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_心率_3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动_3});
            this.xrTable_心率_3.Dpi = 254F;
            this.xrTable_心率_3.Name = "xrTable_心率_3";
            this.xrTable_心率_3.StylePriority.UseBorders = false;
            this.xrTable_心率_3.Weight = 0.27875653099319897D;
            // 
            // xrLabel_足背动脉搏动_3
            // 
            this.xrLabel_足背动脉搏动_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动_3.CanGrow = false;
            this.xrLabel_足背动脉搏动_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 10F);
            this.xrLabel_足背动脉搏动_3.Name = "xrLabel_足背动脉搏动_3";
            this.xrLabel_足背动脉搏动_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_足背动脉搏动_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_足背动脉搏动_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动_3.StylePriority.UseFont = false;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Dpi = 254F;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = " 1 触及正常";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell233.Weight = 1.3877951744337624D;
            // 
            // xrTable_心率_4
            // 
            this.xrTable_心率_4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_心率_4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动_4});
            this.xrTable_心率_4.Dpi = 254F;
            this.xrTable_心率_4.Name = "xrTable_心率_4";
            this.xrTable_心率_4.StylePriority.UseBorders = false;
            this.xrTable_心率_4.Weight = 0.27875653099319897D;
            // 
            // xrLabel_足背动脉搏动_4
            // 
            this.xrLabel_足背动脉搏动_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动_4.CanGrow = false;
            this.xrLabel_足背动脉搏动_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 10F);
            this.xrLabel_足背动脉搏动_4.Name = "xrLabel_足背动脉搏动_4";
            this.xrLabel_足背动脉搏动_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_足背动脉搏动_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_足背动脉搏动_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动_4.StylePriority.UseFont = false;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell119,
            this.xrTableCell214,
            this.xrTableCell217,
            this.xrTableCell210,
            this.xrTableCell216,
            this.xrTableCell211,
            this.xrTableCell215,
            this.xrTableCell120,
            this.xrTableCell240,
            this.xrTableCell234,
            this.xrTableCell239,
            this.xrTableCell235,
            this.xrTableCell238,
            this.xrTableCell241,
            this.xrTableCell123,
            this.xrTableCell250,
            this.xrTableCell244,
            this.xrTableCell251,
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell124,
            this.xrTableCell257,
            this.xrTableCell252,
            this.xrTableCell258,
            this.xrTableCell253,
            this.xrTableCell256,
            this.xrTableCell259,
            this.xrTableCell129});
            this.xrTableRow45.Dpi = 254F;
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Text = "征";
            this.xrTableCell117.Weight = 0.31666635997653825D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Text = "足背动脉搏动";
            this.xrTableCell119.Weight = 1.0250003830654202D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Dpi = 254F;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseTextAlignment = false;
            this.xrTableCell214.Text = "  2减弱（双侧";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell214.Weight = 0.6692913194270782D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱双侧_1});
            this.xrTableCell217.Dpi = 254F;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 0.13779526958999094D;
            // 
            // xrLabel_足背动脉搏动减弱双侧_1
            // 
            this.xrLabel_足背动脉搏动减弱双侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱双侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱双侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱双侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱双侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱双侧_1.Name = "xrLabel_足背动脉搏动减弱双侧_1";
            this.xrLabel_足背动脉搏动减弱双侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱双侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱双侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱双侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱双侧_1.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱双侧_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱双侧_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Dpi = 254F;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "左侧";
            this.xrTableCell210.Weight = 0.22834644559256992D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱左侧_1});
            this.xrTableCell216.Dpi = 254F;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.13779526958999094D;
            // 
            // xrLabel_足背动脉搏动减弱左侧_1
            // 
            this.xrLabel_足背动脉搏动减弱左侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱左侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱左侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱左侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱左侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱左侧_1.Name = "xrLabel_足背动脉搏动减弱左侧_1";
            this.xrLabel_足背动脉搏动减弱左侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱左侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱左侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱左侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱左侧_1.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱左侧_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱左侧_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Dpi = 254F;
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Text = "右侧";
            this.xrTableCell211.Weight = 0.22834644559256986D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱右侧_1});
            this.xrTableCell215.Dpi = 254F;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 0.14173224720529232D;
            // 
            // xrLabel_足背动脉搏动减弱右侧_1
            // 
            this.xrLabel_足背动脉搏动减弱右侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱右侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱右侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱右侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱右侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱右侧_1.Name = "xrLabel_足背动脉搏动减弱右侧_1";
            this.xrLabel_足背动脉搏动减弱右侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱右侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱右侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱右侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱右侧_1.StylePriority.UsePadding = false;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.StylePriority.UseTextAlignment = false;
            this.xrTableCell120.Text = "）";
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell120.Weight = 0.12324470842946936D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Dpi = 254F;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseTextAlignment = false;
            this.xrTableCell240.Text = "  2减弱（双侧";
            this.xrTableCell240.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell240.Weight = 0.66929128939009352D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱双侧_2});
            this.xrTableCell234.Dpi = 254F;
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动减弱双侧_2
            // 
            this.xrLabel_足背动脉搏动减弱双侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱双侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱双侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱双侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱双侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱双侧_2.Name = "xrLabel_足背动脉搏动减弱双侧_2";
            this.xrLabel_足背动脉搏动减弱双侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱双侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱双侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱双侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱双侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱双侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱双侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Dpi = 254F;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Text = "左侧";
            this.xrTableCell239.Weight = 0.22834643808332367D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱左侧_2});
            this.xrTableCell235.Dpi = 254F;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动减弱左侧_2
            // 
            this.xrLabel_足背动脉搏动减弱左侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱左侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱左侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱左侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱左侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱左侧_2.Name = "xrLabel_足背动脉搏动减弱左侧_2";
            this.xrLabel_足背动脉搏动减弱左侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱左侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱左侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱左侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱左侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱左侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱左侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Dpi = 254F;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Text = "右侧";
            this.xrTableCell238.Weight = 0.22834643808332367D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱右侧_2});
            this.xrTableCell241.Dpi = 254F;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动减弱右侧_2
            // 
            this.xrLabel_足背动脉搏动减弱右侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱右侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱右侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱右侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱右侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱右侧_2.Name = "xrLabel_足背动脉搏动减弱右侧_2";
            this.xrLabel_足背动脉搏动减弱右侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱右侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱右侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱右侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱右侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱右侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱右侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = "）";
            this.xrTableCell123.Weight = 0.123244726322595D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Dpi = 254F;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseTextAlignment = false;
            this.xrTableCell250.Text = "  2减弱（双侧";
            this.xrTableCell250.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell250.Weight = 0.66929128939009352D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱双侧_3});
            this.xrTableCell244.Dpi = 254F;
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动减弱双侧_3
            // 
            this.xrLabel_足背动脉搏动减弱双侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱双侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱双侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱双侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱双侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱双侧_3.Name = "xrLabel_足背动脉搏动减弱双侧_3";
            this.xrLabel_足背动脉搏动减弱双侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱双侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱双侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱双侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱双侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱双侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱双侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Dpi = 254F;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Text = "左侧";
            this.xrTableCell251.Weight = 0.22834643808332367D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱左侧_3});
            this.xrTableCell245.Dpi = 254F;
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动减弱左侧_3
            // 
            this.xrLabel_足背动脉搏动减弱左侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱左侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱左侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱左侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱左侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱左侧_3.Name = "xrLabel_足背动脉搏动减弱左侧_3";
            this.xrLabel_足背动脉搏动减弱左侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱左侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱左侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱左侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱左侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱左侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱左侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Dpi = 254F;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Text = "右侧";
            this.xrTableCell246.Weight = 0.22834643808332367D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱右侧_3});
            this.xrTableCell247.Dpi = 254F;
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动减弱右侧_3
            // 
            this.xrLabel_足背动脉搏动减弱右侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱右侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱右侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱右侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱右侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱右侧_3.Name = "xrLabel_足背动脉搏动减弱右侧_3";
            this.xrLabel_足背动脉搏动减弱右侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱右侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱右侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱右侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱右侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱右侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱右侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "）";
            this.xrTableCell124.Weight = 0.123244726322595D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Dpi = 254F;
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "  2减弱（双侧";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell257.Weight = 0.66929128939009352D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱双侧_4});
            this.xrTableCell252.Dpi = 254F;
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动减弱双侧_4
            // 
            this.xrLabel_足背动脉搏动减弱双侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱双侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱双侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱双侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱双侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱双侧_4.Name = "xrLabel_足背动脉搏动减弱双侧_4";
            this.xrLabel_足背动脉搏动减弱双侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱双侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱双侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱双侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱双侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱双侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱双侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Dpi = 254F;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Text = "左侧";
            this.xrTableCell258.Weight = 0.22834643808332367D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱左侧_4});
            this.xrTableCell253.Dpi = 254F;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动减弱左侧_4
            // 
            this.xrLabel_足背动脉搏动减弱左侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱左侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱左侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱左侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱左侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱左侧_4.Name = "xrLabel_足背动脉搏动减弱左侧_4";
            this.xrLabel_足背动脉搏动减弱左侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱左侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱左侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱左侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱左侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱左侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱左侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Dpi = 254F;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Text = "右侧";
            this.xrTableCell256.Weight = 0.22834643808332367D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动减弱右侧_4});
            this.xrTableCell259.Dpi = 254F;
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动减弱右侧_4
            // 
            this.xrLabel_足背动脉搏动减弱右侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动减弱右侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动减弱右侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动减弱右侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动减弱右侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动减弱右侧_4.Name = "xrLabel_足背动脉搏动减弱右侧_4";
            this.xrLabel_足背动脉搏动减弱右侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动减弱右侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动减弱右侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动减弱右侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动减弱右侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动减弱右侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动减弱右侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Text = "）";
            this.xrTableCell129.Weight = 0.123244726322595D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell222,
            this.xrTableCell227,
            this.xrTableCell220,
            this.xrTableCell226,
            this.xrTableCell221,
            this.xrTableCell223,
            this.xrTableCell132,
            this.xrTableCell332,
            this.xrTableCell328,
            this.xrTableCell340,
            this.xrTableCell336,
            this.xrTableCell344,
            this.xrTableCell348,
            this.xrTableCell145,
            this.xrTableCell268,
            this.xrTableCell266,
            this.xrTableCell270,
            this.xrTableCell269,
            this.xrTableCell271,
            this.xrTableCell274,
            this.xrTableCell208,
            this.xrTableCell276,
            this.xrTableCell275,
            this.xrTableCell286,
            this.xrTableCell277,
            this.xrTableCell302,
            this.xrTableCell317,
            this.xrTableCell209});
            this.xrTableRow46.Dpi = 254F;
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.Weight = 0.31666635997653825D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 1.0250003830654202D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell222.Dpi = 254F;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.Text = "  3消失（双侧";
            this.xrTableCell222.Weight = 0.6692913194270782D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell227.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失双侧_1});
            this.xrTableCell227.Dpi = 254F;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.13779514944205232D;
            // 
            // xrLabel_足背动脉搏动消失双侧_1
            // 
            this.xrLabel_足背动脉搏动消失双侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失双侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动消失双侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失双侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失双侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失双侧_1.Name = "xrLabel_足背动脉搏动消失双侧_1";
            this.xrLabel_足背动脉搏动消失双侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失双侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失双侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失双侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失双侧_1.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失双侧_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失双侧_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell220.Dpi = 254F;
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseBorders = false;
            this.xrTableCell220.Text = "左侧";
            this.xrTableCell220.Weight = 0.22834644559256984D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell226.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失左侧_1});
            this.xrTableCell226.Dpi = 254F;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.13779499174788293D;
            // 
            // xrLabel_足背动脉搏动消失左侧_1
            // 
            this.xrLabel_足背动脉搏动消失左侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失左侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动消失左侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失左侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失左侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失左侧_1.Name = "xrLabel_足背动脉搏动消失左侧_1";
            this.xrLabel_足背动脉搏动消失左侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失左侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失左侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失左侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失左侧_1.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失左侧_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失左侧_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell221.Dpi = 254F;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.Text = "右侧";
            this.xrTableCell221.Weight = 0.228346453101816D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失右侧_1});
            this.xrTableCell223.Dpi = 254F;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseBorders = false;
            this.xrTableCell223.Weight = 0.14173251002890802D;
            // 
            // xrLabel_足背动脉搏动消失右侧_1
            // 
            this.xrLabel_足背动脉搏动消失右侧_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失右侧_1.CanGrow = false;
            this.xrLabel_足背动脉搏动消失右侧_1.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失右侧_1.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失右侧_1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失右侧_1.Name = "xrLabel_足背动脉搏动消失右侧_1";
            this.xrLabel_足背动脉搏动消失右侧_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失右侧_1.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失右侧_1.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失右侧_1.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失右侧_1.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失右侧_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失右侧_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Text = "）";
            this.xrTableCell132.Weight = 0.12324483608665415D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell332.Dpi = 254F;
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseBorders = false;
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "  3消失（双侧";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell332.Weight = 0.66929128939009352D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell328.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失双侧_2});
            this.xrTableCell328.Dpi = 254F;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBorders = false;
            this.xrTableCell328.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动消失双侧_2
            // 
            this.xrLabel_足背动脉搏动消失双侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失双侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动消失双侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失双侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失双侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失双侧_2.Name = "xrLabel_足背动脉搏动消失双侧_2";
            this.xrLabel_足背动脉搏动消失双侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失双侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失双侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失双侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失双侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失双侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失双侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell340.Dpi = 254F;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.Text = "左侧";
            this.xrTableCell340.Weight = 0.22834643808332367D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell336.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失左侧_2});
            this.xrTableCell336.Dpi = 254F;
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动消失左侧_2
            // 
            this.xrLabel_足背动脉搏动消失左侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失左侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动消失左侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失左侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失左侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失左侧_2.Name = "xrLabel_足背动脉搏动消失左侧_2";
            this.xrLabel_足背动脉搏动消失左侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失左侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失左侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失左侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失左侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失左侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失左侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell344.Dpi = 254F;
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseBorders = false;
            this.xrTableCell344.Text = "右侧";
            this.xrTableCell344.Weight = 0.22834643808332367D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell348.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失右侧_2});
            this.xrTableCell348.Dpi = 254F;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动消失右侧_2
            // 
            this.xrLabel_足背动脉搏动消失右侧_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失右侧_2.CanGrow = false;
            this.xrLabel_足背动脉搏动消失右侧_2.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失右侧_2.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失右侧_2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失右侧_2.Name = "xrLabel_足背动脉搏动消失右侧_2";
            this.xrLabel_足背动脉搏动消失右侧_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失右侧_2.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失右侧_2.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失右侧_2.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失右侧_2.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失右侧_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失右侧_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.Text = "）";
            this.xrTableCell145.Weight = 0.123244726322595D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell268.Dpi = 254F;
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseTextAlignment = false;
            this.xrTableCell268.Text = "  3消失（双侧";
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell268.Weight = 0.66929128939009352D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell266.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失双侧_3});
            this.xrTableCell266.Dpi = 254F;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动消失双侧_3
            // 
            this.xrLabel_足背动脉搏动消失双侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失双侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动消失双侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失双侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失双侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失双侧_3.Name = "xrLabel_足背动脉搏动消失双侧_3";
            this.xrLabel_足背动脉搏动消失双侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失双侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失双侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失双侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失双侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失双侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失双侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell270.Dpi = 254F;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Text = "左侧";
            this.xrTableCell270.Weight = 0.22834643808332367D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell269.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失左侧_3});
            this.xrTableCell269.Dpi = 254F;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBorders = false;
            this.xrTableCell269.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动消失左侧_3
            // 
            this.xrLabel_足背动脉搏动消失左侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失左侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动消失左侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失左侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失左侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失左侧_3.Name = "xrLabel_足背动脉搏动消失左侧_3";
            this.xrLabel_足背动脉搏动消失左侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失左侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失左侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失左侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失左侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失左侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失左侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell271.Dpi = 254F;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Text = "右侧";
            this.xrTableCell271.Weight = 0.22834643808332367D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell274.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失右侧_3});
            this.xrTableCell274.Dpi = 254F;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseBorders = false;
            this.xrTableCell274.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动消失右侧_3
            // 
            this.xrLabel_足背动脉搏动消失右侧_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失右侧_3.CanGrow = false;
            this.xrLabel_足背动脉搏动消失右侧_3.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失右侧_3.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失右侧_3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失右侧_3.Name = "xrLabel_足背动脉搏动消失右侧_3";
            this.xrLabel_足背动脉搏动消失右侧_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失右侧_3.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失右侧_3.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失右侧_3.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失右侧_3.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失右侧_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失右侧_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell208.Dpi = 254F;
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseBorders = false;
            this.xrTableCell208.Text = "）";
            this.xrTableCell208.Weight = 0.123244726322595D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell276.Dpi = 254F;
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBorders = false;
            this.xrTableCell276.StylePriority.UseTextAlignment = false;
            this.xrTableCell276.Text = "  3消失（双侧";
            this.xrTableCell276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell276.Weight = 0.66929128939009352D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell275.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失双侧_4});
            this.xrTableCell275.Dpi = 254F;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.Weight = 0.13779527134997052D;
            // 
            // xrLabel_足背动脉搏动消失双侧_4
            // 
            this.xrLabel_足背动脉搏动消失双侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失双侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动消失双侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失双侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失双侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失双侧_4.Name = "xrLabel_足背动脉搏动消失双侧_4";
            this.xrLabel_足背动脉搏动消失双侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失双侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失双侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失双侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失双侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失双侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失双侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell286.Dpi = 254F;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Text = "左侧";
            this.xrTableCell286.Weight = 0.22834643808332367D;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell277.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失左侧_4});
            this.xrTableCell277.Dpi = 254F;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.Weight = 0.13779526208074477D;
            // 
            // xrLabel_足背动脉搏动消失左侧_4
            // 
            this.xrLabel_足背动脉搏动消失左侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失左侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动消失左侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失左侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失左侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失左侧_4.Name = "xrLabel_足背动脉搏动消失左侧_4";
            this.xrLabel_足背动脉搏动消失左侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失左侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失左侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失左侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失左侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失左侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失左侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell302.Dpi = 254F;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Text = "右侧";
            this.xrTableCell302.Weight = 0.22834643808332367D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell317.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动消失右侧_4});
            this.xrTableCell317.Dpi = 254F;
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseBorders = false;
            this.xrTableCell317.Weight = 0.14173228011691028D;
            // 
            // xrLabel_足背动脉搏动消失右侧_4
            // 
            this.xrLabel_足背动脉搏动消失右侧_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动消失右侧_4.CanGrow = false;
            this.xrLabel_足背动脉搏动消失右侧_4.Dpi = 254F;
            this.xrLabel_足背动脉搏动消失右侧_4.Font = new System.Drawing.Font("微软雅黑", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_足背动脉搏动消失右侧_4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrLabel_足背动脉搏动消失右侧_4.Name = "xrLabel_足背动脉搏动消失右侧_4";
            this.xrLabel_足背动脉搏动消失右侧_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrLabel_足背动脉搏动消失右侧_4.SizeF = new System.Drawing.SizeF(30F, 30F);
            this.xrLabel_足背动脉搏动消失右侧_4.StylePriority.UseBorders = false;
            this.xrLabel_足背动脉搏动消失右侧_4.StylePriority.UseFont = false;
            this.xrLabel_足背动脉搏动消失右侧_4.StylePriority.UsePadding = false;
            this.xrLabel_足背动脉搏动消失右侧_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_足背动脉搏动消失右侧_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell209.Dpi = 254F;
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Text = "）";
            this.xrTableCell209.Weight = 0.123244726322595D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTable_体征其他_1,
            this.xrTable_体征其他_2,
            this.xrTable_体征其他_3,
            this.xrTable_体征其他_4});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Weight = 0.31666635997653825D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "其他";
            this.xrTableCell29.Weight = 1.0250003830654202D;
            // 
            // xrTable_体征其他_1
            // 
            this.xrTable_体征其他_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体征其他_1.CanGrow = false;
            this.xrTable_体征其他_1.Dpi = 254F;
            this.xrTable_体征其他_1.Name = "xrTable_体征其他_1";
            this.xrTable_体征其他_1.StylePriority.UseBorders = false;
            this.xrTable_体征其他_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_体征其他_2
            // 
            this.xrTable_体征其他_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体征其他_2.CanGrow = false;
            this.xrTable_体征其他_2.Dpi = 254F;
            this.xrTable_体征其他_2.Name = "xrTable_体征其他_2";
            this.xrTable_体征其他_2.StylePriority.UseBorders = false;
            this.xrTable_体征其他_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_体征其他_3
            // 
            this.xrTable_体征其他_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体征其他_3.CanGrow = false;
            this.xrTable_体征其他_3.Dpi = 254F;
            this.xrTable_体征其他_3.Name = "xrTable_体征其他_3";
            this.xrTable_体征其他_3.StylePriority.UseBorders = false;
            this.xrTable_体征其他_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_体征其他_4
            // 
            this.xrTable_体征其他_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体征其他_4.CanGrow = false;
            this.xrTable_体征其他_4.Dpi = 254F;
            this.xrTable_体征其他_4.Name = "xrTable_体征其他_4";
            this.xrTable_体征其他_4.StylePriority.UseBorders = false;
            this.xrTable_体征其他_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTable_日吸烟量_1_1,
            this.xrTableCell157,
            this.xrTable_日吸烟量_1_2,
            this.xrTable_日吸烟量_2_1,
            this.xrTableCell161,
            this.xrTable_日吸烟量_2_2,
            this.xrTable_日吸烟量_3_1,
            this.xrTableCell165,
            this.xrTable_日吸烟量_3_2,
            this.xrTable_日吸烟量_4_1,
            this.xrTableCell167,
            this.xrTable_日吸烟量_4_2});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Text = "生";
            this.xrTableCell31.Weight = 0.31666684056829264D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Text = "日吸烟量（支）";
            this.xrTableCell32.Weight = 1.0249997823257266D;
            // 
            // xrTable_日吸烟量_1_1
            // 
            this.xrTable_日吸烟量_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日吸烟量_1_1.CanGrow = false;
            this.xrTable_日吸烟量_1_1.Dpi = 254F;
            this.xrTable_日吸烟量_1_1.Name = "xrTable_日吸烟量_1_1";
            this.xrTable_日吸烟量_1_1.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_1_1.Weight = 0.79173210873009991D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Text = "/";
            this.xrTableCell157.Weight = 0.083071241947351826D;
            // 
            // xrTable_日吸烟量_1_2
            // 
            this.xrTable_日吸烟量_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日吸烟量_1_2.CanGrow = false;
            this.xrTable_日吸烟量_1_2.Dpi = 254F;
            this.xrTable_日吸烟量_1_2.Name = "xrTable_日吸烟量_1_2";
            this.xrTable_日吸烟量_1_2.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_1_2.Weight = 0.79174838478649412D;
            // 
            // xrTable_日吸烟量_2_1
            // 
            this.xrTable_日吸烟量_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日吸烟量_2_1.CanGrow = false;
            this.xrTable_日吸烟量_2_1.Dpi = 254F;
            this.xrTable_日吸烟量_2_1.Name = "xrTable_日吸烟量_2_1";
            this.xrTable_日吸烟量_2_1.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_2_1.Weight = 0.79173234902597711D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.Text = "/";
            this.xrTableCell161.Weight = 0.083070521059720237D;
            // 
            // xrTable_日吸烟量_2_2
            // 
            this.xrTable_日吸烟量_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日吸烟量_2_2.CanGrow = false;
            this.xrTable_日吸烟量_2_2.Dpi = 254F;
            this.xrTable_日吸烟量_2_2.Name = "xrTable_日吸烟量_2_2";
            this.xrTable_日吸烟量_2_2.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_2_2.Weight = 0.79174886537824851D;
            // 
            // xrTable_日吸烟量_3_1
            // 
            this.xrTable_日吸烟量_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日吸烟量_3_1.CanGrow = false;
            this.xrTable_日吸烟量_3_1.Dpi = 254F;
            this.xrTable_日吸烟量_3_1.Name = "xrTable_日吸烟量_3_1";
            this.xrTable_日吸烟量_3_1.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_3_1.Weight = 0.79390005813433118D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Text = "/";
            this.xrTableCell165.Weight = 0.080902811951366227D;
            // 
            // xrTable_日吸烟量_3_2
            // 
            this.xrTable_日吸烟量_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日吸烟量_3_2.CanGrow = false;
            this.xrTable_日吸烟量_3_2.Dpi = 254F;
            this.xrTable_日吸烟量_3_2.Name = "xrTable_日吸烟量_3_2";
            this.xrTable_日吸烟量_3_2.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_3_2.Weight = 0.79174886537824851D;
            // 
            // xrTable_日吸烟量_4_1
            // 
            this.xrTable_日吸烟量_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日吸烟量_4_1.CanGrow = false;
            this.xrTable_日吸烟量_4_1.Dpi = 254F;
            this.xrTable_日吸烟量_4_1.Name = "xrTable_日吸烟量_4_1";
            this.xrTable_日吸烟量_4_1.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_4_1.Weight = 0.79173159810136073D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.Text = "/";
            this.xrTableCell167.Weight = 0.083070288273089266D;
            // 
            // xrTable_日吸烟量_4_2
            // 
            this.xrTable_日吸烟量_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日吸烟量_4_2.CanGrow = false;
            this.xrTable_日吸烟量_4_2.Dpi = 254F;
            this.xrTable_日吸烟量_4_2.Name = "xrTable_日吸烟量_4_2";
            this.xrTable_日吸烟量_4_2.StylePriority.UseBorders = false;
            this.xrTable_日吸烟量_4_2.Weight = 0.79174984908949586D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTable_日饮酒量_1_1,
            this.xrTableCell158,
            this.xrTable_日饮酒量_1_2,
            this.xrTable_日饮酒量_2_1,
            this.xrTableCell162,
            this.xrTable_日饮酒量_2_2,
            this.xrTable_日饮酒量_3_1,
            this.xrTableCell166,
            this.xrTable_日饮酒量_3_2,
            this.xrTable_日饮酒量_4_1,
            this.xrTableCell169,
            this.xrTable_日饮酒量_4_2});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "活";
            this.xrTableCell34.Weight = 0.31666672042035415D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Text = "日饮酒量（两）";
            this.xrTableCell35.Weight = 1.0250000226216041D;
            // 
            // xrTable_日饮酒量_1_1
            // 
            this.xrTable_日饮酒量_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日饮酒量_1_1.CanGrow = false;
            this.xrTable_日饮酒量_1_1.Dpi = 254F;
            this.xrTable_日饮酒量_1_1.Name = "xrTable_日饮酒量_1_1";
            this.xrTable_日饮酒量_1_1.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_1_1.Weight = 0.7917321012208538D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "/";
            this.xrTableCell158.Weight = 0.083071234438105712D;
            // 
            // xrTable_日饮酒量_1_2
            // 
            this.xrTable_日饮酒量_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日饮酒量_1_2.CanGrow = false;
            this.xrTable_日饮酒量_1_2.Dpi = 254F;
            this.xrTable_日饮酒量_1_2.Name = "xrTable_日饮酒量_1_2";
            this.xrTable_日饮酒量_1_2.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_1_2.Weight = 0.79174836976800189D;
            // 
            // xrTable_日饮酒量_2_1
            // 
            this.xrTable_日饮酒量_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日饮酒量_2_1.CanGrow = false;
            this.xrTable_日饮酒量_2_1.Dpi = 254F;
            this.xrTable_日饮酒量_2_1.Name = "xrTable_日饮酒量_2_1";
            this.xrTable_日饮酒量_2_1.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_2_1.Weight = 0.791732341516731D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Text = "/";
            this.xrTableCell162.Weight = 0.083070513550474123D;
            // 
            // xrTable_日饮酒量_2_2
            // 
            this.xrTable_日饮酒量_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日饮酒量_2_2.CanGrow = false;
            this.xrTable_日饮酒量_2_2.Dpi = 254F;
            this.xrTable_日饮酒量_2_2.Name = "xrTable_日饮酒量_2_2";
            this.xrTable_日饮酒量_2_2.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_2_2.Weight = 0.79174885035975628D;
            // 
            // xrTable_日饮酒量_3_1
            // 
            this.xrTable_日饮酒量_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日饮酒量_3_1.CanGrow = false;
            this.xrTable_日饮酒量_3_1.Dpi = 254F;
            this.xrTable_日饮酒量_3_1.Name = "xrTable_日饮酒量_3_1";
            this.xrTable_日饮酒量_3_1.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_3_1.Weight = 0.79390005062508506D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Text = "/";
            this.xrTableCell166.Weight = 0.080902804442120113D;
            // 
            // xrTable_日饮酒量_3_2
            // 
            this.xrTable_日饮酒量_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日饮酒量_3_2.CanGrow = false;
            this.xrTable_日饮酒量_3_2.Dpi = 254F;
            this.xrTable_日饮酒量_3_2.Name = "xrTable_日饮酒量_3_2";
            this.xrTable_日饮酒量_3_2.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_3_2.Weight = 0.79174885035975628D;
            // 
            // xrTable_日饮酒量_4_1
            // 
            this.xrTable_日饮酒量_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_日饮酒量_4_1.CanGrow = false;
            this.xrTable_日饮酒量_4_1.Dpi = 254F;
            this.xrTable_日饮酒量_4_1.Name = "xrTable_日饮酒量_4_1";
            this.xrTable_日饮酒量_4_1.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_4_1.Weight = 0.79173159059211462D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.Text = "/";
            this.xrTableCell169.Weight = 0.083070288273089266D;
            // 
            // xrTable_日饮酒量_4_2
            // 
            this.xrTable_日饮酒量_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_日饮酒量_4_2.CanGrow = false;
            this.xrTable_日饮酒量_4_2.Dpi = 254F;
            this.xrTable_日饮酒量_4_2.Name = "xrTable_日饮酒量_4_2";
            this.xrTable_日饮酒量_4_2.StylePriority.UseBorders = false;
            this.xrTable_日饮酒量_4_2.Weight = 0.79174982656175752D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell15,
            this.xrTable_运动频率_1_1,
            this.xrTableCell24,
            this.xrTable_运动时长_1_1,
            this.xrTableCell21,
            this.xrTableCell59,
            this.xrTableCell171,
            this.xrTable_运动频率_2_1,
            this.xrTableCell163,
            this.xrTable_运动时长_2_1,
            this.xrTableCell170,
            this.xrTableCell70,
            this.xrTableCell176,
            this.xrTable_运动频率_3_1,
            this.xrTableCell172,
            this.xrTable_运动时长_3_1,
            this.xrTableCell175,
            this.xrTableCell72,
            this.xrTableCell181,
            this.xrTable_运动频率_4_1,
            this.xrTableCell177,
            this.xrTable_运动时长_4_1,
            this.xrTableCell180,
            this.xrTableCell78});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Text = "方";
            this.xrTableCell56.Weight = 0.31666672042035415D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Text = "运";
            this.xrTableCell57.Weight = 1.0250000226216041D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.18110199109201403D;
            // 
            // xrTable_运动频率_1_1
            // 
            this.xrTable_运动频率_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_1_1.CanGrow = false;
            this.xrTable_运动频率_1_1.Dpi = 254F;
            this.xrTable_运动频率_1_1.Name = "xrTable_运动频率_1_1";
            this.xrTable_运动频率_1_1.StylePriority.UseBorders = false;
            this.xrTable_运动频率_1_1.Weight = 0.25590550203320545D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "次/周";
            this.xrTableCell24.Weight = 0.32283459457583724D;
            // 
            // xrTable_运动时长_1_1
            // 
            this.xrTable_运动时长_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_1_1.CanGrow = false;
            this.xrTable_运动时长_1_1.Dpi = 254F;
            this.xrTable_运动时长_1_1.Name = "xrTable_运动时长_1_1";
            this.xrTable_运动时长_1_1.StylePriority.UseBorders = false;
            this.xrTable_运动时长_1_1.Weight = 0.25590549452395933D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Text = "分钟/次";
            this.xrTableCell21.Weight = 0.47244139599144586D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Weight = 0.17836250193311465D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 0.18110235939707198D;
            // 
            // xrTable_运动频率_2_1
            // 
            this.xrTable_运动频率_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_2_1.CanGrow = false;
            this.xrTable_运动频率_2_1.Dpi = 254F;
            this.xrTable_运动频率_2_1.Name = "xrTable_运动频率_2_1";
            this.xrTable_运动频率_2_1.StylePriority.UseBorders = false;
            this.xrTable_运动频率_2_1.Weight = 0.25590550479050672D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.Text = "次/周";
            this.xrTableCell163.Weight = 0.32283464227128356D;
            // 
            // xrTable_运动时长_2_1
            // 
            this.xrTable_运动时长_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_2_1.CanGrow = false;
            this.xrTable_运动时长_2_1.Dpi = 254F;
            this.xrTable_运动时长_2_1.Name = "xrTable_运动时长_2_1";
            this.xrTable_运动时长_2_1.StylePriority.UseBorders = false;
            this.xrTable_运动时长_2_1.Weight = 0.25590551218242097D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "分钟/次";
            this.xrTableCell170.Weight = 0.4724409256662388D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Weight = 0.17836298639682413D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.18110234625589122D;
            // 
            // xrTable_运动频率_3_1
            // 
            this.xrTable_运动频率_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_3_1.CanGrow = false;
            this.xrTable_运动频率_3_1.Dpi = 254F;
            this.xrTable_运动频率_3_1.Name = "xrTable_运动频率_3_1";
            this.xrTable_运动频率_3_1.StylePriority.UseBorders = false;
            this.xrTable_运动频率_3_1.Weight = 0.25590549164932591D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Text = "次/周";
            this.xrTableCell172.Weight = 0.32283461598892205D;
            // 
            // xrTable_运动时长_3_1
            // 
            this.xrTable_运动时长_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_3_1.CanGrow = false;
            this.xrTable_运动时长_3_1.Dpi = 254F;
            this.xrTable_运动时长_3_1.Name = "xrTable_运动时长_3_1";
            this.xrTable_运动时长_3_1.StylePriority.UseBorders = false;
            this.xrTable_运动时长_3_1.Weight = 0.25590548590005935D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Text = "分钟/次";
            this.xrTableCell175.Weight = 0.47244092754355044D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Weight = 0.17836283808921241D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.18110234625589122D;
            // 
            // xrTable_运动频率_4_1
            // 
            this.xrTable_运动频率_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_4_1.CanGrow = false;
            this.xrTable_运动频率_4_1.Dpi = 254F;
            this.xrTable_运动频率_4_1.Name = "xrTable_运动频率_4_1";
            this.xrTable_运动频率_4_1.StylePriority.UseBorders = false;
            this.xrTable_运动频率_4_1.Weight = 0.25590549164932591D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Text = "次/周";
            this.xrTableCell177.Weight = 0.32283461598892205D;
            // 
            // xrTable_运动时长_4_1
            // 
            this.xrTable_运动时长_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_4_1.CanGrow = false;
            this.xrTable_运动时长_4_1.Dpi = 254F;
            this.xrTable_运动时长_4_1.Name = "xrTable_运动时长_4_1";
            this.xrTable_运动时长_4_1.StylePriority.UseBorders = false;
            this.xrTable_运动时长_4_1.Weight = 0.25590548590005935D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Text = "分钟/次";
            this.xrTableCell180.Weight = 0.47244092754355044D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.Weight = 0.17836283808921241D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell45,
            this.xrTableCell160,
            this.xrTable_运动频率_1_2,
            this.xrTableCell33,
            this.xrTable_运动时长_1_2,
            this.xrTableCell36,
            this.xrTableCell47,
            this.xrTableCell186,
            this.xrTable_运动频率_2_2,
            this.xrTableCell182,
            this.xrTable_运动时长_2_2,
            this.xrTableCell185,
            this.xrTableCell49,
            this.xrTableCell191,
            this.xrTable_运动频率_3_2,
            this.xrTableCell197,
            this.xrTable_运动时长_3_2,
            this.xrTableCell188,
            this.xrTableCell53,
            this.xrTableCell196,
            this.xrTable_运动频率_4_2,
            this.xrTableCell192,
            this.xrTable_运动时长_4_2,
            this.xrTableCell195,
            this.xrTableCell55});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Text = "试";
            this.xrTableCell42.Weight = 0.31666672042035415D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Text = "动";
            this.xrTableCell45.Weight = 1.0250000226216041D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 0.18110198581207537D;
            // 
            // xrTable_运动频率_1_2
            // 
            this.xrTable_运动频率_1_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_1_2.CanGrow = false;
            this.xrTable_运动频率_1_2.Dpi = 254F;
            this.xrTable_运动频率_1_2.Name = "xrTable_运动频率_1_2";
            this.xrTable_运动频率_1_2.StylePriority.UseBorders = false;
            this.xrTable_运动频率_1_2.Weight = 0.25590537150138737D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Text = "次/周";
            this.xrTableCell33.Weight = 0.32283485628479924D;
            // 
            // xrTable_运动时长_1_2
            // 
            this.xrTable_运动时长_1_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_1_2.CanGrow = false;
            this.xrTable_运动时长_1_2.Dpi = 254F;
            this.xrTable_运动时长_1_2.Name = "xrTable_运动时长_1_2";
            this.xrTable_运动时长_1_2.StylePriority.UseBorders = false;
            this.xrTable_运动时长_1_2.Weight = 0.25590567187123386D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "分钟/次";
            this.xrTableCell36.Weight = 0.47244115282093535D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.17836266713653023D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.18110183562715213D;
            // 
            // xrTable_运动频率_2_2
            // 
            this.xrTable_运动频率_2_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_2_2.CanGrow = false;
            this.xrTable_运动频率_2_2.Dpi = 254F;
            this.xrTable_运动频率_2_2.Name = "xrTable_运动频率_2_2";
            this.xrTable_运动频率_2_2.StylePriority.UseBorders = false;
            this.xrTable_运动频率_2_2.Weight = 0.25590549164932597D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Text = "次/周";
            this.xrTableCell182.Weight = 0.32283461598892205D;
            // 
            // xrTable_运动时长_2_2
            // 
            this.xrTable_运动时长_2_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_2_2.CanGrow = false;
            this.xrTable_运动时长_2_2.Dpi = 254F;
            this.xrTable_运动时长_2_2.Name = "xrTable_运动时长_2_2";
            this.xrTable_运动时长_2_2.StylePriority.UseBorders = false;
            this.xrTable_运动时长_2_2.Weight = 0.25590524560418215D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Text = "分钟/次";
            this.xrTableCell185.Weight = 0.47107148133930371D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.17973303521807543D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.Weight = 0.18110183562715213D;
            // 
            // xrTable_运动频率_3_2
            // 
            this.xrTable_运动频率_3_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_3_2.CanGrow = false;
            this.xrTable_运动频率_3_2.Dpi = 254F;
            this.xrTable_运动频率_3_2.Name = "xrTable_运动频率_3_2";
            this.xrTable_运动频率_3_2.StylePriority.UseBorders = false;
            this.xrTable_运动频率_3_2.Weight = 0.25590597224108036D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.Text = "次/周";
            this.xrTableCell197.Weight = 0.32283414114643422D;
            // 
            // xrTable_运动时长_3_2
            // 
            this.xrTable_运动时长_3_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_3_2.CanGrow = false;
            this.xrTable_运动时长_3_2.Dpi = 254F;
            this.xrTable_运动时长_3_2.Name = "xrTable_运动时长_3_2";
            this.xrTable_运动时长_3_2.StylePriority.UseBorders = false;
            this.xrTable_运动时长_3_2.Weight = 0.25590588213012649D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Text = "分钟/次";
            this.xrTableCell188.Weight = 0.47244090677579148D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.Weight = 0.17836296750637676D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.1811018356271521D;
            // 
            // xrTable_运动频率_4_2
            // 
            this.xrTable_运动频率_4_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动频率_4_2.CanGrow = false;
            this.xrTable_运动频率_4_2.Dpi = 254F;
            this.xrTable_运动频率_4_2.Name = "xrTable_运动频率_4_2";
            this.xrTable_运动频率_4_2.StylePriority.UseBorders = false;
            this.xrTable_运动频率_4_2.Weight = 0.25590549164932591D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Text = "次/周";
            this.xrTableCell192.Weight = 0.32283455591495275D;
            // 
            // xrTable_运动时长_4_2
            // 
            this.xrTable_运动时长_4_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_运动时长_4_2.CanGrow = false;
            this.xrTable_运动时长_4_2.Dpi = 254F;
            this.xrTable_运动时长_4_2.Name = "xrTable_运动时长_4_2";
            this.xrTable_运动时长_4_2.StylePriority.UseBorders = false;
            this.xrTable_运动时长_4_2.Weight = 0.25590571117744421D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell195.Dpi = 254F;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.Text = "分钟/次";
            this.xrTableCell195.Weight = 0.47244139311681255D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.Weight = 0.17836271794127381D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTable_主食_1_1,
            this.xrTable_摄盐情况_1_1,
            this.xrTable_主食_1_2,
            this.xrTable_主食_2_1,
            this.xrTable_摄盐情况_2_1,
            this.xrTable_主食_2_2,
            this.xrTable_主食_3_1,
            this.xrTable_摄盐情况_3_1,
            this.xrTable_主食_3_2,
            this.xrTable_主食_4_1,
            this.xrTable_摄盐情况_4_1,
            this.xrTable_主食_4_2});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.Text = "指";
            this.xrTableCell79.Weight = 0.31666672042035415D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "主食（克/天）";
            this.xrTableCell80.Weight = 1.0250000226216041D;
            // 
            // xrTable_主食_1_1
            // 
            this.xrTable_主食_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_主食_1_1.CanGrow = false;
            this.xrTable_主食_1_1.Dpi = 254F;
            this.xrTable_主食_1_1.Name = "xrTable_主食_1_1";
            this.xrTable_主食_1_1.StylePriority.UseBorders = false;
            this.xrTable_主食_1_1.Weight = 0.79173177081402257D;
            // 
            // xrTable_摄盐情况_1_1
            // 
            this.xrTable_摄盐情况_1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_摄盐情况_1_1.Dpi = 254F;
            this.xrTable_摄盐情况_1_1.Name = "xrTable_摄盐情况_1_1";
            this.xrTable_摄盐情况_1_1.StylePriority.UseBorders = false;
            this.xrTable_摄盐情况_1_1.Text = "/";
            this.xrTable_摄盐情况_1_1.Weight = 0.083071234438105768D;
            // 
            // xrTable_主食_1_2
            // 
            this.xrTable_主食_1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_主食_1_2.CanGrow = false;
            this.xrTable_主食_1_2.Dpi = 254F;
            this.xrTable_主食_1_2.Name = "xrTable_主食_1_2";
            this.xrTable_主食_1_2.StylePriority.UseBorders = false;
            this.xrTable_主食_1_2.Weight = 0.791748700174833D;
            // 
            // xrTable_主食_2_1
            // 
            this.xrTable_主食_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_主食_2_1.CanGrow = false;
            this.xrTable_主食_2_1.Dpi = 254F;
            this.xrTable_主食_2_1.Name = "xrTable_主食_2_1";
            this.xrTable_主食_2_1.StylePriority.UseBorders = false;
            this.xrTable_主食_2_1.Weight = 0.79173225140577708D;
            // 
            // xrTable_摄盐情况_2_1
            // 
            this.xrTable_摄盐情况_2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_摄盐情况_2_1.Dpi = 254F;
            this.xrTable_摄盐情况_2_1.Name = "xrTable_摄盐情况_2_1";
            this.xrTable_摄盐情况_2_1.StylePriority.UseBorders = false;
            this.xrTable_摄盐情况_2_1.Text = "/";
            this.xrTable_摄盐情况_2_1.Weight = 0.083070873994289945D;
            // 
            // xrTable_主食_2_2
            // 
            this.xrTable_主食_2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_主食_2_2.CanGrow = false;
            this.xrTable_主食_2_2.Dpi = 254F;
            this.xrTable_主食_2_2.Name = "xrTable_主食_2_2";
            this.xrTable_主食_2_2.StylePriority.UseBorders = false;
            this.xrTable_主食_2_2.Weight = 0.79174858002689441D;
            // 
            // xrTable_主食_3_1
            // 
            this.xrTable_主食_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_主食_3_1.CanGrow = false;
            this.xrTable_主食_3_1.Dpi = 254F;
            this.xrTable_主食_3_1.Name = "xrTable_主食_3_1";
            this.xrTable_主食_3_1.StylePriority.UseBorders = false;
            this.xrTable_主食_3_1.Weight = 0.79173165066608409D;
            // 
            // xrTable_摄盐情况_3_1
            // 
            this.xrTable_摄盐情况_3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_摄盐情况_3_1.Dpi = 254F;
            this.xrTable_摄盐情况_3_1.Name = "xrTable_摄盐情况_3_1";
            this.xrTable_摄盐情况_3_1.StylePriority.UseBorders = false;
            this.xrTable_摄盐情况_3_1.Text = "/";
            this.xrTable_摄盐情况_3_1.Weight = 0.083071234438105712D;
            // 
            // xrTable_主食_3_2
            // 
            this.xrTable_主食_3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_主食_3_2.CanGrow = false;
            this.xrTable_主食_3_2.Dpi = 254F;
            this.xrTable_主食_3_2.Name = "xrTable_主食_3_2";
            this.xrTable_主食_3_2.StylePriority.UseBorders = false;
            this.xrTable_主食_3_2.Weight = 0.79174882032277161D;
            // 
            // xrTable_主食_4_1
            // 
            this.xrTable_主食_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_主食_4_1.CanGrow = false;
            this.xrTable_主食_4_1.Dpi = 254F;
            this.xrTable_主食_4_1.Name = "xrTable_主食_4_1";
            this.xrTable_主食_4_1.StylePriority.UseBorders = false;
            this.xrTable_主食_4_1.Weight = 0.79173224014190768D;
            // 
            // xrTable_摄盐情况_4_1
            // 
            this.xrTable_摄盐情况_4_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_摄盐情况_4_1.Dpi = 254F;
            this.xrTable_摄盐情况_4_1.Name = "xrTable_摄盐情况_4_1";
            this.xrTable_摄盐情况_4_1.StylePriority.UseBorders = false;
            this.xrTable_摄盐情况_4_1.Text = "/";
            this.xrTable_摄盐情况_4_1.Weight = 0.083069143113049387D;
            // 
            // xrTable_主食_4_2
            // 
            this.xrTable_主食_4_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_主食_4_2.CanGrow = false;
            this.xrTable_主食_4_2.Dpi = 254F;
            this.xrTable_主食_4_2.Name = "xrTable_主食_4_2";
            this.xrTable_主食_4_2.StylePriority.UseBorders = false;
            this.xrTable_主食_4_2.Weight = 0.79175032217200425D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell27,
            this.xrTableCell87,
            this.xrTableCell82,
            this.xrTableCell88,
            this.xrTableCell84,
            this.xrTableCell89,
            this.xrTableCell173,
            this.xrTableCell90});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Text = "导";
            this.xrTableCell85.Weight = 0.31666672042035415D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = "心理调整";
            this.xrTableCell86.Weight = 1.0250000226216041D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Text = "1 良好  2 一般  3 差";
            this.xrTableCell27.Weight = 1.3877949341378852D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心理调整_1});
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.27875677128907617D;
            // 
            // xrLabel_心理调整_1
            // 
            this.xrLabel_心理调整_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心理调整_1.CanGrow = false;
            this.xrLabel_心理调整_1.Dpi = 254F;
            this.xrLabel_心理调整_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_心理调整_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_心理调整_1.Name = "xrLabel_心理调整_1";
            this.xrLabel_心理调整_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心理调整_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_心理调整_1.StylePriority.UseBorders = false;
            this.xrLabel_心理调整_1.StylePriority.UseFont = false;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.Text = "1 良好  2 一般  3 差";
            this.xrTableCell82.Weight = 1.3877978176884118D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心理调整_2});
            this.xrTableCell88.Dpi = 254F;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.27875388773854959D;
            // 
            // xrLabel_心理调整_2
            // 
            this.xrLabel_心理调整_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心理调整_2.CanGrow = false;
            this.xrLabel_心理调整_2.Dpi = 254F;
            this.xrLabel_心理调整_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_心理调整_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_心理调整_2.Name = "xrLabel_心理调整_2";
            this.xrLabel_心理调整_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心理调整_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_心理调整_2.StylePriority.UseBorders = false;
            this.xrLabel_心理调整_2.StylePriority.UseFont = false;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "1 良好  2 一般  3 差";
            this.xrTableCell84.Weight = 1.38780271371691D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心理调整_3});
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.27874899171005141D;
            // 
            // xrLabel_心理调整_3
            // 
            this.xrLabel_心理调整_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心理调整_3.CanGrow = false;
            this.xrLabel_心理调整_3.Dpi = 254F;
            this.xrLabel_心理调整_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_心理调整_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_心理调整_3.Name = "xrLabel_心理调整_3";
            this.xrLabel_心理调整_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心理调整_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_心理调整_3.StylePriority.UseBorders = false;
            this.xrLabel_心理调整_3.StylePriority.UseFont = false;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.Text = "1 良好  2 一般  3 差";
            this.xrTableCell173.Weight = 1.3877850219329502D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心理调整_4});
            this.xrTableCell90.Dpi = 254F;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Weight = 0.27876668349401124D;
            // 
            // xrLabel_心理调整_4
            // 
            this.xrLabel_心理调整_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心理调整_4.CanGrow = false;
            this.xrLabel_心理调整_4.Dpi = 254F;
            this.xrLabel_心理调整_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_心理调整_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_心理调整_4.Name = "xrLabel_心理调整_4";
            this.xrLabel_心理调整_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心理调整_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_心理调整_4.StylePriority.UseBorders = false;
            this.xrLabel_心理调整_4.StylePriority.UseFont = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell81,
            this.xrTableCell93,
            this.xrTableCell83,
            this.xrTableCell94,
            this.xrTableCell159,
            this.xrTableCell95,
            this.xrTableCell179,
            this.xrTableCell96});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.31666672042035415D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Text = "遵医行为";
            this.xrTableCell92.Weight = 1.0250000226216041D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Text = "1 良好  2 一般  3 差";
            this.xrTableCell81.Weight = 1.3877949341378852D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_遵医行为_1});
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.27875677128907617D;
            // 
            // xrLabel_遵医行为_1
            // 
            this.xrLabel_遵医行为_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_遵医行为_1.CanGrow = false;
            this.xrLabel_遵医行为_1.Dpi = 254F;
            this.xrLabel_遵医行为_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_遵医行为_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_遵医行为_1.Name = "xrLabel_遵医行为_1";
            this.xrLabel_遵医行为_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_遵医行为_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_遵医行为_1.StylePriority.UseBorders = false;
            this.xrLabel_遵医行为_1.StylePriority.UseFont = false;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Text = "1 良好  2 一般  3 差";
            this.xrTableCell83.Weight = 1.3877978176884118D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_遵医行为_2});
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Weight = 0.27875388773854959D;
            // 
            // xrLabel_遵医行为_2
            // 
            this.xrLabel_遵医行为_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_遵医行为_2.CanGrow = false;
            this.xrLabel_遵医行为_2.Dpi = 254F;
            this.xrLabel_遵医行为_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_遵医行为_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_遵医行为_2.Name = "xrLabel_遵医行为_2";
            this.xrLabel_遵医行为_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_遵医行为_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_遵医行为_2.StylePriority.UseBorders = false;
            this.xrLabel_遵医行为_2.StylePriority.UseFont = false;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.Text = "1 良好  2 一般  3 差";
            this.xrTableCell159.Weight = 1.3878017825703859D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_遵医行为_3});
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.27874992285657552D;
            // 
            // xrLabel_遵医行为_3
            // 
            this.xrLabel_遵医行为_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_遵医行为_3.CanGrow = false;
            this.xrLabel_遵医行为_3.Dpi = 254F;
            this.xrLabel_遵医行为_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_遵医行为_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_遵医行为_3.Name = "xrLabel_遵医行为_3";
            this.xrLabel_遵医行为_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_遵医行为_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_遵医行为_3.StylePriority.UseBorders = false;
            this.xrLabel_遵医行为_3.StylePriority.UseFont = false;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.Text = "1 良好  2 一般  3 差";
            this.xrTableCell179.Weight = 1.3877850219329502D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_遵医行为_4});
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.27876668349401124D;
            // 
            // xrLabel_遵医行为_4
            // 
            this.xrLabel_遵医行为_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_遵医行为_4.CanGrow = false;
            this.xrLabel_遵医行为_4.Dpi = 254F;
            this.xrLabel_遵医行为_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_遵医行为_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_遵医行为_4.Name = "xrLabel_遵医行为_4";
            this.xrLabel_遵医行为_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_遵医行为_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_遵医行为_4.StylePriority.UseBorders = false;
            this.xrLabel_遵医行为_4.StylePriority.UseFont = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell164,
            this.xrTable_空腹血糖_1,
            this.xrTableCell168,
            this.xrTable_空腹血糖_2,
            this.xrTableCell174,
            this.xrTable_空腹血糖_3,
            this.xrTableCell178,
            this.xrTable_空腹血糖_4,
            this.xrTableCell183});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Text = "辅";
            this.xrTableCell41.Weight = 0.31666680489937321D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Text = "空腹血糖值";
            this.xrTableCell164.Weight = 1.0249999381425849D;
            // 
            // xrTable_空腹血糖_1
            // 
            this.xrTable_空腹血糖_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_空腹血糖_1.CanGrow = false;
            this.xrTable_空腹血糖_1.Dpi = 254F;
            this.xrTable_空腹血糖_1.Name = "xrTable_空腹血糖_1";
            this.xrTable_空腹血糖_1.StylePriority.UseBorders = false;
            this.xrTable_空腹血糖_1.Weight = 1.0236219665973039D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.StylePriority.UseTextAlignment = false;
            this.xrTableCell168.Text = "mmol/L";
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell168.Weight = 0.64292973882965754D;
            // 
            // xrTable_空腹血糖_2
            // 
            this.xrTable_空腹血糖_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_空腹血糖_2.CanGrow = false;
            this.xrTable_空腹血糖_2.Dpi = 254F;
            this.xrTable_空腹血糖_2.Name = "xrTable_空腹血糖_2";
            this.xrTable_空腹血糖_2.StylePriority.UseBorders = false;
            this.xrTable_空腹血糖_2.Weight = 1.0236219665973039D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.Text = "mmol/L";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell174.Weight = 0.64292973882965754D;
            // 
            // xrTable_空腹血糖_3
            // 
            this.xrTable_空腹血糖_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_空腹血糖_3.CanGrow = false;
            this.xrTable_空腹血糖_3.Dpi = 254F;
            this.xrTable_空腹血糖_3.Name = "xrTable_空腹血糖_3";
            this.xrTable_空腹血糖_3.StylePriority.UseBorders = false;
            this.xrTable_空腹血糖_3.Weight = 1.0236219665973039D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.Text = "mmol/L";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell178.Weight = 0.64292973882965754D;
            // 
            // xrTable_空腹血糖_4
            // 
            this.xrTable_空腹血糖_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_空腹血糖_4.CanGrow = false;
            this.xrTable_空腹血糖_4.Dpi = 254F;
            this.xrTable_空腹血糖_4.Name = "xrTable_空腹血糖_4";
            this.xrTable_空腹血糖_4.StylePriority.UseBorders = false;
            this.xrTable_空腹血糖_4.Weight = 1.0236219665973039D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.StylePriority.UseTextAlignment = false;
            this.xrTableCell183.Text = "mmol/L";
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell183.Weight = 0.64292973882965754D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell98,
            this.xrTableCell375,
            this.xrTable_糖化血红蛋白_1,
            this.xrTableCell367,
            this.xrTableCell377,
            this.xrTable_糖化血红蛋白_2,
            this.xrTableCell368,
            this.xrTableCell379,
            this.xrTable_糖化血红蛋白_3,
            this.xrTableCell369,
            this.xrTableCell381,
            this.xrTable_糖化血红蛋白_4,
            this.xrTableCell370});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Text = "助";
            this.xrTableCell30.Weight = 0.31666680489937321D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 1.0249999381425849D;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Dpi = 254F;
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseTextAlignment = false;
            this.xrTableCell375.Text = "   糖化血红蛋白";
            this.xrTableCell375.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell375.Weight = 0.75984188319143076D;
            // 
            // xrTable_糖化血红蛋白_1
            // 
            this.xrTable_糖化血红蛋白_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_糖化血红蛋白_1.CanGrow = false;
            this.xrTable_糖化血红蛋白_1.Dpi = 254F;
            this.xrTable_糖化血红蛋白_1.ForeColor = System.Drawing.Color.Black;
            this.xrTable_糖化血红蛋白_1.Name = "xrTable_糖化血红蛋白_1";
            this.xrTable_糖化血红蛋白_1.StylePriority.UseBorders = false;
            this.xrTable_糖化血红蛋白_1.StylePriority.UseForeColor = false;
            this.xrTable_糖化血红蛋白_1.Weight = 0.4166535155517746D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell367.Dpi = 254F;
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.StylePriority.UseTextAlignment = false;
            this.xrTableCell367.Text = "%";
            this.xrTableCell367.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell367.Weight = 0.490056306683756D;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Dpi = 254F;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.StylePriority.UseTextAlignment = false;
            this.xrTableCell377.Text = "   糖化血红蛋白";
            this.xrTableCell377.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell377.Weight = 0.75984245389413918D;
            // 
            // xrTable_糖化血红蛋白_2
            // 
            this.xrTable_糖化血红蛋白_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_糖化血红蛋白_2.CanGrow = false;
            this.xrTable_糖化血红蛋白_2.Dpi = 254F;
            this.xrTable_糖化血红蛋白_2.Name = "xrTable_糖化血红蛋白_2";
            this.xrTable_糖化血红蛋白_2.StylePriority.UseBorders = false;
            this.xrTable_糖化血红蛋白_2.Weight = 0.49007132517608187D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell368.Dpi = 254F;
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBorders = false;
            this.xrTableCell368.StylePriority.UseTextAlignment = false;
            this.xrTableCell368.Text = "%";
            this.xrTableCell368.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell368.Weight = 0.41663792635674035D;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Dpi = 254F;
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.StylePriority.UseTextAlignment = false;
            this.xrTableCell379.Text = "   糖化血红蛋白";
            this.xrTableCell379.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell379.Weight = 0.75984245389413918D;
            // 
            // xrTable_糖化血红蛋白_3
            // 
            this.xrTable_糖化血红蛋白_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_糖化血红蛋白_3.CanGrow = false;
            this.xrTable_糖化血红蛋白_3.Dpi = 254F;
            this.xrTable_糖化血红蛋白_3.Name = "xrTable_糖化血红蛋白_3";
            this.xrTable_糖化血红蛋白_3.StylePriority.UseBorders = false;
            this.xrTable_糖化血红蛋白_3.Weight = 0.49007132517608187D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell369.Dpi = 254F;
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.StylePriority.UseTextAlignment = false;
            this.xrTableCell369.Text = "%";
            this.xrTableCell369.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell369.Weight = 0.41663792635674035D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Dpi = 254F;
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseTextAlignment = false;
            this.xrTableCell381.Text = "   糖化血红蛋白";
            this.xrTableCell381.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell381.Weight = 0.75984245389413918D;
            // 
            // xrTable_糖化血红蛋白_4
            // 
            this.xrTable_糖化血红蛋白_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_糖化血红蛋白_4.CanGrow = false;
            this.xrTable_糖化血红蛋白_4.Dpi = 254F;
            this.xrTable_糖化血红蛋白_4.Name = "xrTable_糖化血红蛋白_4";
            this.xrTable_糖化血红蛋白_4.StylePriority.UseBorders = false;
            this.xrTable_糖化血红蛋白_4.Weight = 0.49007132517608187D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell370.Dpi = 254F;
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBorders = false;
            this.xrTableCell370.StylePriority.UseTextAlignment = false;
            this.xrTableCell370.Text = "%";
            this.xrTableCell370.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell370.Weight = 0.41663792635674035D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell356,
            this.xrTableCell383,
            this.xrTable_检查日期月_1,
            this.xrTableCell384,
            this.xrTable_检查日期日_1,
            this.xrTableCell357,
            this.xrTableCell387,
            this.xrTable_检查日期月_2,
            this.xrTableCell388,
            this.xrTable_检查日期日_2,
            this.xrTableCell358,
            this.xrTableCell391,
            this.xrTable_检查日期月_3,
            this.xrTableCell392,
            this.xrTable_检查日期日_3,
            this.xrTableCell359,
            this.xrTableCell395,
            this.xrTable_检查日期月_4,
            this.xrTableCell396,
            this.xrTable_检查日期日_4,
            this.xrTableCell360});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Text = "检";
            this.xrTableCell184.Weight = 0.31666680489937321D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell356.Dpi = 254F;
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseBorders = false;
            this.xrTableCell356.Text = "其他检查*";
            this.xrTableCell356.Weight = 1.0249999381425849D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Dpi = 254F;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseTextAlignment = false;
            this.xrTableCell383.Text = "   检查日期：";
            this.xrTableCell383.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell383.Weight = 0.75984173300650748D;
            // 
            // xrTable_检查日期月_1
            // 
            this.xrTable_检查日期月_1.CanGrow = false;
            this.xrTable_检查日期月_1.Dpi = 254F;
            this.xrTable_检查日期月_1.Name = "xrTable_检查日期月_1";
            this.xrTable_检查日期月_1.Weight = 0.19685039188352771D;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Dpi = 254F;
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseTextAlignment = false;
            this.xrTableCell384.Text = "月";
            this.xrTableCell384.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell384.Weight = 0.19685039188352768D;
            // 
            // xrTable_检查日期日_1
            // 
            this.xrTable_检查日期日_1.CanGrow = false;
            this.xrTable_检查日期日_1.Dpi = 254F;
            this.xrTable_检查日期日_1.Name = "xrTable_检查日期日_1";
            this.xrTable_检查日期日_1.Weight = 0.19685039188352771D;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell357.Dpi = 254F;
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBorders = false;
            this.xrTableCell357.StylePriority.UseTextAlignment = false;
            this.xrTableCell357.Text = "  日";
            this.xrTableCell357.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell357.Weight = 0.3161587967698708D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Dpi = 254F;
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.Text = "   检查日期：";
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell387.Weight = 0.75984245389413918D;
            // 
            // xrTable_检查日期月_2
            // 
            this.xrTable_检查日期月_2.CanGrow = false;
            this.xrTable_检查日期月_2.Dpi = 254F;
            this.xrTable_检查日期月_2.Name = "xrTable_检查日期月_2";
            this.xrTable_检查日期月_2.Weight = 0.19685039188352771D;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Dpi = 254F;
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.Text = "月";
            this.xrTableCell388.Weight = 0.19685039188352768D;
            // 
            // xrTable_检查日期日_2
            // 
            this.xrTable_检查日期日_2.CanGrow = false;
            this.xrTable_检查日期日_2.Dpi = 254F;
            this.xrTable_检查日期日_2.Name = "xrTable_检查日期日_2";
            this.xrTable_检查日期日_2.Weight = 0.19685039188352765D;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell358.Dpi = 254F;
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.StylePriority.UseTextAlignment = false;
            this.xrTableCell358.Text = "  日";
            this.xrTableCell358.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell358.Weight = 0.31615807588223915D;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.Dpi = 254F;
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.StylePriority.UseTextAlignment = false;
            this.xrTableCell391.Text = "   检查日期：";
            this.xrTableCell391.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell391.Weight = 0.75984245389413918D;
            // 
            // xrTable_检查日期月_3
            // 
            this.xrTable_检查日期月_3.CanGrow = false;
            this.xrTable_检查日期月_3.Dpi = 254F;
            this.xrTable_检查日期月_3.Name = "xrTable_检查日期月_3";
            this.xrTable_检查日期月_3.Weight = 0.19685039188352771D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Dpi = 254F;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.Text = "月";
            this.xrTableCell392.Weight = 0.19685039188352768D;
            // 
            // xrTable_检查日期日_3
            // 
            this.xrTable_检查日期日_3.CanGrow = false;
            this.xrTable_检查日期日_3.Dpi = 254F;
            this.xrTable_检查日期日_3.Name = "xrTable_检查日期日_3";
            this.xrTable_检查日期日_3.Weight = 0.19685039188352765D;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell359.Dpi = 254F;
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseBorders = false;
            this.xrTableCell359.StylePriority.UseTextAlignment = false;
            this.xrTableCell359.Text = "  日";
            this.xrTableCell359.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell359.Weight = 0.31615807588223915D;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.Dpi = 254F;
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.StylePriority.UseTextAlignment = false;
            this.xrTableCell395.Text = "   检查日期：";
            this.xrTableCell395.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell395.Weight = 0.75984245389413918D;
            // 
            // xrTable_检查日期月_4
            // 
            this.xrTable_检查日期月_4.CanGrow = false;
            this.xrTable_检查日期月_4.Dpi = 254F;
            this.xrTable_检查日期月_4.Name = "xrTable_检查日期月_4";
            this.xrTable_检查日期月_4.Weight = 0.19685039188352771D;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Dpi = 254F;
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Text = "月";
            this.xrTableCell396.Weight = 0.19685039188352768D;
            // 
            // xrTable_检查日期日_4
            // 
            this.xrTable_检查日期日_4.CanGrow = false;
            this.xrTable_检查日期日_4.Dpi = 254F;
            this.xrTable_检查日期日_4.Name = "xrTable_检查日期日_4";
            this.xrTable_检查日期日_4.Weight = 0.19685039188352765D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell360.Dpi = 254F;
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.StylePriority.UseTextAlignment = false;
            this.xrTableCell360.Text = "  日";
            this.xrTableCell360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell360.Weight = 0.31615807588223915D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell361,
            this.xrTableCell362,
            this.xrTable_其他检查其他_1,
            this.xrTable_其他检查其他_2,
            this.xrTable_其他检查其他_3,
            this.xrTable_其他检查其他_4});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell361.Dpi = 254F;
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.StylePriority.UseBorders = false;
            this.xrTableCell361.Text = "查";
            this.xrTableCell361.Weight = 0.31666680489937321D;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell362.Dpi = 254F;
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseBorders = false;
            this.xrTableCell362.Weight = 1.0249999381425849D;
            // 
            // xrTable_其他检查其他_1
            // 
            this.xrTable_其他检查其他_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_其他检查其他_1.CanGrow = false;
            this.xrTable_其他检查其他_1.Dpi = 254F;
            this.xrTable_其他检查其他_1.Name = "xrTable_其他检查其他_1";
            this.xrTable_其他检查其他_1.StylePriority.UseBorders = false;
            this.xrTable_其他检查其他_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_其他检查其他_2
            // 
            this.xrTable_其他检查其他_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_其他检查其他_2.CanGrow = false;
            this.xrTable_其他检查其他_2.Dpi = 254F;
            this.xrTable_其他检查其他_2.Name = "xrTable_其他检查其他_2";
            this.xrTable_其他检查其他_2.StylePriority.UseBorders = false;
            this.xrTable_其他检查其他_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_其他检查其他_3
            // 
            this.xrTable_其他检查其他_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_其他检查其他_3.CanGrow = false;
            this.xrTable_其他检查其他_3.Dpi = 254F;
            this.xrTable_其他检查其他_3.Name = "xrTable_其他检查其他_3";
            this.xrTable_其他检查其他_3.StylePriority.UseBorders = false;
            this.xrTable_其他检查其他_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_其他检查其他_4
            // 
            this.xrTable_其他检查其他_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_其他检查其他_4.CanGrow = false;
            this.xrTable_其他检查其他_4.Dpi = 254F;
            this.xrTable_其他检查其他_4.Name = "xrTable_其他检查其他_4";
            this.xrTable_其他检查其他_4.StylePriority.UseBorders = false;
            this.xrTable_其他检查其他_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTable_药物名称目前1_1,
            this.xrTable_药物名称目前1_2,
            this.xrTable_药物名称目前1_3,
            this.xrTable_药物名称目前1_4});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.31666672042035415D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Text = "药物名称 1";
            this.xrTableCell104.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称目前1_1
            // 
            this.xrTable_药物名称目前1_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前1_1.CanGrow = false;
            this.xrTable_药物名称目前1_1.Dpi = 254F;
            this.xrTable_药物名称目前1_1.Name = "xrTable_药物名称目前1_1";
            this.xrTable_药物名称目前1_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前1_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前1_2
            // 
            this.xrTable_药物名称目前1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前1_2.CanGrow = false;
            this.xrTable_药物名称目前1_2.Dpi = 254F;
            this.xrTable_药物名称目前1_2.Name = "xrTable_药物名称目前1_2";
            this.xrTable_药物名称目前1_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前1_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前1_3
            // 
            this.xrTable_药物名称目前1_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前1_3.CanGrow = false;
            this.xrTable_药物名称目前1_3.Dpi = 254F;
            this.xrTable_药物名称目前1_3.Name = "xrTable_药物名称目前1_3";
            this.xrTable_药物名称目前1_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前1_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前1_4
            // 
            this.xrTable_药物名称目前1_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前1_4.CanGrow = false;
            this.xrTable_药物名称目前1_4.Dpi = 254F;
            this.xrTable_药物名称目前1_4.Name = "xrTable_药物名称目前1_4";
            this.xrTable_药物名称目前1_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前1_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell280,
            this.xrTable_用法用量每日目前1_1,
            this.xrTableCell279,
            this.xrTableCell281,
            this.xrTable_用法用量每次目前1_1,
            this.xrTableCell284,
            this.xrTable_用法用量每日目前1_2,
            this.xrTableCell283,
            this.xrTableCell285,
            this.xrTable_用法用量每次目前1_2,
            this.xrTableCell288,
            this.xrTable_用法用量每日目前1_3,
            this.xrTableCell287,
            this.xrTableCell289,
            this.xrTable_用法用量每次目前1_3,
            this.xrTableCell291,
            this.xrTable_用法用量每日目前1_4,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTable_用法用量每次目前1_4});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell109.Dpi = 254F;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Text = "目";
            this.xrTableCell109.Weight = 0.31666672042035415D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Dpi = 254F;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Text = "用法用量";
            this.xrTableCell110.Weight = 1.0250000226216041D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell280.Dpi = 254F;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.StylePriority.UseTextAlignment = false;
            this.xrTableCell280.Text = "每日";
            this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell280.Weight = 0.34415967361234273D;
            // 
            // xrTable_用法用量每日目前1_1
            // 
            this.xrTable_用法用量每日目前1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前1_1.CanGrow = false;
            this.xrTable_用法用量每日目前1_1.Dpi = 254F;
            this.xrTable_用法用量每日目前1_1.Name = "xrTable_用法用量每日目前1_1";
            this.xrTable_用法用量每日目前1_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前1_1.Weight = 0.2463912259561119D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell279.Dpi = 254F;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.StylePriority.UseTextAlignment = false;
            this.xrTableCell279.Text = "次";
            this.xrTableCell279.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell279.Weight = 0.20118087828548614D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell281.Dpi = 254F;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseTextAlignment = false;
            this.xrTableCell281.Text = "每次";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell281.Weight = 0.28700844383098334D;
            // 
            // xrTable_用法用量每次目前1_1
            // 
            this.xrTable_用法用量每次目前1_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前1_1.CanGrow = false;
            this.xrTable_用法用量每次目前1_1.Dpi = 254F;
            this.xrTable_用法用量每次目前1_1.Name = "xrTable_用法用量每次目前1_1";
            this.xrTable_用法用量每次目前1_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前1_1.Weight = 0.58781148374203729D;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell284.Dpi = 254F;
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseBorders = false;
            this.xrTableCell284.StylePriority.UseTextAlignment = false;
            this.xrTableCell284.Text = "每日";
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell284.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前1_2
            // 
            this.xrTable_用法用量每日目前1_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前1_2.CanGrow = false;
            this.xrTable_用法用量每日目前1_2.Dpi = 254F;
            this.xrTable_用法用量每日目前1_2.Name = "xrTable_用法用量每日目前1_2";
            this.xrTable_用法用量每日目前1_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前1_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell283.Dpi = 254F;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBorders = false;
            this.xrTableCell283.StylePriority.UseTextAlignment = false;
            this.xrTableCell283.Text = "次";
            this.xrTableCell283.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell283.Weight = 0.20118110356287111D;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell285.Dpi = 254F;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.StylePriority.UseTextAlignment = false;
            this.xrTableCell285.Text = "每次";
            this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell285.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前1_2
            // 
            this.xrTable_用法用量每次目前1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前1_2.CanGrow = false;
            this.xrTable_用法用量每次目前1_2.Dpi = 254F;
            this.xrTable_用法用量每次目前1_2.Name = "xrTable_用法用量每次目前1_2";
            this.xrTable_用法用量每次目前1_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前1_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell288.Dpi = 254F;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.StylePriority.UseTextAlignment = false;
            this.xrTableCell288.Text = "每日";
            this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell288.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前1_3
            // 
            this.xrTable_用法用量每日目前1_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前1_3.CanGrow = false;
            this.xrTable_用法用量每日目前1_3.Dpi = 254F;
            this.xrTable_用法用量每日目前1_3.Name = "xrTable_用法用量每日目前1_3";
            this.xrTable_用法用量每日目前1_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前1_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell287.Dpi = 254F;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.StylePriority.UseTextAlignment = false;
            this.xrTableCell287.Text = "次";
            this.xrTableCell287.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell287.Weight = 0.20118110356287111D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell289.Dpi = 254F;
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBorders = false;
            this.xrTableCell289.StylePriority.UseTextAlignment = false;
            this.xrTableCell289.Text = "每次";
            this.xrTableCell289.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell289.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前1_3
            // 
            this.xrTable_用法用量每次目前1_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前1_3.CanGrow = false;
            this.xrTable_用法用量每次目前1_3.Dpi = 254F;
            this.xrTable_用法用量每次目前1_3.Name = "xrTable_用法用量每次目前1_3";
            this.xrTable_用法用量每次目前1_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前1_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell291.Dpi = 254F;
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.StylePriority.UseBorders = false;
            this.xrTableCell291.StylePriority.UseTextAlignment = false;
            this.xrTableCell291.Text = "每日";
            this.xrTableCell291.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell291.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前1_4
            // 
            this.xrTable_用法用量每日目前1_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前1_4.CanGrow = false;
            this.xrTable_用法用量每日目前1_4.Dpi = 254F;
            this.xrTable_用法用量每日目前1_4.Name = "xrTable_用法用量每日目前1_4";
            this.xrTable_用法用量每日目前1_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前1_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell292.Dpi = 254F;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBorders = false;
            this.xrTableCell292.StylePriority.UseTextAlignment = false;
            this.xrTableCell292.Text = "次";
            this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell292.Weight = 0.20118110356287111D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell293.Dpi = 254F;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBorders = false;
            this.xrTableCell293.StylePriority.UseTextAlignment = false;
            this.xrTableCell293.Text = "每次";
            this.xrTableCell293.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell293.Weight = 0.287007873128275D;
            // 
            // xrTable_用法用量每次目前1_4
            // 
            this.xrTable_用法用量每次目前1_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前1_4.CanGrow = false;
            this.xrTable_用法用量每次目前1_4.Dpi = 254F;
            this.xrTable_用法用量每次目前1_4.Name = "xrTable_用法用量每次目前1_4";
            this.xrTable_用法用量每次目前1_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前1_4.Weight = 0.58781160388997578D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTable_药物名称目前2_1,
            this.xrTable_药物名称目前2_2,
            this.xrTable_药物名称目前2_3,
            this.xrTable_药物名称目前2_4});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell115.Dpi = 254F;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Text = "前";
            this.xrTableCell115.Weight = 0.31666672042035415D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell116.Dpi = 254F;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Text = "药物名称 2";
            this.xrTableCell116.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称目前2_1
            // 
            this.xrTable_药物名称目前2_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前2_1.CanGrow = false;
            this.xrTable_药物名称目前2_1.Dpi = 254F;
            this.xrTable_药物名称目前2_1.Name = "xrTable_药物名称目前2_1";
            this.xrTable_药物名称目前2_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前2_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前2_2
            // 
            this.xrTable_药物名称目前2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前2_2.CanGrow = false;
            this.xrTable_药物名称目前2_2.Dpi = 254F;
            this.xrTable_药物名称目前2_2.Name = "xrTable_药物名称目前2_2";
            this.xrTable_药物名称目前2_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前2_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前2_3
            // 
            this.xrTable_药物名称目前2_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前2_3.CanGrow = false;
            this.xrTable_药物名称目前2_3.Dpi = 254F;
            this.xrTable_药物名称目前2_3.Name = "xrTable_药物名称目前2_3";
            this.xrTable_药物名称目前2_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前2_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前2_4
            // 
            this.xrTable_药物名称目前2_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前2_4.CanGrow = false;
            this.xrTable_药物名称目前2_4.Dpi = 254F;
            this.xrTable_药物名称目前2_4.Name = "xrTable_药物名称目前2_4";
            this.xrTable_药物名称目前2_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前2_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell295,
            this.xrTable_用法用量每日目前2_1,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTable_用法用量每次目前2_1,
            this.xrTableCell299,
            this.xrTable_用法用量每日目前2_2,
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTable_用法用量每次目前2_2,
            this.xrTableCell304,
            this.xrTable_用法用量每日目前2_3,
            this.xrTableCell303,
            this.xrTableCell325,
            this.xrTable_用法用量每次目前2_3,
            this.xrTableCell306,
            this.xrTable_用法用量每日目前2_4,
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTable_用法用量每次目前2_4});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Text = "用";
            this.xrTableCell121.Weight = 0.31666672042035415D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Text = "用法用量";
            this.xrTableCell122.Weight = 1.0250000226216041D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell295.Dpi = 254F;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.StylePriority.UseTextAlignment = false;
            this.xrTableCell295.Text = "每日";
            this.xrTableCell295.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell295.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前2_1
            // 
            this.xrTable_用法用量每日目前2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前2_1.CanGrow = false;
            this.xrTable_用法用量每日目前2_1.Dpi = 254F;
            this.xrTable_用法用量每日目前2_1.Name = "xrTable_用法用量每日目前2_1";
            this.xrTable_用法用量每日目前2_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前2_1.Weight = 0.24637793459040347D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell296.Dpi = 254F;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.Text = "次";
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell296.Weight = 0.20118110356287111D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell297.Dpi = 254F;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBorders = false;
            this.xrTableCell297.StylePriority.UseTextAlignment = false;
            this.xrTableCell297.Text = "每次";
            this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell297.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前2_1
            // 
            this.xrTable_用法用量每次目前2_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前2_1.CanGrow = false;
            this.xrTable_用法用量每次目前2_1.Dpi = 254F;
            this.xrTable_用法用量每次目前2_1.Name = "xrTable_用法用量每次目前2_1";
            this.xrTable_用法用量每次目前2_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前2_1.Weight = 0.58781160388997589D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell299.Dpi = 254F;
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.StylePriority.UseTextAlignment = false;
            this.xrTableCell299.Text = "每日";
            this.xrTableCell299.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell299.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前2_2
            // 
            this.xrTable_用法用量每日目前2_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前2_2.CanGrow = false;
            this.xrTable_用法用量每日目前2_2.Dpi = 254F;
            this.xrTable_用法用量每日目前2_2.Name = "xrTable_用法用量每日目前2_2";
            this.xrTable_用法用量每日目前2_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前2_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell300.Dpi = 254F;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBorders = false;
            this.xrTableCell300.StylePriority.UseTextAlignment = false;
            this.xrTableCell300.Text = "次";
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell300.Weight = 0.20118110356287111D;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell301.Dpi = 254F;
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBorders = false;
            this.xrTableCell301.StylePriority.UseTextAlignment = false;
            this.xrTableCell301.Text = "每次";
            this.xrTableCell301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell301.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前2_2
            // 
            this.xrTable_用法用量每次目前2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前2_2.CanGrow = false;
            this.xrTable_用法用量每次目前2_2.Dpi = 254F;
            this.xrTable_用法用量每次目前2_2.Name = "xrTable_用法用量每次目前2_2";
            this.xrTable_用法用量每次目前2_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前2_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell304.Dpi = 254F;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.StylePriority.UseTextAlignment = false;
            this.xrTableCell304.Text = "每日";
            this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell304.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前2_3
            // 
            this.xrTable_用法用量每日目前2_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前2_3.CanGrow = false;
            this.xrTable_用法用量每日目前2_3.Dpi = 254F;
            this.xrTable_用法用量每日目前2_3.Name = "xrTable_用法用量每日目前2_3";
            this.xrTable_用法用量每日目前2_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前2_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell303.Dpi = 254F;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.StylePriority.UseTextAlignment = false;
            this.xrTableCell303.Text = "次";
            this.xrTableCell303.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell303.Weight = 0.20118110356287111D;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell325.Dpi = 254F;
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseBorders = false;
            this.xrTableCell325.StylePriority.UseTextAlignment = false;
            this.xrTableCell325.Text = "每次";
            this.xrTableCell325.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell325.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前2_3
            // 
            this.xrTable_用法用量每次目前2_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前2_3.CanGrow = false;
            this.xrTable_用法用量每次目前2_3.Dpi = 254F;
            this.xrTable_用法用量每次目前2_3.Name = "xrTable_用法用量每次目前2_3";
            this.xrTable_用法用量每次目前2_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前2_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell306.Dpi = 254F;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.StylePriority.UseTextAlignment = false;
            this.xrTableCell306.Text = "每日";
            this.xrTableCell306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell306.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前2_4
            // 
            this.xrTable_用法用量每日目前2_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前2_4.CanGrow = false;
            this.xrTable_用法用量每日目前2_4.Dpi = 254F;
            this.xrTable_用法用量每日目前2_4.Name = "xrTable_用法用量每日目前2_4";
            this.xrTable_用法用量每日目前2_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前2_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell307.Dpi = 254F;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.StylePriority.UseTextAlignment = false;
            this.xrTableCell307.Text = "次";
            this.xrTableCell307.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell307.Weight = 0.20118110356287111D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell308.Dpi = 254F;
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.StylePriority.UseTextAlignment = false;
            this.xrTableCell308.Text = "每次";
            this.xrTableCell308.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell308.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前2_4
            // 
            this.xrTable_用法用量每次目前2_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前2_4.CanGrow = false;
            this.xrTable_用法用量每次目前2_4.Dpi = 254F;
            this.xrTable_用法用量每次目前2_4.Name = "xrTable_用法用量每次目前2_4";
            this.xrTable_用法用量每次目前2_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前2_4.Weight = 0.58781160388997589D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTable_药物名称目前3_1,
            this.xrTable_药物名称目前3_2,
            this.xrTable_药物名称目前3_3,
            this.xrTable_药物名称目前3_4});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Text = "药";
            this.xrTableCell127.Weight = 0.31666672042035415D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.Text = "药物名称 3";
            this.xrTableCell128.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称目前3_1
            // 
            this.xrTable_药物名称目前3_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前3_1.CanGrow = false;
            this.xrTable_药物名称目前3_1.Dpi = 254F;
            this.xrTable_药物名称目前3_1.Name = "xrTable_药物名称目前3_1";
            this.xrTable_药物名称目前3_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前3_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前3_2
            // 
            this.xrTable_药物名称目前3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前3_2.CanGrow = false;
            this.xrTable_药物名称目前3_2.Dpi = 254F;
            this.xrTable_药物名称目前3_2.Name = "xrTable_药物名称目前3_2";
            this.xrTable_药物名称目前3_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前3_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前3_3
            // 
            this.xrTable_药物名称目前3_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前3_3.CanGrow = false;
            this.xrTable_药物名称目前3_3.Dpi = 254F;
            this.xrTable_药物名称目前3_3.Name = "xrTable_药物名称目前3_3";
            this.xrTable_药物名称目前3_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前3_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称目前3_4
            // 
            this.xrTable_药物名称目前3_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称目前3_4.CanGrow = false;
            this.xrTable_药物名称目前3_4.Dpi = 254F;
            this.xrTable_药物名称目前3_4.Name = "xrTable_药物名称目前3_4";
            this.xrTable_药物名称目前3_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称目前3_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell311,
            this.xrTable_用法用量每日目前3_1,
            this.xrTableCell310,
            this.xrTableCell312,
            this.xrTable_用法用量每次目前3_1,
            this.xrTableCell316,
            this.xrTable_用法用量每日目前3_2,
            this.xrTableCell314,
            this.xrTableCell315,
            this.xrTable_用法用量每次目前3_2,
            this.xrTableCell320,
            this.xrTable_用法用量每日目前3_3,
            this.xrTableCell318,
            this.xrTableCell319,
            this.xrTable_用法用量每次目前3_3,
            this.xrTableCell322,
            this.xrTable_用法用量每日目前3_4,
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTable_用法用量每次目前3_4});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Text = "情";
            this.xrTableCell133.Weight = 0.31666672042035415D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Text = "用法用量";
            this.xrTableCell134.Weight = 1.0250000226216041D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell311.Dpi = 254F;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.StylePriority.UseTextAlignment = false;
            this.xrTableCell311.Text = "每日";
            this.xrTableCell311.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell311.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前3_1
            // 
            this.xrTable_用法用量每日目前3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前3_1.CanGrow = false;
            this.xrTable_用法用量每日目前3_1.Dpi = 254F;
            this.xrTable_用法用量每日目前3_1.Name = "xrTable_用法用量每日目前3_1";
            this.xrTable_用法用量每日目前3_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前3_1.Weight = 0.24637793459040347D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell310.Dpi = 254F;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.StylePriority.UseTextAlignment = false;
            this.xrTableCell310.Text = "次";
            this.xrTableCell310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell310.Weight = 0.20118110356287111D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell312.Dpi = 254F;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.StylePriority.UseTextAlignment = false;
            this.xrTableCell312.Text = "每次";
            this.xrTableCell312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell312.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前3_1
            // 
            this.xrTable_用法用量每次目前3_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前3_1.CanGrow = false;
            this.xrTable_用法用量每次目前3_1.Dpi = 254F;
            this.xrTable_用法用量每次目前3_1.Name = "xrTable_用法用量每次目前3_1";
            this.xrTable_用法用量每次目前3_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前3_1.Weight = 0.58781160388997589D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell316.Dpi = 254F;
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBorders = false;
            this.xrTableCell316.StylePriority.UseTextAlignment = false;
            this.xrTableCell316.Text = "每日";
            this.xrTableCell316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell316.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前3_2
            // 
            this.xrTable_用法用量每日目前3_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前3_2.CanGrow = false;
            this.xrTable_用法用量每日目前3_2.Dpi = 254F;
            this.xrTable_用法用量每日目前3_2.Name = "xrTable_用法用量每日目前3_2";
            this.xrTable_用法用量每日目前3_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前3_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell314.Dpi = 254F;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBorders = false;
            this.xrTableCell314.StylePriority.UseTextAlignment = false;
            this.xrTableCell314.Text = "次";
            this.xrTableCell314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell314.Weight = 0.20118110356287111D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell315.Dpi = 254F;
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBorders = false;
            this.xrTableCell315.StylePriority.UseTextAlignment = false;
            this.xrTableCell315.Text = "每次";
            this.xrTableCell315.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell315.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前3_2
            // 
            this.xrTable_用法用量每次目前3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前3_2.CanGrow = false;
            this.xrTable_用法用量每次目前3_2.Dpi = 254F;
            this.xrTable_用法用量每次目前3_2.Name = "xrTable_用法用量每次目前3_2";
            this.xrTable_用法用量每次目前3_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前3_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell320.Dpi = 254F;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBorders = false;
            this.xrTableCell320.StylePriority.UseTextAlignment = false;
            this.xrTableCell320.Text = "每日";
            this.xrTableCell320.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell320.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前3_3
            // 
            this.xrTable_用法用量每日目前3_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前3_3.CanGrow = false;
            this.xrTable_用法用量每日目前3_3.Dpi = 254F;
            this.xrTable_用法用量每日目前3_3.Name = "xrTable_用法用量每日目前3_3";
            this.xrTable_用法用量每日目前3_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前3_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell318.Dpi = 254F;
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.StylePriority.UseTextAlignment = false;
            this.xrTableCell318.Text = "次";
            this.xrTableCell318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell318.Weight = 0.20118110356287111D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell319.Dpi = 254F;
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.StylePriority.UseTextAlignment = false;
            this.xrTableCell319.Text = "每次";
            this.xrTableCell319.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell319.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前3_3
            // 
            this.xrTable_用法用量每次目前3_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前3_3.CanGrow = false;
            this.xrTable_用法用量每次目前3_3.Dpi = 254F;
            this.xrTable_用法用量每次目前3_3.Name = "xrTable_用法用量每次目前3_3";
            this.xrTable_用法用量每次目前3_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前3_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell322.Dpi = 254F;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.StylePriority.UseTextAlignment = false;
            this.xrTableCell322.Text = "每日";
            this.xrTableCell322.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell322.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日目前3_4
            // 
            this.xrTable_用法用量每日目前3_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日目前3_4.CanGrow = false;
            this.xrTable_用法用量每日目前3_4.Dpi = 254F;
            this.xrTable_用法用量每日目前3_4.Name = "xrTable_用法用量每日目前3_4";
            this.xrTable_用法用量每日目前3_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日目前3_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell323.Dpi = 254F;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.StylePriority.UseTextAlignment = false;
            this.xrTableCell323.Text = "次";
            this.xrTableCell323.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell323.Weight = 0.20118110356287111D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell324.Dpi = 254F;
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.StylePriority.UseBorders = false;
            this.xrTableCell324.StylePriority.UseTextAlignment = false;
            this.xrTableCell324.Text = "每次";
            this.xrTableCell324.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell324.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次目前3_4
            // 
            this.xrTable_用法用量每次目前3_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次目前3_4.CanGrow = false;
            this.xrTable_用法用量每次目前3_4.Dpi = 254F;
            this.xrTable_用法用量每次目前3_4.Name = "xrTable_用法用量每次目前3_4";
            this.xrTable_用法用量每次目前3_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次目前3_4.Weight = 0.58781160388997589D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell363,
            this.xrTableCell364,
            this.xrTableCell365,
            this.xrTable_胰岛素种类目前_1,
            this.xrTableCell385,
            this.xrTable_胰岛素种类目前_2,
            this.xrTableCell394,
            this.xrTable_胰岛素种类目前_3,
            this.xrTableCell401,
            this.xrTable_胰岛素种类目前_4});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell363.Dpi = 254F;
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBorders = false;
            this.xrTableCell363.Text = "况";
            this.xrTableCell363.Weight = 0.31666672042035415D;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell364.Dpi = 254F;
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.StylePriority.UseBorders = false;
            this.xrTableCell364.Text = "胰岛素";
            this.xrTableCell364.Weight = 1.0250000226216041D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell365.Dpi = 254F;
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.StylePriority.UseTextAlignment = false;
            this.xrTableCell365.Text = "  种类：";
            this.xrTableCell365.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell365.Weight = 0.38976374607778475D;
            // 
            // xrTable_胰岛素种类目前_1
            // 
            this.xrTable_胰岛素种类目前_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类目前_1.CanGrow = false;
            this.xrTable_胰岛素种类目前_1.Dpi = 254F;
            this.xrTable_胰岛素种类目前_1.Name = "xrTable_胰岛素种类目前_1";
            this.xrTable_胰岛素种类目前_1.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类目前_1.Weight = 1.2767879593491771D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell385.Dpi = 254F;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.StylePriority.UseTextAlignment = false;
            this.xrTableCell385.Text = "  种类：";
            this.xrTableCell385.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell385.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类目前_2
            // 
            this.xrTable_胰岛素种类目前_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类目前_2.CanGrow = false;
            this.xrTable_胰岛素种类目前_2.Dpi = 254F;
            this.xrTable_胰岛素种类目前_2.Name = "xrTable_胰岛素种类目前_2";
            this.xrTable_胰岛素种类目前_2.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类目前_2.Weight = 1.2767879593491769D;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell394.Dpi = 254F;
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.StylePriority.UseBorders = false;
            this.xrTableCell394.StylePriority.UseTextAlignment = false;
            this.xrTableCell394.Text = "  种类：";
            this.xrTableCell394.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell394.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类目前_3
            // 
            this.xrTable_胰岛素种类目前_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类目前_3.CanGrow = false;
            this.xrTable_胰岛素种类目前_3.Dpi = 254F;
            this.xrTable_胰岛素种类目前_3.Name = "xrTable_胰岛素种类目前_3";
            this.xrTable_胰岛素种类目前_3.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类目前_3.Weight = 1.2767879593491769D;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell401.Dpi = 254F;
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.StylePriority.UseBorders = false;
            this.xrTableCell401.StylePriority.UseTextAlignment = false;
            this.xrTableCell401.Text = "  种类：";
            this.xrTableCell401.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell401.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类目前_4
            // 
            this.xrTable_胰岛素种类目前_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类目前_4.CanGrow = false;
            this.xrTable_胰岛素种类目前_4.Dpi = 254F;
            this.xrTable_胰岛素种类目前_4.Name = "xrTable_胰岛素种类目前_4";
            this.xrTable_胰岛素种类目前_4.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类目前_4.Weight = 1.2767879593491769D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell408,
            this.xrTable_胰岛素用法用量目前_1,
            this.xrTableCell413,
            this.xrTable_胰岛素用法用量目前_2,
            this.xrTableCell418,
            this.xrTable_胰岛素用法用量目前_3,
            this.xrTableCell423,
            this.xrTable_胰岛素用法用量目前_4});
            this.xrTableRow51.Dpi = 254F;
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell406.Dpi = 254F;
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.StylePriority.UseBorders = false;
            this.xrTableCell406.Weight = 0.31666672042035415D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell407.Dpi = 254F;
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.StylePriority.UseBorders = false;
            this.xrTableCell407.Weight = 1.0250000226216041D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell408.Dpi = 254F;
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseBorders = false;
            this.xrTableCell408.StylePriority.UseTextAlignment = false;
            this.xrTableCell408.Text = "  用法和用量：";
            this.xrTableCell408.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell408.Weight = 0.7165353812175258D;
            // 
            // xrTable_胰岛素用法用量目前_1
            // 
            this.xrTable_胰岛素用法用量目前_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量目前_1.CanGrow = false;
            this.xrTable_胰岛素用法用量目前_1.Dpi = 254F;
            this.xrTable_胰岛素用法用量目前_1.Name = "xrTable_胰岛素用法用量目前_1";
            this.xrTable_胰岛素用法用量目前_1.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量目前_1.Weight = 0.9500163242094356D;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell413.Dpi = 254F;
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.StylePriority.UseBorders = false;
            this.xrTableCell413.StylePriority.UseTextAlignment = false;
            this.xrTableCell413.Text = "  用法和用量：";
            this.xrTableCell413.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell413.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量目前_2
            // 
            this.xrTable_胰岛素用法用量目前_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量目前_2.CanGrow = false;
            this.xrTable_胰岛素用法用量目前_2.Dpi = 254F;
            this.xrTable_胰岛素用法用量目前_2.Name = "xrTable_胰岛素用法用量目前_2";
            this.xrTable_胰岛素用法用量目前_2.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量目前_2.Weight = 0.95001632420943549D;
            // 
            // xrTableCell418
            // 
            this.xrTableCell418.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell418.Dpi = 254F;
            this.xrTableCell418.Name = "xrTableCell418";
            this.xrTableCell418.StylePriority.UseBorders = false;
            this.xrTableCell418.StylePriority.UseTextAlignment = false;
            this.xrTableCell418.Text = "  用法和用量：";
            this.xrTableCell418.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell418.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量目前_3
            // 
            this.xrTable_胰岛素用法用量目前_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量目前_3.CanGrow = false;
            this.xrTable_胰岛素用法用量目前_3.Dpi = 254F;
            this.xrTable_胰岛素用法用量目前_3.Name = "xrTable_胰岛素用法用量目前_3";
            this.xrTable_胰岛素用法用量目前_3.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量目前_3.Weight = 0.95001632420943549D;
            // 
            // xrTableCell423
            // 
            this.xrTableCell423.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell423.Dpi = 254F;
            this.xrTableCell423.Name = "xrTableCell423";
            this.xrTableCell423.StylePriority.UseBorders = false;
            this.xrTableCell423.StylePriority.UseTextAlignment = false;
            this.xrTableCell423.Text = "  用法和用量：";
            this.xrTableCell423.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell423.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量目前_4
            // 
            this.xrTable_胰岛素用法用量目前_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量目前_4.CanGrow = false;
            this.xrTable_胰岛素用法用量目前_4.Dpi = 254F;
            this.xrTable_胰岛素用法用量目前_4.Name = "xrTable_胰岛素用法用量目前_4";
            this.xrTable_胰岛素用法用量目前_4.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量目前_4.Weight = 0.95001632420943549D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell105,
            this.xrTableCell141,
            this.xrTableCell106,
            this.xrTableCell142,
            this.xrTableCell107,
            this.xrTableCell143,
            this.xrTableCell108,
            this.xrTableCell144});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Dpi = 254F;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Text = "服药依从性";
            this.xrTableCell140.Weight = 1.3416667430419582D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Text = "1 规律 2 间断 3 不服药";
            this.xrTableCell105.Weight = 1.3877951744337624D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell141.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_服药依从性_1});
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.27875653099319897D;
            // 
            // xrLabel_服药依从性_1
            // 
            this.xrLabel_服药依从性_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_服药依从性_1.CanGrow = false;
            this.xrLabel_服药依从性_1.Dpi = 254F;
            this.xrLabel_服药依从性_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_服药依从性_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_服药依从性_1.Name = "xrLabel_服药依从性_1";
            this.xrLabel_服药依从性_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_服药依从性_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_服药依从性_1.StylePriority.UseBorders = false;
            this.xrLabel_服药依从性_1.StylePriority.UseFont = false;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "1 规律 2 间断 3 不服药";
            this.xrTableCell106.Weight = 1.3877951744337624D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_服药依从性_2});
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Weight = 0.27875653099319897D;
            // 
            // xrLabel_服药依从性_2
            // 
            this.xrLabel_服药依从性_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_服药依从性_2.CanGrow = false;
            this.xrLabel_服药依从性_2.Dpi = 254F;
            this.xrLabel_服药依从性_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_服药依从性_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_服药依从性_2.Name = "xrLabel_服药依从性_2";
            this.xrLabel_服药依从性_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_服药依从性_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_服药依从性_2.StylePriority.UseBorders = false;
            this.xrLabel_服药依从性_2.StylePriority.UseFont = false;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Text = "1 规律 2 间断 3 不服药";
            this.xrTableCell107.Weight = 1.3877951744337624D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_服药依从性_3});
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.27875653099319897D;
            // 
            // xrLabel_服药依从性_3
            // 
            this.xrLabel_服药依从性_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_服药依从性_3.CanGrow = false;
            this.xrLabel_服药依从性_3.Dpi = 254F;
            this.xrLabel_服药依从性_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_服药依从性_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_服药依从性_3.Name = "xrLabel_服药依从性_3";
            this.xrLabel_服药依从性_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_服药依从性_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_服药依从性_3.StylePriority.UseBorders = false;
            this.xrLabel_服药依从性_3.StylePriority.UseFont = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell108.Dpi = 254F;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Text = "1 规律 2 间断 3 不服药";
            this.xrTableCell108.Weight = 1.3877951744337624D;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell144.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_服药依从性_4});
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.27875653099319897D;
            // 
            // xrLabel_服药依从性_4
            // 
            this.xrLabel_服药依从性_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_服药依从性_4.CanGrow = false;
            this.xrLabel_服药依从性_4.Dpi = 254F;
            this.xrLabel_服药依从性_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_服药依从性_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_服药依从性_4.Name = "xrLabel_服药依从性_4";
            this.xrLabel_服药依从性_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_服药依从性_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_服药依从性_4.StylePriority.UseBorders = false;
            this.xrLabel_服药依从性_4.StylePriority.UseFont = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell376,
            this.xrTableCell380,
            this.xrTable_药物不良反应有_1,
            this.xrTableCell386,
            this.xrTableCell393,
            this.xrTable_药物不良反应有_2,
            this.xrTableCell390,
            this.xrTableCell412,
            this.xrTable_药物不良反应有_3,
            this.xrTableCell398,
            this.xrTableCell405,
            this.xrTable_药物不良反应有_4,
            this.xrTableCell402});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell376.Dpi = 254F;
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.Text = "药物不良反应";
            this.xrTableCell376.Weight = 1.3416667430419582D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell380.Dpi = 254F;
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.Text = "1 无 2 有";
            this.xrTableCell380.Weight = 0.45275586851436134D;
            // 
            // xrTable_药物不良反应有_1
            // 
            this.xrTable_药物不良反应有_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_药物不良反应有_1.CanGrow = false;
            this.xrTable_药物不良反应有_1.Dpi = 254F;
            this.xrTable_药物不良反应有_1.Name = "xrTable_药物不良反应有_1";
            this.xrTable_药物不良反应有_1.StylePriority.UseBorders = false;
            this.xrTable_药物不良反应有_1.StylePriority.UseTextAlignment = false;
            this.xrTable_药物不良反应有_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_药物不良反应有_1.Weight = 0.9350393059194011D;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell386.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_药物不良反应_1});
            this.xrTableCell386.Dpi = 254F;
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.StylePriority.UseBorders = false;
            this.xrTableCell386.Weight = 0.27875653099319897D;
            // 
            // xrLabel_药物不良反应_1
            // 
            this.xrLabel_药物不良反应_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_药物不良反应_1.CanGrow = false;
            this.xrLabel_药物不良反应_1.Dpi = 254F;
            this.xrLabel_药物不良反应_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_药物不良反应_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_药物不良反应_1.Name = "xrLabel_药物不良反应_1";
            this.xrLabel_药物不良反应_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_药物不良反应_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_药物不良反应_1.StylePriority.UseBorders = false;
            this.xrLabel_药物不良反应_1.StylePriority.UseFont = false;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell393.Dpi = 254F;
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseBorders = false;
            this.xrTableCell393.Text = "1 无 2 有";
            this.xrTableCell393.Weight = 0.45275586851436134D;
            // 
            // xrTable_药物不良反应有_2
            // 
            this.xrTable_药物不良反应有_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_药物不良反应有_2.CanGrow = false;
            this.xrTable_药物不良反应有_2.Dpi = 254F;
            this.xrTable_药物不良反应有_2.Name = "xrTable_药物不良反应有_2";
            this.xrTable_药物不良反应有_2.StylePriority.UseBorders = false;
            this.xrTable_药物不良反应有_2.StylePriority.UseTextAlignment = false;
            this.xrTable_药物不良反应有_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_药物不良反应有_2.Weight = 0.9350393059194011D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell390.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_药物不良反应_2});
            this.xrTableCell390.Dpi = 254F;
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Weight = 0.27875653099319897D;
            // 
            // xrLabel_药物不良反应_2
            // 
            this.xrLabel_药物不良反应_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_药物不良反应_2.CanGrow = false;
            this.xrLabel_药物不良反应_2.Dpi = 254F;
            this.xrLabel_药物不良反应_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_药物不良反应_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_药物不良反应_2.Name = "xrLabel_药物不良反应_2";
            this.xrLabel_药物不良反应_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_药物不良反应_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_药物不良反应_2.StylePriority.UseBorders = false;
            this.xrLabel_药物不良反应_2.StylePriority.UseFont = false;
            // 
            // xrTableCell412
            // 
            this.xrTableCell412.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell412.Dpi = 254F;
            this.xrTableCell412.Name = "xrTableCell412";
            this.xrTableCell412.StylePriority.UseBorders = false;
            this.xrTableCell412.Text = "1 无 2 有";
            this.xrTableCell412.Weight = 0.45275586851436134D;
            // 
            // xrTable_药物不良反应有_3
            // 
            this.xrTable_药物不良反应有_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_药物不良反应有_3.CanGrow = false;
            this.xrTable_药物不良反应有_3.Dpi = 254F;
            this.xrTable_药物不良反应有_3.Name = "xrTable_药物不良反应有_3";
            this.xrTable_药物不良反应有_3.StylePriority.UseBorders = false;
            this.xrTable_药物不良反应有_3.StylePriority.UseTextAlignment = false;
            this.xrTable_药物不良反应有_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_药物不良反应有_3.Weight = 0.9350393059194011D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell398.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_药物不良反应_3});
            this.xrTableCell398.Dpi = 254F;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseBorders = false;
            this.xrTableCell398.Weight = 0.27875653099319897D;
            // 
            // xrLabel_药物不良反应_3
            // 
            this.xrLabel_药物不良反应_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_药物不良反应_3.CanGrow = false;
            this.xrLabel_药物不良反应_3.Dpi = 254F;
            this.xrLabel_药物不良反应_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_药物不良反应_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_药物不良反应_3.Name = "xrLabel_药物不良反应_3";
            this.xrLabel_药物不良反应_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_药物不良反应_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_药物不良反应_3.StylePriority.UseBorders = false;
            this.xrLabel_药物不良反应_3.StylePriority.UseFont = false;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell405.Dpi = 254F;
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseBorders = false;
            this.xrTableCell405.Text = "1 无 2 有";
            this.xrTableCell405.Weight = 0.45275586851436134D;
            // 
            // xrTable_药物不良反应有_4
            // 
            this.xrTable_药物不良反应有_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_药物不良反应有_4.CanGrow = false;
            this.xrTable_药物不良反应有_4.Dpi = 254F;
            this.xrTable_药物不良反应有_4.Name = "xrTable_药物不良反应有_4";
            this.xrTable_药物不良反应有_4.StylePriority.UseBorders = false;
            this.xrTable_药物不良反应有_4.StylePriority.UseTextAlignment = false;
            this.xrTable_药物不良反应有_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_药物不良反应有_4.Weight = 0.9350393059194011D;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell402.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_药物不良反应_4});
            this.xrTableCell402.Dpi = 254F;
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.StylePriority.UseBorders = false;
            this.xrTableCell402.Weight = 0.27875653099319897D;
            // 
            // xrLabel_药物不良反应_4
            // 
            this.xrLabel_药物不良反应_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_药物不良反应_4.CanGrow = false;
            this.xrLabel_药物不良反应_4.Dpi = 254F;
            this.xrLabel_药物不良反应_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_药物不良反应_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_药物不良反应_4.Name = "xrLabel_药物不良反应_4";
            this.xrLabel_药物不良反应_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_药物不良反应_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_药物不良反应_4.StylePriority.UseBorders = false;
            this.xrLabel_药物不良反应_4.StylePriority.UseFont = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell403,
            this.xrTableCell404,
            this.xrTableCell409,
            this.xrTableCell410,
            this.xrTableCell411,
            this.xrTableCell414,
            this.xrTableCell415,
            this.xrTableCell416,
            this.xrTableCell419});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell403.Dpi = 254F;
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseBorders = false;
            this.xrTableCell403.Text = "低血糖反应";
            this.xrTableCell403.Weight = 1.3416667430419582D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell404.Dpi = 254F;
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseBorders = false;
            this.xrTableCell404.Text = "1 无    2 偶尔    3 频繁";
            this.xrTableCell404.Weight = 1.3877951744337624D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell409.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_低血糖反应_1});
            this.xrTableCell409.Dpi = 254F;
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.Weight = 0.27875653099319897D;
            // 
            // xrLabel_低血糖反应_1
            // 
            this.xrLabel_低血糖反应_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_低血糖反应_1.CanGrow = false;
            this.xrLabel_低血糖反应_1.Dpi = 254F;
            this.xrLabel_低血糖反应_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_低血糖反应_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_低血糖反应_1.Name = "xrLabel_低血糖反应_1";
            this.xrLabel_低血糖反应_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_低血糖反应_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_低血糖反应_1.StylePriority.UseBorders = false;
            this.xrLabel_低血糖反应_1.StylePriority.UseFont = false;
            // 
            // xrTableCell410
            // 
            this.xrTableCell410.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell410.Dpi = 254F;
            this.xrTableCell410.Name = "xrTableCell410";
            this.xrTableCell410.StylePriority.UseBorders = false;
            this.xrTableCell410.Text = "1 无    2 偶尔    3 频繁";
            this.xrTableCell410.Weight = 1.3877951744337624D;
            // 
            // xrTableCell411
            // 
            this.xrTableCell411.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell411.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_低血糖反应_2});
            this.xrTableCell411.Dpi = 254F;
            this.xrTableCell411.Name = "xrTableCell411";
            this.xrTableCell411.StylePriority.UseBorders = false;
            this.xrTableCell411.Weight = 0.27875653099319897D;
            // 
            // xrLabel_低血糖反应_2
            // 
            this.xrLabel_低血糖反应_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_低血糖反应_2.CanGrow = false;
            this.xrLabel_低血糖反应_2.Dpi = 254F;
            this.xrLabel_低血糖反应_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_低血糖反应_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_低血糖反应_2.Name = "xrLabel_低血糖反应_2";
            this.xrLabel_低血糖反应_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_低血糖反应_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_低血糖反应_2.StylePriority.UseBorders = false;
            this.xrLabel_低血糖反应_2.StylePriority.UseFont = false;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell414.Dpi = 254F;
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.StylePriority.UseBorders = false;
            this.xrTableCell414.Text = "1 无    2 偶尔    3 频繁";
            this.xrTableCell414.Weight = 1.3877951744337624D;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell415.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_低血糖反应_3});
            this.xrTableCell415.Dpi = 254F;
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.StylePriority.UseBorders = false;
            this.xrTableCell415.Weight = 0.27875653099319897D;
            // 
            // xrLabel_低血糖反应_3
            // 
            this.xrLabel_低血糖反应_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_低血糖反应_3.CanGrow = false;
            this.xrLabel_低血糖反应_3.Dpi = 254F;
            this.xrLabel_低血糖反应_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_低血糖反应_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_低血糖反应_3.Name = "xrLabel_低血糖反应_3";
            this.xrLabel_低血糖反应_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_低血糖反应_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_低血糖反应_3.StylePriority.UseBorders = false;
            this.xrLabel_低血糖反应_3.StylePriority.UseFont = false;
            // 
            // xrTableCell416
            // 
            this.xrTableCell416.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell416.Dpi = 254F;
            this.xrTableCell416.Name = "xrTableCell416";
            this.xrTableCell416.StylePriority.UseBorders = false;
            this.xrTableCell416.Text = "1 无    2 偶尔    3 频繁";
            this.xrTableCell416.Weight = 1.3877951744337624D;
            // 
            // xrTableCell419
            // 
            this.xrTableCell419.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell419.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_低血糖反应_4});
            this.xrTableCell419.Dpi = 254F;
            this.xrTableCell419.Name = "xrTableCell419";
            this.xrTableCell419.StylePriority.UseBorders = false;
            this.xrTableCell419.Weight = 0.27875653099319897D;
            // 
            // xrLabel_低血糖反应_4
            // 
            this.xrLabel_低血糖反应_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_低血糖反应_4.CanGrow = false;
            this.xrLabel_低血糖反应_4.Dpi = 254F;
            this.xrLabel_低血糖反应_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_低血糖反应_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_低血糖反应_4.Name = "xrLabel_低血糖反应_4";
            this.xrLabel_低血糖反应_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_低血糖反应_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_低血糖反应_4.StylePriority.UseBorders = false;
            this.xrLabel_低血糖反应_4.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Text = "此次";
            this.xrTableCell146.Weight = 1.3416667430419582D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.StylePriority.UseTextAlignment = false;
            this.xrTableCell147.Text = "1 控制满意   2 控制不满意";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell147.Weight = 1.6665517054269614D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Text = "1 控制满意   2 控制不满意";
            this.xrTableCell148.Weight = 1.6665517054269614D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Text = "1 控制满意   2 控制不满意";
            this.xrTableCell149.Weight = 1.6665517054269614D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Text = "1 控制满意   2 控制不满意";
            this.xrTableCell150.Weight = 1.6665517054269614D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTableCell111,
            this.xrTableCell153,
            this.xrTableCell112,
            this.xrTableCell154,
            this.xrTableCell113,
            this.xrTableCell155,
            this.xrTableCell114,
            this.xrTableCell156});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Text = "随访分类";
            this.xrTableCell152.Weight = 1.3416667430419582D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell111.Dpi = 254F;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Text = "3 不良反应 4 并发症";
            this.xrTableCell111.Weight = 1.3877952945817009D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_此次随访分类_1});
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.StylePriority.UseTextAlignment = false;
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell153.Weight = 0.27875641084526037D;
            // 
            // xrLabel_此次随访分类_1
            // 
            this.xrLabel_此次随访分类_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_此次随访分类_1.CanGrow = false;
            this.xrLabel_此次随访分类_1.Dpi = 254F;
            this.xrLabel_此次随访分类_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_此次随访分类_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_此次随访分类_1.Name = "xrLabel_此次随访分类_1";
            this.xrLabel_此次随访分类_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_此次随访分类_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_此次随访分类_1.StylePriority.UseBorders = false;
            this.xrLabel_此次随访分类_1.StylePriority.UseFont = false;
            this.xrLabel_此次随访分类_1.StylePriority.UseTextAlignment = false;
            this.xrLabel_此次随访分类_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell112.Dpi = 254F;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Text = "3 不良反应 4 并发症";
            this.xrTableCell112.Weight = 1.3877951744337624D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell154.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_此次随访分类_2});
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Weight = 0.27875653099319897D;
            // 
            // xrLabel_此次随访分类_2
            // 
            this.xrLabel_此次随访分类_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_此次随访分类_2.CanGrow = false;
            this.xrLabel_此次随访分类_2.Dpi = 254F;
            this.xrLabel_此次随访分类_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_此次随访分类_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_此次随访分类_2.Name = "xrLabel_此次随访分类_2";
            this.xrLabel_此次随访分类_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_此次随访分类_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_此次随访分类_2.StylePriority.UseBorders = false;
            this.xrLabel_此次随访分类_2.StylePriority.UseFont = false;
            this.xrLabel_此次随访分类_2.StylePriority.UseTextAlignment = false;
            this.xrLabel_此次随访分类_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell113.Dpi = 254F;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Text = "3 不良反应 4 并发症";
            this.xrTableCell113.Weight = 1.3877951744337624D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell155.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_此次随访分类_3});
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Weight = 0.27875653099319897D;
            // 
            // xrLabel_此次随访分类_3
            // 
            this.xrLabel_此次随访分类_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_此次随访分类_3.CanGrow = false;
            this.xrLabel_此次随访分类_3.Dpi = 254F;
            this.xrLabel_此次随访分类_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_此次随访分类_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_此次随访分类_3.Name = "xrLabel_此次随访分类_3";
            this.xrLabel_此次随访分类_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_此次随访分类_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_此次随访分类_3.StylePriority.UseBorders = false;
            this.xrLabel_此次随访分类_3.StylePriority.UseFont = false;
            this.xrLabel_此次随访分类_3.StylePriority.UseTextAlignment = false;
            this.xrLabel_此次随访分类_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Dpi = 254F;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Text = "3 不良反应 4 并发症";
            this.xrTableCell114.Weight = 1.3877951744337624D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_此次随访分类_4});
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Weight = 0.27875653099319897D;
            // 
            // xrLabel_此次随访分类_4
            // 
            this.xrLabel_此次随访分类_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_此次随访分类_4.CanGrow = false;
            this.xrLabel_此次随访分类_4.Dpi = 254F;
            this.xrLabel_此次随访分类_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_此次随访分类_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_此次随访分类_4.Name = "xrLabel_此次随访分类_4";
            this.xrLabel_此次随访分类_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_此次随访分类_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_此次随访分类_4.StylePriority.UseBorders = false;
            this.xrLabel_此次随访分类_4.StylePriority.UseFont = false;
            this.xrLabel_此次随访分类_4.StylePriority.UseTextAlignment = false;
            this.xrLabel_此次随访分类_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell187});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.Weight = 1.3416667430419582D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.Text = "1常规随访 2第一次控制不满";
            this.xrTableCell100.Weight = 1.6665517054269614D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Text = "1常规随访 2第一次控制不满";
            this.xrTableCell101.Weight = 1.6665517054269614D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Text = "1常规随访 2第一次控制不满";
            this.xrTableCell102.Weight = 1.6665517054269614D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Text = "1常规随访 2第一次控制不满";
            this.xrTableCell187.Weight = 1.6665517054269614D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell190,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell198,
            this.xrTableCell199});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Text = "下一步管理措施";
            this.xrTableCell190.Weight = 1.3416667430419582D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.Text = "意2周随访 3两次控制不满意";
            this.xrTableCell193.Weight = 1.6665517054269614D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Text = "意2周随访 3两次控制不满意";
            this.xrTableCell194.Weight = 1.6665517054269614D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Text = "意2周随访 3两次控制不满意";
            this.xrTableCell198.Weight = 1.6665517054269614D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.Text = "意2周随访 3两次控制不满意";
            this.xrTableCell199.Weight = 1.6665517054269614D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell201,
            this.xrTableCell97,
            this.xrTableCell202,
            this.xrTableCell135,
            this.xrTableCell203,
            this.xrTableCell136,
            this.xrTableCell204,
            this.xrTableCell137,
            this.xrTableCell205});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseBorders = false;
            this.xrTableCell201.Weight = 1.3416667430419582D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseFont = false;
            this.xrTableCell97.StylePriority.UseTextAlignment = false;
            this.xrTableCell97.Text = "     转诊随访 4紧急转诊";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell97.Weight = 1.3877951744337624D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell202.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下一步管理措施_1});
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.StylePriority.UseBorders = false;
            this.xrTableCell202.Weight = 0.27875653099319897D;
            // 
            // xrLabel_下一步管理措施_1
            // 
            this.xrLabel_下一步管理措施_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下一步管理措施_1.CanGrow = false;
            this.xrLabel_下一步管理措施_1.Dpi = 254F;
            this.xrLabel_下一步管理措施_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_下一步管理措施_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_下一步管理措施_1.Name = "xrLabel_下一步管理措施_1";
            this.xrLabel_下一步管理措施_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_下一步管理措施_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_下一步管理措施_1.StylePriority.UseBorders = false;
            this.xrLabel_下一步管理措施_1.StylePriority.UseFont = false;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.StylePriority.UseTextAlignment = false;
            this.xrTableCell135.Text = "     转诊随访 4紧急转诊";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell135.Weight = 1.3877951744337624D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下一步管理措施_2});
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.Weight = 0.27875653099319897D;
            // 
            // xrLabel_下一步管理措施_2
            // 
            this.xrLabel_下一步管理措施_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下一步管理措施_2.CanGrow = false;
            this.xrLabel_下一步管理措施_2.Dpi = 254F;
            this.xrLabel_下一步管理措施_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_下一步管理措施_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_下一步管理措施_2.Name = "xrLabel_下一步管理措施_2";
            this.xrLabel_下一步管理措施_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_下一步管理措施_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_下一步管理措施_2.StylePriority.UseBorders = false;
            this.xrLabel_下一步管理措施_2.StylePriority.UseFont = false;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.StylePriority.UseTextAlignment = false;
            this.xrTableCell136.Text = "     转诊随访 4紧急转诊";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell136.Weight = 1.3877951744337624D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下一步管理措施_3});
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.Weight = 0.27875653099319897D;
            // 
            // xrLabel_下一步管理措施_3
            // 
            this.xrLabel_下一步管理措施_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下一步管理措施_3.CanGrow = false;
            this.xrLabel_下一步管理措施_3.Dpi = 254F;
            this.xrLabel_下一步管理措施_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_下一步管理措施_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_下一步管理措施_3.Name = "xrLabel_下一步管理措施_3";
            this.xrLabel_下一步管理措施_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_下一步管理措施_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_下一步管理措施_3.StylePriority.UseBorders = false;
            this.xrLabel_下一步管理措施_3.StylePriority.UseFont = false;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.StylePriority.UseTextAlignment = false;
            this.xrTableCell137.Text = "     转诊随访 4紧急转诊";
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell137.Weight = 1.3877951744337624D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell205.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下一步管理措施_4});
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseBorders = false;
            this.xrTableCell205.Weight = 0.27875653099319897D;
            // 
            // xrLabel_下一步管理措施_4
            // 
            this.xrLabel_下一步管理措施_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下一步管理措施_4.CanGrow = false;
            this.xrLabel_下一步管理措施_4.Dpi = 254F;
            this.xrLabel_下一步管理措施_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_下一步管理措施_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_下一步管理措施_4.Name = "xrLabel_下一步管理措施_4";
            this.xrLabel_下一步管理措施_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_下一步管理措施_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_下一步管理措施_4.StylePriority.UseBorders = false;
            this.xrLabel_下一步管理措施_4.StylePriority.UseFont = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTable_药物名称调整1_1,
            this.xrTable_药物名称调整1_2,
            this.xrTable_药物名称调整1_3,
            this.xrTable_药物名称调整1_4});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseBorders = false;
            this.xrTableCell206.Weight = 0.31666672042035415D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell207.Dpi = 254F;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseBorders = false;
            this.xrTableCell207.Text = "药物名称1";
            this.xrTableCell207.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称调整1_1
            // 
            this.xrTable_药物名称调整1_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整1_1.CanGrow = false;
            this.xrTable_药物名称调整1_1.Dpi = 254F;
            this.xrTable_药物名称调整1_1.Name = "xrTable_药物名称调整1_1";
            this.xrTable_药物名称调整1_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整1_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整1_2
            // 
            this.xrTable_药物名称调整1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整1_2.CanGrow = false;
            this.xrTable_药物名称调整1_2.Dpi = 254F;
            this.xrTable_药物名称调整1_2.Name = "xrTable_药物名称调整1_2";
            this.xrTable_药物名称调整1_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整1_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整1_3
            // 
            this.xrTable_药物名称调整1_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整1_3.CanGrow = false;
            this.xrTable_药物名称调整1_3.Dpi = 254F;
            this.xrTable_药物名称调整1_3.Name = "xrTable_药物名称调整1_3";
            this.xrTable_药物名称调整1_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整1_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整1_4
            // 
            this.xrTable_药物名称调整1_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整1_4.Dpi = 254F;
            this.xrTable_药物名称调整1_4.Name = "xrTable_药物名称调整1_4";
            this.xrTable_药物名称调整1_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整1_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell125,
            this.xrTable_用法用量每日调整1_1,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTable_用法用量每次调整1_1,
            this.xrTableCell151,
            this.xrTable_用法用量每日调整1_2,
            this.xrTableCell189,
            this.xrTableCell200,
            this.xrTable_用法用量每次调整1_2,
            this.xrTableCell282,
            this.xrTable_用法用量每日调整1_3,
            this.xrTableCell272,
            this.xrTableCell278,
            this.xrTable_用法用量每次调整1_3,
            this.xrTableCell294,
            this.xrTable_用法用量每日调整1_4,
            this.xrTableCell290,
            this.xrTableCell298,
            this.xrTable_用法用量每次调整1_4});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell212.Dpi = 254F;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseBorders = false;
            this.xrTableCell212.Text = "用";
            this.xrTableCell212.Weight = 0.31666672042035415D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell213.Dpi = 254F;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseBorders = false;
            this.xrTableCell213.Text = "用法用量";
            this.xrTableCell213.Weight = 1.0250000226216041D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.StylePriority.UseTextAlignment = false;
            this.xrTableCell125.Text = "每日";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell125.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整1_1
            // 
            this.xrTable_用法用量每日调整1_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整1_1.CanGrow = false;
            this.xrTable_用法用量每日调整1_1.Dpi = 254F;
            this.xrTable_用法用量每日调整1_1.Name = "xrTable_用法用量每日调整1_1";
            this.xrTable_用法用量每日调整1_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整1_1.Weight = 0.24637793459040347D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.Dpi = 254F;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.StylePriority.UseTextAlignment = false;
            this.xrTableCell138.Text = "次";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell138.Weight = 0.20118110356287111D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell139.Dpi = 254F;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.StylePriority.UseTextAlignment = false;
            this.xrTableCell139.Text = "每次";
            this.xrTableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell139.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整1_1
            // 
            this.xrTable_用法用量每次调整1_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整1_1.CanGrow = false;
            this.xrTable_用法用量每次调整1_1.Dpi = 254F;
            this.xrTable_用法用量每次调整1_1.Name = "xrTable_用法用量每次调整1_1";
            this.xrTable_用法用量每次调整1_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整1_1.Weight = 0.58781160388997589D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.StylePriority.UseTextAlignment = false;
            this.xrTableCell151.Text = "每日";
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell151.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整1_2
            // 
            this.xrTable_用法用量每日调整1_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整1_2.CanGrow = false;
            this.xrTable_用法用量每日调整1_2.Dpi = 254F;
            this.xrTable_用法用量每日调整1_2.Name = "xrTable_用法用量每日调整1_2";
            this.xrTable_用法用量每日调整1_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整1_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.Text = "次";
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell189.Weight = 0.20118110356287111D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.StylePriority.UseTextAlignment = false;
            this.xrTableCell200.Text = "每次";
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell200.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整1_2
            // 
            this.xrTable_用法用量每次调整1_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整1_2.CanGrow = false;
            this.xrTable_用法用量每次调整1_2.Dpi = 254F;
            this.xrTable_用法用量每次调整1_2.Name = "xrTable_用法用量每次调整1_2";
            this.xrTable_用法用量每次调整1_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整1_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell282.Dpi = 254F;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBorders = false;
            this.xrTableCell282.StylePriority.UseTextAlignment = false;
            this.xrTableCell282.Text = "每日";
            this.xrTableCell282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell282.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整1_3
            // 
            this.xrTable_用法用量每日调整1_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整1_3.CanGrow = false;
            this.xrTable_用法用量每日调整1_3.Dpi = 254F;
            this.xrTable_用法用量每日调整1_3.Name = "xrTable_用法用量每日调整1_3";
            this.xrTable_用法用量每日调整1_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整1_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell272.Dpi = 254F;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.StylePriority.UseTextAlignment = false;
            this.xrTableCell272.Text = "次";
            this.xrTableCell272.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell272.Weight = 0.20118110356287111D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell278.Dpi = 254F;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.StylePriority.UseTextAlignment = false;
            this.xrTableCell278.Text = "每次";
            this.xrTableCell278.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell278.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整1_3
            // 
            this.xrTable_用法用量每次调整1_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整1_3.CanGrow = false;
            this.xrTable_用法用量每次调整1_3.Dpi = 254F;
            this.xrTable_用法用量每次调整1_3.Name = "xrTable_用法用量每次调整1_3";
            this.xrTable_用法用量每次调整1_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整1_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell294.Dpi = 254F;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.StylePriority.UseBorders = false;
            this.xrTableCell294.StylePriority.UseTextAlignment = false;
            this.xrTableCell294.Text = "每日";
            this.xrTableCell294.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell294.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整1_4
            // 
            this.xrTable_用法用量每日调整1_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整1_4.CanGrow = false;
            this.xrTable_用法用量每日调整1_4.Dpi = 254F;
            this.xrTable_用法用量每日调整1_4.Name = "xrTable_用法用量每日调整1_4";
            this.xrTable_用法用量每日调整1_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整1_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell290.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell290.Dpi = 254F;
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell290.StylePriority.UseBorders = false;
            this.xrTableCell290.StylePriority.UseTextAlignment = false;
            this.xrTableCell290.Text = "次";
            this.xrTableCell290.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell290.Weight = 0.20118110356287111D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell298.Dpi = 254F;
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.StylePriority.UseBorders = false;
            this.xrTableCell298.StylePriority.UseTextAlignment = false;
            this.xrTableCell298.Text = "每次";
            this.xrTableCell298.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell298.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整1_4
            // 
            this.xrTable_用法用量每次调整1_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整1_4.CanGrow = false;
            this.xrTable_用法用量每次调整1_4.Dpi = 254F;
            this.xrTable_用法用量每次调整1_4.Name = "xrTable_用法用量每次调整1_4";
            this.xrTable_用法用量每次调整1_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整1_4.Weight = 0.58781160388997589D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTable_药物名称调整2_1,
            this.xrTable_药物名称调整2_2,
            this.xrTable_药物名称调整2_3,
            this.xrTable_药物名称调整2_4});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell218.Dpi = 254F;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBorders = false;
            this.xrTableCell218.Text = "药";
            this.xrTableCell218.Weight = 0.31666672042035415D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell219.Dpi = 254F;
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseBorders = false;
            this.xrTableCell219.Text = "药物名称2";
            this.xrTableCell219.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称调整2_1
            // 
            this.xrTable_药物名称调整2_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整2_1.CanGrow = false;
            this.xrTable_药物名称调整2_1.Dpi = 254F;
            this.xrTable_药物名称调整2_1.Name = "xrTable_药物名称调整2_1";
            this.xrTable_药物名称调整2_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整2_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整2_2
            // 
            this.xrTable_药物名称调整2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整2_2.CanGrow = false;
            this.xrTable_药物名称调整2_2.Dpi = 254F;
            this.xrTable_药物名称调整2_2.Name = "xrTable_药物名称调整2_2";
            this.xrTable_药物名称调整2_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整2_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整2_3
            // 
            this.xrTable_药物名称调整2_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整2_3.CanGrow = false;
            this.xrTable_药物名称调整2_3.Dpi = 254F;
            this.xrTable_药物名称调整2_3.Name = "xrTable_药物名称调整2_3";
            this.xrTable_药物名称调整2_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整2_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整2_4
            // 
            this.xrTable_药物名称调整2_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整2_4.CanGrow = false;
            this.xrTable_药物名称调整2_4.Dpi = 254F;
            this.xrTable_药物名称调整2_4.Name = "xrTable_药物名称调整2_4";
            this.xrTable_药物名称调整2_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整2_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224,
            this.xrTableCell225,
            this.xrTableCell305,
            this.xrTable_用法用量每日调整2_1,
            this.xrTableCell309,
            this.xrTableCell313,
            this.xrTable_用法用量每次调整2_1,
            this.xrTableCell321,
            this.xrTable_用法用量每日调整2_2,
            this.xrTableCell326,
            this.xrTableCell327,
            this.xrTable_用法用量每次调整2_2,
            this.xrTableCell329,
            this.xrTable_用法用量每日调整2_3,
            this.xrTableCell330,
            this.xrTableCell331,
            this.xrTable_用法用量每次调整2_3,
            this.xrTableCell333,
            this.xrTable_用法用量每日调整2_4,
            this.xrTableCell334,
            this.xrTableCell335,
            this.xrTable_用法用量每次调整2_4});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell224.Dpi = 254F;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.Text = "调";
            this.xrTableCell224.Weight = 0.31666672042035415D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell225.Dpi = 254F;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Text = "用法用量";
            this.xrTableCell225.Weight = 1.0250000226216041D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell305.Dpi = 254F;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.StylePriority.UseTextAlignment = false;
            this.xrTableCell305.Text = "每日";
            this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell305.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整2_1
            // 
            this.xrTable_用法用量每日调整2_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整2_1.CanGrow = false;
            this.xrTable_用法用量每日调整2_1.Dpi = 254F;
            this.xrTable_用法用量每日调整2_1.Name = "xrTable_用法用量每日调整2_1";
            this.xrTable_用法用量每日调整2_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整2_1.Weight = 0.24637793459040347D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell309.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell309.Dpi = 254F;
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.StylePriority.UseTextAlignment = false;
            this.xrTableCell309.Text = "次";
            this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell309.Weight = 0.20118110356287111D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell313.Dpi = 254F;
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseBorders = false;
            this.xrTableCell313.StylePriority.UseTextAlignment = false;
            this.xrTableCell313.Text = "每次";
            this.xrTableCell313.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell313.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整2_1
            // 
            this.xrTable_用法用量每次调整2_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整2_1.CanGrow = false;
            this.xrTable_用法用量每次调整2_1.Dpi = 254F;
            this.xrTable_用法用量每次调整2_1.Name = "xrTable_用法用量每次调整2_1";
            this.xrTable_用法用量每次调整2_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整2_1.Weight = 0.58781160388997589D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell321.Dpi = 254F;
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseBorders = false;
            this.xrTableCell321.StylePriority.UseTextAlignment = false;
            this.xrTableCell321.Text = "每日";
            this.xrTableCell321.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell321.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整2_2
            // 
            this.xrTable_用法用量每日调整2_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整2_2.CanGrow = false;
            this.xrTable_用法用量每日调整2_2.Dpi = 254F;
            this.xrTable_用法用量每日调整2_2.Name = "xrTable_用法用量每日调整2_2";
            this.xrTable_用法用量每日调整2_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整2_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell326.Dpi = 254F;
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell326.StylePriority.UseBorders = false;
            this.xrTableCell326.StylePriority.UseTextAlignment = false;
            this.xrTableCell326.Text = "次";
            this.xrTableCell326.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell326.Weight = 0.20118110356287111D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell327.Dpi = 254F;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBorders = false;
            this.xrTableCell327.StylePriority.UseTextAlignment = false;
            this.xrTableCell327.Text = "每次";
            this.xrTableCell327.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell327.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整2_2
            // 
            this.xrTable_用法用量每次调整2_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整2_2.CanGrow = false;
            this.xrTable_用法用量每次调整2_2.Dpi = 254F;
            this.xrTable_用法用量每次调整2_2.Name = "xrTable_用法用量每次调整2_2";
            this.xrTable_用法用量每次调整2_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整2_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell329.Dpi = 254F;
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.StylePriority.UseTextAlignment = false;
            this.xrTableCell329.Text = "每日";
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell329.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整2_3
            // 
            this.xrTable_用法用量每日调整2_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整2_3.CanGrow = false;
            this.xrTable_用法用量每日调整2_3.Dpi = 254F;
            this.xrTable_用法用量每日调整2_3.Name = "xrTable_用法用量每日调整2_3";
            this.xrTable_用法用量每日调整2_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整2_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell330.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell330.Dpi = 254F;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell330.StylePriority.UseBorders = false;
            this.xrTableCell330.StylePriority.UseTextAlignment = false;
            this.xrTableCell330.Text = "次";
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell330.Weight = 0.20118110356287111D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell331.Dpi = 254F;
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.StylePriority.UseBorders = false;
            this.xrTableCell331.StylePriority.UseTextAlignment = false;
            this.xrTableCell331.Text = "每次";
            this.xrTableCell331.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell331.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整2_3
            // 
            this.xrTable_用法用量每次调整2_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整2_3.CanGrow = false;
            this.xrTable_用法用量每次调整2_3.Dpi = 254F;
            this.xrTable_用法用量每次调整2_3.Name = "xrTable_用法用量每次调整2_3";
            this.xrTable_用法用量每次调整2_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整2_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell333.Dpi = 254F;
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBorders = false;
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "每日";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell333.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整2_4
            // 
            this.xrTable_用法用量每日调整2_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整2_4.CanGrow = false;
            this.xrTable_用法用量每日调整2_4.Dpi = 254F;
            this.xrTable_用法用量每日调整2_4.Name = "xrTable_用法用量每日调整2_4";
            this.xrTable_用法用量每日调整2_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整2_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell334.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell334.Dpi = 254F;
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.StylePriority.UseTextAlignment = false;
            this.xrTableCell334.Text = "次";
            this.xrTableCell334.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell334.Weight = 0.20118110356287111D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell335.Dpi = 254F;
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.StylePriority.UseTextAlignment = false;
            this.xrTableCell335.Text = "每次";
            this.xrTableCell335.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell335.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整2_4
            // 
            this.xrTable_用法用量每次调整2_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整2_4.CanGrow = false;
            this.xrTable_用法用量每次调整2_4.Dpi = 254F;
            this.xrTable_用法用量每次调整2_4.Name = "xrTable_用法用量每次调整2_4";
            this.xrTable_用法用量每次调整2_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整2_4.Weight = 0.58781160388997589D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTable_药物名称调整3_1,
            this.xrTable_药物名称调整3_2,
            this.xrTable_药物名称调整3_3,
            this.xrTable_药物名称调整3_4});
            this.xrTableRow35.Dpi = 254F;
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell230.Dpi = 254F;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Text = "整";
            this.xrTableCell230.Weight = 0.31666672042035415D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell231.Dpi = 254F;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.Text = "药物名称3";
            this.xrTableCell231.Weight = 1.0250000226216041D;
            // 
            // xrTable_药物名称调整3_1
            // 
            this.xrTable_药物名称调整3_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整3_1.CanGrow = false;
            this.xrTable_药物名称调整3_1.Dpi = 254F;
            this.xrTable_药物名称调整3_1.Name = "xrTable_药物名称调整3_1";
            this.xrTable_药物名称调整3_1.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整3_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整3_2
            // 
            this.xrTable_药物名称调整3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整3_2.CanGrow = false;
            this.xrTable_药物名称调整3_2.Dpi = 254F;
            this.xrTable_药物名称调整3_2.Name = "xrTable_药物名称调整3_2";
            this.xrTable_药物名称调整3_2.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整3_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整3_3
            // 
            this.xrTable_药物名称调整3_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整3_3.CanGrow = false;
            this.xrTable_药物名称调整3_3.Dpi = 254F;
            this.xrTable_药物名称调整3_3.Name = "xrTable_药物名称调整3_3";
            this.xrTable_药物名称调整3_3.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整3_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_药物名称调整3_4
            // 
            this.xrTable_药物名称调整3_4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable_药物名称调整3_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称调整3_4.CanGrow = false;
            this.xrTable_药物名称调整3_4.Dpi = 254F;
            this.xrTable_药物名称调整3_4.Name = "xrTable_药物名称调整3_4";
            this.xrTable_药物名称调整3_4.StylePriority.UseBorderDashStyle = false;
            this.xrTable_药物名称调整3_4.StylePriority.UseBorders = false;
            this.xrTable_药物名称调整3_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236,
            this.xrTableCell237,
            this.xrTableCell339,
            this.xrTable_用法用量每日调整3_1,
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTable_用法用量每次调整3_1,
            this.xrTableCell343,
            this.xrTable_用法用量每日调整3_2,
            this.xrTableCell341,
            this.xrTableCell342,
            this.xrTable_用法用量每次调整3_2,
            this.xrTableCell345,
            this.xrTable_用法用量每日调整3_3,
            this.xrTableCell346,
            this.xrTableCell347,
            this.xrTable_用法用量每次调整3_3,
            this.xrTableCell349,
            this.xrTable_用法用量每日调整3_4,
            this.xrTableCell350,
            this.xrTableCell351,
            this.xrTable_用法用量每次调整3_4});
            this.xrTableRow36.Dpi = 254F;
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell236.Dpi = 254F;
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.Text = "意";
            this.xrTableCell236.Weight = 0.31666672042035415D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell237.Dpi = 254F;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Text = "用法用量";
            this.xrTableCell237.Weight = 1.0250000226216041D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell339.Dpi = 254F;
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.StylePriority.UseTextAlignment = false;
            this.xrTableCell339.Text = "每日";
            this.xrTableCell339.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell339.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整3_1
            // 
            this.xrTable_用法用量每日调整3_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整3_1.CanGrow = false;
            this.xrTable_用法用量每日调整3_1.Dpi = 254F;
            this.xrTable_用法用量每日调整3_1.Name = "xrTable_用法用量每日调整3_1";
            this.xrTable_用法用量每日调整3_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整3_1.Weight = 0.24637793459040347D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell337.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell337.Dpi = 254F;
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell337.StylePriority.UseBorders = false;
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "次";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell337.Weight = 0.20118110356287111D;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell338.Dpi = 254F;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.StylePriority.UseTextAlignment = false;
            this.xrTableCell338.Text = "每次";
            this.xrTableCell338.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell338.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整3_1
            // 
            this.xrTable_用法用量每次调整3_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整3_1.CanGrow = false;
            this.xrTable_用法用量每次调整3_1.Dpi = 254F;
            this.xrTable_用法用量每次调整3_1.Name = "xrTable_用法用量每次调整3_1";
            this.xrTable_用法用量每次调整3_1.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整3_1.Weight = 0.58781160388997589D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell343.Dpi = 254F;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "每日";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell343.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整3_2
            // 
            this.xrTable_用法用量每日调整3_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整3_2.CanGrow = false;
            this.xrTable_用法用量每日调整3_2.Dpi = 254F;
            this.xrTable_用法用量每日调整3_2.Name = "xrTable_用法用量每日调整3_2";
            this.xrTable_用法用量每日调整3_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整3_2.Weight = 0.24637793459040347D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell341.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell341.Dpi = 254F;
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell341.StylePriority.UseBorders = false;
            this.xrTableCell341.StylePriority.UseTextAlignment = false;
            this.xrTableCell341.Text = "次";
            this.xrTableCell341.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell341.Weight = 0.20118110356287111D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell342.Dpi = 254F;
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.StylePriority.UseTextAlignment = false;
            this.xrTableCell342.Text = "每次";
            this.xrTableCell342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell342.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整3_2
            // 
            this.xrTable_用法用量每次调整3_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整3_2.CanGrow = false;
            this.xrTable_用法用量每次调整3_2.Dpi = 254F;
            this.xrTable_用法用量每次调整3_2.Name = "xrTable_用法用量每次调整3_2";
            this.xrTable_用法用量每次调整3_2.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整3_2.Weight = 0.58781160388997589D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell345.Dpi = 254F;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.StylePriority.UseTextAlignment = false;
            this.xrTableCell345.Text = "每日";
            this.xrTableCell345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell345.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整3_3
            // 
            this.xrTable_用法用量每日调整3_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整3_3.CanGrow = false;
            this.xrTable_用法用量每日调整3_3.Dpi = 254F;
            this.xrTable_用法用量每日调整3_3.Name = "xrTable_用法用量每日调整3_3";
            this.xrTable_用法用量每日调整3_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整3_3.Weight = 0.24637793459040347D;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell346.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell346.Dpi = 254F;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.StylePriority.UseTextAlignment = false;
            this.xrTableCell346.Text = "次";
            this.xrTableCell346.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell346.Weight = 0.20118110356287111D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell347.Dpi = 254F;
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.StylePriority.UseTextAlignment = false;
            this.xrTableCell347.Text = "每次";
            this.xrTableCell347.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell347.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整3_3
            // 
            this.xrTable_用法用量每次调整3_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整3_3.CanGrow = false;
            this.xrTable_用法用量每次调整3_3.Dpi = 254F;
            this.xrTable_用法用量每次调整3_3.Name = "xrTable_用法用量每次调整3_3";
            this.xrTable_用法用量每次调整3_3.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整3_3.Weight = 0.58781160388997589D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell349.Dpi = 254F;
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseBorders = false;
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.Text = "每日";
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell349.Weight = 0.344173190255436D;
            // 
            // xrTable_用法用量每日调整3_4
            // 
            this.xrTable_用法用量每日调整3_4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_用法用量每日调整3_4.CanGrow = false;
            this.xrTable_用法用量每日调整3_4.Dpi = 254F;
            this.xrTable_用法用量每日调整3_4.Name = "xrTable_用法用量每日调整3_4";
            this.xrTable_用法用量每日调整3_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每日调整3_4.Weight = 0.24637793459040347D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell350.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell350.Dpi = 254F;
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell350.StylePriority.UseBorders = false;
            this.xrTableCell350.StylePriority.UseTextAlignment = false;
            this.xrTableCell350.Text = "次";
            this.xrTableCell350.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell350.Weight = 0.20118110356287111D;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell351.Dpi = 254F;
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.StylePriority.UseTextAlignment = false;
            this.xrTableCell351.Text = "每次";
            this.xrTableCell351.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell351.Weight = 0.28700787312827492D;
            // 
            // xrTable_用法用量每次调整3_4
            // 
            this.xrTable_用法用量每次调整3_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用法用量每次调整3_4.CanGrow = false;
            this.xrTable_用法用量每次调整3_4.Dpi = 254F;
            this.xrTable_用法用量每次调整3_4.Name = "xrTable_用法用量每次调整3_4";
            this.xrTable_用法用量每次调整3_4.StylePriority.UseBorders = false;
            this.xrTable_用法用量每次调整3_4.Weight = 0.58781160388997589D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell428,
            this.xrTableCell429,
            this.xrTableCell430,
            this.xrTable_胰岛素种类调整_1,
            this.xrTableCell435,
            this.xrTable_胰岛素种类调整_2,
            this.xrTableCell440,
            this.xrTable_胰岛素种类调整_3,
            this.xrTableCell445,
            this.xrTable_胰岛素种类调整_4});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell428
            // 
            this.xrTableCell428.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell428.Dpi = 254F;
            this.xrTableCell428.Name = "xrTableCell428";
            this.xrTableCell428.StylePriority.UseBorders = false;
            this.xrTableCell428.Text = "见";
            this.xrTableCell428.Weight = 0.31666672042035415D;
            // 
            // xrTableCell429
            // 
            this.xrTableCell429.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell429.Dpi = 254F;
            this.xrTableCell429.Name = "xrTableCell429";
            this.xrTableCell429.StylePriority.UseBorders = false;
            this.xrTableCell429.Text = "胰岛素";
            this.xrTableCell429.Weight = 1.0250000226216041D;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell430.Dpi = 254F;
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.StylePriority.UseBorders = false;
            this.xrTableCell430.StylePriority.UseTextAlignment = false;
            this.xrTableCell430.Text = "  种类：";
            this.xrTableCell430.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell430.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类调整_1
            // 
            this.xrTable_胰岛素种类调整_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类调整_1.CanGrow = false;
            this.xrTable_胰岛素种类调整_1.Dpi = 254F;
            this.xrTable_胰岛素种类调整_1.Name = "xrTable_胰岛素种类调整_1";
            this.xrTable_胰岛素种类调整_1.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类调整_1.Weight = 1.2767879593491769D;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell435.Dpi = 254F;
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.StylePriority.UseBorders = false;
            this.xrTableCell435.StylePriority.UseTextAlignment = false;
            this.xrTableCell435.Text = "  种类：";
            this.xrTableCell435.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell435.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类调整_2
            // 
            this.xrTable_胰岛素种类调整_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类调整_2.CanGrow = false;
            this.xrTable_胰岛素种类调整_2.Dpi = 254F;
            this.xrTable_胰岛素种类调整_2.Name = "xrTable_胰岛素种类调整_2";
            this.xrTable_胰岛素种类调整_2.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类调整_2.Weight = 1.2767879593491769D;
            // 
            // xrTableCell440
            // 
            this.xrTableCell440.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell440.Dpi = 254F;
            this.xrTableCell440.Name = "xrTableCell440";
            this.xrTableCell440.StylePriority.UseBorders = false;
            this.xrTableCell440.StylePriority.UseTextAlignment = false;
            this.xrTableCell440.Text = "  种类：";
            this.xrTableCell440.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell440.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类调整_3
            // 
            this.xrTable_胰岛素种类调整_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类调整_3.CanGrow = false;
            this.xrTable_胰岛素种类调整_3.Dpi = 254F;
            this.xrTable_胰岛素种类调整_3.Name = "xrTable_胰岛素种类调整_3";
            this.xrTable_胰岛素种类调整_3.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类调整_3.Weight = 1.2767879593491769D;
            // 
            // xrTableCell445
            // 
            this.xrTableCell445.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell445.Dpi = 254F;
            this.xrTableCell445.Name = "xrTableCell445";
            this.xrTableCell445.StylePriority.UseBorders = false;
            this.xrTableCell445.StylePriority.UseTextAlignment = false;
            this.xrTableCell445.Text = "  种类：";
            this.xrTableCell445.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell445.Weight = 0.38976374607778469D;
            // 
            // xrTable_胰岛素种类调整_4
            // 
            this.xrTable_胰岛素种类调整_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素种类调整_4.CanGrow = false;
            this.xrTable_胰岛素种类调整_4.Dpi = 254F;
            this.xrTable_胰岛素种类调整_4.Name = "xrTable_胰岛素种类调整_4";
            this.xrTable_胰岛素种类调整_4.StylePriority.UseBorders = false;
            this.xrTable_胰岛素种类调整_4.Weight = 1.2767879593491769D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell366,
            this.xrTableCell451,
            this.xrTableCell452,
            this.xrTable_胰岛素用法用量调整_1,
            this.xrTableCell457,
            this.xrTable_胰岛素用法用量调整_2,
            this.xrTableCell462,
            this.xrTable_胰岛素用法用量调整_3,
            this.xrTableCell467,
            this.xrTable_胰岛素用法用量调整_4});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell366.Dpi = 254F;
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.StylePriority.UseBorders = false;
            this.xrTableCell366.Weight = 0.31666632430761876D;
            // 
            // xrTableCell451
            // 
            this.xrTableCell451.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell451.Dpi = 254F;
            this.xrTableCell451.Name = "xrTableCell451";
            this.xrTableCell451.StylePriority.UseBorders = false;
            this.xrTableCell451.Weight = 1.0250004187343396D;
            // 
            // xrTableCell452
            // 
            this.xrTableCell452.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell452.Dpi = 254F;
            this.xrTableCell452.Name = "xrTableCell452";
            this.xrTableCell452.StylePriority.UseBorders = false;
            this.xrTableCell452.StylePriority.UseTextAlignment = false;
            this.xrTableCell452.Text = "  用法和用量：";
            this.xrTableCell452.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell452.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量调整_1
            // 
            this.xrTable_胰岛素用法用量调整_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量调整_1.CanGrow = false;
            this.xrTable_胰岛素用法用量调整_1.Dpi = 254F;
            this.xrTable_胰岛素用法用量调整_1.Name = "xrTable_胰岛素用法用量调整_1";
            this.xrTable_胰岛素用法用量调整_1.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量调整_1.Weight = 0.95001632420943549D;
            // 
            // xrTableCell457
            // 
            this.xrTableCell457.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell457.Dpi = 254F;
            this.xrTableCell457.Name = "xrTableCell457";
            this.xrTableCell457.StylePriority.UseBorders = false;
            this.xrTableCell457.StylePriority.UseTextAlignment = false;
            this.xrTableCell457.Text = "  用法和用量：";
            this.xrTableCell457.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell457.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量调整_2
            // 
            this.xrTable_胰岛素用法用量调整_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量调整_2.CanGrow = false;
            this.xrTable_胰岛素用法用量调整_2.Dpi = 254F;
            this.xrTable_胰岛素用法用量调整_2.Name = "xrTable_胰岛素用法用量调整_2";
            this.xrTable_胰岛素用法用量调整_2.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量调整_2.Weight = 0.95001632420943549D;
            // 
            // xrTableCell462
            // 
            this.xrTableCell462.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell462.Dpi = 254F;
            this.xrTableCell462.Name = "xrTableCell462";
            this.xrTableCell462.StylePriority.UseBorders = false;
            this.xrTableCell462.StylePriority.UseTextAlignment = false;
            this.xrTableCell462.Text = "  用法和用量：";
            this.xrTableCell462.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell462.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量调整_3
            // 
            this.xrTable_胰岛素用法用量调整_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量调整_3.CanGrow = false;
            this.xrTable_胰岛素用法用量调整_3.Dpi = 254F;
            this.xrTable_胰岛素用法用量调整_3.Name = "xrTable_胰岛素用法用量调整_3";
            this.xrTable_胰岛素用法用量调整_3.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量调整_3.Weight = 0.95001632420943549D;
            // 
            // xrTableCell467
            // 
            this.xrTableCell467.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell467.Dpi = 254F;
            this.xrTableCell467.Name = "xrTableCell467";
            this.xrTableCell467.StylePriority.UseBorders = false;
            this.xrTableCell467.StylePriority.UseTextAlignment = false;
            this.xrTableCell467.Text = "  用法和用量：";
            this.xrTableCell467.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell467.Weight = 0.71653538121752591D;
            // 
            // xrTable_胰岛素用法用量调整_4
            // 
            this.xrTable_胰岛素用法用量调整_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_胰岛素用法用量调整_4.CanGrow = false;
            this.xrTable_胰岛素用法用量调整_4.Dpi = 254F;
            this.xrTable_胰岛素用法用量调整_4.Name = "xrTable_胰岛素用法用量调整_4";
            this.xrTable_胰岛素用法用量调整_4.StylePriority.UseBorders = false;
            this.xrTable_胰岛素用法用量调整_4.Weight = 0.95001632420943549D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTable_转诊原因_1,
            this.xrTable_转诊原因_2,
            this.xrTable_转诊原因_3,
            this.xrTable_转诊原因_4});
            this.xrTableRow37.Dpi = 254F;
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell242.Dpi = 254F;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Weight = 0.31666672042035415D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell243.Dpi = 254F;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.Text = "原    因";
            this.xrTableCell243.Weight = 1.0250000226216041D;
            // 
            // xrTable_转诊原因_1
            // 
            this.xrTable_转诊原因_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊原因_1.CanGrow = false;
            this.xrTable_转诊原因_1.Dpi = 254F;
            this.xrTable_转诊原因_1.Name = "xrTable_转诊原因_1";
            this.xrTable_转诊原因_1.StylePriority.UseBorders = false;
            this.xrTable_转诊原因_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊原因_2
            // 
            this.xrTable_转诊原因_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊原因_2.CanGrow = false;
            this.xrTable_转诊原因_2.Dpi = 254F;
            this.xrTable_转诊原因_2.Name = "xrTable_转诊原因_2";
            this.xrTable_转诊原因_2.StylePriority.UseBorders = false;
            this.xrTable_转诊原因_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊原因_3
            // 
            this.xrTable_转诊原因_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊原因_3.CanGrow = false;
            this.xrTable_转诊原因_3.Dpi = 254F;
            this.xrTable_转诊原因_3.Name = "xrTable_转诊原因_3";
            this.xrTable_转诊原因_3.StylePriority.UseBorders = false;
            this.xrTable_转诊原因_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊原因_4
            // 
            this.xrTable_转诊原因_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊原因_4.CanGrow = false;
            this.xrTable_转诊原因_4.Dpi = 254F;
            this.xrTable_转诊原因_4.Name = "xrTable_转诊原因_4";
            this.xrTable_转诊原因_4.StylePriority.UseBorders = false;
            this.xrTable_转诊原因_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTable_转诊机构_1,
            this.xrTable_转诊机构_2,
            this.xrTable_转诊机构_3,
            this.xrTable_转诊机构_4});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell248.Dpi = 254F;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Text = "转";
            this.xrTableCell248.Weight = 0.31666672042035415D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Dpi = 254F;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Text = "机构及科别";
            this.xrTableCell249.Weight = 1.0250000226216041D;
            // 
            // xrTable_转诊机构_1
            // 
            this.xrTable_转诊机构_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊机构_1.CanGrow = false;
            this.xrTable_转诊机构_1.Dpi = 254F;
            this.xrTable_转诊机构_1.Name = "xrTable_转诊机构_1";
            this.xrTable_转诊机构_1.StylePriority.UseBorders = false;
            this.xrTable_转诊机构_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊机构_2
            // 
            this.xrTable_转诊机构_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊机构_2.CanGrow = false;
            this.xrTable_转诊机构_2.Dpi = 254F;
            this.xrTable_转诊机构_2.Name = "xrTable_转诊机构_2";
            this.xrTable_转诊机构_2.StylePriority.UseBorders = false;
            this.xrTable_转诊机构_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊机构_3
            // 
            this.xrTable_转诊机构_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊机构_3.CanGrow = false;
            this.xrTable_转诊机构_3.Dpi = 254F;
            this.xrTable_转诊机构_3.Name = "xrTable_转诊机构_3";
            this.xrTable_转诊机构_3.StylePriority.UseBorders = false;
            this.xrTable_转诊机构_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊机构_4
            // 
            this.xrTable_转诊机构_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊机构_4.CanGrow = false;
            this.xrTable_转诊机构_4.Dpi = 254F;
            this.xrTable_转诊机构_4.Name = "xrTable_转诊机构_4";
            this.xrTable_转诊机构_4.StylePriority.UseBorders = false;
            this.xrTable_转诊机构_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTable_转诊联系人_1,
            this.xrTable_转诊联系人_2,
            this.xrTable_转诊联系人_3,
            this.xrTable_转诊联系人_4});
            this.xrTableRow39.Dpi = 254F;
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell254.Dpi = 254F;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Text = "诊";
            this.xrTableCell254.Weight = 0.31666672042035415D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell255.Dpi = 254F;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Text = "联系人及电话";
            this.xrTableCell255.Weight = 1.0250000226216041D;
            // 
            // xrTable_转诊联系人_1
            // 
            this.xrTable_转诊联系人_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊联系人_1.CanGrow = false;
            this.xrTable_转诊联系人_1.Dpi = 254F;
            this.xrTable_转诊联系人_1.Name = "xrTable_转诊联系人_1";
            this.xrTable_转诊联系人_1.StylePriority.UseBorders = false;
            this.xrTable_转诊联系人_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊联系人_2
            // 
            this.xrTable_转诊联系人_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊联系人_2.CanGrow = false;
            this.xrTable_转诊联系人_2.Dpi = 254F;
            this.xrTable_转诊联系人_2.Name = "xrTable_转诊联系人_2";
            this.xrTable_转诊联系人_2.StylePriority.UseBorders = false;
            this.xrTable_转诊联系人_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊联系人_3
            // 
            this.xrTable_转诊联系人_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊联系人_3.CanGrow = false;
            this.xrTable_转诊联系人_3.Dpi = 254F;
            this.xrTable_转诊联系人_3.Name = "xrTable_转诊联系人_3";
            this.xrTable_转诊联系人_3.StylePriority.UseBorders = false;
            this.xrTable_转诊联系人_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_转诊联系人_4
            // 
            this.xrTable_转诊联系人_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_转诊联系人_4.CanGrow = false;
            this.xrTable_转诊联系人_4.Dpi = 254F;
            this.xrTable_转诊联系人_4.Name = "xrTable_转诊联系人_4";
            this.xrTable_转诊联系人_4.StylePriority.UseBorders = false;
            this.xrTable_转诊联系人_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell260,
            this.xrTableCell261,
            this.xrTableCell352,
            this.xrTableCell262,
            this.xrTableCell353,
            this.xrTableCell263,
            this.xrTableCell354,
            this.xrTableCell264,
            this.xrTableCell355,
            this.xrTableCell265});
            this.xrTableRow40.Dpi = 254F;
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell260.Dpi = 254F;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.Weight = 0.31666672042035415D;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell261.Dpi = 254F;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.Text = "结    果";
            this.xrTableCell261.Weight = 1.0250000226216041D;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell352.Dpi = 254F;
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.StylePriority.UseBorders = false;
            this.xrTableCell352.Text = "1 到位    2 不到位";
            this.xrTableCell352.Weight = 1.3877951744337624D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell262.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_转诊结果_1});
            this.xrTableCell262.Dpi = 254F;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Weight = 0.27875653099319897D;
            // 
            // xrLabel_转诊结果_1
            // 
            this.xrLabel_转诊结果_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_转诊结果_1.CanGrow = false;
            this.xrLabel_转诊结果_1.Dpi = 254F;
            this.xrLabel_转诊结果_1.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_转诊结果_1.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_转诊结果_1.Name = "xrLabel_转诊结果_1";
            this.xrLabel_转诊结果_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_转诊结果_1.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_转诊结果_1.StylePriority.UseBorders = false;
            this.xrLabel_转诊结果_1.StylePriority.UseFont = false;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell353.Dpi = 254F;
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.Text = "1 到位    2 不到位";
            this.xrTableCell353.Weight = 1.3877951744337624D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell263.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_转诊结果_2});
            this.xrTableCell263.Dpi = 254F;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.27875653099319897D;
            // 
            // xrLabel_转诊结果_2
            // 
            this.xrLabel_转诊结果_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_转诊结果_2.CanGrow = false;
            this.xrLabel_转诊结果_2.Dpi = 254F;
            this.xrLabel_转诊结果_2.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_转诊结果_2.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_转诊结果_2.Name = "xrLabel_转诊结果_2";
            this.xrLabel_转诊结果_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_转诊结果_2.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_转诊结果_2.StylePriority.UseBorders = false;
            this.xrLabel_转诊结果_2.StylePriority.UseFont = false;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell354.Dpi = 254F;
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.Text = "1 到位    2 不到位";
            this.xrTableCell354.Weight = 1.3877951744337624D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell264.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_转诊结果_3});
            this.xrTableCell264.Dpi = 254F;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.Weight = 0.27875653099319897D;
            // 
            // xrLabel_转诊结果_3
            // 
            this.xrLabel_转诊结果_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_转诊结果_3.CanGrow = false;
            this.xrLabel_转诊结果_3.Dpi = 254F;
            this.xrLabel_转诊结果_3.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_转诊结果_3.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_转诊结果_3.Name = "xrLabel_转诊结果_3";
            this.xrLabel_转诊结果_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_转诊结果_3.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_转诊结果_3.StylePriority.UseBorders = false;
            this.xrLabel_转诊结果_3.StylePriority.UseFont = false;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell355.Dpi = 254F;
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseBorders = false;
            this.xrTableCell355.Text = "1 到位    2 不到位";
            this.xrTableCell355.Weight = 1.3877951744337624D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell265.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_转诊结果_4});
            this.xrTableCell265.Dpi = 254F;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Weight = 0.27875653099319897D;
            // 
            // xrLabel_转诊结果_4
            // 
            this.xrLabel_转诊结果_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_转诊结果_4.CanGrow = false;
            this.xrLabel_转诊结果_4.Dpi = 254F;
            this.xrLabel_转诊结果_4.Font = new System.Drawing.Font("微软雅黑", 6.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_转诊结果_4.LocationFloat = new DevExpress.Utils.PointFloat(15F, 6F);
            this.xrLabel_转诊结果_4.Name = "xrLabel_转诊结果_4";
            this.xrLabel_转诊结果_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_转诊结果_4.SizeF = new System.Drawing.SizeF(36F, 36F);
            this.xrLabel_转诊结果_4.StylePriority.UseBorders = false;
            this.xrLabel_转诊结果_4.StylePriority.UseFont = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTable_下次随访日期_1,
            this.xrTable_下次随访日期_2,
            this.xrTable_下次随访日期_3,
            this.xrTable_下次随访日期_4});
            this.xrTableRow41.Dpi = 254F;
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell267.Dpi = 254F;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Text = "下次随访日期";
            this.xrTableCell267.Weight = 1.3416667430419582D;
            // 
            // xrTable_下次随访日期_1
            // 
            this.xrTable_下次随访日期_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_下次随访日期_1.CanGrow = false;
            this.xrTable_下次随访日期_1.Dpi = 254F;
            this.xrTable_下次随访日期_1.Name = "xrTable_下次随访日期_1";
            this.xrTable_下次随访日期_1.StylePriority.UseBorders = false;
            this.xrTable_下次随访日期_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_下次随访日期_2
            // 
            this.xrTable_下次随访日期_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_下次随访日期_2.CanGrow = false;
            this.xrTable_下次随访日期_2.Dpi = 254F;
            this.xrTable_下次随访日期_2.Name = "xrTable_下次随访日期_2";
            this.xrTable_下次随访日期_2.StylePriority.UseBorders = false;
            this.xrTable_下次随访日期_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_下次随访日期_3
            // 
            this.xrTable_下次随访日期_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_下次随访日期_3.CanGrow = false;
            this.xrTable_下次随访日期_3.Dpi = 254F;
            this.xrTable_下次随访日期_3.Name = "xrTable_下次随访日期_3";
            this.xrTable_下次随访日期_3.StylePriority.UseBorders = false;
            this.xrTable_下次随访日期_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_下次随访日期_4
            // 
            this.xrTable_下次随访日期_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_下次随访日期_4.CanGrow = false;
            this.xrTable_下次随访日期_4.Dpi = 254F;
            this.xrTable_下次随访日期_4.Name = "xrTable_下次随访日期_4";
            this.xrTable_下次随访日期_4.StylePriority.UseBorders = false;
            this.xrTable_下次随访日期_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell273,
            this.xrTable_随访医生签字_1,
            this.xrTable_随访医生签字_2,
            this.xrTable_随访医生签字_3,
            this.xrTable_随访医生签字_4});
            this.xrTableRow42.Dpi = 254F;
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell273.Dpi = 254F;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.Text = "随访医生签字";
            this.xrTableCell273.Weight = 1.3416667430419582D;
            // 
            // xrTable_随访医生签字_1
            // 
            this.xrTable_随访医生签字_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访医生签字_1.CanGrow = false;
            this.xrTable_随访医生签字_1.Dpi = 254F;
            this.xrTable_随访医生签字_1.Name = "xrTable_随访医生签字_1";
            this.xrTable_随访医生签字_1.StylePriority.UseBorders = false;
            this.xrTable_随访医生签字_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_随访医生签字_2
            // 
            this.xrTable_随访医生签字_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访医生签字_2.CanGrow = false;
            this.xrTable_随访医生签字_2.Dpi = 254F;
            this.xrTable_随访医生签字_2.Name = "xrTable_随访医生签字_2";
            this.xrTable_随访医生签字_2.StylePriority.UseBorders = false;
            this.xrTable_随访医生签字_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_随访医生签字_3
            // 
            this.xrTable_随访医生签字_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访医生签字_3.CanGrow = false;
            this.xrTable_随访医生签字_3.Dpi = 254F;
            this.xrTable_随访医生签字_3.Name = "xrTable_随访医生签字_3";
            this.xrTable_随访医生签字_3.StylePriority.UseBorders = false;
            this.xrTable_随访医生签字_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_随访医生签字_4
            // 
            this.xrTable_随访医生签字_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_随访医生签字_4.CanGrow = false;
            this.xrTable_随访医生签字_4.Dpi = 254F;
            this.xrTable_随访医生签字_4.Name = "xrTable_随访医生签字_4";
            this.xrTable_随访医生签字_4.StylePriority.UseBorders = false;
            this.xrTable_随访医生签字_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118,
            this.xrTable_居民签字_1,
            this.xrTable_居民签字_2,
            this.xrTable_居民签字_3,
            this.xrTable_居民签字_4});
            this.xrTableRow43.Dpi = 254F;
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "居民签字";
            this.xrTableCell118.Weight = 1.3416667430419582D;
            // 
            // xrTable_居民签字_1
            // 
            this.xrTable_居民签字_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_居民签字_1.CanGrow = false;
            this.xrTable_居民签字_1.Dpi = 254F;
            this.xrTable_居民签字_1.Name = "xrTable_居民签字_1";
            this.xrTable_居民签字_1.StylePriority.UseBorders = false;
            this.xrTable_居民签字_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_居民签字_2
            // 
            this.xrTable_居民签字_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_居民签字_2.CanGrow = false;
            this.xrTable_居民签字_2.Dpi = 254F;
            this.xrTable_居民签字_2.Name = "xrTable_居民签字_2";
            this.xrTable_居民签字_2.StylePriority.UseBorders = false;
            this.xrTable_居民签字_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_居民签字_3
            // 
            this.xrTable_居民签字_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_居民签字_3.CanGrow = false;
            this.xrTable_居民签字_3.Dpi = 254F;
            this.xrTable_居民签字_3.Name = "xrTable_居民签字_3";
            this.xrTable_居民签字_3.StylePriority.UseBorders = false;
            this.xrTable_居民签字_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_居民签字_4
            // 
            this.xrTable_居民签字_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_居民签字_4.CanGrow = false;
            this.xrTable_居民签字_4.Dpi = 254F;
            this.xrTable_居民签字_4.Name = "xrTable_居民签字_4";
            this.xrTable_居民签字_4.StylePriority.UseBorders = false;
            this.xrTable_居民签字_4.Weight = 1.6665517054269614D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTable_备注_1,
            this.xrTable_备注_2,
            this.xrTable_备注_3,
            this.xrTable_备注_4});
            this.xrTableRow44.Dpi = 254F;
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Text = "备    注";
            this.xrTableCell126.Weight = 1.3416667430419582D;
            // 
            // xrTable_备注_1
            // 
            this.xrTable_备注_1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_备注_1.CanGrow = false;
            this.xrTable_备注_1.Dpi = 254F;
            this.xrTable_备注_1.Name = "xrTable_备注_1";
            this.xrTable_备注_1.StylePriority.UseBorders = false;
            this.xrTable_备注_1.Weight = 1.6665517054269614D;
            // 
            // xrTable_备注_2
            // 
            this.xrTable_备注_2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_备注_2.CanGrow = false;
            this.xrTable_备注_2.Dpi = 254F;
            this.xrTable_备注_2.Name = "xrTable_备注_2";
            this.xrTable_备注_2.StylePriority.UseBorders = false;
            this.xrTable_备注_2.Weight = 1.6665517054269614D;
            // 
            // xrTable_备注_3
            // 
            this.xrTable_备注_3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_备注_3.CanGrow = false;
            this.xrTable_备注_3.Dpi = 254F;
            this.xrTable_备注_3.Name = "xrTable_备注_3";
            this.xrTable_备注_3.StylePriority.UseBorders = false;
            this.xrTable_备注_3.Weight = 1.6665517054269614D;
            // 
            // xrTable_备注_4
            // 
            this.xrTable_备注_4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_备注_4.CanGrow = false;
            this.xrTable_备注_4.Dpi = 254F;
            this.xrTable_备注_4.Name = "xrTable_备注_4";
            this.xrTable_备注_4.StylePriority.UseBorders = false;
            this.xrTable_备注_4.Weight = 1.6665517054269614D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report2型糖尿病患者随访服务记录表_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(28, 33, 0, 0);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_年_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_月_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_日_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_年_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_月_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_日_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_年_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_月_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_日_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_年_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_月_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访日期_日_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_随访方式_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_随访方式_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_随访方式_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_随访方式_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_1_8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_2_8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_3_8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状_4_8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_4_1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状其他_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体质指数_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_心率_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_心率_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_心率_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_心率_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体征其他_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体征其他_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体征其他_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体征其他_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日吸烟量_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_日饮酒量_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动频率_4_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_运动时长_4_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_摄盐情况_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_摄盐情况_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_摄盐情况_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_摄盐情况_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_4_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心理调整_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心理调整_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心理调整_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心理调整_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_遵医行为_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_遵医行为_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_遵医行为_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_遵医行为_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前1_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前1_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前1_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前2_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前2_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前2_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称目前3_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日目前3_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次目前3_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_服药依从性_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_服药依从性_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_服药依从性_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_服药依从性_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_此次随访分类_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_此次随访分类_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_此次随访分类_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_此次随访分类_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下一步管理措施_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下一步管理措施_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下一步管理措施_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下一步管理措施_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整1_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整1_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整1_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整1_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整2_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整2_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整2_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整2_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称调整3_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整3_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每日调整3_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用法用量每次调整3_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊原因_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊原因_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊原因_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊原因_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊机构_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊机构_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊机构_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊机构_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊联系人_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊联系人_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊联系人_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_转诊联系人_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_转诊结果_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_转诊结果_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_转诊结果_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_转诊结果_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_下次随访日期_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_下次随访日期_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_下次随访日期_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_下次随访日期_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访医生签字_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访医生签字_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访医生签字_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_随访医生签字_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_居民签字_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_居民签字_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_居民签字_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_居民签字_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_备注_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_备注_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_备注_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_备注_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号4;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱双侧_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱左侧_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱右侧_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱双侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱左侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱右侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱双侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱左侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱右侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱双侧_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱左侧_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动减弱右侧_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失双侧_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失左侧_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失右侧_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失双侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失左侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失右侧_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失双侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失左侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失右侧_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失双侧_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失左侧_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动消失右侧_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_主食_1_2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_其他检查其他_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_其他检查其他_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_其他检查其他_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_其他检查其他_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_空腹血糖_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_空腹血糖_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_空腹血糖_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_空腹血糖_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_糖化血红蛋白_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_糖化血红蛋白_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_糖化血红蛋白_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_糖化血红蛋白_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期月_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期日_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期月_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期日_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期月_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期日_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期月_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_检查日期日_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类目前_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类目前_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类目前_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类目前_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量目前_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量目前_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量目前_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量目前_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell429;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类调整_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell440;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类调整_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell445;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类调整_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell451;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell452;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量调整_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell457;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量调整_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell462;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量调整_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell467;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素用法用量调整_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_胰岛素种类调整_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物不良反应有_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物不良反应有_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物不良反应有_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物不良反应有_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell411;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_药物不良反应_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_药物不良反应_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_药物不良反应_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_药物不良反应_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_低血糖反应_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_低血糖反应_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_低血糖反应_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_低血糖反应_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_血压_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_血压_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_血压_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_血压_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
    }
}
