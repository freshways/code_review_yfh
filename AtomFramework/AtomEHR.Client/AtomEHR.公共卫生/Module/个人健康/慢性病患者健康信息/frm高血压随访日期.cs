﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;


namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class frm高血压随访日期 : Form
    {
        #region Fields
        private string year;
        private string docNo;
        DataSet _ds高血压;
        bllMXB高血压随访表 _bll高血压 = new bllMXB高血压随访表();
        #endregion

        public frm高血压随访日期(string docNo, string year)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.year = year;
            _ds高血压 = _bll高血压.GetInfoByDate(docNo, year, true);
            DoBindingDataSource(_ds高血压);
        }

        private void DoBindingDataSource(DataSet _ds高血压)
        {
            DataTable dt高血压 = _ds高血压.Tables[Models.tb_MXB高血压随访表.__TableName];
            if (dt高血压 != null && dt高血压.Rows.Count > 0)
            {
                gc高血压随访.DataSource = _ds高血压.Tables[0];
                this.gv高血压随访.BestFitColumns();
                this.gc高血压随访.BringToFront();
                //在表中增加一个列 add（列名，列类型）  
                dt高血压.Columns.Add("check", typeof(bool));
                dt高血压.Columns.Add("col随访日期", typeof(string));
                //给每一行的该列赋值  
                for (int i = 0; i < dt高血压.Rows.Count; i++)
                {
                    dt高血压.Rows[i]["check"] = "false";
                    dt高血压.Rows[i]["col随访日期"] = dt高血压.Rows[i][Models.tb_MXB高血压随访表.发生时间].ToString();
                }

                string value = "";
                string strSelected = "";
                for (int i = 0; i < gv高血压随访.RowCount; i++)
                {
                    //获取选中行的check值
                    value = gv高血压随访.GetDataRow(i)["check"].ToString();
                    if (value == "true")
                    {
                        strSelected += gv高血压随访.GetRowCellValue(i, "随访日期");
                    }
                }
            }
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (gv高血压随访.SelectedRowsCount == 0)
            {
                MessageBox.Show("请选择随访日期！", "错误提示");
            }
            else if (gv高血压随访.SelectedRowsCount < 5)
            {
                int[] ints = this.gv高血压随访.GetSelectedRows();
                List<string> list = new List<string>();
                for (int i = 0; i < ints.Length; i++)
                {
                    DataRowView row = (DataRowView)this.gv高血压随访.GetRow(ints[i]);
                    string selectDate;
                    if (row != null)
                    {
                        selectDate = row["发生时间"].ToString();
                        list.Add(selectDate);
                    }
                }
                //report高血压患者随访服务记录表 report= new report高血压患者随访服务记录表(docNo, list);
                report高血压患者随访服务记录表_2017 report = new report高血压患者随访服务记录表_2017(docNo, list);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("最多选择4次随访记录！","错误提示");
            }

        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();//关闭窗体
        }
    }
}
