﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC糖尿病患者管理卡
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC糖尿病患者管理卡));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn修改保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txt胰岛素用量 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio胰岛素 = new DevExpress.XtraEditors.RadioGroup();
            this.Layout1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt管理卡编号 = new DevExpress.XtraEditors.TextEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.txt餐后血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.radio终止理由 = new DevExpress.XtraEditors.RadioGroup();
            this.txt终止管理日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio终止管理 = new DevExpress.XtraEditors.RadioGroup();
            this.txt胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt甘油三脂 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt低密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt高密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txtBMI = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio生活自理能力 = new DevExpress.XtraEditors.RadioGroup();
            this.radio体育锻炼 = new DevExpress.XtraEditors.RadioGroup();
            this.radio饮酒情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radio吸烟情况 = new DevExpress.XtraEditors.RadioGroup();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.ck无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.ck症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt目前症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.radio病例来源 = new DevExpress.XtraEditors.RadioGroup();
            this.radio管理组别 = new DevExpress.XtraEditors.RadioGroup();
            this.fl家族史 = new System.Windows.Forms.FlowLayoutPanel();
            this.jzs_ck高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck以上都无 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck不详 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck拒答 = new DevExpress.XtraEditors.CheckEdit();
            this.radio降糖药 = new DevExpress.XtraEditors.RadioGroup();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt确诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt确诊单位 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit并发症情况 = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txt脑血管 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.txt足部 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.txt视网膜 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.txt心脏 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.txt神经 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.txt肾脏 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.radio尿微量蛋白 = new DevExpress.XtraEditors.RadioGroup();
            this.txt糖化血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt糖尿病类型 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout糖尿病并发症 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout终止管理 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout终止理由 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout用药列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio胰岛素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).BeginInit();
            this.Layout1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止理由.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio体育锻炼.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio病例来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理组别.Properties)).BeginInit();
            this.fl家族史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck以上都无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck不详.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck拒答.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio降糖药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit并发症情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio尿微量蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖尿病类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout糖尿病并发症)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout终止管理)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout终止理由)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout用药列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn修改保存);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(733, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn修改保存
            // 
            this.btn修改保存.Image = ((System.Drawing.Image)(resources.GetObject("btn修改保存.Image")));
            this.btn修改保存.Location = new System.Drawing.Point(3, 3);
            this.btn修改保存.Name = "btn修改保存";
            this.btn修改保存.Size = new System.Drawing.Size(75, 23);
            this.btn修改保存.TabIndex = 0;
            this.btn修改保存.Text = "修改保存";
            this.btn修改保存.Click += new System.EventHandler(this.btn修改保存_Click);
            // 
            // txt胰岛素用量
            // 
            this.txt胰岛素用量.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt胰岛素用量.Lbl1Text = "pmol/L";
            this.txt胰岛素用量.Location = new System.Drawing.Point(393, 414);
            this.txt胰岛素用量.Margin = new System.Windows.Forms.Padding(0);
            this.txt胰岛素用量.Name = "txt胰岛素用量";
            this.txt胰岛素用量.Size = new System.Drawing.Size(315, 25);
            this.txt胰岛素用量.TabIndex = 104;
            this.txt胰岛素用量.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // radio胰岛素
            // 
            this.radio胰岛素.EditValue = "2";
            this.radio胰岛素.Location = new System.Drawing.Point(103, 414);
            this.radio胰岛素.Margin = new System.Windows.Forms.Padding(0);
            this.radio胰岛素.Name = "radio胰岛素";
            this.radio胰岛素.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio胰岛素.Size = new System.Drawing.Size(221, 25);
            this.radio胰岛素.StyleController = this.Layout1;
            this.radio胰岛素.TabIndex = 102;
            this.radio胰岛素.SelectedIndexChanged += new System.EventHandler(this.radio胰岛素_SelectedIndexChanged);
            // 
            // Layout1
            // 
            this.Layout1.Controls.Add(this.txt联系电话);
            this.Layout1.Controls.Add(this.txt职业);
            this.Layout1.Controls.Add(this.txt身份证号);
            this.Layout1.Controls.Add(this.txt出生日期);
            this.Layout1.Controls.Add(this.txt性别);
            this.Layout1.Controls.Add(this.txt姓名);
            this.Layout1.Controls.Add(this.txt个人档案编号);
            this.Layout1.Controls.Add(this.txt管理卡编号);
            this.Layout1.Controls.Add(this.gcDetail);
            this.Layout1.Controls.Add(this.btn删除药物);
            this.Layout1.Controls.Add(this.btn添加药物);
            this.Layout1.Controls.Add(this.lab创建人);
            this.Layout1.Controls.Add(this.lab当前所属机构);
            this.Layout1.Controls.Add(this.txt胰岛素用量);
            this.Layout1.Controls.Add(this.radio胰岛素);
            this.Layout1.Controls.Add(this.txt餐后血糖);
            this.Layout1.Controls.Add(this.txt发生时间);
            this.Layout1.Controls.Add(this.radio终止理由);
            this.Layout1.Controls.Add(this.txt终止管理日期);
            this.Layout1.Controls.Add(this.radio终止管理);
            this.Layout1.Controls.Add(this.txt胆固醇);
            this.Layout1.Controls.Add(this.txt甘油三脂);
            this.Layout1.Controls.Add(this.txt低密度蛋白);
            this.Layout1.Controls.Add(this.txt高密度蛋白);
            this.Layout1.Controls.Add(this.txt空腹血糖);
            this.Layout1.Controls.Add(this.txt腰围);
            this.Layout1.Controls.Add(this.txtBMI);
            this.Layout1.Controls.Add(this.txt体重);
            this.Layout1.Controls.Add(this.radio生活自理能力);
            this.Layout1.Controls.Add(this.radio体育锻炼);
            this.Layout1.Controls.Add(this.radio饮酒情况);
            this.Layout1.Controls.Add(this.radio吸烟情况);
            this.Layout1.Controls.Add(this.fl症状);
            this.Layout1.Controls.Add(this.radio病例来源);
            this.Layout1.Controls.Add(this.radio管理组别);
            this.Layout1.Controls.Add(this.fl家族史);
            this.Layout1.Controls.Add(this.radio降糖药);
            this.Layout1.Controls.Add(this.txt身高);
            this.Layout1.Controls.Add(this.txt血压值);
            this.Layout1.Controls.Add(this.txt确诊时间);
            this.Layout1.Controls.Add(this.txt确诊单位);
            this.Layout1.Controls.Add(this.checkEdit并发症情况);
            this.Layout1.Controls.Add(this.panelControl2);
            this.Layout1.Controls.Add(this.radio尿微量蛋白);
            this.Layout1.Controls.Add(this.txt糖化血红蛋白);
            this.Layout1.Controls.Add(this.lab创建机构);
            this.Layout1.Controls.Add(this.lab最近修改人);
            this.Layout1.Controls.Add(this.lab最近更新时间);
            this.Layout1.Controls.Add(this.labelControl2);
            this.Layout1.Controls.Add(this.lab创建时间);
            this.Layout1.Controls.Add(this.txt居住地址);
            this.Layout1.Controls.Add(this.txt糖尿病类型);
            this.Layout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Layout1.Location = new System.Drawing.Point(0, 28);
            this.Layout1.Name = "Layout1";
            this.Layout1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(688, 165, 250, 350);
            this.Layout1.Root = this.layoutControlGroup1;
            this.Layout1.Size = new System.Drawing.Size(733, 483);
            this.Layout1.TabIndex = 1;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(100, 127);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(256, 20);
            this.txt联系电话.StyleController = this.Layout1;
            this.txt联系电话.TabIndex = 117;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(455, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(256, 20);
            this.txt职业.StyleController = this.Layout1;
            this.txt职业.TabIndex = 116;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(100, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(256, 20);
            this.txt身份证号.StyleController = this.Layout1;
            this.txt身份证号.TabIndex = 115;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(455, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(256, 20);
            this.txt出生日期.StyleController = this.Layout1;
            this.txt出生日期.TabIndex = 114;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(100, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(256, 20);
            this.txt性别.StyleController = this.Layout1;
            this.txt性别.TabIndex = 113;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(455, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(256, 20);
            this.txt姓名.StyleController = this.Layout1;
            this.txt姓名.TabIndex = 112;
            // 
            // txt个人档案编号
            // 
            this.txt个人档案编号.Location = new System.Drawing.Point(100, 48);
            this.txt个人档案编号.Name = "txt个人档案编号";
            this.txt个人档案编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案编号.Properties.ReadOnly = true;
            this.txt个人档案编号.Size = new System.Drawing.Size(256, 20);
            this.txt个人档案编号.StyleController = this.Layout1;
            this.txt个人档案编号.TabIndex = 111;
            // 
            // txt管理卡编号
            // 
            this.txt管理卡编号.Location = new System.Drawing.Point(100, 161);
            this.txt管理卡编号.Name = "txt管理卡编号";
            this.txt管理卡编号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt管理卡编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt管理卡编号.Properties.ReadOnly = true;
            this.txt管理卡编号.Size = new System.Drawing.Size(256, 20);
            this.txt管理卡编号.StyleController = this.Layout1;
            this.txt管理卡编号.TabIndex = 110;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(103, 472);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(605, 71);
            this.gcDetail.TabIndex = 92;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Location = new System.Drawing.Point(367, 443);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(35, 22);
            this.btn删除药物.StyleController = this.Layout1;
            this.btn删除药物.TabIndex = 109;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            this.btn删除药物.Click += new System.EventHandler(this.btn删除药物_Click);
            // 
            // btn添加药物
            // 
            this.btn添加药物.Location = new System.Drawing.Point(328, 443);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(35, 22);
            this.btn添加药物.StyleController = this.Layout1;
            this.btn添加药物.TabIndex = 108;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            this.btn添加药物.Click += new System.EventHandler(this.btn添加药物_Click);
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(103, 892);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(154, 14);
            this.lab创建人.StyleController = this.Layout1;
            this.lab创建人.TabIndex = 107;
            this.lab创建人.Text = "录入人";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.Location = new System.Drawing.Point(103, 910);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(48, 14);
            this.lab当前所属机构.StyleController = this.Layout1;
            this.lab当前所属机构.TabIndex = 106;
            this.lab当前所属机构.Text = "所属机构";
            // 
            // txt餐后血糖
            // 
            this.txt餐后血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt餐后血糖.Lbl1Text = "mmol/L";
            this.txt餐后血糖.Location = new System.Drawing.Point(559, 712);
            this.txt餐后血糖.Margin = new System.Windows.Forms.Padding(0);
            this.txt餐后血糖.Name = "txt餐后血糖";
            this.txt餐后血糖.Size = new System.Drawing.Size(149, 20);
            this.txt餐后血糖.TabIndex = 98;
            this.txt餐后血糖.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(103, 868);
            this.txt发生时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Size = new System.Drawing.Size(154, 20);
            this.txt发生时间.StyleController = this.Layout1;
            this.txt发生时间.TabIndex = 76;
            // 
            // radio终止理由
            // 
            this.radio终止理由.Location = new System.Drawing.Point(341, 841);
            this.radio终止理由.Margin = new System.Windows.Forms.Padding(0);
            this.radio终止理由.Name = "radio终止理由";
            this.radio终止理由.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "死亡"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "迁出"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "拒绝"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "失访")});
            this.radio终止理由.Size = new System.Drawing.Size(371, 24);
            this.radio终止理由.StyleController = this.Layout1;
            this.radio终止理由.TabIndex = 74;
            // 
            // txt终止管理日期
            // 
            this.txt终止管理日期.EditValue = null;
            this.txt终止管理日期.Location = new System.Drawing.Point(103, 842);
            this.txt终止管理日期.Margin = new System.Windows.Forms.Padding(0);
            this.txt终止管理日期.Name = "txt终止管理日期";
            this.txt终止管理日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt终止管理日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt终止管理日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt终止管理日期.Size = new System.Drawing.Size(154, 20);
            this.txt终止管理日期.StyleController = this.Layout1;
            this.txt终止管理日期.TabIndex = 72;
            // 
            // radio终止管理
            // 
            this.radio终止管理.EditValue = "2";
            this.radio终止管理.Location = new System.Drawing.Point(103, 813);
            this.radio终止管理.Margin = new System.Windows.Forms.Padding(0);
            this.radio终止管理.Name = "radio终止管理";
            this.radio终止管理.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio终止管理.Size = new System.Drawing.Size(154, 25);
            this.radio终止管理.StyleController = this.Layout1;
            this.radio终止管理.TabIndex = 69;
            this.radio终止管理.SelectedIndexChanged += new System.EventHandler(this.radio终止管理_SelectedIndexChanged);
            // 
            // txt胆固醇
            // 
            this.txt胆固醇.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt胆固醇.Lbl1Text = "mmol/L";
            this.txt胆固醇.Location = new System.Drawing.Point(103, 760);
            this.txt胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt胆固醇.Name = "txt胆固醇";
            this.txt胆固醇.Size = new System.Drawing.Size(145, 22);
            this.txt胆固醇.TabIndex = 63;
            this.txt胆固醇.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt甘油三脂
            // 
            this.txt甘油三脂.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt甘油三脂.Lbl1Text = "mmol/L";
            this.txt甘油三脂.Location = new System.Drawing.Point(559, 736);
            this.txt甘油三脂.Margin = new System.Windows.Forms.Padding(0);
            this.txt甘油三脂.Name = "txt甘油三脂";
            this.txt甘油三脂.Size = new System.Drawing.Size(149, 22);
            this.txt甘油三脂.TabIndex = 61;
            this.txt甘油三脂.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt低密度蛋白
            // 
            this.txt低密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt低密度蛋白.Lbl1Text = "mmol/L";
            this.txt低密度蛋白.Location = new System.Drawing.Point(333, 736);
            this.txt低密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt低密度蛋白.Name = "txt低密度蛋白";
            this.txt低密度蛋白.Size = new System.Drawing.Size(141, 20);
            this.txt低密度蛋白.TabIndex = 59;
            this.txt低密度蛋白.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt高密度蛋白
            // 
            this.txt高密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt高密度蛋白.Lbl1Text = "mmol/L";
            this.txt高密度蛋白.Location = new System.Drawing.Point(103, 736);
            this.txt高密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt高密度蛋白.Name = "txt高密度蛋白";
            this.txt高密度蛋白.Size = new System.Drawing.Size(145, 20);
            this.txt高密度蛋白.TabIndex = 57;
            this.txt高密度蛋白.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(333, 712);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(0);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(141, 20);
            this.txt空腹血糖.TabIndex = 55;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt腰围.Lbl1Text = "CM";
            this.txt腰围.Location = new System.Drawing.Point(559, 688);
            this.txt腰围.Margin = new System.Windows.Forms.Padding(0);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(149, 20);
            this.txt腰围.TabIndex = 51;
            this.txt腰围.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txtBMI
            // 
            this.txtBMI.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txtBMI.Lbl1Text = "kg/㎡";
            this.txtBMI.Location = new System.Drawing.Point(103, 786);
            this.txtBMI.Margin = new System.Windows.Forms.Padding(0);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Size = new System.Drawing.Size(605, 20);
            this.txtBMI.TabIndex = 49;
            this.txtBMI.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt体重.Lbl1Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(333, 688);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(141, 20);
            this.txt体重.TabIndex = 47;
            this.txt体重.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // radio生活自理能力
            // 
            this.radio生活自理能力.EditValue = "1";
            this.radio生活自理能力.Location = new System.Drawing.Point(103, 637);
            this.radio生活自理能力.Margin = new System.Windows.Forms.Padding(0);
            this.radio生活自理能力.Name = "radio生活自理能力";
            this.radio生活自理能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "完全自理"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "部分自理"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "完全不能自理")});
            this.radio生活自理能力.Size = new System.Drawing.Size(243, 25);
            this.radio生活自理能力.StyleController = this.Layout1;
            this.radio生活自理能力.TabIndex = 42;
            // 
            // radio体育锻炼
            // 
            this.radio体育锻炼.EditValue = "1";
            this.radio体育锻炼.Location = new System.Drawing.Point(103, 608);
            this.radio体育锻炼.Margin = new System.Windows.Forms.Padding(0);
            this.radio体育锻炼.Name = "radio体育锻炼";
            this.radio体育锻炼.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不锻炼"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "每月1-3次"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "每周1-2次"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "每周>=3次")});
            this.radio体育锻炼.Size = new System.Drawing.Size(321, 25);
            this.radio体育锻炼.StyleController = this.Layout1;
            this.radio体育锻炼.TabIndex = 40;
            // 
            // radio饮酒情况
            // 
            this.radio饮酒情况.EditValue = "1";
            this.radio饮酒情况.Location = new System.Drawing.Point(103, 579);
            this.radio饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio饮酒情况.Name = "radio饮酒情况";
            this.radio饮酒情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不饮酒"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "经常饮酒")});
            this.radio饮酒情况.Size = new System.Drawing.Size(243, 25);
            this.radio饮酒情况.StyleController = this.Layout1;
            this.radio饮酒情况.TabIndex = 38;
            // 
            // radio吸烟情况
            // 
            this.radio吸烟情况.EditValue = "1";
            this.radio吸烟情况.Location = new System.Drawing.Point(103, 550);
            this.radio吸烟情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio吸烟情况.Name = "radio吸烟情况";
            this.radio吸烟情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不吸烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "吸烟")});
            this.radio吸烟情况.Size = new System.Drawing.Size(243, 25);
            this.radio吸烟情况.StyleController = this.Layout1;
            this.radio吸烟情况.TabIndex = 36;
            // 
            // fl症状
            // 
            this.fl症状.AutoScroll = true;
            this.fl症状.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fl症状.Controls.Add(this.ck无症状);
            this.fl症状.Controls.Add(this.checkEdit9);
            this.fl症状.Controls.Add(this.checkEdit10);
            this.fl症状.Controls.Add(this.checkEdit11);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Controls.Add(this.checkEdit15);
            this.fl症状.Controls.Add(this.checkEdit16);
            this.fl症状.Controls.Add(this.ck症状其他);
            this.fl症状.Controls.Add(this.txt目前症状其他);
            this.fl症状.Location = new System.Drawing.Point(100, 238);
            this.fl症状.Margin = new System.Windows.Forms.Padding(0);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(611, 51);
            this.fl症状.TabIndex = 27;
            // 
            // ck无症状
            // 
            this.ck无症状.EditValue = true;
            this.ck无症状.Location = new System.Drawing.Point(1, 1);
            this.ck无症状.Margin = new System.Windows.Forms.Padding(1);
            this.ck无症状.Name = "ck无症状";
            this.ck无症状.Properties.Caption = "无症状";
            this.ck无症状.Size = new System.Drawing.Size(66, 19);
            this.ck无症状.TabIndex = 0;
            this.ck无症状.Tag = "0";
            this.ck无症状.CheckedChanged += new System.EventHandler(this.ck无症状_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(69, 1);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "多饮";
            this.checkEdit9.Size = new System.Drawing.Size(68, 19);
            this.checkEdit9.TabIndex = 1;
            this.checkEdit9.Tag = "1";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(139, 1);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "多食";
            this.checkEdit10.Size = new System.Drawing.Size(62, 19);
            this.checkEdit10.TabIndex = 2;
            this.checkEdit10.Tag = "2";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(203, 1);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "多尿";
            this.checkEdit11.Size = new System.Drawing.Size(62, 19);
            this.checkEdit11.TabIndex = 3;
            this.checkEdit11.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(267, 1);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "视力模糊";
            this.checkEdit12.Size = new System.Drawing.Size(75, 19);
            this.checkEdit12.TabIndex = 4;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(344, 1);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "感染";
            this.checkEdit13.Size = new System.Drawing.Size(50, 19);
            this.checkEdit13.TabIndex = 5;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(396, 1);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "手脚麻木";
            this.checkEdit14.Size = new System.Drawing.Size(75, 19);
            this.checkEdit14.TabIndex = 6;
            this.checkEdit14.Tag = "6";
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(473, 1);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "下肢浮肿";
            this.checkEdit15.Size = new System.Drawing.Size(75, 19);
            this.checkEdit15.TabIndex = 7;
            this.checkEdit15.Tag = "7";
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(1, 22);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "体重明显下降";
            this.checkEdit16.Size = new System.Drawing.Size(99, 19);
            this.checkEdit16.TabIndex = 8;
            this.checkEdit16.Tag = "8";
            // 
            // ck症状其他
            // 
            this.ck症状其他.Location = new System.Drawing.Point(102, 22);
            this.ck症状其他.Margin = new System.Windows.Forms.Padding(1);
            this.ck症状其他.Name = "ck症状其他";
            this.ck症状其他.Properties.Caption = "其他";
            this.ck症状其他.Size = new System.Drawing.Size(56, 19);
            this.ck症状其他.TabIndex = 9;
            this.ck症状其他.Tag = "99";
            this.ck症状其他.CheckedChanged += new System.EventHandler(this.ck症状其他_CheckedChanged);
            // 
            // txt目前症状其他
            // 
            this.txt目前症状其他.Location = new System.Drawing.Point(160, 22);
            this.txt目前症状其他.Margin = new System.Windows.Forms.Padding(1);
            this.txt目前症状其他.Name = "txt目前症状其他";
            this.txt目前症状其他.Size = new System.Drawing.Size(182, 20);
            this.txt目前症状其他.TabIndex = 10;
            this.txt目前症状其他.Visible = false;
            // 
            // radio病例来源
            // 
            this.radio病例来源.EditValue = "1";
            this.radio病例来源.Location = new System.Drawing.Point(100, 185);
            this.radio病例来源.Margin = new System.Windows.Forms.Padding(0);
            this.radio病例来源.Name = "radio病例来源";
            this.radio病例来源.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "健康档案"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "健康体检"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio病例来源.Size = new System.Drawing.Size(354, 25);
            this.radio病例来源.StyleController = this.Layout1;
            this.radio病例来源.TabIndex = 23;
            // 
            // radio管理组别
            // 
            this.radio管理组别.EditValue = "1";
            this.radio管理组别.Location = new System.Drawing.Point(455, 159);
            this.radio管理组别.Margin = new System.Windows.Forms.Padding(0);
            this.radio管理组别.Name = "radio管理组别";
            this.radio管理组别.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "常规管理"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "强化管理")});
            this.radio管理组别.Size = new System.Drawing.Size(256, 22);
            this.radio管理组别.StyleController = this.Layout1;
            this.radio管理组别.TabIndex = 21;
            // 
            // fl家族史
            // 
            this.fl家族史.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fl家族史.Controls.Add(this.jzs_ck高血压);
            this.fl家族史.Controls.Add(this.jzs_ck冠心病);
            this.fl家族史.Controls.Add(this.jzs_ck脑卒中);
            this.fl家族史.Controls.Add(this.jzs_ck糖尿病);
            this.fl家族史.Controls.Add(this.jzs_ck以上都无);
            this.fl家族史.Controls.Add(this.jzs_ck不详);
            this.fl家族史.Controls.Add(this.jzs_ck拒答);
            this.fl家族史.Location = new System.Drawing.Point(100, 214);
            this.fl家族史.Margin = new System.Windows.Forms.Padding(0);
            this.fl家族史.Name = "fl家族史";
            this.fl家族史.Size = new System.Drawing.Size(611, 20);
            this.fl家族史.TabIndex = 25;
            // 
            // jzs_ck高血压
            // 
            this.jzs_ck高血压.Location = new System.Drawing.Point(1, 1);
            this.jzs_ck高血压.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck高血压.Name = "jzs_ck高血压";
            this.jzs_ck高血压.Properties.Caption = "高血压";
            this.jzs_ck高血压.Size = new System.Drawing.Size(66, 19);
            this.jzs_ck高血压.TabIndex = 0;
            this.jzs_ck高血压.Tag = "1";
            // 
            // jzs_ck冠心病
            // 
            this.jzs_ck冠心病.Location = new System.Drawing.Point(69, 1);
            this.jzs_ck冠心病.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck冠心病.Name = "jzs_ck冠心病";
            this.jzs_ck冠心病.Properties.Caption = "冠心病";
            this.jzs_ck冠心病.Size = new System.Drawing.Size(68, 19);
            this.jzs_ck冠心病.TabIndex = 1;
            this.jzs_ck冠心病.Tag = "2";
            // 
            // jzs_ck脑卒中
            // 
            this.jzs_ck脑卒中.Location = new System.Drawing.Point(139, 1);
            this.jzs_ck脑卒中.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck脑卒中.Name = "jzs_ck脑卒中";
            this.jzs_ck脑卒中.Properties.Caption = "脑卒中";
            this.jzs_ck脑卒中.Size = new System.Drawing.Size(62, 19);
            this.jzs_ck脑卒中.TabIndex = 2;
            this.jzs_ck脑卒中.Tag = "3";
            // 
            // jzs_ck糖尿病
            // 
            this.jzs_ck糖尿病.Location = new System.Drawing.Point(203, 1);
            this.jzs_ck糖尿病.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck糖尿病.Name = "jzs_ck糖尿病";
            this.jzs_ck糖尿病.Properties.Caption = "糖尿病";
            this.jzs_ck糖尿病.Size = new System.Drawing.Size(62, 19);
            this.jzs_ck糖尿病.TabIndex = 3;
            this.jzs_ck糖尿病.Tag = "4";
            // 
            // jzs_ck以上都无
            // 
            this.jzs_ck以上都无.Location = new System.Drawing.Point(267, 1);
            this.jzs_ck以上都无.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck以上都无.Name = "jzs_ck以上都无";
            this.jzs_ck以上都无.Properties.Caption = "以上都无";
            this.jzs_ck以上都无.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck以上都无.TabIndex = 4;
            this.jzs_ck以上都无.Tag = "98";
            this.jzs_ck以上都无.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // jzs_ck不详
            // 
            this.jzs_ck不详.EditValue = true;
            this.jzs_ck不详.Location = new System.Drawing.Point(344, 1);
            this.jzs_ck不详.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck不详.Name = "jzs_ck不详";
            this.jzs_ck不详.Properties.Caption = "不详";
            this.jzs_ck不详.Size = new System.Drawing.Size(50, 19);
            this.jzs_ck不详.TabIndex = 5;
            this.jzs_ck不详.Tag = "99";
            this.jzs_ck不详.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // jzs_ck拒答
            // 
            this.jzs_ck拒答.Location = new System.Drawing.Point(396, 1);
            this.jzs_ck拒答.Margin = new System.Windows.Forms.Padding(1);
            this.jzs_ck拒答.Name = "jzs_ck拒答";
            this.jzs_ck拒答.Properties.Caption = "拒答";
            this.jzs_ck拒答.Size = new System.Drawing.Size(59, 19);
            this.jzs_ck拒答.TabIndex = 6;
            this.jzs_ck拒答.Tag = "100";
            this.jzs_ck拒答.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // radio降糖药
            // 
            this.radio降糖药.EditValue = "2";
            this.radio降糖药.Location = new System.Drawing.Point(103, 443);
            this.radio降糖药.Margin = new System.Windows.Forms.Padding(0);
            this.radio降糖药.Name = "radio降糖药";
            this.radio降糖药.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio降糖药.Size = new System.Drawing.Size(221, 25);
            this.radio降糖药.StyleController = this.Layout1;
            this.radio降糖药.TabIndex = 32;
            this.radio降糖药.SelectedIndexChanged += new System.EventHandler(this.radio降糖药_SelectedIndexChanged);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "CM";
            this.txt身高.Location = new System.Drawing.Point(103, 688);
            this.txt身高.Margin = new System.Windows.Forms.Padding(0);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(145, 20);
            this.txt身高.TabIndex = 45;
            this.txt身高.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(103, 712);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(0);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(145, 20);
            this.txt血压值.TabIndex = 53;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(40, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(40, 20);
            // 
            // txt确诊时间
            // 
            this.txt确诊时间.EditValue = null;
            this.txt确诊时间.Location = new System.Drawing.Point(288, 293);
            this.txt确诊时间.Name = "txt确诊时间";
            this.txt确诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt确诊时间.Size = new System.Drawing.Size(104, 20);
            this.txt确诊时间.StyleController = this.Layout1;
            this.txt确诊时间.TabIndex = 91;
            // 
            // txt确诊单位
            // 
            this.txt确诊单位.Location = new System.Drawing.Point(453, 293);
            this.txt确诊单位.Name = "txt确诊单位";
            this.txt确诊单位.Size = new System.Drawing.Size(258, 20);
            this.txt确诊单位.StyleController = this.Layout1;
            this.txt确诊单位.TabIndex = 93;
            // 
            // checkEdit并发症情况
            // 
            this.checkEdit并发症情况.EditValue = true;
            this.checkEdit并发症情况.Location = new System.Drawing.Point(100, 317);
            this.checkEdit并发症情况.Name = "checkEdit并发症情况";
            this.checkEdit并发症情况.Properties.AutoHeight = false;
            this.checkEdit并发症情况.Properties.Caption = "无";
            this.checkEdit并发症情况.Size = new System.Drawing.Size(126, 71);
            this.checkEdit并发症情况.StyleController = this.Layout1;
            this.checkEdit并发症情况.TabIndex = 95;
            this.checkEdit并发症情况.CheckedChanged += new System.EventHandler(this.checkEdit并发症情况_CheckedChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txt脑血管);
            this.panelControl2.Controls.Add(this.labelControl70);
            this.panelControl2.Controls.Add(this.txt足部);
            this.panelControl2.Controls.Add(this.labelControl69);
            this.panelControl2.Controls.Add(this.txt视网膜);
            this.panelControl2.Controls.Add(this.labelControl68);
            this.panelControl2.Controls.Add(this.txt心脏);
            this.panelControl2.Controls.Add(this.labelControl67);
            this.panelControl2.Controls.Add(this.txt神经);
            this.panelControl2.Controls.Add(this.labelControl66);
            this.panelControl2.Controls.Add(this.txt肾脏);
            this.panelControl2.Controls.Add(this.labelControl26);
            this.panelControl2.Location = new System.Drawing.Point(229, 316);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(483, 73);
            this.panelControl2.TabIndex = 96;
            // 
            // txt脑血管
            // 
            this.txt脑血管.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt脑血管.Lbl1Text = "年";
            this.txt脑血管.Location = new System.Drawing.Point(267, 45);
            this.txt脑血管.Name = "txt脑血管";
            this.txt脑血管.Size = new System.Drawing.Size(129, 18);
            this.txt脑血管.TabIndex = 11;
            this.txt脑血管.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl70
            // 
            this.labelControl70.Location = new System.Drawing.Point(174, 47);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(88, 14);
            this.labelControl70.TabIndex = 10;
            this.labelControl70.Text = "脑血管病变时间:";
            // 
            // txt足部
            // 
            this.txt足部.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt足部.Lbl1Text = "年";
            this.txt足部.Location = new System.Drawing.Point(96, 46);
            this.txt足部.Name = "txt足部";
            this.txt足部.Size = new System.Drawing.Size(129, 18);
            this.txt足部.TabIndex = 9;
            this.txt足部.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl69
            // 
            this.labelControl69.Location = new System.Drawing.Point(8, 47);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(80, 14);
            this.labelControl69.TabIndex = 8;
            this.labelControl69.Text = "足部病变 时间:";
            // 
            // txt视网膜
            // 
            this.txt视网膜.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt视网膜.Lbl1Text = "年";
            this.txt视网膜.Location = new System.Drawing.Point(267, 23);
            this.txt视网膜.Name = "txt视网膜";
            this.txt视网膜.Size = new System.Drawing.Size(129, 18);
            this.txt视网膜.TabIndex = 7;
            this.txt视网膜.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(174, 26);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(88, 14);
            this.labelControl68.TabIndex = 6;
            this.labelControl68.Text = "视网膜病变时间:";
            // 
            // txt心脏
            // 
            this.txt心脏.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt心脏.Lbl1Text = "年";
            this.txt心脏.Location = new System.Drawing.Point(96, 23);
            this.txt心脏.Name = "txt心脏";
            this.txt心脏.Size = new System.Drawing.Size(129, 18);
            this.txt心脏.TabIndex = 5;
            this.txt心脏.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl67
            // 
            this.labelControl67.Location = new System.Drawing.Point(8, 26);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(80, 14);
            this.labelControl67.TabIndex = 4;
            this.labelControl67.Text = "心脏病变 时间:";
            // 
            // txt神经
            // 
            this.txt神经.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt神经.Lbl1Text = "年";
            this.txt神经.Location = new System.Drawing.Point(267, 1);
            this.txt神经.Name = "txt神经";
            this.txt神经.Size = new System.Drawing.Size(129, 18);
            this.txt神经.TabIndex = 3;
            this.txt神经.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl66
            // 
            this.labelControl66.Location = new System.Drawing.Point(182, 3);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(80, 14);
            this.labelControl66.TabIndex = 2;
            this.labelControl66.Text = "神经病变 时间:";
            // 
            // txt肾脏
            // 
            this.txt肾脏.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt肾脏.Lbl1Text = "年";
            this.txt肾脏.Location = new System.Drawing.Point(96, 0);
            this.txt肾脏.Name = "txt肾脏";
            this.txt肾脏.Size = new System.Drawing.Size(129, 18);
            this.txt肾脏.TabIndex = 1;
            this.txt肾脏.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(8, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(80, 14);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "肾脏病变 时间:";
            // 
            // radio尿微量蛋白
            // 
            this.radio尿微量蛋白.EditValue = "2";
            this.radio尿微量蛋白.Location = new System.Drawing.Point(332, 759);
            this.radio尿微量蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.radio尿微量蛋白.Name = "radio尿微量蛋白";
            this.radio尿微量蛋白.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio尿微量蛋白.Size = new System.Drawing.Size(143, 24);
            this.radio尿微量蛋白.StyleController = this.Layout1;
            this.radio尿微量蛋白.TabIndex = 65;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt糖化血红蛋白.Lbl1Text = "%";
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(559, 762);
            this.txt糖化血红蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(149, 20);
            this.txt糖化血红蛋白.TabIndex = 99;
            this.txt糖化血红蛋白.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(549, 892);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(162, 14);
            this.lab创建机构.StyleController = this.Layout1;
            this.lab创建机构.TabIndex = 86;
            this.lab创建机构.Text = "创建机构";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.Location = new System.Drawing.Point(342, 892);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(122, 14);
            this.lab最近修改人.StyleController = this.Layout1;
            this.lab最近修改人.TabIndex = 84;
            this.lab最近修改人.Text = "修改人 ";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.Location = new System.Drawing.Point(549, 871);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(122, 14);
            this.lab最近更新时间.StyleController = this.Layout1;
            this.lab最近更新时间.TabIndex = 80;
            this.lab最近更新时间.Text = "2015-07-26 10:14:18 ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(5, 30);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(706, 14);
            this.labelControl2.StyleController = this.Layout1;
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "考核项：25     缺项：25 完整度：0% ";
            // 
            // lab创建时间
            // 
            this.lab创建时间.Location = new System.Drawing.Point(342, 871);
            this.lab创建时间.Name = "lab创建时间";
            this.lab创建时间.Size = new System.Drawing.Size(122, 14);
            this.lab创建时间.StyleController = this.Layout1;
            this.lab创建时间.TabIndex = 78;
            this.lab创建时间.Text = "2015-07-26 10:14:18 ";
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(455, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(256, 37);
            this.txt居住地址.StyleController = this.Layout1;
            this.txt居住地址.TabIndex = 118;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt糖尿病类型
            // 
            this.txt糖尿病类型.Location = new System.Drawing.Point(100, 293);
            this.txt糖尿病类型.Name = "txt糖尿病类型";
            this.txt糖尿病类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt糖尿病类型.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt糖尿病类型.Properties.NullText = "请选择";
            this.txt糖尿病类型.Properties.PopupSizeable = false;
            this.txt糖尿病类型.Size = new System.Drawing.Size(127, 20);
            this.txt糖尿病类型.StyleController = this.Layout1;
            this.txt糖尿病类型.TabIndex = 89;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "糖尿病患者管理卡";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem58,
            this.layout糖尿病并发症,
            this.layoutControlItem7,
            this.layout终止管理,
            this.layout终止理由,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem25,
            this.layoutControlItem41,
            this.emptySpaceItem7,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem32,
            this.layoutControlItem38,
            this.layoutControlItem40,
            this.layoutControlItem42,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem4,
            this.layoutControlItem36,
            this.layoutControlItem46,
            this.layoutControlItem43,
            this.layoutControlItem39,
            this.layoutControlItem37});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(716, 929);
            this.layoutControlGroup1.Text = "糖尿病患者管理卡";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.radio生活自理能力;
            this.layoutControlItem16.CustomizationFormText = "生活自理能力";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 607);
            this.layoutControlItem16.Name = "radioGroup7item";
            this.layoutControlItem16.Size = new System.Drawing.Size(345, 29);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "生活自理能力";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.radio体育锻炼;
            this.layoutControlItem17.CustomizationFormText = "体育锻炼";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 578);
            this.layoutControlItem17.Name = "radioGroup6item";
            this.layoutControlItem17.Size = new System.Drawing.Size(423, 29);
            this.layoutControlItem17.Tag = "check";
            this.layoutControlItem17.Text = "体育锻炼";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.radio饮酒情况;
            this.layoutControlItem18.CustomizationFormText = "饮酒情况";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 549);
            this.layoutControlItem18.Name = "radioGroup5item";
            this.layoutControlItem18.Size = new System.Drawing.Size(345, 29);
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "饮酒情况";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.radio吸烟情况;
            this.layoutControlItem19.CustomizationFormText = "吸烟情况";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 520);
            this.layoutControlItem19.Name = "radioGroup4item";
            this.layoutControlItem19.Size = new System.Drawing.Size(345, 29);
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "吸烟情况";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem20.Control = this.fl症状;
            this.layoutControlItem20.CustomizationFormText = "目前症状(可多选)";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 208);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(185, 55);
            this.layoutControlItem20.Name = "flowLayoutPanel3item";
            this.layoutControlItem20.Size = new System.Drawing.Size(710, 55);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "目前症状(可多选)";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.radio病例来源;
            this.layoutControlItem21.CustomizationFormText = "病例来源";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem21.Name = "radioGroup2item";
            this.layoutControlItem21.Size = new System.Drawing.Size(453, 29);
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "病例来源";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.radio管理组别;
            this.layoutControlItem22.CustomizationFormText = "管理组别";
            this.layoutControlItem22.Location = new System.Drawing.Point(355, 131);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(153, 24);
            this.layoutControlItem22.Name = "radioGroup1item";
            this.layoutControlItem22.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 2);
            this.layoutControlItem22.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Tag = "check";
            this.layoutControlItem22.Text = "管理组别";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.fl家族史;
            this.layoutControlItem23.CustomizationFormText = "家族史";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 184);
            this.layoutControlItem23.Name = "flowLayoutPanel2item";
            this.layoutControlItem23.Size = new System.Drawing.Size(710, 24);
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "家族史";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt糖尿病类型;
            this.layoutControlItem28.CustomizationFormText = "糖尿病类型";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 263);
            this.layoutControlItem28.Name = "comboBoxEdit1item";
            this.layoutControlItem28.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "糖尿病类型";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt确诊时间;
            this.layoutControlItem29.CustomizationFormText = "确诊时间";
            this.layoutControlItem29.Location = new System.Drawing.Point(226, 263);
            this.layoutControlItem29.Name = "comboBoxEdit2item";
            this.layoutControlItem29.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem29.Tag = "check";
            this.layoutControlItem29.Text = "确诊时间";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt确诊单位;
            this.layoutControlItem30.CustomizationFormText = "确诊单位";
            this.layoutControlItem30.Location = new System.Drawing.Point(391, 263);
            this.layoutControlItem30.Name = "dateEdit3item";
            this.layoutControlItem30.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "确诊单位";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.checkEdit并发症情况;
            this.layoutControlItem31.CustomizationFormText = "糖尿病并发症情况";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 287);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(225, 75);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(225, 75);
            this.layoutControlItem31.Name = "checkEdit18item";
            this.layoutControlItem31.Size = new System.Drawing.Size(225, 75);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "糖尿病并发症情况";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.labelControl2;
            this.layoutControlItem58.CustomizationFormText = "labelControl2item";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem58.Name = "labelControl2item";
            this.layoutControlItem58.Size = new System.Drawing.Size(710, 18);
            this.layoutControlItem58.Text = "labelControl2item";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem58.TextToControlDistance = 0;
            this.layoutControlItem58.TextVisible = false;
            // 
            // layout糖尿病并发症
            // 
            this.layout糖尿病并发症.Control = this.panelControl2;
            this.layout糖尿病并发症.CustomizationFormText = "近期药物治疗情况";
            this.layout糖尿病并发症.Location = new System.Drawing.Point(225, 287);
            this.layout糖尿病并发症.MinSize = new System.Drawing.Size(183, 22);
            this.layout糖尿病并发症.Name = "layout糖尿病并发症";
            this.layout糖尿病并发症.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layout糖尿病并发症.Size = new System.Drawing.Size(485, 75);
            this.layout糖尿病并发症.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout糖尿病并发症.Text = "病症情况";
            this.layout糖尿病并发症.TextSize = new System.Drawing.Size(0, 0);
            this.layout糖尿病并发症.TextToControlDistance = 0;
            this.layout糖尿病并发症.TextVisible = false;
            this.layout糖尿病并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.radio终止管理;
            this.layoutControlItem7.CustomizationFormText = "是否终止管理";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 783);
            this.layoutControlItem7.Name = "radioGroup8item";
            this.layoutControlItem7.Size = new System.Drawing.Size(256, 29);
            this.layoutControlItem7.Text = "是否终止管理";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layout终止管理
            // 
            this.layout终止管理.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layout终止管理.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layout终止管理.Control = this.txt终止管理日期;
            this.layout终止管理.CustomizationFormText = "终止管理日期";
            this.layout终止管理.Location = new System.Drawing.Point(0, 812);
            this.layout终止管理.Name = "layout终止管理";
            this.layout终止管理.Size = new System.Drawing.Size(256, 26);
            this.layout终止管理.Text = "终止管理日期";
            this.layout终止管理.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout终止管理.TextSize = new System.Drawing.Size(93, 14);
            this.layout终止管理.TextToControlDistance = 5;
            // 
            // layout终止理由
            // 
            this.layout终止理由.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layout终止理由.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layout终止理由.Control = this.radio终止理由;
            this.layout终止理由.CustomizationFormText = "终止理由";
            this.layout终止理由.Location = new System.Drawing.Point(256, 812);
            this.layout终止理由.MinSize = new System.Drawing.Size(153, 26);
            this.layout终止理由.Name = "layout终止理由";
            this.layout终止理由.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layout终止理由.Size = new System.Drawing.Size(454, 26);
            this.layout终止理由.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout终止理由.Text = "终止理由";
            this.layout终止理由.TextSize = new System.Drawing.Size(78, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(256, 783);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(454, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(345, 607);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(365, 29);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(423, 578);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(287, 29);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(345, 549);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(365, 29);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(345, 520);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(365, 29);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.lab当前所属机构;
            this.layoutControlItem25.CustomizationFormText = "所属机构";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 880);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(710, 18);
            this.layoutControlItem25.Text = "所属机构";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt管理卡编号;
            this.layoutControlItem41.CustomizationFormText = "管理卡编号";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem41.Text = "管理卡编号";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(453, 155);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(257, 29);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt个人档案编号;
            this.layoutControlItem5.CustomizationFormText = "个人档案号";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem5.Text = "个人档案号";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt姓名;
            this.layoutControlItem6.CustomizationFormText = "姓名";
            this.layoutControlItem6.Location = new System.Drawing.Point(355, 18);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem6.Text = "姓名";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt性别;
            this.layoutControlItem32.CustomizationFormText = "性别";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem32.Text = "性别";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt出生日期;
            this.layoutControlItem38.CustomizationFormText = "出生日期";
            this.layoutControlItem38.Location = new System.Drawing.Point(355, 42);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem38.Text = "出生日期";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.txt身份证号;
            this.layoutControlItem40.CustomizationFormText = "身份证号";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem40.Text = "身份证号";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt职业;
            this.layoutControlItem42.CustomizationFormText = "职业";
            this.layoutControlItem42.Location = new System.Drawing.Point(355, 66);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem42.Text = "职业";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.txt联系电话;
            this.layoutControlItem44.CustomizationFormText = "联系电话";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 9, 2);
            this.layoutControlItem44.Size = new System.Drawing.Size(355, 41);
            this.layoutControlItem44.Text = "联系电话";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.txt居住地址;
            this.layoutControlItem45.CustomizationFormText = "居住地址";
            this.layoutControlItem45.Location = new System.Drawing.Point(355, 90);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(109, 41);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(355, 41);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "居住地址";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "近期药物治疗情况";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem24,
            this.layout添加药物,
            this.layout删除药物,
            this.layout用药列表,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 362);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(710, 158);
            this.layoutControlGroup2.Text = "近期药物治疗情况";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.radio胰岛素;
            this.layoutControlItem2.CustomizationFormText = "胰岛素";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "radioGroup10item";
            this.layoutControlItem2.Size = new System.Drawing.Size(320, 29);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "胰岛素";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt胰岛素用量;
            this.layoutControlItem1.CustomizationFormText = "胰岛素用量";
            this.layoutControlItem1.Location = new System.Drawing.Point(320, 0);
            this.layoutControlItem1.Name = "UCTxtLblitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(384, 29);
            this.layoutControlItem1.Text = "胰岛素用量";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.radio降糖药;
            this.layoutControlItem24.CustomizationFormText = "口服降糖药";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem24.Name = "radioGroup3item";
            this.layoutControlItem24.Size = new System.Drawing.Size(320, 29);
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "口服降糖药";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layoutControlItem38";
            this.layout添加药物.Location = new System.Drawing.Point(320, 29);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(39, 29);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layoutControlItem32";
            this.layout删除药物.Location = new System.Drawing.Point(359, 29);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(39, 29);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout用药列表
            // 
            this.layout用药列表.Control = this.gcDetail;
            this.layout用药列表.CustomizationFormText = " ";
            this.layout用药列表.Location = new System.Drawing.Point(0, 58);
            this.layout用药列表.MaxSize = new System.Drawing.Size(0, 75);
            this.layout用药列表.MinSize = new System.Drawing.Size(219, 75);
            this.layout用药列表.Name = "layout用药列表";
            this.layout用药列表.Size = new System.Drawing.Size(704, 75);
            this.layout用药列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout用药列表.Text = " ";
            this.layout用药列表.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout用药列表.TextSize = new System.Drawing.Size(90, 14);
            this.layout用药列表.TextToControlDistance = 5;
            this.layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(398, 29);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(306, 29);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "建档时的体检结果";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem15,
            this.layoutControlItem13,
            this.layoutControlItem27,
            this.layoutControlItem12,
            this.layoutControlItem3,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem14});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 636);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(710, 147);
            this.layoutControlGroup3.Text = "建档时的体检结果";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt身高;
            this.layoutControlItem26.CustomizationFormText = "身高";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "item7";
            this.layoutControlItem26.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem26.Tag = "check";
            this.layoutControlItem26.Text = "身高";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt体重;
            this.layoutControlItem15.CustomizationFormText = "体重";
            this.layoutControlItem15.Location = new System.Drawing.Point(244, 0);
            this.layoutControlItem15.Name = "item6";
            this.layoutControlItem15.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "体重";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt腰围;
            this.layoutControlItem13.CustomizationFormText = "腰围";
            this.layoutControlItem13.Location = new System.Drawing.Point(470, 0);
            this.layoutControlItem13.Name = "item5";
            this.layoutControlItem13.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "腰围";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt血压值;
            this.layoutControlItem27.CustomizationFormText = "血压";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem27.Name = "UCTxtLblTxtLblitem";
            this.layoutControlItem27.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "血压";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt空腹血糖;
            this.layoutControlItem12.CustomizationFormText = "空腹血糖";
            this.layoutControlItem12.Location = new System.Drawing.Point(244, 24);
            this.layoutControlItem12.Name = "item4";
            this.layoutControlItem12.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "空腹血糖";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt餐后血糖;
            this.layoutControlItem3.CustomizationFormText = "餐后2h血糖";
            this.layoutControlItem3.Location = new System.Drawing.Point(470, 24);
            this.layoutControlItem3.Name = "item0";
            this.layoutControlItem3.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "餐后2h血糖";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt高密度蛋白;
            this.layoutControlItem11.CustomizationFormText = "高密度脂蛋白";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.Name = "item3";
            this.layoutControlItem11.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "高密度脂蛋白";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt低密度蛋白;
            this.layoutControlItem10.CustomizationFormText = "低密度脂蛋白";
            this.layoutControlItem10.Location = new System.Drawing.Point(244, 48);
            this.layoutControlItem10.Name = "item2";
            this.layoutControlItem10.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "低密度脂蛋白";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt甘油三脂;
            this.layoutControlItem9.CustomizationFormText = "甘油三脂";
            this.layoutControlItem9.Location = new System.Drawing.Point(470, 48);
            this.layoutControlItem9.Name = "item1";
            this.layoutControlItem9.Size = new System.Drawing.Size(234, 26);
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "甘油三脂";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt胆固醇;
            this.layoutControlItem8.CustomizationFormText = "胆固醇";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem8.Name = "item8";
            this.layoutControlItem8.Size = new System.Drawing.Size(244, 26);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "胆固醇";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.radio尿微量蛋白;
            this.layoutControlItem33.CustomizationFormText = "尿微量蛋白";
            this.layoutControlItem33.Location = new System.Drawing.Point(244, 72);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(153, 26);
            this.layoutControlItem33.Name = "textEdit3item";
            this.layoutControlItem33.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem33.Size = new System.Drawing.Size(226, 26);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "尿微量蛋白";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt糖化血红蛋白;
            this.layoutControlItem34.CustomizationFormText = "糖化血红蛋白";
            this.layoutControlItem34.Location = new System.Drawing.Point(470, 74);
            this.layoutControlItem34.Name = "item9";
            this.layoutControlItem34.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "糖化血红蛋白";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txtBMI;
            this.layoutControlItem14.CustomizationFormText = "BMI";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem14.Name = "item10";
            this.layoutControlItem14.Size = new System.Drawing.Size(704, 24);
            this.layoutControlItem14.Text = "BMI";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt发生时间;
            this.layoutControlItem4.CustomizationFormText = "发生时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 838);
            this.layoutControlItem4.Name = "dateEdit2item";
            this.layoutControlItem4.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem4.Text = "发生时间";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.lab创建人;
            this.layoutControlItem36.CustomizationFormText = "录入人";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 862);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(256, 18);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(256, 18);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(256, 18);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "录入人";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(93, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab创建时间;
            this.layoutControlItem46.CustomizationFormText = "录入时间:";
            this.layoutControlItem46.Location = new System.Drawing.Point(256, 838);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlItem46.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem46.Text = "录入时间:";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lab最近更新时间;
            this.layoutControlItem43.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem43.Location = new System.Drawing.Point(463, 838);
            this.layoutControlItem43.Name = "labelControl54item";
            this.layoutControlItem43.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlItem43.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem43.Text = "最近更新时间:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.lab最近修改人;
            this.layoutControlItem39.CustomizationFormText = "修改人:";
            this.layoutControlItem39.Location = new System.Drawing.Point(256, 862);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(148, 18);
            this.layoutControlItem39.Name = "labelControl58item";
            this.layoutControlItem39.Size = new System.Drawing.Size(207, 18);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "修改人:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.lab创建机构;
            this.layoutControlItem37.CustomizationFormText = "创建机构:";
            this.layoutControlItem37.Location = new System.Drawing.Point(463, 862);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(156, 18);
            this.layoutControlItem37.Name = "labelControl60item";
            this.layoutControlItem37.Size = new System.Drawing.Size(247, 18);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "创建机构:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(78, 14);
            // 
            // UC糖尿病患者管理卡
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Layout1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC糖尿病患者管理卡";
            this.Size = new System.Drawing.Size(733, 511);
            this.Load += new System.EventHandler(this.UC糖尿病患者管理卡_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.Layout1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio胰岛素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).EndInit();
            this.Layout1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止理由.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio体育锻炼.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ck无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio病例来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理组别.Properties)).EndInit();
            this.fl家族史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck以上都无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck不详.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck拒答.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio降糖药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit并发症情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio尿微量蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖尿病类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout糖尿病并发症)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout终止管理)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout终止理由)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout用药列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改保存;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.RadioGroup radio管理组别;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraEditors.CheckEdit ck无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit ck症状其他;
        private DevExpress.XtraEditors.TextEdit txt目前症状其他;
        private DevExpress.XtraEditors.RadioGroup radio病例来源;
        private System.Windows.Forms.FlowLayoutPanel fl家族史;
        private DevExpress.XtraEditors.CheckEdit jzs_ck高血压;
        private DevExpress.XtraEditors.CheckEdit jzs_ck冠心病;
        private DevExpress.XtraEditors.CheckEdit jzs_ck脑卒中;
        private DevExpress.XtraEditors.CheckEdit jzs_ck糖尿病;
        private DevExpress.XtraEditors.CheckEdit jzs_ck以上都无;
        private DevExpress.XtraEditors.CheckEdit jzs_ck不详;
        private DevExpress.XtraEditors.CheckEdit jzs_ck拒答;
        private DevExpress.XtraEditors.RadioGroup radio降糖药;
        private DevExpress.XtraEditors.RadioGroup radio生活自理能力;
        private DevExpress.XtraEditors.RadioGroup radio体育锻炼;
        private DevExpress.XtraEditors.RadioGroup radio饮酒情况;
        private DevExpress.XtraEditors.RadioGroup radio吸烟情况;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLbl txtBMI;
        private Library.UserControls.UCTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt腰围;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private Library.UserControls.UCTxtLbl txt胆固醇;
        private Library.UserControls.UCTxtLbl txt甘油三脂;
        private Library.UserControls.UCTxtLbl txt低密度蛋白;
        private Library.UserControls.UCTxtLbl txt高密度蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private DevExpress.XtraEditors.RadioGroup radio终止管理;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.RadioGroup radio终止理由;
        private DevExpress.XtraEditors.DateEdit txt终止管理日期;
        private DevExpress.XtraEditors.LabelControl lab创建时间;
        private DevExpress.XtraEditors.DateEdit txt确诊时间;
        private DevExpress.XtraEditors.TextEdit txt确诊单位;
        private DevExpress.XtraEditors.CheckEdit checkEdit并发症情况;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private Library.UserControls.UCTxtLbl txt肾脏;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private Library.UserControls.UCTxtLbl txt胰岛素用量;
        private DevExpress.XtraEditors.RadioGroup radio胰岛素;
        private Library.UserControls.UCTxtLbl txt餐后血糖;
        private Library.UserControls.UCTxtLbl txt脑血管;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private Library.UserControls.UCTxtLbl txt足部;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private Library.UserControls.UCTxtLbl txt视网膜;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private Library.UserControls.UCTxtLbl txt心脏;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private Library.UserControls.UCTxtLbl txt神经;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.RadioGroup radio尿微量蛋白;
        private Library.UserControls.UCTxtLbl txt糖化血红蛋白;
        private DevExpress.XtraLayout.LayoutControl Layout1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layout终止理由;
        private DevExpress.XtraLayout.LayoutControlItem layout终止管理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layout糖尿病并发症;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layout用药列表;
        private DevExpress.XtraEditors.TextEdit txt管理卡编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private DevExpress.XtraEditors.LookUpEdit txt糖尿病类型;
    }
}
