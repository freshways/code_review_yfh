﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class Frm随访照片查看 : frmBase
    {
        public Frm随访照片查看()
        {
            InitializeComponent();
        }

        public Frm随访照片查看(Image img)
        {
            InitializeComponent();
            this.pictureEdit1.Image = img;
        }
    }
}
