﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC脑卒中患者随访记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC脑卒中患者随访记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt随访方式其他 = new System.Windows.Forms.TextBox();
            this.radio转诊情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt每次持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.radio不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.txt辅助检查 = new DevExpress.XtraEditors.TextEdit();
            this.txt遵医行为 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.LookUpEdit();
            this.radio脑卒中类型 = new DevExpress.XtraEditors.RadioGroup();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.fl康复治疗方式 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch康复治疗方式无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit46 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit47 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit48 = new DevExpress.XtraEditors.CheckEdit();
            this.ch康复治疗方式其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt康复治疗方式其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt新发卒中症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.fl新发卒中症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch新发卒中症状无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit33 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit37 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit44 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit45 = new DevExpress.XtraEditors.CheckEdit();
            this.fl并发症情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch并发症无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.ch并发症其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt并发症其他 = new DevExpress.XtraEditors.TextEdit();
            this.fl个人病史 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.ch个人病史无 = new DevExpress.XtraEditors.CheckEdit();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.check无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.ch目前症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt目前症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.fl随访分类 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch控制满意 = new DevExpress.XtraEditors.CheckEdit();
            this.ch控制不满意 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.ck随访并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.txt随访分类并发症 = new DevExpress.XtraEditors.TextEdit();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.txt脑卒中部位 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt肢体功能恢复情况 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt生活自理能力 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio脑卒中类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            this.fl康复治疗方式.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch康复治疗方式无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch康复治疗方式其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt康复治疗方式其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状其他.Properties)).BeginInit();
            this.fl新发卒中症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch新发卒中症状无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).BeginInit();
            this.fl并发症情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch并发症无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch并发症其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症其他.Properties)).BeginInit();
            this.fl个人病史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch个人病史无.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch目前症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            this.fl随访分类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制不满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck随访并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肢体功能恢复情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 0;
            this.btn重置.Text = "重置";
            this.btn重置.Visible = false;
            // 
            // btn填表说明
            // 
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 1;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt随访方式其他);
            this.layoutControl1.Controls.Add(this.radio转诊情况);
            this.layoutControl1.Controls.Add(this.txt每次持续时间);
            this.layoutControl1.Controls.Add(this.txt运动频率);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt体质指数);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.txt转诊科别);
            this.layoutControl1.Controls.Add(this.txt药物副作用详述);
            this.layoutControl1.Controls.Add(this.radio不良反应);
            this.layoutControl1.Controls.Add(this.txt辅助检查);
            this.layoutControl1.Controls.Add(this.txt遵医行为);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt摄盐情况2);
            this.layoutControl1.Controls.Add(this.txt摄盐情况1);
            this.layoutControl1.Controls.Add(this.radio脑卒中类型);
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.btn删除药物);
            this.layoutControl1.Controls.Add(this.btn添加药物);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.fl康复治疗方式);
            this.layoutControl1.Controls.Add(this.txt新发卒中症状其他);
            this.layoutControl1.Controls.Add(this.fl新发卒中症状);
            this.layoutControl1.Controls.Add(this.fl并发症情况);
            this.layoutControl1.Controls.Add(this.fl个人病史);
            this.layoutControl1.Controls.Add(this.fl症状);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Controls.Add(this.radio用药情况);
            this.layoutControl1.Controls.Add(this.fl随访分类);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt医生建议);
            this.layoutControl1.Controls.Add(this.txt脑卒中部位);
            this.layoutControl1.Controls.Add(this.txt肢体功能恢复情况);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Controls.Add(this.txt生活自理能力);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem23});
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(618, 275, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt随访方式其他
            // 
            this.txt随访方式其他.Location = new System.Drawing.Point(583, 144);
            this.txt随访方式其他.Name = "txt随访方式其他";
            this.txt随访方式其他.Size = new System.Drawing.Size(145, 20);
            this.txt随访方式其他.TabIndex = 132;
            // 
            // radio转诊情况
            // 
            this.radio转诊情况.EditValue = "2";
            this.radio转诊情况.Location = new System.Drawing.Point(108, 757);
            this.radio转诊情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio转诊情况.Name = "radio转诊情况";
            this.radio转诊情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio转诊情况.Size = new System.Drawing.Size(100, 25);
            this.radio转诊情况.StyleController = this.layoutControl1;
            this.radio转诊情况.TabIndex = 116;
            this.radio转诊情况.SelectedIndexChanged += new System.EventHandler(this.radio转诊情况_SelectedIndexChanged);
            // 
            // txt每次持续时间
            // 
            this.txt每次持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt每次持续时间.Lbl1Text = "/";
            this.txt每次持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt每次持续时间.Lbl2Text = "(分钟/次)";
            this.txt每次持续时间.Location = new System.Drawing.Point(465, 450);
            this.txt每次持续时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt每次持续时间.Name = "txt每次持续时间";
            this.txt每次持续时间.Size = new System.Drawing.Size(260, 20);
            this.txt每次持续时间.TabIndex = 98;
            this.txt每次持续时间.Txt1EditValue = null;
            this.txt每次持续时间.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt每次持续时间.Txt2EditValue = null;
            this.txt每次持续时间.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(111, 450);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(0);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(245, 20);
            this.txt运动频率.TabIndex = 96;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(465, 426);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(260, 20);
            this.txt饮酒情况.TabIndex = 95;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(111, 426);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(245, 20);
            this.txt日吸烟量.TabIndex = 94;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(465, 327);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(260, 20);
            this.txt体质指数.TabIndex = 131;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(20, 14);
            this.txt体重.Lbl2Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(111, 327);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(245, 20);
            this.txt体重.TabIndex = 130;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Enabled = false;
            this.txt转诊原因.Location = new System.Drawing.Point(530, 757);
            this.txt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(198, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 129;
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Enabled = false;
            this.txt转诊科别.Location = new System.Drawing.Point(277, 757);
            this.txt转诊科别.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(194, 20);
            this.txt转诊科别.StyleController = this.layoutControl1;
            this.txt转诊科别.TabIndex = 117;
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Enabled = false;
            this.txt药物副作用详述.Location = new System.Drawing.Point(455, 573);
            this.txt药物副作用详述.Margin = new System.Windows.Forms.Padding(0);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(273, 20);
            this.txt药物副作用详述.StyleController = this.layoutControl1;
            this.txt药物副作用详述.TabIndex = 110;
            // 
            // radio不良反应
            // 
            this.radio不良反应.EditValue = "1";
            this.radio不良反应.Location = new System.Drawing.Point(108, 573);
            this.radio不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.radio不良反应.Name = "radio不良反应";
            this.radio不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio不良反应.Size = new System.Drawing.Size(248, 25);
            this.radio不良反应.StyleController = this.layoutControl1;
            this.radio不良反应.TabIndex = 108;
            this.radio不良反应.SelectedIndexChanged += new System.EventHandler(this.radio不良反应_SelectedIndexChanged);
            // 
            // txt辅助检查
            // 
            this.txt辅助检查.Location = new System.Drawing.Point(108, 525);
            this.txt辅助检查.Margin = new System.Windows.Forms.Padding(0);
            this.txt辅助检查.Name = "txt辅助检查";
            this.txt辅助检查.Size = new System.Drawing.Size(620, 20);
            this.txt辅助检查.StyleController = this.layoutControl1;
            this.txt辅助检查.TabIndex = 106;
            this.txt辅助检查.ToolTip = "123";
            this.txt辅助检查.ToolTipTitle = "1";
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.Location = new System.Drawing.Point(111, 498);
            this.txt遵医行为.Margin = new System.Windows.Forms.Padding(0);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt遵医行为.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt遵医行为.Properties.NullText = "请选择";
            this.txt遵医行为.Properties.PopupSizeable = false;
            this.txt遵医行为.Size = new System.Drawing.Size(245, 20);
            this.txt遵医行为.StyleController = this.layoutControl1;
            this.txt遵医行为.TabIndex = 104;
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "请选择";
            this.txt心理调整.Location = new System.Drawing.Point(465, 474);
            this.txt心理调整.Margin = new System.Windows.Forms.Padding(0);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(260, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 128;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.Location = new System.Drawing.Point(239, 474);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(117, 20);
            this.txt摄盐情况2.StyleController = this.layoutControl1;
            this.txt摄盐情况2.TabIndex = 127;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.Location = new System.Drawing.Point(111, 474);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(110, 20);
            this.txt摄盐情况1.StyleController = this.layoutControl1;
            this.txt摄盐情况1.TabIndex = 126;
            // 
            // radio脑卒中类型
            // 
            this.radio脑卒中类型.EditValue = "-1";
            this.radio脑卒中类型.Location = new System.Drawing.Point(108, 168);
            this.radio脑卒中类型.Margin = new System.Windows.Forms.Padding(0);
            this.radio脑卒中类型.MaximumSize = new System.Drawing.Size(800, 0);
            this.radio脑卒中类型.Name = "radio脑卒中类型";
            this.radio脑卒中类型.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.radio脑卒中类型.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "缺血性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "蛛网膜下腔出血"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "脑出血"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "颈动脉内膜切除术"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "支架移入术"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("7", "未分类脑卒中")});
            this.radio脑卒中类型.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radio脑卒中类型.Size = new System.Drawing.Size(620, 21);
            this.radio脑卒中类型.StyleController = this.layoutControl1;
            this.radio脑卒中类型.TabIndex = 99;
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(725, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 5;
            this.lab考核项.Text = "考核项：27     缺项：0 完整度：100% ";
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(3, 682);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(725, 71);
            this.gcDetail.TabIndex = 94;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 337;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 370;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物.Image")));
            this.btn删除药物.Location = new System.Drawing.Point(418, 656);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物.StyleController = this.layoutControl1;
            this.btn删除药物.TabIndex = 97;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物.Image")));
            this.btn添加药物.Location = new System.Drawing.Point(360, 656);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(54, 22);
            this.btn添加药物.StyleController = this.layoutControl1;
            this.btn添加药物.TabIndex = 96;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(455, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(273, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 73;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(108, 120);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(238, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 72;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(455, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(273, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 71;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(108, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(238, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 70;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(455, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(273, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 69;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(108, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(238, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 68;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(455, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(273, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 67;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(108, 48);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(238, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 66;
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(111, 303);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(4);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(245, 20);
            this.txt血压值.TabIndex = 64;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt腰围.Lbl1Text = "cm";
            this.txt腰围.Location = new System.Drawing.Point(465, 351);
            this.txt腰围.Margin = new System.Windows.Forms.Padding(4);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(260, 20);
            this.txt腰围.TabIndex = 63;
            this.txt腰围.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // fl康复治疗方式
            // 
            this.fl康复治疗方式.Controls.Add(this.ch康复治疗方式无);
            this.fl康复治疗方式.Controls.Add(this.checkEdit46);
            this.fl康复治疗方式.Controls.Add(this.checkEdit47);
            this.fl康复治疗方式.Controls.Add(this.checkEdit48);
            this.fl康复治疗方式.Controls.Add(this.ch康复治疗方式其他);
            this.fl康复治疗方式.Controls.Add(this.txt康复治疗方式其他);
            this.fl康复治疗方式.Location = new System.Drawing.Point(108, 629);
            this.fl康复治疗方式.Margin = new System.Windows.Forms.Padding(0);
            this.fl康复治疗方式.Name = "fl康复治疗方式";
            this.fl康复治疗方式.Size = new System.Drawing.Size(620, 23);
            this.fl康复治疗方式.TabIndex = 54;
            // 
            // ch康复治疗方式无
            // 
            this.ch康复治疗方式无.EditValue = true;
            this.ch康复治疗方式无.Location = new System.Drawing.Point(0, 0);
            this.ch康复治疗方式无.Margin = new System.Windows.Forms.Padding(0);
            this.ch康复治疗方式无.Name = "ch康复治疗方式无";
            this.ch康复治疗方式无.Properties.Caption = "无";
            this.ch康复治疗方式无.Size = new System.Drawing.Size(52, 19);
            this.ch康复治疗方式无.TabIndex = 4;
            this.ch康复治疗方式无.Tag = "0";
            this.ch康复治疗方式无.CheckedChanged += new System.EventHandler(this.ch康复治疗方式无_CheckedChanged);
            // 
            // checkEdit46
            // 
            this.checkEdit46.Location = new System.Drawing.Point(52, 0);
            this.checkEdit46.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit46.Name = "checkEdit46";
            this.checkEdit46.Properties.Caption = "按摩";
            this.checkEdit46.Size = new System.Drawing.Size(53, 19);
            this.checkEdit46.TabIndex = 0;
            this.checkEdit46.Tag = "1";
            // 
            // checkEdit47
            // 
            this.checkEdit47.Location = new System.Drawing.Point(105, 0);
            this.checkEdit47.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit47.Name = "checkEdit47";
            this.checkEdit47.Properties.Caption = "针灸";
            this.checkEdit47.Size = new System.Drawing.Size(53, 19);
            this.checkEdit47.TabIndex = 1;
            this.checkEdit47.Tag = "2";
            // 
            // checkEdit48
            // 
            this.checkEdit48.Location = new System.Drawing.Point(158, 0);
            this.checkEdit48.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit48.Name = "checkEdit48";
            this.checkEdit48.Properties.Caption = "运动训练";
            this.checkEdit48.Size = new System.Drawing.Size(75, 19);
            this.checkEdit48.TabIndex = 2;
            this.checkEdit48.Tag = "3";
            // 
            // ch康复治疗方式其他
            // 
            this.ch康复治疗方式其他.Location = new System.Drawing.Point(233, 0);
            this.ch康复治疗方式其他.Margin = new System.Windows.Forms.Padding(0);
            this.ch康复治疗方式其他.Name = "ch康复治疗方式其他";
            this.ch康复治疗方式其他.Properties.Caption = "其他方式";
            this.ch康复治疗方式其他.Size = new System.Drawing.Size(75, 19);
            this.ch康复治疗方式其他.TabIndex = 3;
            this.ch康复治疗方式其他.Tag = "99";
            this.ch康复治疗方式其他.CheckedChanged += new System.EventHandler(this.ch康复治疗方式其他_CheckedChanged);
            // 
            // txt康复治疗方式其他
            // 
            this.txt康复治疗方式其他.Location = new System.Drawing.Point(308, 0);
            this.txt康复治疗方式其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt康复治疗方式其他.Name = "txt康复治疗方式其他";
            this.txt康复治疗方式其他.Size = new System.Drawing.Size(152, 20);
            this.txt康复治疗方式其他.TabIndex = 5;
            this.txt康复治疗方式其他.Visible = false;
            // 
            // txt新发卒中症状其他
            // 
            this.txt新发卒中症状其他.Location = new System.Drawing.Point(108, 272);
            this.txt新发卒中症状其他.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.txt新发卒中症状其他.Name = "txt新发卒中症状其他";
            this.txt新发卒中症状其他.Properties.AutoHeight = false;
            this.txt新发卒中症状其他.Size = new System.Drawing.Size(620, 36);
            this.txt新发卒中症状其他.StyleController = this.layoutControl1;
            this.txt新发卒中症状其他.TabIndex = 56;
            // 
            // fl新发卒中症状
            // 
            this.fl新发卒中症状.Controls.Add(this.ch新发卒中症状无);
            this.fl新发卒中症状.Controls.Add(this.checkEdit33);
            this.fl新发卒中症状.Controls.Add(this.checkEdit34);
            this.fl新发卒中症状.Controls.Add(this.checkEdit35);
            this.fl新发卒中症状.Controls.Add(this.checkEdit36);
            this.fl新发卒中症状.Controls.Add(this.checkEdit37);
            this.fl新发卒中症状.Controls.Add(this.checkEdit38);
            this.fl新发卒中症状.Controls.Add(this.checkEdit39);
            this.fl新发卒中症状.Controls.Add(this.checkEdit40);
            this.fl新发卒中症状.Controls.Add(this.checkEdit41);
            this.fl新发卒中症状.Controls.Add(this.checkEdit42);
            this.fl新发卒中症状.Controls.Add(this.checkEdit43);
            this.fl新发卒中症状.Controls.Add(this.checkEdit44);
            this.fl新发卒中症状.Controls.Add(this.checkEdit45);
            this.fl新发卒中症状.Location = new System.Drawing.Point(108, 272);
            this.fl新发卒中症状.Margin = new System.Windows.Forms.Padding(0);
            this.fl新发卒中症状.Name = "fl新发卒中症状";
            this.fl新发卒中症状.Size = new System.Drawing.Size(620, 41);
            this.fl新发卒中症状.TabIndex = 55;
            // 
            // ch新发卒中症状无
            // 
            this.ch新发卒中症状无.Location = new System.Drawing.Point(0, 0);
            this.ch新发卒中症状无.Margin = new System.Windows.Forms.Padding(0);
            this.ch新发卒中症状无.Name = "ch新发卒中症状无";
            this.ch新发卒中症状无.Properties.Caption = "无症状";
            this.ch新发卒中症状无.Size = new System.Drawing.Size(58, 19);
            this.ch新发卒中症状无.TabIndex = 21;
            this.ch新发卒中症状无.Tag = "0";
            this.ch新发卒中症状无.CheckedChanged += new System.EventHandler(this.ch新发卒中症状无_CheckedChanged);
            // 
            // checkEdit33
            // 
            this.checkEdit33.Location = new System.Drawing.Point(58, 0);
            this.checkEdit33.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit33.Name = "checkEdit33";
            this.checkEdit33.Properties.Caption = "构音障碍";
            this.checkEdit33.Size = new System.Drawing.Size(75, 19);
            this.checkEdit33.TabIndex = 22;
            this.checkEdit33.Tag = "1";
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(133, 0);
            this.checkEdit34.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "失语";
            this.checkEdit34.Size = new System.Drawing.Size(50, 19);
            this.checkEdit34.TabIndex = 23;
            this.checkEdit34.Tag = "2";
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(183, 0);
            this.checkEdit35.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "面瘫";
            this.checkEdit35.Size = new System.Drawing.Size(51, 19);
            this.checkEdit35.TabIndex = 24;
            this.checkEdit35.Tag = "3";
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(234, 0);
            this.checkEdit36.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Caption = "感觉障碍";
            this.checkEdit36.Size = new System.Drawing.Size(75, 19);
            this.checkEdit36.TabIndex = 25;
            this.checkEdit36.Tag = "4";
            // 
            // checkEdit37
            // 
            this.checkEdit37.Location = new System.Drawing.Point(309, 0);
            this.checkEdit37.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit37.Name = "checkEdit37";
            this.checkEdit37.Properties.Caption = "左侧肢体瘫痪";
            this.checkEdit37.Size = new System.Drawing.Size(96, 19);
            this.checkEdit37.TabIndex = 26;
            this.checkEdit37.Tag = "5";
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(405, 0);
            this.checkEdit38.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Caption = "共济失调";
            this.checkEdit38.Size = new System.Drawing.Size(75, 19);
            this.checkEdit38.TabIndex = 27;
            this.checkEdit38.Tag = "6";
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(480, 0);
            this.checkEdit39.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Caption = "昏迷";
            this.checkEdit39.Size = new System.Drawing.Size(52, 19);
            this.checkEdit39.TabIndex = 28;
            this.checkEdit39.Tag = "7";
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(0, 19);
            this.checkEdit40.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Caption = "右侧肢体瘫痪";
            this.checkEdit40.Size = new System.Drawing.Size(94, 19);
            this.checkEdit40.TabIndex = 29;
            this.checkEdit40.Tag = "8";
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(94, 19);
            this.checkEdit41.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Caption = "头疼";
            this.checkEdit41.Size = new System.Drawing.Size(47, 19);
            this.checkEdit41.TabIndex = 30;
            this.checkEdit41.Tag = "9";
            // 
            // checkEdit42
            // 
            this.checkEdit42.Location = new System.Drawing.Point(141, 19);
            this.checkEdit42.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit42.Name = "checkEdit42";
            this.checkEdit42.Properties.Caption = "呕吐";
            this.checkEdit42.Size = new System.Drawing.Size(49, 19);
            this.checkEdit42.TabIndex = 31;
            this.checkEdit42.Tag = "10";
            // 
            // checkEdit43
            // 
            this.checkEdit43.Location = new System.Drawing.Point(190, 19);
            this.checkEdit43.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit43.Name = "checkEdit43";
            this.checkEdit43.Properties.Caption = "意识障碍";
            this.checkEdit43.Size = new System.Drawing.Size(75, 19);
            this.checkEdit43.TabIndex = 32;
            this.checkEdit43.Tag = "11";
            // 
            // checkEdit44
            // 
            this.checkEdit44.Location = new System.Drawing.Point(265, 19);
            this.checkEdit44.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit44.Name = "checkEdit44";
            this.checkEdit44.Properties.Caption = "眩晕";
            this.checkEdit44.Size = new System.Drawing.Size(44, 19);
            this.checkEdit44.TabIndex = 33;
            this.checkEdit44.Tag = "12";
            // 
            // checkEdit45
            // 
            this.checkEdit45.Location = new System.Drawing.Point(309, 19);
            this.checkEdit45.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit45.Name = "checkEdit45";
            this.checkEdit45.Properties.Caption = "癫痫";
            this.checkEdit45.Size = new System.Drawing.Size(54, 19);
            this.checkEdit45.TabIndex = 34;
            this.checkEdit45.Tag = "13";
            // 
            // fl并发症情况
            // 
            this.fl并发症情况.Controls.Add(this.ch并发症无);
            this.fl并发症情况.Controls.Add(this.checkEdit27);
            this.fl并发症情况.Controls.Add(this.checkEdit28);
            this.fl并发症情况.Controls.Add(this.checkEdit29);
            this.fl并发症情况.Controls.Add(this.checkEdit30);
            this.fl并发症情况.Controls.Add(this.ch并发症其他);
            this.fl并发症情况.Controls.Add(this.txt并发症其他);
            this.fl并发症情况.Location = new System.Drawing.Point(108, 272);
            this.fl并发症情况.Margin = new System.Windows.Forms.Padding(0);
            this.fl并发症情况.Name = "fl并发症情况";
            this.fl并发症情况.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.fl并发症情况.Size = new System.Drawing.Size(620, 36);
            this.fl并发症情况.TabIndex = 54;
            // 
            // ch并发症无
            // 
            this.ch并发症无.Location = new System.Drawing.Point(1, 10);
            this.ch并发症无.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.ch并发症无.Name = "ch并发症无";
            this.ch并发症无.Properties.Caption = "无";
            this.ch并发症无.Size = new System.Drawing.Size(44, 19);
            this.ch并发症无.TabIndex = 0;
            this.ch并发症无.Tag = "0";
            this.ch并发症无.CheckedChanged += new System.EventHandler(this.ch并发症无_CheckedChanged);
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(45, 10);
            this.checkEdit27.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "褥疮";
            this.checkEdit27.Size = new System.Drawing.Size(53, 19);
            this.checkEdit27.TabIndex = 1;
            this.checkEdit27.Tag = "1";
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(98, 10);
            this.checkEdit28.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "呼吸道感染";
            this.checkEdit28.Size = new System.Drawing.Size(88, 19);
            this.checkEdit28.TabIndex = 2;
            this.checkEdit28.Tag = "2";
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(186, 10);
            this.checkEdit29.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "泌尿道感染";
            this.checkEdit29.Size = new System.Drawing.Size(89, 19);
            this.checkEdit29.TabIndex = 3;
            this.checkEdit29.Tag = "3";
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(275, 10);
            this.checkEdit30.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "深静脉炎";
            this.checkEdit30.Size = new System.Drawing.Size(75, 19);
            this.checkEdit30.TabIndex = 4;
            this.checkEdit30.Tag = "4";
            // 
            // ch并发症其他
            // 
            this.ch并发症其他.Location = new System.Drawing.Point(350, 10);
            this.ch并发症其他.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.ch并发症其他.Name = "ch并发症其他";
            this.ch并发症其他.Properties.Caption = "其他";
            this.ch并发症其他.Size = new System.Drawing.Size(56, 19);
            this.ch并发症其他.TabIndex = 5;
            this.ch并发症其他.Tag = "99";
            this.ch并发症其他.CheckedChanged += new System.EventHandler(this.ch并发症其他_CheckedChanged);
            // 
            // txt并发症其他
            // 
            this.txt并发症其他.Location = new System.Drawing.Point(406, 10);
            this.txt并发症其他.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.txt并发症其他.Name = "txt并发症其他";
            this.txt并发症其他.Size = new System.Drawing.Size(152, 20);
            this.txt并发症其他.TabIndex = 6;
            this.txt并发症其他.Visible = false;
            // 
            // fl个人病史
            // 
            this.fl个人病史.Controls.Add(this.checkEdit21);
            this.fl个人病史.Controls.Add(this.checkEdit22);
            this.fl个人病史.Controls.Add(this.checkEdit23);
            this.fl个人病史.Controls.Add(this.checkEdit24);
            this.fl个人病史.Controls.Add(this.ch个人病史无);
            this.fl个人病史.Location = new System.Drawing.Point(108, 253);
            this.fl个人病史.Margin = new System.Windows.Forms.Padding(0);
            this.fl个人病史.Name = "fl个人病史";
            this.fl个人病史.Size = new System.Drawing.Size(620, 23);
            this.fl个人病史.TabIndex = 53;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(0, 0);
            this.checkEdit21.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "冠心病";
            this.checkEdit21.Size = new System.Drawing.Size(71, 19);
            this.checkEdit21.TabIndex = 0;
            this.checkEdit21.Tag = "1";
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(71, 0);
            this.checkEdit22.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "高血压";
            this.checkEdit22.Size = new System.Drawing.Size(69, 19);
            this.checkEdit22.TabIndex = 1;
            this.checkEdit22.Tag = "2";
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(140, 0);
            this.checkEdit23.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "高血脂症";
            this.checkEdit23.Size = new System.Drawing.Size(75, 19);
            this.checkEdit23.TabIndex = 2;
            this.checkEdit23.Tag = "3";
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(215, 0);
            this.checkEdit24.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "糖尿病";
            this.checkEdit24.Size = new System.Drawing.Size(75, 19);
            this.checkEdit24.TabIndex = 3;
            this.checkEdit24.Tag = "4";
            // 
            // ch个人病史无
            // 
            this.ch个人病史无.EditValue = true;
            this.ch个人病史无.Location = new System.Drawing.Point(290, 0);
            this.ch个人病史无.Margin = new System.Windows.Forms.Padding(0);
            this.ch个人病史无.Name = "ch个人病史无";
            this.ch个人病史无.Properties.Caption = "无";
            this.ch个人病史无.Size = new System.Drawing.Size(75, 19);
            this.ch个人病史无.TabIndex = 4;
            this.ch个人病史无.Tag = "100";
            this.ch个人病史无.CheckedChanged += new System.EventHandler(this.ch个人病史无_CheckedChanged);
            // 
            // fl症状
            // 
            this.fl症状.Controls.Add(this.check无症状);
            this.fl症状.Controls.Add(this.checkEdit16);
            this.fl症状.Controls.Add(this.checkEdit17);
            this.fl症状.Controls.Add(this.checkEdit18);
            this.fl症状.Controls.Add(this.checkEdit19);
            this.fl症状.Controls.Add(this.checkEdit1);
            this.fl症状.Controls.Add(this.checkEdit2);
            this.fl症状.Controls.Add(this.checkEdit4);
            this.fl症状.Controls.Add(this.ch目前症状其他);
            this.fl症状.Controls.Add(this.txt目前症状其他);
            this.fl症状.Location = new System.Drawing.Point(108, 203);
            this.fl症状.Margin = new System.Windows.Forms.Padding(0);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(620, 46);
            this.fl症状.TabIndex = 52;
            // 
            // check无症状
            // 
            this.check无症状.EditValue = true;
            this.check无症状.Location = new System.Drawing.Point(0, 0);
            this.check无症状.Margin = new System.Windows.Forms.Padding(0);
            this.check无症状.Name = "check无症状";
            this.check无症状.Properties.Caption = "1.无";
            this.check无症状.Size = new System.Drawing.Size(71, 19);
            this.check无症状.TabIndex = 0;
            this.check无症状.Tag = "0";
            this.check无症状.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(71, 0);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "2.清醒";
            this.checkEdit16.Size = new System.Drawing.Size(82, 19);
            this.checkEdit16.TabIndex = 1;
            this.checkEdit16.Tag = "1";
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(153, 0);
            this.checkEdit17.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "3.嗜睡";
            this.checkEdit17.Size = new System.Drawing.Size(80, 19);
            this.checkEdit17.TabIndex = 2;
            this.checkEdit17.Tag = "2";
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(233, 0);
            this.checkEdit18.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "4.昏迷";
            this.checkEdit18.Size = new System.Drawing.Size(75, 19);
            this.checkEdit18.TabIndex = 3;
            this.checkEdit18.Tag = "3";
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(308, 0);
            this.checkEdit19.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "5.烦躁";
            this.checkEdit19.Size = new System.Drawing.Size(81, 19);
            this.checkEdit19.TabIndex = 4;
            this.checkEdit19.Tag = "4";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(389, 0);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "6.面瘫";
            this.checkEdit1.Size = new System.Drawing.Size(81, 19);
            this.checkEdit1.TabIndex = 7;
            this.checkEdit1.Tag = "5";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(470, 0);
            this.checkEdit2.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "7.不能平卧";
            this.checkEdit2.Size = new System.Drawing.Size(83, 19);
            this.checkEdit2.TabIndex = 8;
            this.checkEdit2.Tag = "6";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(0, 19);
            this.checkEdit4.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "8.下肢水肿";
            this.checkEdit4.Size = new System.Drawing.Size(95, 19);
            this.checkEdit4.TabIndex = 9;
            this.checkEdit4.Tag = "7";
            // 
            // ch目前症状其他
            // 
            this.ch目前症状其他.Location = new System.Drawing.Point(95, 19);
            this.ch目前症状其他.Margin = new System.Windows.Forms.Padding(0);
            this.ch目前症状其他.Name = "ch目前症状其他";
            this.ch目前症状其他.Properties.Caption = "9.其他症状";
            this.ch目前症状其他.Size = new System.Drawing.Size(87, 19);
            this.ch目前症状其他.TabIndex = 5;
            this.ch目前症状其他.Tag = "99";
            this.ch目前症状其他.CheckedChanged += new System.EventHandler(this.ch目前症状其他_CheckedChanged);
            // 
            // txt目前症状其他
            // 
            this.txt目前症状其他.Location = new System.Drawing.Point(182, 19);
            this.txt目前症状其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt目前症状其他.Name = "txt目前症状其他";
            this.txt目前症状其他.Size = new System.Drawing.Size(106, 20);
            this.txt目前症状其他.TabIndex = 6;
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(108, 894);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(247, 18);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 51;
            this.lab创建机构.Text = "labelControl16";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(464, 872);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(264, 18);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 50;
            this.lab最近修改人.Text = "labelControl15";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(108, 872);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(247, 18);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 49;
            this.lab创建人.Text = "labelControl14";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(464, 894);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(264, 18);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 48;
            this.lab当前所属机构.Text = "labelControl13";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(464, 850);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(264, 18);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 47;
            this.lbl最近更新时间.Text = "labelControl12";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(108, 850);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(247, 18);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "labelControl11";
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(356, 826);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(372, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 45;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(108, 826);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Size = new System.Drawing.Size(149, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 44;
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(108, 656);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio用药情况.Size = new System.Drawing.Size(248, 22);
            this.radio用药情况.StyleController = this.layoutControl1;
            this.radio用药情况.TabIndex = 39;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // fl随访分类
            // 
            this.fl随访分类.Controls.Add(this.ch控制满意);
            this.fl随访分类.Controls.Add(this.ch控制不满意);
            this.fl随访分类.Controls.Add(this.checkEdit3);
            this.fl随访分类.Controls.Add(this.ck随访并发症);
            this.fl随访分类.Controls.Add(this.txt随访分类并发症);
            this.fl随访分类.Location = new System.Drawing.Point(108, 602);
            this.fl随访分类.Name = "fl随访分类";
            this.fl随访分类.Size = new System.Drawing.Size(620, 23);
            this.fl随访分类.TabIndex = 37;
            // 
            // ch控制满意
            // 
            this.ch控制满意.Location = new System.Drawing.Point(0, 0);
            this.ch控制满意.Margin = new System.Windows.Forms.Padding(0);
            this.ch控制满意.Name = "ch控制满意";
            this.ch控制满意.Properties.Caption = "控制满意";
            this.ch控制满意.Size = new System.Drawing.Size(72, 19);
            this.ch控制满意.TabIndex = 15;
            this.ch控制满意.Tag = "1";
            this.ch控制满意.CheckedChanged += new System.EventHandler(this.ch控制满意_CheckedChanged);
            // 
            // ch控制不满意
            // 
            this.ch控制不满意.Location = new System.Drawing.Point(72, 0);
            this.ch控制不满意.Margin = new System.Windows.Forms.Padding(0);
            this.ch控制不满意.Name = "ch控制不满意";
            this.ch控制不满意.Properties.Caption = "控制不满意";
            this.ch控制不满意.Size = new System.Drawing.Size(81, 19);
            this.ch控制不满意.TabIndex = 16;
            this.ch控制不满意.Tag = "2";
            this.ch控制不满意.CheckedChanged += new System.EventHandler(this.ch控制满意_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(153, 0);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "不良反应";
            this.checkEdit3.Size = new System.Drawing.Size(69, 19);
            this.checkEdit3.TabIndex = 17;
            this.checkEdit3.Tag = "3";
            // 
            // ck随访并发症
            // 
            this.ck随访并发症.Location = new System.Drawing.Point(222, 0);
            this.ck随访并发症.Margin = new System.Windows.Forms.Padding(0);
            this.ck随访并发症.Name = "ck随访并发症";
            this.ck随访并发症.Properties.Caption = "并发症";
            this.ck随访并发症.Size = new System.Drawing.Size(63, 19);
            this.ck随访并发症.TabIndex = 18;
            this.ck随访并发症.Tag = "4";
            this.ck随访并发症.CheckedChanged += new System.EventHandler(this.ck随访并发症_CheckedChanged);
            // 
            // txt随访分类并发症
            // 
            this.txt随访分类并发症.Location = new System.Drawing.Point(285, 0);
            this.txt随访分类并发症.Margin = new System.Windows.Forms.Padding(0);
            this.txt随访分类并发症.Name = "txt随访分类并发症";
            this.txt随访分类并发症.Size = new System.Drawing.Size(146, 20);
            this.txt随访分类并发症.TabIndex = 19;
            this.txt随访分类并发症.Visible = false;
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(111, 351);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(245, 20);
            this.txt空腹血糖.TabIndex = 28;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(130, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(465, 303);
            this.txt身高.Margin = new System.Windows.Forms.Padding(4);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(260, 20);
            this.txt身高.TabIndex = 17;
            this.txt身高.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // radio随访方式
            // 
            this.radio随访方式.Location = new System.Drawing.Point(352, 144);
            this.radio随访方式.Margin = new System.Windows.Forms.Padding(0);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio随访方式.Size = new System.Drawing.Size(227, 20);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 13;
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(108, 144);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Size = new System.Drawing.Size(135, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 12;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(108, 786);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(620, 36);
            this.txt医生建议.StyleController = this.layoutControl1;
            this.txt医生建议.TabIndex = 38;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // txt脑卒中部位
            // 
            this.txt脑卒中部位.Location = new System.Drawing.Point(108, 193);
            this.txt脑卒中部位.Name = "txt脑卒中部位";
            this.txt脑卒中部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt脑卒中部位.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt脑卒中部位.Properties.NullText = "";
            this.txt脑卒中部位.Properties.PopupSizeable = false;
            this.txt脑卒中部位.Size = new System.Drawing.Size(154, 20);
            this.txt脑卒中部位.StyleController = this.layoutControl1;
            this.txt脑卒中部位.TabIndex = 26;
            // 
            // txt肢体功能恢复情况
            // 
            this.txt肢体功能恢复情况.EditValue = "1";
            this.txt肢体功能恢复情况.Location = new System.Drawing.Point(111, 375);
            this.txt肢体功能恢复情况.Name = "txt肢体功能恢复情况";
            this.txt肢体功能恢复情况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt肢体功能恢复情况.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt肢体功能恢复情况.Properties.NullText = "";
            this.txt肢体功能恢复情况.Properties.PopupSizeable = false;
            this.txt肢体功能恢复情况.Size = new System.Drawing.Size(245, 20);
            this.txt肢体功能恢复情况.StyleController = this.layoutControl1;
            this.txt肢体功能恢复情况.TabIndex = 65;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(108, 549);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(248, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 33;
            this.txt服药依从性.EditValueChanged += new System.EventHandler(this.txt服药依从性_EditValueChanged);
            // 
            // txt生活自理能力
            // 
            this.txt生活自理能力.Location = new System.Drawing.Point(455, 549);
            this.txt生活自理能力.Name = "txt生活自理能力";
            this.txt生活自理能力.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt生活自理能力.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt生活自理能力.Properties.NullText = "";
            this.txt生活自理能力.Properties.PopupSizeable = false;
            this.txt生活自理能力.Size = new System.Drawing.Size(273, 20);
            this.txt生活自理能力.StyleController = this.layoutControl1;
            this.txt生活自理能力.TabIndex = 36;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem51.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem51.Control = this.fl并发症情况;
            this.layoutControlItem51.CustomizationFormText = "脑卒中并发症情况";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(211, 40);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(729, 40);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Tag = "check";
            this.layoutControlItem51.Text = "脑卒中并发症情况";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem52.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem52.Control = this.fl新发卒中症状;
            this.layoutControlItem52.CustomizationFormText = "新发卒中症状";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem52.MinSize = new System.Drawing.Size(203, 45);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(729, 45);
            this.layoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem52.Tag = "check";
            this.layoutControlItem52.Text = "新发卒中症状";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem53.Control = this.txt新发卒中症状其他;
            this.layoutControlItem53.CustomizationFormText = "其他新发卒中症状";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem53.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(161, 40);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(729, 40);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Tag = "";
            this.layoutControlItem53.Text = "其他新发卒中症状";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt脑卒中部位;
            this.layoutControlItem23.CustomizationFormText = "心理调整";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 163);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(263, 25);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "脑卒中部位";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlGroup5,
            this.emptySpaceItem4,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem36,
            this.emptySpaceItem3,
            this.layout添加药物,
            this.layout删除药物,
            this.layout药物列表,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem35,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem45,
            this.layoutControlItem7,
            this.layoutControlItem30,
            this.layoutControlItem33,
            this.layoutControlItem8,
            this.layoutControlItem11,
            this.layoutControlItem34,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem54,
            this.layoutControlItem39,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem40});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 916);
            this.layoutControlGroup1.Text = "脑卒中患者随访记录表";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt发生时间;
            this.layoutControlItem9.CustomizationFormText = "随访日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(169, 22);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "随访日期";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.radio随访方式;
            this.layoutControlItem10.CustomizationFormText = "随访方式";
            this.layoutControlItem10.Location = new System.Drawing.Point(244, 114);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(336, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "随访方式";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 886);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(729, 1);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 163);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(729, 10);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem49.Control = this.fl症状;
            this.layoutControlItem49.CustomizationFormText = "目前症状";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(211, 50);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Tag = "check";
            this.layoutControlItem49.Text = "目前症状";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem50.Control = this.fl个人病史;
            this.layoutControlItem50.CustomizationFormText = "个人病史";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 223);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(211, 27);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(729, 27);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Tag = "check";
            this.layoutControlItem50.Text = "个人病史";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.radio用药情况;
            this.layoutControlItem36.CustomizationFormText = "用药情况";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 626);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(357, 26);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Tag = "check";
            this.layoutControlItem36.Text = "用药情况";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(473, 626);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(256, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layout添加药物";
            this.layout添加药物.Location = new System.Drawing.Point(357, 626);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(58, 26);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layout删除药物";
            this.layout删除药物.Location = new System.Drawing.Point(415, 626);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(58, 26);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = "layout药物列表";
            this.layout药物列表.Location = new System.Drawing.Point(0, 652);
            this.layout药物列表.MinSize = new System.Drawing.Size(212, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(729, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = "layout药物列表";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lab考核项;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(729, 18);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.radio脑卒中类型;
            this.layoutControlItem3.CustomizationFormText = "脑卒中类型";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(160, 25);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(729, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "卒中诊断";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem41.Control = this.txt下次随访时间;
            this.layoutControlItem41.CustomizationFormText = "下次随访时间";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 796);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem41.Tag = "check";
            this.layoutControlItem41.Text = "下次随访时间";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem42.Control = this.txt医生签名;
            this.layoutControlItem42.CustomizationFormText = "随访医生签名";
            this.layoutControlItem42.Location = new System.Drawing.Point(258, 796);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(471, 24);
            this.layoutControlItem42.Tag = "check";
            this.layoutControlItem42.Text = "随访医生签名";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem35.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem35.Control = this.txt医生建议;
            this.layoutControlItem35.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 756);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(109, 40);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(729, 40);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Tag = "check";
            this.layoutControlItem35.Text = "此次随访医生建议";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "体  征";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.emptySpaceItem1,
            this.layoutControlItem15,
            this.layoutControlItem14,
            this.layoutControlItem29,
            this.layoutControlItem31,
            this.layoutControlItem25,
            this.layoutControlItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 250);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 123);
            this.layoutControlGroup2.Text = "体  征";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt肢体功能恢复情况;
            this.layoutControlItem16.CustomizationFormText = "肢体功能恢复情况";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(354, 25);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "肢体功能恢复情况";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(354, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(369, 25);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt血压值;
            this.layoutControlItem15.CustomizationFormText = "血压值";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(203, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "血压值";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt身高;
            this.layoutControlItem14.CustomizationFormText = "身高";
            this.layoutControlItem14.Location = new System.Drawing.Point(354, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Tag = "";
            this.layoutControlItem14.Text = "身高";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt体重;
            this.layoutControlItem29.CustomizationFormText = "体重";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem29.Tag = "check";
            this.layoutControlItem29.Text = "体重";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt体质指数;
            this.layoutControlItem31.CustomizationFormText = "体质指数";
            this.layoutControlItem31.Location = new System.Drawing.Point(354, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem31.Text = "体质指数";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.txt空腹血糖;
            this.layoutControlItem25.CustomizationFormText = "空腹血糖";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem25.Tag = "";
            this.layoutControlItem25.Text = "空腹血糖";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt腰围;
            this.layoutControlItem13.CustomizationFormText = "腰围";
            this.layoutControlItem13.Location = new System.Drawing.Point(354, 48);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(203, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Tag = "";
            this.layoutControlItem13.Text = "腰围";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "生活行为";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem12,
            this.layoutControlItem32});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 373);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 122);
            this.layoutControlGroup3.Text = "生活方式指导";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt摄盐情况1;
            this.layoutControlItem2.CustomizationFormText = "摄盐情况";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "摄盐情况";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt摄盐情况2;
            this.layoutControlItem4.CustomizationFormText = "~";
            this.layoutControlItem4.Location = new System.Drawing.Point(219, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem4.Text = "~";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt心理调整;
            this.layoutControlItem5.CustomizationFormText = "心理调整";
            this.layoutControlItem5.Location = new System.Drawing.Point(354, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "心理调整";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt遵医行为;
            this.layoutControlItem6.CustomizationFormText = "遵医行为";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "遵医行为";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(354, 72);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(369, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.txt运动频率;
            this.layoutControlItem37.CustomizationFormText = "运动频率";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem37.Tag = "check";
            this.layoutControlItem37.Text = "运动频率";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt每次持续时间;
            this.layoutControlItem38.CustomizationFormText = "每次持续时间";
            this.layoutControlItem38.Location = new System.Drawing.Point(354, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem38.Tag = "check";
            this.layoutControlItem38.Text = "每次持续时间";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt日吸烟量;
            this.layoutControlItem12.CustomizationFormText = "吸烟情况";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "吸烟情况";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt饮酒情况;
            this.layoutControlItem32.CustomizationFormText = "饮酒情况";
            this.layoutControlItem32.Location = new System.Drawing.Point(354, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem32.Tag = "check";
            this.layoutControlItem32.Text = "饮酒情况";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lbl创建时间;
            this.layoutControlItem43.CustomizationFormText = "layoutControlItem43";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 820);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(356, 22);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem43.TextToControlDistance = 0;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lbl最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "layoutControlItem44";
            this.layoutControlItem44.Location = new System.Drawing.Point(356, 820);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(373, 22);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem44.TextToControlDistance = 0;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab创建人;
            this.layoutControlItem46.CustomizationFormText = "layoutControlItem46";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 842);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(356, 22);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "录入人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem46.TextToControlDistance = 0;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lab最近修改人;
            this.layoutControlItem47.CustomizationFormText = "layoutControlItem47";
            this.layoutControlItem47.Location = new System.Drawing.Point(356, 842);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(373, 22);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "最近更新人";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem47.TextToControlDistance = 0;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lab创建机构;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 864);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(356, 22);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "创建机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem48.TextToControlDistance = 0;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lab当前所属机构;
            this.layoutControlItem45.CustomizationFormText = "layoutControlItem45";
            this.layoutControlItem45.Location = new System.Drawing.Point(356, 864);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(373, 22);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "当前所属机构";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(105, 22);
            this.layoutControlItem45.TextToControlDistance = 0;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt辅助检查;
            this.layoutControlItem7.CustomizationFormText = "辅助检查*";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 495);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem7.Tag = "check";
            this.layoutControlItem7.Text = "辅助检查*";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem30.Control = this.txt服药依从性;
            this.layoutControlItem30.CustomizationFormText = "服药依从性";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 519);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "用药依从性";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem33.Control = this.txt生活自理能力;
            this.layoutControlItem33.CustomizationFormText = "生活能否自理能力";
            this.layoutControlItem33.Location = new System.Drawing.Point(357, 519);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "生活能否自理能力";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.radio不良反应;
            this.layoutControlItem8.CustomizationFormText = "药物不良反应";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 543);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(357, 29);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "药物不良反应";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt药物副作用详述;
            this.layoutControlItem11.CustomizationFormText = "副作用详述";
            this.layoutControlItem11.Location = new System.Drawing.Point(357, 543);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(372, 29);
            this.layoutControlItem11.Text = "副作用详述";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem34.Control = this.fl随访分类;
            this.layoutControlItem34.CustomizationFormText = "此次随访分类";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 572);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(209, 27);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(729, 27);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "此次随访分类";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt转诊科别;
            this.layoutControlItem27.CustomizationFormText = "机构及科别";
            this.layoutControlItem27.Location = new System.Drawing.Point(209, 727);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(263, 29);
            this.layoutControlItem27.Text = "机构及科别";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt转诊原因;
            this.layoutControlItem28.CustomizationFormText = "原因";
            this.layoutControlItem28.Location = new System.Drawing.Point(472, 727);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(257, 29);
            this.layoutControlItem28.Text = "原因";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem54.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem54.Control = this.fl康复治疗方式;
            this.layoutControlItem54.CustomizationFormText = "康复治疗的方式";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 599);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(191, 27);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(729, 27);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Tag = "check";
            this.layoutControlItem54.Text = "康复治疗的方式";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.radio转诊情况;
            this.layoutControlItem39.CustomizationFormText = "转诊情况";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 727);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(209, 29);
            this.layoutControlItem39.Tag = "check";
            this.layoutControlItem39.Text = "转诊情况";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt性别;
            this.layoutControlItem19.CustomizationFormText = "性别";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(347, 24);
            this.layoutControlItem19.Text = "性别";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt出生日期;
            this.layoutControlItem20.CustomizationFormText = "出生日期";
            this.layoutControlItem20.Location = new System.Drawing.Point(347, 42);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem20.Text = "出生日期";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt联系电话;
            this.layoutControlItem24.CustomizationFormText = "联系电话";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(347, 24);
            this.layoutControlItem24.Text = "联系电话";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt居住地址;
            this.layoutControlItem26.CustomizationFormText = "居住地址";
            this.layoutControlItem26.Location = new System.Drawing.Point(347, 90);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem26.Text = "居住地址";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt身份证号;
            this.layoutControlItem21.CustomizationFormText = "身份证号";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(347, 24);
            this.layoutControlItem21.Text = "身份证号";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt职业;
            this.layoutControlItem22.CustomizationFormText = "职业";
            this.layoutControlItem22.Location = new System.Drawing.Point(347, 66);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem22.Text = "职业";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt个人档案号;
            this.layoutControlItem17.CustomizationFormText = "个人档案号";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(347, 24);
            this.layoutControlItem17.Text = "个人档案号";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt姓名;
            this.layoutControlItem18.CustomizationFormText = "姓名";
            this.layoutControlItem18.Location = new System.Drawing.Point(347, 18);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem18.Text = "姓名";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.txt随访方式其他;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem40";
            this.layoutControlItem40.Location = new System.Drawing.Point(580, 114);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(149, 24);
            this.layoutControlItem40.Text = "layoutControlItem40";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // UC脑卒中患者随访记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC脑卒中患者随访记录表";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC脑卒中患者随访记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio脑卒中类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            this.fl康复治疗方式.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch康复治疗方式无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch康复治疗方式其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt康复治疗方式其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新发卒中症状其他.Properties)).EndInit();
            this.fl新发卒中症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch新发卒中症状无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).EndInit();
            this.fl并发症情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch并发症无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch并发症其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症其他.Properties)).EndInit();
            this.fl个人病史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch个人病史无.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch目前症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            this.fl随访分类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch控制满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制不满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck随访并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑卒中部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肢体功能恢复情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private Library.UserControls.UCTxtLbl txt身高;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private DevExpress.XtraEditors.CheckEdit ch控制满意;
        private DevExpress.XtraEditors.CheckEdit ch控制不满意;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit ck随访并发症;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private System.Windows.Forms.FlowLayoutPanel fl新发卒中症状;
        private System.Windows.Forms.FlowLayoutPanel fl并发症情况;
        private DevExpress.XtraEditors.CheckEdit ch并发症无;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit ch并发症其他;
        private System.Windows.Forms.FlowLayoutPanel fl个人病史;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit ch个人病史无;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraEditors.CheckEdit check无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit ch目前症状其他;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private System.Windows.Forms.FlowLayoutPanel fl康复治疗方式;
        private DevExpress.XtraEditors.CheckEdit ch康复治疗方式无;
        private DevExpress.XtraEditors.CheckEdit checkEdit46;
        private DevExpress.XtraEditors.CheckEdit checkEdit47;
        private DevExpress.XtraEditors.CheckEdit checkEdit48;
        private DevExpress.XtraEditors.CheckEdit ch康复治疗方式其他;
        private DevExpress.XtraEditors.TextEdit txt新发卒中症状其他;
        private DevExpress.XtraEditors.CheckEdit ch新发卒中症状无;
        private DevExpress.XtraEditors.CheckEdit checkEdit33;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraEditors.CheckEdit checkEdit37;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraEditors.CheckEdit checkEdit42;
        private DevExpress.XtraEditors.CheckEdit checkEdit43;
        private DevExpress.XtraEditors.CheckEdit checkEdit44;
        private DevExpress.XtraEditors.CheckEdit checkEdit45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private Library.UserControls.UCTxtLbl txt腰围;
        private DevExpress.XtraEditors.TextEdit txt随访分类并发症;
        private DevExpress.XtraEditors.TextEdit txt并发症其他;
        private DevExpress.XtraEditors.TextEdit txt目前症状其他;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraEditors.TextEdit txt康复治疗方式其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit txt脑卒中部位;
        private DevExpress.XtraEditors.LookUpEdit txt肢体功能恢复情况;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
        private DevExpress.XtraEditors.LookUpEdit txt生活自理能力;
        private DevExpress.XtraEditors.RadioGroup radio脑卒中类型;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LookUpEdit txt遵医行为;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txt辅助检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.RadioGroup radio不良反应;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private Library.UserControls.UCTxtLblTxtLbl txt运动频率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private Library.UserControls.UCTxtLblTxtLbl txt每次持续时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.RadioGroup radio转诊情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private System.Windows.Forms.TextBox txt随访方式其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
    }
}
