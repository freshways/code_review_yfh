﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using System.Collections.Generic;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class report2型糖尿病患者随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        string docNo;
        List<string> dates;
        DataSet _ds糖尿病;
        DataSet _ds用药情况;
        bllMXB糖尿病随访表 _bll糖尿病 = new bllMXB糖尿病随访表();
        #endregion
        public report2型糖尿病患者随访服务记录表()
        {
            InitializeComponent();
        }

        public report2型糖尿病患者随访服务记录表(string docNo, List<string> _dates)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.dates = _dates;
            _ds糖尿病 = _bll糖尿病.GetInfoByTNB(docNo, dates, true);
            DoBindingDataSource(_ds糖尿病);
        }

        private void DoBindingDataSource(DataSet _ds糖尿病)
        {
            DataTable dt糖尿病 = _ds糖尿病.Tables[Models.tb_MXB糖尿病随访表.__TableName];

            //用药情况：根据个人档案编号遍历糖尿病随访表的创建时间来获取用药情况
            for (int i = 0; i < dt糖尿病.Rows.Count;i++ )
            {
                string year=dt糖尿病.Rows[i][Models.tb_MXB糖尿病随访表.创建时间].ToString();
                _ds用药情况 = new bllMXB糖尿病随访表().GetInfoByYY(docNo,year,false);
                if(i==0)
                {
                    BindYYQK1();
                }
                else if (i == 1)
                {
                    BindYYQK2();
                }
                else if (i == 2)
                {
                    BindYYQK3();
                }
                else if (i == 3)
                {
                    BindYYQK4();
                }
            }

            #region 用药情况方法2
            //for (int i = 0; i < dt糖尿病.Rows.Count;i++ )
            //{
            //    string year = dt糖尿病.Rows[i][Models.tb_MXB糖尿病随访表.创建时间].ToString();
            //    _ds用药情况 = new bllMXB糖尿病随访表().GetInfoByYY(docNo, year, false);
            //  switch(i)
            //  {
            //      case 0:
            //          BindYYQK1();
            //          break;
            //      case 1:
            //          BindYYQK2();
            //          break;
            //      case 2:
            //          BindYYQK3();
            //          break;
            //      case 3:
            //          BindYYQK4();
            //          break;
            //      default:
            //          break;
            //  }
            //}
            #endregion

            DataTable dt健康档案 = _ds糖尿病.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][Models.tb_健康档案.姓名].ToString());

            #region 第1次随访记录

            if (dt糖尿病 != null && dt糖尿病.Rows.Count > 0)
            {
                this.txt第1次随访日期.Text = Convert.ToDateTime(dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第1次随访方式.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.随访方式].ToString();
                //第1次随访症状
                BindZZ1(dt糖尿病);
                this.txt第1次随访症状其他.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.目前症状其他].ToString();
                this.txt第1次随访血压1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.收缩压].ToString();//高压
                this.txt第1次随访血压2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.舒张压].ToString();//低压
                this.txt第1次随访体重1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.体重].ToString();
                this.txt第1次随访体重2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.体重2].ToString();
                this.txt第1次随访体质指数1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.BMI].ToString();
                this.txt第1次随访体质指数2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.BMI2].ToString();
                this.txt第1次随访足背动脉搏动.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.足背动脉搏动].ToString();
                this.txt第1次随访体征其他.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.体征其他].ToString();
                this.txt第1次随访日吸烟量1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.吸烟数量].ToString();
                this.txt第1次随访日吸烟量2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.吸烟数量2].ToString();
                this.txt第1次随访日饮酒量1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.饮酒数量].ToString();
                this.txt第1次随访日饮酒量2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.饮酒数量2].ToString();
                this.txt第1次随访运动次数1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.运动频率].ToString();
                this.txt第1次随访运动时间1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.运动持续时间].ToString();
                this.txt第1次随访运动次数2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.运动频率2].ToString();
                this.txt第1次随访运动时间2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.运动持续时间2].ToString();
                this.txt第1次随访主食量1.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.适合主食].ToString();
                this.txt第1次随访主食量2.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.适合主食2].ToString();
                this.txt第1次随访心理调整.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.心理调整].ToString();
                this.txt第1次随访遵医行为.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.遵医行为].ToString();
                this.txt第1次随访空腹血糖.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.空腹血糖].ToString();
                this.txt第1次随访血红蛋白.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.糖化血红蛋白].ToString();
                this.txt第1次随访检查日期.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.辅助检查日期].ToString();
                //辅助检查其他
                this.txt第1次随访服药依从性.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.服药依从性].ToString();
                this.txt第1次随访药物不良反应.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.药物副作用].ToString();
                this.txt第1次随访药物不良反应其他.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.副作用详述].ToString();
                this.txt第1次低血糖反应.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.低血糖反应].ToString();
                this.txt第1次随访分类.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.本次随访分类].ToString();
                //this.txt第1次随访胰岛素种类.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.胰岛素种类].ToString();
                //this.txt第1次随访胰岛素用法.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.胰岛素用法].ToString();
                this.txt第1次随访转诊原因.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.转诊原因].ToString();
                this.txt第1次随访转诊机构.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.转诊科别].ToString();
                this.txt第1次随访下次随访日期.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.下次随访时间].ToString();
                this.txt第1次随访医生签名.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.随访医生].ToString();
            }
            #endregion

            #region 第2次随访记录

            if (dt糖尿病 != null && dt糖尿病.Rows.Count > 1)
            {
                this.txt第2次随访日期.Text = Convert.ToDateTime(dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第2次随访方式.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.随访方式].ToString();
                //第2次随访症状
                BindZZ2(dt糖尿病);
                this.txt第2次随访症状其他.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.目前症状其他].ToString();
                this.txt第2次随访血压1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.收缩压].ToString();
                this.txt第2次随访血压2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.舒张压].ToString();
                this.txt第2次随访体重1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.体重].ToString();
                this.txt第2次随访体重2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.体重2].ToString();
                this.txt第2次随访体质指数1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.BMI].ToString();
                this.txt第2次随访体质指数2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.BMI2].ToString();
                this.txt第2次随访足背动脉搏动.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.足背动脉搏动].ToString();
                this.txt第2次随访体征其他.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.体征其他].ToString();
                this.txt第2次随访日吸烟量1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.吸烟数量].ToString();
                this.txt第2次随访日吸烟量2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.吸烟数量2].ToString();
                this.txt第2次随访日饮酒量1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.饮酒数量].ToString();
                this.txt第2次随访日饮酒量2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.饮酒数量2].ToString();
                this.txt第2次随访运动次数1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.运动频率].ToString();
                this.txt第2次随访运动时间1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.运动持续时间].ToString();
                this.txt第2次随访运动次数2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.运动频率2].ToString();
                this.txt第2次随访运动时间2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.运动持续时间2].ToString();
                this.txt第2次随访主食量1.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.适合主食].ToString();
                this.txt第2次随访主食量2.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.适合主食2].ToString();
                this.txt第2次随访心理调整.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.心理调整].ToString();
                this.txt第2次随访遵医行为.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.遵医行为].ToString();
                this.txt第2次随访空腹血糖.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.空腹血糖].ToString();
                this.txt第2次随访血红蛋白.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.糖化血红蛋白].ToString();
                this.txt第2次随访检查日期.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.辅助检查日期].ToString();
                //辅助检查其他
                this.txt第2次随访服药依从性.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.服药依从性].ToString();
                this.txt第2次随访药物不良反应.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.药物副作用].ToString();
                this.txt第2次随访药物不良反应其他.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.副作用详述].ToString();
                this.txt第2次低血糖反应.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.低血糖反应].ToString();
                this.txt第2次随访分类.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.本次随访分类].ToString();
                //this.txt第2次随访胰岛素种类.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.胰岛素种类].ToString();
                //this.txt第2次随访胰岛素用法.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.胰岛素用法].ToString();
                this.txt第2次随访转诊原因.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.转诊原因].ToString();
                this.txt第2次随访转诊机构.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.转诊科别].ToString();
                this.txt第2次随访下次随访日期.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.下次随访时间].ToString();
                this.txt第2次随访医生签名.Text = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.随访医生].ToString();
            }
            #endregion

            #region 第3次随访记录

            if (dt糖尿病 != null && dt糖尿病.Rows.Count > 2)
            {
                this.txt第3次随访日期.Text = Convert.ToDateTime(dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第3次随访方式.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.随访方式].ToString();
                //第3次随访症状
                BindZZ3(dt糖尿病);
                this.txt第3次随访症状其他.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.目前症状其他].ToString();
                this.txt第3次随访血压1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.收缩压].ToString();
                this.txt第3次随访血压2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.舒张压].ToString();
                this.txt第3次随访体重1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.体重].ToString();
                this.txt第3次随访体重2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.体重2].ToString();
                this.txt第3次随访体质指数1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.BMI].ToString();
                this.txt第3次随访体质指数2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.BMI2].ToString();
                this.txt第3次随访足背动脉搏动.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.足背动脉搏动].ToString();
                this.txt第3次随访体征其他.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.体征其他].ToString();
                this.txt第3次随访日吸烟量1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.吸烟数量].ToString();
                this.txt第3次随访日吸烟量2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.吸烟数量2].ToString();
                this.txt第3次随访日饮酒量1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.饮酒数量].ToString();
                this.txt第3次随访日饮酒量2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.饮酒数量2].ToString();
                this.txt第3次随访运动次数1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.运动频率].ToString();
                this.txt第3次随访运动时间1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.运动持续时间].ToString();
                this.txt第3次随访运动次数2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.运动频率2].ToString();
                this.txt第3次随访运动时间2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.运动持续时间2].ToString();
                this.txt第3次随访主食量1.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.适合主食].ToString();
                this.txt第3次随访主食量2.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.适合主食2].ToString();
                this.txt第3次随访心理调整.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.心理调整].ToString();
                this.txt第3次随访遵医行为.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.遵医行为].ToString();
                this.txt第3次随访空腹血糖.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.空腹血糖].ToString();
                this.txt第3次随访血红蛋白.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.糖化血红蛋白].ToString();
                this.txt第3次随访检查日期.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.辅助检查日期].ToString();
                //辅助检查其他
                this.txt第3次随访服药依从性.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.服药依从性].ToString();
                this.txt第3次随访药物不良反应.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.药物副作用].ToString();
                this.txt第3次随访药物不良反应其他.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.副作用详述].ToString();
                this.txt第3次低血糖反应.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.低血糖反应].ToString();
                this.txt第3次随访分类.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.本次随访分类].ToString();
                //this.txt第3次随访胰岛素种类.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.胰岛素种类].ToString();
                //this.txt第3次随访胰岛素用法.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.胰岛素用法].ToString();
                this.txt第3次随访转诊原因.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.转诊原因].ToString();
                this.txt第3次随访转诊机构.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.转诊科别].ToString();
                this.txt第3次随访下次随访日期.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.下次随访时间].ToString();
                this.txt第3次随访医生签名.Text = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.随访医生].ToString();
            }
            #endregion

            #region 第4次随访记录

            if (dt糖尿病 != null && dt糖尿病.Rows.Count > 3)
            {
                this.txt第4次随访日期.Text = Convert.ToDateTime(dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第4次随访方式.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.随访方式].ToString();
                //第4次随访症状
                BindZZ4(dt糖尿病);
                this.txt第4次随访症状其他.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.目前症状其他].ToString();
                this.txt第4次随访血压1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.收缩压].ToString();
                this.txt第4次随访血压2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.舒张压].ToString();
                this.txt第4次随访体重1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.体重].ToString();
                this.txt第4次随访体重2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.体重2].ToString();
                this.txt第4次随访体质指数1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.BMI].ToString();
                this.txt第4次随访体质指数2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.BMI2].ToString();
                this.txt第4次随访足背动脉搏动.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.足背动脉搏动].ToString();
                this.txt第4次随访体征其他.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.体征其他].ToString();
                this.txt第4次随访日吸烟量1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.吸烟数量].ToString();
                this.txt第4次随访日吸烟量2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.吸烟数量2].ToString();
                this.txt第4次随访日饮酒量1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.饮酒数量].ToString();
                this.txt第4次随访日饮酒量2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.饮酒数量2].ToString();
                this.txt第4次随访运动次数1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.运动频率].ToString();
                this.txt第4次随访运动时间1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.运动持续时间].ToString();
                this.txt第4次随访运动次数2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.运动频率2].ToString();
                this.txt第4次随访运动时间2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.运动持续时间2].ToString();
                this.txt第4次随访主食量1.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.适合主食].ToString();
                this.txt第4次随访主食量2.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.适合主食2].ToString();
                this.txt第4次随访心理调整.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.心理调整].ToString();
                this.txt第4次随访遵医行为.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.遵医行为].ToString();
                this.txt第4次随访空腹血糖.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.空腹血糖].ToString();
                this.txt第4次随访血红蛋白.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.糖化血红蛋白].ToString();
                this.txt第4次随访检查日期.Text = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.辅助检查日期].ToString();
                //辅助检查其他
                this.txt第4次随访服药依从性.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.服药依从性].ToString();
                this.txt第4次随访药物不良反应.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.药物副作用].ToString();
                this.txt第4次随访药物不良反应其他.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.副作用详述].ToString();
                this.txt第4次低血糖反应.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.低血糖反应].ToString();
                this.txt第4次随访分类.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.本次随访分类].ToString();
                //this.txt第4次随访胰岛素种类.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.胰岛素种类].ToString();
                //this.txt第4次随访胰岛素用法.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.胰岛素用法].ToString();
                this.txt第4次随访转诊原因.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.转诊原因].ToString();
                this.txt第4次随访转诊机构.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.转诊科别].ToString();
                this.txt第4次随访下次随访日期.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.下次随访时间].ToString();
                this.txt第4次随访医生签名.Text = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.随访医生].ToString();
            }

            #endregion
        }

        #region 症状
        private void BindZZ4(DataTable dt糖尿病)
        {
            string 第4次随访症状 = dt糖尿病.Rows[3][Models.tb_MXB糖尿病随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第4次随访症状))
            {
                string[] a = 第4次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第4次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ3(DataTable dt糖尿病)
        {
            string 第3次随访症状 = dt糖尿病.Rows[2][Models.tb_MXB糖尿病随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第3次随访症状))
            {
                string[] a = 第3次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第3次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ2(DataTable dt糖尿病)
        {
            string 第2次随访症状 = dt糖尿病.Rows[1][Models.tb_MXB糖尿病随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第2次随访症状))
            {
                string[] a = 第2次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第2次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ1(DataTable dt糖尿病)
        {
            string 第1次随访症状 = dt糖尿病.Rows[0][Models.tb_MXB糖尿病随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第1次随访症状))
            {
                string[] a = 第1次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第1次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        #endregion

        #region 用药情况
        private void BindYYQK1()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第1次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第1次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第1次随访药物名称1" && ctr用法 == "txt第1次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次随访药物名称2" && ctr用法 == "txt第1次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次随访药物名称3" && ctr用法 == "txt第1次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK2()
        {

            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第2次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第2次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第2次随访药物名称1" && ctr用法 == "txt第2次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次随访药物名称2" && ctr用法 == "txt第2次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次随访药物名称3" && ctr用法 == "txt第2次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK3()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第3次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第3次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第3次随访药物名称1" && ctr用法 == "txt第3次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次随访药物名称2" && ctr用法 == "txt第3次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次随访药物名称3" && ctr用法 == "txt第3次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK4()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第4次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第4次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第4次随访药物名称1" && ctr用法 == "txt第4次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第4次随访药物名称2" && ctr用法 == "txt第4次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第4次随访药物名称3" && ctr用法 == "txt4次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB糖尿病随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        #endregion

    }
}
