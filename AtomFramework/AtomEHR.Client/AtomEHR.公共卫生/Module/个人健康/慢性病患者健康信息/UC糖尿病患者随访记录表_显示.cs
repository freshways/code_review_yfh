﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC糖尿病患者随访记录表_显示 : UserControlBaseNavBar
    {
        DataRow[] _dr个人档案信息 = null;
        string _创建日期 = "";
        string _ID = "";
        public UC糖尿病患者随访记录表_显示()
        {
            InitializeComponent();
        }

        public UC糖尿病患者随访记录表_显示(DataRow[] dr, object date)
        {
            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bllMXB糖尿病随访表();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString()); 
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txt体征其他.Text = "无";//体征其他默认“无”
            this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            //this.txt身高.Txt1.Text = dr[0][tb_健康体检.身高].ToString();
        }
        
        private void UC糖尿病患者随访记录表_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_MXB糖尿病随访表.__TableName];

            CreateNavBarButton_new( dt缓存数据,  tb_MXB糖尿病随访表.发生时间);
            //已经有数据进行绑定
            if (dt缓存数据 != null && dt缓存数据.Rows.Count > 0)
            {
                if (_创建日期 != null && _创建日期 != "")
                {
                    DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);
                }
                else
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);
                SetItemColorToRed(_ID);//设置左侧当前随访日期颜色

                lab考核项.Text = string.Format("考核项：26     缺项：{0} 完整度：{1}% ",
                    _dt缓存数据.Rows[0][tb_MXB糖尿病随访表.缺项].ToString(),
                    _dt缓存数据.Rows[0][tb_MXB糖尿病随访表.完整度].ToString());
            }
            else
            {//打开新增页面
                btn添加.PerformClick();
            }
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName];
            gc药物调整.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName];

            SetDetailEditorsAccessable(panel1, false);

            int isallow = DataDictCache.Cache.IsAllow延时加载数据();
            if (isallow > 0 && !Loginer.CurrentUser.IsSubAdmin())
            {
                this.layoutControlItem修改时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.layoutControlItem修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Add;
            UC糖尿病患者随访记录表 control = new UC糖尿病患者随访记录表(_dr个人档案信息, _UpdateType, "", null);
            ShowControl(control);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC糖尿病患者随访记录表 control = new UC糖尿病患者随访记录表(_dr个人档案信息, _UpdateType, _ID, null);
                ShowControl(control);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(_ID))
                {
                    ((bllMXB糖尿病随访表)BLL).Set个人健康特征(txt个人档案号.Text);
                    //_BLL.WriteLog(txt个人档案号.Text, tb_MXB糖尿病随访表.__TableName, this.txt姓名.Text + "&" + txt发生时间.Text); //随访表这种暂时就不添加删除记录了
                    _创建日期 = "";
                    this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }
        }
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(Object objdataSource)
        {
            DataTable dataSource = null;
            if (objdataSource == null) return;
            if (objdataSource is DataRow)
            {
                dataSource = BLL.ConvertRowsToTable(objdataSource as DataRow);
            }
            _ID = dataSource.Rows[0][tb_MXB糖尿病随访表.ID].ToString();

            DataBinder.BindingTextEdit(txt发生时间, dataSource, tb_MXB糖尿病随访表.发生时间);
            //TextEdit - 体征
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB糖尿病随访表.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB糖尿病随访表.舒张压);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB糖尿病随访表.体重);
            DataBinder.BindingTextEdit(txt体重.Txt2, dataSource, tb_MXB糖尿病随访表.体重2);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB糖尿病随访表.身高);
            DataBinder.BindingTextEdit(txt体质指数.Txt1, dataSource, tb_MXB糖尿病随访表.BMI);
            DataBinder.BindingTextEdit(txt体质指数.Txt2, dataSource, tb_MXB糖尿病随访表.BMI2);
            DataBinder.BindingTextEdit(txt体征其他, dataSource, tb_MXB糖尿病随访表.体征其他);
            //TextEdit - 生活方式指导
            DataBinder.BindingTextEdit(txt日吸烟量.Txt1, dataSource, tb_MXB糖尿病随访表.吸烟数量);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt2, dataSource, tb_MXB糖尿病随访表.吸烟数量2);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt1, dataSource, tb_MXB糖尿病随访表.饮酒数量);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt2, dataSource, tb_MXB糖尿病随访表.饮酒数量2);
            DataBinder.BindingTextEdit(txt运动频率.Txt1, dataSource, tb_MXB糖尿病随访表.运动频率);
            DataBinder.BindingTextEdit(txt运动频率.Txt2, dataSource, tb_MXB糖尿病随访表.运动频率2);
            DataBinder.BindingTextEdit(txt持续时间.Txt1, dataSource, tb_MXB糖尿病随访表.运动持续时间);
            DataBinder.BindingTextEdit(txt持续时间.Txt2, dataSource, tb_MXB糖尿病随访表.运动持续时间2);
            DataBinder.BindingTextEdit(txt主食.Txt1, dataSource, tb_MXB糖尿病随访表.适合主食);
            DataBinder.BindingTextEdit(txt主食.Txt2, dataSource, tb_MXB糖尿病随访表.适合主食2);
            //辅助检查
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB糖尿病随访表.空腹血糖);
            DataBinder.BindingTextEdit(txt糖化血红蛋白.Txt1, dataSource, tb_MXB糖尿病随访表.糖化血红蛋白);
            DataBinder.BindingTextEdit(txt检查日期, dataSource, tb_MXB糖尿病随访表.辅助检查日期);
            DataBinder.BindingTextEdit(txt随机血糖.Txt1, dataSource, tb_MXB糖尿病随访表.随机血糖);
            DataBinder.BindingTextEdit(txt餐后2h血糖.Txt1, dataSource, tb_MXB糖尿病随访表.餐后2h血糖);
            DataBinder.BindingTextEdit(txt药物副作用详述, dataSource, tb_MXB糖尿病随访表.副作用详述);

            DataBinder.BindingTextEdit(txt医生建议, dataSource, tb_MXB糖尿病随访表.随访医生建议);
            DataBinder.BindingTextEdit(txt转诊科别, dataSource, tb_MXB糖尿病随访表.转诊科别);
            DataBinder.BindingTextEdit(txt转诊原因, dataSource, tb_MXB糖尿病随访表.转诊原因);
            DataBinder.BindingTextEdit(txt下次随访时间, dataSource, tb_MXB糖尿病随访表.下次随访时间);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_MXB糖尿病随访表.随访医生);

            this.txt随访方式.Text = BLL.ReturnDis字典显示("随访方式", dataSource.Rows[0][tb_MXB糖尿病随访表.随访方式].ToString());
            //添加随访方式为其他的填空项
            if (this.txt随访方式.Text == "4")
            {
                this.txt随访方式.Text = dataSource.Rows[0][tb_MXB糖尿病随访表.随访方式其他].ToString();
            }
            this.txt目前症状.Text = SetFlowLayoutValues("tnbmqzz", dataSource.Rows[0][tb_MXB糖尿病随访表.目前症状].ToString());
            if (dataSource.Rows[0][tb_MXB糖尿病随访表.目前症状其他].ToString() != "")
            { txt目前症状.Text += "(" + dataSource.Rows[0][tb_MXB糖尿病随访表.目前症状其他].ToString() + ")"; }
            this.txt足背动脉搏动.Text = BLL.ReturnDis字典显示("cjwcj", dataSource.Rows[0][tb_MXB糖尿病随访表.足背动脉搏动].ToString());
            this.txt心理调整.Text = BLL.ReturnDis字典显示("lhybjc", dataSource.Rows[0][tb_MXB糖尿病随访表.心理调整].ToString());
            this.txt遵医行为.Text = BLL.ReturnDis字典显示("lhybjc", dataSource.Rows[0][tb_MXB糖尿病随访表.遵医行为].ToString());
            this.txt服药依从性.Text = BLL.ReturnDis字典显示("fyycx-mb", dataSource.Rows[0][tb_MXB糖尿病随访表.服药依从性].ToString());
            this.txt不良反应.Text = BLL.ReturnDis字典显示("wy_wuyou", dataSource.Rows[0][tb_MXB糖尿病随访表.药物副作用].ToString());
            this.txt低血糖反应.Text = BLL.ReturnDis字典显示("mb_dxtfy", dataSource.Rows[0][tb_MXB糖尿病随访表.低血糖反应].ToString());
            //this.txt随访分类.Text = BLL.ReturnDis字典显示("sffl", dataSource.Rows[0][tb_MXB糖尿病随访表.本次随访分类].ToString());
            this.txt随访分类.Text = SetFlowLayoutValues("sffl", dataSource.Rows[0][tb_MXB糖尿病随访表.本次随访分类].ToString());
            if (dataSource.Rows[0][tb_MXB糖尿病随访表.慢病并发症].ToString() != "")
            { txt随访分类.Text += "(" + SetFlowLayoutValues("mb_bfz", dataSource.Rows[0][tb_MXB糖尿病随访表.慢病并发症].ToString()) + ")"; }

            this.txt用药情况.Text = BLL.ReturnDis字典显示("使用不使用", dataSource.Rows[0][tb_MXB糖尿病随访表.降压药].ToString());
            if (txt用药情况.Text == "使用")
            { //处理用药列表，进行绑定
                _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药情况.__TableName].DefaultView.RowFilter = "创建时间='" + dataSource.Rows[0][tb_MXB糖尿病随访表.创建时间].ToString() + "'";
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            { //处理用药列表，进行绑定
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            this.txt转诊情况.Text = BLL.ReturnDis字典显示("sf_shifou", dataSource.Rows[0][tb_MXB糖尿病随访表.转诊情况].ToString());

            #region  新版本添加
            DataBinder.BindingTextEdit(txt胰岛素种类, dataSource, tb_MXB糖尿病随访表.胰岛素种类);
            DataBinder.BindingTextEdit(txt胰岛素用法用量, dataSource, tb_MXB糖尿病随访表.胰岛素用法用量);
            this.txt管理措施.Text = BLL.ReturnDis字典显示("xybglcs", dataSource.Rows[0][tb_MXB糖尿病随访表.下一步管理措施].ToString());
            this.txt用药调整意见.Text = BLL.ReturnDis字典显示("yw_youwu", dataSource.Rows[0][tb_MXB糖尿病随访表.用药调整意见].ToString());
            DataBinder.BindingTextEdit(txt调整胰岛素种类, dataSource, tb_MXB糖尿病随访表.调整胰岛素种类);
            DataBinder.BindingTextEdit(txt调整胰岛素用法用量, dataSource, tb_MXB糖尿病随访表.调整胰岛素用法用量);
            if (txt用药调整意见.Text == "有")
            {
                _BLL.CurrentBusiness.Tables[tb_MXB糖尿病随访表_用药调整.__TableName].DefaultView.RowFilter = "创建时间='" + dataSource.Rows[0][tb_MXB糖尿病随访表.创建时间].ToString() + "'";
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            DataBinder.BindingTextEdit(txt转诊联系人, dataSource, tb_MXB糖尿病随访表.转诊联系人);
            DataBinder.BindingTextEdit(txt转诊联系电话, dataSource, tb_MXB糖尿病随访表.转诊联系电话);
            this.txt转诊结果.Text = BLL.ReturnDis字典显示("sfdw", dataSource.Rows[0][tb_MXB糖尿病随访表.转诊结果].ToString());
            //DataBinder.BindingTextEdit(txt居民签名, dataSource, tb_MXB糖尿病随访表.居民签名);
            string SQ = dataSource.Rows[0][tb_MXB糖尿病随访表.居民签名].ToString();
            if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
            {
                Byte[] bitmapData = new Byte[SQ.Length];

                bitmapData = Convert.FromBase64String(SQ);

                System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);

                this.txt居民签名.Image = Image.FromStream(streamBitmap);
            }
            else
            {
                if (string.IsNullOrEmpty(SQ))
                    SQ = "暂无签名";
                Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                Font font = new Font("宋体", 9);
                SizeF sizeF = g.MeasureString(SQ, font); //测量出字体的高度和宽度  
                Brush brush; //笔刷，颜色  
                brush = Brushes.Black;
                PointF pf = new PointF(0, 0);
                Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                g = Graphics.FromImage(img);
                g.DrawString(SQ, font, brush, pf);
                this.txt居民签名.Image = img;
            }

            DataBinder.BindingTextEdit(txt备注, dataSource, tb_MXB糖尿病随访表.备注);
            #endregion

            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压随访表.访视情况].ToString(), fl访视情况);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压随访表.失访原因].ToString(), fl失访原因);
            dte死亡日期.Text = dataSource.Rows[0][tb_MXB高血压随访表.死亡日期].ToString();
            txt死亡原因.Text = dataSource.Rows[0][tb_MXB高血压随访表.死亡原因].ToString();

            //非编辑项
            this.lbl当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.所属机构].ToString()); 
            this.lbl创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建机构].ToString());
            this.lbl创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.创建时间].ToString();
            this.lbl最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB糖尿病管理卡.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_MXB糖尿病管理卡.修改时间].ToString();

            //设置颜色
            Set考核项颜色_new(layoutControl1, null);//lab考核项
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.txt个人档案号.Text.Trim();
            string year = this.txt发生时间.Text.Trim();
            if(!string.IsNullOrEmpty(docNo))
            {
                frm糖尿病随访日期 frm糖尿病 = new frm糖尿病随访日期(docNo,year);
                frm糖尿病.ShowDialog();

                //report2型糖尿病患者随访服务记录表 report = new report2型糖尿病患者随访服务记录表(docNo,year);
                //ReportPrintTool tool = new ReportPrintTool(report);
                //tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }

        private void btn查看随访照片_Click(object sender, EventArgs e)
        {
            try
            {
                //string filename = this.txt个人档案号.Text + txt发生时间.Text.Replace("-", "");
                string filename = this.txt个人档案号.Text + Convert.ToDateTime(lbl创建时间.Text).ToString("yyyyMMdd");
                // 服务器IP
                string host = "192.168.10.118";
                // 服务器端口
                string port = "83";
                // 获取用户信息接口
                string sUrl = string.Format("http://{0}:{1}/WebApi/api/AtomEHR/Get_PIC", host, port);
                string sMessage = "";
                string sEntity = "{\"cno\":\"" + filename + "\",\"ctype\":\"TNB\"}";

                // string sReq = GetHttpWebRequestMethod(sUrl, sEntity, out sMessage);
                string sReq = WebApiMethod.RequestJsonPostMethod(sUrl, sEntity, out sMessage);

                byte[] pic = Convert.FromBase64String(sReq.Replace("\"", ""));
                if (pic.Length > 0)
                {
                    System.IO.MemoryStream m = new System.IO.MemoryStream(pic);

                    Frm随访照片查看 frm = new Frm随访照片查看(Image.FromStream(m));
                    frm.ShowDialog();
                }
                else
                { //二次判断
                    string filename2 = this.txt个人档案号.Text + txt发生时间.Text.Replace("-", "");

                    string sEntity2 = "{\"cno\":\"" + filename2 + "\",\"ctype\":\"TNB\"}";
                    string sReq2 = WebApiMethod.RequestJsonPostMethod(sUrl, sEntity2, out sMessage);
                    byte[] pic2 = Convert.FromBase64String(sReq2.Replace("\"", ""));
                    if (pic2.Length > 0)
                    {
                        System.IO.MemoryStream m = new System.IO.MemoryStream(pic2);

                        Frm随访照片查看 frm = new Frm随访照片查看(Image.FromStream(m));
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chk失访_CheckedChanged(object sender, EventArgs e)
        {
            this.lcl失访原因.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }

        private void chk死亡_CheckedChanged(object sender, EventArgs e)
        {
            this.lcl死亡信息.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }

    }
}
