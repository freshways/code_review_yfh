﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC高血压患者分级管理 : UserControlBase
    {
        DataRow _dr个人档案信息 = null;
        string _ID = "";
        public UC高血压患者分级管理()
        {
            InitializeComponent();
        }

        public UC高血压患者分级管理(DataRow dr, UpdateType _UpdateType, object ID)
        {
            base._UpdateType = _UpdateType;
            _dr个人档案信息 = dr;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bllMXB高血压患者三级管理记录表();
            InitializeComponent();
            if (dr == null) return;
            //默认绑定
            txt个人档案号.Text = dr[tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = dr[tb_健康档案.姓名].ToString();
            this.txt性别.Text = dr[tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[tb_健康档案.本人电话].ToString();
            }
            this.txt居住地址.Text = dr[tb_健康档案.居住地址].ToString();
            //this.txt职业.Text = dr[tb_健康档案.职业].ToString();
            //this.txt体征其他.Text = "无";//体征其他默认“无”
            //this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            //this.txt身高.Txt1.Text = dr[0][tb_健康体检.身高].ToString();
        }

        private void UC高血压患者分级管理_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                #region 初始化
                this.txt发生时间.Focus();//先聚焦到随访日期，随访日期是生成下次随访日期的基础。
                
                txt血压值.Leave += new System.EventHandler(this.txt血压值_Leave);
                radio管理措施.EditValueChanged += radio管理措施_EditValueChanged;
                txt发生时间.EditValueChanged += txt发生时间_EditValueChanged2;
                txt备注.EditValueChanged += txt备注_EditValueChanged; 
                #endregion

                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.个人档案编号] = txt个人档案号.Text;
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.姓名] = this.txt姓名.Text;
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.性别] = this.txt性别.Text;
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.身份证号] = this.txt身份证号.Text;
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.出生日期] = this.txt出生日期.Text;
                //绑定联系电话
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.联系电话] = this.txt联系电话.Text;
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.居住地址] = this.txt居住地址.Text;

                //判断绑定的身高是否为空(当体检表中的身高为空时报错)
                if (_dr个人档案信息.Table.Columns.Contains("身高"))
                {
                    if (string.IsNullOrEmpty(_dr个人档案信息["身高"].ToString()))
                    {
                        _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.身高] = 0.00;
                    }
                    else
                    {
                        _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.身高] = _dr个人档案信息[tb_健康体检.身高].ToString();
                    }
                }
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                if (_ID != null && _ID != "")
                {
                    ((bllMXB高血压患者三级管理记录表)_BLL).GetBusinessByKey(_ID, true);
                }
                else return;
            }

            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_MXB高血压患者三级管理记录表.__TableName]);

            if (_BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药情况.__TableName].Rows.Count >= 1)
                radio用药情况.EditValue = 1;

            if (_BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药调整.__TableName].Rows.Count >= 1)
                radio药物调整.EditValue = 1;

            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药情况.__TableName];
            gc药物调整.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药调整.__TableName];

            //初始化
            Init();
            //设置颜色   
            //if (_UpdateType == UpdateType.Modify)
            //    Set考核项颜色_new(Layout1, lab考核项);
            
        }


        #region 初始化

        void Init() //初始化
        {
            DataBinder.BindingLookupEditDataSource(txt心理调整, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='lhybjc' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt摄盐情况1, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='qzz' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt摄盐情况2, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='qzz' ")), "P_DESC", "P_CODE"); 
            DataBinder.BindingLookupEditDataSource(txt服药依从性, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='fyycx-mb' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt转诊结果, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='sfdw' ")), "P_DESC", "P_CODE");

            btn添加药物.Click += btn添加药物_Click;
            btn删除药物.Click += btn删除药物_Click;

            btn添加药物调整.Click += btn添加药物调整_Click;
            btn删除药物调整.Click += btn删除药物调整_Click;

            this.txt身高.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt2.Leave += 身高_Leave;
            this.txt体质指数.Txt1.Enter += BMI_Enter;
            this.txt体质指数.Txt2.Enter += BMI_Enter;
        }

        void 身高_Leave(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        void BMI_Enter(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        #endregion

        #region 保存相关
        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (!Msg.AskQuestion("信息保存后，‘随访日期‘将不允许修改，确认保存信息？")) return;
            if (_UpdateType == UpdateType.None) return;

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.随访日期] = this.txt发生时间.Text;
            //根据用户要求，下次随访时间默认“发生时间”的3个月以后
            if (string.IsNullOrEmpty(txt下次随访时间.Text))
            {
                string str发生时间 = this.txt发生时间.Text;
                this.txt下次随访时间.Text = Convert.ToDateTime(str发生时间).AddMonths(3).ToShortDateString();
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.下次随访日期] = this.txt下次随访时间.Text;
            }
            else
            {
                _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.下次随访日期] = this.txt下次随访时间.Text;
            }
            _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.症状] = GetFlowLayoutResult(this.fl症状);
            _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.家族史] = GetFlowLayoutResult(this.fl家族史);
            _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.既往史] = GetFlowLayoutResult(this.fl既往史);
            _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.并发症] = GetFlowLayoutResult(this.fl并发症);
            //判断血压值，自动绑定分类类型
            string str收缩压 = this.txt血压值.Txt1.Text;
            string str舒张压 = this.txt血压值.Txt2.Text;

            //_BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.缺项] = _base缺项;
            //_BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.完整度] = _base完整度;
           

            //更新手签 ▽
            if (b手签更新)
            {
                using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                {
                    this.txt居民签名.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    _BLL.DataBinder.Rows[0][tb_MXB高血压患者三级管理记录表.居民签名] = Convert.ToBase64String(arr1);
                }
            }
            //更新手签 △

            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人
                //((bllMXB高血压随访表)BLL).Set个人健康特征(txt个人档案号.Text);

                Msg.ShowInformation("保存成功!");

                this._UpdateType = UpdateType.None; // 最后情况操作状态

                this.btn重置.PerformClick();
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt发生时间.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                txt发生时间.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(ConvertEx.ToString(txt分级管理.Text)))
            {
                Msg.Warning("高血压分级不能为空!");
                txt分级管理.Focus();
                return false;
            }

            if (chk高血压.Checked || chk冠心病.Checked)
            {
                return true;
            }


            if (this.txt发生时间.DateTime > Convert.ToDateTime(lbl创建时间.Text))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                txt发生时间.Focus();
                return false;
            }

            //合理值区间提醒
            StringBuilder sbNotity = new StringBuilder();

            //判断"血压值"是否填写完整
            if (string.IsNullOrEmpty(txt血压值.Txt1.Text.Trim()) || string.IsNullOrEmpty(txt血压值.Txt2.Text.Trim()))
            {
                Msg.Warning("请完整填写血压值!");
                txt血压值.Focus();
                return false;
            }
            else
            {
                if(int.Parse(txt血压值.Txt1.Text) >140 || int.Parse(txt血压值.Txt1.Text)<90)
                {
                    sbNotity.Append("收缩压："+txt血压值.Txt1.Text+"，不在90--140之间\n");
                }

                if (int.Parse(txt血压值.Txt2.Text) > 90 || int.Parse(txt血压值.Txt2.Text) < 60)
                {
                    sbNotity.Append("舒张压：" + txt血压值.Txt2.Text + "，不在60--90之间\n");
                }
            }

            if (!string.IsNullOrWhiteSpace(txt心率.Text))
            {
                if (!Regex.IsMatch(txt心率.Text, @"^\d+$") 
                    || (Regex.IsMatch(txt心率.Text, @"^\d+$") && (int.Parse(txt心率.Text) < 60 || int.Parse(txt心率.Text) > 100)))
                {
                    sbNotity.Append("心率：" + txt心率.Text + "，不在60--100之间\n");
                }
            }

            if(string.IsNullOrWhiteSpace(txt体重.Txt1.Text) || string.IsNullOrWhiteSpace(txt体重.Txt2.Text))
            {
            }
            else
            {
                if (Convert.ToDecimal(txt体重.Txt1.Text) > 100m || Convert.ToDecimal(txt体重.Txt1.Text) < 40m
                    || (Convert.ToDecimal(txt体重.Txt2.Text) > 100m || Convert.ToDecimal(txt体重.Txt2.Text) < 40m))
                {
                    sbNotity.Append("体重：" + txt体重.Txt1.Text + "/" + txt体重.Txt2.Text + "，不在40--100之间\n");
                }
            }

            if (!string.IsNullOrWhiteSpace(txt体质指数.Txt1.Text) && !string.IsNullOrWhiteSpace(txt体质指数.Txt2.Text))
            {
                if (Convert.ToDecimal(txt体质指数.Txt1.Text) > 28m || Convert.ToDecimal(txt体质指数.Txt1.Text) < 18.5m
                    || Convert.ToDecimal(txt体质指数.Txt2.Text) > 28m || Convert.ToDecimal(txt体质指数.Txt2.Text) < 18.5m
                   )
                {
                    sbNotity.Append("体质指数：" + txt体质指数.Txt1.Text + "/" + txt体质指数.Txt2.Text + "，不在18.5--28之间\n");
                }
            }

            if (radio随访方式.EditValue == null|| string.IsNullOrWhiteSpace(radio随访方式.EditValue.ToString()))
            {
                Msg.Warning("随访方式不能为空!");
                radio随访方式.Focus();
                return false;
            }

            if (string.IsNullOrWhiteSpace(GetFlowLayoutResult(this.fl症状).ToString()))
            {
                Msg.Warning("症状不能为空!");
                fl症状.Focus();
                return false;
            }

            //生活方式指导 ▼
            if ((string.IsNullOrWhiteSpace(txt日吸烟量.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt日吸烟量.Txt2.Text))
                 )
            {
                Msg.Warning("日吸烟量不能为空!");
                txt日吸烟量.Focus();
                return false;
            }
             else
            {
                if(int.Parse(txt日吸烟量.Txt1.Text) < 0 || int.Parse(txt日吸烟量.Txt1.Text) > 20
                    || int.Parse(txt日吸烟量.Txt2.Text) < 0 || int.Parse(txt日吸烟量.Txt2.Text) > 20)
                {
                    sbNotity.Append("日吸烟量：" + txt日吸烟量.Txt1.Text + "/" + txt日吸烟量.Txt2.Text + "，不在0--20之间\n");
                }
            }

            if ((string.IsNullOrWhiteSpace(txt饮酒情况.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt饮酒情况.Txt2.Text))
                 )
            {
                Msg.Warning("饮酒情况不能为空!");
                txt饮酒情况.Focus();
                return false;
            }
            else
            {
                if (Convert.ToDecimal(txt饮酒情况.Txt1.Text) < 0m || Convert.ToDecimal(txt饮酒情况.Txt1.Text) > 10m
                    || Convert.ToDecimal(txt饮酒情况.Txt2.Text) < 0m || Convert.ToDecimal(txt饮酒情况.Txt2.Text) > 10m)
                {
                    sbNotity.Append("日饮酒量：" + txt饮酒情况.Txt1.Text + "/" + txt饮酒情况.Txt2.Text + "，不在0--10之间\n");
                }
            }


            if ((string.IsNullOrWhiteSpace(txt运动.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt运动.Txt2.Text))
                )
            {
                Msg.Warning("运动频率不能为空!");
                txt运动.Focus();
                return false;
            }
            

            if ((string.IsNullOrWhiteSpace(txt指导运动.Txt1.Text)
                || string.IsNullOrWhiteSpace(txt指导运动.Txt2.Text)))
            {
                Msg.Warning("持续时间不能为空!");
                txt指导运动.Focus();
                return false;
            }
            else
            {
                if(Convert.ToDecimal(txt指导运动.Txt1.Text) < 10m || Convert.ToDecimal(txt指导运动.Txt1.Text) > 120m
                    || Convert.ToDecimal(txt指导运动.Txt2.Text) < 10m || Convert.ToDecimal(txt指导运动.Txt2.Text) > 120m)
                {
                    sbNotity.Append("运动持续时间：" + txt指导运动.Txt1.Text + "/" + txt指导运动.Txt2.Text + "，不在10--120之间\n");
                }
            }

            if (txt摄盐情况1.EditValue == null
                || string.IsNullOrWhiteSpace(txt摄盐情况1.EditValue.ToString())
                )
            {
                Msg.Warning("摄盐情况不能为空!");
                txt摄盐情况1.Focus();
                return false;
            }
            if (txt摄盐情况2.EditValue == null
                || string.IsNullOrWhiteSpace(txt摄盐情况2.EditValue.ToString()))
            {
                Msg.Warning("摄盐情况不能为空!");
                txt摄盐情况2.Focus();
                return false;
            }
            if (txt心理调整.EditValue == null
                || string.IsNullOrWhiteSpace(txt心理调整.EditValue.ToString())
               )
            {
                Msg.Warning("心理调整不能为空!");
                txt心理调整.Focus();
                return false;
            }

            //生活方式指导 ▲

            if (radio不良反应.EditValue == null
                || (radio不良反应.EditValue.ToString() == "2" && string.IsNullOrWhiteSpace(txt药物副作用详述.Text)))
            {
                Msg.Warning("请确认药物不良反应!");
                radio不良反应.Focus();
                return false;
            }


            if (radio用药情况.EditValue == null || string.IsNullOrWhiteSpace(radio用药情况.EditValue.ToString())
            || (radio用药情况.EditValue.ToString() == "1" && gvDetail.DataRowCount == 0))
            {
                Msg.Warning("请填写用药情况!");
                radio用药情况.Focus();
                return false;
            }
            else
            {
                //for (int index = 0; index < gvDetail.DataRowCount; index++)
                //{
                //    //添加用法说明默认值
                //    //DataRow row = gvDetail.GetDataRow(index);
                //    //if (row[tb_MXB高血压患者三级管理记录表_用药情况.用法].ToString().Contains("每日  次")
                //    //    || row[tb_MXB高血压患者三级管理记录表_用药情况.用法].ToString().Contains("每次  mg")
                //    //    || string.IsNullOrWhiteSpace(row[tb_MXB高血压患者三级管理记录表_用药情况.药物名称].ToString()))
                //    //{
                //    //    Msg.Warning("请检查用药情况!");
                //    //    gvDetail.Focus();
                //    //    return false;
                //    //}
                //}
            }

            if (string.IsNullOrWhiteSpace(txt医生签名.Text))
            {
                Msg.Warning("请填写医生签名!");
                txt医生签名.Focus();
                return false;
            }

            if (sbNotity.Length > 0)
            {
                return Msg.AskQuestion("请确认下方数据：\r\n" + sbNotity.ToString() + "确定要保存吗？");
            }


            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.个人档案编号] = txt个人档案号.Text;

            DataBinder.BindingTextEditDateTime(txt发生时间, dataSource, tb_MXB高血压患者三级管理记录表.随访日期);
            DataBinder.BindingComboEdit(txt分级管理, dataSource, tb_MXB高血压患者三级管理记录表.高血压分级);
            DataBinder.BindingTextEdit(txt症状其他, dataSource, tb_MXB高血压患者三级管理记录表.症状其他);
            //TextEdit - 体征
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.舒张压);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.体重);
            DataBinder.BindingTextEdit(txt体重.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.体重2);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.身高);
            DataBinder.BindingTextEdit(txt体质指数.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.体质指数);
            DataBinder.BindingTextEdit(txt体质指数.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.体质指数2);
            DataBinder.BindingTextEdit(txt心率, dataSource, tb_MXB高血压患者三级管理记录表.心率);
            DataBinder.BindingTextEdit(txt体征其他, dataSource, tb_MXB高血压患者三级管理记录表.体征其他);
            //TextEdit - 生活方式指导
            DataBinder.BindingTextEdit(txt日吸烟量.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.日吸烟量);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.日吸烟量2);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.日饮酒量);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.日饮酒量2);
            DataBinder.BindingTextEdit(txt运动.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.运动);
            DataBinder.BindingTextEdit(txt运动.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.运动2);
            DataBinder.BindingTextEdit(txt指导运动.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.指导运动);
            DataBinder.BindingTextEdit(txt指导运动.Txt2, dataSource, tb_MXB高血压患者三级管理记录表.指导运动2);
            DataBinder.BindingTextEdit(txt摄盐情况1, dataSource, tb_MXB高血压患者三级管理记录表.摄盐情况);
            DataBinder.BindingTextEdit(txt摄盐情况2, dataSource, tb_MXB高血压患者三级管理记录表.摄盐情况2);
            DataBinder.BindingTextEdit(txt心理调整, dataSource, tb_MXB高血压患者三级管理记录表.心理调整);
            //DataBinder.BindingTextEdit(txt遵医行为, dataSource, tb_MXB高血压患者三级管理记录表.遵医行为);
            //辅助检查
            DataBinder.BindingTextEdit(txt血红蛋白.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血红蛋白);
            DataBinder.BindingTextEdit(txt白细胞.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.白细胞);
            DataBinder.BindingTextEdit(txt血小板.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血小板);
            DataBinder.BindingTextEdit(txt血常规其他, dataSource, tb_MXB高血压患者三级管理记录表.血常规其他);
            DataBinder.BindingTextEdit(txt尿蛋白.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.尿蛋白);
            DataBinder.BindingTextEdit(txt尿糖.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.尿糖);
            DataBinder.BindingTextEdit(txt尿酮体.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.尿酮体);
            DataBinder.BindingTextEdit(txt尿潜血.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.尿潜血);
            DataBinder.BindingTextEdit(txt尿常规其他, dataSource, tb_MXB高血压患者三级管理记录表.尿常规其他);
            DataBinder.BindingTextEdit(txt血清谷丙转氨酶.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血清谷丙转氨酶);
            DataBinder.BindingTextEdit(txt血清谷草转氨酶.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血清谷草转氨酶);
            DataBinder.BindingTextEdit(txt总胆红素.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.总胆红素);
            DataBinder.BindingTextEdit(txt血清肌酐.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血清肌酐);
            DataBinder.BindingTextEdit(txt血尿素.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血尿素);
            DataBinder.BindingTextEdit(txt血钾浓度.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血钾浓度);
            DataBinder.BindingTextEdit(txt血钠浓度.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.血钠浓度);
            DataBinder.BindingTextEdit(txt总胆固醇.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.总胆固醇);
            DataBinder.BindingTextEdit(txt甘油三酯.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.甘油三酯);
            DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.低密度脂蛋白);
            DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.高密度脂蛋白);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.空腹血糖);
            DataBinder.BindingTextEdit(txt同型半胱氨酸.Txt1, dataSource, tb_MXB高血压患者三级管理记录表.同型半胱氨酸);
            DataBinder.BindingTextEdit(txt其他1, dataSource, tb_MXB高血压患者三级管理记录表.血脂其他);
            //辅助检查 radio
            DataBinder.BindingRadioEdit(radio心电图, dataSource, tb_MXB高血压患者三级管理记录表.心电图);
            DataBinder.BindingTextEdit(txt心电图异常, dataSource, tb_MXB高血压患者三级管理记录表.心电图异常说明);
            DataBinder.BindingRadioEdit(radio超声心动图, dataSource, tb_MXB高血压患者三级管理记录表.超声心动图);
            DataBinder.BindingTextEdit(txt超声心动图异常, dataSource, tb_MXB高血压患者三级管理记录表.超声心动图异常说明);
            DataBinder.BindingRadioEdit(radio颈动脉超声, dataSource, tb_MXB高血压患者三级管理记录表.颈动脉超声);
            DataBinder.BindingTextEdit(txt颈动脉超声异常, dataSource, tb_MXB高血压患者三级管理记录表.颈动脉超声异常说明);
            DataBinder.BindingRadioEdit(radio胸片, dataSource, tb_MXB高血压患者三级管理记录表.胸片);
            DataBinder.BindingTextEdit(txt胸片异常, dataSource, tb_MXB高血压患者三级管理记录表.胸片异常说明);


            DataBinder.BindingTextEdit(txt药物副作用详述, dataSource, tb_MXB高血压患者三级管理记录表.药物不良反应说明);
            DataBinder.BindingTextEdit(txt服药依从性, dataSource, tb_MXB高血压患者三级管理记录表.服药依从性);
            //DataBinder.BindingTextEdit(txt医生建议, dataSource, tb_MXB高血压患者三级管理记录表.随访医生建议);
            DataBinder.BindingTextEdit(txt转诊科别, dataSource, tb_MXB高血压患者三级管理记录表.转诊机构及科别);
            DataBinder.BindingTextEdit(txt转诊原因, dataSource, tb_MXB高血压患者三级管理记录表.转诊原因);

            DataBinder.BindingTextEditDateTime(txt下次随访时间, dataSource, tb_MXB高血压患者三级管理记录表.下次随访日期);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_MXB高血压患者三级管理记录表.随访医生签名);

            //RadioEdit
            DataBinder.BindingRadioEdit(radio随访方式, dataSource, tb_MXB高血压患者三级管理记录表.随访方式);
            DataBinder.BindingRadioEdit(radio不良反应, dataSource, tb_MXB高血压患者三级管理记录表.药物不良反应);
            //DataBinder.BindingRadioEdit(radio用药情况, dataSource, tb_MXB高血压患者三级管理记录表.降压药);
            //DataBinder.BindingRadioEdit(radio转诊情况, dataSource, tb_MXB高血压患者三级管理记录表.转诊情况);


            //flowLayoutPanel
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.症状].ToString(), fl症状);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.家族史].ToString(), fl家族史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.既往史].ToString(), fl既往史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.并发症].ToString(), fl并发症);

            #region  新版本添加
            DataBinder.BindingRadioEdit(radio管理措施, dataSource, tb_MXB高血压患者三级管理记录表.下一步管理措施);
            //DataBinder.BindingRadioEdit(radio药物调整, dataSource, tb_MXB高血压患者三级管理记录表.用药调整意见);
            DataBinder.BindingTextEdit(txt转诊联系人, dataSource, tb_MXB高血压患者三级管理记录表.联系人及电话);
            //DataBinder.BindingTextEdit(txt转诊联系电话, dataSource, tb_MXB高血压患者三级管理记录表.转诊联系电话);
            DataBinder.BindingTextEdit(txt转诊结果, dataSource, tb_MXB高血压患者三级管理记录表.转诊结果);
            //DataBinder.BindingTextEdit(txt居民签名, dataSource, tb_MXB高血压患者三级管理记录表.居民签名);

            //指纹 ▽
            string SQ = dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.居民签名].ToString();
            if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
            {
                Byte[] bitmapData = new Byte[SQ.Length];
                bitmapData = Convert.FromBase64String(SQ);
                using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData))
                {
                    this.txt居民签名.Image = Image.FromStream(streamBitmap);
                    streamBitmap.Close();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(SQ))
                    SQ = "暂无电子签名";
                Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                Font font = new Font("宋体", 9);
                SizeF sizeF = g.MeasureString(SQ, font); //测量出字体的高度和宽度  
                Brush brush; //笔刷，颜色  
                brush = Brushes.Black;
                PointF pf = new PointF(0, 0);
                Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                g = Graphics.FromImage(img);
                g.DrawString(SQ, font, brush, pf);
                this.txt居民签名.Image = img;
            }
            //指纹 △

            DataBinder.BindingTextEdit(txt备注, dataSource, tb_MXB高血压患者三级管理记录表.备注);
            #endregion



            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_MXB高血压患者三级管理记录表.修改时间].ToString();
        }
        #endregion

        #region 用药情况
        private void btn添加药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物.Tag, gcDetail);
        }

        private void btn删除药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物.Tag, gcDetail);
        }

        protected override void CreateOneDetail(GridView gridView)
        {
            if (gridView == gvDetail)//用药情况
            {
                gvDetail.MoveLast();
                DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药情况.__TableName].NewRow();
                //添加用法说明默认值
                row[tb_MXB高血压三级管理记录表_用药情况.用法] = "每日  次，每次  mg  （口服）";
                row[tb_MXB高血压三级管理记录表_用药情况.个人档案编号] = this.txt个人档案号.Text;
                row[tb_MXB高血压三级管理记录表_用药情况.创建时间] = lbl创建时间.Text;

                _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药情况.__TableName].Rows.Add(row); //增加一条明细记录

                gcDetail.RefreshDataSource();
                gvDetail.FocusedRowHandle = gvDetail.RowCount - 1;

                gvDetail.FocusedColumn = gvDetail.VisibleColumns[0];
            }

            if (gridView == gv药物调整)//药物调整 
            {
                gv药物调整.MoveLast();
                DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药调整.__TableName].NewRow();
                //添加用法说明默认值
                row[tb_MXB高血压三级管理记录表_用药调整.用法] = "每日  次，每次  mg  （口服）";
                row[tb_MXB高血压三级管理记录表_用药调整.个人档案编号] = this.txt个人档案号.Text;
                row[tb_MXB高血压三级管理记录表_用药调整.创建时间] = lbl创建时间.Text;

                _BLL.CurrentBusiness.Tables[tb_MXB高血压三级管理记录表_用药调整.__TableName].Rows.Add(row); //增加一条明细记录

                gc药物调整.RefreshDataSource();
                gv药物调整.FocusedRowHandle = gv药物调整.RowCount - 1;

                gv药物调整.FocusedColumn = gv药物调整.VisibleColumns[0];
            }

        }

        #endregion

        #region 页面控制

        private void check无症状_CheckedChanged(object sender, EventArgs e)
        {
            //通用fl设置check方法
            CheckEdit chkEv = (CheckEdit)(sender);
            FlowLayoutPanel fl = chkEv.Parent as FlowLayoutPanel;
            if (chkEv.Checked)
            {
                SetFlCheckEnabled(fl, false, chkEv.Text);
                SetFlChecked(fl, false, chkEv.Text);
            }
            else
                SetFlCheckEnabled(fl, true, chkEv.Text);

            //if (check无症状.Checked)
            //{
            //    SetFlCheckEnabled(fl症状, false, check无症状.Text);
            //    SetFlChecked(fl症状, false, check无症状.Text);
            //}
            //else
            //    SetFlCheckEnabled(fl症状, true, check无症状.Text);
        }

        private void radio用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio用药情况.EditValue != null && radio用药情况.EditValue.ToString() == "1")
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                //if (_BLL.CurrentBusiness.Tables[tb_MXB高血压患者三级管理记录表_用药情况.__TableName].Rows.Count <= 0)
                //    btn添加药物.PerformClick();
            }
            else
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                //if (_BLL.CurrentBusiness.Tables[tb_MXB高血压患者三级管理记录表_用药情况.__TableName].Rows.Count > 0)
                //    _BLL.CurrentBusiness.Tables[tb_MXB高血压患者三级管理记录表_用药情况.__TableName].Rows.Clear();
            }
        }

        private void radio不良反应_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio不良反应.EditValue != null && radio不良反应.EditValue.ToString() == "2")
                txt药物副作用详述.Enabled = true;
            else
                txt药物副作用详述.Enabled = false;
        }

        private void SetFlCheckEnabled(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Enabled = value;
                }
            }
        }

        private void SetFlChecked(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Checked = value;
                }
            }
        }


        #endregion

        #region 用药调整
        private void btn添加药物调整_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物调整.Tag, gc药物调整);
        }

        private void btn删除药物调整_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物调整.Tag, gc药物调整);
        }

        private void radio药物调整_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio药物调整.EditValue != null && radio药物调整.EditValue.ToString() == "1")
            {
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                //if (_BLL.CurrentBusiness.Tables[tb_MXB高血压患者三级管理记录表_用药调整.__TableName].Rows.Count <= 0)
                //    btn添加药物调整.PerformClick();
            }
            else
            {
                layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }
        #endregion

        private void radio转诊情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio转诊情况.EditValue != null && radio转诊情况.EditValue.ToString() == "1")
            {
                this.txt转诊原因.Enabled = this.txt转诊科别.Enabled = this.txt转诊联系人.Enabled = this.txt转诊联系电话.Enabled = this.txt转诊结果.Enabled = true;
            }
            else
            {
                this.txt转诊原因.Enabled = this.txt转诊科别.Enabled = this.txt转诊联系人.Enabled = this.txt转诊联系电话.Enabled = this.txt转诊结果.Enabled = false;
                this.txt转诊原因.Text = this.txt转诊科别.Text = this.txt转诊联系人.Text = this.txt转诊联系电话.Text = this.txt转诊结果.Text = "";
            }
        }

        bool b手签更新 = false;
        FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
        private void sbtnFingerPrint_Click(object sender, EventArgs e)
        {
            DialogResult result = frm.ShowDialog();
            if (result == DialogResult.OK)
            {
                b手签更新 = true;
                txt居民签名.Image = frm.bmp;
            }
        }
		//Begin WXF 2018-11-28 | 15:18
        //根据随访日期和下一步管理措施自动推算下次随访时间 
        private void radio管理措施_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt发生时间.Text))
            {
                XtraMessageBox.Show("无随访日期无法推算下次随访时间!");
                return;
            }

            DateTime date = Convert.ToDateTime(txt发生时间.Text);
            if (!radio管理措施.EditValue.ToString().Equals("1"))
            {
                date = date.AddDays(+13);
            }
            else
            {
                date = date.AddMonths(+3);
            }

            txt下次随访时间.Text = date.ToString("yyyy-MM-dd");
        }

        private void txt发生时间_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt下次随访时间.Text) && radio管理措施.EditValue.ToString().Equals("1") && !string.IsNullOrEmpty(txt发生时间.Text))
            {
                txt下次随访时间.Text = Convert.ToDateTime(txt发生时间.Text).AddMonths(+3).ToString("yyyy-MM-dd");
            }
        }
        
        //20190104 如果收缩压≥180mmHg或者舒张压≥110mmHg，此次随访分类逻辑选择控制不满意；下一步管理措施中逻辑性选择紧急转诊。 ▼
        string serverDateTemp = "";

        private void txt血压值_Leave(object sender, EventArgs e)
        {
            if ((!string.IsNullOrWhiteSpace(txt血压值.Txt1.Text) && Convert.ToDecimal(this.txt血压值.Txt1.Text) >= 180m)
                 || (!string.IsNullOrWhiteSpace(txt血压值.Txt2.Text) && Convert.ToDecimal(this.txt血压值.Txt2.Text) >= 110m))
            {
                radio管理措施.EditValue = "4";
            }

            //计算年龄
            //如果收缩压≥140mmHg（年龄≥65岁的患者 收缩压≥150mmHg）或者＞舒张压≥90mmHg，此次随访分类逻辑性填入控制不满意 ▼
            int age = 0;
            if (txt身份证号.Text.Length == 18)
            {
                age = Convert.ToInt32(serverDateTemp.Substring(0, 4)) - Convert.ToInt32(txt身份证号.Text.Substring(6, 4));

            }
            else if (txt身份证号.Text.Length == 15)
            {
                age = Convert.ToInt32(serverDateTemp.Substring(0, 4)) - Convert.ToInt32("19" + txt身份证号.Text.Substring(6, 4));
            }
            else { }
            //计算年龄
        }
        
        private void radio管理措施_EditValueChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(txt发生时间.Text))
            {
                switch (radio管理措施.EditValue.ToString())
                {
                    case "1":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                        break;
                    case "2":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        break;
                    case "3":
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        break;
                    case "4":
                        if (txt备注.Text.Contains("拒绝服药") || txt备注.Text.Contains("拒绝转诊"))
                        {
                            txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                        }
                        break;
                }
            }
        }
        
        private void sbtn便捷录入备注_Click(object sender, EventArgs e)
        {
            ShowLittleForm(new string[] {
            "拒绝服药",
            "拒绝转诊"}, txt备注);
        }

        private void ShowLittleForm(string[] arrlist, TextEdit edit)
        {
            Form frm = new Form();
            frm.ClientSize = new System.Drawing.Size(390, 176);
            frm.MaximizeBox = false;
            frm.MinimizeBox = false;
            frm.Text = "双击选择，选择完后关闭此窗口即可";
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

            CheckedListBox list = new CheckedListBox();
            list.FormattingEnabled = true;
            list.Items.AddRange(arrlist);
            list.Location = new System.Drawing.Point(12, 12);
            list.MultiColumn = true;
            list.Name = "checkedListBox便捷录入备注";
            list.Size = new System.Drawing.Size(359, 148);
            list.TabIndex = 0;
            frm.Controls.Add(list);

            frm.ShowDialog();

            string strTemp = "";
            for (int index = 0; index < list.CheckedItems.Count; index++)
            {
                if (!edit.Text.Contains(list.CheckedItems[index].ToString()))
                {
                    strTemp += "," + list.CheckedItems[index].ToString(); ;
                }
            }

            if (string.IsNullOrWhiteSpace(edit.Text))
            {
                edit.Text = strTemp.Trim(',');
            }
            else
            {
                edit.Text += strTemp;
            }

            list.Dispose();
            frm.Dispose();
        }

        private void txt发生时间_EditValueChanged2(object sender, EventArgs e)
        {
            radio管理措施_EditValueChanged(null, null);
        }


        private void txt备注_EditValueChanged(object sender, EventArgs e)
        {
            if (txt备注.Text.Contains("拒绝转诊") || txt备注.Text.Contains("拒绝服药"))
            {
                if (radio管理措施.EditValue=="4")
                {
                    if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
                    {
                        txt下次随访时间.Text = txt发生时间.DateTime.AddMonths(3).ToString("yyyy-MM-dd");
                    }
                }
            }
            else
            {
                if (radio管理措施.EditValue == "4")
                {
                    if (!string.IsNullOrWhiteSpace(txt发生时间.Text))
                    {
                        txt下次随访时间.Text = txt发生时间.DateTime.AddDays(14).ToString("yyyy-MM-dd");
                    }
                }
            }
        }

        private void btn重置_Click(object sender, EventArgs e)
        {
            Form f = this.Parent as Form;
            if (f != null)
                f.Close();
        }
											 
    }
}
