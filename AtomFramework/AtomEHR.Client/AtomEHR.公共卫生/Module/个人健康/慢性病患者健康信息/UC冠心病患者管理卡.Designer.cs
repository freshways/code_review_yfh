﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC冠心病患者管理卡
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC冠心病患者管理卡));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.txt管理卡编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.radio生活自理能力 = new DevExpress.XtraEditors.RadioGroup();
            this.radio体育锻炼 = new DevExpress.XtraEditors.RadioGroup();
            this.radio饮酒情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radio吸烟情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt心肌酶学 = new DevExpress.XtraEditors.TextEdit();
            this.txt冠状动脉 = new DevExpress.XtraEditors.TextEdit();
            this.txt心脏彩超 = new DevExpress.XtraEditors.TextEdit();
            this.txt心电运动 = new DevExpress.XtraEditors.TextEdit();
            this.txt心电检查 = new DevExpress.XtraEditors.TextEdit();
            this.txt心率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt甘油三酯 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio冠心病类型 = new DevExpress.XtraEditors.RadioGroup();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.radio终止理由 = new DevExpress.XtraEditors.RadioGroup();
            this.txt终止管理日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio终止管理 = new DevExpress.XtraEditors.RadioGroup();
            this.fl非药物治疗 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit44 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit45 = new DevExpress.XtraEditors.CheckEdit();
            this.ck非药物未采取措施 = new DevExpress.XtraEditors.CheckEdit();
            this.fl特殊治疗 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch特殊治疗无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt体检日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt低密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt高密度蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txtBMI = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.fl病史 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.ck无以上病史 = new DevExpress.XtraEditors.CheckEdit();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.ck无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.fl家族史 = new System.Windows.Forms.FlowLayoutPanel();
            this.jzs_ck高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck以上都无 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck不详 = new DevExpress.XtraEditors.CheckEdit();
            this.jzs_ck拒答 = new DevExpress.XtraEditors.CheckEdit();
            this.radio病例来源 = new DevExpress.XtraEditors.RadioGroup();
            this.txt确诊机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt确诊日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio管理组别 = new DevExpress.XtraEditors.RadioGroup();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem终止日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem终止理由 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout用药列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活自理能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio体育锻炼.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心肌酶学.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt冠状动脉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏彩超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电运动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio冠心病类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止理由.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止管理.Properties)).BeginInit();
            this.fl非药物治疗.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck非药物未采取措施.Properties)).BeginInit();
            this.fl特殊治疗.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch特殊治疗无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).BeginInit();
            this.fl病史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck无以上病史.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            this.fl家族史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck以上都无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck不详.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck拒答.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio病例来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理组别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem终止日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem终止理由)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout用药列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(757, 32);
            this.panelControl1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(753, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(3, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "保存";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.btn删除药物);
            this.layoutControl1.Controls.Add(this.btn添加药物);
            this.layoutControl1.Controls.Add(this.txt管理卡编号);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案编号);
            this.layoutControl1.Controls.Add(this.radio生活自理能力);
            this.layoutControl1.Controls.Add(this.radio体育锻炼);
            this.layoutControl1.Controls.Add(this.radio饮酒情况);
            this.layoutControl1.Controls.Add(this.radio吸烟情况);
            this.layoutControl1.Controls.Add(this.txt心肌酶学);
            this.layoutControl1.Controls.Add(this.txt冠状动脉);
            this.layoutControl1.Controls.Add(this.txt心脏彩超);
            this.layoutControl1.Controls.Add(this.txt心电运动);
            this.layoutControl1.Controls.Add(this.txt心电检查);
            this.layoutControl1.Controls.Add(this.txt心率);
            this.layoutControl1.Controls.Add(this.txt甘油三酯);
            this.layoutControl1.Controls.Add(this.radio冠心病类型);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab最近更新时间);
            this.layoutControl1.Controls.Add(this.lab创建时间);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.radio终止理由);
            this.layoutControl1.Controls.Add(this.txt终止管理日期);
            this.layoutControl1.Controls.Add(this.radio终止管理);
            this.layoutControl1.Controls.Add(this.fl非药物治疗);
            this.layoutControl1.Controls.Add(this.fl特殊治疗);
            this.layoutControl1.Controls.Add(this.radio用药情况);
            this.layoutControl1.Controls.Add(this.txt体检日期);
            this.layoutControl1.Controls.Add(this.txt胆固醇);
            this.layoutControl1.Controls.Add(this.txt低密度蛋白);
            this.layoutControl1.Controls.Add(this.txt高密度蛋白);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.txtBMI);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.fl病史);
            this.layoutControl1.Controls.Add(this.fl症状);
            this.layoutControl1.Controls.Add(this.fl家族史);
            this.layoutControl1.Controls.Add(this.radio病例来源);
            this.layoutControl1.Controls.Add(this.txt确诊机构);
            this.layoutControl1.Controls.Add(this.txt确诊日期);
            this.layoutControl1.Controls.Add(this.radio管理组别);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(591, 224, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(757, 468);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(734, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 4;
            this.lab考核项.Text = "考核项：17     缺项：17 完整度：0% ";
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(3, 599);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(734, 71);
            this.gcDetail.TabIndex = 92;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 349;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 363;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Location = new System.Drawing.Point(413, 573);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(35, 22);
            this.btn删除药物.StyleController = this.layoutControl1;
            this.btn删除药物.TabIndex = 94;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Location = new System.Drawing.Point(373, 573);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(36, 22);
            this.btn添加药物.StyleController = this.layoutControl1;
            this.btn添加药物.TabIndex = 93;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // txt管理卡编号
            // 
            this.txt管理卡编号.Location = new System.Drawing.Point(88, 144);
            this.txt管理卡编号.Name = "txt管理卡编号";
            this.txt管理卡编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt管理卡编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt管理卡编号.Properties.ReadOnly = true;
            this.txt管理卡编号.Size = new System.Drawing.Size(280, 20);
            this.txt管理卡编号.StyleController = this.layoutControl1;
            this.txt管理卡编号.TabIndex = 71;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(457, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(280, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 70;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(88, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(280, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 69;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(457, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(280, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 68;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(88, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(280, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 67;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(457, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(280, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 66;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(88, 120);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(280, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 65;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(457, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(280, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 64;
            // 
            // txt个人档案编号
            // 
            this.txt个人档案编号.Location = new System.Drawing.Point(88, 48);
            this.txt个人档案编号.Name = "txt个人档案编号";
            this.txt个人档案编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案编号.Properties.ReadOnly = true;
            this.txt个人档案编号.Size = new System.Drawing.Size(280, 20);
            this.txt个人档案编号.StyleController = this.layoutControl1;
            this.txt个人档案编号.TabIndex = 63;
            // 
            // radio生活自理能力
            // 
            this.radio生活自理能力.EditValue = "1";
            this.radio生活自理能力.Location = new System.Drawing.Point(456, 544);
            this.radio生活自理能力.Name = "radio生活自理能力";
            this.radio生活自理能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "完全自理"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "部分自理"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "完全不能自理")});
            this.radio生活自理能力.Size = new System.Drawing.Size(281, 25);
            this.radio生活自理能力.StyleController = this.layoutControl1;
            this.radio生活自理能力.TabIndex = 62;
            // 
            // radio体育锻炼
            // 
            this.radio体育锻炼.EditValue = "1";
            this.radio体育锻炼.Location = new System.Drawing.Point(68, 544);
            this.radio体育锻炼.Name = "radio体育锻炼";
            this.radio体育锻炼.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不锻炼"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "每月1-3次"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "每周1-2次"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "每周>=3次")});
            this.radio体育锻炼.Size = new System.Drawing.Size(301, 25);
            this.radio体育锻炼.StyleController = this.layoutControl1;
            this.radio体育锻炼.TabIndex = 61;
            // 
            // radio饮酒情况
            // 
            this.radio饮酒情况.EditValue = "1";
            this.radio饮酒情况.Location = new System.Drawing.Point(456, 515);
            this.radio饮酒情况.Name = "radio饮酒情况";
            this.radio饮酒情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不饮酒"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "经常饮酒")});
            this.radio饮酒情况.Size = new System.Drawing.Size(281, 25);
            this.radio饮酒情况.StyleController = this.layoutControl1;
            this.radio饮酒情况.TabIndex = 60;
            // 
            // radio吸烟情况
            // 
            this.radio吸烟情况.EditValue = "1";
            this.radio吸烟情况.Location = new System.Drawing.Point(68, 515);
            this.radio吸烟情况.Name = "radio吸烟情况";
            this.radio吸烟情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "不吸烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "吸烟")});
            this.radio吸烟情况.Size = new System.Drawing.Size(301, 25);
            this.radio吸烟情况.StyleController = this.layoutControl1;
            this.radio吸烟情况.TabIndex = 59;
            // 
            // txt心肌酶学
            // 
            this.txt心肌酶学.Location = new System.Drawing.Point(139, 490);
            this.txt心肌酶学.Name = "txt心肌酶学";
            this.txt心肌酶学.Size = new System.Drawing.Size(229, 20);
            this.txt心肌酶学.StyleController = this.layoutControl1;
            this.txt心肌酶学.TabIndex = 58;
            // 
            // txt冠状动脉
            // 
            this.txt冠状动脉.Location = new System.Drawing.Point(507, 466);
            this.txt冠状动脉.Name = "txt冠状动脉";
            this.txt冠状动脉.Size = new System.Drawing.Size(229, 20);
            this.txt冠状动脉.StyleController = this.layoutControl1;
            this.txt冠状动脉.TabIndex = 57;
            // 
            // txt心脏彩超
            // 
            this.txt心脏彩超.Location = new System.Drawing.Point(139, 466);
            this.txt心脏彩超.Name = "txt心脏彩超";
            this.txt心脏彩超.Size = new System.Drawing.Size(229, 20);
            this.txt心脏彩超.StyleController = this.layoutControl1;
            this.txt心脏彩超.TabIndex = 56;
            // 
            // txt心电运动
            // 
            this.txt心电运动.Location = new System.Drawing.Point(507, 442);
            this.txt心电运动.Name = "txt心电运动";
            this.txt心电运动.Size = new System.Drawing.Size(229, 20);
            this.txt心电运动.StyleController = this.layoutControl1;
            this.txt心电运动.TabIndex = 55;
            // 
            // txt心电检查
            // 
            this.txt心电检查.Location = new System.Drawing.Point(139, 442);
            this.txt心电检查.Name = "txt心电检查";
            this.txt心电检查.Size = new System.Drawing.Size(229, 20);
            this.txt心电检查.StyleController = this.layoutControl1;
            this.txt心电检查.TabIndex = 54;
            // 
            // txt心率
            // 
            this.txt心率.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt心率.Lbl1Text = "次/分";
            this.txt心率.Location = new System.Drawing.Point(636, 348);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(100, 20);
            this.txt心率.TabIndex = 53;
            this.txt心率.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt甘油三酯
            // 
            this.txt甘油三酯.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt甘油三酯.Lbl1Text = "mmol/L";
            this.txt甘油三酯.Location = new System.Drawing.Point(69, 396);
            this.txt甘油三酯.Name = "txt甘油三酯";
            this.txt甘油三酯.Size = new System.Drawing.Size(120, 20);
            this.txt甘油三酯.TabIndex = 29;
            this.txt甘油三酯.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // radio冠心病类型
            // 
            this.radio冠心病类型.EditValue = "99";
            this.radio冠心病类型.Location = new System.Drawing.Point(88, 250);
            this.radio冠心病类型.Name = "radio冠心病类型";
            this.radio冠心病类型.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "稳定型心绞痛"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不稳定型心绞痛"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "急性心肌梗死"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "陈旧性心肌梗死"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "猝死型"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他不确定类型")});
            this.radio冠心病类型.Size = new System.Drawing.Size(649, 25);
            this.radio冠心病类型.StyleController = this.layoutControl1;
            this.radio冠心病类型.TabIndex = 52;
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(98, 834);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(639, 20);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 51;
            this.lab当前所属机构.Text = "labelControl16";
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(558, 810);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(179, 20);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 50;
            this.lab创建机构.Text = "labelControl15";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(319, 810);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(140, 20);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 49;
            this.lab最近修改人.Text = "labelControl14";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(98, 810);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(122, 20);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 48;
            this.lab创建人.Text = "labelControl13";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近更新时间.Location = new System.Drawing.Point(558, 786);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(179, 14);
            this.lab最近更新时间.StyleController = this.layoutControl1;
            this.lab最近更新时间.TabIndex = 47;
            this.lab最近更新时间.Text = "labelControl12";
            // 
            // lab创建时间
            // 
            this.lab创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建时间.Location = new System.Drawing.Point(319, 786);
            this.lab创建时间.Name = "lab创建时间";
            this.lab创建时间.Size = new System.Drawing.Size(140, 20);
            this.lab创建时间.StyleController = this.layoutControl1;
            this.lab创建时间.TabIndex = 46;
            this.lab创建时间.Text = "labelControl11";
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(98, 786);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(122, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 45;
            // 
            // radio终止理由
            // 
            this.radio终止理由.Location = new System.Drawing.Point(319, 762);
            this.radio终止理由.Name = "radio终止理由";
            this.radio终止理由.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "死亡"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "迁出"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "拒绝"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "失访")});
            this.radio终止理由.Size = new System.Drawing.Size(418, 20);
            this.radio终止理由.StyleController = this.layoutControl1;
            this.radio终止理由.TabIndex = 44;
            // 
            // txt终止管理日期
            // 
            this.txt终止管理日期.EditValue = null;
            this.txt终止管理日期.Location = new System.Drawing.Point(98, 762);
            this.txt终止管理日期.Name = "txt终止管理日期";
            this.txt终止管理日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt终止管理日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt终止管理日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt终止管理日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt终止管理日期.Size = new System.Drawing.Size(122, 20);
            this.txt终止管理日期.StyleController = this.layoutControl1;
            this.txt终止管理日期.TabIndex = 43;
            // 
            // radio终止管理
            // 
            this.radio终止管理.EditValue = "2";
            this.radio终止管理.Location = new System.Drawing.Point(98, 738);
            this.radio终止管理.Name = "radio终止管理";
            this.radio终止管理.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio终止管理.Size = new System.Drawing.Size(122, 20);
            this.radio终止管理.StyleController = this.layoutControl1;
            this.radio终止管理.TabIndex = 42;
            this.radio终止管理.SelectedIndexChanged += new System.EventHandler(this.radio终止管理_SelectedIndexChanged);
            // 
            // fl非药物治疗
            // 
            this.fl非药物治疗.Controls.Add(this.checkEdit38);
            this.fl非药物治疗.Controls.Add(this.checkEdit39);
            this.fl非药物治疗.Controls.Add(this.checkEdit40);
            this.fl非药物治疗.Controls.Add(this.checkEdit41);
            this.fl非药物治疗.Controls.Add(this.checkEdit42);
            this.fl非药物治疗.Controls.Add(this.checkEdit43);
            this.fl非药物治疗.Controls.Add(this.checkEdit44);
            this.fl非药物治疗.Controls.Add(this.checkEdit45);
            this.fl非药物治疗.Controls.Add(this.ck非药物未采取措施);
            this.fl非药物治疗.Location = new System.Drawing.Point(98, 698);
            this.fl非药物治疗.Name = "fl非药物治疗";
            this.fl非药物治疗.Size = new System.Drawing.Size(639, 36);
            this.fl非药物治疗.TabIndex = 41;
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(0, 0);
            this.checkEdit38.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Caption = "限盐";
            this.checkEdit38.Size = new System.Drawing.Size(79, 19);
            this.checkEdit38.TabIndex = 25;
            this.checkEdit38.Tag = "1";
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(79, 0);
            this.checkEdit39.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Caption = "减少吸烟量或戒烟";
            this.checkEdit39.Size = new System.Drawing.Size(124, 19);
            this.checkEdit39.TabIndex = 26;
            this.checkEdit39.Tag = "2";
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(203, 0);
            this.checkEdit40.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Caption = "减少饮酒量或戒酒";
            this.checkEdit40.Size = new System.Drawing.Size(129, 19);
            this.checkEdit40.TabIndex = 27;
            this.checkEdit40.Tag = "3";
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(332, 0);
            this.checkEdit41.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Caption = "减少膳食脂肪";
            this.checkEdit41.Size = new System.Drawing.Size(97, 19);
            this.checkEdit41.TabIndex = 28;
            this.checkEdit41.Tag = "4";
            // 
            // checkEdit42
            // 
            this.checkEdit42.Location = new System.Drawing.Point(429, 0);
            this.checkEdit42.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit42.Name = "checkEdit42";
            this.checkEdit42.Properties.Caption = "减轻体重";
            this.checkEdit42.Size = new System.Drawing.Size(72, 19);
            this.checkEdit42.TabIndex = 29;
            this.checkEdit42.Tag = "5";
            // 
            // checkEdit43
            // 
            this.checkEdit43.Location = new System.Drawing.Point(501, 0);
            this.checkEdit43.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit43.Name = "checkEdit43";
            this.checkEdit43.Properties.Caption = "有规律体育运动";
            this.checkEdit43.Size = new System.Drawing.Size(110, 19);
            this.checkEdit43.TabIndex = 30;
            this.checkEdit43.Tag = "6";
            // 
            // checkEdit44
            // 
            this.checkEdit44.Location = new System.Drawing.Point(0, 19);
            this.checkEdit44.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit44.Name = "checkEdit44";
            this.checkEdit44.Properties.Caption = "放松精神";
            this.checkEdit44.Size = new System.Drawing.Size(79, 19);
            this.checkEdit44.TabIndex = 31;
            this.checkEdit44.Tag = "7";
            // 
            // checkEdit45
            // 
            this.checkEdit45.Location = new System.Drawing.Point(79, 19);
            this.checkEdit45.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit45.Name = "checkEdit45";
            this.checkEdit45.Properties.Caption = "心理指导";
            this.checkEdit45.Size = new System.Drawing.Size(75, 19);
            this.checkEdit45.TabIndex = 32;
            this.checkEdit45.Tag = "8";
            // 
            // ck非药物未采取措施
            // 
            this.ck非药物未采取措施.Location = new System.Drawing.Point(154, 19);
            this.ck非药物未采取措施.Margin = new System.Windows.Forms.Padding(0);
            this.ck非药物未采取措施.Name = "ck非药物未采取措施";
            this.ck非药物未采取措施.Properties.Caption = "未采取措施";
            this.ck非药物未采取措施.Size = new System.Drawing.Size(84, 19);
            this.ck非药物未采取措施.TabIndex = 33;
            this.ck非药物未采取措施.Tag = "99";
            this.ck非药物未采取措施.CheckedChanged += new System.EventHandler(this.ck非药物未采取措施_CheckedChanged);
            // 
            // fl特殊治疗
            // 
            this.fl特殊治疗.Controls.Add(this.ch特殊治疗无);
            this.fl特殊治疗.Controls.Add(this.checkEdit34);
            this.fl特殊治疗.Controls.Add(this.checkEdit35);
            this.fl特殊治疗.Controls.Add(this.checkEdit36);
            this.fl特殊治疗.Location = new System.Drawing.Point(319, 674);
            this.fl特殊治疗.Name = "fl特殊治疗";
            this.fl特殊治疗.Size = new System.Drawing.Size(418, 20);
            this.fl特殊治疗.TabIndex = 40;
            // 
            // ch特殊治疗无
            // 
            this.ch特殊治疗无.Location = new System.Drawing.Point(0, 0);
            this.ch特殊治疗无.Margin = new System.Windows.Forms.Padding(0);
            this.ch特殊治疗无.Name = "ch特殊治疗无";
            this.ch特殊治疗无.Properties.Caption = "无";
            this.ch特殊治疗无.Size = new System.Drawing.Size(50, 19);
            this.ch特殊治疗无.TabIndex = 21;
            this.ch特殊治疗无.Tag = "0";
            this.ch特殊治疗无.CheckedChanged += new System.EventHandler(this.ch特殊治疗无_CheckedChanged);
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(50, 0);
            this.checkEdit34.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "外科手术治疗";
            this.checkEdit34.Size = new System.Drawing.Size(102, 19);
            this.checkEdit34.TabIndex = 22;
            this.checkEdit34.Tag = "1";
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(152, 0);
            this.checkEdit35.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "介入治疗";
            this.checkEdit35.Size = new System.Drawing.Size(80, 19);
            this.checkEdit35.TabIndex = 23;
            this.checkEdit35.Tag = "2";
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(232, 0);
            this.checkEdit36.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Caption = "起搏器";
            this.checkEdit36.Size = new System.Drawing.Size(79, 19);
            this.checkEdit36.TabIndex = 24;
            this.checkEdit36.Tag = "3";
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(68, 573);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio用药情况.Size = new System.Drawing.Size(301, 22);
            this.radio用药情况.StyleController = this.layoutControl1;
            this.radio用药情况.TabIndex = 37;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // txt体检日期
            // 
            this.txt体检日期.EditValue = null;
            this.txt体检日期.Location = new System.Drawing.Point(636, 396);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt体检日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt体检日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt体检日期.Size = new System.Drawing.Size(100, 20);
            this.txt体检日期.StyleController = this.layoutControl1;
            this.txt体检日期.TabIndex = 36;
            // 
            // txt胆固醇
            // 
            this.txt胆固醇.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt胆固醇.Lbl1Text = "mmol/L";
            this.txt胆固醇.Location = new System.Drawing.Point(258, 396);
            this.txt胆固醇.Name = "txt胆固醇";
            this.txt胆固醇.Size = new System.Drawing.Size(86, 20);
            this.txt胆固醇.TabIndex = 35;
            this.txt胆固醇.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt低密度蛋白
            // 
            this.txt低密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt低密度蛋白.Lbl1Text = "mmol/L";
            this.txt低密度蛋白.Location = new System.Drawing.Point(636, 372);
            this.txt低密度蛋白.Name = "txt低密度蛋白";
            this.txt低密度蛋白.Size = new System.Drawing.Size(100, 20);
            this.txt低密度蛋白.TabIndex = 34;
            this.txt低密度蛋白.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt高密度蛋白
            // 
            this.txt高密度蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt高密度蛋白.Lbl1Text = "mmol/L";
            this.txt高密度蛋白.Location = new System.Drawing.Point(447, 372);
            this.txt高密度蛋白.Name = "txt高密度蛋白";
            this.txt高密度蛋白.Size = new System.Drawing.Size(81, 20);
            this.txt高密度蛋白.TabIndex = 33;
            this.txt高密度蛋白.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(258, 372);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(86, 20);
            this.txt空腹血糖.TabIndex = 32;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(8, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(69, 372);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(120, 20);
            this.txt血压值.TabIndex = 31;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(35, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(35, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt腰围.Lbl1Text = "CM";
            this.txt腰围.Location = new System.Drawing.Point(447, 396);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(81, 20);
            this.txt腰围.TabIndex = 30;
            this.txt腰围.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txtBMI
            // 
            this.txtBMI.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txtBMI.Lbl1Text = "kg/㎡";
            this.txtBMI.Location = new System.Drawing.Point(447, 348);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Size = new System.Drawing.Size(100, 20);
            this.txtBMI.TabIndex = 29;
            this.txtBMI.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt体重.Lbl1Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(258, 348);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(100, 20);
            this.txt体重.TabIndex = 28;
            this.txt体重.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(69, 348);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(120, 20);
            this.txt身高.TabIndex = 27;
            this.txt身高.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // fl病史
            // 
            this.fl病史.Controls.Add(this.checkEdit22);
            this.fl病史.Controls.Add(this.checkEdit23);
            this.fl病史.Controls.Add(this.checkEdit24);
            this.fl病史.Controls.Add(this.checkEdit30);
            this.fl病史.Controls.Add(this.ck无以上病史);
            this.fl病史.Location = new System.Drawing.Point(88, 303);
            this.fl病史.Name = "fl病史";
            this.fl病史.Size = new System.Drawing.Size(649, 20);
            this.fl病史.TabIndex = 21;
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(0, 0);
            this.checkEdit22.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "高血压";
            this.checkEdit22.Size = new System.Drawing.Size(56, 19);
            this.checkEdit22.TabIndex = 14;
            this.checkEdit22.Tag = "1";
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(56, 0);
            this.checkEdit23.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "糖尿病";
            this.checkEdit23.Size = new System.Drawing.Size(62, 19);
            this.checkEdit23.TabIndex = 15;
            this.checkEdit23.Tag = "2";
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(118, 0);
            this.checkEdit24.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "高脂血症";
            this.checkEdit24.Size = new System.Drawing.Size(75, 19);
            this.checkEdit24.TabIndex = 16;
            this.checkEdit24.Tag = "3";
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(193, 0);
            this.checkEdit30.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "脑卒中";
            this.checkEdit30.Size = new System.Drawing.Size(59, 19);
            this.checkEdit30.TabIndex = 22;
            this.checkEdit30.Tag = "4";
            // 
            // ck无以上病史
            // 
            this.ck无以上病史.EditValue = true;
            this.ck无以上病史.Location = new System.Drawing.Point(252, 0);
            this.ck无以上病史.Margin = new System.Windows.Forms.Padding(0);
            this.ck无以上病史.Name = "ck无以上病史";
            this.ck无以上病史.Properties.Caption = "无以上病史";
            this.ck无以上病史.Size = new System.Drawing.Size(87, 19);
            this.ck无以上病史.TabIndex = 24;
            this.ck无以上病史.Tag = "100";
            this.ck无以上病史.CheckedChanged += new System.EventHandler(this.ck无以上病史_CheckedChanged);
            // 
            // fl症状
            // 
            this.fl症状.Controls.Add(this.ck无症状);
            this.fl症状.Controls.Add(this.checkEdit9);
            this.fl症状.Controls.Add(this.checkEdit10);
            this.fl症状.Controls.Add(this.checkEdit11);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Location = new System.Drawing.Point(88, 279);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(649, 20);
            this.fl症状.TabIndex = 19;
            // 
            // ck无症状
            // 
            this.ck无症状.EditValue = true;
            this.ck无症状.Location = new System.Drawing.Point(0, 0);
            this.ck无症状.Margin = new System.Windows.Forms.Padding(0);
            this.ck无症状.Name = "ck无症状";
            this.ck无症状.Properties.Caption = "无症状";
            this.ck无症状.Size = new System.Drawing.Size(58, 19);
            this.ck无症状.TabIndex = 7;
            this.ck无症状.Tag = "0";
            this.ck无症状.CheckedChanged += new System.EventHandler(this.ck无症状_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(58, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "胸痛";
            this.checkEdit9.Size = new System.Drawing.Size(51, 19);
            this.checkEdit9.TabIndex = 8;
            this.checkEdit9.Tag = "1";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(109, 0);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "胸闷";
            this.checkEdit10.Size = new System.Drawing.Size(50, 19);
            this.checkEdit10.TabIndex = 9;
            this.checkEdit10.Tag = "2";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(159, 0);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "心悸";
            this.checkEdit11.Size = new System.Drawing.Size(51, 19);
            this.checkEdit11.TabIndex = 10;
            this.checkEdit11.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(210, 0);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "肩、背等部位放射性疼痛";
            this.checkEdit12.Size = new System.Drawing.Size(157, 19);
            this.checkEdit12.TabIndex = 11;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(367, 0);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "上腹痛";
            this.checkEdit13.Size = new System.Drawing.Size(61, 19);
            this.checkEdit13.TabIndex = 12;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(428, 0);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "心动过速或过缓 ";
            this.checkEdit14.Size = new System.Drawing.Size(114, 19);
            this.checkEdit14.TabIndex = 13;
            this.checkEdit14.Tag = "6";
            // 
            // fl家族史
            // 
            this.fl家族史.Controls.Add(this.jzs_ck高血压);
            this.fl家族史.Controls.Add(this.jzs_ck冠心病);
            this.fl家族史.Controls.Add(this.jzs_ck脑卒中);
            this.fl家族史.Controls.Add(this.jzs_ck糖尿病);
            this.fl家族史.Controls.Add(this.jzs_ck以上都无);
            this.fl家族史.Controls.Add(this.jzs_ck不详);
            this.fl家族史.Controls.Add(this.jzs_ck拒答);
            this.fl家族史.Location = new System.Drawing.Point(88, 202);
            this.fl家族史.Name = "fl家族史";
            this.fl家族史.Size = new System.Drawing.Size(649, 20);
            this.fl家族史.TabIndex = 18;
            // 
            // jzs_ck高血压
            // 
            this.jzs_ck高血压.Location = new System.Drawing.Point(0, 0);
            this.jzs_ck高血压.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck高血压.Name = "jzs_ck高血压";
            this.jzs_ck高血压.Properties.Caption = "高血压";
            this.jzs_ck高血压.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck高血压.TabIndex = 7;
            this.jzs_ck高血压.Tag = "1";
            // 
            // jzs_ck冠心病
            // 
            this.jzs_ck冠心病.Location = new System.Drawing.Point(75, 0);
            this.jzs_ck冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck冠心病.Name = "jzs_ck冠心病";
            this.jzs_ck冠心病.Properties.Caption = "冠心病";
            this.jzs_ck冠心病.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck冠心病.TabIndex = 8;
            this.jzs_ck冠心病.Tag = "2";
            // 
            // jzs_ck脑卒中
            // 
            this.jzs_ck脑卒中.Location = new System.Drawing.Point(150, 0);
            this.jzs_ck脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck脑卒中.Name = "jzs_ck脑卒中";
            this.jzs_ck脑卒中.Properties.Caption = "脑卒中";
            this.jzs_ck脑卒中.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck脑卒中.TabIndex = 9;
            this.jzs_ck脑卒中.Tag = "3";
            // 
            // jzs_ck糖尿病
            // 
            this.jzs_ck糖尿病.Location = new System.Drawing.Point(225, 0);
            this.jzs_ck糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck糖尿病.Name = "jzs_ck糖尿病";
            this.jzs_ck糖尿病.Properties.Caption = "糖尿病";
            this.jzs_ck糖尿病.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck糖尿病.TabIndex = 10;
            this.jzs_ck糖尿病.Tag = "4";
            // 
            // jzs_ck以上都无
            // 
            this.jzs_ck以上都无.Location = new System.Drawing.Point(300, 0);
            this.jzs_ck以上都无.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck以上都无.Name = "jzs_ck以上都无";
            this.jzs_ck以上都无.Properties.Caption = "以上都无";
            this.jzs_ck以上都无.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck以上都无.TabIndex = 11;
            this.jzs_ck以上都无.Tag = "98";
            this.jzs_ck以上都无.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // jzs_ck不详
            // 
            this.jzs_ck不详.EditValue = true;
            this.jzs_ck不详.Location = new System.Drawing.Point(375, 0);
            this.jzs_ck不详.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck不详.Name = "jzs_ck不详";
            this.jzs_ck不详.Properties.Caption = "不详";
            this.jzs_ck不详.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck不详.TabIndex = 12;
            this.jzs_ck不详.Tag = "99";
            this.jzs_ck不详.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // jzs_ck拒答
            // 
            this.jzs_ck拒答.Location = new System.Drawing.Point(450, 0);
            this.jzs_ck拒答.Margin = new System.Windows.Forms.Padding(0);
            this.jzs_ck拒答.Name = "jzs_ck拒答";
            this.jzs_ck拒答.Properties.Caption = "拒答";
            this.jzs_ck拒答.Size = new System.Drawing.Size(75, 19);
            this.jzs_ck拒答.TabIndex = 13;
            this.jzs_ck拒答.Tag = "100";
            this.jzs_ck拒答.CheckedChanged += new System.EventHandler(this.jzs_ck以上都无_CheckedChanged);
            // 
            // radio病例来源
            // 
            this.radio病例来源.EditValue = "2";
            this.radio病例来源.Location = new System.Drawing.Point(88, 173);
            this.radio病例来源.Name = "radio病例来源";
            this.radio病例来源.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "健康档案"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "健康体检"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio病例来源.Size = new System.Drawing.Size(280, 25);
            this.radio病例来源.StyleController = this.layoutControl1;
            this.radio病例来源.TabIndex = 17;
            // 
            // txt确诊机构
            // 
            this.txt确诊机构.Location = new System.Drawing.Point(375, 226);
            this.txt确诊机构.Name = "txt确诊机构";
            this.txt确诊机构.Size = new System.Drawing.Size(362, 20);
            this.txt确诊机构.StyleController = this.layoutControl1;
            this.txt确诊机构.TabIndex = 16;
            // 
            // txt确诊日期
            // 
            this.txt确诊日期.EditValue = null;
            this.txt确诊日期.Location = new System.Drawing.Point(88, 226);
            this.txt确诊日期.Name = "txt确诊日期";
            this.txt确诊日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt确诊日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt确诊日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt确诊日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt确诊日期.Size = new System.Drawing.Size(188, 20);
            this.txt确诊日期.StyleController = this.layoutControl1;
            this.txt确诊日期.TabIndex = 15;
            // 
            // radio管理组别
            // 
            this.radio管理组别.EditValue = "2";
            this.radio管理组别.Location = new System.Drawing.Point(457, 144);
            this.radio管理组别.Name = "radio管理组别";
            this.radio管理组别.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "重点组"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "常规组")});
            this.radio管理组别.Size = new System.Drawing.Size(280, 25);
            this.radio管理组别.StyleController = this.layoutControl1;
            this.radio管理组别.TabIndex = 14;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(98, 674);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "请选择";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(122, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 39;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "冠心病患者管理卡";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(740, 857);
            this.layoutControlGroup1.Text = "冠心病患者管理卡";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem49,
            this.layoutControlItem18,
            this.layoutControlGroup5,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlItem17,
            this.layoutControlItem20,
            this.layoutControlItem61,
            this.layoutControlItem60,
            this.layoutControlItem23,
            this.layoutControlItem22,
            this.layoutControlItem21,
            this.layoutControlItem62,
            this.layoutControlItem63,
            this.layoutControlItem11,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 18);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(738, 810);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.fl症状;
            this.layoutControlItem16.CustomizationFormText = "目前症状";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 231);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(738, 24);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "目前症状";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt确诊日期;
            this.layoutControlItem12.CustomizationFormText = "发病时间";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "确诊日期";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt确诊机构;
            this.layoutControlItem13.CustomizationFormText = "诊断医院";
            this.layoutControlItem13.Location = new System.Drawing.Point(277, 178);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "确诊机构";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.radio病例来源;
            this.layoutControlItem14.CustomizationFormText = "病例来源";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 125);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(369, 29);
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "病例来源";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.fl家族史;
            this.layoutControlItem15.CustomizationFormText = "家族史";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 154);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(738, 24);
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "家族史";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.radio冠心病类型;
            this.layoutControlItem49.CustomizationFormText = "冠心病类型";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(738, 29);
            this.layoutControlItem49.Tag = "check";
            this.layoutControlItem49.Text = "冠心病类型";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.fl病史;
            this.layoutControlItem18.CustomizationFormText = "病史";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 255);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(738, 24);
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "病史";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem58,
            this.layoutControlItem56,
            this.layoutControlItem59,
            this.layoutControlItem57,
            this.layoutControlItem34,
            this.emptySpaceItem1,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem终止日期,
            this.layoutControlItem终止理由,
            this.emptySpaceItem3,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layout添加药物,
            this.layout删除药物,
            this.layout用药列表});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 467);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(738, 343);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem58.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem58.Control = this.radio体育锻炼;
            this.layoutControlItem58.CustomizationFormText = "体育锻炼";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(370, 29);
            this.layoutControlItem58.Tag = "check";
            this.layoutControlItem58.Text = "体育锻炼";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem56.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.radio吸烟情况;
            this.layoutControlItem56.CustomizationFormText = "吸烟情况";
            this.layoutControlItem56.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(370, 29);
            this.layoutControlItem56.Tag = "check";
            this.layoutControlItem56.Text = "吸烟情况";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem59.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.radio生活自理能力;
            this.layoutControlItem59.CustomizationFormText = "生活自理能力";
            this.layoutControlItem59.Location = new System.Drawing.Point(370, 29);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(368, 29);
            this.layoutControlItem59.Tag = "check";
            this.layoutControlItem59.Text = "生活自理能力";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(78, 14);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem57.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.radio饮酒情况;
            this.layoutControlItem57.CustomizationFormText = "饮酒情况";
            this.layoutControlItem57.Location = new System.Drawing.Point(370, 0);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(368, 29);
            this.layoutControlItem57.Tag = "check";
            this.layoutControlItem57.Text = "饮酒情况";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(78, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem34.Control = this.radio用药情况;
            this.layoutControlItem34.CustomizationFormText = "用药情况";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(370, 26);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "用药情况";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(449, 58);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(289, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.txt服药依从性;
            this.layoutControlItem36.CustomizationFormText = "服药依从性";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 159);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem36.Tag = "check";
            this.layoutControlItem36.Text = "服药依从性";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem37.Control = this.fl特殊治疗;
            this.layoutControlItem37.CustomizationFormText = "特殊治疗";
            this.layoutControlItem37.Location = new System.Drawing.Point(221, 159);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(517, 24);
            this.layoutControlItem37.Tag = "check";
            this.layoutControlItem37.Text = "特殊治疗";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Control = this.fl非药物治疗;
            this.layoutControlItem38.CustomizationFormText = "非药物治疗措施";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 183);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(192, 40);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(738, 40);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Tag = "check";
            this.layoutControlItem38.Text = "非药物治疗措施";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.radio终止管理;
            this.layoutControlItem39.CustomizationFormText = "是否终止管理";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 223);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "是否终止管理";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem终止日期
            // 
            this.layoutControlItem终止日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem终止日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem终止日期.Control = this.txt终止管理日期;
            this.layoutControlItem终止日期.CustomizationFormText = "终止管理日期";
            this.layoutControlItem终止日期.Location = new System.Drawing.Point(0, 247);
            this.layoutControlItem终止日期.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem终止日期.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem终止日期.Name = "layoutControlItem终止日期";
            this.layoutControlItem终止日期.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem终止日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem终止日期.Text = "终止管理日期";
            this.layoutControlItem终止日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem终止日期.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem终止日期.TextToControlDistance = 5;
            this.layoutControlItem终止日期.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem终止理由
            // 
            this.layoutControlItem终止理由.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem终止理由.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem终止理由.Control = this.radio终止理由;
            this.layoutControlItem终止理由.CustomizationFormText = "终止理由";
            this.layoutControlItem终止理由.Location = new System.Drawing.Point(221, 247);
            this.layoutControlItem终止理由.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem终止理由.Name = "layoutControlItem终止理由";
            this.layoutControlItem终止理由.Size = new System.Drawing.Size(517, 24);
            this.layoutControlItem终止理由.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem终止理由.Text = "终止理由";
            this.layoutControlItem终止理由.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem终止理由.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem终止理由.TextToControlDistance = 5;
            this.layoutControlItem终止理由.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(221, 223);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(517, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt发生时间;
            this.layoutControlItem42.CustomizationFormText = "发生时间";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 271);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "发生时间";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lab创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(221, 271);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(239, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lab最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(460, 271);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 18);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lab创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 295);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "录入人";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab最近修改人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(221, 295);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(239, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近更新人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lab创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(460, 295);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lab当前所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 319);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(738, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layout添加药物";
            this.layout添加药物.Location = new System.Drawing.Point(370, 58);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(40, 26);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layout删除药物";
            this.layout删除药物.Location = new System.Drawing.Point(410, 58);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(39, 26);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout用药列表
            // 
            this.layout用药列表.Control = this.gcDetail;
            this.layout用药列表.CustomizationFormText = " ";
            this.layout用药列表.Location = new System.Drawing.Point(0, 84);
            this.layout用药列表.MinSize = new System.Drawing.Size(199, 75);
            this.layout用药列表.Name = "layout用药列表";
            this.layout用药列表.Size = new System.Drawing.Size(738, 75);
            this.layout用药列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout用药列表.Text = " ";
            this.layout用药列表.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout用药列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout用药列表.TextToControlDistance = 0;
            this.layout用药列表.TextVisible = false;
            this.layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "体检情况";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem50,
            this.layoutControlItem32,
            this.layoutControlItem27,
            this.layoutControlItem33,
            this.layoutControlItem19});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 279);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(738, 94);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "体检情况";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.txt体重;
            this.layoutControlItem25.CustomizationFormText = "体重";
            this.layoutControlItem25.Location = new System.Drawing.Point(189, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "体重";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem24.Control = this.txt身高;
            this.layoutControlItem24.CustomizationFormText = "身高";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "身高";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem26.Control = this.txtBMI;
            this.layoutControlItem26.CustomizationFormText = "BMI";
            this.layoutControlItem26.Location = new System.Drawing.Point(358, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem26.Tag = "check";
            this.layoutControlItem26.Text = "BMI";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.txt血压值;
            this.layoutControlItem28.CustomizationFormText = "血压值";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(185, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "血压";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt空腹血糖;
            this.layoutControlItem29.CustomizationFormText = "空腹血糖";
            this.layoutControlItem29.Location = new System.Drawing.Point(189, 24);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Tag = "check";
            this.layoutControlItem29.Text = "空腹血糖";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem30.Control = this.txt高密度蛋白;
            this.layoutControlItem30.CustomizationFormText = "高密度脂蛋白";
            this.layoutControlItem30.Location = new System.Drawing.Point(358, 24);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(170, 24);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(170, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "高密度脂蛋白";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.txt低密度蛋白;
            this.layoutControlItem31.CustomizationFormText = "低密度脂蛋白";
            this.layoutControlItem31.Location = new System.Drawing.Point(547, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "低密度脂蛋白";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt甘油三酯;
            this.layoutControlItem50.CustomizationFormText = "甘油三酯";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem50.Tag = "check";
            this.layoutControlItem50.Text = "甘油三酯";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem32.Control = this.txt胆固醇;
            this.layoutControlItem32.CustomizationFormText = "胆固醇";
            this.layoutControlItem32.Location = new System.Drawing.Point(189, 48);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Tag = "check";
            this.layoutControlItem32.Text = "胆固醇";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem27.Control = this.txt腰围;
            this.layoutControlItem27.CustomizationFormText = "腰围";
            this.layoutControlItem27.Location = new System.Drawing.Point(358, 48);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(170, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(170, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "腰围";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem33.Control = this.txt体检日期;
            this.layoutControlItem33.CustomizationFormText = "体检时间";
            this.layoutControlItem33.Location = new System.Drawing.Point(547, 48);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "体检日期";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt心率;
            this.layoutControlItem19.CustomizationFormText = "心率";
            this.layoutControlItem19.Location = new System.Drawing.Point(547, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "心率";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup4.CustomizationFormText = "辅助检查";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.emptySpaceItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 373);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(738, 94);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "辅助检查";
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.txt心电检查;
            this.layoutControlItem51.CustomizationFormText = "心电图检查结果";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem51.Text = "心电图检查结果";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.txt心电运动;
            this.layoutControlItem52.CustomizationFormText = "心电图运动负荷试验结果";
            this.layoutControlItem52.Location = new System.Drawing.Point(368, 0);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem52.Text = "心电图运动负荷试验结果";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.txt心脏彩超;
            this.layoutControlItem53.CustomizationFormText = "心脏彩超检查结果 ";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem53.Text = "心脏彩超检查结果 ";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.txt冠状动脉;
            this.layoutControlItem54.CustomizationFormText = "冠状动脉造影结果";
            this.layoutControlItem54.Location = new System.Drawing.Point(368, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem54.Text = "冠状动脉造影结果";
            this.layoutControlItem54.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.txt心肌酶学;
            this.layoutControlItem55.CustomizationFormText = "心肌酶学检查结果";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(368, 24);
            this.layoutControlItem55.Text = "心肌酶学检查结果";
            this.layoutControlItem55.TextSize = new System.Drawing.Size(132, 14);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(368, 48);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(368, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt个人档案编号;
            this.layoutControlItem17.CustomizationFormText = "个人档案号";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem17.Text = "个人档案号";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt姓名;
            this.layoutControlItem20.CustomizationFormText = "姓名";
            this.layoutControlItem20.Location = new System.Drawing.Point(369, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem20.Text = "姓名";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.txt性别;
            this.layoutControlItem61.CustomizationFormText = "性别";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem61.Text = "性别";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.txt出生日期;
            this.layoutControlItem60.CustomizationFormText = "出生日期";
            this.layoutControlItem60.Location = new System.Drawing.Point(369, 24);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem60.Text = "出生日期";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt身份证号;
            this.layoutControlItem23.CustomizationFormText = "身份证号";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem23.Text = "身份证号";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt职业;
            this.layoutControlItem22.CustomizationFormText = "职业";
            this.layoutControlItem22.Location = new System.Drawing.Point(369, 48);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem22.Text = "职业";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt联系电话;
            this.layoutControlItem21.CustomizationFormText = "联系电话";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem21.Text = "联系电话";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.txt居住地址;
            this.layoutControlItem62.CustomizationFormText = "居住地址";
            this.layoutControlItem62.Location = new System.Drawing.Point(369, 72);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem62.Text = "居住地址";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.txt管理卡编号;
            this.layoutControlItem63.CustomizationFormText = "管理卡编号";
            this.layoutControlItem63.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(369, 29);
            this.layoutControlItem63.Text = "管理卡编号";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.radio管理组别;
            this.layoutControlItem11.CustomizationFormText = "管理组别";
            this.layoutControlItem11.Location = new System.Drawing.Point(369, 96);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(369, 29);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "管理组别";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(369, 125);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(369, 29);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lab考核项;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(738, 18);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // UC冠心病患者管理卡
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC冠心病患者管理卡";
            this.Size = new System.Drawing.Size(757, 500);
            this.Load += new System.EventHandler(this.UC冠心病患者管理卡_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理卡编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活自理能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio体育锻炼.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心肌酶学.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt冠状动脉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏彩超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电运动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio冠心病类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止理由.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt终止管理日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio终止管理.Properties)).EndInit();
            this.fl非药物治疗.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck非药物未采取措施.Properties)).EndInit();
            this.fl特殊治疗.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch特殊治疗无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).EndInit();
            this.fl病史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck无以上病史.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ck无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            this.fl家族史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck以上都无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck不详.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jzs_ck拒答.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio病例来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt确诊日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理组别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem终止日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem终止理由)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout用药列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.FlowLayoutPanel fl病史;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private System.Windows.Forms.FlowLayoutPanel fl家族史;
        private DevExpress.XtraEditors.RadioGroup radio病例来源;
        private DevExpress.XtraEditors.TextEdit txt确诊机构;
        private DevExpress.XtraEditors.DateEdit txt确诊日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private Library.UserControls.UCTxtLbl txt腰围;
        private Library.UserControls.UCTxtLbl txtBMI;
        private Library.UserControls.UCTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt身高;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit ck无以上病史;
        private DevExpress.XtraEditors.CheckEdit ck无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit jzs_ck高血压;
        private DevExpress.XtraEditors.CheckEdit jzs_ck冠心病;
        private DevExpress.XtraEditors.CheckEdit jzs_ck脑卒中;
        private DevExpress.XtraEditors.CheckEdit jzs_ck糖尿病;
        private DevExpress.XtraEditors.CheckEdit jzs_ck以上都无;
        private DevExpress.XtraEditors.CheckEdit jzs_ck不详;
        private DevExpress.XtraEditors.CheckEdit jzs_ck拒答;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private System.Windows.Forms.FlowLayoutPanel fl非药物治疗;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraEditors.CheckEdit checkEdit42;
        private DevExpress.XtraEditors.CheckEdit checkEdit43;
        private DevExpress.XtraEditors.CheckEdit checkEdit44;
        private DevExpress.XtraEditors.CheckEdit checkEdit45;
        private DevExpress.XtraEditors.CheckEdit ck非药物未采取措施;
        private System.Windows.Forms.FlowLayoutPanel fl特殊治疗;
        private DevExpress.XtraEditors.CheckEdit ch特殊治疗无;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private DevExpress.XtraEditors.DateEdit txt体检日期;
        private Library.UserControls.UCTxtLbl txt胆固醇;
        private Library.UserControls.UCTxtLbl txt低密度蛋白;
        private Library.UserControls.UCTxtLbl txt高密度蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建时间;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.RadioGroup radio终止理由;
        private DevExpress.XtraEditors.DateEdit txt终止管理日期;
        private DevExpress.XtraEditors.RadioGroup radio终止管理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem终止日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem终止理由;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup radio冠心病类型;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private Library.UserControls.UCTxtLbl txt心率;
        private Library.UserControls.UCTxtLbl txt甘油三酯;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.TextEdit txt心肌酶学;
        private DevExpress.XtraEditors.TextEdit txt冠状动脉;
        private DevExpress.XtraEditors.TextEdit txt心脏彩超;
        private DevExpress.XtraEditors.TextEdit txt心电运动;
        private DevExpress.XtraEditors.TextEdit txt心电检查;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.RadioGroup radio生活自理能力;
        private DevExpress.XtraEditors.RadioGroup radio体育锻炼;
        private DevExpress.XtraEditors.RadioGroup radio饮酒情况;
        private DevExpress.XtraEditors.RadioGroup radio吸烟情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.TextEdit txt管理卡编号;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layout用药列表;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.RadioGroup radio管理组别;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
    }
}
