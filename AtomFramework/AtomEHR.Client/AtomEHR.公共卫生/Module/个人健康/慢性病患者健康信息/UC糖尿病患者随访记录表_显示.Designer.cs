﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC糖尿病患者随访记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC糖尿病患者随访记录表_显示));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看随访照片 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.fl死亡信息 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dte死亡日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt死亡原因 = new DevExpress.XtraEditors.TextEdit();
            this.fl访视情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk失访 = new DevExpress.XtraEditors.CheckEdit();
            this.chk死亡 = new DevExpress.XtraEditors.CheckEdit();
            this.fl失访原因 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk外出打工 = new DevExpress.XtraEditors.CheckEdit();
            this.chk迁居他处 = new DevExpress.XtraEditors.CheckEdit();
            this.chk走失 = new DevExpress.XtraEditors.CheckEdit();
            this.chk连续3次 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt调整胰岛素用法用量 = new DevExpress.XtraEditors.TextEdit();
            this.txt调整胰岛素种类 = new DevExpress.XtraEditors.TextEdit();
            this.txt胰岛素用法用量 = new DevExpress.XtraEditors.TextEdit();
            this.txt胰岛素种类 = new DevExpress.XtraEditors.TextEdit();
            this.gc药物调整 = new DevExpress.XtraGrid.GridControl();
            this.gv药物调整 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊结果 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系人 = new DevExpress.XtraEditors.TextEdit();
            this.txt用药调整意见 = new DevExpress.XtraEditors.TextEdit();
            this.txt管理措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.txt转诊情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt用药情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访分类 = new DevExpress.XtraEditors.TextEdit();
            this.txt目前症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访方式 = new DevExpress.XtraEditors.TextEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.txt餐后2h血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt随机血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt检查日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt糖化血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt主食 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体征其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.txt不良反应 = new DevExpress.XtraEditors.TextEdit();
            this.txt足背动脉搏动 = new DevExpress.XtraEditors.TextEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.TextEdit();
            this.txt遵医行为 = new DevExpress.XtraEditors.TextEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.TextEdit();
            this.txt低血糖反应 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt居民签名 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物调整列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem修改时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem修改人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl失访原因 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl死亡信息 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.fl死亡信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).BeginInit();
            this.fl访视情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).BeginInit();
            this.fl失访原因.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素用法用量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素种类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素用法用量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素种类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药调整意见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt低血糖反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访原因)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl死亡信息)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Margin = new System.Windows.Forms.Padding(4);
            this.panelControlNavbar.Size = new System.Drawing.Size(106, 498);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.btn查看随访照片);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(677, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(65, 23);
            this.btn添加.TabIndex = 3;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(74, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(65, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(145, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(65, 23);
            this.btn删除.TabIndex = 1;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(216, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(65, 23);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // btn查看随访照片
            // 
            this.btn查看随访照片.Image = ((System.Drawing.Image)(resources.GetObject("btn查看随访照片.Image")));
            this.btn查看随访照片.Location = new System.Drawing.Point(287, 3);
            this.btn查看随访照片.Name = "btn查看随访照片";
            this.btn查看随访照片.Size = new System.Drawing.Size(112, 23);
            this.btn查看随访照片.TabIndex = 10;
            this.btn查看随访照片.Text = "查看随访照片";
            this.btn查看随访照片.Click += new System.EventHandler(this.btn查看随访照片_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.layoutControl1);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(106, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(677, 498);
            this.panel1.TabIndex = 4;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.fl死亡信息);
            this.layoutControl1.Controls.Add(this.fl访视情况);
            this.layoutControl1.Controls.Add(this.fl失访原因);
            this.layoutControl1.Controls.Add(this.txt调整胰岛素用法用量);
            this.layoutControl1.Controls.Add(this.txt调整胰岛素种类);
            this.layoutControl1.Controls.Add(this.txt胰岛素用法用量);
            this.layoutControl1.Controls.Add(this.txt胰岛素种类);
            this.layoutControl1.Controls.Add(this.gc药物调整);
            this.layoutControl1.Controls.Add(this.txt备注);
            this.layoutControl1.Controls.Add(this.txt转诊结果);
            this.layoutControl1.Controls.Add(this.txt转诊联系电话);
            this.layoutControl1.Controls.Add(this.txt转诊联系人);
            this.layoutControl1.Controls.Add(this.txt用药调整意见);
            this.layoutControl1.Controls.Add(this.txt管理措施);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.txt转诊情况);
            this.layoutControl1.Controls.Add(this.txt用药情况);
            this.layoutControl1.Controls.Add(this.txt随访分类);
            this.layoutControl1.Controls.Add(this.txt目前症状);
            this.layoutControl1.Controls.Add(this.txt随访方式);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.lbl创建机构);
            this.layoutControl1.Controls.Add(this.lbl最近修改人);
            this.layoutControl1.Controls.Add(this.lbl创建人);
            this.layoutControl1.Controls.Add(this.lbl当前所属机构);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.txt转诊科别);
            this.layoutControl1.Controls.Add(this.txt药物副作用详述);
            this.layoutControl1.Controls.Add(this.txt餐后2h血糖);
            this.layoutControl1.Controls.Add(this.txt随机血糖);
            this.layoutControl1.Controls.Add(this.txt检查日期);
            this.layoutControl1.Controls.Add(this.txt糖化血红蛋白);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt主食);
            this.layoutControl1.Controls.Add(this.txt持续时间);
            this.layoutControl1.Controls.Add(this.txt运动频率);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt体征其他);
            this.layoutControl1.Controls.Add(this.txt体质指数);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt医生建议);
            this.layoutControl1.Controls.Add(this.txt不良反应);
            this.layoutControl1.Controls.Add(this.txt足背动脉搏动);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt遵医行为);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Controls.Add(this.txt低血糖反应);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt居民签名);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(689, 275, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(677, 470);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // fl死亡信息
            // 
            this.fl死亡信息.Controls.Add(this.labelControl1);
            this.fl死亡信息.Controls.Add(this.dte死亡日期);
            this.fl死亡信息.Controls.Add(this.labelControl2);
            this.fl死亡信息.Controls.Add(this.txt死亡原因);
            this.fl死亡信息.Location = new System.Drawing.Point(88, -428);
            this.fl死亡信息.Name = "fl死亡信息";
            this.fl死亡信息.Size = new System.Drawing.Size(569, 26);
            this.fl死亡信息.TabIndex = 154;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "死亡日期：";
            // 
            // dte死亡日期
            // 
            this.dte死亡日期.EditValue = null;
            this.dte死亡日期.Location = new System.Drawing.Point(69, 3);
            this.dte死亡日期.Name = "dte死亡日期";
            this.dte死亡日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte死亡日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte死亡日期.Properties.ReadOnly = true;
            this.dte死亡日期.Size = new System.Drawing.Size(100, 20);
            this.dte死亡日期.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(175, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "    死亡原因：";
            // 
            // txt死亡原因
            // 
            this.txt死亡原因.Location = new System.Drawing.Point(257, 3);
            this.txt死亡原因.Name = "txt死亡原因";
            this.txt死亡原因.Properties.ReadOnly = true;
            this.txt死亡原因.Size = new System.Drawing.Size(210, 20);
            this.txt死亡原因.TabIndex = 3;
            // 
            // fl访视情况
            // 
            this.fl访视情况.Controls.Add(this.chk失访);
            this.fl访视情况.Controls.Add(this.chk死亡);
            this.fl访视情况.Location = new System.Drawing.Point(88, -488);
            this.fl访视情况.Name = "fl访视情况";
            this.fl访视情况.Size = new System.Drawing.Size(569, 26);
            this.fl访视情况.TabIndex = 156;
            // 
            // chk失访
            // 
            this.chk失访.Location = new System.Drawing.Point(3, 3);
            this.chk失访.Name = "chk失访";
            this.chk失访.Properties.Caption = "失访";
            this.chk失访.Properties.ReadOnly = true;
            this.chk失访.Size = new System.Drawing.Size(61, 19);
            this.chk失访.TabIndex = 1;
            this.chk失访.Tag = "1";
            this.chk失访.CheckedChanged += new System.EventHandler(this.chk失访_CheckedChanged);
            // 
            // chk死亡
            // 
            this.chk死亡.Location = new System.Drawing.Point(70, 3);
            this.chk死亡.Name = "chk死亡";
            this.chk死亡.Properties.Caption = "死亡";
            this.chk死亡.Properties.ReadOnly = true;
            this.chk死亡.Size = new System.Drawing.Size(61, 19);
            this.chk死亡.TabIndex = 2;
            this.chk死亡.Tag = "2";
            this.chk死亡.CheckedChanged += new System.EventHandler(this.chk死亡_CheckedChanged);
            // 
            // fl失访原因
            // 
            this.fl失访原因.Controls.Add(this.chk外出打工);
            this.fl失访原因.Controls.Add(this.chk迁居他处);
            this.fl失访原因.Controls.Add(this.chk走失);
            this.fl失访原因.Controls.Add(this.chk连续3次);
            this.fl失访原因.Controls.Add(this.chk其他);
            this.fl失访原因.Location = new System.Drawing.Point(88, -458);
            this.fl失访原因.Name = "fl失访原因";
            this.fl失访原因.Size = new System.Drawing.Size(569, 26);
            this.fl失访原因.TabIndex = 153;
            // 
            // chk外出打工
            // 
            this.chk外出打工.Location = new System.Drawing.Point(3, 3);
            this.chk外出打工.Name = "chk外出打工";
            this.chk外出打工.Properties.Caption = "外出打工";
            this.chk外出打工.Properties.ReadOnly = true;
            this.chk外出打工.Size = new System.Drawing.Size(75, 19);
            this.chk外出打工.TabIndex = 0;
            this.chk外出打工.Tag = "1";
            // 
            // chk迁居他处
            // 
            this.chk迁居他处.Location = new System.Drawing.Point(84, 3);
            this.chk迁居他处.Name = "chk迁居他处";
            this.chk迁居他处.Properties.Caption = "迁居他处";
            this.chk迁居他处.Properties.ReadOnly = true;
            this.chk迁居他处.Size = new System.Drawing.Size(75, 19);
            this.chk迁居他处.TabIndex = 0;
            this.chk迁居他处.Tag = "2";
            // 
            // chk走失
            // 
            this.chk走失.Location = new System.Drawing.Point(165, 3);
            this.chk走失.Name = "chk走失";
            this.chk走失.Properties.Caption = "走失";
            this.chk走失.Properties.ReadOnly = true;
            this.chk走失.Size = new System.Drawing.Size(62, 19);
            this.chk走失.TabIndex = 0;
            this.chk走失.Tag = "3";
            // 
            // chk连续3次
            // 
            this.chk连续3次.Location = new System.Drawing.Point(233, 3);
            this.chk连续3次.Name = "chk连续3次";
            this.chk连续3次.Properties.Caption = "连续3次未到访";
            this.chk连续3次.Properties.ReadOnly = true;
            this.chk连续3次.Size = new System.Drawing.Size(114, 19);
            this.chk连续3次.TabIndex = 0;
            this.chk连续3次.Tag = "4";
            // 
            // chk其他
            // 
            this.chk其他.Location = new System.Drawing.Point(353, 3);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Caption = "其他";
            this.chk其他.Properties.ReadOnly = true;
            this.chk其他.Size = new System.Drawing.Size(76, 19);
            this.chk其他.TabIndex = 0;
            this.chk其他.Tag = "5";
            // 
            // txt调整胰岛素用法用量
            // 
            this.txt调整胰岛素用法用量.Location = new System.Drawing.Point(467, 197);
            this.txt调整胰岛素用法用量.Name = "txt调整胰岛素用法用量";
            this.txt调整胰岛素用法用量.Size = new System.Drawing.Size(187, 20);
            this.txt调整胰岛素用法用量.StyleController = this.layoutControl1;
            this.txt调整胰岛素用法用量.TabIndex = 135;
            // 
            // txt调整胰岛素种类
            // 
            this.txt调整胰岛素种类.Location = new System.Drawing.Point(111, 197);
            this.txt调整胰岛素种类.Name = "txt调整胰岛素种类";
            this.txt调整胰岛素种类.Size = new System.Drawing.Size(217, 20);
            this.txt调整胰岛素种类.StyleController = this.layoutControl1;
            this.txt调整胰岛素种类.TabIndex = 134;
            // 
            // txt胰岛素用法用量
            // 
            this.txt胰岛素用法用量.Location = new System.Drawing.Point(467, 50);
            this.txt胰岛素用法用量.Name = "txt胰岛素用法用量";
            this.txt胰岛素用法用量.Size = new System.Drawing.Size(187, 20);
            this.txt胰岛素用法用量.StyleController = this.layoutControl1;
            this.txt胰岛素用法用量.TabIndex = 133;
            // 
            // txt胰岛素种类
            // 
            this.txt胰岛素种类.Location = new System.Drawing.Point(91, 50);
            this.txt胰岛素种类.Name = "txt胰岛素种类";
            this.txt胰岛素种类.Size = new System.Drawing.Size(237, 20);
            this.txt胰岛素种类.StyleController = this.layoutControl1;
            this.txt胰岛素种类.TabIndex = 132;
            // 
            // gc药物调整
            // 
            this.gc药物调整.Location = new System.Drawing.Point(6, 221);
            this.gc药物调整.MainView = this.gv药物调整;
            this.gc药物调整.Name = "gc药物调整";
            this.gc药物调整.Size = new System.Drawing.Size(648, 66);
            this.gc药物调整.TabIndex = 131;
            this.gc药物调整.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv药物调整});
            // 
            // gv药物调整
            // 
            this.gv药物调整.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gv药物调整.GridControl = this.gc药物调整;
            this.gv药物调整.Name = "gv药物调整";
            this.gv药物调整.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "药物名称";
            this.gridColumn5.FieldName = "药物名称";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "用法";
            this.gridColumn6.FieldName = "用法";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "个人档案编号";
            this.gridColumn7.FieldName = "个人档案编号";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "创建时间";
            this.gridColumn8.FieldName = "创建时间";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(342, 363);
            this.txt备注.Name = "txt备注";
            this.txt备注.Size = new System.Drawing.Size(312, 20);
            this.txt备注.StyleController = this.layoutControl1;
            this.txt备注.TabIndex = 130;
            // 
            // txt转诊结果
            // 
            this.txt转诊结果.Location = new System.Drawing.Point(508, 315);
            this.txt转诊结果.Name = "txt转诊结果";
            this.txt转诊结果.Size = new System.Drawing.Size(146, 20);
            this.txt转诊结果.StyleController = this.layoutControl1;
            this.txt转诊结果.TabIndex = 128;
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Location = new System.Drawing.Point(297, 315);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Size = new System.Drawing.Size(122, 20);
            this.txt转诊联系电话.StyleController = this.layoutControl1;
            this.txt转诊联系电话.TabIndex = 127;
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Location = new System.Drawing.Point(91, 315);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Size = new System.Drawing.Size(87, 20);
            this.txt转诊联系人.StyleController = this.layoutControl1;
            this.txt转诊联系人.TabIndex = 126;
            // 
            // txt用药调整意见
            // 
            this.txt用药调整意见.Location = new System.Drawing.Point(111, 173);
            this.txt用药调整意见.Name = "txt用药调整意见";
            this.txt用药调整意见.Size = new System.Drawing.Size(543, 20);
            this.txt用药调整意见.StyleController = this.layoutControl1;
            this.txt用药调整意见.TabIndex = 125;
            // 
            // txt管理措施
            // 
            this.txt管理措施.Location = new System.Drawing.Point(111, 149);
            this.txt管理措施.Name = "txt管理措施";
            this.txt管理措施.Size = new System.Drawing.Size(543, 20);
            this.txt管理措施.StyleController = this.layoutControl1;
            this.txt管理措施.TabIndex = 124;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(88, -546);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Size = new System.Drawing.Size(208, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 122;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(385, -577);
            this.txt职业.Name = "txt职业";
            this.txt职业.Size = new System.Drawing.Size(272, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 121;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(88, -577);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(208, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 120;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(385, -601);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Size = new System.Drawing.Size(272, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 119;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(88, -601);
            this.txt性别.Name = "txt性别";
            this.txt性别.Size = new System.Drawing.Size(208, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 118;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(385, -625);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(272, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 117;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(88, -625);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Size = new System.Drawing.Size(208, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 116;
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, -643);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(654, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 23;
            this.lab考核项.Text = "考核项：26     缺项：25 完整度：0% ";
            // 
            // txt转诊情况
            // 
            this.txt转诊情况.Location = new System.Drawing.Point(91, 291);
            this.txt转诊情况.Name = "txt转诊情况";
            this.txt转诊情况.Size = new System.Drawing.Size(116, 20);
            this.txt转诊情况.StyleController = this.layoutControl1;
            this.txt转诊情况.TabIndex = 115;
            // 
            // txt用药情况
            // 
            this.txt用药情况.Location = new System.Drawing.Point(91, 26);
            this.txt用药情况.Name = "txt用药情况";
            this.txt用药情况.Size = new System.Drawing.Size(563, 20);
            this.txt用药情况.StyleController = this.layoutControl1;
            this.txt用药情况.TabIndex = 114;
            // 
            // txt随访分类
            // 
            this.txt随访分类.Location = new System.Drawing.Point(91, -33);
            this.txt随访分类.Name = "txt随访分类";
            this.txt随访分类.Size = new System.Drawing.Size(563, 20);
            this.txt随访分类.StyleController = this.layoutControl1;
            this.txt随访分类.TabIndex = 113;
            // 
            // txt目前症状
            // 
            this.txt目前症状.Location = new System.Drawing.Point(88, -398);
            this.txt目前症状.Name = "txt目前症状";
            this.txt目前症状.Size = new System.Drawing.Size(569, 20);
            this.txt目前症状.StyleController = this.layoutControl1;
            this.txt目前症状.TabIndex = 112;
            // 
            // txt随访方式
            // 
            this.txt随访方式.Location = new System.Drawing.Point(385, -512);
            this.txt随访方式.Name = "txt随访方式";
            this.txt随访方式.Size = new System.Drawing.Size(272, 20);
            this.txt随访方式.StyleController = this.layoutControl1;
            this.txt随访方式.TabIndex = 111;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(6, 74);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(648, 71);
            this.gcDetail.TabIndex = 93;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(296, 448);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(135, 18);
            this.lbl创建机构.StyleController = this.layoutControl1;
            this.lbl创建机构.TabIndex = 51;
            this.lbl创建机构.Text = "labelControl16";
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近修改人.Location = new System.Drawing.Point(521, 448);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(136, 18);
            this.lbl最近修改人.StyleController = this.layoutControl1;
            this.lbl最近修改人.TabIndex = 50;
            this.lbl最近修改人.Text = "labelControl15";
            // 
            // lbl创建人
            // 
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(93, 448);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(113, 18);
            this.lbl创建人.StyleController = this.layoutControl1;
            this.lbl创建人.TabIndex = 49;
            this.lbl创建人.Text = "labelControl14";
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Location = new System.Drawing.Point(296, 426);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(135, 18);
            this.lbl当前所属机构.StyleController = this.layoutControl1;
            this.lbl当前所属机构.TabIndex = 48;
            this.lbl当前所属机构.Text = "labelControl13";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(521, 426);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(136, 18);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 47;
            this.lbl最近更新时间.Text = "labelControl12";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(93, 426);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(113, 18);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "labelControl11";
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(342, 339);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(312, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 45;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(91, 339);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt下次随访时间.Size = new System.Drawing.Size(162, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 44;
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Location = new System.Drawing.Point(252, 291);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(168, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 43;
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Location = new System.Drawing.Point(509, 291);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(145, 20);
            this.txt转诊科别.StyleController = this.layoutControl1;
            this.txt转诊科别.TabIndex = 42;
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Location = new System.Drawing.Point(329, -81);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(325, 20);
            this.txt药物副作用详述.StyleController = this.layoutControl1;
            this.txt药物副作用详述.TabIndex = 35;
            // 
            // txt餐后2h血糖
            // 
            this.txt餐后2h血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt餐后2h血糖.Lbl1Text = "mmol/L ";
            this.txt餐后2h血糖.Location = new System.Drawing.Point(329, -129);
            this.txt餐后2h血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt餐后2h血糖.Name = "txt餐后2h血糖";
            this.txt餐后2h血糖.Size = new System.Drawing.Size(325, 20);
            this.txt餐后2h血糖.TabIndex = 32;
            this.txt餐后2h血糖.Txt1Size = new System.Drawing.Size(90, 20);
            // 
            // txt随机血糖
            // 
            this.txt随机血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt随机血糖.Lbl1Text = "mmol/L ";
            this.txt随机血糖.Location = new System.Drawing.Point(91, -129);
            this.txt随机血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt随机血糖.Name = "txt随机血糖";
            this.txt随机血糖.Size = new System.Drawing.Size(149, 20);
            this.txt随机血糖.TabIndex = 31;
            this.txt随机血糖.Txt1Size = new System.Drawing.Size(90, 20);
            // 
            // txt检查日期
            // 
            this.txt检查日期.EditValue = null;
            this.txt检查日期.Location = new System.Drawing.Point(537, -153);
            this.txt检查日期.Name = "txt检查日期";
            this.txt检查日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt检查日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt检查日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt检查日期.Size = new System.Drawing.Size(117, 20);
            this.txt检查日期.StyleController = this.layoutControl1;
            this.txt检查日期.TabIndex = 30;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.Lbl1Size = new System.Drawing.Size(22, 18);
            this.txt糖化血红蛋白.Lbl1Text = "(%)";
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(329, -153);
            this.txt糖化血红蛋白.Margin = new System.Windows.Forms.Padding(4);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(134, 20);
            this.txt糖化血红蛋白.TabIndex = 29;
            this.txt糖化血红蛋白.Txt1Size = new System.Drawing.Size(90, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(91, -153);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(149, 20);
            this.txt空腹血糖.TabIndex = 28;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(90, 20);
            // 
            // txt主食
            // 
            this.txt主食.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt主食.Lbl1Text = "/";
            this.txt主食.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt主食.Lbl2Text = "(克/天)";
            this.txt主食.Location = new System.Drawing.Point(91, -229);
            this.txt主食.Margin = new System.Windows.Forms.Padding(4);
            this.txt主食.Name = "txt主食";
            this.txt主食.Size = new System.Drawing.Size(209, 21);
            this.txt主食.TabIndex = 25;
            this.txt主食.Txt1EditValue = null;
            this.txt主食.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt主食.Txt2EditValue = null;
            this.txt主食.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt持续时间
            // 
            this.txt持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt持续时间.Lbl1Text = "/";
            this.txt持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt持续时间.Lbl2Text = "(分钟/次)";
            this.txt持续时间.Location = new System.Drawing.Point(389, -253);
            this.txt持续时间.Margin = new System.Windows.Forms.Padding(4);
            this.txt持续时间.Name = "txt持续时间";
            this.txt持续时间.Size = new System.Drawing.Size(265, 20);
            this.txt持续时间.TabIndex = 24;
            this.txt持续时间.Txt1EditValue = null;
            this.txt持续时间.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt持续时间.Txt2EditValue = null;
            this.txt持续时间.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(91, -253);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(4);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(209, 20);
            this.txt运动频率.TabIndex = 23;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(389, -277);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(4);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(265, 20);
            this.txt饮酒情况.TabIndex = 22;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(91, -277);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(4);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(209, 20);
            this.txt日吸烟量.TabIndex = 21;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt体征其他
            // 
            this.txt体征其他.Location = new System.Drawing.Point(543, -327);
            this.txt体征其他.Name = "txt体征其他";
            this.txt体征其他.Size = new System.Drawing.Size(111, 20);
            this.txt体征其他.StyleController = this.layoutControl1;
            this.txt体征其他.TabIndex = 20;
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(71, -327);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(4);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(166, 20);
            this.txt体质指数.TabIndex = 18;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(543, -351);
            this.txt身高.Margin = new System.Windows.Forms.Padding(4);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(111, 20);
            this.txt身高.TabIndex = 17;
            this.txt身高.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(18, 14);
            this.txt体重.Lbl2Text = "kg";
            this.txt体重.Location = new System.Drawing.Point(326, -351);
            this.txt体重.Margin = new System.Windows.Forms.Padding(4);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(158, 20);
            this.txt体重.TabIndex = 16;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(71, -351);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(4);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(166, 20);
            this.txt血压值.TabIndex = 15;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(88, -512);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(208, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 12;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(91, -9);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(563, 31);
            this.txt医生建议.StyleController = this.layoutControl1;
            this.txt医生建议.TabIndex = 38;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // txt不良反应
            // 
            this.txt不良反应.Location = new System.Drawing.Point(91, -81);
            this.txt不良反应.Name = "txt不良反应";
            this.txt不良反应.Properties.AutoHeight = false;
            this.txt不良反应.Size = new System.Drawing.Size(149, 20);
            this.txt不良反应.StyleController = this.layoutControl1;
            this.txt不良反应.TabIndex = 34;
            // 
            // txt足背动脉搏动
            // 
            this.txt足背动脉搏动.Location = new System.Drawing.Point(326, -327);
            this.txt足背动脉搏动.Name = "txt足背动脉搏动";
            this.txt足背动脉搏动.Size = new System.Drawing.Size(158, 20);
            this.txt足背动脉搏动.StyleController = this.layoutControl1;
            this.txt足背动脉搏动.TabIndex = 19;
            // 
            // txt心理调整
            // 
            this.txt心理调整.Location = new System.Drawing.Point(389, -229);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Size = new System.Drawing.Size(265, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 26;
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.Location = new System.Drawing.Point(91, -204);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Size = new System.Drawing.Size(209, 20);
            this.txt遵医行为.StyleController = this.layoutControl1;
            this.txt遵医行为.TabIndex = 27;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(91, -105);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Size = new System.Drawing.Size(563, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 33;
            // 
            // txt低血糖反应
            // 
            this.txt低血糖反应.Location = new System.Drawing.Point(91, -57);
            this.txt低血糖反应.Name = "txt低血糖反应";
            this.txt低血糖反应.Size = new System.Drawing.Size(563, 20);
            this.txt低血糖反应.StyleController = this.layoutControl1;
            this.txt低血糖反应.TabIndex = 36;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(385, -553);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Size = new System.Drawing.Size(272, 37);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 123;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(91, 363);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Properties.ReadOnly = true;
            this.txt居民签名.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.txt居民签名.Size = new System.Drawing.Size(162, 56);
            this.txt居民签名.StyleController = this.layoutControl1;
            this.txt居民签名.TabIndex = 129;
            this.txt居民签名.TabStop = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlItem43,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem37,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.lcl失访原因,
            this.layoutControlItem60,
            this.lcl死亡信息,
            this.layoutControlItem修改时间,
            this.layoutControlItem修改人});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -673);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(660, 1143);
            this.layoutControlGroup1.Text = "糖尿病患者随访记录表";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt发生时间;
            this.layoutControlItem9.CustomizationFormText = "随访日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(169, 22);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "随访日期";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "体征";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem17});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 269);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(658, 74);
            this.layoutControlGroup2.Text = "体征";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt血压值;
            this.layoutControlItem12.CustomizationFormText = "血压";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(230, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "血压";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt体质指数;
            this.layoutControlItem15.CustomizationFormText = "体质指数";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "体质指数";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt足背动脉搏动;
            this.layoutControlItem16.CustomizationFormText = "足背动脉搏动";
            this.layoutControlItem16.Location = new System.Drawing.Point(235, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "足背动脉搏动";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt体重;
            this.layoutControlItem13.CustomizationFormText = "体重";
            this.layoutControlItem13.Location = new System.Drawing.Point(235, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "体重";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt身高;
            this.layoutControlItem14.CustomizationFormText = "身高";
            this.layoutControlItem14.Location = new System.Drawing.Point(482, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem14.Tag = "";
            this.layoutControlItem14.Text = "身高";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt体征其他;
            this.layoutControlItem17.CustomizationFormText = "其他";
            this.layoutControlItem17.Location = new System.Drawing.Point(482, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem17.Tag = "check";
            this.layoutControlItem17.Text = "其他";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "生活方式指导";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 343);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(658, 124);
            this.layoutControlGroup3.Text = "生活方式指导";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt饮酒情况;
            this.layoutControlItem19.CustomizationFormText = "日饮酒量";
            this.layoutControlItem19.Location = new System.Drawing.Point(298, 0);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(309, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "日饮酒量";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt日吸烟量;
            this.layoutControlItem18.CustomizationFormText = "日吸烟量";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "日吸烟量";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt持续时间;
            this.layoutControlItem21.CustomizationFormText = "每次持续时间";
            this.layoutControlItem21.Location = new System.Drawing.Point(298, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(309, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "每次持续时间";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt运动频率;
            this.layoutControlItem20.CustomizationFormText = "运动频率";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "运动频率";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt主食;
            this.layoutControlItem22.CustomizationFormText = "主食";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(298, 25);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Tag = "check";
            this.layoutControlItem22.Text = "主食";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt心理调整;
            this.layoutControlItem23.CustomizationFormText = "心理调整";
            this.layoutControlItem23.Location = new System.Drawing.Point(298, 48);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(354, 25);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "心理调整";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt遵医行为;
            this.layoutControlItem24.CustomizationFormText = "遵医行为";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(298, 25);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "遵医行为";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(298, 73);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(354, 25);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "辅助检查";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem29,
            this.layoutControlItem28,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem39,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layout药物列表,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem34,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem40,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layout药物调整列表,
            this.layoutControlItem8,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 467);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(658, 602);
            this.layoutControlGroup4.Text = "辅助检查";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.txt空腹血糖;
            this.layoutControlItem25.CustomizationFormText = "空腹血糖";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "空腹血糖";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem26.Control = this.txt糖化血红蛋白;
            this.layoutControlItem26.CustomizationFormText = "糖化血红蛋白";
            this.layoutControlItem26.Location = new System.Drawing.Point(238, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem26.Tag = "";
            this.layoutControlItem26.Text = "糖化血红蛋白";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem27.Control = this.txt检查日期;
            this.layoutControlItem27.CustomizationFormText = "检查日期";
            this.layoutControlItem27.Location = new System.Drawing.Point(461, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(191, 24);
            this.layoutControlItem27.Tag = "";
            this.layoutControlItem27.Text = "检查日期";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt餐后2h血糖;
            this.layoutControlItem29.CustomizationFormText = "餐后2小时血糖";
            this.layoutControlItem29.Location = new System.Drawing.Point(238, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(414, 24);
            this.layoutControlItem29.Tag = "";
            this.layoutControlItem29.Text = "餐后2小时血糖";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.txt随机血糖;
            this.layoutControlItem28.CustomizationFormText = "随机血糖";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem28.Text = "随机血糖";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            this.layoutControlItem28.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem30.Control = this.txt服药依从性;
            this.layoutControlItem30.CustomizationFormText = "服药依从性";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "用药依从性";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.txt不良反应;
            this.layoutControlItem31.CustomizationFormText = "药物不良反应";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "药物不良反应";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem32.Control = this.txt药物副作用详述;
            this.layoutControlItem32.CustomizationFormText = "副作用详述";
            this.layoutControlItem32.Location = new System.Drawing.Point(238, 72);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(414, 24);
            this.layoutControlItem32.Text = "副作用详述";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem33.Control = this.txt低血糖反应;
            this.layoutControlItem33.CustomizationFormText = "低血糖反应";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "低血糖反应";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem35.Control = this.txt医生建议;
            this.layoutControlItem35.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(109, 35);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(652, 35);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Tag = "";
            this.layoutControlItem35.Text = "此次随访医生建议";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem35.TextToControlDistance = 5;
            this.layoutControlItem35.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem39.Control = this.txt转诊科别;
            this.layoutControlItem39.CustomizationFormText = "机构及科别";
            this.layoutControlItem39.Location = new System.Drawing.Point(418, 444);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem39.Text = "机构及科别：";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem41.Control = this.txt下次随访时间;
            this.layoutControlItem41.CustomizationFormText = "下次随访时间";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 492);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem41.Tag = "check";
            this.layoutControlItem41.Text = "下次随访时间";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt医生签名;
            this.layoutControlItem42.CustomizationFormText = "随访医生签名";
            this.layoutControlItem42.Location = new System.Drawing.Point(251, 492);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(401, 24);
            this.layoutControlItem42.Tag = "check";
            this.layoutControlItem42.Text = "随访医生签名";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = " ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 227);
            this.layout药物列表.MinSize = new System.Drawing.Size(111, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(652, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = " ";
            this.layout药物列表.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.Control = this.txt随访分类;
            this.layoutControlItem10.CustomizationFormText = "此次随访分类";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "此次随访分类";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt用药情况;
            this.layoutControlItem11.CustomizationFormText = "用药情况";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 179);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "用药情况";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt转诊情况;
            this.layoutControlItem34.CustomizationFormText = "转诊情况";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 444);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(205, 24);
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "转诊情况";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt管理措施;
            this.layoutControlItem1.CustomizationFormText = "下一步管理措施";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 302);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "下一步管理措施";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt用药调整意见;
            this.layoutControlItem2.CustomizationFormText = "调整用药意见";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 326);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(652, 24);
            this.layoutControlItem2.Tag = "";
            this.layoutControlItem2.Text = "调整用药意见";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt转诊联系电话;
            this.layoutControlItem4.CustomizationFormText = "转诊联系电话：";
            this.layoutControlItem4.Location = new System.Drawing.Point(176, 468);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem4.Text = "转诊联系电话：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt转诊联系人;
            this.layoutControlItem3.CustomizationFormText = "转诊联系人：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 468);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(176, 24);
            this.layoutControlItem3.Text = "转诊联系人：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt转诊结果;
            this.layoutControlItem5.CustomizationFormText = "转诊结果：";
            this.layoutControlItem5.Location = new System.Drawing.Point(417, 468);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem5.Text = "转诊结果：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.txt转诊原因;
            this.layoutControlItem40.CustomizationFormText = "原因";
            this.layoutControlItem40.Location = new System.Drawing.Point(205, 444);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem40.Text = "原因：";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt居民签名;
            this.layoutControlItem6.CustomizationFormText = "居民签名";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 516);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(109, 60);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(251, 60);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Tag = "";
            this.layoutControlItem6.Text = "居民签名";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt备注;
            this.layoutControlItem7.CustomizationFormText = "备注";
            this.layoutControlItem7.Location = new System.Drawing.Point(251, 516);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(401, 60);
            this.layoutControlItem7.Tag = "";
            this.layoutControlItem7.Text = "备注";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layout药物调整列表
            // 
            this.layout药物调整列表.Control = this.gc药物调整;
            this.layout药物调整列表.CustomizationFormText = "layout药物调整列表";
            this.layout药物调整列表.Location = new System.Drawing.Point(0, 374);
            this.layout药物调整列表.MinSize = new System.Drawing.Size(212, 70);
            this.layout药物调整列表.Name = "layout药物调整列表";
            this.layout药物调整列表.Size = new System.Drawing.Size(652, 70);
            this.layout药物调整列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物调整列表.Text = "layout药物调整列表";
            this.layout药物调整列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物调整列表.TextToControlDistance = 0;
            this.layout药物调整列表.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt胰岛素种类;
            this.layoutControlItem8.CustomizationFormText = "胰岛素种类：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 203);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(326, 24);
            this.layoutControlItem8.Text = "胰岛素种类：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.txt胰岛素用法用量;
            this.layoutControlItem57.CustomizationFormText = "胰岛素用法和用量：";
            this.layoutControlItem57.Location = new System.Drawing.Point(326, 203);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(326, 24);
            this.layoutControlItem57.Text = "胰岛素用法和用量：";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem58.Control = this.txt调整胰岛素种类;
            this.layoutControlItem58.CustomizationFormText = "调整胰岛素种类：";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 350);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(326, 24);
            this.layoutControlItem58.Text = "调整胰岛素种类：";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.txt调整胰岛素用法用量;
            this.layoutControlItem59.CustomizationFormText = "调整胰岛素用法和用量：";
            this.layoutControlItem59.Location = new System.Drawing.Point(326, 350);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(326, 24);
            this.layoutControlItem59.Text = "调整胰岛素用法和用量：";
            this.layoutControlItem59.TextSize = new System.Drawing.Size(132, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 1113);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(658, 1);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lbl创建时间;
            this.layoutControlItem43.CustomizationFormText = "layoutControlItem43";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 1069);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(207, 22);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem43.TextToControlDistance = 0;
            // 
            // layoutControlItem修改时间
            // 
            this.layoutControlItem修改时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem修改时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem修改时间.Control = this.lbl最近更新时间;
            this.layoutControlItem修改时间.CustomizationFormText = "layoutControlItem44";
            this.layoutControlItem修改时间.Location = new System.Drawing.Point(432, 1069);
            this.layoutControlItem修改时间.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem修改时间.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem修改时间.Name = "layoutControlItem修改时间";
            this.layoutControlItem修改时间.Size = new System.Drawing.Size(226, 22);
            this.layoutControlItem修改时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem修改时间.Text = "最近更新时间";
            this.layoutControlItem修改时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem修改时间.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem修改时间.TextToControlDistance = 0;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lbl当前所属机构;
            this.layoutControlItem45.CustomizationFormText = "layoutControlItem45";
            this.layoutControlItem45.Location = new System.Drawing.Point(207, 1069);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(225, 22);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "当前所属机构";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem45.TextToControlDistance = 0;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lbl创建人;
            this.layoutControlItem46.CustomizationFormText = "layoutControlItem46";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 1091);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(207, 22);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "录入人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem46.TextToControlDistance = 0;
            // 
            // layoutControlItem修改人
            // 
            this.layoutControlItem修改人.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem修改人.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem修改人.Control = this.lbl最近修改人;
            this.layoutControlItem修改人.CustomizationFormText = "layoutControlItem47";
            this.layoutControlItem修改人.Location = new System.Drawing.Point(432, 1091);
            this.layoutControlItem修改人.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem修改人.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem修改人.Name = "layoutControlItem修改人";
            this.layoutControlItem修改人.Size = new System.Drawing.Size(226, 22);
            this.layoutControlItem修改人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem修改人.Text = "最近更新人";
            this.layoutControlItem修改人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem修改人.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem修改人.TextToControlDistance = 0;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lbl创建机构;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem48.Location = new System.Drawing.Point(207, 1091);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(225, 22);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "创建机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem48.TextToControlDistance = 0;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.txt目前症状;
            this.layoutControlItem49.CustomizationFormText = "症状";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 245);
            this.layoutControlItem49.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(658, 24);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Tag = "check";
            this.layoutControlItem49.Text = "症状";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.txt随访方式;
            this.layoutControlItem37.CustomizationFormText = "随访方式";
            this.layoutControlItem37.Location = new System.Drawing.Point(297, 131);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(361, 24);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Tag = "check";
            this.layoutControlItem37.Text = "随访方式";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.lab考核项;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(658, 18);
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt个人档案号;
            this.layoutControlItem38.CustomizationFormText = "个人档案号";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem38.Text = "个人档案号";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt姓名;
            this.layoutControlItem50.CustomizationFormText = "姓名";
            this.layoutControlItem50.Location = new System.Drawing.Point(297, 18);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(361, 24);
            this.layoutControlItem50.Text = "姓名";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt性别;
            this.layoutControlItem51.CustomizationFormText = "性别";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem51.Text = "性别";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt出生日期;
            this.layoutControlItem52.CustomizationFormText = "出生日期";
            this.layoutControlItem52.Location = new System.Drawing.Point(297, 42);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(361, 24);
            this.layoutControlItem52.Text = "出生日期";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.txt身份证号;
            this.layoutControlItem53.CustomizationFormText = "身份证号";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem53.Text = "身份证号";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.txt职业;
            this.layoutControlItem54.CustomizationFormText = "职业";
            this.layoutControlItem54.Location = new System.Drawing.Point(297, 66);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(361, 24);
            this.layoutControlItem54.Text = "职业";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.txt联系电话;
            this.layoutControlItem55.CustomizationFormText = "联系电话";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 9, 2);
            this.layoutControlItem55.Size = new System.Drawing.Size(297, 41);
            this.layoutControlItem55.Text = "联系电话";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.txt居住地址;
            this.layoutControlItem56.CustomizationFormText = "居住地址";
            this.layoutControlItem56.Location = new System.Drawing.Point(297, 90);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(50, 41);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(361, 41);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "居住地址";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // lcl失访原因
            // 
            this.lcl失访原因.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl失访原因.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl失访原因.Control = this.fl失访原因;
            this.lcl失访原因.CustomizationFormText = "失访原因：";
            this.lcl失访原因.Location = new System.Drawing.Point(0, 185);
            this.lcl失访原因.MaxSize = new System.Drawing.Size(0, 30);
            this.lcl失访原因.MinSize = new System.Drawing.Size(239, 30);
            this.lcl失访原因.Name = "lcl失访原因";
            this.lcl失访原因.Size = new System.Drawing.Size(658, 30);
            this.lcl失访原因.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl失访原因.Text = "失访原因：";
            this.lcl失访原因.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl失访原因.TextSize = new System.Drawing.Size(80, 20);
            this.lcl失访原因.TextToControlDistance = 5;
            this.lcl失访原因.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.fl访视情况;
            this.layoutControlItem60.CustomizationFormText = "访视情况：";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem60.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(239, 30);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(658, 30);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "访视情况：";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // lcl死亡信息
            // 
            this.lcl死亡信息.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl死亡信息.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl死亡信息.Control = this.fl死亡信息;
            this.lcl死亡信息.CustomizationFormText = "死亡信息：";
            this.lcl死亡信息.Location = new System.Drawing.Point(0, 215);
            this.lcl死亡信息.MaxSize = new System.Drawing.Size(0, 30);
            this.lcl死亡信息.MinSize = new System.Drawing.Size(239, 30);
            this.lcl死亡信息.Name = "lcl死亡信息";
            this.lcl死亡信息.Size = new System.Drawing.Size(658, 30);
            this.lcl死亡信息.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl死亡信息.Text = "死亡信息：";
            this.lcl死亡信息.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl死亡信息.TextSize = new System.Drawing.Size(80, 20);
            this.lcl死亡信息.TextToControlDistance = 5;
            this.lcl死亡信息.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // UC糖尿病患者随访记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "UC糖尿病患者随访记录表_显示";
            this.Size = new System.Drawing.Size(783, 498);
            this.Load += new System.EventHandler(this.UC糖尿病患者随访记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.fl死亡信息.ResumeLayout(false);
            this.fl死亡信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).EndInit();
            this.fl访视情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).EndInit();
            this.fl失访原因.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素用法用量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素种类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素用法用量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素种类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药调整意见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt管理措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt目前症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt低血糖反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访原因)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl死亡信息)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近修改人;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private Library.UserControls.UCTxtLbl txt餐后2h血糖;
        private Library.UserControls.UCTxtLbl txt随机血糖;
        private DevExpress.XtraEditors.DateEdit txt检查日期;
        private Library.UserControls.UCTxtLbl txt糖化血红蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLblTxtLbl txt主食;
        private Library.UserControls.UCTxtLblTxtLbl txt持续时间;
        private Library.UserControls.UCTxtLblTxtLbl txt运动频率;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private DevExpress.XtraEditors.TextEdit txt体征其他;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem修改时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem修改人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraEditors.TextEdit txt转诊情况;
        private DevExpress.XtraEditors.TextEdit txt用药情况;
        private DevExpress.XtraEditors.TextEdit txt随访分类;
        private DevExpress.XtraEditors.TextEdit txt目前症状;
        private DevExpress.XtraEditors.TextEdit txt随访方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit txt不良反应;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraEditors.TextEdit txt足背动脉搏动;
        private DevExpress.XtraEditors.TextEdit txt心理调整;
        private DevExpress.XtraEditors.TextEdit txt遵医行为;
        private DevExpress.XtraEditors.TextEdit txt服药依从性;
        private DevExpress.XtraEditors.TextEdit txt低血糖反应;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private DevExpress.XtraGrid.GridControl gc药物调整;
        private DevExpress.XtraGrid.Views.Grid.GridView gv药物调整;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private DevExpress.XtraEditors.TextEdit txt转诊结果;
        private DevExpress.XtraEditors.TextEdit txt转诊联系电话;
        private DevExpress.XtraEditors.TextEdit txt转诊联系人;
        private DevExpress.XtraEditors.TextEdit txt用药调整意见;
        private DevExpress.XtraEditors.TextEdit txt管理措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layout药物调整列表;
        private DevExpress.XtraEditors.TextEdit txt调整胰岛素用法用量;
        private DevExpress.XtraEditors.TextEdit txt调整胰岛素种类;
        private DevExpress.XtraEditors.TextEdit txt胰岛素用法用量;
        private DevExpress.XtraEditors.TextEdit txt胰岛素种类;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraEditors.PictureEdit txt居民签名;
        private DevExpress.XtraEditors.SimpleButton btn查看随访照片;
        private System.Windows.Forms.FlowLayoutPanel fl访视情况;
        private DevExpress.XtraEditors.CheckEdit chk失访;
        private DevExpress.XtraEditors.CheckEdit chk死亡;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private System.Windows.Forms.FlowLayoutPanel fl失访原因;
        private DevExpress.XtraEditors.CheckEdit chk外出打工;
        private DevExpress.XtraEditors.CheckEdit chk迁居他处;
        private DevExpress.XtraEditors.CheckEdit chk走失;
        private DevExpress.XtraEditors.CheckEdit chk连续3次;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private DevExpress.XtraLayout.LayoutControlItem lcl失访原因;
        private System.Windows.Forms.FlowLayoutPanel fl死亡信息;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dte死亡日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt死亡原因;
        private DevExpress.XtraLayout.LayoutControlItem lcl死亡信息;
    }
}
