﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using System.Collections.Generic;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class report高血压患者随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        string docNo;
        List<string> dates;
        DataSet _ds高血压;
        DataSet _ds用药情况;
        bllMXB高血压随访表 _bll高血压 = new bllMXB高血压随访表();
        #endregion
        public report高血压患者随访服务记录表()
        {
            InitializeComponent();
        }

        public report高血压患者随访服务记录表(string docNo, List<string> _dates)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.dates = _dates;
            _ds高血压 = _bll高血压.GetInfoByGXY(docNo, dates, true);
            DoBindingDataSource(_ds高血压);
        }

        private void DoBindingDataSource(DataSet _ds高血压)
        {
            DataTable dt高血压 = _ds高血压.Tables[Models.tb_MXB高血压随访表.__TableName];

            for (int i = 0; i < dt高血压.Rows.Count; i++)
            {
                string year = dt高血压.Rows[i][Models.tb_MXB高血压随访表.创建时间].ToString();
                _ds用药情况 = new bllMXB高血压随访表().GetInfoByYY(docNo, year, false);
                switch (i)
                {
                    case 0:
                        BindYYQK1();
                        break;
                    case 1:
                        BindYYQK2();
                        break;
                    case 2:
                        BindYYQK3();
                        break;
                    case 3:
                        BindYYQK4();
                        break;
                    default:
                        break;
                }

            }

            DataTable dt健康档案 = _ds高血压.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][Models.tb_健康档案.姓名].ToString());

            #region 1次随访记录

            if (dt高血压 != null && dt高血压.Rows.Count > 0)
            {
                this.txt第1次随访日期.Text = Convert.ToDateTime(dt高血压.Rows[0][Models.tb_MXB高血压随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第1次随访方式.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.随访方式].ToString();
                //第1次随访症状
                BindZZ1(dt高血压);
                this.txt第1次随访症状其他.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.目前症状其他].ToString();
                this.txt第1次随访血压1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.收缩压].ToString();//高压
                this.txt第1次随访血压2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.舒张压].ToString();//低压
                this.txt第1次随访体重1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.体重].ToString();
                this.txt第1次随访体重2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.体重2].ToString();
                this.txt第1次随访体质指数1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.BMI].ToString();
                this.txt第1次随访体质指数2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.BMI2].ToString();
                this.txt第1次随访心率.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.心率].ToString();
                this.txt第1次随访体征其他.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.体征其他].ToString();
                this.txt第1次随访日吸烟量1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.吸烟数量].ToString();
                this.txt第1次随访日饮酒量1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.饮酒数量].ToString();
                this.txt第1次随访日吸烟量2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.吸烟数量2].ToString();
                this.txt第1次随访日饮酒量2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.饮酒数量2].ToString();
                this.txt第1次随访运动次数1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.运动频率].ToString();
                this.txt第1次随访运动时间1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.运动持续时间].ToString();
                this.txt第1次随访运动次数2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.运动频率2].ToString();
                this.txt第1次随访运动时间2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.运动持续时间2].ToString();
                //第1次随访摄盐情况
                BindSYQK1(dt高血压);
                this.txt第1次随访心理调整.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt第1次随访遵医行为.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt第1次随访辅助检查.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt第1次随访服药依从性.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt第1次随访药物不良反应.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt第1次随访药物不良反应其他.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt第1次随访分类.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.本次随访分类].ToString();
                this.txt第1次随访转诊原因.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt第1次随访转诊机构.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt第1次随访下次随访日期.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt第1次随访医生签名.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.随访医生].ToString();
            }

            #endregion

            #region 2次随访记录

            if (dt高血压 != null && dt高血压.Rows.Count > 1)
            {
                this.txt第2次随访日期.Text = Convert.ToDateTime(dt高血压.Rows[1][Models.tb_MXB高血压随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第2次随访方式.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.随访方式].ToString();
                //第2次随访症状
                BindZZ2(dt高血压);
                this.txt第2次随访症状其他.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.目前症状其他].ToString();
                this.txt第2次随访血压1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.收缩压].ToString();
                this.txt第2次随访血压2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.舒张压].ToString();
                this.txt第2次随访体重1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.体重].ToString();
                this.txt第2次随访体重2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.体重2].ToString();
                this.txt第2次随访体质指数1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.BMI].ToString();
                this.txt第2次随访体质指数2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.BMI2].ToString();
                this.txt第2次随访心率.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.心率].ToString();
                this.txt第2次随访体征其他.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.体征其他].ToString();
                this.txt第2次随访日吸烟量1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.吸烟数量].ToString();
                this.txt第2次随访日饮酒量1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.饮酒数量].ToString();
                this.txt第2次随访日吸烟量2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.吸烟数量2].ToString();
                this.txt第2次随访日饮酒量2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.饮酒数量2].ToString();
                this.txt第2次随访运动次数1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.运动频率].ToString();
                this.txt第2次随访运动时间1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.运动持续时间].ToString();
                this.txt第2次随访运动次数2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.运动频率2].ToString();
                this.txt第2次随访运动时间2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.运动持续时间2].ToString();
                //第2次随访摄盐情况
                BindSYQK2(dt高血压);
                this.txt第2次随访心理调整.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt第2次随访遵医行为.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt第2次随访辅助检查.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt第2次随访服药依从性.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt第2次随访药物不良反应.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt第2次随访药物不良反应其他.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt第2次随访分类.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.本次随访分类].ToString();
                this.txt第2次随访转诊原因.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt第2次随访转诊机构.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt第2次随访下次随访日期.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt第2次随访医生签名.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.随访医生].ToString();
            }
            #endregion

            #region 3次随访记录

            if (dt高血压 != null && dt高血压.Rows.Count > 2)
            {
                this.txt第3次随访日期.Text = Convert.ToDateTime(dt高血压.Rows[2][Models.tb_MXB高血压随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第3次随访方式.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.随访方式].ToString();
                //第3次随访症状
                BindZZ3(dt高血压);
                this.txt第3次随访症状其他.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.目前症状其他].ToString();
                this.txt第3次随访血压1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.收缩压].ToString();
                this.txt第3次随访血压2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.舒张压].ToString();
                this.txt第3次随访体重1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.体重].ToString();
                this.txt第3次随访体重2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.体重2].ToString();
                this.txt第3次随访体质指数1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.BMI].ToString();
                this.txt第3次随访体质指数2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.BMI2].ToString();
                this.txt第3次随访心率.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.心率].ToString();
                this.txt第3次随访体征其他.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.体征其他].ToString();
                this.txt第3次随访日吸烟量1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.吸烟数量].ToString();
                this.txt第3次随访日饮酒量1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.饮酒数量].ToString();
                this.txt第3次随访日吸烟量2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.吸烟数量2].ToString();
                this.txt第3次随访日饮酒量2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.饮酒数量2].ToString();
                this.txt第3次随访运动次数1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.运动频率].ToString();
                this.txt第3次随访运动时间1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.运动持续时间].ToString();
                this.txt第3次随访运动次数2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.运动频率2].ToString();
                this.txt第3次随访运动时间2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.运动持续时间2].ToString();
                //第3次随访摄盐情况
                BindSYQK3(dt高血压);
                this.txt第3次随访心理调整.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt第3次随访遵医行为.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt第3次随访辅助检查.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt第3次随访服药依从性.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt第3次随访药物不良反应.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt第3次随访药物不良反应其他.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt第3次随访分类.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.本次随访分类].ToString();
                this.txt第3次随访转诊原因.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt第3次随访转诊机构.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt第3次随访下次随访日期.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt第3次随访医生签名.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.随访医生].ToString();
            }

            #endregion

            #region 4次随访记录

            if (dt高血压 != null && dt高血压.Rows.Count > 3)
            {
                this.txt第4次随访日期.Text = Convert.ToDateTime(dt高血压.Rows[3][Models.tb_MXB高血压随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt第4次随访方式.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.随访方式].ToString();
                //第4次随访症状
                BindZZ4(dt高血压);
                this.txt第4次随访症状其他.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.目前症状其他].ToString();
                this.txt第4次随访血压1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.收缩压].ToString();
                this.txt第4次随访血压2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.舒张压].ToString();
                this.txt第4次随访体重1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.体重].ToString();
                this.txt第4次随访体重2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.体重2].ToString();
                this.txt第4次随访体质指数1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.BMI].ToString();
                this.txt第4次随访体质指数2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.BMI2].ToString();
                this.txt第4次随访心率.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.心率].ToString();
                this.txt第4次随访体征其他.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.体征其他].ToString();
                this.txt第4次随访日吸烟量1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.吸烟数量].ToString();
                this.txt第4次随访日饮酒量1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.饮酒数量].ToString();
                this.txt第4次随访日吸烟量2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.吸烟数量2].ToString();
                this.txt第4次随访日饮酒量2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.饮酒数量2].ToString();
                this.txt第4次随访运动次数1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.运动频率].ToString();
                this.txt第4次随访运动时间1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.运动持续时间].ToString();
                this.txt第4次随访运动次数2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.运动频率2].ToString();
                this.txt第4次随访运动时间2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.运动持续时间2].ToString();
                //第4次随访摄盐情况
                BindSYQK4(dt高血压);
                this.txt第4次随访心理调整.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt第4次随访遵医行为.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt第4次随访辅助检查.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt第4次随访服药依从性.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt第4次随访药物不良反应.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt第4次随访药物不良反应其他.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt第4次随访分类.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.本次随访分类].ToString();
                this.txt第4次随访转诊原因.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt第4次随访转诊机构.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt第4次随访下次随访日期.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt第4次随访医生签名.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.随访医生].ToString();

            #endregion
            }
        }

        #region 摄盐情况
        private void BindSYQK4(DataTable dt高血压)
        {
            this.txt第4次随访摄盐情况1.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.摄盐情况].ToString();
            if (this.txt第4次随访摄盐情况1.Text == "1")
            {
                this.txt第4次随访摄盐情况1.Text = "轻";
            }
            else if (this.txt第4次随访摄盐情况1.Text == "2")
            {
                this.txt第4次随访摄盐情况1.Text = "中";
            }
            else if (this.txt第4次随访摄盐情况1.Text == "3")
            {
                this.txt第4次随访摄盐情况1.Text = "重";
            }
            this.txt第4次随访摄盐情况2.Text = dt高血压.Rows[3][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
            if (this.txt第4次随访摄盐情况2.Text == "1")
            {
                this.txt第4次随访摄盐情况2.Text = "轻";
            }
            else if (this.txt第4次随访摄盐情况2.Text == "2")
            {
                this.txt第4次随访摄盐情况2.Text = "中";
            }
            else if (this.txt第4次随访摄盐情况2.Text == "3")
            {
                this.txt第4次随访摄盐情况2.Text = "重";
            }
        }

        private void BindSYQK3(DataTable dt高血压)
        {
            this.txt第3次随访摄盐情况1.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.摄盐情况].ToString();
            if (this.txt第3次随访摄盐情况1.Text == "1")
            {
                this.txt第3次随访摄盐情况1.Text = "轻";
            }
            else if (this.txt第3次随访摄盐情况1.Text == "2")
            {
                this.txt第3次随访摄盐情况1.Text = "中";
            }
            else if (this.txt第3次随访摄盐情况1.Text == "3")
            {
                this.txt第3次随访摄盐情况1.Text = "重";
            }
            this.txt第3次随访摄盐情况2.Text = dt高血压.Rows[2][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
            if (this.txt第3次随访摄盐情况2.Text == "1")
            {
                this.txt第3次随访摄盐情况2.Text = "轻";
            }
            else if (this.txt第3次随访摄盐情况2.Text == "2")
            {
                this.txt第3次随访摄盐情况2.Text = "中";
            }
            else if (this.txt第3次随访摄盐情况2.Text == "3")
            {
                this.txt第3次随访摄盐情况2.Text = "重";
            }
        }

        private void BindSYQK2(DataTable dt高血压)
        {
            this.txt第2次随访摄盐情况1.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.摄盐情况].ToString();
            if (this.txt第2次随访摄盐情况1.Text == "1")
            {
                this.txt第2次随访摄盐情况1.Text = "轻";
            }
            else if (this.txt第2次随访摄盐情况1.Text == "2")
            {
                this.txt第2次随访摄盐情况1.Text = "中";
            }
            else if (this.txt第2次随访摄盐情况1.Text == "3")
            {
                this.txt第2次随访摄盐情况1.Text = "重";
            }
            this.txt第2次随访摄盐情况2.Text = dt高血压.Rows[1][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
            if (this.txt第2次随访摄盐情况2.Text == "1")
            {
                this.txt第2次随访摄盐情况2.Text = "轻";
            }
            else if (this.txt第2次随访摄盐情况2.Text == "2")
            {
                this.txt第2次随访摄盐情况2.Text = "中";
            }
            else if (this.txt第2次随访摄盐情况2.Text == "3")
            {
                this.txt第2次随访摄盐情况2.Text = "重";
            }
        }

        private void BindSYQK1(DataTable dt高血压)
        {
            this.txt第1次随访摄盐情况1.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.摄盐情况].ToString();
            if (this.txt第1次随访摄盐情况1.Text == "1")
            {
                this.txt第1次随访摄盐情况1.Text = "轻";
            }
            else if (this.txt第1次随访摄盐情况1.Text == "2")
            {
                this.txt第1次随访摄盐情况1.Text = "中";
            }
            else if (this.txt第1次随访摄盐情况1.Text == "3")
            {
                this.txt第1次随访摄盐情况1.Text = "重";
            }
            this.txt第1次随访摄盐情况2.Text = dt高血压.Rows[0][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
            if (this.txt第1次随访摄盐情况2.Text == "1")
            {
                this.txt第1次随访摄盐情况2.Text = "轻";
            }
            else if (this.txt第1次随访摄盐情况2.Text == "2")
            {
                this.txt第1次随访摄盐情况2.Text = "中";
            }
            else if (this.txt第1次随访摄盐情况2.Text == "3")
            {
                this.txt第1次随访摄盐情况2.Text = "重";
            }
        }

        #endregion

        #region 症状
        private void BindZZ4(DataTable dt高血压)
        {
            string 第4次随访症状 = dt高血压.Rows[3][Models.tb_MXB高血压随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第4次随访症状))
            {
                string[] a = 第4次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第4次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ3(DataTable dt高血压)
        {
            string 第3次随访症状 = dt高血压.Rows[2][Models.tb_MXB高血压随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第3次随访症状))
            {
                string[] a = 第3次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第3次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ2(DataTable dt高血压)
        {
            string 第2次随访症状 = dt高血压.Rows[1][Models.tb_MXB高血压随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第2次随访症状))
            {
                string[] a = 第2次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第2次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        private void BindZZ1(DataTable dt高血压)
        {
            string 第1次随访症状 = dt高血压.Rows[0][Models.tb_MXB高血压随访表.目前症状].ToString();
            if (!string.IsNullOrEmpty(第1次随访症状))
            {
                string[] a = 第1次随访症状.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt第1次随访症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = " ";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
        }

        #endregion

        #region 用药情况
        private void BindYYQK4()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第4次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第4次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第4次随访药物名称1" && ctr用法 == "txt第4次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第4次随访药物名称2" && ctr用法 == "txt第4次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第4次随访药物名称3" && ctr用法 == "txt第4次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK3()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第3次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第3次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第3次随访药物名称1" && ctr用法 == "txt第3次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次随访药物名称2" && ctr用法 == "txt第3次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次随访药物名称3" && ctr用法 == "txt第3次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK2()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第2次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第2次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第2次随访药物名称1" && ctr用法 == "txt第2次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次随访药物名称2" && ctr用法 == "txt第2次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次随访药物名称3" && ctr用法 == "txt第2次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }

                }
            }
        }

        private void BindYYQK1()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    string ctr药物 = "txt第1次随访药物名称" + (i + 1);
                    string ctr用法 = "txt第1次随访药物" + (i + 1) + "用法";
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第1次随访药物名称1" && ctr用法 == "txt第1次随访药物1用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次随访药物名称2" && ctr用法 == "txt第1次随访药物2用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次随访药物名称3" && ctr用法 == "txt第1次随访药物3用法")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB高血压随访表_用药情况.用法].ToString();
                    }

                }
            }
        }
        
        #endregion

    }
}
