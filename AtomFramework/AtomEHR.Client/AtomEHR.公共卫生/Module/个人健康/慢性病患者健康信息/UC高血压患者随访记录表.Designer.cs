﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC高血压患者随访记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC高血压患者随访记录表));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.Layout1 = new DevExpress.XtraLayout.LayoutControl();
            this.sbtn便捷录入备注 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn查看下一步措施 = new DevExpress.XtraEditors.SimpleButton();
            this.fl访视情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk失访 = new DevExpress.XtraEditors.CheckEdit();
            this.chk死亡 = new DevExpress.XtraEditors.CheckEdit();
            this.fl死亡信息 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dte死亡日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt死亡原因 = new DevExpress.XtraEditors.TextEdit();
            this.fl失访原因 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk外出打工 = new DevExpress.XtraEditors.CheckEdit();
            this.chk迁居他处 = new DevExpress.XtraEditors.CheckEdit();
            this.chk走失 = new DevExpress.XtraEditors.CheckEdit();
            this.chk连续3次 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.sbtnFingerPrint = new DevExpress.XtraEditors.SimpleButton();
            this.gc药物调整 = new DevExpress.XtraGrid.GridControl();
            this.gv药物调整 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.btn删除药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.txt转诊结果 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系人 = new DevExpress.XtraEditors.TextEdit();
            this.radio药物调整 = new DevExpress.XtraEditors.RadioGroup();
            this.radio管理措施 = new DevExpress.XtraEditors.RadioGroup();
            this.txt随访方式其他 = new System.Windows.Forms.TextBox();
            this.fl随访分类并发症 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.LookUpEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.fl随访分类 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk控制满意 = new DevExpress.XtraEditors.CheckEdit();
            this.chk控制不满意 = new DevExpress.XtraEditors.CheckEdit();
            this.chk不良反应 = new DevExpress.XtraEditors.CheckEdit();
            this.ck并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.check无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.ch症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.sbtn便捷录入 = new DevExpress.XtraEditors.SimpleButton();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt心率 = new DevExpress.XtraEditors.TextEdit();
            this.txt体征其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt辅助检查 = new DevExpress.XtraEditors.TextEdit();
            this.radio不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.radio转诊情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt遵医行为 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt居民签名 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout随访分类并发症 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物调整列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl失访 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl死亡 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).BeginInit();
            this.Layout1.SuspendLayout();
            this.fl访视情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).BeginInit();
            this.fl死亡信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).BeginInit();
            this.fl失访原因.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).BeginInit();
            this.fl随访分类并发症.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            this.fl随访分类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk控制满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk控制不满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout随访分类并发症)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl死亡)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(729, 28);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            this.btn重置.Visible = false;
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 2;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // Layout1
            // 
            this.Layout1.Controls.Add(this.sbtn便捷录入备注);
            this.Layout1.Controls.Add(this.sbtn查看下一步措施);
            this.Layout1.Controls.Add(this.fl访视情况);
            this.Layout1.Controls.Add(this.fl死亡信息);
            this.Layout1.Controls.Add(this.fl失访原因);
            this.Layout1.Controls.Add(this.sbtnFingerPrint);
            this.Layout1.Controls.Add(this.gc药物调整);
            this.Layout1.Controls.Add(this.layoutControl1);
            this.Layout1.Controls.Add(this.btn删除药物调整);
            this.Layout1.Controls.Add(this.btn添加药物调整);
            this.Layout1.Controls.Add(this.txt转诊结果);
            this.Layout1.Controls.Add(this.txt备注);
            this.Layout1.Controls.Add(this.txt转诊联系电话);
            this.Layout1.Controls.Add(this.txt转诊联系人);
            this.Layout1.Controls.Add(this.radio药物调整);
            this.Layout1.Controls.Add(this.radio管理措施);
            this.Layout1.Controls.Add(this.txt随访方式其他);
            this.Layout1.Controls.Add(this.fl随访分类并发症);
            this.Layout1.Controls.Add(this.txt联系电话);
            this.Layout1.Controls.Add(this.txt职业);
            this.Layout1.Controls.Add(this.txt身份证号);
            this.Layout1.Controls.Add(this.txt出生日期);
            this.Layout1.Controls.Add(this.txt性别);
            this.Layout1.Controls.Add(this.txt姓名);
            this.Layout1.Controls.Add(this.txt个人档案号);
            this.Layout1.Controls.Add(this.txt摄盐情况2);
            this.Layout1.Controls.Add(this.txt摄盐情况1);
            this.Layout1.Controls.Add(this.gcDetail);
            this.Layout1.Controls.Add(this.btn删除药物);
            this.Layout1.Controls.Add(this.btn添加药物);
            this.Layout1.Controls.Add(this.txt发生时间);
            this.Layout1.Controls.Add(this.fl随访分类);
            this.Layout1.Controls.Add(this.txt下次随访时间);
            this.Layout1.Controls.Add(this.radio用药情况);
            this.Layout1.Controls.Add(this.fl症状);
            this.Layout1.Controls.Add(this.radio随访方式);
            this.Layout1.Controls.Add(this.txt血压值);
            this.Layout1.Controls.Add(this.txt体重);
            this.Layout1.Controls.Add(this.txt身高);
            this.Layout1.Controls.Add(this.txt体质指数);
            this.Layout1.Controls.Add(this.txt心率);
            this.Layout1.Controls.Add(this.txt体征其他);
            this.Layout1.Controls.Add(this.txt日吸烟量);
            this.Layout1.Controls.Add(this.txt饮酒情况);
            this.Layout1.Controls.Add(this.txt运动频率);
            this.Layout1.Controls.Add(this.txt持续时间);
            this.Layout1.Controls.Add(this.txt辅助检查);
            this.Layout1.Controls.Add(this.radio不良反应);
            this.Layout1.Controls.Add(this.txt药物副作用详述);
            this.Layout1.Controls.Add(this.txt医生建议);
            this.Layout1.Controls.Add(this.radio转诊情况);
            this.Layout1.Controls.Add(this.txt转诊科别);
            this.Layout1.Controls.Add(this.txt医生签名);
            this.Layout1.Controls.Add(this.lbl创建时间);
            this.Layout1.Controls.Add(this.lab创建机构);
            this.Layout1.Controls.Add(this.lbl最近更新时间);
            this.Layout1.Controls.Add(this.lab创建人);
            this.Layout1.Controls.Add(this.lab考核项);
            this.Layout1.Controls.Add(this.txt转诊原因);
            this.Layout1.Controls.Add(this.lab最近修改人);
            this.Layout1.Controls.Add(this.lab当前所属机构);
            this.Layout1.Controls.Add(this.txt心理调整);
            this.Layout1.Controls.Add(this.txt遵医行为);
            this.Layout1.Controls.Add(this.txt服药依从性);
            this.Layout1.Controls.Add(this.txt居住地址);
            this.Layout1.Controls.Add(this.txt居民签名);
            this.Layout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Layout1.Location = new System.Drawing.Point(0, 28);
            this.Layout1.Name = "Layout1";
            this.Layout1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(46, 193, 250, 350);
            this.Layout1.Root = this.layoutControlGroup1;
            this.Layout1.Size = new System.Drawing.Size(729, 463);
            this.Layout1.TabIndex = 5;
            // 
            // sbtn便捷录入备注
            // 
            this.sbtn便捷录入备注.Location = new System.Drawing.Point(617, 639);
            this.sbtn便捷录入备注.Name = "sbtn便捷录入备注";
            this.sbtn便捷录入备注.Size = new System.Drawing.Size(89, 22);
            this.sbtn便捷录入备注.StyleController = this.Layout1;
            this.sbtn便捷录入备注.TabIndex = 156;
            this.sbtn便捷录入备注.Text = "便捷录入备注";
            this.sbtn便捷录入备注.Click += new System.EventHandler(this.sbtn便捷录入备注_Click);
            // 
            // sbtn查看下一步措施
            // 
            this.sbtn查看下一步措施.Location = new System.Drawing.Point(533, 372);
            this.sbtn查看下一步措施.Name = "sbtn查看下一步措施";
            this.sbtn查看下一步措施.Size = new System.Drawing.Size(173, 36);
            this.sbtn查看下一步措施.StyleController = this.Layout1;
            this.sbtn查看下一步措施.TabIndex = 155;
            this.sbtn查看下一步措施.Text = "查看近期的下一步管理措施";
            this.sbtn查看下一步措施.Click += new System.EventHandler(this.sbtn查看下一步措施_Click);
            // 
            // fl访视情况
            // 
            this.fl访视情况.Controls.Add(this.chk失访);
            this.fl访视情况.Controls.Add(this.chk死亡);
            this.fl访视情况.Location = new System.Drawing.Point(95, -170);
            this.fl访视情况.Name = "fl访视情况";
            this.fl访视情况.Size = new System.Drawing.Size(611, 26);
            this.fl访视情况.TabIndex = 154;
            // 
            // chk失访
            // 
            this.chk失访.Location = new System.Drawing.Point(3, 3);
            this.chk失访.Name = "chk失访";
            this.chk失访.Properties.Caption = "失访";
            this.chk失访.Size = new System.Drawing.Size(61, 19);
            this.chk失访.TabIndex = 1;
            this.chk失访.Tag = "1";
            this.chk失访.CheckedChanged += new System.EventHandler(this.chk失访_CheckedChanged);
            // 
            // chk死亡
            // 
            this.chk死亡.Location = new System.Drawing.Point(70, 3);
            this.chk死亡.Name = "chk死亡";
            this.chk死亡.Properties.Caption = "死亡";
            this.chk死亡.Size = new System.Drawing.Size(61, 19);
            this.chk死亡.TabIndex = 2;
            this.chk死亡.Tag = "2";
            this.chk死亡.CheckedChanged += new System.EventHandler(this.chk死亡_CheckedChanged);
            // 
            // fl死亡信息
            // 
            this.fl死亡信息.Controls.Add(this.labelControl1);
            this.fl死亡信息.Controls.Add(this.dte死亡日期);
            this.fl死亡信息.Controls.Add(this.labelControl2);
            this.fl死亡信息.Controls.Add(this.txt死亡原因);
            this.fl死亡信息.Enabled = false;
            this.fl死亡信息.Location = new System.Drawing.Point(95, -110);
            this.fl死亡信息.Name = "fl死亡信息";
            this.fl死亡信息.Size = new System.Drawing.Size(611, 26);
            this.fl死亡信息.TabIndex = 152;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "死亡日期：";
            // 
            // dte死亡日期
            // 
            this.dte死亡日期.EditValue = null;
            this.dte死亡日期.Location = new System.Drawing.Point(69, 3);
            this.dte死亡日期.Name = "dte死亡日期";
            this.dte死亡日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte死亡日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte死亡日期.Size = new System.Drawing.Size(100, 20);
            this.dte死亡日期.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(175, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "    死亡原因：";
            // 
            // txt死亡原因
            // 
            this.txt死亡原因.Location = new System.Drawing.Point(257, 3);
            this.txt死亡原因.Name = "txt死亡原因";
            this.txt死亡原因.Size = new System.Drawing.Size(210, 20);
            this.txt死亡原因.TabIndex = 3;
            // 
            // fl失访原因
            // 
            this.fl失访原因.Controls.Add(this.chk外出打工);
            this.fl失访原因.Controls.Add(this.chk迁居他处);
            this.fl失访原因.Controls.Add(this.chk走失);
            this.fl失访原因.Controls.Add(this.chk连续3次);
            this.fl失访原因.Controls.Add(this.chk其他);
            this.fl失访原因.Enabled = false;
            this.fl失访原因.Location = new System.Drawing.Point(95, -140);
            this.fl失访原因.Name = "fl失访原因";
            this.fl失访原因.Size = new System.Drawing.Size(611, 26);
            this.fl失访原因.TabIndex = 151;
            // 
            // chk外出打工
            // 
            this.chk外出打工.Location = new System.Drawing.Point(3, 3);
            this.chk外出打工.Name = "chk外出打工";
            this.chk外出打工.Properties.Caption = "外出打工";
            this.chk外出打工.Size = new System.Drawing.Size(75, 19);
            this.chk外出打工.TabIndex = 0;
            this.chk外出打工.Tag = "1";
            // 
            // chk迁居他处
            // 
            this.chk迁居他处.Location = new System.Drawing.Point(84, 3);
            this.chk迁居他处.Name = "chk迁居他处";
            this.chk迁居他处.Properties.Caption = "迁居他处";
            this.chk迁居他处.Size = new System.Drawing.Size(75, 19);
            this.chk迁居他处.TabIndex = 0;
            this.chk迁居他处.Tag = "2";
            // 
            // chk走失
            // 
            this.chk走失.Location = new System.Drawing.Point(165, 3);
            this.chk走失.Name = "chk走失";
            this.chk走失.Properties.Caption = "走失";
            this.chk走失.Size = new System.Drawing.Size(62, 19);
            this.chk走失.TabIndex = 0;
            this.chk走失.Tag = "3";
            // 
            // chk连续3次
            // 
            this.chk连续3次.Location = new System.Drawing.Point(233, 3);
            this.chk连续3次.Name = "chk连续3次";
            this.chk连续3次.Properties.Caption = "连续3次未到访";
            this.chk连续3次.Size = new System.Drawing.Size(114, 19);
            this.chk连续3次.TabIndex = 0;
            this.chk连续3次.Tag = "4";
            // 
            // chk其他
            // 
            this.chk其他.Location = new System.Drawing.Point(353, 3);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Caption = "其他";
            this.chk其他.Size = new System.Drawing.Size(76, 19);
            this.chk其他.TabIndex = 0;
            this.chk其他.Tag = "5";
            // 
            // sbtnFingerPrint
            // 
            this.sbtnFingerPrint.Location = new System.Drawing.Point(234, 639);
            this.sbtnFingerPrint.Name = "sbtnFingerPrint";
            this.sbtnFingerPrint.Size = new System.Drawing.Size(78, 22);
            this.sbtnFingerPrint.StyleController = this.Layout1;
            this.sbtnFingerPrint.TabIndex = 150;
            this.sbtnFingerPrint.Text = "更新指纹";
            this.sbtnFingerPrint.Click += new System.EventHandler(this.sbtnFingerPrint_Click);
            // 
            // gc药物调整
            // 
            this.gc药物调整.Location = new System.Drawing.Point(6, 462);
            this.gc药物调整.MainView = this.gv药物调整;
            this.gc药物调整.Name = "gc药物调整";
            this.gc药物调整.Size = new System.Drawing.Size(700, 66);
            this.gc药物调整.TabIndex = 149;
            this.gc药物调整.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv药物调整});
            // 
            // gv药物调整
            // 
            this.gv药物调整.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gv药物调整.GridControl = this.gc药物调整;
            this.gv药物调整.Name = "gv药物调整";
            this.gv药物调整.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "药物名称";
            this.gridColumn5.FieldName = "药物名称";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "用法";
            this.gridColumn6.FieldName = "用法";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "个人档案编号";
            this.gridColumn7.FieldName = "个人档案编号";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "创建时间";
            this.gridColumn8.FieldName = "创建时间";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Location = new System.Drawing.Point(416, 432);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(290, 26);
            this.layoutControl1.TabIndex = 148;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(290, 26);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // btn删除药物调整
            // 
            this.btn删除药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物调整.Image")));
            this.btn删除药物调整.Location = new System.Drawing.Point(349, 432);
            this.btn删除药物调整.Name = "btn删除药物调整";
            this.btn删除药物调整.Size = new System.Drawing.Size(63, 22);
            this.btn删除药物调整.StyleController = this.Layout1;
            this.btn删除药物调整.TabIndex = 147;
            this.btn删除药物调整.Tag = "2";
            this.btn删除药物调整.Text = "删除";
            // 
            // btn添加药物调整
            // 
            this.btn添加药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物调整.Image")));
            this.btn添加药物调整.Location = new System.Drawing.Point(279, 432);
            this.btn添加药物调整.Name = "btn添加药物调整";
            this.btn添加药物调整.Size = new System.Drawing.Size(66, 22);
            this.btn添加药物调整.StyleController = this.Layout1;
            this.btn添加药物调整.TabIndex = 146;
            this.btn添加药物调整.Tag = "0";
            this.btn添加药物调整.Text = "添加";
            // 
            // txt转诊结果
            // 
            this.txt转诊结果.Enabled = false;
            this.txt转诊结果.Location = new System.Drawing.Point(559, 591);
            this.txt转诊结果.Name = "txt转诊结果";
            this.txt转诊结果.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt转诊结果.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt转诊结果.Properties.NullText = "";
            this.txt转诊结果.Size = new System.Drawing.Size(147, 20);
            this.txt转诊结果.StyleController = this.Layout1;
            this.txt转诊结果.TabIndex = 145;
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(407, 639);
            this.txt备注.Name = "txt备注";
            this.txt备注.Size = new System.Drawing.Size(206, 20);
            this.txt备注.StyleController = this.Layout1;
            this.txt备注.TabIndex = 144;
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Enabled = false;
            this.txt转诊联系电话.Location = new System.Drawing.Point(284, 591);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Size = new System.Drawing.Size(180, 20);
            this.txt转诊联系电话.StyleController = this.Layout1;
            this.txt转诊联系电话.TabIndex = 142;
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Enabled = false;
            this.txt转诊联系人.Location = new System.Drawing.Point(97, 591);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Size = new System.Drawing.Size(93, 20);
            this.txt转诊联系人.StyleController = this.Layout1;
            this.txt转诊联系人.TabIndex = 141;
            // 
            // radio药物调整
            // 
            this.radio药物调整.EditValue = "2";
            this.radio药物调整.Location = new System.Drawing.Point(100, 432);
            this.radio药物调整.Name = "radio药物调整";
            this.radio药物调整.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio药物调整.Size = new System.Drawing.Size(175, 26);
            this.radio药物调整.StyleController = this.Layout1;
            this.radio药物调整.TabIndex = 140;
            this.radio药物调整.SelectedIndexChanged += new System.EventHandler(this.radio药物调整_SelectedIndexChanged);
            // 
            // radio管理措施
            // 
            this.radio管理措施.EditValue = "1";
            this.radio管理措施.Location = new System.Drawing.Point(100, 372);
            this.radio管理措施.Name = "radio管理措施";
            this.radio管理措施.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "常规随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "第1次控制不满意2周随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "两次控制不满意转诊随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "紧急转诊")});
            this.radio管理措施.Size = new System.Drawing.Size(429, 56);
            this.radio管理措施.StyleController = this.Layout1;
            this.radio管理措施.TabIndex = 139;
            // 
            // txt随访方式其他
            // 
            this.txt随访方式其他.Location = new System.Drawing.Point(522, -199);
            this.txt随访方式其他.Name = "txt随访方式其他";
            this.txt随访方式其他.Size = new System.Drawing.Size(184, 20);
            this.txt随访方式其他.TabIndex = 138;
            // 
            // fl随访分类并发症
            // 
            this.fl随访分类并发症.Controls.Add(this.labelControl39);
            this.fl随访分类并发症.Controls.Add(this.checkEdit19);
            this.fl随访分类并发症.Controls.Add(this.checkEdit20);
            this.fl随访分类并发症.Controls.Add(this.checkEdit18);
            this.fl随访分类并发症.Controls.Add(this.labelControl41);
            this.fl随访分类并发症.Location = new System.Drawing.Point(450, 245);
            this.fl随访分类并发症.Name = "fl随访分类并发症";
            this.fl随访分类并发症.Size = new System.Drawing.Size(256, 20);
            this.fl随访分类并发症.TabIndex = 137;
            // 
            // labelControl39
            // 
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Location = new System.Drawing.Point(3, 3);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(10, 14);
            this.labelControl39.TabIndex = 10;
            this.labelControl39.Text = "（";
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(17, 1);
            this.checkEdit19.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "脑卒中";
            this.checkEdit19.Size = new System.Drawing.Size(57, 19);
            this.checkEdit19.TabIndex = 8;
            this.checkEdit19.Tag = "1";
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(76, 1);
            this.checkEdit20.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "冠心病";
            this.checkEdit20.Size = new System.Drawing.Size(57, 19);
            this.checkEdit20.TabIndex = 9;
            this.checkEdit20.Tag = "2";
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(135, 1);
            this.checkEdit18.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "其他";
            this.checkEdit18.Size = new System.Drawing.Size(45, 19);
            this.checkEdit18.TabIndex = 7;
            this.checkEdit18.Tag = "99";
            // 
            // labelControl41
            // 
            this.labelControl41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl41.Location = new System.Drawing.Point(184, 3);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(12, 14);
            this.labelControl41.TabIndex = 11;
            this.labelControl41.Text = "）";
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(95, -232);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Size = new System.Drawing.Size(218, 20);
            this.txt联系电话.StyleController = this.Layout1;
            this.txt联系电话.TabIndex = 136;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(411, -263);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Size = new System.Drawing.Size(295, 20);
            this.txt职业.StyleController = this.Layout1;
            this.txt职业.TabIndex = 135;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(95, -263);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Size = new System.Drawing.Size(218, 20);
            this.txt身份证号.StyleController = this.Layout1;
            this.txt身份证号.TabIndex = 131;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(411, -287);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Size = new System.Drawing.Size(295, 20);
            this.txt出生日期.StyleController = this.Layout1;
            this.txt出生日期.TabIndex = 130;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(95, -287);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Size = new System.Drawing.Size(218, 20);
            this.txt性别.StyleController = this.Layout1;
            this.txt性别.TabIndex = 129;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(411, -311);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Size = new System.Drawing.Size(295, 20);
            this.txt姓名.StyleController = this.Layout1;
            this.txt姓名.TabIndex = 128;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(95, -311);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Size = new System.Drawing.Size(218, 20);
            this.txt个人档案号.StyleController = this.Layout1;
            this.txt个人档案号.TabIndex = 127;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.Location = new System.Drawing.Point(230, 117);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(125, 20);
            this.txt摄盐情况2.StyleController = this.Layout1;
            this.txt摄盐情况2.TabIndex = 126;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.Location = new System.Drawing.Point(96, 117);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(120, 20);
            this.txt摄盐情况1.StyleController = this.Layout1;
            this.txt摄盐情况1.TabIndex = 125;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(6, 298);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(700, 70);
            this.gcDetail.TabIndex = 92;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物.Image")));
            this.btn删除药物.Location = new System.Drawing.Point(340, 269);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(56, 22);
            this.btn删除药物.StyleController = this.Layout1;
            this.btn删除药物.TabIndex = 95;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物.Image")));
            this.btn添加药物.Location = new System.Drawing.Point(279, 269);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(57, 22);
            this.btn添加药物.StyleController = this.Layout1;
            this.btn添加药物.TabIndex = 94;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(95, -199);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(100, 20);
            this.txt发生时间.StyleController = this.Layout1;
            this.txt发生时间.TabIndex = 124;
            // 
            // fl随访分类
            // 
            this.fl随访分类.Controls.Add(this.chk控制满意);
            this.fl随访分类.Controls.Add(this.chk控制不满意);
            this.fl随访分类.Controls.Add(this.chk不良反应);
            this.fl随访分类.Controls.Add(this.ck并发症);
            this.fl随访分类.Location = new System.Drawing.Point(97, 245);
            this.fl随访分类.Margin = new System.Windows.Forms.Padding(0);
            this.fl随访分类.Name = "fl随访分类";
            this.fl随访分类.Size = new System.Drawing.Size(349, 20);
            this.fl随访分类.TabIndex = 112;
            // 
            // chk控制满意
            // 
            this.chk控制满意.Location = new System.Drawing.Point(1, 1);
            this.chk控制满意.Margin = new System.Windows.Forms.Padding(1);
            this.chk控制满意.Name = "chk控制满意";
            this.chk控制满意.Properties.Caption = "控制满意";
            this.chk控制满意.Size = new System.Drawing.Size(70, 19);
            this.chk控制满意.TabIndex = 0;
            this.chk控制满意.Tag = "1";
            // 
            // chk控制不满意
            // 
            this.chk控制不满意.Location = new System.Drawing.Point(73, 1);
            this.chk控制不满意.Margin = new System.Windows.Forms.Padding(1);
            this.chk控制不满意.Name = "chk控制不满意";
            this.chk控制不满意.Properties.Caption = "控制不满意";
            this.chk控制不满意.Size = new System.Drawing.Size(80, 19);
            this.chk控制不满意.TabIndex = 1;
            this.chk控制不满意.Tag = "2";
            // 
            // chk不良反应
            // 
            this.chk不良反应.Location = new System.Drawing.Point(155, 1);
            this.chk不良反应.Margin = new System.Windows.Forms.Padding(1);
            this.chk不良反应.Name = "chk不良反应";
            this.chk不良反应.Properties.Caption = "不良反应";
            this.chk不良反应.Size = new System.Drawing.Size(70, 19);
            this.chk不良反应.TabIndex = 2;
            this.chk不良反应.Tag = "3";
            // 
            // ck并发症
            // 
            this.ck并发症.Location = new System.Drawing.Point(227, 1);
            this.ck并发症.Margin = new System.Windows.Forms.Padding(1);
            this.ck并发症.Name = "ck并发症";
            this.ck并发症.Properties.Caption = "并发症";
            this.ck并发症.Size = new System.Drawing.Size(57, 19);
            this.ck并发症.TabIndex = 3;
            this.ck并发症.Tag = "4";
            this.ck并发症.CheckedChanged += new System.EventHandler(this.ck并发症_CheckedChanged);
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(97, 615);
            this.txt下次随访时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt下次随访时间.Size = new System.Drawing.Size(215, 20);
            this.txt下次随访时间.StyleController = this.Layout1;
            this.txt下次随访时间.TabIndex = 76;
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(97, 269);
            this.radio用药情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio用药情况.Size = new System.Drawing.Size(178, 25);
            this.radio用药情况.StyleController = this.Layout1;
            this.radio用药情况.TabIndex = 69;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // fl症状
            // 
            this.fl症状.AutoScroll = true;
            this.fl症状.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fl症状.Controls.Add(this.check无症状);
            this.fl症状.Controls.Add(this.checkEdit9);
            this.fl症状.Controls.Add(this.checkEdit10);
            this.fl症状.Controls.Add(this.checkEdit11);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Controls.Add(this.checkEdit15);
            this.fl症状.Controls.Add(this.checkEdit16);
            this.fl症状.Controls.Add(this.ch症状其他);
            this.fl症状.Controls.Add(this.txt症状其他);
            this.fl症状.Controls.Add(this.sbtn便捷录入);
            this.fl症状.Location = new System.Drawing.Point(95, -80);
            this.fl症状.Margin = new System.Windows.Forms.Padding(0);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(611, 50);
            this.fl症状.TabIndex = 27;
            // 
            // check无症状
            // 
            this.check无症状.Location = new System.Drawing.Point(1, 1);
            this.check无症状.Margin = new System.Windows.Forms.Padding(1);
            this.check无症状.Name = "check无症状";
            this.check无症状.Properties.Caption = "1.无症状";
            this.check无症状.Size = new System.Drawing.Size(75, 19);
            this.check无症状.TabIndex = 0;
            this.check无症状.Tag = "0";
            this.check无症状.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(78, 1);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "2.头晕头疼";
            this.checkEdit9.Size = new System.Drawing.Size(93, 19);
            this.checkEdit9.TabIndex = 1;
            this.checkEdit9.Tag = "1";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(173, 1);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "3.恶心呕吐";
            this.checkEdit10.Size = new System.Drawing.Size(87, 19);
            this.checkEdit10.TabIndex = 2;
            this.checkEdit10.Tag = "2";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(262, 1);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "4.眼花耳鸣";
            this.checkEdit11.Size = new System.Drawing.Size(85, 19);
            this.checkEdit11.TabIndex = 3;
            this.checkEdit11.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(349, 1);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "5.呼吸困难";
            this.checkEdit12.Size = new System.Drawing.Size(91, 19);
            this.checkEdit12.TabIndex = 4;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(442, 1);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "6.心悸胸闷";
            this.checkEdit13.Size = new System.Drawing.Size(93, 19);
            this.checkEdit13.TabIndex = 5;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(1, 24);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "7.鼻衄出血不止";
            this.checkEdit14.Size = new System.Drawing.Size(109, 19);
            this.checkEdit14.TabIndex = 6;
            this.checkEdit14.Tag = "6";
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(112, 24);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "8.四肢发麻";
            this.checkEdit15.Size = new System.Drawing.Size(88, 19);
            this.checkEdit15.TabIndex = 7;
            this.checkEdit15.Tag = "7";
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(202, 24);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "9.下肢水肿";
            this.checkEdit16.Size = new System.Drawing.Size(90, 19);
            this.checkEdit16.TabIndex = 8;
            this.checkEdit16.Tag = "8";
            // 
            // ch症状其他
            // 
            this.ch症状其他.Location = new System.Drawing.Point(294, 24);
            this.ch症状其他.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.ch症状其他.Name = "ch症状其他";
            this.ch症状其他.Properties.Caption = "10.其他症状";
            this.ch症状其他.Size = new System.Drawing.Size(94, 19);
            this.ch症状其他.TabIndex = 9;
            this.ch症状其他.Tag = "99";
            this.ch症状其他.CheckedChanged += new System.EventHandler(this.ch症状其他_CheckedChanged);
            // 
            // txt症状其他
            // 
            this.txt症状其他.Location = new System.Drawing.Point(390, 24);
            this.txt症状其他.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.txt症状其他.Name = "txt症状其他";
            this.txt症状其他.Size = new System.Drawing.Size(98, 20);
            this.txt症状其他.TabIndex = 10;
            // 
            // sbtn便捷录入
            // 
            this.sbtn便捷录入.Location = new System.Drawing.Point(492, 24);
            this.sbtn便捷录入.Name = "sbtn便捷录入";
            this.sbtn便捷录入.Size = new System.Drawing.Size(104, 23);
            this.sbtn便捷录入.TabIndex = 11;
            this.sbtn便捷录入.Text = "其他症状便捷录入";
            this.sbtn便捷录入.Visible = false;
            this.sbtn便捷录入.Click += new System.EventHandler(this.sbtn便捷录入_Click);
            // 
            // radio随访方式
            // 
            this.radio随访方式.EditValue = "-1";
            this.radio随访方式.Location = new System.Drawing.Point(293, -199);
            this.radio随访方式.Margin = new System.Windows.Forms.Padding(0);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "其他")});
            this.radio随访方式.Size = new System.Drawing.Size(225, 25);
            this.radio随访方式.StyleController = this.Layout1;
            this.radio随访方式.TabIndex = 21;
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(96, -4);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(0);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(170, 20);
            this.txt血压值.TabIndex = 53;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(20, 14);
            this.txt体重.Lbl2Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(315, -4);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(158, 20);
            this.txt体重.TabIndex = 90;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "CM";
            this.txt身高.Location = new System.Drawing.Point(522, -4);
            this.txt身高.Margin = new System.Windows.Forms.Padding(0);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(181, 20);
            this.txt身高.TabIndex = 45;
            this.txt身高.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(96, 20);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(170, 20);
            this.txt体质指数.TabIndex = 91;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt心率
            // 
            this.txt心率.Location = new System.Drawing.Point(315, 20);
            this.txt心率.Margin = new System.Windows.Forms.Padding(0);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(158, 20);
            this.txt心率.StyleController = this.Layout1;
            this.txt心率.TabIndex = 65;
            // 
            // txt体征其他
            // 
            this.txt体征其他.Location = new System.Drawing.Point(522, 20);
            this.txt体征其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt体征其他.Name = "txt体征其他";
            this.txt体征其他.Size = new System.Drawing.Size(181, 20);
            this.txt体征其他.StyleController = this.Layout1;
            this.txt体征其他.TabIndex = 92;
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(96, 69);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(259, 20);
            this.txt日吸烟量.TabIndex = 93;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(453, 69);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(250, 20);
            this.txt饮酒情况.TabIndex = 94;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(96, 93);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(0);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(259, 20);
            this.txt运动频率.TabIndex = 95;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt持续时间
            // 
            this.txt持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt持续时间.Lbl1Text = "/";
            this.txt持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt持续时间.Lbl2Text = "(分钟/次)";
            this.txt持续时间.Location = new System.Drawing.Point(453, 93);
            this.txt持续时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt持续时间.Name = "txt持续时间";
            this.txt持续时间.Size = new System.Drawing.Size(250, 20);
            this.txt持续时间.TabIndex = 97;
            this.txt持续时间.Txt1EditValue = null;
            this.txt持续时间.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt持续时间.Txt2EditValue = null;
            this.txt持续时间.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt辅助检查
            // 
            this.txt辅助检查.Location = new System.Drawing.Point(97, 168);
            this.txt辅助检查.Margin = new System.Windows.Forms.Padding(0);
            this.txt辅助检查.Name = "txt辅助检查";
            this.txt辅助检查.Size = new System.Drawing.Size(609, 20);
            this.txt辅助检查.StyleController = this.Layout1;
            this.txt辅助检查.TabIndex = 105;
            this.txt辅助检查.ToolTip = "123";
            this.txt辅助检查.ToolTipTitle = "1";
            // 
            // radio不良反应
            // 
            this.radio不良反应.EditValue = "1";
            this.radio不良反应.Location = new System.Drawing.Point(97, 192);
            this.radio不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.radio不良反应.Name = "radio不良反应";
            this.radio不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio不良反应.Size = new System.Drawing.Size(177, 25);
            this.radio不良反应.StyleController = this.Layout1;
            this.radio不良反应.TabIndex = 107;
            this.radio不良反应.SelectedIndexChanged += new System.EventHandler(this.radio不良反应_SelectedIndexChanged);
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Location = new System.Drawing.Point(372, 192);
            this.txt药物副作用详述.Margin = new System.Windows.Forms.Padding(0);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(334, 20);
            this.txt药物副作用详述.StyleController = this.Layout1;
            this.txt药物副作用详述.TabIndex = 109;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(97, 532);
            this.txt医生建议.Margin = new System.Windows.Forms.Padding(0);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(609, 26);
            this.txt医生建议.StyleController = this.Layout1;
            this.txt医生建议.TabIndex = 113;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // radio转诊情况
            // 
            this.radio转诊情况.EditValue = "2";
            this.radio转诊情况.Location = new System.Drawing.Point(97, 562);
            this.radio转诊情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio转诊情况.Name = "radio转诊情况";
            this.radio转诊情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio转诊情况.Size = new System.Drawing.Size(177, 25);
            this.radio转诊情况.StyleController = this.Layout1;
            this.radio转诊情况.TabIndex = 115;
            this.radio转诊情况.SelectedIndexChanged += new System.EventHandler(this.radio转诊情况_SelectedIndexChanged);
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Enabled = false;
            this.txt转诊科别.Location = new System.Drawing.Point(557, 562);
            this.txt转诊科别.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(149, 20);
            this.txt转诊科别.StyleController = this.Layout1;
            this.txt转诊科别.TabIndex = 116;
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(407, 615);
            this.txt医生签名.Margin = new System.Windows.Forms.Padding(0);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(299, 20);
            this.txt医生签名.StyleController = this.Layout1;
            this.txt医生签名.TabIndex = 119;
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(100, 699);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(132, 14);
            this.lbl创建时间.StyleController = this.Layout1;
            this.lbl创建时间.TabIndex = 88;
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(560, 699);
            this.lab创建机构.MaximumSize = new System.Drawing.Size(140, 0);
            this.lab创建机构.MinimumSize = new System.Drawing.Size(140, 0);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(140, 14);
            this.lab创建机构.StyleController = this.Layout1;
            this.lab创建机构.TabIndex = 86;
            this.lab创建机构.Text = "创建机构";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Location = new System.Drawing.Point(330, 699);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(132, 14);
            this.lbl最近更新时间.StyleController = this.Layout1;
            this.lbl最近更新时间.TabIndex = 84;
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(100, 717);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(132, 14);
            this.lab创建人.StyleController = this.Layout1;
            this.lab创建人.TabIndex = 82;
            this.lab创建人.Text = "录入人";
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(6, -329);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(700, 14);
            this.lab考核项.StyleController = this.Layout1;
            this.lab考核项.TabIndex = 1;
            this.lab考核项.Text = "考核项：22    缺项：0 完整度：100% ";
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Enabled = false;
            this.txt转诊原因.Location = new System.Drawing.Point(319, 562);
            this.txt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(143, 20);
            this.txt转诊原因.StyleController = this.Layout1;
            this.txt转诊原因.TabIndex = 118;
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(330, 717);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(132, 14);
            this.lab最近修改人.StyleController = this.Layout1;
            this.lab最近修改人.TabIndex = 122;
            this.lab最近修改人.Text = "最近更新人";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(560, 717);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(146, 14);
            this.lab当前所属机构.StyleController = this.Layout1;
            this.lab当前所属机构.TabIndex = 123;
            this.lab当前所属机构.Text = "当前所属机构";
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "请选择";
            this.txt心理调整.Location = new System.Drawing.Point(453, 117);
            this.txt心理调整.Margin = new System.Windows.Forms.Padding(0);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(250, 20);
            this.txt心理调整.StyleController = this.Layout1;
            this.txt心理调整.TabIndex = 101;
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.Location = new System.Drawing.Point(100, 141);
            this.txt遵医行为.Margin = new System.Windows.Forms.Padding(0);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt遵医行为.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt遵医行为.Properties.NullText = "请选择";
            this.txt遵医行为.Properties.PopupSizeable = false;
            this.txt遵医行为.Size = new System.Drawing.Size(254, 20);
            this.txt遵医行为.StyleController = this.Layout1;
            this.txt遵医行为.TabIndex = 103;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(97, 221);
            this.txt服药依从性.Margin = new System.Windows.Forms.Padding(0);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "请选择";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(177, 20);
            this.txt服药依从性.StyleController = this.Layout1;
            this.txt服药依从性.TabIndex = 111;
            this.txt服药依从性.EditValueChanged += new System.EventHandler(this.txt服药依从性_EditValueChanged);
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(411, -239);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Size = new System.Drawing.Size(295, 36);
            this.txt居住地址.StyleController = this.Layout1;
            this.txt居住地址.TabIndex = 132;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(97, 639);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Properties.ReadOnly = true;
            this.txt居民签名.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.txt居民签名.Size = new System.Drawing.Size(133, 56);
            this.txt居民签名.StyleController = this.Layout1;
            this.txt居民签名.TabIndex = 143;
            this.txt居民签名.TabStop = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "高血压患者随访记录表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem34,
            this.layoutControlItem49,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem24,
            this.emptySpaceItem3,
            this.layout添加药物,
            this.layout删除药物,
            this.layout药物列表,
            this.layoutControlItem28,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem21,
            this.layoutControlItem32,
            this.layoutControlItem30,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem19,
            this.layoutControlItem31,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem42,
            this.layoutControlItem41,
            this.layoutControlItem38,
            this.layout随访分类并发症,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem3,
            this.layoutControlItem27,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem43,
            this.layoutControlItem58,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layout添加药物调整,
            this.layout删除药物调整,
            this.layoutControlItem52,
            this.layout药物调整列表,
            this.layoutControlItem23,
            this.layoutControlItem50,
            this.lcl失访,
            this.lcl死亡,
            this.layoutControlItem55,
            this.layoutControlItem51,
            this.layoutControlItem53});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -360);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(712, 1097);
            this.layoutControlGroup1.Text = "高血压患者随访记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.fl随访分类;
            this.layoutControlItem1.CustomizationFormText = "此次随访分类";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 574);
            this.layoutControlItem1.Name = "flowLayoutPanel2item";
            this.layoutControlItem1.Size = new System.Drawing.Size(444, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "此次随访分类";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.fl症状;
            this.layoutControlItem5.CustomizationFormText = "症状";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 249);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(203, 54);
            this.layoutControlItem5.Name = "flowLayoutPanel3item";
            this.layoutControlItem5.Size = new System.Drawing.Size(704, 54);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "症状";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio随访方式;
            this.layoutControlItem6.CustomizationFormText = "随访方式";
            this.layoutControlItem6.Location = new System.Drawing.Point(193, 130);
            this.layoutControlItem6.Name = "radioGroup1item";
            this.layoutControlItem6.Size = new System.Drawing.Size(323, 29);
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "随访方式";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.radio不良反应;
            this.layoutControlItem20.CustomizationFormText = "药物不良反应";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 521);
            this.layoutControlItem20.Name = "radioGroup2item";
            this.layoutControlItem20.Size = new System.Drawing.Size(272, 29);
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "药物不良反应";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt服药依从性;
            this.layoutControlItem22.CustomizationFormText = "服药依从性";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 550);
            this.layoutControlItem22.Name = "comboBoxEdit2item";
            this.layoutControlItem22.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem22.Tag = "check";
            this.layoutControlItem22.Text = "用药依从性";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.lab创建人;
            this.layoutControlItem34.CustomizationFormText = "labelControl56item";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 1046);
            this.layoutControlItem34.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(129, 18);
            this.layoutControlItem34.Name = "labelControl56item";
            this.layoutControlItem34.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "录入人";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.lab考核项;
            this.layoutControlItem49.CustomizationFormText = "labelControl2item";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.Name = "labelControl2item";
            this.layoutControlItem49.Size = new System.Drawing.Size(704, 18);
            this.layoutControlItem49.Text = "labelControl2item";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextToControlDistance = 0;
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.radio用药情况;
            this.layoutControlItem4.CustomizationFormText = "用药情况";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 598);
            this.layoutControlItem4.Name = "radioGroup8item";
            this.layoutControlItem4.Size = new System.Drawing.Size(273, 29);
            this.layoutControlItem4.Text = "用药情况";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(272, 545);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(432, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt发生时间;
            this.layoutControlItem24.CustomizationFormText = "随访日期";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 130);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(193, 29);
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "随访日期";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(394, 598);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(310, 29);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layoutControlItem29";
            this.layout添加药物.Location = new System.Drawing.Point(273, 598);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(61, 29);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layoutControlItem31";
            this.layout删除药物.Location = new System.Drawing.Point(334, 598);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(60, 29);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = "                   ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 627);
            this.layout药物列表.MinSize = new System.Drawing.Size(193, 74);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(704, 74);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = "                   ";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.lbl创建时间;
            this.layoutControlItem28.CustomizationFormText = "labelControl62item";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 1028);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(230, 18);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(211, 18);
            this.layoutControlItem28.Name = "labelControl62item";
            this.layoutControlItem28.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "录入时间";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.lab最近修改人;
            this.layoutControlItem61.CustomizationFormText = "labelControl47item";
            this.layoutControlItem61.Location = new System.Drawing.Point(230, 1046);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(105, 18);
            this.layoutControlItem61.Name = "labelControl47item";
            this.layoutControlItem61.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "最近更新人";
            this.layoutControlItem61.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.lab当前所属机构;
            this.layoutControlItem62.CustomizationFormText = "labelControl52item";
            this.layoutControlItem62.Location = new System.Drawing.Point(460, 1046);
            this.layoutControlItem62.Name = "labelControl52item";
            this.layoutControlItem62.Size = new System.Drawing.Size(244, 18);
            this.layoutControlItem62.Text = "当前所属机构";
            this.layoutControlItem62.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt药物副作用详述;
            this.layoutControlItem21.CustomizationFormText = "副作用详述";
            this.layoutControlItem21.Location = new System.Drawing.Point(272, 521);
            this.layoutControlItem21.Name = "textEdit6item";
            this.layoutControlItem21.Size = new System.Drawing.Size(432, 24);
            this.layoutControlItem21.Tag = "";
            this.layoutControlItem21.Text = "副作用详述";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.lbl最近更新时间;
            this.layoutControlItem32.CustomizationFormText = "labelControl58item";
            this.layoutControlItem32.Location = new System.Drawing.Point(230, 1028);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(230, 220);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(215, 18);
            this.layoutControlItem32.Name = "labelControl58item";
            this.layoutControlItem32.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "最近更新时间";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.lab创建机构;
            this.layoutControlItem30.CustomizationFormText = "labelControl60item";
            this.layoutControlItem30.Location = new System.Drawing.Point(460, 1028);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(189, 18);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(141, 18);
            this.layoutControlItem30.Name = "labelControl60item";
            this.layoutControlItem30.Size = new System.Drawing.Size(244, 18);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "创建机构";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "体征";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 303);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(704, 73);
            this.layoutControlGroup2.Text = "体征";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt血压值;
            this.layoutControlItem7.CustomizationFormText = "血压";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "UCTxtLblTxtLblitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem7.Tag = "check";
            this.layoutControlItem7.Text = "血压";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt体重;
            this.layoutControlItem8.CustomizationFormText = "体重";
            this.layoutControlItem8.Location = new System.Drawing.Point(261, 0);
            this.layoutControlItem8.Name = "item0";
            this.layoutControlItem8.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "体重";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt身高;
            this.layoutControlItem9.CustomizationFormText = "身高";
            this.layoutControlItem9.Location = new System.Drawing.Point(468, 0);
            this.layoutControlItem9.Name = "UCTxtLblitem";
            this.layoutControlItem9.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem9.Tag = "";
            this.layoutControlItem9.Text = "身高";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt体质指数;
            this.layoutControlItem10.CustomizationFormText = "体质指数";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "item1";
            this.layoutControlItem10.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem10.Text = "体质指数";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt心率;
            this.layoutControlItem11.CustomizationFormText = "心率";
            this.layoutControlItem11.Location = new System.Drawing.Point(261, 24);
            this.layoutControlItem11.Name = "textEdit3item";
            this.layoutControlItem11.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "心率";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt体征其他;
            this.layoutControlItem12.CustomizationFormText = "其他";
            this.layoutControlItem12.Location = new System.Drawing.Point(468, 24);
            this.layoutControlItem12.Name = "textEdit2item";
            this.layoutControlItem12.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "其他";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "生活方式指导";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem29,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem2,
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 376);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(704, 121);
            this.layoutControlGroup3.Text = "生活方式指导";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt日吸烟量;
            this.layoutControlItem13.CustomizationFormText = "日吸烟量";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "item2";
            this.layoutControlItem13.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "日吸烟量";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt饮酒情况;
            this.layoutControlItem14.CustomizationFormText = "饮酒情况";
            this.layoutControlItem14.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem14.Name = "item3";
            this.layoutControlItem14.Size = new System.Drawing.Size(348, 24);
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "饮酒情况";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt运动频率;
            this.layoutControlItem15.CustomizationFormText = "运动频率";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "item4";
            this.layoutControlItem15.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "运动频率";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt持续时间;
            this.layoutControlItem16.CustomizationFormText = "每次持续时间";
            this.layoutControlItem16.Location = new System.Drawing.Point(350, 24);
            this.layoutControlItem16.Name = "item5";
            this.layoutControlItem16.Size = new System.Drawing.Size(348, 24);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "每次持续时间";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt摄盐情况1;
            this.layoutControlItem29.CustomizationFormText = "摄盐情况";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem29.Text = "摄盐情况";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt摄盐情况2;
            this.layoutControlItem17.CustomizationFormText = "/";
            this.layoutControlItem17.Location = new System.Drawing.Point(211, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(139, 24);
            this.layoutControlItem17.Text = "/";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(5, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt心理调整;
            this.layoutControlItem18.CustomizationFormText = "心理调整";
            this.layoutControlItem18.Location = new System.Drawing.Point(350, 48);
            this.layoutControlItem18.Name = "dateEdit3item";
            this.layoutControlItem18.Size = new System.Drawing.Size(348, 24);
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "心理调整";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt遵医行为;
            this.layoutControlItem2.CustomizationFormText = "遵医行为";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem2.Name = "comboBoxEdit1item";
            this.layoutControlItem2.Size = new System.Drawing.Size(349, 24);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "遵医行为";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(349, 72);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(349, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt辅助检查;
            this.layoutControlItem19.CustomizationFormText = "辅助检查";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 497);
            this.layoutControlItem19.Name = "textEdit5item";
            this.layoutControlItem19.Size = new System.Drawing.Size(704, 24);
            this.layoutControlItem19.Tag = "";
            this.layoutControlItem19.Text = "辅助检查";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt个人档案号;
            this.layoutControlItem31.CustomizationFormText = "个人档案号";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem31.Text = "个人档案号";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.txt姓名;
            this.layoutControlItem33.CustomizationFormText = "姓名";
            this.layoutControlItem33.Location = new System.Drawing.Point(311, 18);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem33.Text = "姓名";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt性别;
            this.layoutControlItem35.CustomizationFormText = "性别";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem35.Text = "性别";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.txt出生日期;
            this.layoutControlItem36.CustomizationFormText = "出生日期";
            this.layoutControlItem36.Location = new System.Drawing.Point(311, 42);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem36.Text = "出生日期";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.txt身份证号;
            this.layoutControlItem37.CustomizationFormText = "身份证号";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem37.Text = "身份证号";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt联系电话;
            this.layoutControlItem42.CustomizationFormText = "联系电话";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 9, 2);
            this.layoutControlItem42.Size = new System.Drawing.Size(311, 40);
            this.layoutControlItem42.Text = "联系电话";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt职业;
            this.layoutControlItem41.CustomizationFormText = "职业";
            this.layoutControlItem41.Location = new System.Drawing.Point(311, 66);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem41.Text = "职业";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt居住地址;
            this.layoutControlItem38.CustomizationFormText = "居住地址";
            this.layoutControlItem38.Location = new System.Drawing.Point(311, 90);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(103, 40);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(393, 40);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "居住地址";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layout随访分类并发症
            // 
            this.layout随访分类并发症.Control = this.fl随访分类并发症;
            this.layout随访分类并发症.CustomizationFormText = "此次随访分类并发症";
            this.layout随访分类并发症.Location = new System.Drawing.Point(444, 574);
            this.layout随访分类并发症.Name = "layout随访分类并发症";
            this.layout随访分类并发症.Size = new System.Drawing.Size(260, 24);
            this.layout随访分类并发症.Text = "此次随访分类并发症";
            this.layout随访分类并发症.TextSize = new System.Drawing.Size(0, 0);
            this.layout随访分类并发症.TextToControlDistance = 0;
            this.layout随访分类并发症.TextVisible = false;
            this.layout随访分类并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.radio转诊情况;
            this.layoutControlItem25.CustomizationFormText = "转诊情况";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 891);
            this.layoutControlItem25.Name = "radioGroup3item";
            this.layoutControlItem25.Size = new System.Drawing.Size(272, 29);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "转诊情况";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt转诊科别;
            this.layoutControlItem26.CustomizationFormText = "机构及科别";
            this.layoutControlItem26.Location = new System.Drawing.Point(460, 891);
            this.layoutControlItem26.Name = "textEdit4item";
            this.layoutControlItem26.Size = new System.Drawing.Size(244, 29);
            this.layoutControlItem26.Text = "机构及科别：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt下次随访时间;
            this.layoutControlItem3.CustomizationFormText = "下次随访时间";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 944);
            this.layoutControlItem3.Name = "dateEdit2item";
            this.layoutControlItem3.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "下次随访时间";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt医生签名;
            this.layoutControlItem27.CustomizationFormText = " 随访医生签名 ";
            this.layoutControlItem27.Location = new System.Drawing.Point(310, 944);
            this.layoutControlItem27.Name = "textEdit8item";
            this.layoutControlItem27.Size = new System.Drawing.Size(394, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = " 随访医生签名 ";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.txt随访方式其他;
            this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
            this.layoutControlItem39.Location = new System.Drawing.Point(516, 130);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(188, 29);
            this.layoutControlItem39.Text = "layoutControlItem39";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextToControlDistance = 0;
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.radio管理措施;
            this.layoutControlItem40.CustomizationFormText = "下一步管理措施";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 701);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(141, 60);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(527, 60);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Tag = "check";
            this.layoutControlItem40.Text = "下一步管理措施";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.radio药物调整;
            this.layoutControlItem43.CustomizationFormText = "用药调整意见";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 761);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(169, 30);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(273, 30);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Tag = "";
            this.layoutControlItem43.Text = "用药调整意见";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.txt转诊原因;
            this.layoutControlItem58.CustomizationFormText = "原因";
            this.layoutControlItem58.Location = new System.Drawing.Point(272, 891);
            this.layoutControlItem58.Name = "textEdit7item";
            this.layoutControlItem58.Size = new System.Drawing.Size(188, 29);
            this.layoutControlItem58.Text = "原因：";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.txt转诊联系人;
            this.layoutControlItem44.CustomizationFormText = "转诊联系人：";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 920);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem44.Text = "转诊联系人：";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.txt转诊联系电话;
            this.layoutControlItem45.CustomizationFormText = "转诊联系电话：";
            this.layoutControlItem45.Location = new System.Drawing.Point(188, 920);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem45.Text = "转诊联系电话：";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(85, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.txt居民签名;
            this.layoutControlItem46.CustomizationFormText = "居民签名";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 968);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(115, 60);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(228, 60);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Tag = "";
            this.layoutControlItem46.Text = "居民签名";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem47.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.txt备注;
            this.layoutControlItem47.CustomizationFormText = "备注";
            this.layoutControlItem47.Location = new System.Drawing.Point(310, 968);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(301, 60);
            this.layoutControlItem47.Tag = "";
            this.layoutControlItem47.Text = "备注";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.txt转诊结果;
            this.layoutControlItem48.CustomizationFormText = "转诊结果：";
            this.layoutControlItem48.Location = new System.Drawing.Point(462, 920);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(242, 24);
            this.layoutControlItem48.Text = "转诊结果：";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layout添加药物调整
            // 
            this.layout添加药物调整.Control = this.btn添加药物调整;
            this.layout添加药物调整.CustomizationFormText = "layout添加药物调整";
            this.layout添加药物调整.Location = new System.Drawing.Point(273, 761);
            this.layout添加药物调整.Name = "layout添加药物调整";
            this.layout添加药物调整.Size = new System.Drawing.Size(70, 30);
            this.layout添加药物调整.Text = "layout添加药物调整";
            this.layout添加药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物调整.TextToControlDistance = 0;
            this.layout添加药物调整.TextVisible = false;
            this.layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物调整
            // 
            this.layout删除药物调整.Control = this.btn删除药物调整;
            this.layout删除药物调整.CustomizationFormText = "layout删除药物调整";
            this.layout删除药物调整.Location = new System.Drawing.Point(343, 761);
            this.layout删除药物调整.Name = "layout删除药物调整";
            this.layout删除药物调整.Size = new System.Drawing.Size(67, 30);
            this.layout删除药物调整.Text = "layout删除药物调整";
            this.layout删除药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物调整.TextToControlDistance = 0;
            this.layout删除药物调整.TextVisible = false;
            this.layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.layoutControl1;
            this.layoutControlItem52.CustomizationFormText = "layoutControlItem52";
            this.layoutControlItem52.Location = new System.Drawing.Point(410, 761);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(294, 30);
            this.layoutControlItem52.Text = "layoutControlItem52";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem52.TextToControlDistance = 0;
            this.layoutControlItem52.TextVisible = false;
            // 
            // layout药物调整列表
            // 
            this.layout药物调整列表.Control = this.gc药物调整;
            this.layout药物调整列表.CustomizationFormText = "layout药物调整列表";
            this.layout药物调整列表.Location = new System.Drawing.Point(0, 791);
            this.layout药物调整列表.MinSize = new System.Drawing.Size(219, 70);
            this.layout药物调整列表.Name = "layout药物调整列表";
            this.layout药物调整列表.Size = new System.Drawing.Size(704, 70);
            this.layout药物调整列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物调整列表.Text = "layout药物调整列表";
            this.layout药物调整列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物调整列表.TextToControlDistance = 0;
            this.layout药物调整列表.TextVisible = false;
            this.layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8F);
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem23.Control = this.txt医生建议;
            this.layoutControlItem23.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 861);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(97, 30);
            this.layoutControlItem23.Name = "memoEdit1item";
            this.layoutControlItem23.Size = new System.Drawing.Size(704, 30);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "此次随访医生建议";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(86, 12);
            this.layoutControlItem23.TextToControlDistance = 5;
            this.layoutControlItem23.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.sbtnFingerPrint;
            this.layoutControlItem50.CustomizationFormText = "layoutControlItem50";
            this.layoutControlItem50.Location = new System.Drawing.Point(228, 968);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(82, 60);
            this.layoutControlItem50.Text = "layoutControlItem50";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem50.TextToControlDistance = 0;
            this.layoutControlItem50.TextVisible = false;
            // 
            // lcl失访
            // 
            this.lcl失访.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl失访.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl失访.Control = this.fl失访原因;
            this.lcl失访.CustomizationFormText = "失访原因：";
            this.lcl失访.Location = new System.Drawing.Point(0, 189);
            this.lcl失访.MinSize = new System.Drawing.Size(193, 30);
            this.lcl失访.Name = "lcl失访";
            this.lcl失访.Size = new System.Drawing.Size(704, 30);
            this.lcl失访.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl失访.Text = "失访原因：";
            this.lcl失访.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl失访.TextSize = new System.Drawing.Size(84, 20);
            this.lcl失访.TextToControlDistance = 5;
            // 
            // lcl死亡
            // 
            this.lcl死亡.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl死亡.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl死亡.Control = this.fl死亡信息;
            this.lcl死亡.CustomizationFormText = "死亡信息：";
            this.lcl死亡.Location = new System.Drawing.Point(0, 219);
            this.lcl死亡.MinSize = new System.Drawing.Size(193, 30);
            this.lcl死亡.Name = "lcl死亡";
            this.lcl死亡.Size = new System.Drawing.Size(704, 30);
            this.lcl死亡.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl死亡.Text = "死亡信息：";
            this.lcl死亡.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl死亡.TextSize = new System.Drawing.Size(84, 20);
            this.lcl死亡.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.fl访视情况;
            this.layoutControlItem55.CustomizationFormText = "访视情况：";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 159);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(193, 30);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(704, 30);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "访视情况：";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.sbtn查看下一步措施;
            this.layoutControlItem51.CustomizationFormText = "layoutControlItem51";
            this.layoutControlItem51.Location = new System.Drawing.Point(527, 701);
            this.layoutControlItem51.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(159, 40);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(177, 60);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "layoutControlItem51";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem51.TextToControlDistance = 0;
            this.layoutControlItem51.TextVisible = false;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.sbtn便捷录入备注;
            this.layoutControlItem53.CustomizationFormText = "layoutControlItem53";
            this.layoutControlItem53.Location = new System.Drawing.Point(611, 968);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(93, 60);
            this.layoutControlItem53.Text = "layoutControlItem53";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextToControlDistance = 0;
            this.layoutControlItem53.TextVisible = false;
            // 
            // UC高血压患者随访记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Layout1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC高血压患者随访记录表";
            this.Size = new System.Drawing.Size(729, 491);
            this.Load += new System.EventHandler(this.UC高血压患者随访记录表_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.Layout1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).EndInit();
            this.Layout1.ResumeLayout(false);
            this.fl访视情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).EndInit();
            this.fl死亡信息.ResumeLayout(false);
            this.fl死亡信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).EndInit();
            this.fl失访原因.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).EndInit();
            this.fl随访分类并发症.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            this.fl随访分类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk控制满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk控制不满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout随访分类并发症)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl死亡)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl Layout1;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类;
        private DevExpress.XtraEditors.CheckEdit chk控制满意;
        private DevExpress.XtraEditors.CheckEdit chk控制不满意;
        private DevExpress.XtraEditors.CheckEdit chk不良反应;
        private DevExpress.XtraEditors.CheckEdit ck并发症;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraEditors.CheckEdit check无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit ch症状其他;
        private DevExpress.XtraEditors.TextEdit txt症状其他;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private DevExpress.XtraEditors.TextEdit txt心率;
        private DevExpress.XtraEditors.TextEdit txt体征其他;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt运动频率;
        private Library.UserControls.UCTxtLblTxtLbl txt持续时间;
        private DevExpress.XtraEditors.TextEdit txt辅助检查;
        private DevExpress.XtraEditors.RadioGroup radio不良反应;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraEditors.RadioGroup radio转诊情况;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况2;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.LookUpEdit txt遵医行为;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类并发症;
        private DevExpress.XtraLayout.LayoutControlItem layout随访分类并发症;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private System.Windows.Forms.TextBox txt随访方式其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.LookUpEdit txt转诊结果;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private DevExpress.XtraEditors.TextEdit txt转诊联系电话;
        private DevExpress.XtraEditors.TextEdit txt转诊联系人;
        private DevExpress.XtraEditors.RadioGroup radio药物调整;
        private DevExpress.XtraEditors.RadioGroup radio管理措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.SimpleButton btn删除药物调整;
        private DevExpress.XtraEditors.SimpleButton btn添加药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraGrid.GridControl gc药物调整;
        private DevExpress.XtraGrid.Views.Grid.GridView gv药物调整;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraLayout.LayoutControlItem layout药物调整列表;
        private DevExpress.XtraEditors.PictureEdit txt居民签名;
        private DevExpress.XtraEditors.SimpleButton sbtnFingerPrint;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private System.Windows.Forms.FlowLayoutPanel fl失访原因;
        private DevExpress.XtraLayout.LayoutControlItem lcl失访;
        private System.Windows.Forms.FlowLayoutPanel fl死亡信息;
        private DevExpress.XtraLayout.LayoutControlItem lcl死亡;
        private DevExpress.XtraEditors.CheckEdit chk外出打工;
        private DevExpress.XtraEditors.CheckEdit chk迁居他处;
        private DevExpress.XtraEditors.CheckEdit chk走失;
        private DevExpress.XtraEditors.CheckEdit chk连续3次;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dte死亡日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt死亡原因;
        private System.Windows.Forms.FlowLayoutPanel fl访视情况;
        private DevExpress.XtraEditors.CheckEdit chk失访;
        private DevExpress.XtraEditors.CheckEdit chk死亡;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraEditors.SimpleButton sbtn查看下一步措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraEditors.SimpleButton sbtn便捷录入;
        private DevExpress.XtraEditors.SimpleButton sbtn便捷录入备注;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;

    }
}
