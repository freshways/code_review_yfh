﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC冠心病患者随访服务记录表_显示 : UserControlBaseNavBar
    {
        DataRow[] _dr个人档案信息 = null;
        string _创建日期 = "";
        string _ID = "";
        public UC冠心病患者随访服务记录表_显示()
        {
            InitializeComponent();
        }

        public UC冠心病患者随访服务记录表_显示(DataRow[] dr, object date)
        {
            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bllMXB冠心病随访表();
            InitializeComponent();
            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            this.txt身高.Txt1.Text = dr[0][tb_健康体检.身高].ToString();
        }

        private void UC冠心病患者随访服务记录表_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_MXB冠心病随访表.__TableName];

            CreateNavBarButton_new(dt缓存数据, tb_MXB冠心病随访表.发生时间);

            //已经有数据进行绑定
            if (dt缓存数据 != null && dt缓存数据.Rows.Count > 0)
            {
                if (_创建日期 != null && _创建日期 != "")
                {
                    DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);
                }
                else
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);

                SetItemColorToRed(_ID); //设置左侧当前随访日期颜色

                lab考核项.Text = string.Format("考核项：28     缺项：{0} 完整度：{1}% ",
                    _dt缓存数据.Rows[0][tb_MXB冠心病随访表.缺项].ToString(),
                    _dt缓存数据.Rows[0][tb_MXB冠心病随访表.完整度].ToString());
            }
            else
            {//打开新增页面
                btn新增.PerformClick();
            }
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB冠心病随访表_用药情况.__TableName];

            SetDetailEditorsAccessable(layoutControl1, false);
        }

        #region buttonClick
        private void btn新增_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Add;
            UC冠心病患者随访服务记录表 control = new UC冠心病患者随访服务记录表(_dr个人档案信息, _UpdateType, "",null);
            ShowControl(control);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC冠心病患者随访服务记录表 control = new UC冠心病患者随访服务记录表(_dr个人档案信息, _UpdateType, _ID,null);
                ShowControl(control);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(_ID))
                {
                    ((bllMXB冠心病随访表)BLL).Set个人健康特征(txt个人档案号.Text);
                    _创建日期 = "";
                    this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }            
        } 
        #endregion

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(Object objdataSource)
        {
            DataTable dataSource = null;
            if (objdataSource == null) return;
            if (objdataSource is DataRow)
            {
                dataSource = BLL.ConvertRowsToTable(objdataSource as DataRow);
            }
            _ID = dataSource.Rows[0][tb_MXB冠心病随访表.ID].ToString();

            DataBinder.BindingTextEdit(txt发生时间, dataSource, tb_MXB冠心病随访表.发生时间);
            //TextEdit 
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB冠心病随访表.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB冠心病随访表.舒张压);
            //DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB冠心病随访表.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB冠心病随访表.体重);
            DataBinder.BindingTextEdit(txt体重.Txt2, dataSource, tb_MXB冠心病随访表.体重2);
            DataBinder.BindingTextEdit(txt体质指数.Txt1, dataSource, tb_MXB冠心病随访表.D_BMI);
            DataBinder.BindingTextEdit(txt体质指数.Txt2, dataSource, tb_MXB冠心病随访表.D_BMI2);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB冠心病随访表.空腹血糖);
            DataBinder.BindingTextEdit(txt心率, dataSource, tb_MXB冠心病随访表.心率);
            //DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB冠心病随访表.高密度蛋白);
            //DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB冠心病随访表.低密度蛋白);
            //DataBinder.BindingTextEdit(txt甘油三脂.Txt1, dataSource, tb_MXB冠心病随访表.甘油三脂);
            //DataBinder.BindingTextEdit(txt总胆固醇.Txt1, dataSource, tb_MXB冠心病随访表.总胆固醇);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt1, dataSource, tb_MXB冠心病随访表.吸烟数量);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt2, dataSource, tb_MXB冠心病随访表.吸烟数量2);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt1, dataSource, tb_MXB冠心病随访表.饮酒数量);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt2, dataSource, tb_MXB冠心病随访表.饮酒数量2);
            DataBinder.BindingTextEdit(txt运动频率.Txt1, dataSource, tb_MXB冠心病随访表.运动频率);
            DataBinder.BindingTextEdit(txt运动频率.Txt2, dataSource, tb_MXB冠心病随访表.运动频率2);
            DataBinder.BindingTextEdit(txt每次持续时间.Txt1, dataSource, tb_MXB冠心病随访表.运动持续时间);
            DataBinder.BindingTextEdit(txt每次持续时间.Txt2, dataSource, tb_MXB冠心病随访表.运动持续时间2);

            DataBinder.BindingTextEdit(txt心电图检查, dataSource, tb_MXB冠心病随访表.心电图检查);
            DataBinder.BindingTextEdit(txt心电图运动负荷, dataSource, tb_MXB冠心病随访表.心电图运动负荷);
            DataBinder.BindingTextEdit(txt心脏彩超, dataSource, tb_MXB冠心病随访表.心脏彩超);
            DataBinder.BindingTextEdit(txt冠状动脉造影, dataSource, tb_MXB冠心病随访表.冠状动脉造影);
            DataBinder.BindingTextEdit(txt心肌酶学, dataSource, tb_MXB冠心病随访表.心肌酶学);
            DataBinder.BindingTextEdit(txt辅助检查, dataSource, tb_MXB冠心病随访表.辅助检查);
            DataBinder.BindingTextEdit(txt药物副作用详述, dataSource, tb_MXB冠心病随访表.副作用详述);
            
            DataBinder.BindingTextEdit(txt医生建议, dataSource, tb_MXB冠心病随访表.随访医生建议);
            DataBinder.BindingTextEdit(txt下次随访时间, dataSource, tb_MXB冠心病随访表.下次随访时间);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_MXB冠心病随访表.随访医生);

            this.txt随访方式.Text = BLL.ReturnDis字典显示("随访方式", dataSource.Rows[0][tb_MXB冠心病随访表.随访方式].ToString());
            //添加随访方式为其他的填空项
            if (this.txt随访方式.Text == "其他")
            {
                this.txt随访方式.Text = dataSource.Rows[0][tb_MXB冠心病随访表.随访方式其他].ToString();
            }
            this.txt冠心病类型.Text = SetFlowLayoutValues("gxblx", dataSource.Rows[0][tb_MXB冠心病随访表.冠心病类型].ToString());
            this.txt目前症状.Text = SetFlowLayoutValues("gxbmqzz", dataSource.Rows[0][tb_MXB冠心病随访表.目前症状].ToString());
            if (dataSource.Rows[0][tb_MXB冠心病随访表.目前症状其他].ToString() != "")
            { txt目前症状.Text += "(" + dataSource.Rows[0][tb_MXB冠心病随访表.目前症状其他].ToString() + ")"; }
            this.txt心理调整.Text = BLL.ReturnDis字典显示("lhybjc", dataSource.Rows[0][tb_MXB冠心病随访表.心理调整].ToString());
            this.txt摄盐情况1.Text = BLL.ReturnDis字典显示("qzz", dataSource.Rows[0][tb_MXB冠心病随访表.摄盐情况].ToString());
            this.txt摄盐情况2.Text = BLL.ReturnDis字典显示("qzz", dataSource.Rows[0][tb_MXB冠心病随访表.摄盐情况2].ToString());
            this.txt遵医行为.Text = BLL.ReturnDis字典显示("lhybjc", dataSource.Rows[0][tb_MXB冠心病随访表.遵医行为].ToString());
            this.txt服药依从性.Text = BLL.ReturnDis字典显示("fyycx-mb", dataSource.Rows[0][tb_MXB冠心病随访表.服药依从性].ToString());
            this.txt不良反应.Text = BLL.ReturnDis字典显示("wy_wuyou", dataSource.Rows[0][tb_MXB高血压随访表.药物副作用].ToString());
            this.txt用药情况.Text = BLL.ReturnDis字典显示("使用不使用", dataSource.Rows[0][tb_MXB冠心病随访表.降压药].ToString());
            if (txt用药情况.Text == "使用")
            { //处理用药列表，进行绑定
                _BLL.CurrentBusiness.Tables[tb_MXB冠心病随访表_用药情况.__TableName].DefaultView.RowFilter = "创建时间='" + dataSource.Rows[0][tb_MXB冠心病随访表.创建时间].ToString() + "'";
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            { //处理用药列表，进行绑定
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            this.txt特殊治疗.Text = SetFlowLayoutValues("gxbtszl", dataSource.Rows[0][tb_MXB冠心病随访表.特殊治疗].ToString());
            this.txt非药物治疗措施.Text = SetFlowLayoutValues("mqfywzl", dataSource.Rows[0][tb_MXB冠心病随访表.非药物治疗措施].ToString());
            this.txt此次随访分类.Text = SetFlowLayoutValues("sffl", dataSource.Rows[0][tb_MXB冠心病随访表.本次随访分类].ToString());
            if (dataSource.Rows[0][tb_MXB冠心病随访表.此次随访分类并发症].ToString() != "")
            { txt此次随访分类.Text += "(" + dataSource.Rows[0][tb_MXB冠心病随访表.此次随访分类并发症].ToString() + ")"; }

            //非编辑项            
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB冠心病随访表.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB冠心病随访表.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB冠心病随访表.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB冠心病随访表.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB冠心病随访表.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_MXB冠心病随访表.修改时间].ToString();

            //设置颜色
            Set考核项颜色_new(layoutControl1, null); //lab考核项
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.txt个人档案号.Text.Trim();
            string year = this.txt发生时间.Text.Trim();
            if(!string.IsNullOrEmpty(docNo) && !string.IsNullOrEmpty(year))
            {
                frm冠心病随访日期 frm冠心病 = new frm冠心病随访日期(docNo,year);//跳转到随访日期页面
                frm冠心病.ShowDialog();
            }
        }
    }
}
