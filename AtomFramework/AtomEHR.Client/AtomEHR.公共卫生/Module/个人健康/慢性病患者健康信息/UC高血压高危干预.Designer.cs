﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC高血压高危干预
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC高血压高危干预));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gc食物 = new DevExpress.XtraGrid.GridControl();
            this.gv食物 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lab当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.lab最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建人 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.lbl创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt家庭膳食3 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食2 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt运动2 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动1 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt摄盐量 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucLblTxtLbl14 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl15 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食1 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食11 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc食物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv食物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFocusForSave.Location = new System.Drawing.Point(0, 0);
            this.txtFocusForSave.Size = new System.Drawing.Size(815, 14);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(815, 28);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "取消";
            this.btn重置.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 2;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gc食物);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.txt家庭膳食3);
            this.layoutControl1.Controls.Add(this.txt家庭膳食2);
            this.layoutControl1.Controls.Add(this.txt摄盐情况2);
            this.layoutControl1.Controls.Add(this.txt摄盐情况1);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt运动2);
            this.layoutControl1.Controls.Add(this.txt运动1);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt摄盐量);
            this.layoutControl1.Controls.Add(this.ucLblTxtLbl14);
            this.layoutControl1.Controls.Add(this.ucLblTxtLbl15);
            this.layoutControl1.Controls.Add(this.txt家庭膳食1);
            this.layoutControl1.Controls.Add(this.txt家庭膳食11);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt随访日期);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1038, 489, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(815, 548);
            this.layoutControl1.TabIndex = 124;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gc食物
            // 
            this.gc食物.Location = new System.Drawing.Point(11, 300);
            this.gc食物.MainView = this.gv食物;
            this.gc食物.Name = "gc食物";
            this.gc食物.Size = new System.Drawing.Size(776, 150);
            this.gc食物.TabIndex = 147;
            this.gc食物.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv食物});
            // 
            // gv食物
            // 
            this.gv食物.ColumnPanelRowHeight = 35;
            this.gv食物.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gv食物.GridControl = this.gc食物;
            this.gv食物.Name = "gv食物";
            this.gv食物.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "食物名称";
            this.gridColumn1.DisplayFormat.FormatString = "[1-2]*";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn1.FieldName = "食物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 126;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "是否吃\r(1是2否)";
            this.gridColumn2.FieldName = "是否吃";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 108;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "次/天";
            this.gridColumn3.FieldName = "次_天";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 128;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "次/周";
            this.gridColumn4.FieldName = "次_周";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 128;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "次/月";
            this.gridColumn5.FieldName = "次_月";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 128;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "平均每次\r食用量(克)";
            this.gridColumn6.FieldName = "平均每次食用量";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 140;
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.Location = new System.Drawing.Point(635, 797);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Properties.ReadOnly = true;
            this.lab当前所属机构.Size = new System.Drawing.Size(158, 20);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 146;
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.Location = new System.Drawing.Point(364, 797);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Properties.ReadOnly = true;
            this.lab最近修改人.Size = new System.Drawing.Size(182, 20);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 145;
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(60, 797);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Properties.ReadOnly = true;
            this.lab创建人.Size = new System.Drawing.Size(215, 20);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 144;
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(635, 773);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Properties.ReadOnly = true;
            this.lab创建机构.Size = new System.Drawing.Size(158, 20);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 143;
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Location = new System.Drawing.Point(364, 773);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Properties.ReadOnly = true;
            this.lbl最近更新时间.Size = new System.Drawing.Size(182, 20);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 142;
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.Location = new System.Drawing.Point(60, 773);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Properties.ReadOnly = true;
            this.lbl创建时间.Size = new System.Drawing.Size(215, 20);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 141;
            // 
            // txt家庭膳食3
            // 
            this.txt家庭膳食3.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食3.Lbl1Text = "3.  过去的3个月，您家平均一个月吃";
            this.txt家庭膳食3.Lbl2Size = new System.Drawing.Size(46, 18);
            this.txt家庭膳食3.Lbl2Text = "克酱油？";
            this.txt家庭膳食3.Location = new System.Drawing.Point(13, 242);
            this.txt家庭膳食3.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食3.Name = "txt家庭膳食3";
            this.txt家庭膳食3.Size = new System.Drawing.Size(772, 20);
            this.txt家庭膳食3.TabIndex = 140;
            this.txt家庭膳食3.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食2
            // 
            this.txt家庭膳食2.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食2.Lbl1Text = "2.  过去的3个月，您家平均一个月吃";
            this.txt家庭膳食2.Lbl2Size = new System.Drawing.Size(46, 18);
            this.txt家庭膳食2.Lbl2Text = "克盐？";
            this.txt家庭膳食2.Location = new System.Drawing.Point(13, 218);
            this.txt家庭膳食2.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食2.Name = "txt家庭膳食2";
            this.txt家庭膳食2.Size = new System.Drawing.Size(772, 20);
            this.txt家庭膳食2.TabIndex = 139;
            this.txt家庭膳食2.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.EditValue = "";
            this.txt摄盐情况2.Location = new System.Drawing.Point(264, 655);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(84, 20);
            this.txt摄盐情况2.StyleController = this.layoutControl1;
            this.txt摄盐情况2.TabIndex = 138;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.EditValue = "";
            this.txt摄盐情况1.Location = new System.Drawing.Point(148, 655);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(100, 20);
            this.txt摄盐情况1.StyleController = this.layoutControl1;
            this.txt摄盐情况1.TabIndex = 137;
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "";
            this.txt心理调整.Location = new System.Drawing.Point(148, 703);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(100, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 136;
            // 
            // txt运动2
            // 
            this.txt运动2.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt运动2.Lbl1Text = "次/周";
            this.txt运动2.Lbl2Size = new System.Drawing.Size(41, 14);
            this.txt运动2.Lbl2Text = "分钟/次";
            this.txt运动2.Location = new System.Drawing.Point(149, 631);
            this.txt运动2.Name = "txt运动2";
            this.txt运动2.Size = new System.Drawing.Size(638, 20);
            this.txt运动2.TabIndex = 135;
            this.txt运动2.Txt1EditValue = null;
            this.txt运动2.Txt1Size = new System.Drawing.Size(58, 20);
            this.txt运动2.Txt2EditValue = null;
            this.txt运动2.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // txt运动1
            // 
            this.txt运动1.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt运动1.Lbl1Text = "次/周";
            this.txt运动1.Lbl2Size = new System.Drawing.Size(41, 14);
            this.txt运动1.Lbl2Text = "分钟/次";
            this.txt运动1.Location = new System.Drawing.Point(149, 607);
            this.txt运动1.Name = "txt运动1";
            this.txt运动1.Size = new System.Drawing.Size(638, 20);
            this.txt运动1.TabIndex = 134;
            this.txt运动1.Txt1EditValue = null;
            this.txt运动1.Txt1Size = new System.Drawing.Size(58, 20);
            this.txt运动1.Txt2EditValue = null;
            this.txt运动1.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(18, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(38, 14);
            this.txt饮酒情况.Lbl2Text = "(两)";
            this.txt饮酒情况.Location = new System.Drawing.Point(431, 583);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(356, 20);
            this.txt饮酒情况.TabIndex = 131;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(58, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt日吸烟量.Lbl1Text = "   /";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(38, 14);
            this.txt日吸烟量.Lbl2Text = "(支)";
            this.txt日吸烟量.Location = new System.Drawing.Point(148, 583);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(198, 20);
            this.txt日吸烟量.TabIndex = 130;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(58, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(18, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(51, 14);
            this.txt血压值.Lbl2Text = "(mmHg)";
            this.txt血压值.Location = new System.Drawing.Point(92, 527);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(695, 20);
            this.txt血压值.TabIndex = 129;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(58, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // txt摄盐量
            // 
            this.txt摄盐量.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt摄盐量.Lbl1Text = "克/天";
            this.txt摄盐量.Location = new System.Drawing.Point(148, 679);
            this.txt摄盐量.Name = "txt摄盐量";
            this.txt摄盐量.Size = new System.Drawing.Size(639, 20);
            this.txt摄盐量.TabIndex = 3;
            this.txt摄盐量.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucLblTxtLbl14
            // 
            this.ucLblTxtLbl14.Lbl1Size = new System.Drawing.Size(550, 18);
            this.ucLblTxtLbl14.Lbl1Text = "  过去的3个月，您家平均一个月吃";
            this.ucLblTxtLbl14.Lbl2Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl14.Lbl2Text = "克酱油？";
            this.ucLblTxtLbl14.Location = new System.Drawing.Point(127, -357);
            this.ucLblTxtLbl14.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl14.Name = "ucLblTxtLbl14";
            this.ucLblTxtLbl14.Size = new System.Drawing.Size(651, 20);
            this.ucLblTxtLbl14.TabIndex = 127;
            this.ucLblTxtLbl14.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxtLbl15
            // 
            this.ucLblTxtLbl15.Lbl1Size = new System.Drawing.Size(550, 18);
            this.ucLblTxtLbl15.Lbl1Text = "  过去的3个月，您家平均一个月吃";
            this.ucLblTxtLbl15.Lbl2Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl15.Lbl2Text = "克盐？";
            this.ucLblTxtLbl15.Location = new System.Drawing.Point(127, -357);
            this.ucLblTxtLbl15.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl15.Name = "ucLblTxtLbl15";
            this.ucLblTxtLbl15.Size = new System.Drawing.Size(651, 20);
            this.ucLblTxtLbl15.TabIndex = 128;
            this.ucLblTxtLbl15.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食1
            // 
            this.txt家庭膳食1.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食1.Lbl1Text = "1.  过去3个月内，常住在家中一起吃饭的人数？：";
            this.txt家庭膳食1.Lbl2Size = new System.Drawing.Size(350, 18);
            this.txt家庭膳食1.Lbl2Text = "（包括没有户籍但在您家居住的人，如亲戚、保姆等）";
            this.txt家庭膳食1.Location = new System.Drawing.Point(13, 170);
            this.txt家庭膳食1.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食1.Name = "txt家庭膳食1";
            this.txt家庭膳食1.Size = new System.Drawing.Size(772, 20);
            this.txt家庭膳食1.TabIndex = 125;
            this.txt家庭膳食1.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食11
            // 
            this.txt家庭膳食11.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食11.Lbl1Text = "1.1  其中一起吃饭的低于6岁的儿童有几人？";
            this.txt家庭膳食11.Lbl2Size = new System.Drawing.Size(50, 18);
            this.txt家庭膳食11.Lbl2Text = "";
            this.txt家庭膳食11.Location = new System.Drawing.Point(13, 194);
            this.txt家庭膳食11.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食11.Name = "txt家庭膳食11";
            this.txt家庭膳食11.Size = new System.Drawing.Size(772, 20);
            this.txt家庭膳食11.TabIndex = 126;
            this.txt家庭膳食11.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(495, 741);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(290, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 40;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(94, 110);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(481, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 14;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(459, 86);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(326, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 13;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(94, 86);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(280, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 12;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(459, 62);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(326, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 11;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(94, 62);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(280, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 10;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(459, 38);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(326, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(94, 38);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(280, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 4;
            // 
            // txt随访日期
            // 
            this.txt随访日期.EditValue = null;
            this.txt随访日期.Location = new System.Drawing.Point(92, 466);
            this.txt随访日期.Name = "txt随访日期";
            this.txt随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt随访日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt随访日期.Size = new System.Drawing.Size(305, 20);
            this.txt随访日期.StyleController = this.layoutControl1;
            this.txt随访日期.TabIndex = 8;
            // 
            // radio随访方式
            // 
            this.radio随访方式.EditValue = "";
            this.radio随访方式.Location = new System.Drawing.Point(482, 466);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话")});
            this.radio随访方式.Size = new System.Drawing.Size(305, 25);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 9;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(146, 741);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt下次随访时间.Size = new System.Drawing.Size(264, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 39;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup3,
            this.layoutControlGroup8,
            this.layoutControlItem36,
            this.layoutControlGroup7,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem42});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(798, 822);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "高血压高危人群干预调查与随访记录表";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(792, 137);
            this.layoutControlGroup2.Text = "高血压高危人群干预调查与随访记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt个人档案号;
            this.layoutControlItem1.CustomizationFormText = "个人档案编号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem1.Text = "个人档案编号";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt性别;
            this.layoutControlItem7.CustomizationFormText = "性别";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem7.Text = "性别";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem2.Text = "姓名";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt出生日期;
            this.layoutControlItem8.CustomizationFormText = "出生日期";
            this.layoutControlItem8.Location = new System.Drawing.Point(365, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem8.Text = "出生日期";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt身份证号;
            this.layoutControlItem9.CustomizationFormText = "身份证号";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem9.Text = "身份证号";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt联系电话;
            this.layoutControlItem10.CustomizationFormText = "联系电话";
            this.layoutControlItem10.Location = new System.Drawing.Point(365, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem10.Text = "联系电话";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt居住地址;
            this.layoutControlItem11.CustomizationFormText = "家庭住址";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(566, 24);
            this.layoutControlItem11.Text = "家庭住址";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(78, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(566, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(210, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 455);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup4.Size = new System.Drawing.Size(792, 41);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt随访日期;
            this.layoutControlItem5.CustomizationFormText = "随访日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(390, 29);
            this.layoutControlItem5.Text = "随访日期";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio随访方式;
            this.layoutControlItem6.CustomizationFormText = "随访方式";
            this.layoutControlItem6.Location = new System.Drawing.Point(390, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(390, 29);
            this.layoutControlItem6.Text = "随访方式";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup5.CustomizationFormText = "体征 ";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem41});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 496);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup5.Size = new System.Drawing.Size(792, 56);
            this.layoutControlGroup5.Text = "体征 ";
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt血压值;
            this.layoutControlItem41.CustomizationFormText = "血压";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(780, 24);
            this.layoutControlItem41.Text = "血压";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.BorderColor = System.Drawing.Color.Gray;
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.layoutControlGroup6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlGroup6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlGroup6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup6.CustomizationFormText = "生活方式";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem28,
            this.layoutControlItem24,
            this.layoutControlItem29,
            this.layoutControlItem23,
            this.layoutControlItem3,
            this.layoutControlItem26,
            this.emptySpaceItem4,
            this.layoutControlItem25,
            this.layoutControlItem27,
            this.emptySpaceItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 552);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup6.Size = new System.Drawing.Size(792, 176);
            this.layoutControlGroup6.Text = "生活方式";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseBorderColor = true;
            this.emptySpaceItem2.CustomizationFormText = "运动";
            this.emptySpaceItem2.Location = new System.Drawing.Point(28, 24);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(110, 24);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(110, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(110, 48);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "运动";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(78, 0);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AllowHtmlStringInCaption = true;
            this.emptySpaceItem3.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseBorderColor = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem3.CustomizationFormText = "生活随访方式指导";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(28, 144);
            this.emptySpaceItem3.Text = "生活随访方式指导";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(78, 0);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txt摄盐量;
            this.layoutControlItem28.CustomizationFormText = "食盐摄入量";
            this.layoutControlItem28.Location = new System.Drawing.Point(28, 96);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(752, 24);
            this.layoutControlItem28.Text = "食盐摄入量";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(104, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlItem24.Control = this.txt日吸烟量;
            this.layoutControlItem24.CustomizationFormText = "日吸烟量";
            this.layoutControlItem24.Location = new System.Drawing.Point(28, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem24.Text = "日吸烟量";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(104, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txt饮酒情况;
            this.layoutControlItem29.CustomizationFormText = "日饮酒量";
            this.layoutControlItem29.Location = new System.Drawing.Point(339, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItem29.Text = "日饮酒量";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txt运动1;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(138, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(642, 24);
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt运动2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(138, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(642, 24);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txt心理调整;
            this.layoutControlItem26.CustomizationFormText = "心理调整";
            this.layoutControlItem26.Location = new System.Drawing.Point(28, 120);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem26.Text = "心理调整";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(104, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(241, 120);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(539, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.txt摄盐情况1;
            this.layoutControlItem25.CustomizationFormText = "摄盐情况（咸淡）";
            this.layoutControlItem25.Location = new System.Drawing.Point(28, 72);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem25.Text = "摄盐情况（咸淡）";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(104, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txt摄盐情况2;
            this.layoutControlItem27.CustomizationFormText = "/";
            this.layoutControlItem27.Location = new System.Drawing.Point(241, 72);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem27.Text = "/";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(7, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(341, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(439, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "家庭膳食";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 137);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(792, 132);
            this.layoutControlGroup3.Text = "家庭膳食";
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem32.Control = this.txt家庭膳食1;
            this.layoutControlItem32.CustomizationFormText = "1.";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(776, 24);
            this.layoutControlItem32.Text = "1.  ";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem33.Control = this.txt家庭膳食11;
            this.layoutControlItem33.CustomizationFormText = "1.1";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(776, 24);
            this.layoutControlItem33.Text = "1.1  ";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem34.Control = this.txt家庭膳食2;
            this.layoutControlItem34.CustomizationFormText = "2.  ";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(776, 24);
            this.layoutControlItem34.Text = "2.  ";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem35.Control = this.txt家庭膳食3;
            this.layoutControlItem35.CustomizationFormText = "3.  ";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(776, 24);
            this.layoutControlItem35.Text = "3.  ";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "个人膳食：请回忆在过去3个月里，使用以下食物的次数和平均每次使用量（可根据当地情况酌情增加）";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 269);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup8.Size = new System.Drawing.Size(792, 186);
            this.layoutControlGroup8.Text = "个人膳食：请回忆在过去3个月里，使用以下食物的次数和平均每次使用量（可根据当地情况酌情增加）";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gc食物;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(212, 154);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(780, 154);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.lbl创建时间;
            this.layoutControlItem36.CustomizationFormText = "录入时间";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 768);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem36.Text = "录入时间";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem30,
            this.layoutControlItem31});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 728);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(792, 40);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt下次随访时间;
            this.layoutControlItem30.CustomizationFormText = "下次随访时间";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(401, 24);
            this.layoutControlItem30.Text = "下次随访时间";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(128, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt医生签名;
            this.layoutControlItem31.CustomizationFormText = "随访医生签名";
            this.layoutControlItem31.Location = new System.Drawing.Point(401, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem31.Text = "随访医生签名";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.lbl最近更新时间;
            this.layoutControlItem37.CustomizationFormText = "最近更新时间";
            this.layoutControlItem37.Location = new System.Drawing.Point(274, 768);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem37.Text = "最近更新时间";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.lab创建机构;
            this.layoutControlItem38.CustomizationFormText = "创建机构";
            this.layoutControlItem38.Location = new System.Drawing.Point(545, 768);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem38.Text = "创建机构";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.lab创建人;
            this.layoutControlItem39.CustomizationFormText = "录入人";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 792);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem39.Text = "录入人";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.lab最近修改人;
            this.layoutControlItem40.CustomizationFormText = "最近更新人";
            this.layoutControlItem40.Location = new System.Drawing.Point(274, 792);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem40.Text = "最近更新人";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.lab当前所属机构;
            this.layoutControlItem42.CustomizationFormText = "当前所属机构";
            this.layoutControlItem42.Location = new System.Drawing.Point(545, 792);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem42.Text = "当前所属机构";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.CustomizationFormText = "日吸烟量";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "item2";
            this.layoutControlItem14.Size = new System.Drawing.Size(197, 30);
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "日吸烟量";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup12.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(205, 56);
            this.layoutControlGroup12.Text = "layoutControlGroup12";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(205, 38);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            // 
            // UC高血压高危干预
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC高血压高危干预";
            this.Size = new System.Drawing.Size(815, 576);
            this.Load += new System.EventHandler(this.UC高血压高危干预_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc食物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv食物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.DateEdit txt随访日期;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private Library.UserControls.UCTxtLbl txt摄盐量;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl14;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食1;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private Library.UserControls.UCTxtLblTxtLbl txt运动1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private Library.UserControls.UCTxtLblTxtLbl txt运动2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况2;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食3;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraEditors.TextEdit lab当前所属机构;
        private DevExpress.XtraEditors.TextEdit lab最近修改人;
        private DevExpress.XtraEditors.TextEdit lab创建人;
        private DevExpress.XtraEditors.TextEdit lab创建机构;
        private DevExpress.XtraEditors.TextEdit lbl最近更新时间;
        private DevExpress.XtraEditors.TextEdit lbl创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraGrid.GridControl gc食物;
        private DevExpress.XtraGrid.Views.Grid.GridView gv食物;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
    }
}
