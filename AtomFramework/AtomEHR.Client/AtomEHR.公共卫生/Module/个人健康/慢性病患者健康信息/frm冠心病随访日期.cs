﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;


namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class frm冠心病随访日期 : Form
    {
        #region Fields
        private string year;
        private string docNo;
        DataSet _ds冠心病;
        bllMXB冠心病随访表 _bll冠心病 = new bllMXB冠心病随访表();
        #endregion

        public frm冠心病随访日期(string docNo, string year)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.year = year;
            _ds冠心病 = _bll冠心病.GetInfoByDate(docNo, year, true);
            DoBindingDataSource(_ds冠心病);
        }

        private void DoBindingDataSource(DataSet _ds冠心病)
        {
            DataTable dt冠心病 = _ds冠心病.Tables[Models.tb_MXB冠心病随访表.__TableName];
            if (dt冠心病 != null && dt冠心病.Rows.Count > 0)
            {
                gc冠心病随访.DataSource = _ds冠心病.Tables[0];
                this.gv冠心病随访.BestFitColumns();
                this.gc冠心病随访.BringToFront();
                //在表中增加一个列 add（列名，列类型）  
                dt冠心病.Columns.Add("check", typeof(bool));
                dt冠心病.Columns.Add("col随访日期", typeof(string));
                //给每一行的该列赋值  
                for (int i = 0; i < dt冠心病.Rows.Count; i++)
                {
                    dt冠心病.Rows[i]["check"] = "false";
                    dt冠心病.Rows[i]["col随访日期"] = dt冠心病.Rows[i][Models.tb_MXB冠心病随访表.发生时间].ToString();
                }

                string value = "";
                string strSelected = "";
                for (int i = 0; i < gv冠心病随访.RowCount; i++)
                {
                    //获取选中行的check值
                    value = gv冠心病随访.GetDataRow(i)["check"].ToString();
                    if (value == "true")
                    {
                        strSelected += gv冠心病随访.GetRowCellValue(i, "随访日期");
                    }
                }
            }
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (gv冠心病随访.SelectedRowsCount==0)
            {
                MessageBox.Show("请选择随访日期！","错误提示");
            }
             else if (gv冠心病随访.SelectedRowsCount < 5)
            {
                int[] ints = this.gv冠心病随访.GetSelectedRows();
                List<string> list = new List<string>();
                for (int i = 0; i < ints.Length; i++)
                {
                    DataRowView row = (DataRowView)this.gv冠心病随访.GetRow(ints[i]);
                    string selectDate;
                    if (row != null)
                    {
                        selectDate = row["发生时间"].ToString();
                        list.Add(selectDate);
                    }
                }
                report冠心病患者随访服务记录表 report = new report冠心病患者随访服务记录表(docNo, list);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("最多选择4次随访记录！","错误提示");
            }

        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();//关闭窗体
        }
    }
}
