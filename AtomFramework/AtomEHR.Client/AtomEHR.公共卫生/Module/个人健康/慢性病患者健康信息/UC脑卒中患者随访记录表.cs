﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC脑卒中患者随访记录表 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        string _ID = "";
        public UC脑卒中患者随访记录表()
        {
            InitializeComponent();
        }

        public UC脑卒中患者随访记录表(DataRow[] dr, UpdateType _UpdateType, object ID, DataRow dr慢性病)
        {
            base._UpdateType = _UpdateType;
            _dr个人档案信息 = dr;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bllMXB脑卒中随访表();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            this.txt身高.Txt1.Text = dr[0][tb_健康体检.身高].ToString();
        }

        private void UC脑卒中患者随访记录表_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.个人档案编号] = _dr个人档案信息[0][tb_健康档案.个人档案编号].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.姓名] = util.DESEncrypt.DES解密(_dr个人档案信息[0][tb_健康档案.姓名].ToString());
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.性别] = _dr个人档案信息[0][tb_健康档案.性别].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.身份证号] = _dr个人档案信息[0][tb_健康档案.身份证号].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.出生日期] = _dr个人档案信息[0][tb_健康档案.出生日期].ToString();
                //绑定联系电话
                string str联系电话 = _dr个人档案信息[0][tb_健康档案.本人电话].ToString();
                if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
                {
                    _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.联系电话] = _dr个人档案信息[0][tb_健康档案.本人电话].ToString();
                }
                else
                {
                    _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.联系电话] = _dr个人档案信息[0][tb_健康档案.联系人电话].ToString();
                }
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.省] = _dr个人档案信息[0][tb_健康档案.省].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.市] = _dr个人档案信息[0][tb_健康档案.市].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.区] = _dr个人档案信息[0][tb_健康档案.区].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.街道] = _dr个人档案信息[0][tb_健康档案.街道].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.居委会] = _dr个人档案信息[0][tb_健康档案.居委会].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.居住地址] = _dr个人档案信息[0][tb_健康档案.居住地址].ToString();
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.职业] = _dr个人档案信息[0][tb_健康档案.职业].ToString();

                //判断绑定的身高是否为空(当体检表中的身高为空时报错)
                if (string.IsNullOrEmpty(_dr个人档案信息[0][tb_健康体检.身高].ToString()))
                {
                    _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.身高] = 0.00;
                }
                else
                {
                    _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.身高] = _dr个人档案信息[0][tb_健康体检.身高].ToString();
                }

                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.身高] = _dr个人档案信息[0][tb_健康体检.身高].ToString();
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                if (_ID != null && _ID != "")
                {
                    ((bllMXB脑卒中随访表)_BLL).GetBusinessByKeyEdit(_ID, true);
                }
                else return;
            }

            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_MXB脑卒中随访表.__TableName]);
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB脑卒中随访表_用药情况.__TableName];

            //初始化
            Init();
            //设置颜色    
            if (_UpdateType == UpdateType.Modify)
                Set考核项颜色_new(layoutControl1, lab考核项);        
        }

        #region 初始化

        void Init() //初始化
        {
            DataBinder.BindingLookupEditDataSource(txt脑卒中部位, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='nzzbw' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt肢体功能恢复情况, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='nzzzthf' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt心理调整, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='lhybjc' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt摄盐情况1, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='qzz' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt摄盐情况2, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='qzz' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt遵医行为, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='lhybjc' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt服药依从性, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='fyycx-mb' ")), "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt生活自理能力, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='glkshzl' ")), "P_DESC", "P_CODE");
            
            btn添加药物.Click += btn添加药物_Click;
            btn删除药物.Click += btn删除药物_Click;

            this.txt身高.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt2.Leave += 身高_Leave;
            this.txt体质指数.Txt1.Enter += BMI_Enter;
            this.txt体质指数.Txt2.Enter += BMI_Enter;
        }

        void 身高_Leave(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        void BMI_Enter(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txt体质指数.Txt1);
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt2.Text, txt体质指数.Txt2);
        }

        #endregion

        #region 保存相关
        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (!Msg.AskQuestion("信息保存后，‘随访日期’将不允许修改，确认保存信息？")) return;
            if (_UpdateType == UpdateType.None) return;

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            //设置颜色
            Set考核项颜色_new(layoutControl1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.发生时间] = this.txt发生时间.Text;
            //根据用户要求，下次随访时间默认“发生时间”的3个月以后
            if (string.IsNullOrEmpty(txt下次随访时间.Text))
            {
                string str发生时间 = this.txt发生时间.Text;
                this.txt下次随访时间.Text = Convert.ToDateTime(str发生时间).AddMonths(3).ToShortDateString();
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.下次随访时间] = this.txt下次随访时间.Text;
            }
            else
            {
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.下次随访时间] = this.txt下次随访时间.Text;
            }
            //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.下次随访时间] = this.txt下次随访时间.Text;
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.目前症状] = GetFlowLayoutResult(this.fl症状);
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.个人病史] = GetFlowLayoutResult(this.fl个人病史);
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.并发症情况] = GetFlowLayoutResult(this.fl并发症情况);
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.新发卒中症状] = GetFlowLayoutResult(this.fl新发卒中症状);
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.康复治疗方式] = GetFlowLayoutResult(this.fl康复治疗方式);
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.本次随访分类] = GetFlowLayoutResult(this.fl随访分类);
            //添加随访方式为其他的填空项
            if (_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.随访方式].ToString() == "99")
            {
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.随访方式其他] = this.txt随访方式其他.Text;
            }
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.完整度] = _base完整度;

            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人
                ((bllMXB脑卒中随访表)BLL).Set个人健康特征(txt个人档案号.Text);

                Msg.ShowInformation("保存成功!");

                if (_UpdateType == UpdateType.Add) //新增随访时添加判断
                {
                    if (((bllMXB脑卒中随访表)BLL).Check高血压随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为高血压患者，若录入高血压随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC高血压患者随访记录表 ctl高血压 = new UC高血压患者随访记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl高血压, DockStyle.Fill);
                            return;
                        }
                    }
                    if (((bllMXB脑卒中随访表)BLL).Check糖尿病随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为糖尿病患者，若录入糖尿病随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC糖尿病患者随访记录表 ctl糖尿病 = new UC糖尿病患者随访记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl糖尿病, DockStyle.Fill);
                            return;
                        }
                    }
                    if (((bllMXB脑卒中随访表)BLL).Check冠心病随访添加(this.txt个人档案号.Text, this.txt发生时间.Text))
                    {
                        if (Msg.AskQuestion("该居民为冠心病患者，若录入冠心病随访记录，请点击[是]，否则点击[否]！"))
                        {
                            UC冠心病患者随访服务记录表 ctl冠心病 = new UC冠心病患者随访服务记录表(_dr个人档案信息, _UpdateType, "", _BLL.DataBinder.Rows[0]);
                            ShowControl(ctl冠心病, DockStyle.Fill);
                            return;
                        }
                    }
                }


                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面
                UC脑卒中患者随访记录表_显示 control = new UC脑卒中患者随访记录表_显示(_dr个人档案信息, this.lbl创建时间.Text);
                ShowControl(control, DockStyle.Fill);
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt发生时间.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                txt发生时间.Focus();
                return false;
            }
            #region  允许修改下次随访时间(设置默认值)
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt下次随访时间.Text)))
            //{
            //    Msg.Warning("下次随访日期不能为空!");
            //    txt下次随访时间.Focus();
            //    return false;
            //}
            #endregion

            if (this.txt发生时间.DateTime > Convert.ToDateTime(lbl创建时间.Text))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                txt发生时间.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            dataSource.Rows[0][tb_MXB脑卒中随访表.个人档案编号] = txt个人档案号.Text;

            DataBinder.BindingTextEditDateTime(txt发生时间, dataSource, tb_MXB脑卒中随访表.发生时间);
            //TextEdit
            DataBinder.BindingTextEdit(txt目前症状其他, dataSource, tb_MXB脑卒中随访表.目前症状其他);
            DataBinder.BindingTextEdit(txt并发症其他, dataSource, tb_MXB脑卒中随访表.并发症情况其他);
            DataBinder.BindingTextEdit(txt新发卒中症状其他, dataSource, tb_MXB脑卒中随访表.新发卒中症状其他);

            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB脑卒中随访表.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB脑卒中随访表.舒张压);
            //DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB脑卒中随访表.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB脑卒中随访表.体重);
            DataBinder.BindingTextEdit(txt体重.Txt2, dataSource, tb_MXB脑卒中随访表.体重2);
            DataBinder.BindingTextEdit(txt体质指数.Txt1, dataSource, tb_MXB脑卒中随访表.D_BMI);
            DataBinder.BindingTextEdit(txt体质指数.Txt2, dataSource, tb_MXB脑卒中随访表.D_BMI2);
            DataBinder.BindingTextEdit(txt腰围.Txt1, dataSource, tb_MXB脑卒中随访表.腰围);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB脑卒中随访表.空腹血糖);
            DataBinder.BindingTextEdit(txt肢体功能恢复情况, dataSource, tb_MXB脑卒中随访表.肢体功能恢复情况);

            DataBinder.BindingTextEdit(txt日吸烟量.Txt1, dataSource, tb_MXB脑卒中随访表.吸烟数量);
            DataBinder.BindingTextEdit(txt日吸烟量.Txt2, dataSource, tb_MXB脑卒中随访表.吸烟数量2);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt1, dataSource, tb_MXB脑卒中随访表.饮酒数量);
            DataBinder.BindingTextEdit(txt饮酒情况.Txt2, dataSource, tb_MXB脑卒中随访表.饮酒数量2);
            DataBinder.BindingTextEdit(txt运动频率.Txt1, dataSource, tb_MXB脑卒中随访表.运动频率);
            DataBinder.BindingTextEdit(txt运动频率.Txt2, dataSource, tb_MXB脑卒中随访表.运动频率2);
            DataBinder.BindingTextEdit(txt每次持续时间.Txt1, dataSource, tb_MXB脑卒中随访表.运动持续时间);
            DataBinder.BindingTextEdit(txt每次持续时间.Txt2, dataSource, tb_MXB脑卒中随访表.运动持续时间2);
            DataBinder.BindingTextEdit(txt辅助检查, dataSource, tb_MXB脑卒中随访表.辅助检查);
            DataBinder.BindingTextEdit(txt药物副作用详述, dataSource, tb_MXB脑卒中随访表.副作用详述);
            DataBinder.BindingTextEdit(txt随访分类并发症, dataSource, tb_MXB脑卒中随访表.本次随访分类并发症);
            DataBinder.BindingTextEdit(txt康复治疗方式其他, dataSource, tb_MXB脑卒中随访表.康复治疗方式其他);
            DataBinder.BindingTextEdit(txt转诊科别, dataSource, tb_MXB脑卒中随访表.转诊科别);
            DataBinder.BindingTextEdit(txt转诊原因, dataSource, tb_MXB脑卒中随访表.转诊原因);

            DataBinder.BindingTextEdit(txt医生建议, dataSource, tb_MXB脑卒中随访表.随访医生建议);
            DataBinder.BindingTextEditDateTime(txt下次随访时间, dataSource, tb_MXB脑卒中随访表.下次随访时间);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_MXB脑卒中随访表.随访医生);

            //RadioEdit
            DataBinder.BindingRadioEdit(radio随访方式, dataSource, tb_MXB脑卒中随访表.随访方式);
            DataBinder.BindingRadioEdit(radio脑卒中类型, dataSource, tb_MXB脑卒中随访表.脑卒中类型);
            DataBinder.BindingRadioEdit(radio不良反应, dataSource, tb_MXB脑卒中随访表.药物副作用);
            DataBinder.BindingRadioEdit(radio用药情况, dataSource, tb_MXB脑卒中随访表.降压药);
            DataBinder.BindingRadioEdit(radio转诊情况, dataSource, tb_MXB脑卒中随访表.转诊情况);

            //lookupedit                
            DataBinder.BindingTextEdit(txt脑卒中部位, dataSource, tb_MXB脑卒中随访表.脑卒中部位);
            DataBinder.BindingTextEdit(txt摄盐情况1, dataSource, tb_MXB脑卒中随访表.摄盐情况);
            DataBinder.BindingTextEdit(txt摄盐情况2, dataSource, tb_MXB脑卒中随访表.摄盐情况2);
            DataBinder.BindingTextEdit(txt心理调整, dataSource, tb_MXB脑卒中随访表.心理调整);
            DataBinder.BindingTextEdit(txt遵医行为, dataSource, tb_MXB脑卒中随访表.遵医行为);
            DataBinder.BindingTextEdit(txt服药依从性, dataSource, tb_MXB脑卒中随访表.服药依从性);
            DataBinder.BindingTextEdit(txt生活自理能力, dataSource, tb_MXB脑卒中随访表.生活自理能力);

            //flowLayoutPanel
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.目前症状].ToString(), fl症状);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.个人病史].ToString(), fl个人病史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.并发症情况].ToString(), fl并发症情况);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.新发卒中症状].ToString(), fl新发卒中症状);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.康复治疗方式].ToString(), fl康复治疗方式);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB脑卒中随访表.本次随访分类].ToString(), fl随访分类);

            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB脑卒中随访表.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB脑卒中随访表.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB脑卒中随访表.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_MXB脑卒中随访表.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB脑卒中随访表.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_MXB脑卒中随访表.修改时间].ToString();
        }   
        #endregion

        #region 用药情况
        private void btn添加药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物.Tag, gcDetail);
        }

        private void btn删除药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物.Tag, gcDetail);
        }

        protected override void CreateOneDetail(GridView gridView)
        {
            gvDetail.MoveLast();

            DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB脑卒中随访表_用药情况.__TableName].NewRow();
            //添加用法说明默认值
            row[tb_MXB脑卒中随访表_用药情况.用法] = "每日  次，每次  mg  （口服）";
            row[tb_MXB脑卒中随访表_用药情况.个人档案编号] = this.txt个人档案号.Text;
            row[tb_MXB脑卒中随访表_用药情况.创建时间] = lbl创建时间.Text;

            _BLL.CurrentBusiness.Tables[tb_MXB脑卒中随访表_用药情况.__TableName].Rows.Add(row); //增加一条明细记录

            gcDetail.RefreshDataSource();
            gvDetail.FocusedRowHandle = gvDetail.RowCount - 1;

            gvDetail.FocusedColumn = gvDetail.VisibleColumns[0];
        }

        #endregion

        #region 页面控制

        private void radio用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio用药情况.EditValue.ToString() == "1")
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                if (_BLL.CurrentBusiness.Tables[tb_MXB脑卒中随访表_用药情况.__TableName].Rows.Count <= 0)
                    btn添加药物.PerformClick();
            }
            else
            {
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void check无症状_CheckedChanged(object sender, EventArgs e)
        {
            if (check无症状.Checked)
            {
                SetFlCheckEnabled(fl症状, false, check无症状.Text);
                SetFlChecked(fl症状, false, check无症状.Text);
            }
            else
                SetFlCheckEnabled(fl症状, true, check无症状.Text);
        }

        private void ch个人病史无_CheckedChanged(object sender, EventArgs e)
        {
            if (ch个人病史无.Checked)
            {
                SetFlCheckEnabled(fl个人病史, false, ch个人病史无.Text);
                SetFlChecked(fl个人病史, false, ch个人病史无.Text);
            }
            else
                SetFlCheckEnabled(fl个人病史, true, ch个人病史无.Text);
        }

        private void ch并发症无_CheckedChanged(object sender, EventArgs e)
        {
            if (ch并发症无.Checked)
            {
                SetFlCheckEnabled(fl并发症情况, false, ch并发症无.Text);
                SetFlChecked(fl并发症情况, false, ch并发症无.Text);
            }
            else
                SetFlCheckEnabled(fl并发症情况, true, ch并发症无.Text);
        }

        private void ch新发卒中症状无_CheckedChanged(object sender, EventArgs e)
        {
            if (ch新发卒中症状无.Checked)
            {
                SetFlCheckEnabled(fl新发卒中症状, false, ch新发卒中症状无.Text);
                SetFlChecked(fl新发卒中症状, false, ch新发卒中症状无.Text);
            }
            else
                SetFlCheckEnabled(fl新发卒中症状, true, ch新发卒中症状无.Text);
        }

        private void ch康复治疗方式无_CheckedChanged(object sender, EventArgs e)
        {
            if (ch康复治疗方式无.Checked)
            {
                SetFlCheckEnabled(fl康复治疗方式, false, ch康复治疗方式无.Text);
                SetFlChecked(fl康复治疗方式, false, ch康复治疗方式无.Text);
            }
            else
                SetFlCheckEnabled(fl康复治疗方式, true, ch新发卒中症状无.Text);
        }
        
        private void ch目前症状其他_CheckedChanged(object sender, EventArgs e)
        {
            if (ch目前症状其他.Checked)
                this.txt目前症状其他.Visible = true;
            else
            {
                this.txt目前症状其他.Visible = false;
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.目前症状其他] = "";
            }
        }

        private void ch并发症其他_CheckedChanged(object sender, EventArgs e)
        {
            if (ch并发症其他.Checked)
                this.txt并发症其他.Visible = true;
            else
            {
                this.txt并发症其他.Visible = false;
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.并发症情况其他] = "";
            }
        }

        private void ch康复治疗方式其他_CheckedChanged(object sender, EventArgs e)
        {
            if (ch康复治疗方式其他.Checked)
                this.txt康复治疗方式其他.Visible = true;
            else
            {
                this.txt康复治疗方式其他.Visible = false;
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.康复治疗方式其他] = "";
            }
        }

        private void ck随访并发症_CheckedChanged(object sender, EventArgs e)
        {
            if (ck随访并发症.Checked)
                this.txt随访分类并发症.Visible = true;
            else
            {
                this.txt随访分类并发症.Visible = false;
                _BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.本次随访分类并发症] = "";
            }
        }

        private void SetFlCheckEnabled(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Enabled = value;
                }
            }
        }

        private void SetFlChecked(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Checked = value;
                }
            }
        }

        private void txt服药依从性_EditValueChanged(object sender, EventArgs e)
        {
            if (txt服药依从性.EditValue.ToString() != "3")
                this.radio用药情况.EditValue = "1"; //_BLL.DataBinder.Rows[0][tb_MXB冠心病随访表.降压药] = "1";
            else
                this.radio用药情况.EditValue = "2"; //_BLL.DataBinder.Rows[0][tb_MXB冠心病随访表.降压药] = "2";
        }

        private void ch控制满意_CheckedChanged(object sender, EventArgs e)
        {
            string 点击名称 = ((DevExpress.XtraEditors.CheckEdit)sender).Text;
            if (ch控制满意.Text == 点击名称)
            {
                if (ch控制满意.Checked)
                {
                    ch控制满意.CheckedChanged -= ch控制满意_CheckedChanged;
                    ch控制不满意.Enabled = false;
                    ch控制不满意.Checked = false;
                    ch控制满意.CheckedChanged += ch控制满意_CheckedChanged;
                }
                else
                    ch控制不满意.Enabled = true;
            }

            if (ch控制不满意.Text == 点击名称)
            {
                if (ch控制不满意.Checked)
                {
                    ch控制不满意.CheckedChanged -= ch控制满意_CheckedChanged;
                    ch控制满意.Enabled = false;
                    ch控制满意.Checked = false;
                    ch控制不满意.CheckedChanged += ch控制满意_CheckedChanged;
                }
                else
                    ch控制满意.Enabled = true;
            }
        }
        
        private void radio不良反应_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio不良反应.EditValue!=null && radio不良反应.EditValue.ToString() == "2")
                this.txt药物副作用详述.Enabled = true;
            else
            {
                this.txt药物副作用详述.Enabled = false;
                this.txt药物副作用详述.Text = "";
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.副作用详述] = "";
            }
        }

        private void radio转诊情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio转诊情况.EditValue!=null && radio转诊情况.EditValue.ToString() == "1")
            {
                this.txt转诊科别.Enabled = true;
                this.txt转诊原因.Enabled = true;
            }
            else
            {
                this.txt转诊科别.Enabled = false;
                this.txt转诊原因.Enabled = false;
                this.txt转诊科别.Text = "";
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.转诊科别] = "";
                this.txt转诊原因.Text = "";
                //_BLL.DataBinder.Rows[0][tb_MXB脑卒中随访表.转诊原因] = "";
            }
        }

        #endregion  

        //Begin WXF 2018-11-28 | 15:30
        //推后随访日期三个月为下次随访时间 
        private void txt发生时间_EditValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt下次随访时间.Text) && !string.IsNullOrEmpty(txt发生时间.Text))
            {
                txt下次随访时间.Text = Convert.ToDateTime(txt发生时间.Text).AddMonths(+3).ToString("yyyy-MM-dd");
            }
        }
        //End

    }
}
