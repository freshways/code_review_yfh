﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC高血压患者管理卡_显示 : UserControlBase
    {
        DataRow[] _dr个人信息 = null;

        public UC高血压患者管理卡_显示()
        {
            InitializeComponent();
        }

        public UC高血压患者管理卡_显示(DataRow[] dr)
        {
            _dr个人信息 = dr;
            _BLL = new bllMXB高血压管理卡();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txtEKG检查结果.Text = "无";//默认为“无”
        }


        private void UC高血压患者管理卡_Load(object sender, EventArgs e)
        {

            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_MXB高血压管理卡.__TableName];

            if (_dt缓存数据 != null && _dt缓存数据.Rows.Count > 0)
            {
                DoBindingSummaryEditor(_dt缓存数据);
            }

            //DevStyle.SetGridControlLayout(gcDetail, true);//表格设置   
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB高血压管理卡_用药情况.__TableName];

            //设置控件不可用
            SetDetailEditorsAccessable(panel1, false);

            Set考核项颜色_new(this.Layout1, labelControl2);
        }


        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC高血压患者管理卡 uc = new UC高血压患者管理卡(_dr个人信息, _UpdateType);
                ShowControl(uc);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("删除管理卡时，会将相关的随访记录删除，是否继续进行？")) return;

            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(txt个人档案号.Text))
                {
                    Msg.ShowInformation("删除成功！");
                    _BLL.WriteLog(txt个人档案号.Text, tb_MXB高血压管理卡.__TableName, this.txt姓名.Text + "&" + txt发生时间.Text);
                    UC个人基本信息表_显示 uc = new UC个人基本信息表_显示(个人健康.Common.frm个人健康);
                    ShowControl(uc);
                    //this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }       
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            DataBinder.BindingTextEdit(txt管理卡编号, dataSource, tb_MXB高血压管理卡.管理卡编号);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB高血压管理卡.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB高血压管理卡.体重);
            DataBinder.BindingTextEdit(txtBMI.Txt1, dataSource, tb_MXB高血压管理卡.BMI);
            DataBinder.BindingTextEdit(txt腰围.Txt1, dataSource, tb_MXB高血压管理卡.腰围);
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB高血压管理卡.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB高血压管理卡.舒张压);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB高血压管理卡.空腹血糖);
            DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB高血压管理卡.高密度蛋白);
            DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB高血压管理卡.低密度蛋白);
            DataBinder.BindingTextEdit(txt甘油三脂.Txt1, dataSource, tb_MXB高血压管理卡.甘油三脂);
            DataBinder.BindingTextEdit(txt胆固醇.Txt1, dataSource, tb_MXB高血压管理卡.总胆固醇);
            //DataBinder.BindingTextEdit(txtEKG检查结果, dataSource, tb_MXB高血压管理卡.EKG);
            DataBinder.BindingTextEdit(txt体检日期, dataSource, tb_MXB高血压管理卡.体检时间);
            DataBinder.BindingTextEdit(txt终止管理日期, dataSource, tb_MXB高血压管理卡.终止管理日期);
            DataBinder.BindingTextEdit(txt发生时间, dataSource, tb_MXB高血压管理卡.发生时间);
            
            this.txt管理组别.Text = BLL.ReturnDis字典显示("高血压管理组别",dataSource.Rows[0][tb_MXB高血压管理卡.管理组别].ToString());
            //关联分级管理
            switch (dataSource.Rows[0][tb_MXB高血压管理卡.管理组别].ToString())
            {
                case "1":
                    this.txt分级管理.Text = "三级";
                    break;
                case "2":
                    this.txt分级管理.Text = "二级";
                    break;
                case "3":
                    this.txt分级管理.Text = "一级";
                    break;
                default:
                    break;
            }
            this.txt病例来源.Text = BLL.ReturnDis字典显示("高血压病例来源", dataSource.Rows[0][tb_MXB高血压管理卡.病例来源].ToString());
            this.txt家族史.Text = SetFlowLayoutValues("jzs_mb", dataSource.Rows[0][tb_MXB高血压管理卡.家族史].ToString());

            this.txt目前症状.Text = SetFlowLayoutValues("高血压目前症状", dataSource.Rows[0][tb_MXB高血压管理卡.目前症状].ToString());
            if (dataSource.Rows[0][tb_MXB高血压管理卡.目前症状其他].ToString() != "") 
            { txt目前症状.Text += "(" + dataSource.Rows[0][tb_MXB高血压管理卡.目前症状其他].ToString() + ")"; }

            this.txt高血压并发症情况.Text = SetFlowLayoutValues("高血压并发症", dataSource.Rows[0][tb_MXB高血压管理卡.并发症情况].ToString());
            if (dataSource.Rows[0][tb_MXB高血压管理卡.并发症情况其他].ToString() != "") 
            { txt高血压并发症情况.Text += "(" + dataSource.Rows[0][tb_MXB高血压管理卡.并发症情况其他].ToString() + ")"; }
                
            this.txt降压药.Text = BLL.ReturnDis字典显示("使用不使用", dataSource.Rows[0][tb_MXB高血压管理卡.降压药].ToString());
            if (txt降压药.Text == "使用")
            { //处理用药列表，进行绑定
                _BLL.CurrentBusiness.Tables[tb_MXB高血压管理卡_用药情况.__TableName].DefaultView.RowFilter = "创建时间='" + dataSource.Rows[0][tb_MXB高血压管理卡.创建时间].ToString() + "'";
                lay药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            { //处理用药列表，进行绑定
                lay药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            this.txt吸烟情况.Text = BLL.ReturnDis字典显示("管理卡吸烟情况", dataSource.Rows[0][tb_MXB高血压管理卡.吸烟情况].ToString());
            this.txt饮酒情况.Text = BLL.ReturnDis字典显示("管理卡饮酒情况", dataSource.Rows[0][tb_MXB高血压管理卡.饮酒情况].ToString());
            this.txt体育锻炼.Text = BLL.ReturnDis字典显示("管理卡体育锻炼", dataSource.Rows[0][tb_MXB高血压管理卡.运动频率].ToString());
            this.txt生活自理能力.Text = BLL.ReturnDis字典显示("管理卡生活自理能力", dataSource.Rows[0][tb_MXB高血压管理卡.生活自理能力].ToString());
            this.txt终止管理.Text = BLL.ReturnDis字典显示("sf_shifou", dataSource.Rows[0][tb_MXB高血压管理卡.终止管理].ToString());
            this.txt终止理由.Text = BLL.ReturnDis字典显示("终止理由", dataSource.Rows[0][tb_MXB高血压管理卡.终止理由].ToString());

            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB高血压管理卡.所属机构].ToString()); 
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB高血压管理卡.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB高血压管理卡.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB高血压管理卡.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB高血压管理卡.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_MXB高血压管理卡.修改时间].ToString();

        }


    }
}
