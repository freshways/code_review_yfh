﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraGrid.Views.Grid;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC脑卒中患者管理卡_显示 : UserControlBase
    {
        DataRow[] _dr个人信息 = null;

        public UC脑卒中患者管理卡_显示()
        {
            InitializeComponent();
        }

        public UC脑卒中患者管理卡_显示(DataRow[] dr)
        {
            _dr个人信息 = dr;
            _BLL = new bllMXB脑卒中管理卡();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.lab当前所属机构.Text = dr[0][tb_健康档案.所属机构].ToString(); 
        }

        private void UC脑卒中患者管理卡_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_MXB脑卒中管理卡.__TableName];

            if (_dt缓存数据 != null && _dt缓存数据.Rows.Count > 0)
            {
                DoBindingSummaryEditor(_dt缓存数据);
            }
            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB脑卒中管理卡_用药情况.__TableName];
            SetDetailEditorsAccessable(layoutControl1, false);

            base.Set考核项颜色_new(layoutControl1, this.lab考核项);
        }
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            //TextEdit
            DataBinder.BindingTextEdit(txt管理卡编号, dataSource, tb_MXB脑卒中管理卡.管理卡编号);
            DataBinder.BindingTextEdit(txt发病时间, dataSource, tb_MXB脑卒中管理卡.发病时间);
            DataBinder.BindingTextEdit(txt诊断医院, dataSource, tb_MXB脑卒中管理卡.诊断医院);
            DataBinder.BindingTextEdit(txtmRS评分, dataSource, tb_MXB脑卒中管理卡.MRS评分);
            DataBinder.BindingTextEdit(txtCT检查, dataSource, tb_MXB脑卒中管理卡.CT);
            DataBinder.BindingTextEdit(txtMRI检查, dataSource, tb_MXB脑卒中管理卡.MRI);
            DataBinder.BindingTextEdit(txt脑卒中分类, dataSource, tb_MXB脑卒中管理卡.脑卒中分类);
            DataBinder.BindingTextEdit(txt脑卒中部位, dataSource, tb_MXB脑卒中管理卡.脑卒中部位);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB脑卒中管理卡.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB脑卒中管理卡.体重);
            DataBinder.BindingTextEdit(txtBMI.Txt1, dataSource, tb_MXB脑卒中管理卡.BMI);
            DataBinder.BindingTextEdit(txt腰围.Txt1, dataSource, tb_MXB脑卒中管理卡.腰围);
            DataBinder.BindingTextEdit(txt血压.Txt1, dataSource, tb_MXB脑卒中管理卡.收缩压);
            DataBinder.BindingTextEdit(txt血压.Txt2, dataSource, tb_MXB脑卒中管理卡.舒张压);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB脑卒中管理卡.空腹血糖);
            DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB脑卒中管理卡.高密度蛋白);
            DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB脑卒中管理卡.低密度蛋白);
            DataBinder.BindingTextEdit(txt胆固醇.Txt1, dataSource, tb_MXB脑卒中管理卡.总胆固醇);
            DataBinder.BindingTextEditDateTime(txt体检日期, dataSource, tb_MXB脑卒中管理卡.体检时间);
            DataBinder.BindingTextEditDateTime(txt终止管理日期, dataSource, tb_MXB脑卒中管理卡.终止管理日期);
            DataBinder.BindingTextEditDateTime(txt发生时间, dataSource, tb_MXB脑卒中管理卡.发生时间);
            this.txt服药依从性.Text = BLL.ReturnDis字典显示("fyycx-mb", dataSource.Rows[0][tb_MXB脑卒中管理卡.服药依从性].ToString());

            //RadioEdit
            this.txt管理组别.Text = BLL.ReturnDis字典显示("脑卒中管理组别", dataSource.Rows[0][tb_MXB脑卒中管理卡.管理组别].ToString());
            this.txt病例来源.Text = BLL.ReturnDis字典显示("脑卒中病例来源", dataSource.Rows[0][tb_MXB脑卒中管理卡.病例来源].ToString());
            this.txt生活自理能力.Text = BLL.ReturnDis字典显示("管理卡生活自理能力", dataSource.Rows[0][tb_MXB脑卒中管理卡.生活自理能力].ToString());
            this.txt用药情况.Text = BLL.ReturnDis字典显示("使用不使用", dataSource.Rows[0][tb_MXB脑卒中管理卡.G_JYY].ToString());
            if (txt用药情况.Text == "使用")
            { //处理用药列表，进行绑定
                _BLL.CurrentBusiness.Tables[tb_MXB脑卒中管理卡_用药情况.__TableName].DefaultView.RowFilter = "创建时间='" + dataSource.Rows[0][tb_MXB脑卒中管理卡.创建时间].ToString() + "'";
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            { //处理用药列表，进行绑定
                layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            //flowLayoutPanel
            this.txt家族史.Text = SetFlowLayoutValues("jzs_mb", dataSource.Rows[0][tb_MXB脑卒中管理卡.家族史].ToString());
            this.txt目前症状.Text = SetFlowLayoutValues("脑卒中症状", dataSource.Rows[0][tb_MXB脑卒中管理卡.就诊时症状].ToString());
            if (dataSource.Rows[0][tb_MXB脑卒中管理卡.就诊时症状].ToString() != "") 
            { txt目前症状.Text += "(" + dataSource.Rows[0][tb_MXB脑卒中管理卡.就诊时症状].ToString() + ")"; }
            this.txt危险因素.Text = SetFlowLayoutValues("脑卒中危险因素", dataSource.Rows[0][tb_MXB脑卒中管理卡.危险因素].ToString());
            this.txt特殊治疗.Text = SetFlowLayoutValues("冠心病特殊治疗", dataSource.Rows[0][tb_MXB脑卒中管理卡.特殊治疗].ToString());
            this.txt非药物治疗措施.Text = SetFlowLayoutValues("目前的非药物治疗措施", dataSource.Rows[0][tb_MXB脑卒中管理卡.非药物治疗措施].ToString());
            
            this.txt终止管理.Text = BLL.ReturnDis字典显示("sf_shifou", dataSource.Rows[0][tb_MXB脑卒中管理卡.终止管理].ToString());
            this.txt终止理由.Text = BLL.ReturnDis字典显示("终止理由", dataSource.Rows[0][tb_MXB脑卒中管理卡.终止理由].ToString());

            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB脑卒中管理卡.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB脑卒中管理卡.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB脑卒中管理卡.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB脑卒中管理卡.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_MXB脑卒中管理卡.修改时间].ToString();
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;

                UC脑卒中患者管理卡 uc = new UC脑卒中患者管理卡(_dr个人信息, _UpdateType);
                ShowControl(uc);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("删除管理卡时，会将相关的随访记录删除，是否继续进行？")) return;

            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(txt个人档案号.Text))
                {
                    Msg.ShowInformation("删除成功！");
                    _BLL.WriteLog(txt个人档案号.Text, tb_MXB脑卒中管理卡.__TableName, this.txt姓名.Text + "&" + txt发生时间.Text);
                    UC个人基本信息表_显示 uc = new UC个人基本信息表_显示(个人健康.Common.frm个人健康);
                    ShowControl(uc);
                    //this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }     
        }

    }
}
