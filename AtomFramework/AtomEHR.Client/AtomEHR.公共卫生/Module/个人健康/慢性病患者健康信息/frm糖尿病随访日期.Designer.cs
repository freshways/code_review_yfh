﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class frm糖尿病随访日期
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn确定 = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gc糖尿病随访 = new DevExpress.XtraGrid.GridControl();
            this.gv糖尿病随访 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col随访日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gc糖尿病随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv糖尿病随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn确定
            // 
            this.btn确定.Location = new System.Drawing.Point(43, 303);
            this.btn确定.Name = "btn确定";
            this.btn确定.Size = new System.Drawing.Size(80, 30);
            this.btn确定.TabIndex = 2;
            this.btn确定.Text = "确定";
            this.btn确定.Click += new System.EventHandler(this.btn确定_Click);
            // 
            // btn取消
            // 
            this.btn取消.Location = new System.Drawing.Point(230, 303);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(80, 30);
            this.btn取消.TabIndex = 3;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(331, 14);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "请选择需要导出的随访记录日期（最多只能选择4次随访记录）";
            // 
            // gc糖尿病随访
            // 
            this.gc糖尿病随访.Location = new System.Drawing.Point(12, 32);
            this.gc糖尿病随访.MainView = this.gv糖尿病随访;
            this.gc糖尿病随访.Name = "gc糖尿病随访";
            this.gc糖尿病随访.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2});
            this.gc糖尿病随访.Size = new System.Drawing.Size(363, 251);
            this.gc糖尿病随访.TabIndex = 6;
            this.gc糖尿病随访.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv糖尿病随访});
            // 
            // gv糖尿病随访
            // 
            this.gv糖尿病随访.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col随访日期});
            this.gv糖尿病随访.GridControl = this.gc糖尿病随访;
            this.gv糖尿病随访.Name = "gv糖尿病随访";
            this.gv糖尿病随访.OptionsSelection.MultiSelect = true;
            this.gv糖尿病随访.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv糖尿病随访.OptionsView.ShowGroupedColumns = true;
            this.gv糖尿病随访.OptionsView.ShowGroupPanel = false;
            // 
            // col随访日期
            // 
            this.col随访日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col随访日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col随访日期.Caption = "随访日期";
            this.col随访日期.FieldName = "col随访日期";
            this.col随访日期.Name = "col随访日期";
            this.col随访日期.Visible = true;
            this.col随访日期.VisibleIndex = 1;
            this.col随访日期.Width = 774;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // frm高血压随访日期
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 345);
            this.Controls.Add(this.gc糖尿病随访);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btn取消);
            this.Controls.Add(this.btn确定);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frm高血压随访日期";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "糖尿病随访日期";
            ((System.ComponentModel.ISupportInitialize)(this.gc糖尿病随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv糖尿病随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn确定;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gc糖尿病随访;
        private DevExpress.XtraGrid.Views.Grid.GridView gv糖尿病随访;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn col随访日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;





    }
}