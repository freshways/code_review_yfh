﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC糖尿病患者随访记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC糖尿病患者随访记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.sbtn便捷录入备注 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn查看近期下一步 = new DevExpress.XtraEditors.SimpleButton();
            this.fl失访原因 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk外出打工 = new DevExpress.XtraEditors.CheckEdit();
            this.chk迁居他处 = new DevExpress.XtraEditors.CheckEdit();
            this.chk走失 = new DevExpress.XtraEditors.CheckEdit();
            this.chk连续3次 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.fl访视情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk失访 = new DevExpress.XtraEditors.CheckEdit();
            this.chk死亡 = new DevExpress.XtraEditors.CheckEdit();
            this.fl死亡信息 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dte死亡日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt死亡原因 = new DevExpress.XtraEditors.TextEdit();
            this.sbtnFingerPrint = new DevExpress.XtraEditors.SimpleButton();
            this.gc药物调整 = new DevExpress.XtraGrid.GridControl();
            this.gv药物调整 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊结果 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt胰岛素用法用量 = new DevExpress.XtraEditors.TextEdit();
            this.txt胰岛素种类 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系人 = new DevExpress.XtraEditors.TextEdit();
            this.txt调整胰岛素用法用量 = new DevExpress.XtraEditors.TextEdit();
            this.txt调整胰岛素种类 = new DevExpress.XtraEditors.TextEdit();
            this.btn删除药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.radio药物调整 = new DevExpress.XtraEditors.RadioGroup();
            this.radio管理措施 = new DevExpress.XtraEditors.RadioGroup();
            this.txt随访方式其他 = new System.Windows.Forms.TextBox();
            this.fl随访分类并发症 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.radio转诊情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.fl随访分类 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit控制满意 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit控制不满意 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit不良反应 = new DevExpress.XtraEditors.CheckEdit();
            this.ck随访并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.radio不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.txt餐后2h血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt随机血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt检查日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt糖化血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt主食 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体征其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.check无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit多饮 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit多食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit多尿 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.ch症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.sbtn便捷录入症状 = new DevExpress.XtraEditors.SimpleButton();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.txt低血糖反应 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt遵医行为 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt足背动脉搏动 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt居民签名 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout随访分类并发症 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl管理措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物调整列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.fl失访原因.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            this.fl访视情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).BeginInit();
            this.fl死亡信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素用法用量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素种类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素用法用量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素种类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).BeginInit();
            this.fl随访分类并发症.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            this.fl随访分类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit控制满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit控制不满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck随访并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多饮.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多尿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt低血糖反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout随访分类并发症)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl管理措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(63, 23);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(72, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(63, 23);
            this.btn重置.TabIndex = 0;
            this.btn重置.Text = "重置";
            this.btn重置.Visible = false;
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(141, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 1;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.sbtn便捷录入备注);
            this.layoutControl1.Controls.Add(this.sbtn查看近期下一步);
            this.layoutControl1.Controls.Add(this.fl失访原因);
            this.layoutControl1.Controls.Add(this.fl访视情况);
            this.layoutControl1.Controls.Add(this.fl死亡信息);
            this.layoutControl1.Controls.Add(this.sbtnFingerPrint);
            this.layoutControl1.Controls.Add(this.gc药物调整);
            this.layoutControl1.Controls.Add(this.txt备注);
            this.layoutControl1.Controls.Add(this.txt转诊结果);
            this.layoutControl1.Controls.Add(this.txt胰岛素用法用量);
            this.layoutControl1.Controls.Add(this.txt胰岛素种类);
            this.layoutControl1.Controls.Add(this.txt转诊联系电话);
            this.layoutControl1.Controls.Add(this.txt转诊联系人);
            this.layoutControl1.Controls.Add(this.txt调整胰岛素用法用量);
            this.layoutControl1.Controls.Add(this.txt调整胰岛素种类);
            this.layoutControl1.Controls.Add(this.btn删除药物调整);
            this.layoutControl1.Controls.Add(this.btn添加药物调整);
            this.layoutControl1.Controls.Add(this.radio药物调整);
            this.layoutControl1.Controls.Add(this.radio管理措施);
            this.layoutControl1.Controls.Add(this.txt随访方式其他);
            this.layoutControl1.Controls.Add(this.fl随访分类并发症);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.btn删除药物);
            this.layoutControl1.Controls.Add(this.btn添加药物);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab最近更新时间);
            this.layoutControl1.Controls.Add(this.lab创建时间);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.txt转诊科别);
            this.layoutControl1.Controls.Add(this.radio转诊情况);
            this.layoutControl1.Controls.Add(this.radio用药情况);
            this.layoutControl1.Controls.Add(this.fl随访分类);
            this.layoutControl1.Controls.Add(this.txt药物副作用详述);
            this.layoutControl1.Controls.Add(this.radio不良反应);
            this.layoutControl1.Controls.Add(this.txt餐后2h血糖);
            this.layoutControl1.Controls.Add(this.txt随机血糖);
            this.layoutControl1.Controls.Add(this.txt检查日期);
            this.layoutControl1.Controls.Add(this.txt糖化血红蛋白);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt主食);
            this.layoutControl1.Controls.Add(this.txt持续时间);
            this.layoutControl1.Controls.Add(this.txt运动频率);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt体征其他);
            this.layoutControl1.Controls.Add(this.txt体质指数);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.fl症状);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt医生建议);
            this.layoutControl1.Controls.Add(this.txt低血糖反应);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt遵医行为);
            this.layoutControl1.Controls.Add(this.txt足背动脉搏动);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt居民签名);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(616, 268, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 473);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // sbtn便捷录入备注
            // 
            this.sbtn便捷录入备注.Location = new System.Drawing.Point(609, 1113);
            this.sbtn便捷录入备注.Name = "sbtn便捷录入备注";
            this.sbtn便捷录入备注.Size = new System.Drawing.Size(118, 22);
            this.sbtn便捷录入备注.StyleController = this.layoutControl1;
            this.sbtn便捷录入备注.TabIndex = 157;
            this.sbtn便捷录入备注.Text = "便捷录入备注";
            this.sbtn便捷录入备注.Click += new System.EventHandler(this.sbtn便捷录入备注_Click);
            // 
            // sbtn查看近期下一步
            // 
            this.sbtn查看近期下一步.Location = new System.Drawing.Point(579, 822);
            this.sbtn查看近期下一步.Name = "sbtn查看近期下一步";
            this.sbtn查看近期下一步.Size = new System.Drawing.Size(148, 36);
            this.sbtn查看近期下一步.StyleController = this.layoutControl1;
            this.sbtn查看近期下一步.TabIndex = 156;
            this.sbtn查看近期下一步.Text = "查看近期下一步管理措施";
            this.sbtn查看近期下一步.Click += new System.EventHandler(this.sbtn查看近期下一步_Click);
            // 
            // fl失访原因
            // 
            this.fl失访原因.Controls.Add(this.chk外出打工);
            this.fl失访原因.Controls.Add(this.chk迁居他处);
            this.fl失访原因.Controls.Add(this.chk走失);
            this.fl失访原因.Controls.Add(this.chk连续3次);
            this.fl失访原因.Controls.Add(this.chk其他);
            this.fl失访原因.Enabled = false;
            this.fl失访原因.Location = new System.Drawing.Point(98, 215);
            this.fl失访原因.Name = "fl失访原因";
            this.fl失访原因.Size = new System.Drawing.Size(632, 26);
            this.fl失访原因.TabIndex = 152;
            // 
            // chk外出打工
            // 
            this.chk外出打工.Location = new System.Drawing.Point(3, 3);
            this.chk外出打工.Name = "chk外出打工";
            this.chk外出打工.Properties.Caption = "外出打工";
            this.chk外出打工.Size = new System.Drawing.Size(75, 19);
            this.chk外出打工.TabIndex = 0;
            this.chk外出打工.Tag = "1";
            // 
            // chk迁居他处
            // 
            this.chk迁居他处.Location = new System.Drawing.Point(84, 3);
            this.chk迁居他处.Name = "chk迁居他处";
            this.chk迁居他处.Properties.Caption = "迁居他处";
            this.chk迁居他处.Size = new System.Drawing.Size(75, 19);
            this.chk迁居他处.TabIndex = 0;
            this.chk迁居他处.Tag = "2";
            // 
            // chk走失
            // 
            this.chk走失.Location = new System.Drawing.Point(165, 3);
            this.chk走失.Name = "chk走失";
            this.chk走失.Properties.Caption = "走失";
            this.chk走失.Size = new System.Drawing.Size(62, 19);
            this.chk走失.TabIndex = 0;
            this.chk走失.Tag = "3";
            // 
            // chk连续3次
            // 
            this.chk连续3次.Location = new System.Drawing.Point(233, 3);
            this.chk连续3次.Name = "chk连续3次";
            this.chk连续3次.Properties.Caption = "连续3次未到访";
            this.chk连续3次.Size = new System.Drawing.Size(114, 19);
            this.chk连续3次.TabIndex = 0;
            this.chk连续3次.Tag = "4";
            // 
            // chk其他
            // 
            this.chk其他.Location = new System.Drawing.Point(353, 3);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Caption = "其他";
            this.chk其他.Size = new System.Drawing.Size(76, 19);
            this.chk其他.TabIndex = 0;
            this.chk其他.Tag = "5";
            // 
            // fl访视情况
            // 
            this.fl访视情况.Controls.Add(this.chk失访);
            this.fl访视情况.Controls.Add(this.chk死亡);
            this.fl访视情况.Location = new System.Drawing.Point(98, 185);
            this.fl访视情况.Name = "fl访视情况";
            this.fl访视情况.Size = new System.Drawing.Size(632, 26);
            this.fl访视情况.TabIndex = 155;
            // 
            // chk失访
            // 
            this.chk失访.Location = new System.Drawing.Point(3, 3);
            this.chk失访.Name = "chk失访";
            this.chk失访.Properties.Caption = "失访";
            this.chk失访.Size = new System.Drawing.Size(61, 19);
            this.chk失访.TabIndex = 1;
            this.chk失访.Tag = "1";
            this.chk失访.CheckedChanged += new System.EventHandler(this.chk失访_CheckedChanged);
            // 
            // chk死亡
            // 
            this.chk死亡.Location = new System.Drawing.Point(70, 3);
            this.chk死亡.Name = "chk死亡";
            this.chk死亡.Properties.Caption = "死亡";
            this.chk死亡.Size = new System.Drawing.Size(61, 19);
            this.chk死亡.TabIndex = 2;
            this.chk死亡.Tag = "2";
            this.chk死亡.CheckedChanged += new System.EventHandler(this.chk死亡_CheckedChanged);
            // 
            // fl死亡信息
            // 
            this.fl死亡信息.Controls.Add(this.labelControl1);
            this.fl死亡信息.Controls.Add(this.dte死亡日期);
            this.fl死亡信息.Controls.Add(this.labelControl2);
            this.fl死亡信息.Controls.Add(this.txt死亡原因);
            this.fl死亡信息.Enabled = false;
            this.fl死亡信息.Location = new System.Drawing.Point(98, 245);
            this.fl死亡信息.Name = "fl死亡信息";
            this.fl死亡信息.Size = new System.Drawing.Size(632, 26);
            this.fl死亡信息.TabIndex = 153;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "死亡日期：";
            // 
            // dte死亡日期
            // 
            this.dte死亡日期.EditValue = null;
            this.dte死亡日期.Location = new System.Drawing.Point(69, 3);
            this.dte死亡日期.Name = "dte死亡日期";
            this.dte死亡日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte死亡日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte死亡日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte死亡日期.Size = new System.Drawing.Size(100, 20);
            this.dte死亡日期.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(175, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "    死亡原因：";
            // 
            // txt死亡原因
            // 
            this.txt死亡原因.Location = new System.Drawing.Point(257, 3);
            this.txt死亡原因.Name = "txt死亡原因";
            this.txt死亡原因.Size = new System.Drawing.Size(210, 20);
            this.txt死亡原因.TabIndex = 3;
            // 
            // sbtnFingerPrint
            // 
            this.sbtnFingerPrint.Location = new System.Drawing.Point(255, 1113);
            this.sbtnFingerPrint.Name = "sbtnFingerPrint";
            this.sbtnFingerPrint.Size = new System.Drawing.Size(81, 22);
            this.sbtnFingerPrint.StyleController = this.layoutControl1;
            this.sbtnFingerPrint.TabIndex = 136;
            this.sbtnFingerPrint.Text = "更新指纹";
            this.sbtnFingerPrint.Click += new System.EventHandler(this.sbtnFingerPrint_Click);
            // 
            // gc药物调整
            // 
            this.gc药物调整.Location = new System.Drawing.Point(6, 936);
            this.gc药物调整.MainView = this.gv药物调整;
            this.gc药物调整.Name = "gc药物调整";
            this.gc药物调整.Size = new System.Drawing.Size(721, 66);
            this.gc药物调整.TabIndex = 135;
            this.gc药物调整.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv药物调整});
            // 
            // gv药物调整
            // 
            this.gv药物调整.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gv药物调整.GridControl = this.gc药物调整;
            this.gv药物调整.Name = "gv药物调整";
            this.gv药物调整.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "药物名称";
            this.gridColumn5.FieldName = "药物名称";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "用法";
            this.gridColumn6.FieldName = "用法";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "个人档案编号";
            this.gridColumn7.FieldName = "个人档案编号";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "创建时间";
            this.gridColumn8.FieldName = "创建时间";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(435, 1113);
            this.txt备注.Name = "txt备注";
            this.txt备注.Size = new System.Drawing.Size(170, 20);
            this.txt备注.StyleController = this.layoutControl1;
            this.txt备注.TabIndex = 134;
            // 
            // txt转诊结果
            // 
            this.txt转诊结果.Enabled = false;
            this.txt转诊结果.Location = new System.Drawing.Point(547, 1065);
            this.txt转诊结果.Name = "txt转诊结果";
            this.txt转诊结果.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt转诊结果.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt转诊结果.Properties.NullText = "";
            this.txt转诊结果.Size = new System.Drawing.Size(180, 20);
            this.txt转诊结果.StyleController = this.layoutControl1;
            this.txt转诊结果.TabIndex = 132;
            // 
            // txt胰岛素用法用量
            // 
            this.txt胰岛素用法用量.Location = new System.Drawing.Point(446, 723);
            this.txt胰岛素用法用量.Name = "txt胰岛素用法用量";
            this.txt胰岛素用法用量.Size = new System.Drawing.Size(281, 20);
            this.txt胰岛素用法用量.StyleController = this.layoutControl1;
            this.txt胰岛素用法用量.TabIndex = 131;
            // 
            // txt胰岛素种类
            // 
            this.txt胰岛素种类.Location = new System.Drawing.Point(101, 723);
            this.txt胰岛素种类.Name = "txt胰岛素种类";
            this.txt胰岛素种类.Size = new System.Drawing.Size(236, 20);
            this.txt胰岛素种类.StyleController = this.layoutControl1;
            this.txt胰岛素种类.TabIndex = 130;
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Enabled = false;
            this.txt转诊联系电话.Location = new System.Drawing.Point(312, 1065);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Size = new System.Drawing.Size(136, 20);
            this.txt转诊联系电话.StyleController = this.layoutControl1;
            this.txt转诊联系电话.TabIndex = 128;
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Enabled = false;
            this.txt转诊联系人.Location = new System.Drawing.Point(101, 1065);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Size = new System.Drawing.Size(112, 20);
            this.txt转诊联系人.StyleController = this.layoutControl1;
            this.txt转诊联系人.TabIndex = 127;
            // 
            // txt调整胰岛素用法用量
            // 
            this.txt调整胰岛素用法用量.Location = new System.Drawing.Point(449, 912);
            this.txt调整胰岛素用法用量.Name = "txt调整胰岛素用法用量";
            this.txt调整胰岛素用法用量.Size = new System.Drawing.Size(278, 20);
            this.txt调整胰岛素用法用量.StyleController = this.layoutControl1;
            this.txt调整胰岛素用法用量.TabIndex = 126;
            // 
            // txt调整胰岛素种类
            // 
            this.txt调整胰岛素种类.Location = new System.Drawing.Point(101, 912);
            this.txt调整胰岛素种类.Name = "txt调整胰岛素种类";
            this.txt调整胰岛素种类.Size = new System.Drawing.Size(221, 20);
            this.txt调整胰岛素种类.StyleController = this.layoutControl1;
            this.txt调整胰岛素种类.TabIndex = 125;
            // 
            // btn删除药物调整
            // 
            this.btn删除药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物调整.Image")));
            this.btn删除药物调整.Location = new System.Drawing.Point(324, 882);
            this.btn删除药物调整.Name = "btn删除药物调整";
            this.btn删除药物调整.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物调整.StyleController = this.layoutControl1;
            this.btn删除药物调整.TabIndex = 124;
            this.btn删除药物调整.Tag = "2";
            this.btn删除药物调整.Text = "删除";
            // 
            // btn添加药物调整
            // 
            this.btn添加药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物调整.Image")));
            this.btn添加药物调整.Location = new System.Drawing.Point(266, 882);
            this.btn添加药物调整.Name = "btn添加药物调整";
            this.btn添加药物调整.Size = new System.Drawing.Size(54, 22);
            this.btn添加药物调整.StyleController = this.layoutControl1;
            this.btn添加药物调整.TabIndex = 123;
            this.btn添加药物调整.Tag = "0";
            this.btn添加药物调整.Text = "添加";
            // 
            // radio药物调整
            // 
            this.radio药物调整.EditValue = "2";
            this.radio药物调整.Location = new System.Drawing.Point(101, 882);
            this.radio药物调整.Name = "radio药物调整";
            this.radio药物调整.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio药物调整.Size = new System.Drawing.Size(161, 26);
            this.radio药物调整.StyleController = this.layoutControl1;
            this.radio药物调整.TabIndex = 122;
            this.radio药物调整.SelectedIndexChanged += new System.EventHandler(this.radio药物调整_SelectedIndexChanged);
            // 
            // radio管理措施
            // 
            this.radio管理措施.EditValue = "1";
            this.radio管理措施.Location = new System.Drawing.Point(101, 822);
            this.radio管理措施.Name = "radio管理措施";
            this.radio管理措施.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "常规随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "第1次控制不满意2周随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "两次控制不满意转诊随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "紧急转诊")});
            this.radio管理措施.Size = new System.Drawing.Size(474, 56);
            this.radio管理措施.StyleController = this.layoutControl1;
            this.radio管理措施.TabIndex = 121;
            // 
            // txt随访方式其他
            // 
            this.txt随访方式其他.Location = new System.Drawing.Point(583, 161);
            this.txt随访方式其他.Name = "txt随访方式其他";
            this.txt随访方式其他.Size = new System.Drawing.Size(147, 20);
            this.txt随访方式其他.TabIndex = 120;
            // 
            // fl随访分类并发症
            // 
            this.fl随访分类并发症.Controls.Add(this.labelControl9);
            this.fl随访分类并发症.Controls.Add(this.checkEdit5);
            this.fl随访分类并发症.Controls.Add(this.checkEdit6);
            this.fl随访分类并发症.Controls.Add(this.checkEdit7);
            this.fl随访分类并发症.Controls.Add(this.labelControl10);
            this.fl随访分类并发症.Location = new System.Drawing.Point(457, 673);
            this.fl随访分类并发症.Name = "fl随访分类并发症";
            this.fl随访分类并发症.Size = new System.Drawing.Size(270, 20);
            this.fl随访分类并发症.TabIndex = 119;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(3, 3);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(5, 14);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "(";
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(11, 0);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "脑卒中";
            this.checkEdit5.Size = new System.Drawing.Size(56, 19);
            this.checkEdit5.TabIndex = 21;
            this.checkEdit5.Tag = "1";
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(67, 0);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "冠心病";
            this.checkEdit6.Size = new System.Drawing.Size(59, 19);
            this.checkEdit6.TabIndex = 22;
            this.checkEdit6.Tag = "2";
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(126, 0);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "其他";
            this.checkEdit7.Size = new System.Drawing.Size(51, 19);
            this.checkEdit7.TabIndex = 23;
            this.checkEdit7.Tag = "99";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(180, 3);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(5, 14);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = ")";
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(98, 127);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(272, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 117;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(469, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(261, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 116;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(98, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(272, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 115;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(469, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(261, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 114;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(98, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(272, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 113;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(469, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(261, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 112;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(98, 48);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(272, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 111;
            // 
            // btn删除药物
            // 
            this.btn删除药物.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物.Image")));
            this.btn删除药物.Location = new System.Drawing.Point(322, 697);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物.StyleController = this.layoutControl1;
            this.btn删除药物.TabIndex = 110;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物.Image")));
            this.btn添加药物.Location = new System.Drawing.Point(263, 697);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(55, 22);
            this.btn添加药物.StyleController = this.layoutControl1;
            this.btn添加药物.TabIndex = 109;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(6, 747);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(721, 71);
            this.gcDetail.TabIndex = 93;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(568, 1198);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(162, 18);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 51;
            this.lab创建机构.Text = "labelControl16";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(344, 1198);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(140, 18);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 50;
            this.lab最近修改人.Text = "labelControl15";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(83, 1198);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(177, 18);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 49;
            this.lab创建人.Text = "labelControl14";
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(727, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 22;
            this.lab考核项.Text = "考核项：25     缺项：25 完整度：0% ";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(568, 1176);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(162, 18);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 48;
            this.lab当前所属机构.Text = "labelControl13";
            // 
            // lab最近更新时间
            // 
            this.lab最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近更新时间.Location = new System.Drawing.Point(344, 1176);
            this.lab最近更新时间.Name = "lab最近更新时间";
            this.lab最近更新时间.Size = new System.Drawing.Size(140, 18);
            this.lab最近更新时间.StyleController = this.layoutControl1;
            this.lab最近更新时间.TabIndex = 47;
            this.lab最近更新时间.Text = "labelControl12";
            // 
            // lab创建时间
            // 
            this.lab创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建时间.Location = new System.Drawing.Point(83, 1176);
            this.lab创建时间.Name = "lab创建时间";
            this.lab创建时间.Size = new System.Drawing.Size(177, 18);
            this.lab创建时间.StyleController = this.layoutControl1;
            this.lab创建时间.TabIndex = 46;
            this.lab创建时间.Text = "labelControl11";
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(435, 1089);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(292, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 45;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(101, 1089);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt下次随访时间.Size = new System.Drawing.Size(235, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 44;
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Enabled = false;
            this.txt转诊原因.Location = new System.Drawing.Point(306, 1041);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(142, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 43;
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Enabled = false;
            this.txt转诊科别.Location = new System.Drawing.Point(547, 1041);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(180, 20);
            this.txt转诊科别.StyleController = this.layoutControl1;
            this.txt转诊科别.TabIndex = 42;
            // 
            // radio转诊情况
            // 
            this.radio转诊情况.EditValue = "2";
            this.radio转诊情况.Location = new System.Drawing.Point(101, 1041);
            this.radio转诊情况.Name = "radio转诊情况";
            this.radio转诊情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio转诊情况.Size = new System.Drawing.Size(160, 20);
            this.radio转诊情况.StyleController = this.layoutControl1;
            this.radio转诊情况.TabIndex = 41;
            this.radio转诊情况.SelectedIndexChanged += new System.EventHandler(this.radio转诊情况_SelectedIndexChanged);
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(101, 697);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio用药情况.Size = new System.Drawing.Size(158, 22);
            this.radio用药情况.StyleController = this.layoutControl1;
            this.radio用药情况.TabIndex = 39;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // fl随访分类
            // 
            this.fl随访分类.Controls.Add(this.checkEdit控制满意);
            this.fl随访分类.Controls.Add(this.checkEdit控制不满意);
            this.fl随访分类.Controls.Add(this.checkEdit不良反应);
            this.fl随访分类.Controls.Add(this.ck随访并发症);
            this.fl随访分类.Location = new System.Drawing.Point(101, 673);
            this.fl随访分类.Name = "fl随访分类";
            this.fl随访分类.Size = new System.Drawing.Size(352, 20);
            this.fl随访分类.TabIndex = 37;
            // 
            // checkEdit控制满意
            // 
            this.checkEdit控制满意.Location = new System.Drawing.Point(0, 0);
            this.checkEdit控制满意.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit控制满意.Name = "checkEdit控制满意";
            this.checkEdit控制满意.Properties.Caption = "控制满意";
            this.checkEdit控制满意.Size = new System.Drawing.Size(72, 19);
            this.checkEdit控制满意.TabIndex = 15;
            this.checkEdit控制满意.Tag = "1";
            // 
            // checkEdit控制不满意
            // 
            this.checkEdit控制不满意.Location = new System.Drawing.Point(72, 0);
            this.checkEdit控制不满意.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit控制不满意.Name = "checkEdit控制不满意";
            this.checkEdit控制不满意.Properties.Caption = "控制不满意";
            this.checkEdit控制不满意.Size = new System.Drawing.Size(81, 19);
            this.checkEdit控制不满意.TabIndex = 16;
            this.checkEdit控制不满意.Tag = "2";
            // 
            // checkEdit不良反应
            // 
            this.checkEdit不良反应.Location = new System.Drawing.Point(153, 0);
            this.checkEdit不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit不良反应.Name = "checkEdit不良反应";
            this.checkEdit不良反应.Properties.Caption = "不良反应";
            this.checkEdit不良反应.Size = new System.Drawing.Size(69, 19);
            this.checkEdit不良反应.TabIndex = 17;
            this.checkEdit不良反应.Tag = "3";
            // 
            // ck随访并发症
            // 
            this.ck随访并发症.Location = new System.Drawing.Point(222, 0);
            this.ck随访并发症.Margin = new System.Windows.Forms.Padding(0);
            this.ck随访并发症.Name = "ck随访并发症";
            this.ck随访并发症.Properties.Caption = "并发症";
            this.ck随访并发症.Size = new System.Drawing.Size(63, 19);
            this.ck随访并发症.TabIndex = 18;
            this.ck随访并发症.Tag = "4";
            this.ck随访并发症.CheckedChanged += new System.EventHandler(this.ck随访并发症_CheckedChanged);
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Location = new System.Drawing.Point(362, 622);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(362, 20);
            this.txt药物副作用详述.StyleController = this.layoutControl1;
            this.txt药物副作用详述.TabIndex = 35;
            // 
            // radio不良反应
            // 
            this.radio不良反应.EditValue = "1";
            this.radio不良反应.Location = new System.Drawing.Point(101, 619);
            this.radio不良反应.Name = "radio不良反应";
            this.radio不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio不良反应.Size = new System.Drawing.Size(159, 26);
            this.radio不良反应.StyleController = this.layoutControl1;
            this.radio不良反应.TabIndex = 34;
            this.radio不良反应.SelectedIndexChanged += new System.EventHandler(this.radio不良反应_SelectedIndexChanged);
            // 
            // txt餐后2h血糖
            // 
            this.txt餐后2h血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt餐后2h血糖.Lbl1Text = "mmol/L ";
            this.txt餐后2h血糖.Location = new System.Drawing.Point(358, 571);
            this.txt餐后2h血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt餐后2h血糖.Name = "txt餐后2h血糖";
            this.txt餐后2h血糖.Size = new System.Drawing.Size(369, 20);
            this.txt餐后2h血糖.TabIndex = 32;
            this.txt餐后2h血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt随机血糖
            // 
            this.txt随机血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt随机血糖.Lbl1Text = "mmol/L ";
            this.txt随机血糖.Location = new System.Drawing.Point(101, 571);
            this.txt随机血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt随机血糖.Name = "txt随机血糖";
            this.txt随机血糖.Size = new System.Drawing.Size(158, 20);
            this.txt随机血糖.TabIndex = 31;
            this.txt随机血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt检查日期
            // 
            this.txt检查日期.EditValue = null;
            this.txt检查日期.Location = new System.Drawing.Point(611, 547);
            this.txt检查日期.Name = "txt检查日期";
            this.txt检查日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt检查日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt检查日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt检查日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt检查日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt检查日期.Size = new System.Drawing.Size(116, 20);
            this.txt检查日期.StyleController = this.layoutControl1;
            this.txt检查日期.TabIndex = 30;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.Lbl1Size = new System.Drawing.Size(22, 18);
            this.txt糖化血红蛋白.Lbl1Text = "(%)";
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(358, 547);
            this.txt糖化血红蛋白.Margin = new System.Windows.Forms.Padding(4);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(154, 20);
            this.txt糖化血红蛋白.TabIndex = 29;
            this.txt糖化血红蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(101, 547);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(4);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(158, 20);
            this.txt空腹血糖.TabIndex = 28;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt主食
            // 
            this.txt主食.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt主食.Lbl1Text = "/";
            this.txt主食.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt主食.Lbl2Text = "(克/天)";
            this.txt主食.Location = new System.Drawing.Point(101, 471);
            this.txt主食.Margin = new System.Windows.Forms.Padding(4);
            this.txt主食.Name = "txt主食";
            this.txt主食.Size = new System.Drawing.Size(256, 21);
            this.txt主食.TabIndex = 25;
            this.txt主食.Txt1EditValue = null;
            this.txt主食.Txt1Size = new System.Drawing.Size(70, 20);
            this.txt主食.Txt2EditValue = null;
            this.txt主食.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // txt持续时间
            // 
            this.txt持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt持续时间.Lbl1Text = "/";
            this.txt持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt持续时间.Lbl2Text = "(分钟/次)";
            this.txt持续时间.Location = new System.Drawing.Point(456, 447);
            this.txt持续时间.Margin = new System.Windows.Forms.Padding(4);
            this.txt持续时间.Name = "txt持续时间";
            this.txt持续时间.Size = new System.Drawing.Size(271, 20);
            this.txt持续时间.TabIndex = 24;
            this.txt持续时间.Txt1EditValue = null;
            this.txt持续时间.Txt1Size = new System.Drawing.Size(70, 20);
            this.txt持续时间.Txt2EditValue = null;
            this.txt持续时间.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(101, 447);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(4);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(256, 20);
            this.txt运动频率.TabIndex = 23;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(70, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(456, 423);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(4);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(271, 20);
            this.txt饮酒情况.TabIndex = 22;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(70, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(101, 423);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(4);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(256, 20);
            this.txt日吸烟量.TabIndex = 21;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(70, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // txt体征其他
            // 
            this.txt体征其他.Location = new System.Drawing.Point(593, 372);
            this.txt体征其他.Name = "txt体征其他";
            this.txt体征其他.Size = new System.Drawing.Size(134, 20);
            this.txt体征其他.StyleController = this.layoutControl1;
            this.txt体征其他.TabIndex = 20;
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(101, 372);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(4);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(162, 21);
            this.txt体质指数.TabIndex = 18;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(593, 348);
            this.txt身高.Margin = new System.Windows.Forms.Padding(4);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(134, 20);
            this.txt身高.TabIndex = 17;
            this.txt身高.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(18, 14);
            this.txt体重.Lbl2Text = "kg";
            this.txt体重.Location = new System.Drawing.Point(352, 348);
            this.txt体重.Margin = new System.Windows.Forms.Padding(4);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(152, 20);
            this.txt体重.TabIndex = 16;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(101, 348);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(4);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(162, 20);
            this.txt血压值.TabIndex = 15;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // fl症状
            // 
            this.fl症状.Controls.Add(this.check无症状);
            this.fl症状.Controls.Add(this.checkEdit多饮);
            this.fl症状.Controls.Add(this.checkEdit多食);
            this.fl症状.Controls.Add(this.checkEdit多尿);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Controls.Add(this.checkEdit15);
            this.fl症状.Controls.Add(this.checkEdit16);
            this.fl症状.Controls.Add(this.ch症状其他);
            this.fl症状.Controls.Add(this.txt症状其他);
            this.fl症状.Controls.Add(this.sbtn便捷录入症状);
            this.fl症状.Location = new System.Drawing.Point(98, 275);
            this.fl症状.Name = "fl症状";
            this.fl症状.Padding = new System.Windows.Forms.Padding(1);
            this.fl症状.Size = new System.Drawing.Size(632, 46);
            this.fl症状.TabIndex = 14;
            // 
            // check无症状
            // 
            this.check无症状.EditValue = true;
            this.check无症状.Location = new System.Drawing.Point(1, 1);
            this.check无症状.Margin = new System.Windows.Forms.Padding(0);
            this.check无症状.Name = "check无症状";
            this.check无症状.Properties.Caption = "1.无症状";
            this.check无症状.Size = new System.Drawing.Size(83, 19);
            this.check无症状.TabIndex = 11;
            this.check无症状.Tag = "0";
            this.check无症状.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit多饮
            // 
            this.checkEdit多饮.Location = new System.Drawing.Point(84, 1);
            this.checkEdit多饮.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit多饮.Name = "checkEdit多饮";
            this.checkEdit多饮.Properties.Caption = "2.多饮";
            this.checkEdit多饮.Size = new System.Drawing.Size(72, 19);
            this.checkEdit多饮.TabIndex = 12;
            this.checkEdit多饮.Tag = "1";
            // 
            // checkEdit多食
            // 
            this.checkEdit多食.Location = new System.Drawing.Point(156, 1);
            this.checkEdit多食.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit多食.Name = "checkEdit多食";
            this.checkEdit多食.Properties.Caption = "3.多食";
            this.checkEdit多食.Size = new System.Drawing.Size(69, 19);
            this.checkEdit多食.TabIndex = 13;
            this.checkEdit多食.Tag = "2";
            // 
            // checkEdit多尿
            // 
            this.checkEdit多尿.Location = new System.Drawing.Point(225, 1);
            this.checkEdit多尿.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit多尿.Name = "checkEdit多尿";
            this.checkEdit多尿.Properties.Caption = "4.多尿";
            this.checkEdit多尿.Size = new System.Drawing.Size(72, 19);
            this.checkEdit多尿.TabIndex = 14;
            this.checkEdit多尿.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(297, 1);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "5.视力模糊";
            this.checkEdit12.Size = new System.Drawing.Size(99, 19);
            this.checkEdit12.TabIndex = 15;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(396, 1);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "6.感染";
            this.checkEdit13.Size = new System.Drawing.Size(85, 19);
            this.checkEdit13.TabIndex = 16;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(481, 1);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "7.手脚麻木";
            this.checkEdit14.Size = new System.Drawing.Size(90, 19);
            this.checkEdit14.TabIndex = 17;
            this.checkEdit14.Tag = "6";
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(1, 20);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "8.下肢浮肿";
            this.checkEdit15.Size = new System.Drawing.Size(83, 19);
            this.checkEdit15.TabIndex = 18;
            this.checkEdit15.Tag = "7";
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(84, 20);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "9.体重明显下降";
            this.checkEdit16.Size = new System.Drawing.Size(113, 19);
            this.checkEdit16.TabIndex = 19;
            this.checkEdit16.Tag = "8";
            // 
            // ch症状其他
            // 
            this.ch症状其他.Location = new System.Drawing.Point(197, 20);
            this.ch症状其他.Margin = new System.Windows.Forms.Padding(0);
            this.ch症状其他.Name = "ch症状其他";
            this.ch症状其他.Properties.Caption = "10.其他";
            this.ch症状其他.Size = new System.Drawing.Size(75, 19);
            this.ch症状其他.TabIndex = 20;
            this.ch症状其他.Tag = "99";
            this.ch症状其他.CheckedChanged += new System.EventHandler(this.ch症状其他_CheckedChanged);
            // 
            // txt症状其他
            // 
            this.txt症状其他.Location = new System.Drawing.Point(272, 20);
            this.txt症状其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt症状其他.Name = "txt症状其他";
            this.txt症状其他.Size = new System.Drawing.Size(107, 20);
            this.txt症状其他.TabIndex = 21;
            // 
            // sbtn便捷录入症状
            // 
            this.sbtn便捷录入症状.Location = new System.Drawing.Point(379, 20);
            this.sbtn便捷录入症状.Margin = new System.Windows.Forms.Padding(0);
            this.sbtn便捷录入症状.Name = "sbtn便捷录入症状";
            this.sbtn便捷录入症状.Size = new System.Drawing.Size(106, 23);
            this.sbtn便捷录入症状.TabIndex = 22;
            this.sbtn便捷录入症状.Text = "便捷录入其他症状";
            this.sbtn便捷录入症状.Visible = false;
            this.sbtn便捷录入症状.Click += new System.EventHandler(this.sbtn便捷录入症状_Click);
            // 
            // radio随访方式
            // 
            this.radio随访方式.Location = new System.Drawing.Point(373, 160);
            this.radio随访方式.Margin = new System.Windows.Forms.Padding(0);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "其他")});
            this.radio随访方式.Size = new System.Drawing.Size(206, 21);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 13;
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(98, 161);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(176, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 12;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(101, 1006);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(626, 31);
            this.txt医生建议.StyleController = this.layoutControl1;
            this.txt医生建议.TabIndex = 38;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // txt低血糖反应
            // 
            this.txt低血糖反应.Location = new System.Drawing.Point(101, 649);
            this.txt低血糖反应.Name = "txt低血糖反应";
            this.txt低血糖反应.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt低血糖反应.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt低血糖反应.Properties.NullText = "请选择";
            this.txt低血糖反应.Properties.PopupSizeable = false;
            this.txt低血糖反应.Size = new System.Drawing.Size(159, 20);
            this.txt低血糖反应.StyleController = this.layoutControl1;
            this.txt低血糖反应.TabIndex = 36;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(101, 595);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "请选择";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(159, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 33;
            this.txt服药依从性.EditValueChanged += new System.EventHandler(this.txt服药依从性_EditValueChanged);
            // 
            // txt心理调整
            // 
            this.txt心理调整.Location = new System.Drawing.Point(456, 471);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(149, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 26;
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.Location = new System.Drawing.Point(101, 496);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt遵医行为.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt遵医行为.Properties.NullText = "请选择";
            this.txt遵医行为.Properties.PopupSizeable = false;
            this.txt遵医行为.Size = new System.Drawing.Size(153, 20);
            this.txt遵医行为.StyleController = this.layoutControl1;
            this.txt遵医行为.TabIndex = 27;
            // 
            // txt足背动脉搏动
            // 
            this.txt足背动脉搏动.Location = new System.Drawing.Point(352, 372);
            this.txt足背动脉搏动.Name = "txt足背动脉搏动";
            this.txt足背动脉搏动.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt足背动脉搏动.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt足背动脉搏动.Properties.NullText = "请选择";
            this.txt足背动脉搏动.Properties.PopupSizeable = false;
            this.txt足背动脉搏动.Size = new System.Drawing.Size(152, 20);
            this.txt足背动脉搏动.StyleController = this.layoutControl1;
            this.txt足背动脉搏动.TabIndex = 19;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(469, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(261, 37);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 118;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(101, 1113);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Properties.ReadOnly = true;
            this.txt居民签名.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.txt居民签名.Size = new System.Drawing.Size(150, 56);
            this.txt居民签名.StyleController = this.layoutControl1;
            this.txt居民签名.TabIndex = 133;
            this.txt居民签名.TabStop = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem37,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.layoutControlItem51,
            this.layoutControlItem53,
            this.layoutControlItem52,
            this.layoutControlItem54,
            this.layoutControlItem1,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem63});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 1220);
            this.layoutControlGroup1.Text = "糖尿病患者随访记录表";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt发生时间;
            this.layoutControlItem9.CustomizationFormText = "随访日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(169, 22);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "随访日期";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.radio随访方式;
            this.layoutControlItem10.CustomizationFormText = "随访方式";
            this.layoutControlItem10.Location = new System.Drawing.Point(275, 131);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 1, 2);
            this.layoutControlItem10.Size = new System.Drawing.Size(305, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "随访方式";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.fl症状;
            this.layoutControlItem11.CustomizationFormText = "症状";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 245);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(219, 50);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(731, 50);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "症状";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "体征";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem17});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 295);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(731, 75);
            this.layoutControlGroup2.Text = "体征";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt血压值;
            this.layoutControlItem12.CustomizationFormText = "血压";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "血压";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt体质指数;
            this.layoutControlItem15.CustomizationFormText = "体质指数";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(261, 25);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "体质指数";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt足背动脉搏动;
            this.layoutControlItem16.CustomizationFormText = "足背动脉搏动";
            this.layoutControlItem16.Location = new System.Drawing.Point(261, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(241, 25);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "足背动脉搏动";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt体重;
            this.layoutControlItem13.CustomizationFormText = "体重";
            this.layoutControlItem13.Location = new System.Drawing.Point(261, 0);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "体重";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt身高;
            this.layoutControlItem14.CustomizationFormText = "身高";
            this.layoutControlItem14.Location = new System.Drawing.Point(502, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Tag = "";
            this.layoutControlItem14.Text = "身高";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt体征其他;
            this.layoutControlItem17.CustomizationFormText = "其他";
            this.layoutControlItem17.Location = new System.Drawing.Point(502, 24);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(223, 25);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Tag = "check";
            this.layoutControlItem17.Text = "其他";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "生活方式指导";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 370);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(731, 124);
            this.layoutControlGroup3.Text = "生活方式指导";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt饮酒情况;
            this.layoutControlItem19.CustomizationFormText = "日饮酒量";
            this.layoutControlItem19.Location = new System.Drawing.Point(355, 0);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "日饮酒量";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt日吸烟量;
            this.layoutControlItem18.CustomizationFormText = "日吸烟量";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "日吸烟量";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt持续时间;
            this.layoutControlItem21.CustomizationFormText = "每次持续时间";
            this.layoutControlItem21.Location = new System.Drawing.Point(355, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "每次持续时间";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt运动频率;
            this.layoutControlItem20.CustomizationFormText = "运动频率";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "运动频率";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt主食;
            this.layoutControlItem22.CustomizationFormText = "主食";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(355, 25);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Tag = "check";
            this.layoutControlItem22.Text = "主食";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt心理调整;
            this.layoutControlItem23.CustomizationFormText = "心理调整";
            this.layoutControlItem23.Location = new System.Drawing.Point(355, 48);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(248, 25);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "心理调整";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt遵医行为;
            this.layoutControlItem24.CustomizationFormText = "遵医行为";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(252, 25);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "遵医行为";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(603, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(122, 25);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(252, 73);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(473, 25);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "辅助检查";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem29,
            this.layoutControlItem28,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.emptySpaceItem3,
            this.layout药物列表,
            this.layout添加药物,
            this.layout删除药物,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layout随访分类并发症,
            this.lbl管理措施,
            this.layoutControlItem3,
            this.layout添加药物调整,
            this.layout删除药物调整,
            this.emptySpaceItem6,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem40,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem57,
            this.layoutControlItem7,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.layout药物调整列表,
            this.layoutControlItem35,
            this.layoutControlItem60,
            this.layoutControlItem64,
            this.layoutControlItem65});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 494);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(731, 652);
            this.layoutControlGroup4.Text = "辅助检查";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.txt空腹血糖;
            this.layoutControlItem25.CustomizationFormText = "空腹血糖";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(257, 24);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "空腹血糖";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem26.Control = this.txt糖化血红蛋白;
            this.layoutControlItem26.CustomizationFormText = "糖化血红蛋白";
            this.layoutControlItem26.Location = new System.Drawing.Point(257, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(253, 24);
            this.layoutControlItem26.Tag = "";
            this.layoutControlItem26.Text = "糖化血红蛋白";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem27.Control = this.txt检查日期;
            this.layoutControlItem27.CustomizationFormText = "检查日期";
            this.layoutControlItem27.Location = new System.Drawing.Point(510, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "检查日期";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt餐后2h血糖;
            this.layoutControlItem29.CustomizationFormText = "餐后2小时血糖";
            this.layoutControlItem29.Location = new System.Drawing.Point(257, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(468, 24);
            this.layoutControlItem29.Tag = "";
            this.layoutControlItem29.Text = "餐后2小时血糖";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            this.layoutControlItem29.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.txt随机血糖;
            this.layoutControlItem28.CustomizationFormText = "随机血糖";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(257, 24);
            this.layoutControlItem28.Text = "随机血糖";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            this.layoutControlItem28.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem30.Control = this.txt服药依从性;
            this.layoutControlItem30.CustomizationFormText = "服药依从性";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem30.Tag = "check";
            this.layoutControlItem30.Text = "用药依从性";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem31.Control = this.radio不良反应;
            this.layoutControlItem31.CustomizationFormText = "药物不良反应";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(258, 30);
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "药物不良反应";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem32.Control = this.txt药物副作用详述;
            this.layoutControlItem32.CustomizationFormText = "副作用详述";
            this.layoutControlItem32.Location = new System.Drawing.Point(258, 72);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem32.Size = new System.Drawing.Size(467, 30);
            this.layoutControlItem32.Text = "副作用详述";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem33.Control = this.txt低血糖反应;
            this.layoutControlItem33.CustomizationFormText = "低血糖反应";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem33.Tag = "check";
            this.layoutControlItem33.Text = "低血糖反应";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem34.Control = this.fl随访分类;
            this.layoutControlItem34.CustomizationFormText = "此次随访分类";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem34.Tag = "check";
            this.layoutControlItem34.Text = "此次随访分类";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.radio用药情况;
            this.layoutControlItem36.CustomizationFormText = "用药情况";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(257, 26);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Tag = "check";
            this.layoutControlItem36.Text = "用药情况";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Control = this.radio转诊情况;
            this.layoutControlItem38.CustomizationFormText = "转诊情况";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 494);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(259, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Tag = "check";
            this.layoutControlItem38.Text = "转诊情况";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem39.Control = this.txt转诊科别;
            this.layoutControlItem39.CustomizationFormText = "机构及科别";
            this.layoutControlItem39.Location = new System.Drawing.Point(446, 494);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem39.Text = "机构及科别：";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem41.Control = this.txt下次随访时间;
            this.layoutControlItem41.CustomizationFormText = "下次随访时间";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 542);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem41.Tag = "check";
            this.layoutControlItem41.Text = "下次随访时间";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem42.Control = this.txt医生签名;
            this.layoutControlItem42.CustomizationFormText = "随访医生签名";
            this.layoutControlItem42.Location = new System.Drawing.Point(334, 542);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem42.Tag = "check";
            this.layoutControlItem42.Text = "随访医生签名";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(374, 150);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(351, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = " ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 200);
            this.layout药物列表.MinSize = new System.Drawing.Size(111, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(725, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = " ";
            this.layout药物列表.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layoutControlItem37";
            this.layout添加药物.Location = new System.Drawing.Point(257, 150);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(59, 26);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layoutControlItem50";
            this.layout删除药物.Location = new System.Drawing.Point(316, 150);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(58, 26);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(258, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(467, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(258, 102);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(467, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout随访分类并发症
            // 
            this.layout随访分类并发症.Control = this.fl随访分类并发症;
            this.layout随访分类并发症.CustomizationFormText = "随访分类并发症";
            this.layout随访分类并发症.Location = new System.Drawing.Point(451, 126);
            this.layout随访分类并发症.Name = "layout随访分类并发症";
            this.layout随访分类并发症.Size = new System.Drawing.Size(274, 24);
            this.layout随访分类并发症.Text = "随访分类并发症";
            this.layout随访分类并发症.TextSize = new System.Drawing.Size(0, 0);
            this.layout随访分类并发症.TextToControlDistance = 0;
            this.layout随访分类并发症.TextVisible = false;
            this.layout随访分类并发症.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl管理措施
            // 
            this.lbl管理措施.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl管理措施.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl管理措施.AppearanceItemCaption.Options.UseFont = true;
            this.lbl管理措施.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl管理措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl管理措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl管理措施.Control = this.radio管理措施;
            this.lbl管理措施.CustomizationFormText = "下一步管理措施";
            this.lbl管理措施.Location = new System.Drawing.Point(0, 275);
            this.lbl管理措施.MinSize = new System.Drawing.Size(162, 60);
            this.lbl管理措施.Name = "lbl管理措施";
            this.lbl管理措施.Size = new System.Drawing.Size(573, 60);
            this.lbl管理措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl管理措施.Tag = "check";
            this.lbl管理措施.Text = "下一步管理措施";
            this.lbl管理措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl管理措施.TextSize = new System.Drawing.Size(90, 20);
            this.lbl管理措施.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.radio药物调整;
            this.layoutControlItem3.CustomizationFormText = "调整用药意见";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 335);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(162, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(260, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "调整用药意见";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layout添加药物调整
            // 
            this.layout添加药物调整.Control = this.btn添加药物调整;
            this.layout添加药物调整.CustomizationFormText = "layout添加药物调整";
            this.layout添加药物调整.Location = new System.Drawing.Point(260, 335);
            this.layout添加药物调整.Name = "layout添加药物调整";
            this.layout添加药物调整.Size = new System.Drawing.Size(58, 30);
            this.layout添加药物调整.Text = "layout添加药物调整";
            this.layout添加药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物调整.TextToControlDistance = 0;
            this.layout添加药物调整.TextVisible = false;
            this.layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物调整
            // 
            this.layout删除药物调整.Control = this.btn删除药物调整;
            this.layout删除药物调整.CustomizationFormText = "layout删除药物调整";
            this.layout删除药物调整.Location = new System.Drawing.Point(318, 335);
            this.layout删除药物调整.Name = "layout删除药物调整";
            this.layout删除药物调整.Size = new System.Drawing.Size(58, 30);
            this.layout删除药物调整.Text = "layout删除药物调整";
            this.layout删除药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物调整.TextToControlDistance = 0;
            this.layout删除药物调整.TextVisible = false;
            this.layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(376, 335);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(349, 30);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt调整胰岛素种类;
            this.layoutControlItem2.CustomizationFormText = "调整胰岛素种类";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 365);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem2.Text = "调整胰岛素种类";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt调整胰岛素用法用量;
            this.layoutControlItem4.CustomizationFormText = "调整胰岛素用法和用量";
            this.layoutControlItem4.Location = new System.Drawing.Point(320, 365);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItem4.Text = "调整胰岛素用法和用量";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(120, 14);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.txt转诊原因;
            this.layoutControlItem40.CustomizationFormText = "原因";
            this.layoutControlItem40.Location = new System.Drawing.Point(259, 494);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem40.Text = "原因：";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt转诊联系人;
            this.layoutControlItem5.CustomizationFormText = "转诊联系人：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 518);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem5.Text = "转诊联系人：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt转诊联系电话;
            this.layoutControlItem6.CustomizationFormText = "转诊联系电话：";
            this.layoutControlItem6.Location = new System.Drawing.Point(211, 518);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem6.Text = "转诊联系电话：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt胰岛素种类;
            this.layoutControlItem8.CustomizationFormText = "胰岛素种类";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem8.Text = "胰岛素种类";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.txt胰岛素用法用量;
            this.layoutControlItem57.CustomizationFormText = "胰岛素用法和用量";
            this.layoutControlItem57.Location = new System.Drawing.Point(335, 176);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(390, 24);
            this.layoutControlItem57.Text = "胰岛素用法和用量";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt转诊结果;
            this.layoutControlItem7.CustomizationFormText = "转诊结果：";
            this.layoutControlItem7.Location = new System.Drawing.Point(446, 518);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem7.Text = "转诊结果：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem58.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem58.Control = this.txt居民签名;
            this.layoutControlItem58.CustomizationFormText = "居民签名";
            this.layoutControlItem58.Location = new System.Drawing.Point(0, 566);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(50, 60);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(249, 60);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Tag = "";
            this.layoutControlItem58.Text = "居民签名";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.txt备注;
            this.layoutControlItem59.CustomizationFormText = "备注";
            this.layoutControlItem59.Location = new System.Drawing.Point(334, 566);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(269, 60);
            this.layoutControlItem59.Tag = "";
            this.layoutControlItem59.Text = "备注";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layout药物调整列表
            // 
            this.layout药物调整列表.Control = this.gc药物调整;
            this.layout药物调整列表.CustomizationFormText = "layout药物调整列表";
            this.layout药物调整列表.Location = new System.Drawing.Point(0, 389);
            this.layout药物调整列表.MinSize = new System.Drawing.Size(227, 70);
            this.layout药物调整列表.Name = "layout药物调整列表";
            this.layout药物调整列表.Size = new System.Drawing.Size(725, 70);
            this.layout药物调整列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物调整列表.Text = "layout药物调整列表";
            this.layout药物调整列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物调整列表.TextToControlDistance = 0;
            this.layout药物调整列表.TextVisible = false;
            this.layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem35.Control = this.txt医生建议;
            this.layoutControlItem35.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 459);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(109, 35);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(725, 35);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Tag = "";
            this.layoutControlItem35.Text = "此次随访医生建议";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem35.TextToControlDistance = 5;
            this.layoutControlItem35.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.sbtnFingerPrint;
            this.layoutControlItem60.CustomizationFormText = "layoutControlItem60";
            this.layoutControlItem60.Location = new System.Drawing.Point(249, 566);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(85, 60);
            this.layoutControlItem60.Text = "layoutControlItem60";
            this.layoutControlItem60.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem60.TextToControlDistance = 0;
            this.layoutControlItem60.TextVisible = false;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.Control = this.sbtn查看近期下一步;
            this.layoutControlItem64.CustomizationFormText = "layoutControlItem64";
            this.layoutControlItem64.Location = new System.Drawing.Point(573, 275);
            this.layoutControlItem64.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(93, 40);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(152, 60);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Text = "layoutControlItem64";
            this.layoutControlItem64.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem64.TextToControlDistance = 0;
            this.layoutControlItem64.TextVisible = false;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.Control = this.sbtn便捷录入备注;
            this.layoutControlItem65.CustomizationFormText = "layoutControlItem65";
            this.layoutControlItem65.Location = new System.Drawing.Point(603, 566);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(122, 60);
            this.layoutControlItem65.Text = "layoutControlItem65";
            this.layoutControlItem65.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem65.TextToControlDistance = 0;
            this.layoutControlItem65.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 1190);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(731, 1);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lab创建时间;
            this.layoutControlItem43.CustomizationFormText = "layoutControlItem43";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 1146);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(261, 22);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem43.TextToControlDistance = 0;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lab最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "layoutControlItem44";
            this.layoutControlItem44.Location = new System.Drawing.Point(261, 1146);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(224, 22);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem44.TextToControlDistance = 0;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lab当前所属机构;
            this.layoutControlItem45.CustomizationFormText = "layoutControlItem45";
            this.layoutControlItem45.Location = new System.Drawing.Point(485, 1146);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(246, 22);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "当前所属机构";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem45.TextToControlDistance = 0;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab创建人;
            this.layoutControlItem46.CustomizationFormText = "layoutControlItem46";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 1168);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(261, 22);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "录入人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem46.TextToControlDistance = 0;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lab最近修改人;
            this.layoutControlItem47.CustomizationFormText = "layoutControlItem47";
            this.layoutControlItem47.Location = new System.Drawing.Point(261, 1168);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(224, 22);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "最近更新人";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem47.TextToControlDistance = 0;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lab创建机构;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem48.Location = new System.Drawing.Point(485, 1168);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(90, 22);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(246, 22);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "创建机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(80, 22);
            this.layoutControlItem48.TextToControlDistance = 0;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.lab考核项;
            this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(731, 18);
            this.layoutControlItem37.Text = "layoutControlItem37";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextToControlDistance = 0;
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.txt个人档案号;
            this.layoutControlItem49.CustomizationFormText = "个人档案号";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem49.Text = "个人档案号";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt姓名;
            this.layoutControlItem50.CustomizationFormText = "姓名";
            this.layoutControlItem50.Location = new System.Drawing.Point(371, 18);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem50.Text = "姓名";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.txt联系电话;
            this.layoutControlItem55.CustomizationFormText = "联系电话";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 9, 2);
            this.layoutControlItem55.Size = new System.Drawing.Size(371, 41);
            this.layoutControlItem55.Text = "联系电话";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.txt居住地址;
            this.layoutControlItem56.CustomizationFormText = "居住地址";
            this.layoutControlItem56.Location = new System.Drawing.Point(371, 90);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(109, 41);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(360, 41);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "居住地址";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt性别;
            this.layoutControlItem51.CustomizationFormText = "性别";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem51.Text = "性别";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.txt身份证号;
            this.layoutControlItem53.CustomizationFormText = "身份证号";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem53.Text = "身份证号";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt出生日期;
            this.layoutControlItem52.CustomizationFormText = "出生日期";
            this.layoutControlItem52.Location = new System.Drawing.Point(371, 42);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem52.Text = "出生日期";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.txt职业;
            this.layoutControlItem54.CustomizationFormText = "职业";
            this.layoutControlItem54.Location = new System.Drawing.Point(371, 66);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem54.Text = "职业";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt随访方式其他;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(580, 131);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(151, 24);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.fl访视情况;
            this.layoutControlItem61.CustomizationFormText = "访视情况：";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(199, 30);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(731, 30);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "访视情况：";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.fl失访原因;
            this.layoutControlItem62.CustomizationFormText = "失访原因：";
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 185);
            this.layoutControlItem62.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(199, 30);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(731, 30);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "失访原因：";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.fl死亡信息;
            this.layoutControlItem63.CustomizationFormText = "死亡信息：";
            this.layoutControlItem63.Location = new System.Drawing.Point(0, 215);
            this.layoutControlItem63.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(199, 30);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(731, 30);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "死亡信息：";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // UC糖尿病患者随访记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC糖尿病患者随访记录表";
            this.Size = new System.Drawing.Size(750, 505);
            this.Load += new System.EventHandler(this.UC糖尿病患者随访记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.fl失访原因.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk外出打工.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk迁居他处.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk走失.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            this.fl访视情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk失访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk死亡.Properties)).EndInit();
            this.fl死亡信息.ResumeLayout(false);
            this.fl死亡信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte死亡日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素用法用量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胰岛素种类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素用法用量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调整胰岛素种类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).EndInit();
            this.fl随访分类并发症.ResumeLayout(false);
            this.fl随访分类并发症.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            this.fl随访分类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit控制满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit控制不满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck随访并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt检查日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多饮.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit多尿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt低血糖反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout随访分类并发症)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl管理措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit check无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit多饮;
        private DevExpress.XtraEditors.CheckEdit checkEdit多食;
        private DevExpress.XtraEditors.CheckEdit checkEdit多尿;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit ch症状其他;
        private DevExpress.XtraEditors.TextEdit txt症状其他;
        private Library.UserControls.UCTxtLblTxtLbl txt主食;
        private Library.UserControls.UCTxtLblTxtLbl txt持续时间;
        private Library.UserControls.UCTxtLblTxtLbl txt运动频率;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private DevExpress.XtraEditors.TextEdit txt体征其他;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private DevExpress.XtraEditors.RadioGroup radio不良反应;
        private Library.UserControls.UCTxtLbl txt餐后2h血糖;
        private Library.UserControls.UCTxtLbl txt随机血糖;
        private DevExpress.XtraEditors.DateEdit txt检查日期;
        private Library.UserControls.UCTxtLbl txt糖化血红蛋白;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraEditors.RadioGroup radio转诊情况;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private DevExpress.XtraEditors.CheckEdit checkEdit控制满意;
        private DevExpress.XtraEditors.CheckEdit checkEdit控制不满意;
        private DevExpress.XtraEditors.CheckEdit checkEdit不良反应;
        private DevExpress.XtraEditors.CheckEdit ck随访并发症;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lab最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.LookUpEdit txt低血糖反应;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraEditors.LookUpEdit txt遵医行为;
        private DevExpress.XtraEditors.LookUpEdit txt足背动脉搏动;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类并发症;
        private DevExpress.XtraLayout.LayoutControlItem layout随访分类并发症;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private System.Windows.Forms.TextBox txt随访方式其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gc药物调整;
        private DevExpress.XtraGrid.Views.Grid.GridView gv药物调整;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private DevExpress.XtraEditors.LookUpEdit txt转诊结果;
        private DevExpress.XtraEditors.TextEdit txt胰岛素用法用量;
        private DevExpress.XtraEditors.TextEdit txt胰岛素种类;
        private DevExpress.XtraEditors.TextEdit txt转诊联系电话;
        private DevExpress.XtraEditors.TextEdit txt转诊联系人;
        private DevExpress.XtraEditors.TextEdit txt调整胰岛素用法用量;
        private DevExpress.XtraEditors.TextEdit txt调整胰岛素种类;
        private DevExpress.XtraEditors.SimpleButton btn删除药物调整;
        private DevExpress.XtraEditors.SimpleButton btn添加药物调整;
        private DevExpress.XtraEditors.RadioGroup radio药物调整;
        private DevExpress.XtraEditors.RadioGroup radio管理措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl管理措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物调整;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layout药物调整列表;
        private DevExpress.XtraEditors.SimpleButton sbtnFingerPrint;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraEditors.PictureEdit txt居民签名;
        private System.Windows.Forms.FlowLayoutPanel fl访视情况;
        private DevExpress.XtraEditors.CheckEdit chk失访;
        private DevExpress.XtraEditors.CheckEdit chk死亡;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private System.Windows.Forms.FlowLayoutPanel fl失访原因;
        private DevExpress.XtraEditors.CheckEdit chk外出打工;
        private DevExpress.XtraEditors.CheckEdit chk迁居他处;
        private DevExpress.XtraEditors.CheckEdit chk走失;
        private DevExpress.XtraEditors.CheckEdit chk连续3次;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private System.Windows.Forms.FlowLayoutPanel fl死亡信息;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dte死亡日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt死亡原因;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraEditors.SimpleButton sbtn查看近期下一步;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraEditors.SimpleButton sbtn便捷录入症状;
        private DevExpress.XtraEditors.SimpleButton sbtn便捷录入备注;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
    }
}
