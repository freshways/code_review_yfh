﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC高血压患者分级管理
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC高血压患者分级管理));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.btn关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.Layout1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt同型半胱氨酸 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt高密度蛋白 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt低密度蛋白 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt甘油三酯 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt总胆固醇 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血钠浓度 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血钾浓度 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血尿素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血清肌酐 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt总胆红素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血清谷草转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血清谷丙转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt尿常规其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt尿潜血 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt尿酮体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt尿糖 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt尿蛋白 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt血小板 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt白细胞 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血红蛋白 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt血常规其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt分级管理 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sbtn便捷录入备注 = new DevExpress.XtraEditors.SimpleButton();
            this.fl家族史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk以上都无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk不详 = new DevExpress.XtraEditors.CheckEdit();
            this.chk拒答 = new DevExpress.XtraEditors.CheckEdit();
            this.fl并发症 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk无并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.fl既往史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk无既往史 = new DevExpress.XtraEditors.CheckEdit();
            this.chk既往冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk既往心力衰竭 = new DevExpress.XtraEditors.CheckEdit();
            this.chk连续3次 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.sbtnFingerPrint = new DevExpress.XtraEditors.SimpleButton();
            this.gc药物调整 = new DevExpress.XtraGrid.GridControl();
            this.gv药物调整 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.btn删除药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物调整 = new DevExpress.XtraEditors.SimpleButton();
            this.txt转诊结果 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系人 = new DevExpress.XtraEditors.TextEdit();
            this.radio药物调整 = new DevExpress.XtraEditors.RadioGroup();
            this.radio管理措施 = new DevExpress.XtraEditors.RadioGroup();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.LookUpEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.check无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.ch症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt心率 = new DevExpress.XtraEditors.TextEdit();
            this.txt体征其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt指导运动 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt其他1 = new DevExpress.XtraEditors.TextEdit();
            this.radio不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.radio转诊情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt转诊科别 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.MemoEdit();
            this.txt居民签名 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物调整 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物调整列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl失访 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.radio心电图 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.radio胸片 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.radio超声心动图 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.radio颈动脉超声 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt其他2 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt超声心动图异常 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt颈动脉超声异常 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt胸片异常 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem83 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt心电图异常 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).BeginInit();
            this.Layout1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分级管理.Properties)).BeginInit();
            this.fl家族史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk以上都无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不详.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk拒答.Properties)).BeginInit();
            this.fl并发症.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk无并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            this.fl既往史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk无既往史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk既往冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk既往心力衰竭.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio心电图.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio胸片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio超声心动图.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio颈动脉超声.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声心动图异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt颈动脉超声异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸片异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Location = new System.Drawing.Point(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(725, 29);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "取消";
            this.btn重置.Click += new System.EventHandler(this.btn重置_Click);
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 2;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // btn关闭
            // 
            this.btn关闭.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn关闭.Image = ((System.Drawing.Image)(resources.GetObject("btn关闭.Image")));
            this.btn关闭.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn关闭.Location = new System.Drawing.Point(688, 2);
            this.btn关闭.Name = "btn关闭";
            this.btn关闭.Size = new System.Drawing.Size(39, 29);
            this.btn关闭.TabIndex = 3;
            this.btn关闭.Click += new System.EventHandler(this.btn重置_Click);
            // 
            // Layout1
            // 
            this.Layout1.Controls.Add(this.txt心电图异常);
            this.Layout1.Controls.Add(this.txt胸片异常);
            this.Layout1.Controls.Add(this.txt颈动脉超声异常);
            this.Layout1.Controls.Add(this.txt超声心动图异常);
            this.Layout1.Controls.Add(this.txt其他2);
            this.Layout1.Controls.Add(this.radio颈动脉超声);
            this.Layout1.Controls.Add(this.radio超声心动图);
            this.Layout1.Controls.Add(this.radio胸片);
            this.Layout1.Controls.Add(this.radio心电图);
            this.Layout1.Controls.Add(this.txt同型半胱氨酸);
            this.Layout1.Controls.Add(this.txt空腹血糖);
            this.Layout1.Controls.Add(this.txt高密度蛋白);
            this.Layout1.Controls.Add(this.txt低密度蛋白);
            this.Layout1.Controls.Add(this.txt甘油三酯);
            this.Layout1.Controls.Add(this.txt总胆固醇);
            this.Layout1.Controls.Add(this.txt血钠浓度);
            this.Layout1.Controls.Add(this.txt血钾浓度);
            this.Layout1.Controls.Add(this.txt血尿素);
            this.Layout1.Controls.Add(this.txt血清肌酐);
            this.Layout1.Controls.Add(this.txt总胆红素);
            this.Layout1.Controls.Add(this.txt血清谷草转氨酶);
            this.Layout1.Controls.Add(this.txt血清谷丙转氨酶);
            this.Layout1.Controls.Add(this.txt尿常规其他);
            this.Layout1.Controls.Add(this.txt尿潜血);
            this.Layout1.Controls.Add(this.txt尿酮体);
            this.Layout1.Controls.Add(this.txt尿糖);
            this.Layout1.Controls.Add(this.txt尿蛋白);
            this.Layout1.Controls.Add(this.txt血小板);
            this.Layout1.Controls.Add(this.txt白细胞);
            this.Layout1.Controls.Add(this.txt血红蛋白);
            this.Layout1.Controls.Add(this.txt血常规其他);
            this.Layout1.Controls.Add(this.txt服药依从性);
            this.Layout1.Controls.Add(this.txt分级管理);
            this.Layout1.Controls.Add(this.sbtn便捷录入备注);
            this.Layout1.Controls.Add(this.fl家族史);
            this.Layout1.Controls.Add(this.fl并发症);
            this.Layout1.Controls.Add(this.fl既往史);
            this.Layout1.Controls.Add(this.sbtnFingerPrint);
            this.Layout1.Controls.Add(this.gc药物调整);
            this.Layout1.Controls.Add(this.layoutControl1);
            this.Layout1.Controls.Add(this.btn删除药物调整);
            this.Layout1.Controls.Add(this.btn添加药物调整);
            this.Layout1.Controls.Add(this.txt转诊结果);
            this.Layout1.Controls.Add(this.txt备注);
            this.Layout1.Controls.Add(this.txt转诊联系电话);
            this.Layout1.Controls.Add(this.txt转诊联系人);
            this.Layout1.Controls.Add(this.radio药物调整);
            this.Layout1.Controls.Add(this.radio管理措施);
            this.Layout1.Controls.Add(this.txt联系电话);
            this.Layout1.Controls.Add(this.txt身份证号);
            this.Layout1.Controls.Add(this.txt出生日期);
            this.Layout1.Controls.Add(this.txt性别);
            this.Layout1.Controls.Add(this.txt姓名);
            this.Layout1.Controls.Add(this.txt个人档案号);
            this.Layout1.Controls.Add(this.txt摄盐情况2);
            this.Layout1.Controls.Add(this.txt摄盐情况1);
            this.Layout1.Controls.Add(this.gcDetail);
            this.Layout1.Controls.Add(this.btn删除药物);
            this.Layout1.Controls.Add(this.btn添加药物);
            this.Layout1.Controls.Add(this.txt发生时间);
            this.Layout1.Controls.Add(this.txt下次随访时间);
            this.Layout1.Controls.Add(this.radio用药情况);
            this.Layout1.Controls.Add(this.fl症状);
            this.Layout1.Controls.Add(this.radio随访方式);
            this.Layout1.Controls.Add(this.txt血压值);
            this.Layout1.Controls.Add(this.txt体重);
            this.Layout1.Controls.Add(this.txt身高);
            this.Layout1.Controls.Add(this.txt体质指数);
            this.Layout1.Controls.Add(this.txt心率);
            this.Layout1.Controls.Add(this.txt体征其他);
            this.Layout1.Controls.Add(this.txt日吸烟量);
            this.Layout1.Controls.Add(this.txt饮酒情况);
            this.Layout1.Controls.Add(this.txt运动);
            this.Layout1.Controls.Add(this.txt指导运动);
            this.Layout1.Controls.Add(this.txt其他1);
            this.Layout1.Controls.Add(this.radio不良反应);
            this.Layout1.Controls.Add(this.txt药物副作用详述);
            this.Layout1.Controls.Add(this.txt医生建议);
            this.Layout1.Controls.Add(this.radio转诊情况);
            this.Layout1.Controls.Add(this.txt转诊科别);
            this.Layout1.Controls.Add(this.txt医生签名);
            this.Layout1.Controls.Add(this.lbl创建时间);
            this.Layout1.Controls.Add(this.lab创建机构);
            this.Layout1.Controls.Add(this.lbl最近更新时间);
            this.Layout1.Controls.Add(this.lab创建人);
            this.Layout1.Controls.Add(this.txt转诊原因);
            this.Layout1.Controls.Add(this.lab最近修改人);
            this.Layout1.Controls.Add(this.lab当前所属机构);
            this.Layout1.Controls.Add(this.txt心理调整);
            this.Layout1.Controls.Add(this.txt居住地址);
            this.Layout1.Controls.Add(this.txt居民签名);
            this.Layout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Layout1.Location = new System.Drawing.Point(0, 33);
            this.Layout1.Name = "Layout1";
            this.Layout1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(513, 262, 250, 350);
            this.Layout1.Root = this.layoutControlGroup1;
            this.Layout1.Size = new System.Drawing.Size(729, 458);
            this.Layout1.TabIndex = 5;
            // 
            // txt同型半胱氨酸
            // 
            this.txt同型半胱氨酸.Lbl1Size = new System.Drawing.Size(41, 18);
            this.txt同型半胱氨酸.Lbl1Text = "umol/L";
            this.txt同型半胱氨酸.Location = new System.Drawing.Point(341, 181);
            this.txt同型半胱氨酸.Name = "txt同型半胱氨酸";
            this.txt同型半胱氨酸.Size = new System.Drawing.Size(121, 20);
            this.txt同型半胱氨酸.TabIndex = 179;
            this.txt同型半胱氨酸.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(41, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(100, 181);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(143, 20);
            this.txt空腹血糖.TabIndex = 178;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt高密度蛋白
            // 
            this.txt高密度蛋白.Lbl1Size = new System.Drawing.Size(61, 18);
            this.txt高密度蛋白.Lbl1Text = "高密度蛋白";
            this.txt高密度蛋白.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt高密度蛋白.Lbl2Text = "mmol/L";
            this.txt高密度蛋白.Location = new System.Drawing.Point(543, 157);
            this.txt高密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt高密度蛋白.Name = "txt高密度蛋白";
            this.txt高密度蛋白.Size = new System.Drawing.Size(160, 20);
            this.txt高密度蛋白.TabIndex = 177;
            this.txt高密度蛋白.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt低密度蛋白
            // 
            this.txt低密度蛋白.Lbl1Size = new System.Drawing.Size(61, 18);
            this.txt低密度蛋白.Lbl1Text = "低密度蛋白";
            this.txt低密度蛋白.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt低密度蛋白.Lbl2Text = "mmol/L";
            this.txt低密度蛋白.Location = new System.Drawing.Point(391, 157);
            this.txt低密度蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt低密度蛋白.Name = "txt低密度蛋白";
            this.txt低密度蛋白.Size = new System.Drawing.Size(148, 20);
            this.txt低密度蛋白.TabIndex = 176;
            this.txt低密度蛋白.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt甘油三酯
            // 
            this.txt甘油三酯.Lbl1Size = new System.Drawing.Size(51, 18);
            this.txt甘油三酯.Lbl1Text = "甘油三酯";
            this.txt甘油三酯.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt甘油三酯.Lbl2Text = "mmol/L";
            this.txt甘油三酯.Location = new System.Drawing.Point(247, 157);
            this.txt甘油三酯.Margin = new System.Windows.Forms.Padding(0);
            this.txt甘油三酯.Name = "txt甘油三酯";
            this.txt甘油三酯.Size = new System.Drawing.Size(140, 20);
            this.txt甘油三酯.TabIndex = 175;
            this.txt甘油三酯.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt总胆固醇
            // 
            this.txt总胆固醇.Lbl1Size = new System.Drawing.Size(51, 18);
            this.txt总胆固醇.Lbl1Text = "总胆固醇";
            this.txt总胆固醇.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt总胆固醇.Lbl2Text = "mmol/L";
            this.txt总胆固醇.Location = new System.Drawing.Point(100, 157);
            this.txt总胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt总胆固醇.Name = "txt总胆固醇";
            this.txt总胆固醇.Size = new System.Drawing.Size(143, 20);
            this.txt总胆固醇.TabIndex = 174;
            this.txt总胆固醇.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt血钠浓度
            // 
            this.txt血钠浓度.Lbl1Size = new System.Drawing.Size(61, 18);
            this.txt血钠浓度.Lbl1Text = "血钠浓度";
            this.txt血钠浓度.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt血钠浓度.Lbl2Text = "mmol/L";
            this.txt血钠浓度.Location = new System.Drawing.Point(543, 133);
            this.txt血钠浓度.Margin = new System.Windows.Forms.Padding(0);
            this.txt血钠浓度.Name = "txt血钠浓度";
            this.txt血钠浓度.Size = new System.Drawing.Size(160, 20);
            this.txt血钠浓度.TabIndex = 173;
            this.txt血钠浓度.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt血钾浓度
            // 
            this.txt血钾浓度.Lbl1Size = new System.Drawing.Size(61, 18);
            this.txt血钾浓度.Lbl1Text = "血钾浓度";
            this.txt血钾浓度.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt血钾浓度.Lbl2Text = "mmol/L";
            this.txt血钾浓度.Location = new System.Drawing.Point(391, 133);
            this.txt血钾浓度.Margin = new System.Windows.Forms.Padding(0);
            this.txt血钾浓度.Name = "txt血钾浓度";
            this.txt血钾浓度.Size = new System.Drawing.Size(148, 20);
            this.txt血钾浓度.TabIndex = 172;
            this.txt血钾浓度.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt血尿素
            // 
            this.txt血尿素.Lbl1Size = new System.Drawing.Size(51, 18);
            this.txt血尿素.Lbl1Text = "血尿素";
            this.txt血尿素.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt血尿素.Lbl2Text = "mmol/L";
            this.txt血尿素.Location = new System.Drawing.Point(247, 133);
            this.txt血尿素.Margin = new System.Windows.Forms.Padding(0);
            this.txt血尿素.Name = "txt血尿素";
            this.txt血尿素.Size = new System.Drawing.Size(140, 20);
            this.txt血尿素.TabIndex = 171;
            this.txt血尿素.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt血清肌酐
            // 
            this.txt血清肌酐.Lbl1Size = new System.Drawing.Size(51, 18);
            this.txt血清肌酐.Lbl1Text = "血清肌酐";
            this.txt血清肌酐.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt血清肌酐.Lbl2Text = "mmol/L";
            this.txt血清肌酐.Location = new System.Drawing.Point(100, 133);
            this.txt血清肌酐.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清肌酐.Name = "txt血清肌酐";
            this.txt血清肌酐.Size = new System.Drawing.Size(143, 20);
            this.txt血清肌酐.TabIndex = 170;
            this.txt血清肌酐.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt总胆红素
            // 
            this.txt总胆红素.Lbl1Size = new System.Drawing.Size(61, 18);
            this.txt总胆红素.Lbl1Text = "总胆红素";
            this.txt总胆红素.Lbl2Size = new System.Drawing.Size(41, 18);
            this.txt总胆红素.Lbl2Text = "μmol/L";
            this.txt总胆红素.Location = new System.Drawing.Point(543, 109);
            this.txt总胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.txt总胆红素.Name = "txt总胆红素";
            this.txt总胆红素.Size = new System.Drawing.Size(160, 20);
            this.txt总胆红素.TabIndex = 169;
            this.txt总胆红素.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // txt血清谷草转氨酶
            // 
            this.txt血清谷草转氨酶.Lbl1Size = new System.Drawing.Size(88, 18);
            this.txt血清谷草转氨酶.Lbl1Text = "血清谷草转氨酶";
            this.txt血清谷草转氨酶.Lbl2Size = new System.Drawing.Size(25, 18);
            this.txt血清谷草转氨酶.Lbl2Text = "U/L";
            this.txt血清谷草转氨酶.Location = new System.Drawing.Point(302, 109);
            this.txt血清谷草转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清谷草转氨酶.Name = "txt血清谷草转氨酶";
            this.txt血清谷草转氨酶.Size = new System.Drawing.Size(237, 20);
            this.txt血清谷草转氨酶.TabIndex = 168;
            this.txt血清谷草转氨酶.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt血清谷丙转氨酶
            // 
            this.txt血清谷丙转氨酶.Lbl1Size = new System.Drawing.Size(91, 18);
            this.txt血清谷丙转氨酶.Lbl1Text = "血清谷丙转氨酶";
            this.txt血清谷丙转氨酶.Lbl2Size = new System.Drawing.Size(25, 18);
            this.txt血清谷丙转氨酶.Lbl2Text = "U/L";
            this.txt血清谷丙转氨酶.Location = new System.Drawing.Point(100, 109);
            this.txt血清谷丙转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清谷丙转氨酶.Name = "txt血清谷丙转氨酶";
            this.txt血清谷丙转氨酶.Size = new System.Drawing.Size(198, 20);
            this.txt血清谷丙转氨酶.TabIndex = 167;
            this.txt血清谷丙转氨酶.Txt1Size = new System.Drawing.Size(54, 20);
            // 
            // txt尿常规其他
            // 
            this.txt尿常规其他.Location = new System.Drawing.Point(629, 85);
            this.txt尿常规其他.Name = "txt尿常规其他";
            this.txt尿常规其他.Size = new System.Drawing.Size(74, 20);
            this.txt尿常规其他.StyleController = this.Layout1;
            this.txt尿常规其他.TabIndex = 166;
            // 
            // txt尿潜血
            // 
            this.txt尿潜血.Lbl1Size = new System.Drawing.Size(45, 18);
            this.txt尿潜血.Lbl1Text = "尿潜血";
            this.txt尿潜血.Location = new System.Drawing.Point(485, 85);
            this.txt尿潜血.Name = "txt尿潜血";
            this.txt尿潜血.Size = new System.Drawing.Size(111, 20);
            this.txt尿潜血.TabIndex = 165;
            this.txt尿潜血.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt尿酮体
            // 
            this.txt尿酮体.Lbl1Size = new System.Drawing.Size(45, 18);
            this.txt尿酮体.Lbl1Text = "尿酮体";
            this.txt尿酮体.Location = new System.Drawing.Point(359, 85);
            this.txt尿酮体.Name = "txt尿酮体";
            this.txt尿酮体.Size = new System.Drawing.Size(122, 20);
            this.txt尿酮体.TabIndex = 164;
            this.txt尿酮体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt尿糖
            // 
            this.txt尿糖.Lbl1Size = new System.Drawing.Size(35, 18);
            this.txt尿糖.Lbl1Text = "尿糖";
            this.txt尿糖.Location = new System.Drawing.Point(239, 85);
            this.txt尿糖.Name = "txt尿糖";
            this.txt尿糖.Size = new System.Drawing.Size(116, 20);
            this.txt尿糖.TabIndex = 163;
            this.txt尿糖.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt尿蛋白
            // 
            this.txt尿蛋白.Lbl1Size = new System.Drawing.Size(45, 18);
            this.txt尿蛋白.Lbl1Text = "尿蛋白";
            this.txt尿蛋白.Location = new System.Drawing.Point(100, 85);
            this.txt尿蛋白.Name = "txt尿蛋白";
            this.txt尿蛋白.Size = new System.Drawing.Size(135, 20);
            this.txt尿蛋白.TabIndex = 162;
            this.txt尿蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt血小板
            // 
            this.txt血小板.Lbl1Size = new System.Drawing.Size(45, 18);
            this.txt血小板.Lbl1Text = "血小板";
            this.txt血小板.Lbl2Size = new System.Drawing.Size(35, 18);
            this.txt血小板.Lbl2Text = "×10/L";
            this.txt血小板.Location = new System.Drawing.Point(432, 61);
            this.txt血小板.Margin = new System.Windows.Forms.Padding(0);
            this.txt血小板.Name = "txt血小板";
            this.txt血小板.Size = new System.Drawing.Size(164, 20);
            this.txt血小板.TabIndex = 161;
            this.txt血小板.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt白细胞
            // 
            this.txt白细胞.Lbl1Size = new System.Drawing.Size(45, 18);
            this.txt白细胞.Lbl1Text = "白细胞";
            this.txt白细胞.Lbl2Size = new System.Drawing.Size(35, 18);
            this.txt白细胞.Lbl2Text = "×10/L";
            this.txt白细胞.Location = new System.Drawing.Point(269, 61);
            this.txt白细胞.Margin = new System.Windows.Forms.Padding(0);
            this.txt白细胞.Name = "txt白细胞";
            this.txt白细胞.Size = new System.Drawing.Size(159, 20);
            this.txt白细胞.TabIndex = 160;
            this.txt白细胞.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt血红蛋白
            // 
            this.txt血红蛋白.Lbl1Size = new System.Drawing.Size(55, 18);
            this.txt血红蛋白.Lbl1Text = "血红蛋白";
            this.txt血红蛋白.Lbl2Size = new System.Drawing.Size(35, 18);
            this.txt血红蛋白.Lbl2Text = "g/L";
            this.txt血红蛋白.Location = new System.Drawing.Point(100, 61);
            this.txt血红蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt血红蛋白.Name = "txt血红蛋白";
            this.txt血红蛋白.Size = new System.Drawing.Size(165, 20);
            this.txt血红蛋白.TabIndex = 159;
            this.txt血红蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt血常规其他
            // 
            this.txt血常规其他.Location = new System.Drawing.Point(629, 61);
            this.txt血常规其他.Name = "txt血常规其他";
            this.txt血常规其他.Size = new System.Drawing.Size(74, 20);
            this.txt血常规其他.StyleController = this.Layout1;
            this.txt血常规其他.TabIndex = 158;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(100, 297);
            this.txt服药依从性.Margin = new System.Windows.Forms.Padding(0);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "请选择";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(122, 20);
            this.txt服药依从性.StyleController = this.Layout1;
            this.txt服药依从性.TabIndex = 125;
            // 
            // txt分级管理
            // 
            this.txt分级管理.Location = new System.Drawing.Point(485, -363);
            this.txt分级管理.Name = "txt分级管理";
            this.txt分级管理.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt分级管理.Properties.Items.AddRange(new object[] {
            "一级",
            "二级",
            "三级"});
            this.txt分级管理.Size = new System.Drawing.Size(221, 20);
            this.txt分级管理.StyleController = this.Layout1;
            this.txt分级管理.TabIndex = 157;
            // 
            // sbtn便捷录入备注
            // 
            this.sbtn便捷录入备注.Location = new System.Drawing.Point(499, 720);
            this.sbtn便捷录入备注.Name = "sbtn便捷录入备注";
            this.sbtn便捷录入备注.Size = new System.Drawing.Size(207, 22);
            this.sbtn便捷录入备注.StyleController = this.Layout1;
            this.sbtn便捷录入备注.TabIndex = 156;
            this.sbtn便捷录入备注.Text = "便捷录入备注";
            this.sbtn便捷录入备注.Click += new System.EventHandler(this.sbtn便捷录入备注_Click);
            // 
            // fl家族史
            // 
            this.fl家族史.Controls.Add(this.chk高血压);
            this.fl家族史.Controls.Add(this.chk冠心病);
            this.fl家族史.Controls.Add(this.chk脑卒中);
            this.fl家族史.Controls.Add(this.chk糖尿病);
            this.fl家族史.Controls.Add(this.chk以上都无);
            this.fl家族史.Controls.Add(this.chk不详);
            this.fl家族史.Controls.Add(this.chk拒答);
            this.fl家族史.Location = new System.Drawing.Point(86, -336);
            this.fl家族史.Name = "fl家族史";
            this.fl家族史.Size = new System.Drawing.Size(620, 26);
            this.fl家族史.TabIndex = 154;
            // 
            // chk高血压
            // 
            this.chk高血压.Location = new System.Drawing.Point(1, 1);
            this.chk高血压.Margin = new System.Windows.Forms.Padding(1);
            this.chk高血压.Name = "chk高血压";
            this.chk高血压.Properties.Caption = "高血压";
            this.chk高血压.Size = new System.Drawing.Size(65, 19);
            this.chk高血压.TabIndex = 1;
            this.chk高血压.Tag = "1";
            // 
            // chk冠心病
            // 
            this.chk冠心病.Location = new System.Drawing.Point(68, 1);
            this.chk冠心病.Margin = new System.Windows.Forms.Padding(1);
            this.chk冠心病.Name = "chk冠心病";
            this.chk冠心病.Properties.Caption = "冠心病";
            this.chk冠心病.Size = new System.Drawing.Size(65, 19);
            this.chk冠心病.TabIndex = 2;
            this.chk冠心病.Tag = "2";
            // 
            // chk脑卒中
            // 
            this.chk脑卒中.Location = new System.Drawing.Point(135, 1);
            this.chk脑卒中.Margin = new System.Windows.Forms.Padding(1);
            this.chk脑卒中.Name = "chk脑卒中";
            this.chk脑卒中.Properties.Caption = "脑卒中";
            this.chk脑卒中.Size = new System.Drawing.Size(65, 19);
            this.chk脑卒中.TabIndex = 2;
            this.chk脑卒中.Tag = "3";
            // 
            // chk糖尿病
            // 
            this.chk糖尿病.Location = new System.Drawing.Point(202, 1);
            this.chk糖尿病.Margin = new System.Windows.Forms.Padding(1);
            this.chk糖尿病.Name = "chk糖尿病";
            this.chk糖尿病.Properties.Caption = "糖尿病";
            this.chk糖尿病.Size = new System.Drawing.Size(65, 19);
            this.chk糖尿病.TabIndex = 2;
            this.chk糖尿病.Tag = "4";
            // 
            // chk以上都无
            // 
            this.chk以上都无.Location = new System.Drawing.Point(269, 1);
            this.chk以上都无.Margin = new System.Windows.Forms.Padding(1);
            this.chk以上都无.Name = "chk以上都无";
            this.chk以上都无.Properties.Caption = "以上都无";
            this.chk以上都无.Size = new System.Drawing.Size(78, 19);
            this.chk以上都无.TabIndex = 2;
            this.chk以上都无.Tag = "5";
            this.chk以上都无.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // chk不详
            // 
            this.chk不详.Location = new System.Drawing.Point(349, 1);
            this.chk不详.Margin = new System.Windows.Forms.Padding(1);
            this.chk不详.Name = "chk不详";
            this.chk不详.Properties.Caption = "不详";
            this.chk不详.Size = new System.Drawing.Size(61, 19);
            this.chk不详.TabIndex = 2;
            this.chk不详.Tag = "6";
            this.chk不详.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // chk拒答
            // 
            this.chk拒答.Location = new System.Drawing.Point(412, 1);
            this.chk拒答.Margin = new System.Windows.Forms.Padding(1);
            this.chk拒答.Name = "chk拒答";
            this.chk拒答.Properties.Caption = "拒答";
            this.chk拒答.Size = new System.Drawing.Size(61, 19);
            this.chk拒答.TabIndex = 2;
            this.chk拒答.Tag = "7";
            this.chk拒答.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // fl并发症
            // 
            this.fl并发症.Controls.Add(this.chk无并发症);
            this.fl并发症.Controls.Add(this.checkEdit2);
            this.fl并发症.Controls.Add(this.checkEdit3);
            this.fl并发症.Controls.Add(this.checkEdit4);
            this.fl并发症.Controls.Add(this.checkEdit22);
            this.fl并发症.Controls.Add(this.checkEdit23);
            this.fl并发症.Controls.Add(this.checkEdit24);
            this.fl并发症.Controls.Add(this.checkEdit5);
            this.fl并发症.Controls.Add(this.textEdit2);
            this.fl并发症.Location = new System.Drawing.Point(86, -255);
            this.fl并发症.Name = "fl并发症";
            this.fl并发症.Size = new System.Drawing.Size(620, 67);
            this.fl并发症.TabIndex = 151;
            // 
            // chk无并发症
            // 
            this.chk无并发症.Location = new System.Drawing.Point(1, 1);
            this.chk无并发症.Margin = new System.Windows.Forms.Padding(1);
            this.chk无并发症.Name = "chk无并发症";
            this.chk无并发症.Properties.Caption = "无";
            this.chk无并发症.Size = new System.Drawing.Size(41, 19);
            this.chk无并发症.TabIndex = 0;
            this.chk无并发症.Tag = "0";
            this.chk无并发症.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(44, 1);
            this.checkEdit2.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "脑卒中（缺血、出血）";
            this.checkEdit2.Size = new System.Drawing.Size(138, 19);
            this.checkEdit2.TabIndex = 0;
            this.checkEdit2.Tag = "1";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(184, 1);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "短暂性脑缺血发作(TIA)";
            this.checkEdit3.Size = new System.Drawing.Size(151, 19);
            this.checkEdit3.TabIndex = 0;
            this.checkEdit3.Tag = "2";
            // 
            // checkEdit4
            // 
            this.fl并发症.SetFlowBreak(this.checkEdit4, true);
            this.checkEdit4.Location = new System.Drawing.Point(337, 1);
            this.checkEdit4.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "心脏病（心肌梗死、心绞痛、充血性心力衰竭）";
            this.checkEdit4.Size = new System.Drawing.Size(268, 19);
            this.checkEdit4.TabIndex = 0;
            this.checkEdit4.Tag = "3";
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(1, 22);
            this.checkEdit22.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "肾脏疾病（高血压肾病、肾功能衰竭）";
            this.checkEdit22.Size = new System.Drawing.Size(230, 19);
            this.checkEdit22.TabIndex = 0;
            this.checkEdit22.Tag = "4";
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(233, 22);
            this.checkEdit23.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "眼部疾病（视网膜出血或渗出  视乳头水肿）";
            this.checkEdit23.Size = new System.Drawing.Size(255, 19);
            this.checkEdit23.TabIndex = 0;
            this.checkEdit23.Tag = "5";
            // 
            // checkEdit24
            // 
            this.fl并发症.SetFlowBreak(this.checkEdit24, true);
            this.checkEdit24.Location = new System.Drawing.Point(490, 22);
            this.checkEdit24.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "外周血管疾病";
            this.checkEdit24.Size = new System.Drawing.Size(106, 19);
            this.checkEdit24.TabIndex = 0;
            this.checkEdit24.Tag = "6";
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(1, 43);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "其他";
            this.checkEdit5.Size = new System.Drawing.Size(65, 19);
            this.checkEdit5.TabIndex = 0;
            this.checkEdit5.Tag = "90";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(70, 45);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(277, 20);
            this.textEdit2.TabIndex = 1;
            // 
            // fl既往史
            // 
            this.fl既往史.Controls.Add(this.chk无既往史);
            this.fl既往史.Controls.Add(this.chk既往冠心病);
            this.fl既往史.Controls.Add(this.chk既往心力衰竭);
            this.fl既往史.Controls.Add(this.chk连续3次);
            this.fl既往史.Controls.Add(this.checkEdit6);
            this.fl既往史.Controls.Add(this.checkEdit7);
            this.fl既往史.Controls.Add(this.checkEdit8);
            this.fl既往史.Controls.Add(this.checkEdit17);
            this.fl既往史.Controls.Add(this.checkEdit18);
            this.fl既往史.Controls.Add(this.checkEdit19);
            this.fl既往史.Controls.Add(this.checkEdit20);
            this.fl既往史.Controls.Add(this.checkEdit21);
            this.fl既往史.Controls.Add(this.chk其他);
            this.fl既往史.Controls.Add(this.textEdit1);
            this.fl既往史.Location = new System.Drawing.Point(86, -306);
            this.fl既往史.Name = "fl既往史";
            this.fl既往史.Size = new System.Drawing.Size(620, 47);
            this.fl既往史.TabIndex = 151;
            // 
            // chk无既往史
            // 
            this.chk无既往史.Location = new System.Drawing.Point(1, 1);
            this.chk无既往史.Margin = new System.Windows.Forms.Padding(1);
            this.chk无既往史.Name = "chk无既往史";
            this.chk无既往史.Properties.Caption = "无";
            this.chk无既往史.Size = new System.Drawing.Size(41, 19);
            this.chk无既往史.TabIndex = 0;
            this.chk无既往史.Tag = "0";
            this.chk无既往史.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // chk既往冠心病
            // 
            this.chk既往冠心病.Location = new System.Drawing.Point(44, 1);
            this.chk既往冠心病.Margin = new System.Windows.Forms.Padding(1);
            this.chk既往冠心病.Name = "chk既往冠心病";
            this.chk既往冠心病.Properties.Caption = "冠心病";
            this.chk既往冠心病.Size = new System.Drawing.Size(61, 19);
            this.chk既往冠心病.TabIndex = 0;
            this.chk既往冠心病.Tag = "1";
            // 
            // chk既往心力衰竭
            // 
            this.chk既往心力衰竭.Location = new System.Drawing.Point(107, 1);
            this.chk既往心力衰竭.Margin = new System.Windows.Forms.Padding(1);
            this.chk既往心力衰竭.Name = "chk既往心力衰竭";
            this.chk既往心力衰竭.Properties.Caption = "心力衰竭";
            this.chk既往心力衰竭.Size = new System.Drawing.Size(75, 19);
            this.chk既往心力衰竭.TabIndex = 0;
            this.chk既往心力衰竭.Tag = "2";
            // 
            // chk连续3次
            // 
            this.chk连续3次.Location = new System.Drawing.Point(184, 1);
            this.chk连续3次.Margin = new System.Windows.Forms.Padding(1);
            this.chk连续3次.Name = "chk连续3次";
            this.chk连续3次.Properties.Caption = "脑血管病";
            this.chk连续3次.Size = new System.Drawing.Size(75, 19);
            this.chk连续3次.TabIndex = 0;
            this.chk连续3次.Tag = "3";
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(261, 1);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "外周血管病";
            this.checkEdit6.Size = new System.Drawing.Size(85, 19);
            this.checkEdit6.TabIndex = 0;
            this.checkEdit6.Tag = "4";
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(348, 1);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "糖尿病";
            this.checkEdit7.Size = new System.Drawing.Size(61, 19);
            this.checkEdit7.TabIndex = 0;
            this.checkEdit7.Tag = "5";
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(411, 1);
            this.checkEdit8.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "痛风";
            this.checkEdit8.Size = new System.Drawing.Size(51, 19);
            this.checkEdit8.TabIndex = 0;
            this.checkEdit8.Tag = "6";
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(464, 1);
            this.checkEdit17.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "血脂异常";
            this.checkEdit17.Size = new System.Drawing.Size(75, 19);
            this.checkEdit17.TabIndex = 0;
            this.checkEdit17.Tag = "7";
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(1, 22);
            this.checkEdit18.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "支气管哮喘";
            this.checkEdit18.Size = new System.Drawing.Size(85, 19);
            this.checkEdit18.TabIndex = 0;
            this.checkEdit18.Tag = "8";
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(88, 22);
            this.checkEdit19.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "睡眠呼吸暂停征";
            this.checkEdit19.Size = new System.Drawing.Size(110, 19);
            this.checkEdit19.TabIndex = 0;
            this.checkEdit19.Tag = "9";
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(200, 22);
            this.checkEdit20.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "肾病";
            this.checkEdit20.Size = new System.Drawing.Size(51, 19);
            this.checkEdit20.TabIndex = 0;
            this.checkEdit20.Tag = "10";
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(253, 22);
            this.checkEdit21.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "甲状腺疾病";
            this.checkEdit21.Size = new System.Drawing.Size(85, 19);
            this.checkEdit21.TabIndex = 0;
            this.checkEdit21.Tag = "11";
            // 
            // chk其他
            // 
            this.chk其他.Location = new System.Drawing.Point(340, 22);
            this.chk其他.Margin = new System.Windows.Forms.Padding(1);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Caption = "其他疾病";
            this.chk其他.Size = new System.Drawing.Size(75, 19);
            this.chk其他.TabIndex = 0;
            this.chk其他.Tag = "90";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(419, 24);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(133, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // sbtnFingerPrint
            // 
            this.sbtnFingerPrint.Location = new System.Drawing.Point(190, 720);
            this.sbtnFingerPrint.Name = "sbtnFingerPrint";
            this.sbtnFingerPrint.Size = new System.Drawing.Size(68, 22);
            this.sbtnFingerPrint.StyleController = this.Layout1;
            this.sbtnFingerPrint.TabIndex = 150;
            this.sbtnFingerPrint.Text = "更新指纹";
            this.sbtnFingerPrint.Click += new System.EventHandler(this.sbtnFingerPrint_Click);
            // 
            // gc药物调整
            // 
            this.gc药物调整.Location = new System.Drawing.Point(6, 483);
            this.gc药物调整.MainView = this.gv药物调整;
            this.gc药物调整.Name = "gc药物调整";
            this.gc药物调整.Size = new System.Drawing.Size(700, 66);
            this.gc药物调整.TabIndex = 149;
            this.gc药物调整.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv药物调整});
            // 
            // gv药物调整
            // 
            this.gv药物调整.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gv药物调整.GridControl = this.gc药物调整;
            this.gv药物调整.Name = "gv药物调整";
            this.gv药物调整.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "药物名称";
            this.gridColumn5.FieldName = "药物名称";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "用法";
            this.gridColumn6.FieldName = "用法";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "个人档案编号";
            this.gridColumn7.FieldName = "个人档案编号";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "创建时间";
            this.gridColumn8.FieldName = "创建时间";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Location = new System.Drawing.Point(342, 453);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(364, 26);
            this.layoutControl1.TabIndex = 148;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(364, 26);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // btn删除药物调整
            // 
            this.btn删除药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物调整.Image")));
            this.btn删除药物调整.Location = new System.Drawing.Point(284, 453);
            this.btn删除药物调整.Name = "btn删除药物调整";
            this.btn删除药物调整.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物调整.StyleController = this.Layout1;
            this.btn删除药物调整.TabIndex = 147;
            this.btn删除药物调整.Tag = "2";
            this.btn删除药物调整.Text = "删除";
            // 
            // btn添加药物调整
            // 
            this.btn添加药物调整.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物调整.Image")));
            this.btn添加药物调整.Location = new System.Drawing.Point(226, 453);
            this.btn添加药物调整.Name = "btn添加药物调整";
            this.btn添加药物调整.Size = new System.Drawing.Size(54, 22);
            this.btn添加药物调整.StyleController = this.Layout1;
            this.btn添加药物调整.TabIndex = 146;
            this.btn添加药物调整.Tag = "0";
            this.btn添加药物调整.Text = "添加";
            // 
            // txt转诊结果
            // 
            this.txt转诊结果.Enabled = false;
            this.txt转诊结果.Location = new System.Drawing.Point(468, 672);
            this.txt转诊结果.Name = "txt转诊结果";
            this.txt转诊结果.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt转诊结果.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt转诊结果.Properties.NullText = "";
            this.txt转诊结果.Size = new System.Drawing.Size(238, 20);
            this.txt转诊结果.StyleController = this.Layout1;
            this.txt转诊结果.TabIndex = 145;
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(353, 720);
            this.txt备注.Name = "txt备注";
            this.txt备注.Size = new System.Drawing.Size(142, 20);
            this.txt备注.StyleController = this.Layout1;
            this.txt备注.TabIndex = 144;
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Enabled = false;
            this.txt转诊联系电话.Location = new System.Drawing.Point(276, 672);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Size = new System.Drawing.Size(97, 20);
            this.txt转诊联系电话.StyleController = this.Layout1;
            this.txt转诊联系电话.TabIndex = 142;
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Enabled = false;
            this.txt转诊联系人.Location = new System.Drawing.Point(97, 672);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Size = new System.Drawing.Size(85, 20);
            this.txt转诊联系人.StyleController = this.Layout1;
            this.txt转诊联系人.TabIndex = 141;
            // 
            // radio药物调整
            // 
            this.radio药物调整.EditValue = "2";
            this.radio药物调整.Location = new System.Drawing.Point(99, 453);
            this.radio药物调整.Name = "radio药物调整";
            this.radio药物调整.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio药物调整.Size = new System.Drawing.Size(123, 26);
            this.radio药物调整.StyleController = this.Layout1;
            this.radio药物调整.TabIndex = 140;
            this.radio药物调整.SelectedIndexChanged += new System.EventHandler(this.radio药物调整_SelectedIndexChanged);
            // 
            // radio管理措施
            // 
            this.radio管理措施.EditValue = "1";
            this.radio管理措施.Location = new System.Drawing.Point(98, 553);
            this.radio管理措施.Name = "radio管理措施";
            this.radio管理措施.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "常规随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "第1次控制不满意2周随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "两次控制不满意转诊随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "紧急转诊")});
            this.radio管理措施.Size = new System.Drawing.Size(608, 56);
            this.radio管理措施.StyleController = this.Layout1;
            this.radio管理措施.TabIndex = 139;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(351, -419);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Size = new System.Drawing.Size(355, 20);
            this.txt联系电话.StyleController = this.Layout1;
            this.txt联系电话.TabIndex = 136;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(86, -419);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Size = new System.Drawing.Size(167, 20);
            this.txt身份证号.StyleController = this.Layout1;
            this.txt身份证号.TabIndex = 131;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(351, -443);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Size = new System.Drawing.Size(355, 20);
            this.txt出生日期.StyleController = this.Layout1;
            this.txt出生日期.TabIndex = 130;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(86, -443);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Size = new System.Drawing.Size(167, 20);
            this.txt性别.StyleController = this.Layout1;
            this.txt性别.TabIndex = 129;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(351, -467);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Size = new System.Drawing.Size(355, 20);
            this.txt姓名.StyleController = this.Layout1;
            this.txt姓名.TabIndex = 128;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(86, -467);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Size = new System.Drawing.Size(167, 20);
            this.txt个人档案号.StyleController = this.Layout1;
            this.txt个人档案号.TabIndex = 127;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.Location = new System.Drawing.Point(189, 10);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(98, 20);
            this.txt摄盐情况2.StyleController = this.Layout1;
            this.txt摄盐情况2.TabIndex = 126;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.Location = new System.Drawing.Point(96, 10);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(79, 20);
            this.txt摄盐情况1.StyleController = this.Layout1;
            this.txt摄盐情况1.TabIndex = 125;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(6, 350);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(700, 70);
            this.gcDetail.TabIndex = 92;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物.Image")));
            this.btn删除药物.Location = new System.Drawing.Point(284, 321);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物.StyleController = this.Layout1;
            this.btn删除药物.TabIndex = 95;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物.Image")));
            this.btn添加药物.Location = new System.Drawing.Point(226, 321);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(54, 22);
            this.btn添加药物.StyleController = this.Layout1;
            this.btn添加药物.TabIndex = 94;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(86, -363);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt发生时间.Size = new System.Drawing.Size(72, 20);
            this.txt发生时间.StyleController = this.Layout1;
            this.txt发生时间.TabIndex = 124;
            this.txt发生时间.EditValueChanged += new System.EventHandler(this.txt发生时间_EditValueChanged);
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(97, 696);
            this.txt下次随访时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt下次随访时间.Size = new System.Drawing.Size(161, 20);
            this.txt下次随访时间.StyleController = this.Layout1;
            this.txt下次随访时间.TabIndex = 76;
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(99, 321);
            this.radio用药情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio用药情况.Size = new System.Drawing.Size(123, 25);
            this.radio用药情况.StyleController = this.Layout1;
            this.radio用药情况.TabIndex = 69;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // fl症状
            // 
            this.fl症状.AutoScroll = true;
            this.fl症状.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fl症状.Controls.Add(this.check无症状);
            this.fl症状.Controls.Add(this.checkEdit9);
            this.fl症状.Controls.Add(this.checkEdit10);
            this.fl症状.Controls.Add(this.checkEdit11);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Controls.Add(this.checkEdit15);
            this.fl症状.Controls.Add(this.checkEdit16);
            this.fl症状.Controls.Add(this.ch症状其他);
            this.fl症状.Controls.Add(this.txt症状其他);
            this.fl症状.Location = new System.Drawing.Point(86, -184);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(620, 47);
            this.fl症状.TabIndex = 27;
            // 
            // check无症状
            // 
            this.check无症状.Location = new System.Drawing.Point(1, 1);
            this.check无症状.Margin = new System.Windows.Forms.Padding(1);
            this.check无症状.Name = "check无症状";
            this.check无症状.Properties.Caption = "1.无症状";
            this.check无症状.Size = new System.Drawing.Size(75, 19);
            this.check无症状.TabIndex = 0;
            this.check无症状.Tag = "0";
            this.check无症状.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(78, 1);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "2.头晕头疼";
            this.checkEdit9.Size = new System.Drawing.Size(93, 19);
            this.checkEdit9.TabIndex = 1;
            this.checkEdit9.Tag = "1";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(173, 1);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "3.恶心呕吐";
            this.checkEdit10.Size = new System.Drawing.Size(87, 19);
            this.checkEdit10.TabIndex = 2;
            this.checkEdit10.Tag = "2";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(262, 1);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "4.眼花耳鸣";
            this.checkEdit11.Size = new System.Drawing.Size(85, 19);
            this.checkEdit11.TabIndex = 3;
            this.checkEdit11.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(349, 1);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "5.呼吸困难";
            this.checkEdit12.Size = new System.Drawing.Size(91, 19);
            this.checkEdit12.TabIndex = 4;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(442, 1);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "6.心悸胸闷";
            this.checkEdit13.Size = new System.Drawing.Size(93, 19);
            this.checkEdit13.TabIndex = 5;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(1, 24);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "7.鼻衄出血不止";
            this.checkEdit14.Size = new System.Drawing.Size(109, 19);
            this.checkEdit14.TabIndex = 6;
            this.checkEdit14.Tag = "6";
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(112, 24);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "8.四肢发麻";
            this.checkEdit15.Size = new System.Drawing.Size(88, 19);
            this.checkEdit15.TabIndex = 7;
            this.checkEdit15.Tag = "7";
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(202, 24);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "9.下肢水肿";
            this.checkEdit16.Size = new System.Drawing.Size(90, 19);
            this.checkEdit16.TabIndex = 8;
            this.checkEdit16.Tag = "8";
            // 
            // ch症状其他
            // 
            this.ch症状其他.Location = new System.Drawing.Point(294, 24);
            this.ch症状其他.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.ch症状其他.Name = "ch症状其他";
            this.ch症状其他.Properties.Caption = "10.其他";
            this.ch症状其他.Size = new System.Drawing.Size(77, 19);
            this.ch症状其他.TabIndex = 9;
            this.ch症状其他.Tag = "90";
            // 
            // txt症状其他
            // 
            this.txt症状其他.Location = new System.Drawing.Point(373, 24);
            this.txt症状其他.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.txt症状其他.Name = "txt症状其他";
            this.txt症状其他.Size = new System.Drawing.Size(139, 20);
            this.txt症状其他.TabIndex = 10;
            // 
            // radio随访方式
            // 
            this.radio随访方式.EditValue = "-1";
            this.radio随访方式.Location = new System.Drawing.Point(256, -365);
            this.radio随访方式.Margin = new System.Windows.Forms.Padding(0);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话")});
            this.radio随访方式.Size = new System.Drawing.Size(131, 25);
            this.radio随访方式.StyleController = this.Layout1;
            this.radio随访方式.TabIndex = 21;
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(96, -111);
            this.txt血压值.Margin = new System.Windows.Forms.Padding(0);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(119, 20);
            this.txt血压值.TabIndex = 53;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(20, 14);
            this.txt体重.Lbl2Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(264, -111);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(118, 20);
            this.txt体重.TabIndex = 90;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "CM";
            this.txt身高.Location = new System.Drawing.Point(431, -111);
            this.txt身高.Margin = new System.Windows.Forms.Padding(0);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(272, 20);
            this.txt身高.TabIndex = 45;
            this.txt身高.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(96, -87);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(119, 20);
            this.txt体质指数.TabIndex = 91;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt心率
            // 
            this.txt心率.Location = new System.Drawing.Point(264, -87);
            this.txt心率.Margin = new System.Windows.Forms.Padding(0);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(118, 20);
            this.txt心率.StyleController = this.Layout1;
            this.txt心率.TabIndex = 65;
            // 
            // txt体征其他
            // 
            this.txt体征其他.Location = new System.Drawing.Point(431, -87);
            this.txt体征其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt体征其他.Name = "txt体征其他";
            this.txt体征其他.Size = new System.Drawing.Size(272, 20);
            this.txt体征其他.StyleController = this.Layout1;
            this.txt体征其他.TabIndex = 92;
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt日吸烟量.Lbl1Text = "    /";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(96, -38);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(191, 20);
            this.txt日吸烟量.TabIndex = 93;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt饮酒情况.Lbl1Text = "    /";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(385, -38);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(318, 20);
            this.txt饮酒情况.TabIndex = 94;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt运动
            // 
            this.txt运动.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt运动.Lbl1Text = "次/周";
            this.txt运动.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动.Lbl2Text = "分钟/次";
            this.txt运动.Location = new System.Drawing.Point(96, -14);
            this.txt运动.Margin = new System.Windows.Forms.Padding(0);
            this.txt运动.Name = "txt运动";
            this.txt运动.Size = new System.Drawing.Size(191, 20);
            this.txt运动.TabIndex = 95;
            this.txt运动.Txt1EditValue = null;
            this.txt运动.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt运动.Txt2EditValue = null;
            this.txt运动.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt指导运动
            // 
            this.txt指导运动.Lbl1Size = new System.Drawing.Size(33, 14);
            this.txt指导运动.Lbl1Text = "次/周";
            this.txt指导运动.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt指导运动.Lbl2Text = "分钟/次";
            this.txt指导运动.Location = new System.Drawing.Point(385, -14);
            this.txt指导运动.Margin = new System.Windows.Forms.Padding(0);
            this.txt指导运动.Name = "txt指导运动";
            this.txt指导运动.Size = new System.Drawing.Size(318, 20);
            this.txt指导运动.TabIndex = 97;
            this.txt指导运动.Txt1EditValue = null;
            this.txt指导运动.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt指导运动.Txt2EditValue = null;
            this.txt指导运动.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt其他1
            // 
            this.txt其他1.Location = new System.Drawing.Point(555, 181);
            this.txt其他1.Margin = new System.Windows.Forms.Padding(0);
            this.txt其他1.Name = "txt其他1";
            this.txt其他1.Size = new System.Drawing.Size(148, 20);
            this.txt其他1.StyleController = this.Layout1;
            this.txt其他1.TabIndex = 105;
            this.txt其他1.ToolTip = "123";
            this.txt其他1.ToolTipTitle = "1";
            // 
            // radio不良反应
            // 
            this.radio不良反应.EditValue = "1";
            this.radio不良反应.Location = new System.Drawing.Point(99, 424);
            this.radio不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.radio不良反应.Name = "radio不良反应";
            this.radio不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio不良反应.Size = new System.Drawing.Size(123, 25);
            this.radio不良反应.StyleController = this.Layout1;
            this.radio不良反应.TabIndex = 107;
            this.radio不良反应.SelectedIndexChanged += new System.EventHandler(this.radio不良反应_SelectedIndexChanged);
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Location = new System.Drawing.Point(320, 426);
            this.txt药物副作用详述.Margin = new System.Windows.Forms.Padding(0);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(386, 20);
            this.txt药物副作用详述.StyleController = this.Layout1;
            this.txt药物副作用详述.TabIndex = 109;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(97, 613);
            this.txt医生建议.Margin = new System.Windows.Forms.Padding(0);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(609, 26);
            this.txt医生建议.StyleController = this.Layout1;
            this.txt医生建议.TabIndex = 113;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // radio转诊情况
            // 
            this.radio转诊情况.EditValue = "2";
            this.radio转诊情况.Location = new System.Drawing.Point(97, 643);
            this.radio转诊情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio转诊情况.Name = "radio转诊情况";
            this.radio转诊情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio转诊情况.Size = new System.Drawing.Size(124, 25);
            this.radio转诊情况.StyleController = this.Layout1;
            this.radio转诊情况.TabIndex = 115;
            this.radio转诊情况.SelectedIndexChanged += new System.EventHandler(this.radio转诊情况_SelectedIndexChanged);
            // 
            // txt转诊科别
            // 
            this.txt转诊科别.Enabled = false;
            this.txt转诊科别.Location = new System.Drawing.Point(468, 645);
            this.txt转诊科别.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊科别.Name = "txt转诊科别";
            this.txt转诊科别.Size = new System.Drawing.Size(238, 20);
            this.txt转诊科别.StyleController = this.Layout1;
            this.txt转诊科别.TabIndex = 116;
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(353, 696);
            this.txt医生签名.Margin = new System.Windows.Forms.Padding(0);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(353, 20);
            this.txt医生签名.StyleController = this.Layout1;
            this.txt医生签名.TabIndex = 119;
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(100, 780);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(132, 14);
            this.lbl创建时间.StyleController = this.Layout1;
            this.lbl创建时间.TabIndex = 88;
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(560, 780);
            this.lab创建机构.MaximumSize = new System.Drawing.Size(140, 0);
            this.lab创建机构.MinimumSize = new System.Drawing.Size(140, 0);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(140, 14);
            this.lab创建机构.StyleController = this.Layout1;
            this.lab创建机构.TabIndex = 86;
            this.lab创建机构.Text = "创建机构";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Location = new System.Drawing.Point(330, 780);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(132, 14);
            this.lbl最近更新时间.StyleController = this.Layout1;
            this.lbl最近更新时间.TabIndex = 84;
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(100, 798);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(132, 14);
            this.lab创建人.StyleController = this.Layout1;
            this.lab创建人.TabIndex = 82;
            this.lab创建人.Text = "录入人";
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Enabled = false;
            this.txt转诊原因.Location = new System.Drawing.Point(266, 645);
            this.txt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(107, 20);
            this.txt转诊原因.StyleController = this.Layout1;
            this.txt转诊原因.TabIndex = 118;
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(330, 798);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(132, 14);
            this.lab最近修改人.StyleController = this.Layout1;
            this.lab最近修改人.TabIndex = 122;
            this.lab最近修改人.Text = "最近更新人";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(560, 798);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(146, 14);
            this.lab当前所属机构.StyleController = this.Layout1;
            this.lab当前所属机构.TabIndex = 123;
            this.lab当前所属机构.Text = "当前所属机构";
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "请选择";
            this.txt心理调整.Location = new System.Drawing.Point(385, 10);
            this.txt心理调整.Margin = new System.Windows.Forms.Padding(0);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(318, 20);
            this.txt心理调整.StyleController = this.Layout1;
            this.txt心理调整.TabIndex = 101;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(86, -395);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Size = new System.Drawing.Size(620, 26);
            this.txt居住地址.StyleController = this.Layout1;
            this.txt居住地址.TabIndex = 132;
            this.txt居住地址.UseOptimizedRendering = true;
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(97, 720);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Properties.ReadOnly = true;
            this.txt居民签名.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.txt居民签名.Size = new System.Drawing.Size(89, 56);
            this.txt居民签名.StyleController = this.Layout1;
            this.txt居民签名.TabIndex = 143;
            this.txt居民签名.TabStop = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "高血压患者分级管理";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem34,
            this.layoutControlItem4,
            this.layoutControlItem24,
            this.emptySpaceItem3,
            this.layout添加药物,
            this.layout删除药物,
            this.layout药物列表,
            this.layoutControlItem28,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem32,
            this.layoutControlItem30,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem31,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem3,
            this.layoutControlItem27,
            this.layoutControlItem43,
            this.layoutControlItem58,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layout添加药物调整,
            this.layout删除药物调整,
            this.layoutControlItem52,
            this.layout药物调整列表,
            this.layoutControlItem23,
            this.layoutControlItem50,
            this.lcl失访,
            this.layoutControlItem55,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem42,
            this.layoutControlItem38,
            this.layoutControlItem41,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlGroup4,
            this.layoutControlItem40,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -498);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(712, 1316);
            this.layoutControlGroup1.Text = "高血压患者分级管理";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.fl症状;
            this.layoutControlItem5.CustomizationFormText = "症状";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 283);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(203, 51);
            this.layoutControlItem5.Name = "flowLayoutPanel3item";
            this.layoutControlItem5.Size = new System.Drawing.Size(704, 51);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "症状";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio随访方式;
            this.layoutControlItem6.CustomizationFormText = "随访方式";
            this.layoutControlItem6.Location = new System.Drawing.Point(156, 102);
            this.layoutControlItem6.Name = "radioGroup1item";
            this.layoutControlItem6.Size = new System.Drawing.Size(229, 29);
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "随访方式";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.lab创建人;
            this.layoutControlItem34.CustomizationFormText = "labelControl56item";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 1265);
            this.layoutControlItem34.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(129, 18);
            this.layoutControlItem34.Name = "labelControl56item";
            this.layoutControlItem34.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "录入人";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.radio用药情况;
            this.layoutControlItem4.CustomizationFormText = "用药情况";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 788);
            this.layoutControlItem4.Name = "radioGroup8item";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(220, 29);
            this.layoutControlItem4.Text = "用药情况";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt发生时间;
            this.layoutControlItem24.CustomizationFormText = "随访日期";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem24.Size = new System.Drawing.Size(156, 29);
            this.layoutControlItem24.Tag = "check";
            this.layoutControlItem24.Text = "随访日期";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(336, 788);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(368, 29);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layoutControlItem29";
            this.layout添加药物.Location = new System.Drawing.Point(220, 788);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(58, 29);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layoutControlItem31";
            this.layout删除药物.Location = new System.Drawing.Point(278, 788);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(58, 29);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = "                   ";
            this.layout药物列表.Location = new System.Drawing.Point(0, 817);
            this.layout药物列表.MinSize = new System.Drawing.Size(193, 74);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(704, 74);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = "                   ";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.lbl创建时间;
            this.layoutControlItem28.CustomizationFormText = "labelControl62item";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 1247);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(230, 18);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(211, 18);
            this.layoutControlItem28.Name = "labelControl62item";
            this.layoutControlItem28.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "录入时间";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.lab最近修改人;
            this.layoutControlItem61.CustomizationFormText = "labelControl47item";
            this.layoutControlItem61.Location = new System.Drawing.Point(230, 1265);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(105, 18);
            this.layoutControlItem61.Name = "labelControl47item";
            this.layoutControlItem61.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "最近更新人";
            this.layoutControlItem61.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.lab当前所属机构;
            this.layoutControlItem62.CustomizationFormText = "labelControl52item";
            this.layoutControlItem62.Location = new System.Drawing.Point(460, 1265);
            this.layoutControlItem62.Name = "labelControl52item";
            this.layoutControlItem62.Size = new System.Drawing.Size(244, 18);
            this.layoutControlItem62.Text = "当前所属机构";
            this.layoutControlItem62.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.lbl最近更新时间;
            this.layoutControlItem32.CustomizationFormText = "labelControl58item";
            this.layoutControlItem32.Location = new System.Drawing.Point(230, 1247);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(230, 220);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(215, 18);
            this.layoutControlItem32.Name = "labelControl58item";
            this.layoutControlItem32.Size = new System.Drawing.Size(230, 18);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "最近更新时间";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.lab创建机构;
            this.layoutControlItem30.CustomizationFormText = "labelControl60item";
            this.layoutControlItem30.Location = new System.Drawing.Point(460, 1247);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(189, 18);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(141, 18);
            this.layoutControlItem30.Name = "labelControl60item";
            this.layoutControlItem30.Size = new System.Drawing.Size(244, 18);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "创建机构";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "体征";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 334);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(704, 73);
            this.layoutControlGroup2.Text = "体征";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt血压值;
            this.layoutControlItem7.CustomizationFormText = "血压";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "UCTxtLblTxtLblitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem7.Tag = "check";
            this.layoutControlItem7.Text = "血压";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt体重;
            this.layoutControlItem8.CustomizationFormText = "体重";
            this.layoutControlItem8.Location = new System.Drawing.Point(210, 0);
            this.layoutControlItem8.Name = "item0";
            this.layoutControlItem8.Size = new System.Drawing.Size(167, 24);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "体重";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt身高;
            this.layoutControlItem9.CustomizationFormText = "身高";
            this.layoutControlItem9.Location = new System.Drawing.Point(377, 0);
            this.layoutControlItem9.Name = "UCTxtLblitem";
            this.layoutControlItem9.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem9.Tag = "";
            this.layoutControlItem9.Text = "身高";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt体质指数;
            this.layoutControlItem10.CustomizationFormText = "体质指数";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "item1";
            this.layoutControlItem10.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem10.Text = "体质指数";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt心率;
            this.layoutControlItem11.CustomizationFormText = "心率";
            this.layoutControlItem11.Location = new System.Drawing.Point(210, 24);
            this.layoutControlItem11.Name = "textEdit3item";
            this.layoutControlItem11.Size = new System.Drawing.Size(167, 24);
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "心率";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt体征其他;
            this.layoutControlItem12.CustomizationFormText = "其他";
            this.layoutControlItem12.Location = new System.Drawing.Point(377, 24);
            this.layoutControlItem12.Name = "textEdit2item";
            this.layoutControlItem12.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "其他";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "生活方式指导";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem29,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 407);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(704, 99);
            this.layoutControlGroup3.Text = "生活方式指导";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt日吸烟量;
            this.layoutControlItem13.CustomizationFormText = "日吸烟量";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "item2";
            this.layoutControlItem13.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "日吸烟量";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt饮酒情况;
            this.layoutControlItem14.CustomizationFormText = "饮酒情况";
            this.layoutControlItem14.Location = new System.Drawing.Point(282, 0);
            this.layoutControlItem14.Name = "item3";
            this.layoutControlItem14.Size = new System.Drawing.Size(416, 24);
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "饮酒情况";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt运动;
            this.layoutControlItem15.CustomizationFormText = "运动频率";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "item4";
            this.layoutControlItem15.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "运    动";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt指导运动;
            this.layoutControlItem16.CustomizationFormText = "每次持续时间";
            this.layoutControlItem16.Location = new System.Drawing.Point(282, 24);
            this.layoutControlItem16.Name = "item5";
            this.layoutControlItem16.Size = new System.Drawing.Size(416, 24);
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "指导运动";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt摄盐情况1;
            this.layoutControlItem29.CustomizationFormText = "摄盐情况";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(170, 24);
            this.layoutControlItem29.Text = "摄盐情况";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt摄盐情况2;
            this.layoutControlItem17.CustomizationFormText = "/";
            this.layoutControlItem17.Location = new System.Drawing.Point(170, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(112, 24);
            this.layoutControlItem17.Text = "/";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(5, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt心理调整;
            this.layoutControlItem18.CustomizationFormText = "心理调整";
            this.layoutControlItem18.Location = new System.Drawing.Point(282, 48);
            this.layoutControlItem18.Name = "dateEdit3item";
            this.layoutControlItem18.Size = new System.Drawing.Size(416, 24);
            this.layoutControlItem18.Tag = "check";
            this.layoutControlItem18.Text = "心理调整";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt个人档案号;
            this.layoutControlItem31.CustomizationFormText = "个人档案号";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem31.Text = "个人档案号";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.txt姓名;
            this.layoutControlItem33.CustomizationFormText = "姓名";
            this.layoutControlItem33.Location = new System.Drawing.Point(251, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(453, 24);
            this.layoutControlItem33.Text = "姓名";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt性别;
            this.layoutControlItem35.CustomizationFormText = "性别";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem35.Text = "性别";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.txt出生日期;
            this.layoutControlItem36.CustomizationFormText = "出生日期";
            this.layoutControlItem36.Location = new System.Drawing.Point(251, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(453, 24);
            this.layoutControlItem36.Text = "出生日期";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.txt身份证号;
            this.layoutControlItem37.CustomizationFormText = "身份证号";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem37.Text = "身份证号";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.radio转诊情况;
            this.layoutControlItem25.CustomizationFormText = "转诊情况";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 1110);
            this.layoutControlItem25.Name = "radioGroup3item";
            this.layoutControlItem25.Size = new System.Drawing.Size(219, 29);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "转诊情况";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt转诊科别;
            this.layoutControlItem26.CustomizationFormText = "机构及科别";
            this.layoutControlItem26.Location = new System.Drawing.Point(371, 1110);
            this.layoutControlItem26.Name = "textEdit4item";
            this.layoutControlItem26.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem26.Size = new System.Drawing.Size(333, 29);
            this.layoutControlItem26.Text = "机构及科别：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt下次随访时间;
            this.layoutControlItem3.CustomizationFormText = "下次随访时间";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 1163);
            this.layoutControlItem3.Name = "dateEdit2item";
            this.layoutControlItem3.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "下次随访时间";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt医生签名;
            this.layoutControlItem27.CustomizationFormText = " 随访医生签名 ";
            this.layoutControlItem27.Location = new System.Drawing.Point(256, 1163);
            this.layoutControlItem27.Name = "textEdit8item";
            this.layoutControlItem27.Size = new System.Drawing.Size(448, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = " 随访医生签名 ";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.radio药物调整;
            this.layoutControlItem43.CustomizationFormText = "用药调整意见";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 920);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(169, 30);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
            this.layoutControlItem43.Size = new System.Drawing.Size(220, 30);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Tag = "";
            this.layoutControlItem43.Text = "用药调整意见";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.txt转诊原因;
            this.layoutControlItem58.CustomizationFormText = "原因";
            this.layoutControlItem58.Location = new System.Drawing.Point(219, 1110);
            this.layoutControlItem58.Name = "textEdit7item";
            this.layoutControlItem58.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem58.Size = new System.Drawing.Size(152, 29);
            this.layoutControlItem58.Text = "原因：";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.txt转诊联系人;
            this.layoutControlItem44.CustomizationFormText = "转诊联系人：";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 1139);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(180, 24);
            this.layoutControlItem44.Text = "转诊联系人：";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.txt转诊联系电话;
            this.layoutControlItem45.CustomizationFormText = "转诊联系电话：";
            this.layoutControlItem45.Location = new System.Drawing.Point(180, 1139);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(191, 24);
            this.layoutControlItem45.Text = "转诊联系电话：";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(85, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.txt居民签名;
            this.layoutControlItem46.CustomizationFormText = "居民签名";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 1187);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(115, 60);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(184, 60);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Tag = "";
            this.layoutControlItem46.Text = "居民签名";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem47.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.txt备注;
            this.layoutControlItem47.CustomizationFormText = "备注";
            this.layoutControlItem47.Location = new System.Drawing.Point(256, 1187);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(237, 60);
            this.layoutControlItem47.Tag = "";
            this.layoutControlItem47.Text = "备注";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.txt转诊结果;
            this.layoutControlItem48.CustomizationFormText = "转诊结果：";
            this.layoutControlItem48.Location = new System.Drawing.Point(371, 1139);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(333, 24);
            this.layoutControlItem48.Text = "转诊结果：";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layout添加药物调整
            // 
            this.layout添加药物调整.Control = this.btn添加药物调整;
            this.layout添加药物调整.CustomizationFormText = "layout添加药物调整";
            this.layout添加药物调整.Location = new System.Drawing.Point(220, 920);
            this.layout添加药物调整.Name = "layout添加药物调整";
            this.layout添加药物调整.Size = new System.Drawing.Size(58, 30);
            this.layout添加药物调整.Text = "layout添加药物调整";
            this.layout添加药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物调整.TextToControlDistance = 0;
            this.layout添加药物调整.TextVisible = false;
            this.layout添加药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物调整
            // 
            this.layout删除药物调整.Control = this.btn删除药物调整;
            this.layout删除药物调整.CustomizationFormText = "layout删除药物调整";
            this.layout删除药物调整.Location = new System.Drawing.Point(278, 920);
            this.layout删除药物调整.Name = "layout删除药物调整";
            this.layout删除药物调整.Size = new System.Drawing.Size(58, 30);
            this.layout删除药物调整.Text = "layout删除药物调整";
            this.layout删除药物调整.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物调整.TextToControlDistance = 0;
            this.layout删除药物调整.TextVisible = false;
            this.layout删除药物调整.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.layoutControl1;
            this.layoutControlItem52.CustomizationFormText = "layoutControlItem52";
            this.layoutControlItem52.Location = new System.Drawing.Point(336, 920);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(368, 30);
            this.layoutControlItem52.Text = "layoutControlItem52";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem52.TextToControlDistance = 0;
            this.layoutControlItem52.TextVisible = false;
            // 
            // layout药物调整列表
            // 
            this.layout药物调整列表.Control = this.gc药物调整;
            this.layout药物调整列表.CustomizationFormText = "layout药物调整列表";
            this.layout药物调整列表.Location = new System.Drawing.Point(0, 950);
            this.layout药物调整列表.MinSize = new System.Drawing.Size(219, 70);
            this.layout药物调整列表.Name = "layout药物调整列表";
            this.layout药物调整列表.Size = new System.Drawing.Size(704, 70);
            this.layout药物调整列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物调整列表.Text = "layout药物调整列表";
            this.layout药物调整列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物调整列表.TextToControlDistance = 0;
            this.layout药物调整列表.TextVisible = false;
            this.layout药物调整列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8F);
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem23.Control = this.txt医生建议;
            this.layoutControlItem23.CustomizationFormText = "此次随访医生建议";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 1080);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(97, 30);
            this.layoutControlItem23.Name = "memoEdit1item";
            this.layoutControlItem23.Size = new System.Drawing.Size(704, 30);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "此次随访医生建议";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(86, 12);
            this.layoutControlItem23.TextToControlDistance = 5;
            this.layoutControlItem23.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.sbtnFingerPrint;
            this.layoutControlItem50.CustomizationFormText = "layoutControlItem50";
            this.layoutControlItem50.Location = new System.Drawing.Point(184, 1187);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(72, 60);
            this.layoutControlItem50.Text = "layoutControlItem50";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem50.TextToControlDistance = 0;
            this.layoutControlItem50.TextVisible = false;
            // 
            // lcl失访
            // 
            this.lcl失访.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl失访.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl失访.Control = this.fl既往史;
            this.lcl失访.CustomizationFormText = "失访原因：";
            this.lcl失访.Location = new System.Drawing.Point(0, 161);
            this.lcl失访.MinSize = new System.Drawing.Size(193, 51);
            this.lcl失访.Name = "lcl失访";
            this.lcl失访.Size = new System.Drawing.Size(704, 51);
            this.lcl失访.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl失访.Text = "既往史：";
            this.lcl失访.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl失访.TextSize = new System.Drawing.Size(75, 20);
            this.lcl失访.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.fl家族史;
            this.layoutControlItem55.CustomizationFormText = "访视情况：";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(193, 30);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(704, 30);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "家族史：";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.sbtn便捷录入备注;
            this.layoutControlItem53.CustomizationFormText = "layoutControlItem53";
            this.layoutControlItem53.Location = new System.Drawing.Point(493, 1187);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(211, 60);
            this.layoutControlItem53.Text = "layoutControlItem53";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextToControlDistance = 0;
            this.layoutControlItem53.TextVisible = false;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.fl并发症;
            this.layoutControlItem54.CustomizationFormText = "并发症：";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(193, 71);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(704, 71);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "并发症：";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt联系电话;
            this.layoutControlItem42.CustomizationFormText = "联系电话";
            this.layoutControlItem42.Location = new System.Drawing.Point(251, 48);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(453, 24);
            this.layoutControlItem42.Text = "联系电话";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.txt居住地址;
            this.layoutControlItem38.CustomizationFormText = "居住地址";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(103, 30);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(704, 30);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "居住地址";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt分级管理;
            this.layoutControlItem41.CustomizationFormText = "高血压分级";
            this.layoutControlItem41.Location = new System.Drawing.Point(385, 102);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem41.Size = new System.Drawing.Size(319, 29);
            this.layoutControlItem41.Text = "高血压分级";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.radio不良反应;
            this.layoutControlItem20.CustomizationFormText = "药物不良反应";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 891);
            this.layoutControlItem20.Name = "radioGroup2item";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
            this.layoutControlItem20.Size = new System.Drawing.Size(220, 29);
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "药物不良反应";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt药物副作用详述;
            this.layoutControlItem21.CustomizationFormText = "副作用详述";
            this.layoutControlItem21.Location = new System.Drawing.Point(220, 891);
            this.layoutControlItem21.Name = "textEdit6item";
            this.layoutControlItem21.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem21.Size = new System.Drawing.Size(484, 29);
            this.layoutControlItem21.Tag = "";
            this.layoutControlItem21.Text = "副作用详述";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "辅助检查";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem22,
            this.layoutControlItem39,
            this.layoutControlItem49,
            this.layoutControlItem2,
            this.layoutControlItem51,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem59,
            this.layoutControlItem60,
            this.layoutControlItem63,
            this.layoutControlItem64,
            this.layoutControlItem65,
            this.layoutControlItem66,
            this.layoutControlItem67,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.layoutControlItem70,
            this.layoutControlItem71,
            this.layoutControlItem72,
            this.layoutControlItem73,
            this.layoutControlItem74,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem77,
            this.layoutControlItem78,
            this.layoutControlItem80,
            this.layoutControlItem79,
            this.layoutControlItem81,
            this.layoutControlItem82,
            this.layoutControlItem83,
            this.layoutControlItem84});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 506);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(704, 258);
            this.layoutControlGroup4.Text = "辅助检查";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt其他1;
            this.layoutControlItem19.CustomizationFormText = "辅助检查";
            this.layoutControlItem19.Location = new System.Drawing.Point(457, 120);
            this.layoutControlItem19.Name = "textEdit5item";
            this.layoutControlItem19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutControlItem19.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem19.Tag = "";
            this.layoutControlItem19.Text = "其他";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt血红蛋白;
            this.layoutControlItem22.CustomizationFormText = "血常规";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem22.Text = "血常规";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.txt白细胞;
            this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
            this.layoutControlItem39.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem39.Text = "layoutControlItem39";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextToControlDistance = 0;
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txt血小板;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(423, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(168, 24);
            this.layoutControlItem49.Text = "layoutControlItem49";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextToControlDistance = 0;
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt血常规其他;
            this.layoutControlItem2.CustomizationFormText = "其他";
            this.layoutControlItem2.Location = new System.Drawing.Point(591, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(107, 24);
            this.layoutControlItem2.Text = "其他";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(24, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt尿蛋白;
            this.layoutControlItem51.CustomizationFormText = "尿常规";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem51.Text = "尿常规";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.txt尿糖;
            this.layoutControlItem56.CustomizationFormText = "layoutControlItem56";
            this.layoutControlItem56.Location = new System.Drawing.Point(230, 24);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(120, 24);
            this.layoutControlItem56.Text = "layoutControlItem56";
            this.layoutControlItem56.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem56.TextToControlDistance = 0;
            this.layoutControlItem56.TextVisible = false;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.txt尿酮体;
            this.layoutControlItem57.CustomizationFormText = "layoutControlItem57";
            this.layoutControlItem57.Location = new System.Drawing.Point(350, 24);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(126, 24);
            this.layoutControlItem57.Text = "layoutControlItem57";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem57.TextToControlDistance = 0;
            this.layoutControlItem57.TextVisible = false;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.txt尿潜血;
            this.layoutControlItem59.CustomizationFormText = "layoutControlItem59";
            this.layoutControlItem59.Location = new System.Drawing.Point(476, 24);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(115, 24);
            this.layoutControlItem59.Text = "layoutControlItem59";
            this.layoutControlItem59.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem59.TextToControlDistance = 0;
            this.layoutControlItem59.TextVisible = false;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.txt尿常规其他;
            this.layoutControlItem60.CustomizationFormText = "其他";
            this.layoutControlItem60.Location = new System.Drawing.Point(591, 24);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(107, 24);
            this.layoutControlItem60.Text = "其他";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(24, 14);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.txt血清谷丙转氨酶;
            this.layoutControlItem63.CustomizationFormText = "肝功能";
            this.layoutControlItem63.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem63.Text = "肝功能";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.Control = this.txt血清谷草转氨酶;
            this.layoutControlItem64.CustomizationFormText = "layoutControlItem64";
            this.layoutControlItem64.Location = new System.Drawing.Point(293, 48);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem64.Text = "layoutControlItem64";
            this.layoutControlItem64.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem64.TextToControlDistance = 0;
            this.layoutControlItem64.TextVisible = false;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.Control = this.txt总胆红素;
            this.layoutControlItem65.CustomizationFormText = "layoutControlItem65";
            this.layoutControlItem65.Location = new System.Drawing.Point(534, 48);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(164, 24);
            this.layoutControlItem65.Text = "layoutControlItem65";
            this.layoutControlItem65.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem65.TextToControlDistance = 0;
            this.layoutControlItem65.TextVisible = false;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem66.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem66.Control = this.txt血清肌酐;
            this.layoutControlItem66.CustomizationFormText = "肾功能";
            this.layoutControlItem66.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem66.Text = "肾功能";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.txt血尿素;
            this.layoutControlItem67.CustomizationFormText = "layoutControlItem67";
            this.layoutControlItem67.Location = new System.Drawing.Point(238, 72);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(144, 24);
            this.layoutControlItem67.Text = "layoutControlItem67";
            this.layoutControlItem67.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem67.TextToControlDistance = 0;
            this.layoutControlItem67.TextVisible = false;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.txt血钾浓度;
            this.layoutControlItem68.CustomizationFormText = "layoutControlItem68";
            this.layoutControlItem68.Location = new System.Drawing.Point(382, 72);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(152, 24);
            this.layoutControlItem68.Text = "layoutControlItem68";
            this.layoutControlItem68.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem68.TextToControlDistance = 0;
            this.layoutControlItem68.TextVisible = false;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.Control = this.txt血钠浓度;
            this.layoutControlItem69.CustomizationFormText = "layoutControlItem69";
            this.layoutControlItem69.Location = new System.Drawing.Point(534, 72);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(164, 24);
            this.layoutControlItem69.Text = "layoutControlItem69";
            this.layoutControlItem69.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem69.TextToControlDistance = 0;
            this.layoutControlItem69.TextVisible = false;
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem70.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem70.Control = this.txt总胆固醇;
            this.layoutControlItem70.CustomizationFormText = "血脂";
            this.layoutControlItem70.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem70.Text = "血脂";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem70.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.Control = this.txt甘油三酯;
            this.layoutControlItem71.CustomizationFormText = "layoutControlItem71";
            this.layoutControlItem71.Location = new System.Drawing.Point(238, 96);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(144, 24);
            this.layoutControlItem71.Text = "layoutControlItem71";
            this.layoutControlItem71.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem71.TextToControlDistance = 0;
            this.layoutControlItem71.TextVisible = false;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.Control = this.txt低密度蛋白;
            this.layoutControlItem72.CustomizationFormText = "layoutControlItem72";
            this.layoutControlItem72.Location = new System.Drawing.Point(382, 96);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(152, 24);
            this.layoutControlItem72.Text = "layoutControlItem72";
            this.layoutControlItem72.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem72.TextToControlDistance = 0;
            this.layoutControlItem72.TextVisible = false;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.txt高密度蛋白;
            this.layoutControlItem73.CustomizationFormText = "layoutControlItem73";
            this.layoutControlItem73.Location = new System.Drawing.Point(534, 96);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(164, 24);
            this.layoutControlItem73.Text = "layoutControlItem73";
            this.layoutControlItem73.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem73.TextToControlDistance = 0;
            this.layoutControlItem73.TextVisible = false;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem74.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem74.Control = this.txt空腹血糖;
            this.layoutControlItem74.CustomizationFormText = "空腹血糖";
            this.layoutControlItem74.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem74.Text = "空腹血糖";
            this.layoutControlItem74.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem74.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem74.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem75.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem75.Control = this.txt同型半胱氨酸;
            this.layoutControlItem75.CustomizationFormText = "同型半胱氨酸";
            this.layoutControlItem75.Location = new System.Drawing.Point(238, 120);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem75.Text = "同型半胱氨酸";
            this.layoutControlItem75.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem40.Control = this.radio管理措施;
            this.layoutControlItem40.CustomizationFormText = "下一步管理措施";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 1020);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(141, 60);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 2, 2, 2);
            this.layoutControlItem40.Size = new System.Drawing.Size(704, 60);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Tag = "check";
            this.layoutControlItem40.Text = "下一步管理措施";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt服药依从性;
            this.layoutControlItem1.CustomizationFormText = "用药依从性";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 764);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem1.Text = "用药依从性";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(220, 764);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(484, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn关闭);
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(729, 33);
            this.panelControl1.TabIndex = 124;
            // 
            // radio心电图
            // 
            this.radio心电图.Location = new System.Drawing.Point(100, 205);
            this.radio心电图.Name = "radio心电图";
            this.radio心电图.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio心电图.Size = new System.Drawing.Size(104, 25);
            this.radio心电图.StyleController = this.Layout1;
            this.radio心电图.TabIndex = 180;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem76.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem76.Control = this.radio心电图;
            this.layoutControlItem76.CustomizationFormText = "心电图";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(199, 29);
            this.layoutControlItem76.Text = "心电图";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // radio胸片
            // 
            this.radio胸片.Location = new System.Drawing.Point(100, 263);
            this.radio胸片.Name = "radio胸片";
            this.radio胸片.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio胸片.Size = new System.Drawing.Size(104, 25);
            this.radio胸片.StyleController = this.Layout1;
            this.radio胸片.TabIndex = 181;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem77.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem77.Control = this.radio胸片;
            this.layoutControlItem77.CustomizationFormText = "胸片";
            this.layoutControlItem77.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Size = new System.Drawing.Size(199, 29);
            this.layoutControlItem77.Text = "胸片";
            this.layoutControlItem77.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem77.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem77.TextToControlDistance = 5;
            // 
            // radio超声心动图
            // 
            this.radio超声心动图.Location = new System.Drawing.Point(100, 234);
            this.radio超声心动图.Name = "radio超声心动图";
            this.radio超声心动图.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio超声心动图.Size = new System.Drawing.Size(104, 25);
            this.radio超声心动图.StyleController = this.Layout1;
            this.radio超声心动图.TabIndex = 182;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem78.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem78.Control = this.radio超声心动图;
            this.layoutControlItem78.CustomizationFormText = "超声心动图";
            this.layoutControlItem78.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Size = new System.Drawing.Size(199, 29);
            this.layoutControlItem78.Text = "超声心动图";
            this.layoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem78.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem78.TextToControlDistance = 5;
            // 
            // radio颈动脉超声
            // 
            this.radio颈动脉超声.Location = new System.Drawing.Point(455, 234);
            this.radio颈动脉超声.Name = "radio颈动脉超声";
            this.radio颈动脉超声.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio颈动脉超声.Size = new System.Drawing.Size(97, 25);
            this.radio颈动脉超声.StyleController = this.Layout1;
            this.radio颈动脉超声.TabIndex = 183;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem79.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem79.Control = this.radio颈动脉超声;
            this.layoutControlItem79.CustomizationFormText = "颈动脉超声";
            this.layoutControlItem79.Location = new System.Drawing.Point(355, 173);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Size = new System.Drawing.Size(192, 29);
            this.layoutControlItem79.Text = "颈动脉超声";
            this.layoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem79.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem79.TextToControlDistance = 5;
            // 
            // txt其他2
            // 
            this.txt其他2.Location = new System.Drawing.Point(455, 265);
            this.txt其他2.Name = "txt其他2";
            this.txt其他2.Size = new System.Drawing.Size(248, 20);
            this.txt其他2.StyleController = this.Layout1;
            this.txt其他2.TabIndex = 184;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem80.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem80.Control = this.txt其他2;
            this.layoutControlItem80.CustomizationFormText = "其他";
            this.layoutControlItem80.Location = new System.Drawing.Point(355, 202);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem80.Size = new System.Drawing.Size(343, 29);
            this.layoutControlItem80.Text = "其他";
            this.layoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem80.TextSize = new System.Drawing.Size(86, 20);
            this.layoutControlItem80.TextToControlDistance = 5;
            // 
            // txt超声心动图异常
            // 
            this.txt超声心动图异常.Location = new System.Drawing.Point(208, 236);
            this.txt超声心动图异常.Name = "txt超声心动图异常";
            this.txt超声心动图异常.Size = new System.Drawing.Size(152, 20);
            this.txt超声心动图异常.StyleController = this.Layout1;
            this.txt超声心动图异常.TabIndex = 185;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.Control = this.txt超声心动图异常;
            this.layoutControlItem81.CustomizationFormText = "layoutControlItem81";
            this.layoutControlItem81.Location = new System.Drawing.Point(199, 173);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem81.Size = new System.Drawing.Size(156, 29);
            this.layoutControlItem81.Text = "layoutControlItem81";
            this.layoutControlItem81.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem81.TextToControlDistance = 0;
            this.layoutControlItem81.TextVisible = false;
            // 
            // txt颈动脉超声异常
            // 
            this.txt颈动脉超声异常.Location = new System.Drawing.Point(556, 236);
            this.txt颈动脉超声异常.Name = "txt颈动脉超声异常";
            this.txt颈动脉超声异常.Size = new System.Drawing.Size(147, 20);
            this.txt颈动脉超声异常.StyleController = this.Layout1;
            this.txt颈动脉超声异常.TabIndex = 186;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.Control = this.txt颈动脉超声异常;
            this.layoutControlItem82.CustomizationFormText = "layoutControlItem82";
            this.layoutControlItem82.Location = new System.Drawing.Point(547, 173);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem82.Size = new System.Drawing.Size(151, 29);
            this.layoutControlItem82.Text = "layoutControlItem82";
            this.layoutControlItem82.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem82.TextToControlDistance = 0;
            this.layoutControlItem82.TextVisible = false;
            // 
            // txt胸片异常
            // 
            this.txt胸片异常.Location = new System.Drawing.Point(208, 265);
            this.txt胸片异常.Name = "txt胸片异常";
            this.txt胸片异常.Size = new System.Drawing.Size(152, 20);
            this.txt胸片异常.StyleController = this.Layout1;
            this.txt胸片异常.TabIndex = 187;
            // 
            // layoutControlItem83
            // 
            this.layoutControlItem83.Control = this.txt胸片异常;
            this.layoutControlItem83.CustomizationFormText = "layoutControlItem83";
            this.layoutControlItem83.Location = new System.Drawing.Point(199, 202);
            this.layoutControlItem83.Name = "layoutControlItem83";
            this.layoutControlItem83.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem83.Size = new System.Drawing.Size(156, 29);
            this.layoutControlItem83.Text = "layoutControlItem83";
            this.layoutControlItem83.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem83.TextToControlDistance = 0;
            this.layoutControlItem83.TextVisible = false;
            // 
            // txt心电图异常
            // 
            this.txt心电图异常.Location = new System.Drawing.Point(208, 207);
            this.txt心电图异常.Name = "txt心电图异常";
            this.txt心电图异常.Size = new System.Drawing.Size(495, 20);
            this.txt心电图异常.StyleController = this.Layout1;
            this.txt心电图异常.TabIndex = 188;
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.Control = this.txt心电图异常;
            this.layoutControlItem84.CustomizationFormText = "layoutControlItem84";
            this.layoutControlItem84.Location = new System.Drawing.Point(199, 144);
            this.layoutControlItem84.Name = "layoutControlItem84";
            this.layoutControlItem84.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem84.Size = new System.Drawing.Size(499, 29);
            this.layoutControlItem84.Text = "layoutControlItem84";
            this.layoutControlItem84.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem84.TextToControlDistance = 0;
            this.layoutControlItem84.TextVisible = false;
            // 
            // UC高血压患者分级管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Layout1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC高血压患者分级管理";
            this.Size = new System.Drawing.Size(729, 491);
            this.Load += new System.EventHandler(this.UC高血压患者分级管理_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.Layout1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Layout1)).EndInit();
            this.Layout1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt分级管理.Properties)).EndInit();
            this.fl家族史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk以上都无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不详.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk拒答.Properties)).EndInit();
            this.fl并发症.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk无并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            this.fl既往史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk无既往史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk既往冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk既往心力衰竭.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk连续3次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio管理措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体征其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio转诊情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物调整)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物调整列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl失访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio心电图.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio胸片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio超声心动图.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio颈动脉超声.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声心动图异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt颈动脉超声异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸片异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl Layout1;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraEditors.CheckEdit check无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit ch症状其他;
        private DevExpress.XtraEditors.TextEdit txt症状其他;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private Library.UserControls.UCTxtLblTxtLbl txt体重;
        private Library.UserControls.UCTxtLbl txt身高;
        private Library.UserControls.UCTxtLblTxtLbl txt体质指数;
        private DevExpress.XtraEditors.TextEdit txt心率;
        private DevExpress.XtraEditors.TextEdit txt体征其他;
        private Library.UserControls.UCTxtLblTxtLbl txt日吸烟量;
        private Library.UserControls.UCTxtLblTxtLbl txt饮酒情况;
        private Library.UserControls.UCTxtLblTxtLbl txt运动;
        private Library.UserControls.UCTxtLblTxtLbl txt指导运动;
        private DevExpress.XtraEditors.TextEdit txt其他1;
        private DevExpress.XtraEditors.RadioGroup radio不良反应;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraEditors.RadioGroup radio转诊情况;
        private DevExpress.XtraEditors.TextEdit txt转诊科别;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况2;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraEditors.MemoEdit txt居住地址;
        private DevExpress.XtraEditors.LookUpEdit txt转诊结果;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private DevExpress.XtraEditors.TextEdit txt转诊联系电话;
        private DevExpress.XtraEditors.TextEdit txt转诊联系人;
        private DevExpress.XtraEditors.RadioGroup radio药物调整;
        private DevExpress.XtraEditors.RadioGroup radio管理措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.SimpleButton btn删除药物调整;
        private DevExpress.XtraEditors.SimpleButton btn添加药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物调整;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraGrid.GridControl gc药物调整;
        private DevExpress.XtraGrid.Views.Grid.GridView gv药物调整;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraLayout.LayoutControlItem layout药物调整列表;
        private DevExpress.XtraEditors.PictureEdit txt居民签名;
        private DevExpress.XtraEditors.SimpleButton sbtnFingerPrint;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private System.Windows.Forms.FlowLayoutPanel fl既往史;
        private DevExpress.XtraLayout.LayoutControlItem lcl失访;
        private DevExpress.XtraEditors.CheckEdit chk无既往史;
        private DevExpress.XtraEditors.CheckEdit chk既往冠心病;
        private DevExpress.XtraEditors.CheckEdit chk既往心力衰竭;
        private DevExpress.XtraEditors.CheckEdit chk连续3次;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private System.Windows.Forms.FlowLayoutPanel fl家族史;
        private DevExpress.XtraEditors.CheckEdit chk高血压;
        private DevExpress.XtraEditors.CheckEdit chk冠心病;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraEditors.SimpleButton sbtn便捷录入备注;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private System.Windows.Forms.FlowLayoutPanel fl并发症;
        private DevExpress.XtraEditors.CheckEdit chk无并发症;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraEditors.SimpleButton btn关闭;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit txt分级管理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.CheckEdit chk脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk以上都无;
        private DevExpress.XtraEditors.CheckEdit chk不详;
        private DevExpress.XtraEditors.CheckEdit chk拒答;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private Library.UserControls.UCLblTxtLbl txt血钠浓度;
        private Library.UserControls.UCLblTxtLbl txt血钾浓度;
        private Library.UserControls.UCLblTxtLbl txt血尿素;
        private Library.UserControls.UCLblTxtLbl txt血清肌酐;
        private Library.UserControls.UCLblTxtLbl txt总胆红素;
        private Library.UserControls.UCLblTxtLbl txt血清谷草转氨酶;
        private Library.UserControls.UCLblTxtLbl txt血清谷丙转氨酶;
        private DevExpress.XtraEditors.TextEdit txt尿常规其他;
        private Library.UserControls.UCLblTxt txt尿潜血;
        private Library.UserControls.UCLblTxt txt尿酮体;
        private Library.UserControls.UCLblTxt txt尿糖;
        private Library.UserControls.UCLblTxt txt尿蛋白;
        private Library.UserControls.UCLblTxtLbl txt血小板;
        private Library.UserControls.UCLblTxtLbl txt白细胞;
        private Library.UserControls.UCLblTxtLbl txt血红蛋白;
        private DevExpress.XtraEditors.TextEdit txt血常规其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private Library.UserControls.UCTxtLbl txt同型半胱氨酸;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCLblTxtLbl txt高密度蛋白;
        private Library.UserControls.UCLblTxtLbl txt低密度蛋白;
        private Library.UserControls.UCLblTxtLbl txt甘油三酯;
        private Library.UserControls.UCLblTxtLbl txt总胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraEditors.RadioGroup radio颈动脉超声;
        private DevExpress.XtraEditors.RadioGroup radio超声心动图;
        private DevExpress.XtraEditors.RadioGroup radio胸片;
        private DevExpress.XtraEditors.RadioGroup radio心电图;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraEditors.TextEdit txt心电图异常;
        private DevExpress.XtraEditors.TextEdit txt胸片异常;
        private DevExpress.XtraEditors.TextEdit txt颈动脉超声异常;
        private DevExpress.XtraEditors.TextEdit txt超声心动图异常;
        private DevExpress.XtraEditors.TextEdit txt其他2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem83;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;

    }
}
