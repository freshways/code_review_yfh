﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using System.Collections.Generic;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class report高血压患者随访服务记录表_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        List<string> dates;
        DataSet _ds高血压;
        bllMXB高血压随访表 _bll高血压 = new bllMXB高血压随访表();
        public report高血压患者随访服务记录表_2017()
        {
            InitializeComponent();

        }

        public report高血压患者随访服务记录表_2017(string docNo, List<string> _dates)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.dates = _dates;
            _ds高血压 = _bll高血压.GetInfoByGXY(docNo, dates, true);

            Bind_DataSource(_ds高血压);
        }

        private void Bind_DataSource(DataSet _ds高血压)
        {
            DataTable dt_高血压 = _ds高血压.Tables[Models.tb_MXB高血压随访表.__TableName];
            DataTable dt_健康档案 = _ds高血压.Tables[Models.tb_健康档案.__TableName];

            //姓名
            if (dt_健康档案 == null || dt_健康档案.Rows.Count == 0) return;
            xrLabel_姓名.Text = dt_健康档案.Rows[0][Models.tb_健康档案.姓名].ToString();
            //编号
            string str_个人档案编号 = dt_健康档案.Rows[0][Models.tb_健康档案.个人档案编号].ToString();
            char[] char_编号 = str_个人档案编号.Substring(str_个人档案编号.Length - 8, 8).ToCharArray();
            for (int i = 0; i < char_编号.Length; i++)
            {
                string xrName = "xrLabel_编号" + (i + 1);
                XRLabel xrl = (XRLabel)FindControl(xrName, false);
                xrl.Text = char_编号[i].ToString();
            }

            for (int i = 0; i < dt_高血压.Rows.Count; i++)
            {
                //随访日期
                string[] strs_随访日期 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.发生时间].ToString().Split('-');
                string xrTable_随访日期_年 = "xrTable_随访日期_年_" + (i + 1);
                string xrTable_随访日期_月 = "xrTable_随访日期_月_" + (i + 1);
                string xrTable_随访日期_日 = "xrTable_随访日期_日_" + (i + 1);
                XRTableCell XRT__随访日期_年 = (XRTableCell)FindControl(xrTable_随访日期_年, false);
                XRTableCell XRT__随访日期_月 = (XRTableCell)FindControl(xrTable_随访日期_月, false);
                XRTableCell XRT__随访日期_日 = (XRTableCell)FindControl(xrTable_随访日期_日, false);
                XRT__随访日期_年.Text = strs_随访日期[0];
                XRT__随访日期_月.Text = strs_随访日期[1];
                XRT__随访日期_日.Text = strs_随访日期[2];
                //随访方式
                string xrLabel_随访方式 = "xrLabel_随访方式_" + (i + 1);
                XRLabel XRL_随访方式 = (XRLabel)FindControl(xrLabel_随访方式, false);
                string str_随访方式 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.随访方式].ToString();
                XRL_随访方式.Text = str_随访方式.Equals("4") ? "" : str_随访方式;
                //症状
                string str_症状 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.目前症状].ToString();
                if (!string.IsNullOrEmpty(str_症状))
                {
                    string[] strs_症状 = str_症状.Split(',');
                    for (int j = 0; j < strs_症状.Length; j++)
                    {
                        if (j == 8) break;

                        if (!string.IsNullOrEmpty(strs_症状[j]))
                        {
                            string xrLabel_症状 = "xrLabel_症状_" + (i + 1) + "_" + (j + 1);
                            XRLabel XRL_症状 = (XRLabel)FindControl(xrLabel_症状, false);
                            XRL_症状.Text = strs_症状[j].Equals("99") ? "" : (Convert.ToInt32(strs_症状[j]) + 1).ToString();
                        }
                    }
                }
                string str_症状其他 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.目前症状其他].ToString();
                string xrTable_症状其他1 = "xrTable_症状其他_" + (i + 1) + "_1";
                string xrTable_症状其他2 = "xrTable_症状其他_" + (i + 1) + "_2";
                XRTableCell XRT_症状其他1 = (XRTableCell)FindControl(xrTable_症状其他1, false);
                XRTableCell XRT_症状其他2 = (XRTableCell)FindControl(xrTable_症状其他2, false);
                if (str_症状其他.Length <= 11)
                {
                    XRT_症状其他1.Text = str_症状其他;
                }
                else
                {
                    XRT_症状其他1.Text = str_症状其他.Substring(0, 11);
                    if (str_症状其他.Length <= 25)
                    {
                        XRT_症状其他2.Text = str_症状其他.Substring(11, str_症状其他.Length - 11);
                    }
                    else
                    {
                        XRT_症状其他2.Text = str_症状其他.Substring(11, 15);
                    }
                }
                //血压
                string str_收缩压 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.收缩压].ToString();
                string str_舒张压 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.舒张压].ToString();
                string xrTable_血压 = "xrTable_血压_" + (i + 1);
                XRTableCell XRT_血压 = (XRTableCell)FindControl(xrTable_血压, false);
                XRT_血压.Text = str_收缩压 + " / " + str_舒张压;
                //体重
                string xrTable_体重1 = "xrTable_体重_" + (i + 1) + "_1";
                string xrTable_体重2 = "xrTable_体重_" + (i + 1) + "_2";
                XRTableCell XRT_体重1 = (XRTableCell)FindControl(xrTable_体重1, false);
                XRTableCell XRT_体重2 = (XRTableCell)FindControl(xrTable_体重2, false);
                XRT_体重1.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.体重].ToString();
                XRT_体重2.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.体重2].ToString();
                //体质指数
                string xrTable_体质指数1 = "xrTable_体质指数_" + (i + 1) + "_1";
                string xrTable_体质指数2 = "xrTable_体质指数_" + (i + 1) + "_2";
                XRTableCell XRT_体质指数1 = (XRTableCell)FindControl(xrTable_体质指数1, false);
                XRTableCell XRT_体质指数2 = (XRTableCell)FindControl(xrTable_体质指数2, false);
                XRT_体质指数1.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.BMI].ToString();
                XRT_体质指数2.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.BMI2].ToString();
                //心率
                string xrTable_心率 = "xrTable_心率_" + (i + 1);
                XRTableCell XRT_心率 = (XRTableCell)FindControl(xrTable_心率, false);
                XRT_心率.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.心率].ToString();
                //体征其他
                string xrTable_体征其他 = "xrTable_体征其他_" + (i + 1);
                XRTableCell XRT_体征其他 = (XRTableCell)FindControl(xrTable_体征其他, false);
                XRT_体征其他.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.体征其他].ToString();
                //日吸烟量
                string xrTable_日吸烟量1 = "xrTable_日吸烟量_" + (i + 1) + "_1";
                string xrTable_日吸烟量2 = "xrTable_日吸烟量_" + (i + 1) + "_2";
                XRTableCell XRT_日吸烟量1 = (XRTableCell)FindControl(xrTable_日吸烟量1, false);
                XRTableCell XRT_日吸烟量2 = (XRTableCell)FindControl(xrTable_日吸烟量2, false);
                XRT_日吸烟量1.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.吸烟数量].ToString();
                XRT_日吸烟量2.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.吸烟数量2].ToString();
                //日饮酒量
                string xrTable_日饮酒量1 = "xrTable_日饮酒量_" + (i + 1) + "_1";
                string xrTable_日饮酒量2 = "xrTable_日饮酒量_" + (i + 1) + "_2";
                XRTableCell XRT_日饮酒量1 = (XRTableCell)FindControl(xrTable_日饮酒量1, false);
                XRTableCell XRT_日饮酒量2 = (XRTableCell)FindControl(xrTable_日饮酒量2, false);
                XRT_日饮酒量1.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.饮酒数量].ToString();
                XRT_日饮酒量2.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.饮酒数量2].ToString();
                //运动
                string xrTable_运动频率1 = "xrTable_运动频率_" + (i + 1) + "_1";
                string xrTable_运动频率2 = "xrTable_运动频率_" + (i + 1) + "_2";
                string xrTable_运动时长1 = "xrTable_运动时长_" + (i + 1) + "_1";
                string xrTable_运动时长2 = "xrTable_运动时长_" + (i + 1) + "_2";
                XRTableCell XRT_运动频率1 = (XRTableCell)FindControl(xrTable_运动频率1, false);
                XRTableCell XRT_运动频率2 = (XRTableCell)FindControl(xrTable_运动频率2, false);
                XRTableCell XRT_运动时长1 = (XRTableCell)FindControl(xrTable_运动时长1, false);
                XRTableCell XRT_运动时长2 = (XRTableCell)FindControl(xrTable_运动时长2, false);
                XRT_运动频率1.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动频率].ToString();
                XRT_运动频率2.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动频率2].ToString();
                object fl_运动时长1 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动持续时间] is DBNull ? 0 : dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动持续时间];
                object fl_运动时长2 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动持续时间2] is DBNull ? 0 : dt_高血压.Rows[i][Models.tb_MXB高血压随访表.运动持续时间2];
                XRT_运动时长1.Text = Convert.ToInt32(fl_运动时长1).ToString();
                XRT_运动时长2.Text = Convert.ToInt32(fl_运动时长1).ToString();
                //摄盐情况
                string str_摄盐情况1 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.摄盐情况].ToString();
                string str_摄盐情况2 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
                string xrTable_摄盐情况1 = "xrTable_摄盐情况_" + (i + 1) + "_1";
                string xrTable_摄盐情况2 = "xrTable_摄盐情况_" + (i + 1) + "_2";
                XRTableCell XRT_摄盐情况1 = (XRTableCell)FindControl(xrTable_摄盐情况1, false);
                XRTableCell XRT_摄盐情况2 = (XRTableCell)FindControl(xrTable_摄盐情况2, false);
                switch (str_摄盐情况1)
                {
                    case "1":
                        XRT_摄盐情况1.Text = "轻";
                        break;
                    case "2":
                        XRT_摄盐情况1.Text = "中";
                        break;
                    case "3":
                        XRT_摄盐情况1.Text = "重";
                        break;
                    default:
                        break;
                }
                switch (str_摄盐情况2)
                {
                    case "1":
                        XRT_摄盐情况2.Text = "轻";
                        break;
                    case "2":
                        XRT_摄盐情况2.Text = "中";
                        break;
                    case "3":
                        XRT_摄盐情况2.Text = "重";
                        break;
                    default:
                        break;
                }
                //心里调整
                string xrLabel_心理调整 = "xrLabel_心理调整_" + (i + 1);
                XRLabel XRL_心理调整 = (XRLabel)FindControl(xrLabel_心理调整, false);
                XRL_心理调整.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.心理调整].ToString();
                //遵医行为
                string xrLabel_遵医行为 = "xrLabel_遵医行为_" + (i + 1);
                XRLabel XRL_遵医行为 = (XRLabel)FindControl(xrLabel_遵医行为, false);
                XRL_遵医行为.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.遵医行为].ToString();
                //辅助检查
                string xrTable_辅助检查 = "xrTable_辅助检查_" + (i + 1);
                XRTableCell XRT_辅助检查 = (XRTableCell)FindControl(xrTable_辅助检查, false);
                XRT_辅助检查.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.辅助检查].ToString();
                //目前用药
                string str_创建时间目前 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.创建时间].ToString();
                DataTable dt用药情况目前 = new bllMXB高血压随访表().GetInfoByYY(docNo, str_创建时间目前, false).Tables[0];
                if (dt用药情况目前.Rows.Count > 0)
                {
                    for (int j = 0; j < dt用药情况目前.Rows.Count; j++)
                    {
                        if (j == 3) break;
                        string xrTable_药物名称目前 = "xrTable_药物名称目前" + (j + 1) + "_" + (i + 1);
                        string xrTable_用法用量每日目前 = "xrTable_用法用量每日目前" + (j + 1) + "_" + (i + 1);
                        string xrTable_用法用量每次目前 = "xrTable_用法用量每次目前" + (j + 1) + "_" + (i + 1);
                        XRTableCell XRT_药物名称目前 = (XRTableCell)FindControl(xrTable_药物名称目前, false);
                        XRTableCell XRT_用法用量每日目前 = (XRTableCell)FindControl(xrTable_用法用量每日目前, false);
                        XRTableCell XRT_用法用量每次目前 = (XRTableCell)FindControl(xrTable_用法用量每次目前, false);
                        XRT_药物名称目前.Text = dt用药情况目前.Rows[j][Models.tb_MXB高血压随访表_用药情况.药物名称].ToString();
                        string str_用法目前 = dt用药情况目前.Rows[j][Models.tb_MXB高血压随访表_用药情况.用法].ToString();
                        if (!string.IsNullOrEmpty(str_用法目前))
                        {
                            XRT_用法用量每日目前.Text = str_用法目前.Substring(2, str_用法目前.IndexOf("次") - 2).Trim();
                            XRT_用法用量每次目前.Text = str_用法目前.Substring(str_用法目前.IndexOf("次", str_用法目前.IndexOf("次", 0) + 1) + 1, str_用法目前.Length - (str_用法目前.IndexOf("次", str_用法目前.IndexOf("次", 0) + 1) + 1));
                        }
                    }
                }
                //服药依从性
                string xrLabel_服药依从性 = "xrLabel_服药依从性_" + (i + 1);
                XRLabel XRL_服药依从性 = (XRLabel)FindControl(xrLabel_服药依从性, false);
                XRL_服药依从性.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.服药依从性].ToString();
                //药物不良反应
                string xrLabel_药物不良反应 = "xrLabel_药物不良反应_" + (i + 1);
                string xrTabel_药物不良反应有 = "xrTable_药物不良反应有_" + (i + 1);
                XRLabel XRL_药物不良反应 = (XRLabel)FindControl(xrLabel_药物不良反应, false);
                XRTableCell XRT_药物不良反应有 = (XRTableCell)FindControl(xrTabel_药物不良反应有, false);
                XRL_药物不良反应.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.药物副作用].ToString();
                XRT_药物不良反应有.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.副作用详述].ToString();
                //此次随访分类
                string xrLabel_此次随访分类 = "xrLabel_此次随访分类_" + (i + 1);
                XRLabel XRL_此次随访分类 = (XRLabel)FindControl(xrLabel_此次随访分类, false);
                XRL_此次随访分类.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.本次随访分类].ToString();
                //下一步管理措施
                string xrLabel_下一步管理措施 = "xrLabel_下一步管理措施_" + (i + 1);
                XRLabel XRL_下一步管理措施 = (XRLabel)FindControl(xrLabel_下一步管理措施, false);
                XRL_下一步管理措施.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.下一步管理措施].ToString();
                //调整用药
                string str_创建时间调整 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.创建时间].ToString();
                DataTable dt用药情况调整 = new bllMXB高血压随访表().GetInfoByYY_TZ(docNo, str_创建时间调整, false).Tables[0];
                if (dt用药情况调整.Rows.Count > 0)
                {
                    for (int j = 0; j < dt用药情况调整.Rows.Count; j++)
                    {
                        if (j == 3) break;
                        string xrTable_药物名称调整 = "xrTable_药物名称调整" + (j + 1) + "_" + (i + 1);
                        string xrTable_用法用量每日调整 = "xrTable_用法用量每日调整" + (j + 1) + "_" + (i + 1);
                        string xrTable_用法用量每次调整 = "xrTable_用法用量每次调整" + (j + 1) + "_" + (i + 1);
                        XRTableCell XRT_药物名称调整 = (XRTableCell)FindControl(xrTable_药物名称调整, false);
                        XRTableCell XRT_用法用量每日调整 = (XRTableCell)FindControl(xrTable_用法用量每日调整, false);
                        XRTableCell XRT_用法用量每次调整 = (XRTableCell)FindControl(xrTable_用法用量每次调整, false);
                        XRT_药物名称调整.Text = dt用药情况调整.Rows[j][Models.tb_MXB高血压随访表_用药调整.药物名称].ToString();
                        string str_用法调整 = dt用药情况调整.Rows[j][Models.tb_MXB高血压随访表_用药调整.用法].ToString();
                        if (!string.IsNullOrEmpty(str_用法调整))
                        {
                            XRT_用法用量每日调整.Text = str_用法调整.Substring(2, str_用法调整.IndexOf("次") - 2).Trim();
                            XRT_用法用量每次调整.Text = str_用法调整.Substring(str_用法调整.IndexOf("次", str_用法调整.IndexOf("次", 0) + 1) + 1, str_用法调整.Length - (str_用法调整.IndexOf("次", str_用法调整.IndexOf("次", 0) + 1) + 1));
                        }
                    }
                }
                //转诊原因
                string xrTable_转诊原因 = "xrTable_转诊原因_" + (i + 1);
                XRTableCell XRT_转诊原因 = (XRTableCell)FindControl(xrTable_转诊原因, false);
                XRT_转诊原因.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.转诊原因].ToString();
                //转诊机构
                string xrTable_转诊机构 = "xrTable_转诊机构_" + (i + 1);
                XRTableCell XRT_转诊机构 = (XRTableCell)FindControl(xrTable_转诊机构, false);
                XRT_转诊机构.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.转诊科别].ToString();
                //转诊联系人
                string xrTable_转诊联系人 = "xrTable_转诊联系人_" + (i + 1);
                XRTableCell XRT_转诊联系人 = (XRTableCell)FindControl(xrTable_转诊联系人, false);
                string str_转诊联系人 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.转诊联系人].ToString();
                string str_转诊联系电话 = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.转诊联系电话].ToString();
                XRT_转诊联系人.Text = str_转诊联系人 + ":" + str_转诊联系电话;
                //转诊结果
                string xrLabel_转诊结果 = "xrLabel_转诊结果_" + (i + 1);
                XRLabel XRL_转诊结果 = (XRLabel)FindControl(xrLabel_转诊结果, false);
                XRL_转诊结果.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.转诊结果].ToString();
                //下次随访日期
                string xrTable_下次随访日期 = "xrTable_下次随访日期_" + (i + 1);
                XRTableCell XRT_下次随访日期 = (XRTableCell)FindControl(xrTable_下次随访日期, false);
                XRT_下次随访日期.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                //随访医生签字
                string xrTable_随访医生签字 = "xrTable_随访医生签字_" + (i + 1);
                XRTableCell XRT_随访医生签字 = (XRTableCell)FindControl(xrTable_随访医生签字, false);
                XRT_随访医生签字.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.随访医生].ToString();
                //居民签字
                string xrTable_居民签字 = "xrTable_居民签字_" + (i + 1);
                XRTableCell XRT_居民签字 = (XRTableCell)FindControl(xrTable_居民签字, false);
                XRT_居民签字.Text = dt_健康档案.Rows[0][Models.tb_健康档案.姓名].ToString();
                //备注
                string xrTable_备注 = "xrTable_备注_" + (i + 1);
                XRTableCell XRT_备注 = (XRTableCell)FindControl(xrTable_备注, false);
                XRT_备注.Text = dt_高血压.Rows[i][Models.tb_MXB高血压随访表.备注].ToString();
            }
        }
    }
}
