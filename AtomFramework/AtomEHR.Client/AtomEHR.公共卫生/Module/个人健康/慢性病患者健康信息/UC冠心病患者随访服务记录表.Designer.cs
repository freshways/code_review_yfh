﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC冠心病患者随访服务记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC冠心病患者随访服务记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt随访方式其他 = new System.Windows.Forms.TextBox();
            this.txt体质指数 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt遵医行为 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt心理调整 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况2 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt摄盐情况1 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt每次持续时间 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt运动频率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt饮酒情况 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt药物副作用详述 = new DevExpress.XtraEditors.TextEdit();
            this.radio不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.txt辅助检查 = new DevExpress.XtraEditors.TextEdit();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt心率 = new DevExpress.XtraEditors.TextEdit();
            this.gcDetail = new DevExpress.XtraGrid.GridControl();
            this.gvDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.btn删除药物 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生建议 = new DevExpress.XtraEditors.MemoEdit();
            this.fl随访分类 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch控制满意 = new DevExpress.XtraEditors.CheckEdit();
            this.ch控制不满意 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.ck并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.txt此次随访分类并发症 = new DevExpress.XtraEditors.TextEdit();
            this.txt心肌酶学 = new DevExpress.XtraEditors.TextEdit();
            this.txt冠状动脉造影 = new DevExpress.XtraEditors.TextEdit();
            this.txt心脏彩超 = new DevExpress.XtraEditors.TextEdit();
            this.txt心电图运动负荷 = new DevExpress.XtraEditors.TextEdit();
            this.txt心电图检查 = new DevExpress.XtraEditors.TextEdit();
            this.radio冠心病类型 = new DevExpress.XtraEditors.RadioGroup();
            this.lab当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lab最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lab创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.fl非药物治疗 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit44 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit45 = new DevExpress.XtraEditors.CheckEdit();
            this.ck非药物未采取措施 = new DevExpress.XtraEditors.CheckEdit();
            this.fl特殊治疗 = new System.Windows.Forms.FlowLayoutPanel();
            this.ch特殊治疗无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.radio用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血压值 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.fl症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.check无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.ch症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt服药依从性 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout添加药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout删除药物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout药物列表 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).BeginInit();
            this.fl随访分类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制不满意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt此次随访分类并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心肌酶学.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt冠状动脉造影.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏彩超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图运动负荷.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio冠心病类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            this.fl非药物治疗.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck非药物未采取措施.Properties)).BeginInit();
            this.fl特殊治疗.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch特殊治疗无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).BeginInit();
            this.fl症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn删除
            // 
            this.btn删除.Location = new System.Drawing.Point(84, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 1;
            this.btn删除.Text = "重置";
            this.btn删除.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt随访方式其他);
            this.layoutControl1.Controls.Add(this.txt体质指数);
            this.layoutControl1.Controls.Add(this.txt遵医行为);
            this.layoutControl1.Controls.Add(this.txt心理调整);
            this.layoutControl1.Controls.Add(this.txt摄盐情况2);
            this.layoutControl1.Controls.Add(this.txt摄盐情况1);
            this.layoutControl1.Controls.Add(this.txt每次持续时间);
            this.layoutControl1.Controls.Add(this.txt运动频率);
            this.layoutControl1.Controls.Add(this.txt饮酒情况);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt药物副作用详述);
            this.layoutControl1.Controls.Add(this.radio不良反应);
            this.layoutControl1.Controls.Add(this.txt辅助检查);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt心率);
            this.layoutControl1.Controls.Add(this.gcDetail);
            this.layoutControl1.Controls.Add(this.lab考核项);
            this.layoutControl1.Controls.Add(this.btn删除药物);
            this.layoutControl1.Controls.Add(this.btn添加药物);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt医生建议);
            this.layoutControl1.Controls.Add(this.fl随访分类);
            this.layoutControl1.Controls.Add(this.txt心肌酶学);
            this.layoutControl1.Controls.Add(this.txt冠状动脉造影);
            this.layoutControl1.Controls.Add(this.txt心脏彩超);
            this.layoutControl1.Controls.Add(this.txt心电图运动负荷);
            this.layoutControl1.Controls.Add(this.txt心电图检查);
            this.layoutControl1.Controls.Add(this.radio冠心病类型);
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Controls.Add(this.fl非药物治疗);
            this.layoutControl1.Controls.Add(this.fl特殊治疗);
            this.layoutControl1.Controls.Add(this.radio用药情况);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt血压值);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.fl症状);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.txt发生时间);
            this.layoutControl1.Controls.Add(this.txt服药依从性);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem49,
            this.layoutControlItem52,
            this.layoutControlItem55,
            this.layoutControlItem54,
            this.layoutControlItem53});
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(616, 276, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 468);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt随访方式其他
            // 
            this.txt随访方式其他.Location = new System.Drawing.Point(576, 144);
            this.txt随访方式其他.Name = "txt随访方式其他";
            this.txt随访方式其他.Size = new System.Drawing.Size(154, 20);
            this.txt随访方式其他.TabIndex = 128;
            // 
            // txt体质指数
            // 
            this.txt体质指数.Lbl1Size = new System.Drawing.Size(8, 14);
            this.txt体质指数.Lbl1Text = "/";
            this.txt体质指数.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt体质指数.Lbl2Text = "kg/m2";
            this.txt体质指数.Location = new System.Drawing.Point(99, 265);
            this.txt体质指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Size = new System.Drawing.Size(189, 20);
            this.txt体质指数.TabIndex = 92;
            this.txt体质指数.Txt1EditValue = null;
            this.txt体质指数.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt体质指数.Txt2EditValue = null;
            this.txt体质指数.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt遵医行为
            // 
            this.txt遵医行为.Location = new System.Drawing.Point(91, 383);
            this.txt遵医行为.Margin = new System.Windows.Forms.Padding(0);
            this.txt遵医行为.Name = "txt遵医行为";
            this.txt遵医行为.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt遵医行为.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt遵医行为.Properties.NullText = "请选择";
            this.txt遵医行为.Properties.PopupSizeable = false;
            this.txt遵医行为.Size = new System.Drawing.Size(266, 20);
            this.txt遵医行为.StyleController = this.layoutControl1;
            this.txt遵医行为.TabIndex = 104;
            // 
            // txt心理调整
            // 
            this.txt心理调整.EditValue = "请选择";
            this.txt心理调整.Location = new System.Drawing.Point(448, 359);
            this.txt心理调整.Margin = new System.Windows.Forms.Padding(0);
            this.txt心理调整.Name = "txt心理调整";
            this.txt心理调整.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt心理调整.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt心理调整.Properties.NullText = "请选择";
            this.txt心理调整.Properties.PopupSizeable = false;
            this.txt心理调整.Size = new System.Drawing.Size(281, 20);
            this.txt心理调整.StyleController = this.layoutControl1;
            this.txt心理调整.TabIndex = 102;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.Location = new System.Drawing.Point(254, 359);
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况2.Properties.NullText = "请选择";
            this.txt摄盐情况2.Size = new System.Drawing.Size(103, 20);
            this.txt摄盐情况2.StyleController = this.layoutControl1;
            this.txt摄盐情况2.TabIndex = 127;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.Location = new System.Drawing.Point(91, 359);
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt摄盐情况1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt摄盐情况1.Properties.NullText = "请选择";
            this.txt摄盐情况1.Size = new System.Drawing.Size(145, 20);
            this.txt摄盐情况1.StyleController = this.layoutControl1;
            this.txt摄盐情况1.TabIndex = 126;
            // 
            // txt每次持续时间
            // 
            this.txt每次持续时间.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt每次持续时间.Lbl1Text = "/";
            this.txt每次持续时间.Lbl2Size = new System.Drawing.Size(50, 14);
            this.txt每次持续时间.Lbl2Text = "(分钟/次)";
            this.txt每次持续时间.Location = new System.Drawing.Point(448, 335);
            this.txt每次持续时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt每次持续时间.Name = "txt每次持续时间";
            this.txt每次持续时间.Size = new System.Drawing.Size(281, 20);
            this.txt每次持续时间.TabIndex = 98;
            this.txt每次持续时间.Txt1EditValue = null;
            this.txt每次持续时间.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt每次持续时间.Txt2EditValue = null;
            this.txt每次持续时间.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt运动频率
            // 
            this.txt运动频率.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt运动频率.Lbl1Text = "/";
            this.txt运动频率.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt运动频率.Lbl2Text = "(次/周)";
            this.txt运动频率.Location = new System.Drawing.Point(91, 335);
            this.txt运动频率.Margin = new System.Windows.Forms.Padding(0);
            this.txt运动频率.Name = "txt运动频率";
            this.txt运动频率.Size = new System.Drawing.Size(266, 20);
            this.txt运动频率.TabIndex = 96;
            this.txt运动频率.Txt1EditValue = null;
            this.txt运动频率.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt运动频率.Txt2EditValue = null;
            this.txt运动频率.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt饮酒情况
            // 
            this.txt饮酒情况.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt饮酒情况.Lbl1Text = "/";
            this.txt饮酒情况.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt饮酒情况.Lbl2Text = "(两/天)";
            this.txt饮酒情况.Location = new System.Drawing.Point(448, 311);
            this.txt饮酒情况.Margin = new System.Windows.Forms.Padding(0);
            this.txt饮酒情况.Name = "txt饮酒情况";
            this.txt饮酒情况.Size = new System.Drawing.Size(281, 20);
            this.txt饮酒情况.TabIndex = 95;
            this.txt饮酒情况.Txt1EditValue = null;
            this.txt饮酒情况.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt饮酒情况.Txt2EditValue = null;
            this.txt饮酒情况.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt日吸烟量.Lbl1Text = "/";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt日吸烟量.Lbl2Text = "(支/天)";
            this.txt日吸烟量.Location = new System.Drawing.Point(91, 311);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(266, 20);
            this.txt日吸烟量.TabIndex = 111;
            this.txt日吸烟量.Txt1EditValue = null;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt日吸烟量.Txt2EditValue = null;
            this.txt日吸烟量.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt药物副作用详述
            // 
            this.txt药物副作用详述.Enabled = false;
            this.txt药物副作用详述.Location = new System.Drawing.Point(422, 503);
            this.txt药物副作用详述.Margin = new System.Windows.Forms.Padding(0);
            this.txt药物副作用详述.Name = "txt药物副作用详述";
            this.txt药物副作用详述.Size = new System.Drawing.Size(308, 20);
            this.txt药物副作用详述.StyleController = this.layoutControl1;
            this.txt药物副作用详述.TabIndex = 110;
            // 
            // radio不良反应
            // 
            this.radio不良反应.EditValue = "1";
            this.radio不良反应.Location = new System.Drawing.Point(98, 503);
            this.radio不良反应.Margin = new System.Windows.Forms.Padding(0);
            this.radio不良反应.Name = "radio不良反应";
            this.radio不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio不良反应.Size = new System.Drawing.Size(205, 25);
            this.radio不良反应.StyleController = this.layoutControl1;
            this.radio不良反应.TabIndex = 108;
            this.radio不良反应.SelectedIndexChanged += new System.EventHandler(this.radio不良反应_SelectedIndexChanged);
            // 
            // txt辅助检查
            // 
            this.txt辅助检查.Location = new System.Drawing.Point(91, 453);
            this.txt辅助检查.Name = "txt辅助检查";
            this.txt辅助检查.Size = new System.Drawing.Size(638, 20);
            this.txt辅助检查.StyleController = this.layoutControl1;
            this.txt辅助检查.TabIndex = 98;
            // 
            // txt体重
            // 
            this.txt体重.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt体重.Lbl1Text = "/";
            this.txt体重.Lbl2Size = new System.Drawing.Size(20, 14);
            this.txt体重.Lbl2Text = "Kg";
            this.txt体重.Location = new System.Drawing.Point(357, 241);
            this.txt体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(172, 20);
            this.txt体重.TabIndex = 91;
            this.txt体重.Txt1EditValue = null;
            this.txt体重.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt体重.Txt2EditValue = null;
            this.txt体重.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt心率
            // 
            this.txt心率.Location = new System.Drawing.Point(357, 265);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(110, 20);
            this.txt心率.StyleController = this.layoutControl1;
            this.txt心率.TabIndex = 97;
            // 
            // gcDetail
            // 
            this.gcDetail.Location = new System.Drawing.Point(3, 558);
            this.gcDetail.MainView = this.gvDetail;
            this.gcDetail.Name = "gcDetail";
            this.gcDetail.Size = new System.Drawing.Size(727, 71);
            this.gcDetail.TabIndex = 93;
            this.gcDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDetail});
            // 
            // gvDetail
            // 
            this.gvDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gvDetail.GridControl = this.gcDetail;
            this.gvDetail.Name = "gvDetail";
            this.gvDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 342;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 365;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "创建时间";
            this.gridColumn4.FieldName = "创建时间";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.White;
            this.lab考核项.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab考核项.Location = new System.Drawing.Point(3, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(727, 14);
            this.lab考核项.StyleController = this.layoutControl1;
            this.lab考核项.TabIndex = 2;
            this.lab考核项.Text = "考核项：24     缺项：0 完整度：100% ";
            // 
            // btn删除药物
            // 
            this.btn删除药物.Image = ((System.Drawing.Image)(resources.GetObject("btn删除药物.Image")));
            this.btn删除药物.Location = new System.Drawing.Point(365, 532);
            this.btn删除药物.Name = "btn删除药物";
            this.btn删除药物.Size = new System.Drawing.Size(54, 22);
            this.btn删除药物.StyleController = this.layoutControl1;
            this.btn删除药物.TabIndex = 96;
            this.btn删除药物.Tag = "2";
            this.btn删除药物.Text = "删除";
            // 
            // btn添加药物
            // 
            this.btn添加药物.Image = ((System.Drawing.Image)(resources.GetObject("btn添加药物.Image")));
            this.btn添加药物.Location = new System.Drawing.Point(307, 532);
            this.btn添加药物.Name = "btn添加药物";
            this.btn添加药物.Size = new System.Drawing.Size(54, 22);
            this.btn添加药物.StyleController = this.layoutControl1;
            this.btn添加药物.TabIndex = 95;
            this.btn添加药物.Tag = "0";
            this.btn添加药物.Text = "添加";
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(454, 120);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(276, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 72;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(98, 120);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(257, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 71;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(454, 96);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(276, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 70;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(98, 96);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(257, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 69;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(454, 72);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(276, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 68;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(98, 72);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(257, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 67;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(454, 48);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(276, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 66;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(98, 48);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(257, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 65;
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(408, 761);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(322, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 64;
            // 
            // txt医生建议
            // 
            this.txt医生建议.Location = new System.Drawing.Point(98, 721);
            this.txt医生建议.Name = "txt医生建议";
            this.txt医生建议.Size = new System.Drawing.Size(632, 36);
            this.txt医生建议.StyleController = this.layoutControl1;
            this.txt医生建议.TabIndex = 63;
            this.txt医生建议.UseOptimizedRendering = true;
            // 
            // fl随访分类
            // 
            this.fl随访分类.Controls.Add(this.ch控制满意);
            this.fl随访分类.Controls.Add(this.ch控制不满意);
            this.fl随访分类.Controls.Add(this.checkEdit3);
            this.fl随访分类.Controls.Add(this.ck并发症);
            this.fl随访分类.Controls.Add(this.txt此次随访分类并发症);
            this.fl随访分类.Location = new System.Drawing.Point(98, 697);
            this.fl随访分类.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.fl随访分类.Name = "fl随访分类";
            this.fl随访分类.Size = new System.Drawing.Size(632, 20);
            this.fl随访分类.TabIndex = 41;
            // 
            // ch控制满意
            // 
            this.ch控制满意.Location = new System.Drawing.Point(0, 0);
            this.ch控制满意.Margin = new System.Windows.Forms.Padding(0);
            this.ch控制满意.Name = "ch控制满意";
            this.ch控制满意.Properties.Caption = "控制满意";
            this.ch控制满意.Size = new System.Drawing.Size(72, 19);
            this.ch控制满意.TabIndex = 20;
            this.ch控制满意.Tag = "1";
            this.ch控制满意.CheckedChanged += new System.EventHandler(this.ch控制满意_CheckedChanged);
            // 
            // ch控制不满意
            // 
            this.ch控制不满意.Location = new System.Drawing.Point(72, 0);
            this.ch控制不满意.Margin = new System.Windows.Forms.Padding(0);
            this.ch控制不满意.Name = "ch控制不满意";
            this.ch控制不满意.Properties.Caption = "控制不满意";
            this.ch控制不满意.Size = new System.Drawing.Size(81, 19);
            this.ch控制不满意.TabIndex = 21;
            this.ch控制不满意.Tag = "2";
            this.ch控制不满意.CheckedChanged += new System.EventHandler(this.ch控制满意_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(153, 0);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "不良反应";
            this.checkEdit3.Size = new System.Drawing.Size(69, 19);
            this.checkEdit3.TabIndex = 22;
            this.checkEdit3.Tag = "3";
            // 
            // ck并发症
            // 
            this.ck并发症.Location = new System.Drawing.Point(222, 0);
            this.ck并发症.Margin = new System.Windows.Forms.Padding(0);
            this.ck并发症.Name = "ck并发症";
            this.ck并发症.Properties.Caption = "并发症";
            this.ck并发症.Size = new System.Drawing.Size(63, 19);
            this.ck并发症.TabIndex = 23;
            this.ck并发症.Tag = "4";
            this.ck并发症.CheckedChanged += new System.EventHandler(this.ck并发症_CheckedChanged);
            // 
            // txt此次随访分类并发症
            // 
            this.txt此次随访分类并发症.Location = new System.Drawing.Point(285, 0);
            this.txt此次随访分类并发症.Margin = new System.Windows.Forms.Padding(0);
            this.txt此次随访分类并发症.Name = "txt此次随访分类并发症";
            this.txt此次随访分类并发症.Size = new System.Drawing.Size(158, 20);
            this.txt此次随访分类并发症.TabIndex = 24;
            this.txt此次随访分类并发症.Visible = false;
            // 
            // txt心肌酶学
            // 
            this.txt心肌酶学.Location = new System.Drawing.Point(467, 395);
            this.txt心肌酶学.Name = "txt心肌酶学";
            this.txt心肌酶学.Size = new System.Drawing.Size(262, 20);
            this.txt心肌酶学.StyleController = this.layoutControl1;
            this.txt心肌酶学.TabIndex = 58;
            // 
            // txt冠状动脉造影
            // 
            this.txt冠状动脉造影.Location = new System.Drawing.Point(107, 395);
            this.txt冠状动脉造影.Name = "txt冠状动脉造影";
            this.txt冠状动脉造影.Size = new System.Drawing.Size(622, 20);
            this.txt冠状动脉造影.StyleController = this.layoutControl1;
            this.txt冠状动脉造影.TabIndex = 57;
            // 
            // txt心脏彩超
            // 
            this.txt心脏彩超.Location = new System.Drawing.Point(107, 371);
            this.txt心脏彩超.Name = "txt心脏彩超";
            this.txt心脏彩超.Size = new System.Drawing.Size(622, 20);
            this.txt心脏彩超.StyleController = this.layoutControl1;
            this.txt心脏彩超.TabIndex = 56;
            // 
            // txt心电图运动负荷
            // 
            this.txt心电图运动负荷.Location = new System.Drawing.Point(139, 371);
            this.txt心电图运动负荷.Name = "txt心电图运动负荷";
            this.txt心电图运动负荷.Size = new System.Drawing.Size(221, 20);
            this.txt心电图运动负荷.StyleController = this.layoutControl1;
            this.txt心电图运动负荷.TabIndex = 55;
            // 
            // txt心电图检查
            // 
            this.txt心电图检查.Location = new System.Drawing.Point(91, 429);
            this.txt心电图检查.Name = "txt心电图检查";
            this.txt心电图检查.Size = new System.Drawing.Size(638, 20);
            this.txt心电图检查.StyleController = this.layoutControl1;
            this.txt心电图检查.TabIndex = 54;
            // 
            // radio冠心病类型
            // 
            this.radio冠心病类型.Location = new System.Drawing.Point(98, 170);
            this.radio冠心病类型.Name = "radio冠心病类型";
            this.radio冠心病类型.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "稳定型心绞痛"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不稳定型心绞痛"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "急性心肌梗死"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "陈旧性心肌梗死"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "猝死型"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他不确定类型")});
            this.radio冠心病类型.Size = new System.Drawing.Size(632, 25);
            this.radio冠心病类型.StyleController = this.layoutControl1;
            this.radio冠心病类型.TabIndex = 52;
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab当前所属机构.Location = new System.Drawing.Point(408, 833);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Size = new System.Drawing.Size(322, 20);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 51;
            this.lab当前所属机构.Text = "labelControl16";
            // 
            // lab创建机构
            // 
            this.lab创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建机构.Location = new System.Drawing.Point(98, 833);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Size = new System.Drawing.Size(211, 20);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 50;
            this.lab创建机构.Text = "labelControl15";
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab最近修改人.Location = new System.Drawing.Point(408, 809);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Size = new System.Drawing.Size(322, 20);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 49;
            this.lab最近修改人.Text = "labelControl14";
            // 
            // lab创建人
            // 
            this.lab创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab创建人.Location = new System.Drawing.Point(98, 809);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Size = new System.Drawing.Size(211, 20);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 48;
            this.lab创建人.Text = "labelControl13";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(408, 785);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(322, 14);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 47;
            this.lbl最近更新时间.Text = "labelControl12";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(98, 785);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(211, 20);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "labelControl11";
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(98, 761);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt下次随访时间.Size = new System.Drawing.Size(211, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 45;
            // 
            // fl非药物治疗
            // 
            this.fl非药物治疗.Controls.Add(this.checkEdit38);
            this.fl非药物治疗.Controls.Add(this.checkEdit39);
            this.fl非药物治疗.Controls.Add(this.checkEdit40);
            this.fl非药物治疗.Controls.Add(this.checkEdit41);
            this.fl非药物治疗.Controls.Add(this.checkEdit42);
            this.fl非药物治疗.Controls.Add(this.checkEdit43);
            this.fl非药物治疗.Controls.Add(this.checkEdit44);
            this.fl非药物治疗.Controls.Add(this.checkEdit45);
            this.fl非药物治疗.Controls.Add(this.ck非药物未采取措施);
            this.fl非药物治疗.Location = new System.Drawing.Point(98, 657);
            this.fl非药物治疗.Name = "fl非药物治疗";
            this.fl非药物治疗.Size = new System.Drawing.Size(632, 36);
            this.fl非药物治疗.TabIndex = 41;
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(0, 0);
            this.checkEdit38.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Caption = "限盐";
            this.checkEdit38.Size = new System.Drawing.Size(51, 19);
            this.checkEdit38.TabIndex = 25;
            this.checkEdit38.Tag = "1";
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(51, 0);
            this.checkEdit39.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Caption = "减少吸烟量或戒烟";
            this.checkEdit39.Size = new System.Drawing.Size(124, 19);
            this.checkEdit39.TabIndex = 26;
            this.checkEdit39.Tag = "2";
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(175, 0);
            this.checkEdit40.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Caption = "减少饮酒量或戒酒";
            this.checkEdit40.Size = new System.Drawing.Size(129, 19);
            this.checkEdit40.TabIndex = 27;
            this.checkEdit40.Tag = "3";
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(304, 0);
            this.checkEdit41.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Caption = "减少膳食脂肪";
            this.checkEdit41.Size = new System.Drawing.Size(97, 19);
            this.checkEdit41.TabIndex = 28;
            this.checkEdit41.Tag = "4";
            // 
            // checkEdit42
            // 
            this.checkEdit42.Location = new System.Drawing.Point(401, 0);
            this.checkEdit42.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit42.Name = "checkEdit42";
            this.checkEdit42.Properties.Caption = "减轻体重";
            this.checkEdit42.Size = new System.Drawing.Size(72, 19);
            this.checkEdit42.TabIndex = 29;
            this.checkEdit42.Tag = "5";
            // 
            // checkEdit43
            // 
            this.checkEdit43.Location = new System.Drawing.Point(473, 0);
            this.checkEdit43.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit43.Name = "checkEdit43";
            this.checkEdit43.Properties.Caption = "有规律体育运动";
            this.checkEdit43.Size = new System.Drawing.Size(110, 19);
            this.checkEdit43.TabIndex = 30;
            this.checkEdit43.Tag = "6";
            // 
            // checkEdit44
            // 
            this.checkEdit44.Location = new System.Drawing.Point(0, 19);
            this.checkEdit44.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit44.Name = "checkEdit44";
            this.checkEdit44.Properties.Caption = "放松精神";
            this.checkEdit44.Size = new System.Drawing.Size(79, 19);
            this.checkEdit44.TabIndex = 31;
            this.checkEdit44.Tag = "7";
            // 
            // checkEdit45
            // 
            this.checkEdit45.Location = new System.Drawing.Point(79, 19);
            this.checkEdit45.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit45.Name = "checkEdit45";
            this.checkEdit45.Properties.Caption = "心理指导";
            this.checkEdit45.Size = new System.Drawing.Size(75, 19);
            this.checkEdit45.TabIndex = 32;
            this.checkEdit45.Tag = "8";
            // 
            // ck非药物未采取措施
            // 
            this.ck非药物未采取措施.Location = new System.Drawing.Point(154, 19);
            this.ck非药物未采取措施.Margin = new System.Windows.Forms.Padding(0);
            this.ck非药物未采取措施.Name = "ck非药物未采取措施";
            this.ck非药物未采取措施.Properties.Caption = "未采取措施";
            this.ck非药物未采取措施.Size = new System.Drawing.Size(84, 19);
            this.ck非药物未采取措施.TabIndex = 33;
            this.ck非药物未采取措施.Tag = "99";
            this.ck非药物未采取措施.CheckedChanged += new System.EventHandler(this.ck非药物未采取措施_CheckedChanged);
            // 
            // fl特殊治疗
            // 
            this.fl特殊治疗.Controls.Add(this.ch特殊治疗无);
            this.fl特殊治疗.Controls.Add(this.checkEdit34);
            this.fl特殊治疗.Controls.Add(this.checkEdit35);
            this.fl特殊治疗.Controls.Add(this.checkEdit36);
            this.fl特殊治疗.Location = new System.Drawing.Point(98, 633);
            this.fl特殊治疗.Name = "fl特殊治疗";
            this.fl特殊治疗.Size = new System.Drawing.Size(632, 20);
            this.fl特殊治疗.TabIndex = 40;
            // 
            // ch特殊治疗无
            // 
            this.ch特殊治疗无.EditValue = true;
            this.ch特殊治疗无.Location = new System.Drawing.Point(0, 0);
            this.ch特殊治疗无.Margin = new System.Windows.Forms.Padding(0);
            this.ch特殊治疗无.Name = "ch特殊治疗无";
            this.ch特殊治疗无.Properties.Caption = "无";
            this.ch特殊治疗无.Size = new System.Drawing.Size(38, 19);
            this.ch特殊治疗无.TabIndex = 21;
            this.ch特殊治疗无.Tag = "0";
            this.ch特殊治疗无.CheckedChanged += new System.EventHandler(this.ch特殊治疗无_CheckedChanged);
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(38, 0);
            this.checkEdit34.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "外科手术治疗";
            this.checkEdit34.Size = new System.Drawing.Size(102, 19);
            this.checkEdit34.TabIndex = 22;
            this.checkEdit34.Tag = "1";
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(140, 0);
            this.checkEdit35.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "介入治疗";
            this.checkEdit35.Size = new System.Drawing.Size(79, 19);
            this.checkEdit35.TabIndex = 23;
            this.checkEdit35.Tag = "2";
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(219, 0);
            this.checkEdit36.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Caption = "起搏器";
            this.checkEdit36.Size = new System.Drawing.Size(69, 19);
            this.checkEdit36.TabIndex = 24;
            this.checkEdit36.Tag = "3";
            // 
            // radio用药情况
            // 
            this.radio用药情况.EditValue = "2";
            this.radio用药情况.Location = new System.Drawing.Point(98, 532);
            this.radio用药情况.Name = "radio用药情况";
            this.radio用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "使用"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不使用")});
            this.radio用药情况.Size = new System.Drawing.Size(205, 22);
            this.radio用药情况.StyleController = this.layoutControl1;
            this.radio用药情况.TabIndex = 37;
            this.radio用药情况.SelectedIndexChanged += new System.EventHandler(this.radio用药情况_SelectedIndexChanged);
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L";
            this.txt空腹血糖.Location = new System.Drawing.Point(536, 265);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(193, 20);
            this.txt空腹血糖.TabIndex = 32;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt血压值
            // 
            this.txt血压值.Lbl1Size = new System.Drawing.Size(8, 14);
            this.txt血压值.Lbl1Text = "/";
            this.txt血压值.Lbl2Size = new System.Drawing.Size(35, 14);
            this.txt血压值.Lbl2Text = "mmHg";
            this.txt血压值.Location = new System.Drawing.Point(99, 241);
            this.txt血压值.Name = "txt血压值";
            this.txt血压值.Size = new System.Drawing.Size(189, 20);
            this.txt血压值.TabIndex = 31;
            this.txt血压值.Txt1EditValue = null;
            this.txt血压值.Txt1Size = new System.Drawing.Size(60, 20);
            this.txt血压值.Txt2EditValue = null;
            this.txt血压值.Txt2Size = new System.Drawing.Size(60, 20);
            // 
            // txt身高
            // 
            this.txt身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt身高.Lbl1Text = "cm";
            this.txt身高.Location = new System.Drawing.Point(598, 241);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(131, 20);
            this.txt身高.TabIndex = 27;
            this.txt身高.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // fl症状
            // 
            this.fl症状.Controls.Add(this.check无症状);
            this.fl症状.Controls.Add(this.checkEdit9);
            this.fl症状.Controls.Add(this.checkEdit10);
            this.fl症状.Controls.Add(this.checkEdit11);
            this.fl症状.Controls.Add(this.checkEdit12);
            this.fl症状.Controls.Add(this.checkEdit13);
            this.fl症状.Controls.Add(this.checkEdit14);
            this.fl症状.Controls.Add(this.checkEdit1);
            this.fl症状.Controls.Add(this.checkEdit2);
            this.fl症状.Controls.Add(this.ch症状其他);
            this.fl症状.Controls.Add(this.txt症状其他);
            this.fl症状.Location = new System.Drawing.Point(98, 170);
            this.fl症状.Name = "fl症状";
            this.fl症状.Size = new System.Drawing.Size(632, 46);
            this.fl症状.TabIndex = 19;
            // 
            // check无症状
            // 
            this.check无症状.Location = new System.Drawing.Point(0, 0);
            this.check无症状.Margin = new System.Windows.Forms.Padding(0);
            this.check无症状.Name = "check无症状";
            this.check无症状.Properties.Caption = "1.无症状";
            this.check无症状.Size = new System.Drawing.Size(88, 19);
            this.check无症状.TabIndex = 7;
            this.check无症状.Tag = "0";
            this.check无症状.CheckedChanged += new System.EventHandler(this.check无症状_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(88, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "2.心慌";
            this.checkEdit9.Size = new System.Drawing.Size(71, 19);
            this.checkEdit9.TabIndex = 8;
            this.checkEdit9.Tag = "1";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(159, 0);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "3.胸口闷痛";
            this.checkEdit10.Size = new System.Drawing.Size(98, 19);
            this.checkEdit10.TabIndex = 9;
            this.checkEdit10.Tag = "2";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(257, 0);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "4.心绞痛";
            this.checkEdit11.Size = new System.Drawing.Size(90, 19);
            this.checkEdit11.TabIndex = 10;
            this.checkEdit11.Tag = "3";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(347, 0);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "5.呼吸困难";
            this.checkEdit12.Size = new System.Drawing.Size(110, 19);
            this.checkEdit12.TabIndex = 11;
            this.checkEdit12.Tag = "4";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(457, 0);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "6.心悸胸闷";
            this.checkEdit13.Size = new System.Drawing.Size(103, 19);
            this.checkEdit13.TabIndex = 12;
            this.checkEdit13.Tag = "5";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(0, 22);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "7.喘憋";
            this.checkEdit14.Size = new System.Drawing.Size(88, 19);
            this.checkEdit14.TabIndex = 13;
            this.checkEdit14.Tag = "6";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(91, 22);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "8.不能平卧";
            this.checkEdit1.Size = new System.Drawing.Size(99, 19);
            this.checkEdit1.TabIndex = 16;
            this.checkEdit1.Tag = "7";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(196, 22);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "9.下肢水肿";
            this.checkEdit2.Size = new System.Drawing.Size(85, 19);
            this.checkEdit2.TabIndex = 17;
            this.checkEdit2.Tag = "8";
            // 
            // ch症状其他
            // 
            this.ch症状其他.Location = new System.Drawing.Point(287, 22);
            this.ch症状其他.Name = "ch症状其他";
            this.ch症状其他.Properties.Caption = "10.其他";
            this.ch症状其他.Size = new System.Drawing.Size(81, 19);
            this.ch症状其他.TabIndex = 14;
            this.ch症状其他.Tag = "99";
            this.ch症状其他.CheckedChanged += new System.EventHandler(this.ch症状其他_CheckedChanged);
            // 
            // txt症状其他
            // 
            this.txt症状其他.Location = new System.Drawing.Point(374, 22);
            this.txt症状其他.Name = "txt症状其他";
            this.txt症状其他.Size = new System.Drawing.Size(100, 20);
            this.txt症状其他.TabIndex = 15;
            // 
            // radio随访方式
            // 
            this.radio随访方式.Location = new System.Drawing.Point(356, 144);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio随访方式.Size = new System.Drawing.Size(216, 22);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 17;
            // 
            // txt发生时间
            // 
            this.txt发生时间.EditValue = null;
            this.txt发生时间.Location = new System.Drawing.Point(98, 144);
            this.txt发生时间.Name = "txt发生时间";
            this.txt发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt发生时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt发生时间.Size = new System.Drawing.Size(159, 20);
            this.txt发生时间.StyleController = this.layoutControl1;
            this.txt发生时间.TabIndex = 15;
            // 
            // txt服药依从性
            // 
            this.txt服药依从性.Location = new System.Drawing.Point(98, 478);
            this.txt服药依从性.Name = "txt服药依从性";
            this.txt服药依从性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt服药依从性.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "名称")});
            this.txt服药依从性.Properties.NullText = "请选择";
            this.txt服药依从性.Properties.PopupSizeable = false;
            this.txt服药依从性.Size = new System.Drawing.Size(205, 20);
            this.txt服药依从性.StyleController = this.layoutControl1;
            this.txt服药依从性.TabIndex = 39;
            this.txt服药依从性.EditValueChanged += new System.EventHandler(this.txt服药依从性_EditValueChanged);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.radio冠心病类型;
            this.layoutControlItem49.CustomizationFormText = "冠心病类型";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(731, 29);
            this.layoutControlItem49.Tag = "check";
            this.layoutControlItem49.Text = "冠心病类型";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt心电图运动负荷;
            this.layoutControlItem52.CustomizationFormText = "心电图运动负荷试验结果";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem52.Text = "心电图运动负荷试验结果";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(132, 14);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.txt心肌酶学;
            this.layoutControlItem55.CustomizationFormText = "心肌酶学检查结果";
            this.layoutControlItem55.Location = new System.Drawing.Point(360, 72);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(369, 24);
            this.layoutControlItem55.Text = "心肌酶学检查结果";
            this.layoutControlItem55.TextSize = new System.Drawing.Size(132, 14);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.txt冠状动脉造影;
            this.layoutControlItem54.CustomizationFormText = "冠状动脉造影结果";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem54.Text = "冠状动脉造影结果";
            this.layoutControlItem54.TextSize = new System.Drawing.Size(132, 14);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.txt心脏彩超;
            this.layoutControlItem53.CustomizationFormText = "心脏彩超检查结果 ";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem53.Text = "心脏彩超检查结果 ";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(132, 14);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "冠心病患者管理卡";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 856);
            this.layoutControlGroup1.Text = "冠心病患者随访服务记录表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem2,
            this.layoutControlItem34,
            this.layoutControlItem38,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem1,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem42,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layout添加药物,
            this.layout删除药物,
            this.layout药物列表,
            this.emptySpaceItem1,
            this.layoutControlItem37,
            this.layoutControlItem36,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.layoutControlGroup5,
            this.layoutControlItem26});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(731, 827);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "体检情况";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem4,
            this.layoutControlItem33,
            this.layoutControlItem24,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 190);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(731, 70);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "体检情况";
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.txt血压值;
            this.layoutControlItem28.CustomizationFormText = "血压值";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(288, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Tag = "check";
            this.layoutControlItem28.Text = "血压";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem29.Control = this.txt空腹血糖;
            this.layoutControlItem29.CustomizationFormText = "空腹血糖";
            this.layoutControlItem29.Location = new System.Drawing.Point(467, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(160, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Tag = "";
            this.layoutControlItem29.Text = "空腹血糖";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt体重;
            this.layoutControlItem4.CustomizationFormText = "体重";
            this.layoutControlItem4.Location = new System.Drawing.Point(288, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem4.Tag = "check";
            this.layoutControlItem4.Text = "体重";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.txt体质指数;
            this.layoutControlItem33.CustomizationFormText = "体质指数";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(288, 24);
            this.layoutControlItem33.Text = "体质指数";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem24.Control = this.txt身高;
            this.layoutControlItem24.CustomizationFormText = "身高";
            this.layoutControlItem24.Location = new System.Drawing.Point(529, 0);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Tag = "";
            this.layoutControlItem24.Text = "身高";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt心率;
            this.layoutControlItem3.CustomizationFormText = "心率";
            this.layoutControlItem3.Location = new System.Drawing.Point(288, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "心率";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "辅助检查";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem27,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.emptySpaceItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 260);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(731, 118);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "生活方式指导";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt日吸烟量;
            this.layoutControlItem8.CustomizationFormText = "日吸烟量";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "日吸烟量";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt饮酒情况;
            this.layoutControlItem9.CustomizationFormText = "日饮酒量";
            this.layoutControlItem9.Location = new System.Drawing.Point(357, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "日饮酒量";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt运动频率;
            this.layoutControlItem23.CustomizationFormText = "运动频率";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "运动频率";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt每次持续时间;
            this.layoutControlItem25.CustomizationFormText = "每次持续时间";
            this.layoutControlItem25.Location = new System.Drawing.Point(357, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem25.Tag = "check";
            this.layoutControlItem25.Text = "每次持续时间";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt摄盐情况1;
            this.layoutControlItem27.CustomizationFormText = "摄盐情况";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem27.Tag = "check";
            this.layoutControlItem27.Text = "摄盐情况";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.txt摄盐情况2;
            this.layoutControlItem30.CustomizationFormText = "~";
            this.layoutControlItem30.Location = new System.Drawing.Point(236, 48);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(121, 24);
            this.layoutControlItem30.Text = "~";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(9, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt心理调整;
            this.layoutControlItem31.CustomizationFormText = "心理调整";
            this.layoutControlItem31.Location = new System.Drawing.Point(357, 48);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "心理调整";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt遵医行为;
            this.layoutControlItem32.CustomizationFormText = "遵医行为";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem32.Tag = "check";
            this.layoutControlItem32.Text = "遵医行为";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(84, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(357, 72);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(372, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt发生时间;
            this.layoutControlItem12.CustomizationFormText = "随访日期";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 114);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(258, 26);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Tag = "check";
            this.layoutControlItem12.Text = "随访日期";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.radio随访方式;
            this.layoutControlItem14.CustomizationFormText = "随访方式";
            this.layoutControlItem14.Location = new System.Drawing.Point(258, 114);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(315, 26);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "随访方式";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.fl症状;
            this.layoutControlItem16.CustomizationFormText = "目前症状";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(199, 50);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(731, 50);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "目前症状";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt个人档案号;
            this.layoutControlItem13.CustomizationFormText = "个人档案号";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(356, 24);
            this.layoutControlItem13.Text = "个人档案号";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt姓名;
            this.layoutControlItem15.CustomizationFormText = "姓名";
            this.layoutControlItem15.Location = new System.Drawing.Point(356, 18);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem15.Text = "姓名";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt性别;
            this.layoutControlItem17.CustomizationFormText = "性别";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(356, 24);
            this.layoutControlItem17.Text = "性别";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt出生日期;
            this.layoutControlItem18.CustomizationFormText = "出生日期";
            this.layoutControlItem18.Location = new System.Drawing.Point(356, 42);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem18.Text = "出生日期";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.txt身份证号;
            this.layoutControlItem19.CustomizationFormText = "身份证号";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(356, 24);
            this.layoutControlItem19.Text = "身份证号";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt职业;
            this.layoutControlItem20.CustomizationFormText = "职业";
            this.layoutControlItem20.Location = new System.Drawing.Point(356, 66);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem20.Text = "职业";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt联系电话;
            this.layoutControlItem21.CustomizationFormText = "联系电话";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(356, 24);
            this.layoutControlItem21.Text = "联系电话";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt居住地址;
            this.layoutControlItem22.CustomizationFormText = "居住地址";
            this.layoutControlItem22.Location = new System.Drawing.Point(356, 90);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem22.Text = "居住地址";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lab考核项;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(731, 18);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem34.Control = this.radio用药情况;
            this.layoutControlItem34.CustomizationFormText = "用药情况";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 502);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(304, 26);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "用药情况";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Control = this.fl非药物治疗;
            this.layoutControlItem38.CustomizationFormText = "非药物治疗措施";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 627);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(192, 40);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(731, 40);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Tag = "check";
            this.layoutControlItem38.Text = "非药物治疗措施";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lbl创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 755);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "录入时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lbl最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(310, 755);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 18);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 18);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.fl随访分类;
            this.layoutControlItem1.CustomizationFormText = "此次随访分类";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 667);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(731, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "此次随访分类";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt医生建议;
            this.layoutControlItem10.CustomizationFormText = "此次随访\r\n医生建议";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 691);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(69, 40);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(731, 40);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "此次随访\r\n医生建议";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt医生签名;
            this.layoutControlItem11.CustomizationFormText = "随访医生签名";
            this.layoutControlItem11.Location = new System.Drawing.Point(310, 731);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Tag = "check";
            this.layoutControlItem11.Text = "随访医生签名";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt下次随访时间;
            this.layoutControlItem42.CustomizationFormText = "发生时间";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 731);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Tag = "check";
            this.layoutControlItem42.Text = "下次随访时间";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lab创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 779);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "录入人";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lab最近修改人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(310, 779);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近更新人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lab创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 803);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lab当前所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(310, 803);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(421, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layout添加药物
            // 
            this.layout添加药物.Control = this.btn添加药物;
            this.layout添加药物.CustomizationFormText = "layout添加药物";
            this.layout添加药物.Location = new System.Drawing.Point(304, 502);
            this.layout添加药物.Name = "layout添加药物";
            this.layout添加药物.Size = new System.Drawing.Size(58, 26);
            this.layout添加药物.Text = "layout添加药物";
            this.layout添加药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout添加药物.TextToControlDistance = 0;
            this.layout添加药物.TextVisible = false;
            this.layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout删除药物
            // 
            this.layout删除药物.Control = this.btn删除药物;
            this.layout删除药物.CustomizationFormText = "layout删除药物";
            this.layout删除药物.Location = new System.Drawing.Point(362, 502);
            this.layout删除药物.Name = "layout删除药物";
            this.layout删除药物.Size = new System.Drawing.Size(58, 26);
            this.layout删除药物.Text = "layout删除药物";
            this.layout删除药物.TextSize = new System.Drawing.Size(0, 0);
            this.layout删除药物.TextToControlDistance = 0;
            this.layout删除药物.TextVisible = false;
            this.layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layout药物列表
            // 
            this.layout药物列表.Control = this.gcDetail;
            this.layout药物列表.CustomizationFormText = "layout药物列表";
            this.layout药物列表.Location = new System.Drawing.Point(0, 528);
            this.layout药物列表.MinSize = new System.Drawing.Size(250, 75);
            this.layout药物列表.Name = "layout药物列表";
            this.layout药物列表.Size = new System.Drawing.Size(731, 75);
            this.layout药物列表.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout药物列表.Text = "layout药物列表";
            this.layout药物列表.TextSize = new System.Drawing.Size(0, 0);
            this.layout药物列表.TextToControlDistance = 0;
            this.layout药物列表.TextVisible = false;
            this.layout药物列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(420, 502);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(311, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem37.Control = this.fl特殊治疗;
            this.layoutControlItem37.CustomizationFormText = "特殊治疗";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 603);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(731, 24);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Tag = "check";
            this.layoutControlItem37.Text = "其他治疗";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.txt服药依从性;
            this.layoutControlItem36.CustomizationFormText = "服药依从性";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 448);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(304, 25);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Tag = "check";
            this.layoutControlItem36.Text = "用药依从性";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio不良反应;
            this.layoutControlItem6.CustomizationFormText = "药物不良反应";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 473);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(304, 29);
            this.layoutControlItem6.Text = "药物不良反应";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt药物副作用详述;
            this.layoutControlItem7.CustomizationFormText = "副作用详述";
            this.layoutControlItem7.Location = new System.Drawing.Point(304, 473);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(427, 29);
            this.layoutControlItem7.Text = "副作用详述";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(304, 448);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(427, 25);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.CustomizationFormText = "辅助检查";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem51,
            this.layoutControlItem5});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 378);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(731, 70);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "辅助检查";
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt心电图检查;
            this.layoutControlItem51.CustomizationFormText = "心电图检查结果";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem51.Tag = "";
            this.layoutControlItem51.Text = "心电图检查结果";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt辅助检查;
            this.layoutControlItem5.CustomizationFormText = "辅助检查";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem5.Tag = "";
            this.layoutControlItem5.Text = "辅助检查";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txt随访方式其他;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(573, 114);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(158, 26);
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // UC冠心病患者随访服务记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC冠心病患者随访服务记录表";
            this.Size = new System.Drawing.Size(750, 500);
            this.Load += new System.EventHandler(this.UC冠心病患者随访服务记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心理调整.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt摄盐情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物副作用详述.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生建议.Properties)).EndInit();
            this.fl随访分类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch控制满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch控制不满意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt此次随访分类并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心肌酶学.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt冠状动脉造影.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心脏彩超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图运动负荷.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio冠心病类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            this.fl非药物治疗.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck非药物未采取措施.Properties)).EndInit();
            this.fl特殊治疗.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch特殊治疗无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio用药情况.Properties)).EndInit();
            this.fl症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.check无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout添加药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout删除药物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout药物列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.FlowLayoutPanel fl症状;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraEditors.DateEdit txt发生时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private Library.UserControls.UCTxtLbl txt身高;
        private DevExpress.XtraEditors.CheckEdit check无症状;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private System.Windows.Forms.FlowLayoutPanel fl非药物治疗;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraEditors.CheckEdit checkEdit42;
        private DevExpress.XtraEditors.CheckEdit checkEdit43;
        private DevExpress.XtraEditors.CheckEdit checkEdit44;
        private DevExpress.XtraEditors.CheckEdit checkEdit45;
        private DevExpress.XtraEditors.CheckEdit ck非药物未采取措施;
        private System.Windows.Forms.FlowLayoutPanel fl特殊治疗;
        private DevExpress.XtraEditors.CheckEdit ch特殊治疗无;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraEditors.RadioGroup radio用药情况;
        private Library.UserControls.UCTxtLbl txt空腹血糖;
        private Library.UserControls.UCTxtLblTxtLbl txt血压值;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.LabelControl lab当前所属机构;
        private DevExpress.XtraEditors.LabelControl lab创建机构;
        private DevExpress.XtraEditors.LabelControl lab最近修改人;
        private DevExpress.XtraEditors.LabelControl lab创建人;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraEditors.RadioGroup radio冠心病类型;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraEditors.TextEdit txt心肌酶学;
        private DevExpress.XtraEditors.TextEdit txt冠状动脉造影;
        private DevExpress.XtraEditors.TextEdit txt心脏彩超;
        private DevExpress.XtraEditors.TextEdit txt心电图运动负荷;
        private DevExpress.XtraEditors.TextEdit txt心电图检查;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.FlowLayoutPanel fl随访分类;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.MemoEdit txt医生建议;
        private DevExpress.XtraEditors.CheckEdit ch控制满意;
        private DevExpress.XtraEditors.CheckEdit ch控制不满意;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit ck并发症;
        private DevExpress.XtraEditors.TextEdit txt此次随访分类并发症;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.SimpleButton btn添加药物;
        private DevExpress.XtraEditors.SimpleButton btn删除药物;
        private DevExpress.XtraGrid.GridControl gcDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LookUpEdit txt服药依从性;
        private DevExpress.XtraEditors.CheckEdit ch症状其他;
        private DevExpress.XtraEditors.TextEdit txt症状其他;
        private DevExpress.XtraEditors.TextEdit txt心率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private UCTxtLblTxtLbl txt体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txt辅助检查;
        private DevExpress.XtraEditors.RadioGroup radio不良反应;
        private DevExpress.XtraEditors.TextEdit txt药物副作用详述;
        private UCTxtLblTxtLbl txt日吸烟量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private UCTxtLblTxtLbl txt饮酒情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private UCTxtLblTxtLbl txt运动频率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private UCTxtLblTxtLbl txt每次持续时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.LookUpEdit txt摄盐情况2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.LookUpEdit txt心理调整;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.LookUpEdit txt遵医行为;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private UCTxtLblTxtLbl txt体质指数;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layout添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layout删除药物;
        private DevExpress.XtraLayout.LayoutControlItem layout药物列表;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private System.Windows.Forms.TextBox txt随访方式其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
    }
}
