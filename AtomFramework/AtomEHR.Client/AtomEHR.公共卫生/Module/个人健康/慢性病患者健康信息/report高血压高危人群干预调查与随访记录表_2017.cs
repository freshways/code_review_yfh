﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using AtomEHR.Business;
using System.Data;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class report高血压高危人群干预调查与随访记录表_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        List<string> dates;
        DataSet _ds高危人群;
        bllMXB高血压高危人群干预调查与随访记录表 _bll高危人群 = new bllMXB高血压高危人群干预调查与随访记录表();
        public report高血压高危人群干预调查与随访记录表_2017()
        {
            InitializeComponent();
        }
        public report高血压高危人群干预调查与随访记录表_2017(string docNo, List<string> _dates)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.dates = _dates;
            _ds高危人群 = _bll高危人群.GetReportData(docNo, dates, true);

            Bind_DataSource(_ds高危人群);
        }

        private void Bind_DataSource(DataSet _ds高危人群)
        {
            DataTable dt_高危人群 = _ds高危人群.Tables[Models.tb_MXB高血压高危人群干预调查与随访记录表.__TableName];
            DataTable dt_高危人群个人膳食 = _ds高危人群.Tables[Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.__TableName];
            DataTable dt_健康档案 = _ds高危人群.Tables[Models.tb_健康档案.__TableName];

            this.DataSource = dt_高危人群个人膳食;
            this.DataMember = dt_高危人群个人膳食.TableName;

            this.xrTableCell_食物名称.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.食物名称);
            this.xrTableCell_是否吃.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.是否吃);
            this.xrTableCell_次天.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.次_天);
            this.xrTableCell_次周.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.次_周);
            this.xrTableCell_次月.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.次_月);
            this.xrTableCell_食用量.DataBindings.Add("Text", dt_高危人群个人膳食, Models.tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.平均每次食用量);

            //姓名
            if (dt_健康档案 == null || dt_健康档案.Rows.Count == 0) return;
            xrLabel_姓名.Text = dt_健康档案.Rows[0][Models.tb_健康档案.姓名].ToString();
            //编号
            string str_个人档案编号 = dt_健康档案.Rows[0][Models.tb_健康档案.个人档案编号].ToString();
            char[] char_编号 = str_个人档案编号.Substring(str_个人档案编号.Length - 8, 8).ToCharArray();
            for (int i = 0; i < char_编号.Length; i++)
            {
                string xrName = "xrLabel_编号" + (i + 1);
                XRLabel xrl = (XRLabel)FindControl(xrName, false);
                xrl.Text = char_编号[i].ToString();
            }
            if (dt_高危人群 != null && dt_高危人群.Rows.Count > 0)
            {
                this.xrLabel_膳食1.Text = dt_高危人群.Rows[0][Models.tb_MXB高血压高危人群干预调查与随访记录表.家庭膳食1].ToString();
                this.xrLabel_膳食11.Text = dt_高危人群.Rows[0][Models.tb_MXB高血压高危人群干预调查与随访记录表.家庭膳食1_1].ToString();
                this.xrLabel_膳食2.Text = dt_高危人群.Rows[0][Models.tb_MXB高血压高危人群干预调查与随访记录表.家庭膳食2].ToString();
                this.xrLabel_膳食3.Text = dt_高危人群.Rows[0][Models.tb_MXB高血压高危人群干预调查与随访记录表.家庭膳食3].ToString();
                //循环绑定控件
                for (int i = 0; i < dt_高危人群.Rows.Count; i++)
                {
                    //随访日期
                    DateTime strs_随访日期 = Convert.ToDateTime(dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.随访日期].ToString());
                    string xrTable_随访日期 = "xrLabel随访日期" + (i + 1);
                    XRTableCell XRT__随访日期 = (XRTableCell)FindControl(xrTable_随访日期, false);
                    XRT__随访日期.Text = strs_随访日期.Year.ToString() + "年" + strs_随访日期.Month.ToString() + "月" + strs_随访日期.Day.ToString() + "日";
                    //随访方式
                    string xrLabel_随访方式 = "xrLabel_随访方式_" + (i + 1);
                    XRLabel XRL_随访方式 = (XRLabel)FindControl(xrLabel_随访方式, false);
                    string str_随访方式 = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.随访方式].ToString();
                    XRL_随访方式.Text = str_随访方式.Equals("4") ? "" : str_随访方式;
                    //血压
                    string str_收缩压 = dt_高危人群.Rows[i][Models.tb_MXB高血压随访表.收缩压].ToString();
                    string str_舒张压 = dt_高危人群.Rows[i][Models.tb_MXB高血压随访表.舒张压].ToString();
                    string xrTable_血压 = "xrTable_血压_" + (i + 1);
                    XRTableCell XRT_血压 = (XRTableCell)FindControl(xrTable_血压, false);
                    XRT_血压.Text = str_收缩压 + " / " + str_舒张压;
                    //日吸烟量
                    string xrTable_日吸烟量1 = "xrTable_日吸烟量_" + (i + 1) + "_1";
                    string xrTable_日吸烟量2 = "xrTable_日吸烟量_" + (i + 1) + "_2";
                    XRLabel XRT_日吸烟量1 = (XRLabel)FindControl(xrTable_日吸烟量1, false);
                    XRLabel XRT_日吸烟量2 = (XRLabel)FindControl(xrTable_日吸烟量2, false);
                    XRT_日吸烟量1.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.日吸烟量1].ToString();
                    XRT_日吸烟量2.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.日吸烟量2].ToString();
                    //日饮酒量
                    string xrTable_日饮酒量1 = "xrTable_日饮酒量_" + (i + 1) + "_1";
                    string xrTable_日饮酒量2 = "xrTable_日饮酒量_" + (i + 1) + "_2";
                    XRLabel XRT_日饮酒量1 = (XRLabel)FindControl(xrTable_日饮酒量1, false);
                    XRLabel XRT_日饮酒量2 = (XRLabel)FindControl(xrTable_日饮酒量2, false);
                    XRT_日饮酒量1.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.日饮酒量1].ToString();
                    XRT_日饮酒量2.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.日饮酒量2].ToString();
                    //运动
                    string xrTable_运动频率1 = "xrTable_运动频率_" + (i + 1) + "_1";
                    string xrTable_运动频率2 = "xrTable_运动频率_" + (i + 1) + "_2";
                    string xrTable_运动时长1 = "xrTable_运动时长_" + (i + 1) + "_1";
                    string xrTable_运动时长2 = "xrTable_运动时长_" + (i + 1) + "_2";
                    XRLabel XRT_运动频率1 = (XRLabel)FindControl(xrTable_运动频率1, false);
                    XRLabel XRT_运动频率2 = (XRLabel)FindControl(xrTable_运动频率2, false);
                    XRLabel XRT_运动时长1 = (XRLabel)FindControl(xrTable_运动时长1, false);
                    XRLabel XRT_运动时长2 = (XRLabel)FindControl(xrTable_运动时长2, false);
                    XRT_运动频率1.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动次周1].ToString();
                    XRT_运动频率2.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动次周2].ToString();
                    object fl_运动时长1 = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动分钟次1] is DBNull ? 0 : dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动分钟次1];
                    object fl_运动时长2 = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动分钟次2] is DBNull ? 0 : dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.运动分钟次2];
                    XRT_运动时长1.Text = Convert.ToInt32(fl_运动时长1).ToString();
                    XRT_运动时长2.Text = Convert.ToInt32(fl_运动时长1).ToString();
                    //摄盐情况
                    string str_摄盐情况1 = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.摄盐情况1].ToString();
                    string str_摄盐情况2 = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.摄盐情况2].ToString();
                    string xrTable_摄盐情况1 = "xrTable_摄盐情况_" + (i + 1) + "_1";
                    string xrTable_摄盐情况2 = "xrTable_摄盐情况_" + (i + 1) + "_2";
                    XRLabel XRT_摄盐情况1 = (XRLabel)FindControl(xrTable_摄盐情况1, false);
                    XRLabel XRT_摄盐情况2 = (XRLabel)FindControl(xrTable_摄盐情况2, false);
                    switch (str_摄盐情况1)
                    {
                        case "1":
                            XRT_摄盐情况1.Text = "轻";
                            break;
                        case "2":
                            XRT_摄盐情况1.Text = "中";
                            break;
                        case "3":
                            XRT_摄盐情况1.Text = "重";
                            break;
                        default:
                            break;
                    }
                    switch (str_摄盐情况2)
                    {
                        case "1":
                            XRT_摄盐情况2.Text = "轻";
                            break;
                        case "2":
                            XRT_摄盐情况2.Text = "中";
                            break;
                        case "3":
                            XRT_摄盐情况2.Text = "重";
                            break;
                        default:
                            break;
                    }
                    //食盐摄入量
                    string xrLabel_食盐摄入量 = "xrLabel_食盐摄入量_" + (i + 1);
                    XRLabel XRL_食盐摄入量 = (XRTableCell)FindControl(xrLabel_食盐摄入量, false);
                    XRL_食盐摄入量.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.食盐摄入量].ToString();
                    //心里调整
                    string xrLabel_心理调整 = "xrLabel_心理调整_" + (i + 1);
                    XRLabel XRL_心理调整 = (XRLabel)FindControl(xrLabel_心理调整, false);
                    XRL_心理调整.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.心理调整].ToString();

                    //下次随访日期
                    DateTime strs_下次随访日期 = Convert.ToDateTime(dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.随访日期].ToString());
                    string xrTable_下次随访日期 = "xrTable_下次随访日期_" + (i + 1);
                    XRTableCell XRT_下次随访日期 = (XRTableCell)FindControl(xrTable_下次随访日期, false);

                    XRT_下次随访日期.Text = strs_下次随访日期.Year.ToString() + "年" + strs_下次随访日期.Month.ToString() + "月" + strs_下次随访日期.Day.ToString() + "日";
                    //随访医生签字
                    string xrTable_随访医生签字 = "xrTable_随访医生签字_" + (i + 1);
                    XRTableCell XRT_随访医生签字 = (XRTableCell)FindControl(xrTable_随访医生签字, false);
                    XRT_随访医生签字.Text = dt_高危人群.Rows[i][Models.tb_MXB高血压高危人群干预调查与随访记录表.随访医生签名].ToString();
                }
            }
            
        }

    }
}
