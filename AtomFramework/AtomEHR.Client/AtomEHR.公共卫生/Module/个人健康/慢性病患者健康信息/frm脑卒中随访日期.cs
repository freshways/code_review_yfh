﻿using AtomEHR.Business;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class frm脑卒中随访日期 : Form
    {
        #region Fields
        private string docNo;
        private string year;
        DataSet _ds脑卒中;
        bllMXB脑卒中随访表 _bll脑卒中 = new bllMXB脑卒中随访表();
        #endregion
        public frm脑卒中随访日期()
        {
            InitializeComponent();
        }

        public frm脑卒中随访日期(string docNo, string year)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.year = year;
            _ds脑卒中 = _bll脑卒中.GetInfoByDate(docNo, year, true);
            DoBindingDataSource(_ds脑卒中);
        }

        private void DoBindingDataSource(DataSet _ds脑卒中)
        {
            DataTable dt脑卒中 = _ds脑卒中.Tables[Models.tb_MXB脑卒中随访表.__TableName];
            if (dt脑卒中 != null && dt脑卒中.Rows.Count > 0)
            {
                gc脑卒中随访.DataSource = _ds脑卒中.Tables[0];
                this.gv脑卒中随访.BestFitColumns();
                this.gc脑卒中随访.BringToFront();
                //在表中增加一个列 add（列名，列类型）  
                dt脑卒中.Columns.Add("check", typeof(bool));
                dt脑卒中.Columns.Add("col随访日期", typeof(string));
                //给每一行的该列赋值  
                for (int i = 0; i < dt脑卒中.Rows.Count; i++)
                {
                    dt脑卒中.Rows[i]["check"] = "false";
                    dt脑卒中.Rows[i]["col随访日期"] = dt脑卒中.Rows[i][Models.tb_MXB脑卒中随访表.发生时间].ToString();
                }

                string value = "";
                string strSelected = "";
                for (int i = 0; i < gv脑卒中随访.RowCount; i++)
                {
                    //获取选中行的check值
                    value = gv脑卒中随访.GetDataRow(i)["check"].ToString();
                    if (value == "true")
                    {
                        strSelected += gv脑卒中随访.GetRowCellValue(i, "随访日期");
                    }
                }
            }
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (gv脑卒中随访.SelectedRowsCount == 0)
            {
                MessageBox.Show("请选择随访日期！", "错误提示");
            }
            else if (gv脑卒中随访.SelectedRowsCount < 5)
            {
                int[] ints = this.gv脑卒中随访.GetSelectedRows();
                List<string> list = new List<string>();
                for (int i = 0; i < ints.Length; i++)
                {
                    DataRowView row = (DataRowView)this.gv脑卒中随访.GetRow(ints[i]);
                    string selectDate;
                    if (row != null)
                    {
                        selectDate = row["发生时间"].ToString();
                        list.Add(selectDate);
                    }
                }
                report脑卒中患者随访服务记录表 report = new report脑卒中患者随访服务记录表(docNo, list);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("最多选择4次随访记录！");

            }
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();//关闭窗体
        }
    }
}
