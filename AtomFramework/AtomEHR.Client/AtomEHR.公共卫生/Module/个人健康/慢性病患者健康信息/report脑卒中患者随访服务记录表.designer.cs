﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class report脑卒中患者随访服务记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访日期3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访日期4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访方式1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访方式2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访方式3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访方式4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中分类12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脑卒中部位12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt无症状47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt半身不遂1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt半身不遂2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt半身不遂3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt半身不遂4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人病史12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt并发症12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新发卒中12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身高1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身高2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身高3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身高4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次体重1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次体重2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次体重1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次体重2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次体重1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次体重2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次体重1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次体重2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次体质指数1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次体质指数2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次体质指数1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次体质指数2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次体质指数1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次体质指数2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次体质指数1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次体质指数2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腰围1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腰围2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腰围3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腰围4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt空腹血糖1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt空腹血糖2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt空腹血糖3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt空腹血糖4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肢体功能1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肢体功能2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肢体功能3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肢体功能4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次吸烟量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次吸烟量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次吸烟量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次吸烟量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次吸烟量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次吸烟量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次吸烟量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次吸烟量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次饮酒量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次饮酒量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次饮酒量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次饮酒量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次饮酒量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次饮酒量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次饮酒量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次饮酒量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt次周24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt分钟24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt摄盐情况8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心理调整1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心理调整2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心理调整3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心理调整4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt遵医行为1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt遵医行为2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt遵医行为3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt遵医行为4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt辅助检查1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt辅助检查2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt辅助检查3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt辅助检查4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt服药依从性1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt服药依从性2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt服药依从性3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt服药依从性4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应有1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应有2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应有3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应有4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt药物不良反应4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt随访分类1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访分类2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访分类3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访分类4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次用法用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次用法用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次用法用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次用法用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell456 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell457 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次用法用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次用法用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次用法用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次用法用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell482 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell483 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell508 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell509 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次用法用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次用法用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次用法用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次用法用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell534 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell535 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt其他药物1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt其他药物2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt其他药物3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt其他药物4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell560 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell561 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次用法用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次用法用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次用法用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次用法用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt转诊原因1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt转诊原因2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt转诊原因3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt转诊原因4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell438 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt机构及科别1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt机构及科别2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt机构及科别3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt机构及科别4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell446 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt医生建议1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt医生建议2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt医生建议3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt医生建议4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell455 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell489 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell444 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell454 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell488 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell496 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell497 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.txt姓名,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable1});
            this.Detail.HeightF = 1264.583F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(542F, 69.66999F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "一";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt姓名
            // 
            this.txt姓名.CanGrow = false;
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(150F, 64.66667F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(150F, 25F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UsePadding = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(494F, 69.67F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(522F, 69.67F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(562F, 69.66667F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(590F, 69.67F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(618F, 69.67F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(646F, 69.67F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(674F, 69.67F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(466.665F, 69.66667F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(419.79F, 67.66666F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(46.87503F, 25F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "编号";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(86.45834F, 64.66667F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(63.54F, 25F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("仿宋", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(200F, 24.91668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(301.04F, 30F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "脑卒中患者随访服务记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.66666F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow54});
            this.xrTable1.SizeF = new System.Drawing.SizeF(774F, 980F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.txt随访日期1,
            this.txt随访日期2,
            this.txt随访日期3,
            this.txt随访日期4});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.Weight = 0.8D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "随访日期";
            this.xrTableCell4.Weight = 0.49033341485415372D;
            // 
            // txt随访日期1
            // 
            this.txt随访日期1.Name = "txt随访日期1";
            this.txt随访日期1.Weight = 0.63042867963420413D;
            // 
            // txt随访日期2
            // 
            this.txt随访日期2.Name = "txt随访日期2";
            this.txt随访日期2.Weight = 0.63042867963420424D;
            // 
            // txt随访日期3
            // 
            this.txt随访日期3.Name = "txt随访日期3";
            this.txt随访日期3.Weight = 0.63042867963420413D;
            // 
            // txt随访日期4
            // 
            this.txt随访日期4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访日期4.Name = "txt随访日期4";
            this.txt随访日期4.StylePriority.UseBorders = false;
            this.txt随访日期4.Weight = 0.630428739014318D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell11,
            this.xrTableCell7,
            this.xrTableCell12,
            this.xrTableCell8,
            this.xrTableCell13,
            this.xrTableCell9,
            this.xrTableCell14,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 0.8D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "随访方式";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.49033344454421057D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "1门诊 2家庭 3电话";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.56427260707908544D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访方式1});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 0.066156065132604552D;
            // 
            // txt随访方式1
            // 
            this.txt随访方式1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访方式1.CanGrow = false;
            this.txt随访方式1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访方式1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt随访方式1.Name = "txt随访方式1";
            this.txt随访方式1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访方式1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访方式1.StylePriority.UseBorders = false;
            this.txt随访方式1.StylePriority.UseFont = false;
            this.txt随访方式1.StylePriority.UsePadding = false;
            this.txt随访方式1.StylePriority.UseTextAlignment = false;
            this.txt随访方式1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "1门诊 2家庭 3电话";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 0.564272606871255D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访方式2});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Weight = 0.0661561098755202D;
            // 
            // txt随访方式2
            // 
            this.txt随访方式2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访方式2.CanGrow = false;
            this.txt随访方式2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访方式2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt随访方式2.Name = "txt随访方式2";
            this.txt随访方式2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访方式2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访方式2.StylePriority.UseBorders = false;
            this.txt随访方式2.StylePriority.UseFont = false;
            this.txt随访方式2.StylePriority.UsePadding = false;
            this.txt随访方式2.StylePriority.UseTextAlignment = false;
            this.txt随访方式2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "1门诊 2家庭 3电话";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell13.Weight = 0.56427258831496951D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访方式3});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Weight = 0.066156091319234639D;
            // 
            // txt随访方式3
            // 
            this.txt随访方式3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访方式3.CanGrow = false;
            this.txt随访方式3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访方式3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt随访方式3.Name = "txt随访方式3";
            this.txt随访方式3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访方式3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访方式3.StylePriority.UseBorders = false;
            this.txt随访方式3.StylePriority.UseFont = false;
            this.txt随访方式3.StylePriority.UsePadding = false;
            this.txt随访方式3.StylePriority.UseTextAlignment = false;
            this.txt随访方式3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "1门诊 2家庭 3电话";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.56427258831496963D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访方式4});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 0.066156091319234639D;
            // 
            // txt随访方式4
            // 
            this.txt随访方式4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访方式4.CanGrow = false;
            this.txt随访方式4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访方式4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt随访方式4.Name = "txt随访方式4";
            this.txt随访方式4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访方式4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访方式4.StylePriority.UseBorders = false;
            this.txt随访方式4.StylePriority.UseFont = false;
            this.txt随访方式4.StylePriority.UsePadding = false;
            this.txt随访方式4.StylePriority.UseTextAlignment = false;
            this.txt随访方式4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell25,
            this.xrTableCell24,
            this.xrTableCell35,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell30,
            this.xrTableCell29,
            this.xrTableCell31,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell34,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell22,
            this.xrTableCell23});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 0.8D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "脑卒中分类";
            this.xrTableCell15.Weight = 0.49033344454421057D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类1});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.24905825491091971D;
            // 
            // txt脑卒中分类1
            // 
            this.txt脑卒中分类1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类1.CanGrow = false;
            this.txt脑卒中分类1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类1.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt脑卒中分类1.Name = "txt脑卒中分类1";
            this.txt脑卒中分类1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类1.StylePriority.UseBorders = false;
            this.txt脑卒中分类1.StylePriority.UseFont = false;
            this.txt脑卒中分类1.StylePriority.UsePadding = false;
            this.txt脑卒中分类1.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "/";
            this.xrTableCell24.Weight = 0.038915351327791774D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类2});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 0.066156096752515048D;
            // 
            // txt脑卒中分类2
            // 
            this.txt脑卒中分类2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类2.CanGrow = false;
            this.txt脑卒中分类2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt脑卒中分类2.Name = "txt脑卒中分类2";
            this.txt脑卒中分类2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类2.StylePriority.UseBorders = false;
            this.txt脑卒中分类2.StylePriority.UseFont = false;
            this.txt脑卒中分类2.StylePriority.UsePadding = false;
            this.txt脑卒中分类2.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "/";
            this.xrTableCell16.Weight = 0.038915350823060794D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类3});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.23738361839740263D;
            // 
            // txt脑卒中分类3
            // 
            this.txt脑卒中分类3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类3.CanGrow = false;
            this.txt脑卒中分类3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中分类3.Name = "txt脑卒中分类3";
            this.txt脑卒中分类3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类3.StylePriority.UseBorders = false;
            this.txt脑卒中分类3.StylePriority.UseFont = false;
            this.txt脑卒中分类3.StylePriority.UsePadding = false;
            this.txt脑卒中分类3.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类4});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Weight = 0.24905825165243595D;
            // 
            // txt脑卒中分类4
            // 
            this.txt脑卒中分类4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类4.CanGrow = false;
            this.txt脑卒中分类4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类4.LocationFloat = new DevExpress.Utils.PointFloat(49.00004F, 2.799988F);
            this.txt脑卒中分类4.Name = "txt脑卒中分类4";
            this.txt脑卒中分类4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类4.StylePriority.UseBorders = false;
            this.txt脑卒中分类4.StylePriority.UseFont = false;
            this.txt脑卒中分类4.StylePriority.UsePadding = false;
            this.txt脑卒中分类4.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "/";
            this.xrTableCell26.Weight = 0.038915350771103217D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类5});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.066156096700557443D;
            // 
            // txt脑卒中分类5
            // 
            this.txt脑卒中分类5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类5.CanGrow = false;
            this.txt脑卒中分类5.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中分类5.Name = "txt脑卒中分类5";
            this.txt脑卒中分类5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类5.StylePriority.UseBorders = false;
            this.txt脑卒中分类5.StylePriority.UseFont = false;
            this.txt脑卒中分类5.StylePriority.UsePadding = false;
            this.txt脑卒中分类5.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "/";
            this.xrTableCell18.Weight = 0.038915347059846089D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类6});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.23738367056283252D;
            // 
            // txt脑卒中分类6
            // 
            this.txt脑卒中分类6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类6.CanGrow = false;
            this.txt脑卒中分类6.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中分类6.Name = "txt脑卒中分类6";
            this.txt脑卒中分类6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类6.StylePriority.UseBorders = false;
            this.txt脑卒中分类6.StylePriority.UseFont = false;
            this.txt脑卒中分类6.StylePriority.UsePadding = false;
            this.txt脑卒中分类6.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类7});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Weight = 0.2490582470133646D;
            // 
            // txt脑卒中分类7
            // 
            this.txt脑卒中分类7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类7.CanGrow = false;
            this.txt脑卒中分类7.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类7.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt脑卒中分类7.Name = "txt脑卒中分类7";
            this.txt脑卒中分类7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类7.StylePriority.UseBorders = false;
            this.txt脑卒中分类7.StylePriority.UseFont = false;
            this.txt脑卒中分类7.StylePriority.UsePadding = false;
            this.txt脑卒中分类7.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "/";
            this.xrTableCell29.Weight = 0.038915351698917466D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类8});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.066156093917114633D;
            // 
            // txt脑卒中分类8
            // 
            this.txt脑卒中分类8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类8.CanGrow = false;
            this.txt脑卒中分类8.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt脑卒中分类8.Name = "txt脑卒中分类8";
            this.txt脑卒中分类8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类8.StylePriority.UseBorders = false;
            this.txt脑卒中分类8.StylePriority.UseFont = false;
            this.txt脑卒中分类8.StylePriority.UsePadding = false;
            this.txt脑卒中分类8.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "/";
            this.xrTableCell20.Weight = 0.038915349843288927D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类9});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.2373836371615185D;
            // 
            // txt脑卒中分类9
            // 
            this.txt脑卒中分类9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类9.CanGrow = false;
            this.txt脑卒中分类9.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt脑卒中分类9.Name = "txt脑卒中分类9";
            this.txt脑卒中分类9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类9.StylePriority.UseBorders = false;
            this.txt脑卒中分类9.StylePriority.UseFont = false;
            this.txt脑卒中分类9.StylePriority.UsePadding = false;
            this.txt脑卒中分类9.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类10});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Weight = 0.2490582470133646D;
            // 
            // txt脑卒中分类10
            // 
            this.txt脑卒中分类10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类10.CanGrow = false;
            this.txt脑卒中分类10.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类10.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt脑卒中分类10.Name = "txt脑卒中分类10";
            this.txt脑卒中分类10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类10.StylePriority.UseBorders = false;
            this.txt脑卒中分类10.StylePriority.UseFont = false;
            this.txt脑卒中分类10.StylePriority.UsePadding = false;
            this.txt脑卒中分类10.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "/";
            this.xrTableCell32.Weight = 0.03891535169891755D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类11});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.066156093917114689D;
            // 
            // txt脑卒中分类11
            // 
            this.txt脑卒中分类11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类11.CanGrow = false;
            this.txt脑卒中分类11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中分类11.Name = "txt脑卒中分类11";
            this.txt脑卒中分类11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类11.StylePriority.UseBorders = false;
            this.txt脑卒中分类11.StylePriority.UseFont = false;
            this.txt脑卒中分类11.StylePriority.UsePadding = false;
            this.txt脑卒中分类11.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "/";
            this.xrTableCell22.Weight = 0.038915349843288954D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中分类12});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.2373836371615185D;
            // 
            // txt脑卒中分类12
            // 
            this.txt脑卒中分类12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中分类12.CanGrow = false;
            this.txt脑卒中分类12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中分类12.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt脑卒中分类12.Name = "txt脑卒中分类12";
            this.txt脑卒中分类12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中分类12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中分类12.StylePriority.UseBorders = false;
            this.txt脑卒中分类12.StylePriority.UseFont = false;
            this.txt脑卒中分类12.StylePriority.UsePadding = false;
            this.txt脑卒中分类12.StylePriority.UseTextAlignment = false;
            this.txt脑卒中分类12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.8D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "脑卒中部位";
            this.xrTableCell36.Weight = 0.49033344454421057D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位1});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.24905825491091971D;
            // 
            // txt脑卒中部位1
            // 
            this.txt脑卒中部位1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位1.CanGrow = false;
            this.txt脑卒中部位1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位1.LocationFloat = new DevExpress.Utils.PointFloat(49F, 2.8F);
            this.txt脑卒中部位1.Name = "txt脑卒中部位1";
            this.txt脑卒中部位1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位1.StylePriority.UseBorders = false;
            this.txt脑卒中部位1.StylePriority.UseFont = false;
            this.txt脑卒中部位1.StylePriority.UsePadding = false;
            this.txt脑卒中部位1.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "/";
            this.xrTableCell38.Weight = 0.038915351327791774D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位2});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.066156096752515048D;
            // 
            // txt脑卒中部位2
            // 
            this.txt脑卒中部位2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位2.CanGrow = false;
            this.txt脑卒中部位2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位2.Name = "txt脑卒中部位2";
            this.txt脑卒中部位2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位2.StylePriority.UseBorders = false;
            this.txt脑卒中部位2.StylePriority.UseFont = false;
            this.txt脑卒中部位2.StylePriority.UsePadding = false;
            this.txt脑卒中部位2.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "/";
            this.xrTableCell40.Weight = 0.038915350823060794D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位3});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.23738361839740263D;
            // 
            // txt脑卒中部位3
            // 
            this.txt脑卒中部位3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位3.CanGrow = false;
            this.txt脑卒中部位3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位3.Name = "txt脑卒中部位3";
            this.txt脑卒中部位3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位3.StylePriority.UseBorders = false;
            this.txt脑卒中部位3.StylePriority.UseFont = false;
            this.txt脑卒中部位3.StylePriority.UsePadding = false;
            this.txt脑卒中部位3.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位4});
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.24905825165243595D;
            // 
            // txt脑卒中部位4
            // 
            this.txt脑卒中部位4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位4.CanGrow = false;
            this.txt脑卒中部位4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位4.LocationFloat = new DevExpress.Utils.PointFloat(49.00004F, 2.799988F);
            this.txt脑卒中部位4.Name = "txt脑卒中部位4";
            this.txt脑卒中部位4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位4.StylePriority.UseBorders = false;
            this.txt脑卒中部位4.StylePriority.UseFont = false;
            this.txt脑卒中部位4.StylePriority.UsePadding = false;
            this.txt脑卒中部位4.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "/";
            this.xrTableCell43.Weight = 0.038915350771103217D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位5});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.066156096700557443D;
            // 
            // txt脑卒中部位5
            // 
            this.txt脑卒中部位5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位5.CanGrow = false;
            this.txt脑卒中部位5.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位5.Name = "txt脑卒中部位5";
            this.txt脑卒中部位5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位5.StylePriority.UseBorders = false;
            this.txt脑卒中部位5.StylePriority.UseFont = false;
            this.txt脑卒中部位5.StylePriority.UsePadding = false;
            this.txt脑卒中部位5.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "/";
            this.xrTableCell45.Weight = 0.038915347059846089D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位6});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.23738367056283252D;
            // 
            // txt脑卒中部位6
            // 
            this.txt脑卒中部位6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位6.CanGrow = false;
            this.txt脑卒中部位6.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位6.Name = "txt脑卒中部位6";
            this.txt脑卒中部位6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位6.StylePriority.UseBorders = false;
            this.txt脑卒中部位6.StylePriority.UseFont = false;
            this.txt脑卒中部位6.StylePriority.UsePadding = false;
            this.txt脑卒中部位6.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位7});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.2490582470133646D;
            // 
            // txt脑卒中部位7
            // 
            this.txt脑卒中部位7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位7.CanGrow = false;
            this.txt脑卒中部位7.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位7.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt脑卒中部位7.Name = "txt脑卒中部位7";
            this.txt脑卒中部位7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位7.StylePriority.UseBorders = false;
            this.txt脑卒中部位7.StylePriority.UseFont = false;
            this.txt脑卒中部位7.StylePriority.UsePadding = false;
            this.txt脑卒中部位7.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Text = "/";
            this.xrTableCell48.Weight = 0.038915351698917466D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位8});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.066156093917114633D;
            // 
            // txt脑卒中部位8
            // 
            this.txt脑卒中部位8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位8.CanGrow = false;
            this.txt脑卒中部位8.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位8.Name = "txt脑卒中部位8";
            this.txt脑卒中部位8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位8.StylePriority.UseBorders = false;
            this.txt脑卒中部位8.StylePriority.UseFont = false;
            this.txt脑卒中部位8.StylePriority.UsePadding = false;
            this.txt脑卒中部位8.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "/";
            this.xrTableCell50.Weight = 0.038915349843288927D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位9});
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.2373836371615185D;
            // 
            // txt脑卒中部位9
            // 
            this.txt脑卒中部位9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位9.CanGrow = false;
            this.txt脑卒中部位9.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位9.Name = "txt脑卒中部位9";
            this.txt脑卒中部位9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位9.StylePriority.UseBorders = false;
            this.txt脑卒中部位9.StylePriority.UseFont = false;
            this.txt脑卒中部位9.StylePriority.UsePadding = false;
            this.txt脑卒中部位9.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位10});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Weight = 0.2490582470133646D;
            // 
            // txt脑卒中部位10
            // 
            this.txt脑卒中部位10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位10.CanGrow = false;
            this.txt脑卒中部位10.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位10.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt脑卒中部位10.Name = "txt脑卒中部位10";
            this.txt脑卒中部位10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位10.StylePriority.UseBorders = false;
            this.txt脑卒中部位10.StylePriority.UseFont = false;
            this.txt脑卒中部位10.StylePriority.UsePadding = false;
            this.txt脑卒中部位10.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "/";
            this.xrTableCell53.Weight = 0.03891535169891755D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位11});
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 0.066156093917114689D;
            // 
            // txt脑卒中部位11
            // 
            this.txt脑卒中部位11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位11.CanGrow = false;
            this.txt脑卒中部位11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位11.Name = "txt脑卒中部位11";
            this.txt脑卒中部位11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位11.StylePriority.UseBorders = false;
            this.txt脑卒中部位11.StylePriority.UseFont = false;
            this.txt脑卒中部位11.StylePriority.UsePadding = false;
            this.txt脑卒中部位11.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "/";
            this.xrTableCell55.Weight = 0.038915349843288954D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脑卒中部位12});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Weight = 0.2373836371615185D;
            // 
            // txt脑卒中部位12
            // 
            this.txt脑卒中部位12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脑卒中部位12.CanGrow = false;
            this.txt脑卒中部位12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt脑卒中部位12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt脑卒中部位12.Name = "txt脑卒中部位12";
            this.txt脑卒中部位12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脑卒中部位12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt脑卒中部位12.StylePriority.UseBorders = false;
            this.txt脑卒中部位12.StylePriority.UseFont = false;
            this.txt脑卒中部位12.StylePriority.UsePadding = false;
            this.txt脑卒中部位12.StylePriority.UseTextAlignment = false;
            this.txt脑卒中部位12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell57,
            this.xrTableCell84,
            this.xrTableCell82,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell85,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell83,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell62,
            this.xrTableCell92,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell93,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell90,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell91,
            this.xrTableCell67,
            this.xrTableCell98,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell100,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell99,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell101,
            this.xrTableCell72,
            this.xrTableCell105,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell107,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell108,
            this.xrTableCell102,
            this.xrTableCell106,
            this.xrTableCell109,
            this.xrTableCell77});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.Weight = 0.8D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Weight = 0.058373028476190462D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "1 无症状";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell57.Weight = 0.4319604160680201D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状11});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Weight = 0.066156098058877566D;
            // 
            // txt无症状11
            // 
            this.txt无症状11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状11.CanGrow = false;
            this.txt无症状11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状11.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2.8F);
            this.txt无症状11.Name = "txt无症状11";
            this.txt无症状11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状11.StylePriority.UseBorders = false;
            this.txt无症状11.StylePriority.UseFont = false;
            this.txt无症状11.StylePriority.UsePadding = false;
            this.txt无症状11.StylePriority.UseTextAlignment = false;
            this.txt无症状11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "/";
            this.xrTableCell82.Weight = 0.034245510712339729D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状12});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.058373028594950735D;
            // 
            // txt无症状12
            // 
            this.txt无症状12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状12.CanGrow = false;
            this.txt无症状12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状12.Name = "txt无症状12";
            this.txt无症状12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状12.StylePriority.UseBorders = false;
            this.txt无症状12.StylePriority.UseFont = false;
            this.txt无症状12.StylePriority.UsePadding = false;
            this.txt无症状12.StylePriority.UseTextAlignment = false;
            this.txt无症状12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "/";
            this.xrTableCell79.Weight = 0.034245511513971254D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状13});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.058373027793319217D;
            // 
            // txt无症状13
            // 
            this.txt无症状13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状13.CanGrow = false;
            this.txt无症状13.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799956F);
            this.txt无症状13.Name = "txt无症状13";
            this.txt无症状13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状13.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状13.StylePriority.UseBorders = false;
            this.txt无症状13.StylePriority.UseFont = false;
            this.txt无症状13.StylePriority.UsePadding = false;
            this.txt无症状13.StylePriority.UseTextAlignment = false;
            this.txt无症状13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Text = "/";
            this.xrTableCell58.Weight = 0.034245510712339715D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状14});
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.058373026991687671D;
            // 
            // txt无症状14
            // 
            this.txt无症状14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状14.CanGrow = false;
            this.txt无症状14.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状14.Name = "txt无症状14";
            this.txt无症状14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状14.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状14.StylePriority.UseBorders = false;
            this.txt无症状14.StylePriority.UseFont = false;
            this.txt无症状14.StylePriority.UsePadding = false;
            this.txt无症状14.StylePriority.UseTextAlignment = false;
            this.txt无症状14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "/";
            this.xrTableCell60.Weight = 0.034245509405977211D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状15});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.058373026486956692D;
            // 
            // txt无症状15
            // 
            this.txt无症状15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状15.CanGrow = false;
            this.txt无症状15.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状15.Name = "txt无症状15";
            this.txt无症状15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状15.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状15.StylePriority.UseBorders = false;
            this.txt无症状15.StylePriority.UseFont = false;
            this.txt无症状15.StylePriority.UsePadding = false;
            this.txt无症状15.StylePriority.UseTextAlignment = false;
            this.txt无症状15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Text = "/";
            this.xrTableCell83.Weight = 0.034245508693415895D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状16});
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.058373026991687657D;
            // 
            // txt无症状16
            // 
            this.txt无症状16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状16.CanGrow = false;
            this.txt无症状16.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799924F);
            this.txt无症状16.Name = "txt无症状16";
            this.txt无症状16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状16.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状16.StylePriority.UseBorders = false;
            this.txt无症状16.StylePriority.UseFont = false;
            this.txt无症状16.StylePriority.UsePadding = false;
            this.txt无症状16.StylePriority.UseTextAlignment = false;
            this.txt无症状16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Text = "/";
            this.xrTableCell81.Weight = 0.0342455086934159D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell62.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状17});
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Weight = 0.066934377562750683D;
            // 
            // txt无症状17
            // 
            this.txt无症状17.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状17.CanGrow = false;
            this.txt无症状17.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799892F);
            this.txt无症状17.Name = "txt无症状17";
            this.txt无症状17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状17.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状17.StylePriority.UseBorders = false;
            this.txt无症状17.StylePriority.UseFont = false;
            this.txt无症状17.StylePriority.UsePadding = false;
            this.txt无症状17.StylePriority.UseTextAlignment = false;
            this.txt无症状17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状21});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.066156098045888165D;
            // 
            // txt无症状21
            // 
            this.txt无症状21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状21.CanGrow = false;
            this.txt无症状21.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状21.LocationFloat = new DevExpress.Utils.PointFloat(1.999982F, 2.799861F);
            this.txt无症状21.Name = "txt无症状21";
            this.txt无症状21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状21.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状21.StylePriority.UseBorders = false;
            this.txt无症状21.StylePriority.UseFont = false;
            this.txt无症状21.StylePriority.UsePadding = false;
            this.txt无症状21.StylePriority.UseTextAlignment = false;
            this.txt无症状21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Text = "/";
            this.xrTableCell86.Weight = 0.034245510699350362D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状22});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.058373024069072708D;
            // 
            // txt无症状22
            // 
            this.txt无症状22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状22.CanGrow = false;
            this.txt无症状22.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状22.Name = "txt无症状22";
            this.txt无症状22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状22.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状22.StylePriority.UseBorders = false;
            this.txt无症状22.StylePriority.UseFont = false;
            this.txt无症状22.StylePriority.UsePadding = false;
            this.txt无症状22.StylePriority.UseTextAlignment = false;
            this.txt无症状22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Text = "/";
            this.xrTableCell63.Weight = 0.034245510699350348D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状23});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 0.058373030146256187D;
            // 
            // txt无症状23
            // 
            this.txt无症状23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状23.CanGrow = false;
            this.txt无症状23.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状23.Name = "txt无症状23";
            this.txt无症状23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状23.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状23.StylePriority.UseBorders = false;
            this.txt无症状23.StylePriority.UseFont = false;
            this.txt无症状23.StylePriority.UsePadding = false;
            this.txt无症状23.StylePriority.UseTextAlignment = false;
            this.txt无症状23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Text = "/";
            this.xrTableCell93.Weight = 0.034245509632363891D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状24});
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.058373030424600458D;
            // 
            // txt无症状24
            // 
            this.txt无症状24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状24.CanGrow = false;
            this.txt无症状24.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状24.Name = "txt无症状24";
            this.txt无症状24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状24.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状24.StylePriority.UseBorders = false;
            this.txt无症状24.StylePriority.UseFont = false;
            this.txt无症状24.StylePriority.UsePadding = false;
            this.txt无症状24.StylePriority.UseTextAlignment = false;
            this.txt无症状24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "/";
            this.xrTableCell66.Weight = 0.034245509354019613D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状25});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.058373026434999128D;
            // 
            // txt无症状25
            // 
            this.txt无症状25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状25.CanGrow = false;
            this.txt无症状25.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状25.Name = "txt无症状25";
            this.txt无症状25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状25.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状25.StylePriority.UseBorders = false;
            this.txt无症状25.StylePriority.UseFont = false;
            this.txt无症状25.StylePriority.UsePadding = false;
            this.txt无症状25.StylePriority.UseTextAlignment = false;
            this.txt无症状25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "/";
            this.xrTableCell88.Weight = 0.034245509354019661D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状26});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.058373030146256229D;
            // 
            // txt无症状26
            // 
            this.txt无症状26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状26.CanGrow = false;
            this.txt无症状26.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状26.Name = "txt无症状26";
            this.txt无症状26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状26.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状26.StylePriority.UseBorders = false;
            this.txt无症状26.StylePriority.UseFont = false;
            this.txt无症状26.StylePriority.UsePadding = false;
            this.txt无症状26.StylePriority.UseTextAlignment = false;
            this.txt无症状26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "/";
            this.xrTableCell91.Weight = 0.034245513343621026D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状27});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.066934414396977449D;
            // 
            // txt无症状27
            // 
            this.txt无症状27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状27.CanGrow = false;
            this.txt无症状27.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状27.Name = "txt无症状27";
            this.txt无症状27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状27.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状27.StylePriority.UseBorders = false;
            this.txt无症状27.StylePriority.UseFont = false;
            this.txt无症状27.StylePriority.UsePadding = false;
            this.txt无症状27.StylePriority.UseTextAlignment = false;
            this.txt无症状27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状31});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 0.066156096886120327D;
            // 
            // txt无症状31
            // 
            this.txt无症状31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状31.CanGrow = false;
            this.txt无症状31.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状31.LocationFloat = new DevExpress.Utils.PointFloat(2.000046F, 2.799861F);
            this.txt无症状31.Name = "txt无症状31";
            this.txt无症状31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状31.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状31.StylePriority.UseBorders = false;
            this.txt无症状31.StylePriority.UseFont = false;
            this.txt无症状31.StylePriority.UsePadding = false;
            this.txt无症状31.StylePriority.UseTextAlignment = false;
            this.txt无症状31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "/";
            this.xrTableCell94.Weight = 0.034245509539582511D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状32});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.058373030331819079D;
            // 
            // txt无症状32
            // 
            this.txt无症状32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状32.CanGrow = false;
            this.txt无症状32.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状32.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状32.Name = "txt无症状32";
            this.txt无症状32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状32.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状32.StylePriority.UseBorders = false;
            this.txt无症状32.StylePriority.UseFont = false;
            this.txt无症状32.StylePriority.UsePadding = false;
            this.txt无症状32.StylePriority.UseTextAlignment = false;
            this.txt无症状32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "/";
            this.xrTableCell68.Weight = 0.034245509539582511D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状33});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.058373023651556229D;
            // 
            // txt无症状33
            // 
            this.txt无症状33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状33.CanGrow = false;
            this.txt无症状33.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状33.Name = "txt无症状33";
            this.txt无症状33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状33.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状33.StylePriority.UseBorders = false;
            this.txt无症状33.StylePriority.UseFont = false;
            this.txt无症状33.StylePriority.UsePadding = false;
            this.txt无症状33.StylePriority.UseTextAlignment = false;
            this.txt无症状33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "/";
            this.xrTableCell100.Weight = 0.0342455082406425D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状34});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.058373029032879067D;
            // 
            // txt无症状34
            // 
            this.txt无症状34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状34.CanGrow = false;
            this.txt无症状34.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状34.Name = "txt无症状34";
            this.txt无症状34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状34.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状34.StylePriority.UseBorders = false;
            this.txt无症状34.StylePriority.UseFont = false;
            this.txt无症状34.StylePriority.UsePadding = false;
            this.txt无症状34.StylePriority.UseTextAlignment = false;
            this.txt无症状34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Text = "/";
            this.xrTableCell71.Weight = 0.034245508426205329D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状35});
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.058373029218441938D;
            // 
            // txt无症状35
            // 
            this.txt无症状35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状35.CanGrow = false;
            this.txt无症状35.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状35.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状35.Name = "txt无症状35";
            this.txt无症状35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状35.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状35.StylePriority.UseBorders = false;
            this.txt无症状35.StylePriority.UseFont = false;
            this.txt无症状35.StylePriority.UsePadding = false;
            this.txt无症状35.StylePriority.UseTextAlignment = false;
            this.txt无症状35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Text = "/";
            this.xrTableCell96.Weight = 0.034245508426205364D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状36});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.058373029218441938D;
            // 
            // txt无症状36
            // 
            this.txt无症状36.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状36.CanGrow = false;
            this.txt无症状36.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状36.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状36.Name = "txt无症状36";
            this.txt无症状36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状36.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状36.StylePriority.UseBorders = false;
            this.txt无症状36.StylePriority.UseFont = false;
            this.txt无症状36.StylePriority.UsePadding = false;
            this.txt无症状36.StylePriority.UseTextAlignment = false;
            this.txt无症状36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Text = "/";
            this.xrTableCell101.Weight = 0.034245509168456763D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状37});
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.066934387954270577D;
            // 
            // txt无症状37
            // 
            this.txt无症状37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状37.CanGrow = false;
            this.txt无症状37.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状37.Name = "txt无症状37";
            this.txt无症状37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状37.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状37.StylePriority.UseBorders = false;
            this.txt无症状37.StylePriority.UseFont = false;
            this.txt无症状37.StylePriority.UsePadding = false;
            this.txt无症状37.StylePriority.UseTextAlignment = false;
            this.txt无症状37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状41});
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.066156096886120327D;
            // 
            // txt无症状41
            // 
            this.txt无症状41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状41.CanGrow = false;
            this.txt无症状41.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状41.LocationFloat = new DevExpress.Utils.PointFloat(1.999982F, 2.799988F);
            this.txt无症状41.Name = "txt无症状41";
            this.txt无症状41.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状41.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状41.StylePriority.UseBorders = false;
            this.txt无症状41.StylePriority.UseFont = false;
            this.txt无症状41.StylePriority.UsePadding = false;
            this.txt无症状41.StylePriority.UseTextAlignment = false;
            this.txt无症状41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "/";
            this.xrTableCell103.Weight = 0.0342455095395825D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状42});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 0.058373030331819079D;
            // 
            // txt无症状42
            // 
            this.txt无症状42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状42.CanGrow = false;
            this.txt无症状42.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状42.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状42.Name = "txt无症状42";
            this.txt无症状42.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状42.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状42.StylePriority.UseBorders = false;
            this.txt无症状42.StylePriority.UseFont = false;
            this.txt无症状42.StylePriority.UsePadding = false;
            this.txt无症状42.StylePriority.UseTextAlignment = false;
            this.txt无症状42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "/";
            this.xrTableCell73.Weight = 0.03424550953958249D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状43});
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.058373023651556319D;
            // 
            // txt无症状43
            // 
            this.txt无症状43.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状43.CanGrow = false;
            this.txt无症状43.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状43.Name = "txt无症状43";
            this.txt无症状43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状43.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状43.StylePriority.UseBorders = false;
            this.txt无症状43.StylePriority.UseFont = false;
            this.txt无症状43.StylePriority.UsePadding = false;
            this.txt无症状43.StylePriority.UseTextAlignment = false;
            this.txt无症状43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "/";
            this.xrTableCell107.Weight = 0.034245508240642521D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状44});
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.0583730290328791D;
            // 
            // txt无症状44
            // 
            this.txt无症状44.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状44.CanGrow = false;
            this.txt无症状44.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状44.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状44.Name = "txt无症状44";
            this.txt无症状44.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状44.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状44.StylePriority.UseBorders = false;
            this.txt无症状44.StylePriority.UseFont = false;
            this.txt无症状44.StylePriority.UsePadding = false;
            this.txt无症状44.StylePriority.UseTextAlignment = false;
            this.txt无症状44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "/";
            this.xrTableCell76.Weight = 0.034245508426205357D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状45});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.058373029218441931D;
            // 
            // txt无症状45
            // 
            this.txt无症状45.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状45.CanGrow = false;
            this.txt无症状45.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799861F);
            this.txt无症状45.Name = "txt无症状45";
            this.txt无症状45.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状45.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状45.StylePriority.UseBorders = false;
            this.txt无症状45.StylePriority.UseFont = false;
            this.txt无症状45.StylePriority.UsePadding = false;
            this.txt无症状45.StylePriority.UseTextAlignment = false;
            this.txt无症状45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "/";
            this.xrTableCell102.Weight = 0.034245508426205357D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状46});
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.058373029218441924D;
            // 
            // txt无症状46
            // 
            this.txt无症状46.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状46.CanGrow = false;
            this.txt无症状46.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状46.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状46.Name = "txt无症状46";
            this.txt无症状46.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状46.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状46.StylePriority.UseBorders = false;
            this.txt无症状46.StylePriority.UseFont = false;
            this.txt无症状46.StylePriority.UsePadding = false;
            this.txt无症状46.StylePriority.UseTextAlignment = false;
            this.txt无症状46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "/";
            this.xrTableCell109.Weight = 0.034245509168456763D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt无症状47});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.0669343879542706D;
            // 
            // txt无症状47
            // 
            this.txt无症状47.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt无症状47.CanGrow = false;
            this.txt无症状47.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt无症状47.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt无症状47.Name = "txt无症状47";
            this.txt无症状47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt无症状47.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt无症状47.StylePriority.UseBorders = false;
            this.txt无症状47.StylePriority.UseFont = false;
            this.txt无症状47.StylePriority.UsePadding = false;
            this.txt无症状47.StylePriority.UseTextAlignment = false;
            this.txt无症状47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell128,
            this.xrTableCell114,
            this.xrTableCell129,
            this.xrTableCell140,
            this.xrTableCell115,
            this.xrTableCell141,
            this.xrTableCell153,
            this.xrTableCell116,
            this.xrTableCell162,
            this.xrTableCell113,
            this.xrTableCell118,
            this.xrTableCell164});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.8D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell111.CanGrow = false;
            this.xrTableCell111.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.StylePriority.UseFont = false;
            this.xrTableCell111.Text = "症";
            this.xrTableCell111.Weight = 0.058373028476190483D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UsePadding = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "2 嘴眼歪斜";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell112.Weight = 0.4319604160680201D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.StylePriority.UsePadding = false;
            this.xrTableCell128.StylePriority.UseTextAlignment = false;
            this.xrTableCell128.Text = "5 肌力障碍";
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell128.Weight = 0.311322808534752D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.29575667976368147D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Weight = 0.023348982948684004D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.StylePriority.UseTextAlignment = false;
            this.xrTableCell140.Text = "5 肌力障碍";
            this.xrTableCell140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell140.Weight = 0.3113228171170343D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.29575667176128329D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Weight = 0.023349420297138795D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.StylePriority.UseTextAlignment = false;
            this.xrTableCell153.Text = "5 肌力障碍";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell153.Weight = 0.311322825096237D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.2957566634109548D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.023349204116412323D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.StylePriority.UseTextAlignment = false;
            this.xrTableCell113.Text = "5 肌力障碍";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell113.Weight = 0.3113228024575686D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Weight = 0.29575666805002615D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Weight = 0.023349204673100817D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell119,
            this.xrTableCell120,
            this.txt半身不遂1,
            this.xrTableCell122,
            this.xrTableCell123,
            this.txt半身不遂2,
            this.xrTableCell125,
            this.xrTableCell126,
            this.txt半身不遂3,
            this.xrTableCell130,
            this.xrTableCell131,
            this.txt半身不遂4,
            this.xrTableCell133});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.8D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.058373028476190483D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.StylePriority.UsePadding = false;
            this.xrTableCell119.StylePriority.UseTextAlignment = false;
            this.xrTableCell119.Text = "3 半身不遂";
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell119.Weight = 0.4319604160680201D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.StylePriority.UsePadding = false;
            this.xrTableCell120.StylePriority.UseTextAlignment = false;
            this.xrTableCell120.Text = "6 其他症状";
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell120.Weight = 0.311322808534752D;
            // 
            // txt半身不遂1
            // 
            this.txt半身不遂1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt半身不遂1.Name = "txt半身不遂1";
            this.txt半身不遂1.StylePriority.UseBorders = false;
            this.txt半身不遂1.Weight = 0.29575667976368147D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.023348982948684004D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.StylePriority.UseTextAlignment = false;
            this.xrTableCell123.Text = "6 其他症状";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell123.Weight = 0.3113228171170343D;
            // 
            // txt半身不遂2
            // 
            this.txt半身不遂2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt半身不遂2.Name = "txt半身不遂2";
            this.txt半身不遂2.StylePriority.UseBorders = false;
            this.txt半身不遂2.Weight = 0.29575667176128329D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.023349420297138795D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.StylePriority.UseTextAlignment = false;
            this.xrTableCell126.Text = "6 其他症状";
            this.xrTableCell126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell126.Weight = 0.311322825096237D;
            // 
            // txt半身不遂3
            // 
            this.txt半身不遂3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt半身不遂3.Name = "txt半身不遂3";
            this.txt半身不遂3.StylePriority.UseBorders = false;
            this.txt半身不遂3.Weight = 0.2957566634109548D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.023349204116412323D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.StylePriority.UseTextAlignment = false;
            this.xrTableCell131.Text = "6 其他症状";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell131.Weight = 0.3113228024575686D;
            // 
            // txt半身不遂4
            // 
            this.txt半身不遂4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt半身不遂4.Name = "txt半身不遂4";
            this.txt半身不遂4.StylePriority.UseBorders = false;
            this.txt半身不遂4.Weight = 0.29575666805002615D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.023349204673100817D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell143,
            this.xrTableCell148,
            this.xrTableCell149});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 0.8D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.CanGrow = false;
            this.xrTableCell134.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseFont = false;
            this.xrTableCell134.Text = "状";
            this.xrTableCell134.Weight = 0.058373028476190462D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell135.StylePriority.UsePadding = false;
            this.xrTableCell135.StylePriority.UseTextAlignment = false;
            this.xrTableCell135.Text = "4 舌强言蹇";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell135.Weight = 0.4319604160680201D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.63042833314196245D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.63042904728061155D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.63042868241764694D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 0.63042868538665275D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell147,
            this.xrTableCell138,
            this.xrTableCell146,
            this.xrTableCell150,
            this.xrTableCell139,
            this.xrTableCell151,
            this.xrTableCell155,
            this.xrTableCell152,
            this.xrTableCell154,
            this.xrTableCell142,
            this.xrTableCell156,
            this.xrTableCell159,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell144,
            this.xrTableCell160,
            this.xrTableCell165,
            this.xrTableCell161,
            this.xrTableCell163,
            this.xrTableCell145});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 0.8D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 15, 0, 0, 100F);
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.StylePriority.UsePadding = false;
            this.xrTableCell137.Text = "个人病史";
            this.xrTableCell137.Weight = 0.490333450482222D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell147.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史1});
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.24905824649146907D;
            // 
            // txt个人病史1
            // 
            this.txt个人病史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史1.CanGrow = false;
            this.txt个人病史1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史1.LocationFloat = new DevExpress.Utils.PointFloat(49F, 2.8F);
            this.txt个人病史1.Name = "txt个人病史1";
            this.txt个人病史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史1.StylePriority.UseBorders = false;
            this.txt个人病史1.StylePriority.UseFont = false;
            this.txt个人病史1.StylePriority.UsePadding = false;
            this.txt个人病史1.StylePriority.UseTextAlignment = false;
            this.txt个人病史1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "/";
            this.xrTableCell138.Weight = 0.038915349321393344D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史2});
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 0.066155917110506446D;
            // 
            // txt个人病史2
            // 
            this.txt个人病史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史2.CanGrow = false;
            this.txt个人病史2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt个人病史2.Name = "txt个人病史2";
            this.txt个人病史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史2.StylePriority.UseBorders = false;
            this.txt个人病史2.StylePriority.UseFont = false;
            this.txt个人病史2.StylePriority.UsePadding = false;
            this.txt个人病史2.StylePriority.UseTextAlignment = false;
            this.txt个人病史2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Text = "/";
            this.xrTableCell150.Weight = 0.038915350324592576D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史3});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Weight = 0.23738346395598958D;
            // 
            // txt个人病史3
            // 
            this.txt个人病史3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史3.CanGrow = false;
            this.txt个人病史3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史3.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 2.799988F);
            this.txt个人病史3.Name = "txt个人病史3";
            this.txt个人病史3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史3.StylePriority.UseBorders = false;
            this.txt个人病史3.StylePriority.UseFont = false;
            this.txt个人病史3.StylePriority.UsePadding = false;
            this.txt个人病史3.StylePriority.UseTextAlignment = false;
            this.txt个人病史3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史4});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 0.24905824824271855D;
            // 
            // txt个人病史4
            // 
            this.txt个人病史4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史4.CanGrow = false;
            this.txt个人病史4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史4.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt个人病史4.Name = "txt个人病史4";
            this.txt个人病史4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史4.StylePriority.UseBorders = false;
            this.txt个人病史4.StylePriority.UseFont = false;
            this.txt个人病史4.StylePriority.UsePadding = false;
            this.txt个人病史4.StylePriority.UseTextAlignment = false;
            this.txt个人病史4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "/";
            this.xrTableCell155.Weight = 0.038915351264004555D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史5});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.066156097193458829D;
            // 
            // txt个人病史5
            // 
            this.txt个人病史5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史5.CanGrow = false;
            this.txt个人病史5.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史5.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 2.799988F);
            this.txt个人病史5.Name = "txt个人病史5";
            this.txt个人病史5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史5.StylePriority.UseBorders = false;
            this.txt个人病史5.StylePriority.UseFont = false;
            this.txt个人病史5.StylePriority.UsePadding = false;
            this.txt个人病史5.StylePriority.UseTextAlignment = false;
            this.txt个人病史5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "/";
            this.xrTableCell154.Weight = 0.038915351264004555D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史6});
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.23738399931642507D;
            // 
            // txt个人病史6
            // 
            this.txt个人病史6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史6.CanGrow = false;
            this.txt个人病史6.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt个人病史6.Name = "txt个人病史6";
            this.txt个人病史6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史6.StylePriority.UseBorders = false;
            this.txt个人病史6.StylePriority.UseFont = false;
            this.txt个人病史6.StylePriority.UsePadding = false;
            this.txt个人病史6.StylePriority.UseTextAlignment = false;
            this.txt个人病史6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史7});
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Weight = 0.24905824395157739D;
            // 
            // txt个人病史7
            // 
            this.txt个人病史7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史7.CanGrow = false;
            this.txt个人病史7.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史7.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt个人病史7.Name = "txt个人病史7";
            this.txt个人病史7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史7.StylePriority.UseBorders = false;
            this.txt个人病史7.StylePriority.UseFont = false;
            this.txt个人病史7.StylePriority.UsePadding = false;
            this.txt个人病史7.StylePriority.UseTextAlignment = false;
            this.txt个人病史7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "/";
            this.xrTableCell159.Weight = 0.038915350191219278D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史8});
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.06615609612067351D;
            // 
            // txt个人病史8
            // 
            this.txt个人病史8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史8.CanGrow = false;
            this.txt个人病史8.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt个人病史8.Name = "txt个人病史8";
            this.txt个人病史8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史8.StylePriority.UseBorders = false;
            this.txt个人病史8.StylePriority.UseFont = false;
            this.txt个人病史8.StylePriority.UsePadding = false;
            this.txt个人病史8.StylePriority.UseTextAlignment = false;
            this.txt个人病史8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "/";
            this.xrTableCell158.Weight = 0.038915350191219306D;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史9});
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.23738364196295742D;
            // 
            // txt个人病史9
            // 
            this.txt个人病史9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史9.CanGrow = false;
            this.txt个人病史9.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt个人病史9.Name = "txt个人病史9";
            this.txt个人病史9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史9.StylePriority.UseBorders = false;
            this.txt个人病史9.StylePriority.UseFont = false;
            this.txt个人病史9.StylePriority.UsePadding = false;
            this.txt个人病史9.StylePriority.UseTextAlignment = false;
            this.txt个人病史9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell160.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史10});
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 0.24905824543608032D;
            // 
            // txt个人病史10
            // 
            this.txt个人病史10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史10.CanGrow = false;
            this.txt个人病史10.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史10.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.799988F);
            this.txt个人病史10.Name = "txt个人病史10";
            this.txt个人病史10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史10.StylePriority.UseBorders = false;
            this.txt个人病史10.StylePriority.UseFont = false;
            this.txt个人病史10.StylePriority.UsePadding = false;
            this.txt个人病史10.StylePriority.UseTextAlignment = false;
            this.txt个人病史10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "/";
            this.xrTableCell165.Weight = 0.038915350562345019D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史11});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.066156096491799252D;
            // 
            // txt个人病史11
            // 
            this.txt个人病史11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史11.CanGrow = false;
            this.txt个人病史11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt个人病史11.Name = "txt个人病史11";
            this.txt个人病史11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史11.StylePriority.UseBorders = false;
            this.txt个人病史11.StylePriority.UseFont = false;
            this.txt个人病史11.StylePriority.UsePadding = false;
            this.txt个人病史11.StylePriority.UseTextAlignment = false;
            this.txt个人病史11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "/";
            this.xrTableCell163.Weight = 0.038915350562344991D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人病史12});
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.Weight = 0.23738364233408316D;
            // 
            // txt个人病史12
            // 
            this.txt个人病史12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人病史12.CanGrow = false;
            this.txt个人病史12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt个人病史12.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 2.799988F);
            this.txt个人病史12.Name = "txt个人病史12";
            this.txt个人病史12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt个人病史12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人病史12.StylePriority.UseBorders = false;
            this.txt个人病史12.StylePriority.UseFont = false;
            this.txt个人病史12.StylePriority.UsePadding = false;
            this.txt个人病史12.StylePriority.UseTextAlignment = false;
            this.txt个人病史12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 0.8D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 15, 0, 0, 100F);
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.StylePriority.UsePadding = false;
            this.xrTableCell166.Text = "并 发 症";
            this.xrTableCell166.Weight = 0.490333450482222D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症1});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.Weight = 0.24905824649146907D;
            // 
            // txt并发症1
            // 
            this.txt并发症1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症1.CanGrow = false;
            this.txt并发症1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症1.LocationFloat = new DevExpress.Utils.PointFloat(49F, 2.8F);
            this.txt并发症1.Name = "txt并发症1";
            this.txt并发症1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症1.StylePriority.UseBorders = false;
            this.txt并发症1.StylePriority.UseFont = false;
            this.txt并发症1.StylePriority.UsePadding = false;
            this.txt并发症1.StylePriority.UseTextAlignment = false;
            this.txt并发症1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Text = "/";
            this.xrTableCell168.Weight = 0.038915349321393344D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症2});
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Weight = 0.066155917110506446D;
            // 
            // txt并发症2
            // 
            this.txt并发症2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症2.CanGrow = false;
            this.txt并发症2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800051F);
            this.txt并发症2.Name = "txt并发症2";
            this.txt并发症2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症2.StylePriority.UseBorders = false;
            this.txt并发症2.StylePriority.UseFont = false;
            this.txt并发症2.StylePriority.UsePadding = false;
            this.txt并发症2.StylePriority.UseTextAlignment = false;
            this.txt并发症2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Text = "/";
            this.xrTableCell170.Weight = 0.038915350324592576D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症3});
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 0.23738346395598958D;
            // 
            // txt并发症3
            // 
            this.txt并发症3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症3.CanGrow = false;
            this.txt并发症3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症3.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt并发症3.Name = "txt并发症3";
            this.txt并发症3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症3.StylePriority.UseBorders = false;
            this.txt并发症3.StylePriority.UseFont = false;
            this.txt并发症3.StylePriority.UsePadding = false;
            this.txt并发症3.StylePriority.UseTextAlignment = false;
            this.txt并发症3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell172.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症4});
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Weight = 0.24905824824271855D;
            // 
            // txt并发症4
            // 
            this.txt并发症4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症4.CanGrow = false;
            this.txt并发症4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症4.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.80002F);
            this.txt并发症4.Name = "txt并发症4";
            this.txt并发症4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症4.StylePriority.UseBorders = false;
            this.txt并发症4.StylePriority.UseFont = false;
            this.txt并发症4.StylePriority.UsePadding = false;
            this.txt并发症4.StylePriority.UseTextAlignment = false;
            this.txt并发症4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "/";
            this.xrTableCell173.Weight = 0.038915351264004555D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症5});
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.066156097193458829D;
            // 
            // txt并发症5
            // 
            this.txt并发症5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症5.CanGrow = false;
            this.txt并发症5.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症5.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 2.800051F);
            this.txt并发症5.Name = "txt并发症5";
            this.txt并发症5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症5.StylePriority.UseBorders = false;
            this.txt并发症5.StylePriority.UseFont = false;
            this.txt并发症5.StylePriority.UsePadding = false;
            this.txt并发症5.StylePriority.UseTextAlignment = false;
            this.txt并发症5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Text = "/";
            this.xrTableCell175.Weight = 0.038915351264004555D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症6});
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.23738399931642507D;
            // 
            // txt并发症6
            // 
            this.txt并发症6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症6.CanGrow = false;
            this.txt并发症6.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt并发症6.Name = "txt并发症6";
            this.txt并发症6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症6.StylePriority.UseBorders = false;
            this.txt并发症6.StylePriority.UseFont = false;
            this.txt并发症6.StylePriority.UsePadding = false;
            this.txt并发症6.StylePriority.UseTextAlignment = false;
            this.txt并发症6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell177.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症7});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Weight = 0.24905824395157739D;
            // 
            // txt并发症7
            // 
            this.txt并发症7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症7.CanGrow = false;
            this.txt并发症7.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症7.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.80002F);
            this.txt并发症7.Name = "txt并发症7";
            this.txt并发症7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症7.StylePriority.UseBorders = false;
            this.txt并发症7.StylePriority.UseFont = false;
            this.txt并发症7.StylePriority.UsePadding = false;
            this.txt并发症7.StylePriority.UseTextAlignment = false;
            this.txt并发症7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "/";
            this.xrTableCell178.Weight = 0.038915350191219278D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症8});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.06615609612067351D;
            // 
            // txt并发症8
            // 
            this.txt并发症8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症8.CanGrow = false;
            this.txt并发症8.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800083F);
            this.txt并发症8.Name = "txt并发症8";
            this.txt并发症8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症8.StylePriority.UseBorders = false;
            this.txt并发症8.StylePriority.UseFont = false;
            this.txt并发症8.StylePriority.UsePadding = false;
            this.txt并发症8.StylePriority.UseTextAlignment = false;
            this.txt并发症8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "/";
            this.xrTableCell180.Weight = 0.038915350191219306D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症9});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.23738364196295742D;
            // 
            // txt并发症9
            // 
            this.txt并发症9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症9.CanGrow = false;
            this.txt并发症9.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800051F);
            this.txt并发症9.Name = "txt并发症9";
            this.txt并发症9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症9.StylePriority.UseBorders = false;
            this.txt并发症9.StylePriority.UseFont = false;
            this.txt并发症9.StylePriority.UsePadding = false;
            this.txt并发症9.StylePriority.UseTextAlignment = false;
            this.txt并发症9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell182.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症10});
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Weight = 0.24905824543608032D;
            // 
            // txt并发症10
            // 
            this.txt并发症10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症10.CanGrow = false;
            this.txt并发症10.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症10.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.800083F);
            this.txt并发症10.Name = "txt并发症10";
            this.txt并发症10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症10.StylePriority.UseBorders = false;
            this.txt并发症10.StylePriority.UseFont = false;
            this.txt并发症10.StylePriority.UsePadding = false;
            this.txt并发症10.StylePriority.UseTextAlignment = false;
            this.txt并发症10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "/";
            this.xrTableCell183.Weight = 0.038915350562345019D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症11});
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Weight = 0.066156096491799252D;
            // 
            // txt并发症11
            // 
            this.txt并发症11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症11.CanGrow = false;
            this.txt并发症11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt并发症11.Name = "txt并发症11";
            this.txt并发症11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症11.StylePriority.UseBorders = false;
            this.txt并发症11.StylePriority.UseFont = false;
            this.txt并发症11.StylePriority.UsePadding = false;
            this.txt并发症11.StylePriority.UseTextAlignment = false;
            this.txt并发症11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "/";
            this.xrTableCell185.Weight = 0.038915350562344991D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell186.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt并发症12});
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.23738364233408316D;
            // 
            // txt并发症12
            // 
            this.txt并发症12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt并发症12.CanGrow = false;
            this.txt并发症12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt并发症12.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt并发症12.Name = "txt并发症12";
            this.txt并发症12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt并发症12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt并发症12.StylePriority.UseBorders = false;
            this.txt并发症12.StylePriority.UseFont = false;
            this.txt并发症12.StylePriority.UsePadding = false;
            this.txt并发症12.StylePriority.UseTextAlignment = false;
            this.txt并发症12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell207});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 0.8D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 100F);
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.StylePriority.UsePadding = false;
            this.xrTableCell187.StylePriority.UseTextAlignment = false;
            this.xrTableCell187.Text = "新发卒中症状";
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell187.Weight = 0.490333450482222D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell188.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中1});
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Weight = 0.24905824649146907D;
            // 
            // txt新发卒中1
            // 
            this.txt新发卒中1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中1.CanGrow = false;
            this.txt新发卒中1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中1.LocationFloat = new DevExpress.Utils.PointFloat(49F, 2.8F);
            this.txt新发卒中1.Name = "txt新发卒中1";
            this.txt新发卒中1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中1.StylePriority.UseBorders = false;
            this.txt新发卒中1.StylePriority.UseFont = false;
            this.txt新发卒中1.StylePriority.UsePadding = false;
            this.txt新发卒中1.StylePriority.UseTextAlignment = false;
            this.txt新发卒中1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "/";
            this.xrTableCell189.Weight = 0.038915349321393344D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中2});
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Weight = 0.066155917110506446D;
            // 
            // txt新发卒中2
            // 
            this.txt新发卒中2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中2.CanGrow = false;
            this.txt新发卒中2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt新发卒中2.Name = "txt新发卒中2";
            this.txt新发卒中2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中2.StylePriority.UseBorders = false;
            this.txt新发卒中2.StylePriority.UseFont = false;
            this.txt新发卒中2.StylePriority.UsePadding = false;
            this.txt新发卒中2.StylePriority.UseTextAlignment = false;
            this.txt新发卒中2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "/";
            this.xrTableCell191.Weight = 0.038915350324592576D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中3});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.23738346395598958D;
            // 
            // txt新发卒中3
            // 
            this.txt新发卒中3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中3.CanGrow = false;
            this.txt新发卒中3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中3.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt新发卒中3.Name = "txt新发卒中3";
            this.txt新发卒中3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中3.StylePriority.UseBorders = false;
            this.txt新发卒中3.StylePriority.UseFont = false;
            this.txt新发卒中3.StylePriority.UsePadding = false;
            this.txt新发卒中3.StylePriority.UseTextAlignment = false;
            this.txt新发卒中3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell193.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中4});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.Weight = 0.24905824824271855D;
            // 
            // txt新发卒中4
            // 
            this.txt新发卒中4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中4.CanGrow = false;
            this.txt新发卒中4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中4.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.80002F);
            this.txt新发卒中4.Name = "txt新发卒中4";
            this.txt新发卒中4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中4.StylePriority.UseBorders = false;
            this.txt新发卒中4.StylePriority.UseFont = false;
            this.txt新发卒中4.StylePriority.UsePadding = false;
            this.txt新发卒中4.StylePriority.UseTextAlignment = false;
            this.txt新发卒中4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = "/";
            this.xrTableCell194.Weight = 0.038915351264004555D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中5});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.066156097193458829D;
            // 
            // txt新发卒中5
            // 
            this.txt新发卒中5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中5.CanGrow = false;
            this.txt新发卒中5.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt新发卒中5.Name = "txt新发卒中5";
            this.txt新发卒中5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中5.StylePriority.UseBorders = false;
            this.txt新发卒中5.StylePriority.UseFont = false;
            this.txt新发卒中5.StylePriority.UsePadding = false;
            this.txt新发卒中5.StylePriority.UseTextAlignment = false;
            this.txt新发卒中5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "/";
            this.xrTableCell196.Weight = 0.038915351264004555D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中6});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 0.23738399931642507D;
            // 
            // txt新发卒中6
            // 
            this.txt新发卒中6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中6.CanGrow = false;
            this.txt新发卒中6.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中6.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt新发卒中6.Name = "txt新发卒中6";
            this.txt新发卒中6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中6.StylePriority.UseBorders = false;
            this.txt新发卒中6.StylePriority.UseFont = false;
            this.txt新发卒中6.StylePriority.UsePadding = false;
            this.txt新发卒中6.StylePriority.UseTextAlignment = false;
            this.txt新发卒中6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中7});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.24905824395157739D;
            // 
            // txt新发卒中7
            // 
            this.txt新发卒中7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中7.CanGrow = false;
            this.txt新发卒中7.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中7.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.80002F);
            this.txt新发卒中7.Name = "txt新发卒中7";
            this.txt新发卒中7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中7.StylePriority.UseBorders = false;
            this.txt新发卒中7.StylePriority.UseFont = false;
            this.txt新发卒中7.StylePriority.UsePadding = false;
            this.txt新发卒中7.StylePriority.UseTextAlignment = false;
            this.txt新发卒中7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Text = "/";
            this.xrTableCell199.Weight = 0.038915350191219278D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中8});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.06615609612067351D;
            // 
            // txt新发卒中8
            // 
            this.txt新发卒中8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中8.CanGrow = false;
            this.txt新发卒中8.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt新发卒中8.Name = "txt新发卒中8";
            this.txt新发卒中8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中8.StylePriority.UseBorders = false;
            this.txt新发卒中8.StylePriority.UseFont = false;
            this.txt新发卒中8.StylePriority.UsePadding = false;
            this.txt新发卒中8.StylePriority.UseTextAlignment = false;
            this.txt新发卒中8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Text = "/";
            this.xrTableCell201.Weight = 0.038915350191219306D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中9});
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.23738364196295742D;
            // 
            // txt新发卒中9
            // 
            this.txt新发卒中9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中9.CanGrow = false;
            this.txt新发卒中9.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt新发卒中9.Name = "txt新发卒中9";
            this.txt新发卒中9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中9.StylePriority.UseBorders = false;
            this.txt新发卒中9.StylePriority.UseFont = false;
            this.txt新发卒中9.StylePriority.UsePadding = false;
            this.txt新发卒中9.StylePriority.UseTextAlignment = false;
            this.txt新发卒中9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中10});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.Weight = 0.24905824543608032D;
            // 
            // txt新发卒中10
            // 
            this.txt新发卒中10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中10.CanGrow = false;
            this.txt新发卒中10.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中10.LocationFloat = new DevExpress.Utils.PointFloat(48.99998F, 2.80002F);
            this.txt新发卒中10.Name = "txt新发卒中10";
            this.txt新发卒中10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中10.StylePriority.UseBorders = false;
            this.txt新发卒中10.StylePriority.UseFont = false;
            this.txt新发卒中10.StylePriority.UsePadding = false;
            this.txt新发卒中10.StylePriority.UseTextAlignment = false;
            this.txt新发卒中10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Text = "/";
            this.xrTableCell204.Weight = 0.038915350562345019D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中11});
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.066156096491799252D;
            // 
            // txt新发卒中11
            // 
            this.txt新发卒中11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中11.CanGrow = false;
            this.txt新发卒中11.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt新发卒中11.Name = "txt新发卒中11";
            this.txt新发卒中11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中11.StylePriority.UseBorders = false;
            this.txt新发卒中11.StylePriority.UseFont = false;
            this.txt新发卒中11.StylePriority.UsePadding = false;
            this.txt新发卒中11.StylePriority.UseTextAlignment = false;
            this.txt新发卒中11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "/";
            this.xrTableCell206.Weight = 0.038915350562344991D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell207.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新发卒中12});
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseBorders = false;
            this.xrTableCell207.Weight = 0.23738364233408316D;
            // 
            // txt新发卒中12
            // 
            this.txt新发卒中12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新发卒中12.CanGrow = false;
            this.txt新发卒中12.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新发卒中12.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 2.80002F);
            this.txt新发卒中12.Name = "txt新发卒中12";
            this.txt新发卒中12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新发卒中12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt新发卒中12.StylePriority.UseBorders = false;
            this.txt新发卒中12.StylePriority.UseFont = false;
            this.txt新发卒中12.StylePriority.UsePadding = false;
            this.txt新发卒中12.StylePriority.UseTextAlignment = false;
            this.txt新发卒中12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229,
            this.xrTableCell208,
            this.txt第1次血压1,
            this.xrTableCell2,
            this.txt第1次血压2,
            this.txt第2次血压1,
            this.xrTableCell5,
            this.txt第2次血压2,
            this.txt第3次血压1,
            this.xrTableCell124,
            this.txt第3次血压2,
            this.txt第4次血压1,
            this.xrTableCell132,
            this.txt第4次血压2});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 0.8D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.058373024022681956D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell208.StylePriority.UseBorders = false;
            this.xrTableCell208.StylePriority.UsePadding = false;
            this.xrTableCell208.StylePriority.UseTextAlignment = false;
            this.xrTableCell208.Text = "血压（mmHg）";
            this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell208.Weight = 0.43196042645954D;
            // 
            // txt第1次血压1
            // 
            this.txt第1次血压1.Name = "txt第1次血压1";
            this.txt第1次血压1.StylePriority.UseTextAlignment = false;
            this.txt第1次血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第1次血压1.Weight = 0.29575666908801845D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "/";
            this.xrTableCell2.Weight = 0.038915351327791795D;
            // 
            // txt第1次血压2
            // 
            this.txt第1次血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次血压2.Name = "txt第1次血压2";
            this.txt第1次血压2.StylePriority.UseBorders = false;
            this.txt第1次血压2.StylePriority.UseTextAlignment = false;
            this.txt第1次血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第1次血压2.Weight = 0.29575631280733616D;
            // 
            // txt第2次血压1
            // 
            this.txt第2次血压1.Name = "txt第2次血压1";
            this.txt第2次血压1.StylePriority.UseTextAlignment = false;
            this.txt第2次血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第2次血压1.Weight = 0.29575666708162D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Text = "/";
            this.xrTableCell5.Weight = 0.038915349822992967D;
            // 
            // txt第2次血压2
            // 
            this.txt第2次血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次血压2.Name = "txt第2次血压2";
            this.txt第2次血压2.StylePriority.UseBorders = false;
            this.txt第2次血压2.StylePriority.UseTextAlignment = false;
            this.txt第2次血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第2次血压2.Weight = 0.29575654982619137D;
            // 
            // txt第3次血压1
            // 
            this.txt第3次血压1.Name = "txt第3次血压1";
            this.txt第3次血压1.StylePriority.UseTextAlignment = false;
            this.txt第3次血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第3次血压1.Weight = 0.29575666699463732D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "/";
            this.xrTableCell124.Weight = 0.038915349779501673D;
            // 
            // txt第3次血压2
            // 
            this.txt第3次血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次血压2.Name = "txt第3次血压2";
            this.txt第3次血压2.StylePriority.UseBorders = false;
            this.txt第3次血压2.StylePriority.UseTextAlignment = false;
            this.txt第3次血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第3次血压2.Weight = 0.29575619350201771D;
            // 
            // txt第4次血压1
            // 
            this.txt第4次血压1.Name = "txt第4次血压1";
            this.txt第4次血压1.StylePriority.UseTextAlignment = false;
            this.txt第4次血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第4次血压1.Weight = 0.29575666532457173D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Text = "/";
            this.xrTableCell132.Weight = 0.038915348944468853D;
            // 
            // txt第4次血压2
            // 
            this.txt第4次血压2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次血压2.Name = "txt第4次血压2";
            this.txt第4次血压2.StylePriority.UseBorders = false;
            this.txt第4次血压2.StylePriority.UseTextAlignment = false;
            this.txt第4次血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次血压2.Weight = 0.29575761778971421D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.xrTableCell211,
            this.txt身高1,
            this.txt身高2,
            this.txt身高3,
            this.txt身高4});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 0.8D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseBorders = false;
            this.xrTableCell210.Weight = 0.058373024022681956D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell211.StylePriority.UsePadding = false;
            this.xrTableCell211.StylePriority.UseTextAlignment = false;
            this.xrTableCell211.Text = "身高（cm）";
            this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell211.Weight = 0.43196042645954D;
            // 
            // txt身高1
            // 
            this.txt身高1.Name = "txt身高1";
            this.txt身高1.Weight = 0.6304283332231464D;
            // 
            // txt身高2
            // 
            this.txt身高2.Name = "txt身高2";
            this.txt身高2.Weight = 0.63042856673080438D;
            // 
            // txt身高3
            // 
            this.txt身高3.Name = "txt身高3";
            this.txt身高3.Weight = 0.6304282102761567D;
            // 
            // txt身高4
            // 
            this.txt身高4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt身高4.Name = "txt身高4";
            this.txt身高4.StylePriority.UseBorders = false;
            this.txt身高4.Weight = 0.6304296320587548D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.txt第1次体重1,
            this.xrTableCell214,
            this.txt第1次体重2,
            this.txt第2次体重1,
            this.xrTableCell216,
            this.txt第2次体重2,
            this.txt第3次体重1,
            this.xrTableCell235,
            this.txt第3次体重2,
            this.txt第4次体重1,
            this.xrTableCell237,
            this.txt第4次体重2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.Weight = 0.8D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell217.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseBorders = false;
            this.xrTableCell217.StylePriority.UseFont = false;
            this.xrTableCell217.Text = "体";
            this.xrTableCell217.Weight = 0.058373024022681956D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell218.StylePriority.UsePadding = false;
            this.xrTableCell218.StylePriority.UseTextAlignment = false;
            this.xrTableCell218.Text = "体重（kg）";
            this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell218.Weight = 0.43196042645954D;
            // 
            // txt第1次体重1
            // 
            this.txt第1次体重1.Name = "txt第1次体重1";
            this.txt第1次体重1.StylePriority.UseTextAlignment = false;
            this.txt第1次体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第1次体重1.Weight = 0.29575666908801845D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseBorders = false;
            this.xrTableCell214.Text = "/";
            this.xrTableCell214.Weight = 0.038915350826192213D;
            // 
            // txt第1次体重2
            // 
            this.txt第1次体重2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次体重2.Name = "txt第1次体重2";
            this.txt第1次体重2.StylePriority.UseBorders = false;
            this.txt第1次体重2.StylePriority.UseTextAlignment = false;
            this.txt第1次体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第1次体重2.Weight = 0.2957563133089357D;
            // 
            // txt第2次体重1
            // 
            this.txt第2次体重1.Name = "txt第2次体重1";
            this.txt第2次体重1.StylePriority.UseTextAlignment = false;
            this.txt第2次体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第2次体重1.Weight = 0.29575666708162D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseBorders = false;
            this.xrTableCell216.Text = "/";
            this.xrTableCell216.Weight = 0.038915349822992967D;
            // 
            // txt第2次体重2
            // 
            this.txt第2次体重2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次体重2.Name = "txt第2次体重2";
            this.txt第2次体重2.StylePriority.UseBorders = false;
            this.txt第2次体重2.StylePriority.UseTextAlignment = false;
            this.txt第2次体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第2次体重2.Weight = 0.29575654982619137D;
            // 
            // txt第3次体重1
            // 
            this.txt第3次体重1.Name = "txt第3次体重1";
            this.txt第3次体重1.StylePriority.UseTextAlignment = false;
            this.txt第3次体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第3次体重1.Weight = 0.29575666699463732D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.Text = "/";
            this.xrTableCell235.Weight = 0.038915349779501673D;
            // 
            // txt第3次体重2
            // 
            this.txt第3次体重2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次体重2.Name = "txt第3次体重2";
            this.txt第3次体重2.StylePriority.UseBorders = false;
            this.txt第3次体重2.StylePriority.UseTextAlignment = false;
            this.txt第3次体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第3次体重2.Weight = 0.29575619350201771D;
            // 
            // txt第4次体重1
            // 
            this.txt第4次体重1.Name = "txt第4次体重1";
            this.txt第4次体重1.StylePriority.UseTextAlignment = false;
            this.txt第4次体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第4次体重1.Weight = 0.29575666532457173D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Text = "/";
            this.xrTableCell237.Weight = 0.038915348944468853D;
            // 
            // txt第4次体重2
            // 
            this.txt第4次体重2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次体重2.Name = "txt第4次体重2";
            this.txt第4次体重2.StylePriority.UseBorders = false;
            this.txt第4次体重2.StylePriority.UseTextAlignment = false;
            this.txt第4次体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次体重2.Weight = 0.29575761778971421D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell226,
            this.txt第1次体质指数1,
            this.xrTableCell251,
            this.txt第1次体质指数2,
            this.txt第2次体质指数1,
            this.xrTableCell253,
            this.txt第2次体质指数2,
            this.txt第3次体质指数1,
            this.xrTableCell265,
            this.txt第3次体质指数2,
            this.txt第4次体质指数1,
            this.xrTableCell286,
            this.txt第4次体质指数2});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.8D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Weight = 0.058373024022681956D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell226.StylePriority.UsePadding = false;
            this.xrTableCell226.StylePriority.UseTextAlignment = false;
            this.xrTableCell226.Text = "体质指数";
            this.xrTableCell226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell226.Weight = 0.43196042645954D;
            // 
            // txt第1次体质指数1
            // 
            this.txt第1次体质指数1.Name = "txt第1次体质指数1";
            this.txt第1次体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第1次体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第1次体质指数1.Weight = 0.29575666908801845D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Text = "/";
            this.xrTableCell251.Weight = 0.038915350826192213D;
            // 
            // txt第1次体质指数2
            // 
            this.txt第1次体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次体质指数2.Name = "txt第1次体质指数2";
            this.txt第1次体质指数2.StylePriority.UseBorders = false;
            this.txt第1次体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第1次体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第1次体质指数2.Weight = 0.2957563133089357D;
            // 
            // txt第2次体质指数1
            // 
            this.txt第2次体质指数1.Name = "txt第2次体质指数1";
            this.txt第2次体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第2次体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第2次体质指数1.Weight = 0.29575666708162D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Text = "/";
            this.xrTableCell253.Weight = 0.038915349822992967D;
            // 
            // txt第2次体质指数2
            // 
            this.txt第2次体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次体质指数2.Name = "txt第2次体质指数2";
            this.txt第2次体质指数2.StylePriority.UseBorders = false;
            this.txt第2次体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第2次体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第2次体质指数2.Weight = 0.29575654982619137D;
            // 
            // txt第3次体质指数1
            // 
            this.txt第3次体质指数1.Name = "txt第3次体质指数1";
            this.txt第3次体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第3次体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第3次体质指数1.Weight = 0.29575666699463732D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Text = "/";
            this.xrTableCell265.Weight = 0.038915349779501673D;
            // 
            // txt第3次体质指数2
            // 
            this.txt第3次体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次体质指数2.Name = "txt第3次体质指数2";
            this.txt第3次体质指数2.StylePriority.UseBorders = false;
            this.txt第3次体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第3次体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第3次体质指数2.Weight = 0.29575619350201771D;
            // 
            // txt第4次体质指数1
            // 
            this.txt第4次体质指数1.Name = "txt第4次体质指数1";
            this.txt第4次体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第4次体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第4次体质指数1.Weight = 0.29575666532457173D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Text = "/";
            this.xrTableCell286.Weight = 0.038915348944468853D;
            // 
            // txt第4次体质指数2
            // 
            this.txt第4次体质指数2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次体质指数2.Name = "txt第4次体质指数2";
            this.txt第4次体质指数2.StylePriority.UseBorders = false;
            this.txt第4次体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第4次体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次体质指数2.Weight = 0.29575761778971421D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell232,
            this.xrTableCell233,
            this.txt腰围1,
            this.txt腰围2,
            this.txt腰围3,
            this.txt腰围4});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 0.8D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell232.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.StylePriority.UseFont = false;
            this.xrTableCell232.Text = "征";
            this.xrTableCell232.Weight = 0.058373024022681956D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell233.StylePriority.UsePadding = false;
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "腰围（cm）";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell233.Weight = 0.43196042645954D;
            // 
            // txt腰围1
            // 
            this.txt腰围1.Name = "txt腰围1";
            this.txt腰围1.Weight = 0.6304283332231464D;
            // 
            // txt腰围2
            // 
            this.txt腰围2.Name = "txt腰围2";
            this.txt腰围2.Weight = 0.63042856673080438D;
            // 
            // txt腰围3
            // 
            this.txt腰围3.Name = "txt腰围3";
            this.txt腰围3.Weight = 0.6304282102761567D;
            // 
            // txt腰围4
            // 
            this.txt腰围4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt腰围4.Name = "txt腰围4";
            this.txt腰围4.StylePriority.UseBorders = false;
            this.txt腰围4.Weight = 0.6304296320587548D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238,
            this.xrTableCell239,
            this.txt空腹血糖1,
            this.xrTableCell240,
            this.txt空腹血糖2,
            this.xrTableCell241,
            this.txt空腹血糖3,
            this.xrTableCell242,
            this.txt空腹血糖4,
            this.xrTableCell243});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 0.8D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Weight = 0.058373024022681956D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell239.StylePriority.UsePadding = false;
            this.xrTableCell239.StylePriority.UseTextAlignment = false;
            this.xrTableCell239.Text = "空腹血糖";
            this.xrTableCell239.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell239.Weight = 0.43196042645954D;
            // 
            // txt空腹血糖1
            // 
            this.txt空腹血糖1.Name = "txt空腹血糖1";
            this.txt空腹血糖1.Weight = 0.38915351227471878D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBorders = false;
            this.xrTableCell240.Text = "mmol/L";
            this.xrTableCell240.Weight = 0.24127482094842762D;
            // 
            // txt空腹血糖2
            // 
            this.txt空腹血糖2.Name = "txt空腹血糖2";
            this.txt空腹血糖2.Weight = 0.38915351026832035D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Text = "mmol/L";
            this.xrTableCell241.Weight = 0.24127505646248407D;
            // 
            // txt空腹血糖3
            // 
            this.txt空腹血糖3.Name = "txt空腹血糖3";
            this.txt空腹血糖3.Weight = 0.38915351018133765D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Text = "mmol/L";
            this.xrTableCell242.Weight = 0.24127470009481905D;
            // 
            // txt空腹血糖4
            // 
            this.txt空腹血糖4.Name = "txt空腹血糖4";
            this.txt空腹血糖4.Weight = 0.38915350851127206D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.Text = "mmol/L";
            this.xrTableCell243.Weight = 0.24127612354748274D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell244,
            this.xrTableCell245,
            this.xrTableCell254,
            this.xrTableCell246,
            this.xrTableCell255,
            this.xrTableCell247,
            this.xrTableCell256,
            this.xrTableCell248,
            this.xrTableCell257,
            this.xrTableCell249});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 0.8D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 0.058373024022681956D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell245.StylePriority.UsePadding = false;
            this.xrTableCell245.StylePriority.UseTextAlignment = false;
            this.xrTableCell245.Text = "肢体功能恢复";
            this.xrTableCell245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell245.Weight = 0.43196042645954D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell254.StylePriority.UsePadding = false;
            this.xrTableCell254.StylePriority.UseTextAlignment = false;
            this.xrTableCell254.Text = "1好 2一般 3差";
            this.xrTableCell254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell254.Weight = 0.56427259324978185D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell246.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肢体功能1});
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 0.066155739973364547D;
            // 
            // txt肢体功能1
            // 
            this.txt肢体功能1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肢体功能1.CanGrow = false;
            this.txt肢体功能1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt肢体功能1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt肢体功能1.Name = "txt肢体功能1";
            this.txt肢体功能1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt肢体功能1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt肢体功能1.StylePriority.UseBorders = false;
            this.txt肢体功能1.StylePriority.UseFont = false;
            this.txt肢体功能1.StylePriority.UsePadding = false;
            this.txt肢体功能1.StylePriority.UseTextAlignment = false;
            this.txt肢体功能1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell255.StylePriority.UsePadding = false;
            this.xrTableCell255.StylePriority.UseTextAlignment = false;
            this.xrTableCell255.Text = "1好 2一般 3差";
            this.xrTableCell255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell255.Weight = 0.56427259124338336D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肢体功能2});
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Weight = 0.066155975487421D;
            // 
            // txt肢体功能2
            // 
            this.txt肢体功能2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肢体功能2.CanGrow = false;
            this.txt肢体功能2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt肢体功能2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt肢体功能2.Name = "txt肢体功能2";
            this.txt肢体功能2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt肢体功能2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt肢体功能2.StylePriority.UseBorders = false;
            this.txt肢体功能2.StylePriority.UseFont = false;
            this.txt肢体功能2.StylePriority.UsePadding = false;
            this.txt肢体功能2.StylePriority.UseTextAlignment = false;
            this.txt肢体功能2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell256.StylePriority.UsePadding = false;
            this.xrTableCell256.StylePriority.UseTextAlignment = false;
            this.xrTableCell256.Text = "1好 2一般 3差";
            this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell256.Weight = 0.56427259115640072D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell248.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肢体功能3});
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Weight = 0.066155619119755982D;
            // 
            // txt肢体功能3
            // 
            this.txt肢体功能3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肢体功能3.CanGrow = false;
            this.txt肢体功能3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt肢体功能3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt肢体功能3.Name = "txt肢体功能3";
            this.txt肢体功能3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt肢体功能3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt肢体功能3.StylePriority.UseBorders = false;
            this.txt肢体功能3.StylePriority.UseFont = false;
            this.txt肢体功能3.StylePriority.UsePadding = false;
            this.txt肢体功能3.StylePriority.UseTextAlignment = false;
            this.txt肢体功能3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell257.StylePriority.UsePadding = false;
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "1好 2一般 3差";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell257.Weight = 0.56427258948633507D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肢体功能4});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Weight = 0.0661570425724197D;
            // 
            // txt肢体功能4
            // 
            this.txt肢体功能4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肢体功能4.CanGrow = false;
            this.txt肢体功能4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt肢体功能4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.799988F);
            this.txt肢体功能4.Name = "txt肢体功能4";
            this.txt肢体功能4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt肢体功能4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt肢体功能4.StylePriority.UseBorders = false;
            this.txt肢体功能4.StylePriority.UseFont = false;
            this.txt肢体功能4.StylePriority.UsePadding = false;
            this.txt肢体功能4.StylePriority.UseTextAlignment = false;
            this.txt肢体功能4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell258,
            this.xrTableCell259,
            this.txt第1次吸烟量1,
            this.xrTableCell295,
            this.txt第1次吸烟量2,
            this.txt第2次吸烟量1,
            this.xrTableCell305,
            this.txt第2次吸烟量2,
            this.txt第3次吸烟量1,
            this.xrTableCell319,
            this.txt第3次吸烟量2,
            this.txt第4次吸烟量1,
            this.xrTableCell321,
            this.txt第4次吸烟量2});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.8D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.058373024022681956D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell259.StylePriority.UsePadding = false;
            this.xrTableCell259.StylePriority.UseTextAlignment = false;
            this.xrTableCell259.Text = "日吸烟量（支）";
            this.xrTableCell259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell259.Weight = 0.43196042645954D;
            // 
            // txt第1次吸烟量1
            // 
            this.txt第1次吸烟量1.Name = "txt第1次吸烟量1";
            this.txt第1次吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第1次吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第1次吸烟量1.Weight = 0.2957566695896181D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.Text = "/";
            this.xrTableCell295.Weight = 0.038915351076992094D;
            // 
            // txt第1次吸烟量2
            // 
            this.txt第1次吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次吸烟量2.Name = "txt第1次吸烟量2";
            this.txt第1次吸烟量2.StylePriority.UseBorders = false;
            this.txt第1次吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第1次吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第1次吸烟量2.Weight = 0.29575655108019039D;
            // 
            // txt第2次吸烟量1
            // 
            this.txt第2次吸烟量1.Name = "txt第2次吸烟量1";
            this.txt第2次吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第2次吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第2次吸烟量1.Weight = 0.29575666808481921D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.Text = "/";
            this.xrTableCell305.Weight = 0.038915350324592618D;
            // 
            // txt第2次吸烟量2
            // 
            this.txt第2次吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次吸烟量2.Name = "txt第2次吸烟量2";
            this.txt第2次吸烟量2.StylePriority.UseBorders = false;
            this.txt第2次吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第2次吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第2次吸烟量2.Weight = 0.29575678784824577D;
            // 
            // txt第3次吸烟量1
            // 
            this.txt第3次吸烟量1.Name = "txt第3次吸烟量1";
            this.txt第3次吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第3次吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第3次吸烟量1.Weight = 0.2957566670381287D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.Text = "/";
            this.xrTableCell319.Weight = 0.038915349801247334D;
            // 
            // txt第3次吸烟量2
            // 
            this.txt第3次吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次吸烟量2.Name = "txt第3次吸烟量2";
            this.txt第3次吸烟量2.StylePriority.UseBorders = false;
            this.txt第3次吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第3次吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第3次吸烟量2.Weight = 0.29575666856467314D;
            // 
            // txt第4次吸烟量1
            // 
            this.txt第4次吸烟量1.Name = "txt第4次吸烟量1";
            this.txt第4次吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第4次吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第4次吸烟量1.Weight = 0.29575666377628157D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseBorders = false;
            this.xrTableCell321.Text = "/";
            this.xrTableCell321.Weight = 0.038915348170323744D;
            // 
            // txt第4次吸烟量2
            // 
            this.txt第4次吸烟量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次吸烟量2.Name = "txt第4次吸烟量2";
            this.txt第4次吸烟量2.StylePriority.UseBorders = false;
            this.txt第4次吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第4次吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次吸烟量2.Weight = 0.29575666693374958D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell268,
            this.xrTableCell269,
            this.txt第1次饮酒量1,
            this.xrTableCell297,
            this.txt第1次饮酒量2,
            this.txt第2次饮酒量1,
            this.xrTableCell307,
            this.txt第2次饮酒量2,
            this.txt第3次饮酒量1,
            this.xrTableCell345,
            this.txt第3次饮酒量2,
            this.txt第4次饮酒量1,
            this.xrTableCell347,
            this.txt第4次饮酒量2});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 0.8D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell268.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseFont = false;
            this.xrTableCell268.StylePriority.UseTextAlignment = false;
            this.xrTableCell268.Text = "生";
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell268.Weight = 0.058373024022681956D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "日饮酒量（两）";
            this.xrTableCell269.Weight = 0.43196042645954D;
            // 
            // txt第1次饮酒量1
            // 
            this.txt第1次饮酒量1.Name = "txt第1次饮酒量1";
            this.txt第1次饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第1次饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第1次饮酒量1.Weight = 0.2957566695896181D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBorders = false;
            this.xrTableCell297.Text = "/";
            this.xrTableCell297.Weight = 0.038915351076992094D;
            // 
            // txt第1次饮酒量2
            // 
            this.txt第1次饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次饮酒量2.Name = "txt第1次饮酒量2";
            this.txt第1次饮酒量2.StylePriority.UseBorders = false;
            this.txt第1次饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第1次饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第1次饮酒量2.Weight = 0.29575666984041782D;
            // 
            // txt第2次饮酒量1
            // 
            this.txt第2次饮酒量1.Name = "txt第2次饮酒量1";
            this.txt第2次饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第2次饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第2次饮酒量1.Weight = 0.29575666808481921D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Text = "/";
            this.xrTableCell307.Weight = 0.038915350324592646D;
            // 
            // txt第2次饮酒量2
            // 
            this.txt第2次饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次饮酒量2.Name = "txt第2次饮酒量2";
            this.txt第2次饮酒量2.StylePriority.UseBorders = false;
            this.txt第2次饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第2次饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第2次饮酒量2.Weight = 0.29575666908801834D;
            // 
            // txt第3次饮酒量1
            // 
            this.txt第3次饮酒量1.Name = "txt第3次饮酒量1";
            this.txt第3次饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第3次饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第3次饮酒量1.Weight = 0.2957566670381287D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.Text = "/";
            this.xrTableCell345.Weight = 0.038915349801247334D;
            // 
            // txt第3次饮酒量2
            // 
            this.txt第3次饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次饮酒量2.Name = "txt第3次饮酒量2";
            this.txt第3次饮酒量2.StylePriority.UseBorders = false;
            this.txt第3次饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第3次饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第3次饮酒量2.Weight = 0.29575666856467314D;
            // 
            // txt第4次饮酒量1
            // 
            this.txt第4次饮酒量1.Name = "txt第4次饮酒量1";
            this.txt第4次饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第4次饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt第4次饮酒量1.Weight = 0.29575666377628157D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Text = "/";
            this.xrTableCell347.Weight = 0.038915348170323744D;
            // 
            // txt第4次饮酒量2
            // 
            this.txt第4次饮酒量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次饮酒量2.Name = "txt第4次饮酒量2";
            this.txt第4次饮酒量2.StylePriority.UseBorders = false;
            this.txt第4次饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第4次饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次饮酒量2.Weight = 0.29575666693374958D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell278,
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell282,
            this.xrTableCell285,
            this.xrTableCell287});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 1.6000000000000003D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell278.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell278.Multiline = true;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.StylePriority.UseFont = false;
            this.xrTableCell278.Text = "活\r\n方";
            this.xrTableCell278.Weight = 0.058373024022681956D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "运动";
            this.xrTableCell279.Weight = 0.43196042645954D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Weight = 0.630428690507028D;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23});
            this.xrTable2.SizeF = new System.Drawing.SizeF(162F, 40F);
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281,
            this.txt次周11,
            this.xrTableCell290,
            this.txt分钟11,
            this.xrTableCell292,
            this.xrTableCell288});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.Weight = 0.092592592592592587D;
            // 
            // txt次周11
            // 
            this.txt次周11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周11.CanGrow = false;
            this.txt次周11.Name = "txt次周11";
            this.txt次周11.StylePriority.UseBorders = false;
            this.txt次周11.Weight = 0.18518518518518523D;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBorders = false;
            this.xrTableCell290.Text = "次/周";
            this.xrTableCell290.Weight = 0.83333333333333348D;
            // 
            // txt分钟11
            // 
            this.txt分钟11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟11.CanGrow = false;
            this.txt分钟11.Name = "txt分钟11";
            this.txt分钟11.StylePriority.UseBorders = false;
            this.txt分钟11.Weight = 0.83333333333333326D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBorders = false;
            this.xrTableCell292.Text = "分钟/次";
            this.xrTableCell292.Weight = 0.98148148148148162D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.Weight = 0.07407407407407407D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell283,
            this.txt次周21,
            this.xrTableCell291,
            this.txt分钟21,
            this.xrTableCell293,
            this.xrTableCell289});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Weight = 0.092592592592592587D;
            // 
            // txt次周21
            // 
            this.txt次周21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周21.CanGrow = false;
            this.txt次周21.Name = "txt次周21";
            this.txt次周21.StylePriority.UseBorders = false;
            this.txt次周21.Weight = 0.18518518518518526D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.StylePriority.UseBorders = false;
            this.xrTableCell291.Text = "次/周";
            this.xrTableCell291.Weight = 0.83333333333333326D;
            // 
            // txt分钟21
            // 
            this.txt分钟21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟21.CanGrow = false;
            this.txt分钟21.Name = "txt分钟21";
            this.txt分钟21.StylePriority.UseBorders = false;
            this.txt分钟21.Weight = 0.83333361590350119D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBorders = false;
            this.xrTableCell293.Text = "分钟/次";
            this.xrTableCell293.Weight = 0.98148148148148151D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBorders = false;
            this.xrTableCell289.Weight = 0.074073791503906236D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Weight = 0.63042821245652048D;
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow25});
            this.xrTable3.SizeF = new System.Drawing.SizeF(162F, 40F);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell300,
            this.txt次周12,
            this.xrTableCell302,
            this.txt分钟12,
            this.xrTableCell298,
            this.xrTableCell263});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBorders = false;
            this.xrTableCell300.Weight = 0.092592592592592643D;
            // 
            // txt次周12
            // 
            this.txt次周12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周12.CanGrow = false;
            this.txt次周12.Name = "txt次周12";
            this.txt次周12.StylePriority.UseBorders = false;
            this.txt次周12.Weight = 0.18518518518518526D;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Text = "次/周";
            this.xrTableCell302.Weight = 0.83333333333333326D;
            // 
            // txt分钟12
            // 
            this.txt分钟12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟12.CanGrow = false;
            this.txt分钟12.Name = "txt分钟12";
            this.txt分钟12.StylePriority.UseBorders = false;
            this.txt分钟12.Weight = 0.83333333333333326D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.StylePriority.UseBorders = false;
            this.xrTableCell298.Text = "分钟/次";
            this.xrTableCell298.Weight = 0.98148148148148162D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.0740740740740741D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell301,
            this.txt次周22,
            this.xrTableCell303,
            this.txt分钟22,
            this.xrTableCell299,
            this.xrTableCell267});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBorders = false;
            this.xrTableCell301.Weight = 0.092592592592592643D;
            // 
            // txt次周22
            // 
            this.txt次周22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周22.CanGrow = false;
            this.txt次周22.Name = "txt次周22";
            this.txt次周22.StylePriority.UseBorders = false;
            this.txt次周22.Weight = 0.185185185185185D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Text = "次/周";
            this.xrTableCell303.Weight = 0.83333333333333348D;
            // 
            // txt分钟22
            // 
            this.txt分钟22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟22.CanGrow = false;
            this.txt分钟22.Name = "txt分钟22";
            this.txt分钟22.StylePriority.UseBorders = false;
            this.txt分钟22.Weight = 0.83333333333333326D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.Text = "分钟/次";
            this.xrTableCell299.Weight = 0.98148148148148162D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Weight = 0.07407407407407407D;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Weight = 0.63042820726655913D;
            // 
            // xrTable4
            // 
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.0001220703F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27});
            this.xrTable4.SizeF = new System.Drawing.SizeF(162F, 40F);
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell308,
            this.txt次周13,
            this.xrTableCell310,
            this.txt分钟13,
            this.xrTableCell312,
            this.xrTableCell271});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.Weight = 0.092592592592592643D;
            // 
            // txt次周13
            // 
            this.txt次周13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周13.CanGrow = false;
            this.txt次周13.Name = "txt次周13";
            this.txt次周13.StylePriority.UseBorders = false;
            this.txt次周13.Weight = 0.18518518518518523D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Text = "次/周";
            this.xrTableCell310.Weight = 0.83333333333333348D;
            // 
            // txt分钟13
            // 
            this.txt分钟13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟13.CanGrow = false;
            this.txt分钟13.Name = "txt分钟13";
            this.txt分钟13.StylePriority.UseBorders = false;
            this.txt分钟13.Weight = 0.83333333333333326D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.Text = "分钟/次";
            this.xrTableCell312.Weight = 0.98148148148148162D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Weight = 0.07407407407407407D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell309,
            this.txt次周23,
            this.xrTableCell311,
            this.txt分钟23,
            this.xrTableCell313,
            this.xrTableCell273});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Weight = 0.092592592592592643D;
            // 
            // txt次周23
            // 
            this.txt次周23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周23.CanGrow = false;
            this.txt次周23.Name = "txt次周23";
            this.txt次周23.StylePriority.UseBorders = false;
            this.txt次周23.Weight = 0.18518518518518523D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Text = "次/周";
            this.xrTableCell311.Weight = 0.83333333333333337D;
            // 
            // txt分钟23
            // 
            this.txt分钟23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟23.CanGrow = false;
            this.txt分钟23.Name = "txt分钟23";
            this.txt分钟23.StylePriority.UseBorders = false;
            this.txt分钟23.Weight = 0.83333333333333326D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseBorders = false;
            this.xrTableCell313.Text = "分钟/次";
            this.xrTableCell313.Weight = 0.98148148148148151D;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.Weight = 0.074074074074074084D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell287.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Weight = 0.6304296320587548D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 6.357829E-05F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28,
            this.xrTableRow29});
            this.xrTable5.SizeF = new System.Drawing.SizeF(161.5001F, 39.99994F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell314,
            this.txt次周14,
            this.xrTableCell275,
            this.txt分钟14,
            this.xrTableCell316,
            this.xrTableCell322});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBorders = false;
            this.xrTableCell314.Weight = 0.092879195044641219D;
            // 
            // txt次周14
            // 
            this.txt次周14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt次周14.CanGrow = false;
            this.txt次周14.Name = "txt次周14";
            this.txt次周14.StylePriority.UseBorders = false;
            this.txt次周14.Weight = 0.18575840780458314D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.Text = "次/周";
            this.xrTableCell275.Weight = 0.83591275786994657D;
            // 
            // txt分钟14
            // 
            this.txt分钟14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分钟14.CanGrow = false;
            this.txt分钟14.Name = "txt分钟14";
            this.txt分钟14.StylePriority.UseBorders = false;
            this.txt分钟14.Weight = 0.8359127497527491D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBorders = false;
            this.xrTableCell316.Text = "分钟/次";
            this.xrTableCell316.Weight = 0.98451948937883993D;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.Weight = 0.065017400149239915D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell315,
            this.txt次周24,
            this.xrTableCell277,
            this.txt分钟24,
            this.xrTableCell317,
            this.xrTableCell323});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBorders = false;
            this.xrTableCell315.Weight = 0.092879195044641233D;
            // 
            // txt次周24
            // 
            this.txt次周24.CanGrow = false;
            this.txt次周24.Name = "txt次周24";
            this.txt次周24.Weight = 0.18575840780426173D;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.Text = "次/周";
            this.xrTableCell277.Weight = 0.8359127578705895D;
            // 
            // txt分钟24
            // 
            this.txt分钟24.CanGrow = false;
            this.txt分钟24.Name = "txt分钟24";
            this.txt分钟24.Weight = 0.835912749752749D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.Text = "分钟/次";
            this.xrTableCell317.Weight = 0.98451948937851852D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.Weight = 0.065017400149239929D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell324,
            this.xrTableCell325,
            this.txt摄盐情况1,
            this.xrTableCell330,
            this.txt摄盐情况2,
            this.xrTableCell326,
            this.txt摄盐情况3,
            this.xrTableCell331,
            this.txt摄盐情况4,
            this.xrTableCell327,
            this.txt摄盐情况5,
            this.xrTableCell332,
            this.txt摄盐情况6,
            this.xrTableCell328,
            this.txt摄盐情况7,
            this.xrTableCell333,
            this.txt摄盐情况8,
            this.xrTableCell329});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 0.8D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell324.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.StylePriority.UseBorders = false;
            this.xrTableCell324.StylePriority.UseFont = false;
            this.xrTableCell324.Text = "式";
            this.xrTableCell324.Weight = 0.058373024022681956D;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell325.StylePriority.UseBorders = false;
            this.xrTableCell325.StylePriority.UsePadding = false;
            this.xrTableCell325.StylePriority.UseTextAlignment = false;
            this.xrTableCell325.Text = "摄盐情况(咸淡)";
            this.xrTableCell325.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell325.Weight = 0.43196042645954D;
            // 
            // txt摄盐情况1
            // 
            this.txt摄盐情况1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt摄盐情况1.Name = "txt摄盐情况1";
            this.txt摄盐情况1.StylePriority.UseBorders = false;
            this.txt摄盐情况1.Weight = 0.077830702404783819D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.Text = "轻/中/重";
            this.xrTableCell330.Weight = 0.23738364284873018D;
            // 
            // txt摄盐情况2
            // 
            this.txt摄盐情况2.Name = "txt摄盐情况2";
            this.txt摄盐情况2.Weight = 0.077830702404783819D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.Text = "轻/中/重";
            this.xrTableCell326.Weight = 0.23738364284873018D;
            // 
            // txt摄盐情况3
            // 
            this.txt摄盐情况3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt摄盐情况3.Name = "txt摄盐情况3";
            this.txt摄盐情况3.StylePriority.UseBorders = false;
            this.txt摄盐情况3.Weight = 0.077830701652384385D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Text = "轻/中/重";
            this.xrTableCell331.Weight = 0.23738340457587587D;
            // 
            // txt摄盐情况4
            // 
            this.txt摄盐情况4.Name = "txt摄盐情况4";
            this.txt摄盐情况4.Weight = 0.077830701652384385D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Text = "轻/中/重";
            this.xrTableCell327.Weight = 0.23738340457587587D;
            // 
            // txt摄盐情况5
            // 
            this.txt摄盐情况5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt摄盐情况5.Name = "txt摄盐情况5";
            this.txt摄盐情况5.StylePriority.UseBorders = false;
            this.txt摄盐情况5.Weight = 0.077830700354894047D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.Text = "轻/中/重";
            this.xrTableCell332.Weight = 0.23738340327838553D;
            // 
            // txt摄盐情况6
            // 
            this.txt摄盐情况6.Name = "txt摄盐情况6";
            this.txt摄盐情况6.Weight = 0.077830700354894033D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Text = "轻/中/重";
            this.xrTableCell328.Weight = 0.23738340327838553D;
            // 
            // txt摄盐情况7
            // 
            this.txt摄盐情况7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt摄盐情况7.Name = "txt摄盐情况7";
            this.txt摄盐情况7.StylePriority.UseBorders = false;
            this.txt摄盐情况7.Weight = 0.077830700272260619D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.Text = "轻/中/重";
            this.xrTableCell333.Weight = 0.23738411575711677D;
            // 
            // txt摄盐情况8
            // 
            this.txt摄盐情况8.Name = "txt摄盐情况8";
            this.txt摄盐情况8.Weight = 0.077830700272260619D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.Text = "轻/中/重";
            this.xrTableCell329.Weight = 0.23738411575711677D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell334,
            this.xrTableCell335,
            this.xrTableCell336,
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340,
            this.xrTableCell341,
            this.xrTableCell342,
            this.xrTableCell343});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 0.8D;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell334.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.StylePriority.UseFont = false;
            this.xrTableCell334.Text = "指";
            this.xrTableCell334.Weight = 0.058373024022681956D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.StylePriority.UsePadding = false;
            this.xrTableCell335.StylePriority.UseTextAlignment = false;
            this.xrTableCell335.Text = "心理调整";
            this.xrTableCell335.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell335.Weight = 0.43196042645954D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.StylePriority.UsePadding = false;
            this.xrTableCell336.StylePriority.UseTextAlignment = false;
            this.xrTableCell336.Text = "1良好 2一般 3差";
            this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell336.Weight = 0.56427259375138139D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心理调整1});
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.Weight = 0.066156096755646543D;
            // 
            // txt心理调整1
            // 
            this.txt心理调整1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心理调整1.CanGrow = false;
            this.txt心理调整1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt心理调整1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt心理调整1.Name = "txt心理调整1";
            this.txt心理调整1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt心理调整1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt心理调整1.StylePriority.UseBorders = false;
            this.txt心理调整1.StylePriority.UseFont = false;
            this.txt心理调整1.StylePriority.UsePadding = false;
            this.txt心理调整1.StylePriority.UseTextAlignment = false;
            this.txt心理调整1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.StylePriority.UsePadding = false;
            this.xrTableCell338.StylePriority.UseTextAlignment = false;
            this.xrTableCell338.Text = "1良好 2一般 3差";
            this.xrTableCell338.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell338.Weight = 0.56427259224658255D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心理调整2});
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Weight = 0.066155620209937871D;
            // 
            // txt心理调整2
            // 
            this.txt心理调整2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心理调整2.CanGrow = false;
            this.txt心理调整2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt心理调整2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800051F);
            this.txt心理调整2.Name = "txt心理调整2";
            this.txt心理调整2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt心理调整2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt心理调整2.StylePriority.UseBorders = false;
            this.txt心理调整2.StylePriority.UseFont = false;
            this.txt心理调整2.StylePriority.UsePadding = false;
            this.txt心理调整2.StylePriority.UseTextAlignment = false;
            this.txt心理调整2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.StylePriority.UsePadding = false;
            this.xrTableCell340.StylePriority.UseTextAlignment = false;
            this.xrTableCell340.Text = "1良好 2一般 3差";
            this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell340.Weight = 0.56427258965160187D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心理调整3});
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.Weight = 0.066155617614957252D;
            // 
            // txt心理调整3
            // 
            this.txt心理调整3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心理调整3.CanGrow = false;
            this.txt心理调整3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt心理调整3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800083F);
            this.txt心理调整3.Name = "txt心理调整3";
            this.txt心理调整3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt心理调整3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt心理调整3.StylePriority.UseBorders = false;
            this.txt心理调整3.StylePriority.UseFont = false;
            this.txt心理调整3.StylePriority.UsePadding = false;
            this.txt心理调整3.StylePriority.UseTextAlignment = false;
            this.txt心理调整3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.StylePriority.UsePadding = false;
            this.xrTableCell342.StylePriority.UseTextAlignment = false;
            this.xrTableCell342.Text = "1良好 2一般 3差";
            this.xrTableCell342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell342.Weight = 0.56427258948633507D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell343.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心理调整4});
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.Weight = 0.0661570425724197D;
            // 
            // txt心理调整4
            // 
            this.txt心理调整4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心理调整4.CanGrow = false;
            this.txt心理调整4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt心理调整4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800115F);
            this.txt心理调整4.Name = "txt心理调整4";
            this.txt心理调整4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt心理调整4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt心理调整4.StylePriority.UseBorders = false;
            this.txt心理调整4.StylePriority.UseFont = false;
            this.txt心理调整4.StylePriority.UsePadding = false;
            this.txt心理调整4.StylePriority.UseTextAlignment = false;
            this.txt心理调整4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell352,
            this.xrTableCell353,
            this.xrTableCell354,
            this.xrTableCell355,
            this.xrTableCell356,
            this.xrTableCell357,
            this.xrTableCell358,
            this.xrTableCell359,
            this.xrTableCell360,
            this.xrTableCell361});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseBorders = false;
            this.xrTableRow32.Weight = 0.8D;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell352.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.StylePriority.UseBorders = false;
            this.xrTableCell352.StylePriority.UseFont = false;
            this.xrTableCell352.Text = "导";
            this.xrTableCell352.Weight = 0.058373024022681956D;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.StylePriority.UsePadding = false;
            this.xrTableCell353.StylePriority.UseTextAlignment = false;
            this.xrTableCell353.Text = "遵医行为";
            this.xrTableCell353.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell353.Weight = 0.43196042645954D;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.StylePriority.UsePadding = false;
            this.xrTableCell354.StylePriority.UseTextAlignment = false;
            this.xrTableCell354.Text = "1良好 2一般 3差";
            this.xrTableCell354.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell354.Weight = 0.56427259375138139D;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt遵医行为1});
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.Weight = 0.066156096755646543D;
            // 
            // txt遵医行为1
            // 
            this.txt遵医行为1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt遵医行为1.CanGrow = false;
            this.txt遵医行为1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遵医行为1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.333387F);
            this.txt遵医行为1.Name = "txt遵医行为1";
            this.txt遵医行为1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt遵医行为1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt遵医行为1.StylePriority.UseBorders = false;
            this.txt遵医行为1.StylePriority.UseFont = false;
            this.txt遵医行为1.StylePriority.UsePadding = false;
            this.txt遵医行为1.StylePriority.UseTextAlignment = false;
            this.txt遵医行为1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell356.StylePriority.UseBorders = false;
            this.xrTableCell356.StylePriority.UsePadding = false;
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.Text = "1良好 2一般 3差";
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell356.Weight = 0.56427259224658255D;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt遵医行为2});
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.Weight = 0.066155620209937871D;
            // 
            // txt遵医行为2
            // 
            this.txt遵医行为2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt遵医行为2.CanGrow = false;
            this.txt遵医行为2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遵医行为2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.333419F);
            this.txt遵医行为2.Name = "txt遵医行为2";
            this.txt遵医行为2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt遵医行为2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt遵医行为2.StylePriority.UseBorders = false;
            this.txt遵医行为2.StylePriority.UseFont = false;
            this.txt遵医行为2.StylePriority.UsePadding = false;
            this.txt遵医行为2.StylePriority.UseTextAlignment = false;
            this.txt遵医行为2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.StylePriority.UsePadding = false;
            this.xrTableCell358.StylePriority.UseTextAlignment = false;
            this.xrTableCell358.Text = "1良好 2一般 3差";
            this.xrTableCell358.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell358.Weight = 0.56427258965160187D;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt遵医行为3});
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.Weight = 0.066155617614957252D;
            // 
            // txt遵医行为3
            // 
            this.txt遵医行为3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt遵医行为3.CanGrow = false;
            this.txt遵医行为3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遵医行为3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.333355F);
            this.txt遵医行为3.Name = "txt遵医行为3";
            this.txt遵医行为3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt遵医行为3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt遵医行为3.StylePriority.UseBorders = false;
            this.txt遵医行为3.StylePriority.UseFont = false;
            this.txt遵医行为3.StylePriority.UsePadding = false;
            this.txt遵医行为3.StylePriority.UseTextAlignment = false;
            this.txt遵医行为3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.StylePriority.UsePadding = false;
            this.xrTableCell360.StylePriority.UseTextAlignment = false;
            this.xrTableCell360.Text = "1良好 2一般 3差";
            this.xrTableCell360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell360.Weight = 0.56427258948633507D;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell361.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt遵医行为4});
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.StylePriority.UseBorders = false;
            this.xrTableCell361.Weight = 0.0661570425724197D;
            // 
            // txt遵医行为4
            // 
            this.txt遵医行为4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt遵医行为4.CanGrow = false;
            this.txt遵医行为4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遵医行为4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.333387F);
            this.txt遵医行为4.Name = "txt遵医行为4";
            this.txt遵医行为4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt遵医行为4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt遵医行为4.StylePriority.UseBorders = false;
            this.txt遵医行为4.StylePriority.UseFont = false;
            this.txt遵医行为4.StylePriority.UsePadding = false;
            this.txt遵医行为4.StylePriority.UseTextAlignment = false;
            this.txt遵医行为4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell362,
            this.txt辅助检查1,
            this.txt辅助检查2,
            this.txt辅助检查3,
            this.txt辅助检查4});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 1.6000000000000005D;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.Text = "辅助检查*";
            this.xrTableCell362.Weight = 0.49033342376117089D;
            // 
            // txt辅助检查1
            // 
            this.txt辅助检查1.Name = "txt辅助检查1";
            this.txt辅助检查1.Weight = 0.63042836144899628D;
            // 
            // txt辅助检查2
            // 
            this.txt辅助检查2.Name = "txt辅助检查2";
            this.txt辅助检查2.Weight = 0.63042904528291155D;
            // 
            // txt辅助检查3
            // 
            this.txt辅助检查3.Name = "txt辅助检查3";
            this.txt辅助检查3.Weight = 0.63042868490244952D;
            // 
            // txt辅助检查4
            // 
            this.txt辅助检查4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt辅助检查4.Name = "txt辅助检查4";
            this.txt辅助检查4.StylePriority.UseBorders = false;
            this.txt辅助检查4.Weight = 0.63042867737555619D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell363,
            this.xrTableCell372,
            this.xrTableCell365,
            this.xrTableCell373,
            this.xrTableCell367,
            this.xrTableCell374,
            this.xrTableCell369,
            this.xrTableCell375,
            this.xrTableCell371});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.StylePriority.UseBorders = false;
            this.xrTableRow34.Weight = 0.80000000000000016D;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell363.StylePriority.UsePadding = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "服药依从性";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 0.49033342376117089D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell372.StylePriority.UsePadding = false;
            this.xrTableCell372.StylePriority.UseTextAlignment = false;
            this.xrTableCell372.Text = "1规律 2间断 3不服药";
            this.xrTableCell372.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell372.Weight = 0.56427260736270668D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell365.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt服药依从性1});
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.Weight = 0.066155754086289542D;
            // 
            // txt服药依从性1
            // 
            this.txt服药依从性1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt服药依从性1.CanGrow = false;
            this.txt服药依从性1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt服药依从性1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt服药依从性1.Name = "txt服药依从性1";
            this.txt服药依从性1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt服药依从性1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt服药依从性1.StylePriority.UseBorders = false;
            this.txt服药依从性1.StylePriority.UseFont = false;
            this.txt服药依从性1.StylePriority.UsePadding = false;
            this.txt服药依从性1.StylePriority.UseTextAlignment = false;
            this.txt服药依从性1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell373.StylePriority.UsePadding = false;
            this.xrTableCell373.StylePriority.UseTextAlignment = false;
            this.xrTableCell373.Text = "1规律 2间断 3不服药";
            this.xrTableCell373.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell373.Weight = 0.564272592998982D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell367.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt服药依从性2});
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.Weight = 0.066156452283929523D;
            // 
            // txt服药依从性2
            // 
            this.txt服药依从性2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt服药依从性2.CanGrow = false;
            this.txt服药依从性2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt服药依从性2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt服药依从性2.Name = "txt服药依从性2";
            this.txt服药依从性2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt服药依从性2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt服药依从性2.StylePriority.UseBorders = false;
            this.txt服药依从性2.StylePriority.UseFont = false;
            this.txt服药依从性2.StylePriority.UsePadding = false;
            this.txt服药依从性2.StylePriority.UseTextAlignment = false;
            this.txt服药依从性2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell374.StylePriority.UsePadding = false;
            this.xrTableCell374.StylePriority.UseTextAlignment = false;
            this.xrTableCell374.Text = "1规律 2间断 3不服药";
            this.xrTableCell374.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell374.Weight = 0.56427259094909221D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell369.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt服药依从性3});
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.Weight = 0.066156093953357337D;
            // 
            // txt服药依从性3
            // 
            this.txt服药依从性3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt服药依从性3.CanGrow = false;
            this.txt服药依从性3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt服药依从性3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800051F);
            this.txt服药依从性3.Name = "txt服药依从性3";
            this.txt服药依从性3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt服药依从性3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt服药依从性3.StylePriority.UseBorders = false;
            this.txt服药依从性3.StylePriority.UseFont = false;
            this.txt服药依从性3.StylePriority.UsePadding = false;
            this.txt服药依从性3.StylePriority.UseTextAlignment = false;
            this.txt服药依从性3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell375.StylePriority.UsePadding = false;
            this.xrTableCell375.StylePriority.UseTextAlignment = false;
            this.xrTableCell375.Text = "1规律 2间断 3不服药";
            this.xrTableCell375.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell375.Weight = 0.56427258718564555D;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell371.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt服药依从性4});
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.StylePriority.UseBorders = false;
            this.xrTableCell371.Weight = 0.066156090189910671D;
            // 
            // txt服药依从性4
            // 
            this.txt服药依从性4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt服药依从性4.CanGrow = false;
            this.txt服药依从性4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt服药依从性4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.80002F);
            this.txt服药依从性4.Name = "txt服药依从性4";
            this.txt服药依从性4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt服药依从性4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt服药依从性4.StylePriority.UseBorders = false;
            this.txt服药依从性4.StylePriority.UseFont = false;
            this.txt服药依从性4.StylePriority.UsePadding = false;
            this.txt服药依从性4.StylePriority.UseTextAlignment = false;
            this.txt服药依从性4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell385,
            this.xrTableCell378,
            this.xrTableCell386,
            this.xrTableCell379,
            this.xrTableCell380,
            this.xrTableCell387,
            this.xrTableCell381,
            this.xrTableCell382,
            this.xrTableCell388,
            this.xrTableCell383,
            this.xrTableCell384});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.Weight = 0.80000000000000016D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.StylePriority.UsePadding = false;
            this.xrTableCell376.StylePriority.UseTextAlignment = false;
            this.xrTableCell376.Text = "药物不良反应";
            this.xrTableCell376.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell376.Weight = 0.49033342376117089D;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.StylePriority.UsePadding = false;
            this.xrTableCell377.StylePriority.UseTextAlignment = false;
            this.xrTableCell377.Text = "1无 2有";
            this.xrTableCell377.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell377.Weight = 0.26462440213870986D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应有1});
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.Weight = 0.2996482117788597D;
            // 
            // txt药物不良反应有1
            // 
            this.txt药物不良反应有1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt药物不良反应有1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt药物不良反应有1.Name = "txt药物不良反应有1";
            this.txt药物不良反应有1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应有1.SizeF = new System.Drawing.SizeF(77.00002F, 18F);
            this.txt药物不良反应有1.StylePriority.UseBorders = false;
            this.txt药物不良反应有1.StylePriority.UsePadding = false;
            this.txt药物不良反应有1.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应有1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应1});
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.Weight = 0.06615574753142664D;
            // 
            // txt药物不良反应1
            // 
            this.txt药物不良反应1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物不良反应1.CanGrow = false;
            this.txt药物不良反应1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物不良反应1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt药物不良反应1.Name = "txt药物不良反应1";
            this.txt药物不良反应1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物不良反应1.StylePriority.UseBorders = false;
            this.txt药物不良反应1.StylePriority.UseFont = false;
            this.txt药物不良反应1.StylePriority.UsePadding = false;
            this.txt药物不良反应1.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell386.StylePriority.UseBorders = false;
            this.xrTableCell386.StylePriority.UsePadding = false;
            this.xrTableCell386.StylePriority.UseTextAlignment = false;
            this.xrTableCell386.Text = "1无 2有";
            this.xrTableCell386.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell386.Weight = 0.26462438840198471D;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应有2});
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.Weight = 0.29964867963790709D;
            // 
            // txt药物不良反应有2
            // 
            this.txt药物不良反应有2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt药物不良反应有2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt药物不良反应有2.Name = "txt药物不良反应有2";
            this.txt药物不良反应有2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应有2.SizeF = new System.Drawing.SizeF(77F, 18F);
            this.txt药物不良反应有2.StylePriority.UseBorders = false;
            this.txt药物不良反应有2.StylePriority.UsePadding = false;
            this.txt药物不良反应有2.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应有2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应2});
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.Weight = 0.066155977243019748D;
            // 
            // txt药物不良反应2
            // 
            this.txt药物不良反应2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物不良反应2.CanGrow = false;
            this.txt药物不良反应2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物不良反应2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.8F);
            this.txt药物不良反应2.Name = "txt药物不良反应2";
            this.txt药物不良反应2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物不良反应2.StylePriority.UseBorders = false;
            this.txt药物不良反应2.StylePriority.UseFont = false;
            this.txt药物不良反应2.StylePriority.UsePadding = false;
            this.txt药物不良反应2.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell387.StylePriority.UseBorders = false;
            this.xrTableCell387.StylePriority.UsePadding = false;
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.Text = "1无 2有";
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell387.Weight = 0.2646243873770398D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应有3});
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.Weight = 0.29964820357205241D;
            // 
            // txt药物不良反应有3
            // 
            this.txt药物不良反应有3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt药物不良反应有3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt药物不良反应有3.Name = "txt药物不良反应有3";
            this.txt药物不良反应有3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应有3.SizeF = new System.Drawing.SizeF(77F, 18F);
            this.txt药物不良反应有3.StylePriority.UseBorders = false;
            this.txt药物不良反应有3.StylePriority.UsePadding = false;
            this.txt药物不良反应有3.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应有3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应3});
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.Weight = 0.066156093953357337D;
            // 
            // txt药物不良反应3
            // 
            this.txt药物不良反应3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物不良反应3.CanGrow = false;
            this.txt药物不良反应3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物不良反应3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800083F);
            this.txt药物不良反应3.Name = "txt药物不良反应3";
            this.txt药物不良反应3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物不良反应3.StylePriority.UseBorders = false;
            this.txt药物不良反应3.StylePriority.UseFont = false;
            this.txt药物不良反应3.StylePriority.UsePadding = false;
            this.txt药物不良反应3.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell388.StylePriority.UseBorders = false;
            this.xrTableCell388.StylePriority.UsePadding = false;
            this.xrTableCell388.StylePriority.UseTextAlignment = false;
            this.xrTableCell388.Text = "1无 2有";
            this.xrTableCell388.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell388.Weight = 0.26462438549531647D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应有4});
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.Weight = 0.29886865955740788D;
            // 
            // txt药物不良反应有4
            // 
            this.txt药物不良反应有4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt药物不良反应有4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt药物不良反应有4.Name = "txt药物不良反应有4";
            this.txt药物不良反应有4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应有4.SizeF = new System.Drawing.SizeF(76.79968F, 18F);
            this.txt药物不良反应有4.StylePriority.UseBorders = false;
            this.txt药物不良反应有4.StylePriority.UsePadding = false;
            this.txt药物不良反应有4.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应有4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell384.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt药物不良反应4});
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseBorders = false;
            this.xrTableCell384.Weight = 0.066935632322831881D;
            // 
            // txt药物不良反应4
            // 
            this.txt药物不良反应4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物不良反应4.CanGrow = false;
            this.txt药物不良反应4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物不良反应4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.800051F);
            this.txt药物不良反应4.Name = "txt药物不良反应4";
            this.txt药物不良反应4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt药物不良反应4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物不良反应4.StylePriority.UseBorders = false;
            this.txt药物不良反应4.StylePriority.UseFont = false;
            this.txt药物不良反应4.StylePriority.UsePadding = false;
            this.txt药物不良反应4.StylePriority.UseTextAlignment = false;
            this.txt药物不良反应4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell389,
            this.xrTableCell391,
            this.xrTableCell390,
            this.xrTableCell392,
            this.xrTableCell393,
            this.xrTableCell394,
            this.xrTableCell398,
            this.xrTableCell395,
            this.xrTableCell401});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.StylePriority.UseBorders = false;
            this.xrTableRow36.Weight = 1.6000000000000005D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.Text = "此次随访分类";
            this.xrTableCell389.Weight = 0.49033342376117089D;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.CanGrow = false;
            this.xrTableCell391.Multiline = true;
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell391.StylePriority.UsePadding = false;
            this.xrTableCell391.StylePriority.UseTextAlignment = false;
            this.xrTableCell391.Text = "1控制满意 2控制不满\r\n3不良反应 4并发症";
            this.xrTableCell391.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell391.Weight = 0.56427260080784369D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell390.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel131,
            this.txt随访分类1});
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Weight = 0.066156222572336415D;
            // 
            // xrLabel131
            // 
            this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel131.CanGrow = false;
            this.xrLabel131.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel131.StylePriority.UseBorders = false;
            this.xrLabel131.StylePriority.UseFont = false;
            this.xrLabel131.StylePriority.UsePadding = false;
            this.xrLabel131.StylePriority.UseTextAlignment = false;
            this.xrLabel131.Text = "意";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt随访分类1
            // 
            this.txt随访分类1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访分类1.CanGrow = false;
            this.txt随访分类1.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访分类1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.txt随访分类1.Name = "txt随访分类1";
            this.txt随访分类1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访分类1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访分类1.StylePriority.UseBorders = false;
            this.txt随访分类1.StylePriority.UseFont = false;
            this.txt随访分类1.StylePriority.UsePadding = false;
            this.txt随访分类1.StylePriority.UseTextAlignment = false;
            this.txt随访分类1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.CanGrow = false;
            this.xrTableCell392.Multiline = true;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell392.StylePriority.UsePadding = false;
            this.xrTableCell392.StylePriority.UseTextAlignment = false;
            this.xrTableCell392.Text = "1控制满意 2控制不满\r\n3不良反应 4并发症";
            this.xrTableCell392.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell392.Weight = 0.56427260049434413D;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell393.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访分类2,
            this.xrLabel132});
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseBorders = false;
            this.xrTableCell393.Weight = 0.066155509697472081D;
            // 
            // txt随访分类2
            // 
            this.txt随访分类2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访分类2.CanGrow = false;
            this.txt随访分类2.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访分类2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.txt随访分类2.Name = "txt随访分类2";
            this.txt随访分类2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访分类2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访分类2.StylePriority.UseBorders = false;
            this.txt随访分类2.StylePriority.UseFont = false;
            this.txt随访分类2.StylePriority.UsePadding = false;
            this.txt随访分类2.StylePriority.UseTextAlignment = false;
            this.txt随访分类2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel132.CanGrow = false;
            this.xrLabel132.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.500016F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.StylePriority.UseFont = false;
            this.xrLabel132.StylePriority.UsePadding = false;
            this.xrLabel132.StylePriority.UseTextAlignment = false;
            this.xrLabel132.Text = "意";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.CanGrow = false;
            this.xrTableCell394.Multiline = true;
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell394.StylePriority.UsePadding = false;
            this.xrTableCell394.StylePriority.UseTextAlignment = false;
            this.xrTableCell394.Text = "1控制满意 2控制不满\r\n3不良反应 4并发症";
            this.xrTableCell394.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell394.Weight = 0.56427259000859309D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell398.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访分类3,
            this.xrLabel133});
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseBorders = false;
            this.xrTableCell398.Weight = 0.066156568053767961D;
            // 
            // txt随访分类3
            // 
            this.txt随访分类3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访分类3.CanGrow = false;
            this.txt随访分类3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访分类3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21.00004F);
            this.txt随访分类3.Name = "txt随访分类3";
            this.txt随访分类3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访分类3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访分类3.StylePriority.UseBorders = false;
            this.txt随访分类3.StylePriority.UseFont = false;
            this.txt随访分类3.StylePriority.UsePadding = false;
            this.txt随访分类3.StylePriority.UseTextAlignment = false;
            this.txt随访分类3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel133
            // 
            this.xrLabel133.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel133.CanGrow = false;
            this.xrLabel133.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 3.500048F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel133.StylePriority.UseBorders = false;
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.StylePriority.UsePadding = false;
            this.xrLabel133.StylePriority.UseTextAlignment = false;
            this.xrLabel133.Text = "意";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.CanGrow = false;
            this.xrTableCell395.Multiline = true;
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell395.StylePriority.UsePadding = false;
            this.xrTableCell395.StylePriority.UseTextAlignment = false;
            this.xrTableCell395.Text = "1控制满意 2控制不满\r\n3不良反应 4并发症";
            this.xrTableCell395.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell395.Weight = 0.56427258718564555D;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell401.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访分类4,
            this.xrLabel134});
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.StylePriority.UseBorders = false;
            this.xrTableCell401.Weight = 0.066156090189910657D;
            // 
            // txt随访分类4
            // 
            this.txt随访分类4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访分类4.CanGrow = false;
            this.txt随访分类4.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt随访分类4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21.0001F);
            this.txt随访分类4.Name = "txt随访分类4";
            this.txt随访分类4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt随访分类4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt随访分类4.StylePriority.UseBorders = false;
            this.txt随访分类4.StylePriority.UseFont = false;
            this.txt随访分类4.StylePriority.UsePadding = false;
            this.txt随访分类4.StylePriority.UseTextAlignment = false;
            this.txt随访分类4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel134.CanGrow = false;
            this.xrLabel134.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.500144F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.StylePriority.UseFont = false;
            this.xrLabel134.StylePriority.UsePadding = false;
            this.xrLabel134.StylePriority.UseTextAlignment = false;
            this.xrLabel134.Text = "意";
            this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell407,
            this.xrTableCell396,
            this.txt第1次药物名称1,
            this.txt第2次药物名称1,
            this.txt第3次药物名称1,
            this.txt第4次药物名称1});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.StylePriority.UseBorders = false;
            this.xrTableRow37.Weight = 0.80000000000000016D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.StylePriority.UseBorders = false;
            this.xrTableCell407.Weight = 0.058373025507184872D;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell396.StylePriority.UsePadding = false;
            this.xrTableCell396.StylePriority.UseTextAlignment = false;
            this.xrTableCell396.Text = "药物名称1";
            this.xrTableCell396.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell396.Weight = 0.431960398253986D;
            // 
            // txt第1次药物名称1
            // 
            this.txt第1次药物名称1.CanGrow = false;
            this.txt第1次药物名称1.Name = "txt第1次药物名称1";
            this.txt第1次药物名称1.Weight = 0.63042834178440743D;
            // 
            // txt第2次药物名称1
            // 
            this.txt第2次药物名称1.Name = "txt第2次药物名称1";
            this.txt第2次药物名称1.Weight = 0.63042858554622572D;
            // 
            // txt第3次药物名称1
            // 
            this.txt第3次药物名称1.Name = "txt第3次药物名称1";
            this.txt第3次药物名称1.Weight = 0.6304282184662926D;
            // 
            // txt第4次药物名称1
            // 
            this.txt第4次药物名称1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次药物名称1.Name = "txt第4次药物名称1";
            this.txt第4次药物名称1.StylePriority.UseBorders = false;
            this.txt第4次药物名称1.Weight = 0.63042962321298779D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell408,
            this.xrTableCell409,
            this.txt第1次用法用量1,
            this.txt第2次用法用量1,
            this.txt第3次用法用量1,
            this.txt第4次用法用量1});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.StylePriority.UseBorders = false;
            this.xrTableRow38.Weight = 0.80000000000000016D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseBorders = false;
            this.xrTableCell408.Weight = 0.058373025507184872D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.StylePriority.UsePadding = false;
            this.xrTableCell409.StylePriority.UseTextAlignment = false;
            this.xrTableCell409.Text = "用法用量";
            this.xrTableCell409.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell409.Weight = 0.431960398253986D;
            // 
            // txt第1次用法用量1
            // 
            this.txt第1次用法用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次用法用量1.CanGrow = false;
            this.txt第1次用法用量1.Name = "txt第1次用法用量1";
            this.txt第1次用法用量1.StylePriority.UseBorders = false;
            this.txt第1次用法用量1.Weight = 0.63042882338018014D;
            // 
            // txt第2次用法用量1
            // 
            this.txt第2次用法用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次用法用量1.Name = "txt第2次用法用量1";
            this.txt第2次用法用量1.StylePriority.UseBorders = false;
            this.txt第2次用法用量1.Weight = 0.63042811019181622D;
            // 
            // txt第3次用法用量1
            // 
            this.txt第3次用法用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次用法用量1.Name = "txt第3次用法用量1";
            this.txt第3次用法用量1.StylePriority.UseBorders = false;
            this.txt第3次用法用量1.Weight = 0.63042915806236111D;
            // 
            // txt第4次用法用量1
            // 
            this.txt第4次用法用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次用法用量1.Name = "txt第4次用法用量1";
            this.txt第4次用法用量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.txt第4次用法用量1.StylePriority.UseBorders = false;
            this.txt第4次用法用量1.StylePriority.UsePadding = false;
            this.txt第4次用法用量1.StylePriority.UseTextAlignment = false;
            this.txt第4次用法用量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt第4次用法用量1.Weight = 0.63042867737555619D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell430,
            this.xrTableCell431,
            this.txt第1次药物名称2,
            this.txt第2次药物名称2,
            this.txt第3次药物名称2,
            this.txt第4次药物名称2});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.StylePriority.UseBorders = false;
            this.xrTableRow39.Weight = 0.80000000000000016D;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell430.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.StylePriority.UseBorders = false;
            this.xrTableCell430.StylePriority.UseFont = false;
            this.xrTableCell430.Text = "用";
            this.xrTableCell430.Weight = 0.058373025507184872D;
            // 
            // xrTableCell431
            // 
            this.xrTableCell431.Name = "xrTableCell431";
            this.xrTableCell431.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell431.StylePriority.UsePadding = false;
            this.xrTableCell431.StylePriority.UseTextAlignment = false;
            this.xrTableCell431.Text = "药物名称2";
            this.xrTableCell431.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell431.Weight = 0.431960398253986D;
            // 
            // txt第1次药物名称2
            // 
            this.txt第1次药物名称2.CanGrow = false;
            this.txt第1次药物名称2.Name = "txt第1次药物名称2";
            this.txt第1次药物名称2.Weight = 0.63042869478765828D;
            // 
            // txt第2次药物名称2
            // 
            this.txt第2次药物名称2.Name = "txt第2次药物名称2";
            this.txt第2次药物名称2.Weight = 0.630428707458666D;
            // 
            // txt第3次药物名称2
            // 
            this.txt第3次药物名称2.Name = "txt第3次药物名称2";
            this.txt第3次药物名称2.Weight = 0.63042869044913008D;
            // 
            // txt第4次药物名称2
            // 
            this.txt第4次药物名称2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次药物名称2.Name = "txt第4次药物名称2";
            this.txt第4次药物名称2.StylePriority.UseBorders = false;
            this.txt第4次药物名称2.Weight = 0.6304286763144592D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell456,
            this.xrTableCell457,
            this.txt第1次用法用量2,
            this.txt第2次用法用量2,
            this.txt第3次用法用量2,
            this.txt第4次用法用量2});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.StylePriority.UseBorders = false;
            this.xrTableRow40.Weight = 0.80000000000000016D;
            // 
            // xrTableCell456
            // 
            this.xrTableCell456.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell456.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell456.Name = "xrTableCell456";
            this.xrTableCell456.StylePriority.UseBorders = false;
            this.xrTableCell456.StylePriority.UseFont = false;
            this.xrTableCell456.Text = "药";
            this.xrTableCell456.Weight = 0.058373025507184872D;
            // 
            // xrTableCell457
            // 
            this.xrTableCell457.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell457.Name = "xrTableCell457";
            this.xrTableCell457.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell457.StylePriority.UseBorders = false;
            this.xrTableCell457.StylePriority.UsePadding = false;
            this.xrTableCell457.StylePriority.UseTextAlignment = false;
            this.xrTableCell457.Text = "用法用量";
            this.xrTableCell457.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell457.Weight = 0.431960398253986D;
            // 
            // txt第1次用法用量2
            // 
            this.txt第1次用法用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次用法用量2.CanGrow = false;
            this.txt第1次用法用量2.Name = "txt第1次用法用量2";
            this.txt第1次用法用量2.StylePriority.UseBorders = false;
            this.txt第1次用法用量2.Weight = 0.63042882338018014D;
            // 
            // txt第2次用法用量2
            // 
            this.txt第2次用法用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次用法用量2.Name = "txt第2次用法用量2";
            this.txt第2次用法用量2.StylePriority.UseBorders = false;
            this.txt第2次用法用量2.Weight = 0.6304281101918161D;
            // 
            // txt第3次用法用量2
            // 
            this.txt第3次用法用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次用法用量2.Name = "txt第3次用法用量2";
            this.txt第3次用法用量2.StylePriority.UseBorders = false;
            this.txt第3次用法用量2.Weight = 0.63042915806236111D;
            // 
            // txt第4次用法用量2
            // 
            this.txt第4次用法用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次用法用量2.Name = "txt第4次用法用量2";
            this.txt第4次用法用量2.StylePriority.UseBorders = false;
            this.txt第4次用法用量2.Weight = 0.6304286773755563D;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell482,
            this.xrTableCell483,
            this.txt第1次药物名称3,
            this.txt第2次药物名称3,
            this.txt第3次药物名称3,
            this.txt第4次药物名称3});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.StylePriority.UseBorders = false;
            this.xrTableRow41.Weight = 0.80000000000000016D;
            // 
            // xrTableCell482
            // 
            this.xrTableCell482.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell482.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell482.Name = "xrTableCell482";
            this.xrTableCell482.StylePriority.UseBorders = false;
            this.xrTableCell482.StylePriority.UseFont = false;
            this.xrTableCell482.Text = "情";
            this.xrTableCell482.Weight = 0.058373025507184872D;
            // 
            // xrTableCell483
            // 
            this.xrTableCell483.Name = "xrTableCell483";
            this.xrTableCell483.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell483.StylePriority.UsePadding = false;
            this.xrTableCell483.StylePriority.UseTextAlignment = false;
            this.xrTableCell483.Text = "药物名称3";
            this.xrTableCell483.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell483.Weight = 0.431960398253986D;
            // 
            // txt第1次药物名称3
            // 
            this.txt第1次药物名称3.Name = "txt第1次药物名称3";
            this.txt第1次药物名称3.Weight = 0.6304288343025658D;
            // 
            // txt第2次药物名称3
            // 
            this.txt第2次药物名称3.Name = "txt第2次药物名称3";
            this.txt第2次药物名称3.Weight = 0.6304283322958758D;
            // 
            // txt第3次药物名称3
            // 
            this.txt第3次药物名称3.Name = "txt第3次药物名称3";
            this.txt第3次药物名称3.Weight = 0.63042892609701306D;
            // 
            // txt第4次药物名称3
            // 
            this.txt第4次药物名称3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次药物名称3.Name = "txt第4次药物名称3";
            this.txt第4次药物名称3.StylePriority.UseBorders = false;
            this.txt第4次药物名称3.Weight = 0.6304286763144592D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell508,
            this.xrTableCell509,
            this.txt第1次用法用量3,
            this.txt第2次用法用量3,
            this.txt第3次用法用量3,
            this.txt第4次用法用量3});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 0.80000000000000016D;
            // 
            // xrTableCell508
            // 
            this.xrTableCell508.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell508.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell508.Name = "xrTableCell508";
            this.xrTableCell508.StylePriority.UseBorders = false;
            this.xrTableCell508.StylePriority.UseFont = false;
            this.xrTableCell508.Text = "况";
            this.xrTableCell508.Weight = 0.058373025507184872D;
            // 
            // xrTableCell509
            // 
            this.xrTableCell509.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell509.Name = "xrTableCell509";
            this.xrTableCell509.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell509.StylePriority.UseBorders = false;
            this.xrTableCell509.StylePriority.UsePadding = false;
            this.xrTableCell509.StylePriority.UseTextAlignment = false;
            this.xrTableCell509.Text = "用法用量";
            this.xrTableCell509.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell509.Weight = 0.431960398253986D;
            // 
            // txt第1次用法用量3
            // 
            this.txt第1次用法用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次用法用量3.Name = "txt第1次用法用量3";
            this.txt第1次用法用量3.StylePriority.UseBorders = false;
            this.txt第1次用法用量3.Weight = 0.63042882338018014D;
            // 
            // txt第2次用法用量3
            // 
            this.txt第2次用法用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次用法用量3.Name = "txt第2次用法用量3";
            this.txt第2次用法用量3.StylePriority.UseBorders = false;
            this.txt第2次用法用量3.Weight = 0.6304281101918161D;
            // 
            // txt第3次用法用量3
            // 
            this.txt第3次用法用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次用法用量3.Name = "txt第3次用法用量3";
            this.txt第3次用法用量3.StylePriority.UseBorders = false;
            this.txt第3次用法用量3.Weight = 0.630429158062361D;
            // 
            // txt第4次用法用量3
            // 
            this.txt第4次用法用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次用法用量3.Name = "txt第4次用法用量3";
            this.txt第4次用法用量3.StylePriority.UseBorders = false;
            this.txt第4次用法用量3.Weight = 0.63042867737555619D;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell534,
            this.xrTableCell535,
            this.txt其他药物1,
            this.txt其他药物2,
            this.txt其他药物3,
            this.txt其他药物4});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.StylePriority.UseBorders = false;
            this.xrTableRow43.Weight = 0.80000000000000016D;
            // 
            // xrTableCell534
            // 
            this.xrTableCell534.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell534.Name = "xrTableCell534";
            this.xrTableCell534.StylePriority.UseBorders = false;
            this.xrTableCell534.Weight = 0.058373025507184872D;
            // 
            // xrTableCell535
            // 
            this.xrTableCell535.Name = "xrTableCell535";
            this.xrTableCell535.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell535.StylePriority.UsePadding = false;
            this.xrTableCell535.StylePriority.UseTextAlignment = false;
            this.xrTableCell535.Text = "其他药物";
            this.xrTableCell535.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell535.Weight = 0.431960398253986D;
            // 
            // txt其他药物1
            // 
            this.txt其他药物1.Name = "txt其他药物1";
            this.txt其他药物1.Weight = 0.63042869478765851D;
            // 
            // txt其他药物2
            // 
            this.txt其他药物2.Name = "txt其他药物2";
            this.txt其他药物2.Weight = 0.63042823878433785D;
            // 
            // txt其他药物3
            // 
            this.txt其他药物3.Name = "txt其他药物3";
            this.txt其他药物3.Weight = 0.63042915276185951D;
            // 
            // txt其他药物4
            // 
            this.txt其他药物4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt其他药物4.Name = "txt其他药物4";
            this.txt其他药物4.StylePriority.UseBorders = false;
            this.txt其他药物4.Weight = 0.63042868267605778D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell560,
            this.xrTableCell561,
            this.txt第1次用法用量4,
            this.txt第2次用法用量4,
            this.txt第3次用法用量4,
            this.txt第4次用法用量4});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.StylePriority.UseBorders = false;
            this.xrTableRow44.Weight = 0.80000000000000016D;
            // 
            // xrTableCell560
            // 
            this.xrTableCell560.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell560.Name = "xrTableCell560";
            this.xrTableCell560.StylePriority.UseBorders = false;
            this.xrTableCell560.Weight = 0.058373025507184872D;
            // 
            // xrTableCell561
            // 
            this.xrTableCell561.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell561.Name = "xrTableCell561";
            this.xrTableCell561.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell561.StylePriority.UseBorders = false;
            this.xrTableCell561.StylePriority.UsePadding = false;
            this.xrTableCell561.StylePriority.UseTextAlignment = false;
            this.xrTableCell561.Text = "用法用量";
            this.xrTableCell561.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell561.Weight = 0.431960398253986D;
            // 
            // txt第1次用法用量4
            // 
            this.txt第1次用法用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次用法用量4.Name = "txt第1次用法用量4";
            this.txt第1次用法用量4.StylePriority.UseBorders = false;
            this.txt第1次用法用量4.Weight = 0.63042882338018014D;
            // 
            // txt第2次用法用量4
            // 
            this.txt第2次用法用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次用法用量4.Name = "txt第2次用法用量4";
            this.txt第2次用法用量4.StylePriority.UseBorders = false;
            this.txt第2次用法用量4.Weight = 0.6304281101918161D;
            // 
            // txt第3次用法用量4
            // 
            this.txt第3次用法用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次用法用量4.Name = "txt第3次用法用量4";
            this.txt第3次用法用量4.StylePriority.UseBorders = false;
            this.txt第3次用法用量4.Weight = 0.63042915806236111D;
            // 
            // txt第4次用法用量4
            // 
            this.txt第4次用法用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次用法用量4.Name = "txt第4次用法用量4";
            this.txt第4次用法用量4.StylePriority.UseBorders = false;
            this.txt第4次用法用量4.Weight = 0.6304286773755563D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell434,
            this.xrTableCell435,
            this.txt转诊原因1,
            this.txt转诊原因2,
            this.txt转诊原因3,
            this.txt转诊原因4});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 0.80000000000000016D;
            // 
            // xrTableCell434
            // 
            this.xrTableCell434.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell434.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell434.Name = "xrTableCell434";
            this.xrTableCell434.StylePriority.UseBorders = false;
            this.xrTableCell434.StylePriority.UseFont = false;
            this.xrTableCell434.Text = "转";
            this.xrTableCell434.Weight = 0.058373025507184872D;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell435.StylePriority.UsePadding = false;
            this.xrTableCell435.StylePriority.UseTextAlignment = false;
            this.xrTableCell435.Text = "原因";
            this.xrTableCell435.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell435.Weight = 0.431960398253986D;
            // 
            // txt转诊原因1
            // 
            this.txt转诊原因1.CanGrow = false;
            this.txt转诊原因1.Name = "txt转诊原因1";
            this.txt转诊原因1.Weight = 0.63042823269972448D;
            // 
            // txt转诊原因2
            // 
            this.txt转诊原因2.Name = "txt转诊原因2";
            this.txt转诊原因2.Weight = 0.63042929155272742D;
            // 
            // txt转诊原因3
            // 
            this.txt转诊原因3.Name = "txt转诊原因3";
            this.txt转诊原因3.Weight = 0.630427731677025D;
            // 
            // txt转诊原因4
            // 
            this.txt转诊原因4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt转诊原因4.Name = "txt转诊原因4";
            this.txt转诊原因4.StylePriority.UseBorders = false;
            this.txt转诊原因4.Weight = 0.6304295130804366D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell437,
            this.xrTableCell438,
            this.txt机构及科别1,
            this.txt机构及科别2,
            this.txt机构及科别3,
            this.txt机构及科别4});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.StylePriority.UseBorders = false;
            this.xrTableRow46.Weight = 0.80000000000000016D;
            // 
            // xrTableCell437
            // 
            this.xrTableCell437.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell437.Name = "xrTableCell437";
            this.xrTableCell437.StylePriority.UseFont = false;
            this.xrTableCell437.Text = "诊";
            this.xrTableCell437.Weight = 0.058373025507184872D;
            // 
            // xrTableCell438
            // 
            this.xrTableCell438.Name = "xrTableCell438";
            this.xrTableCell438.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell438.StylePriority.UsePadding = false;
            this.xrTableCell438.StylePriority.UseTextAlignment = false;
            this.xrTableCell438.Text = "机构及科别";
            this.xrTableCell438.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell438.Weight = 0.431960398253986D;
            // 
            // txt机构及科别1
            // 
            this.txt机构及科别1.CanGrow = false;
            this.txt机构及科别1.Name = "txt机构及科别1";
            this.txt机构及科别1.Weight = 0.63042823269972448D;
            // 
            // txt机构及科别2
            // 
            this.txt机构及科别2.Name = "txt机构及科别2";
            this.txt机构及科别2.Weight = 0.63042929155272742D;
            // 
            // txt机构及科别3
            // 
            this.txt机构及科别3.Name = "txt机构及科别3";
            this.txt机构及科别3.Weight = 0.630427731677025D;
            // 
            // txt机构及科别4
            // 
            this.txt机构及科别4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt机构及科别4.Name = "txt机构及科别4";
            this.txt机构及科别4.StylePriority.UseBorders = false;
            this.txt机构及科别4.Weight = 0.6304295130804366D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell446,
            this.txt医生建议1,
            this.txt医生建议2,
            this.txt医生建议3,
            this.txt医生建议4});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.StylePriority.UseBorders = false;
            this.xrTableRow47.Weight = 0.80000000000000016D;
            // 
            // xrTableCell446
            // 
            this.xrTableCell446.Name = "xrTableCell446";
            this.xrTableCell446.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell446.StylePriority.UsePadding = false;
            this.xrTableCell446.StylePriority.UseTextAlignment = false;
            this.xrTableCell446.Text = "本次随访医生建议";
            this.xrTableCell446.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell446.Weight = 0.49033342376117089D;
            // 
            // txt医生建议1
            // 
            this.txt医生建议1.Name = "txt医生建议1";
            this.txt医生建议1.Weight = 0.63042823269972448D;
            // 
            // txt医生建议2
            // 
            this.txt医生建议2.Name = "txt医生建议2";
            this.txt医生建议2.Weight = 0.63042929155272742D;
            // 
            // txt医生建议3
            // 
            this.txt医生建议3.Name = "txt医生建议3";
            this.txt医生建议3.Weight = 0.630427731677025D;
            // 
            // txt医生建议4
            // 
            this.txt医生建议4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt医生建议4.Name = "txt医生建议4";
            this.txt医生建议4.StylePriority.UseBorders = false;
            this.txt医生建议4.Weight = 0.6304295130804366D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell455,
            this.txt下次随访日期1,
            this.txt下次随访日期2,
            this.txt下次随访日期3,
            this.txt下次随访日期4});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.StylePriority.UseBorders = false;
            this.xrTableRow48.Weight = 0.80000000000000016D;
            // 
            // xrTableCell455
            // 
            this.xrTableCell455.Name = "xrTableCell455";
            this.xrTableCell455.Text = "下次随访日期";
            this.xrTableCell455.Weight = 0.49033342376117089D;
            // 
            // txt下次随访日期1
            // 
            this.txt下次随访日期1.Name = "txt下次随访日期1";
            this.txt下次随访日期1.Weight = 0.63042823269972448D;
            // 
            // txt下次随访日期2
            // 
            this.txt下次随访日期2.Name = "txt下次随访日期2";
            this.txt下次随访日期2.Weight = 0.63042929155272742D;
            // 
            // txt下次随访日期3
            // 
            this.txt下次随访日期3.Name = "txt下次随访日期3";
            this.txt下次随访日期3.Weight = 0.630427731677025D;
            // 
            // txt下次随访日期4
            // 
            this.txt下次随访日期4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt下次随访日期4.Name = "txt下次随访日期4";
            this.txt下次随访日期4.StylePriority.UseBorders = false;
            this.txt下次随访日期4.Weight = 0.6304295130804366D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell489,
            this.txt随访医生签名1,
            this.txt随访医生签名2,
            this.txt随访医生签名3,
            this.txt随访医生签名4});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.StylePriority.UseBorders = false;
            this.xrTableRow49.Weight = 0.80000000000000016D;
            // 
            // xrTableCell489
            // 
            this.xrTableCell489.Name = "xrTableCell489";
            this.xrTableCell489.Text = "随访医生签名";
            this.xrTableCell489.Weight = 0.49033342376117089D;
            // 
            // txt随访医生签名1
            // 
            this.txt随访医生签名1.Name = "txt随访医生签名1";
            this.txt随访医生签名1.Weight = 0.63042823269972448D;
            // 
            // txt随访医生签名2
            // 
            this.txt随访医生签名2.Name = "txt随访医生签名2";
            this.txt随访医生签名2.Weight = 0.63042929155272742D;
            // 
            // txt随访医生签名3
            // 
            this.txt随访医生签名3.Name = "txt随访医生签名3";
            this.txt随访医生签名3.Weight = 0.630427731677025D;
            // 
            // txt随访医生签名4
            // 
            this.txt随访医生签名4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt随访医生签名4.Name = "txt随访医生签名4";
            this.txt随访医生签名4.StylePriority.UseBorders = false;
            this.txt随访医生签名4.Weight = 0.6304295130804366D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell444});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.80000000000000016D;
            // 
            // xrTableCell444
            // 
            this.xrTableCell444.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell444.Name = "xrTableCell444";
            this.xrTableCell444.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell444.StylePriority.UseFont = false;
            this.xrTableCell444.StylePriority.UsePadding = false;
            this.xrTableCell444.StylePriority.UseTextAlignment = false;
            this.xrTableCell444.Text = "脑卒中类型：①蛛网膜下腔出血②脑出血③脑血栓形成④脑栓塞⑤脑梗塞⑥未分类脑卒中";
            this.xrTableCell444.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell444.Weight = 3.012048192771084D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell454});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.80000000000000016D;
            // 
            // xrTableCell454
            // 
            this.xrTableCell454.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell454.Name = "xrTableCell454";
            this.xrTableCell454.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell454.StylePriority.UseFont = false;
            this.xrTableCell454.StylePriority.UsePadding = false;
            this.xrTableCell454.StylePriority.UseTextAlignment = false;
            this.xrTableCell454.Text = "脑卒中部位：①大脑中半球左侧②大脑中半球右侧③脑干④小脑⑤不确定";
            this.xrTableCell454.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell454.Weight = 3.012048192771084D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell488});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.80000000000000016D;
            // 
            // xrTableCell488
            // 
            this.xrTableCell488.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell488.Name = "xrTableCell488";
            this.xrTableCell488.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell488.StylePriority.UseFont = false;
            this.xrTableCell488.StylePriority.UsePadding = false;
            this.xrTableCell488.StylePriority.UseTextAlignment = false;
            this.xrTableCell488.Text = "个人史：①冠心病②高血压③糖尿病    并发症：①无②褥疮③呼吸道感染④泌尿系感染⑤深静脉炎";
            this.xrTableCell488.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell488.Weight = 3.012048192771084D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell496});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.80000000000000016D;
            // 
            // xrTableCell496
            // 
            this.xrTableCell496.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell496.Name = "xrTableCell496";
            this.xrTableCell496.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell496.StylePriority.UseFont = false;
            this.xrTableCell496.StylePriority.UsePadding = false;
            this.xrTableCell496.StylePriority.UseTextAlignment = false;
            this.xrTableCell496.Text = "新发卒中症状:①无症状②构音障碍失语③感觉障碍④共济失调⑤右侧肢体瘫痪⑥左侧⑦呕";
            this.xrTableCell496.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell496.Weight = 3.012048192771084D;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell497});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.80000000000000016D;
            // 
            // xrTableCell497
            // 
            this.xrTableCell497.Name = "xrTableCell497";
            this.xrTableCell497.Padding = new DevExpress.XtraPrinting.PaddingInfo(20, 0, 0, 0, 100F);
            this.xrTableCell497.StylePriority.UsePadding = false;
            this.xrTableCell497.StylePriority.UseTextAlignment = false;
            this.xrTableCell497.Text = "吐⑧眩晕⑨昏迷⑩头痛⑾其他";
            this.xrTableCell497.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell497.Weight = 3.012048192771084D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 16F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 16F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report脑卒中患者随访服务记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(26, 27, 16, 16);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell txt随访日期1;
        private DevExpress.XtraReports.UI.XRTableCell txt随访日期2;
        private DevExpress.XtraReports.UI.XRTableCell txt随访日期4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell txt随访日期3;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRLabel txt随访方式1;
        private DevExpress.XtraReports.UI.XRLabel txt随访方式2;
        private DevExpress.XtraReports.UI.XRLabel txt随访方式3;
        private DevExpress.XtraReports.UI.XRLabel txt随访方式4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类1;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类2;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类3;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类4;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类5;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类6;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类7;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类8;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类9;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类10;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类11;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中分类12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位1;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位2;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位3;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位4;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位5;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位6;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位7;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位8;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位9;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位10;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位11;
        private DevExpress.XtraReports.UI.XRLabel txt脑卒中部位12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRLabel txt无症状11;
        private DevExpress.XtraReports.UI.XRLabel txt无症状12;
        private DevExpress.XtraReports.UI.XRLabel txt无症状13;
        private DevExpress.XtraReports.UI.XRLabel txt无症状14;
        private DevExpress.XtraReports.UI.XRLabel txt无症状15;
        private DevExpress.XtraReports.UI.XRLabel txt无症状16;
        private DevExpress.XtraReports.UI.XRLabel txt无症状17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRLabel txt无症状21;
        private DevExpress.XtraReports.UI.XRLabel txt无症状22;
        private DevExpress.XtraReports.UI.XRLabel txt无症状23;
        private DevExpress.XtraReports.UI.XRLabel txt无症状24;
        private DevExpress.XtraReports.UI.XRLabel txt无症状25;
        private DevExpress.XtraReports.UI.XRLabel txt无症状26;
        private DevExpress.XtraReports.UI.XRLabel txt无症状27;
        private DevExpress.XtraReports.UI.XRLabel txt无症状31;
        private DevExpress.XtraReports.UI.XRLabel txt无症状32;
        private DevExpress.XtraReports.UI.XRLabel txt无症状33;
        private DevExpress.XtraReports.UI.XRLabel txt无症状34;
        private DevExpress.XtraReports.UI.XRLabel txt无症状35;
        private DevExpress.XtraReports.UI.XRLabel txt无症状36;
        private DevExpress.XtraReports.UI.XRLabel txt无症状37;
        private DevExpress.XtraReports.UI.XRLabel txt无症状41;
        private DevExpress.XtraReports.UI.XRLabel txt无症状42;
        private DevExpress.XtraReports.UI.XRLabel txt无症状43;
        private DevExpress.XtraReports.UI.XRLabel txt无症状44;
        private DevExpress.XtraReports.UI.XRLabel txt无症状45;
        private DevExpress.XtraReports.UI.XRLabel txt无症状46;
        private DevExpress.XtraReports.UI.XRLabel txt无症状47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell txt半身不遂1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell txt半身不遂2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell txt半身不遂3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell txt半身不遂4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史1;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史2;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史3;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史4;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史5;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史6;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史7;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史8;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史9;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史10;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史11;
        private DevExpress.XtraReports.UI.XRLabel txt个人病史12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRLabel txt并发症1;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中1;
        private DevExpress.XtraReports.UI.XRLabel txt并发症2;
        private DevExpress.XtraReports.UI.XRLabel txt并发症3;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中2;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中3;
        private DevExpress.XtraReports.UI.XRLabel txt并发症4;
        private DevExpress.XtraReports.UI.XRLabel txt并发症5;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中4;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中5;
        private DevExpress.XtraReports.UI.XRLabel txt并发症6;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中6;
        private DevExpress.XtraReports.UI.XRLabel txt并发症7;
        private DevExpress.XtraReports.UI.XRLabel txt并发症8;
        private DevExpress.XtraReports.UI.XRLabel txt并发症9;
        private DevExpress.XtraReports.UI.XRLabel txt并发症10;
        private DevExpress.XtraReports.UI.XRLabel txt并发症11;
        private DevExpress.XtraReports.UI.XRLabel txt并发症12;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中7;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中8;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中9;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中10;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中11;
        private DevExpress.XtraReports.UI.XRLabel txt新发卒中12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次血压2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次血压2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次血压2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次血压2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell txt身高1;
        private DevExpress.XtraReports.UI.XRTableCell txt身高2;
        private DevExpress.XtraReports.UI.XRTableCell txt身高3;
        private DevExpress.XtraReports.UI.XRTableCell txt身高4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次体重2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次体重2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次体重2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次体重2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次体质指数2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次体质指数2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次体质指数2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次体质指数2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell txt腰围1;
        private DevExpress.XtraReports.UI.XRTableCell txt腰围2;
        private DevExpress.XtraReports.UI.XRTableCell txt腰围3;
        private DevExpress.XtraReports.UI.XRTableCell txt腰围4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell txt空腹血糖1;
        private DevExpress.XtraReports.UI.XRTableCell txt空腹血糖2;
        private DevExpress.XtraReports.UI.XRTableCell txt空腹血糖3;
        private DevExpress.XtraReports.UI.XRTableCell txt空腹血糖4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRLabel txt肢体功能1;
        private DevExpress.XtraReports.UI.XRLabel txt肢体功能2;
        private DevExpress.XtraReports.UI.XRLabel txt肢体功能3;
        private DevExpress.XtraReports.UI.XRLabel txt肢体功能4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次吸烟量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次吸烟量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次吸烟量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次吸烟量2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次饮酒量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次饮酒量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次饮酒量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次饮酒量2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟21;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell txt次周11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell txt次周21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell txt次周12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell txt次周22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell txt次周13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell txt次周23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell txt次周14;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell txt次周24;
        private DevExpress.XtraReports.UI.XRTableCell txt分钟24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况1;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况2;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况3;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况4;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况5;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况6;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况7;
        private DevExpress.XtraReports.UI.XRTableCell txt摄盐情况8;
        private DevExpress.XtraReports.UI.XRLabel txt心理调整1;
        private DevExpress.XtraReports.UI.XRLabel txt心理调整2;
        private DevExpress.XtraReports.UI.XRLabel txt心理调整3;
        private DevExpress.XtraReports.UI.XRLabel txt心理调整4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRLabel txt遵医行为1;
        private DevExpress.XtraReports.UI.XRLabel txt遵医行为2;
        private DevExpress.XtraReports.UI.XRLabel txt遵医行为3;
        private DevExpress.XtraReports.UI.XRLabel txt遵医行为4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell txt辅助检查1;
        private DevExpress.XtraReports.UI.XRTableCell txt辅助检查2;
        private DevExpress.XtraReports.UI.XRTableCell txt辅助检查3;
        private DevExpress.XtraReports.UI.XRTableCell txt辅助检查4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应有1;
        private DevExpress.XtraReports.UI.XRLabel txt服药依从性1;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应有2;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应有3;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应有4;
        private DevExpress.XtraReports.UI.XRLabel txt服药依从性2;
        private DevExpress.XtraReports.UI.XRLabel txt服药依从性3;
        private DevExpress.XtraReports.UI.XRLabel txt服药依从性4;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应1;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应2;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应3;
        private DevExpress.XtraReports.UI.XRLabel txt药物不良反应4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRLabel txt随访分类1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel txt随访分类2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel txt随访分类3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel txt随访分类4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次用法用量1;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次用法用量1;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次用法用量1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次药物名称2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell456;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell457;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次用法用量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次用法用量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次用法用量2;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次用法用量2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell482;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell483;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次药物名称3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell508;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell509;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次用法用量3;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次用法用量3;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次用法用量3;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次用法用量3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell534;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell535;
        private DevExpress.XtraReports.UI.XRTableCell txt其他药物1;
        private DevExpress.XtraReports.UI.XRTableCell txt其他药物2;
        private DevExpress.XtraReports.UI.XRTableCell txt其他药物3;
        private DevExpress.XtraReports.UI.XRTableCell txt其他药物4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell560;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell561;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次用法用量4;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次用法用量4;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次用法用量4;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次用法用量4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRTableCell txt转诊原因1;
        private DevExpress.XtraReports.UI.XRTableCell txt转诊原因2;
        private DevExpress.XtraReports.UI.XRTableCell txt转诊原因4;
        private DevExpress.XtraReports.UI.XRTableCell txt转诊原因3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell438;
        private DevExpress.XtraReports.UI.XRTableCell txt机构及科别1;
        private DevExpress.XtraReports.UI.XRTableCell txt机构及科别2;
        private DevExpress.XtraReports.UI.XRTableCell txt机构及科别3;
        private DevExpress.XtraReports.UI.XRTableCell txt机构及科别4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell446;
        private DevExpress.XtraReports.UI.XRTableCell txt医生建议1;
        private DevExpress.XtraReports.UI.XRTableCell txt医生建议2;
        private DevExpress.XtraReports.UI.XRTableCell txt医生建议3;
        private DevExpress.XtraReports.UI.XRTableCell txt医生建议4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell455;
        private DevExpress.XtraReports.UI.XRTableCell txt下次随访日期1;
        private DevExpress.XtraReports.UI.XRTableCell txt下次随访日期2;
        private DevExpress.XtraReports.UI.XRTableCell txt下次随访日期3;
        private DevExpress.XtraReports.UI.XRTableCell txt下次随访日期4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell489;
        private DevExpress.XtraReports.UI.XRTableCell txt随访医生签名1;
        private DevExpress.XtraReports.UI.XRTableCell txt随访医生签名2;
        private DevExpress.XtraReports.UI.XRTableCell txt随访医生签名3;
        private DevExpress.XtraReports.UI.XRTableCell txt随访医生签名4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell444;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell454;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell488;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell496;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell497;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次用法用量1;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次体重1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次体重1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次体重1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次体重1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次吸烟量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次吸烟量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次吸烟量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次吸烟量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell txt第1次饮酒量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次饮酒量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次饮酒量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次饮酒量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
    }
}
