﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using AtomEHR.Business;
using System.Data;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class report脑卒中患者随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        //private string docNo;
        //private System.Collections.Generic.List<string> list;
        #region Fields
        string docNo;
        List<string> dates;
        DataSet _ds脑卒中;
        DataSet _ds用药情况;
        bllMXB脑卒中随访表 _bll脑卒中 = new bllMXB脑卒中随访表();
        #endregion
        public report脑卒中患者随访服务记录表()
        {
            InitializeComponent();
        }



        public report脑卒中患者随访服务记录表(string docNo, List<string> _dates)
        {

            InitializeComponent();
            this.docNo = docNo;
            this.dates = _dates;
            _ds脑卒中 = _bll脑卒中.GetInfoByNZZ(docNo, dates, true);
            DoBindingDataSource(_ds脑卒中);
        }

        private void DoBindingDataSource(DataSet _ds脑卒中)
        {
            DataTable dt脑卒中 = _ds脑卒中.Tables[Models.tb_MXB脑卒中随访表.__TableName];

            for (int i = 0; i < dt脑卒中.Rows.Count; i++)
            {
                string year = dt脑卒中.Rows[i][Models.tb_MXB脑卒中随访表.创建时间].ToString();
                _ds用药情况 = new bllMXB脑卒中随访表().GetInfoByYY(docNo, year, false);

                switch (i)
                {
                    case 0:
                        BindYYQK1();
                        break;
                    case 1:
                        BindYYQK2();
                        break;
                    case 2:
                        BindYYQK3();
                        break;
                    case 3:
                        BindYYQK4();
                        break;
                    default:
                        break;
                }

            }

            DataTable dt健康档案 = _ds脑卒中.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][Models.tb_健康档案.姓名].ToString());

            #region 1次随访记录

            if (dt脑卒中 != null && dt脑卒中.Rows.Count > 0)
            {
                this.txt随访日期1.Text = Convert.ToDateTime(dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt随访方式1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.随访方式].ToString();
                string 第1次脑卒中分类 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.脑卒中类型].ToString();
                if (!string.IsNullOrEmpty(第1次脑卒中分类))
                {
                    string[] a = 第1次脑卒中分类.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中分类" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                        
                    }
                }

                string 第1次脑卒中部位 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.脑卒中部位].ToString();
                if (!string.IsNullOrEmpty(第1次脑卒中部位))
                {
                    string[] a = 第1次脑卒中部位.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中部位" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }
                
                string 第1次随访症状 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.目前症状].ToString();
                if (!string.IsNullOrEmpty(第1次随访症状))
                {
                    string[] a = 第1次随访症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt无症状1" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (lbl == null) break;
                        if (a[i] == "99") lbl.Text = "6";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }
                this.txt半身不遂1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.目前症状其他].ToString();
                string 第1次个人病史 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.个人病史].ToString();
                if (!string.IsNullOrEmpty(第1次个人病史))
                {
                    string[] a = 第1次个人病史.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt个人病史" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (lbl == null) break;
                        lbl.Text = a[i];
                        if (a[i] == "4")
                        {
                            lbl.Text = "3";
                        }                        
                        else if(a[i]=="100"){
                            lbl.Text="";
                        }
                        else if (a[i] == "3")
                        {
                            lbl.Text = "4";
                        }
                        if (i > 2)
                        {
                            break;
                        }
                        
                    }
                }
                string 第1次并发症 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.并发症情况].ToString();
                if (!string.IsNullOrEmpty(第1次并发症))
                {
                    string[] a = 第1次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt并发症" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (lbl == null) break;
                        if (a[i] == "99") { lbl.Text = "6"; }
                        else if (a[i] == "0")
                        {
                            lbl.Text = "1";
                        }
                        else lbl.Text = (Convert.ToInt32(a[i]) - 1).ToString();
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }

                string 第1次新发卒中症状 = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.新发卒中症状].ToString();
                if (!string.IsNullOrEmpty(第1次新发卒中症状))
                {
                    string[] a = 第1次新发卒中症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) 
                            continue;
                        string strName = "txt新发卒中" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (lbl == null) break;
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        switch (a[i])
                        {
                            case "0": 
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "1":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "2":
                                lbl.Text = a[i];
                                break;
                            case "4":
                                lbl.Text = (Convert.ToInt32(a[i]) - 1).ToString();
                                break;
                            case "5":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "6":
                                lbl.Text = (Convert.ToInt32(a[i]) - 2).ToString();
                                break;
                            case "7":
                                lbl.Text = (Convert.ToInt32(a[i]) + 2).ToString();
                                break;
                            case "8":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "9":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "10":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "12":
                                lbl.Text = (Convert.ToInt32(a[i]) - 4).ToString();
                                break;
                            default :
                                lbl.Text = "11";
                                break;
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                this.txt第1次血压1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.收缩压].ToString();
                this.txt第1次血压2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.舒张压].ToString();
                this.txt第1次体重1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重].ToString();
                this.txt第1次体重2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重2].ToString();
                this.txt第1次体质指数1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI].ToString();
                this.txt第1次体质指数2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI2].ToString();
                this.txt第1次吸烟量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量].ToString();
                this.txt第1次吸烟量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量2].ToString();
                this.txt第1次饮酒量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量].ToString();
                this.txt第1次饮酒量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量2].ToString();

                this.txt身高1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.身高].ToString();
                
                this.txt腰围1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.腰围].ToString();
                this.txt空腹血糖1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.空腹血糖].ToString();
                this.txt肢体功能1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.肢体功能恢复情况].ToString();
                
                this.txt次周11.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.运动频率].ToString();
                this.txt分钟11.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.运动持续时间].ToString();
                this.txt次周21.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.运动频率2].ToString();
                this.txt分钟21.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.运动持续时间2].ToString();
                this.txt摄盐情况1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.摄盐情况].ToString();
                if (this.txt摄盐情况1.Text == "1")
                {
                    this.txt摄盐情况1.Text = "轻";
                }
                else if (this.txt摄盐情况1.Text == "2")
                {
                    this.txt摄盐情况1.Text = "中";
                }
                else if (this.txt摄盐情况1.Text == "3")
                {
                    this.txt摄盐情况1.Text = "重";
                }
                this.txt摄盐情况2.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
                if (this.txt摄盐情况2.Text == "1")
                {
                    this.txt摄盐情况2.Text = "轻";
                }
                else if (this.txt摄盐情况2.Text == "2")
                {
                    this.txt摄盐情况2.Text = "中";
                }
                else if (this.txt摄盐情况2.Text == "3")
                {
                    this.txt摄盐情况2.Text = "重";
                }
                this.txt心理调整1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt遵医行为1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt辅助检查1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt服药依从性1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt药物不良反应1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt药物不良反应有1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt随访分类1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.本次随访分类].ToString();

                this.txt转诊原因1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt机构及科别1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt医生建议1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt下次随访日期1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt随访医生签名1.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.随访医生].ToString();
            }

            #endregion

            #region 2次随访记录


            if (dt脑卒中 != null && dt脑卒中.Rows.Count > 1)
            {
                this.txt随访日期2.Text = Convert.ToDateTime(dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt随访方式2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.随访方式].ToString();
                string 第2次脑卒中分类 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.脑卒中类型].ToString();
                if (!string.IsNullOrEmpty(第2次脑卒中分类))
                {
                    string[] a = 第2次脑卒中分类.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中分类" + (i + 4);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }

                string 第2次脑卒中部位 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.脑卒中部位].ToString();
                if (!string.IsNullOrEmpty(第2次脑卒中部位))
                {
                    string[] a = 第2次脑卒中部位.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中部位" + (i + 4);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }
                
                string 第2次随访症状 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.目前症状].ToString();
                if (!string.IsNullOrEmpty(第2次随访症状))
                {
                    string[] a = 第2次随访症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt无症状2" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99") lbl.Text = "6";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }
                this.txt半身不遂2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.目前症状其他].ToString();
                string 第2次个人病史 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.个人病史].ToString();
                if (!string.IsNullOrEmpty(第2次个人病史))
                {
                    string[] a = 第2次个人病史.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt个人病史" + (i + 4);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];
                        if (a[i] == "4")
                        {
                            lbl.Text = "3";
                        }
                        else if (a[i] == "100")
                        {
                            lbl.Text = "";
                        }
                        else if (a[i] == "3")
                        {
                            lbl.Text = "4";
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                string 第2次并发症 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.并发症情况].ToString();
                if (!string.IsNullOrEmpty(第2次并发症))
                {
                    string[] a = 第2次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt并发症" + (i + 4);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "100") lbl.Text = " ";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }

                string 第2次新发卒中症状 = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.新发卒中症状].ToString();
                if (!string.IsNullOrEmpty(第2次新发卒中症状))
                {
                    string[] a = 第2次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt新发卒中" + (i + 4);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        switch (a[i])
                        {
                            case "0":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "1":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "2":
                                lbl.Text = a[i];
                                break;
                            case "4":
                                lbl.Text = (Convert.ToInt32(a[i]) - 1).ToString();
                                break;
                            case "5":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "6":
                                lbl.Text = (Convert.ToInt32(a[i]) - 2).ToString();
                                break;
                            case "7":
                                lbl.Text = (Convert.ToInt32(a[i]) + 2).ToString();
                                break;
                            case "8":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "9":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "10":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "12":
                                lbl.Text = (Convert.ToInt32(a[i]) - 4).ToString();
                                break;
                            default:
                                lbl.Text = "11";
                                break;
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                this.txt第2次血压1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.收缩压].ToString();
                this.txt第2次血压2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.舒张压].ToString();
                this.txt第2次体重1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重].ToString();
                this.txt第2次体重2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重2].ToString();
                this.txt第2次体质指数1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI].ToString();
                this.txt第2次体质指数2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI2].ToString();
                this.txt第2次吸烟量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量].ToString();
                this.txt第2次吸烟量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量2].ToString();
                this.txt第2次饮酒量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量].ToString();
                this.txt第2次饮酒量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量2].ToString();

                this.txt身高2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.身高].ToString();

                this.txt腰围2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.腰围].ToString();
                this.txt空腹血糖2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.空腹血糖].ToString();
                this.txt肢体功能2.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.肢体功能恢复情况].ToString();

                this.txt次周12.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.运动频率].ToString();
                this.txt分钟12.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.运动持续时间].ToString();
                this.txt次周22.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.运动频率2].ToString();
                this.txt分钟22.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.运动持续时间2].ToString();
                this.txt摄盐情况3.Text = dt脑卒中.Rows[1][Models.tb_MXB脑卒中随访表.摄盐情况].ToString();
                if (this.txt摄盐情况3.Text == "1")
                {
                    this.txt摄盐情况3.Text = "轻";
                }
                else if (this.txt摄盐情况3.Text == "2")
                {
                    this.txt摄盐情况3.Text = "中";
                }
                else if (this.txt摄盐情况3.Text == "3")
                {
                    this.txt摄盐情况3.Text = "重";
                }
                this.txt摄盐情况4.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
                if (this.txt摄盐情况4.Text == "1")
                {
                    this.txt摄盐情况4.Text = "轻";
                }
                else if (this.txt摄盐情况4.Text == "2")
                {
                    this.txt摄盐情况4.Text = "中";
                }
                else if (this.txt摄盐情况4.Text == "3")
                {
                    this.txt摄盐情况4.Text = "重";
                }
                this.txt心理调整2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt遵医行为2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt辅助检查2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt服药依从性2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt药物不良反应2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt药物不良反应有2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt随访分类2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.本次随访分类].ToString();

                this.txt转诊原因2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt机构及科别2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt医生建议2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt下次随访日期2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt随访医生签名2.Text = dt脑卒中.Rows[1][Models.tb_MXB高血压随访表.随访医生].ToString();
            }
            #endregion

            #region 3次随访记录

            if (dt脑卒中 != null && dt脑卒中.Rows.Count > 2)
            {
                this.txt随访日期3.Text = Convert.ToDateTime(dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt随访方式3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.随访方式].ToString();
                string 第3次脑卒中分类 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.脑卒中类型].ToString();
                if (!string.IsNullOrEmpty(第3次脑卒中分类))
                {
                    string[] a = 第3次脑卒中分类.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中分类" + (i + 7);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }

                string 第3次脑卒中部位 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.脑卒中部位].ToString();
                if (!string.IsNullOrEmpty(第3次脑卒中部位))
                {
                    string[] a = 第3次脑卒中部位.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中部位" + (i + 7);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }
                
                string 第3次随访症状 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.目前症状].ToString();
                if (!string.IsNullOrEmpty(第3次随访症状))
                {
                    string[] a = 第3次随访症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt无症状3" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99") lbl.Text = "6";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }
                this.txt半身不遂3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.目前症状其他].ToString();
                string 第3次个人病史 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.个人病史].ToString();
                if (!string.IsNullOrEmpty(第3次个人病史))
                {
                    string[] a = 第3次个人病史.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt个人病史" + (i + 7);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];
                        if (a[i] == "4")
                        {
                            lbl.Text = "3";
                        }
                        else if (a[i] == "100")
                        {
                            lbl.Text = "";
                        }
                        else if (a[i] == "3")
                        {
                            lbl.Text = "4";
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                string 第3次并发症 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.并发症情况].ToString();
                if (!string.IsNullOrEmpty(第3次并发症))
                {
                    string[] a = 第3次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt并发症" + (i + 7);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "100") lbl.Text = " ";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }

                string 第3次新发卒中症状 = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.新发卒中症状].ToString();
                if (!string.IsNullOrEmpty(第3次新发卒中症状))
                {
                    string[] a = 第3次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt新发卒中" + (i + 7);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        switch (a[i])
                        {
                            case "0":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "1":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "2":
                                lbl.Text = a[i];
                                break;
                            case "4":
                                lbl.Text = (Convert.ToInt32(a[i]) - 1).ToString();
                                break;
                            case "5":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "6":
                                lbl.Text = (Convert.ToInt32(a[i]) - 2).ToString();
                                break;
                            case "7":
                                lbl.Text = (Convert.ToInt32(a[i]) + 2).ToString();
                                break;
                            case "8":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "9":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "10":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "12":
                                lbl.Text = (Convert.ToInt32(a[i]) - 4).ToString();
                                break;
                            default:
                                lbl.Text = "11";
                                break;
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                this.txt第3次血压1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.收缩压].ToString();
                this.txt第3次血压2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.舒张压].ToString();
                this.txt第3次体重1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重].ToString();
                this.txt第3次体重2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重2].ToString();
                this.txt第3次体质指数1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI].ToString();
                this.txt第3次体质指数2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI2].ToString();
                this.txt第3次吸烟量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量].ToString();
                this.txt第3次吸烟量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量2].ToString();
                this.txt第3次饮酒量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量].ToString();
                this.txt第3次饮酒量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量2].ToString();

                this.txt身高3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.身高].ToString();

                this.txt腰围3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.腰围].ToString();
                this.txt空腹血糖3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.空腹血糖].ToString();
                this.txt肢体功能3.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.肢体功能恢复情况].ToString();

                this.txt次周13.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.运动频率].ToString();
                this.txt分钟13.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.运动持续时间].ToString();
                this.txt次周23.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.运动频率2].ToString();
                this.txt分钟23.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.运动持续时间2].ToString();
                this.txt摄盐情况5.Text = dt脑卒中.Rows[2][Models.tb_MXB脑卒中随访表.摄盐情况].ToString();
                if (this.txt摄盐情况5.Text == "1")
                {
                    this.txt摄盐情况5.Text = "轻";
                }
                else if (this.txt摄盐情况5.Text == "2")
                {
                    this.txt摄盐情况5.Text = "中";
                }
                else if (this.txt摄盐情况5.Text == "3")
                {
                    this.txt摄盐情况5.Text = "重";
                }
                this.txt摄盐情况6.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
                if (this.txt摄盐情况6.Text == "1")
                {
                    this.txt摄盐情况6.Text = "轻";
                }
                else if (this.txt摄盐情况6.Text == "2")
                {
                    this.txt摄盐情况6.Text = "中";
                }
                else if (this.txt摄盐情况6.Text == "3")
                {
                    this.txt摄盐情况6.Text = "重";
                }
                this.txt心理调整3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt遵医行为3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt辅助检查3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt服药依从性3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt药物不良反应3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt药物不良反应有3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt随访分类3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.本次随访分类].ToString();

                this.txt转诊原因3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt机构及科别3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt医生建议3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt下次随访日期3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt随访医生签名3.Text = dt脑卒中.Rows[2][Models.tb_MXB高血压随访表.随访医生].ToString();
            }

            #endregion

            #region 4次随访记录

           if (dt脑卒中 != null && dt脑卒中.Rows.Count > 3)
            {
                this.txt随访日期4.Text = Convert.ToDateTime(dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt随访方式4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.随访方式].ToString();
                string 第4次脑卒中分类 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.脑卒中类型].ToString();
                if (!string.IsNullOrEmpty(第4次脑卒中分类))
                {
                    string[] a = 第4次脑卒中分类.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中分类" + (i + 10);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }

                string 第4次脑卒中部位 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.脑卒中部位].ToString();
                if (!string.IsNullOrEmpty(第4次脑卒中部位))
                {
                    string[] a = 第4次脑卒中部位.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt脑卒中部位" + (i + 10);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        lbl.Text = a[i];
                    }
                }
                
                string 第4次随访症状 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.目前症状].ToString();
                if (!string.IsNullOrEmpty(第4次随访症状))
                {
                    string[] a = 第4次随访症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt无症状4" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99") lbl.Text = "6";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }
                this.txt半身不遂4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.目前症状其他].ToString();
                string 第4次个人病史 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.个人病史].ToString();
                if (!string.IsNullOrEmpty(第4次个人病史))
                {
                    string[] a = 第4次个人病史.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt个人病史" + (i + 10);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];
                        if (a[i] == "4")
                        {
                            lbl.Text = "3";
                        }
                        else if (a[i] == "100")
                        {
                            lbl.Text = "";
                        }
                        else if (a[i] == "3")
                        {
                            lbl.Text = "4";
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                string 第4次并发症 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.并发症情况].ToString();
                if (!string.IsNullOrEmpty(第4次并发症))
                {
                    string[] a = 第4次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt并发症" + (i + 10);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "100") lbl.Text = " ";
                        else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                    }
                }

                string 第4次新发卒中症状 = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.新发卒中症状].ToString();
                if (!string.IsNullOrEmpty(第4次新发卒中症状))
                {
                    string[] a = 第4次并发症.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt新发卒中" + (i + 10);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        //if (a[i] == "100") lbl.Text = " ";
                        //else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                        switch (a[i])
                        {
                            case "0":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "1":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "2":
                                lbl.Text = a[i];
                                break;
                            case "4":
                                lbl.Text = (Convert.ToInt32(a[i]) - 1).ToString();
                                break;
                            case "5":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "6":
                                lbl.Text = (Convert.ToInt32(a[i]) - 2).ToString();
                                break;
                            case "7":
                                lbl.Text = (Convert.ToInt32(a[i]) + 2).ToString();
                                break;
                            case "8":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "9":
                                lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                                break;
                            case "10":
                                lbl.Text = (Convert.ToInt32(a[i]) - 3).ToString();
                                break;
                            case "12":
                                lbl.Text = (Convert.ToInt32(a[i]) - 4).ToString();
                                break;
                            default:
                                lbl.Text = "11";
                                break;
                        }
                        if (i > 2)
                        {
                            break;
                        }
                    }
                }
                this.txt第4次血压1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.收缩压].ToString();
                this.txt第4次血压2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.舒张压].ToString();
                this.txt第4次体重1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重].ToString();
                this.txt第4次体重2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.体重2].ToString();
                this.txt第4次体质指数1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI].ToString();
                this.txt第4次体质指数2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.D_BMI2].ToString();
                this.txt第4次吸烟量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量].ToString();
                this.txt第4次吸烟量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.吸烟数量2].ToString();
                this.txt第4次饮酒量1.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量].ToString();
                this.txt第4次饮酒量2.Text = dt脑卒中.Rows[0][Models.tb_MXB脑卒中随访表.饮酒数量2].ToString(); 

                this.txt身高4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.身高].ToString();

                this.txt腰围4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.腰围].ToString();
                this.txt空腹血糖4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.空腹血糖].ToString();
                this.txt肢体功能4.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.肢体功能恢复情况].ToString();

                this.txt次周14.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.运动频率].ToString();
                this.txt分钟14.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.运动持续时间].ToString();
                this.txt次周24.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.运动频率2].ToString();
                this.txt分钟24.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.运动持续时间2].ToString();
                this.txt摄盐情况7.Text = dt脑卒中.Rows[3][Models.tb_MXB脑卒中随访表.摄盐情况].ToString();
                if (this.txt摄盐情况7.Text == "1")
                {
                    this.txt摄盐情况7.Text = "轻";
                }
                else if (this.txt摄盐情况7.Text == "2")
                {
                    this.txt摄盐情况7.Text = "中";
                }
                else if (this.txt摄盐情况7.Text == "3")
                {
                    this.txt摄盐情况7.Text = "重";
                }
                this.txt摄盐情况8.Text = dt脑卒中.Rows[0][Models.tb_MXB高血压随访表.摄盐情况2].ToString();
                if (this.txt摄盐情况8.Text == "1")
                {
                    this.txt摄盐情况8.Text = "轻";
                }
                else if (this.txt摄盐情况8.Text == "2")
                {
                    this.txt摄盐情况8.Text = "中";
                }
                else if (this.txt摄盐情况8.Text == "3")
                {
                    this.txt摄盐情况8.Text = "重";
                }
                this.txt心理调整4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.心理调整].ToString();
                this.txt遵医行为4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.遵医行为].ToString();
                this.txt辅助检查4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.辅助检查].ToString();
                this.txt服药依从性4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.服药依从性].ToString();
                this.txt药物不良反应4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.药物副作用].ToString();
                this.txt药物不良反应有4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.副作用详述].ToString();
                this.txt随访分类4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.本次随访分类].ToString();

                this.txt转诊原因4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.转诊原因].ToString();
                this.txt机构及科别4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt医生建议4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.转诊科别].ToString();
                this.txt下次随访日期4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.下次随访时间].ToString();
                this.txt随访医生签名4.Text = dt脑卒中.Rows[3][Models.tb_MXB高血压随访表.随访医生].ToString();
            }

            #endregion
            }

        private void BindYYQK4()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    
                    string ctr药物 = "txt其他药物" + (i + 1);
                    string ctr用法 = "txt第4次用法用量" + (i + 1);
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt其他药物1" && ctr用法 == "txt第4次用法用量1")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt其他药物2" && ctr用法 == "txt第4次用法用量2")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt其他药物3" && ctr用法 == "txt第4次用法用量3")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                }
            }
        }

        private void BindYYQK3()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }

                    string ctr药物 = "txt第3次药物名称" + (i + 1);
                    string ctr用法 = "txt第3次用法用量" + (i + 1);
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第3次药物名称1" && ctr用法 == "txt第3次用法用量1")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次药物名称2" && ctr用法 == "txt第3次用法用量2")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第3次药物名称3" && ctr用法 == "txt第3次用法用量3")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                }
            }
        }

        private void BindYYQK2()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }

                    string ctr药物 = "txt第2次药物名称" + (i + 1);
                    string ctr用法 = "txt第2次用法用量" + (i + 1);
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第2次药物名称1" && ctr用法 == "txt第2次用法用量1")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次药物名称2" && ctr用法 == "txt第2次用法用量2")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第2次药物名称3" && ctr用法 == "txt第2次用法用量3")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                }
            }
        }

        private void BindYYQK1()
        {
            if (_ds用药情况.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                {
                    //最多显示3行，超过3行以后不显示
                    if (i > 2)
                    {
                        break;
                    }
                    
                    string ctr药物 = "txt第1次药物名称"+ (i + 1);
                    string ctr用法 = "txt第1次用法用量"+ (i + 1);
                    XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                    XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                    if (ctr药物 == "txt第1次药物名称1" && ctr用法 == "txt第1次用法用量1")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[0][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次药物名称2" && ctr用法 == "txt第1次用法用量2")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[1][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                    else if (ctr药物 == "txt第1次药物名称3" && ctr用法 == "txt第1次用法用量3")
                    {
                        lbl药物.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[2][tb_MXB脑卒中随访表_用药情况.用法].ToString();
                    }
                }
            }
        }

        
    }
}
