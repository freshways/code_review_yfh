﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC冠心病患者管理卡 : UserControlBase
    {
        DataRow[] _dr个人信息 = null;
        public UC冠心病患者管理卡()
        {
            InitializeComponent();
        }

        public UC冠心病患者管理卡(DataRow[] dr, UpdateType _UpdateType)
        {            
            base._UpdateType = _UpdateType;
            _dr个人信息 = dr;
            _BLL = new bllMXB冠心病管理卡();
            InitializeComponent();

            //默认绑定
            txt个人档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.居住地址].ToString();
            this.txt职业.Text = dr[0][tb_健康档案.职业].ToString();
            //this.lab当前所属机构.Text = dr[0][tb_健康档案.所属机构].ToString();            
        }
        
        #region Handler Events
        private void UC冠心病患者管理卡_Load(object sender, EventArgs e)
        {
            this.Init(); // 初始化
            DataTable dt = _BLL.GetBusinessByKey(txt个人档案编号.Text, true).Tables[tb_MXB冠心病管理卡.__TableName];

            if (dt != null && dt.Rows.Count > 0)
            {
                DoBindingSummaryEditor(dt);
            }

            //绑定最新体检数据
            if (_dr个人信息 != null)
            {
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.身高] = _dr个人信息[0][tb_健康体检.身高].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.体重] = _dr个人信息[0][tb_健康体检.体重].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.BMI] = _dr个人信息[0][tb_健康体检.体重指数].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.腰围] = _dr个人信息[0][tb_健康体检.腰围].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.收缩压] = _dr个人信息[0][tb_健康体检.血压左侧1].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.舒张压] = _dr个人信息[0][tb_健康体检.血压左侧2].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.空腹血糖] = _dr个人信息[0][tb_健康体检.空腹血糖].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.高密度蛋白] = _dr个人信息[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.低密度蛋白] = _dr个人信息[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.甘油三酯] = _dr个人信息[0][tb_健康体检.甘油三酯].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.胆固醇] = _dr个人信息[0][tb_健康体检.总胆固醇].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.体检日期] = _dr个人信息[0][tb_健康体检.体检日期].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.吸烟情况] = _dr个人信息[0][tb_健康体检.吸烟状况].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.饮酒情况] = _dr个人信息[0][tb_健康体检.饮酒频率].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.运动频率] = _dr个人信息[0][tb_健康体检.锻炼频率].ToString();
                _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.生活自理能力] = _dr个人信息[0][tb_健康体检.老年人自理评估].ToString();
            }

            gcDetail.DataSource = _BLL.CurrentBusiness.Tables[tb_MXB冠心病管理卡_用药情况.__TableName];
            //设置颜色            
            Set考核项颜色_new(this.layoutControl1, lab考核项);
        }

        #region Init初始化
        void Init()
        {
            DataBinder.BindingLookupEditDataSource(txt服药依从性, _BLL.ConvertRowsToTable(DataDictCache.Cache.t常用字典.Select("P_FUN_CODE='fyycx-mb' ")), "P_DESC", "P_CODE");

            btn添加药物.Click += btn添加药物_Click;
            btn删除药物.Click += btn删除药物_Click;

            this.txt身高.Txt1.Leave += 身高_Leave;
            this.txt体重.Txt1.Leave += 身高_Leave;
            this.txtBMI.Txt1.Enter += BMI_Enter;
        }

        void 身高_Leave(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txtBMI.Txt1);
        }

        void BMI_Enter(object sender, EventArgs e)
        {
            ComputeBMI(this.txt身高.Txt1.Text, this.txt体重.Txt1.Text, txtBMI.Txt1);
        } 
        #endregion

        #region 修改保存
        private void btn修改_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (_UpdateType == UpdateType.None) return;

            Set考核项颜色_new(this.layoutControl1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.家族史] = GetFlowLayoutResult(this.fl家族史);
            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.目前症状] = GetFlowLayoutResult(this.fl症状);
            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.个人病史] = GetFlowLayoutResult(this.fl病史);
            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.特殊治疗] = GetFlowLayoutResult(this.fl特殊治疗);
            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.非药物治疗措施] = GetFlowLayoutResult(this.fl非药物治疗);

            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_MXB冠心病管理卡.完整度] = _base完整度;
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] = _base缺项 + "," + _base完整度;
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否冠心病] = "1";
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0].AcceptChanges();
            _BLL.CurrentBusiness.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0].SetModified();

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人
                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                this._UpdateType = UpdateType.None;
                Msg.ShowInformation("保存成功!");
                //保存成功后跳转到显示页面
                UC冠心病患者管理卡_显示 uc = new UC冠心病患者管理卡_显示(_dr个人信息);
                ShowControl(uc);
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt管理卡编号.Text)))
            //{
            //    Msg.Warning("管理卡编号不能为空!");
            //    txt管理卡编号.Focus();
            //    return false;
            //}
            return true;
        } 
        #endregion

        #endregion

        #region Private Method
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;
            
            //TextEdit
            DataBinder.BindingTextEdit(txt管理卡编号, dataSource, tb_MXB冠心病管理卡.管理卡编号);
            DataBinder.BindingTextEdit(txt确诊日期, dataSource, tb_MXB冠心病管理卡.确诊日期);
            DataBinder.BindingTextEdit(txt确诊机构, dataSource, tb_MXB冠心病管理卡.确诊医院);
            DataBinder.BindingTextEdit(txt身高.Txt1, dataSource, tb_MXB冠心病管理卡.身高);
            DataBinder.BindingTextEdit(txt体重.Txt1, dataSource, tb_MXB冠心病管理卡.体重);
            DataBinder.BindingTextEdit(txtBMI.Txt1, dataSource, tb_MXB冠心病管理卡.BMI);
            DataBinder.BindingTextEdit(txt心率.Txt1, dataSource, tb_MXB冠心病管理卡.心率);
            DataBinder.BindingTextEdit(txt血压值.Txt1, dataSource, tb_MXB冠心病管理卡.收缩压);
            DataBinder.BindingTextEdit(txt血压值.Txt2, dataSource, tb_MXB冠心病管理卡.舒张压);
            DataBinder.BindingTextEdit(txt空腹血糖.Txt1, dataSource, tb_MXB冠心病管理卡.空腹血糖);
            DataBinder.BindingTextEdit(txt高密度蛋白.Txt1, dataSource, tb_MXB冠心病管理卡.高密度蛋白);
            DataBinder.BindingTextEdit(txt低密度蛋白.Txt1, dataSource, tb_MXB冠心病管理卡.低密度蛋白);
            DataBinder.BindingTextEdit(txt甘油三酯.Txt1, dataSource, tb_MXB冠心病管理卡.甘油三酯);
            DataBinder.BindingTextEdit(txt胆固醇.Txt1, dataSource, tb_MXB冠心病管理卡.胆固醇);
            DataBinder.BindingTextEdit(txt腰围.Txt1, dataSource, tb_MXB冠心病管理卡.腰围);
            DataBinder.BindingTextEditDateTime(txt体检日期, dataSource, tb_MXB冠心病管理卡.体检日期);
            DataBinder.BindingTextEdit(txt心电检查, dataSource, tb_MXB冠心病管理卡.心电图检查);
            DataBinder.BindingTextEdit(txt心电运动, dataSource, tb_MXB冠心病管理卡.心电图运动负荷);
            DataBinder.BindingTextEdit(txt心脏彩超, dataSource, tb_MXB冠心病管理卡.心脏彩超检查);
            DataBinder.BindingTextEdit(txt冠状动脉, dataSource, tb_MXB冠心病管理卡.冠状动脉造影);
            DataBinder.BindingTextEdit(txt心肌酶学, dataSource, tb_MXB冠心病管理卡.心肌酶学检查);
            DataBinder.BindingTextEdit(txt服药依从性, dataSource, tb_MXB冠心病管理卡.服药依从性);
            DataBinder.BindingTextEditDateTime(txt终止管理日期, dataSource, tb_MXB冠心病管理卡.终止管理日期);
            DataBinder.BindingTextEditDateTime(txt发生时间, dataSource, tb_MXB冠心病管理卡.发生时间);

            //RadioEdit
            DataBinder.BindingRadioEdit(radio管理组别, dataSource, tb_MXB冠心病管理卡.管理组别);
            DataBinder.BindingRadioEdit(radio病例来源, dataSource, tb_MXB冠心病管理卡.病例来源);
            DataBinder.BindingRadioEdit(radio冠心病类型, dataSource, tb_MXB冠心病管理卡.冠心病类型);
            DataBinder.BindingRadioEdit(radio吸烟情况, dataSource, tb_MXB冠心病管理卡.吸烟情况);
            DataBinder.BindingRadioEdit(radio饮酒情况, dataSource, tb_MXB冠心病管理卡.饮酒情况);
            DataBinder.BindingRadioEdit(radio体育锻炼, dataSource, tb_MXB冠心病管理卡.运动频率);
            DataBinder.BindingRadioEdit(radio生活自理能力, dataSource, tb_MXB冠心病管理卡.生活自理能力);
            DataBinder.BindingRadioEdit(radio用药情况, dataSource, tb_MXB冠心病管理卡.G_JYY);
            DataBinder.BindingRadioEdit(radio终止管理, dataSource, tb_MXB冠心病管理卡.终止管理);
            DataBinder.BindingRadioEdit(radio终止理由, dataSource, tb_MXB冠心病管理卡.终止理由);

            //flowLayoutPanel2
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB冠心病管理卡.家族史].ToString(), fl家族史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB冠心病管理卡.目前症状].ToString(), fl症状);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB冠心病管理卡.个人病史].ToString(), fl病史);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB冠心病管理卡.特殊治疗].ToString(), fl特殊治疗);
            SetFlowLayoutResult(dataSource.Rows[0][tb_MXB冠心病管理卡.非药物治疗措施].ToString(), fl非药物治疗);
            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB冠心病管理卡.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB冠心病管理卡.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB冠心病管理卡.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_MXB冠心病管理卡.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB冠心病管理卡.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_MXB冠心病管理卡.修改时间].ToString();

        }

        #endregion

        #region 使用药物-子表

        private void btn添加药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn添加药物.Tag, gcDetail);
        }

        private void btn删除药物_Click(object sender, EventArgs e)
        {
            OnEmbeddedNavigatorButtonClick(btn删除药物.Tag, gcDetail);
        }

        protected override void CreateOneDetail(GridView gridView)
        {
            gvDetail.MoveLast();

            DataRow row = _BLL.CurrentBusiness.Tables[tb_MXB冠心病管理卡_用药情况.__TableName].NewRow();
            //添加用法说明默认值
            row[tb_MXB冠心病管理卡_用药情况.用法] = "每日  次，每次  mg";
            row[tb_MXB冠心病管理卡_用药情况.个人档案编号] = this.txt个人档案编号.Text;
            row[tb_MXB冠心病管理卡_用药情况.创建时间] = lab创建时间.Text;

            _BLL.CurrentBusiness.Tables[tb_MXB冠心病管理卡_用药情况.__TableName].Rows.Add(row); //增加一条明细记录

            gcDetail.RefreshDataSource();
            gvDetail.FocusedRowHandle = gvDetail.RowCount - 1;

            gvDetail.FocusedColumn = gvDetail.VisibleColumns[0];
        }
        #endregion

        #region 页面显示控制

        private void radio用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.radio用药情况.EditValue!=null&&this.radio用药情况.EditValue.ToString() == "1")
            {
                layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                if (_BLL.CurrentBusiness.Tables[tb_MXB冠心病管理卡_用药情况.__TableName].Rows.Count <= 0)
                    btn添加药物.PerformClick();
            }
            else
            {
                layout用药列表.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout添加药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layout删除药物.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

        }

        private void radio终止管理_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.radio终止管理.EditValue!=null && this.radio终止管理.EditValue.ToString() == "1")
            {
                layoutControlItem终止日期.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem终止理由.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                layoutControlItem终止日期.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem终止理由.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }
        
        private void jzs_ck以上都无_CheckedChanged(object sender, EventArgs e)
        {
            string 点击名称 = ((DevExpress.XtraEditors.CheckEdit)sender).Text;

            #region 以上都无
            if (jzs_ck以上都无.Text == 点击名称)
            {
                if (jzs_ck以上都无.Checked)
                {
                    jzs_ck高血压.Checked = false;
                    jzs_ck高血压.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;
                    return;
                }
                else
                {
                    jzs_ck高血压.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    return;
                }
            }
            #endregion

            #region 不详
            if (jzs_ck不详.Text == 点击名称)
            {
                if (jzs_ck不详.Checked)
                {
                    jzs_ck高血压.Checked = false;
                    jzs_ck高血压.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;

                    jzs_ck以上都无.CheckedChanged -= jzs_ck以上都无_CheckedChanged;
                    jzs_ck以上都无.Checked = false;
                    jzs_ck以上都无.Enabled = false;
                    jzs_ck以上都无.CheckedChanged += jzs_ck以上都无_CheckedChanged;
                    return;
                }
                else
                {
                    jzs_ck高血压.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    jzs_ck以上都无.Enabled = true;
                    return;
                }
            }
            #endregion

            #region 拒答

            if (jzs_ck拒答.Text == 点击名称)
            {
                if (jzs_ck拒答.Checked)
                {
                    jzs_ck高血压.Checked = false;
                    jzs_ck高血压.Enabled = false;

                    jzs_ck冠心病.Checked = false;
                    jzs_ck冠心病.Enabled = false;

                    jzs_ck糖尿病.Checked = false;
                    jzs_ck糖尿病.Enabled = false;

                    jzs_ck脑卒中.Checked = false;
                    jzs_ck脑卒中.Enabled = false;

                    jzs_ck以上都无.CheckedChanged -= jzs_ck以上都无_CheckedChanged;
                    jzs_ck以上都无.Checked = false;
                    jzs_ck以上都无.Enabled = false;
                    jzs_ck以上都无.CheckedChanged += jzs_ck以上都无_CheckedChanged;

                    jzs_ck不详.CheckedChanged -= jzs_ck以上都无_CheckedChanged;
                    jzs_ck不详.Checked = false;
                    jzs_ck不详.Enabled = false;
                    jzs_ck不详.CheckedChanged += jzs_ck以上都无_CheckedChanged;
                    return;
                }
                else
                {
                    jzs_ck高血压.Enabled = true;
                    jzs_ck冠心病.Enabled = true;
                    jzs_ck糖尿病.Enabled = true;
                    jzs_ck脑卒中.Enabled = true;
                    jzs_ck以上都无.Enabled = true;
                    jzs_ck不详.Enabled = true;
                    return;
                }
            }
            #endregion

        }

        private void ck无症状_CheckedChanged(object sender, EventArgs e)
        {
            if (ck无症状.Checked)
                SetFlCheckEnabled(fl症状, false, ck无症状.Text);
            else
                SetFlCheckEnabled(fl症状, true, ck无症状.Text);
        }

        private void ck无以上病史_CheckedChanged(object sender, EventArgs e)
        {
            if (ck无以上病史.Checked)
                SetFlCheckEnabled(fl病史, false, ck无以上病史.Text);
            else
                SetFlCheckEnabled(fl病史, true, ck无以上病史.Text);
        }

        private void ch特殊治疗无_CheckedChanged(object sender, EventArgs e)
        {
            if (ch特殊治疗无.Checked)
                SetFlCheckEnabled(fl特殊治疗, false, ch特殊治疗无.Text);
            else
                SetFlCheckEnabled(fl特殊治疗, true, ch特殊治疗无.Text);
        }

        private void ck非药物未采取措施_CheckedChanged(object sender, EventArgs e)
        {
            if (ck非药物未采取措施.Checked)
                SetFlCheckEnabled(fl非药物治疗, false, ck非药物未采取措施.Text);
            else
                SetFlCheckEnabled(fl非药物治疗, true, ck非药物未采取措施.Text);
        }

        private void SetFlCheckEnabled(FlowLayoutPanel flw, bool value, string checkTxt)
        {
            for (int j = 0; j < flw.Controls.Count; j++)
            {
                if (flw.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flw.Controls[j];
                    if (chk.Text != checkTxt)
                        chk.Enabled = value;
                }
            }
        }

        #endregion       

    }
}
