﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class frm脑卒中随访日期
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gc脑卒中随访 = new DevExpress.XtraGrid.GridControl();
            this.gv脑卒中随访 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col随访日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn确定 = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gc脑卒中随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv脑卒中随访)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(1, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(331, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "请选择需要导出的随访记录日期（最多只能选择4次随访记录）";
            // 
            // gc脑卒中随访
            // 
            this.gc脑卒中随访.Location = new System.Drawing.Point(12, 32);
            this.gc脑卒中随访.MainView = this.gv脑卒中随访;
            this.gc脑卒中随访.Name = "gc脑卒中随访";
            this.gc脑卒中随访.Size = new System.Drawing.Size(363, 251);
            this.gc脑卒中随访.TabIndex = 1;
            this.gc脑卒中随访.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv脑卒中随访});
            // 
            // gv脑卒中随访
            // 
            this.gv脑卒中随访.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col随访日期});
            this.gv脑卒中随访.GridControl = this.gc脑卒中随访;
            this.gv脑卒中随访.Name = "gv脑卒中随访";
            this.gv脑卒中随访.OptionsSelection.MultiSelect = true;
            this.gv脑卒中随访.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv脑卒中随访.OptionsView.ShowGroupPanel = false;
            // 
            // col随访日期
            // 
            this.col随访日期.Caption = "随访日期";
            this.col随访日期.FieldName = "col随访日期";
            this.col随访日期.Name = "col随访日期";
            this.col随访日期.Visible = true;
            this.col随访日期.VisibleIndex = 1;
            // 
            // btn确定
            // 
            this.btn确定.Location = new System.Drawing.Point(44, 306);
            this.btn确定.Name = "btn确定";
            this.btn确定.Size = new System.Drawing.Size(75, 23);
            this.btn确定.TabIndex = 2;
            this.btn确定.Text = "确定";
            this.btn确定.Click += new System.EventHandler(this.btn确定_Click);
            // 
            // btn取消
            // 
            this.btn取消.Location = new System.Drawing.Point(253, 306);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(75, 23);
            this.btn取消.TabIndex = 3;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // frm脑卒中随访日期
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 345);
            this.Controls.Add(this.btn取消);
            this.Controls.Add(this.btn确定);
            this.Controls.Add(this.gc脑卒中随访);
            this.Controls.Add(this.labelControl1);
            this.Name = "frm脑卒中随访日期";
            this.Text = "frm脑卒中随访日期";
            ((System.ComponentModel.ISupportInitialize)(this.gc脑卒中随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv脑卒中随访)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gc脑卒中随访;
        private DevExpress.XtraGrid.Views.Grid.GridView gv脑卒中随访;
        private DevExpress.XtraGrid.Columns.GridColumn col随访日期;
        private DevExpress.XtraEditors.SimpleButton btn确定;
        private DevExpress.XtraEditors.SimpleButton btn取消;
    }
}