﻿namespace AtomEHR.公共卫生.Module.个人健康.肺结核
{
    partial class UC肺结核后续随访_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC肺结核后续随访_显示));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn新增 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt并发症有 = new DevExpress.XtraEditors.TextEdit();
            this.txt药物不良反应有 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.pe评估医生签名 = new DevExpress.XtraEditors.PictureEdit();
            this.pe患者签名 = new DevExpress.XtraEditors.PictureEdit();
            this.pe随访医生签名 = new DevExpress.XtraEditors.PictureEdit();
            this.rg用药用法 = new DevExpress.XtraEditors.RadioGroup();
            this.txt症状及体征其他 = new DevExpress.XtraEditors.MemoEdit();
            this.txt化疗方案 = new DevExpress.XtraEditors.TextEdit();
            this.rg督导人员 = new DevExpress.XtraEditors.RadioGroup();
            this.flp治疗月序 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txt治疗月序 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt编号 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt应访视次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txt实际方式次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txt应服药次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txt实际服药次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txt服药率 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.chk1完成诊疗 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2死亡 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3丢失 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4转入耐药治疗 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.dte停止治疗时间 = new DevExpress.XtraEditors.DateEdit();
            this.dte下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt处理意见 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊2周内随访 = new DevExpress.XtraEditors.MemoEdit();
            this.txt转诊机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.rg并发症 = new DevExpress.XtraEditors.RadioGroup();
            this.rg药物不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.flp漏服药次数 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt漏服药次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.flp药品剂型 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk1固定剂 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2散装药 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3板式药 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4注射剂 = new DevExpress.XtraEditors.CheckEdit();
            this.flp饮酒 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt饮酒1 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txt饮酒2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.flp吸烟 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt吸烟1 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txt吸烟2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.flp症状体征 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk0没有症状 = new DevExpress.XtraEditors.CheckEdit();
            this.chk1咳嗽咳痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2低热盗汗 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3咯血血痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4胸痛消瘦 = new DevExpress.XtraEditors.CheckEdit();
            this.chk5恶心纳差 = new DevExpress.XtraEditors.CheckEdit();
            this.chk6关节疼痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk7头痛失眠 = new DevExpress.XtraEditors.CheckEdit();
            this.chk8视物模糊 = new DevExpress.XtraEditors.CheckEdit();
            this.chk9皮痒皮疹 = new DevExpress.XtraEditors.CheckEdit();
            this.chk10耳鸣 = new DevExpress.XtraEditors.CheckEdit();
            this.rg随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.dte随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcl随访时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl随访方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl症状体征 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl吸烟 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl药品剂型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl漏服药次数 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl药物不良反应 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl并发症 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl原因 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl转诊机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl症状2周随访 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl处理意见 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl下次随访时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl饮酒 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl治疗月序 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl督导人员 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl化疗方案 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl用药用法 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl患者签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症有.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物不良反应有.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe评估医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe患者签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe随访医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg用药用法.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状及体征其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化疗方案.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg督导人员.Properties)).BeginInit();
            this.flp治疗月序.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt治疗月序.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编号.Properties)).BeginInit();
            this.flowLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt应访视次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实际方式次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt应服药次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实际服药次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药率.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1完成诊疗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2死亡.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3丢失.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4转入耐药治疗.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte停止治疗时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte停止治疗时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处理意见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊2周内随访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg药物不良反应.Properties)).BeginInit();
            this.flp漏服药次数.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt漏服药次数.Properties)).BeginInit();
            this.flp药品剂型.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1固定剂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2散装药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3板式药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4注射剂.Properties)).BeginInit();
            this.flp饮酒.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒2.Properties)).BeginInit();
            this.flp吸烟.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟2.Properties)).BeginInit();
            this.flp症状体征.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk0没有症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1咳嗽咳痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2低热盗汗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3咯血血痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4胸痛消瘦.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5恶心纳差.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6关节疼痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7头痛失眠.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8视物模糊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9皮痒皮疹.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10耳鸣.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状体征)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl吸烟)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药品剂型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl漏服药次数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药物不良反应)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl并发症)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl原因)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl转诊机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状2周随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl处理意见)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl下次随访时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl饮酒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl治疗月序)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl督导人员)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl化疗方案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl用药用法)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(100, 527);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFocusForSave.Location = new System.Drawing.Point(100, 0);
            this.txtFocusForSave.Size = new System.Drawing.Size(661, 14);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn新增);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(100, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(661, 28);
            this.flowLayoutPanel1.TabIndex = 125;
            // 
            // btn新增
            // 
            this.btn新增.Image = ((System.Drawing.Image)(resources.GetObject("btn新增.Image")));
            this.btn新增.Location = new System.Drawing.Point(3, 3);
            this.btn新增.Name = "btn新增";
            this.btn新增.Size = new System.Drawing.Size(75, 23);
            this.btn新增.TabIndex = 15;
            this.btn新增.Text = "添加";
            this.btn新增.Click += new System.EventHandler(this.btn新增_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 13;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 14;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 16;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt并发症有);
            this.layoutControl1.Controls.Add(this.txt药物不良反应有);
            this.layoutControl1.Controls.Add(this.textEdit修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit更新时间);
            this.layoutControl1.Controls.Add(this.textEdit所属机构);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.pe评估医生签名);
            this.layoutControl1.Controls.Add(this.pe患者签名);
            this.layoutControl1.Controls.Add(this.pe随访医生签名);
            this.layoutControl1.Controls.Add(this.rg用药用法);
            this.layoutControl1.Controls.Add(this.txt症状及体征其他);
            this.layoutControl1.Controls.Add(this.txt化疗方案);
            this.layoutControl1.Controls.Add(this.rg督导人员);
            this.layoutControl1.Controls.Add(this.flp治疗月序);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.txt编号);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel22);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel21);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel20);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel19);
            this.layoutControl1.Controls.Add(this.dte下次随访时间);
            this.layoutControl1.Controls.Add(this.txt处理意见);
            this.layoutControl1.Controls.Add(this.txt转诊2周内随访);
            this.layoutControl1.Controls.Add(this.txt转诊机构);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.rg并发症);
            this.layoutControl1.Controls.Add(this.rg药物不良反应);
            this.layoutControl1.Controls.Add(this.flp漏服药次数);
            this.layoutControl1.Controls.Add(this.flp药品剂型);
            this.layoutControl1.Controls.Add(this.flp饮酒);
            this.layoutControl1.Controls.Add(this.flp吸烟);
            this.layoutControl1.Controls.Add(this.flp症状体征);
            this.layoutControl1.Controls.Add(this.rg随访方式);
            this.layoutControl1.Controls.Add(this.dte随访时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24});
            this.layoutControl1.Location = new System.Drawing.Point(100, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(661, 499);
            this.layoutControl1.TabIndex = 126;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt并发症有
            // 
            this.txt并发症有.Location = new System.Drawing.Point(247, 527);
            this.txt并发症有.Margin = new System.Windows.Forms.Padding(2);
            this.txt并发症有.Name = "txt并发症有";
            this.txt并发症有.Properties.ReadOnly = true;
            this.txt并发症有.Size = new System.Drawing.Size(394, 20);
            this.txt并发症有.StyleController = this.layoutControl1;
            this.txt并发症有.TabIndex = 2;
            // 
            // txt药物不良反应有
            // 
            this.txt药物不良反应有.Location = new System.Drawing.Point(247, 501);
            this.txt药物不良反应有.Margin = new System.Windows.Forms.Padding(2);
            this.txt药物不良反应有.Name = "txt药物不良反应有";
            this.txt药物不良反应有.Properties.ReadOnly = true;
            this.txt药物不良反应有.Size = new System.Drawing.Size(394, 20);
            this.txt药物不良反应有.StyleController = this.layoutControl1;
            this.txt药物不良反应有.TabIndex = 2;
            // 
            // textEdit修改人
            // 
            this.textEdit修改人.Location = new System.Drawing.Point(514, 964);
            this.textEdit修改人.Name = "textEdit修改人";
            this.textEdit修改人.Properties.ReadOnly = true;
            this.textEdit修改人.Size = new System.Drawing.Size(127, 20);
            this.textEdit修改人.StyleController = this.layoutControl1;
            this.textEdit修改人.TabIndex = 55;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(306, 964);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(117, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 54;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(123, 964);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(92, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 53;
            // 
            // textEdit更新时间
            // 
            this.textEdit更新时间.Location = new System.Drawing.Point(306, 940);
            this.textEdit更新时间.Name = "textEdit更新时间";
            this.textEdit更新时间.Properties.ReadOnly = true;
            this.textEdit更新时间.Size = new System.Drawing.Size(117, 20);
            this.textEdit更新时间.StyleController = this.layoutControl1;
            this.textEdit更新时间.TabIndex = 52;
            // 
            // textEdit所属机构
            // 
            this.textEdit所属机构.Location = new System.Drawing.Point(514, 940);
            this.textEdit所属机构.Name = "textEdit所属机构";
            this.textEdit所属机构.Properties.ReadOnly = true;
            this.textEdit所属机构.Size = new System.Drawing.Size(127, 20);
            this.textEdit所属机构.StyleController = this.layoutControl1;
            this.textEdit所属机构.TabIndex = 51;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(123, 940);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(92, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 50;
            // 
            // pe评估医生签名
            // 
            this.pe评估医生签名.Location = new System.Drawing.Point(210, 890);
            this.pe评估医生签名.Name = "pe评估医生签名";
            this.pe评估医生签名.Properties.ReadOnly = true;
            this.pe评估医生签名.Size = new System.Drawing.Size(151, 36);
            this.pe评估医生签名.StyleController = this.layoutControl1;
            this.pe评估医生签名.TabIndex = 49;
            // 
            // pe患者签名
            // 
            this.pe患者签名.Location = new System.Drawing.Point(363, 698);
            this.pe患者签名.Name = "pe患者签名";
            this.pe患者签名.Properties.ReadOnly = true;
            this.pe患者签名.Size = new System.Drawing.Size(142, 36);
            this.pe患者签名.StyleController = this.layoutControl1;
            this.pe患者签名.TabIndex = 48;
            // 
            // pe随访医生签名
            // 
            this.pe随访医生签名.Location = new System.Drawing.Point(123, 698);
            this.pe随访医生签名.Name = "pe随访医生签名";
            this.pe随访医生签名.Properties.ReadOnly = true;
            this.pe随访医生签名.Size = new System.Drawing.Size(141, 36);
            this.pe随访医生签名.StyleController = this.layoutControl1;
            this.pe随访医生签名.TabIndex = 47;
            // 
            // rg用药用法
            // 
            this.rg用药用法.Location = new System.Drawing.Point(123, 415);
            this.rg用药用法.Name = "rg用药用法";
            this.rg用药用法.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 每日"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 间歇")});
            this.rg用药用法.Properties.ReadOnly = true;
            this.rg用药用法.Size = new System.Drawing.Size(120, 24);
            this.rg用药用法.StyleController = this.layoutControl1;
            this.rg用药用法.TabIndex = 45;
            // 
            // txt症状及体征其他
            // 
            this.txt症状及体征其他.Location = new System.Drawing.Point(177, 276);
            this.txt症状及体征其他.Name = "txt症状及体征其他";
            this.txt症状及体征其他.Properties.ReadOnly = true;
            this.txt症状及体征其他.Size = new System.Drawing.Size(464, 52);
            this.txt症状及体征其他.StyleController = this.layoutControl1;
            this.txt症状及体征其他.TabIndex = 45;
            this.txt症状及体征其他.UseOptimizedRendering = true;
            // 
            // txt化疗方案
            // 
            this.txt化疗方案.Location = new System.Drawing.Point(123, 391);
            this.txt化疗方案.Name = "txt化疗方案";
            this.txt化疗方案.Properties.ReadOnly = true;
            this.txt化疗方案.Size = new System.Drawing.Size(518, 20);
            this.txt化疗方案.StyleController = this.layoutControl1;
            this.txt化疗方案.TabIndex = 43;
            // 
            // rg督导人员
            // 
            this.rg督导人员.Location = new System.Drawing.Point(123, 128);
            this.rg督导人员.Name = "rg督导人员";
            this.rg督导人员.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 医生"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 家属"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3 自服药"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "4 其他")});
            this.rg督导人员.Properties.ReadOnly = true;
            this.rg督导人员.Size = new System.Drawing.Size(276, 25);
            this.rg督导人员.StyleController = this.layoutControl1;
            this.rg督导人员.TabIndex = 42;
            // 
            // flp治疗月序
            // 
            this.flp治疗月序.Controls.Add(this.labelControl17);
            this.flp治疗月序.Controls.Add(this.txt治疗月序);
            this.flp治疗月序.Controls.Add(this.labelControl18);
            this.flp治疗月序.Controls.Add(this.labelControl20);
            this.flp治疗月序.Location = new System.Drawing.Point(340, 102);
            this.flp治疗月序.Name = "flp治疗月序";
            this.flp治疗月序.Size = new System.Drawing.Size(301, 22);
            this.flp治疗月序.TabIndex = 41;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(3, 3);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(12, 14);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "第";
            // 
            // txt治疗月序
            // 
            this.txt治疗月序.Location = new System.Drawing.Point(19, 1);
            this.txt治疗月序.Margin = new System.Windows.Forms.Padding(1);
            this.txt治疗月序.Name = "txt治疗月序";
            this.txt治疗月序.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt治疗月序.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt治疗月序.Properties.ReadOnly = true;
            this.txt治疗月序.Size = new System.Drawing.Size(50, 20);
            this.txt治疗月序.TabIndex = 1;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(73, 3);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(12, 14);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "月";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl20.Location = new System.Drawing.Point(91, 5);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(106, 14);
            this.labelControl20.TabIndex = 3;
            this.labelControl20.Text = "※请填写阿拉伯数字";
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(123, 78);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(518, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 39;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(536, 54);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(105, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 38;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(333, 54);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(112, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 37;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(123, 54);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(119, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 36;
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(536, 30);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit姓名.Properties.ReadOnly = true;
            this.textEdit姓名.Size = new System.Drawing.Size(105, 20);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 35;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(333, 30);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(112, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 34;
            // 
            // txt编号
            // 
            this.txt编号.Location = new System.Drawing.Point(123, 30);
            this.txt编号.Name = "txt编号";
            this.txt编号.Properties.ReadOnly = true;
            this.txt编号.Size = new System.Drawing.Size(119, 20);
            this.txt编号.StyleController = this.layoutControl1;
            this.txt编号.TabIndex = 33;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.Controls.Add(this.labelControl6);
            this.flowLayoutPanel22.Controls.Add(this.txt应访视次数);
            this.flowLayoutPanel22.Controls.Add(this.labelControl10);
            this.flowLayoutPanel22.Controls.Add(this.txt实际方式次数);
            this.flowLayoutPanel22.Controls.Add(this.labelControl11);
            this.flowLayoutPanel22.Controls.Add(this.txt应服药次数);
            this.flowLayoutPanel22.Controls.Add(this.labelControl12);
            this.flowLayoutPanel22.Controls.Add(this.txt实际服药次数);
            this.flowLayoutPanel22.Controls.Add(this.labelControl13);
            this.flowLayoutPanel22.Controls.Add(this.txt服药率);
            this.flowLayoutPanel22.Controls.Add(this.labelControl14);
            this.flowLayoutPanel22.Controls.Add(this.labelControl4);
            this.flowLayoutPanel22.Location = new System.Drawing.Point(121, 826);
            this.flowLayoutPanel22.MaximumSize = new System.Drawing.Size(0, 60);
            this.flowLayoutPanel22.MinimumSize = new System.Drawing.Size(0, 60);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(520, 60);
            this.flowLayoutPanel22.TabIndex = 31;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(3, 3);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "应访视患者";
            // 
            // txt应访视次数
            // 
            this.txt应访视次数.Location = new System.Drawing.Point(69, 3);
            this.txt应访视次数.Name = "txt应访视次数";
            this.txt应访视次数.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt应访视次数.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt应访视次数.Properties.ReadOnly = true;
            this.txt应访视次数.Size = new System.Drawing.Size(50, 20);
            this.txt应访视次数.TabIndex = 6;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(125, 3);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(72, 14);
            this.labelControl10.TabIndex = 1;
            this.labelControl10.Text = "次，实际访视";
            // 
            // txt实际方式次数
            // 
            this.txt实际方式次数.Location = new System.Drawing.Point(203, 3);
            this.txt实际方式次数.Name = "txt实际方式次数";
            this.txt实际方式次数.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt实际方式次数.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt实际方式次数.Properties.ReadOnly = true;
            this.txt实际方式次数.Size = new System.Drawing.Size(50, 20);
            this.txt实际方式次数.TabIndex = 7;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(259, 3);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(144, 14);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "次；患者在疗程中，应服药";
            // 
            // txt应服药次数
            // 
            this.txt应服药次数.Location = new System.Drawing.Point(409, 3);
            this.txt应服药次数.Name = "txt应服药次数";
            this.txt应服药次数.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt应服药次数.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt应服药次数.Properties.ReadOnly = true;
            this.txt应服药次数.Size = new System.Drawing.Size(50, 20);
            this.txt应服药次数.TabIndex = 8;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(3, 29);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(72, 14);
            this.labelControl12.TabIndex = 3;
            this.labelControl12.Text = "次，实际服药";
            // 
            // txt实际服药次数
            // 
            this.txt实际服药次数.Location = new System.Drawing.Point(81, 29);
            this.txt实际服药次数.Name = "txt实际服药次数";
            this.txt实际服药次数.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt实际服药次数.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt实际服药次数.Properties.ReadOnly = true;
            this.txt实际服药次数.Size = new System.Drawing.Size(50, 20);
            this.txt实际服药次数.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(137, 29);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 4;
            this.labelControl13.Text = "次，服药率";
            // 
            // txt服药率
            // 
            this.txt服药率.Location = new System.Drawing.Point(203, 29);
            this.txt服药率.Name = "txt服药率";
            this.txt服药率.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt服药率.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt服药率.Properties.ReadOnly = true;
            this.txt服药率.Size = new System.Drawing.Size(50, 20);
            this.txt服药率.TabIndex = 10;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(259, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(12, 14);
            this.labelControl14.TabIndex = 5;
            this.labelControl14.Text = "%";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl4.Location = new System.Drawing.Point(277, 31);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(106, 14);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "※请填写阿拉伯数字";
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.Location = new System.Drawing.Point(211, 482);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(370, 20);
            this.flowLayoutPanel21.TabIndex = 30;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.Controls.Add(this.labelControl16);
            this.flowLayoutPanel20.Controls.Add(this.chk1完成诊疗);
            this.flowLayoutPanel20.Controls.Add(this.chk2死亡);
            this.flowLayoutPanel20.Controls.Add(this.chk3丢失);
            this.flowLayoutPanel20.Controls.Add(this.chk4转入耐药治疗);
            this.flowLayoutPanel20.Location = new System.Drawing.Point(121, 797);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(520, 25);
            this.flowLayoutPanel20.TabIndex = 29;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(3, 5);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(95, 14);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "2 停止治疗原因：";
            // 
            // chk1完成诊疗
            // 
            this.chk1完成诊疗.Location = new System.Drawing.Point(104, 3);
            this.chk1完成诊疗.Name = "chk1完成诊疗";
            this.chk1完成诊疗.Properties.Caption = "1 完成疗程";
            this.chk1完成诊疗.Properties.ReadOnly = true;
            this.chk1完成诊疗.Size = new System.Drawing.Size(85, 19);
            this.chk1完成诊疗.TabIndex = 0;
            // 
            // chk2死亡
            // 
            this.chk2死亡.Location = new System.Drawing.Point(195, 3);
            this.chk2死亡.Name = "chk2死亡";
            this.chk2死亡.Properties.Caption = "2 死亡";
            this.chk2死亡.Properties.ReadOnly = true;
            this.chk2死亡.Size = new System.Drawing.Size(60, 19);
            this.chk2死亡.TabIndex = 1;
            // 
            // chk3丢失
            // 
            this.chk3丢失.Location = new System.Drawing.Point(261, 3);
            this.chk3丢失.Name = "chk3丢失";
            this.chk3丢失.Properties.Caption = "3 丢失";
            this.chk3丢失.Properties.ReadOnly = true;
            this.chk3丢失.Size = new System.Drawing.Size(59, 19);
            this.chk3丢失.TabIndex = 2;
            // 
            // chk4转入耐药治疗
            // 
            this.chk4转入耐药治疗.Location = new System.Drawing.Point(326, 3);
            this.chk4转入耐药治疗.Name = "chk4转入耐药治疗";
            this.chk4转入耐药治疗.Properties.Caption = "4 转入耐多药治疗";
            this.chk4转入耐药治疗.Properties.ReadOnly = true;
            this.chk4转入耐药治疗.Size = new System.Drawing.Size(118, 19);
            this.chk4转入耐药治疗.TabIndex = 3;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.Controls.Add(this.labelControl15);
            this.flowLayoutPanel19.Controls.Add(this.dte停止治疗时间);
            this.flowLayoutPanel19.Location = new System.Drawing.Point(121, 768);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(520, 25);
            this.flowLayoutPanel19.TabIndex = 28;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(3, 5);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(119, 14);
            this.labelControl15.TabIndex = 7;
            this.labelControl15.Text = "1 出现停止治疗时间：";
            // 
            // dte停止治疗时间
            // 
            this.dte停止治疗时间.EditValue = null;
            this.dte停止治疗时间.Location = new System.Drawing.Point(128, 3);
            this.dte停止治疗时间.Name = "dte停止治疗时间";
            this.dte停止治疗时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte停止治疗时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte停止治疗时间.Properties.ReadOnly = true;
            this.dte停止治疗时间.Size = new System.Drawing.Size(109, 20);
            this.dte停止治疗时间.TabIndex = 8;
            // 
            // dte下次随访时间
            // 
            this.dte下次随访时间.EditValue = null;
            this.dte下次随访时间.Location = new System.Drawing.Point(123, 673);
            this.dte下次随访时间.Name = "dte下次随访时间";
            this.dte下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte下次随访时间.Properties.ReadOnly = true;
            this.dte下次随访时间.Size = new System.Drawing.Size(141, 20);
            this.dte下次随访时间.StyleController = this.layoutControl1;
            this.dte下次随访时间.TabIndex = 0;
            // 
            // txt处理意见
            // 
            this.txt处理意见.Location = new System.Drawing.Point(123, 649);
            this.txt处理意见.Name = "txt处理意见";
            this.txt处理意见.Properties.ReadOnly = true;
            this.txt处理意见.Size = new System.Drawing.Size(518, 20);
            this.txt处理意见.StyleController = this.layoutControl1;
            this.txt处理意见.TabIndex = 0;
            // 
            // txt转诊2周内随访
            // 
            this.txt转诊2周内随访.Location = new System.Drawing.Point(123, 601);
            this.txt转诊2周内随访.Name = "txt转诊2周内随访";
            this.txt转诊2周内随访.Properties.ReadOnly = true;
            this.txt转诊2周内随访.Size = new System.Drawing.Size(518, 44);
            this.txt转诊2周内随访.StyleController = this.layoutControl1;
            this.txt转诊2周内随访.TabIndex = 0;
            this.txt转诊2周内随访.UseOptimizedRendering = true;
            // 
            // txt转诊机构
            // 
            this.txt转诊机构.Location = new System.Drawing.Point(123, 577);
            this.txt转诊机构.Name = "txt转诊机构";
            this.txt转诊机构.Properties.ReadOnly = true;
            this.txt转诊机构.Size = new System.Drawing.Size(518, 20);
            this.txt转诊机构.StyleController = this.layoutControl1;
            this.txt转诊机构.TabIndex = 0;
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Location = new System.Drawing.Point(123, 553);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Properties.ReadOnly = true;
            this.txt转诊原因.Size = new System.Drawing.Size(518, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 0;
            // 
            // rg并发症
            // 
            this.rg并发症.Location = new System.Drawing.Point(123, 527);
            this.rg并发症.Margin = new System.Windows.Forms.Padding(2);
            this.rg并发症.Name = "rg并发症";
            this.rg并发症.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 有")});
            this.rg并发症.Properties.ReadOnly = true;
            this.rg并发症.Size = new System.Drawing.Size(120, 22);
            this.rg并发症.StyleController = this.layoutControl1;
            this.rg并发症.TabIndex = 4;
            // 
            // rg药物不良反应
            // 
            this.rg药物不良反应.Location = new System.Drawing.Point(123, 501);
            this.rg药物不良反应.Margin = new System.Windows.Forms.Padding(2);
            this.rg药物不良反应.Name = "rg药物不良反应";
            this.rg药物不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 有")});
            this.rg药物不良反应.Properties.ReadOnly = true;
            this.rg药物不良反应.Size = new System.Drawing.Size(120, 22);
            this.rg药物不良反应.StyleController = this.layoutControl1;
            this.rg药物不良反应.TabIndex = 3;
            // 
            // flp漏服药次数
            // 
            this.flp漏服药次数.Controls.Add(this.txt漏服药次数);
            this.flp漏服药次数.Controls.Add(this.labelControl3);
            this.flp漏服药次数.Controls.Add(this.labelControl19);
            this.flp漏服药次数.Location = new System.Drawing.Point(123, 472);
            this.flp漏服药次数.Name = "flp漏服药次数";
            this.flp漏服药次数.Size = new System.Drawing.Size(518, 25);
            this.flp漏服药次数.TabIndex = 18;
            // 
            // txt漏服药次数
            // 
            this.txt漏服药次数.Location = new System.Drawing.Point(3, 3);
            this.txt漏服药次数.Name = "txt漏服药次数";
            this.txt漏服药次数.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt漏服药次数.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt漏服药次数.Properties.ReadOnly = true;
            this.txt漏服药次数.Size = new System.Drawing.Size(52, 20);
            this.txt漏服药次数.TabIndex = 0;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(61, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(12, 14);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "次";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl19.Location = new System.Drawing.Point(79, 5);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(106, 14);
            this.labelControl19.TabIndex = 2;
            this.labelControl19.Text = "※请填写阿拉伯数字";
            // 
            // flp药品剂型
            // 
            this.flp药品剂型.Controls.Add(this.chk1固定剂);
            this.flp药品剂型.Controls.Add(this.chk2散装药);
            this.flp药品剂型.Controls.Add(this.chk3板式药);
            this.flp药品剂型.Controls.Add(this.chk4注射剂);
            this.flp药品剂型.Location = new System.Drawing.Point(123, 443);
            this.flp药品剂型.Name = "flp药品剂型";
            this.flp药品剂型.Size = new System.Drawing.Size(518, 25);
            this.flp药品剂型.TabIndex = 17;
            // 
            // chk1固定剂
            // 
            this.chk1固定剂.Location = new System.Drawing.Point(3, 3);
            this.chk1固定剂.Name = "chk1固定剂";
            this.chk1固定剂.Properties.Caption = "1 固定剂量复合制剂";
            this.chk1固定剂.Properties.ReadOnly = true;
            this.chk1固定剂.Size = new System.Drawing.Size(135, 19);
            this.chk1固定剂.TabIndex = 0;
            // 
            // chk2散装药
            // 
            this.chk2散装药.Location = new System.Drawing.Point(144, 3);
            this.chk2散装药.Name = "chk2散装药";
            this.chk2散装药.Properties.Caption = "2 散装药";
            this.chk2散装药.Properties.ReadOnly = true;
            this.chk2散装药.Size = new System.Drawing.Size(75, 19);
            this.chk2散装药.TabIndex = 1;
            // 
            // chk3板式药
            // 
            this.chk3板式药.Location = new System.Drawing.Point(225, 3);
            this.chk3板式药.Name = "chk3板式药";
            this.chk3板式药.Properties.Caption = "3 板式组合";
            this.chk3板式药.Properties.ReadOnly = true;
            this.chk3板式药.Size = new System.Drawing.Size(93, 19);
            this.chk3板式药.TabIndex = 2;
            // 
            // chk4注射剂
            // 
            this.chk4注射剂.Location = new System.Drawing.Point(324, 3);
            this.chk4注射剂.Name = "chk4注射剂";
            this.chk4注射剂.Properties.Caption = "4 注射剂";
            this.chk4注射剂.Properties.ReadOnly = true;
            this.chk4注射剂.Size = new System.Drawing.Size(75, 19);
            this.chk4注射剂.TabIndex = 3;
            // 
            // flp饮酒
            // 
            this.flp饮酒.Controls.Add(this.txt饮酒1);
            this.flp饮酒.Controls.Add(this.label2);
            this.flp饮酒.Controls.Add(this.txt饮酒2);
            this.flp饮酒.Controls.Add(this.labelControl2);
            this.flp饮酒.Location = new System.Drawing.Point(177, 361);
            this.flp饮酒.Name = "flp饮酒";
            this.flp饮酒.Size = new System.Drawing.Size(464, 26);
            this.flp饮酒.TabIndex = 14;
            // 
            // txt饮酒1
            // 
            this.txt饮酒1.Location = new System.Drawing.Point(3, 3);
            this.txt饮酒1.Name = "txt饮酒1";
            this.txt饮酒1.Properties.ReadOnly = true;
            this.txt饮酒1.Size = new System.Drawing.Size(53, 20);
            this.txt饮酒1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "/";
            // 
            // txt饮酒2
            // 
            this.txt饮酒2.Location = new System.Drawing.Point(79, 3);
            this.txt饮酒2.Name = "txt饮酒2";
            this.txt饮酒2.Properties.ReadOnly = true;
            this.txt饮酒2.Size = new System.Drawing.Size(48, 20);
            this.txt饮酒2.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(133, 6);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(29, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "两/天";
            // 
            // flp吸烟
            // 
            this.flp吸烟.Controls.Add(this.txt吸烟1);
            this.flp吸烟.Controls.Add(this.label1);
            this.flp吸烟.Controls.Add(this.txt吸烟2);
            this.flp吸烟.Controls.Add(this.labelControl1);
            this.flp吸烟.Location = new System.Drawing.Point(177, 332);
            this.flp吸烟.Name = "flp吸烟";
            this.flp吸烟.Size = new System.Drawing.Size(464, 25);
            this.flp吸烟.TabIndex = 13;
            // 
            // txt吸烟1
            // 
            this.txt吸烟1.Location = new System.Drawing.Point(3, 3);
            this.txt吸烟1.Name = "txt吸烟1";
            this.txt吸烟1.Properties.ReadOnly = true;
            this.txt吸烟1.Size = new System.Drawing.Size(53, 20);
            this.txt吸烟1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "/";
            // 
            // txt吸烟2
            // 
            this.txt吸烟2.Location = new System.Drawing.Point(79, 3);
            this.txt吸烟2.Name = "txt吸烟2";
            this.txt吸烟2.Properties.ReadOnly = true;
            this.txt吸烟2.Size = new System.Drawing.Size(48, 20);
            this.txt吸烟2.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.labelControl1.Location = new System.Drawing.Point(133, 6);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "支/天";
            // 
            // flp症状体征
            // 
            this.flp症状体征.Controls.Add(this.chk0没有症状);
            this.flp症状体征.Controls.Add(this.chk1咳嗽咳痰);
            this.flp症状体征.Controls.Add(this.chk2低热盗汗);
            this.flp症状体征.Controls.Add(this.chk3咯血血痰);
            this.flp症状体征.Controls.Add(this.chk4胸痛消瘦);
            this.flp症状体征.Controls.Add(this.chk5恶心纳差);
            this.flp症状体征.Controls.Add(this.chk6关节疼痛);
            this.flp症状体征.Controls.Add(this.chk7头痛失眠);
            this.flp症状体征.Controls.Add(this.chk8视物模糊);
            this.flp症状体征.Controls.Add(this.chk9皮痒皮疹);
            this.flp症状体征.Controls.Add(this.chk10耳鸣);
            this.flp症状体征.Location = new System.Drawing.Point(122, 186);
            this.flp症状体征.Name = "flp症状体征";
            this.flp症状体征.Size = new System.Drawing.Size(519, 86);
            this.flp症状体征.TabIndex = 10;
            // 
            // chk0没有症状
            // 
            this.chk0没有症状.Location = new System.Drawing.Point(5, 3);
            this.chk0没有症状.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.chk0没有症状.Name = "chk0没有症状";
            this.chk0没有症状.Properties.Caption = "0 没有症状";
            this.chk0没有症状.Properties.ReadOnly = true;
            this.chk0没有症状.Size = new System.Drawing.Size(85, 19);
            this.chk0没有症状.TabIndex = 0;
            // 
            // chk1咳嗽咳痰
            // 
            this.chk1咳嗽咳痰.Location = new System.Drawing.Point(96, 3);
            this.chk1咳嗽咳痰.Name = "chk1咳嗽咳痰";
            this.chk1咳嗽咳痰.Properties.Caption = "1 咳嗽咳痰";
            this.chk1咳嗽咳痰.Properties.ReadOnly = true;
            this.chk1咳嗽咳痰.Size = new System.Drawing.Size(82, 19);
            this.chk1咳嗽咳痰.TabIndex = 1;
            // 
            // chk2低热盗汗
            // 
            this.chk2低热盗汗.Location = new System.Drawing.Point(184, 3);
            this.chk2低热盗汗.Name = "chk2低热盗汗";
            this.chk2低热盗汗.Properties.Caption = "2 低热盗汗";
            this.chk2低热盗汗.Properties.ReadOnly = true;
            this.chk2低热盗汗.Size = new System.Drawing.Size(87, 19);
            this.chk2低热盗汗.TabIndex = 2;
            // 
            // chk3咯血血痰
            // 
            this.chk3咯血血痰.Location = new System.Drawing.Point(277, 3);
            this.chk3咯血血痰.Name = "chk3咯血血痰";
            this.chk3咯血血痰.Properties.Caption = "3 咯血或血痰";
            this.chk3咯血血痰.Properties.ReadOnly = true;
            this.chk3咯血血痰.Size = new System.Drawing.Size(96, 19);
            this.chk3咯血血痰.TabIndex = 3;
            // 
            // chk4胸痛消瘦
            // 
            this.chk4胸痛消瘦.Location = new System.Drawing.Point(379, 3);
            this.chk4胸痛消瘦.Name = "chk4胸痛消瘦";
            this.chk4胸痛消瘦.Properties.Caption = "4 胸痛消瘦";
            this.chk4胸痛消瘦.Properties.ReadOnly = true;
            this.chk4胸痛消瘦.Size = new System.Drawing.Size(84, 19);
            this.chk4胸痛消瘦.TabIndex = 4;
            // 
            // chk5恶心纳差
            // 
            this.chk5恶心纳差.Location = new System.Drawing.Point(3, 28);
            this.chk5恶心纳差.Name = "chk5恶心纳差";
            this.chk5恶心纳差.Properties.Caption = "5 恶心纳差";
            this.chk5恶心纳差.Properties.ReadOnly = true;
            this.chk5恶心纳差.Size = new System.Drawing.Size(82, 19);
            this.chk5恶心纳差.TabIndex = 5;
            // 
            // chk6关节疼痛
            // 
            this.chk6关节疼痛.Location = new System.Drawing.Point(93, 28);
            this.chk6关节疼痛.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.chk6关节疼痛.Name = "chk6关节疼痛";
            this.chk6关节疼痛.Properties.Caption = "6 关节疼痛";
            this.chk6关节疼痛.Properties.ReadOnly = true;
            this.chk6关节疼痛.Size = new System.Drawing.Size(85, 19);
            this.chk6关节疼痛.TabIndex = 6;
            // 
            // chk7头痛失眠
            // 
            this.chk7头痛失眠.Location = new System.Drawing.Point(184, 28);
            this.chk7头痛失眠.Name = "chk7头痛失眠";
            this.chk7头痛失眠.Properties.Caption = "7 头痛失眠";
            this.chk7头痛失眠.Properties.ReadOnly = true;
            this.chk7头痛失眠.Size = new System.Drawing.Size(82, 19);
            this.chk7头痛失眠.TabIndex = 7;
            // 
            // chk8视物模糊
            // 
            this.chk8视物模糊.Location = new System.Drawing.Point(272, 28);
            this.chk8视物模糊.Name = "chk8视物模糊";
            this.chk8视物模糊.Properties.Caption = "8 视物模糊";
            this.chk8视物模糊.Properties.ReadOnly = true;
            this.chk8视物模糊.Size = new System.Drawing.Size(87, 19);
            this.chk8视物模糊.TabIndex = 8;
            // 
            // chk9皮痒皮疹
            // 
            this.chk9皮痒皮疹.Location = new System.Drawing.Point(365, 28);
            this.chk9皮痒皮疹.Name = "chk9皮痒皮疹";
            this.chk9皮痒皮疹.Properties.Caption = "9 皮肤瘙痒、皮疹";
            this.chk9皮痒皮疹.Properties.ReadOnly = true;
            this.chk9皮痒皮疹.Size = new System.Drawing.Size(125, 19);
            this.chk9皮痒皮疹.TabIndex = 9;
            // 
            // chk10耳鸣
            // 
            this.chk10耳鸣.Location = new System.Drawing.Point(3, 53);
            this.chk10耳鸣.Name = "chk10耳鸣";
            this.chk10耳鸣.Properties.Caption = "10耳鸣、听力下降";
            this.chk10耳鸣.Properties.ReadOnly = true;
            this.chk10耳鸣.Size = new System.Drawing.Size(128, 19);
            this.chk10耳鸣.TabIndex = 10;
            // 
            // rg随访方式
            // 
            this.rg随访方式.Location = new System.Drawing.Point(123, 157);
            this.rg随访方式.Name = "rg随访方式";
            this.rg随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3电话")});
            this.rg随访方式.Properties.ReadOnly = true;
            this.rg随访方式.Size = new System.Drawing.Size(210, 25);
            this.rg随访方式.StyleController = this.layoutControl1;
            this.rg随访方式.TabIndex = 9;
            // 
            // dte随访时间
            // 
            this.dte随访时间.EditValue = null;
            this.dte随访时间.Location = new System.Drawing.Point(123, 102);
            this.dte随访时间.Name = "dte随访时间";
            this.dte随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, false, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.dte随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间.Properties.ReadOnly = true;
            this.dte随访时间.Size = new System.Drawing.Size(118, 20);
            this.dte随访时间.StyleController = this.layoutControl1;
            this.dte随访时间.TabIndex = 6;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.flowLayoutPanel21;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(90, 557);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "肺结核患者随访服务记录表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcl随访时间,
            this.lcl随访方式,
            this.layoutControlItem5,
            this.lcl症状体征,
            this.lcl吸烟,
            this.emptySpaceItem2,
            this.lcl药品剂型,
            this.emptySpaceItem3,
            this.lcl漏服药次数,
            this.lcl药物不良反应,
            this.lcl并发症,
            this.lcl原因,
            this.lcl转诊机构,
            this.lcl症状2周随访,
            this.emptySpaceItem4,
            this.lcl处理意见,
            this.lcl下次随访时间,
            this.layoutControlItem22,
            this.emptySpaceItem6,
            this.layoutControlItem23,
            this.emptySpaceItem5,
            this.layoutControlItem25,
            this.lcl饮酒,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.lcl治疗月序,
            this.emptySpaceItem7,
            this.lcl督导人员,
            this.emptySpaceItem8,
            this.lcl化疗方案,
            this.layoutControlItem7,
            this.emptySpaceItem10,
            this.lcl用药用法,
            this.lcl随访医生,
            this.lcl患者签名,
            this.emptySpaceItem9,
            this.layoutControlItem20,
            this.emptySpaceItem11,
            this.layoutControlItem21,
            this.layoutControlItem26,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem13,
            this.emptySpaceItem12,
            this.emptySpaceItem1,
            this.emptySpaceItem14});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(644, 997);
            this.layoutControlGroup1.Text = "肺结核患者随访服务记录表";
            // 
            // lcl随访时间
            // 
            this.lcl随访时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl随访时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl随访时间.AppearanceItemCaption.Options.UseFont = true;
            this.lcl随访时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl随访时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl随访时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl随访时间.Control = this.dte随访时间;
            this.lcl随访时间.CustomizationFormText = "随访时间";
            this.lcl随访时间.Location = new System.Drawing.Point(0, 72);
            this.lcl随访时间.MaxSize = new System.Drawing.Size(0, 24);
            this.lcl随访时间.MinSize = new System.Drawing.Size(174, 24);
            this.lcl随访时间.Name = "lcl随访时间";
            this.lcl随访时间.Size = new System.Drawing.Size(242, 26);
            this.lcl随访时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl随访时间.Text = "随访时间";
            this.lcl随访时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl随访时间.TextSize = new System.Drawing.Size(115, 14);
            this.lcl随访时间.TextToControlDistance = 5;
            // 
            // lcl随访方式
            // 
            this.lcl随访方式.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl随访方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl随访方式.AppearanceItemCaption.Options.UseFont = true;
            this.lcl随访方式.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl随访方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl随访方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl随访方式.Control = this.rg随访方式;
            this.lcl随访方式.CustomizationFormText = "随访方式";
            this.lcl随访方式.Location = new System.Drawing.Point(0, 127);
            this.lcl随访方式.MaxSize = new System.Drawing.Size(334, 29);
            this.lcl随访方式.MinSize = new System.Drawing.Size(334, 29);
            this.lcl随访方式.Name = "lcl随访方式";
            this.lcl随访方式.Size = new System.Drawing.Size(334, 29);
            this.lcl随访方式.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl随访方式.Text = "随访方式";
            this.lcl随访方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl随访方式.TextSize = new System.Drawing.Size(115, 14);
            this.lcl随访方式.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.flp症状体征;
            this.layoutControlItem5.CustomizationFormText = "症状及体征：";
            this.layoutControlItem5.Location = new System.Drawing.Point(119, 156);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(104, 90);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(523, 90);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "症状及体征：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // lcl症状体征
            // 
            this.lcl症状体征.AllowHotTrack = false;
            this.lcl症状体征.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl症状体征.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl症状体征.AppearanceItemCaption.Options.UseFont = true;
            this.lcl症状体征.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl症状体征.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl症状体征.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl症状体征.CustomizationFormText = "蟋蟀搁置";
            this.lcl症状体征.Location = new System.Drawing.Point(0, 156);
            this.lcl症状体征.MaxSize = new System.Drawing.Size(119, 146);
            this.lcl症状体征.MinSize = new System.Drawing.Size(119, 146);
            this.lcl症状体征.Name = "lcl症状体征";
            this.lcl症状体征.Size = new System.Drawing.Size(119, 146);
            this.lcl症状体征.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl症状体征.Text = "症状及体征";
            this.lcl症状体征.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl症状体征.TextSize = new System.Drawing.Size(115, 20);
            this.lcl症状体征.TextVisible = true;
            // 
            // lcl吸烟
            // 
            this.lcl吸烟.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl吸烟.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl吸烟.AppearanceItemCaption.Options.UseFont = true;
            this.lcl吸烟.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl吸烟.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl吸烟.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl吸烟.Control = this.flp吸烟;
            this.lcl吸烟.CustomizationFormText = "吸烟";
            this.lcl吸烟.Location = new System.Drawing.Point(119, 302);
            this.lcl吸烟.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl吸烟.MinSize = new System.Drawing.Size(222, 29);
            this.lcl吸烟.Name = "lcl吸烟";
            this.lcl吸烟.Size = new System.Drawing.Size(523, 29);
            this.lcl吸烟.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl吸烟.Text = "吸烟";
            this.lcl吸烟.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl吸烟.TextSize = new System.Drawing.Size(50, 14);
            this.lcl吸烟.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "生活方式指导";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 302);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(119, 59);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(119, 59);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(119, 59);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "生 活\n方 式\n指 导";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // lcl药品剂型
            // 
            this.lcl药品剂型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl药品剂型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseFont = true;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl药品剂型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl药品剂型.Control = this.flp药品剂型;
            this.lcl药品剂型.CustomizationFormText = "药品剂型";
            this.lcl药品剂型.Location = new System.Drawing.Point(36, 413);
            this.lcl药品剂型.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl药品剂型.MinSize = new System.Drawing.Size(90, 29);
            this.lcl药品剂型.Name = "lcl药品剂型";
            this.lcl药品剂型.Size = new System.Drawing.Size(606, 29);
            this.lcl药品剂型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl药品剂型.Text = "药品剂型";
            this.lcl药品剂型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl药品剂型.TextSize = new System.Drawing.Size(79, 14);
            this.lcl药品剂型.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "用药";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 361);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(36, 116);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(36, 110);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(36, 110);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "用\n\n药";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // lcl漏服药次数
            // 
            this.lcl漏服药次数.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl漏服药次数.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl漏服药次数.AppearanceItemCaption.Options.UseFont = true;
            this.lcl漏服药次数.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl漏服药次数.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl漏服药次数.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl漏服药次数.Control = this.flp漏服药次数;
            this.lcl漏服药次数.CustomizationFormText = "漏服药次数";
            this.lcl漏服药次数.Location = new System.Drawing.Point(36, 442);
            this.lcl漏服药次数.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl漏服药次数.MinSize = new System.Drawing.Size(90, 29);
            this.lcl漏服药次数.Name = "lcl漏服药次数";
            this.lcl漏服药次数.Size = new System.Drawing.Size(606, 29);
            this.lcl漏服药次数.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl漏服药次数.Text = "漏服药次数";
            this.lcl漏服药次数.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl漏服药次数.TextSize = new System.Drawing.Size(79, 14);
            this.lcl漏服药次数.TextToControlDistance = 5;
            // 
            // lcl药物不良反应
            // 
            this.lcl药物不良反应.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl药物不良反应.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl药物不良反应.AppearanceItemCaption.Options.UseFont = true;
            this.lcl药物不良反应.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl药物不良反应.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl药物不良反应.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl药物不良反应.Control = this.rg药物不良反应;
            this.lcl药物不良反应.CustomizationFormText = "药物不良反应";
            this.lcl药物不良反应.Location = new System.Drawing.Point(0, 471);
            this.lcl药物不良反应.MaxSize = new System.Drawing.Size(244, 32);
            this.lcl药物不良反应.MinSize = new System.Drawing.Size(244, 26);
            this.lcl药物不良反应.Name = "lcl药物不良反应";
            this.lcl药物不良反应.Size = new System.Drawing.Size(244, 26);
            this.lcl药物不良反应.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl药物不良反应.Text = "药物不良反应";
            this.lcl药物不良反应.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl药物不良反应.TextSize = new System.Drawing.Size(115, 14);
            this.lcl药物不良反应.TextToControlDistance = 5;
            // 
            // lcl并发症
            // 
            this.lcl并发症.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl并发症.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl并发症.AppearanceItemCaption.Options.UseFont = true;
            this.lcl并发症.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl并发症.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl并发症.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl并发症.Control = this.rg并发症;
            this.lcl并发症.CustomizationFormText = "并发症或合并症";
            this.lcl并发症.Location = new System.Drawing.Point(0, 497);
            this.lcl并发症.MaxSize = new System.Drawing.Size(244, 32);
            this.lcl并发症.MinSize = new System.Drawing.Size(244, 26);
            this.lcl并发症.Name = "lcl并发症";
            this.lcl并发症.Size = new System.Drawing.Size(244, 26);
            this.lcl并发症.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl并发症.Text = "并发症或合并症";
            this.lcl并发症.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl并发症.TextSize = new System.Drawing.Size(115, 14);
            this.lcl并发症.TextToControlDistance = 5;
            // 
            // lcl原因
            // 
            this.lcl原因.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.lcl原因.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl原因.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl原因.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl原因.Control = this.txt转诊原因;
            this.lcl原因.CustomizationFormText = "原因";
            this.lcl原因.Location = new System.Drawing.Point(36, 523);
            this.lcl原因.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl原因.MinSize = new System.Drawing.Size(222, 24);
            this.lcl原因.Name = "lcl原因";
            this.lcl原因.Size = new System.Drawing.Size(606, 24);
            this.lcl原因.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl原因.Text = "原因";
            this.lcl原因.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl原因.TextSize = new System.Drawing.Size(79, 14);
            this.lcl原因.TextToControlDistance = 5;
            // 
            // lcl转诊机构
            // 
            this.lcl转诊机构.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.lcl转诊机构.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl转诊机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl转诊机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl转诊机构.Control = this.txt转诊机构;
            this.lcl转诊机构.CustomizationFormText = "机构及科别";
            this.lcl转诊机构.Location = new System.Drawing.Point(36, 547);
            this.lcl转诊机构.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl转诊机构.MinSize = new System.Drawing.Size(222, 24);
            this.lcl转诊机构.Name = "lcl转诊机构";
            this.lcl转诊机构.Size = new System.Drawing.Size(606, 24);
            this.lcl转诊机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl转诊机构.Text = "机构及科别";
            this.lcl转诊机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl转诊机构.TextSize = new System.Drawing.Size(79, 14);
            this.lcl转诊机构.TextToControlDistance = 5;
            // 
            // lcl症状2周随访
            // 
            this.lcl症状2周随访.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.lcl症状2周随访.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl症状2周随访.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl症状2周随访.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl症状2周随访.Control = this.txt转诊2周内随访;
            this.lcl症状2周随访.CustomizationFormText = "2周内随访，随访结果";
            this.lcl症状2周随访.Location = new System.Drawing.Point(36, 571);
            this.lcl症状2周随访.MaxSize = new System.Drawing.Size(0, 56);
            this.lcl症状2周随访.MinSize = new System.Drawing.Size(222, 48);
            this.lcl症状2周随访.Name = "lcl症状2周随访";
            this.lcl症状2周随访.Size = new System.Drawing.Size(606, 48);
            this.lcl症状2周随访.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl症状2周随访.Text = "2周内随访，\n随访结果";
            this.lcl症状2周随访.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl症状2周随访.TextSize = new System.Drawing.Size(79, 14);
            this.lcl症状2周随访.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "转诊";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 523);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(36, 106);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(36, 90);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(36, 96);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "转\n\n诊";
            this.emptySpaceItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(115, 20);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // lcl处理意见
            // 
            this.lcl处理意见.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl处理意见.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl处理意见.AppearanceItemCaption.Options.UseFont = true;
            this.lcl处理意见.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl处理意见.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl处理意见.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl处理意见.Control = this.txt处理意见;
            this.lcl处理意见.CustomizationFormText = "处理意见";
            this.lcl处理意见.Location = new System.Drawing.Point(0, 619);
            this.lcl处理意见.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl处理意见.MinSize = new System.Drawing.Size(222, 24);
            this.lcl处理意见.Name = "lcl处理意见";
            this.lcl处理意见.Size = new System.Drawing.Size(642, 24);
            this.lcl处理意见.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl处理意见.Text = "处理意见";
            this.lcl处理意见.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl处理意见.TextSize = new System.Drawing.Size(115, 14);
            this.lcl处理意见.TextToControlDistance = 5;
            // 
            // lcl下次随访时间
            // 
            this.lcl下次随访时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl下次随访时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseFont = true;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl下次随访时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl下次随访时间.Control = this.dte下次随访时间;
            this.lcl下次随访时间.CustomizationFormText = "下次随访时间";
            this.lcl下次随访时间.Location = new System.Drawing.Point(0, 643);
            this.lcl下次随访时间.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl下次随访时间.MinSize = new System.Drawing.Size(222, 25);
            this.lcl下次随访时间.Name = "lcl下次随访时间";
            this.lcl下次随访时间.Size = new System.Drawing.Size(265, 25);
            this.lcl下次随访时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl下次随访时间.Text = "下次随访时间";
            this.lcl下次随访时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl下次随访时间.TextSize = new System.Drawing.Size(115, 14);
            this.lcl下次随访时间.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.flowLayoutPanel19;
            this.layoutControlItem22.CustomizationFormText = "出现停止治疗时间";
            this.layoutControlItem22.Location = new System.Drawing.Point(118, 738);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(222, 29);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(524, 29);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "停止治疗及原因";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 738);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(118, 58);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(118, 58);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(118, 58);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "停止治疗及原因";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(113, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.flowLayoutPanel20;
            this.layoutControlItem23.CustomizationFormText = "停止治疗原因：";
            this.layoutControlItem23.Location = new System.Drawing.Point(118, 767);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(222, 29);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(524, 29);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "全程管理情况";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 796);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(118, 104);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(118, 104);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(118, 104);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "全程管理情况";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.flowLayoutPanel22;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(118, 796);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(524, 64);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // lcl饮酒
            // 
            this.lcl饮酒.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl饮酒.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl饮酒.AppearanceItemCaption.Options.UseFont = true;
            this.lcl饮酒.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl饮酒.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl饮酒.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl饮酒.Control = this.flp饮酒;
            this.lcl饮酒.CustomizationFormText = "饮酒";
            this.lcl饮酒.Location = new System.Drawing.Point(119, 331);
            this.lcl饮酒.MaxSize = new System.Drawing.Size(0, 30);
            this.lcl饮酒.MinSize = new System.Drawing.Size(159, 30);
            this.lcl饮酒.Name = "lcl饮酒";
            this.lcl饮酒.Size = new System.Drawing.Size(523, 30);
            this.lcl饮酒.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl饮酒.Text = "饮酒";
            this.lcl饮酒.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl饮酒.TextSize = new System.Drawing.Size(50, 14);
            this.lcl饮酒.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt编号;
            this.layoutControlItem27.CustomizationFormText = "编号";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem27.Text = "编号";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(115, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.textEdit档案编号;
            this.layoutControlItem28.CustomizationFormText = "档案编号";
            this.layoutControlItem28.Location = new System.Drawing.Point(243, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem28.Text = "档案编号";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.textEdit姓名;
            this.layoutControlItem29.CustomizationFormText = "姓名";
            this.layoutControlItem29.Location = new System.Drawing.Point(446, 0);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "姓名";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.textEdit身份证号;
            this.layoutControlItem30.CustomizationFormText = "身份证号";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem30.Text = "身份证号";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(115, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.textEdit出生日期;
            this.layoutControlItem31.CustomizationFormText = "出生日期";
            this.layoutControlItem31.Location = new System.Drawing.Point(243, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem31.Text = "出生日期";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit联系电话;
            this.layoutControlItem32.CustomizationFormText = "联系电话";
            this.layoutControlItem32.Location = new System.Drawing.Point(446, 24);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "联系电话";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.textEdit居住地址;
            this.layoutControlItem33.CustomizationFormText = "居住地址";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(642, 24);
            this.layoutControlItem33.Text = "居住地址";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(115, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // lcl治疗月序
            // 
            this.lcl治疗月序.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl治疗月序.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl治疗月序.AppearanceItemCaption.Options.UseFont = true;
            this.lcl治疗月序.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl治疗月序.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl治疗月序.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl治疗月序.Control = this.flp治疗月序;
            this.lcl治疗月序.CustomizationFormText = "治疗月序";
            this.lcl治疗月序.Location = new System.Drawing.Point(242, 72);
            this.lcl治疗月序.MinSize = new System.Drawing.Size(179, 26);
            this.lcl治疗月序.Name = "lcl治疗月序";
            this.lcl治疗月序.Size = new System.Drawing.Size(400, 26);
            this.lcl治疗月序.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl治疗月序.Text = "治疗月序";
            this.lcl治疗月序.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl治疗月序.TextSize = new System.Drawing.Size(90, 14);
            this.lcl治疗月序.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(334, 127);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(308, 29);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl督导人员
            // 
            this.lcl督导人员.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl督导人员.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl督导人员.AppearanceItemCaption.Options.UseFont = true;
            this.lcl督导人员.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl督导人员.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl督导人员.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl督导人员.Control = this.rg督导人员;
            this.lcl督导人员.CustomizationFormText = "督导人员";
            this.lcl督导人员.Location = new System.Drawing.Point(0, 98);
            this.lcl督导人员.MaxSize = new System.Drawing.Size(400, 29);
            this.lcl督导人员.MinSize = new System.Drawing.Size(400, 29);
            this.lcl督导人员.Name = "lcl督导人员";
            this.lcl督导人员.Size = new System.Drawing.Size(400, 29);
            this.lcl督导人员.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl督导人员.Text = "督导人员";
            this.lcl督导人员.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl督导人员.TextSize = new System.Drawing.Size(115, 20);
            this.lcl督导人员.TextToControlDistance = 5;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(400, 98);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(242, 29);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl化疗方案
            // 
            this.lcl化疗方案.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl化疗方案.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseFont = true;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl化疗方案.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl化疗方案.Control = this.txt化疗方案;
            this.lcl化疗方案.CustomizationFormText = "化疗方案";
            this.lcl化疗方案.Location = new System.Drawing.Point(36, 361);
            this.lcl化疗方案.MaxSize = new System.Drawing.Size(0, 26);
            this.lcl化疗方案.MinSize = new System.Drawing.Size(109, 24);
            this.lcl化疗方案.Name = "lcl化疗方案";
            this.lcl化疗方案.Size = new System.Drawing.Size(606, 24);
            this.lcl化疗方案.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl化疗方案.Text = "化疗方案";
            this.lcl化疗方案.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl化疗方案.TextSize = new System.Drawing.Size(79, 20);
            this.lcl化疗方案.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt症状及体征其他;
            this.layoutControlItem7.CustomizationFormText = "其他：";
            this.layoutControlItem7.Location = new System.Drawing.Point(119, 246);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(523, 56);
            this.layoutControlItem7.Text = "其他：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.CustomizationFormText = "肺结核患者治疗结案时填写下方内容";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 708);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 30);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(188, 30);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(642, 30);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "肺结核患者治疗结案时填写下方内容";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // lcl用药用法
            // 
            this.lcl用药用法.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl用药用法.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl用药用法.AppearanceItemCaption.Options.UseFont = true;
            this.lcl用药用法.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl用药用法.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl用药用法.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl用药用法.Control = this.rg用药用法;
            this.lcl用药用法.CustomizationFormText = "用法";
            this.lcl用药用法.Location = new System.Drawing.Point(36, 385);
            this.lcl用药用法.MaxSize = new System.Drawing.Size(208, 0);
            this.lcl用药用法.MinSize = new System.Drawing.Size(208, 28);
            this.lcl用药用法.Name = "lcl用药用法";
            this.lcl用药用法.Size = new System.Drawing.Size(208, 28);
            this.lcl用药用法.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl用药用法.Text = "用法";
            this.lcl用药用法.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl用药用法.TextSize = new System.Drawing.Size(79, 20);
            this.lcl用药用法.TextToControlDistance = 5;
            // 
            // lcl随访医生
            // 
            this.lcl随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl随访医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.lcl随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl随访医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl随访医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl随访医生.Control = this.pe随访医生签名;
            this.lcl随访医生.CustomizationFormText = "随访医生签名";
            this.lcl随访医生.Location = new System.Drawing.Point(0, 668);
            this.lcl随访医生.MaxSize = new System.Drawing.Size(0, 40);
            this.lcl随访医生.MinSize = new System.Drawing.Size(99, 40);
            this.lcl随访医生.Name = "lcl随访医生";
            this.lcl随访医生.Size = new System.Drawing.Size(265, 40);
            this.lcl随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl随访医生.Text = "随访医生签名";
            this.lcl随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl随访医生.TextSize = new System.Drawing.Size(115, 20);
            this.lcl随访医生.TextToControlDistance = 5;
            // 
            // lcl患者签名
            // 
            this.lcl患者签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl患者签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl患者签名.AppearanceItemCaption.Options.UseFont = true;
            this.lcl患者签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl患者签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl患者签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl患者签名.Control = this.pe患者签名;
            this.lcl患者签名.CustomizationFormText = "患者/家属签名";
            this.lcl患者签名.Location = new System.Drawing.Point(265, 668);
            this.lcl患者签名.MinSize = new System.Drawing.Size(79, 40);
            this.lcl患者签名.Name = "lcl患者签名";
            this.lcl患者签名.Size = new System.Drawing.Size(241, 40);
            this.lcl患者签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl患者签名.Text = "患者/家属签名";
            this.lcl患者签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl患者签名.TextSize = new System.Drawing.Size(90, 20);
            this.lcl患者签名.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(506, 668);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(136, 40);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.pe评估医生签名;
            this.layoutControlItem20.CustomizationFormText = "评估医生签名：";
            this.layoutControlItem20.Location = new System.Drawing.Point(118, 860);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(99, 40);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(244, 40);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "评估医生签名：";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(362, 860);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(280, 40);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.textEdit创建时间;
            this.layoutControlItem21.CustomizationFormText = "创建时间:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 910);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem21.Text = "创建时间:";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(115, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit所属机构;
            this.layoutControlItem26.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem26.Location = new System.Drawing.Point(424, 910);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "当前所属机构：";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.textEdit更新时间;
            this.layoutControlItem36.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem36.Location = new System.Drawing.Point(216, 910);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(208, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "最近更新时间：";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.textEdit创建机构;
            this.layoutControlItem37.CustomizationFormText = "创建机构：";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 934);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem37.Text = "创建机构：";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(115, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.textEdit创建人;
            this.layoutControlItem38.CustomizationFormText = "创建人:";
            this.layoutControlItem38.Location = new System.Drawing.Point(216, 934);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(208, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "创建人:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.textEdit修改人;
            this.layoutControlItem39.CustomizationFormText = "最近修改人：";
            this.layoutControlItem39.Location = new System.Drawing.Point(424, 934);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "最近修改人：";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt药物不良反应有;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(244, 471);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(398, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt并发症有;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(244, 497);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(398, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(265, 643);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(377, 25);
            this.emptySpaceItem13.Text = "emptySpaceItem13";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 958);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(642, 10);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 900);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(642, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(244, 385);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(398, 28);
            this.emptySpaceItem14.Text = "emptySpaceItem14";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // UC肺结核后续随访_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC肺结核后续随访_显示";
            this.Size = new System.Drawing.Size(761, 527);
            this.Load += new System.EventHandler(this.UC肺结核后续随访_显示_Load);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt并发症有.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物不良反应有.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe评估医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe患者签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pe随访医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg用药用法.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状及体征其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化疗方案.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg督导人员.Properties)).EndInit();
            this.flp治疗月序.ResumeLayout(false);
            this.flp治疗月序.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt治疗月序.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编号.Properties)).EndInit();
            this.flowLayoutPanel22.ResumeLayout(false);
            this.flowLayoutPanel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt应访视次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实际方式次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt应服药次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实际服药次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服药率.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            this.flowLayoutPanel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1完成诊疗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2死亡.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3丢失.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4转入耐药治疗.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            this.flowLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte停止治疗时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte停止治疗时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处理意见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊2周内随访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg药物不良反应.Properties)).EndInit();
            this.flp漏服药次数.ResumeLayout(false);
            this.flp漏服药次数.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt漏服药次数.Properties)).EndInit();
            this.flp药品剂型.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk1固定剂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2散装药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3板式药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4注射剂.Properties)).EndInit();
            this.flp饮酒.ResumeLayout(false);
            this.flp饮酒.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒2.Properties)).EndInit();
            this.flp吸烟.ResumeLayout(false);
            this.flp吸烟.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟2.Properties)).EndInit();
            this.flp症状体征.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk0没有症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1咳嗽咳痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2低热盗汗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3咯血血痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4胸痛消瘦.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5恶心纳差.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6关节疼痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7头痛失眠.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8视物模糊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9皮痒皮疹.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10耳鸣.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rg随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状体征)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl吸烟)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药品剂型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl漏服药次数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药物不良反应)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl并发症)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl原因)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl转诊机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状2周随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl处理意见)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl下次随访时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl饮酒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl治疗月序)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl督导人员)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl化疗方案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl用药用法)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.FlowLayoutPanel flp症状体征;
        private DevExpress.XtraEditors.CheckEdit chk0没有症状;
        private DevExpress.XtraEditors.CheckEdit chk1咳嗽咳痰;
        private DevExpress.XtraEditors.CheckEdit chk2低热盗汗;
        private DevExpress.XtraEditors.CheckEdit chk3咯血血痰;
        private DevExpress.XtraEditors.CheckEdit chk4胸痛消瘦;
        private DevExpress.XtraEditors.CheckEdit chk5恶心纳差;
        private DevExpress.XtraEditors.CheckEdit chk6关节疼痛;
        private DevExpress.XtraEditors.CheckEdit chk7头痛失眠;
        private DevExpress.XtraEditors.CheckEdit chk8视物模糊;
        private DevExpress.XtraEditors.CheckEdit chk9皮痒皮疹;
        private DevExpress.XtraEditors.CheckEdit chk10耳鸣;
        private DevExpress.XtraEditors.RadioGroup rg随访方式;
        private DevExpress.XtraEditors.DateEdit dte随访时间;
        private DevExpress.XtraLayout.LayoutControlItem lcl随访时间;
        private DevExpress.XtraLayout.LayoutControlItem lcl随访方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem lcl症状体征;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.TextEdit txt转诊机构;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.TextEdit txt并发症有;
        //private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.TextEdit txt药物不良反应有;
        private System.Windows.Forms.FlowLayoutPanel flp漏服药次数;
        private DevExpress.XtraEditors.TextEdit txt漏服药次数;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.FlowLayoutPanel flp药品剂型;
        private DevExpress.XtraEditors.CheckEdit chk1固定剂;
        private DevExpress.XtraEditors.CheckEdit chk2散装药;
        private DevExpress.XtraEditors.CheckEdit chk3板式药;
        private DevExpress.XtraEditors.CheckEdit chk4注射剂;
        private System.Windows.Forms.FlowLayoutPanel flp饮酒;
        private DevExpress.XtraEditors.TextEdit txt饮酒1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.FlowLayoutPanel flp吸烟;
        private DevExpress.XtraEditors.TextEdit txt吸烟1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem lcl吸烟;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lcl饮酒;
        private DevExpress.XtraLayout.LayoutControlItem lcl药品剂型;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem lcl漏服药次数;
        private DevExpress.XtraLayout.LayoutControlItem lcl药物不良反应;
        private DevExpress.XtraLayout.LayoutControlItem lcl并发症;
        private DevExpress.XtraLayout.LayoutControlItem lcl原因;
        private DevExpress.XtraLayout.LayoutControlItem lcl转诊机构;
        private DevExpress.XtraLayout.LayoutControlItem lcl症状2周随访;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem lcl处理意见;
        private DevExpress.XtraLayout.LayoutControlItem lcl下次随访时间;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.TextEdit txt处理意见;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.CheckEdit chk1完成诊疗;
        private DevExpress.XtraEditors.CheckEdit chk2死亡;
        private DevExpress.XtraEditors.CheckEdit chk3丢失;
        private DevExpress.XtraEditors.CheckEdit chk4转入耐药治疗;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit txt应访视次数;
        private DevExpress.XtraEditors.TextEdit txt实际方式次数;
        private DevExpress.XtraEditors.TextEdit txt应服药次数;
        private DevExpress.XtraEditors.TextEdit txt实际服药次数;
        private DevExpress.XtraEditors.TextEdit txt服药率;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt吸烟2;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txt饮酒2;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit txt编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.FlowLayoutPanel flp治疗月序;
        private DevExpress.XtraLayout.LayoutControlItem lcl治疗月序;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.RadioGroup rg督导人员;
        private DevExpress.XtraLayout.LayoutControlItem lcl督导人员;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txt治疗月序;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txt化疗方案;
        private DevExpress.XtraLayout.LayoutControlItem lcl化疗方案;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.RadioGroup rg并发症;
        private DevExpress.XtraEditors.RadioGroup rg药物不良反应;
        private DevExpress.XtraEditors.MemoEdit txt转诊2周内随访;
        private DevExpress.XtraEditors.MemoEdit txt症状及体征其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.DateEdit dte下次随访时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.DateEdit dte停止治疗时间;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        //private System.Windows.Forms.FlowLayoutPanel flp用药用法;
        private DevExpress.XtraEditors.RadioGroup rg用药用法;
        private DevExpress.XtraLayout.LayoutControlItem lcl用药用法;
        private DevExpress.XtraEditors.PictureEdit pe患者签名;
        private DevExpress.XtraEditors.PictureEdit pe随访医生签名;
        private DevExpress.XtraLayout.LayoutControlItem lcl随访医生;
        private DevExpress.XtraLayout.LayoutControlItem lcl患者签名;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.PictureEdit pe评估医生签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit textEdit所属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.TextEdit textEdit更新时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.TextEdit textEdit修改人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.SimpleButton btn新增;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        //private System.Windows.Forms.FlowLayoutPanel Panel14;

    }
}
