﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.肺结核
{
    public partial class UC肺结核第一次随访 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        string _ID = "";
        public UC肺结核第一次随访()
        {
            InitializeComponent();
        }

        public UC肺结核第一次随访(DataRow[] dr, UpdateType updateType, object ID)
        {
            InitializeComponent();

            base._UpdateType = updateType;
            _dr个人档案信息 = dr;
            _ID = ID==null?"":ID.ToString();
            _BLL = new bll肺结核第一次随访();

            //默认绑定
            textEdit档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.textEdit姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.textEdit身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.textEdit出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.textEdit居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.textEdit评估医生.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
            this.txt居民签名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
        }

        private void SetLclColor()
        {
            SetRadiogroupDateeditTexteditLCI(lcl随访时间, dte随访时间);
            SetRadiogroupDateeditTexteditLCI(lcl随访方式, radioGroup随访方式);
            SetRadiogroupDateeditTexteditLCI(lcl痰菌情况, radioGroup痰菌情况);
            SetRadiogroupDateeditTexteditLCI(lcl患者类型, radioGroup患者类型);
            SetRadiogroupDateeditTexteditLCI(lcl耐药情况, radioGroup耐药情况);
            SetCheckPanelLayoutControlItem(lcl症状体征, flowLayoutPanel2);
            SetRadiogroupDateeditTexteditLCI(lcl化疗方案, txt化疗方案);
            SetRadiogroupDateeditTexteditLCI(lcl用药用法, radioGroup用药用法);
            SetCheckPanelLayoutControlItem(lcl药品剂型, flowLayoutPanel3);
            SetRadiogroupDateeditTexteditLCI(lcl督导人员, radioGroup督导人员);
            SetRadiogroupDateeditTexteditLCI(lcl单独居室, radioGroup单独居室);
            SetRadiogroupDateeditTexteditLCI(lcl通风情况, radioGroup通风情况);
            SetEditPanelControLayoutControlItem(lcl吸烟, flowLayoutPanel4.Controls);
            SetEditPanelControLayoutControlItem(lcl饮酒, flowLayoutPanel5.Controls);
            SetEditPanelControLayoutControlItem(lcl取地点时间, panelControl1.Controls);
            SetRadiogroupDateeditTexteditLCI(lcl服药记录卡, radioGrou服药记录卡);
            SetRadiogroupDateeditTexteditLCI(lcl服药方法, radioGroup服药方法);
            SetRadiogroupDateeditTexteditLCI(lcl肺结核治疗疗程,radioGroup肺结核治疗疗程);
            SetRadiogroupDateeditTexteditLCI(lcl不规律服药,radioGroup不规律服药危害);
            SetRadiogroupDateeditTexteditLCI(lcl服药不良反应,radioGroup服药不良反应);
            SetRadiogroupDateeditTexteditLCI(lcl治疗期间复诊,radioGroup治疗期间复诊);
            SetRadiogroupDateeditTexteditLCI(lcl外出服药,radioGroup外出服药);
            SetRadiogroupDateeditTexteditLCI(lcl生活习惯,radioGroup生活习惯);
            SetRadiogroupDateeditTexteditLCI(lcl密切接触者, radioGroup密切接触者);
            SetRadiogroupDateeditTexteditLCI(lcl下次随访时间, dte下次随访时间);
            SetRadiogroupDateeditTexteditLCI(lcl医生签名, textEdit评估医生);
            SetRadiogroupDateeditTexteditLCI(lcl患者签名, txt居民签名);
        }

        private void UC肺结核第一次随访_Load(object sender, EventArgs e)
        {
            if(_UpdateType==UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);
                _BLL.NewBusiness();
                _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.个人档案编号] = _dr个人档案信息[0][tb_健康档案.个人档案编号].ToString();
            }
            else if (_UpdateType==UpdateType.Modify)
            {
                dte随访时间.Properties.ReadOnly = true;
                dte随访时间.Properties.Buttons[0].Enabled = false;

                if(_ID!=null && _ID!="")
                {
                    ((bll肺结核第一次随访)_BLL).GetBusinessByKeyEdit(_ID, true);
                }
                else
                {
                    return;
                }
            }
            _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.评估医生签名] = this.textEdit评估医生.Text.Trim();
            _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.患者签名]= this.txt居民签名.Text.Trim();
            DataTable dataSource = _BLL.CurrentBusiness.Tables[tb_MXB肺结核第一次随访.__TableName];

            DoBindingSummaryEditor(dataSource);

            //非编辑项
            this.textEdit当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.所属机构].ToString());
            this.textEdit创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.创建机构].ToString());
            this.textEdit创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.创建人].ToString());
            this.textEdit创建时间.Text = dataSource.Rows[0][tb_MXB肺结核第一次随访.创建时间].ToString();
            this.textEdit最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.修改人].ToString());
            this.textEdit最近更新时间.Text = dataSource.Rows[0][tb_MXB肺结核第一次随访.修改时间].ToString();

            //SetLclColor();
        }

        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if(dataSource==null)
            {
                return;
            }

            dataSource.Rows[0][tb_MXB肺结核第一次随访.个人档案编号] = textEdit档案编号.Text;

            DataBinder.BindingTextEdit(textEdit编号, dataSource, tb_MXB肺结核第一次随访.编号);
            DataBinder.BindingTextEditDateTime(dte随访时间, dataSource, tb_MXB肺结核第一次随访.随访时间);
            DataBinder.BindingRadioEdit(radioGroup随访方式, dataSource, tb_MXB肺结核第一次随访.随访方式);
            DataBinder.BindingRadioEdit(radioGroup痰菌情况, dataSource, tb_MXB肺结核第一次随访.痰菌情况);
            DataBinder.BindingRadioEdit(radioGroup患者类型, dataSource, tb_MXB肺结核第一次随访.患者类型);
            DataBinder.BindingRadioEdit(radioGroup耐药情况, dataSource, tb_MXB肺结核第一次随访.耐药情况);

            string temp体征 = dataSource.Rows[0][tb_MXB肺结核第一次随访.症状及体征].ToString();
            string[] arr体征 = temp体征.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr体征.Length; index++)
            {
                switch (arr体征[index])
                {
                    case "0":
                        chk0没有症状.Checked = true;
                        break;
                    case "1":
                        chk1咳嗽咳痰.Checked = true;
                        break;
                    case "2": chk2低热盗汗.Checked = true;
                        break;
                    case "3": chk3咯血血痰.Checked = true;
                        break;
                    case "4": chk4胸痛消瘦.Checked = true;
                        break;
                    case "5": chk5恶心纳差.Checked = true;
                        break;
                    case "6": chk6头痛失眠.Checked = true;
                        break;
                    case "7": chk7视物模糊.Checked = true;
                        break;
                    case "8": chk8皮肤瘙痒.Checked = true;
                        break;
                    case "9": chk9耳鸣.Checked = true;
                        break;
                    default:
                        break;
                }
            }

            DataBinder.BindingTextEdit(med症状其他, dataSource, tb_MXB肺结核第一次随访.症状及体征其他);
            DataBinder.BindingTextEdit(txt化疗方案, dataSource, tb_MXB肺结核第一次随访.化疗方案);
            DataBinder.BindingRadioEdit(radioGroup用药用法, dataSource, tb_MXB肺结核第一次随访.用药用法);

            string temp药品剂型 = dataSource.Rows[0][tb_MXB肺结核第一次随访.药品剂型].ToString();
            string[] arr药品剂型 = temp药品剂型.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for(int index = 0; index < arr药品剂型.Length; index++)
            {
                switch(arr药品剂型[index])
                {
                    case "1": chk1固定剂.Checked = true; break;
                    case "2": chk2散装药.Checked = true; break;
                    case "3": chk3板式药.Checked = true; break;
                    case "4": chk4注射剂.Checked = true; break;
                    default: break;
                }
            }

            DataBinder.BindingRadioEdit(radioGroup督导人员, dataSource, tb_MXB肺结核第一次随访.督导人员);
            DataBinder.BindingRadioEdit(radioGroup单独居室, dataSource, tb_MXB肺结核第一次随访.单独居室);
            DataBinder.BindingRadioEdit(radioGroup通风情况, dataSource, tb_MXB肺结核第一次随访.通风情况);
            DataBinder.BindingTextEdit(txt吸烟1, dataSource, tb_MXB肺结核第一次随访.吸烟1);
            DataBinder.BindingTextEdit(txt吸烟2, dataSource, tb_MXB肺结核第一次随访.吸烟2);
            DataBinder.BindingTextEdit(txt饮酒1, dataSource, tb_MXB肺结核第一次随访.饮酒1);
            DataBinder.BindingTextEdit(txt饮酒2, dataSource, tb_MXB肺结核第一次随访.饮酒2);
            DataBinder.BindingTextEdit(txt取药地点, dataSource, tb_MXB肺结核第一次随访.取药地点);
            DataBinder.BindingTextEditDateTime(dateEdit取药时间, dataSource, tb_MXB肺结核第一次随访.取药时间);
            DataBinder.BindingRadioEdit(radioGrou服药记录卡, dataSource, tb_MXB肺结核第一次随访.服药记录卡的填写);
            DataBinder.BindingRadioEdit(radioGroup服药方法, dataSource, tb_MXB肺结核第一次随访.服药方法及药品存放);
            DataBinder.BindingRadioEdit(radioGroup肺结核治疗疗程, dataSource, tb_MXB肺结核第一次随访.肺结核治疗疗程);
            DataBinder.BindingRadioEdit(radioGroup不规律服药危害, dataSource, tb_MXB肺结核第一次随访.不规律服药危害);
            DataBinder.BindingRadioEdit(radioGroup服药不良反应, dataSource, tb_MXB肺结核第一次随访.服药后不良反应及处理);
            DataBinder.BindingRadioEdit(radioGroup治疗期间复诊, dataSource, tb_MXB肺结核第一次随访.治疗期间复诊查痰);
            DataBinder.BindingRadioEdit(radioGroup外出服药, dataSource, tb_MXB肺结核第一次随访.外出期间如何坚持服药);
            DataBinder.BindingRadioEdit(radioGroup生活习惯, dataSource, tb_MXB肺结核第一次随访.生活习惯及注意事项);
            DataBinder.BindingRadioEdit(radioGroup密切接触者, dataSource, tb_MXB肺结核第一次随访.密切接触者检查);
            DataBinder.BindingTextEditDateTime(dte下次随访时间, dataSource, tb_MXB肺结核第一次随访.下次随访时间);
            DataBinder.BindingTextEdit(textEdit评估医生, dataSource, tb_MXB肺结核第一次随访.评估医生签名);
            DataBinder.BindingTextEdit(txt居民签名, dataSource, tb_MXB肺结核第一次随访.患者签名);

            SetLclColor();
        }

        //protected void UpdateLastControl()
        //{
        //    try
        //    {
        //        if (ActiveControl == null) return;
        //        Control ctl = ActiveControl;
        //        txtFocusForSave.Focus();
        //        ActiveControl = ctl;
        //    }
        //    catch (Exception ex)
        //    { Msg.ShowException(ex); }
        //}

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(dte随访时间.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                dte随访时间.Focus();
                return false;
            }
            #region  允许修改下次随访时间(设置默认值)
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt下次随访时间.Text)))
            //{
            //    Msg.Warning("下次随访日期不能为空!");
            //    txt下次随访时间.Focus();
            //    return false;
            //}
            #endregion

            if (this.dte随访时间.DateTime > Convert.ToDateTime(_BLL.ServiceDateTime))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                dte随访时间.Focus();
                return false;
            }
            return true;
        }


        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();

            if (!ValidatingSummaryData()) 
            {
                return; //检查主表数据合法性
            }
            if (!Msg.AskQuestion("信息保存后，‘随访日期’将不允许修改，确认保存信息？")) 
            {
                return;
            }
            if (_UpdateType == UpdateType.None) 
            {
                return;
            }

            string str症状 = "";
            if (chk0没有症状.Checked)
            {
                str症状 = "0";
            }
            else
            {
                if (chk1咳嗽咳痰.Checked) { str症状 += "1,"; }
                if (chk2低热盗汗.Checked) { str症状 += "2,"; }
                if (chk3咯血血痰.Checked) { str症状 += "3,"; }
                if (chk4胸痛消瘦.Checked) { str症状 += "4,"; }
                if (chk5恶心纳差.Checked) { str症状 += "5,"; }
                if (chk6头痛失眠.Checked) { str症状 += "6,"; }
                if (chk7视物模糊.Checked) { str症状 += "7,"; }
                if (chk8皮肤瘙痒.Checked) { str症状 += "8,"; }
                if (chk9耳鸣.Checked) { str症状 += "9,"; }

                if(str症状.Length > 0)
                {
                    str症状=str症状.Remove(str症状.Length - 1);
                }
            }

            _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.症状及体征] = str症状;

            string str药品剂型 = "";
            if (chk1固定剂.Checked) { str药品剂型 += "1,"; }
            if (chk2散装药.Checked) { str药品剂型 += "2,"; }
            if (chk3板式药.Checked) { str药品剂型 += "3,"; }
            if (chk4注射剂.Checked) { str药品剂型 += "4,"; }
            if (str药品剂型.Length >= 1)
            {
                str药品剂型=str药品剂型.Remove(str药品剂型.Length - 1);
            }
            _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.药品剂型] = str药品剂型;

            if (_UpdateType == UpdateType.Modify)
            {
                
                _BLL.WriteLog(); //注意:只有修改状态下保存修改日志
            }

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据
            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法
            if (result.Success)
            {
                //Msg.ShowInformation("保存成功!");
                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面
                UC肺结核第一次随访_显示 control = new UC肺结核第一次随访_显示(_dr个人档案信息, _BLL.DataBinder.Rows[0][tb_MXB肺结核第一次随访.创建时间]);
                ShowControl(control, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("保存失败!"+result.Description);
            }
        }

        private void chk0没有症状_CheckedChanged(object sender, EventArgs e)
        {
            if (chk0没有症状.Checked)
            {
                chk1咳嗽咳痰.Enabled = false;
                chk2低热盗汗.Enabled = false;
                chk3咯血血痰.Enabled = false;
                chk4胸痛消瘦.Enabled = false;
                chk5恶心纳差.Enabled = false;
                chk6头痛失眠.Enabled = false;
                chk7视物模糊.Enabled = false;
                chk8皮肤瘙痒.Enabled = false;
                chk9耳鸣.Enabled = false;
            }
            else
            {
                chk1咳嗽咳痰.Enabled = true;
                chk2低热盗汗.Enabled = true;
                chk3咯血血痰.Enabled = true;
                chk4胸痛消瘦.Enabled = true;
                chk5恶心纳差.Enabled = true;
                chk6头痛失眠.Enabled = true;
                chk7视物模糊.Enabled = true;
                chk8皮肤瘙痒.Enabled = true;
                chk9耳鸣.Enabled = true;
            }
        }
    }
}
