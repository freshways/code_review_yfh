﻿namespace AtomEHR.公共卫生.Module.个人健康.肺结核
{
    partial class UC肺结核第一次随访_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC肺结核第一次随访_显示));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dateEdit取药时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt取药地点 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radioGroup治疗期间复诊 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup服药不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup肺结核治疗疗程 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup服药方法 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup外出服药 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup不规律服药危害 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup生活习惯 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup密切接触者 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGrou服药记录卡 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt饮酒1 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txt饮酒2 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt吸烟1 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txt吸烟2 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.radioGroup通风情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup单独居室 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup督导人员 = new DevExpress.XtraEditors.RadioGroup();
            this.med症状其他 = new DevExpress.XtraEditors.MemoEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk1固定剂 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2散装药 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3板式药 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4注射剂 = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup用药用法 = new DevExpress.XtraEditors.RadioGroup();
            this.txt化疗方案 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk0没有症状 = new DevExpress.XtraEditors.CheckEdit();
            this.chk1咳嗽咳痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2低热盗汗 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3咯血血痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4胸痛消瘦 = new DevExpress.XtraEditors.CheckEdit();
            this.chk5恶心纳差 = new DevExpress.XtraEditors.CheckEdit();
            this.chk6头痛失眠 = new DevExpress.XtraEditors.CheckEdit();
            this.chk7视物模糊 = new DevExpress.XtraEditors.CheckEdit();
            this.chk8皮肤瘙痒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk9耳鸣 = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup耐药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup痰菌情况 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup患者类型 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt居民签名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit评估医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit编号 = new DevExpress.XtraEditors.TextEdit();
            this.dte下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl医生签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl随访时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl患者签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl随访方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl耐药情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl痰菌情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl患者类型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl化疗方案 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl用药用法 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl症状体征 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl督导人员 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl单独居室 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl通风情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl吸烟 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl饮酒 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl服药记录卡 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl不规律服药 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl服药方法 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl肺结核治疗疗程 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl服药不良反应 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl取地点时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcl下次随访时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl药品剂型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl治疗期间复诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl外出服药 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl生活习惯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcl密切接触者 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn新增 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit取药时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit取药时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt取药地点.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup治疗期间复诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup肺结核治疗疗程.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药方法.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup外出服药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup不规律服药危害.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup生活习惯.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup密切接触者.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGrou服药记录卡.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒2.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup通风情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup单独居室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup督导人员.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med症状其他.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1固定剂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2散装药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3板式药.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4注射剂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup用药用法.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化疗方案.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk0没有症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1咳嗽咳痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2低热盗汗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3咯血血痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4胸痛消瘦.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5恶心纳差.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6头痛失眠.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7视物模糊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8皮肤瘙痒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9耳鸣.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup耐药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup痰菌情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup患者类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit评估医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl医生签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl耐药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl痰菌情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者类型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl化疗方案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl用药用法)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状体征)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl督导人员)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl单独居室)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl通风情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl吸烟)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl饮酒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药记录卡)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl不规律服药)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药方法)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl肺结核治疗疗程)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药不良反应)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl取地点时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl下次随访时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药品剂型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl治疗期间复诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl外出服药)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl生活习惯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl密切接触者)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(136, 533);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.radioGroup治疗期间复诊);
            this.layoutControl1.Controls.Add(this.radioGroup服药不良反应);
            this.layoutControl1.Controls.Add(this.radioGroup肺结核治疗疗程);
            this.layoutControl1.Controls.Add(this.radioGroup服药方法);
            this.layoutControl1.Controls.Add(this.radioGroup外出服药);
            this.layoutControl1.Controls.Add(this.radioGroup不规律服药危害);
            this.layoutControl1.Controls.Add(this.radioGroup生活习惯);
            this.layoutControl1.Controls.Add(this.radioGroup密切接触者);
            this.layoutControl1.Controls.Add(this.radioGrou服药记录卡);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.radioGroup通风情况);
            this.layoutControl1.Controls.Add(this.radioGroup单独居室);
            this.layoutControl1.Controls.Add(this.radioGroup督导人员);
            this.layoutControl1.Controls.Add(this.med症状其他);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.radioGroup用药用法);
            this.layoutControl1.Controls.Add(this.txt化疗方案);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.radioGroup耐药情况);
            this.layoutControl1.Controls.Add(this.radioGroup痰菌情况);
            this.layoutControl1.Controls.Add(this.radioGroup患者类型);
            this.layoutControl1.Controls.Add(this.radioGroup随访方式);
            this.layoutControl1.Controls.Add(this.txt居民签名);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit评估医生);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.textEdit编号);
            this.layoutControl1.Controls.Add(this.dte下次随访时间);
            this.layoutControl1.Controls.Add(this.dte随访时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(136, 28);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(597, 505);
            this.layoutControl1.TabIndex = 125;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dateEdit取药时间);
            this.panelControl1.Controls.Add(this.txt取药地点);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Location = new System.Drawing.Point(165, 552);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(412, 54);
            this.panelControl1.TabIndex = 79;
            // 
            // dateEdit取药时间
            // 
            this.dateEdit取药时间.EditValue = null;
            this.dateEdit取药时间.Location = new System.Drawing.Point(52, 25);
            this.dateEdit取药时间.Name = "dateEdit取药时间";
            this.dateEdit取药时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit取药时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit取药时间.Size = new System.Drawing.Size(122, 20);
            this.dateEdit取药时间.TabIndex = 3;
            // 
            // txt取药地点
            // 
            this.txt取药地点.Location = new System.Drawing.Point(52, 3);
            this.txt取药地点.Name = "txt取药地点";
            this.txt取药地点.Size = new System.Drawing.Size(252, 20);
            this.txt取药地点.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "时间：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "地点：";
            // 
            // radioGroup治疗期间复诊
            // 
            this.radioGroup治疗期间复诊.Location = new System.Drawing.Point(165, 750);
            this.radioGroup治疗期间复诊.Name = "radioGroup治疗期间复诊";
            this.radioGroup治疗期间复诊.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup治疗期间复诊.Size = new System.Drawing.Size(138, 24);
            this.radioGroup治疗期间复诊.StyleController = this.layoutControl1;
            this.radioGroup治疗期间复诊.TabIndex = 78;
            // 
            // radioGroup服药不良反应
            // 
            this.radioGroup服药不良反应.Location = new System.Drawing.Point(165, 722);
            this.radioGroup服药不良反应.Name = "radioGroup服药不良反应";
            this.radioGroup服药不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup服药不良反应.Size = new System.Drawing.Size(138, 24);
            this.radioGroup服药不良反应.StyleController = this.layoutControl1;
            this.radioGroup服药不良反应.TabIndex = 77;
            // 
            // radioGroup肺结核治疗疗程
            // 
            this.radioGroup肺结核治疗疗程.Location = new System.Drawing.Point(165, 666);
            this.radioGroup肺结核治疗疗程.Name = "radioGroup肺结核治疗疗程";
            this.radioGroup肺结核治疗疗程.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup肺结核治疗疗程.Size = new System.Drawing.Size(138, 24);
            this.radioGroup肺结核治疗疗程.StyleController = this.layoutControl1;
            this.radioGroup肺结核治疗疗程.TabIndex = 76;
            // 
            // radioGroup服药方法
            // 
            this.radioGroup服药方法.Location = new System.Drawing.Point(165, 638);
            this.radioGroup服药方法.Name = "radioGroup服药方法";
            this.radioGroup服药方法.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup服药方法.Size = new System.Drawing.Size(138, 24);
            this.radioGroup服药方法.StyleController = this.layoutControl1;
            this.radioGroup服药方法.TabIndex = 75;
            // 
            // radioGroup外出服药
            // 
            this.radioGroup外出服药.Location = new System.Drawing.Point(165, 778);
            this.radioGroup外出服药.Name = "radioGroup外出服药";
            this.radioGroup外出服药.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup外出服药.Size = new System.Drawing.Size(138, 24);
            this.radioGroup外出服药.StyleController = this.layoutControl1;
            this.radioGroup外出服药.TabIndex = 74;
            // 
            // radioGroup不规律服药危害
            // 
            this.radioGroup不规律服药危害.Location = new System.Drawing.Point(165, 694);
            this.radioGroup不规律服药危害.Name = "radioGroup不规律服药危害";
            this.radioGroup不规律服药危害.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup不规律服药危害.Size = new System.Drawing.Size(138, 24);
            this.radioGroup不规律服药危害.StyleController = this.layoutControl1;
            this.radioGroup不规律服药危害.TabIndex = 73;
            // 
            // radioGroup生活习惯
            // 
            this.radioGroup生活习惯.Location = new System.Drawing.Point(165, 806);
            this.radioGroup生活习惯.Name = "radioGroup生活习惯";
            this.radioGroup生活习惯.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup生活习惯.Size = new System.Drawing.Size(138, 24);
            this.radioGroup生活习惯.StyleController = this.layoutControl1;
            this.radioGroup生活习惯.TabIndex = 72;
            // 
            // radioGroup密切接触者
            // 
            this.radioGroup密切接触者.Location = new System.Drawing.Point(165, 834);
            this.radioGroup密切接触者.Name = "radioGroup密切接触者";
            this.radioGroup密切接触者.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGroup密切接触者.Size = new System.Drawing.Size(138, 24);
            this.radioGroup密切接触者.StyleController = this.layoutControl1;
            this.radioGroup密切接触者.TabIndex = 71;
            // 
            // radioGrou服药记录卡
            // 
            this.radioGrou服药记录卡.Location = new System.Drawing.Point(165, 610);
            this.radioGrou服药记录卡.Name = "radioGrou服药记录卡";
            this.radioGrou服药记录卡.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 掌握"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 未掌握")});
            this.radioGrou服药记录卡.Size = new System.Drawing.Size(138, 24);
            this.radioGrou服药记录卡.StyleController = this.layoutControl1;
            this.radioGrou服药记录卡.TabIndex = 70;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.txt饮酒1);
            this.flowLayoutPanel5.Controls.Add(this.label3);
            this.flowLayoutPanel5.Controls.Add(this.txt饮酒2);
            this.flowLayoutPanel5.Controls.Add(this.label4);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(165, 522);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(412, 26);
            this.flowLayoutPanel5.TabIndex = 69;
            // 
            // txt饮酒1
            // 
            this.txt饮酒1.Location = new System.Drawing.Point(3, 3);
            this.txt饮酒1.Name = "txt饮酒1";
            this.txt饮酒1.Size = new System.Drawing.Size(59, 20);
            this.txt饮酒1.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 7, 6, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "/";
            // 
            // txt饮酒2
            // 
            this.txt饮酒2.Location = new System.Drawing.Point(91, 3);
            this.txt饮酒2.Name = "txt饮酒2";
            this.txt饮酒2.Size = new System.Drawing.Size(62, 20);
            this.txt饮酒2.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(162, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "两/天";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.txt吸烟1);
            this.flowLayoutPanel4.Controls.Add(this.label1);
            this.flowLayoutPanel4.Controls.Add(this.txt吸烟2);
            this.flowLayoutPanel4.Controls.Add(this.label2);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(165, 492);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(412, 26);
            this.flowLayoutPanel4.TabIndex = 68;
            // 
            // txt吸烟1
            // 
            this.txt吸烟1.Location = new System.Drawing.Point(3, 3);
            this.txt吸烟1.Name = "txt吸烟1";
            this.txt吸烟1.Size = new System.Drawing.Size(59, 20);
            this.txt吸烟1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "/";
            // 
            // txt吸烟2
            // 
            this.txt吸烟2.Location = new System.Drawing.Point(91, 3);
            this.txt吸烟2.Name = "txt吸烟2";
            this.txt吸烟2.Size = new System.Drawing.Size(62, 20);
            this.txt吸烟2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "支/天";
            // 
            // radioGroup通风情况
            // 
            this.radioGroup通风情况.Location = new System.Drawing.Point(165, 463);
            this.radioGroup通风情况.Name = "radioGroup通风情况";
            this.radioGroup通风情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3 差")});
            this.radioGroup通风情况.Size = new System.Drawing.Size(194, 25);
            this.radioGroup通风情况.StyleController = this.layoutControl1;
            this.radioGroup通风情况.TabIndex = 67;
            // 
            // radioGroup单独居室
            // 
            this.radioGroup单独居室.Location = new System.Drawing.Point(165, 432);
            this.radioGroup单独居室.Name = "radioGroup单独居室";
            this.radioGroup单独居室.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 无")});
            this.radioGroup单独居室.Size = new System.Drawing.Size(132, 27);
            this.radioGroup单独居室.StyleController = this.layoutControl1;
            this.radioGroup单独居室.TabIndex = 66;
            // 
            // radioGroup督导人员
            // 
            this.radioGroup督导人员.Location = new System.Drawing.Point(165, 402);
            this.radioGroup督导人员.Name = "radioGroup督导人员";
            this.radioGroup督导人员.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 医生"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 家属"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3 自服药"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "4 其他")});
            this.radioGroup督导人员.Size = new System.Drawing.Size(258, 26);
            this.radioGroup督导人员.StyleController = this.layoutControl1;
            this.radioGroup督导人员.TabIndex = 65;
            // 
            // med症状其他
            // 
            this.med症状其他.Location = new System.Drawing.Point(197, 266);
            this.med症状其他.Name = "med症状其他";
            this.med症状其他.Size = new System.Drawing.Size(380, 45);
            this.med症状其他.StyleController = this.layoutControl1;
            this.med症状其他.TabIndex = 64;
            this.med症状其他.UseOptimizedRendering = true;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.chk1固定剂);
            this.flowLayoutPanel3.Controls.Add(this.chk2散装药);
            this.flowLayoutPanel3.Controls.Add(this.chk3板式药);
            this.flowLayoutPanel3.Controls.Add(this.chk4注射剂);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(165, 373);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(412, 25);
            this.flowLayoutPanel3.TabIndex = 62;
            // 
            // chk1固定剂
            // 
            this.chk1固定剂.Location = new System.Drawing.Point(3, 3);
            this.chk1固定剂.Name = "chk1固定剂";
            this.chk1固定剂.Properties.Caption = "1 固定剂量符合制剂";
            this.chk1固定剂.Size = new System.Drawing.Size(138, 19);
            this.chk1固定剂.TabIndex = 4;
            // 
            // chk2散装药
            // 
            this.chk2散装药.Location = new System.Drawing.Point(147, 3);
            this.chk2散装药.Name = "chk2散装药";
            this.chk2散装药.Properties.Caption = "2 散装药";
            this.chk2散装药.Size = new System.Drawing.Size(75, 19);
            this.chk2散装药.TabIndex = 5;
            // 
            // chk3板式药
            // 
            this.chk3板式药.Location = new System.Drawing.Point(228, 3);
            this.chk3板式药.Name = "chk3板式药";
            this.chk3板式药.Properties.Caption = "3 板式组合药";
            this.chk3板式药.Size = new System.Drawing.Size(97, 19);
            this.chk3板式药.TabIndex = 6;
            // 
            // chk4注射剂
            // 
            this.chk4注射剂.Location = new System.Drawing.Point(331, 3);
            this.chk4注射剂.Name = "chk4注射剂";
            this.chk4注射剂.Properties.Caption = "4 注射剂";
            this.chk4注射剂.Size = new System.Drawing.Size(77, 19);
            this.chk4注射剂.TabIndex = 7;
            // 
            // radioGroup用药用法
            // 
            this.radioGroup用药用法.Location = new System.Drawing.Point(165, 341);
            this.radioGroup用药用法.Name = "radioGroup用药用法";
            this.radioGroup用药用法.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 每日"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 间隔")});
            this.radioGroup用药用法.Size = new System.Drawing.Size(152, 28);
            this.radioGroup用药用法.StyleController = this.layoutControl1;
            this.radioGroup用药用法.TabIndex = 61;
            // 
            // txt化疗方案
            // 
            this.txt化疗方案.Location = new System.Drawing.Point(165, 315);
            this.txt化疗方案.Name = "txt化疗方案";
            this.txt化疗方案.Size = new System.Drawing.Size(412, 20);
            this.txt化疗方案.StyleController = this.layoutControl1;
            this.txt化疗方案.TabIndex = 60;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.chk0没有症状);
            this.flowLayoutPanel2.Controls.Add(this.chk1咳嗽咳痰);
            this.flowLayoutPanel2.Controls.Add(this.chk2低热盗汗);
            this.flowLayoutPanel2.Controls.Add(this.chk3咯血血痰);
            this.flowLayoutPanel2.Controls.Add(this.chk4胸痛消瘦);
            this.flowLayoutPanel2.Controls.Add(this.chk5恶心纳差);
            this.flowLayoutPanel2.Controls.Add(this.chk6头痛失眠);
            this.flowLayoutPanel2.Controls.Add(this.chk7视物模糊);
            this.flowLayoutPanel2.Controls.Add(this.chk8皮肤瘙痒);
            this.flowLayoutPanel2.Controls.Add(this.chk9耳鸣);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(164, 185);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(413, 77);
            this.flowLayoutPanel2.TabIndex = 59;
            // 
            // chk0没有症状
            // 
            this.chk0没有症状.Location = new System.Drawing.Point(3, 3);
            this.chk0没有症状.Name = "chk0没有症状";
            this.chk0没有症状.Properties.Caption = "0 没有症状";
            this.chk0没有症状.Size = new System.Drawing.Size(90, 19);
            this.chk0没有症状.TabIndex = 0;
            // 
            // chk1咳嗽咳痰
            // 
            this.chk1咳嗽咳痰.Location = new System.Drawing.Point(99, 3);
            this.chk1咳嗽咳痰.Name = "chk1咳嗽咳痰";
            this.chk1咳嗽咳痰.Properties.Caption = "1 咳嗽咳痰";
            this.chk1咳嗽咳痰.Size = new System.Drawing.Size(90, 19);
            this.chk1咳嗽咳痰.TabIndex = 1;
            // 
            // chk2低热盗汗
            // 
            this.chk2低热盗汗.Location = new System.Drawing.Point(195, 3);
            this.chk2低热盗汗.Name = "chk2低热盗汗";
            this.chk2低热盗汗.Properties.Caption = "2 低热盗汗";
            this.chk2低热盗汗.Size = new System.Drawing.Size(90, 19);
            this.chk2低热盗汗.TabIndex = 2;
            // 
            // chk3咯血血痰
            // 
            this.chk3咯血血痰.Location = new System.Drawing.Point(291, 3);
            this.chk3咯血血痰.Name = "chk3咯血血痰";
            this.chk3咯血血痰.Properties.Caption = "3 咯血或血痰";
            this.chk3咯血血痰.Size = new System.Drawing.Size(95, 19);
            this.chk3咯血血痰.TabIndex = 3;
            // 
            // chk4胸痛消瘦
            // 
            this.chk4胸痛消瘦.Location = new System.Drawing.Point(3, 28);
            this.chk4胸痛消瘦.Name = "chk4胸痛消瘦";
            this.chk4胸痛消瘦.Properties.Caption = "4 胸痛消瘦";
            this.chk4胸痛消瘦.Size = new System.Drawing.Size(90, 19);
            this.chk4胸痛消瘦.TabIndex = 4;
            // 
            // chk5恶心纳差
            // 
            this.chk5恶心纳差.Location = new System.Drawing.Point(99, 28);
            this.chk5恶心纳差.Name = "chk5恶心纳差";
            this.chk5恶心纳差.Properties.Caption = "5 恶心纳差";
            this.chk5恶心纳差.Size = new System.Drawing.Size(90, 19);
            this.chk5恶心纳差.TabIndex = 5;
            // 
            // chk6头痛失眠
            // 
            this.chk6头痛失眠.Location = new System.Drawing.Point(195, 28);
            this.chk6头痛失眠.Name = "chk6头痛失眠";
            this.chk6头痛失眠.Properties.Caption = "6 头痛失眠";
            this.chk6头痛失眠.Size = new System.Drawing.Size(90, 19);
            this.chk6头痛失眠.TabIndex = 6;
            // 
            // chk7视物模糊
            // 
            this.chk7视物模糊.Location = new System.Drawing.Point(291, 28);
            this.chk7视物模糊.Name = "chk7视物模糊";
            this.chk7视物模糊.Properties.Caption = "7 视物模糊";
            this.chk7视物模糊.Size = new System.Drawing.Size(90, 19);
            this.chk7视物模糊.TabIndex = 7;
            // 
            // chk8皮肤瘙痒
            // 
            this.chk8皮肤瘙痒.Location = new System.Drawing.Point(3, 53);
            this.chk8皮肤瘙痒.Name = "chk8皮肤瘙痒";
            this.chk8皮肤瘙痒.Properties.Caption = "8 皮肤瘙痒、皮疹";
            this.chk8皮肤瘙痒.Size = new System.Drawing.Size(117, 19);
            this.chk8皮肤瘙痒.TabIndex = 8;
            // 
            // chk9耳鸣
            // 
            this.chk9耳鸣.Location = new System.Drawing.Point(126, 53);
            this.chk9耳鸣.Name = "chk9耳鸣";
            this.chk9耳鸣.Properties.Caption = "9 耳鸣、听力下降";
            this.chk9耳鸣.Size = new System.Drawing.Size(116, 19);
            this.chk9耳鸣.TabIndex = 9;
            // 
            // radioGroup耐药情况
            // 
            this.radioGroup耐药情况.Location = new System.Drawing.Point(360, 156);
            this.radioGroup耐药情况.Name = "radioGroup耐药情况";
            this.radioGroup耐药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 耐药"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 非耐药"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3 未检测")});
            this.radioGroup耐药情况.Size = new System.Drawing.Size(217, 25);
            this.radioGroup耐药情况.StyleController = this.layoutControl1;
            this.radioGroup耐药情况.TabIndex = 58;
            // 
            // radioGroup痰菌情况
            // 
            this.radioGroup痰菌情况.Location = new System.Drawing.Point(360, 127);
            this.radioGroup痰菌情况.Name = "radioGroup痰菌情况";
            this.radioGroup痰菌情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 阳性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3 未查痰")});
            this.radioGroup痰菌情况.Size = new System.Drawing.Size(217, 25);
            this.radioGroup痰菌情况.StyleController = this.layoutControl1;
            this.radioGroup痰菌情况.TabIndex = 57;
            // 
            // radioGroup患者类型
            // 
            this.radioGroup患者类型.Location = new System.Drawing.Point(165, 156);
            this.radioGroup患者类型.Name = "radioGroup患者类型";
            this.radioGroup患者类型.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 初治"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 复治")});
            this.radioGroup患者类型.Size = new System.Drawing.Size(116, 25);
            this.radioGroup患者类型.StyleController = this.layoutControl1;
            this.radioGroup患者类型.TabIndex = 56;
            // 
            // radioGroup随访方式
            // 
            this.radioGroup随访方式.Location = new System.Drawing.Point(165, 127);
            this.radioGroup随访方式.Name = "radioGroup随访方式";
            this.radioGroup随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1 门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2 家庭")});
            this.radioGroup随访方式.Size = new System.Drawing.Size(116, 25);
            this.radioGroup随访方式.StyleController = this.layoutControl1;
            this.radioGroup随访方式.TabIndex = 55;
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(412, 887);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Size = new System.Drawing.Size(144, 20);
            this.txt居民签名.StyleController = this.layoutControl1;
            this.txt居民签名.TabIndex = 54;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(470, 935);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近修改人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(107, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 50;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(278, 935);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(93, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 49;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(98, 935);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(91, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 48;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(470, 911);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit当前所属机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(107, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 47;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(278, 911);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近更新时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(93, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 46;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(98, 911);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(91, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 45;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(473, 54);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(104, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 7;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(138, 78);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(439, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 9;
            // 
            // textEdit评估医生
            // 
            this.textEdit评估医生.Location = new System.Drawing.Point(166, 887);
            this.textEdit评估医生.Name = "textEdit评估医生";
            this.textEdit评估医生.Size = new System.Drawing.Size(137, 20);
            this.textEdit评估医生.StyleController = this.layoutControl1;
            this.textEdit评估医生.TabIndex = 44;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(298, 54);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(86, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 6;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(138, 54);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(71, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 5;
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(473, 30);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.ReadOnly = true;
            this.textEdit姓名.Size = new System.Drawing.Size(104, 20);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 4;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(298, 30);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(86, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 3;
            // 
            // textEdit编号
            // 
            this.textEdit编号.Location = new System.Drawing.Point(138, 30);
            this.textEdit编号.Name = "textEdit编号";
            this.textEdit编号.Size = new System.Drawing.Size(71, 20);
            this.textEdit编号.StyleController = this.layoutControl1;
            this.textEdit编号.TabIndex = 2;
            // 
            // dte下次随访时间
            // 
            this.dte下次随访时间.EditValue = null;
            this.dte下次随访时间.Location = new System.Drawing.Point(166, 862);
            this.dte下次随访时间.Name = "dte下次随访时间";
            this.dte下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte下次随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte下次随访时间.Size = new System.Drawing.Size(137, 20);
            this.dte下次随访时间.StyleController = this.layoutControl1;
            this.dte下次随访时间.TabIndex = 43;
            // 
            // dte随访时间
            // 
            this.dte随访时间.EditValue = null;
            this.dte随访时间.Location = new System.Drawing.Point(165, 102);
            this.dte随访时间.Name = "dte随访时间";
            this.dte随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访时间.Size = new System.Drawing.Size(120, 20);
            this.dte随访时间.StyleController = this.layoutControl1;
            this.dte随访时间.TabIndex = 10;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.lcl医生签名,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem12,
            this.lcl随访时间,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem22,
            this.lcl患者签名,
            this.lcl随访方式,
            this.lcl耐药情况,
            this.layoutControlItem18,
            this.lcl痰菌情况,
            this.lcl患者类型,
            this.emptySpaceItem2,
            this.lcl化疗方案,
            this.lcl用药用法,
            this.lcl症状体征,
            this.layoutControlItem20,
            this.lcl督导人员,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.lcl单独居室,
            this.lcl通风情况,
            this.emptySpaceItem8,
            this.lcl吸烟,
            this.lcl饮酒,
            this.emptySpaceItem10,
            this.lcl服药记录卡,
            this.lcl不规律服药,
            this.lcl服药方法,
            this.lcl肺结核治疗疗程,
            this.lcl服药不良反应,
            this.lcl取地点时间,
            this.lcl下次随访时间,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.emptySpaceItem1,
            this.lcl药品剂型,
            this.emptySpaceItem4,
            this.emptySpaceItem9,
            this.emptySpaceItem7,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.emptySpaceItem11,
            this.lcl治疗期间复诊,
            this.emptySpaceItem18,
            this.lcl外出服药,
            this.emptySpaceItem19,
            this.lcl生活习惯,
            this.emptySpaceItem20,
            this.lcl密切接触者,
            this.emptySpaceItem21,
            this.emptySpaceItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(580, 958);
            this.layoutControlGroup1.Text = "肺结核患者第一次入户随访记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit编号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "编 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(130, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(210, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(385, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // lcl医生签名
            // 
            this.lcl医生签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl医生签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl医生签名.AppearanceItemCaption.Options.UseFont = true;
            this.lcl医生签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl医生签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl医生签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl医生签名.Control = this.textEdit评估医生;
            this.lcl医生签名.CustomizationFormText = "联系电话";
            this.lcl医生签名.Location = new System.Drawing.Point(0, 857);
            this.lcl医生签名.MinSize = new System.Drawing.Size(171, 24);
            this.lcl医生签名.Name = "lcl医生签名";
            this.lcl医生签名.Size = new System.Drawing.Size(304, 24);
            this.lcl医生签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl医生签名.Text = "评估医生签名";
            this.lcl医生签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl医生签名.TextSize = new System.Drawing.Size(158, 14);
            this.lcl医生签名.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 905);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构: ";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(190, 905);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(182, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(372, 905);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人:";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(578, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(130, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // lcl随访时间
            // 
            this.lcl随访时间.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lcl随访时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl随访时间.AppearanceItemCaption.Options.UseFont = true;
            this.lcl随访时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl随访时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl随访时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl随访时间.Control = this.dte随访时间;
            this.lcl随访时间.CustomizationFormText = "随访日期";
            this.lcl随访时间.Location = new System.Drawing.Point(0, 72);
            this.lcl随访时间.MinSize = new System.Drawing.Size(50, 25);
            this.lcl随访时间.Name = "lcl随访时间";
            this.lcl随访时间.Size = new System.Drawing.Size(286, 25);
            this.lcl随访时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl随访时间.Text = "随访时间";
            this.lcl随访时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl随访时间.TextSize = new System.Drawing.Size(157, 20);
            this.lcl随访时间.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(130, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(210, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.textEdit联系电话;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(385, 24);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "联系电话";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // lcl患者签名
            // 
            this.lcl患者签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lcl患者签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl患者签名.AppearanceItemCaption.Options.UseFont = true;
            this.lcl患者签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl患者签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl患者签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl患者签名.Control = this.txt居民签名;
            this.lcl患者签名.CustomizationFormText = "居民/家属签名";
            this.lcl患者签名.Location = new System.Drawing.Point(304, 857);
            this.lcl患者签名.Name = "lcl患者签名";
            this.lcl患者签名.Size = new System.Drawing.Size(253, 24);
            this.lcl患者签名.Text = "患者(家属)签名";
            this.lcl患者签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl患者签名.TextSize = new System.Drawing.Size(100, 20);
            this.lcl患者签名.TextToControlDistance = 5;
            // 
            // lcl随访方式
            // 
            this.lcl随访方式.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl随访方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl随访方式.AppearanceItemCaption.Options.UseFont = true;
            this.lcl随访方式.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl随访方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl随访方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl随访方式.Control = this.radioGroup随访方式;
            this.lcl随访方式.CustomizationFormText = "随访方式";
            this.lcl随访方式.Location = new System.Drawing.Point(0, 97);
            this.lcl随访方式.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl随访方式.MinSize = new System.Drawing.Size(109, 29);
            this.lcl随访方式.Name = "lcl随访方式";
            this.lcl随访方式.Size = new System.Drawing.Size(282, 29);
            this.lcl随访方式.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl随访方式.Text = "随访方式";
            this.lcl随访方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl随访方式.TextSize = new System.Drawing.Size(157, 20);
            this.lcl随访方式.TextToControlDistance = 5;
            // 
            // lcl耐药情况
            // 
            this.lcl耐药情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl耐药情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl耐药情况.AppearanceItemCaption.Options.UseFont = true;
            this.lcl耐药情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl耐药情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl耐药情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl耐药情况.Control = this.radioGroup耐药情况;
            this.lcl耐药情况.CustomizationFormText = "耐药情况";
            this.lcl耐药情况.Location = new System.Drawing.Point(282, 126);
            this.lcl耐药情况.MinSize = new System.Drawing.Size(119, 18);
            this.lcl耐药情况.Name = "lcl耐药情况";
            this.lcl耐药情况.Size = new System.Drawing.Size(296, 29);
            this.lcl耐药情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl耐药情况.Text = "耐药情况";
            this.lcl耐药情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl耐药情况.TextSize = new System.Drawing.Size(70, 14);
            this.lcl耐药情况.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.flowLayoutPanel2;
            this.layoutControlItem18.CustomizationFormText = "症状及体征";
            this.layoutControlItem18.Location = new System.Drawing.Point(161, 155);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(417, 81);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "症状及体征";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // lcl痰菌情况
            // 
            this.lcl痰菌情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl痰菌情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl痰菌情况.AppearanceItemCaption.Options.UseFont = true;
            this.lcl痰菌情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl痰菌情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl痰菌情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl痰菌情况.Control = this.radioGroup痰菌情况;
            this.lcl痰菌情况.CustomizationFormText = "痰菌情况";
            this.lcl痰菌情况.Location = new System.Drawing.Point(282, 97);
            this.lcl痰菌情况.MinSize = new System.Drawing.Size(119, 18);
            this.lcl痰菌情况.Name = "lcl痰菌情况";
            this.lcl痰菌情况.Size = new System.Drawing.Size(296, 29);
            this.lcl痰菌情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl痰菌情况.Text = "痰菌情况";
            this.lcl痰菌情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl痰菌情况.TextSize = new System.Drawing.Size(70, 14);
            this.lcl痰菌情况.TextToControlDistance = 5;
            // 
            // lcl患者类型
            // 
            this.lcl患者类型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl患者类型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl患者类型.AppearanceItemCaption.Options.UseFont = true;
            this.lcl患者类型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl患者类型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl患者类型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl患者类型.Control = this.radioGroup患者类型;
            this.lcl患者类型.CustomizationFormText = "患者类型";
            this.lcl患者类型.Location = new System.Drawing.Point(0, 126);
            this.lcl患者类型.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl患者类型.MinSize = new System.Drawing.Size(216, 29);
            this.lcl患者类型.Name = "lcl患者类型";
            this.lcl患者类型.Size = new System.Drawing.Size(282, 29);
            this.lcl患者类型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl患者类型.Text = "患者类型";
            this.lcl患者类型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl患者类型.TextSize = new System.Drawing.Size(157, 14);
            this.lcl患者类型.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem2.CustomizationFormText = "用  药";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 285);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(25, 87);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(25, 87);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(25, 87);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "用药";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // lcl化疗方案
            // 
            this.lcl化疗方案.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl化疗方案.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseFont = true;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl化疗方案.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl化疗方案.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl化疗方案.Control = this.txt化疗方案;
            this.lcl化疗方案.CustomizationFormText = "化疗方案";
            this.lcl化疗方案.Location = new System.Drawing.Point(25, 285);
            this.lcl化疗方案.MaxSize = new System.Drawing.Size(0, 26);
            this.lcl化疗方案.MinSize = new System.Drawing.Size(109, 26);
            this.lcl化疗方案.Name = "lcl化疗方案";
            this.lcl化疗方案.Size = new System.Drawing.Size(553, 26);
            this.lcl化疗方案.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl化疗方案.Text = "化疗方案";
            this.lcl化疗方案.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl化疗方案.TextSize = new System.Drawing.Size(132, 20);
            this.lcl化疗方案.TextToControlDistance = 5;
            // 
            // lcl用药用法
            // 
            this.lcl用药用法.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl用药用法.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl用药用法.AppearanceItemCaption.Options.UseFont = true;
            this.lcl用药用法.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl用药用法.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl用药用法.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl用药用法.Control = this.radioGroup用药用法;
            this.lcl用药用法.CustomizationFormText = "用  法";
            this.lcl用药用法.Location = new System.Drawing.Point(25, 311);
            this.lcl用药用法.MinSize = new System.Drawing.Size(109, 24);
            this.lcl用药用法.Name = "lcl用药用法";
            this.lcl用药用法.Size = new System.Drawing.Size(293, 32);
            this.lcl用药用法.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl用药用法.Text = "用  法";
            this.lcl用药用法.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl用药用法.TextSize = new System.Drawing.Size(132, 20);
            this.lcl用药用法.TextToControlDistance = 5;
            // 
            // lcl症状体征
            // 
            this.lcl症状体征.AllowHotTrack = false;
            this.lcl症状体征.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl症状体征.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl症状体征.AppearanceItemCaption.Options.UseFont = true;
            this.lcl症状体征.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl症状体征.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl症状体征.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl症状体征.CustomizationFormText = "症状及体征";
            this.lcl症状体征.Location = new System.Drawing.Point(0, 155);
            this.lcl症状体征.MaxSize = new System.Drawing.Size(161, 0);
            this.lcl症状体征.MinSize = new System.Drawing.Size(161, 130);
            this.lcl症状体征.Name = "lcl症状体征";
            this.lcl症状体征.Size = new System.Drawing.Size(161, 130);
            this.lcl症状体征.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl症状体征.Text = "症状及体征";
            this.lcl症状体征.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl症状体征.TextSize = new System.Drawing.Size(77, 20);
            this.lcl症状体征.TextVisible = true;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.med症状其他;
            this.layoutControlItem20.CustomizationFormText = "其他:";
            this.layoutControlItem20.Location = new System.Drawing.Point(161, 236);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 49);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(47, 49);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(417, 49);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "其他:";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(28, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // lcl督导人员
            // 
            this.lcl督导人员.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl督导人员.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl督导人员.AppearanceItemCaption.Options.UseFont = true;
            this.lcl督导人员.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl督导人员.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl督导人员.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl督导人员.Control = this.radioGroup督导人员;
            this.lcl督导人员.CustomizationFormText = "督导人员选择";
            this.lcl督导人员.Location = new System.Drawing.Point(0, 372);
            this.lcl督导人员.MaxSize = new System.Drawing.Size(0, 30);
            this.lcl督导人员.MinSize = new System.Drawing.Size(109, 30);
            this.lcl督导人员.Name = "lcl督导人员";
            this.lcl督导人员.Size = new System.Drawing.Size(424, 30);
            this.lcl督导人员.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl督导人员.Text = "督导人员选择";
            this.lcl督导人员.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl督导人员.TextSize = new System.Drawing.Size(157, 20);
            this.lcl督导人员.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(298, 402);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(280, 31);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem6.CustomizationFormText = "家庭居住环境评估";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 402);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(60, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(60, 60);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(60, 60);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "家庭居住环境评估";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // lcl单独居室
            // 
            this.lcl单独居室.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl单独居室.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl单独居室.AppearanceItemCaption.Options.UseFont = true;
            this.lcl单独居室.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl单独居室.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl单独居室.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl单独居室.Control = this.radioGroup单独居室;
            this.lcl单独居室.CustomizationFormText = "单独的居室";
            this.lcl单独居室.Location = new System.Drawing.Point(60, 402);
            this.lcl单独居室.MaxSize = new System.Drawing.Size(0, 31);
            this.lcl单独居室.MinSize = new System.Drawing.Size(109, 31);
            this.lcl单独居室.Name = "lcl单独居室";
            this.lcl单独居室.Size = new System.Drawing.Size(238, 31);
            this.lcl单独居室.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl单独居室.Text = "单独的居室";
            this.lcl单独居室.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl单独居室.TextSize = new System.Drawing.Size(97, 20);
            this.lcl单独居室.TextToControlDistance = 5;
            // 
            // lcl通风情况
            // 
            this.lcl通风情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl通风情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl通风情况.AppearanceItemCaption.Options.UseFont = true;
            this.lcl通风情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl通风情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl通风情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl通风情况.Control = this.radioGroup通风情况;
            this.lcl通风情况.CustomizationFormText = "通风情况";
            this.lcl通风情况.Location = new System.Drawing.Point(60, 433);
            this.lcl通风情况.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl通风情况.MinSize = new System.Drawing.Size(109, 29);
            this.lcl通风情况.Name = "lcl通风情况";
            this.lcl通风情况.Size = new System.Drawing.Size(300, 29);
            this.lcl通风情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl通风情况.Text = "通风情况";
            this.lcl通风情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl通风情况.TextSize = new System.Drawing.Size(97, 20);
            this.lcl通风情况.TextToControlDistance = 5;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem8.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 462);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(60, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(60, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(60, 60);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "生活方式评估";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // lcl吸烟
            // 
            this.lcl吸烟.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl吸烟.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl吸烟.AppearanceItemCaption.Options.UseFont = true;
            this.lcl吸烟.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl吸烟.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl吸烟.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl吸烟.Control = this.flowLayoutPanel4;
            this.lcl吸烟.CustomizationFormText = "吸　　烟";
            this.lcl吸烟.Location = new System.Drawing.Point(60, 462);
            this.lcl吸烟.MinSize = new System.Drawing.Size(159, 30);
            this.lcl吸烟.Name = "lcl吸烟";
            this.lcl吸烟.Size = new System.Drawing.Size(518, 30);
            this.lcl吸烟.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl吸烟.Text = "吸　　烟";
            this.lcl吸烟.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl吸烟.TextSize = new System.Drawing.Size(97, 20);
            this.lcl吸烟.TextToControlDistance = 5;
            // 
            // lcl饮酒
            // 
            this.lcl饮酒.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl饮酒.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl饮酒.AppearanceItemCaption.Options.UseFont = true;
            this.lcl饮酒.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl饮酒.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl饮酒.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl饮酒.Control = this.flowLayoutPanel5;
            this.lcl饮酒.CustomizationFormText = "饮　　酒";
            this.lcl饮酒.Location = new System.Drawing.Point(60, 492);
            this.lcl饮酒.MinSize = new System.Drawing.Size(159, 30);
            this.lcl饮酒.Name = "lcl饮酒";
            this.lcl饮酒.Size = new System.Drawing.Size(518, 30);
            this.lcl饮酒.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl饮酒.Text = "饮　　酒";
            this.lcl饮酒.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl饮酒.TextSize = new System.Drawing.Size(97, 20);
            this.lcl饮酒.TextToControlDistance = 5;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem10.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem10.CustomizationFormText = "健　康　教　育　及　培　训";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 522);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(22, 0);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(22, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(22, 310);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "健　康　教　育　及　培　训";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // lcl服药记录卡
            // 
            this.lcl服药记录卡.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl服药记录卡.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl服药记录卡.AppearanceItemCaption.Options.UseFont = true;
            this.lcl服药记录卡.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl服药记录卡.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl服药记录卡.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl服药记录卡.Control = this.radioGrou服药记录卡;
            this.lcl服药记录卡.CustomizationFormText = "服药记录卡的填写";
            this.lcl服药记录卡.Location = new System.Drawing.Point(22, 580);
            this.lcl服药记录卡.MinSize = new System.Drawing.Size(109, 28);
            this.lcl服药记录卡.Name = "lcl服药记录卡";
            this.lcl服药记录卡.Size = new System.Drawing.Size(282, 28);
            this.lcl服药记录卡.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl服药记录卡.Text = "服药记录卡的填写";
            this.lcl服药记录卡.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl服药记录卡.TextSize = new System.Drawing.Size(135, 20);
            this.lcl服药记录卡.TextToControlDistance = 5;
            // 
            // lcl不规律服药
            // 
            this.lcl不规律服药.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl不规律服药.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl不规律服药.AppearanceItemCaption.Options.UseFont = true;
            this.lcl不规律服药.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl不规律服药.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl不规律服药.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl不规律服药.Control = this.radioGroup不规律服药危害;
            this.lcl不规律服药.CustomizationFormText = "不规律服药危害";
            this.lcl不规律服药.Location = new System.Drawing.Point(22, 664);
            this.lcl不规律服药.MinSize = new System.Drawing.Size(109, 28);
            this.lcl不规律服药.Name = "lcl不规律服药";
            this.lcl不规律服药.Size = new System.Drawing.Size(282, 28);
            this.lcl不规律服药.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl不规律服药.Text = "不规律服药危害";
            this.lcl不规律服药.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl不规律服药.TextSize = new System.Drawing.Size(135, 20);
            this.lcl不规律服药.TextToControlDistance = 5;
            // 
            // lcl服药方法
            // 
            this.lcl服药方法.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl服药方法.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl服药方法.AppearanceItemCaption.Options.UseFont = true;
            this.lcl服药方法.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl服药方法.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl服药方法.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl服药方法.Control = this.radioGroup服药方法;
            this.lcl服药方法.CustomizationFormText = "服药方法及药品存放";
            this.lcl服药方法.Location = new System.Drawing.Point(22, 608);
            this.lcl服药方法.MinSize = new System.Drawing.Size(109, 28);
            this.lcl服药方法.Name = "lcl服药方法";
            this.lcl服药方法.Size = new System.Drawing.Size(282, 28);
            this.lcl服药方法.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl服药方法.Text = "服药方法及药品存放";
            this.lcl服药方法.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl服药方法.TextSize = new System.Drawing.Size(135, 20);
            this.lcl服药方法.TextToControlDistance = 5;
            // 
            // lcl肺结核治疗疗程
            // 
            this.lcl肺结核治疗疗程.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl肺结核治疗疗程.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl肺结核治疗疗程.AppearanceItemCaption.Options.UseFont = true;
            this.lcl肺结核治疗疗程.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl肺结核治疗疗程.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl肺结核治疗疗程.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl肺结核治疗疗程.Control = this.radioGroup肺结核治疗疗程;
            this.lcl肺结核治疗疗程.CustomizationFormText = "肺结核治疗疗程";
            this.lcl肺结核治疗疗程.Location = new System.Drawing.Point(22, 636);
            this.lcl肺结核治疗疗程.MinSize = new System.Drawing.Size(109, 28);
            this.lcl肺结核治疗疗程.Name = "lcl肺结核治疗疗程";
            this.lcl肺结核治疗疗程.Size = new System.Drawing.Size(282, 28);
            this.lcl肺结核治疗疗程.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl肺结核治疗疗程.Text = "肺结核治疗疗程";
            this.lcl肺结核治疗疗程.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl肺结核治疗疗程.TextSize = new System.Drawing.Size(135, 20);
            this.lcl肺结核治疗疗程.TextToControlDistance = 5;
            // 
            // lcl服药不良反应
            // 
            this.lcl服药不良反应.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl服药不良反应.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl服药不良反应.AppearanceItemCaption.Options.UseFont = true;
            this.lcl服药不良反应.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl服药不良反应.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl服药不良反应.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl服药不良反应.Control = this.radioGroup服药不良反应;
            this.lcl服药不良反应.CustomizationFormText = "服药后不良反应及处理";
            this.lcl服药不良反应.Location = new System.Drawing.Point(22, 692);
            this.lcl服药不良反应.MinSize = new System.Drawing.Size(109, 28);
            this.lcl服药不良反应.Name = "lcl服药不良反应";
            this.lcl服药不良反应.Size = new System.Drawing.Size(282, 28);
            this.lcl服药不良反应.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl服药不良反应.Text = "服药后不良反应及处理";
            this.lcl服药不良反应.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl服药不良反应.TextSize = new System.Drawing.Size(135, 20);
            this.lcl服药不良反应.TextToControlDistance = 5;
            // 
            // lcl取地点时间
            // 
            this.lcl取地点时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl取地点时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl取地点时间.AppearanceItemCaption.Options.UseFont = true;
            this.lcl取地点时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl取地点时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl取地点时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl取地点时间.Control = this.panelControl1;
            this.lcl取地点时间.CustomizationFormText = "取药地点、时间";
            this.lcl取地点时间.Location = new System.Drawing.Point(22, 522);
            this.lcl取地点时间.MinSize = new System.Drawing.Size(159, 58);
            this.lcl取地点时间.Name = "lcl取地点时间";
            this.lcl取地点时间.Size = new System.Drawing.Size(556, 58);
            this.lcl取地点时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl取地点时间.Text = "取药地点、时间";
            this.lcl取地点时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl取地点时间.TextSize = new System.Drawing.Size(135, 20);
            this.lcl取地点时间.TextToControlDistance = 5;
            // 
            // lcl下次随访时间
            // 
            this.lcl下次随访时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl下次随访时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseFont = true;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl下次随访时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl下次随访时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl下次随访时间.Control = this.dte下次随访时间;
            this.lcl下次随访时间.CustomizationFormText = "产后休养地";
            this.lcl下次随访时间.Location = new System.Drawing.Point(0, 832);
            this.lcl下次随访时间.MinSize = new System.Drawing.Size(50, 25);
            this.lcl下次随访时间.Name = "lcl下次随访时间";
            this.lcl下次随访时间.Size = new System.Drawing.Size(304, 25);
            this.lcl下次随访时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl下次随访时间.Text = "下次随访时间";
            this.lcl下次随访时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl下次随访时间.TextSize = new System.Drawing.Size(158, 14);
            this.lcl下次随访时间.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 881);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(190, 881);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(182, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(372, 881);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(286, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(292, 25);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl药品剂型
            // 
            this.lcl药品剂型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl药品剂型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseFont = true;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl药品剂型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl药品剂型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl药品剂型.Control = this.flowLayoutPanel3;
            this.lcl药品剂型.CustomizationFormText = "药品剂型";
            this.lcl药品剂型.Location = new System.Drawing.Point(25, 343);
            this.lcl药品剂型.MaxSize = new System.Drawing.Size(0, 29);
            this.lcl药品剂型.MinSize = new System.Drawing.Size(159, 29);
            this.lcl药品剂型.Name = "lcl药品剂型";
            this.lcl药品剂型.Size = new System.Drawing.Size(553, 29);
            this.lcl药品剂型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl药品剂型.Text = "药品剂型";
            this.lcl药品剂型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl药品剂型.TextSize = new System.Drawing.Size(132, 20);
            this.lcl药品剂型.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(318, 311);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(260, 32);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(424, 372);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(154, 30);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(360, 433);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(218, 29);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(304, 580);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem13.Text = "emptySpaceItem13";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(304, 608);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem14.Text = "emptySpaceItem14";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.CustomizationFormText = "emptySpaceItem15";
            this.emptySpaceItem15.Location = new System.Drawing.Point(304, 636);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem15.Text = "emptySpaceItem15";
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(304, 664);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem16.Text = "emptySpaceItem16";
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(304, 692);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem17.Text = "emptySpaceItem17";
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.AppearanceItemCaption.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.emptySpaceItem11.AppearanceItemCaption.Options.UseBorderColor = true;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(304, 832);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(274, 25);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl治疗期间复诊
            // 
            this.lcl治疗期间复诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl治疗期间复诊.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl治疗期间复诊.AppearanceItemCaption.Options.UseFont = true;
            this.lcl治疗期间复诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl治疗期间复诊.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl治疗期间复诊.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl治疗期间复诊.Control = this.radioGroup治疗期间复诊;
            this.lcl治疗期间复诊.CustomizationFormText = "治疗期间复诊查痰";
            this.lcl治疗期间复诊.Location = new System.Drawing.Point(22, 720);
            this.lcl治疗期间复诊.MinSize = new System.Drawing.Size(109, 28);
            this.lcl治疗期间复诊.Name = "lcl治疗期间复诊";
            this.lcl治疗期间复诊.Size = new System.Drawing.Size(282, 28);
            this.lcl治疗期间复诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl治疗期间复诊.Text = "治疗期间复诊查痰";
            this.lcl治疗期间复诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl治疗期间复诊.TextSize = new System.Drawing.Size(135, 20);
            this.lcl治疗期间复诊.TextToControlDistance = 5;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.CustomizationFormText = "emptySpaceItem18";
            this.emptySpaceItem18.Location = new System.Drawing.Point(304, 720);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem18.Text = "emptySpaceItem18";
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl外出服药
            // 
            this.lcl外出服药.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl外出服药.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl外出服药.AppearanceItemCaption.Options.UseFont = true;
            this.lcl外出服药.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl外出服药.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl外出服药.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl外出服药.Control = this.radioGroup外出服药;
            this.lcl外出服药.CustomizationFormText = "外出期间如何坚持服药";
            this.lcl外出服药.Location = new System.Drawing.Point(22, 748);
            this.lcl外出服药.MinSize = new System.Drawing.Size(109, 28);
            this.lcl外出服药.Name = "lcl外出服药";
            this.lcl外出服药.Size = new System.Drawing.Size(282, 28);
            this.lcl外出服药.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl外出服药.Text = "外出期间如何坚持服药";
            this.lcl外出服药.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl外出服药.TextSize = new System.Drawing.Size(135, 20);
            this.lcl外出服药.TextToControlDistance = 5;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.CustomizationFormText = "emptySpaceItem19";
            this.emptySpaceItem19.Location = new System.Drawing.Point(304, 748);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem19.Text = "emptySpaceItem19";
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl生活习惯
            // 
            this.lcl生活习惯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl生活习惯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl生活习惯.AppearanceItemCaption.Options.UseFont = true;
            this.lcl生活习惯.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl生活习惯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl生活习惯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl生活习惯.Control = this.radioGroup生活习惯;
            this.lcl生活习惯.CustomizationFormText = "生活习惯及注意事项";
            this.lcl生活习惯.Location = new System.Drawing.Point(22, 776);
            this.lcl生活习惯.MinSize = new System.Drawing.Size(109, 28);
            this.lcl生活习惯.Name = "lcl生活习惯";
            this.lcl生活习惯.Size = new System.Drawing.Size(282, 28);
            this.lcl生活习惯.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl生活习惯.Text = "生活习惯及注意事项";
            this.lcl生活习惯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl生活习惯.TextSize = new System.Drawing.Size(135, 20);
            this.lcl生活习惯.TextToControlDistance = 5;
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.CustomizationFormText = "emptySpaceItem20";
            this.emptySpaceItem20.Location = new System.Drawing.Point(304, 776);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem20.Text = "emptySpaceItem20";
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcl密切接触者
            // 
            this.lcl密切接触者.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lcl密切接触者.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lcl密切接触者.AppearanceItemCaption.Options.UseFont = true;
            this.lcl密切接触者.AppearanceItemCaption.Options.UseForeColor = true;
            this.lcl密切接触者.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcl密切接触者.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcl密切接触者.Control = this.radioGroup密切接触者;
            this.lcl密切接触者.CustomizationFormText = "密切接触者检查";
            this.lcl密切接触者.Location = new System.Drawing.Point(22, 804);
            this.lcl密切接触者.MinSize = new System.Drawing.Size(109, 28);
            this.lcl密切接触者.Name = "lcl密切接触者";
            this.lcl密切接触者.Size = new System.Drawing.Size(282, 28);
            this.lcl密切接触者.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcl密切接触者.Text = "密切接触者检查";
            this.lcl密切接触者.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lcl密切接触者.TextSize = new System.Drawing.Size(135, 20);
            this.lcl密切接触者.TextToControlDistance = 5;
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.CustomizationFormText = "emptySpaceItem21";
            this.emptySpaceItem21.Location = new System.Drawing.Point(304, 804);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(274, 28);
            this.emptySpaceItem21.Text = "emptySpaceItem21";
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(557, 857);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(21, 24);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn新增);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(136, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(597, 28);
            this.flowLayoutPanel1.TabIndex = 124;
            // 
            // btn新增
            // 
            this.btn新增.Image = ((System.Drawing.Image)(resources.GetObject("btn新增.Image")));
            this.btn新增.Location = new System.Drawing.Point(3, 3);
            this.btn新增.Name = "btn新增";
            this.btn新增.Size = new System.Drawing.Size(75, 23);
            this.btn新增.TabIndex = 11;
            this.btn新增.Text = "添加";
            this.btn新增.Click += new System.EventHandler(this.btn新增_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 9;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 10;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 12;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // UC肺结核第一次随访_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC肺结核第一次随访_显示";
            this.Size = new System.Drawing.Size(733, 533);
            this.Load += new System.EventHandler(this.UC肺结核第一次随访_显示_Load);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit取药时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit取药时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt取药地点.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup治疗期间复诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup肺结核治疗疗程.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药方法.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup外出服药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup不规律服药危害.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup生活习惯.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup密切接触者.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGrou服药记录卡.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒2.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup通风情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup单独居室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup督导人员.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med症状其他.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk1固定剂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2散装药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3板式药.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4注射剂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup用药用法.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化疗方案.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk0没有症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1咳嗽咳痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2低热盗汗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3咯血血痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4胸痛消瘦.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5恶心纳差.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6头痛失眠.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7视物模糊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8皮肤瘙痒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9耳鸣.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup耐药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup痰菌情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup患者类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit评估医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl医生签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl随访方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl耐药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl痰菌情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl患者类型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl化疗方案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl用药用法)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl症状体征)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl督导人员)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl单独居室)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl通风情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl吸烟)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl饮酒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药记录卡)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl不规律服药)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药方法)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl肺结核治疗疗程)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl服药不良反应)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl取地点时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl下次随访时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl药品剂型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl治疗期间复诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl外出服药)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl生活习惯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcl密切接触者)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt居民签名;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit评估医生;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit textEdit编号;
        private DevExpress.XtraEditors.DateEdit dte下次随访时间;
        private DevExpress.XtraEditors.DateEdit dte随访时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem lcl下次随访时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem lcl医生签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem lcl随访时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem lcl患者签名;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.RadioGroup radioGroup耐药情况;
        private DevExpress.XtraEditors.RadioGroup radioGroup痰菌情况;
        private DevExpress.XtraEditors.RadioGroup radioGroup患者类型;
        private DevExpress.XtraEditors.RadioGroup radioGroup随访方式;
        private DevExpress.XtraLayout.LayoutControlItem lcl随访方式;
        private DevExpress.XtraLayout.LayoutControlItem lcl患者类型;
        private DevExpress.XtraLayout.LayoutControlItem lcl痰菌情况;
        private DevExpress.XtraLayout.LayoutControlItem lcl耐药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.RadioGroup radioGroup用药用法;
        private DevExpress.XtraEditors.TextEdit txt化疗方案;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lcl化疗方案;
        private DevExpress.XtraLayout.LayoutControlItem lcl用药用法;
        private DevExpress.XtraLayout.LayoutControlItem lcl药品剂型;
        private DevExpress.XtraEditors.CheckEdit chk0没有症状;
        private DevExpress.XtraEditors.CheckEdit chk1咳嗽咳痰;
        private DevExpress.XtraEditors.CheckEdit chk2低热盗汗;
        private DevExpress.XtraEditors.CheckEdit chk3咯血血痰;
        private DevExpress.XtraEditors.CheckEdit chk4胸痛消瘦;
        private DevExpress.XtraEditors.CheckEdit chk5恶心纳差;
        private DevExpress.XtraEditors.CheckEdit chk6头痛失眠;
        private DevExpress.XtraEditors.CheckEdit chk7视物模糊;
        private DevExpress.XtraEditors.CheckEdit chk8皮肤瘙痒;
        private DevExpress.XtraEditors.CheckEdit chk9耳鸣;
        private DevExpress.XtraLayout.EmptySpaceItem lcl症状体征;
        private DevExpress.XtraEditors.MemoEdit med症状其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.CheckEdit chk1固定剂;
        private DevExpress.XtraEditors.CheckEdit chk2散装药;
        private DevExpress.XtraEditors.CheckEdit chk3板式药;
        private DevExpress.XtraEditors.CheckEdit chk4注射剂;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.RadioGroup radioGroup督导人员;
        private DevExpress.XtraLayout.LayoutControlItem lcl督导人员;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.RadioGroup radioGroup单独居室;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem lcl单独居室;
        private DevExpress.XtraEditors.RadioGroup radioGroup通风情况;
        private DevExpress.XtraLayout.LayoutControlItem lcl通风情况;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraLayout.LayoutControlItem lcl吸烟;
        private DevExpress.XtraLayout.LayoutControlItem lcl饮酒;
        private DevExpress.XtraEditors.TextEdit txt吸烟1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt吸烟2;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txt饮酒1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txt饮酒2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.RadioGroup radioGroup治疗期间复诊;
        private DevExpress.XtraEditors.RadioGroup radioGroup服药不良反应;
        private DevExpress.XtraEditors.RadioGroup radioGroup肺结核治疗疗程;
        private DevExpress.XtraEditors.RadioGroup radioGroup服药方法;
        private DevExpress.XtraEditors.RadioGroup radioGroup外出服药;
        private DevExpress.XtraEditors.RadioGroup radioGroup不规律服药危害;
        private DevExpress.XtraEditors.RadioGroup radioGroup生活习惯;
        private DevExpress.XtraEditors.RadioGroup radioGroup密切接触者;
        private DevExpress.XtraEditors.RadioGroup radioGrou服药记录卡;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem lcl服药记录卡;
        private DevExpress.XtraLayout.LayoutControlItem lcl密切接触者;
        private DevExpress.XtraLayout.LayoutControlItem lcl生活习惯;
        private DevExpress.XtraLayout.LayoutControlItem lcl不规律服药;
        private DevExpress.XtraLayout.LayoutControlItem lcl外出服药;
        private DevExpress.XtraLayout.LayoutControlItem lcl服药方法;
        private DevExpress.XtraLayout.LayoutControlItem lcl肺结核治疗疗程;
        private DevExpress.XtraLayout.LayoutControlItem lcl服药不良反应;
        private DevExpress.XtraLayout.LayoutControlItem lcl治疗期间复诊;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem lcl取地点时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txt取药地点;
        private DevExpress.XtraEditors.DateEdit dateEdit取药时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraEditors.SimpleButton btn新增;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
    }
}
