﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.肺结核
{
    public partial class UC肺结核第一次随访_显示 : UserControlBaseNavBar
    {
        DataRow[] _dr个人档案信息 = null;
        string _创建日期 = "";
        string _ID = "";

        public UC肺结核第一次随访_显示()
        {
            InitializeComponent();

            SetShowStatus();
        }

        public UC肺结核第一次随访_显示(DataRow[] dr, object date)
        {
            InitializeComponent();

            SetShowStatus();

            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bll肺结核第一次随访();

            //默认绑定
            this.textEdit档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.textEdit姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.textEdit身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.textEdit出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.textEdit居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            //this.textEdit评估医生.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
        }

        private void SetShowStatus()
        {
            textEdit编号.Properties.ReadOnly = true;
            dte随访时间.Properties.ReadOnly = true;
            
            radioGroup随访方式.Properties.ReadOnly = true;
            radioGroup痰菌情况.Properties.ReadOnly = true;
            radioGroup患者类型.Properties.ReadOnly = true;
            radioGroup耐药情况.Properties.ReadOnly = true;

            chk0没有症状.Properties.ReadOnly = true;
            chk1咳嗽咳痰.Properties.ReadOnly = true;
            chk2低热盗汗.Properties.ReadOnly = true;
            chk3咯血血痰.Properties.ReadOnly = true;
            chk4胸痛消瘦.Properties.ReadOnly = true;
            chk5恶心纳差.Properties.ReadOnly = true;
            chk6头痛失眠.Properties.ReadOnly = true;
            chk7视物模糊.Properties.ReadOnly = true;
            chk8皮肤瘙痒.Properties.ReadOnly = true;
            chk9耳鸣.Properties.ReadOnly = true;

            med症状其他.Properties.ReadOnly = true;

            txt化疗方案.Properties.ReadOnly = true;
            radioGroup用药用法.Properties.ReadOnly = true;

            chk1固定剂.Properties.ReadOnly = true;
            chk2散装药.Properties.ReadOnly = true;
            chk3板式药.Properties.ReadOnly = true;
            chk4注射剂.Properties.ReadOnly = true;

            radioGroup督导人员.Properties.ReadOnly = true;
            radioGroup单独居室.Properties.ReadOnly = true;
            radioGroup通风情况.Properties.ReadOnly = true;

            txt吸烟1.Properties.ReadOnly = true;
            txt吸烟2.Properties.ReadOnly = true;
            txt饮酒1.Properties.ReadOnly = true;
            txt饮酒2.Properties.ReadOnly = true;

            txt取药地点.Properties.ReadOnly = true;
            dateEdit取药时间.Properties.ReadOnly = true;

            radioGrou服药记录卡.Properties.ReadOnly = true;
            radioGroup服药方法.Properties.ReadOnly = true;
            radioGroup肺结核治疗疗程.Properties.ReadOnly = true;
            radioGroup不规律服药危害.Properties.ReadOnly = true;
            radioGroup服药不良反应.Properties.ReadOnly = true;
            radioGroup治疗期间复诊.Properties.ReadOnly = true;
            radioGroup外出服药.Properties.ReadOnly = true;
            radioGroup生活习惯.Properties.ReadOnly = true;
            radioGroup密切接触者.Properties.ReadOnly = true;

            dte下次随访时间.Properties.ReadOnly = true;
        }

        private void SetLclColor()
        {
            SetRadiogroupDateeditTexteditLCI(lcl随访时间, dte随访时间);
            SetRadiogroupDateeditTexteditLCI(lcl随访方式, radioGroup随访方式);
            SetRadiogroupDateeditTexteditLCI(lcl痰菌情况, radioGroup痰菌情况);
            SetRadiogroupDateeditTexteditLCI(lcl患者类型, radioGroup患者类型);
            SetRadiogroupDateeditTexteditLCI(lcl耐药情况, radioGroup耐药情况);
            SetCheckPanelLayoutControlItem(lcl症状体征, flowLayoutPanel2);
            SetRadiogroupDateeditTexteditLCI(lcl化疗方案, txt化疗方案);
            SetRadiogroupDateeditTexteditLCI(lcl用药用法, radioGroup用药用法);
            SetCheckPanelLayoutControlItem(lcl药品剂型, flowLayoutPanel3);
            SetRadiogroupDateeditTexteditLCI(lcl督导人员, radioGroup督导人员);
            SetRadiogroupDateeditTexteditLCI(lcl单独居室, radioGroup单独居室);
            SetRadiogroupDateeditTexteditLCI(lcl通风情况, radioGroup通风情况);
            SetEditPanelControLayoutControlItem(lcl吸烟, flowLayoutPanel4.Controls);
            SetEditPanelControLayoutControlItem(lcl饮酒, flowLayoutPanel5.Controls);
            SetEditPanelControLayoutControlItem(lcl取地点时间, panelControl1.Controls);
            SetRadiogroupDateeditTexteditLCI(lcl服药记录卡, radioGrou服药记录卡);
            SetRadiogroupDateeditTexteditLCI(lcl服药方法, radioGroup服药方法);
            SetRadiogroupDateeditTexteditLCI(lcl肺结核治疗疗程, radioGroup肺结核治疗疗程);
            SetRadiogroupDateeditTexteditLCI(lcl不规律服药, radioGroup不规律服药危害);
            SetRadiogroupDateeditTexteditLCI(lcl服药不良反应, radioGroup服药不良反应);
            SetRadiogroupDateeditTexteditLCI(lcl治疗期间复诊, radioGroup治疗期间复诊);
            SetRadiogroupDateeditTexteditLCI(lcl外出服药, radioGroup外出服药);
            SetRadiogroupDateeditTexteditLCI(lcl生活习惯, radioGroup生活习惯);
            SetRadiogroupDateeditTexteditLCI(lcl密切接触者, radioGroup密切接触者);
            SetRadiogroupDateeditTexteditLCI(lcl下次随访时间, dte下次随访时间);
            SetRadiogroupDateeditTexteditLCI(lcl医生签名, textEdit评估医生);
            SetRadiogroupDateeditTexteditLCI(lcl患者签名, txt居民签名);
        }
        private void UC肺结核第一次随访_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(this.textEdit档案编号.Text,true).Tables[tb_MXB肺结核第一次随访.__TableName];

            CreateNavBarButton_new(dt缓存数据, tb_MXB肺结核第一次随访.随访时间);

            if(dt缓存数据!=null && dt缓存数据.Rows.Count > 0)
            {
                if (_创建日期 != null && _创建日期 != "")
                {
                    DataRow[] drss = dt缓存数据.Select("创建时间='" + _创建日期 + "'");
                    DoBindingSummaryEditor(drss[0]);
                    //DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);
                }
                else
                {
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);
                }
                SetItemColorToRed(_ID);//设置左侧当前随访日期颜色

                //SetLclColor();//设置考核项的颜色
            }
            else
            {//打开新增页面
                btn新增.PerformClick();
            }

            //SetDetailEditorsAccessable(layoutControl1, false);
        }

        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null)
            {
                return;
            }

            dataSource.Rows[0][tb_MXB肺结核第一次随访.个人档案编号] = textEdit档案编号.Text;
            _ID = dataSource.Rows[0][tb_MXB肺结核第一次随访.ID].ToString();

            DataBinder.BindingTextEdit(textEdit编号, dataSource, tb_MXB肺结核第一次随访.编号);
            DataBinder.BindingTextEditDateTime(dte随访时间, dataSource, tb_MXB肺结核第一次随访.随访时间);
            DataBinder.BindingRadioEdit(radioGroup随访方式, dataSource, tb_MXB肺结核第一次随访.随访方式);
            DataBinder.BindingRadioEdit(radioGroup痰菌情况, dataSource, tb_MXB肺结核第一次随访.痰菌情况);
            DataBinder.BindingRadioEdit(radioGroup患者类型, dataSource, tb_MXB肺结核第一次随访.患者类型);
            DataBinder.BindingRadioEdit(radioGroup耐药情况, dataSource, tb_MXB肺结核第一次随访.耐药情况);

            string temp体征 = dataSource.Rows[0][tb_MXB肺结核第一次随访.症状及体征].ToString();
            string[] arr体征 = temp体征.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr体征.Length; index++)
            {
                switch (arr体征[index])
                {
                    case "0":
                        chk0没有症状.Checked = true;
                        break;
                    case "1":
                        chk1咳嗽咳痰.Checked = true;
                        break;
                    case "2": chk2低热盗汗.Checked = true;
                        break;
                    case "3": chk3咯血血痰.Checked = true;
                        break;
                    case "4": chk4胸痛消瘦.Checked = true;
                        break;
                    case "5": chk5恶心纳差.Checked = true;
                        break;
                    case "6": chk6头痛失眠.Checked = true;
                        break;
                    case "7": chk7视物模糊.Checked = true;
                        break;
                    case "8": chk8皮肤瘙痒.Checked = true;
                        break;
                    case "9": chk9耳鸣.Checked = true;
                        break;
                    default:
                        break;
                }
            }

            DataBinder.BindingTextEdit(med症状其他, dataSource, tb_MXB肺结核第一次随访.症状及体征其他);
            DataBinder.BindingTextEdit(txt化疗方案, dataSource, tb_MXB肺结核第一次随访.化疗方案);
            DataBinder.BindingRadioEdit(radioGroup用药用法, dataSource, tb_MXB肺结核第一次随访.用药用法);

            string temp药品剂型 = dataSource.Rows[0][tb_MXB肺结核第一次随访.药品剂型].ToString();
            string[] arr药品剂型 = temp药品剂型.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr药品剂型.Length; index++)
            {
                switch (arr药品剂型[index])
                {
                    case "1": chk1固定剂.Checked = true; break;
                    case "2": chk2散装药.Checked = true; break;
                    case "3": chk3板式药.Checked = true; break;
                    case "4": chk4注射剂.Checked = true; break;
                    default: break;
                }
            }

            DataBinder.BindingRadioEdit(radioGroup督导人员, dataSource, tb_MXB肺结核第一次随访.督导人员);
            DataBinder.BindingRadioEdit(radioGroup单独居室, dataSource, tb_MXB肺结核第一次随访.单独居室);
            DataBinder.BindingRadioEdit(radioGroup通风情况, dataSource, tb_MXB肺结核第一次随访.通风情况);
            DataBinder.BindingTextEdit(txt吸烟1, dataSource, tb_MXB肺结核第一次随访.吸烟1);
            DataBinder.BindingTextEdit(txt吸烟2, dataSource, tb_MXB肺结核第一次随访.吸烟2);
            DataBinder.BindingTextEdit(txt饮酒1, dataSource, tb_MXB肺结核第一次随访.饮酒1);
            DataBinder.BindingTextEdit(txt饮酒2, dataSource, tb_MXB肺结核第一次随访.饮酒2);
            DataBinder.BindingTextEdit(txt取药地点, dataSource, tb_MXB肺结核第一次随访.取药地点);
            DataBinder.BindingTextEditDateTime(dateEdit取药时间, dataSource, tb_MXB肺结核第一次随访.取药时间);
            DataBinder.BindingRadioEdit(radioGrou服药记录卡, dataSource, tb_MXB肺结核第一次随访.服药记录卡的填写);
            DataBinder.BindingRadioEdit(radioGroup服药方法, dataSource, tb_MXB肺结核第一次随访.服药方法及药品存放);
            DataBinder.BindingRadioEdit(radioGroup肺结核治疗疗程, dataSource, tb_MXB肺结核第一次随访.肺结核治疗疗程);
            DataBinder.BindingRadioEdit(radioGroup不规律服药危害, dataSource, tb_MXB肺结核第一次随访.不规律服药危害);
            DataBinder.BindingRadioEdit(radioGroup服药不良反应, dataSource, tb_MXB肺结核第一次随访.服药后不良反应及处理);
            DataBinder.BindingRadioEdit(radioGroup治疗期间复诊, dataSource, tb_MXB肺结核第一次随访.治疗期间复诊查痰);
            DataBinder.BindingRadioEdit(radioGroup外出服药, dataSource, tb_MXB肺结核第一次随访.外出期间如何坚持服药);
            DataBinder.BindingRadioEdit(radioGroup生活习惯, dataSource, tb_MXB肺结核第一次随访.生活习惯及注意事项);
            DataBinder.BindingRadioEdit(radioGroup密切接触者, dataSource, tb_MXB肺结核第一次随访.密切接触者检查);
            DataBinder.BindingTextEditDateTime(dte下次随访时间, dataSource, tb_MXB肺结核第一次随访.下次随访时间);
            DataBinder.BindingTextEdit(textEdit评估医生, dataSource, tb_MXB肺结核第一次随访.评估医生签名);
            DataBinder.BindingTextEdit(txt居民签名, dataSource, tb_MXB肺结核第一次随访.患者签名);

            //非编辑项
            this.textEdit当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.所属机构].ToString());
            this.textEdit创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.创建机构].ToString());
            this.textEdit创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.创建人].ToString());
            this.textEdit创建时间.Text = dataSource.Rows[0][tb_MXB肺结核第一次随访.创建时间].ToString();
            this.textEdit最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核第一次随访.修改人].ToString());
            this.textEdit最近更新时间.Text = dataSource.Rows[0][tb_MXB肺结核第一次随访.修改时间].ToString();

            SetLclColor();//设置考核项的颜色
        }

        protected override void SetRadioGroupDoubleClick(DevExpress.XtraEditors.RadioGroup radio)
        {
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            //Msg.ShowInformation("删除");
            //Msg.ShowInformation(_ID);
            //如果已经添加了后续随访，则做出提示。
            //删除前进行确认。

            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(_ID))
                {
                    _创建日期 = "";
                    this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            Msg.ShowInformation("导出");
        }

        private void btn新增_Click(object sender, EventArgs e)
        {
            //Msg.ShowInformation("新增");
            _UpdateType = UpdateType.Add;
            UC肺结核第一次随访 control = new UC肺结核第一次随访(_dr个人档案信息, _UpdateType, "");
            ShowControl(control);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            //Msg.ShowInformation("修改");
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC肺结核第一次随访 control = new UC肺结核第一次随访(_dr个人档案信息, _UpdateType, _ID);
                ShowControl(control);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        
    }
}
