﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;
using System.Drawing;

namespace AtomEHR.公共卫生.Module.个人健康.肺结核
{
    public partial class UC肺结核后续随访 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        string _ID = "";
        public UC肺结核后续随访()
        {
            InitializeComponent();
        }

        public UC肺结核后续随访(DataRow[] dr, UpdateType updateType, object ID)
        {
            InitializeComponent();

            base._UpdateType = updateType;
            _dr个人档案信息 = dr;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bllMXB肺结核后续随访();

            //默认绑定
            textEdit档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.textEdit姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.textEdit身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.textEdit出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.textEdit联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.textEdit居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            //this.textEdit评估医生.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名
        }

        private void SetLclColor()
        {
            //TODO:
            SetRadiogroupDateeditTexteditLCI(lcl随访时间, dte随访时间);
            SetEditPanelControLayoutControlItem(lcl治疗月序, flp治疗月序.Controls);
            SetRadiogroupDateeditTexteditLCI(lcl督导人员, rg督导人员);
            SetRadiogroupDateeditTexteditLCI(lcl随访方式, rg随访方式);
            SetCheckPanelLayoutControlItem(lcl症状体征, flp症状体征);
            SetEditPanelControLayoutControlItem(lcl吸烟, flp吸烟.Controls);
            SetEditPanelControLayoutControlItem(lcl饮酒, flp饮酒.Controls);
            SetRadiogroupDateeditTexteditLCI(lcl化疗方案, txt化疗方案);
            SetRadiogroupDateeditTexteditLCI(lcl用药用法, rg用药用法);
            SetCheckPanelLayoutControlItem(lcl药品剂型, flp药品剂型);
            SetEditPanelControLayoutControlItem(lcl漏服药次数, flp漏服药次数.Controls);

            //药物不良反应
            if (rg药物不良反应.EditValue == null)
            {
                lcl药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (rg药物不良反应.EditValue.ToString() == "1")
            {
                lcl药物不良反应.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else if (rg药物不良反应.EditValue.ToString() == "2" && txt药物不良反应有.EditValue != null && !string.IsNullOrWhiteSpace(txt药物不良反应有.EditValue.ToString()))
            {
                lcl药物不良反应.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lcl药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
            }

            //并发症
            if (rg并发症.EditValue == null)
            {
                lcl并发症.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (rg并发症.EditValue.ToString() == "1")
            {
                lcl并发症.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else if (rg并发症.EditValue.ToString() == "2" && txt并发症有.EditValue != null && !string.IsNullOrWhiteSpace(txt并发症有.EditValue.ToString()))
            {
                lcl并发症.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lcl并发症.AppearanceItemCaption.ForeColor = Color.Red;
            }

            //SetRadiogroupDateeditTexteditLCI(lcl原因, txt转诊原因);
            //SetRadiogroupDateeditTexteditLCI(lcl转诊机构, txt转诊机构);
            //SetRadiogroupDateeditTexteditLCI(lcl症状2周随访, txt转诊2周内随访);
            SetRadiogroupDateeditTexteditLCI(lcl处理意见, txt处理意见);
            SetRadiogroupDateeditTexteditLCI(lcl下次随访时间, dte下次随访时间);
        }

        private void UC肺结核后续随访_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);
                _BLL.NewBusiness();
                _BLL.DataBinder.Rows[0][tb_MXB肺结核后续随访.个人档案编号] = _dr个人档案信息[0][tb_健康档案.个人档案编号].ToString();
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                dte随访时间.Properties.ReadOnly = true;
                dte随访时间.Properties.Buttons[0].Enabled = false;

                if (_ID != null && _ID != "")
                {
                    ((bllMXB肺结核后续随访)_BLL).GetBusinessByKeyEdit(_ID, true);
                }
                else
                {
                    return;
                }
            }

            DataTable dataSource = _BLL.CurrentBusiness.Tables[tb_MXB肺结核后续随访.__TableName];

            DoBindingSummaryEditor(dataSource);

            //非编辑项
            this.textEdit所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核后续随访.所属机构].ToString());
            this.textEdit创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_MXB肺结核后续随访.创建机构].ToString());
            this.textEdit创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核后续随访.创建人].ToString());
            this.textEdit创建时间.Text = dataSource.Rows[0][tb_MXB肺结核后续随访.创建时间].ToString();
            this.textEdit修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_MXB肺结核后续随访.修改人].ToString());
            this.textEdit更新时间.Text = dataSource.Rows[0][tb_MXB肺结核后续随访.修改时间].ToString();
        }

        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null)
            {
                return;
            }

            //TODO:
            dataSource.Rows[0][tb_MXB肺结核后续随访.个人档案编号] = textEdit档案编号.Text;

            
            
            DataBinder.BindingTextEdit(txt编号, dataSource, tb_MXB肺结核后续随访.编号);
            DataBinder.BindingTextEditDateTime(dte随访时间, dataSource, tb_MXB肺结核后续随访.随访时间);
            DataBinder.BindingTextEdit(txt治疗月序, dataSource, tb_MXB肺结核后续随访.治疗月序);
            DataBinder.BindingRadioEdit(rg督导人员, dataSource, tb_MXB肺结核后续随访.督导人员);
            DataBinder.BindingRadioEdit(rg随访方式, dataSource, tb_MXB肺结核后续随访.随访方式);
            
            //DataBinder.BindingRadioEdit(txt症状及体征, dataSource, tb_MXB肺结核后续随访.症状及体征);
            string temp体征 = dataSource.Rows[0][tb_MXB肺结核后续随访.症状及体征].ToString();
            string[] arr体征 = temp体征.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr体征.Length; index++)
            {
                switch (arr体征[index])
                {
                    case "0":
                        chk0没有症状.Checked = true;
                        break;
                    case "1":
                        chk1咳嗽咳痰.Checked = true;
                        break;
                    case "2": chk2低热盗汗.Checked = true;
                        break;
                    case "3": chk3咯血血痰.Checked = true;
                        break;
                    case "4": chk4胸痛消瘦.Checked = true;
                        break;
                    case "5": chk5恶心纳差.Checked = true;
                        break;
                    case "6": chk6关节疼痛.Checked = true;
                        break;
                    case "7": chk7头痛失眠.Checked = true;
                        break;
                    case "8": chk8视物模糊.Checked = true;
                        break;
                    case "9": chk9皮痒皮疹.Checked = true;
                        break;
                    case "10":
                        chk10耳鸣.Checked = true;
                        break;
                    default:
                        break;
                }
            } 
            
            DataBinder.BindingTextEdit(txt症状及体征其他, dataSource, tb_MXB肺结核后续随访.症状及体征其他);
            DataBinder.BindingTextEdit(txt吸烟1, dataSource, tb_MXB肺结核后续随访.吸烟1);
            DataBinder.BindingTextEdit(txt吸烟2, dataSource, tb_MXB肺结核后续随访.吸烟2);
            DataBinder.BindingTextEdit(txt饮酒1, dataSource, tb_MXB肺结核后续随访.饮酒1);
            DataBinder.BindingTextEdit(txt饮酒2, dataSource, tb_MXB肺结核后续随访.饮酒2);
            DataBinder.BindingTextEdit(txt化疗方案, dataSource, tb_MXB肺结核后续随访.化疗方案);
            DataBinder.BindingRadioEdit(rg用药用法, dataSource, tb_MXB肺结核后续随访.用药用法);
            //DataBinder.BindingTextEdit(txt药品剂型, dataSource, tb_MXB肺结核后续随访.药品剂型);
            string temp药品剂型 = dataSource.Rows[0][tb_MXB肺结核后续随访.药品剂型].ToString();
            string[] arr药品剂型 = temp药品剂型.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr药品剂型.Length; index++)
            {
                switch (arr药品剂型[index])
                {
                    case "1": chk1固定剂.Checked = true; break;
                    case "2": chk2散装药.Checked = true; break;
                    case "3": chk3板式药.Checked = true; break;
                    case "4": chk4注射剂.Checked = true; break;
                    default: break;
                }
            }

            DataBinder.BindingTextEdit(txt漏服药次数, dataSource, tb_MXB肺结核后续随访.漏服药次数);
            DataBinder.BindingRadioEdit(rg药物不良反应, dataSource, tb_MXB肺结核后续随访.药物不良反应);
            DataBinder.BindingTextEdit(txt药物不良反应有, dataSource, tb_MXB肺结核后续随访.药物不良反应有);
            DataBinder.BindingRadioEdit(rg并发症, dataSource, tb_MXB肺结核后续随访.并发症);
            DataBinder.BindingTextEdit(txt并发症有, dataSource, tb_MXB肺结核后续随访.并发症有);
            DataBinder.BindingTextEdit(txt转诊原因, dataSource, tb_MXB肺结核后续随访.转诊原因);
            DataBinder.BindingTextEdit(txt转诊机构, dataSource, tb_MXB肺结核后续随访.转诊机构);
            DataBinder.BindingTextEdit(txt转诊2周内随访, dataSource, tb_MXB肺结核后续随访.转诊2周内随访);
            DataBinder.BindingTextEdit(txt处理意见, dataSource, tb_MXB肺结核后续随访.处理意见);
            DataBinder.BindingTextEditDateTime(dte下次随访时间, dataSource, tb_MXB肺结核后续随访.下次随访时间);
            
            DataBinder.BindingTextEditDateTime(dte停止治疗时间, dataSource, tb_MXB肺结核后续随访.停止治疗时间);
            
            //DataBinder.BindingTextEdit(txt停止治疗原因, dataSource, tb_MXB肺结核后续随访.停止治疗原因);
            string[] arr停止治疗原因 = dataSource.Rows[0][tb_MXB肺结核后续随访.停止治疗原因].ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr停止治疗原因.Length; index++ )
            {
                switch(arr停止治疗原因[index])
                {
                    case "1": chk1完成诊疗.Checked = true;break;
                    case "2": chk2死亡.Checked = true; break;
                    case "3": chk3丢失.Checked = true; break;
                    case "4": chk4转入耐药治疗.Checked = true; break;
                    default: break;
                }
            }

            DataBinder.BindingTextEdit(txt应访视次数, dataSource, tb_MXB肺结核后续随访.应访视次数);
            DataBinder.BindingTextEdit(txt实际方式次数, dataSource, tb_MXB肺结核后续随访.实际方式次数);
            DataBinder.BindingTextEdit(txt应服药次数, dataSource, tb_MXB肺结核后续随访.应服药次数);
            DataBinder.BindingTextEdit(txt实际服药次数, dataSource, tb_MXB肺结核后续随访.实际服药次数);
            DataBinder.BindingTextEdit(txt服药率, dataSource, tb_MXB肺结核后续随访.服药率);
            
            //DataBinder.BindingTextEdit(txt随访医生签名, dataSource, tb_MXB肺结核后续随访.随访医生签名);
            //DataBinder.BindingTextEdit(txt患者签名, dataSource, tb_MXB肺结核后续随访.患者签名);
            //DataBinder.BindingTextEdit(txt评估医生签名, dataSource, tb_MXB肺结核后续随访.评估医生签名);

            ShowImage(lcl随访医生, pe随访医生签名, dataSource.Rows[0][tb_MXB肺结核后续随访.随访医生签名].ToString());
            ShowImage(lcl患者签名, pe患者签名, dataSource.Rows[0][tb_MXB肺结核后续随访.患者签名].ToString());
            ShowImage(null, pe评估医生签名, dataSource.Rows[0][tb_MXB肺结核后续随访.评估医生签名].ToString());

            SetLclColor();
        }

        

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(dte随访时间.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                dte随访时间.Focus();
                return false;
            }
            #region  允许修改下次随访时间(设置默认值)
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt下次随访时间.Text)))
            //{
            //    Msg.Warning("下次随访日期不能为空!");
            //    txt下次随访时间.Focus();
            //    return false;
            //}
            #endregion

            if (this.dte随访时间.DateTime > Convert.ToDateTime(_BLL.ServiceDateTime))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                dte随访时间.Focus();
                return false;
            }
            return true;
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();

            if (!ValidatingSummaryData())
            {
                return; //检查主表数据合法性
            }
            if (!Msg.AskQuestion("信息保存后，‘随访日期’将不允许修改，确认保存信息？"))
            {
                return;
            }
            if (_UpdateType == UpdateType.None)
            {
                return;
            }

            //TODO:

            string str症状 = "";
            if (chk0没有症状.Checked)
            {
                str症状 = "0";
            }
            else
            {
                if (chk1咳嗽咳痰.Checked) { str症状 += "1,"; }
                if (chk2低热盗汗.Checked) { str症状 += "2,"; }
                if (chk3咯血血痰.Checked) { str症状 += "3,"; }
                if (chk4胸痛消瘦.Checked) { str症状 += "4,"; }
                if (chk5恶心纳差.Checked) { str症状 += "5,"; }
                if (chk6关节疼痛.Checked) { str症状 += "6,"; }
                if (chk7头痛失眠.Checked) { str症状 += "7,"; }
                if (chk8视物模糊.Checked) { str症状 += "8,"; }
                if (chk9皮痒皮疹.Checked) { str症状 += "9,"; }
                if (chk10耳鸣.Checked) { str症状 += "10,"; }

                if (str症状.Length > 0)
                {
                    str症状 = str症状.Remove(str症状.Length - 1);
                }
            }
            _BLL.DataBinder.Rows[0][tb_MXB肺结核后续随访.症状及体征] = str症状;

            string str药品剂型 = "";
            if (chk1固定剂.Checked) { str药品剂型 += "1,"; }
            if (chk2散装药.Checked) { str药品剂型 += "2,"; }
            if (chk3板式药.Checked) { str药品剂型 += "3,"; }
            if (chk4注射剂.Checked) { str药品剂型 += "4,"; }
            if (str药品剂型.Length >= 1)
            {
                str药品剂型 = str药品剂型.Remove(str药品剂型.Length - 1);
            }
            _BLL.DataBinder.Rows[0][tb_MXB肺结核后续随访.药品剂型] = str药品剂型;

            string str停药原因 = "";
            if (chk1完成诊疗.Checked) { str停药原因 += "1,"; }
            if (chk2死亡.Checked) { str停药原因 += "2,"; }
            if (chk3丢失.Checked) { str停药原因 += "3,"; }
            if (chk4转入耐药治疗.Checked) { str停药原因 += "4,"; }
            
            _BLL.DataBinder.Rows[0][tb_MXB肺结核后续随访.停止治疗原因] = str停药原因.Length > 0 ? str停药原因.Substring(0, str停药原因.Length-1):str停药原因;

            if (_UpdateType == UpdateType.Modify)
            {
                _BLL.WriteLog(); //注意:只有修改状态下保存修改日志
            }

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据
            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法
            if (result.Success)
            {
                //Msg.ShowInformation("保存成功!");
                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面


                UC肺结核后续随访_显示 control = new UC肺结核后续随访_显示(_dr个人档案信息, _BLL.DataBinder.Rows[0][tb_MXB肺结核后续随访.创建时间]);
                ShowControl(control, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("保存失败!" + result.Description);
            }
        }

        #region 页面控制
        private void chk0没有症状_CheckedChanged(object sender, EventArgs e)
        {
            if (chk0没有症状.Checked)
            {
                chk1咳嗽咳痰.Enabled = false;
                chk2低热盗汗.Enabled = false;
                chk3咯血血痰.Enabled = false;
                chk4胸痛消瘦.Enabled = false;
                chk5恶心纳差.Enabled = false;
                chk6关节疼痛.Enabled = false;
                chk7头痛失眠.Enabled = false;
                chk8视物模糊.Enabled = true;
                chk9皮痒皮疹.Enabled = false;
                chk10耳鸣.Enabled = false;
            }
            else
            {
                chk1咳嗽咳痰.Enabled = true;
                chk2低热盗汗.Enabled = true;
                chk3咯血血痰.Enabled = true;
                chk4胸痛消瘦.Enabled = true;
                chk5恶心纳差.Enabled = true;
                chk6关节疼痛.Enabled = true;
                chk7头痛失眠.Enabled = true;
                chk8视物模糊.Enabled = true;
                chk9皮痒皮疹.Enabled = true;
                chk10耳鸣.Enabled = true;
            }
        }

        private void gr药物不良反应_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(rg药物不良反应.EditValue == "2")
            {
                txt药物不良反应有.Enabled = true;
            }
            else
            {
                txt药物不良反应有.Enabled = false;
            }
        }

        private void rg并发症_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rg并发症.EditValue == "2")
            {
                txt并发症有.Enabled = true;
            }
            else
            {
                txt并发症有.Enabled = false;
            }
        }
        #endregion
    }
}
