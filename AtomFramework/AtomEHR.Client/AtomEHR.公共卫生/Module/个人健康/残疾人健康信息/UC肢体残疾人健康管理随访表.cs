﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    public partial class UC肢体残疾人健康管理随访表 : UserControlBase
    {

        #region Fields
        AtomEHR.Business.bll残疾人_肢体 _Bll = new Business.bll残疾人_肢体();
        DataSet _ds残疾人肢体;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC肢体残疾人健康管理随访表()
        {
            InitializeComponent();
        }

        public UC肢体残疾人健康管理随访表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                _Bll.NewBusiness();
                _ds残疾人肢体 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds残疾人肢体 = _Bll.GetOneDataByKey(_docNo, _id, true);
                this.dateEdit本次随访日期.Properties.ReadOnly = true;
                this.dateEdit下次随访日期.Properties.ReadOnly = true;
            }

        }
        private void DoBindingSummaryEditor(DataSet _ds残疾人肢体)
        {
            if (_ds残疾人肢体 == null) return;
            if (_ds残疾人肢体.Tables.Count == 0) return;
            DataTable dt残疾人基本信息 = _ds残疾人肢体.Tables[tb_残疾人_基本信息.__TableName];
            DataTable dt随访 = _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName];
            if (dt残疾人基本信息.Rows.Count == 1)
            {
                DataRow dr = dt残疾人基本信息.Rows[0];
                this.labelControl姓名.Text = util.DESEncrypt.DES解密(dr[tb_残疾人_基本信息.姓名].ToString());
                this.labelControl性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_残疾人_基本信息.性别].ToString());
                this.labelControl婚姻状况.Text = _BLL.ReturnDis字典显示("hyzk", dr[tb_残疾人_基本信息.婚姻状况].ToString());
                this.labelControl个人档案号.Text = dr[tb_残疾人_基本信息.个人档案编号].ToString();
                this.labelControl身份证号.Text = dr[tb_残疾人_基本信息.身份证号].ToString();
                this.labelControl出生日期.Text = dr[tb_残疾人_基本信息.出生日期].ToString();
                this.labelControl联系电话.Text = dr[tb_残疾人_基本信息.联系电话].ToString();
                this.labelControl居住地址.Text = _Bll.Return地区名称(dr[tb_残疾人_基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.居委会].ToString()) + dr[tb_残疾人_基本信息.居住地址].ToString();

            }
            if (dt随访.Rows.Count == 1)
            {
                BindSuiFangData(dt随访);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.labelControl创建人.Text = Loginer.CurrentUser.AccountName;
                this.labelControl创建时间.Text = _serverDateTime;
                this.labelControl最近更新时间.Text = _serverDateTime;
                this.labelControl当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.labelControl创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.labelControl最近修改人.Text = Loginer.CurrentUser.AccountName;
                this.labelControl录入医生.Text = Loginer.CurrentUser.AccountName;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.labelControl创建人.Text = _Bll.Return用户名称(dt随访.Rows[0][tb_残疾人_肢体.创建人].ToString());
                this.labelControl创建时间.Text = _serverDateTime;
                this.labelControl最近更新时间.Text = _serverDateTime;
                this.labelControl当前所属机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_残疾人_肢体.所属机构].ToString());
                this.labelControl创建机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_残疾人_肢体.创建机构].ToString());
                this.labelControl最近修改人.Text = Loginer.CurrentUser.AccountName;
                this.labelControl录入医生.Text = dt随访.Rows[0][tb_残疾人_肢体.随访医生].ToString();
            }
        }
        private void BindSuiFangData(DataTable dataTable)
        {
            if (dataTable == null) return;
            DataBinder.BindingTextEditDateTime(dte残疾时间, dataTable, tb_残疾人_肢体.致残时间);
            DataBinder.BindingTextEdit(textEdit残疾证号, dataTable, tb_残疾人_肢体.残疾证号);
            DataBinder.BindingTextEdit(textEdit致残原因其他, dataTable, tb_残疾人_肢体.致残原因其他);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人_肢体.致残原因].ToString(), flow致残原因);
            DataBinder.BindingRadioEdit(radio残疾程度, dataTable, tb_残疾人_肢体.残疾程度);
            DataBinder.BindingRadioEdit(radio持续时间, dataTable, tb_残疾人_肢体.持续时间);
            if (dataTable.Rows[0][tb_残疾人_肢体.其他伴随残疾].ToString() == "1")
            {
                this.checkEdit伴随残疾无.Checked = true;
            }
            else
            {
                this.checkEdit伴随残疾无.Checked = false;
                if (dataTable.Rows[0][tb_残疾人_肢体.FIELD2].ToString() == "on")//智力残疾
                {
                    this.checkEdit伴随残疾智力.Checked = true;
                    if (!string.IsNullOrEmpty(dataTable.Rows[0][tb_残疾人_肢体.智力残疾].ToString()))//成人 或者 儿童
                    {
                        this.radio伴随残疾智力.EditValue = dataTable.Rows[0][tb_残疾人_肢体.智力残疾].ToString();
                    }
                }
                if (dataTable.Rows[0][tb_残疾人_肢体.FIELD3].ToString() == "on")//肢体残疾
                {
                    this.checkEdit伴随残疾肢体.Checked = true;
                    if (!string.IsNullOrEmpty(dataTable.Rows[0][tb_残疾人_肢体.肢体残疾].ToString()))//成人 或者 儿童
                    {
                        this.radio伴随残疾智力.EditValue = dataTable.Rows[0][tb_残疾人_肢体.肢体残疾].ToString();
                    }
                }
                if (dataTable.Rows[0][tb_残疾人_肢体.FIELD5].ToString() == "on")//精神病残疾
                {
                    this.checkEdit伴随残疾精神.Checked = true;
                }
            }
            // SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人_肢体.其他伴随残疾].ToString(), flow其他伴随残疾);
            //DataBinder.BindingRadioEdit(radio伴随残疾智力, dataTable, tb_残疾人_肢体.持续时间);
            DataBinder.BindingRadioEdit(radio个人自理, dataTable, tb_残疾人_肢体.个人自理);
            DataBinder.BindingTextEdit(textEdit监护人姓名, dataTable, tb_残疾人_肢体.监护人姓名);
            DataBinder.BindingTextEdit(textEdit监护人联系电话, dataTable, tb_残疾人_肢体.监护人电话);
            if (dataTable.Rows[0][tb_残疾人_肢体.盲校].ToString() == "on") this.checkEdit盲校.Checked = true;
            DataBinder.BindingTextEdit(ucLblTxt盲校年.Txt1, dataTable, tb_残疾人_肢体.盲校年级);
            if (dataTable.Rows[0][tb_残疾人_肢体.聋校].ToString() == "on") this.checkEdit聋校.Checked = true;
            DataBinder.BindingTextEdit(ucLblTxt聋校年.Txt1, dataTable, tb_残疾人_肢体.聋校年纪);
            if (dataTable.Rows[0][tb_残疾人_肢体.其他特殊学校].ToString() == "on") this.checkEdit其他特殊学校.Checked = true;
            DataBinder.BindingTextEdit(textEdit学历其他, dataTable, tb_残疾人_肢体.其他特殊学校年级);
            DataBinder.BindingRadioEdit(radio就业状况, dataTable, tb_残疾人_肢体.就业情况);
            DataBinder.BindingTextEdit(textEdit就业状况其他, dataTable, tb_残疾人_肢体.就业情况其他);
            //DataBinder.BindingTextEdit(textEdit就业状况其他, dataTable, tb_残疾人_肢体.就业情况其他);
            //DataBinder.BindingTextEdit(textEdit工作单位, dataTable, tb_残疾人_肢体.工作单位);
            //DataBinder.BindingTextEdit(textEdit电话, dataTable, tb_残疾人_肢体.电话);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人_肢体.收入来源].ToString(), flow收入来源);
            DataBinder.BindingTextEdit(textEdit收入来源其他, dataTable, tb_残疾人_肢体.收入来源其他);
            DataBinder.BindingRadioEdit(radio平均收入, dataTable, tb_残疾人_肢体.平均收入);
            DataBinder.BindingRadioEdit(radio劳动能力, dataTable, tb_残疾人_肢体.劳动能力);
            DataBinder.BindingRadioEdit(radio劳动技能, dataTable, tb_残疾人_肢体.劳动技能);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人_肢体.能力来源].ToString(), flow能力来源);
            DataBinder.BindingTextEdit(textEdit能力来源, dataTable, tb_残疾人_肢体.能力来源其他);
            DataBinder.BindingControl(memoEdit康复治疗情况, dataTable, tb_残疾人_肢体.康复治疗情况, "Text");
            DataBinder.BindingControl(memoEdit康复需求, dataTable, tb_残疾人_肢体.康复需求, "Text");
            DataBinder.BindingControl(memoEdit伤残部位, dataTable, tb_残疾人_肢体.伤残部位, "Text");

            DataBinder.BindingTextEditDateTime(dateEdit本次随访日期, dataTable, tb_残疾人_肢体.随访时间);
            DataBinder.BindingTextEditDateTime(dateEdit下次随访日期, dataTable, tb_残疾人_肢体.下次随访日期);
            DataBinder.BindingTextEdit(textEdit随访医生, dataTable, tb_残疾人_肢体.随访医生);
        }
        private void UC肢体残疾人健康管理随访表_Load(object sender, EventArgs e)
        {
            DoBindingSummaryEditor(_ds残疾人肢体);//绑定数据
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (check验证())
            {
                if ((_UpdateType == UpdateType.Add && Msg.AskQuestion("信息保存后，‘随访日期’和‘下次随访时间’将不允许修改，确认保存信息？")) || _UpdateType == UpdateType.Modify)
                {
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.致残时间] = this.dte残疾时间.Text;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.随访时间] = this.dateEdit本次随访日期.Text;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.下次随访日期] = this.dateEdit下次随访日期.Text;

                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.个人档案编号] = _docNo;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.其他伴随残疾] = this.checkEdit伴随残疾无.Checked ? "1" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.智力残疾] = this.radio伴随残疾智力.EditValue;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.肢体残疾] = this.radio伴随残疾脑瘫.EditValue;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.FIELD2] = this.checkEdit伴随残疾智力.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.FIELD3] = this.checkEdit伴随残疾肢体.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.FIELD5] = this.checkEdit伴随残疾精神.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.盲校] = this.checkEdit盲校.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.聋校] = this.checkEdit聋校.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.其他特殊学校] = this.checkEdit其他特殊学校.Checked ? "on" : "";
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.收入来源] = GetFlowLayoutResult(flow收入来源);
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.能力来源] = GetFlowLayoutResult(flow能力来源);
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.致残原因] = GetFlowLayoutResult(flow致残原因);

                    int i = Get缺项();
                    int j = Get完整度(i);
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.缺项] = i;
                    _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.完整度] = j;
                    if (_UpdateType == UpdateType.Add)
                    {
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.创建机构] = Loginer.CurrentUser.所属机构;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.所属机构] = Loginer.CurrentUser.所属机构;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.创建日期] = this.labelControl创建时间.Text;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.创建人] = Loginer.CurrentUser.用户编码;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.修改人] = Loginer.CurrentUser.用户编码;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.修改日期] = this.labelControl最近更新时间.Text;
                    }
                    else if (_UpdateType == UpdateType.Modify)
                    {
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.修改人] = Loginer.CurrentUser.用户编码;
                        _ds残疾人肢体.Tables[tb_残疾人_肢体.__TableName].Rows[0][tb_残疾人_肢体.修改日期] = this.labelControl最近更新时间.Text;
                    }
                    _ds残疾人肢体.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = i + "," + j;

                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 ▽
                    #region old code
                    if (_UpdateType == UpdateType.Modify) _Bll.WriteLog();
                    DataSet dsTemplate = _Bll.CreateSaveData(_ds残疾人肢体, _UpdateType); //创建用于保存的临时数据
                    SaveResult result = _Bll.Save(dsTemplate);//调用业务逻辑保存数据方法
                    #endregion
                    //SaveResult result = _Bll.Save(_ds孕妇产前一次随访);//调用业务逻辑保存数据方法
                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 △

                    if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
                    {
                        //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                        //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                        this._UpdateType = UpdateType.None;
                        Msg.ShowInformation("保存成功!");
                        //保存成功后跳转到显示页面
                        UC肢体残疾人健康管理随访表_显示 uc = new UC肢体残疾人健康管理随访表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                        Msg.Warning("保存失败!");
                }
            }
        }
        private int Get缺项()
        {
            int i = 0;
            //if (this.result1.Text.Trim() == "") i++;
            //if (this.result2.Text.Trim() == "") i++;
            //if (this.result3.Text.Trim() == "") i++;
            //if (this.result4.Text.Trim() == "") i++;
            //if (this.result5.Text.Trim() == "") i++;
            //if (this.txt下次随访目标.Text.Trim() == "") i++;
            //if (this.dte随访日期.Text.Trim() == "") i++;
            //if (this.dte下次随访日期.Text.Trim() == "") i++;
            //if (this.txt随访医生签名.Text.Trim() == "") i++;
            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((9 - i) * 100 / 9.0);
            return k;
        }
        private bool check验证()
        {
            if (string.IsNullOrEmpty(this.dateEdit本次随访日期.Text.Trim()))
            {
                Msg.Warning("随访日期是必填项！");
                this.dateEdit本次随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dateEdit下次随访日期.Text.Trim()))
            {
                Msg.Warning("下次随访日期是必填项！");
                this.dateEdit下次随访日期.Focus();
                return false;
            }
            if (this.dateEdit下次随访日期.DateTime <= this.dateEdit本次随访日期.DateTime)
            {
                Msg.Warning("下次随访日期填写不能小于本次随访日期！");
                this.dateEdit下次随访日期.Focus();
                return false;
            }
            return true;
        }
        private void btn重置_Click(object sender, EventArgs e)
        {

        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            string name = chk.Name;//chk11
            string result = "result";//result
            if (chk.Checked)
            {
                int value = Convert.ToInt32(chk.Tag);
                result += name.Substring(3, 1);
                Control ctrl = this.Controls.Find(result, true)[0];//根据name找到控件
                if (ctrl is LabelControl)
                {
                    ctrl.Text = value.ToString();
                }
            }
        }
        private void checkEdit伴随残疾无_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkEdit伴随残疾无.Checked)
            {
                this.checkEdit伴随残疾精神.Enabled = this.checkEdit伴随残疾肢体.Enabled = this.checkEdit伴随残疾智力.Enabled = false;
            }
            else
            {
                this.checkEdit伴随残疾精神.Enabled = this.checkEdit伴随残疾肢体.Enabled = this.checkEdit伴随残疾智力.Enabled = true;
            }
        }
        private void checkEdit伴随残疾智力_CheckedChanged(object sender, EventArgs e)
        {
            this.radio伴随残疾智力.Visible = this.checkEdit伴随残疾智力.Checked;
        }
        private void checkEdit伴随残疾肢体_CheckedChanged(object sender, EventArgs e)
        {
            this.radio伴随残疾脑瘫.Visible = this.checkEdit伴随残疾肢体.Checked;
        }
    }
}
