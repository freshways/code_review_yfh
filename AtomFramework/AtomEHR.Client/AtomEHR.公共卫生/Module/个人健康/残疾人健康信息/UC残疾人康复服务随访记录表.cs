﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    public partial class UC残疾人康复服务随访记录表 : UserControlBase
    {

        #region Fields
        AtomEHR.Business.bll残疾人康复服务随访记录表 _Bll = new Business.bll残疾人康复服务随访记录表();
        DataSet _ds残疾人;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC残疾人康复服务随访记录表()
        {
            InitializeComponent();
        }

        public UC残疾人康复服务随访记录表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                _Bll.NewBusiness();
                _ds残疾人 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds残疾人 = _Bll.GetOneDataByKey(_docNo, _id, true);
                this.dateEdit随访日期.Properties.ReadOnly = true;
                this.dateEdit下次随访日期.Properties.ReadOnly = true;
            }

        }
        private void DoBindingSummaryEditor(DataSet _ds残疾人)
        {
            if (_ds残疾人 == null) return;
            if (_ds残疾人.Tables.Count == 0) return;
            DataTable dt残疾人基本信息 = _ds残疾人.Tables[tb_残疾人_基本信息.__TableName];
            DataTable dt随访 = _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName];
            if (dt残疾人基本信息.Rows.Count == 1)
            {
                DataRow dr = dt残疾人基本信息.Rows[0];
                this.labelControl姓名.Text = util.DESEncrypt.DES解密(dr[tb_残疾人_基本信息.姓名].ToString());
                this.labelControl性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_残疾人_基本信息.性别].ToString());
                this.labelControl婚姻状况.Text = _BLL.ReturnDis字典显示("hyzk", dr[tb_残疾人_基本信息.婚姻状况].ToString());
                this.labelControl个人档案号.Text = dr[tb_残疾人_基本信息.个人档案编号].ToString();
                this.labelControl身份证号.Text = dr[tb_残疾人_基本信息.身份证号].ToString();
                this.labelControl出生日期.Text = dr[tb_残疾人_基本信息.出生日期].ToString();
                this.labelControl联系电话.Text = dr[tb_残疾人_基本信息.联系电话].ToString();
                this.labelControl居住地址.Text = _Bll.Return地区名称(dr[tb_残疾人_基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.居委会].ToString()) + dr[tb_残疾人_基本信息.居住地址].ToString();

            }
            if (dt随访.Rows.Count == 1)
            {
                BindSuiFangData(dt随访);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.labelControl创建人.Text = Loginer.CurrentUser.AccountName;
                this.labelControl创建时间.Text = _serverDateTime;
                this.labelControl最近更新时间.Text = _serverDateTime;
                this.labelControl当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.labelControl创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.labelControl最近修改人.Text = Loginer.CurrentUser.AccountName;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.labelControl创建人.Text = _Bll.Return用户名称(dt随访.Rows[0][tb_残疾人康复服务随访记录表.创建人].ToString());
                this.labelControl创建时间.Text = _serverDateTime;
                this.labelControl最近更新时间.Text = _serverDateTime;
                this.labelControl当前所属机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_残疾人康复服务随访记录表.所属机构].ToString());
                this.labelControl创建机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_残疾人康复服务随访记录表.创建机构].ToString());
                this.labelControl最近修改人.Text = Loginer.CurrentUser.AccountName;
            }
        }
        private void BindSuiFangData(DataTable dataTable)
        {
            if (dataTable == null) return;
            DataBinder.BindingRadioEdit(radio主要残疾, dataTable, tb_残疾人康复服务随访记录表.主要残疾);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人康复服务随访记录表.多重残疾].ToString(), flow多重残疾);
            DataBinder.BindingRadioEdit(radio残疾程度, dataTable, tb_残疾人康复服务随访记录表.残疾程度);
            DataBinder.BindingTextEditDateTime(dateEdit随访日期, dataTable, tb_残疾人康复服务随访记录表.随访日期);
            DataBinder.BindingRadioEdit(radio随访方式, dataTable, tb_残疾人康复服务随访记录表.随访方式);
            DataBinder.BindingTextEdit(textEdit血压.Txt1, dataTable, tb_残疾人康复服务随访记录表.血压高压);
            DataBinder.BindingTextEdit(textEdit血压.Txt2, dataTable, tb_残疾人康复服务随访记录表.血压低压);
            DataBinder.BindingTextEdit(textEdit体重.Txt1, dataTable, tb_残疾人康复服务随访记录表.体重);
            DataBinder.BindingTextEdit(textEdit心率.Txt1, dataTable, tb_残疾人康复服务随访记录表.心率1);
            DataBinder.BindingTextEdit(textEdit心率.Txt2, dataTable, tb_残疾人康复服务随访记录表.心率2);
            DataBinder.BindingTextEdit(textEdit体检其他, dataTable, tb_残疾人康复服务随访记录表.体检其他);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人康复服务随访记录表.康复服务情况].ToString(), flow康复服务情况);
            DataBinder.BindingTextEdit(textEdit康复服务其他, dataTable, tb_残疾人康复服务随访记录表.康复服务情况其他);
            DataBinder.BindingTextEdit(textEdit转介原因, dataTable, tb_残疾人康复服务随访记录表.转介原因);
            DataBinder.BindingTextEdit(textEdit转介去向, dataTable, tb_残疾人康复服务随访记录表.转介去向);
            DataBinder.BindingTextEdit(textEdit功能训练1.Txt1, dataTable, tb_残疾人康复服务随访记录表.功能训练每月次1);
            DataBinder.BindingTextEdit(textEdit功能训练1.Txt2, dataTable, tb_残疾人康复服务随访记录表.功能训练每次分钟1);
            DataBinder.BindingTextEdit(textEdit功能训练2.Txt1, dataTable, tb_残疾人康复服务随访记录表.功能训练每月次2);
            DataBinder.BindingTextEdit(textEdit功能训练2.Txt2, dataTable, tb_残疾人康复服务随访记录表.功能训练每次分钟2);
            DataBinder.BindingRadioEdit(radio训练场地, dataTable, tb_残疾人康复服务随访记录表.训练场地);
            DataBinder.BindingTextEdit(textEdit训练评估分数.Txt1, dataTable, tb_残疾人康复服务随访记录表.训练评估分数);
            SetFlowLayoutResult(dataTable.Rows[0][tb_残疾人康复服务随访记录表.康复目标].ToString(), flow康复目标);
            DataBinder.BindingTextEdit(textEdit康复目标其他, dataTable, tb_残疾人康复服务随访记录表.康复目标其他);
            DataBinder.BindingRadioEdit(radio训练效果, dataTable, tb_残疾人康复服务随访记录表.训练效果);
            DataBinder.BindingRadioEdit(radio遵医行为, dataTable, tb_残疾人康复服务随访记录表.遵医行为);
            DataBinder.BindingRadioEdit(radio随访分类, dataTable, tb_残疾人康复服务随访记录表.此次随访分类);
            DataBinder.BindingTextEditDateTime(dateEdit下次随访日期, dataTable, tb_残疾人康复服务随访记录表.下次随访日期);
            DataBinder.BindingTextEdit(textEdit服务对象或家属, dataTable, tb_残疾人康复服务随访记录表.服务对象或家属);
            DataBinder.BindingTextEdit(textEdit随访医生, dataTable, tb_残疾人康复服务随访记录表.随访医生);
        }
        private void UC残疾人康复服务随访记录表_Load(object sender, EventArgs e)
        {
            DoBindingSummaryEditor(_ds残疾人);//绑定数据
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (check验证())
            {
                if ((_UpdateType == UpdateType.Add && Msg.AskQuestion("信息保存后，‘随访日期’和‘下次随访时间’将不允许修改，确认保存信息？")) || _UpdateType == UpdateType.Modify)
                {
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.随访日期] = this.dateEdit随访日期.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.下次随访日期] = this.dateEdit下次随访日期.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.个人档案编号] = _docNo;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.主要残疾] = radio主要残疾.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.多重残疾] = GetFlowLayoutResult(flow多重残疾);
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.残疾程度] = radio残疾程度.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.随访方式] = radio随访方式.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.是否多重残疾] = this.chk多重残疾_否.Checked ? "1" : "";
                    if(!string.IsNullOrEmpty(this.textEdit血压.Txt1.Text))
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.血压高压] =Convert.ToInt32(this.textEdit血压.Txt1.Text);

                    }
                    if (!string.IsNullOrEmpty(this.textEdit血压.Txt2.Text))
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.血压低压] =Convert.ToInt32(this.textEdit血压.Txt2.Text);

                    }
                    if (!string.IsNullOrEmpty(this.textEdit体重.Txt1.Text))
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.体重] = Convert.ToDecimal(this.textEdit体重.Txt1.Text);
                    }
                    if (!string.IsNullOrEmpty(this.textEdit心率.Txt1.Text))
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.心率1] = Convert.ToInt32(this.textEdit心率.Txt1.Text);
                    }
                    if (!string.IsNullOrEmpty(this.textEdit心率.Txt2.Text))
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.心率2] = Convert.ToInt32(this.textEdit心率.Txt2.Text);
                    }
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.体检其他] = this.textEdit体检其他.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.康复服务情况] = GetFlowLayoutResult(flow康复服务情况);
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.康复服务情况其他] = this.textEdit康复服务其他.Text.Trim();
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.转介原因] = this.textEdit转介原因.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.转介去向] = this.textEdit转介去向.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.功能训练每月次1] = this.textEdit功能训练1.Txt1.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.功能训练每次分钟1] = this.textEdit功能训练1.Txt2.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.功能训练每月次2] = textEdit功能训练2.Txt1.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.功能训练每次分钟2] = textEdit功能训练2.Txt2.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.训练评估分数] = this.textEdit训练评估分数.Txt1.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.训练场地] = radio训练场地.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.训练效果] = radio训练效果.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.遵医行为] = radio遵医行为.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.此次随访分类] = radio随访分类.EditValue;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.康复目标] = GetFlowLayoutResult(flow康复目标);
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.康复目标其他] = this.textEdit康复目标其他.Text.Trim();
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.服务对象或家属] = this.textEdit服务对象或家属.Text;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.随访医生] = this.textEdit随访医生.Text;

                    int i = Get缺项();
                    int j = Get完整度(i);
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.缺项] = i;
                    _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.完整度] = j;
                    if (_UpdateType == UpdateType.Add)
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.创建机构] = Loginer.CurrentUser.所属机构;
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.所属机构] = Loginer.CurrentUser.所属机构;
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.创建时间] = this.labelControl创建时间.Text;
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.修改人] = Loginer.CurrentUser.用户编码;
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.修改时间] = this.labelControl最近更新时间.Text;
                    }
                    else if (_UpdateType == UpdateType.Modify)
                    {
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.修改人] = Loginer.CurrentUser.用户编码;
                        _ds残疾人.Tables[tb_残疾人康复服务随访记录表.__TableName].Rows[0][tb_残疾人康复服务随访记录表.修改时间] = this.labelControl最近更新时间.Text;
                    }

                    _ds残疾人.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表] = i + "," + j;

                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 ▽
                    #region old code
                    if (_UpdateType == UpdateType.Modify) _Bll.WriteLog();
                    DataSet dsTemplate = _Bll.CreateSaveData(_ds残疾人, _UpdateType); //创建用于保存的临时数据
                    SaveResult result = _Bll.Save(dsTemplate);//调用业务逻辑保存数据方法
                    #endregion
                    //SaveResult result = _Bll.Save(_ds孕妇产前一次随访);//调用业务逻辑保存数据方法
                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 △

                    if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
                    {
                        //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                        //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                        this._UpdateType = UpdateType.None;
                        Msg.ShowInformation("保存成功!");
                        //保存成功后跳转到显示页面
                        UC残疾人康复服务随访记录表_显示 uc = new UC残疾人康复服务随访记录表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                        Msg.Warning("保存失败!");
                }
            }
        }

        private int Get缺项()
        {
            int i = 0;
            //if (this.result1.Text.Trim() == "") i++;
            //if (this.result2.Text.Trim() == "") i++;
            //if (this.result3.Text.Trim() == "") i++;
            //if (this.result4.Text.Trim() == "") i++;
            //if (this.result5.Text.Trim() == "") i++;
            //if (this.txt下次随访目标.Text.Trim() == "") i++;
            //if (this.dte随访日期.Text.Trim() == "") i++;
            //if (this.dte下次随访日期.Text.Trim() == "") i++;
            //if (this.txt随访医生签名.Text.Trim() == "") i++;
            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((9 - i) * 100 / 9.0);
            return k;
        }
        private bool check验证()
        {
            if (string.IsNullOrEmpty(this.dateEdit随访日期.Text.Trim()))
            {
                Msg.Warning("随访日期是必填项！");
                this.dateEdit随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dateEdit下次随访日期.Text.Trim()))
            {
                Msg.Warning("请填写下次随访日期！");
                this.dateEdit下次随访日期.Focus();
                return false;
            }
            if (this.dateEdit下次随访日期.DateTime <= this.dateEdit随访日期.DateTime)
            {
                Msg.Warning("下次随访日期填写不能小于随访日期！");
                this.dateEdit下次随访日期.Focus();
                return false;
            }
            return true;
        }
        private void btn重置_Click(object sender, EventArgs e)
        {

        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            string name = chk.Name;//chk11
            string result = "result";//result
            if (chk.Checked)
            {
                int value = Convert.ToInt32(chk.Tag);
                result += name.Substring(3, 1);
                Control ctrl = this.Controls.Find(result, true)[0];//根据name找到控件
                if (ctrl is LabelControl)
                {
                    ctrl.Text = value.ToString();
                }
            }
        }

        private void chk多重残疾_否_CheckedChanged(object sender, EventArgs e)
        {
            this.chk多重残疾_视力残疾.Enabled = this.chk多重残疾_听力残疾.Enabled =
                this.chk多重残疾_言语残疾.Enabled = this.chk多重残疾_肢体残疾.Enabled =
                this.chk多重残疾_智力残疾.Enabled = this.chk多重残疾_精神残疾.Enabled = !this.chk多重残疾_否.Checked;
            if (this.chk多重残疾_否.Checked)
            {
                this.chk多重残疾_视力残疾.Checked = this.chk多重残疾_听力残疾.Checked =
              this.chk多重残疾_言语残疾.Checked = this.chk多重残疾_肢体残疾.Checked =
              this.chk多重残疾_智力残疾.Checked = this.chk多重残疾_精神残疾.Checked = false;
            }

        }

        private void btn添加药物_Click(object sender, EventArgs e)
        {
              
        }
    }
}

