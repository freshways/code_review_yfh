﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;
using AtomEHR.Library.UserControls;

namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    public partial class UC残疾人康复服务随访记录表_显示 : UserControlBaseNavBar
    {

        #region Fields
        AtomEHR.Business.bll残疾人康复服务随访记录表 _Bll = new Business.bll残疾人康复服务随访记录表();
        DataSet _ds残疾人;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        private string dno;
        private Form frm个人健康;
        DataRow _dr当前数据;
        #endregion
        public UC残疾人康复服务随访记录表_显示()
        {
            InitializeComponent();
        }
        public UC残疾人康复服务随访记录表_显示(Form frm)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _id = _frm._param as string;
            //_ds残疾人 = _Bll.GetAllDataByKey(_docNo, true);
            //DoBindingSummaryEditor(_ds残疾人);//绑定数据
        }
        private void btn添加随访_Click(object sender, EventArgs e)
        {
            UC残疾人康复服务随访记录表 uc = new UC残疾人康复服务随访记录表(_frm, UpdateType.Add);
            ShowControl(uc);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_残疾人_基本信息.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC残疾人康复服务随访记录表 uc = new UC残疾人康复服务随访记录表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_残疾人_基本信息.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                if (_Bll.Delete(_docNo, _id))
                {
                    Msg.Warning("删除数据成功！");

                    if (!_Bll.CheckNoExists(_docNo))
                    {
                        UC残疾人康复服务随访记录表 uc = new UC残疾人康复服务随访记录表(_frm, UpdateType.Add);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                    {
                        _id = "";
                        _ds残疾人 = _Bll.GetAllDataByKey(_docNo, true);
                        DoBindingDataSource(_ds残疾人);//绑定数据
                        this.layoutControl1.VerticalScroll.Value = 0;
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void UC残疾人康复服务随访记录表_显示_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i] is TextEdit)
                {
                    TextEdit txt = layoutControl1.Controls[i] as TextEdit;
                    txt.Properties.ReadOnly = true;
                    txt.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxt)
                {
                    UCLblTxt txt = layoutControl1.Controls[i] as UCLblTxt;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLbl)
                {
                    UCTxtLbl txt = layoutControl1.Controls[i] as UCTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLblTxtLbl)
                {
                    UCTxtLblTxtLbl txt = layoutControl1.Controls[i] as UCTxtLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                    txt.Txt2.Properties.ReadOnly = true;
                    txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = layoutControl1.Controls[i] as FlowLayoutPanel;
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is TextEdit)
                        {
                            TextEdit txt = flow.Controls[j] as TextEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxt)
                        {
                            UCLblTxt txt = flow.Controls[j] as UCLblTxt;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLbl)
                        {
                            UCTxtLbl txt = flow.Controls[j] as UCTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxtLbl)
                        {
                            UCLblTxtLbl txt = flow.Controls[j] as UCLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLblTxtLbl)
                        {
                            UCTxtLblTxtLbl txt = flow.Controls[j] as UCTxtLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                            txt.Txt2.Properties.ReadOnly = true;
                            txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is CheckEdit)
                        {
                            CheckEdit txt = flow.Controls[j] as CheckEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                    }
                }
            }
            //根据个人档案编号 查询全部的  体检数据
            _ds残疾人 = _Bll.GetAllDataByKey(_docNo, true);
            DoBindingDataSource(_ds残疾人);//绑定数据
        }
        private void DoBindingDataSource(DataSet  _ds残疾人)
        {
            if (_ds残疾人 == null) return;
            DataTable dt残疾人康复服务随访记录表 = _ds残疾人.Tables[1];
            DataTable dt残疾人基本信息 = _ds残疾人.Tables[0];
            #region 先绑定残疾人基本信息
            Bind基本信息(dt残疾人基本信息);
            #endregion

            #region 创建页面左边的导航树
            CreateNavBarButton_new(dt残疾人康复服务随访记录表, tb_残疾人康复服务随访记录表.随访日期);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt残疾人康复服务随访记录表.Rows.Count > 0)
            {
                row = dt残疾人康复服务随访记录表.Rows[0];
            }
            else
            {
                DataRow[] rows = dt残疾人康复服务随访记录表.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
            //设置日期栏中字体颜色
            SetItemColorToRed(_id);
        }
        private void Bind基本信息(DataTable dt残疾人基本信息)
        {
            if (dt残疾人基本信息.Rows.Count == 1)
            {
                DataRow dr = dt残疾人基本信息.Rows[0];
                this.labelControl姓名.Text = util.DESEncrypt.DES解密(dr[tb_残疾人_基本信息.姓名].ToString());
                this.labelControl性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_残疾人_基本信息.性别].ToString());
                this.labelControl婚姻状况.Text = _BLL.ReturnDis字典显示("hyzk", dr[tb_残疾人_基本信息.婚姻状况].ToString());
                this.labelControl个人档案号.Text = dr[tb_残疾人_基本信息.个人档案编号].ToString();
                this.labelControl身份证号.Text = dr[tb_残疾人_基本信息.身份证号].ToString();
                this.labelControl出生日期.Text = dr[tb_残疾人_基本信息.出生日期].ToString();
                this.labelControl联系电话.Text = dr[tb_残疾人_基本信息.联系电话].ToString();
                this.labelControl居住地址.Text = _Bll.Return地区名称(dr[tb_残疾人_基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.居委会].ToString());
                //dr[tb_残疾人_基本信息.居住地址].ToString();

            }
        }
        protected override void DoBindingSummaryEditor(object dataSource) 
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;
            this.radio主要残疾.Text = _Bll.ReturnDis字典显示("zycj", _dr当前数据[tb_残疾人康复服务随访记录表.主要残疾].ToString());
            SetFlowLayoutResult(_dr当前数据[tb_残疾人康复服务随访记录表.多重残疾].ToString(), flow多重残疾);
            this.radio残疾程度.Text = _Bll.ReturnDis字典显示("cjcd", _dr当前数据[tb_残疾人康复服务随访记录表.残疾程度].ToString());
            this.radio随访方式.Text = _Bll.ReturnDis字典显示("sffs", _dr当前数据[tb_残疾人康复服务随访记录表.随访方式].ToString());
            this.radio训练场地.Text = _Bll.ReturnDis字典显示("xlcd", _dr当前数据[tb_残疾人康复服务随访记录表.训练场地].ToString());
            this.radio训练效果.Text = _Bll.ReturnDis字典显示("xlxg", _dr当前数据[tb_残疾人康复服务随访记录表.训练效果].ToString());
            this.radio遵医行为.Text = _Bll.ReturnDis字典显示("zyxw", _dr当前数据[tb_残疾人康复服务随访记录表.遵医行为].ToString());
            this.radio随访分类.Text = _Bll.ReturnDis字典显示("ccsffl", _dr当前数据[tb_残疾人康复服务随访记录表.此次随访分类].ToString());
            SetFlowLayoutResult(_dr当前数据[tb_残疾人康复服务随访记录表.康复服务情况].ToString(), flow康复服务情况);
            this.textEdit康复服务情况其他.Text = _dr当前数据[tb_残疾人康复服务随访记录表.康复服务情况其他].ToString();
            SetFlowLayoutResult(_dr当前数据[tb_残疾人康复服务随访记录表.康复目标].ToString(), flow康复目标);
            this.textEdit康复目标其他.Text = _dr当前数据[tb_残疾人康复服务随访记录表.康复目标其他].ToString();
            this.textEdit血压.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.血压高压].ToString();
            this.textEdit血压.Txt2.Text = _dr当前数据[tb_残疾人康复服务随访记录表.血压低压].ToString();
            this.textEdit体重.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.体重].ToString();
            this.textEdit心率.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.心率1].ToString();
            this.textEdit心率.Txt2.Text = _dr当前数据[tb_残疾人康复服务随访记录表.心率2].ToString();
            this.textEdit体检其他.Text = _dr当前数据[tb_残疾人康复服务随访记录表.体检其他].ToString();
            this.textEdit转介原因.Text = _dr当前数据[tb_残疾人康复服务随访记录表.转介原因].ToString();
            this.textEdit转介去向.Text = _dr当前数据[tb_残疾人康复服务随访记录表.转介去向].ToString();
            this.textEdit功能训练1.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.功能训练每月次1].ToString();
            this.textEdit功能训练1.Txt2.Text = _dr当前数据[tb_残疾人康复服务随访记录表.功能训练每次分钟1].ToString();
            this.textEdit功能训练2.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.功能训练每月次2].ToString();
            this.textEdit功能训练2.Txt2.Text = _dr当前数据[tb_残疾人康复服务随访记录表.功能训练每次分钟2].ToString();
            this.textEdit训练评估分数.Txt1.Text = _dr当前数据[tb_残疾人康复服务随访记录表.训练评估分数].ToString();
            this.dateEdit随访日期.Text = _dr当前数据[tb_残疾人康复服务随访记录表.随访日期].ToString();
            this.dateEdit下次随访日期.Text = _dr当前数据[tb_残疾人康复服务随访记录表.下次随访日期].ToString();
            this.textEdit服务对象或家属.Text = _dr当前数据[tb_残疾人康复服务随访记录表.服务对象或家属].ToString();
            this.textEdit随访医生.Text = _dr当前数据[tb_残疾人康复服务随访记录表.随访医生].ToString();

            this.labelControl创建时间.Text = _dr当前数据[tb_残疾人康复服务随访记录表.创建时间].ToString();
            this.labelControl最近更新时间.Text = _dr当前数据[tb_残疾人康复服务随访记录表.修改时间].ToString();
            this.labelControl当前所属机构.Text = _Bll.Return机构名称(_dr当前数据[tb_残疾人康复服务随访记录表.所属机构].ToString());
            this.labelControl创建机构.Text = _Bll.Return机构名称(_dr当前数据[tb_残疾人康复服务随访记录表.创建机构].ToString());
            this.labelControl创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_残疾人康复服务随访记录表.创建人].ToString());
            this.labelControl最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_残疾人康复服务随访记录表.修改人].ToString());

        }
    }
}
