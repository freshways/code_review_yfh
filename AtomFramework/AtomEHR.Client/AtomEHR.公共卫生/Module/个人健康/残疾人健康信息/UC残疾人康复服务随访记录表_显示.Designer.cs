﻿namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    partial class UC残疾人康复服务随访记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC残疾人康复服务随访记录表_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加随访 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gv用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit心率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.flow多重残疾 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk多重残疾_否 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_视力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_听力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_言语残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_肢体残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_智力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_精神残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.radio随访分类 = new DevExpress.XtraEditors.TextEdit();
            this.radio遵医行为 = new DevExpress.XtraEditors.TextEdit();
            this.radio训练效果 = new DevExpress.XtraEditors.TextEdit();
            this.radio训练场地 = new DevExpress.XtraEditors.TextEdit();
            this.radio随访方式 = new DevExpress.XtraEditors.TextEdit();
            this.radio残疾程度 = new DevExpress.XtraEditors.TextEdit();
            this.radio主要残疾 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit体检其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flow康复服务情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk康复服务_医疗康复 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_功能训练 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_辅助器具 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_心理服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_知识普及 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_转介服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit康复服务情况其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit功能训练2 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.labelControl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl性别 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit转介去向 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转介原因 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit功能训练1 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit服务对象或家属 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit训练评估分数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flow康复目标 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk康复目标_运动 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_感知 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_认知 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_交往 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_自理 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_适应 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit康复目标其他 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator8 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).BeginInit();
            this.flow多重残疾.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_否.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_视力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_听力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_言语残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_肢体残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_智力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_精神残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio遵医行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练效果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练场地.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检其他.Properties)).BeginInit();
            this.flow康复服务情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_医疗康复.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_功能训练.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_辅助器具.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_心理服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_知识普及.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_转介服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复服务情况其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介去向.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit服务对象或家属.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            this.flow康复目标.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_运动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_感知.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_认知.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_交往.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_自理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_适应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复目标其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(107, 573);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(107, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加随访);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加随访
            // 
            this.btn添加随访.Image = global::AtomEHR.公共卫生.Properties.Resources.add_16x16;
            this.btn添加随访.Location = new System.Drawing.Point(3, 3);
            this.btn添加随访.Name = "btn添加随访";
            this.btn添加随访.Size = new System.Drawing.Size(84, 23);
            this.btn添加随访.TabIndex = 0;
            this.btn添加随访.Text = "添加随访";
            this.btn添加随访.Click += new System.EventHandler(this.btn添加随访_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(93, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 1;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(174, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 2;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Location = new System.Drawing.Point(255, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 3;
            this.btn导出.Text = "导出";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.textEdit心率);
            this.layoutControl1.Controls.Add(this.flow多重残疾);
            this.layoutControl1.Controls.Add(this.radio随访分类);
            this.layoutControl1.Controls.Add(this.radio遵医行为);
            this.layoutControl1.Controls.Add(this.radio训练效果);
            this.layoutControl1.Controls.Add(this.radio训练场地);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.radio残疾程度);
            this.layoutControl1.Controls.Add(this.radio主要残疾);
            this.layoutControl1.Controls.Add(this.textEdit体检其他);
            this.layoutControl1.Controls.Add(this.textEdit体重);
            this.layoutControl1.Controls.Add(this.flow康复服务情况);
            this.layoutControl1.Controls.Add(this.textEdit功能训练2);
            this.layoutControl1.Controls.Add(this.labelControl最近修改人);
            this.layoutControl1.Controls.Add(this.labelControl创建人);
            this.layoutControl1.Controls.Add(this.labelControl创建机构);
            this.layoutControl1.Controls.Add(this.labelControl当前所属机构);
            this.layoutControl1.Controls.Add(this.labelControl最近更新时间);
            this.layoutControl1.Controls.Add(this.labelControl创建时间);
            this.layoutControl1.Controls.Add(this.labelControl婚姻状况);
            this.layoutControl1.Controls.Add(this.labelControl居住地址);
            this.layoutControl1.Controls.Add(this.labelControl联系电话);
            this.layoutControl1.Controls.Add(this.labelControl出生日期);
            this.layoutControl1.Controls.Add(this.labelControl身份证号);
            this.layoutControl1.Controls.Add(this.labelControl性别);
            this.layoutControl1.Controls.Add(this.labelControl姓名);
            this.layoutControl1.Controls.Add(this.labelControl个人档案号);
            this.layoutControl1.Controls.Add(this.textEdit转介去向);
            this.layoutControl1.Controls.Add(this.textEdit转介原因);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Controls.Add(this.textEdit血压);
            this.layoutControl1.Controls.Add(this.textEdit功能训练1);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit服务对象或家属);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit训练评估分数);
            this.layoutControl1.Controls.Add(this.flow康复目标);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(107, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(594, 78, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 541);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(140, 694);
            this.gridControl1.MainView = this.gv用药情况;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(576, 60);
            this.gridControl1.TabIndex = 116;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv用药情况});
            // 
            // gv用药情况
            // 
            this.gv用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gv用药情况.GridControl = this.gridControl1;
            this.gv用药情况.Name = "gv用药情况";
            this.gv用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // textEdit心率
            // 
            this.textEdit心率.Lbl1Size = new System.Drawing.Size(18, 14);
            this.textEdit心率.Lbl1Text = "/";
            this.textEdit心率.Lbl2Size = new System.Drawing.Size(18, 14);
            this.textEdit心率.Lbl2Text = "";
            this.textEdit心率.Location = new System.Drawing.Point(137, 289);
            this.textEdit心率.Name = "textEdit心率";
            this.textEdit心率.Size = new System.Drawing.Size(195, 20);
            this.textEdit心率.TabIndex = 115;
            this.textEdit心率.Txt1EditValue = null;
            this.textEdit心率.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit心率.Txt2EditValue = null;
            this.textEdit心率.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // flow多重残疾
            // 
            this.flow多重残疾.Controls.Add(this.chk多重残疾_否);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_视力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_听力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_言语残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_肢体残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_智力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_精神残疾);
            this.flow多重残疾.Location = new System.Drawing.Point(125, 150);
            this.flow多重残疾.Name = "flow多重残疾";
            this.flow多重残疾.Size = new System.Drawing.Size(603, 20);
            this.flow多重残疾.TabIndex = 114;
            // 
            // chk多重残疾_否
            // 
            this.chk多重残疾_否.Location = new System.Drawing.Point(3, 3);
            this.chk多重残疾_否.Name = "chk多重残疾_否";
            this.chk多重残疾_否.Properties.Caption = "否";
            this.chk多重残疾_否.Size = new System.Drawing.Size(42, 19);
            this.chk多重残疾_否.TabIndex = 0;
            this.chk多重残疾_否.Tag = "1";
            // 
            // chk多重残疾_视力残疾
            // 
            this.chk多重残疾_视力残疾.Location = new System.Drawing.Point(51, 3);
            this.chk多重残疾_视力残疾.Name = "chk多重残疾_视力残疾";
            this.chk多重残疾_视力残疾.Properties.Caption = "视力残疾";
            this.chk多重残疾_视力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_视力残疾.TabIndex = 1;
            this.chk多重残疾_视力残疾.Tag = "2";
            // 
            // chk多重残疾_听力残疾
            // 
            this.chk多重残疾_听力残疾.Location = new System.Drawing.Point(132, 3);
            this.chk多重残疾_听力残疾.Name = "chk多重残疾_听力残疾";
            this.chk多重残疾_听力残疾.Properties.Caption = "听力残疾";
            this.chk多重残疾_听力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_听力残疾.TabIndex = 2;
            this.chk多重残疾_听力残疾.Tag = "3";
            // 
            // chk多重残疾_言语残疾
            // 
            this.chk多重残疾_言语残疾.Location = new System.Drawing.Point(213, 3);
            this.chk多重残疾_言语残疾.Name = "chk多重残疾_言语残疾";
            this.chk多重残疾_言语残疾.Properties.Caption = "言语残疾";
            this.chk多重残疾_言语残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_言语残疾.TabIndex = 3;
            this.chk多重残疾_言语残疾.Tag = "4";
            // 
            // chk多重残疾_肢体残疾
            // 
            this.chk多重残疾_肢体残疾.Location = new System.Drawing.Point(294, 3);
            this.chk多重残疾_肢体残疾.Name = "chk多重残疾_肢体残疾";
            this.chk多重残疾_肢体残疾.Properties.Caption = "肢体残疾";
            this.chk多重残疾_肢体残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_肢体残疾.TabIndex = 4;
            this.chk多重残疾_肢体残疾.Tag = "5";
            // 
            // chk多重残疾_智力残疾
            // 
            this.chk多重残疾_智力残疾.Location = new System.Drawing.Point(375, 3);
            this.chk多重残疾_智力残疾.Name = "chk多重残疾_智力残疾";
            this.chk多重残疾_智力残疾.Properties.Caption = "智力残疾";
            this.chk多重残疾_智力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_智力残疾.TabIndex = 5;
            this.chk多重残疾_智力残疾.Tag = "6";
            // 
            // chk多重残疾_精神残疾
            // 
            this.chk多重残疾_精神残疾.Location = new System.Drawing.Point(456, 3);
            this.chk多重残疾_精神残疾.Name = "chk多重残疾_精神残疾";
            this.chk多重残疾_精神残疾.Properties.Caption = "精神残疾";
            this.chk多重残疾_精神残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_精神残疾.TabIndex = 6;
            this.chk多重残疾_精神残疾.Tag = "7";
            // 
            // radio随访分类
            // 
            this.radio随访分类.Location = new System.Drawing.Point(137, 758);
            this.radio随访分类.Name = "radio随访分类";
            this.radio随访分类.Size = new System.Drawing.Size(226, 20);
            this.radio随访分类.StyleController = this.layoutControl1;
            this.radio随访分类.TabIndex = 113;
            // 
            // radio遵医行为
            // 
            this.radio遵医行为.Location = new System.Drawing.Point(489, 670);
            this.radio遵医行为.Name = "radio遵医行为";
            this.radio遵医行为.Size = new System.Drawing.Size(227, 20);
            this.radio遵医行为.StyleController = this.layoutControl1;
            this.radio遵医行为.TabIndex = 112;
            // 
            // radio训练效果
            // 
            this.radio训练效果.Location = new System.Drawing.Point(137, 670);
            this.radio训练效果.Name = "radio训练效果";
            this.radio训练效果.Size = new System.Drawing.Size(226, 20);
            this.radio训练效果.StyleController = this.layoutControl1;
            this.radio训练效果.TabIndex = 111;
            // 
            // radio训练场地
            // 
            this.radio训练场地.Location = new System.Drawing.Point(137, 590);
            this.radio训练场地.Name = "radio训练场地";
            this.radio训练场地.Size = new System.Drawing.Size(225, 20);
            this.radio训练场地.StyleController = this.layoutControl1;
            this.radio训练场地.TabIndex = 110;
            // 
            // radio随访方式
            // 
            this.radio随访方式.Location = new System.Drawing.Point(460, 198);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Size = new System.Drawing.Size(268, 20);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 109;
            // 
            // radio残疾程度
            // 
            this.radio残疾程度.Location = new System.Drawing.Point(125, 174);
            this.radio残疾程度.Name = "radio残疾程度";
            this.radio残疾程度.Size = new System.Drawing.Size(603, 20);
            this.radio残疾程度.StyleController = this.layoutControl1;
            this.radio残疾程度.TabIndex = 108;
            // 
            // radio主要残疾
            // 
            this.radio主要残疾.Location = new System.Drawing.Point(125, 126);
            this.radio主要残疾.Name = "radio主要残疾";
            this.radio主要残疾.Size = new System.Drawing.Size(603, 20);
            this.radio主要残疾.StyleController = this.layoutControl1;
            this.radio主要残疾.TabIndex = 106;
            // 
            // textEdit体检其他
            // 
            this.textEdit体检其他.Location = new System.Drawing.Point(458, 289);
            this.textEdit体检其他.Name = "textEdit体检其他";
            this.textEdit体检其他.Size = new System.Drawing.Size(258, 20);
            this.textEdit体检其他.StyleController = this.layoutControl1;
            this.textEdit体检其他.TabIndex = 105;
            // 
            // textEdit体重
            // 
            this.textEdit体重.Lbl1Size = new System.Drawing.Size(30, 18);
            this.textEdit体重.Lbl1Text = "kg";
            this.textEdit体重.Location = new System.Drawing.Point(459, 265);
            this.textEdit体重.Name = "textEdit体重";
            this.textEdit体重.Size = new System.Drawing.Size(257, 20);
            this.textEdit体重.TabIndex = 104;
            this.textEdit体重.Txt1Size = new System.Drawing.Size(200, 20);
            // 
            // flow康复服务情况
            // 
            this.flow康复服务情况.Controls.Add(this.chk康复服务_医疗康复);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_功能训练);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_辅助器具);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_心理服务);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_知识普及);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_转介服务);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_其他);
            this.flow康复服务情况.Controls.Add(this.textEdit康复服务情况其他);
            this.flow康复服务情况.Location = new System.Drawing.Point(15, 364);
            this.flow康复服务情况.Name = "flow康复服务情况";
            this.flow康复服务情况.Size = new System.Drawing.Size(701, 52);
            this.flow康复服务情况.TabIndex = 103;
            // 
            // chk康复服务_医疗康复
            // 
            this.chk康复服务_医疗康复.Location = new System.Drawing.Point(3, 3);
            this.chk康复服务_医疗康复.Name = "chk康复服务_医疗康复";
            this.chk康复服务_医疗康复.Properties.Caption = "医疗康复";
            this.chk康复服务_医疗康复.Size = new System.Drawing.Size(105, 19);
            this.chk康复服务_医疗康复.TabIndex = 0;
            this.chk康复服务_医疗康复.Tag = "1";
            // 
            // chk康复服务_功能训练
            // 
            this.chk康复服务_功能训练.Location = new System.Drawing.Point(114, 3);
            this.chk康复服务_功能训练.Name = "chk康复服务_功能训练";
            this.chk康复服务_功能训练.Properties.Caption = "功能训练";
            this.chk康复服务_功能训练.Size = new System.Drawing.Size(106, 19);
            this.chk康复服务_功能训练.TabIndex = 1;
            this.chk康复服务_功能训练.Tag = "2";
            // 
            // chk康复服务_辅助器具
            // 
            this.chk康复服务_辅助器具.Location = new System.Drawing.Point(226, 3);
            this.chk康复服务_辅助器具.Name = "chk康复服务_辅助器具";
            this.chk康复服务_辅助器具.Properties.Caption = "辅助器具";
            this.chk康复服务_辅助器具.Size = new System.Drawing.Size(108, 19);
            this.chk康复服务_辅助器具.TabIndex = 2;
            this.chk康复服务_辅助器具.Tag = "3";
            // 
            // chk康复服务_心理服务
            // 
            this.chk康复服务_心理服务.Location = new System.Drawing.Point(340, 3);
            this.chk康复服务_心理服务.Name = "chk康复服务_心理服务";
            this.chk康复服务_心理服务.Properties.Caption = "心理服务";
            this.chk康复服务_心理服务.Size = new System.Drawing.Size(100, 19);
            this.chk康复服务_心理服务.TabIndex = 3;
            this.chk康复服务_心理服务.Tag = "4";
            // 
            // chk康复服务_知识普及
            // 
            this.chk康复服务_知识普及.Location = new System.Drawing.Point(446, 3);
            this.chk康复服务_知识普及.Name = "chk康复服务_知识普及";
            this.chk康复服务_知识普及.Properties.Caption = "知识普及";
            this.chk康复服务_知识普及.Size = new System.Drawing.Size(101, 19);
            this.chk康复服务_知识普及.TabIndex = 4;
            this.chk康复服务_知识普及.Tag = "5";
            // 
            // chk康复服务_转介服务
            // 
            this.chk康复服务_转介服务.Location = new System.Drawing.Point(553, 3);
            this.chk康复服务_转介服务.Name = "chk康复服务_转介服务";
            this.chk康复服务_转介服务.Properties.Caption = "转介服务";
            this.chk康复服务_转介服务.Size = new System.Drawing.Size(102, 19);
            this.chk康复服务_转介服务.TabIndex = 5;
            this.chk康复服务_转介服务.Tag = "6";
            // 
            // chk康复服务_其他
            // 
            this.chk康复服务_其他.Location = new System.Drawing.Point(3, 28);
            this.chk康复服务_其他.Name = "chk康复服务_其他";
            this.chk康复服务_其他.Properties.Caption = "其他";
            this.chk康复服务_其他.Size = new System.Drawing.Size(62, 19);
            this.chk康复服务_其他.TabIndex = 6;
            this.chk康复服务_其他.Tag = "99";
            // 
            // textEdit康复服务情况其他
            // 
            this.textEdit康复服务情况其他.Location = new System.Drawing.Point(71, 28);
            this.textEdit康复服务情况其他.Name = "textEdit康复服务情况其他";
            this.textEdit康复服务情况其他.Size = new System.Drawing.Size(97, 20);
            this.textEdit康复服务情况其他.TabIndex = 7;
            // 
            // textEdit功能训练2
            // 
            this.textEdit功能训练2.Lbl1Size = new System.Drawing.Size(30, 14);
            this.textEdit功能训练2.Lbl1Text = "次/月";
            this.textEdit功能训练2.Lbl2Size = new System.Drawing.Size(40, 14);
            this.textEdit功能训练2.Lbl2Text = "分钟/次";
            this.textEdit功能训练2.Location = new System.Drawing.Point(367, 566);
            this.textEdit功能训练2.Name = "textEdit功能训练2";
            this.textEdit功能训练2.Size = new System.Drawing.Size(349, 20);
            this.textEdit功能训练2.TabIndex = 100;
            this.textEdit功能训练2.Txt1EditValue = null;
            this.textEdit功能训练2.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit功能训练2.Txt2EditValue = null;
            this.textEdit功能训练2.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // labelControl最近修改人
            // 
            this.labelControl最近修改人.Location = new System.Drawing.Point(448, 866);
            this.labelControl最近修改人.Name = "labelControl最近修改人";
            this.labelControl最近修改人.Size = new System.Drawing.Size(280, 20);
            this.labelControl最近修改人.StyleController = this.layoutControl1;
            this.labelControl最近修改人.TabIndex = 99;
            // 
            // labelControl创建人
            // 
            this.labelControl创建人.Location = new System.Drawing.Point(98, 866);
            this.labelControl创建人.Name = "labelControl创建人";
            this.labelControl创建人.Size = new System.Drawing.Size(251, 20);
            this.labelControl创建人.StyleController = this.layoutControl1;
            this.labelControl创建人.TabIndex = 98;
            // 
            // labelControl创建机构
            // 
            this.labelControl创建机构.Location = new System.Drawing.Point(448, 842);
            this.labelControl创建机构.Name = "labelControl创建机构";
            this.labelControl创建机构.Size = new System.Drawing.Size(280, 20);
            this.labelControl创建机构.StyleController = this.layoutControl1;
            this.labelControl创建机构.TabIndex = 97;
            // 
            // labelControl当前所属机构
            // 
            this.labelControl当前所属机构.Location = new System.Drawing.Point(98, 842);
            this.labelControl当前所属机构.Name = "labelControl当前所属机构";
            this.labelControl当前所属机构.Size = new System.Drawing.Size(251, 20);
            this.labelControl当前所属机构.StyleController = this.layoutControl1;
            this.labelControl当前所属机构.TabIndex = 96;
            // 
            // labelControl最近更新时间
            // 
            this.labelControl最近更新时间.Location = new System.Drawing.Point(448, 818);
            this.labelControl最近更新时间.Name = "labelControl最近更新时间";
            this.labelControl最近更新时间.Size = new System.Drawing.Size(280, 20);
            this.labelControl最近更新时间.StyleController = this.layoutControl1;
            this.labelControl最近更新时间.TabIndex = 95;
            // 
            // labelControl创建时间
            // 
            this.labelControl创建时间.Location = new System.Drawing.Point(98, 818);
            this.labelControl创建时间.Name = "labelControl创建时间";
            this.labelControl创建时间.Size = new System.Drawing.Size(251, 20);
            this.labelControl创建时间.StyleController = this.layoutControl1;
            this.labelControl创建时间.TabIndex = 94;
            // 
            // labelControl婚姻状况
            // 
            this.labelControl婚姻状况.Location = new System.Drawing.Point(437, 102);
            this.labelControl婚姻状况.Name = "labelControl婚姻状况";
            this.labelControl婚姻状况.Size = new System.Drawing.Size(291, 20);
            this.labelControl婚姻状况.StyleController = this.layoutControl1;
            this.labelControl婚姻状况.TabIndex = 93;
            // 
            // labelControl居住地址
            // 
            this.labelControl居住地址.Location = new System.Drawing.Point(98, 102);
            this.labelControl居住地址.Name = "labelControl居住地址";
            this.labelControl居住地址.Size = new System.Drawing.Size(240, 20);
            this.labelControl居住地址.StyleController = this.layoutControl1;
            this.labelControl居住地址.TabIndex = 92;
            // 
            // labelControl联系电话
            // 
            this.labelControl联系电话.Location = new System.Drawing.Point(437, 78);
            this.labelControl联系电话.Name = "labelControl联系电话";
            this.labelControl联系电话.Size = new System.Drawing.Size(291, 20);
            this.labelControl联系电话.StyleController = this.layoutControl1;
            this.labelControl联系电话.TabIndex = 91;
            // 
            // labelControl出生日期
            // 
            this.labelControl出生日期.Location = new System.Drawing.Point(98, 78);
            this.labelControl出生日期.Name = "labelControl出生日期";
            this.labelControl出生日期.Size = new System.Drawing.Size(240, 20);
            this.labelControl出生日期.StyleController = this.layoutControl1;
            this.labelControl出生日期.TabIndex = 90;
            // 
            // labelControl身份证号
            // 
            this.labelControl身份证号.Location = new System.Drawing.Point(437, 54);
            this.labelControl身份证号.Name = "labelControl身份证号";
            this.labelControl身份证号.Size = new System.Drawing.Size(291, 20);
            this.labelControl身份证号.StyleController = this.layoutControl1;
            this.labelControl身份证号.TabIndex = 89;
            // 
            // labelControl性别
            // 
            this.labelControl性别.Location = new System.Drawing.Point(98, 54);
            this.labelControl性别.Name = "labelControl性别";
            this.labelControl性别.Size = new System.Drawing.Size(240, 20);
            this.labelControl性别.StyleController = this.layoutControl1;
            this.labelControl性别.TabIndex = 88;
            // 
            // labelControl姓名
            // 
            this.labelControl姓名.Location = new System.Drawing.Point(437, 30);
            this.labelControl姓名.Name = "labelControl姓名";
            this.labelControl姓名.Size = new System.Drawing.Size(291, 20);
            this.labelControl姓名.StyleController = this.layoutControl1;
            this.labelControl姓名.TabIndex = 87;
            // 
            // labelControl个人档案号
            // 
            this.labelControl个人档案号.Location = new System.Drawing.Point(98, 30);
            this.labelControl个人档案号.Name = "labelControl个人档案号";
            this.labelControl个人档案号.Size = new System.Drawing.Size(240, 20);
            this.labelControl个人档案号.StyleController = this.layoutControl1;
            this.labelControl个人档案号.TabIndex = 86;
            // 
            // textEdit转介去向
            // 
            this.textEdit转介去向.Location = new System.Drawing.Point(137, 493);
            this.textEdit转介去向.Name = "textEdit转介去向";
            this.textEdit转介去向.Size = new System.Drawing.Size(579, 20);
            this.textEdit转介去向.StyleController = this.layoutControl1;
            this.textEdit转介去向.TabIndex = 1;
            // 
            // textEdit转介原因
            // 
            this.textEdit转介原因.Location = new System.Drawing.Point(137, 469);
            this.textEdit转介原因.Name = "textEdit转介原因";
            this.textEdit转介原因.Size = new System.Drawing.Size(579, 20);
            this.textEdit转介原因.StyleController = this.layoutControl1;
            this.textEdit转介原因.TabIndex = 0;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(125, 198);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Size = new System.Drawing.Size(209, 20);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 0;
            // 
            // textEdit血压
            // 
            this.textEdit血压.Lbl1Size = new System.Drawing.Size(18, 14);
            this.textEdit血压.Lbl1Text = "/";
            this.textEdit血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.textEdit血压.Lbl2Text = "mmHg";
            this.textEdit血压.Location = new System.Drawing.Point(137, 265);
            this.textEdit血压.Name = "textEdit血压";
            this.textEdit血压.Size = new System.Drawing.Size(196, 20);
            this.textEdit血压.TabIndex = 0;
            this.textEdit血压.Txt1EditValue = null;
            this.textEdit血压.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit血压.Txt2EditValue = null;
            this.textEdit血压.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // textEdit功能训练1
            // 
            this.textEdit功能训练1.Lbl1Size = new System.Drawing.Size(40, 14);
            this.textEdit功能训练1.Lbl1Text = "次/月";
            this.textEdit功能训练1.Lbl2Size = new System.Drawing.Size(50, 14);
            this.textEdit功能训练1.Lbl2Text = "分钟/次";
            this.textEdit功能训练1.Location = new System.Drawing.Point(137, 566);
            this.textEdit功能训练1.Name = "textEdit功能训练1";
            this.textEdit功能训练1.Size = new System.Drawing.Size(226, 20);
            this.textEdit功能训练1.TabIndex = 0;
            this.textEdit功能训练1.Txt1EditValue = null;
            this.textEdit功能训练1.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit功能训练1.Txt2EditValue = null;
            this.textEdit功能训练1.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(489, 782);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(227, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 74;
            // 
            // textEdit服务对象或家属
            // 
            this.textEdit服务对象或家属.Location = new System.Drawing.Point(137, 782);
            this.textEdit服务对象或家属.Name = "textEdit服务对象或家属";
            this.textEdit服务对象或家属.Size = new System.Drawing.Size(226, 20);
            this.textEdit服务对象或家属.StyleController = this.layoutControl1;
            this.textEdit服务对象或家属.TabIndex = 73;
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(489, 758);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(227, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 72;
            // 
            // textEdit训练评估分数
            // 
            this.textEdit训练评估分数.Lbl1Size = new System.Drawing.Size(75, 18);
            this.textEdit训练评估分数.Lbl1Text = "分";
            this.textEdit训练评估分数.Location = new System.Drawing.Point(488, 590);
            this.textEdit训练评估分数.Name = "textEdit训练评估分数";
            this.textEdit训练评估分数.Size = new System.Drawing.Size(228, 20);
            this.textEdit训练评估分数.TabIndex = 0;
            this.textEdit训练评估分数.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flow康复目标
            // 
            this.flow康复目标.Controls.Add(this.chk康复目标_运动);
            this.flow康复目标.Controls.Add(this.chk康复目标_感知);
            this.flow康复目标.Controls.Add(this.chk康复目标_认知);
            this.flow康复目标.Controls.Add(this.chk康复目标_交往);
            this.flow康复目标.Controls.Add(this.chk康复目标_自理);
            this.flow康复目标.Controls.Add(this.chk康复目标_适应);
            this.flow康复目标.Controls.Add(this.chk康复目标_其他);
            this.flow康复目标.Controls.Add(this.textEdit康复目标其他);
            this.flow康复目标.Location = new System.Drawing.Point(137, 614);
            this.flow康复目标.Name = "flow康复目标";
            this.flow康复目标.Size = new System.Drawing.Size(579, 52);
            this.flow康复目标.TabIndex = 67;
            // 
            // chk康复目标_运动
            // 
            this.chk康复目标_运动.Location = new System.Drawing.Point(3, 3);
            this.chk康复目标_运动.Name = "chk康复目标_运动";
            this.chk康复目标_运动.Properties.Caption = "运动能力改善";
            this.chk康复目标_运动.Size = new System.Drawing.Size(95, 19);
            this.chk康复目标_运动.TabIndex = 0;
            this.chk康复目标_运动.Tag = "1";
            // 
            // chk康复目标_感知
            // 
            this.chk康复目标_感知.Location = new System.Drawing.Point(104, 3);
            this.chk康复目标_感知.Name = "chk康复目标_感知";
            this.chk康复目标_感知.Properties.Caption = "感知能力提高";
            this.chk康复目标_感知.Size = new System.Drawing.Size(97, 19);
            this.chk康复目标_感知.TabIndex = 1;
            this.chk康复目标_感知.Tag = "2";
            // 
            // chk康复目标_认知
            // 
            this.chk康复目标_认知.Location = new System.Drawing.Point(207, 3);
            this.chk康复目标_认知.Name = "chk康复目标_认知";
            this.chk康复目标_认知.Properties.Caption = "认知能力提高";
            this.chk康复目标_认知.Size = new System.Drawing.Size(111, 19);
            this.chk康复目标_认知.TabIndex = 2;
            this.chk康复目标_认知.Tag = "3";
            // 
            // chk康复目标_交往
            // 
            this.chk康复目标_交往.Location = new System.Drawing.Point(324, 3);
            this.chk康复目标_交往.Name = "chk康复目标_交往";
            this.chk康复目标_交往.Properties.Caption = "交往能力提高";
            this.chk康复目标_交往.Size = new System.Drawing.Size(103, 19);
            this.chk康复目标_交往.TabIndex = 3;
            this.chk康复目标_交往.Tag = "4";
            // 
            // chk康复目标_自理
            // 
            this.chk康复目标_自理.Location = new System.Drawing.Point(433, 3);
            this.chk康复目标_自理.Name = "chk康复目标_自理";
            this.chk康复目标_自理.Properties.Caption = "自理能力提高";
            this.chk康复目标_自理.Size = new System.Drawing.Size(101, 19);
            this.chk康复目标_自理.TabIndex = 4;
            this.chk康复目标_自理.Tag = "5";
            // 
            // chk康复目标_适应
            // 
            this.chk康复目标_适应.Location = new System.Drawing.Point(3, 28);
            this.chk康复目标_适应.Name = "chk康复目标_适应";
            this.chk康复目标_适应.Properties.Caption = "适应能力提高";
            this.chk康复目标_适应.Size = new System.Drawing.Size(95, 19);
            this.chk康复目标_适应.TabIndex = 5;
            this.chk康复目标_适应.Tag = "6";
            // 
            // chk康复目标_其他
            // 
            this.chk康复目标_其他.Location = new System.Drawing.Point(104, 28);
            this.chk康复目标_其他.Name = "chk康复目标_其他";
            this.chk康复目标_其他.Properties.Caption = "其他";
            this.chk康复目标_其他.Size = new System.Drawing.Size(58, 19);
            this.chk康复目标_其他.TabIndex = 7;
            this.chk康复目标_其他.Tag = "99";
            // 
            // textEdit康复目标其他
            // 
            this.textEdit康复目标其他.Location = new System.Drawing.Point(168, 28);
            this.textEdit康复目标其他.Name = "textEdit康复目标其他";
            this.textEdit康复目标其他.Size = new System.Drawing.Size(97, 20);
            this.textEdit康复目标其他.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "残疾人康复服务随访记录表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 890);
            this.layoutControlGroup1.Text = "残疾人康复服务随访记录表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 96);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.labelControl个人档案号;
            this.layoutControlItem11.CustomizationFormText = "个人档案号";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "个人档案号";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.labelControl姓名;
            this.layoutControlItem16.CustomizationFormText = "姓名";
            this.layoutControlItem16.Location = new System.Drawing.Point(339, 0);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(390, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "姓名";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.labelControl性别;
            this.layoutControlItem17.CustomizationFormText = "性别";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "性别";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.labelControl身份证号;
            this.layoutControlItem18.CustomizationFormText = "身份证号";
            this.layoutControlItem18.Location = new System.Drawing.Point(339, 24);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(390, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "身份证号";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.labelControl出生日期;
            this.layoutControlItem19.CustomizationFormText = "出生日期";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "出生日期";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.labelControl联系电话;
            this.layoutControlItem20.CustomizationFormText = "联系电话";
            this.layoutControlItem20.Location = new System.Drawing.Point(339, 48);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(390, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "联系电话";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.labelControl居住地址;
            this.layoutControlItem21.CustomizationFormText = "居住地址";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "居住地址";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.labelControl婚姻状况;
            this.layoutControlItem22.CustomizationFormText = "婚姻状况";
            this.layoutControlItem22.Location = new System.Drawing.Point(339, 72);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(390, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "婚姻状况";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator8,
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.layoutControlGroup15,
            this.layoutControlItem8,
            this.layoutControlItem14,
            this.layoutControlItem24,
            this.layoutControlItem26,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem27,
            this.layoutControlItem12,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem32});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 96);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 764);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // simpleSeparator8
            // 
            this.simpleSeparator8.AllowHotTrack = false;
            this.simpleSeparator8.CustomizationFormText = "simpleSeparator8";
            this.simpleSeparator8.Location = new System.Drawing.Point(0, 199);
            this.simpleSeparator8.Name = "simpleSeparator8";
            this.simpleSeparator8.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator8.Text = "simpleSeparator8";
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup12.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup12.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup12.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup12.CustomizationFormText = "体检";
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem10,
            this.layoutControlItem1,
            this.layoutControlItem6});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 102);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(729, 97);
            this.layoutControlGroup12.Text = "体检";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit血压;
            this.layoutControlItem13.CustomizationFormText = "血压(mmHg)";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(322, 24);
            this.layoutControlItem13.Text = "血压";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit体重;
            this.layoutControlItem10.CustomizationFormText = "体重";
            this.layoutControlItem10.Location = new System.Drawing.Point(322, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(383, 24);
            this.layoutControlItem10.Text = "体重";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit体检其他;
            this.layoutControlItem1.CustomizationFormText = "其他";
            this.layoutControlItem1.Location = new System.Drawing.Point(321, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(384, 24);
            this.layoutControlItem1.Text = "其他";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit心率;
            this.layoutControlItem6.CustomizationFormText = "心率";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem6.Text = "心率";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup13.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup13.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup13.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup13.CustomizationFormText = "康复服务情况";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 201);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(729, 105);
            this.layoutControlGroup13.Text = "康复服务情况";
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.flow康复服务情况;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(226, 56);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(705, 56);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup14.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup14.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup14.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup14.CustomizationFormText = "转介服务";
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem9});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 306);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(729, 97);
            this.layoutControlGroup14.Text = "转介服务";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit转介原因;
            this.layoutControlItem4.CustomizationFormText = "原因";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(705, 24);
            this.layoutControlItem4.Text = "原因";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit转介去向;
            this.layoutControlItem9.CustomizationFormText = "转介去向";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(705, 24);
            this.layoutControlItem9.Text = "转介去向";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup15.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup15.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup15.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup15.CustomizationFormText = "功能训练情况(肢体残疾及智障儿童功能训练者填)";
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem44,
            this.layoutControlItem54,
            this.layoutControlItem60,
            this.layoutControlItem61,
            this.layoutControlItem59,
            this.layoutControlItem62,
            this.layoutControlItem28,
            this.layoutControlItem15,
            this.layoutControlItem5,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem7});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 403);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(729, 289);
            this.layoutControlGroup15.Text = "功能训练情况(肢体残疾及智障儿童功能训练者填)";
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.flow康复目标;
            this.layoutControlItem44.CustomizationFormText = "康复目标";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(214, 56);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(705, 56);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "康复目标";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.textEdit训练评估分数;
            this.layoutControlItem54.CustomizationFormText = "训练评估分数";
            this.layoutControlItem54.Location = new System.Drawing.Point(351, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem54.Text = "训练评估分数";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem60.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.textEdit服务对象或家属;
            this.layoutControlItem60.CustomizationFormText = "服务对象或家属签名";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem60.Text = "服务对象或家属签名";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem61.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.textEdit随访医生;
            this.layoutControlItem61.CustomizationFormText = "随访医生签名";
            this.layoutControlItem61.Location = new System.Drawing.Point(352, 216);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem61.Text = "随访医生签名";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem59.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.dateEdit下次随访日期;
            this.layoutControlItem59.CustomizationFormText = "下次随访日期";
            this.layoutControlItem59.Location = new System.Drawing.Point(352, 192);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem59.Text = "下次随访日期";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.textEdit功能训练1;
            this.layoutControlItem62.CustomizationFormText = "功能训练";
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem62.Text = "功能训练";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit功能训练2;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(352, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.radio训练场地;
            this.layoutControlItem15.CustomizationFormText = "训练场地";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(351, 24);
            this.layoutControlItem15.Text = "训练场地";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.radio训练效果;
            this.layoutControlItem5.CustomizationFormText = "训练效果";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem5.Text = "训练效果";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.radio遵医行为;
            this.layoutControlItem30.CustomizationFormText = "遵医行为";
            this.layoutControlItem30.Location = new System.Drawing.Point(352, 104);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(353, 24);
            this.layoutControlItem30.Text = "遵医行为";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.radio随访分类;
            this.layoutControlItem31.CustomizationFormText = "此次随访分类";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem31.Text = "此次随访分类";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.gridControl1;
            this.layoutControlItem7.CustomizationFormText = "用药";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(705, 64);
            this.layoutControlItem7.Text = "用药";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(120, 60);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.dateEdit随访日期;
            this.layoutControlItem8.CustomizationFormText = "随访日期";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(176, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(335, 30);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "随访日期";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.labelControl创建时间;
            this.layoutControlItem14.CustomizationFormText = "创建时间";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 692);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "创建时间:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.labelControl当前所属机构;
            this.layoutControlItem24.CustomizationFormText = "所属机构";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 716);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "当前所属机构:";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.labelControl创建人;
            this.layoutControlItem26.CustomizationFormText = "创建人";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 740);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "创建人:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.labelControl最近更新时间;
            this.layoutControlItem23.CustomizationFormText = "更新时间";
            this.layoutControlItem23.Location = new System.Drawing.Point(350, 692);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "最近更新时间:";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.labelControl创建机构;
            this.layoutControlItem25.CustomizationFormText = "创建机构";
            this.layoutControlItem25.Location = new System.Drawing.Point(350, 716);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建机构:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.labelControl最近修改人;
            this.layoutControlItem27.CustomizationFormText = "修改人";
            this.layoutControlItem27.Location = new System.Drawing.Point(350, 740);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "最近修改人:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.radio主要残疾;
            this.layoutControlItem12.CustomizationFormText = "主要残疾";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem12.Text = "主要残疾";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.radio残疾程度;
            this.layoutControlItem2.CustomizationFormText = "残疾程度";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem2.Text = "残疾程度";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.radio随访方式;
            this.layoutControlItem3.CustomizationFormText = "随访方式";
            this.layoutControlItem3.Location = new System.Drawing.Point(335, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(394, 30);
            this.layoutControlItem3.Text = "随访方式";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.flow多重残疾;
            this.layoutControlItem32.CustomizationFormText = "多重残疾";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem32.Text = "多重残疾";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 860);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(729, 1);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // UC残疾人康复服务随访记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC残疾人康复服务随访记录表_显示";
            this.Size = new System.Drawing.Size(855, 573);
            this.Load += new System.EventHandler(this.UC残疾人康复服务随访记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).EndInit();
            this.flow多重残疾.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_否.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_视力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_听力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_言语残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_肢体残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_智力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_精神残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio遵医行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练效果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练场地.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检其他.Properties)).EndInit();
            this.flow康复服务情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_医疗康复.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_功能训练.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_辅助器具.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_心理服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_知识普及.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_转介服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复服务情况其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介去向.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit服务对象或家属.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            this.flow康复目标.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_运动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_感知.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_认知.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_交往.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_自理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_适应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复目标其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn添加随访;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator8;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private Library.UserControls.UCTxtLblTxtLbl textEdit血压;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraEditors.TextEdit textEdit转介去向;
        private DevExpress.XtraEditors.TextEdit textEdit转介原因;
        private System.Windows.Forms.FlowLayoutPanel flow康复目标;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private Library.UserControls.UCTxtLblTxtLbl textEdit功能训练1;
        private Library.UserControls.UCTxtLbl textEdit训练评估分数;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_运动;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_感知;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_认知;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_交往;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_自理;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_适应;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit服务对象或家属;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LabelControl labelControl个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl labelControl婚姻状况;
        private DevExpress.XtraEditors.LabelControl labelControl居住地址;
        private DevExpress.XtraEditors.LabelControl labelControl联系电话;
        private DevExpress.XtraEditors.LabelControl labelControl出生日期;
        private DevExpress.XtraEditors.LabelControl labelControl身份证号;
        private DevExpress.XtraEditors.LabelControl labelControl性别;
        private DevExpress.XtraEditors.LabelControl labelControl姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.LabelControl labelControl最近修改人;
        private DevExpress.XtraEditors.LabelControl labelControl创建人;
        private DevExpress.XtraEditors.LabelControl labelControl创建机构;
        private DevExpress.XtraEditors.LabelControl labelControl当前所属机构;
        private DevExpress.XtraEditors.LabelControl labelControl最近更新时间;
        private DevExpress.XtraEditors.LabelControl labelControl创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private Library.UserControls.UCTxtLblTxtLbl textEdit功能训练2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_医疗康复;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_功能训练;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_辅助器具;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_心理服务;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_知识普及;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_转介服务;
        private System.Windows.Forms.FlowLayoutPanel flow康复服务情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_其他;
        private DevExpress.XtraEditors.TextEdit textEdit康复服务情况其他;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_其他;
        private DevExpress.XtraEditors.TextEdit textEdit康复目标其他;
        private Library.UserControls.UCTxtLbl textEdit体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.TextEdit textEdit体检其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit radio主要残疾;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit radio残疾程度;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit radio随访方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit radio训练场地;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit radio训练效果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit radio随访分类;
        private DevExpress.XtraEditors.TextEdit radio遵医行为;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private System.Windows.Forms.FlowLayoutPanel flow多重残疾;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_否;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_视力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_听力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_言语残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_肢体残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_智力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_精神残疾;
        private Library.UserControls.UCTxtLblTxtLbl textEdit心率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gv用药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}
