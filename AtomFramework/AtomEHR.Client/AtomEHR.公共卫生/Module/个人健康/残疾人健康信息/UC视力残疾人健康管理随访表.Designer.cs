﻿namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    partial class UC视力残疾人健康管理随访表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit37 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit32 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit33 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.radioGroup11 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup10 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucLblTxt3 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt4 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup5 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioGroup4 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxt1 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxt2 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.radioGroup3 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup2 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup5.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup3.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            // 
            // btn重置
            // 
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.radioGroup11);
            this.layoutControl1.Controls.Add(this.radioGroup10);
            this.layoutControl1.Controls.Add(this.labelControl15);
            this.layoutControl1.Controls.Add(this.labelControl14);
            this.layoutControl1.Controls.Add(this.labelControl13);
            this.layoutControl1.Controls.Add(this.labelControl12);
            this.layoutControl1.Controls.Add(this.labelControl11);
            this.layoutControl1.Controls.Add(this.labelControl10);
            this.layoutControl1.Controls.Add(this.labelControl9);
            this.layoutControl1.Controls.Add(this.dateEdit2);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.memoEdit2);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.radioGroup5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.radioGroup3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.radioGroup2);
            this.layoutControl1.Controls.Add(this.radioGroup1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.labelControl8);
            this.layoutControl1.Controls.Add(this.labelControl7);
            this.layoutControl1.Controls.Add(this.labelControl6);
            this.layoutControl1.Controls.Add(this.labelControl5);
            this.layoutControl1.Controls.Add(this.labelControl4);
            this.layoutControl1.Controls.Add(this.labelControl3);
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.checkEdit34);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit35);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit36);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit37);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit39);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit40);
            this.flowLayoutPanel10.Controls.Add(this.checkEdit41);
            this.flowLayoutPanel10.Controls.Add(this.textEdit13);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(168, 588);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(560, 42);
            this.flowLayoutPanel10.TabIndex = 33;
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(0, 0);
            this.checkEdit34.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "自已看书、看报";
            this.checkEdit34.Size = new System.Drawing.Size(111, 19);
            this.checkEdit34.TabIndex = 4;
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(111, 0);
            this.checkEdit35.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "请人读报、读书";
            this.checkEdit35.Size = new System.Drawing.Size(113, 19);
            this.checkEdit35.TabIndex = 5;
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(224, 0);
            this.checkEdit36.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Caption = "交谈";
            this.checkEdit36.Size = new System.Drawing.Size(58, 19);
            this.checkEdit36.TabIndex = 6;
            // 
            // checkEdit37
            // 
            this.checkEdit37.Location = new System.Drawing.Point(282, 0);
            this.checkEdit37.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit37.Name = "checkEdit37";
            this.checkEdit37.Properties.Caption = "听看电视与收音机";
            this.checkEdit37.Size = new System.Drawing.Size(123, 19);
            this.checkEdit37.TabIndex = 7;
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(405, 0);
            this.checkEdit39.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Caption = "盲文";
            this.checkEdit39.Size = new System.Drawing.Size(51, 19);
            this.checkEdit39.TabIndex = 8;
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(456, 0);
            this.checkEdit40.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Caption = "计算机";
            this.checkEdit40.Size = new System.Drawing.Size(75, 19);
            this.checkEdit40.TabIndex = 9;
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(0, 19);
            this.checkEdit41.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Caption = "其他 ";
            this.checkEdit41.Size = new System.Drawing.Size(52, 19);
            this.checkEdit41.TabIndex = 10;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(52, 19);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(265, 20);
            this.textEdit13.TabIndex = 11;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.checkEdit30);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit31);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit32);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit33);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit24);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit25);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit26);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit38);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(168, 542);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(560, 42);
            this.flowLayoutPanel9.TabIndex = 32;
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(0, 0);
            this.checkEdit30.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "能独自外出";
            this.checkEdit30.Size = new System.Drawing.Size(97, 19);
            this.checkEdit30.TabIndex = 4;
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(97, 0);
            this.checkEdit31.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Caption = "能上下楼梯";
            this.checkEdit31.Size = new System.Drawing.Size(98, 19);
            this.checkEdit31.TabIndex = 5;
            // 
            // checkEdit32
            // 
            this.checkEdit32.Location = new System.Drawing.Point(195, 0);
            this.checkEdit32.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit32.Name = "checkEdit32";
            this.checkEdit32.Properties.Caption = "能穿越马路";
            this.checkEdit32.Size = new System.Drawing.Size(87, 19);
            this.checkEdit32.TabIndex = 6;
            // 
            // checkEdit33
            // 
            this.checkEdit33.Location = new System.Drawing.Point(282, 0);
            this.checkEdit33.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit33.Name = "checkEdit33";
            this.checkEdit33.Properties.Caption = "能做家务";
            this.checkEdit33.Size = new System.Drawing.Size(82, 19);
            this.checkEdit33.TabIndex = 7;
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(364, 0);
            this.checkEdit24.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "能整理桌面";
            this.checkEdit24.Size = new System.Drawing.Size(92, 19);
            this.checkEdit24.TabIndex = 8;
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(0, 19);
            this.checkEdit25.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Caption = "能独自做个人卫生";
            this.checkEdit25.Size = new System.Drawing.Size(127, 19);
            this.checkEdit25.TabIndex = 9;
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(127, 19);
            this.checkEdit26.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Caption = "能手工劳动";
            this.checkEdit26.Size = new System.Drawing.Size(97, 19);
            this.checkEdit26.TabIndex = 10;
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(224, 19);
            this.checkEdit38.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Caption = "无独立生活能力";
            this.checkEdit38.Size = new System.Drawing.Size(112, 19);
            this.checkEdit38.TabIndex = 11;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.checkEdit21);
            this.flowLayoutPanel8.Controls.Add(this.checkEdit22);
            this.flowLayoutPanel8.Controls.Add(this.checkEdit23);
            this.flowLayoutPanel8.Controls.Add(this.checkEdit28);
            this.flowLayoutPanel8.Controls.Add(this.checkEdit29);
            this.flowLayoutPanel8.Controls.Add(this.textEdit12);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(168, 494);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel8.TabIndex = 17;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(0, 0);
            this.checkEdit21.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "公办培训机构";
            this.checkEdit21.Size = new System.Drawing.Size(97, 19);
            this.checkEdit21.TabIndex = 0;
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(97, 0);
            this.checkEdit22.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "民办培训机构";
            this.checkEdit22.Size = new System.Drawing.Size(93, 19);
            this.checkEdit22.TabIndex = 1;
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(190, 0);
            this.checkEdit23.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "自学";
            this.checkEdit23.Size = new System.Drawing.Size(47, 19);
            this.checkEdit23.TabIndex = 2;
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(237, 0);
            this.checkEdit28.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "他人辅导";
            this.checkEdit28.Size = new System.Drawing.Size(74, 19);
            this.checkEdit28.TabIndex = 3;
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(311, 0);
            this.checkEdit29.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "其他";
            this.checkEdit29.Size = new System.Drawing.Size(52, 19);
            this.checkEdit29.TabIndex = 4;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(363, 0);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(193, 20);
            this.textEdit12.TabIndex = 8;
            // 
            // radioGroup11
            // 
            this.radioGroup11.Location = new System.Drawing.Point(447, 465);
            this.radioGroup11.Name = "radioGroup11";
            this.radioGroup11.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "强"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "差"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "无")});
            this.radioGroup11.Size = new System.Drawing.Size(281, 25);
            this.radioGroup11.StyleController = this.layoutControl1;
            this.radioGroup11.TabIndex = 45;
            // 
            // radioGroup10
            // 
            this.radioGroup10.Location = new System.Drawing.Point(168, 465);
            this.radioGroup10.Name = "radioGroup10";
            this.radioGroup10.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "弱"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "无")});
            this.radioGroup10.Size = new System.Drawing.Size(190, 25);
            this.radioGroup10.StyleController = this.layoutControl1;
            this.radioGroup10.TabIndex = 44;
            // 
            // labelControl15
            // 
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Location = new System.Drawing.Point(416, 830);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(312, 20);
            this.labelControl15.StyleController = this.layoutControl1;
            this.labelControl15.TabIndex = 43;
            this.labelControl15.Text = "labelControl15";
            // 
            // labelControl14
            // 
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(98, 830);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(219, 20);
            this.labelControl14.StyleController = this.layoutControl1;
            this.labelControl14.TabIndex = 42;
            this.labelControl14.Text = "labelControl14";
            // 
            // labelControl13
            // 
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Location = new System.Drawing.Point(416, 806);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(312, 20);
            this.labelControl13.StyleController = this.layoutControl1;
            this.labelControl13.TabIndex = 41;
            this.labelControl13.Text = "labelControl13";
            // 
            // labelControl12
            // 
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(98, 806);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(219, 20);
            this.labelControl12.StyleController = this.layoutControl1;
            this.labelControl12.TabIndex = 40;
            this.labelControl12.Text = "labelControl12";
            // 
            // labelControl11
            // 
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(98, 782);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(219, 20);
            this.labelControl11.StyleController = this.layoutControl1;
            this.labelControl11.TabIndex = 39;
            this.labelControl11.Text = "labelControl11";
            // 
            // labelControl10
            // 
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(416, 782);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(312, 20);
            this.labelControl10.StyleController = this.layoutControl1;
            this.labelControl10.TabIndex = 38;
            this.labelControl10.Text = "labelControl10";
            // 
            // labelControl9
            // 
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(452, 758);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(276, 20);
            this.labelControl9.StyleController = this.layoutControl1;
            this.labelControl9.TabIndex = 37;
            this.labelControl9.Text = "labelControl9";
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(92, 758);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Size = new System.Drawing.Size(271, 20);
            this.dateEdit2.StyleController = this.layoutControl1;
            this.dateEdit2.TabIndex = 36;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(456, 734);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(272, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 35;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(92, 734);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(271, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 34;
            // 
            // memoEdit2
            // 
            this.memoEdit2.Location = new System.Drawing.Point(88, 684);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Size = new System.Drawing.Size(640, 46);
            this.memoEdit2.StyleController = this.layoutControl1;
            this.memoEdit2.TabIndex = 33;
            this.memoEdit2.UseOptimizedRendering = true;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(88, 634);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(640, 46);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 32;
            this.memoEdit1.UseOptimizedRendering = true;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.ucLblTxt3);
            this.flowLayoutPanel7.Controls.Add(this.ucLblTxt4);
            this.flowLayoutPanel7.Controls.Add(this.checkEdit27);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(168, 518);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel7.TabIndex = 31;
            // 
            // ucLblTxt3
            // 
            this.ucLblTxt3.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt3.Lbl1Text = "视力：左";
            this.ucLblTxt3.Location = new System.Drawing.Point(0, 0);
            this.ucLblTxt3.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt3.Name = "ucLblTxt3";
            this.ucLblTxt3.Size = new System.Drawing.Size(181, 22);
            this.ucLblTxt3.TabIndex = 8;
            this.ucLblTxt3.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucLblTxt4
            // 
            this.ucLblTxt4.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucLblTxt4.Lbl1Text = "右";
            this.ucLblTxt4.Location = new System.Drawing.Point(181, 0);
            this.ucLblTxt4.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt4.Name = "ucLblTxt4";
            this.ucLblTxt4.Size = new System.Drawing.Size(139, 22);
            this.ucLblTxt4.TabIndex = 9;
            this.ucLblTxt4.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(320, 0);
            this.checkEdit27.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "无检查结果";
            this.checkEdit27.Size = new System.Drawing.Size(85, 19);
            this.checkEdit27.TabIndex = 7;
            // 
            // radioGroup5
            // 
            this.radioGroup5.Location = new System.Drawing.Point(168, 436);
            this.radioGroup5.Name = "radioGroup5";
            this.radioGroup5.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无收入"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "<500元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "500-1000元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "1000-2000元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", ">2000元/月")});
            this.radioGroup5.Size = new System.Drawing.Size(560, 25);
            this.radioGroup5.StyleController = this.layoutControl1;
            this.radioGroup5.TabIndex = 26;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit16);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit17);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit18);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit19);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit20);
            this.flowLayoutPanel6.Controls.Add(this.textEdit10);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(168, 412);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel6.TabIndex = 16;
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(0, 0);
            this.checkEdit16.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "工资";
            this.checkEdit16.Size = new System.Drawing.Size(51, 19);
            this.checkEdit16.TabIndex = 0;
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(51, 0);
            this.checkEdit17.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "分红";
            this.checkEdit17.Size = new System.Drawing.Size(50, 19);
            this.checkEdit17.TabIndex = 1;
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(101, 0);
            this.checkEdit18.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "房地产租金";
            this.checkEdit18.Size = new System.Drawing.Size(47, 19);
            this.checkEdit18.TabIndex = 2;
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(148, 0);
            this.checkEdit19.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "社会救济（低保）";
            this.checkEdit19.Size = new System.Drawing.Size(120, 19);
            this.checkEdit19.TabIndex = 3;
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(268, 0);
            this.checkEdit20.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "其他";
            this.checkEdit20.Size = new System.Drawing.Size(52, 19);
            this.checkEdit20.TabIndex = 4;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(320, 0);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(240, 20);
            this.textEdit10.TabIndex = 8;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(497, 388);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(231, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 25;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(168, 388);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(236, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 24;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.radioGroup4);
            this.flowLayoutPanel5.Controls.Add(this.textEdit7);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(168, 338);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(560, 46);
            this.flowLayoutPanel5.TabIndex = 23;
            // 
            // radioGroup4
            // 
            this.radioGroup4.Location = new System.Drawing.Point(0, 0);
            this.radioGroup4.Margin = new System.Windows.Forms.Padding(0);
            this.radioGroup4.Name = "radioGroup4";
            this.radioGroup4.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "在岗"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "下岗"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "个体"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "务农"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "无业"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(6)), "不能就业"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("7", "其他")});
            this.radioGroup4.Size = new System.Drawing.Size(472, 20);
            this.radioGroup4.TabIndex = 0;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(3, 23);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(100, 20);
            this.textEdit7.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.checkEdit13);
            this.flowLayoutPanel4.Controls.Add(this.ucLblTxt1);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit14);
            this.flowLayoutPanel4.Controls.Add(this.ucLblTxt2);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit15);
            this.flowLayoutPanel4.Controls.Add(this.textEdit6);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(168, 314);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel4.TabIndex = 22;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(0, 0);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "盲校";
            this.checkEdit13.Size = new System.Drawing.Size(49, 19);
            this.checkEdit13.TabIndex = 0;
            // 
            // ucLblTxt1
            // 
            this.ucLblTxt1.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxt1.Lbl1Text = "年级";
            this.ucLblTxt1.Location = new System.Drawing.Point(49, 0);
            this.ucLblTxt1.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt1.Name = "ucLblTxt1";
            this.ucLblTxt1.Size = new System.Drawing.Size(111, 22);
            this.ucLblTxt1.TabIndex = 1;
            this.ucLblTxt1.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(160, 0);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "聋校";
            this.checkEdit14.Size = new System.Drawing.Size(49, 19);
            this.checkEdit14.TabIndex = 2;
            // 
            // ucLblTxt2
            // 
            this.ucLblTxt2.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxt2.Lbl1Text = "年级";
            this.ucLblTxt2.Location = new System.Drawing.Point(209, 0);
            this.ucLblTxt2.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt2.Name = "ucLblTxt2";
            this.ucLblTxt2.Size = new System.Drawing.Size(111, 22);
            this.ucLblTxt2.TabIndex = 3;
            this.ucLblTxt2.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(320, 0);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "其他特殊学校";
            this.checkEdit15.Size = new System.Drawing.Size(98, 19);
            this.checkEdit15.TabIndex = 4;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(418, 0);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(100, 20);
            this.textEdit6.TabIndex = 5;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(505, 290);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(223, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 21;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(168, 290);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(248, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 20;
            // 
            // radioGroup3
            // 
            this.radioGroup3.Location = new System.Drawing.Point(88, 266);
            this.radioGroup3.Margin = new System.Windows.Forms.Padding(0);
            this.radioGroup3.Name = "radioGroup3";
            this.radioGroup3.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "自知力完全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "自知力不全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "自知力缺失")});
            this.radioGroup3.Size = new System.Drawing.Size(640, 20);
            this.radioGroup3.StyleController = this.layoutControl1;
            this.radioGroup3.TabIndex = 19;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.checkEdit9);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit10);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit11);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit12);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(169, 242);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(559, 20);
            this.flowLayoutPanel3.TabIndex = 18;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(0, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "无";
            this.checkEdit9.Size = new System.Drawing.Size(51, 19);
            this.checkEdit9.TabIndex = 0;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(51, 0);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "智力残疾";
            this.checkEdit10.Size = new System.Drawing.Size(75, 19);
            this.checkEdit10.TabIndex = 1;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(126, 0);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "肢体残疾";
            this.checkEdit11.Size = new System.Drawing.Size(78, 19);
            this.checkEdit11.TabIndex = 2;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(204, 0);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "精神病残疾";
            this.checkEdit12.Size = new System.Drawing.Size(88, 19);
            this.checkEdit12.TabIndex = 3;
            // 
            // radioGroup2
            // 
            this.radioGroup2.Location = new System.Drawing.Point(169, 218);
            this.radioGroup2.Name = "radioGroup2";
            this.radioGroup2.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "5年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "10年以内 "),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "大于10年")});
            this.radioGroup2.Size = new System.Drawing.Size(559, 20);
            this.radioGroup2.StyleController = this.layoutControl1;
            this.radioGroup2.TabIndex = 17;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(169, 194);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "一级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "二级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "三级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "四级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "还未评定等级 ")});
            this.radioGroup1.Size = new System.Drawing.Size(559, 20);
            this.radioGroup1.StyleController = this.layoutControl1;
            this.radioGroup1.TabIndex = 16;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.checkEdit1);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit2);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit3);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit4);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit5);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit6);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit7);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit8);
            this.flowLayoutPanel2.Controls.Add(this.textEdit3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(169, 150);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(559, 40);
            this.flowLayoutPanel2.TabIndex = 15;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(0, 0);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "遗传";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 0;
            this.checkEdit1.Size = new System.Drawing.Size(51, 19);
            this.checkEdit1.TabIndex = 0;
            this.checkEdit1.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(51, 0);
            this.checkEdit2.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "生长发育";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 0;
            this.checkEdit2.Size = new System.Drawing.Size(75, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(126, 0);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "疾病";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 0;
            this.checkEdit3.Size = new System.Drawing.Size(47, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(173, 0);
            this.checkEdit4.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "事故";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 0;
            this.checkEdit4.Size = new System.Drawing.Size(50, 19);
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(223, 0);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "药物中毒";
            this.checkEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit5.Properties.RadioGroupIndex = 0;
            this.checkEdit5.Size = new System.Drawing.Size(75, 19);
            this.checkEdit5.TabIndex = 4;
            this.checkEdit5.TabStop = false;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(298, 0);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "老年";
            this.checkEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit6.Properties.RadioGroupIndex = 0;
            this.checkEdit6.Size = new System.Drawing.Size(51, 19);
            this.checkEdit6.TabIndex = 5;
            this.checkEdit6.TabStop = false;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(349, 0);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "原因不明";
            this.checkEdit7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit7.Properties.RadioGroupIndex = 0;
            this.checkEdit7.Size = new System.Drawing.Size(72, 19);
            this.checkEdit7.TabIndex = 6;
            this.checkEdit7.TabStop = false;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(421, 0);
            this.checkEdit8.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "其他";
            this.checkEdit8.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit8.Properties.RadioGroupIndex = 0;
            this.checkEdit8.Size = new System.Drawing.Size(52, 19);
            this.checkEdit8.TabIndex = 7;
            this.checkEdit8.TabStop = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(0, 19);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(100, 20);
            this.textEdit3.TabIndex = 8;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(422, 126);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(306, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 13;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(88, 126);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(235, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 12;
            // 
            // labelControl8
            // 
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(462, 102);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(266, 20);
            this.labelControl8.StyleController = this.layoutControl1;
            this.labelControl8.TabIndex = 11;
            this.labelControl8.Text = "labelControl8";
            // 
            // labelControl7
            // 
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(98, 102);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(265, 20);
            this.labelControl7.StyleController = this.layoutControl1;
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "labelControl7";
            // 
            // labelControl6
            // 
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(462, 78);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(266, 20);
            this.labelControl6.StyleController = this.layoutControl1;
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "labelControl6";
            // 
            // labelControl5
            // 
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(98, 78);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(265, 20);
            this.labelControl5.StyleController = this.layoutControl1;
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "labelControl5";
            // 
            // labelControl4
            // 
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(462, 54);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(266, 20);
            this.labelControl4.StyleController = this.layoutControl1;
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "labelControl4";
            // 
            // labelControl3
            // 
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(98, 54);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(265, 20);
            this.labelControl3.StyleController = this.layoutControl1;
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "labelControl3";
            // 
            // labelControl2
            // 
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(462, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(266, 20);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "labelControl2";
            // 
            // labelControl1
            // 
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(98, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(265, 20);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "labelControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "听力言语残疾人健康管理随访表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 853);
            this.layoutControlGroup1.Text = "视力残疾人健康管理随访表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 96);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.labelControl8;
            this.layoutControlItem8.CustomizationFormText = "婚姻状况";
            this.layoutControlItem8.Location = new System.Drawing.Point(364, 72);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "婚姻状况";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.labelControl7;
            this.layoutControlItem7.CustomizationFormText = "居住地址 ";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "居住地址 ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.labelControl5;
            this.layoutControlItem5.CustomizationFormText = "出生日期 ";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期 ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.labelControl3;
            this.layoutControlItem3.CustomizationFormText = "性别 ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别 ";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "个人档案号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "个人档案号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.labelControl2;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(364, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.labelControl4;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(364, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.labelControl6;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(364, 48);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "联系电话";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.emptySpaceItem2,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.emptySpaceItem3,
            this.layoutControlItem18,
            this.emptySpaceItem4,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.emptySpaceItem5,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.emptySpaceItem6,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.emptySpaceItem9,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem24,
            this.layoutControlItem25});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 96);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 656);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit2;
            this.layoutControlItem10.CustomizationFormText = "残疾证号";
            this.layoutControlItem10.Location = new System.Drawing.Point(324, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "残疾证号";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit1;
            this.layoutControlItem9.CustomizationFormText = "致残时间";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "致残时间";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "残疾情况";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(81, 116);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "残疾情况";
            this.emptySpaceItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.flowLayoutPanel2;
            this.layoutControlItem12.CustomizationFormText = "致残原因";
            this.layoutControlItem12.Location = new System.Drawing.Point(81, 24);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 44);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(180, 44);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(648, 44);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "致残原因";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.radioGroup1;
            this.layoutControlItem11.CustomizationFormText = "残疾程度";
            this.layoutControlItem11.Location = new System.Drawing.Point(81, 68);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "残疾程度";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.radioGroup2;
            this.layoutControlItem13.CustomizationFormText = "持续时间";
            this.layoutControlItem13.Location = new System.Drawing.Point(81, 92);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "持续时间";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.flowLayoutPanel3;
            this.layoutControlItem14.CustomizationFormText = "其它伴随残疾";
            this.layoutControlItem14.Location = new System.Drawing.Point(81, 116);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(180, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "其它伴随残疾";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.radioGroup3;
            this.layoutControlItem15.CustomizationFormText = "个人自理";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "个人自理";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "监护人";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 164);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "监护人";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.textEdit4;
            this.layoutControlItem16.CustomizationFormText = "姓名";
            this.layoutControlItem16.Location = new System.Drawing.Point(80, 164);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(337, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "姓名";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.textEdit5;
            this.layoutControlItem17.CustomizationFormText = "联系电话";
            this.layoutControlItem17.Location = new System.Drawing.Point(417, 164);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "联系电话";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "文化程度";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 188);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(80, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "文化程度";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.flowLayoutPanel4;
            this.layoutControlItem18.CustomizationFormText = "学历";
            this.layoutControlItem18.Location = new System.Drawing.Point(80, 188);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "学历";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "就业情况";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 212);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(80, 74);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "就业情况";
            this.emptySpaceItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.flowLayoutPanel5;
            this.layoutControlItem19.CustomizationFormText = "就业状况";
            this.layoutControlItem19.Location = new System.Drawing.Point(80, 212);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(187, 50);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(649, 50);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "就业状况";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.textEdit8;
            this.layoutControlItem20.CustomizationFormText = "工作单位 ";
            this.layoutControlItem20.Location = new System.Drawing.Point(80, 262);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(325, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "工作单位 ";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.textEdit9;
            this.layoutControlItem21.CustomizationFormText = "电话 ";
            this.layoutControlItem21.Location = new System.Drawing.Point(405, 262);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "电话 ";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(86, 14);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "个人收入 ";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 286);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(80, 53);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "个人收入 ";
            this.emptySpaceItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.flowLayoutPanel6;
            this.layoutControlItem22.CustomizationFormText = "收入来源";
            this.layoutControlItem22.Location = new System.Drawing.Point(80, 286);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(187, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "收入来源";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.radioGroup5;
            this.layoutControlItem23.CustomizationFormText = "平均收入";
            this.layoutControlItem23.Location = new System.Drawing.Point(80, 310);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(137, 29);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(649, 29);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "平均收入";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "检查评估 ";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 392);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(80, 116);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "检查评估 ";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.flowLayoutPanel7;
            this.layoutControlItem28.CustomizationFormText = "检查结果";
            this.layoutControlItem28.Location = new System.Drawing.Point(80, 392);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "检查结果";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.memoEdit1;
            this.layoutControlItem29.CustomizationFormText = "康复治疗情况";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 508);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(69, 50);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "康复治疗情况";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.memoEdit2;
            this.layoutControlItem30.CustomizationFormText = "康复需求";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 558);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(69, 50);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "康复需求";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.dateEdit1;
            this.layoutControlItem31.CustomizationFormText = "本次随访日期*";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 608);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem31.Text = "本次随访日期*";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit11;
            this.layoutControlItem32.CustomizationFormText = "随访医生";
            this.layoutControlItem32.Location = new System.Drawing.Point(364, 608);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem32.Text = "随访医生";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.dateEdit2;
            this.layoutControlItem33.CustomizationFormText = "下次随访日期*";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 632);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem33.Text = "下次随访日期*";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.labelControl9;
            this.layoutControlItem34.CustomizationFormText = "录入医生 ";
            this.layoutControlItem34.Location = new System.Drawing.Point(364, 632);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem34.Text = "录入医生 ";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "劳动技能";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 339);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(80, 53);
            this.emptySpaceItem9.Text = "劳动技能";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(86, 0);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.radioGroup10;
            this.layoutControlItem41.CustomizationFormText = "劳动能力";
            this.layoutControlItem41.Location = new System.Drawing.Point(80, 339);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(279, 29);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "劳动能力";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.radioGroup11;
            this.layoutControlItem42.CustomizationFormText = "劳动技能";
            this.layoutControlItem42.Location = new System.Drawing.Point(359, 339);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(370, 29);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "劳动技能";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem43.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.flowLayoutPanel8;
            this.layoutControlItem43.CustomizationFormText = "能力来源";
            this.layoutControlItem43.Location = new System.Drawing.Point(80, 368);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(193, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "能力来源";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.flowLayoutPanel9;
            this.layoutControlItem24.CustomizationFormText = "生活能力";
            this.layoutControlItem24.Location = new System.Drawing.Point(80, 416);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(159, 46);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(649, 46);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "生活能力";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.flowLayoutPanel10;
            this.layoutControlItem25.CustomizationFormText = "信息获得方式";
            this.layoutControlItem25.Location = new System.Drawing.Point(80, 462);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(159, 46);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(649, 46);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "信息获得方式";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem35,
            this.layoutControlItem38,
            this.layoutControlItem40,
            this.layoutControlItem39,
            this.layoutControlItem37,
            this.layoutControlItem36});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 752);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(729, 72);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.labelControl10;
            this.layoutControlItem35.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem35.Location = new System.Drawing.Point(318, 0);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "最近更新时间:";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.labelControl13;
            this.layoutControlItem38.CustomizationFormText = "创建机构: ";
            this.layoutControlItem38.Location = new System.Drawing.Point(318, 24);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "创建机构: ";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.labelControl15;
            this.layoutControlItem40.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem40.Location = new System.Drawing.Point(318, 48);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "最近修改人: ";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.labelControl14;
            this.layoutControlItem39.CustomizationFormText = "创建人: ";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "创建人: ";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.labelControl12;
            this.layoutControlItem37.CustomizationFormText = "当前所属机构: ";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "当前所属机构: ";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.labelControl11;
            this.layoutControlItem36.CustomizationFormText = "创建时间: ";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "创建时间: ";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // UC视力残疾人健康管理随访表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC视力残疾人健康管理随访表";
            this.Size = new System.Drawing.Size(748, 498);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup5.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup3.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup radioGroup2;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.RadioGroup radioGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private Library.UserControls.UCLblTxt ucLblTxt1;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private Library.UserControls.UCLblTxt ucLblTxt2;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.RadioGroup radioGroup4;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.RadioGroup radioGroup5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.RadioGroup radioGroup11;
        private DevExpress.XtraEditors.RadioGroup radioGroup10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraEditors.CheckEdit checkEdit37;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraEditors.CheckEdit checkEdit32;
        private DevExpress.XtraEditors.CheckEdit checkEdit33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private Library.UserControls.UCLblTxt ucLblTxt3;
        private Library.UserControls.UCLblTxt ucLblTxt4;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraEditors.TextEdit textEdit13;
    }
}
