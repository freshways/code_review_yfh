﻿  using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;
using AtomEHR.Library.UserControls;

namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    public partial class UC听力言语残疾人健康管理随访表_显示 : UserControlBaseNavBar
    {

        #region Fields
        AtomEHR.Business.bll残疾人_听力 _Bll = new Business.bll残疾人_听力();
        DataSet _ds残疾人听力;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        private string dno;
        private Form frm个人健康;
        DataRow _dr当前数据;
        #endregion
        public UC听力言语残疾人健康管理随访表_显示()
        {
            InitializeComponent();
        }
        public UC听力言语残疾人健康管理随访表_显示(Form frm)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _id = _frm._param as string;
            //_ds残疾人听力 = _Bll.GetAllDataByKey(_docNo, true);
            //DoBindingSummaryEditor(_ds残疾人听力);//绑定数据
        }
        private void btn添加随访_Click(object sender, EventArgs e)
        {
            UC听力言语残疾人健康管理随访表 uc = new UC听力言语残疾人健康管理随访表(_frm, UpdateType.Add);
            ShowControl(uc);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_残疾人_基本信息.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC听力言语残疾人健康管理随访表 uc = new UC听力言语残疾人健康管理随访表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_残疾人_基本信息.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                if (_Bll.Delete(_docNo, _id))
                {
                    Msg.Warning("删除数据成功！");

                    if (!_Bll.CheckNoExists(_docNo))
                    {
                        UC听力言语残疾人健康管理随访表 uc = new UC听力言语残疾人健康管理随访表(_frm, UpdateType.Add);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                    {
                        _id = "";
                        _ds残疾人听力 = _Bll.GetAllDataByKey(_docNo, true);
                        DoBindingDataSource(_ds残疾人听力);//绑定数据
                        this.layoutControl1.VerticalScroll.Value = 0;
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void UC听力言语残疾人健康管理随访表_显示_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i] is TextEdit)
                {
                    TextEdit txt = layoutControl1.Controls[i] as TextEdit;
                    txt.Properties.ReadOnly = true;
                    txt.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxt)
                {
                    UCLblTxt txt = layoutControl1.Controls[i] as UCLblTxt;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLbl)
                {
                    UCTxtLbl txt = layoutControl1.Controls[i] as UCTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLblTxtLbl)
                {
                    UCTxtLblTxtLbl txt = layoutControl1.Controls[i] as UCTxtLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                    txt.Txt2.Properties.ReadOnly = true;
                    txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = layoutControl1.Controls[i] as FlowLayoutPanel;
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is TextEdit)
                        {
                            TextEdit txt = flow.Controls[j] as TextEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxt)
                        {
                            UCLblTxt txt = flow.Controls[j] as UCLblTxt;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLbl)
                        {
                            UCTxtLbl txt = flow.Controls[j] as UCTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxtLbl)
                        {
                            UCLblTxtLbl txt = flow.Controls[j] as UCLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLblTxtLbl)
                        {
                            UCTxtLblTxtLbl txt = flow.Controls[j] as UCTxtLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                            txt.Txt2.Properties.ReadOnly = true;
                            txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is CheckEdit)
                        {
                            CheckEdit txt = flow.Controls[j] as CheckEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                    }
                }
            }
            //根据个人档案编号 查询全部的  体检数据
            _ds残疾人听力 = _Bll.GetAllDataByKey(_docNo, true);
            DoBindingDataSource(_ds残疾人听力);//绑定数据
        }
        private void DoBindingDataSource(DataSet _ds残疾人听力)
        {
            if (_ds残疾人听力 == null) return;
            DataTable dt残疾人听力随访记录 = _ds残疾人听力.Tables[1];
            DataTable dt残疾人基本信息 = _ds残疾人听力.Tables[0];
            #region 先绑定残疾人基本信息
            Bind基本信息(dt残疾人基本信息);
            #endregion

            #region 创建页面左边的导航树
            CreateNavBarButton_new(dt残疾人听力随访记录, tb_残疾人_听力.随访时间);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt残疾人听力随访记录.Rows.Count > 0)
            {
                row = dt残疾人听力随访记录.Rows[0];
            }
            else
            {
                DataRow[] rows = dt残疾人听力随访记录.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
            //设置日期栏中字体颜色
            SetItemColorToRed(_id);
        }
        private void Bind基本信息(DataTable dt残疾人基本信息)
        {
            if (dt残疾人基本信息.Rows.Count == 1)
            {
                DataRow dr = dt残疾人基本信息.Rows[0];
                this.labelControl姓名.Text = util.DESEncrypt.DES解密(dr[tb_残疾人_基本信息.姓名].ToString());
                this.labelControl性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_残疾人_基本信息.性别].ToString());
                this.labelControl婚姻状况.Text = _BLL.ReturnDis字典显示("hyzk", dr[tb_残疾人_基本信息.婚姻状况].ToString());
                this.labelControl个人档案号.Text = dr[tb_残疾人_基本信息.个人档案编号].ToString();
                this.labelControl身份证号.Text = dr[tb_残疾人_基本信息.身份证号].ToString();
                this.labelControl出生日期.Text = dr[tb_残疾人_基本信息.出生日期].ToString();
                this.labelControl联系电话.Text = dr[tb_残疾人_基本信息.联系电话].ToString();
                this.labelControl居住地址.Text = _Bll.Return地区名称(dr[tb_残疾人_基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_残疾人_基本信息.居委会].ToString()) + dr[tb_残疾人_基本信息.居住地址].ToString();

            }
        }
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;
            this.dte残疾时间.Text = _dr当前数据[tb_残疾人_听力.致残时间].ToString();
            this.textEdit残疾证号.Text = _dr当前数据[tb_残疾人_听力.残疾证号].ToString();
            this.textEdit致残原因.Text = _Bll.ReturnDis字典显示("zcyy", _dr当前数据[tb_残疾人_听力.致残原因].ToString());
            this.radio残疾程度.Text = _Bll.ReturnDis字典显示("cjcd", _dr当前数据[tb_残疾人_听力.残疾程度].ToString());
            this.radio持续时间.Text = _Bll.ReturnDis字典显示("cxsj", _dr当前数据[tb_残疾人_听力.持续时间].ToString());
            string qtbscj = string.Empty;
            if (_dr当前数据[tb_残疾人_听力.其他伴随残疾] == null || string.IsNullOrEmpty(_dr当前数据[tb_残疾人_听力.其他伴随残疾].ToString()))
            {
                if (_dr当前数据[tb_残疾人_听力.FIELD2].ToString() == "on")//智力残疾
                {
                    qtbscj = "智力残疾";
                    if (!string.IsNullOrEmpty(_dr当前数据[tb_残疾人_听力.C_ZLCJ].ToString()))//成人 或者 儿童
                    {
                        qtbscj = qtbscj + (" (" + (_dr当前数据[tb_残疾人_听力.C_ZLCJ].ToString() == "1" ? "成人" : "儿童") + ")  ");
                    }
                }
                if (_dr当前数据[tb_残疾人_听力.FIELD3].ToString() == "on")//肢体残疾
                {
                    qtbscj += "肢体残疾";
                    if (!string.IsNullOrEmpty(_dr当前数据[tb_残疾人_听力.C_ZTCJ].ToString()))//成人 或者 儿童
                    {
                        qtbscj += " (" + _dr当前数据[tb_残疾人_听力.C_ZTCJ].ToString() == "1" ? "脑瘫" : "" + ")  ";
                    }
                }
                if (_dr当前数据[tb_残疾人_听力.FIELD5].ToString() == "on")//精神病残疾
                {
                    qtbscj += "   精神病残疾";
                }
            }
            else if (_dr当前数据[tb_残疾人_听力.其他伴随残疾].ToString() == "1")//表示  无其他伴随残疾
            {
                qtbscj = "无";
            }
            this.textEdit伴随残疾.Text = qtbscj;// _Bll.ReturnDis字典显示("qtbscj", _dr当前数据[tb_残疾人_听力.其他伴随残疾].ToString());
            this.radio个人自理.Text = _Bll.ReturnDis字典显示("grzl", _dr当前数据[tb_残疾人_听力.个人自理].ToString());
            this.textEdit监护人姓名.Text = _dr当前数据[tb_残疾人_听力.监护人姓名].ToString();
            this.textEdit监护人联系电话.Text = _dr当前数据[tb_残疾人_听力.监护人电话].ToString();
            this.checkEdit盲校.Checked = _dr当前数据[tb_残疾人_听力.盲校].ToString() == "on" ? true : false;
            this.ucLblTxt盲校年.Txt1.Text = _dr当前数据[tb_残疾人_听力.盲校年级].ToString();
            this.checkEdit聋校.Checked = _dr当前数据[tb_残疾人_听力.聋校].ToString() == "on" ? true : false;
            this.ucLblTxt聋校年.Txt1.Text = _dr当前数据[tb_残疾人_听力.聋校年纪].ToString();
            this.checkEdit其他特殊学校.Checked = _dr当前数据[tb_残疾人_听力.其他特殊学校].ToString() == "on" ? true : false;
            this.textEdit学历其他.Text = _dr当前数据[tb_残疾人_听力.其他特殊学校年级].ToString();
            this.radio就业状况.EditValue = _dr当前数据[tb_残疾人_听力.就业情况].ToString();
            this.textEdit就业状况其他.Text = _dr当前数据[tb_残疾人_听力.就业情况其他].ToString();
            //this.textEdit工作单位.Text = _dr当前数据[tb_残疾人_听力.工作单位].ToString();
            //this.textEdit电话.Text = _dr当前数据[tb_残疾人_听力.电话].ToString();
            SetFlowLayoutResult(_dr当前数据[tb_残疾人_听力.收入来源].ToString(), flow收入来源);
            this.textEdit收入来源其他.Text = _dr当前数据[tb_残疾人_听力.收入来源其他].ToString();
            this.radio平均收入.Text = _Bll.ReturnDis字典显示("pjsr", _dr当前数据[tb_残疾人_听力.平均收入].ToString());
            this.radio劳动能力.Text = _Bll.ReturnDis字典显示("ldnl", _dr当前数据[tb_残疾人_听力.劳动能力].ToString());
            this.radio劳动技能.Text = _Bll.ReturnDis字典显示("ldjn", _dr当前数据[tb_残疾人_听力.劳动技能].ToString());
            SetFlowLayoutResult(_dr当前数据[tb_残疾人_听力.能力来源].ToString(), flow能力来源);
            this.textEdit能力来源.Text = _dr当前数据[tb_残疾人_听力.能力来源其他].ToString();
            this.radio环境声.Text = _Bll.ReturnDis字典显示("ntjdhjs", _dr当前数据[tb_残疾人_听力.环境声].ToString());
            this.radio言语声.Text = _Bll.ReturnDis字典显示("ntjdyys", _dr当前数据[tb_残疾人_听力.言语声].ToString());
            this.radio语言表达能力.Text = _Bll.ReturnDis字典显示("yybdnl", _dr当前数据[tb_残疾人_听力.语言表达能力].ToString());
            this.radio唇读能力.Text = _Bll.ReturnDis字典显示("cdnl", _dr当前数据[tb_残疾人_听力.唇读能力].ToString());
            SetFlowLayoutResult(_dr当前数据[tb_残疾人_听力.沟通方式].ToString(), flow沟通方式);
            this.memoEdit康复治疗情况.Text = _dr当前数据[tb_残疾人_听力.康复治疗情况].ToString();
            this.memoEdit康复需求.Text = _dr当前数据[tb_残疾人_听力.康复需求].ToString();
            this.dateEdit本次随访日期.Text = _dr当前数据[tb_残疾人_听力.随访时间].ToString();
            this.dateEdit下次随访日期.Text = _dr当前数据[tb_残疾人_听力.下次随访日期].ToString();
            this.textEdit随访医生.Text = _dr当前数据[tb_残疾人_听力.随访医生].ToString();

            this.labelControl录入医生.Text = _Bll.Return用户名称(_dr当前数据[tb_残疾人_听力.随访医生].ToString());
            this.labelControl创建时间.Text = _dr当前数据[tb_残疾人_听力.创建日期].ToString();
            this.labelControl最近更新时间.Text = _dr当前数据[tb_残疾人_听力.修改日期].ToString();
            this.labelControl当前所属机构.Text = _Bll.Return机构名称(_dr当前数据[tb_残疾人_听力.所属机构].ToString());
            this.labelControl创建机构.Text = _Bll.Return机构名称(_dr当前数据[tb_残疾人_听力.创建机构].ToString());
            this.labelControl创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_残疾人_听力.创建人].ToString());
            this.labelControl最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_残疾人_听力.修改人].ToString());

        }
    }
}
