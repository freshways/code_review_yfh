﻿namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    partial class UC肢体残疾人健康管理随访表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC肢体残疾人健康管理随访表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.memoEdit伤残部位 = new DevExpress.XtraEditors.MemoEdit();
            this.flow能力来源 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit公办培训机构 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit民办培训机构 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit自学 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit他人辅导 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit能力来源其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit能力来源 = new DevExpress.XtraEditors.TextEdit();
            this.radio劳动技能 = new DevExpress.XtraEditors.RadioGroup();
            this.radio劳动能力 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl录入医生 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit本次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.memoEdit康复需求 = new DevExpress.XtraEditors.MemoEdit();
            this.memoEdit康复治疗情况 = new DevExpress.XtraEditors.MemoEdit();
            this.radio平均收入 = new DevExpress.XtraEditors.RadioGroup();
            this.flow收入来源 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit工资 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit分红 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit房地 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit社会救济 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit收入来源其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit收入来源其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit工作单位 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio就业状况 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit就业状况其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit盲校 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxt盲校年 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.checkEdit聋校 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxt聋校年 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.checkEdit其他特殊学校 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit学历其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit监护人联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit监护人姓名 = new DevExpress.XtraEditors.TextEdit();
            this.radio个人自理 = new DevExpress.XtraEditors.RadioGroup();
            this.flow其他伴随残疾 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit伴随残疾无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit伴随残疾智力 = new DevExpress.XtraEditors.CheckEdit();
            this.radio伴随残疾智力 = new DevExpress.XtraEditors.RadioGroup();
            this.checkEdit伴随残疾肢体 = new DevExpress.XtraEditors.CheckEdit();
            this.radio伴随残疾脑瘫 = new DevExpress.XtraEditors.RadioGroup();
            this.checkEdit伴随残疾精神 = new DevExpress.XtraEditors.CheckEdit();
            this.radio持续时间 = new DevExpress.XtraEditors.RadioGroup();
            this.radio残疾程度 = new DevExpress.XtraEditors.RadioGroup();
            this.flow致残原因 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit致残原因遗传 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因生长发育 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因事故 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因药物中毒 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因老年 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因原因不明 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit致残原因其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit致残原因其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit残疾证号 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl性别 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.dte残疾时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit伤残部位.Properties)).BeginInit();
            this.flow能力来源.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit公办培训机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit民办培训机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自学.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit他人辅导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit能力来源其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit能力来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio劳动技能.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio劳动能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit本次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit本次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit康复需求.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit康复治疗情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio平均收入.Properties)).BeginInit();
            this.flow收入来源.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit工资.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit分红.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit房地.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit社会救济.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit收入来源其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit收入来源其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit工作单位.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio就业状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit就业状况其他.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit盲校.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit聋校.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他特殊学校.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit学历其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit监护人联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit监护人姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio个人自理.Properties)).BeginInit();
            this.flow其他伴随残疾.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾智力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio伴随残疾智力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾肢体.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio伴随残疾脑瘫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾精神.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio持续时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).BeginInit();
            this.flow致残原因.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因遗传.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因生长发育.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因事故.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因药物中毒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因老年.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因原因不明.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit致残原因其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit残疾证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte残疾时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte残疾时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.memoEdit伤残部位);
            this.layoutControl1.Controls.Add(this.flow能力来源);
            this.layoutControl1.Controls.Add(this.radio劳动技能);
            this.layoutControl1.Controls.Add(this.radio劳动能力);
            this.layoutControl1.Controls.Add(this.labelControl最近修改人);
            this.layoutControl1.Controls.Add(this.labelControl创建人);
            this.layoutControl1.Controls.Add(this.labelControl创建机构);
            this.layoutControl1.Controls.Add(this.labelControl当前所属机构);
            this.layoutControl1.Controls.Add(this.labelControl创建时间);
            this.layoutControl1.Controls.Add(this.labelControl最近更新时间);
            this.layoutControl1.Controls.Add(this.labelControl录入医生);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.dateEdit本次随访日期);
            this.layoutControl1.Controls.Add(this.memoEdit康复需求);
            this.layoutControl1.Controls.Add(this.memoEdit康复治疗情况);
            this.layoutControl1.Controls.Add(this.radio平均收入);
            this.layoutControl1.Controls.Add(this.flow收入来源);
            this.layoutControl1.Controls.Add(this.textEdit电话);
            this.layoutControl1.Controls.Add(this.textEdit工作单位);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.textEdit监护人联系电话);
            this.layoutControl1.Controls.Add(this.textEdit监护人姓名);
            this.layoutControl1.Controls.Add(this.radio个人自理);
            this.layoutControl1.Controls.Add(this.flow其他伴随残疾);
            this.layoutControl1.Controls.Add(this.radio持续时间);
            this.layoutControl1.Controls.Add(this.radio残疾程度);
            this.layoutControl1.Controls.Add(this.flow致残原因);
            this.layoutControl1.Controls.Add(this.textEdit残疾证号);
            this.layoutControl1.Controls.Add(this.labelControl婚姻状况);
            this.layoutControl1.Controls.Add(this.labelControl居住地址);
            this.layoutControl1.Controls.Add(this.labelControl联系电话);
            this.layoutControl1.Controls.Add(this.labelControl出生日期);
            this.layoutControl1.Controls.Add(this.labelControl身份证号);
            this.layoutControl1.Controls.Add(this.labelControl性别);
            this.layoutControl1.Controls.Add(this.labelControl姓名);
            this.layoutControl1.Controls.Add(this.labelControl个人档案号);
            this.layoutControl1.Controls.Add(this.dte残疾时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // memoEdit伤残部位
            // 
            this.memoEdit伤残部位.Location = new System.Drawing.Point(88, 197);
            this.memoEdit伤残部位.Name = "memoEdit伤残部位";
            this.memoEdit伤残部位.Size = new System.Drawing.Size(411, 46);
            this.memoEdit伤残部位.StyleController = this.layoutControl1;
            this.memoEdit伤残部位.TabIndex = 46;
            this.memoEdit伤残部位.UseOptimizedRendering = true;
            // 
            // flow能力来源
            // 
            this.flow能力来源.Controls.Add(this.checkEdit公办培训机构);
            this.flow能力来源.Controls.Add(this.checkEdit民办培训机构);
            this.flow能力来源.Controls.Add(this.checkEdit自学);
            this.flow能力来源.Controls.Add(this.checkEdit他人辅导);
            this.flow能力来源.Controls.Add(this.checkEdit能力来源其他);
            this.flow能力来源.Controls.Add(this.textEdit能力来源);
            this.flow能力来源.Location = new System.Drawing.Point(168, 173);
            this.flow能力来源.Name = "flow能力来源";
            this.flow能力来源.Size = new System.Drawing.Size(560, 20);
            this.flow能力来源.TabIndex = 17;
            // 
            // checkEdit公办培训机构
            // 
            this.checkEdit公办培训机构.Location = new System.Drawing.Point(0, 0);
            this.checkEdit公办培训机构.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit公办培训机构.Name = "checkEdit公办培训机构";
            this.checkEdit公办培训机构.Properties.Caption = "公办培训机构";
            this.checkEdit公办培训机构.Size = new System.Drawing.Size(97, 19);
            this.checkEdit公办培训机构.TabIndex = 0;
            this.checkEdit公办培训机构.Tag = "1";
            // 
            // checkEdit民办培训机构
            // 
            this.checkEdit民办培训机构.Location = new System.Drawing.Point(97, 0);
            this.checkEdit民办培训机构.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit民办培训机构.Name = "checkEdit民办培训机构";
            this.checkEdit民办培训机构.Properties.Caption = "民办培训机构";
            this.checkEdit民办培训机构.Size = new System.Drawing.Size(93, 19);
            this.checkEdit民办培训机构.TabIndex = 1;
            this.checkEdit民办培训机构.Tag = "2";
            // 
            // checkEdit自学
            // 
            this.checkEdit自学.Location = new System.Drawing.Point(190, 0);
            this.checkEdit自学.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit自学.Name = "checkEdit自学";
            this.checkEdit自学.Properties.Caption = "自学";
            this.checkEdit自学.Size = new System.Drawing.Size(47, 19);
            this.checkEdit自学.TabIndex = 2;
            this.checkEdit自学.Tag = "3";
            // 
            // checkEdit他人辅导
            // 
            this.checkEdit他人辅导.Location = new System.Drawing.Point(237, 0);
            this.checkEdit他人辅导.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit他人辅导.Name = "checkEdit他人辅导";
            this.checkEdit他人辅导.Properties.Caption = "他人辅导";
            this.checkEdit他人辅导.Size = new System.Drawing.Size(74, 19);
            this.checkEdit他人辅导.TabIndex = 3;
            this.checkEdit他人辅导.Tag = "4";
            // 
            // checkEdit能力来源其他
            // 
            this.checkEdit能力来源其他.Location = new System.Drawing.Point(311, 0);
            this.checkEdit能力来源其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit能力来源其他.Name = "checkEdit能力来源其他";
            this.checkEdit能力来源其他.Properties.Caption = "其他";
            this.checkEdit能力来源其他.Size = new System.Drawing.Size(52, 19);
            this.checkEdit能力来源其他.TabIndex = 4;
            this.checkEdit能力来源其他.Tag = "99";
            // 
            // textEdit能力来源
            // 
            this.textEdit能力来源.Location = new System.Drawing.Point(363, 0);
            this.textEdit能力来源.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit能力来源.Name = "textEdit能力来源";
            this.textEdit能力来源.Size = new System.Drawing.Size(111, 20);
            this.textEdit能力来源.TabIndex = 8;
            // 
            // radio劳动技能
            // 
            this.radio劳动技能.Location = new System.Drawing.Point(447, 144);
            this.radio劳动技能.Name = "radio劳动技能";
            this.radio劳动技能.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "强"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "差"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "无")});
            this.radio劳动技能.Size = new System.Drawing.Size(281, 25);
            this.radio劳动技能.StyleController = this.layoutControl1;
            this.radio劳动技能.TabIndex = 45;
            // 
            // radio劳动能力
            // 
            this.radio劳动能力.Location = new System.Drawing.Point(168, 144);
            this.radio劳动能力.Name = "radio劳动能力";
            this.radio劳动能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "弱"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "无")});
            this.radio劳动能力.Size = new System.Drawing.Size(190, 25);
            this.radio劳动能力.StyleController = this.layoutControl1;
            this.radio劳动能力.TabIndex = 44;
            // 
            // labelControl最近修改人
            // 
            this.labelControl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl最近修改人.Location = new System.Drawing.Point(416, 443);
            this.labelControl最近修改人.Name = "labelControl最近修改人";
            this.labelControl最近修改人.Size = new System.Drawing.Size(312, 20);
            this.labelControl最近修改人.StyleController = this.layoutControl1;
            this.labelControl最近修改人.TabIndex = 43;
            this.labelControl最近修改人.Text = " ";
            // 
            // labelControl创建人
            // 
            this.labelControl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl创建人.Location = new System.Drawing.Point(98, 443);
            this.labelControl创建人.Name = "labelControl创建人";
            this.labelControl创建人.Size = new System.Drawing.Size(219, 20);
            this.labelControl创建人.StyleController = this.layoutControl1;
            this.labelControl创建人.TabIndex = 42;
            this.labelControl创建人.Text = " ";
            // 
            // labelControl创建机构
            // 
            this.labelControl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl创建机构.Location = new System.Drawing.Point(416, 419);
            this.labelControl创建机构.Name = "labelControl创建机构";
            this.labelControl创建机构.Size = new System.Drawing.Size(312, 20);
            this.labelControl创建机构.StyleController = this.layoutControl1;
            this.labelControl创建机构.TabIndex = 41;
            this.labelControl创建机构.Text = " ";
            // 
            // labelControl当前所属机构
            // 
            this.labelControl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl当前所属机构.Location = new System.Drawing.Point(98, 419);
            this.labelControl当前所属机构.Name = "labelControl当前所属机构";
            this.labelControl当前所属机构.Size = new System.Drawing.Size(219, 20);
            this.labelControl当前所属机构.StyleController = this.layoutControl1;
            this.labelControl当前所属机构.TabIndex = 40;
            this.labelControl当前所属机构.Text = " ";
            // 
            // labelControl创建时间
            // 
            this.labelControl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl创建时间.Location = new System.Drawing.Point(98, 395);
            this.labelControl创建时间.Name = "labelControl创建时间";
            this.labelControl创建时间.Size = new System.Drawing.Size(219, 20);
            this.labelControl创建时间.StyleController = this.layoutControl1;
            this.labelControl创建时间.TabIndex = 39;
            this.labelControl创建时间.Text = " ";
            // 
            // labelControl最近更新时间
            // 
            this.labelControl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl最近更新时间.Location = new System.Drawing.Point(416, 395);
            this.labelControl最近更新时间.Name = "labelControl最近更新时间";
            this.labelControl最近更新时间.Size = new System.Drawing.Size(312, 20);
            this.labelControl最近更新时间.StyleController = this.layoutControl1;
            this.labelControl最近更新时间.TabIndex = 38;
            this.labelControl最近更新时间.Text = " ";
            // 
            // labelControl录入医生
            // 
            this.labelControl录入医生.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl录入医生.Location = new System.Drawing.Point(452, 371);
            this.labelControl录入医生.Name = "labelControl录入医生";
            this.labelControl录入医生.Size = new System.Drawing.Size(276, 20);
            this.labelControl录入医生.StyleController = this.layoutControl1;
            this.labelControl录入医生.TabIndex = 37;
            this.labelControl录入医生.Text = " ";
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(92, 371);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(271, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 36;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(456, 347);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(272, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 35;
            // 
            // dateEdit本次随访日期
            // 
            this.dateEdit本次随访日期.EditValue = null;
            this.dateEdit本次随访日期.Location = new System.Drawing.Point(92, 347);
            this.dateEdit本次随访日期.Name = "dateEdit本次随访日期";
            this.dateEdit本次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit本次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit本次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit本次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit本次随访日期.Size = new System.Drawing.Size(271, 20);
            this.dateEdit本次随访日期.StyleController = this.layoutControl1;
            this.dateEdit本次随访日期.TabIndex = 34;
            // 
            // memoEdit康复需求
            // 
            this.memoEdit康复需求.Location = new System.Drawing.Point(88, 297);
            this.memoEdit康复需求.Name = "memoEdit康复需求";
            this.memoEdit康复需求.Size = new System.Drawing.Size(411, 46);
            this.memoEdit康复需求.StyleController = this.layoutControl1;
            this.memoEdit康复需求.TabIndex = 33;
            this.memoEdit康复需求.UseOptimizedRendering = true;
            // 
            // memoEdit康复治疗情况
            // 
            this.memoEdit康复治疗情况.Location = new System.Drawing.Point(88, 247);
            this.memoEdit康复治疗情况.Name = "memoEdit康复治疗情况";
            this.memoEdit康复治疗情况.Size = new System.Drawing.Size(411, 46);
            this.memoEdit康复治疗情况.StyleController = this.layoutControl1;
            this.memoEdit康复治疗情况.TabIndex = 32;
            this.memoEdit康复治疗情况.UseOptimizedRendering = true;
            // 
            // radio平均收入
            // 
            this.radio平均收入.Location = new System.Drawing.Point(168, 115);
            this.radio平均收入.Name = "radio平均收入";
            this.radio平均收入.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无收入"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "<500元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "500-1000元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "1000-2000元/月"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", ">2000元/月")});
            this.radio平均收入.Size = new System.Drawing.Size(560, 25);
            this.radio平均收入.StyleController = this.layoutControl1;
            this.radio平均收入.TabIndex = 26;
            // 
            // flow收入来源
            // 
            this.flow收入来源.Controls.Add(this.checkEdit工资);
            this.flow收入来源.Controls.Add(this.checkEdit分红);
            this.flow收入来源.Controls.Add(this.checkEdit房地);
            this.flow收入来源.Controls.Add(this.checkEdit社会救济);
            this.flow收入来源.Controls.Add(this.checkEdit收入来源其他);
            this.flow收入来源.Controls.Add(this.textEdit收入来源其他);
            this.flow收入来源.Location = new System.Drawing.Point(168, 91);
            this.flow收入来源.Name = "flow收入来源";
            this.flow收入来源.Size = new System.Drawing.Size(560, 20);
            this.flow收入来源.TabIndex = 16;
            // 
            // checkEdit工资
            // 
            this.checkEdit工资.Location = new System.Drawing.Point(0, 0);
            this.checkEdit工资.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit工资.Name = "checkEdit工资";
            this.checkEdit工资.Properties.Caption = "工资";
            this.checkEdit工资.Size = new System.Drawing.Size(51, 19);
            this.checkEdit工资.TabIndex = 0;
            this.checkEdit工资.Tag = "1";
            // 
            // checkEdit分红
            // 
            this.checkEdit分红.Location = new System.Drawing.Point(51, 0);
            this.checkEdit分红.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit分红.Name = "checkEdit分红";
            this.checkEdit分红.Properties.Caption = "分红";
            this.checkEdit分红.Size = new System.Drawing.Size(50, 19);
            this.checkEdit分红.TabIndex = 1;
            this.checkEdit分红.Tag = "2";
            // 
            // checkEdit房地
            // 
            this.checkEdit房地.Location = new System.Drawing.Point(101, 0);
            this.checkEdit房地.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit房地.Name = "checkEdit房地";
            this.checkEdit房地.Properties.Caption = "房地产租金";
            this.checkEdit房地.Size = new System.Drawing.Size(47, 19);
            this.checkEdit房地.TabIndex = 2;
            this.checkEdit房地.Tag = "3";
            // 
            // checkEdit社会救济
            // 
            this.checkEdit社会救济.Location = new System.Drawing.Point(148, 0);
            this.checkEdit社会救济.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit社会救济.Name = "checkEdit社会救济";
            this.checkEdit社会救济.Properties.Caption = "社会救济（低保）";
            this.checkEdit社会救济.Size = new System.Drawing.Size(120, 19);
            this.checkEdit社会救济.TabIndex = 3;
            this.checkEdit社会救济.Tag = "4";
            // 
            // checkEdit收入来源其他
            // 
            this.checkEdit收入来源其他.Location = new System.Drawing.Point(268, 0);
            this.checkEdit收入来源其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit收入来源其他.Name = "checkEdit收入来源其他";
            this.checkEdit收入来源其他.Properties.Caption = "其他";
            this.checkEdit收入来源其他.Size = new System.Drawing.Size(52, 19);
            this.checkEdit收入来源其他.TabIndex = 4;
            this.checkEdit收入来源其他.Tag = "99";
            // 
            // textEdit收入来源其他
            // 
            this.textEdit收入来源其他.Location = new System.Drawing.Point(320, 0);
            this.textEdit收入来源其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit收入来源其他.Name = "textEdit收入来源其他";
            this.textEdit收入来源其他.Size = new System.Drawing.Size(154, 20);
            this.textEdit收入来源其他.TabIndex = 8;
            // 
            // textEdit电话
            // 
            this.textEdit电话.Enabled = false;
            this.textEdit电话.Location = new System.Drawing.Point(497, 67);
            this.textEdit电话.Name = "textEdit电话";
            this.textEdit电话.Size = new System.Drawing.Size(231, 20);
            this.textEdit电话.StyleController = this.layoutControl1;
            this.textEdit电话.TabIndex = 25;
            // 
            // textEdit工作单位
            // 
            this.textEdit工作单位.Enabled = false;
            this.textEdit工作单位.Location = new System.Drawing.Point(168, 67);
            this.textEdit工作单位.Name = "textEdit工作单位";
            this.textEdit工作单位.Size = new System.Drawing.Size(236, 20);
            this.textEdit工作单位.StyleController = this.layoutControl1;
            this.textEdit工作单位.TabIndex = 24;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.radio就业状况);
            this.flowLayoutPanel5.Controls.Add(this.textEdit就业状况其他);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(168, 17);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(560, 46);
            this.flowLayoutPanel5.TabIndex = 23;
            // 
            // radio就业状况
            // 
            this.radio就业状况.Location = new System.Drawing.Point(0, 0);
            this.radio就业状况.Margin = new System.Windows.Forms.Padding(0);
            this.radio就业状况.Name = "radio就业状况";
            this.radio就业状况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "在岗"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "下岗"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "个体"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "务农"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "无业"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(6)), "不能就业"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio就业状况.Size = new System.Drawing.Size(472, 20);
            this.radio就业状况.TabIndex = 0;
            // 
            // textEdit就业状况其他
            // 
            this.textEdit就业状况其他.Location = new System.Drawing.Point(3, 23);
            this.textEdit就业状况其他.Name = "textEdit就业状况其他";
            this.textEdit就业状况其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit就业状况其他.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.checkEdit盲校);
            this.flowLayoutPanel4.Controls.Add(this.ucLblTxt盲校年);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit聋校);
            this.flowLayoutPanel4.Controls.Add(this.ucLblTxt聋校年);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit其他特殊学校);
            this.flowLayoutPanel4.Controls.Add(this.textEdit学历其他);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(168, -7);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel4.TabIndex = 22;
            // 
            // checkEdit盲校
            // 
            this.checkEdit盲校.Location = new System.Drawing.Point(0, 0);
            this.checkEdit盲校.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit盲校.Name = "checkEdit盲校";
            this.checkEdit盲校.Properties.Caption = "盲校";
            this.checkEdit盲校.Size = new System.Drawing.Size(49, 19);
            this.checkEdit盲校.TabIndex = 0;
            // 
            // ucLblTxt盲校年
            // 
            this.ucLblTxt盲校年.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxt盲校年.Lbl1Text = "年级";
            this.ucLblTxt盲校年.Location = new System.Drawing.Point(49, 0);
            this.ucLblTxt盲校年.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt盲校年.Name = "ucLblTxt盲校年";
            this.ucLblTxt盲校年.Size = new System.Drawing.Size(111, 22);
            this.ucLblTxt盲校年.TabIndex = 1;
            this.ucLblTxt盲校年.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit聋校
            // 
            this.checkEdit聋校.Location = new System.Drawing.Point(160, 0);
            this.checkEdit聋校.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit聋校.Name = "checkEdit聋校";
            this.checkEdit聋校.Properties.Caption = "聋校";
            this.checkEdit聋校.Size = new System.Drawing.Size(49, 19);
            this.checkEdit聋校.TabIndex = 2;
            // 
            // ucLblTxt聋校年
            // 
            this.ucLblTxt聋校年.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxt聋校年.Lbl1Text = "年级";
            this.ucLblTxt聋校年.Location = new System.Drawing.Point(209, 0);
            this.ucLblTxt聋校年.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt聋校年.Name = "ucLblTxt聋校年";
            this.ucLblTxt聋校年.Size = new System.Drawing.Size(111, 22);
            this.ucLblTxt聋校年.TabIndex = 3;
            this.ucLblTxt聋校年.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit其他特殊学校
            // 
            this.checkEdit其他特殊学校.Location = new System.Drawing.Point(320, 0);
            this.checkEdit其他特殊学校.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit其他特殊学校.Name = "checkEdit其他特殊学校";
            this.checkEdit其他特殊学校.Properties.Caption = "其他特殊学校";
            this.checkEdit其他特殊学校.Size = new System.Drawing.Size(98, 19);
            this.checkEdit其他特殊学校.TabIndex = 4;
            // 
            // textEdit学历其他
            // 
            this.textEdit学历其他.Location = new System.Drawing.Point(418, 0);
            this.textEdit学历其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit学历其他.Name = "textEdit学历其他";
            this.textEdit学历其他.Size = new System.Drawing.Size(56, 20);
            this.textEdit学历其他.TabIndex = 5;
            // 
            // textEdit监护人联系电话
            // 
            this.textEdit监护人联系电话.Location = new System.Drawing.Point(505, -31);
            this.textEdit监护人联系电话.Name = "textEdit监护人联系电话";
            this.textEdit监护人联系电话.Size = new System.Drawing.Size(223, 20);
            this.textEdit监护人联系电话.StyleController = this.layoutControl1;
            this.textEdit监护人联系电话.TabIndex = 21;
            // 
            // textEdit监护人姓名
            // 
            this.textEdit监护人姓名.Location = new System.Drawing.Point(168, -31);
            this.textEdit监护人姓名.Name = "textEdit监护人姓名";
            this.textEdit监护人姓名.Size = new System.Drawing.Size(248, 20);
            this.textEdit监护人姓名.StyleController = this.layoutControl1;
            this.textEdit监护人姓名.TabIndex = 20;
            // 
            // radio个人自理
            // 
            this.radio个人自理.Location = new System.Drawing.Point(88, -55);
            this.radio个人自理.Margin = new System.Windows.Forms.Padding(0);
            this.radio个人自理.Name = "radio个人自理";
            this.radio个人自理.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "自知力完全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "自知力不全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "自知力缺失")});
            this.radio个人自理.Size = new System.Drawing.Size(311, 20);
            this.radio个人自理.StyleController = this.layoutControl1;
            this.radio个人自理.TabIndex = 19;
            // 
            // flow其他伴随残疾
            // 
            this.flow其他伴随残疾.Controls.Add(this.checkEdit伴随残疾无);
            this.flow其他伴随残疾.Controls.Add(this.checkEdit伴随残疾智力);
            this.flow其他伴随残疾.Controls.Add(this.radio伴随残疾智力);
            this.flow其他伴随残疾.Controls.Add(this.checkEdit伴随残疾肢体);
            this.flow其他伴随残疾.Controls.Add(this.radio伴随残疾脑瘫);
            this.flow其他伴随残疾.Controls.Add(this.checkEdit伴随残疾精神);
            this.flow其他伴随残疾.Location = new System.Drawing.Point(169, -83);
            this.flow其他伴随残疾.Name = "flow其他伴随残疾";
            this.flow其他伴随残疾.Size = new System.Drawing.Size(559, 24);
            this.flow其他伴随残疾.TabIndex = 18;
            // 
            // checkEdit伴随残疾无
            // 
            this.checkEdit伴随残疾无.EditValue = true;
            this.checkEdit伴随残疾无.Location = new System.Drawing.Point(0, 0);
            this.checkEdit伴随残疾无.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit伴随残疾无.Name = "checkEdit伴随残疾无";
            this.checkEdit伴随残疾无.Properties.Caption = "无";
            this.checkEdit伴随残疾无.Size = new System.Drawing.Size(51, 19);
            this.checkEdit伴随残疾无.TabIndex = 0;
            this.checkEdit伴随残疾无.CheckedChanged += new System.EventHandler(this.checkEdit伴随残疾无_CheckedChanged);
            // 
            // checkEdit伴随残疾智力
            // 
            this.checkEdit伴随残疾智力.Enabled = false;
            this.checkEdit伴随残疾智力.Location = new System.Drawing.Point(51, 0);
            this.checkEdit伴随残疾智力.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit伴随残疾智力.Name = "checkEdit伴随残疾智力";
            this.checkEdit伴随残疾智力.Properties.Caption = "智力残疾";
            this.checkEdit伴随残疾智力.Size = new System.Drawing.Size(75, 19);
            this.checkEdit伴随残疾智力.TabIndex = 1;
            this.checkEdit伴随残疾智力.CheckedChanged += new System.EventHandler(this.checkEdit伴随残疾智力_CheckedChanged);
            // 
            // radio伴随残疾智力
            // 
            this.radio伴随残疾智力.Location = new System.Drawing.Point(129, 0);
            this.radio伴随残疾智力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio伴随残疾智力.Name = "radio伴随残疾智力";
            this.radio伴随残疾智力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "成人"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "儿童")});
            this.radio伴随残疾智力.Size = new System.Drawing.Size(107, 25);
            this.radio伴随残疾智力.TabIndex = 4;
            this.radio伴随残疾智力.Visible = false;
            // 
            // checkEdit伴随残疾肢体
            // 
            this.checkEdit伴随残疾肢体.Enabled = false;
            this.checkEdit伴随残疾肢体.Location = new System.Drawing.Point(239, 0);
            this.checkEdit伴随残疾肢体.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit伴随残疾肢体.Name = "checkEdit伴随残疾肢体";
            this.checkEdit伴随残疾肢体.Properties.Caption = "肢体残疾";
            this.checkEdit伴随残疾肢体.Size = new System.Drawing.Size(78, 19);
            this.checkEdit伴随残疾肢体.TabIndex = 2;
            this.checkEdit伴随残疾肢体.CheckedChanged += new System.EventHandler(this.checkEdit伴随残疾肢体_CheckedChanged);
            // 
            // radio伴随残疾脑瘫
            // 
            this.radio伴随残疾脑瘫.Location = new System.Drawing.Point(320, 0);
            this.radio伴随残疾脑瘫.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio伴随残疾脑瘫.Name = "radio伴随残疾脑瘫";
            this.radio伴随残疾脑瘫.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "脑瘫")});
            this.radio伴随残疾脑瘫.Size = new System.Drawing.Size(53, 24);
            this.radio伴随残疾脑瘫.TabIndex = 5;
            this.radio伴随残疾脑瘫.Visible = false;
            // 
            // checkEdit伴随残疾精神
            // 
            this.checkEdit伴随残疾精神.Enabled = false;
            this.checkEdit伴随残疾精神.Location = new System.Drawing.Point(376, 0);
            this.checkEdit伴随残疾精神.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit伴随残疾精神.Name = "checkEdit伴随残疾精神";
            this.checkEdit伴随残疾精神.Properties.Caption = "精神病残疾";
            this.checkEdit伴随残疾精神.Size = new System.Drawing.Size(88, 19);
            this.checkEdit伴随残疾精神.TabIndex = 3;
            // 
            // radio持续时间
            // 
            this.radio持续时间.Location = new System.Drawing.Point(169, -107);
            this.radio持续时间.Name = "radio持续时间";
            this.radio持续时间.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "5年以内"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "10年以内 "),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "大于10年")});
            this.radio持续时间.Size = new System.Drawing.Size(559, 20);
            this.radio持续时间.StyleController = this.layoutControl1;
            this.radio持续时间.TabIndex = 17;
            // 
            // radio残疾程度
            // 
            this.radio残疾程度.Location = new System.Drawing.Point(169, -131);
            this.radio残疾程度.Name = "radio残疾程度";
            this.radio残疾程度.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "一级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "二级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "三级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "四级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "还未评定等级 ")});
            this.radio残疾程度.Size = new System.Drawing.Size(559, 20);
            this.radio残疾程度.StyleController = this.layoutControl1;
            this.radio残疾程度.TabIndex = 16;
            // 
            // flow致残原因
            // 
            this.flow致残原因.Controls.Add(this.checkEdit致残原因遗传);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因生长发育);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因疾病);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因事故);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因药物中毒);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因老年);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因原因不明);
            this.flow致残原因.Controls.Add(this.checkEdit致残原因其他);
            this.flow致残原因.Controls.Add(this.textEdit致残原因其他);
            this.flow致残原因.Location = new System.Drawing.Point(169, -175);
            this.flow致残原因.Name = "flow致残原因";
            this.flow致残原因.Size = new System.Drawing.Size(559, 40);
            this.flow致残原因.TabIndex = 15;
            // 
            // checkEdit致残原因遗传
            // 
            this.checkEdit致残原因遗传.Location = new System.Drawing.Point(0, 0);
            this.checkEdit致残原因遗传.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因遗传.Name = "checkEdit致残原因遗传";
            this.checkEdit致残原因遗传.Properties.Caption = "遗传";
            this.checkEdit致残原因遗传.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因遗传.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因遗传.Size = new System.Drawing.Size(51, 19);
            this.checkEdit致残原因遗传.TabIndex = 0;
            this.checkEdit致残原因遗传.TabStop = false;
            this.checkEdit致残原因遗传.Tag = "1";
            // 
            // checkEdit致残原因生长发育
            // 
            this.checkEdit致残原因生长发育.Location = new System.Drawing.Point(51, 0);
            this.checkEdit致残原因生长发育.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因生长发育.Name = "checkEdit致残原因生长发育";
            this.checkEdit致残原因生长发育.Properties.Caption = "生长发育";
            this.checkEdit致残原因生长发育.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因生长发育.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因生长发育.Size = new System.Drawing.Size(75, 19);
            this.checkEdit致残原因生长发育.TabIndex = 1;
            this.checkEdit致残原因生长发育.TabStop = false;
            this.checkEdit致残原因生长发育.Tag = "2";
            // 
            // checkEdit致残原因疾病
            // 
            this.checkEdit致残原因疾病.Location = new System.Drawing.Point(126, 0);
            this.checkEdit致残原因疾病.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因疾病.Name = "checkEdit致残原因疾病";
            this.checkEdit致残原因疾病.Properties.Caption = "疾病";
            this.checkEdit致残原因疾病.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因疾病.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因疾病.Size = new System.Drawing.Size(47, 19);
            this.checkEdit致残原因疾病.TabIndex = 2;
            this.checkEdit致残原因疾病.TabStop = false;
            this.checkEdit致残原因疾病.Tag = "3";
            // 
            // checkEdit致残原因事故
            // 
            this.checkEdit致残原因事故.Location = new System.Drawing.Point(173, 0);
            this.checkEdit致残原因事故.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因事故.Name = "checkEdit致残原因事故";
            this.checkEdit致残原因事故.Properties.Caption = "事故";
            this.checkEdit致残原因事故.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因事故.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因事故.Size = new System.Drawing.Size(50, 19);
            this.checkEdit致残原因事故.TabIndex = 3;
            this.checkEdit致残原因事故.TabStop = false;
            this.checkEdit致残原因事故.Tag = "4";
            // 
            // checkEdit致残原因药物中毒
            // 
            this.checkEdit致残原因药物中毒.Location = new System.Drawing.Point(223, 0);
            this.checkEdit致残原因药物中毒.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因药物中毒.Name = "checkEdit致残原因药物中毒";
            this.checkEdit致残原因药物中毒.Properties.Caption = "药物中毒";
            this.checkEdit致残原因药物中毒.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因药物中毒.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因药物中毒.Size = new System.Drawing.Size(75, 19);
            this.checkEdit致残原因药物中毒.TabIndex = 4;
            this.checkEdit致残原因药物中毒.TabStop = false;
            this.checkEdit致残原因药物中毒.Tag = "5";
            // 
            // checkEdit致残原因老年
            // 
            this.checkEdit致残原因老年.Location = new System.Drawing.Point(298, 0);
            this.checkEdit致残原因老年.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因老年.Name = "checkEdit致残原因老年";
            this.checkEdit致残原因老年.Properties.Caption = "老年";
            this.checkEdit致残原因老年.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因老年.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因老年.Size = new System.Drawing.Size(51, 19);
            this.checkEdit致残原因老年.TabIndex = 5;
            this.checkEdit致残原因老年.TabStop = false;
            this.checkEdit致残原因老年.Tag = "6";
            // 
            // checkEdit致残原因原因不明
            // 
            this.checkEdit致残原因原因不明.Location = new System.Drawing.Point(349, 0);
            this.checkEdit致残原因原因不明.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因原因不明.Name = "checkEdit致残原因原因不明";
            this.checkEdit致残原因原因不明.Properties.Caption = "原因不明";
            this.checkEdit致残原因原因不明.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因原因不明.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因原因不明.Size = new System.Drawing.Size(72, 19);
            this.checkEdit致残原因原因不明.TabIndex = 6;
            this.checkEdit致残原因原因不明.TabStop = false;
            this.checkEdit致残原因原因不明.Tag = "7";
            // 
            // checkEdit致残原因其他
            // 
            this.checkEdit致残原因其他.Location = new System.Drawing.Point(421, 0);
            this.checkEdit致残原因其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit致残原因其他.Name = "checkEdit致残原因其他";
            this.checkEdit致残原因其他.Properties.Caption = "其他";
            this.checkEdit致残原因其他.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit致残原因其他.Properties.RadioGroupIndex = 0;
            this.checkEdit致残原因其他.Size = new System.Drawing.Size(52, 19);
            this.checkEdit致残原因其他.TabIndex = 7;
            this.checkEdit致残原因其他.TabStop = false;
            this.checkEdit致残原因其他.Tag = "99";
            // 
            // textEdit致残原因其他
            // 
            this.textEdit致残原因其他.Location = new System.Drawing.Point(0, 19);
            this.textEdit致残原因其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit致残原因其他.Name = "textEdit致残原因其他";
            this.textEdit致残原因其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit致残原因其他.TabIndex = 8;
            // 
            // textEdit残疾证号
            // 
            this.textEdit残疾证号.Location = new System.Drawing.Point(422, -200);
            this.textEdit残疾证号.Name = "textEdit残疾证号";
            this.textEdit残疾证号.Size = new System.Drawing.Size(306, 20);
            this.textEdit残疾证号.StyleController = this.layoutControl1;
            this.textEdit残疾证号.TabIndex = 13;
            // 
            // labelControl婚姻状况
            // 
            this.labelControl婚姻状况.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl婚姻状况.Location = new System.Drawing.Point(462, -224);
            this.labelControl婚姻状况.Name = "labelControl婚姻状况";
            this.labelControl婚姻状况.Size = new System.Drawing.Size(266, 20);
            this.labelControl婚姻状况.StyleController = this.layoutControl1;
            this.labelControl婚姻状况.TabIndex = 11;
            this.labelControl婚姻状况.Text = " ";
            // 
            // labelControl居住地址
            // 
            this.labelControl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl居住地址.Location = new System.Drawing.Point(98, -224);
            this.labelControl居住地址.Name = "labelControl居住地址";
            this.labelControl居住地址.Size = new System.Drawing.Size(265, 20);
            this.labelControl居住地址.StyleController = this.layoutControl1;
            this.labelControl居住地址.TabIndex = 10;
            this.labelControl居住地址.Text = " ";
            // 
            // labelControl联系电话
            // 
            this.labelControl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl联系电话.Location = new System.Drawing.Point(462, -248);
            this.labelControl联系电话.Name = "labelControl联系电话";
            this.labelControl联系电话.Size = new System.Drawing.Size(266, 20);
            this.labelControl联系电话.StyleController = this.layoutControl1;
            this.labelControl联系电话.TabIndex = 9;
            this.labelControl联系电话.Text = " ";
            // 
            // labelControl出生日期
            // 
            this.labelControl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl出生日期.Location = new System.Drawing.Point(98, -248);
            this.labelControl出生日期.Name = "labelControl出生日期";
            this.labelControl出生日期.Size = new System.Drawing.Size(265, 20);
            this.labelControl出生日期.StyleController = this.layoutControl1;
            this.labelControl出生日期.TabIndex = 8;
            this.labelControl出生日期.Text = " ";
            // 
            // labelControl身份证号
            // 
            this.labelControl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl身份证号.Location = new System.Drawing.Point(462, -272);
            this.labelControl身份证号.Name = "labelControl身份证号";
            this.labelControl身份证号.Size = new System.Drawing.Size(266, 20);
            this.labelControl身份证号.StyleController = this.layoutControl1;
            this.labelControl身份证号.TabIndex = 7;
            this.labelControl身份证号.Text = " ";
            // 
            // labelControl性别
            // 
            this.labelControl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl性别.Location = new System.Drawing.Point(98, -272);
            this.labelControl性别.Name = "labelControl性别";
            this.labelControl性别.Size = new System.Drawing.Size(265, 20);
            this.labelControl性别.StyleController = this.layoutControl1;
            this.labelControl性别.TabIndex = 6;
            this.labelControl性别.Text = " ";
            // 
            // labelControl姓名
            // 
            this.labelControl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl姓名.Location = new System.Drawing.Point(462, -296);
            this.labelControl姓名.Name = "labelControl姓名";
            this.labelControl姓名.Size = new System.Drawing.Size(266, 20);
            this.labelControl姓名.StyleController = this.layoutControl1;
            this.labelControl姓名.TabIndex = 5;
            this.labelControl姓名.Text = " ";
            // 
            // labelControl个人档案号
            // 
            this.labelControl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl个人档案号.Location = new System.Drawing.Point(98, -296);
            this.labelControl个人档案号.Name = "labelControl个人档案号";
            this.labelControl个人档案号.Size = new System.Drawing.Size(265, 20);
            this.labelControl个人档案号.StyleController = this.layoutControl1;
            this.labelControl个人档案号.TabIndex = 4;
            this.labelControl个人档案号.Text = " ";
            // 
            // dte残疾时间
            // 
            this.dte残疾时间.EditValue = null;
            this.dte残疾时间.Location = new System.Drawing.Point(88, -200);
            this.dte残疾时间.Name = "dte残疾时间";
            this.dte残疾时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte残疾时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte残疾时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte残疾时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte残疾时间.Size = new System.Drawing.Size(235, 20);
            this.dte残疾时间.StyleController = this.layoutControl1;
            this.dte残疾时间.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "听力言语残疾人健康管理随访表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -326);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 792);
            this.layoutControlGroup1.Text = "肢体残疾人健康管理随访表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 96);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.labelControl婚姻状况;
            this.layoutControlItem8.CustomizationFormText = "婚姻状况";
            this.layoutControlItem8.Location = new System.Drawing.Point(364, 72);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "婚姻状况";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.labelControl居住地址;
            this.layoutControlItem7.CustomizationFormText = "居住地址 ";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "居住地址 ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.labelControl出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期 ";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期 ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.labelControl性别;
            this.layoutControlItem3.CustomizationFormText = "性别 ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别 ";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.labelControl个人档案号;
            this.layoutControlItem1.CustomizationFormText = "个人档案号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "个人档案号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.labelControl姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(364, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.labelControl身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(364, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.labelControl联系电话;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(364, 48);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(71, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "联系电话";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.emptySpaceItem2,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.emptySpaceItem3,
            this.layoutControlItem18,
            this.emptySpaceItem4,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.emptySpaceItem5,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.emptySpaceItem9,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 96);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 595);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit残疾证号;
            this.layoutControlItem10.CustomizationFormText = "残疾证号";
            this.layoutControlItem10.Location = new System.Drawing.Point(324, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(405, 25);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "残疾证号";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.dte残疾时间;
            this.layoutControlItem9.CustomizationFormText = "致残时间";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(324, 25);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "致残时间";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "残疾情况";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 25);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(81, 120);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "残疾情况";
            this.emptySpaceItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.flow致残原因;
            this.layoutControlItem12.CustomizationFormText = "致残原因";
            this.layoutControlItem12.Location = new System.Drawing.Point(81, 25);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 44);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(180, 44);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(648, 44);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "致残原因";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.radio残疾程度;
            this.layoutControlItem11.CustomizationFormText = "残疾程度";
            this.layoutControlItem11.Location = new System.Drawing.Point(81, 69);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "残疾程度";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.radio持续时间;
            this.layoutControlItem13.CustomizationFormText = "持续时间";
            this.layoutControlItem13.Location = new System.Drawing.Point(81, 93);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(648, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "持续时间";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.flow其他伴随残疾;
            this.layoutControlItem14.CustomizationFormText = "其它伴随残疾";
            this.layoutControlItem14.Location = new System.Drawing.Point(81, 117);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(180, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(648, 28);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "其它伴随残疾";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.radio个人自理;
            this.layoutControlItem15.CustomizationFormText = "个人自理";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 145);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(400, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "个人自理";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "监护人";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "监护人";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.textEdit监护人姓名;
            this.layoutControlItem16.CustomizationFormText = "姓名";
            this.layoutControlItem16.Location = new System.Drawing.Point(80, 169);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(337, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "姓名";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.textEdit监护人联系电话;
            this.layoutControlItem17.CustomizationFormText = "联系电话";
            this.layoutControlItem17.Location = new System.Drawing.Point(417, 169);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "联系电话";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "文化程度";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 193);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(80, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "文化程度";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.flowLayoutPanel4;
            this.layoutControlItem18.CustomizationFormText = "学历";
            this.layoutControlItem18.Location = new System.Drawing.Point(80, 193);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "学历";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "就业情况";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 217);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(80, 74);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "就业情况";
            this.emptySpaceItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.flowLayoutPanel5;
            this.layoutControlItem19.CustomizationFormText = "就业状况";
            this.layoutControlItem19.Location = new System.Drawing.Point(80, 217);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(187, 50);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(649, 50);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "就业状况";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.textEdit工作单位;
            this.layoutControlItem20.CustomizationFormText = "工作单位 ";
            this.layoutControlItem20.Location = new System.Drawing.Point(80, 267);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(325, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "工作单位 ";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.textEdit电话;
            this.layoutControlItem21.CustomizationFormText = "电话 ";
            this.layoutControlItem21.Location = new System.Drawing.Point(405, 267);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "电话 ";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(86, 14);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "个人收入 ";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 291);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(80, 53);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "个人收入 ";
            this.emptySpaceItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.flow收入来源;
            this.layoutControlItem22.CustomizationFormText = "收入来源";
            this.layoutControlItem22.Location = new System.Drawing.Point(80, 291);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(187, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "收入来源";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.radio平均收入;
            this.layoutControlItem23.CustomizationFormText = "平均收入";
            this.layoutControlItem23.Location = new System.Drawing.Point(80, 315);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(137, 29);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(649, 29);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "平均收入";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.memoEdit康复治疗情况;
            this.layoutControlItem29.CustomizationFormText = "康复治疗情况";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 447);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(69, 50);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "康复治疗情况";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.memoEdit康复需求;
            this.layoutControlItem30.CustomizationFormText = "康复需求";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 497);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(69, 50);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "康复需求";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.dateEdit本次随访日期;
            this.layoutControlItem31.CustomizationFormText = "本次随访日期*";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 547);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem31.Text = "本次随访日期*";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit随访医生;
            this.layoutControlItem32.CustomizationFormText = "随访医生";
            this.layoutControlItem32.Location = new System.Drawing.Point(364, 547);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem32.Text = "随访医生";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.dateEdit下次随访日期;
            this.layoutControlItem33.CustomizationFormText = "下次随访日期*";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 571);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem33.Text = "下次随访日期*";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(86, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.labelControl录入医生;
            this.layoutControlItem34.CustomizationFormText = "录入医生 ";
            this.layoutControlItem34.Location = new System.Drawing.Point(364, 571);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem34.Text = "录入医生 ";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "劳动技能";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 344);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(80, 53);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "劳动技能";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(86, 0);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.radio劳动能力;
            this.layoutControlItem41.CustomizationFormText = "劳动能力";
            this.layoutControlItem41.Location = new System.Drawing.Point(80, 344);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(279, 29);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "劳动能力";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.radio劳动技能;
            this.layoutControlItem42.CustomizationFormText = "劳动技能";
            this.layoutControlItem42.Location = new System.Drawing.Point(359, 344);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(370, 29);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "劳动技能";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem43.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.flow能力来源;
            this.layoutControlItem43.CustomizationFormText = "能力来源";
            this.layoutControlItem43.Location = new System.Drawing.Point(80, 373);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(193, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "能力来源";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem44.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.memoEdit伤残部位;
            this.layoutControlItem44.CustomizationFormText = "伤残部位";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 397);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(99, 50);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "伤残部位";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem35,
            this.layoutControlItem38,
            this.layoutControlItem40,
            this.layoutControlItem39,
            this.layoutControlItem37,
            this.layoutControlItem36});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 691);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(729, 72);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.labelControl最近更新时间;
            this.layoutControlItem35.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem35.Location = new System.Drawing.Point(318, 0);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "最近更新时间:";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.labelControl创建机构;
            this.layoutControlItem38.CustomizationFormText = "创建机构: ";
            this.layoutControlItem38.Location = new System.Drawing.Point(318, 24);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "创建机构: ";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.labelControl最近修改人;
            this.layoutControlItem40.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem40.Location = new System.Drawing.Point(318, 48);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "最近修改人: ";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.labelControl创建人;
            this.layoutControlItem39.CustomizationFormText = "创建人: ";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "创建人: ";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.labelControl当前所属机构;
            this.layoutControlItem37.CustomizationFormText = "当前所属机构: ";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "当前所属机构: ";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.labelControl创建时间;
            this.layoutControlItem36.CustomizationFormText = "创建时间: ";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "创建时间: ";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // UC肢体残疾人健康管理随访表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC肢体残疾人健康管理随访表";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC肢体残疾人健康管理随访表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit伤残部位.Properties)).EndInit();
            this.flow能力来源.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit公办培训机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit民办培训机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自学.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit他人辅导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit能力来源其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit能力来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio劳动技能.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio劳动能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit本次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit本次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit康复需求.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit康复治疗情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio平均收入.Properties)).EndInit();
            this.flow收入来源.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit工资.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit分红.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit房地.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit社会救济.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit收入来源其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit收入来源其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit工作单位.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio就业状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit就业状况其他.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit盲校.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit聋校.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他特殊学校.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit学历其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit监护人联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit监护人姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio个人自理.Properties)).EndInit();
            this.flow其他伴随残疾.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾智力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio伴随残疾智力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾肢体.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio伴随残疾脑瘫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伴随残疾精神.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio持续时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).EndInit();
            this.flow致残原因.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因遗传.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因生长发育.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因事故.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因药物中毒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因老年.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因原因不明.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit致残原因其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit致残原因其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit残疾证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte残疾时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte残疾时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl婚姻状况;
        private DevExpress.XtraEditors.LabelControl labelControl居住地址;
        private DevExpress.XtraEditors.LabelControl labelControl联系电话;
        private DevExpress.XtraEditors.LabelControl labelControl出生日期;
        private DevExpress.XtraEditors.LabelControl labelControl身份证号;
        private DevExpress.XtraEditors.LabelControl labelControl性别;
        private DevExpress.XtraEditors.LabelControl labelControl姓名;
        private DevExpress.XtraEditors.LabelControl labelControl个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit textEdit残疾证号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup radio持续时间;
        private DevExpress.XtraEditors.RadioGroup radio残疾程度;
        private System.Windows.Forms.FlowLayoutPanel flow致残原因;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因遗传;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因生长发育;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因疾病;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因事故;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因药物中毒;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因老年;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因原因不明;
        private DevExpress.XtraEditors.CheckEdit checkEdit致残原因其他;
        private DevExpress.XtraEditors.TextEdit textEdit致残原因其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.FlowLayoutPanel flow其他伴随残疾;
        private DevExpress.XtraEditors.CheckEdit checkEdit伴随残疾无;
        private DevExpress.XtraEditors.CheckEdit checkEdit伴随残疾智力;
        private DevExpress.XtraEditors.CheckEdit checkEdit伴随残疾肢体;
        private DevExpress.XtraEditors.CheckEdit checkEdit伴随残疾精神;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.RadioGroup radio个人自理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit textEdit监护人联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit监护人姓名;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.CheckEdit checkEdit盲校;
        private Library.UserControls.UCLblTxt ucLblTxt盲校年;
        private DevExpress.XtraEditors.CheckEdit checkEdit聋校;
        private Library.UserControls.UCLblTxt ucLblTxt聋校年;
        private DevExpress.XtraEditors.CheckEdit checkEdit其他特殊学校;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.TextEdit textEdit学历其他;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.RadioGroup radio就业状况;
        private DevExpress.XtraEditors.TextEdit textEdit就业状况其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.RadioGroup radio平均收入;
        private System.Windows.Forms.FlowLayoutPanel flow收入来源;
        private DevExpress.XtraEditors.CheckEdit checkEdit工资;
        private DevExpress.XtraEditors.CheckEdit checkEdit分红;
        private DevExpress.XtraEditors.CheckEdit checkEdit房地;
        private DevExpress.XtraEditors.CheckEdit checkEdit社会救济;
        private DevExpress.XtraEditors.CheckEdit checkEdit收入来源其他;
        private DevExpress.XtraEditors.TextEdit textEdit收入来源其他;
        private DevExpress.XtraEditors.TextEdit textEdit电话;
        private DevExpress.XtraEditors.TextEdit textEdit工作单位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.MemoEdit memoEdit康复治疗情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.MemoEdit memoEdit康复需求;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.LabelControl labelControl录入医生;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.DateEdit dateEdit本次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.LabelControl labelControl最近修改人;
        private DevExpress.XtraEditors.LabelControl labelControl创建人;
        private DevExpress.XtraEditors.LabelControl labelControl创建机构;
        private DevExpress.XtraEditors.LabelControl labelControl当前所属机构;
        private DevExpress.XtraEditors.LabelControl labelControl创建时间;
        private DevExpress.XtraEditors.LabelControl labelControl最近更新时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.FlowLayoutPanel flow能力来源;
        private DevExpress.XtraEditors.CheckEdit checkEdit公办培训机构;
        private DevExpress.XtraEditors.CheckEdit checkEdit民办培训机构;
        private DevExpress.XtraEditors.CheckEdit checkEdit自学;
        private DevExpress.XtraEditors.CheckEdit checkEdit他人辅导;
        private DevExpress.XtraEditors.CheckEdit checkEdit能力来源其他;
        private DevExpress.XtraEditors.TextEdit textEdit能力来源;
        private DevExpress.XtraEditors.RadioGroup radio劳动技能;
        private DevExpress.XtraEditors.RadioGroup radio劳动能力;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraEditors.RadioGroup radio伴随残疾智力;
        private DevExpress.XtraEditors.RadioGroup radio伴随残疾脑瘫;
        private DevExpress.XtraEditors.DateEdit dte残疾时间;
        private DevExpress.XtraEditors.MemoEdit memoEdit伤残部位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
    }
}
