﻿namespace AtomEHR.公共卫生.Module.个人健康.残疾人健康信息
{
    partial class UC残疾人康复服务随访记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC残疾人康复服务随访记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gc用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gv用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit心率 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit转介去向 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转介原因 = new DevExpress.XtraEditors.TextEdit();
            this.flow康复服务情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk康复服务_医疗康复 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_功能训练 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_辅助器具 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_心理服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_知识普及 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_转介服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复服务_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit康复服务其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit功能训练2 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit体检其他 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl性别 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.radio主要残疾 = new DevExpress.XtraEditors.RadioGroup();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.radio训练效果 = new DevExpress.XtraEditors.RadioGroup();
            this.radio遵医行为 = new DevExpress.XtraEditors.RadioGroup();
            this.flow康复目标 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk康复目标_运动 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_感知 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_认知 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_交往 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_自理 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_适应 = new DevExpress.XtraEditors.CheckEdit();
            this.chk康复目标_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit康复目标其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit训练评估分数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit功能训练1 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.flow多重残疾 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk多重残疾_否 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_视力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_听力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_言语残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_肢体残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_智力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk多重残疾_精神残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.radio训练场地 = new DevExpress.XtraEditors.RadioGroup();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.radio随访分类 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit服务对象或家属 = new DevExpress.XtraEditors.TextEdit();
            this.radio残疾程度 = new DevExpress.XtraEditors.RadioGroup();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Group康复服务 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.个人档案号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.性别 = new DevExpress.XtraLayout.LayoutControlItem();
            this.出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.身份证号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.联系电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.居住地址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.婚姻状况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.创建时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.当前所属机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.创建人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.最近更新时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.创建机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.最近修改人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介去向.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介原因.Properties)).BeginInit();
            this.flow康复服务情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_医疗康复.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_功能训练.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_辅助器具.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_心理服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_知识普及.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_转介服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复服务其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练效果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio遵医行为.Properties)).BeginInit();
            this.flow康复目标.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_运动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_感知.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_认知.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_交往.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_自理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_适应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复目标其他.Properties)).BeginInit();
            this.flow多重残疾.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_否.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_视力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_听力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_言语残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_肢体残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_智力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_精神残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练场地.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit服务对象或家属.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group康复服务)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.个人档案号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.性别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.身份证号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.联系电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.居住地址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.婚姻状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.当前所属机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.最近更新时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.最近修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(743, 32);
            this.panelControl1.TabIndex = 3;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(739, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gc用药情况);
            this.layoutControl1.Controls.Add(this.textEdit心率);
            this.layoutControl1.Controls.Add(this.textEdit体重);
            this.layoutControl1.Controls.Add(this.textEdit转介去向);
            this.layoutControl1.Controls.Add(this.textEdit转介原因);
            this.layoutControl1.Controls.Add(this.flow康复服务情况);
            this.layoutControl1.Controls.Add(this.textEdit功能训练2);
            this.layoutControl1.Controls.Add(this.textEdit血压);
            this.layoutControl1.Controls.Add(this.textEdit体检其他);
            this.layoutControl1.Controls.Add(this.labelControl最近修改人);
            this.layoutControl1.Controls.Add(this.labelControl创建人);
            this.layoutControl1.Controls.Add(this.labelControl创建机构);
            this.layoutControl1.Controls.Add(this.labelControl当前所属机构);
            this.layoutControl1.Controls.Add(this.labelControl最近更新时间);
            this.layoutControl1.Controls.Add(this.labelControl创建时间);
            this.layoutControl1.Controls.Add(this.labelControl婚姻状况);
            this.layoutControl1.Controls.Add(this.labelControl联系电话);
            this.layoutControl1.Controls.Add(this.labelControl出生日期);
            this.layoutControl1.Controls.Add(this.labelControl身份证号);
            this.layoutControl1.Controls.Add(this.labelControl性别);
            this.layoutControl1.Controls.Add(this.labelControl姓名);
            this.layoutControl1.Controls.Add(this.labelControl居住地址);
            this.layoutControl1.Controls.Add(this.labelControl个人档案号);
            this.layoutControl1.Controls.Add(this.radio主要残疾);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.radio训练效果);
            this.layoutControl1.Controls.Add(this.radio遵医行为);
            this.layoutControl1.Controls.Add(this.flow康复目标);
            this.layoutControl1.Controls.Add(this.textEdit训练评估分数);
            this.layoutControl1.Controls.Add(this.textEdit功能训练1);
            this.layoutControl1.Controls.Add(this.flow多重残疾);
            this.layoutControl1.Controls.Add(this.radio训练场地);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.radio随访分类);
            this.layoutControl1.Controls.Add(this.textEdit服务对象或家属);
            this.layoutControl1.Controls.Add(this.radio残疾程度);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(743, 500);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gc用药情况
            // 
            this.gc用药情况.Location = new System.Drawing.Point(128, 719);
            this.gc用药情况.MainView = this.gv用药情况;
            this.gc用药情况.Name = "gc用药情况";
            this.gc用药情况.Size = new System.Drawing.Size(595, 60);
            this.gc用药情况.TabIndex = 97;
            this.gc用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv用药情况});
            // 
            // gv用药情况
            // 
            this.gv用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gv用药情况.GridControl = this.gc用药情况;
            this.gv用药情况.Name = "gv用药情况";
            this.gv用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "用法";
            this.gridColumn2.FieldName = "用法";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // textEdit心率
            // 
            this.textEdit心率.Lbl1Size = new System.Drawing.Size(18, 14);
            this.textEdit心率.Lbl1Text = "/";
            this.textEdit心率.Lbl2Size = new System.Drawing.Size(18, 14);
            this.textEdit心率.Lbl2Text = "";
            this.textEdit心率.Location = new System.Drawing.Point(135, 294);
            this.textEdit心率.Name = "textEdit心率";
            this.textEdit心率.Size = new System.Drawing.Size(211, 20);
            this.textEdit心率.TabIndex = 96;
            this.textEdit心率.Tag = "1";
            this.textEdit心率.Txt1EditValue = 0;
            this.textEdit心率.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit心率.Txt2EditValue = 0;
            this.textEdit心率.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // textEdit体重
            // 
            this.textEdit体重.Lbl1Size = new System.Drawing.Size(30, 18);
            this.textEdit体重.Lbl1Text = "kg";
            this.textEdit体重.Location = new System.Drawing.Point(470, 270);
            this.textEdit体重.Name = "textEdit体重";
            this.textEdit体重.Size = new System.Drawing.Size(241, 20);
            this.textEdit体重.TabIndex = 95;
            this.textEdit体重.Tag = "1";
            this.textEdit体重.Txt1Size = new System.Drawing.Size(200, 20);
            // 
            // textEdit转介去向
            // 
            this.textEdit转介去向.Location = new System.Drawing.Point(135, 496);
            this.textEdit转介去向.Name = "textEdit转介去向";
            this.textEdit转介去向.Size = new System.Drawing.Size(576, 20);
            this.textEdit转介去向.StyleController = this.layoutControl1;
            this.textEdit转介去向.TabIndex = 94;
            // 
            // textEdit转介原因
            // 
            this.textEdit转介原因.Location = new System.Drawing.Point(135, 472);
            this.textEdit转介原因.Name = "textEdit转介原因";
            this.textEdit转介原因.Size = new System.Drawing.Size(576, 20);
            this.textEdit转介原因.StyleController = this.layoutControl1;
            this.textEdit转介原因.TabIndex = 93;
            // 
            // flow康复服务情况
            // 
            this.flow康复服务情况.Controls.Add(this.chk康复服务_医疗康复);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_功能训练);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_辅助器具);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_心理服务);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_知识普及);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_转介服务);
            this.flow康复服务情况.Controls.Add(this.chk康复服务_其他);
            this.flow康复服务情况.Controls.Add(this.textEdit康复服务其他);
            this.flow康复服务情况.Location = new System.Drawing.Point(15, 367);
            this.flow康复服务情况.Name = "flow康复服务情况";
            this.flow康复服务情况.Size = new System.Drawing.Size(696, 52);
            this.flow康复服务情况.TabIndex = 91;
            this.flow康复服务情况.Tag = "99";
            // 
            // chk康复服务_医疗康复
            // 
            this.chk康复服务_医疗康复.Location = new System.Drawing.Point(3, 3);
            this.chk康复服务_医疗康复.Name = "chk康复服务_医疗康复";
            this.chk康复服务_医疗康复.Properties.Caption = "医疗康复";
            this.chk康复服务_医疗康复.Size = new System.Drawing.Size(98, 19);
            this.chk康复服务_医疗康复.TabIndex = 1;
            this.chk康复服务_医疗康复.Tag = "1";
            // 
            // chk康复服务_功能训练
            // 
            this.chk康复服务_功能训练.Location = new System.Drawing.Point(107, 3);
            this.chk康复服务_功能训练.Name = "chk康复服务_功能训练";
            this.chk康复服务_功能训练.Properties.Caption = "功能训练";
            this.chk康复服务_功能训练.Size = new System.Drawing.Size(91, 19);
            this.chk康复服务_功能训练.TabIndex = 2;
            this.chk康复服务_功能训练.Tag = "2";
            // 
            // chk康复服务_辅助器具
            // 
            this.chk康复服务_辅助器具.Location = new System.Drawing.Point(204, 3);
            this.chk康复服务_辅助器具.Name = "chk康复服务_辅助器具";
            this.chk康复服务_辅助器具.Properties.Caption = "辅助器具";
            this.chk康复服务_辅助器具.Size = new System.Drawing.Size(97, 19);
            this.chk康复服务_辅助器具.TabIndex = 3;
            this.chk康复服务_辅助器具.Tag = "3";
            // 
            // chk康复服务_心理服务
            // 
            this.chk康复服务_心理服务.Location = new System.Drawing.Point(307, 3);
            this.chk康复服务_心理服务.Name = "chk康复服务_心理服务";
            this.chk康复服务_心理服务.Properties.Caption = "心理服务";
            this.chk康复服务_心理服务.Size = new System.Drawing.Size(95, 19);
            this.chk康复服务_心理服务.TabIndex = 4;
            this.chk康复服务_心理服务.Tag = "4";
            // 
            // chk康复服务_知识普及
            // 
            this.chk康复服务_知识普及.Location = new System.Drawing.Point(408, 3);
            this.chk康复服务_知识普及.Name = "chk康复服务_知识普及";
            this.chk康复服务_知识普及.Properties.Caption = "知识普及";
            this.chk康复服务_知识普及.Size = new System.Drawing.Size(106, 19);
            this.chk康复服务_知识普及.TabIndex = 5;
            this.chk康复服务_知识普及.Tag = "5";
            // 
            // chk康复服务_转介服务
            // 
            this.chk康复服务_转介服务.Location = new System.Drawing.Point(520, 3);
            this.chk康复服务_转介服务.Name = "chk康复服务_转介服务";
            this.chk康复服务_转介服务.Properties.Caption = "转介服务";
            this.chk康复服务_转介服务.Size = new System.Drawing.Size(107, 19);
            this.chk康复服务_转介服务.TabIndex = 6;
            this.chk康复服务_转介服务.Tag = "6";
            // 
            // chk康复服务_其他
            // 
            this.chk康复服务_其他.Location = new System.Drawing.Point(3, 28);
            this.chk康复服务_其他.Name = "chk康复服务_其他";
            this.chk康复服务_其他.Properties.Caption = "其他";
            this.chk康复服务_其他.Size = new System.Drawing.Size(62, 19);
            this.chk康复服务_其他.TabIndex = 7;
            this.chk康复服务_其他.Tag = "99";
            // 
            // textEdit康复服务其他
            // 
            this.textEdit康复服务其他.Location = new System.Drawing.Point(71, 28);
            this.textEdit康复服务其他.Name = "textEdit康复服务其他";
            this.textEdit康复服务其他.Size = new System.Drawing.Size(211, 20);
            this.textEdit康复服务其他.TabIndex = 8;
            // 
            // textEdit功能训练2
            // 
            this.textEdit功能训练2.Lbl1Size = new System.Drawing.Size(30, 14);
            this.textEdit功能训练2.Lbl1Text = "次/月";
            this.textEdit功能训练2.Lbl2Size = new System.Drawing.Size(40, 14);
            this.textEdit功能训练2.Lbl2Text = "分钟/次";
            this.textEdit功能训练2.Location = new System.Drawing.Point(365, 569);
            this.textEdit功能训练2.Name = "textEdit功能训练2";
            this.textEdit功能训练2.Size = new System.Drawing.Size(346, 20);
            this.textEdit功能训练2.TabIndex = 90;
            this.textEdit功能训练2.Txt1EditValue = null;
            this.textEdit功能训练2.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit功能训练2.Txt2EditValue = null;
            this.textEdit功能训练2.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // textEdit血压
            // 
            this.textEdit血压.Lbl1Size = new System.Drawing.Size(18, 14);
            this.textEdit血压.Lbl1Text = "/";
            this.textEdit血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.textEdit血压.Lbl2Text = "mmHg";
            this.textEdit血压.Location = new System.Drawing.Point(135, 270);
            this.textEdit血压.Name = "textEdit血压";
            this.textEdit血压.Size = new System.Drawing.Size(211, 20);
            this.textEdit血压.TabIndex = 0;
            this.textEdit血压.Tag = "1";
            this.textEdit血压.Txt1EditValue = 0;
            this.textEdit血压.Txt1Size = new System.Drawing.Size(70, 20);
            this.textEdit血压.Txt2EditValue = 0;
            this.textEdit血压.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // textEdit体检其他
            // 
            this.textEdit体检其他.Location = new System.Drawing.Point(470, 294);
            this.textEdit体检其他.Name = "textEdit体检其他";
            this.textEdit体检其他.Size = new System.Drawing.Size(241, 20);
            this.textEdit体检其他.StyleController = this.layoutControl1;
            this.textEdit体检其他.TabIndex = 9;
            // 
            // labelControl最近修改人
            // 
            this.labelControl最近修改人.Location = new System.Drawing.Point(448, 884);
            this.labelControl最近修改人.Name = "labelControl最近修改人";
            this.labelControl最近修改人.Size = new System.Drawing.Size(275, 20);
            this.labelControl最近修改人.StyleController = this.layoutControl1;
            this.labelControl最近修改人.TabIndex = 1;
            // 
            // labelControl创建人
            // 
            this.labelControl创建人.Location = new System.Drawing.Point(98, 884);
            this.labelControl创建人.Name = "labelControl创建人";
            this.labelControl创建人.Size = new System.Drawing.Size(251, 20);
            this.labelControl创建人.StyleController = this.layoutControl1;
            this.labelControl创建人.TabIndex = 1;
            // 
            // labelControl创建机构
            // 
            this.labelControl创建机构.Location = new System.Drawing.Point(448, 860);
            this.labelControl创建机构.Name = "labelControl创建机构";
            this.labelControl创建机构.Size = new System.Drawing.Size(275, 20);
            this.labelControl创建机构.StyleController = this.layoutControl1;
            this.labelControl创建机构.TabIndex = 1;
            // 
            // labelControl当前所属机构
            // 
            this.labelControl当前所属机构.Location = new System.Drawing.Point(98, 860);
            this.labelControl当前所属机构.Name = "labelControl当前所属机构";
            this.labelControl当前所属机构.Size = new System.Drawing.Size(251, 20);
            this.labelControl当前所属机构.StyleController = this.layoutControl1;
            this.labelControl当前所属机构.TabIndex = 1;
            // 
            // labelControl最近更新时间
            // 
            this.labelControl最近更新时间.Location = new System.Drawing.Point(448, 836);
            this.labelControl最近更新时间.Name = "labelControl最近更新时间";
            this.labelControl最近更新时间.Size = new System.Drawing.Size(275, 20);
            this.labelControl最近更新时间.StyleController = this.layoutControl1;
            this.labelControl最近更新时间.TabIndex = 1;
            // 
            // labelControl创建时间
            // 
            this.labelControl创建时间.Location = new System.Drawing.Point(98, 836);
            this.labelControl创建时间.Name = "labelControl创建时间";
            this.labelControl创建时间.Size = new System.Drawing.Size(251, 20);
            this.labelControl创建时间.StyleController = this.layoutControl1;
            this.labelControl创建时间.TabIndex = 0;
            // 
            // labelControl婚姻状况
            // 
            this.labelControl婚姻状况.Location = new System.Drawing.Point(464, 103);
            this.labelControl婚姻状况.Name = "labelControl婚姻状况";
            this.labelControl婚姻状况.Size = new System.Drawing.Size(259, 20);
            this.labelControl婚姻状况.StyleController = this.layoutControl1;
            this.labelControl婚姻状况.TabIndex = 89;
            // 
            // labelControl联系电话
            // 
            this.labelControl联系电话.Location = new System.Drawing.Point(464, 79);
            this.labelControl联系电话.Name = "labelControl联系电话";
            this.labelControl联系电话.Size = new System.Drawing.Size(259, 20);
            this.labelControl联系电话.StyleController = this.layoutControl1;
            this.labelControl联系电话.TabIndex = 87;
            // 
            // labelControl出生日期
            // 
            this.labelControl出生日期.Location = new System.Drawing.Point(98, 79);
            this.labelControl出生日期.Name = "labelControl出生日期";
            this.labelControl出生日期.Size = new System.Drawing.Size(267, 20);
            this.labelControl出生日期.StyleController = this.layoutControl1;
            this.labelControl出生日期.TabIndex = 86;
            // 
            // labelControl身份证号
            // 
            this.labelControl身份证号.Location = new System.Drawing.Point(464, 55);
            this.labelControl身份证号.Name = "labelControl身份证号";
            this.labelControl身份证号.Size = new System.Drawing.Size(259, 20);
            this.labelControl身份证号.StyleController = this.layoutControl1;
            this.labelControl身份证号.TabIndex = 85;
            // 
            // labelControl性别
            // 
            this.labelControl性别.Location = new System.Drawing.Point(98, 55);
            this.labelControl性别.Name = "labelControl性别";
            this.labelControl性别.Size = new System.Drawing.Size(267, 20);
            this.labelControl性别.StyleController = this.layoutControl1;
            this.labelControl性别.TabIndex = 84;
            // 
            // labelControl姓名
            // 
            this.labelControl姓名.Location = new System.Drawing.Point(464, 31);
            this.labelControl姓名.Name = "labelControl姓名";
            this.labelControl姓名.Size = new System.Drawing.Size(259, 20);
            this.labelControl姓名.StyleController = this.layoutControl1;
            this.labelControl姓名.TabIndex = 83;
            // 
            // labelControl居住地址
            // 
            this.labelControl居住地址.Location = new System.Drawing.Point(98, 103);
            this.labelControl居住地址.Name = "labelControl居住地址";
            this.labelControl居住地址.Size = new System.Drawing.Size(267, 20);
            this.labelControl居住地址.StyleController = this.layoutControl1;
            this.labelControl居住地址.TabIndex = 82;
            // 
            // labelControl个人档案号
            // 
            this.labelControl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl个人档案号.Location = new System.Drawing.Point(98, 31);
            this.labelControl个人档案号.Name = "labelControl个人档案号";
            this.labelControl个人档案号.Size = new System.Drawing.Size(267, 20);
            this.labelControl个人档案号.StyleController = this.layoutControl1;
            this.labelControl个人档案号.TabIndex = 81;
            // 
            // radio主要残疾
            // 
            this.radio主要残疾.Location = new System.Drawing.Point(123, 127);
            this.radio主要残疾.Name = "radio主要残疾";
            this.radio主要残疾.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "视力残疾"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "听力残疾"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "言语残疾"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "肢体残疾"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "智力残疾"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "精神残疾")});
            this.radio主要残疾.Size = new System.Drawing.Size(600, 25);
            this.radio主要残疾.StyleController = this.layoutControl1;
            this.radio主要残疾.TabIndex = 80;
            // 
            // radio随访方式
            // 
            this.radio随访方式.Location = new System.Drawing.Point(470, 204);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话")});
            this.radio随访方式.Size = new System.Drawing.Size(253, 25);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 79;
            // 
            // radio训练效果
            // 
            this.radio训练效果.Location = new System.Drawing.Point(135, 678);
            this.radio训练效果.Name = "radio训练效果";
            this.radio训练效果.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "显效"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有效"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "无效")});
            this.radio训练效果.Size = new System.Drawing.Size(217, 25);
            this.radio训练效果.StyleController = this.layoutControl1;
            this.radio训练效果.TabIndex = 78;
            // 
            // radio遵医行为
            // 
            this.radio遵医行为.Location = new System.Drawing.Point(476, 678);
            this.radio遵医行为.Margin = new System.Windows.Forms.Padding(0);
            this.radio遵医行为.Name = "radio遵医行为";
            this.radio遵医行为.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "差")});
            this.radio遵医行为.Size = new System.Drawing.Size(235, 25);
            this.radio遵医行为.StyleController = this.layoutControl1;
            this.radio遵医行为.TabIndex = 0;
            // 
            // flow康复目标
            // 
            this.flow康复目标.Controls.Add(this.chk康复目标_运动);
            this.flow康复目标.Controls.Add(this.chk康复目标_感知);
            this.flow康复目标.Controls.Add(this.chk康复目标_认知);
            this.flow康复目标.Controls.Add(this.chk康复目标_交往);
            this.flow康复目标.Controls.Add(this.chk康复目标_自理);
            this.flow康复目标.Controls.Add(this.chk康复目标_适应);
            this.flow康复目标.Controls.Add(this.chk康复目标_其他);
            this.flow康复目标.Controls.Add(this.textEdit康复目标其他);
            this.flow康复目标.Location = new System.Drawing.Point(137, 622);
            this.flow康复目标.Name = "flow康复目标";
            this.flow康复目标.Size = new System.Drawing.Size(574, 52);
            this.flow康复目标.TabIndex = 62;
            // 
            // chk康复目标_运动
            // 
            this.chk康复目标_运动.Location = new System.Drawing.Point(3, 3);
            this.chk康复目标_运动.Name = "chk康复目标_运动";
            this.chk康复目标_运动.Properties.Caption = "运动能力改善";
            this.chk康复目标_运动.Size = new System.Drawing.Size(102, 19);
            this.chk康复目标_运动.TabIndex = 0;
            this.chk康复目标_运动.Tag = "1";
            // 
            // chk康复目标_感知
            // 
            this.chk康复目标_感知.Location = new System.Drawing.Point(111, 3);
            this.chk康复目标_感知.Name = "chk康复目标_感知";
            this.chk康复目标_感知.Properties.Caption = "感知能力提高";
            this.chk康复目标_感知.Size = new System.Drawing.Size(101, 19);
            this.chk康复目标_感知.TabIndex = 1;
            this.chk康复目标_感知.Tag = "2";
            // 
            // chk康复目标_认知
            // 
            this.chk康复目标_认知.Location = new System.Drawing.Point(218, 3);
            this.chk康复目标_认知.Name = "chk康复目标_认知";
            this.chk康复目标_认知.Properties.Caption = "认知能力提高";
            this.chk康复目标_认知.Size = new System.Drawing.Size(97, 19);
            this.chk康复目标_认知.TabIndex = 2;
            this.chk康复目标_认知.Tag = "3";
            // 
            // chk康复目标_交往
            // 
            this.chk康复目标_交往.Location = new System.Drawing.Point(321, 3);
            this.chk康复目标_交往.Name = "chk康复目标_交往";
            this.chk康复目标_交往.Properties.Caption = "交往能力提高";
            this.chk康复目标_交往.Size = new System.Drawing.Size(96, 19);
            this.chk康复目标_交往.TabIndex = 3;
            this.chk康复目标_交往.Tag = "4";
            // 
            // chk康复目标_自理
            // 
            this.chk康复目标_自理.Location = new System.Drawing.Point(423, 3);
            this.chk康复目标_自理.Name = "chk康复目标_自理";
            this.chk康复目标_自理.Properties.Caption = "自理能力提高";
            this.chk康复目标_自理.Size = new System.Drawing.Size(100, 19);
            this.chk康复目标_自理.TabIndex = 4;
            this.chk康复目标_自理.Tag = "5";
            // 
            // chk康复目标_适应
            // 
            this.chk康复目标_适应.Location = new System.Drawing.Point(3, 28);
            this.chk康复目标_适应.Name = "chk康复目标_适应";
            this.chk康复目标_适应.Properties.Caption = "适应能力提高";
            this.chk康复目标_适应.Size = new System.Drawing.Size(112, 19);
            this.chk康复目标_适应.TabIndex = 6;
            this.chk康复目标_适应.Tag = "6";
            // 
            // chk康复目标_其他
            // 
            this.chk康复目标_其他.Location = new System.Drawing.Point(121, 28);
            this.chk康复目标_其他.Name = "chk康复目标_其他";
            this.chk康复目标_其他.Properties.Caption = "其他";
            this.chk康复目标_其他.Size = new System.Drawing.Size(65, 19);
            this.chk康复目标_其他.TabIndex = 7;
            this.chk康复目标_其他.Tag = "99";
            // 
            // textEdit康复目标其他
            // 
            this.textEdit康复目标其他.Location = new System.Drawing.Point(192, 28);
            this.textEdit康复目标其他.Name = "textEdit康复目标其他";
            this.textEdit康复目标其他.Size = new System.Drawing.Size(250, 20);
            this.textEdit康复目标其他.TabIndex = 8;
            // 
            // textEdit训练评估分数
            // 
            this.textEdit训练评估分数.Lbl1Size = new System.Drawing.Size(75, 18);
            this.textEdit训练评估分数.Lbl1Text = "分";
            this.textEdit训练评估分数.Location = new System.Drawing.Point(476, 593);
            this.textEdit训练评估分数.Name = "textEdit训练评估分数";
            this.textEdit训练评估分数.Size = new System.Drawing.Size(235, 25);
            this.textEdit训练评估分数.TabIndex = 61;
            this.textEdit训练评估分数.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit功能训练1
            // 
            this.textEdit功能训练1.Lbl1Size = new System.Drawing.Size(30, 14);
            this.textEdit功能训练1.Lbl1Text = "次/月";
            this.textEdit功能训练1.Lbl2Size = new System.Drawing.Size(40, 14);
            this.textEdit功能训练1.Lbl2Text = "分钟/次";
            this.textEdit功能训练1.Location = new System.Drawing.Point(135, 569);
            this.textEdit功能训练1.Name = "textEdit功能训练1";
            this.textEdit功能训练1.Size = new System.Drawing.Size(226, 20);
            this.textEdit功能训练1.TabIndex = 57;
            this.textEdit功能训练1.Txt1EditValue = null;
            this.textEdit功能训练1.Txt1Size = new System.Drawing.Size(58, 20);
            this.textEdit功能训练1.Txt2EditValue = null;
            this.textEdit功能训练1.Txt2Size = new System.Drawing.Size(57, 20);
            // 
            // flow多重残疾
            // 
            this.flow多重残疾.Controls.Add(this.chk多重残疾_否);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_视力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_听力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_言语残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_肢体残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_智力残疾);
            this.flow多重残疾.Controls.Add(this.chk多重残疾_精神残疾);
            this.flow多重残疾.Location = new System.Drawing.Point(123, 156);
            this.flow多重残疾.Name = "flow多重残疾";
            this.flow多重残疾.Size = new System.Drawing.Size(600, 20);
            this.flow多重残疾.TabIndex = 47;
            // 
            // chk多重残疾_否
            // 
            this.chk多重残疾_否.Location = new System.Drawing.Point(3, 3);
            this.chk多重残疾_否.Name = "chk多重残疾_否";
            this.chk多重残疾_否.Properties.Caption = "否";
            this.chk多重残疾_否.Size = new System.Drawing.Size(48, 19);
            this.chk多重残疾_否.TabIndex = 6;
            this.chk多重残疾_否.Tag = "1";
            this.chk多重残疾_否.CheckedChanged += new System.EventHandler(this.chk多重残疾_否_CheckedChanged);
            // 
            // chk多重残疾_视力残疾
            // 
            this.chk多重残疾_视力残疾.Location = new System.Drawing.Point(57, 3);
            this.chk多重残疾_视力残疾.Name = "chk多重残疾_视力残疾";
            this.chk多重残疾_视力残疾.Properties.Caption = "视力残疾";
            this.chk多重残疾_视力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_视力残疾.TabIndex = 7;
            this.chk多重残疾_视力残疾.Tag = "2";
            // 
            // chk多重残疾_听力残疾
            // 
            this.chk多重残疾_听力残疾.Location = new System.Drawing.Point(138, 3);
            this.chk多重残疾_听力残疾.Name = "chk多重残疾_听力残疾";
            this.chk多重残疾_听力残疾.Properties.Caption = "听力残疾";
            this.chk多重残疾_听力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_听力残疾.TabIndex = 8;
            this.chk多重残疾_听力残疾.Tag = "3";
            // 
            // chk多重残疾_言语残疾
            // 
            this.chk多重残疾_言语残疾.Location = new System.Drawing.Point(219, 3);
            this.chk多重残疾_言语残疾.Name = "chk多重残疾_言语残疾";
            this.chk多重残疾_言语残疾.Properties.Caption = "言语残疾";
            this.chk多重残疾_言语残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_言语残疾.TabIndex = 9;
            this.chk多重残疾_言语残疾.Tag = "4";
            // 
            // chk多重残疾_肢体残疾
            // 
            this.chk多重残疾_肢体残疾.Location = new System.Drawing.Point(300, 3);
            this.chk多重残疾_肢体残疾.Name = "chk多重残疾_肢体残疾";
            this.chk多重残疾_肢体残疾.Properties.Caption = "肢体残疾";
            this.chk多重残疾_肢体残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_肢体残疾.TabIndex = 10;
            this.chk多重残疾_肢体残疾.Tag = "5";
            // 
            // chk多重残疾_智力残疾
            // 
            this.chk多重残疾_智力残疾.Location = new System.Drawing.Point(381, 3);
            this.chk多重残疾_智力残疾.Name = "chk多重残疾_智力残疾";
            this.chk多重残疾_智力残疾.Properties.Caption = "智力残疾";
            this.chk多重残疾_智力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_智力残疾.TabIndex = 11;
            this.chk多重残疾_智力残疾.Tag = "6";
            // 
            // chk多重残疾_精神残疾
            // 
            this.chk多重残疾_精神残疾.Location = new System.Drawing.Point(462, 3);
            this.chk多重残疾_精神残疾.Name = "chk多重残疾_精神残疾";
            this.chk多重残疾_精神残疾.Properties.Caption = "精神残疾";
            this.chk多重残疾_精神残疾.Size = new System.Drawing.Size(75, 19);
            this.chk多重残疾_精神残疾.TabIndex = 12;
            this.chk多重残疾_精神残疾.Tag = "7";
            // 
            // radio训练场地
            // 
            this.radio训练场地.Location = new System.Drawing.Point(137, 593);
            this.radio训练场地.Name = "radio训练场地";
            this.radio训练场地.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "社区"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭")});
            this.radio训练场地.Size = new System.Drawing.Size(215, 25);
            this.radio训练场地.StyleController = this.layoutControl1;
            this.radio训练场地.TabIndex = 44;
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(475, 783);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(248, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 36;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(475, 812);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(248, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 35;
            // 
            // radio随访分类
            // 
            this.radio随访分类.Location = new System.Drawing.Point(125, 783);
            this.radio随访分类.Name = "radio随访分类";
            this.radio随访分类.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "不满意")});
            this.radio随访分类.Size = new System.Drawing.Size(226, 25);
            this.radio随访分类.StyleController = this.layoutControl1;
            this.radio随访分类.TabIndex = 26;
            // 
            // textEdit服务对象或家属
            // 
            this.textEdit服务对象或家属.Location = new System.Drawing.Point(123, 812);
            this.textEdit服务对象或家属.Name = "textEdit服务对象或家属";
            this.textEdit服务对象或家属.Size = new System.Drawing.Size(228, 20);
            this.textEdit服务对象或家属.StyleController = this.layoutControl1;
            this.textEdit服务对象或家属.TabIndex = 25;
            // 
            // radio残疾程度
            // 
            this.radio残疾程度.Location = new System.Drawing.Point(125, 180);
            this.radio残疾程度.Name = "radio残疾程度";
            this.radio残疾程度.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "一级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "二级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "三级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "四级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "未评定")});
            this.radio残疾程度.Size = new System.Drawing.Size(598, 20);
            this.radio残疾程度.StyleController = this.layoutControl1;
            this.radio残疾程度.TabIndex = 16;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(125, 204);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit随访日期.Size = new System.Drawing.Size(221, 20);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "听力言语残疾人健康管理随访表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(726, 908);
            this.layoutControlGroup1.Text = "残疾人康复服务随访记录表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(724, 1);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem6,
            this.layoutControlItem11,
            this.layoutControlGroup6,
            this.Group康复服务,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlItem23,
            this.layoutControlItem32,
            this.layoutControlItem21,
            this.layoutControlItem33,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.个人档案号,
            this.姓名,
            this.性别,
            this.出生日期,
            this.身份证号,
            this.联系电话,
            this.居住地址,
            this.婚姻状况,
            this.创建时间,
            this.当前所属机构,
            this.创建人,
            this.最近更新时间,
            this.创建机构,
            this.最近修改人,
            this.layoutControlItem12});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 1);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(724, 877);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.dateEdit随访日期;
            this.layoutControlItem9.CustomizationFormText = "随访日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(347, 29);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "随访日期";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.flow多重残疾;
            this.layoutControlItem6.CustomizationFormText = "多重残疾";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 125);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(724, 24);
            this.layoutControlItem6.Text = "多重残疾";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.radio残疾程度;
            this.layoutControlItem11.CustomizationFormText = "残疾程度";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 149);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(724, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "残疾程度";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup6.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup6.CustomizationFormText = "体检";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem,
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 202);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(724, 97);
            this.layoutControlGroup6.Text = "体检";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.textEdit体检其他;
            this.layoutControlItem18.CustomizationFormText = "其他";
            this.layoutControlItem18.Location = new System.Drawing.Point(335, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem18.Text = "其他";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem
            // 
            this.layoutControlItem.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem.Control = this.textEdit血压;
            this.layoutControlItem.CustomizationFormText = "血压";
            this.layoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem.Name = "layoutControlItem";
            this.layoutControlItem.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem.Text = "血压";
            this.layoutControlItem.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit体重;
            this.layoutControlItem7.CustomizationFormText = "体重";
            this.layoutControlItem7.Location = new System.Drawing.Point(335, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem7.Text = "体重";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit心率;
            this.layoutControlItem8.CustomizationFormText = "心率";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem8.Text = "心率";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(117, 14);
            // 
            // Group康复服务
            // 
            this.Group康复服务.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Group康复服务.AppearanceGroup.Options.UseFont = true;
            this.Group康复服务.AppearanceGroup.Options.UseTextOptions = true;
            this.Group康复服务.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Group康复服务.CustomizationFormText = "康复服务情况";
            this.Group康复服务.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.Group康复服务.Location = new System.Drawing.Point(0, 299);
            this.Group康复服务.Name = "Group康复服务";
            this.Group康复服务.Size = new System.Drawing.Size(724, 105);
            this.Group康复服务.Text = "康复服务情况";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.flow康复服务情况;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(224, 56);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(700, 56);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup7.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup7.CustomizationFormText = "转介服务";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem20});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 404);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(724, 97);
            this.layoutControlGroup7.Text = "转介服务";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit转介原因;
            this.layoutControlItem13.CustomizationFormText = "原因";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(700, 24);
            this.layoutControlItem13.Text = "原因";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.textEdit转介去向;
            this.layoutControlItem20.CustomizationFormText = "转介去向";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(700, 24);
            this.layoutControlItem20.Text = "转介去向";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup8.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup8.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup8.CustomizationFormText = "功能训练情况(肢体残疾及智障儿童功能训练者填)";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem50,
            this.layoutControlItem36,
            this.layoutControlItem26,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem41,
            this.layoutControlItem10});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 501);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(724, 187);
            this.layoutControlGroup8.Text = "功能训练情况(肢体残疾及智障儿童功能训练者填)";
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.textEdit功能训练1;
            this.layoutControlItem50.CustomizationFormText = "功能训练";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem50.Text = "功能训练";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.textEdit训练评估分数;
            this.layoutControlItem36.CustomizationFormText = "训练评估分数";
            this.layoutControlItem36.Location = new System.Drawing.Point(341, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(359, 29);
            this.layoutControlItem36.Text = "训练评估分数";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.flow康复目标;
            this.layoutControlItem26.CustomizationFormText = "康复目标";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(189, 56);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(700, 56);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "康复目标";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.radio训练效果;
            this.layoutControlItem2.CustomizationFormText = "训练效果";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 109);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(341, 29);
            this.layoutControlItem2.Text = "训练效果";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.radio遵医行为;
            this.layoutControlItem1.CustomizationFormText = "遵医行为";
            this.layoutControlItem1.Location = new System.Drawing.Point(341, 109);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(359, 29);
            this.layoutControlItem1.Text = "遵医行为";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.radio训练场地;
            this.layoutControlItem41.CustomizationFormText = "训练场地";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(341, 29);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "训练场地";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit功能训练2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.radio随访分类;
            this.layoutControlItem23.CustomizationFormText = "随访分类";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 752);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(137, 29);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(352, 29);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "此次随访分类";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(117, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit随访医生;
            this.layoutControlItem32.CustomizationFormText = "随访医生";
            this.layoutControlItem32.Location = new System.Drawing.Point(352, 781);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(372, 24);
            this.layoutControlItem32.Text = "随访医生签名";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.textEdit服务对象或家属;
            this.layoutControlItem21.CustomizationFormText = "服务对象或家属";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 781);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "服务对象或家属签名";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.dateEdit下次随访日期;
            this.layoutControlItem33.CustomizationFormText = "下次随访日期";
            this.layoutControlItem33.Location = new System.Drawing.Point(352, 752);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(372, 29);
            this.layoutControlItem33.Text = "下次随访日期";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.radio随访方式;
            this.layoutControlItem4.CustomizationFormText = "随访方式";
            this.layoutControlItem4.Location = new System.Drawing.Point(347, 173);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(377, 29);
            this.layoutControlItem4.Text = "随访方式";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(117, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.radio主要残疾;
            this.layoutControlItem5.CustomizationFormText = "主要残疾";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(724, 29);
            this.layoutControlItem5.Text = "主要残疾";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(117, 14);
            // 
            // 个人档案号
            // 
            this.个人档案号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.个人档案号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.个人档案号.Control = this.labelControl个人档案号;
            this.个人档案号.CustomizationFormText = "layoutControlItem6";
            this.个人档案号.Location = new System.Drawing.Point(0, 0);
            this.个人档案号.MaxSize = new System.Drawing.Size(0, 24);
            this.个人档案号.MinSize = new System.Drawing.Size(71, 24);
            this.个人档案号.Name = "个人档案号";
            this.个人档案号.Size = new System.Drawing.Size(366, 24);
            this.个人档案号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.个人档案号.Text = "个人档案号";
            this.个人档案号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.个人档案号.TextSize = new System.Drawing.Size(90, 20);
            this.个人档案号.TextToControlDistance = 5;
            // 
            // 姓名
            // 
            this.姓名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.姓名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.姓名.Control = this.labelControl姓名;
            this.姓名.CustomizationFormText = "姓名";
            this.姓名.Location = new System.Drawing.Point(366, 0);
            this.姓名.MaxSize = new System.Drawing.Size(0, 24);
            this.姓名.MinSize = new System.Drawing.Size(71, 24);
            this.姓名.Name = "姓名";
            this.姓名.Size = new System.Drawing.Size(358, 24);
            this.姓名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.姓名.Text = "姓名";
            this.姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.姓名.TextSize = new System.Drawing.Size(90, 20);
            this.姓名.TextToControlDistance = 5;
            // 
            // 性别
            // 
            this.性别.AppearanceItemCaption.Options.UseTextOptions = true;
            this.性别.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.性别.Control = this.labelControl性别;
            this.性别.CustomizationFormText = "性别";
            this.性别.Location = new System.Drawing.Point(0, 24);
            this.性别.MaxSize = new System.Drawing.Size(0, 24);
            this.性别.MinSize = new System.Drawing.Size(71, 24);
            this.性别.Name = "性别";
            this.性别.Size = new System.Drawing.Size(366, 24);
            this.性别.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.性别.Text = "性别";
            this.性别.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.性别.TextSize = new System.Drawing.Size(90, 20);
            this.性别.TextToControlDistance = 5;
            // 
            // 出生日期
            // 
            this.出生日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.出生日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.出生日期.Control = this.labelControl出生日期;
            this.出生日期.CustomizationFormText = "出生日期";
            this.出生日期.Location = new System.Drawing.Point(0, 48);
            this.出生日期.MaxSize = new System.Drawing.Size(0, 24);
            this.出生日期.MinSize = new System.Drawing.Size(71, 24);
            this.出生日期.Name = "出生日期";
            this.出生日期.Size = new System.Drawing.Size(366, 24);
            this.出生日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.出生日期.Text = "出生日期";
            this.出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.出生日期.TextSize = new System.Drawing.Size(90, 20);
            this.出生日期.TextToControlDistance = 5;
            // 
            // 身份证号
            // 
            this.身份证号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.身份证号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.身份证号.Control = this.labelControl身份证号;
            this.身份证号.CustomizationFormText = "身份证号";
            this.身份证号.Location = new System.Drawing.Point(366, 24);
            this.身份证号.MaxSize = new System.Drawing.Size(0, 24);
            this.身份证号.MinSize = new System.Drawing.Size(71, 24);
            this.身份证号.Name = "身份证号";
            this.身份证号.Size = new System.Drawing.Size(358, 24);
            this.身份证号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.身份证号.Text = "身份证号";
            this.身份证号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.身份证号.TextSize = new System.Drawing.Size(90, 20);
            this.身份证号.TextToControlDistance = 5;
            // 
            // 联系电话
            // 
            this.联系电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.联系电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.联系电话.Control = this.labelControl联系电话;
            this.联系电话.CustomizationFormText = "联系电话";
            this.联系电话.Location = new System.Drawing.Point(366, 48);
            this.联系电话.MaxSize = new System.Drawing.Size(0, 24);
            this.联系电话.MinSize = new System.Drawing.Size(71, 24);
            this.联系电话.Name = "联系电话";
            this.联系电话.Size = new System.Drawing.Size(358, 24);
            this.联系电话.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.联系电话.Text = "联系电话";
            this.联系电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.联系电话.TextSize = new System.Drawing.Size(90, 20);
            this.联系电话.TextToControlDistance = 5;
            // 
            // 居住地址
            // 
            this.居住地址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.居住地址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.居住地址.Control = this.labelControl居住地址;
            this.居住地址.CustomizationFormText = "居住地址";
            this.居住地址.Location = new System.Drawing.Point(0, 72);
            this.居住地址.MaxSize = new System.Drawing.Size(0, 24);
            this.居住地址.MinSize = new System.Drawing.Size(71, 24);
            this.居住地址.Name = "居住地址";
            this.居住地址.Size = new System.Drawing.Size(366, 24);
            this.居住地址.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.居住地址.Text = "居住地址";
            this.居住地址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.居住地址.TextSize = new System.Drawing.Size(90, 20);
            this.居住地址.TextToControlDistance = 5;
            // 
            // 婚姻状况
            // 
            this.婚姻状况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.婚姻状况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.婚姻状况.Control = this.labelControl婚姻状况;
            this.婚姻状况.CustomizationFormText = "婚姻状况";
            this.婚姻状况.Location = new System.Drawing.Point(366, 72);
            this.婚姻状况.MaxSize = new System.Drawing.Size(0, 24);
            this.婚姻状况.MinSize = new System.Drawing.Size(71, 24);
            this.婚姻状况.Name = "婚姻状况";
            this.婚姻状况.Size = new System.Drawing.Size(358, 24);
            this.婚姻状况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.婚姻状况.Text = "婚姻状况";
            this.婚姻状况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.婚姻状况.TextSize = new System.Drawing.Size(90, 20);
            this.婚姻状况.TextToControlDistance = 5;
            // 
            // 创建时间
            // 
            this.创建时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.创建时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.创建时间.Control = this.labelControl创建时间;
            this.创建时间.CustomizationFormText = "创建时间";
            this.创建时间.Location = new System.Drawing.Point(0, 805);
            this.创建时间.MaxSize = new System.Drawing.Size(0, 24);
            this.创建时间.MinSize = new System.Drawing.Size(71, 24);
            this.创建时间.Name = "创建时间";
            this.创建时间.Size = new System.Drawing.Size(350, 24);
            this.创建时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.创建时间.Text = "创建时间:";
            this.创建时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.创建时间.TextSize = new System.Drawing.Size(90, 20);
            this.创建时间.TextToControlDistance = 5;
            // 
            // 当前所属机构
            // 
            this.当前所属机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.当前所属机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.当前所属机构.Control = this.labelControl当前所属机构;
            this.当前所属机构.CustomizationFormText = "所属机构";
            this.当前所属机构.Location = new System.Drawing.Point(0, 829);
            this.当前所属机构.MaxSize = new System.Drawing.Size(0, 24);
            this.当前所属机构.MinSize = new System.Drawing.Size(71, 24);
            this.当前所属机构.Name = "当前所属机构";
            this.当前所属机构.Size = new System.Drawing.Size(350, 24);
            this.当前所属机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.当前所属机构.Text = "当前所属机构:";
            this.当前所属机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.当前所属机构.TextSize = new System.Drawing.Size(90, 20);
            this.当前所属机构.TextToControlDistance = 5;
            // 
            // 创建人
            // 
            this.创建人.AppearanceItemCaption.Options.UseTextOptions = true;
            this.创建人.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.创建人.Control = this.labelControl创建人;
            this.创建人.CustomizationFormText = "创建人";
            this.创建人.Location = new System.Drawing.Point(0, 853);
            this.创建人.MaxSize = new System.Drawing.Size(0, 24);
            this.创建人.MinSize = new System.Drawing.Size(71, 24);
            this.创建人.Name = "创建人";
            this.创建人.Size = new System.Drawing.Size(350, 24);
            this.创建人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.创建人.Text = "创建人:";
            this.创建人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.创建人.TextSize = new System.Drawing.Size(90, 20);
            this.创建人.TextToControlDistance = 5;
            // 
            // 最近更新时间
            // 
            this.最近更新时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.最近更新时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.最近更新时间.Control = this.labelControl最近更新时间;
            this.最近更新时间.CustomizationFormText = "更新时间";
            this.最近更新时间.Location = new System.Drawing.Point(350, 805);
            this.最近更新时间.MaxSize = new System.Drawing.Size(0, 24);
            this.最近更新时间.MinSize = new System.Drawing.Size(71, 24);
            this.最近更新时间.Name = "最近更新时间";
            this.最近更新时间.Size = new System.Drawing.Size(374, 24);
            this.最近更新时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.最近更新时间.Text = "最近更新时间:";
            this.最近更新时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.最近更新时间.TextSize = new System.Drawing.Size(90, 20);
            this.最近更新时间.TextToControlDistance = 5;
            // 
            // 创建机构
            // 
            this.创建机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.创建机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.创建机构.Control = this.labelControl创建机构;
            this.创建机构.CustomizationFormText = "创建机构";
            this.创建机构.Location = new System.Drawing.Point(350, 829);
            this.创建机构.MaxSize = new System.Drawing.Size(0, 24);
            this.创建机构.MinSize = new System.Drawing.Size(71, 24);
            this.创建机构.Name = "创建机构";
            this.创建机构.Size = new System.Drawing.Size(374, 24);
            this.创建机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.创建机构.Text = "创建机构:";
            this.创建机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.创建机构.TextSize = new System.Drawing.Size(90, 20);
            this.创建机构.TextToControlDistance = 5;
            // 
            // 最近修改人
            // 
            this.最近修改人.AppearanceItemCaption.Options.UseTextOptions = true;
            this.最近修改人.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.最近修改人.Control = this.labelControl最近修改人;
            this.最近修改人.CustomizationFormText = "最近修改人";
            this.最近修改人.Location = new System.Drawing.Point(350, 853);
            this.最近修改人.MaxSize = new System.Drawing.Size(0, 24);
            this.最近修改人.MinSize = new System.Drawing.Size(71, 24);
            this.最近修改人.Name = "最近修改人";
            this.最近修改人.Size = new System.Drawing.Size(374, 24);
            this.最近修改人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.最近修改人.Text = "最近修改人:";
            this.最近修改人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.最近修改人.TextSize = new System.Drawing.Size(90, 20);
            this.最近修改人.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.gc用药情况;
            this.layoutControlItem12.CustomizationFormText = "用药";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 688);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(724, 64);
            this.layoutControlItem12.Text = "用药";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(120, 60);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 878);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(724, 1);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem53.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.radio训练场地;
            this.layoutControlItem53.CustomizationFormText = "劳动能力";
            this.layoutControlItem53.Location = new System.Drawing.Point(352, 0);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(143, 29);
            this.layoutControlItem53.Name = "layoutControlItem41";
            this.layoutControlItem53.Size = new System.Drawing.Size(353, 29);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "训练场地";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // UC残疾人康复服务随访记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC残疾人康复服务随访记录表";
            this.Size = new System.Drawing.Size(743, 532);
            this.Load += new System.EventHandler(this.UC残疾人康复服务随访记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介去向.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转介原因.Properties)).EndInit();
            this.flow康复服务情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_医疗康复.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_功能训练.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_辅助器具.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_心理服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_知识普及.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_转介服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复服务_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复服务其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体检其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练效果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio遵医行为.Properties)).EndInit();
            this.flow康复目标.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_运动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_感知.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_认知.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_交往.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_自理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_适应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk康复目标_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复目标其他.Properties)).EndInit();
            this.flow多重残疾.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_否.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_视力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_听力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_言语残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_肢体残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_智力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk多重残疾_精神残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio训练场地.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit服务对象或家属.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio残疾程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group康复服务)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.个人档案号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.性别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.身份证号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.联系电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.居住地址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.婚姻状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.当前所属机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.最近更新时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.创建机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.最近修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.RadioGroup radio残疾程度;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.RadioGroup radio遵医行为;
        private DevExpress.XtraEditors.RadioGroup radio随访分类;
        private DevExpress.XtraEditors.TextEdit textEdit服务对象或家属;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.RadioGroup radio训练场地;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private System.Windows.Forms.FlowLayoutPanel flow多重残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_否;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_视力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_听力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_言语残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_肢体残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_智力残疾;
        private DevExpress.XtraEditors.CheckEdit chk多重残疾_精神残疾;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_医疗康复;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_功能训练;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_辅助器具;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_心理服务;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_知识普及;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_转介服务;
        private Library.UserControls.UCTxtLblTxtLbl textEdit血压;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup Group康复服务;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private System.Windows.Forms.FlowLayoutPanel flow康复目标;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_运动;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_感知;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_认知;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_交往;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_自理;
        private Library.UserControls.UCTxtLbl textEdit训练评估分数;
        private Library.UserControls.UCTxtLblTxtLbl textEdit功能训练1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraEditors.LabelControl labelControl最近修改人;
        private DevExpress.XtraEditors.LabelControl labelControl创建人;
        private DevExpress.XtraEditors.LabelControl labelControl创建机构;
        private DevExpress.XtraEditors.LabelControl labelControl当前所属机构;
        private DevExpress.XtraEditors.LabelControl labelControl最近更新时间;
        private DevExpress.XtraEditors.LabelControl labelControl创建时间;
        private DevExpress.XtraEditors.TextEdit textEdit体检其他;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_适应;
        private DevExpress.XtraEditors.RadioGroup radio训练效果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.RadioGroup radio主要残疾;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LabelControl labelControl个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem 个人档案号;
        private DevExpress.XtraEditors.LabelControl labelControl居住地址;
        private DevExpress.XtraLayout.LayoutControlItem 居住地址;
        private DevExpress.XtraEditors.LabelControl labelControl姓名;
        private DevExpress.XtraLayout.LayoutControlItem 姓名;
        private DevExpress.XtraEditors.LabelControl labelControl婚姻状况;
        private DevExpress.XtraEditors.LabelControl labelControl联系电话;
        private DevExpress.XtraEditors.LabelControl labelControl出生日期;
        private DevExpress.XtraEditors.LabelControl labelControl身份证号;
        private DevExpress.XtraEditors.LabelControl labelControl性别;
        private DevExpress.XtraLayout.LayoutControlItem 性别;
        private DevExpress.XtraLayout.LayoutControlItem 出生日期;
        private DevExpress.XtraLayout.LayoutControlItem 身份证号;
        private DevExpress.XtraLayout.LayoutControlItem 联系电话;
        private DevExpress.XtraLayout.LayoutControlItem 婚姻状况;
        private DevExpress.XtraLayout.LayoutControlItem 创建时间;
        private DevExpress.XtraLayout.LayoutControlItem 当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem 创建人;
        private DevExpress.XtraLayout.LayoutControlItem 最近更新时间;
        private DevExpress.XtraLayout.LayoutControlItem 创建机构;
        private DevExpress.XtraLayout.LayoutControlItem 最近修改人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem;
        private Library.UserControls.UCTxtLblTxtLbl textEdit功能训练2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.TextEdit textEdit转介去向;
        private DevExpress.XtraEditors.TextEdit textEdit转介原因;
        private System.Windows.Forms.FlowLayoutPanel flow康复服务情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.CheckEdit chk康复服务_其他;
        private DevExpress.XtraEditors.TextEdit textEdit康复服务其他;
        private DevExpress.XtraEditors.CheckEdit chk康复目标_其他;
        private DevExpress.XtraEditors.TextEdit textEdit康复目标其他;
        private Library.UserControls.UCTxtLbl textEdit体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private Library.UserControls.UCTxtLblTxtLbl textEdit心率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.GridControl gc用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gv用药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}
