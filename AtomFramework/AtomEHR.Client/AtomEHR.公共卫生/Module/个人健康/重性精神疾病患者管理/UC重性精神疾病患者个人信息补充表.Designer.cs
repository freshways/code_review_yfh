﻿namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    partial class UC重性精神疾病患者个人信息补充表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC重性精神疾病患者个人信息补充表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit档案类别 = new DevExpress.XtraEditors.TextEdit();
            this.flow影响 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk影响无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk影响1 = new DevExpress.XtraEditors.CheckEdit();
            this.uc轻度滋事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk影响2 = new DevExpress.XtraEditors.CheckEdit();
            this.uc肇事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk影响3 = new DevExpress.XtraEditors.CheckEdit();
            this.uc肇祸 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk影响4 = new DevExpress.XtraEditors.CheckEdit();
            this.uc自伤 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk影响5 = new DevExpress.XtraEditors.CheckEdit();
            this.uc自杀未遂 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.dte确诊日期 = new AtomEHR.Library.UserControls.UCLblDtp();
            this.txt确诊医院 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio门诊 = new DevExpress.XtraEditors.RadioGroup();
            this.dte首次治疗时间 = new AtomEHR.Library.UserControls.UCLblDtp();
            this.txt诊断 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt医生签字 = new DevExpress.XtraEditors.TextEdit();
            this.radio关锁情况 = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txt住院此次 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.dte知情同意签字时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt知情同意签字 = new DevExpress.XtraEditors.TextEdit();
            this.cbo知情同意 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt联系人电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt监护人电话 = new DevExpress.XtraEditors.TextEdit();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.radio经济状况 = new DevExpress.XtraEditors.RadioGroup();
            this.dte检查日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio最后一次治疗效果 = new DevExpress.XtraEditors.RadioGroup();
            this.flow既往主要症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk6 = new DevExpress.XtraEditors.CheckEdit();
            this.chk7 = new DevExpress.XtraEditors.CheckEdit();
            this.chk8 = new DevExpress.XtraEditors.CheckEdit();
            this.chk9 = new DevExpress.XtraEditors.CheckEdit();
            this.chk10 = new DevExpress.XtraEditors.CheckEdit();
            this.chk11 = new DevExpress.XtraEditors.CheckEdit();
            this.chk12 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt监护人地址 = new DevExpress.XtraEditors.TextEdit();
            this.lbl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lbl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl个人档案编号 = new DevExpress.XtraEditors.LabelControl();
            this.txt监护人姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo与患者关系 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte初次发病时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt医生意见 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt患者签名 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.chk_其他危害行为 = new DevExpress.XtraEditors.CheckEdit();
            this.uc其他危害行为 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.radioGroup就业情况 = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案类别.Properties)).BeginInit();
            this.flow影响.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响5.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio门诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio关锁情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte知情同意签字时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte知情同意签字时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt知情同意签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo知情同意.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio经济状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio最后一次治疗效果.Properties)).BeginInit();
            this.flow既往主要症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与患者关系.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte初次发病时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte初次发病时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生意见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_其他危害行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup就业情况.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(3, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(82, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "修改保存";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn导出
            // 
            this.btn导出.Location = new System.Drawing.Point(91, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 1;
            this.btn导出.Text = "填表说明";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt患者签名);
            this.layoutControl1.Controls.Add(this.textEdit档案类别);
            this.layoutControl1.Controls.Add(this.flow影响);
            this.layoutControl1.Controls.Add(this.dte确诊日期);
            this.layoutControl1.Controls.Add(this.txt确诊医院);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.txt诊断);
            this.layoutControl1.Controls.Add(this.txt医生签字);
            this.layoutControl1.Controls.Add(this.radio关锁情况);
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Controls.Add(this.dte知情同意签字时间);
            this.layoutControl1.Controls.Add(this.txt知情同意签字);
            this.layoutControl1.Controls.Add(this.cbo知情同意);
            this.layoutControl1.Controls.Add(this.txt联系人电话);
            this.layoutControl1.Controls.Add(this.txt监护人电话);
            this.layoutControl1.Controls.Add(this.lbl当前所属机构);
            this.layoutControl1.Controls.Add(this.lbl创建机构);
            this.layoutControl1.Controls.Add(this.lbl最近更新人);
            this.layoutControl1.Controls.Add(this.lbl创建人);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.radio经济状况);
            this.layoutControl1.Controls.Add(this.dte检查日期);
            this.layoutControl1.Controls.Add(this.radio最后一次治疗效果);
            this.layoutControl1.Controls.Add(this.flow既往主要症状);
            this.layoutControl1.Controls.Add(this.txt监护人地址);
            this.layoutControl1.Controls.Add(this.lbl居住地址);
            this.layoutControl1.Controls.Add(this.lbl联系电话);
            this.layoutControl1.Controls.Add(this.lbl婚姻状况);
            this.layoutControl1.Controls.Add(this.lbl身份证号);
            this.layoutControl1.Controls.Add(this.lbl出生日期);
            this.layoutControl1.Controls.Add(this.lbl性别);
            this.layoutControl1.Controls.Add(this.lbl姓名);
            this.layoutControl1.Controls.Add(this.lbl个人档案编号);
            this.layoutControl1.Controls.Add(this.txt监护人姓名);
            this.layoutControl1.Controls.Add(this.cbo与患者关系);
            this.layoutControl1.Controls.Add(this.dte初次发病时间);
            this.layoutControl1.Controls.Add(this.txt医生意见);
            this.layoutControl1.Controls.Add(this.radioGroup就业情况);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(869, 298, 250, 350);
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 468);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit档案类别
            // 
            this.textEdit档案类别.Location = new System.Drawing.Point(465, 183);
            this.textEdit档案类别.Name = "textEdit档案类别";
            this.textEdit档案类别.Size = new System.Drawing.Size(265, 20);
            this.textEdit档案类别.StyleController = this.layoutControl1;
            this.textEdit档案类别.TabIndex = 52;
            // 
            // flow影响
            // 
            this.flow影响.Controls.Add(this.chk影响无);
            this.flow影响.Controls.Add(this.chk影响1);
            this.flow影响.Controls.Add(this.uc轻度滋事);
            this.flow影响.Controls.Add(this.chk影响2);
            this.flow影响.Controls.Add(this.uc肇事);
            this.flow影响.Controls.Add(this.chk影响3);
            this.flow影响.Controls.Add(this.uc肇祸);
            this.flow影响.Controls.Add(this.chk_其他危害行为);
            this.flow影响.Controls.Add(this.uc其他危害行为);
            this.flow影响.Controls.Add(this.chk影响4);
            this.flow影响.Controls.Add(this.uc自伤);
            this.flow影响.Controls.Add(this.chk影响5);
            this.flow影响.Controls.Add(this.uc自杀未遂);
            this.flow影响.Location = new System.Drawing.Point(118, 662);
            this.flow影响.Name = "flow影响";
            this.flow影响.Size = new System.Drawing.Size(612, 76);
            this.flow影响.TabIndex = 35;
            // 
            // chk影响无
            // 
            this.chk影响无.Location = new System.Drawing.Point(3, 3);
            this.chk影响无.Name = "chk影响无";
            this.chk影响无.Properties.Caption = "无";
            this.chk影响无.Size = new System.Drawing.Size(45, 19);
            this.chk影响无.TabIndex = 36;
            this.chk影响无.Tag = "6";
            this.chk影响无.CheckedChanged += new System.EventHandler(this.chk影响无_CheckedChanged);
            // 
            // chk影响1
            // 
            this.chk影响1.Enabled = false;
            this.chk影响1.Location = new System.Drawing.Point(54, 3);
            this.chk影响1.Name = "chk影响1";
            this.chk影响1.Properties.Caption = "";
            this.chk影响1.Size = new System.Drawing.Size(25, 19);
            this.chk影响1.TabIndex = 37;
            this.chk影响1.Tag = "1";
            this.chk影响1.CheckedChanged += new System.EventHandler(this.chk影响1_CheckedChanged);
            // 
            // uc轻度滋事
            // 
            this.uc轻度滋事.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc轻度滋事.Lbl1Text = "轻度滋事";
            this.uc轻度滋事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc轻度滋事.Lbl2Text = "次";
            this.uc轻度滋事.Location = new System.Drawing.Point(82, 0);
            this.uc轻度滋事.Margin = new System.Windows.Forms.Padding(0);
            this.uc轻度滋事.Name = "uc轻度滋事";
            this.uc轻度滋事.Size = new System.Drawing.Size(202, 22);
            this.uc轻度滋事.TabIndex = 38;
            this.uc轻度滋事.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // chk影响2
            // 
            this.chk影响2.Enabled = false;
            this.chk影响2.Location = new System.Drawing.Point(287, 3);
            this.chk影响2.Name = "chk影响2";
            this.chk影响2.Properties.Caption = "";
            this.chk影响2.Size = new System.Drawing.Size(25, 19);
            this.chk影响2.TabIndex = 39;
            this.chk影响2.Tag = "2";
            this.chk影响2.CheckedChanged += new System.EventHandler(this.chk影响2_CheckedChanged);
            // 
            // uc肇事
            // 
            this.uc肇事.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc肇事.Lbl1Text = "肇事";
            this.uc肇事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc肇事.Lbl2Text = "次";
            this.uc肇事.Location = new System.Drawing.Point(315, 0);
            this.uc肇事.Margin = new System.Windows.Forms.Padding(0);
            this.uc肇事.Name = "uc肇事";
            this.uc肇事.Size = new System.Drawing.Size(202, 22);
            this.uc肇事.TabIndex = 40;
            this.uc肇事.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // chk影响3
            // 
            this.chk影响3.Enabled = false;
            this.chk影响3.Location = new System.Drawing.Point(520, 3);
            this.chk影响3.Name = "chk影响3";
            this.chk影响3.Properties.Caption = "";
            this.chk影响3.Size = new System.Drawing.Size(25, 19);
            this.chk影响3.TabIndex = 41;
            this.chk影响3.Tag = "3";
            this.chk影响3.CheckedChanged += new System.EventHandler(this.chk影响3_CheckedChanged);
            // 
            // uc肇祸
            // 
            this.uc肇祸.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc肇祸.Lbl1Text = "肇祸";
            this.uc肇祸.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc肇祸.Lbl2Text = "次";
            this.uc肇祸.Location = new System.Drawing.Point(0, 25);
            this.uc肇祸.Margin = new System.Windows.Forms.Padding(0);
            this.uc肇祸.Name = "uc肇祸";
            this.uc肇祸.Size = new System.Drawing.Size(202, 22);
            this.uc肇祸.TabIndex = 42;
            this.uc肇祸.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // chk影响4
            // 
            this.chk影响4.Enabled = false;
            this.chk影响4.Location = new System.Drawing.Point(459, 28);
            this.chk影响4.Name = "chk影响4";
            this.chk影响4.Properties.Caption = "";
            this.chk影响4.Size = new System.Drawing.Size(25, 19);
            this.chk影响4.TabIndex = 43;
            this.chk影响4.Tag = "4";
            this.chk影响4.CheckedChanged += new System.EventHandler(this.chk影响4_CheckedChanged);
            // 
            // uc自伤
            // 
            this.uc自伤.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc自伤.Lbl1Text = "自伤";
            this.uc自伤.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc自伤.Lbl2Text = "次";
            this.uc自伤.Location = new System.Drawing.Point(0, 50);
            this.uc自伤.Margin = new System.Windows.Forms.Padding(0);
            this.uc自伤.Name = "uc自伤";
            this.uc自伤.Size = new System.Drawing.Size(202, 22);
            this.uc自伤.TabIndex = 44;
            this.uc自伤.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // chk影响5
            // 
            this.chk影响5.Enabled = false;
            this.chk影响5.Location = new System.Drawing.Point(205, 53);
            this.chk影响5.Name = "chk影响5";
            this.chk影响5.Properties.Caption = "";
            this.chk影响5.Size = new System.Drawing.Size(25, 19);
            this.chk影响5.TabIndex = 45;
            this.chk影响5.Tag = "5";
            this.chk影响5.CheckedChanged += new System.EventHandler(this.chk影响5_CheckedChanged);
            // 
            // uc自杀未遂
            // 
            this.uc自杀未遂.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc自杀未遂.Lbl1Text = "自杀未遂";
            this.uc自杀未遂.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc自杀未遂.Lbl2Text = "次";
            this.uc自杀未遂.Location = new System.Drawing.Point(233, 50);
            this.uc自杀未遂.Margin = new System.Windows.Forms.Padding(0);
            this.uc自杀未遂.Name = "uc自杀未遂";
            this.uc自杀未遂.Size = new System.Drawing.Size(202, 22);
            this.uc自杀未遂.TabIndex = 46;
            this.uc自杀未遂.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // dte确诊日期
            // 
            this.dte确诊日期.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dte确诊日期.Dtp1Size = new System.Drawing.Size(100, 20);
            this.dte确诊日期.Lbl1Size = new System.Drawing.Size(63, 16);
            this.dte确诊日期.Lbl1Text = "确诊日期：";
            this.dte确诊日期.Location = new System.Drawing.Point(124, 611);
            this.dte确诊日期.Name = "dte确诊日期";
            this.dte确诊日期.Size = new System.Drawing.Size(605, 20);
            this.dte确诊日期.TabIndex = 33;
            // 
            // txt确诊医院
            // 
            this.txt确诊医院.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt确诊医院.Lbl1Text = "确诊医院：";
            this.txt确诊医院.Location = new System.Drawing.Point(124, 587);
            this.txt确诊医院.Name = "txt确诊医院";
            this.txt确诊医院.Size = new System.Drawing.Size(605, 20);
            this.txt确诊医院.TabIndex = 32;
            this.txt确诊医院.Txt1Size = new System.Drawing.Size(200, 20);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.radio门诊);
            this.flowLayoutPanel2.Controls.Add(this.dte首次治疗时间);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(120, 452);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(610, 56);
            this.flowLayoutPanel2.TabIndex = 26;
            // 
            // radio门诊
            // 
            this.radio门诊.Location = new System.Drawing.Point(0, 0);
            this.radio门诊.Margin = new System.Windows.Forms.Padding(0);
            this.radio门诊.Name = "radio门诊";
            this.radio门诊.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1未治"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2间断门诊治疗"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3连续门诊治疗")});
            this.radio门诊.Size = new System.Drawing.Size(433, 22);
            this.radio门诊.TabIndex = 27;
            // 
            // dte首次治疗时间
            // 
            this.dte首次治疗时间.AutoSize = true;
            this.dte首次治疗时间.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dte首次治疗时间.Dtp1Size = new System.Drawing.Size(100, 20);
            this.dte首次治疗时间.Lbl1Size = new System.Drawing.Size(150, 16);
            this.dte首次治疗时间.Lbl1Text = "首次抗精神病药治疗时间";
            this.dte首次治疗时间.Location = new System.Drawing.Point(0, 22);
            this.dte首次治疗时间.Margin = new System.Windows.Forms.Padding(0);
            this.dte首次治疗时间.Name = "dte首次治疗时间";
            this.dte首次治疗时间.Size = new System.Drawing.Size(256, 22);
            this.dte首次治疗时间.TabIndex = 28;
            // 
            // txt诊断
            // 
            this.txt诊断.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt诊断.Lbl1Text = "诊断：";
            this.txt诊断.Location = new System.Drawing.Point(124, 563);
            this.txt诊断.Name = "txt诊断";
            this.txt诊断.Size = new System.Drawing.Size(605, 20);
            this.txt诊断.TabIndex = 31;
            this.txt诊断.Txt1Size = new System.Drawing.Size(200, 20);
            // 
            // txt医生签字
            // 
            this.txt医生签字.Location = new System.Drawing.Point(356, 816);
            this.txt医生签字.Name = "txt医生签字";
            this.txt医生签字.Size = new System.Drawing.Size(141, 20);
            this.txt医生签字.StyleController = this.layoutControl1;
            this.txt医生签字.TabIndex = 51;
            // 
            // radio关锁情况
            // 
            this.radio关锁情况.Location = new System.Drawing.Point(118, 423);
            this.radio关锁情况.Name = "radio关锁情况";
            this.radio关锁情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无关锁"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "关锁"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "关锁已解除")});
            this.radio关锁情况.Size = new System.Drawing.Size(281, 25);
            this.radio关锁情况.StyleController = this.layoutControl1;
            this.radio关锁情况.TabIndex = 47;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.txt住院此次);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Location = new System.Drawing.Point(120, 512);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(610, 46);
            this.panelControl3.TabIndex = 29;
            // 
            // txt住院此次
            // 
            this.txt住院此次.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt住院此次.Lbl1Text = "次";
            this.txt住院此次.Location = new System.Drawing.Point(231, 14);
            this.txt住院此次.Name = "txt住院此次";
            this.txt住院此次.Size = new System.Drawing.Size(181, 22);
            this.txt住院此次.TabIndex = 30;
            this.txt住院此次.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(9, 5);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(233, 36);
            this.labelControl10.TabIndex = 1;
            this.labelControl10.Text = "曾住精神专科医院/综合医院精神专科";
            // 
            // dte知情同意签字时间
            // 
            this.dte知情同意签字时间.EditValue = null;
            this.dte知情同意签字时间.Location = new System.Drawing.Point(211, 325);
            this.dte知情同意签字时间.Name = "dte知情同意签字时间";
            this.dte知情同意签字时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte知情同意签字时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte知情同意签字时间.Size = new System.Drawing.Size(155, 20);
            this.dte知情同意签字时间.StyleController = this.layoutControl1;
            this.dte知情同意签字时间.TabIndex = 11;
            // 
            // txt知情同意签字
            // 
            this.txt知情同意签字.Location = new System.Drawing.Point(211, 301);
            this.txt知情同意签字.Name = "txt知情同意签字";
            this.txt知情同意签字.Size = new System.Drawing.Size(155, 20);
            this.txt知情同意签字.StyleController = this.layoutControl1;
            this.txt知情同意签字.TabIndex = 10;
            // 
            // cbo知情同意
            // 
            this.cbo知情同意.Location = new System.Drawing.Point(120, 277);
            this.cbo知情同意.Name = "cbo知情同意";
            this.cbo知情同意.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo知情同意.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo知情同意.Size = new System.Drawing.Size(96, 20);
            this.cbo知情同意.StyleController = this.layoutControl1;
            this.cbo知情同意.TabIndex = 9;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.Location = new System.Drawing.Point(118, 183);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Size = new System.Drawing.Size(248, 20);
            this.txt联系人电话.StyleController = this.layoutControl1;
            this.txt联系人电话.TabIndex = 7;
            // 
            // txt监护人电话
            // 
            this.txt监护人电话.Location = new System.Drawing.Point(465, 159);
            this.txt监护人电话.Name = "txt监护人电话";
            this.txt监护人电话.Size = new System.Drawing.Size(265, 20);
            this.txt监护人电话.StyleController = this.layoutControl1;
            this.txt监护人电话.TabIndex = 6;
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Location = new System.Drawing.Point(98, 864);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(256, 20);
            this.lbl当前所属机构.StyleController = this.layoutControl1;
            this.lbl当前所属机构.TabIndex = 51;
            this.lbl当前所属机构.Text = " ";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(453, 864);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(277, 20);
            this.lbl创建机构.StyleController = this.layoutControl1;
            this.lbl创建机构.TabIndex = 50;
            this.lbl创建机构.Text = " ";
            // 
            // lbl最近更新人
            // 
            this.lbl最近更新人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新人.Location = new System.Drawing.Point(453, 888);
            this.lbl最近更新人.Name = "lbl最近更新人";
            this.lbl最近更新人.Size = new System.Drawing.Size(277, 20);
            this.lbl最近更新人.StyleController = this.layoutControl1;
            this.lbl最近更新人.TabIndex = 49;
            this.lbl最近更新人.Text = " ";
            // 
            // lbl创建人
            // 
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(98, 888);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(256, 20);
            this.lbl创建人.StyleController = this.layoutControl1;
            this.lbl创建人.TabIndex = 48;
            this.lbl创建人.Text = " ";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(453, 840);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(277, 20);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 47;
            this.lbl最近更新时间.Text = " ";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(98, 840);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(256, 20);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = " ";
            // 
            // radio经济状况
            // 
            this.radio经济状况.Location = new System.Drawing.Point(118, 742);
            this.radio经济状况.Name = "radio经济状况";
            this.radio经济状况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "贫困，在当地贫困线标准以下"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "非贫困"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "不详")});
            this.radio经济状况.Size = new System.Drawing.Size(612, 20);
            this.radio经济状况.StyleController = this.layoutControl1;
            this.radio经济状况.TabIndex = 48;
            // 
            // dte检查日期
            // 
            this.dte检查日期.EditValue = null;
            this.dte检查日期.Location = new System.Drawing.Point(118, 816);
            this.dte检查日期.Name = "dte检查日期";
            this.dte检查日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte检查日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte检查日期.Size = new System.Drawing.Size(119, 20);
            this.dte检查日期.StyleController = this.layoutControl1;
            this.dte检查日期.TabIndex = 50;
            // 
            // radio最后一次治疗效果
            // 
            this.radio最后一次治疗效果.Location = new System.Drawing.Point(118, 636);
            this.radio最后一次治疗效果.Name = "radio最后一次治疗效果";
            this.radio最后一次治疗效果.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1临床痊愈"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2好转"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3无变化"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "4加重")});
            this.radio最后一次治疗效果.Size = new System.Drawing.Size(381, 22);
            this.radio最后一次治疗效果.StyleController = this.layoutControl1;
            this.radio最后一次治疗效果.TabIndex = 34;
            // 
            // flow既往主要症状
            // 
            this.flow既往主要症状.Controls.Add(this.chk1);
            this.flow既往主要症状.Controls.Add(this.chk2);
            this.flow既往主要症状.Controls.Add(this.chk3);
            this.flow既往主要症状.Controls.Add(this.chk4);
            this.flow既往主要症状.Controls.Add(this.chk5);
            this.flow既往主要症状.Controls.Add(this.chk6);
            this.flow既往主要症状.Controls.Add(this.chk7);
            this.flow既往主要症状.Controls.Add(this.chk8);
            this.flow既往主要症状.Controls.Add(this.chk9);
            this.flow既往主要症状.Controls.Add(this.chk10);
            this.flow既往主要症状.Controls.Add(this.chk11);
            this.flow既往主要症状.Controls.Add(this.chk12);
            this.flow既往主要症状.Controls.Add(this.txt症状其他);
            this.flow既往主要症状.Location = new System.Drawing.Point(118, 373);
            this.flow既往主要症状.Name = "flow既往主要症状";
            this.flow既往主要症状.Size = new System.Drawing.Size(612, 46);
            this.flow既往主要症状.TabIndex = 12;
            // 
            // chk1
            // 
            this.chk1.Location = new System.Drawing.Point(0, 0);
            this.chk1.Margin = new System.Windows.Forms.Padding(0);
            this.chk1.Name = "chk1";
            this.chk1.Properties.Caption = "幻觉";
            this.chk1.Size = new System.Drawing.Size(51, 19);
            this.chk1.TabIndex = 13;
            this.chk1.Tag = "1";
            // 
            // chk2
            // 
            this.chk2.Location = new System.Drawing.Point(51, 0);
            this.chk2.Margin = new System.Windows.Forms.Padding(0);
            this.chk2.Name = "chk2";
            this.chk2.Properties.Caption = "交流困难";
            this.chk2.Size = new System.Drawing.Size(80, 19);
            this.chk2.TabIndex = 14;
            this.chk2.Tag = "2";
            // 
            // chk3
            // 
            this.chk3.Location = new System.Drawing.Point(131, 0);
            this.chk3.Margin = new System.Windows.Forms.Padding(0);
            this.chk3.Name = "chk3";
            this.chk3.Properties.Caption = "猜疑";
            this.chk3.Size = new System.Drawing.Size(54, 19);
            this.chk3.TabIndex = 15;
            this.chk3.Tag = "3";
            // 
            // chk4
            // 
            this.chk4.Location = new System.Drawing.Point(185, 0);
            this.chk4.Margin = new System.Windows.Forms.Padding(0);
            this.chk4.Name = "chk4";
            this.chk4.Properties.Caption = "喜怒无常";
            this.chk4.Size = new System.Drawing.Size(81, 19);
            this.chk4.TabIndex = 16;
            this.chk4.Tag = "4";
            // 
            // chk5
            // 
            this.chk5.Location = new System.Drawing.Point(266, 0);
            this.chk5.Margin = new System.Windows.Forms.Padding(0);
            this.chk5.Name = "chk5";
            this.chk5.Properties.Caption = "行为怪异";
            this.chk5.Size = new System.Drawing.Size(72, 19);
            this.chk5.TabIndex = 17;
            this.chk5.Tag = "5";
            // 
            // chk6
            // 
            this.chk6.Location = new System.Drawing.Point(338, 0);
            this.chk6.Margin = new System.Windows.Forms.Padding(0);
            this.chk6.Name = "chk6";
            this.chk6.Properties.Caption = "兴奋话多";
            this.chk6.Size = new System.Drawing.Size(82, 19);
            this.chk6.TabIndex = 18;
            this.chk6.Tag = "6";
            // 
            // chk7
            // 
            this.chk7.Location = new System.Drawing.Point(420, 0);
            this.chk7.Margin = new System.Windows.Forms.Padding(0);
            this.chk7.Name = "chk7";
            this.chk7.Properties.Caption = "伤人毁物";
            this.chk7.Size = new System.Drawing.Size(79, 19);
            this.chk7.TabIndex = 19;
            this.chk7.Tag = "7";
            // 
            // chk8
            // 
            this.chk8.Location = new System.Drawing.Point(499, 0);
            this.chk8.Margin = new System.Windows.Forms.Padding(0);
            this.chk8.Name = "chk8";
            this.chk8.Properties.Caption = "悲观厌世";
            this.chk8.Size = new System.Drawing.Size(75, 19);
            this.chk8.TabIndex = 20;
            this.chk8.Tag = "8";
            // 
            // chk9
            // 
            this.chk9.Location = new System.Drawing.Point(0, 19);
            this.chk9.Margin = new System.Windows.Forms.Padding(0);
            this.chk9.Name = "chk9";
            this.chk9.Properties.Caption = "无故外走";
            this.chk9.Size = new System.Drawing.Size(84, 19);
            this.chk9.TabIndex = 21;
            this.chk9.Tag = "9";
            // 
            // chk10
            // 
            this.chk10.Location = new System.Drawing.Point(84, 19);
            this.chk10.Margin = new System.Windows.Forms.Padding(0);
            this.chk10.Name = "chk10";
            this.chk10.Properties.Caption = "自语自笑";
            this.chk10.Size = new System.Drawing.Size(84, 19);
            this.chk10.TabIndex = 22;
            this.chk10.Tag = "10";
            // 
            // chk11
            // 
            this.chk11.Location = new System.Drawing.Point(168, 19);
            this.chk11.Margin = new System.Windows.Forms.Padding(0);
            this.chk11.Name = "chk11";
            this.chk11.Properties.Caption = "孤僻懒散";
            this.chk11.Size = new System.Drawing.Size(84, 19);
            this.chk11.TabIndex = 23;
            this.chk11.Tag = "11";
            // 
            // chk12
            // 
            this.chk12.Location = new System.Drawing.Point(252, 19);
            this.chk12.Margin = new System.Windows.Forms.Padding(0);
            this.chk12.Name = "chk12";
            this.chk12.Properties.Caption = "其他";
            this.chk12.Size = new System.Drawing.Size(84, 19);
            this.chk12.TabIndex = 24;
            this.chk12.Tag = "99";
            // 
            // txt症状其他
            // 
            this.txt症状其他.Location = new System.Drawing.Point(336, 19);
            this.txt症状其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt症状其他.Name = "txt症状其他";
            this.txt症状其他.Size = new System.Drawing.Size(211, 20);
            this.txt症状其他.TabIndex = 25;
            // 
            // txt监护人地址
            // 
            this.txt监护人地址.Location = new System.Drawing.Point(118, 159);
            this.txt监护人地址.Name = "txt监护人地址";
            this.txt监护人地址.Size = new System.Drawing.Size(248, 20);
            this.txt监护人地址.StyleController = this.layoutControl1;
            this.txt监护人地址.TabIndex = 5;
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl居住地址.Location = new System.Drawing.Point(118, 108);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(248, 22);
            this.lbl居住地址.StyleController = this.layoutControl1;
            this.lbl居住地址.TabIndex = 12;
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl联系电话.Location = new System.Drawing.Point(465, 82);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(265, 22);
            this.lbl联系电话.StyleController = this.layoutControl1;
            this.lbl联系电话.TabIndex = 11;
            // 
            // lbl婚姻状况
            // 
            this.lbl婚姻状况.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl婚姻状况.Location = new System.Drawing.Point(465, 108);
            this.lbl婚姻状况.Name = "lbl婚姻状况";
            this.lbl婚姻状况.Size = new System.Drawing.Size(265, 22);
            this.lbl婚姻状况.StyleController = this.layoutControl1;
            this.lbl婚姻状况.TabIndex = 10;
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl身份证号.Location = new System.Drawing.Point(465, 56);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(265, 22);
            this.lbl身份证号.StyleController = this.layoutControl1;
            this.lbl身份证号.TabIndex = 9;
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl出生日期.Location = new System.Drawing.Point(118, 82);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(248, 22);
            this.lbl出生日期.StyleController = this.layoutControl1;
            this.lbl出生日期.TabIndex = 8;
            // 
            // lbl性别
            // 
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl性别.Location = new System.Drawing.Point(118, 56);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(248, 22);
            this.lbl性别.StyleController = this.layoutControl1;
            this.lbl性别.TabIndex = 7;
            // 
            // lbl姓名
            // 
            this.lbl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl姓名.Location = new System.Drawing.Point(465, 30);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(265, 22);
            this.lbl姓名.StyleController = this.layoutControl1;
            this.lbl姓名.TabIndex = 6;
            // 
            // lbl个人档案编号
            // 
            this.lbl个人档案编号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl个人档案编号.Location = new System.Drawing.Point(118, 30);
            this.lbl个人档案编号.Name = "lbl个人档案编号";
            this.lbl个人档案编号.Size = new System.Drawing.Size(248, 22);
            this.lbl个人档案编号.StyleController = this.layoutControl1;
            this.lbl个人档案编号.TabIndex = 5;
            // 
            // txt监护人姓名
            // 
            this.txt监护人姓名.Location = new System.Drawing.Point(118, 134);
            this.txt监护人姓名.Name = "txt监护人姓名";
            this.txt监护人姓名.Properties.AutoHeight = false;
            this.txt监护人姓名.Size = new System.Drawing.Size(248, 21);
            this.txt监护人姓名.StyleController = this.layoutControl1;
            this.txt监护人姓名.TabIndex = 3;
            // 
            // cbo与患者关系
            // 
            this.cbo与患者关系.Location = new System.Drawing.Point(465, 134);
            this.cbo与患者关系.Name = "cbo与患者关系";
            this.cbo与患者关系.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo与患者关系.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo与患者关系.Size = new System.Drawing.Size(265, 20);
            this.cbo与患者关系.StyleController = this.layoutControl1;
            this.cbo与患者关系.TabIndex = 4;
            // 
            // dte初次发病时间
            // 
            this.dte初次发病时间.EditValue = null;
            this.dte初次发病时间.Location = new System.Drawing.Point(118, 349);
            this.dte初次发病时间.Name = "dte初次发病时间";
            this.dte初次发病时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte初次发病时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte初次发病时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte初次发病时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte初次发病时间.Size = new System.Drawing.Size(612, 20);
            this.dte初次发病时间.StyleController = this.layoutControl1;
            this.dte初次发病时间.TabIndex = 8;
            // 
            // txt医生意见
            // 
            this.txt医生意见.Location = new System.Drawing.Point(118, 766);
            this.txt医生意见.Name = "txt医生意见";
            this.txt医生意见.Size = new System.Drawing.Size(612, 46);
            this.txt医生意见.StyleController = this.layoutControl1;
            this.txt医生意见.TabIndex = 49;
            this.txt医生意见.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "严重精神障碍患者个人信息补充表";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 911);
            this.layoutControlGroup1.Text = "严重精神障碍患者个人信息补充表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem14,
            this.layoutControlGroup5,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem17,
            this.layoutControlItem10,
            this.layoutControlItem22});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(731, 882);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.lbl居住地址;
            this.layoutControlItem9.CustomizationFormText = "居住地址";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "居住地址";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.lbl姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(367, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.lbl个人档案编号;
            this.layoutControlItem2.CustomizationFormText = "个人档案号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "个人档案号";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.lbl性别;
            this.layoutControlItem4.CustomizationFormText = "些别";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "性别";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt监护人姓名;
            this.layoutControlItem14.CustomizationFormText = "病例来源";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(367, 25);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "监护人姓名";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem40,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem38,
            this.layoutControlItem20,
            this.layoutControlItem39,
            this.layoutControlItem41,
            this.layoutControlItem36,
            this.layoutControlItem23,
            this.layoutControlItem48,
            this.layoutControlItem47,
            this.layoutControlItem46,
            this.layoutControlItem45,
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.layoutControlItem21,
            this.layoutControlItem11,
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem27});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 193);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(731, 689);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem40.Control = this.dte检查日期;
            this.layoutControlItem40.CustomizationFormText = "终止管理日期";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 593);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "检查日期*";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lbl创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 617);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "创建时间";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lbl最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(355, 617);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cbo知情同意;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(117, 54);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(614, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt知情同意签字;
            this.layoutControlItem16.CustomizationFormText = "签字：";
            this.layoutControlItem16.Location = new System.Drawing.Point(117, 78);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(614, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "签字：";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(88, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.dte知情同意签字时间;
            this.layoutControlItem18.CustomizationFormText = "签字时间：";
            this.layoutControlItem18.Location = new System.Drawing.Point(117, 102);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(614, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "签字时间：";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(88, 14);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Control = this.flow既往主要症状;
            this.layoutControlItem38.CustomizationFormText = "非药物治疗措施";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(192, 50);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(731, 50);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "既往主要症状";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem20.Control = this.panelControl3;
            this.layoutControlItem20.CustomizationFormText = "住院";
            this.layoutControlItem20.Location = new System.Drawing.Point(57, 289);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(275, 50);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(674, 50);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "住院";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem39.Control = this.radio最后一次治疗效果;
            this.layoutControlItem39.CustomizationFormText = "是否终止管理";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 413);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(130, 26);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(731, 26);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "最近一次治疗效果";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem41.Control = this.radio经济状况;
            this.layoutControlItem41.CustomizationFormText = "终止理由";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 519);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(731, 24);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "经济状况";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem36.Control = this.txt医生意见;
            this.layoutControlItem36.CustomizationFormText = "服药依从性";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 543);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(50, 50);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(731, 50);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "专科医生的意见(如果有请记录)";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem23.Control = this.txt医生签字;
            this.layoutControlItem23.CustomizationFormText = "医生签字";
            this.layoutControlItem23.Location = new System.Drawing.Point(238, 593);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "医生签字";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lbl当前所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 641);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lbl创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(355, 641);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lbl最近更新人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(355, 665);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近更新人";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lbl创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 665);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "创建人";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 54);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(117, 72);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem2.CustomizationFormText = "知情同意";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(115, 100);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(115, 1);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(115, 70);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "知情同意";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(110, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.flowLayoutPanel2;
            this.layoutControlItem1.CustomizationFormText = "门诊";
            this.layoutControlItem1.Location = new System.Drawing.Point(57, 229);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(159, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(674, 60);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "门诊";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(55, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 229);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(57, 110);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem5.CustomizationFormText = "既往治疗情况";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(55, 100);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(55, 40);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(55, 108);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "既往治疗情况";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(88, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.layoutControlItem19,
            this.layoutControlItem25,
            this.layoutControlItem26});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 339);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(731, 74);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.emptySpaceItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem6.CustomizationFormText = "目前诊断情况";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(120, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(120, 70);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(120, 72);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "目前诊断情况";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(168, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txt诊断;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(120, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(609, 24);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.txt确诊医院;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(120, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(609, 24);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.dte确诊日期;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(120, 48);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(609, 24);
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem21.Control = this.flow影响;
            this.layoutControlItem21.CustomizationFormText = "患者对家庭和社会的影响";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 439);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 80);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(200, 80);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(731, 80);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "危险行为";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.dte初次发病时间;
            this.layoutControlItem11.CustomizationFormText = "初次发病时间";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(225, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(731, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "初次发病时间";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.radioGroup就业情况;
            this.layoutControlItem27.CustomizationFormText = "就业情况";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(169, 54);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(731, 54);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "就业情况";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.textEdit档案类别;
            this.layoutControlItem22.CustomizationFormText = "户  别";
            this.layoutControlItem22.Location = new System.Drawing.Point(367, 153);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(364, 40);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "户  别";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem24.Control = this.radio关锁情况;
            this.layoutControlItem24.CustomizationFormText = "关锁情况";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 200);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(400, 0);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(117, 29);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(731, 29);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "既往关锁情况";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.lbl出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.lbl身份证号;
            this.layoutControlItem6.CustomizationFormText = "身份证号";
            this.layoutControlItem6.Location = new System.Drawing.Point(367, 26);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "身份证号";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.lbl联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(367, 52);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "联系电话";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.lbl婚姻状况;
            this.layoutControlItem7.CustomizationFormText = "职业";
            this.layoutControlItem7.Location = new System.Drawing.Point(367, 78);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "婚姻状况 ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.cbo与患者关系;
            this.layoutControlItem12.CustomizationFormText = "发病时间";
            this.layoutControlItem12.Location = new System.Drawing.Point(367, 104);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(364, 25);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "与患者关系";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt监护人地址;
            this.layoutControlItem13.CustomizationFormText = "诊断医院";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 129);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(367, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "监护人住址";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem17.Control = this.txt联系人电话;
            this.layoutControlItem17.CustomizationFormText = "辖区村（居）委会联系人、电话";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 153);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(225, 40);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(367, 40);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "辖区村（居）委会联系人、电话";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt监护人电话;
            this.layoutControlItem10.CustomizationFormText = "监护人电话";
            this.layoutControlItem10.Location = new System.Drawing.Point(367, 129);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(225, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "监护人电话";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // txt患者签名
            // 
            this.txt患者签名.Location = new System.Drawing.Point(592, 816);
            this.txt患者签名.Name = "txt患者签名";
            this.txt患者签名.Size = new System.Drawing.Size(138, 20);
            this.txt患者签名.StyleController = this.layoutControl1;
            this.txt患者签名.TabIndex = 54;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt患者签名;
            this.layoutControlItem28.CustomizationFormText = "患者(家属)签名";
            this.layoutControlItem28.Location = new System.Drawing.Point(498, 593);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem28.Text = "患者(家属)签名";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(88, 14);
            // 
            // chk_其他危害行为
            // 
            this.chk_其他危害行为.Location = new System.Drawing.Point(205, 28);
            this.chk_其他危害行为.Name = "chk_其他危害行为";
            this.chk_其他危害行为.Properties.Caption = "";
            this.chk_其他危害行为.Size = new System.Drawing.Size(25, 19);
            this.chk_其他危害行为.TabIndex = 49;
            this.chk_其他危害行为.Tag = "7";
            // 
            // uc其他危害行为
            // 
            this.uc其他危害行为.Lbl1Size = new System.Drawing.Size(80, 18);
            this.uc其他危害行为.Lbl1Text = "其他危害行为";
            this.uc其他危害行为.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc其他危害行为.Lbl2Text = "次";
            this.uc其他危害行为.Location = new System.Drawing.Point(233, 25);
            this.uc其他危害行为.Margin = new System.Windows.Forms.Padding(0);
            this.uc其他危害行为.Name = "uc其他危害行为";
            this.uc其他危害行为.Size = new System.Drawing.Size(223, 22);
            this.uc其他危害行为.TabIndex = 50;
            this.uc其他危害行为.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radioGroup就业情况
            // 
            this.radioGroup就业情况.Location = new System.Drawing.Point(118, 223);
            this.radioGroup就业情况.Name = "radioGroup就业情况";
            this.radioGroup就业情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "1.在岗工人"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "2.在岗管理者"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "3.农民"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "4.下岗或无业"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "5.在校学生"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "6.退休"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("7", "7.专业技术人员"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("8", "8.其他"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("9", "9.不详")});
            this.radioGroup就业情况.Size = new System.Drawing.Size(612, 50);
            this.radioGroup就业情况.StyleController = this.layoutControl1;
            this.radioGroup就业情况.TabIndex = 53;
            // 
            // UC重性精神疾病患者个人信息补充表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC重性精神疾病患者个人信息补充表";
            this.Size = new System.Drawing.Size(750, 500);
            this.Load += new System.EventHandler(this.UC重性精神疾病患者个人信息补充表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案类别.Properties)).EndInit();
            this.flow影响.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk影响无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk影响5.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio门诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio关锁情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dte知情同意签字时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte知情同意签字时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt知情同意签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo知情同意.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio经济状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio最后一次治疗效果.Properties)).EndInit();
            this.flow既往主要症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt监护人姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与患者关系.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte初次发病时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte初次发病时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生意见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_其他危害行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup就业情况.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt监护人地址;
        private DevExpress.XtraEditors.LabelControl lbl居住地址;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl lbl婚姻状况;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl姓名;
        private DevExpress.XtraEditors.LabelControl lbl个人档案编号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.FlowLayoutPanel flow既往主要症状;
        private DevExpress.XtraEditors.CheckEdit chk1;
        private DevExpress.XtraEditors.CheckEdit chk2;
        private DevExpress.XtraEditors.CheckEdit chk3;
        private DevExpress.XtraEditors.CheckEdit chk4;
        private DevExpress.XtraEditors.CheckEdit chk5;
        private DevExpress.XtraEditors.CheckEdit chk6;
        private DevExpress.XtraEditors.CheckEdit chk7;
        private DevExpress.XtraEditors.CheckEdit chk8;
        private DevExpress.XtraEditors.CheckEdit chk9;
        private DevExpress.XtraEditors.RadioGroup radio关锁情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近更新人;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.RadioGroup radio经济状况;
        private DevExpress.XtraEditors.DateEdit dte检查日期;
        private DevExpress.XtraEditors.RadioGroup radio最后一次治疗效果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DateEdit dte知情同意签字时间;
        private DevExpress.XtraEditors.TextEdit txt知情同意签字;
        private DevExpress.XtraEditors.ComboBoxEdit cbo知情同意;
        private DevExpress.XtraEditors.TextEdit txt联系人电话;
        private DevExpress.XtraEditors.TextEdit txt监护人电话;
        private DevExpress.XtraEditors.TextEdit txt监护人姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo与患者关系;
        private DevExpress.XtraEditors.DateEdit dte初次发病时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.RadioGroup radio门诊;
        private DevExpress.XtraEditors.CheckEdit chk10;
        private DevExpress.XtraEditors.CheckEdit chk11;
        private DevExpress.XtraEditors.CheckEdit chk12;
        private DevExpress.XtraEditors.TextEdit txt症状其他;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private Library.UserControls.UCLblDtp dte首次治疗时间;
        private Library.UserControls.UCLblDtp dte确诊日期;
        private Library.UserControls.UCLblTxt txt确诊医院;
        private Library.UserControls.UCLblTxt txt诊断;
        private DevExpress.XtraEditors.CheckEdit chk影响无;
        private Library.UserControls.UCLblTxtLbl uc自杀未遂;
        private DevExpress.XtraEditors.CheckEdit chk影响5;
        private Library.UserControls.UCLblTxtLbl uc自伤;
        private DevExpress.XtraEditors.CheckEdit chk影响4;
        private Library.UserControls.UCLblTxtLbl uc肇祸;
        private DevExpress.XtraEditors.CheckEdit chk影响3;
        private Library.UserControls.UCLblTxtLbl uc肇事;
        private DevExpress.XtraEditors.CheckEdit chk影响2;
        private Library.UserControls.UCLblTxtLbl uc轻度滋事;
        private DevExpress.XtraEditors.CheckEdit chk影响1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit txt医生签字;
        private DevExpress.XtraEditors.MemoEdit txt医生意见;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private Library.UserControls.UCTxtLbl txt住院此次;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private System.Windows.Forms.FlowLayoutPanel flow影响;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit textEdit档案类别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.TextEdit txt患者签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.CheckEdit chk_其他危害行为;
        private Library.UserControls.UCLblTxtLbl uc其他危害行为;
        private DevExpress.XtraEditors.RadioGroup radioGroup就业情况;
    }
}
