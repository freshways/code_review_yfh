﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class UC重性精神疾病患者随访服务记录表_显示 : UserControlBaseNavBar
    {
        private string m_Grdabh = "";
        private string m_ID = "0";

        private bll精神病随访记录 m_bll随访 = new bll精神病随访记录();
        private bll精神病_用药情况 m_bll用药 = new bll精神病_用药情况();

        private DataSet m_ds随访概要;
        private DataSet m_ds随访单一;
        //private DataSet m_ds用药信息;
        //public UC重性精神疾病患者随访服务记录表_显示()
        //{
        //    InitializeComponent();
        //}

        public UC重性精神疾病患者随访服务记录表_显示(string grdabh, string id)
        {
            InitializeComponent();

            m_Grdabh = grdabh;
            if (string.IsNullOrWhiteSpace(id))
            {
                m_ID = "0";
            }
            else
            {
                m_ID = id;
            }

            BindOrginalDataForComboBox();

            intView();

            Get随访AndShow();
        }

        private void BindOrginalDataForComboBox()
        {
            BindDataForComboBox("wxx", comboBoxEdit危险性);
            BindDataForComboBox("jsbsffl", comboBoxEdit此次随访分类);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }

        private void intView()
        {
            ucLblTxtLbl轻度滋事.Txt1.Properties.ReadOnly = true;
            ucLblTxtLbl肇祸.Txt1.Properties.ReadOnly = true;
            ucLblTxtLbl肇事.Txt1.Properties.ReadOnly = true;
            ucLblTxtLbl自杀未遂.Txt1.Properties.ReadOnly = true;
            ucLblTxtLbl自伤.Txt1.Properties.ReadOnly = true;
            ucLblTxtLbl其他危害行为.Txt1.Properties.ReadOnly = true;
        }

        private void Get随访AndShow()
        {
            try
            {
                m_ds随访概要 = m_bll随访.Get随访概要(m_Grdabh);
                if (m_ds随访概要 == null || m_ds随访概要.Tables.Count == 0 || m_ds随访概要.Tables[0].Rows.Count == 0)
                { }
                else
                {
                    //创建左侧日期栏
                    CreateNavBarButton_new(m_ds随访概要.Tables[0], tb_精神病随访记录.随访日期);

                    //设置个人基本信息
                    Show精神病基本信息(m_ds随访概要.Tables[1]);
                    DataRow[] drTemp = m_ds随访概要.Tables[0].Select("ID=" + m_ID);
                    if (drTemp.Length > 0)
                    { }
                    else
                    {
                        m_ID = m_ds随访概要.Tables[0].Rows[0]["ID"].ToString();
                    }
                    m_ds随访单一 = m_bll随访.Get随访单一(m_Grdabh, m_ID);
                    ShowDetailInfo(m_ds随访单一);
                    SetItemColorToRed(m_ID);
                }
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void Show精神病基本信息(DataTable dt基本信息)
        {
            lbl个人档案编号.Text = dt基本信息.Rows[0][tb_健康档案.个人档案编号].ToString();
            lbl姓名.Text = DESEncrypt.DES解密(dt基本信息.Rows[0][tb_健康档案.姓名].ToString());
            lbl性别.Text = m_bll随访.ReturnDis字典显示("xb_xingbie", dt基本信息.Rows[0][tb_健康档案.性别].ToString());
            lbl身份证号.Text = dt基本信息.Rows[0][tb_健康档案.身份证号].ToString();
            lbl出生日期.Text = dt基本信息.Rows[0][tb_健康档案.出生日期].ToString();
            lbl联系电话.Text = dt基本信息.Rows[0][tb_健康档案.联系人电话].ToString();
            lbl居住地址.Text = m_bll随访.Return地区名称(dt基本信息.Rows[0][tb_健康档案.省].ToString())
                                + m_bll随访.Return地区名称(dt基本信息.Rows[0][tb_健康档案.市].ToString())
                                + m_bll随访.Return地区名称(dt基本信息.Rows[0][tb_健康档案.区].ToString())
                                + m_bll随访.Return地区名称(dt基本信息.Rows[0][tb_健康档案.街道].ToString())
                                + m_bll随访.Return地区名称(dt基本信息.Rows[0][tb_健康档案.居委会].ToString())
                                + dt基本信息.Rows[0][tb_健康档案.居住地址].ToString();
            lbl婚姻状况.Text = m_bll随访.ReturnDis字典显示("hyzk", dt基本信息.Rows[0][tb_健康档案.婚姻状况].ToString());
        }

        private void ShowSpaceInfo()
        {
            lbl随访日期.Text = "";
            util.ControlsHelper.SetComboxData("", comboBoxEdit危险性);
            lbl目前症状.Text = "";
            lbl自知力.Text = "";
            lbl睡眠情况.Text = "";
            lbl饮食情况.Text = "";
            lbl个人生活料理.Text = "";
            lbl家务劳动.Text = "";
            lbl生产劳动及工作.Text = "";
            lbl学习能力.Text = "";
            lbl社会人际交往.Text = "";

            ucLblTxtLbl轻度滋事.Txt1.Text = "";
            ucLblTxtLbl轻度滋事.Visible = false;
            ucLblTxtLbl肇事.Txt1.Text = "";
            ucLblTxtLbl肇事.Visible = false;
            ucLblTxtLbl肇祸.Txt1.Text = "";
            ucLblTxtLbl肇祸.Visible = false;
            ucLblTxtLbl自伤.Txt1.Text = "";
            ucLblTxtLbl自伤.Visible = false;
            ucLblTxtLbl自杀未遂.Txt1.Text = "";
            ucLblTxtLbl自杀未遂.Visible = false;
            lbl无影响.Visible = false;

            lbl关锁情况.Text = "";
            lbl住院情况.Text = "";
            lbl末次住院时间.Text = "";

            lbl实验室检查.Text = "";
            txt实验室检查.Text = "";
            lbl服药依从性.Text = "";
            lbl药物不良反应.Text = "";
            txt药物不良反应.Text = "";
            lbl治疗效果.Text = "";

            util.ControlsHelper.SetComboxData("", comboBoxEdit此次随访分类);

            lbl是否转诊.Text = "";
            textEdit转诊原因.Text = "";
            textEdit转诊机构.Text = "";
            //用药情况清空

            lbl康复措施.Text = "";
            txt下次随访日期.Text = "";
            txt随访医生.Text = "";

            lbl创建时间.Text = "";
            lbl最近更新时间.Text = "";
            lbl所属机构.Text = "";
            lbl创建机构.Text = "";
            lbl创建人.Text = "";
            lbl最近修改人.Text = "";

            #region 新版本添加 
            
            #endregion
        }

        protected override void DoBindingSummaryEditor(object dataSource)
        {
            //将所有控件清空
            ShowSpaceInfo();

            DataRow drShowing = (DataRow)dataSource;
            if (drShowing == null)
            { }
            else
            {
                m_ID = drShowing["ID"].ToString();
                m_ds随访单一 = m_bll随访.Get随访单一(m_Grdabh, m_ID);
                ShowDetailInfo(m_ds随访单一);
                SetItemColorToRed(m_ID);
            }
        }

        private void ShowDetailInfo(DataSet ds)
        {
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            { }
            else
            {
                #region 添加随访信息
                lbl随访日期.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.随访日期].ToString();
                SetItemColor(lbl随访日期, lci随访日期);

                string str危险性 = ds.Tables[0].Rows[0][tb_精神病随访记录.危险性].ToString();
                util.ControlsHelper.SetComboxData(str危险性, comboBoxEdit危险性);
                SetItemColor(comboBoxEdit危险性, lci危险性);

                string db目前症状 = ds.Tables[0].Rows[0][tb_精神病随访记录.目前症状].ToString();
                string str目前症状 = ds.Tables[0].Rows[0][tb_精神病随访记录.目前症状其他].ToString();
                StringBuilder sb目前症状 = new StringBuilder();
                //string str目前症状 = "";
                char[] c分隔符 = { ',' };
                string[] str症状明细 = db目前症状.Split(c分隔符);
                for (int index = 0; index < str症状明细.Length; index++)
                {
                    switch (str症状明细[index])
                    {
                        case "1":
                            sb目前症状.Append(",幻觉");
                            break;
                        case "2":
                            sb目前症状.Append(",交流困难");
                            break;
                        case "3":
                            sb目前症状.Append(",猜疑");
                            break;
                        case "4":
                            sb目前症状.Append(",喜怒无常");
                            break;
                        case "5":
                            sb目前症状.Append(",行为怪异");
                            break;
                        case "6":
                            sb目前症状.Append(",兴奋话多");
                            break;
                        case "7":
                            sb目前症状.Append(",伤人毁物");
                            break;
                        case "8":
                            sb目前症状.Append(",悲观厌世");
                            break;
                        case "9":
                            sb目前症状.Append(",无故外走");
                            break;
                        case "10":
                            sb目前症状.Append(",自语自笑");
                            break;
                        case "11":
                            sb目前症状.Append(",孤僻懒散");
                            break;
                        case "99":
                            sb目前症状.Append(",其他：");
                            sb目前症状.Append(str目前症状);
                            break;
                        default:
                            break;
                    }
                }

                if (sb目前症状.Length > 0)
                {
                    lbl目前症状.Text = sb目前症状.ToString(1, sb目前症状.Length - 1);

                    if (db目前症状.Contains("99") && string.IsNullOrWhiteSpace(str目前症状))
                    {
                        lci目前症状.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lci目前症状.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
                else
                {
                    lbl目前症状.Text = "";
                    lci目前症状.AppearanceItemCaption.ForeColor = Color.Red;
                }

                string str自知力 = ds.Tables[0].Rows[0][tb_精神病随访记录.自知力].ToString();
                lbl自知力.Text = m_bll随访.ReturnDis字典显示("zzl", str自知力);
                SetItemColor(lbl自知力, lci自知力);

                string str睡眠情况 = ds.Tables[0].Rows[0][tb_精神病随访记录.睡眠情况].ToString();
                lbl睡眠情况.Text = m_bll随访.ReturnDis字典显示("glqk", str睡眠情况);
                SetItemColor(lbl睡眠情况, lci睡眠情况);

                string str饮食情况 = ds.Tables[0].Rows[0][tb_精神病随访记录.饮食情况].ToString();
                lbl饮食情况.Text = m_bll随访.ReturnDis字典显示("glqk", str饮食情况);
                SetItemColor(lbl饮食情况, lci饮食情况);

                string str个人生活料理 = ds.Tables[0].Rows[0][tb_精神病随访记录.个人生活料理].ToString();
                lbl个人生活料理.Text = m_bll随访.ReturnDis字典显示("glqk", str个人生活料理);
                SetItemColor(lbl个人生活料理, lci个人生活料理);

                string str家务劳动 = ds.Tables[0].Rows[0][tb_精神病随访记录.家务劳动].ToString();
                lbl家务劳动.Text = m_bll随访.ReturnDis字典显示("glqk", str家务劳动);
                SetItemColor(lbl家务劳动, lci家务劳动);

                string str生产 = ds.Tables[0].Rows[0][tb_精神病随访记录.生产劳动及工作].ToString();
                lbl生产劳动及工作.Text = m_bll随访.ReturnDis字典显示("glqk", str生产);
                SetItemColor(lbl生产劳动及工作, lci生产劳动);

                string str学习能力 = ds.Tables[0].Rows[0][tb_精神病随访记录.学习能力].ToString();
                lbl学习能力.Text = m_bll随访.ReturnDis字典显示("glqk", str学习能力);
                SetItemColor(lbl学习能力, lci学习能力);

                string str社会人际 = ds.Tables[0].Rows[0][tb_精神病随访记录.社会人际交往].ToString();
                lbl社会人际交往.Text = m_bll随访.ReturnDis字典显示("glqk", str社会人际);
                SetItemColor(lbl社会人际交往, lci社会人际交往);

                string str影响 = ds.Tables[0].Rows[0][tb_精神病随访记录.影响].ToString();
                if (str影响.Contains("1"))
                {
                    ucLblTxtLbl轻度滋事.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响轻度滋事].ToString();
                    ucLblTxtLbl轻度滋事.Visible = true;
                }
                else
                {
                    ucLblTxtLbl轻度滋事.Txt1.Text = "";
                    ucLblTxtLbl轻度滋事.Visible = false;
                }
                if (str影响.Contains("2"))
                {
                    ucLblTxtLbl肇事.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响肇事].ToString();
                    ucLblTxtLbl肇事.Visible = false;
                }
                else
                {
                    ucLblTxtLbl肇事.Txt1.Text = "";
                    ucLblTxtLbl肇事.Visible = false;
                }

                if (str影响.Contains("3"))
                {
                    ucLblTxtLbl肇祸.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响肇祸].ToString();
                    ucLblTxtLbl肇祸.Visible = true;
                }
                else
                {
                    ucLblTxtLbl肇祸.Txt1.Text = "";
                    ucLblTxtLbl肇祸.Visible = false;
                }
                if (str影响.Contains("4"))
                {
                    ucLblTxtLbl自伤.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响自伤].ToString();
                    ucLblTxtLbl自伤.Visible = true;
                }
                else
                {
                    ucLblTxtLbl自伤.Txt1.Text = "";
                    ucLblTxtLbl自伤.Visible = false;
                }

                if (str影响.Contains("5"))
                {
                    ucLblTxtLbl自杀未遂.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响自杀未遂].ToString();
                    ucLblTxtLbl自杀未遂.Visible = true;
                }
                else
                {
                    ucLblTxtLbl自杀未遂.Txt1.Text = "";
                    ucLblTxtLbl自杀未遂.Visible = false;
                }
                if (str影响.Contains("6"))
                {
                    lbl无影响.Visible = true;
                }
                else
                {
                    lbl无影响.Visible = false;
                }
                #region 新版本添加
                if (str影响.Contains("7"))
                {
                    ucLblTxtLbl其他危害行为.Txt1.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.影响其他危害行为].ToString();
                    ucLblTxtLbl其他危害行为.Visible = true;
                }
                else
                {
                    ucLblTxtLbl其他危害行为.Txt1.Text = "";
                    ucLblTxtLbl其他危害行为.Visible = false;
                }
                #endregion
                if (ucLblTxtLbl轻度滋事.Visible || ucLblTxtLbl肇事.Visible || ucLblTxtLbl肇祸.Visible
                || ucLblTxtLbl自伤.Visible || ucLblTxtLbl自杀未遂.Visible || lbl无影响.Visible || ucLblTxtLbl其他危害行为.Visible)
                {
                    lci影响.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    lci影响.AppearanceItemCaption.ForeColor = Color.Red;
                }

                string str关锁 = ds.Tables[0].Rows[0][tb_精神病随访记录.关锁情况].ToString();
                lbl关锁情况.Text = m_bll随访.ReturnDis字典显示("gsqk", str关锁);
                SetItemColor(lbl关锁情况, lci关锁情况);

                string str住院情况 = ds.Tables[0].Rows[0][tb_精神病随访记录.住院情况].ToString();
                lbl住院情况.Text = m_bll随访.ReturnDis字典显示("zyqk", str住院情况);
                lbl末次住院时间.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.末次出院时间].ToString();
                //if(str住院情况 == "3")
                //{
                //    lbl末次住院时间.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.末次出院时间].ToString();
                //    if(string.IsNullOrWhiteSpace(lbl末次住院时间.Text))
                //    {
                //        lci住院情况.AppearanceItemCaption.ForeColor = Color.Red;
                //    }
                //    else
                //    {
                //        lci住院情况.AppearanceItemCaption.ForeColor = Color.Blue;
                //    }
                //}
                //else if(string.IsNullOrWhiteSpace(str住院情况))
                //{
                //    lci住院情况.AppearanceItemCaption.ForeColor = Color.Red;
                //}
                //else
                //{
                //    lci住院情况.AppearanceItemCaption.ForeColor = Color.Blue;
                //}
                if (string.IsNullOrWhiteSpace(str住院情况))
                {
                    lci住院情况.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else
                {
                    lci住院情况.AppearanceItemCaption.ForeColor = Color.Blue;
                }


                string str实验室 = ds.Tables[0].Rows[0][tb_精神病随访记录.实验室检查].ToString();
                lbl实验室检查.Text = m_bll随访.ReturnDis字典显示("yw_youwu", str实验室);
                txt实验室检查.Text = "";
                if (str实验室 == "1")
                {
                    txt实验室检查.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.实验室检查其他].ToString();
                    if (string.IsNullOrWhiteSpace(txt实验室检查.Text))
                    {
                        lci实验室检查.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lci实验室检查.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
                else if (str实验室 == "2")
                {
                    lci实验室检查.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    lci实验室检查.AppearanceItemCaption.ForeColor = Color.Red;
                }
                //SetItemColor(lbl实验室检查, lci实验室检查);

                string str服药 = ds.Tables[0].Rows[0][tb_精神病随访记录.服药依从性].ToString();
                lbl服药依从性.Text = m_bll随访.ReturnDis字典显示("fyycx-mb", str服药);
                SetItemColor(lbl服药依从性, lci服药依从性);


                string str药物不良 = ds.Tables[0].Rows[0][tb_精神病随访记录.药物不良反应有无].ToString();
                lbl药物不良反应.Text = m_bll随访.ReturnDis字典显示("yw_youwu", str药物不良);
                txt药物不良反应.Text = "";
                if (str药物不良 == "1")
                {
                    txt药物不良反应.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.药物不良反应].ToString();
                    if (string.IsNullOrWhiteSpace(txt药物不良反应.Text))
                    {
                        lci药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lci药物不良反应.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
                else if (str药物不良 == "2")
                {
                    lci药物不良反应.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    lci药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
                }

                //SetItemColor(lbl药物不良反应, lci药物不良反应);

                string str治疗效果 = ds.Tables[0].Rows[0][tb_精神病随访记录.治疗效果].ToString();
                lbl治疗效果.Text = m_bll随访.ReturnDis字典显示("zlxg", str治疗效果);
                SetItemColor(lbl治疗效果, lci治疗效果);

                string str分类 = ds.Tables[0].Rows[0][tb_精神病随访记录.此次随访分类].ToString();
                util.ControlsHelper.SetComboxData(str分类, comboBoxEdit此次随访分类);
                SetItemColor(comboBoxEdit此次随访分类, lci此次随访分类);

                #region 旧的转诊（未用）
                //string str转诊 = ds.Tables[0].Rows[0][tb_精神病随访记录.是否转诊].ToString();
                //lbl是否转诊.Text = m_bll随访.ReturnDis字典显示("sf_shifou", str转诊);
                //textEdit转诊原因.Text = "";
                //textEdit转诊机构.Text = "";
                //if (str转诊 == "1")
                //{
                //    textEdit转诊原因.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊原因].ToString();
                //    textEdit转诊机构.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊科室].ToString();
                //    if (string.IsNullOrWhiteSpace(textEdit转诊机构.Text) || string.IsNullOrWhiteSpace(textEdit转诊原因.Text))
                //    {
                //        lci转诊.AppearanceItemCaption.ForeColor = Color.Red;
                //    }
                //    else
                //    {
                //        lci转诊.AppearanceItemCaption.ForeColor = Color.Blue;
                //    }
                //}
                //else if (string.IsNullOrWhiteSpace(str转诊))
                //{
                //    lci转诊.AppearanceItemCaption.ForeColor = Color.Red;
                //}
                //else
                //{
                //    lci转诊.AppearanceItemCaption.ForeColor = Color.Blue;
                //}
                #endregion

                string str康复措施 = ds.Tables[0].Rows[0][tb_精神病随访记录.康复措施].ToString();
                string str康复措施其他 = ds.Tables[0].Rows[0][tb_精神病随访记录.康复措施其他].ToString();
                //lbl康复措施.Text = m_bll随访.ReturnDis字典显示("kfcs", str康复措施);
                string[] str康复措施明细 = str康复措施.Split(c分隔符);
                StringBuilder builder = new StringBuilder();
                for (int index = 0; index < str康复措施明细.Length; index++)
                {
                    switch (str康复措施明细[index])
                    {
                        case "1":
                            builder.Append("生活劳动能力,");
                            break;
                        case "2":
                            builder.Append("职业训练,");
                            break;
                        case "3":
                            builder.Append("学习能力,");
                            break;
                        case "4":
                            builder.Append("社会交往,");
                            break;
                        case "99":
                            builder.Append("其他 :");
                            builder.Append(str康复措施其他);
                            builder.Append(",");
                            break;
                    }
                }
                if (builder.Length > 0)
                {
                    lbl康复措施.Text = builder.ToString(0, builder.Length - 1);
                    if (str康复措施.Contains("99") && string.IsNullOrWhiteSpace(str康复措施其他))
                    {
                        lci康复措施.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lci康复措施.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
                else
                {
                    lbl康复措施.Text = "";
                    lci康复措施.AppearanceItemCaption.ForeColor = Color.Red;
                }

                #region 新版本添加
                txt本次随访形式.Text = m_bll随访.ReturnDis字典显示("bcsfxs", ds.Tables[0].Rows[0][tb_精神病随访记录.本次随访形式].ToString());
                txt失访原因.Text = m_bll随访.ReturnDis字典显示("jsb_sfyy", ds.Tables[0].Rows[0][tb_精神病随访记录.若失访原因].ToString());
                txt死亡日期.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.死亡日期].ToString();
                string str死亡原因 = ds.Tables[0].Rows[0][tb_精神病随访记录.死亡原因].ToString();
                string str躯体疾病= m_bll随访.ReturnDis字典显示("jsb_qtjb",ds.Tables[0].Rows[0][tb_精神病随访记录.死亡原因_躯体疾病].ToString());
                if (string.IsNullOrEmpty(str死亡原因))
                {
                    txt死亡原因.Text = "";
                }
                else if (str死亡原因 != null && str死亡原因=="1")
                {
                    txt死亡原因.Text = "躯体疾病：" + str躯体疾病;
                }
                else
                {
                    txt死亡原因.Text = m_bll随访.ReturnDis字典显示("jsb_swyy", str死亡原因);
                }
                string str是否通知联席部门= m_bll随访.ReturnDis字典显示("sf_shifou", ds.Tables[0].Rows[0][tb_精神病随访记录.是否通知联席部门].ToString());
                txt是否通知联席部门.Text =str是否通知联席部门;
                if (!string.IsNullOrEmpty(str是否通知联席部门) && str是否通知联席部门=="是")
                {
                    txt受理人1姓名.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.受理人1姓名].ToString();
                    txt受理人2姓名.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.受理人2姓名].ToString();
                    txt受理人1电话.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.受理人1联系电话].ToString();
                    txt受理人2电话.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.受理人1联系电话].ToString();
                }
                else
                {
                    txt受理人1姓名.Text ="";
                    txt受理人2姓名.Text ="";
                    txt受理人1电话.Text ="";
                    txt受理人2电话.Text ="";
                }
                string str是否需要转诊 = m_bll随访.ReturnDis字典显示("sf_shifou", ds.Tables[0].Rows[0][tb_精神病随访记录.是否需要转诊].ToString());
                txt是否需要转诊.Text = str是否需要转诊;
                if (!string.IsNullOrEmpty(str是否需要转诊) && str是否需要转诊 == "是")
                {
                    txt转诊原因.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊原因].ToString();
                }
                else
                {
                    txt转诊原因.Text = "";
                }
                string str转诊是否成功 = m_bll随访.ReturnDis字典显示("sf_shifou", ds.Tables[0].Rows[0][tb_精神病随访记录.是否转诊].ToString());
                txt转诊是否成功.Text = str转诊是否成功;
                if (!string.IsNullOrEmpty(str转诊是否成功) && str转诊是否成功 == "是")
                {
                    txt转诊机构.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊机构].ToString();
                    txt转诊科室.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊科室].ToString();
                    txt转诊联系人.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊联系人].ToString();
                    txt转诊联系电话.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.转诊联系电话].ToString();
                }
                else
                {
                    txt转诊机构.Text = "";
                    txt转诊科室.Text = "";
                     txt转诊联系人.Text = "";
                     txt转诊联系电话.Text = "";
                }
                string str是否联系精神专科医师 = m_bll随访.ReturnDis字典显示("sf_shifou", ds.Tables[0].Rows[0][tb_精神病随访记录.是否联系精神专科医师].ToString());
                txt是否联系精神专科医师.Text = str是否联系精神专科医师;
                if (!string.IsNullOrEmpty(str是否联系精神专科医师) && str是否联系精神专科医师 == "是")
                {
                    txt医师姓名.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.精神专科医师姓名].ToString();
                    txt医师电话.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.精神专科医师电话].ToString();
                    txt处置结果.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.处置结果].ToString();
                }
                else
                {
                    txt医师姓名.Text = "";
                    txt医师电话.Text = "";
                    txt处置结果.Text = "";
                }
                txt患者或家属签名.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.患者或家属签名].ToString();
                #endregion

                txt下次随访日期.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.下次随访日期].ToString();
                SetItemColor(txt下次随访日期, lci下次随访日期);

                txt随访医生.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.随访医生].ToString();
                SetItemColor(txt随访医生, lci随访医生);

                lbl创建时间.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.创建时间].ToString();
                lbl最近更新时间.Text = ds.Tables[0].Rows[0][tb_精神病随访记录.更新时间].ToString();

                string str所属机构 = ds.Tables[0].Rows[0][tb_精神病随访记录.所属机构].ToString();
                lbl所属机构.Text = m_bll随访.Return机构名称(str所属机构);
                lbl所属机构.Tag = str所属机构;

                string str创建机构 = ds.Tables[0].Rows[0][tb_精神病随访记录.创建机构].ToString();
                lbl创建机构.Text = m_bll随访.Return机构名称(str创建机构);

                string str创建人 = ds.Tables[0].Rows[0][tb_精神病随访记录.创建人].ToString();
                lbl创建人.Text = m_bll随访.Return用户名称(str创建人);

                string str修改人 = ds.Tables[0].Rows[0][tb_精神病随访记录.修改人].ToString();
                lbl最近修改人.Text = m_bll随访.Return用户名称(str修改人);

                labelControl考核项.Text = "考核项：23   缺项：" + ds.Tables[0].Rows[0][tb_精神病随访记录.缺项].ToString()
                    + "   完整度：" + ds.Tables[0].Rows[0][tb_精神病随访记录.完整度].ToString() + "%";
                #endregion

                #region 显示药品信息
                if (ds.Tables.Count == 2)
                {
                    layoutControlItem用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    this.gridControl用药情况.DataSource = ds.Tables[1];
                    int newHeight = 30 + 24 * ds.Tables[1].Rows.Count;
                    layoutControlItem用药情况.MinSize = new Size(layoutControlItem用药情况.MinSize.Width, newHeight);
                    layoutControlItem用药情况.MaxSize = new Size(layoutControlItem用药情况.MaxSize.Width, newHeight);
                }
                #endregion
            }
        }

        private void SetItemColor(Control ctrl, DevExpress.XtraLayout.LayoutControlItem item)
        {
            if (string.IsNullOrWhiteSpace(ctrl.Text))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }



        private void btn添加随访_Click(object sender, EventArgs e)
        {
            UC重性精神疾病患者随访服务记录表 ctrl = new UC重性精神疾病患者随访服务记录表(m_Grdabh, null, UpdateType.Add);
            ShowControl(ctrl, DockStyle.Fill);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            //string str所属机构 = lbl所属机构.Tag.ToString();
            //if (str所属机构 == Loginer.CurrentUser.所属机构)
            if (canModifyBy同一机构(m_ds随访概要.Tables[1].Rows[0][tb_健康档案.所属机构].ToString()))//跨机构修改
            {
                UC重性精神疾病患者随访服务记录表 ctrl = new UC重性精神疾病患者随访服务记录表(m_Grdabh, m_ID, UpdateType.Modify);
                ShowControl(ctrl, DockStyle.Fill);
            }
            else
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            //string str所属机构 = lbl所属机构.Tag.ToString();
            //if (str所属机构 == Loginer.CurrentUser.所属机构)
            //{
            if (canModifyBy同一机构(m_ds随访概要.Tables[1].Rows[0][tb_健康档案.所属机构].ToString()))
            {
                bool isDelete = Msg.AskQuestion("确定要删除数据么？");
                if (isDelete)
                {
                    m_bll随访.DeleteByID(m_ID);//m_bll随访.deletsfbyid(m_ID);
                    //m_ds随访概要.Tables[tb_精神病随访记录.__TableName].Select(m_ID);
                    if (m_ds随访概要.Tables[tb_精神病随访记录.__TableName].Rows.Count == 1)
                    {
                        UC重性精神疾病患者随访服务记录表 ctrl = new UC重性精神疾病患者随访服务记录表(m_Grdabh, "", UpdateType.Add);
                        ShowControl(ctrl);
                    }
                    else
                    {
                        DataRow[] drs = m_ds随访概要.Tables[tb_精神病随访记录.__TableName].Select(tb_精神病随访记录.ID + "='" + m_ID + "'");
                        if (drs.Length > 0)
                        {
                            drs[0].Delete();
                        }
                        m_ds随访概要.Tables[tb_精神病随访记录.__TableName].AcceptChanges();

                        m_ID = m_ds随访概要.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.ID].ToString();

                        //重新创建左侧日期栏
                        navBarControl1.Items.Clear();
                        CreateNavBarButton_new(m_ds随访概要.Tables[0], tb_精神病随访记录.随访日期);
                        m_ds随访单一 = m_bll随访.Get随访单一(m_Grdabh, m_ID);
                        ShowDetailInfo(m_ds随访单一);
                        SetItemColorToRed(m_ID);
                    }
                }
            }
            else
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.lbl个人档案编号.Text.Trim();
            string year = this.lbl随访日期.Text.Trim();
            if (!string.IsNullOrEmpty(docNo) && !string.IsNullOrEmpty(year))
            {
                report重性精神疾病患者随访服务记录表 report = new report重性精神疾病患者随访服务记录表(docNo, year);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }

    }
}
