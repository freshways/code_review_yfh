﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.Business.BLL_Base;


namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class report重性精神疾病患者个人信息补充表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds精神疾病;
        AtomEHR.Business.bll精神疾病信息补充表 _Bll = new Business.bll精神疾病信息补充表();
        string docNo;
        #endregion

        public report重性精神疾病患者个人信息补充表()
        {
            InitializeComponent();
        }

        public report重性精神疾病患者个人信息补充表(string docNo)
        {
            InitializeComponent();
            this.docNo = docNo;
            _ds精神疾病 = _Bll.GetInfoByJSB(docNo, true);
            DoBindingDataSource(_ds精神疾病);
        }

        private void DoBindingDataSource(DataSet _ds精神疾病)
        {
            DataTable dt精神疾病 = _ds精神疾病.Tables[Models.tb_精神疾病信息补充表.__TableName];

            DataTable dt健康档案 = _ds精神疾病.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 != null && dt健康档案.Rows.Count > 0)
            {
                this.txt姓名.Text = dt健康档案.Rows[0][tb_健康档案.姓名].ToString();//util.DESEncrypt.DES解密();
                this.txt户别.Text = dt健康档案.Rows[0][tb_健康档案.档案类别].ToString();
            }

            if (dt精神疾病 != null && dt精神疾病.Rows.Count > 0)
            {
                this.txt监护人姓名.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.监护人姓名].ToString();
                this.txt与患者关系.Text = _Bll.ReturnDis字典显示("yhzgx", dt精神疾病.Rows[0][tb_精神疾病信息补充表.与患者关系].ToString());
                this.txt监护人住址.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.监护人住址].ToString();
                this.txt监护人电话.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.监护人电话].ToString();
                this.txt联系人电话.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.联系人电话].ToString();
                this.txt就业情况.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.就业情况].ToString();
                this.txt知情同意.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.知情同意].ToString();
                this.txt签字.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.签字].ToString();
                this.txt签字时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.签字时间].ToString();
                this.txt初次发病时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.初次发病时间].ToString();
                string 既往主要症状 = dt精神疾病.Rows[0][tb_精神疾病信息补充表.既往主要症状].ToString();
                if (!string.IsNullOrEmpty(既往主要症状))
                {
                    //  1,2,3,4,
                    //[1,2,3,4,]
                    string[] a = 既往主要症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i]))
                        {
                            continue;
                        }
                        string strName = "txt主要症状" + a[i]; //txt主要症状1
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99")
                            lbl.Text = "12";
                        else
                            lbl.Text = a[i];
                    }
                }
                this.txt主要症状其他.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.既往主要症状其他].ToString();
                this.txt门诊治疗.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.门诊状况].ToString();
                this.txt首抗治疗时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.首次抗精神病药治疗时间].ToString();
                this.txt住院次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.住院次数].ToString();
                this.txt诊断.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.诊断].ToString();
                this.txt确诊医院.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.确诊医院].ToString();
                this.txt确诊日期.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.确诊日期].ToString();
                this.txt最近一次治疗效果.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.治疗效果].ToString();
                this.txt轻度滋事次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.轻度滋事].ToString();
                this.txt肇事次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.肇事].ToString();
                this.txt肇祸次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.肇祸].ToString();
                this.txt自伤次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.自伤].ToString();
                this.txt自杀未遂次数.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.自杀未遂].ToString();
                this.txt关锁情况.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.关锁情况].ToString();
                this.txt经济状况.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.经济状况].ToString();
                this.txt医生意见.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.医生意见].ToString();
                this.txt填表日期.Text = Convert.ToDateTime(dt精神疾病.Rows[0][tb_精神疾病信息补充表.检查日期]).ToLongDateString().ToString();
                this.txt医生签字.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.医生签名].ToString();

                //add 公卫改版升级 ▽
                this.txt患者签名.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.患者签字].ToString();
                //add 公卫改版升级 △
            }
        }

    }
}
