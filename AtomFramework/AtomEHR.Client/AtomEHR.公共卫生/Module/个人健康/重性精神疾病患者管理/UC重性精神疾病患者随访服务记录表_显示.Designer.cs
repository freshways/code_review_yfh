﻿namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    partial class UC重性精神疾病患者随访服务记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加随访 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt处置结果 = new DevExpress.XtraEditors.TextEdit();
            this.txt医师电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt医师姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt是否联系精神专科医师 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊联系人 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊科室 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊是否成功 = new DevExpress.XtraEditors.TextEdit();
            this.txt转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt是否需要转诊 = new DevExpress.XtraEditors.TextEdit();
            this.txt受理人2电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt受理人1电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt受理人2姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt受理人1姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt是否通知联席部门 = new DevExpress.XtraEditors.TextEdit();
            this.txt患者或家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt死亡原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt死亡日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt失访原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt本次随访形式 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl考核项 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gridView用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.deleteHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucLblTxtLbl轻度滋事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl肇事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl肇祸 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl其他危害行为 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl自伤 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl自杀未遂 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.lbl无影响 = new DevExpress.XtraEditors.LabelControl();
            this.lbl康复措施 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit转诊机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.lbl是否转诊 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit此次随访分类 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt药物不良反应 = new DevExpress.XtraEditors.TextEdit();
            this.lbl药物不良反应 = new DevExpress.XtraEditors.LabelControl();
            this.lbl服药依从性 = new DevExpress.XtraEditors.LabelControl();
            this.txt实验室检查 = new DevExpress.XtraEditors.TextEdit();
            this.lbl实验室检查 = new DevExpress.XtraEditors.LabelControl();
            this.lbl社会人际交往 = new DevExpress.XtraEditors.LabelControl();
            this.lbl学习能力 = new DevExpress.XtraEditors.LabelControl();
            this.lbl生产劳动及工作 = new DevExpress.XtraEditors.LabelControl();
            this.lbl家务劳动 = new DevExpress.XtraEditors.LabelControl();
            this.lbl个人生活料理 = new DevExpress.XtraEditors.LabelControl();
            this.lbl饮食情况 = new DevExpress.XtraEditors.LabelControl();
            this.txt随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.lbl关锁情况 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.lbl末次住院时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl住院情况 = new DevExpress.XtraEditors.LabelControl();
            this.lbl所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl治疗效果 = new DevExpress.XtraEditors.LabelControl();
            this.lbl目前症状 = new DevExpress.XtraEditors.LabelControl();
            this.lbl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lbl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl个人档案编号 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit危险性 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbl随访日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl自知力 = new DevExpress.XtraEditors.LabelControl();
            this.lbl睡眠情况 = new DevExpress.XtraEditors.LabelControl();
            this.txt下次随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci个人生活料理 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci家务劳动 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci生产劳动 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci学习能力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci社会人际交往 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci关锁情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci住院情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci实验室检查 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci服药依从性 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci药物不良反应 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci治疗效果 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci此次随访分类 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci转诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci影响 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem用药情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci康复措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci危险性 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci自知力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci目前症状 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci睡眠情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci饮食情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt处置结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否联系精神专科医师.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊是否成功.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否需要转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人1电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人1姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否通知联席部门.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者或家属签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本次随访形式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteHyperLinkEdit)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit此次随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实验室检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit危险性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci个人生活料理)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci家务劳动)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci生产劳动)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci学习能力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci社会人际交往)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci关锁情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci住院情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci实验室检查)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci服药依从性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci药物不良反应)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci治疗效果)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci此次随访分类)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci转诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci影响)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci康复措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci危险性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci自知力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci目前症状)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci睡眠情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci饮食情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(112, 500);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(112, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(638, 32);
            this.panelControl1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加随访);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(634, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加随访
            // 
            this.btn添加随访.Location = new System.Drawing.Point(3, 3);
            this.btn添加随访.Name = "btn添加随访";
            this.btn添加随访.Size = new System.Drawing.Size(75, 23);
            this.btn添加随访.TabIndex = 3;
            this.btn添加随访.Text = "添加随访";
            this.btn添加随访.Click += new System.EventHandler(this.btn添加随访_Click);
            // 
            // btn修改
            // 
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 0;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 1;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt处置结果);
            this.layoutControl1.Controls.Add(this.txt医师电话);
            this.layoutControl1.Controls.Add(this.txt医师姓名);
            this.layoutControl1.Controls.Add(this.txt是否联系精神专科医师);
            this.layoutControl1.Controls.Add(this.txt转诊联系电话);
            this.layoutControl1.Controls.Add(this.txt转诊联系人);
            this.layoutControl1.Controls.Add(this.txt转诊科室);
            this.layoutControl1.Controls.Add(this.txt转诊机构);
            this.layoutControl1.Controls.Add(this.txt转诊是否成功);
            this.layoutControl1.Controls.Add(this.txt转诊原因);
            this.layoutControl1.Controls.Add(this.txt是否需要转诊);
            this.layoutControl1.Controls.Add(this.txt受理人2电话);
            this.layoutControl1.Controls.Add(this.txt受理人1电话);
            this.layoutControl1.Controls.Add(this.txt受理人2姓名);
            this.layoutControl1.Controls.Add(this.txt受理人1姓名);
            this.layoutControl1.Controls.Add(this.txt是否通知联席部门);
            this.layoutControl1.Controls.Add(this.txt患者或家属签名);
            this.layoutControl1.Controls.Add(this.txt死亡原因);
            this.layoutControl1.Controls.Add(this.txt死亡日期);
            this.layoutControl1.Controls.Add(this.txt失访原因);
            this.layoutControl1.Controls.Add(this.txt本次随访形式);
            this.layoutControl1.Controls.Add(this.labelControl考核项);
            this.layoutControl1.Controls.Add(this.gridControl用药情况);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.lbl康复措施);
            this.layoutControl1.Controls.Add(this.textEdit转诊机构);
            this.layoutControl1.Controls.Add(this.textEdit转诊原因);
            this.layoutControl1.Controls.Add(this.lbl是否转诊);
            this.layoutControl1.Controls.Add(this.comboBoxEdit此次随访分类);
            this.layoutControl1.Controls.Add(this.txt药物不良反应);
            this.layoutControl1.Controls.Add(this.lbl药物不良反应);
            this.layoutControl1.Controls.Add(this.lbl服药依从性);
            this.layoutControl1.Controls.Add(this.txt实验室检查);
            this.layoutControl1.Controls.Add(this.lbl实验室检查);
            this.layoutControl1.Controls.Add(this.lbl社会人际交往);
            this.layoutControl1.Controls.Add(this.lbl学习能力);
            this.layoutControl1.Controls.Add(this.lbl生产劳动及工作);
            this.layoutControl1.Controls.Add(this.lbl家务劳动);
            this.layoutControl1.Controls.Add(this.lbl个人生活料理);
            this.layoutControl1.Controls.Add(this.lbl饮食情况);
            this.layoutControl1.Controls.Add(this.txt随访医生);
            this.layoutControl1.Controls.Add(this.lbl关锁情况);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.lbl所属机构);
            this.layoutControl1.Controls.Add(this.lbl创建机构);
            this.layoutControl1.Controls.Add(this.lbl最近修改人);
            this.layoutControl1.Controls.Add(this.lbl创建人);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.lbl治疗效果);
            this.layoutControl1.Controls.Add(this.lbl目前症状);
            this.layoutControl1.Controls.Add(this.lbl居住地址);
            this.layoutControl1.Controls.Add(this.lbl联系电话);
            this.layoutControl1.Controls.Add(this.lbl婚姻状况);
            this.layoutControl1.Controls.Add(this.lbl身份证号);
            this.layoutControl1.Controls.Add(this.lbl出生日期);
            this.layoutControl1.Controls.Add(this.lbl性别);
            this.layoutControl1.Controls.Add(this.lbl姓名);
            this.layoutControl1.Controls.Add(this.lbl个人档案编号);
            this.layoutControl1.Controls.Add(this.comboBoxEdit危险性);
            this.layoutControl1.Controls.Add(this.lbl随访日期);
            this.layoutControl1.Controls.Add(this.lbl自知力);
            this.layoutControl1.Controls.Add(this.lbl睡眠情况);
            this.layoutControl1.Controls.Add(this.txt下次随访日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(112, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(638, 468);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt处置结果
            // 
            this.txt处置结果.Location = new System.Drawing.Point(118, 874);
            this.txt处置结果.Name = "txt处置结果";
            this.txt处置结果.Properties.ReadOnly = true;
            this.txt处置结果.Size = new System.Drawing.Size(500, 20);
            this.txt处置结果.StyleController = this.layoutControl1;
            this.txt处置结果.TabIndex = 114;
            // 
            // txt医师电话
            // 
            this.txt医师电话.Location = new System.Drawing.Point(505, 850);
            this.txt医师电话.Name = "txt医师电话";
            this.txt医师电话.Properties.ReadOnly = true;
            this.txt医师电话.Size = new System.Drawing.Size(113, 20);
            this.txt医师电话.StyleController = this.layoutControl1;
            this.txt医师电话.TabIndex = 113;
            // 
            // txt医师姓名
            // 
            this.txt医师姓名.Location = new System.Drawing.Point(292, 850);
            this.txt医师姓名.Name = "txt医师姓名";
            this.txt医师姓名.Properties.ReadOnly = true;
            this.txt医师姓名.Size = new System.Drawing.Size(104, 20);
            this.txt医师姓名.StyleController = this.layoutControl1;
            this.txt医师姓名.TabIndex = 112;
            // 
            // txt是否联系精神专科医师
            // 
            this.txt是否联系精神专科医师.Location = new System.Drawing.Point(133, 850);
            this.txt是否联系精神专科医师.Name = "txt是否联系精神专科医师";
            this.txt是否联系精神专科医师.Properties.ReadOnly = true;
            this.txt是否联系精神专科医师.Size = new System.Drawing.Size(50, 20);
            this.txt是否联系精神专科医师.StyleController = this.layoutControl1;
            this.txt是否联系精神专科医师.TabIndex = 111;
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Location = new System.Drawing.Point(504, 826);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Properties.ReadOnly = true;
            this.txt转诊联系电话.Size = new System.Drawing.Size(114, 20);
            this.txt转诊联系电话.StyleController = this.layoutControl1;
            this.txt转诊联系电话.TabIndex = 110;
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Location = new System.Drawing.Point(292, 826);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Properties.ReadOnly = true;
            this.txt转诊联系人.Size = new System.Drawing.Size(103, 20);
            this.txt转诊联系人.StyleController = this.layoutControl1;
            this.txt转诊联系人.TabIndex = 109;
            // 
            // txt转诊科室
            // 
            this.txt转诊科室.Location = new System.Drawing.Point(504, 802);
            this.txt转诊科室.Name = "txt转诊科室";
            this.txt转诊科室.Properties.ReadOnly = true;
            this.txt转诊科室.Size = new System.Drawing.Size(114, 20);
            this.txt转诊科室.StyleController = this.layoutControl1;
            this.txt转诊科室.TabIndex = 108;
            // 
            // txt转诊机构
            // 
            this.txt转诊机构.Location = new System.Drawing.Point(292, 802);
            this.txt转诊机构.Name = "txt转诊机构";
            this.txt转诊机构.Properties.ReadOnly = true;
            this.txt转诊机构.Size = new System.Drawing.Size(103, 20);
            this.txt转诊机构.StyleController = this.layoutControl1;
            this.txt转诊机构.TabIndex = 107;
            // 
            // txt转诊是否成功
            // 
            this.txt转诊是否成功.Location = new System.Drawing.Point(118, 802);
            this.txt转诊是否成功.Name = "txt转诊是否成功";
            this.txt转诊是否成功.Properties.ReadOnly = true;
            this.txt转诊是否成功.Size = new System.Drawing.Size(65, 20);
            this.txt转诊是否成功.StyleController = this.layoutControl1;
            this.txt转诊是否成功.TabIndex = 106;
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Location = new System.Drawing.Point(292, 778);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Properties.ReadOnly = true;
            this.txt转诊原因.Size = new System.Drawing.Size(326, 20);
            this.txt转诊原因.StyleController = this.layoutControl1;
            this.txt转诊原因.TabIndex = 105;
            // 
            // txt是否需要转诊
            // 
            this.txt是否需要转诊.Location = new System.Drawing.Point(118, 778);
            this.txt是否需要转诊.Name = "txt是否需要转诊";
            this.txt是否需要转诊.Properties.ReadOnly = true;
            this.txt是否需要转诊.Size = new System.Drawing.Size(65, 20);
            this.txt是否需要转诊.StyleController = this.layoutControl1;
            this.txt是否需要转诊.TabIndex = 104;
            // 
            // txt受理人2电话
            // 
            this.txt受理人2电话.Location = new System.Drawing.Point(249, 754);
            this.txt受理人2电话.Name = "txt受理人2电话";
            this.txt受理人2电话.Properties.ReadOnly = true;
            this.txt受理人2电话.Size = new System.Drawing.Size(147, 20);
            this.txt受理人2电话.StyleController = this.layoutControl1;
            this.txt受理人2电话.TabIndex = 103;
            // 
            // txt受理人1电话
            // 
            this.txt受理人1电话.Location = new System.Drawing.Point(118, 754);
            this.txt受理人1电话.Name = "txt受理人1电话";
            this.txt受理人1电话.Properties.ReadOnly = true;
            this.txt受理人1电话.Size = new System.Drawing.Size(127, 20);
            this.txt受理人1电话.StyleController = this.layoutControl1;
            this.txt受理人1电话.TabIndex = 102;
            // 
            // txt受理人2姓名
            // 
            this.txt受理人2姓名.Location = new System.Drawing.Point(502, 730);
            this.txt受理人2姓名.Name = "txt受理人2姓名";
            this.txt受理人2姓名.Properties.ReadOnly = true;
            this.txt受理人2姓名.Size = new System.Drawing.Size(116, 20);
            this.txt受理人2姓名.StyleController = this.layoutControl1;
            this.txt受理人2姓名.TabIndex = 101;
            // 
            // txt受理人1姓名
            // 
            this.txt受理人1姓名.Location = new System.Drawing.Point(387, 730);
            this.txt受理人1姓名.Name = "txt受理人1姓名";
            this.txt受理人1姓名.Properties.ReadOnly = true;
            this.txt受理人1姓名.Size = new System.Drawing.Size(111, 20);
            this.txt受理人1姓名.StyleController = this.layoutControl1;
            this.txt受理人1姓名.TabIndex = 100;
            // 
            // txt是否通知联席部门
            // 
            this.txt是否通知联席部门.Location = new System.Drawing.Point(118, 730);
            this.txt是否通知联席部门.Name = "txt是否通知联席部门";
            this.txt是否通知联席部门.Properties.ReadOnly = true;
            this.txt是否通知联席部门.Size = new System.Drawing.Size(65, 20);
            this.txt是否通知联席部门.StyleController = this.layoutControl1;
            this.txt是否通知联席部门.TabIndex = 99;
            // 
            // txt患者或家属签名
            // 
            this.txt患者或家属签名.Location = new System.Drawing.Point(531, 922);
            this.txt患者或家属签名.Name = "txt患者或家属签名";
            this.txt患者或家属签名.Properties.ReadOnly = true;
            this.txt患者或家属签名.Size = new System.Drawing.Size(87, 20);
            this.txt患者或家属签名.StyleController = this.layoutControl1;
            this.txt患者或家属签名.TabIndex = 98;
            // 
            // txt死亡原因
            // 
            this.txt死亡原因.Location = new System.Drawing.Point(204, 236);
            this.txt死亡原因.Name = "txt死亡原因";
            this.txt死亡原因.Properties.ReadOnly = true;
            this.txt死亡原因.Size = new System.Drawing.Size(414, 20);
            this.txt死亡原因.StyleController = this.layoutControl1;
            this.txt死亡原因.TabIndex = 97;
            // 
            // txt死亡日期
            // 
            this.txt死亡日期.Location = new System.Drawing.Point(204, 212);
            this.txt死亡日期.Name = "txt死亡日期";
            this.txt死亡日期.Properties.ReadOnly = true;
            this.txt死亡日期.Size = new System.Drawing.Size(102, 20);
            this.txt死亡日期.StyleController = this.layoutControl1;
            this.txt死亡日期.TabIndex = 96;
            // 
            // txt失访原因
            // 
            this.txt失访原因.Location = new System.Drawing.Point(404, 188);
            this.txt失访原因.Name = "txt失访原因";
            this.txt失访原因.Properties.ReadOnly = true;
            this.txt失访原因.Size = new System.Drawing.Size(214, 20);
            this.txt失访原因.StyleController = this.layoutControl1;
            this.txt失访原因.TabIndex = 95;
            // 
            // txt本次随访形式
            // 
            this.txt本次随访形式.Location = new System.Drawing.Point(118, 188);
            this.txt本次随访形式.Name = "txt本次随访形式";
            this.txt本次随访形式.Properties.ReadOnly = true;
            this.txt本次随访形式.Size = new System.Drawing.Size(187, 20);
            this.txt本次随访形式.StyleController = this.layoutControl1;
            this.txt本次随访形式.TabIndex = 94;
            // 
            // labelControl考核项
            // 
            this.labelControl考核项.Location = new System.Drawing.Point(3, 30);
            this.labelControl考核项.Name = "labelControl考核项";
            this.labelControl考核项.Size = new System.Drawing.Size(70, 14);
            this.labelControl考核项.StyleController = this.layoutControl1;
            this.labelControl考核项.TabIndex = 93;
            this.labelControl考核项.Text = "labelControl2";
            // 
            // gridControl用药情况
            // 
            this.gridControl用药情况.Location = new System.Drawing.Point(120, 700);
            this.gridControl用药情况.MainView = this.gridView用药情况;
            this.gridControl用药情况.Name = "gridControl用药情况";
            this.gridControl用药情况.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.deleteHyperLinkEdit});
            this.gridControl用药情况.Size = new System.Drawing.Size(498, 26);
            this.gridControl用药情况.TabIndex = 92;
            this.gridControl用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView用药情况});
            // 
            // gridView用药情况
            // 
            this.gridView用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView用药情况.GridControl = this.gridControl用药情况;
            this.gridView用药情况.Name = "gridView用药情况";
            this.gridView用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "剂量";
            this.gridColumn2.FieldName = "剂量";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "用法";
            this.gridColumn3.FieldName = "用法";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // deleteHyperLinkEdit
            // 
            this.deleteHyperLinkEdit.AutoHeight = false;
            this.deleteHyperLinkEdit.Name = "deleteHyperLinkEdit";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl轻度滋事);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl肇事);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl肇祸);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl其他危害行为);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl自伤);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl自杀未遂);
            this.flowLayoutPanel2.Controls.Add(this.lbl无影响);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(118, 458);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(500, 44);
            this.flowLayoutPanel2.TabIndex = 91;
            // 
            // ucLblTxtLbl轻度滋事
            // 
            this.ucLblTxtLbl轻度滋事.Lbl1Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl轻度滋事.Lbl1Text = "轻度滋事";
            this.ucLblTxtLbl轻度滋事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl轻度滋事.Lbl2Text = "次";
            this.ucLblTxtLbl轻度滋事.Location = new System.Drawing.Point(0, 0);
            this.ucLblTxtLbl轻度滋事.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl轻度滋事.Name = "ucLblTxtLbl轻度滋事";
            this.ucLblTxtLbl轻度滋事.Size = new System.Drawing.Size(122, 22);
            this.ucLblTxtLbl轻度滋事.TabIndex = 3;
            this.ucLblTxtLbl轻度滋事.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // ucLblTxtLbl肇事
            // 
            this.ucLblTxtLbl肇事.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl肇事.Lbl1Text = "肇事";
            this.ucLblTxtLbl肇事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl肇事.Lbl2Text = "次";
            this.ucLblTxtLbl肇事.Location = new System.Drawing.Point(122, 0);
            this.ucLblTxtLbl肇事.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl肇事.Name = "ucLblTxtLbl肇事";
            this.ucLblTxtLbl肇事.Size = new System.Drawing.Size(98, 22);
            this.ucLblTxtLbl肇事.TabIndex = 4;
            this.ucLblTxtLbl肇事.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // ucLblTxtLbl肇祸
            // 
            this.ucLblTxtLbl肇祸.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl肇祸.Lbl1Text = "肇祸";
            this.ucLblTxtLbl肇祸.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl肇祸.Lbl2Text = "次";
            this.ucLblTxtLbl肇祸.Location = new System.Drawing.Point(220, 0);
            this.ucLblTxtLbl肇祸.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl肇祸.Name = "ucLblTxtLbl肇祸";
            this.ucLblTxtLbl肇祸.Size = new System.Drawing.Size(102, 22);
            this.ucLblTxtLbl肇祸.TabIndex = 6;
            this.ucLblTxtLbl肇祸.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // ucLblTxtLbl其他危害行为
            // 
            this.ucLblTxtLbl其他危害行为.Lbl1Size = new System.Drawing.Size(75, 18);
            this.ucLblTxtLbl其他危害行为.Lbl1Text = "其他危害行为";
            this.ucLblTxtLbl其他危害行为.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl其他危害行为.Lbl2Text = "次";
            this.ucLblTxtLbl其他危害行为.Location = new System.Drawing.Point(322, 0);
            this.ucLblTxtLbl其他危害行为.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl其他危害行为.Name = "ucLblTxtLbl其他危害行为";
            this.ucLblTxtLbl其他危害行为.Size = new System.Drawing.Size(141, 22);
            this.ucLblTxtLbl其他危害行为.TabIndex = 13;
            this.ucLblTxtLbl其他危害行为.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // ucLblTxtLbl自伤
            // 
            this.ucLblTxtLbl自伤.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl自伤.Lbl1Text = "自伤";
            this.ucLblTxtLbl自伤.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl自伤.Lbl2Text = "次";
            this.ucLblTxtLbl自伤.Location = new System.Drawing.Point(0, 22);
            this.ucLblTxtLbl自伤.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl自伤.Name = "ucLblTxtLbl自伤";
            this.ucLblTxtLbl自伤.Size = new System.Drawing.Size(105, 22);
            this.ucLblTxtLbl自伤.TabIndex = 8;
            this.ucLblTxtLbl自伤.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // ucLblTxtLbl自杀未遂
            // 
            this.ucLblTxtLbl自杀未遂.Lbl1Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl自杀未遂.Lbl1Text = "自杀未遂";
            this.ucLblTxtLbl自杀未遂.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl自杀未遂.Lbl2Text = "次";
            this.ucLblTxtLbl自杀未遂.Location = new System.Drawing.Point(105, 22);
            this.ucLblTxtLbl自杀未遂.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl自杀未遂.Name = "ucLblTxtLbl自杀未遂";
            this.ucLblTxtLbl自杀未遂.Size = new System.Drawing.Size(124, 22);
            this.ucLblTxtLbl自杀未遂.TabIndex = 10;
            this.ucLblTxtLbl自杀未遂.Txt1Size = new System.Drawing.Size(45, 20);
            // 
            // lbl无影响
            // 
            this.lbl无影响.Location = new System.Drawing.Point(232, 25);
            this.lbl无影响.Name = "lbl无影响";
            this.lbl无影响.Size = new System.Drawing.Size(12, 14);
            this.lbl无影响.TabIndex = 12;
            this.lbl无影响.Text = "无";
            // 
            // lbl康复措施
            // 
            this.lbl康复措施.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl康复措施.Location = new System.Drawing.Point(118, 652);
            this.lbl康复措施.Name = "lbl康复措施";
            this.lbl康复措施.Size = new System.Drawing.Size(500, 20);
            this.lbl康复措施.StyleController = this.layoutControl1;
            this.lbl康复措施.TabIndex = 90;
            // 
            // textEdit转诊机构
            // 
            this.textEdit转诊机构.Location = new System.Drawing.Point(530, 898);
            this.textEdit转诊机构.Name = "textEdit转诊机构";
            this.textEdit转诊机构.Properties.ReadOnly = true;
            this.textEdit转诊机构.Size = new System.Drawing.Size(88, 20);
            this.textEdit转诊机构.StyleController = this.layoutControl1;
            this.textEdit转诊机构.TabIndex = 88;
            // 
            // textEdit转诊原因
            // 
            this.textEdit转诊原因.Location = new System.Drawing.Point(303, 898);
            this.textEdit转诊原因.Name = "textEdit转诊原因";
            this.textEdit转诊原因.Properties.ReadOnly = true;
            this.textEdit转诊原因.Size = new System.Drawing.Size(128, 20);
            this.textEdit转诊原因.StyleController = this.layoutControl1;
            this.textEdit转诊原因.TabIndex = 87;
            // 
            // lbl是否转诊
            // 
            this.lbl是否转诊.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl是否转诊.Location = new System.Drawing.Point(118, 898);
            this.lbl是否转诊.Name = "lbl是否转诊";
            this.lbl是否转诊.Size = new System.Drawing.Size(126, 20);
            this.lbl是否转诊.StyleController = this.layoutControl1;
            this.lbl是否转诊.TabIndex = 86;
            // 
            // comboBoxEdit此次随访分类
            // 
            this.comboBoxEdit此次随访分类.Location = new System.Drawing.Point(118, 676);
            this.comboBoxEdit此次随访分类.Name = "comboBoxEdit此次随访分类";
            this.comboBoxEdit此次随访分类.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.comboBoxEdit此次随访分类.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit此次随访分类.Properties.ReadOnly = true;
            this.comboBoxEdit此次随访分类.Size = new System.Drawing.Size(126, 20);
            this.comboBoxEdit此次随访分类.StyleController = this.layoutControl1;
            this.comboBoxEdit此次随访分类.TabIndex = 85;
            // 
            // txt药物不良反应
            // 
            this.txt药物不良反应.Location = new System.Drawing.Point(248, 602);
            this.txt药物不良反应.Name = "txt药物不良反应";
            this.txt药物不良反应.Properties.ReadOnly = true;
            this.txt药物不良反应.Size = new System.Drawing.Size(370, 20);
            this.txt药物不良反应.StyleController = this.layoutControl1;
            this.txt药物不良反应.TabIndex = 84;
            // 
            // lbl药物不良反应
            // 
            this.lbl药物不良反应.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl药物不良反应.Location = new System.Drawing.Point(118, 602);
            this.lbl药物不良反应.Name = "lbl药物不良反应";
            this.lbl药物不良反应.Size = new System.Drawing.Size(126, 22);
            this.lbl药物不良反应.StyleController = this.layoutControl1;
            this.lbl药物不良反应.TabIndex = 83;
            // 
            // lbl服药依从性
            // 
            this.lbl服药依从性.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl服药依从性.Location = new System.Drawing.Point(118, 580);
            this.lbl服药依从性.Name = "lbl服药依从性";
            this.lbl服药依从性.Size = new System.Drawing.Size(500, 18);
            this.lbl服药依从性.StyleController = this.layoutControl1;
            this.lbl服药依从性.TabIndex = 82;
            // 
            // txt实验室检查
            // 
            this.txt实验室检查.Location = new System.Drawing.Point(192, 556);
            this.txt实验室检查.Name = "txt实验室检查";
            this.txt实验室检查.Properties.ReadOnly = true;
            this.txt实验室检查.Size = new System.Drawing.Size(426, 20);
            this.txt实验室检查.StyleController = this.layoutControl1;
            this.txt实验室检查.TabIndex = 81;
            // 
            // lbl实验室检查
            // 
            this.lbl实验室检查.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl实验室检查.Location = new System.Drawing.Point(118, 556);
            this.lbl实验室检查.Name = "lbl实验室检查";
            this.lbl实验室检查.Size = new System.Drawing.Size(70, 20);
            this.lbl实验室检查.StyleController = this.layoutControl1;
            this.lbl实验室检查.TabIndex = 80;
            // 
            // lbl社会人际交往
            // 
            this.lbl社会人际交往.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl社会人际交往.Location = new System.Drawing.Point(207, 436);
            this.lbl社会人际交往.Name = "lbl社会人际交往";
            this.lbl社会人际交往.Size = new System.Drawing.Size(411, 18);
            this.lbl社会人际交往.StyleController = this.layoutControl1;
            this.lbl社会人际交往.TabIndex = 79;
            // 
            // lbl学习能力
            // 
            this.lbl学习能力.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl学习能力.Location = new System.Drawing.Point(207, 414);
            this.lbl学习能力.Name = "lbl学习能力";
            this.lbl学习能力.Size = new System.Drawing.Size(411, 18);
            this.lbl学习能力.StyleController = this.layoutControl1;
            this.lbl学习能力.TabIndex = 78;
            // 
            // lbl生产劳动及工作
            // 
            this.lbl生产劳动及工作.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl生产劳动及工作.Location = new System.Drawing.Point(207, 392);
            this.lbl生产劳动及工作.Name = "lbl生产劳动及工作";
            this.lbl生产劳动及工作.Size = new System.Drawing.Size(411, 18);
            this.lbl生产劳动及工作.StyleController = this.layoutControl1;
            this.lbl生产劳动及工作.TabIndex = 77;
            // 
            // lbl家务劳动
            // 
            this.lbl家务劳动.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl家务劳动.Location = new System.Drawing.Point(207, 370);
            this.lbl家务劳动.Name = "lbl家务劳动";
            this.lbl家务劳动.Size = new System.Drawing.Size(411, 18);
            this.lbl家务劳动.StyleController = this.layoutControl1;
            this.lbl家务劳动.TabIndex = 76;
            // 
            // lbl个人生活料理
            // 
            this.lbl个人生活料理.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl个人生活料理.Location = new System.Drawing.Point(207, 348);
            this.lbl个人生活料理.Name = "lbl个人生活料理";
            this.lbl个人生活料理.Size = new System.Drawing.Size(411, 18);
            this.lbl个人生活料理.StyleController = this.layoutControl1;
            this.lbl个人生活料理.TabIndex = 75;
            // 
            // lbl饮食情况
            // 
            this.lbl饮食情况.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl饮食情况.Location = new System.Drawing.Point(118, 326);
            this.lbl饮食情况.Name = "lbl饮食情况";
            this.lbl饮食情况.Size = new System.Drawing.Size(500, 18);
            this.lbl饮食情况.StyleController = this.layoutControl1;
            this.lbl饮食情况.TabIndex = 74;
            // 
            // txt随访医生
            // 
            this.txt随访医生.Location = new System.Drawing.Point(324, 922);
            this.txt随访医生.Name = "txt随访医生";
            this.txt随访医生.Properties.ReadOnly = true;
            this.txt随访医生.Size = new System.Drawing.Size(88, 20);
            this.txt随访医生.StyleController = this.layoutControl1;
            this.txt随访医生.TabIndex = 73;
            // 
            // lbl关锁情况
            // 
            this.lbl关锁情况.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl关锁情况.Location = new System.Drawing.Point(118, 506);
            this.lbl关锁情况.Name = "lbl关锁情况";
            this.lbl关锁情况.Size = new System.Drawing.Size(126, 18);
            this.lbl关锁情况.StyleController = this.layoutControl1;
            this.lbl关锁情况.TabIndex = 37;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.lbl末次住院时间);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.lbl住院情况);
            this.panelControl2.Location = new System.Drawing.Point(118, 528);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(500, 24);
            this.panelControl2.TabIndex = 69;
            // 
            // lbl末次住院时间
            // 
            this.lbl末次住院时间.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl末次住院时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl末次住院时间.Location = new System.Drawing.Point(239, 0);
            this.lbl末次住院时间.Name = "lbl末次住院时间";
            this.lbl末次住院时间.Size = new System.Drawing.Size(135, 21);
            this.lbl末次住院时间.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(150, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "末次出院时间：";
            // 
            // lbl住院情况
            // 
            this.lbl住院情况.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl住院情况.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl住院情况.Location = new System.Drawing.Point(3, 1);
            this.lbl住院情况.Name = "lbl住院情况";
            this.lbl住院情况.Size = new System.Drawing.Size(136, 20);
            this.lbl住院情况.TabIndex = 0;
            // 
            // lbl所属机构
            // 
            this.lbl所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl所属机构.Location = new System.Drawing.Point(98, 971);
            this.lbl所属机构.Name = "lbl所属机构";
            this.lbl所属机构.Size = new System.Drawing.Size(196, 20);
            this.lbl所属机构.StyleController = this.layoutControl1;
            this.lbl所属机构.TabIndex = 51;
            this.lbl所属机构.Text = "labelControl16";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(393, 971);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(225, 20);
            this.lbl创建机构.StyleController = this.layoutControl1;
            this.lbl创建机构.TabIndex = 50;
            this.lbl创建机构.Text = "labelControl15";
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近修改人.Location = new System.Drawing.Point(393, 995);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(225, 20);
            this.lbl最近修改人.StyleController = this.layoutControl1;
            this.lbl最近修改人.TabIndex = 49;
            this.lbl最近修改人.Text = "labelControl14";
            // 
            // lbl创建人
            // 
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(98, 995);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(196, 20);
            this.lbl创建人.StyleController = this.layoutControl1;
            this.lbl创建人.TabIndex = 48;
            this.lbl创建人.Text = "labelControl13";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(393, 947);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(225, 20);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 47;
            this.lbl最近更新时间.Text = "labelControl12";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(98, 947);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(196, 20);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "labelControl11";
            // 
            // lbl治疗效果
            // 
            this.lbl治疗效果.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl治疗效果.Location = new System.Drawing.Point(118, 628);
            this.lbl治疗效果.Name = "lbl治疗效果";
            this.lbl治疗效果.Size = new System.Drawing.Size(500, 20);
            this.lbl治疗效果.StyleController = this.layoutControl1;
            this.lbl治疗效果.TabIndex = 42;
            // 
            // lbl目前症状
            // 
            this.lbl目前症状.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl目前症状.Location = new System.Drawing.Point(118, 260);
            this.lbl目前症状.Name = "lbl目前症状";
            this.lbl目前症状.Size = new System.Drawing.Size(500, 18);
            this.lbl目前症状.StyleController = this.layoutControl1;
            this.lbl目前症状.TabIndex = 41;
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl居住地址.Location = new System.Drawing.Point(118, 126);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(187, 36);
            this.lbl居住地址.StyleController = this.layoutControl1;
            this.lbl居住地址.TabIndex = 12;
            this.lbl居住地址.Text = "labelControl9";
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl联系电话.Location = new System.Drawing.Point(404, 100);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(214, 22);
            this.lbl联系电话.StyleController = this.layoutControl1;
            this.lbl联系电话.TabIndex = 11;
            this.lbl联系电话.Text = "labelControl8";
            // 
            // lbl婚姻状况
            // 
            this.lbl婚姻状况.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl婚姻状况.Location = new System.Drawing.Point(404, 126);
            this.lbl婚姻状况.Name = "lbl婚姻状况";
            this.lbl婚姻状况.Size = new System.Drawing.Size(214, 36);
            this.lbl婚姻状况.StyleController = this.layoutControl1;
            this.lbl婚姻状况.TabIndex = 10;
            this.lbl婚姻状况.Text = "labelControl7";
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl身份证号.Location = new System.Drawing.Point(404, 74);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(214, 22);
            this.lbl身份证号.StyleController = this.layoutControl1;
            this.lbl身份证号.TabIndex = 9;
            this.lbl身份证号.Text = "labelControl6";
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl出生日期.Location = new System.Drawing.Point(118, 100);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(187, 22);
            this.lbl出生日期.StyleController = this.layoutControl1;
            this.lbl出生日期.TabIndex = 8;
            this.lbl出生日期.Text = "labelControl5";
            // 
            // lbl性别
            // 
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl性别.Location = new System.Drawing.Point(118, 74);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(187, 22);
            this.lbl性别.StyleController = this.layoutControl1;
            this.lbl性别.TabIndex = 7;
            this.lbl性别.Text = "labelControl4";
            // 
            // lbl姓名
            // 
            this.lbl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl姓名.Location = new System.Drawing.Point(404, 48);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(214, 22);
            this.lbl姓名.StyleController = this.layoutControl1;
            this.lbl姓名.TabIndex = 6;
            this.lbl姓名.Text = "labelControl3";
            // 
            // lbl个人档案编号
            // 
            this.lbl个人档案编号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl个人档案编号.Location = new System.Drawing.Point(118, 48);
            this.lbl个人档案编号.Name = "lbl个人档案编号";
            this.lbl个人档案编号.Size = new System.Drawing.Size(187, 22);
            this.lbl个人档案编号.StyleController = this.layoutControl1;
            this.lbl个人档案编号.TabIndex = 5;
            this.lbl个人档案编号.Text = "labelControl2";
            // 
            // comboBoxEdit危险性
            // 
            this.comboBoxEdit危险性.Location = new System.Drawing.Point(404, 166);
            this.comboBoxEdit危险性.Name = "comboBoxEdit危险性";
            this.comboBoxEdit危险性.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit危险性.Properties.ReadOnly = true;
            this.comboBoxEdit危险性.Size = new System.Drawing.Size(214, 20);
            this.comboBoxEdit危险性.StyleController = this.layoutControl1;
            this.comboBoxEdit危险性.TabIndex = 15;
            // 
            // lbl随访日期
            // 
            this.lbl随访日期.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl随访日期.Location = new System.Drawing.Point(118, 166);
            this.lbl随访日期.Name = "lbl随访日期";
            this.lbl随访日期.Size = new System.Drawing.Size(187, 18);
            this.lbl随访日期.StyleController = this.layoutControl1;
            this.lbl随访日期.TabIndex = 17;
            // 
            // lbl自知力
            // 
            this.lbl自知力.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl自知力.Location = new System.Drawing.Point(118, 282);
            this.lbl自知力.Name = "lbl自知力";
            this.lbl自知力.Size = new System.Drawing.Size(500, 18);
            this.lbl自知力.StyleController = this.layoutControl1;
            this.lbl自知力.TabIndex = 16;
            // 
            // lbl睡眠情况
            // 
            this.lbl睡眠情况.Appearance.BackColor = System.Drawing.Color.White;
            this.lbl睡眠情况.Location = new System.Drawing.Point(118, 304);
            this.lbl睡眠情况.Name = "lbl睡眠情况";
            this.lbl睡眠情况.Size = new System.Drawing.Size(500, 18);
            this.lbl睡眠情况.StyleController = this.layoutControl1;
            this.lbl睡眠情况.TabIndex = 63;
            // 
            // txt下次随访日期
            // 
            this.txt下次随访日期.Location = new System.Drawing.Point(118, 922);
            this.txt下次随访日期.Name = "txt下次随访日期";
            this.txt下次随访日期.Properties.Mask.EditMask = "d";
            this.txt下次随访日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txt下次随访日期.Properties.ReadOnly = true;
            this.txt下次随访日期.Size = new System.Drawing.Size(97, 20);
            this.txt下次随访日期.StyleController = this.layoutControl1;
            this.txt下次随访日期.TabIndex = 43;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "冠心病患者管理卡";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(621, 1018);
            this.layoutControlGroup1.Text = "重性精神疾病患者随访服务记录表 ";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.lci随访日期,
            this.layoutControlGroup5,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.lci危险性,
            this.lci自知力,
            this.lci目前症状,
            this.lci睡眠情况,
            this.lci饮食情况,
            this.layoutControlItem21,
            this.layoutControlItem1,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem5,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(619, 989);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.lbl居住地址;
            this.layoutControlItem9.CustomizationFormText = "居住地址";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(111, 40);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(306, 40);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "居住地址";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.lbl姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(306, 18);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(313, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.lbl个人档案编号;
            this.layoutControlItem2.CustomizationFormText = "个人档案号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 18);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(306, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "个人档案号";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.lbl性别;
            this.layoutControlItem4.CustomizationFormText = "些别";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(306, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "性别";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // lci随访日期
            // 
            this.lci随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lci随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci随访日期.Control = this.lbl随访日期;
            this.lci随访日期.CustomizationFormText = "随访日期*";
            this.lci随访日期.Location = new System.Drawing.Point(0, 136);
            this.lci随访日期.MinSize = new System.Drawing.Size(50, 22);
            this.lci随访日期.Name = "lci随访日期";
            this.lci随访日期.Size = new System.Drawing.Size(306, 22);
            this.lci随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci随访日期.Text = "随访日期";
            this.lci随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci随访日期.TextSize = new System.Drawing.Size(110, 20);
            this.lci随访日期.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.emptySpaceItem2,
            this.lci随访医生,
            this.layoutControlItem48,
            this.layoutControlItem47,
            this.layoutControlItem46,
            this.layoutControlItem45,
            this.lci个人生活料理,
            this.lci家务劳动,
            this.lci生产劳动,
            this.lci学习能力,
            this.lci社会人际交往,
            this.lci关锁情况,
            this.emptySpaceItem1,
            this.lci住院情况,
            this.lci实验室检查,
            this.layoutControlItem16,
            this.lci服药依从性,
            this.lci药物不良反应,
            this.layoutControlItem29,
            this.lci治疗效果,
            this.lci此次随访分类,
            this.emptySpaceItem3,
            this.lci转诊,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.lci影响,
            this.layoutControlItem13,
            this.lci下次随访日期,
            this.layoutControlItem用药情况,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.emptySpaceItem6,
            this.layoutControlItem20,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem34,
            this.emptySpaceItem7,
            this.lci康复措施});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 318);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(619, 671);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.lbl创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 599);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "创建时间：";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.lbl最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(295, 599);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间：";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem2.CustomizationFormText = "知情同意";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(99, 110);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "社会功能情况";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(110, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // lci随访医生
            // 
            this.lci随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lci随访医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.lci随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci随访医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci随访医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci随访医生.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci随访医生.Control = this.txt随访医生;
            this.lci随访医生.CustomizationFormText = "随访医生签名";
            this.lci随访医生.Location = new System.Drawing.Point(216, 574);
            this.lci随访医生.MaxSize = new System.Drawing.Size(0, 24);
            this.lci随访医生.MinSize = new System.Drawing.Size(117, 24);
            this.lci随访医生.Name = "lci随访医生";
            this.lci随访医生.Size = new System.Drawing.Size(197, 25);
            this.lci随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci随访医生.Text = "随访医生签名";
            this.lci随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci随访医生.TextSize = new System.Drawing.Size(100, 20);
            this.lci随访医生.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.lbl所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 623);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构：";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.lbl创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(295, 623);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构：";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.lbl最近修改人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(295, 647);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近修改人：";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.lbl创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 647);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "创建人：";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // lci个人生活料理
            // 
            this.lci个人生活料理.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci个人生活料理.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci个人生活料理.AppearanceItemCaption.Options.UseFont = true;
            this.lci个人生活料理.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci个人生活料理.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci个人生活料理.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci个人生活料理.Control = this.lbl个人生活料理;
            this.lci个人生活料理.CustomizationFormText = "个人生活料理";
            this.lci个人生活料理.Location = new System.Drawing.Point(99, 0);
            this.lci个人生活料理.MinSize = new System.Drawing.Size(141, 22);
            this.lci个人生活料理.Name = "lci个人生活料理";
            this.lci个人生活料理.Size = new System.Drawing.Size(520, 22);
            this.lci个人生活料理.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci个人生活料理.Text = "个人生活料理";
            this.lci个人生活料理.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci个人生活料理.TextSize = new System.Drawing.Size(100, 20);
            this.lci个人生活料理.TextToControlDistance = 5;
            // 
            // lci家务劳动
            // 
            this.lci家务劳动.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci家务劳动.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci家务劳动.AppearanceItemCaption.Options.UseFont = true;
            this.lci家务劳动.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci家务劳动.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci家务劳动.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci家务劳动.Control = this.lbl家务劳动;
            this.lci家务劳动.CustomizationFormText = "家务劳动";
            this.lci家务劳动.Location = new System.Drawing.Point(99, 22);
            this.lci家务劳动.MinSize = new System.Drawing.Size(141, 22);
            this.lci家务劳动.Name = "lci家务劳动";
            this.lci家务劳动.Size = new System.Drawing.Size(520, 22);
            this.lci家务劳动.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci家务劳动.Text = "家务劳动";
            this.lci家务劳动.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci家务劳动.TextSize = new System.Drawing.Size(100, 20);
            this.lci家务劳动.TextToControlDistance = 5;
            // 
            // lci生产劳动
            // 
            this.lci生产劳动.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci生产劳动.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci生产劳动.AppearanceItemCaption.Options.UseFont = true;
            this.lci生产劳动.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci生产劳动.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci生产劳动.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci生产劳动.Control = this.lbl生产劳动及工作;
            this.lci生产劳动.CustomizationFormText = "生产劳动及工作";
            this.lci生产劳动.Location = new System.Drawing.Point(99, 44);
            this.lci生产劳动.MinSize = new System.Drawing.Size(141, 22);
            this.lci生产劳动.Name = "lci生产劳动";
            this.lci生产劳动.Size = new System.Drawing.Size(520, 22);
            this.lci生产劳动.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci生产劳动.Text = "生产劳动及工作";
            this.lci生产劳动.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci生产劳动.TextSize = new System.Drawing.Size(100, 20);
            this.lci生产劳动.TextToControlDistance = 5;
            // 
            // lci学习能力
            // 
            this.lci学习能力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci学习能力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci学习能力.AppearanceItemCaption.Options.UseFont = true;
            this.lci学习能力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci学习能力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci学习能力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci学习能力.Control = this.lbl学习能力;
            this.lci学习能力.CustomizationFormText = "学习能力";
            this.lci学习能力.Location = new System.Drawing.Point(99, 66);
            this.lci学习能力.MinSize = new System.Drawing.Size(141, 22);
            this.lci学习能力.Name = "lci学习能力";
            this.lci学习能力.Size = new System.Drawing.Size(520, 22);
            this.lci学习能力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci学习能力.Text = "学习能力";
            this.lci学习能力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci学习能力.TextSize = new System.Drawing.Size(100, 20);
            this.lci学习能力.TextToControlDistance = 5;
            // 
            // lci社会人际交往
            // 
            this.lci社会人际交往.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci社会人际交往.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci社会人际交往.AppearanceItemCaption.Options.UseFont = true;
            this.lci社会人际交往.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci社会人际交往.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci社会人际交往.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci社会人际交往.Control = this.lbl社会人际交往;
            this.lci社会人际交往.CustomizationFormText = "社会人际交往";
            this.lci社会人际交往.Location = new System.Drawing.Point(99, 88);
            this.lci社会人际交往.MinSize = new System.Drawing.Size(141, 22);
            this.lci社会人际交往.Name = "lci社会人际交往";
            this.lci社会人际交往.Size = new System.Drawing.Size(520, 22);
            this.lci社会人际交往.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci社会人际交往.Text = "社会人际交往";
            this.lci社会人际交往.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci社会人际交往.TextSize = new System.Drawing.Size(100, 20);
            this.lci社会人际交往.TextToControlDistance = 5;
            // 
            // lci关锁情况
            // 
            this.lci关锁情况.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lci关锁情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci关锁情况.AppearanceItemCaption.Options.UseFont = true;
            this.lci关锁情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci关锁情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci关锁情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci关锁情况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci关锁情况.Control = this.lbl关锁情况;
            this.lci关锁情况.CustomizationFormText = "关锁情况";
            this.lci关锁情况.Location = new System.Drawing.Point(0, 158);
            this.lci关锁情况.MinSize = new System.Drawing.Size(117, 22);
            this.lci关锁情况.Name = "lci关锁情况";
            this.lci关锁情况.Size = new System.Drawing.Size(245, 22);
            this.lci关锁情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci关锁情况.Text = "关锁情况";
            this.lci关锁情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci关锁情况.TextSize = new System.Drawing.Size(110, 20);
            this.lci关锁情况.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(245, 158);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(374, 22);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lci住院情况
            // 
            this.lci住院情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci住院情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci住院情况.AppearanceItemCaption.Options.UseFont = true;
            this.lci住院情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci住院情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci住院情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci住院情况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci住院情况.Control = this.panelControl2;
            this.lci住院情况.CustomizationFormText = "门诊";
            this.lci住院情况.Location = new System.Drawing.Point(0, 180);
            this.lci住院情况.MinSize = new System.Drawing.Size(275, 28);
            this.lci住院情况.Name = "lci住院情况";
            this.lci住院情况.Size = new System.Drawing.Size(619, 28);
            this.lci住院情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci住院情况.Text = "住院情况";
            this.lci住院情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci住院情况.TextSize = new System.Drawing.Size(110, 20);
            this.lci住院情况.TextToControlDistance = 5;
            // 
            // lci实验室检查
            // 
            this.lci实验室检查.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci实验室检查.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci实验室检查.AppearanceItemCaption.Options.UseFont = true;
            this.lci实验室检查.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci实验室检查.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci实验室检查.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci实验室检查.Control = this.lbl实验室检查;
            this.lci实验室检查.CustomizationFormText = "实验室检查";
            this.lci实验室检查.Location = new System.Drawing.Point(0, 208);
            this.lci实验室检查.MinSize = new System.Drawing.Size(109, 24);
            this.lci实验室检查.Name = "lci实验室检查";
            this.lci实验室检查.Size = new System.Drawing.Size(189, 24);
            this.lci实验室检查.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci实验室检查.Text = "实验室检查";
            this.lci实验室检查.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci实验室检查.TextSize = new System.Drawing.Size(110, 20);
            this.lci实验室检查.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt实验室检查;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(189, 208);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(430, 24);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // lci服药依从性
            // 
            this.lci服药依从性.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci服药依从性.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci服药依从性.AppearanceItemCaption.Options.UseFont = true;
            this.lci服药依从性.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci服药依从性.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci服药依从性.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci服药依从性.Control = this.lbl服药依从性;
            this.lci服药依从性.CustomizationFormText = "服药依从性";
            this.lci服药依从性.Location = new System.Drawing.Point(0, 232);
            this.lci服药依从性.MinSize = new System.Drawing.Size(169, 22);
            this.lci服药依从性.Name = "lci服药依从性";
            this.lci服药依从性.Size = new System.Drawing.Size(619, 22);
            this.lci服药依从性.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci服药依从性.Text = "用药依从性";
            this.lci服药依从性.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci服药依从性.TextSize = new System.Drawing.Size(110, 14);
            this.lci服药依从性.TextToControlDistance = 5;
            // 
            // lci药物不良反应
            // 
            this.lci药物不良反应.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci药物不良反应.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci药物不良反应.AppearanceItemCaption.Options.UseFont = true;
            this.lci药物不良反应.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci药物不良反应.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci药物不良反应.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci药物不良反应.Control = this.lbl药物不良反应;
            this.lci药物不良反应.CustomizationFormText = "药物不良反应";
            this.lci药物不良反应.Location = new System.Drawing.Point(0, 254);
            this.lci药物不良反应.MinSize = new System.Drawing.Size(169, 26);
            this.lci药物不良反应.Name = "lci药物不良反应";
            this.lci药物不良反应.Size = new System.Drawing.Size(245, 26);
            this.lci药物不良反应.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci药物不良反应.Text = "药物不良反应";
            this.lci药物不良反应.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci药物不良反应.TextSize = new System.Drawing.Size(110, 20);
            this.lci药物不良反应.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txt药物不良反应;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(245, 254);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(374, 26);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // lci治疗效果
            // 
            this.lci治疗效果.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lci治疗效果.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci治疗效果.AppearanceItemCaption.Options.UseFont = true;
            this.lci治疗效果.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci治疗效果.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci治疗效果.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci治疗效果.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci治疗效果.Control = this.lbl治疗效果;
            this.lci治疗效果.CustomizationFormText = "是否终止管理";
            this.lci治疗效果.Location = new System.Drawing.Point(0, 280);
            this.lci治疗效果.MinSize = new System.Drawing.Size(130, 24);
            this.lci治疗效果.Name = "lci治疗效果";
            this.lci治疗效果.Size = new System.Drawing.Size(619, 24);
            this.lci治疗效果.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci治疗效果.Text = "治疗效果";
            this.lci治疗效果.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci治疗效果.TextSize = new System.Drawing.Size(110, 20);
            this.lci治疗效果.TextToControlDistance = 5;
            // 
            // lci此次随访分类
            // 
            this.lci此次随访分类.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci此次随访分类.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci此次随访分类.AppearanceItemCaption.Options.UseFont = true;
            this.lci此次随访分类.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci此次随访分类.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci此次随访分类.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci此次随访分类.Control = this.comboBoxEdit此次随访分类;
            this.lci此次随访分类.CustomizationFormText = "此次随访分类";
            this.lci此次随访分类.Location = new System.Drawing.Point(0, 328);
            this.lci此次随访分类.MaxSize = new System.Drawing.Size(0, 24);
            this.lci此次随访分类.MinSize = new System.Drawing.Size(169, 24);
            this.lci此次随访分类.Name = "lci此次随访分类";
            this.lci此次随访分类.Size = new System.Drawing.Size(245, 24);
            this.lci此次随访分类.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci此次随访分类.Text = "此次随访分类";
            this.lci此次随访分类.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci此次随访分类.TextSize = new System.Drawing.Size(110, 20);
            this.lci此次随访分类.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(245, 328);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(374, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lci转诊
            // 
            this.lci转诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci转诊.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci转诊.AppearanceItemCaption.Options.UseFont = true;
            this.lci转诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci转诊.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci转诊.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci转诊.Control = this.lbl是否转诊;
            this.lci转诊.CustomizationFormText = "是否转诊";
            this.lci转诊.Location = new System.Drawing.Point(0, 550);
            this.lci转诊.MinSize = new System.Drawing.Size(169, 22);
            this.lci转诊.Name = "lci转诊";
            this.lci转诊.Size = new System.Drawing.Size(245, 24);
            this.lci转诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci转诊.Text = "是否转诊";
            this.lci转诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci转诊.TextSize = new System.Drawing.Size(110, 20);
            this.lci转诊.TextToControlDistance = 5;
            this.lci转诊.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit转诊原因;
            this.layoutControlItem32.CustomizationFormText = "原因：";
            this.layoutControlItem32.Location = new System.Drawing.Point(245, 550);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "原因：";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            this.layoutControlItem32.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.textEdit转诊机构;
            this.layoutControlItem33.CustomizationFormText = "机构及科室：";
            this.layoutControlItem33.Location = new System.Drawing.Point(432, 550);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "机构及科室：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            this.layoutControlItem33.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lci影响
            // 
            this.lci影响.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci影响.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci影响.AppearanceItemCaption.Options.UseFont = true;
            this.lci影响.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci影响.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci影响.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci影响.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci影响.Control = this.flowLayoutPanel2;
            this.lci影响.CustomizationFormText = "患者对家庭和社会的影响";
            this.lci影响.Location = new System.Drawing.Point(0, 110);
            this.lci影响.MinSize = new System.Drawing.Size(219, 48);
            this.lci影响.Name = "lci影响";
            this.lci影响.Size = new System.Drawing.Size(619, 48);
            this.lci影响.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci影响.Text = "危险行为";
            this.lci影响.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci影响.TextSize = new System.Drawing.Size(110, 20);
            this.lci影响.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt患者或家属签名;
            this.layoutControlItem13.CustomizationFormText = "患者或家属签名";
            this.layoutControlItem13.Location = new System.Drawing.Point(413, 574);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(206, 25);
            this.layoutControlItem13.Text = "患者或家属签名";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // lci下次随访日期
            // 
            this.lci下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lci下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lci下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci下次随访日期.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci下次随访日期.Control = this.txt下次随访日期;
            this.lci下次随访日期.CustomizationFormText = "下次随访日期*";
            this.lci下次随访日期.Location = new System.Drawing.Point(0, 574);
            this.lci下次随访日期.MinSize = new System.Drawing.Size(50, 25);
            this.lci下次随访日期.Name = "lci下次随访日期";
            this.lci下次随访日期.Size = new System.Drawing.Size(216, 25);
            this.lci下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci下次随访日期.Text = "下次随访日期";
            this.lci下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci下次随访日期.TextSize = new System.Drawing.Size(110, 20);
            this.lci下次随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem用药情况
            // 
            this.layoutControlItem用药情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem用药情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem用药情况.Control = this.gridControl用药情况;
            this.layoutControlItem用药情况.CustomizationFormText = "用药情况";
            this.layoutControlItem用药情况.Location = new System.Drawing.Point(0, 352);
            this.layoutControlItem用药情况.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem用药情况.MinSize = new System.Drawing.Size(25, 30);
            this.layoutControlItem用药情况.Name = "layoutControlItem用药情况";
            this.layoutControlItem用药情况.Size = new System.Drawing.Size(619, 30);
            this.layoutControlItem用药情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem用药情况.Text = "用药情况";
            this.layoutControlItem用药情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem用药情况.TextSize = new System.Drawing.Size(112, 20);
            this.layoutControlItem用药情况.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.txt是否通知联席部门;
            this.layoutControlItem14.CustomizationFormText = "是否通知联席部门";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 382);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem14.Text = "是否通知联席部门";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txt受理人1姓名;
            this.layoutControlItem15.CustomizationFormText = "公安部门/社区综治中心受理人姓名：";
            this.layoutControlItem15.Location = new System.Drawing.Point(184, 382);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(315, 24);
            this.layoutControlItem15.Text = "公安部门/社区综治中心受理人姓名：";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(197, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt受理人2姓名;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(499, 382);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(120, 24);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.txt受理人1电话;
            this.layoutControlItem18.CustomizationFormText = "受理人电话：";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 406);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem18.Text = "受理人电话：";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txt受理人2电话;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(246, 406);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(151, 24);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(397, 406);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(222, 24);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt是否需要转诊;
            this.layoutControlItem20.CustomizationFormText = "是否需要转诊";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 430);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem20.Text = "是否需要转诊";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.txt转诊原因;
            this.layoutControlItem22.CustomizationFormText = "转诊原因：";
            this.layoutControlItem22.Location = new System.Drawing.Point(184, 430);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(435, 24);
            this.layoutControlItem22.Text = "转诊原因：";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt转诊是否成功;
            this.layoutControlItem23.CustomizationFormText = "转诊是否成功";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 454);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem23.Text = "转诊是否成功";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt转诊机构;
            this.layoutControlItem24.CustomizationFormText = "转诊机构：";
            this.layoutControlItem24.Location = new System.Drawing.Point(184, 454);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem24.Text = "转诊机构：";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt转诊科室;
            this.layoutControlItem25.CustomizationFormText = "转诊科室：";
            this.layoutControlItem25.Location = new System.Drawing.Point(396, 454);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem25.Text = "转诊科室：";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt转诊联系人;
            this.layoutControlItem26.CustomizationFormText = "转诊联系人：";
            this.layoutControlItem26.Location = new System.Drawing.Point(184, 478);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem26.Text = "转诊联系人：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.txt转诊联系电话;
            this.layoutControlItem27.CustomizationFormText = "转诊联系电话：";
            this.layoutControlItem27.Location = new System.Drawing.Point(396, 478);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem27.Text = "转诊联系电话：";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt是否联系精神专科医师;
            this.layoutControlItem28.CustomizationFormText = "是否联系精神专科医师";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 502);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem28.Text = "是否联系精神专科医师";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(125, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt医师姓名;
            this.layoutControlItem30.CustomizationFormText = "医师姓名：";
            this.layoutControlItem30.Location = new System.Drawing.Point(184, 502);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem30.Text = "医师姓名：";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt医师电话;
            this.layoutControlItem31.CustomizationFormText = "医师电话：";
            this.layoutControlItem31.Location = new System.Drawing.Point(397, 502);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem31.Text = "医师电话：";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt处置结果;
            this.layoutControlItem34.CustomizationFormText = "处置结果：";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 526);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(619, 24);
            this.layoutControlItem34.Text = "处置结果：";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 478);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(184, 24);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lci康复措施
            // 
            this.lci康复措施.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci康复措施.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci康复措施.AppearanceItemCaption.Options.UseFont = true;
            this.lci康复措施.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci康复措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci康复措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci康复措施.Control = this.lbl康复措施;
            this.lci康复措施.CustomizationFormText = "康复措施";
            this.lci康复措施.Location = new System.Drawing.Point(0, 304);
            this.lci康复措施.Name = "lci康复措施";
            this.lci康复措施.Size = new System.Drawing.Size(619, 24);
            this.lci康复措施.Text = "康复措施";
            this.lci康复措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci康复措施.TextSize = new System.Drawing.Size(110, 20);
            this.lci康复措施.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.lbl出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(306, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.lbl身份证号;
            this.layoutControlItem6.CustomizationFormText = "身份证号";
            this.layoutControlItem6.Location = new System.Drawing.Point(306, 44);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(313, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "身份证号";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.lbl联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(306, 70);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(313, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "联系电话";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.lbl婚姻状况;
            this.layoutControlItem7.CustomizationFormText = "职业";
            this.layoutControlItem7.Location = new System.Drawing.Point(306, 96);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(111, 40);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(313, 40);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "婚姻状况 ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // lci危险性
            // 
            this.lci危险性.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci危险性.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci危险性.AppearanceItemCaption.Options.UseFont = true;
            this.lci危险性.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci危险性.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci危险性.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci危险性.Control = this.comboBoxEdit危险性;
            this.lci危险性.CustomizationFormText = "危险性";
            this.lci危险性.Location = new System.Drawing.Point(306, 136);
            this.lci危险性.MinSize = new System.Drawing.Size(50, 22);
            this.lci危险性.Name = "lci危险性";
            this.lci危险性.Size = new System.Drawing.Size(313, 22);
            this.lci危险性.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci危险性.Text = "危险性";
            this.lci危险性.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci危险性.TextSize = new System.Drawing.Size(90, 20);
            this.lci危险性.TextToControlDistance = 5;
            // 
            // lci自知力
            // 
            this.lci自知力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci自知力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci自知力.AppearanceItemCaption.Options.UseFont = true;
            this.lci自知力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci自知力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci自知力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci自知力.Control = this.lbl自知力;
            this.lci自知力.CustomizationFormText = "自知力";
            this.lci自知力.Location = new System.Drawing.Point(0, 252);
            this.lci自知力.MinSize = new System.Drawing.Size(50, 22);
            this.lci自知力.Name = "lci自知力";
            this.lci自知力.Size = new System.Drawing.Size(619, 22);
            this.lci自知力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci自知力.Text = "自知力";
            this.lci自知力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci自知力.TextSize = new System.Drawing.Size(110, 20);
            this.lci自知力.TextToControlDistance = 5;
            // 
            // lci目前症状
            // 
            this.lci目前症状.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci目前症状.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci目前症状.AppearanceItemCaption.Options.UseFont = true;
            this.lci目前症状.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci目前症状.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci目前症状.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci目前症状.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lci目前症状.Control = this.lbl目前症状;
            this.lci目前症状.CustomizationFormText = "目前症状";
            this.lci目前症状.Location = new System.Drawing.Point(0, 230);
            this.lci目前症状.MinSize = new System.Drawing.Size(192, 22);
            this.lci目前症状.Name = "lci目前症状";
            this.lci目前症状.Size = new System.Drawing.Size(619, 22);
            this.lci目前症状.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci目前症状.Text = "目前症状";
            this.lci目前症状.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci目前症状.TextSize = new System.Drawing.Size(110, 20);
            this.lci目前症状.TextToControlDistance = 5;
            // 
            // lci睡眠情况
            // 
            this.lci睡眠情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci睡眠情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci睡眠情况.AppearanceItemCaption.Options.UseFont = true;
            this.lci睡眠情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci睡眠情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci睡眠情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci睡眠情况.Control = this.lbl睡眠情况;
            this.lci睡眠情况.CustomizationFormText = "睡眠情况";
            this.lci睡眠情况.Location = new System.Drawing.Point(0, 274);
            this.lci睡眠情况.MinSize = new System.Drawing.Size(50, 22);
            this.lci睡眠情况.Name = "lci睡眠情况";
            this.lci睡眠情况.Size = new System.Drawing.Size(619, 22);
            this.lci睡眠情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci睡眠情况.Text = "睡眠情况";
            this.lci睡眠情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci睡眠情况.TextSize = new System.Drawing.Size(110, 14);
            this.lci睡眠情况.TextToControlDistance = 5;
            // 
            // lci饮食情况
            // 
            this.lci饮食情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci饮食情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lci饮食情况.AppearanceItemCaption.Options.UseFont = true;
            this.lci饮食情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci饮食情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci饮食情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci饮食情况.Control = this.lbl饮食情况;
            this.lci饮食情况.CustomizationFormText = "饮食情况";
            this.lci饮食情况.Location = new System.Drawing.Point(0, 296);
            this.lci饮食情况.MinSize = new System.Drawing.Size(169, 22);
            this.lci饮食情况.Name = "lci饮食情况";
            this.lci饮食情况.Size = new System.Drawing.Size(619, 22);
            this.lci饮食情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lci饮食情况.Text = "饮食情况";
            this.lci饮食情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lci饮食情况.TextSize = new System.Drawing.Size(110, 20);
            this.lci饮食情况.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.labelControl考核项;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(619, 18);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt本次随访形式;
            this.layoutControlItem1.CustomizationFormText = "本次随访形式";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 158);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(306, 24);
            this.layoutControlItem1.Text = "本次随访形式";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt失访原因;
            this.layoutControlItem10.CustomizationFormText = "若失访，原因";
            this.layoutControlItem10.Location = new System.Drawing.Point(306, 158);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(313, 24);
            this.layoutControlItem10.Text = "若失访，原因";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt死亡日期;
            this.layoutControlItem11.CustomizationFormText = "死亡日期";
            this.layoutControlItem11.Location = new System.Drawing.Point(116, 182);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(191, 24);
            this.layoutControlItem11.Text = "死亡日期";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt死亡原因;
            this.layoutControlItem12.CustomizationFormText = "死亡原因";
            this.layoutControlItem12.Location = new System.Drawing.Point(116, 206);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(503, 24);
            this.layoutControlItem12.Text = "死亡原因";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "如死亡，日期和原因";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(116, 48);
            this.emptySpaceItem5.Text = "如死亡，日期和原因";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(197, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(307, 182);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(312, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // UC重性精神疾病患者随访服务记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC重性精神疾病患者随访服务记录表_显示";
            this.Size = new System.Drawing.Size(750, 500);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt处置结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否联系精神专科医师.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊是否成功.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否需要转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人1电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人1姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否通知联席部门.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者或家属签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本次随访形式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteHyperLinkEdit)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit此次随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt实验室检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit危险性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci个人生活料理)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci家务劳动)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci生产劳动)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci学习能力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci社会人际交往)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci关锁情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci住院情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci实验室检查)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci服药依从性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci药物不良反应)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci治疗效果)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci此次随访分类)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci转诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci影响)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci康复措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci危险性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci自知力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci目前症状)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci睡眠情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci饮食情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl lbl居住地址;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl lbl婚姻状况;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl姓名;
        private DevExpress.XtraEditors.LabelControl lbl个人档案编号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem lci危险性;
        private DevExpress.XtraLayout.LayoutControlItem lci自知力;
        private DevExpress.XtraLayout.LayoutControlItem lci随访日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.LabelControl lbl目前症状;
        //private DevExpress.XtraEditors.CheckEdit checkEdit38;
        //private DevExpress.XtraEditors.CheckEdit checkEdit39;
        //private DevExpress.XtraEditors.CheckEdit checkEdit40;
        //private DevExpress.XtraEditors.CheckEdit checkEdit41;
        //private DevExpress.XtraEditors.CheckEdit checkEdit42;
        //private DevExpress.XtraEditors.CheckEdit checkEdit43;
        //private DevExpress.XtraEditors.CheckEdit checkEdit44;
        //private DevExpress.XtraEditors.CheckEdit checkEdit45;
        //private DevExpress.XtraEditors.CheckEdit checkEdit46;
        private DevExpress.XtraEditors.LabelControl lbl关锁情况;
        private DevExpress.XtraLayout.LayoutControlItem lci目前症状;
        private DevExpress.XtraEditors.LabelControl lbl所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近修改人;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lbl治疗效果;
        private DevExpress.XtraLayout.LayoutControlItem lci治疗效果;
        private DevExpress.XtraLayout.LayoutControlItem lci下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit危险性;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lci睡眠情况;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl lbl住院情况;
        private DevExpress.XtraLayout.LayoutControlItem lci住院情况;
        private DevExpress.XtraEditors.TextEdit txt随访医生;
        private DevExpress.XtraLayout.LayoutControlItem lci关锁情况;
        private DevExpress.XtraLayout.LayoutControlItem lci随访医生;
        private DevExpress.XtraEditors.LabelControl lbl随访日期;
        private DevExpress.XtraEditors.LabelControl lbl自知力;
        private DevExpress.XtraEditors.LabelControl lbl社会人际交往;
        private DevExpress.XtraEditors.LabelControl lbl学习能力;
        private DevExpress.XtraEditors.LabelControl lbl生产劳动及工作;
        private DevExpress.XtraEditors.LabelControl lbl家务劳动;
        private DevExpress.XtraEditors.LabelControl lbl个人生活料理;
        private DevExpress.XtraEditors.LabelControl lbl饮食情况;
        private DevExpress.XtraEditors.LabelControl lbl睡眠情况;
        private DevExpress.XtraLayout.LayoutControlItem lci个人生活料理;
        private DevExpress.XtraLayout.LayoutControlItem lci家务劳动;
        private DevExpress.XtraLayout.LayoutControlItem lci生产劳动;
        private DevExpress.XtraLayout.LayoutControlItem lci学习能力;
        private DevExpress.XtraLayout.LayoutControlItem lci社会人际交往;
        private DevExpress.XtraLayout.LayoutControlItem lci饮食情况;
        private DevExpress.XtraEditors.LabelControl lbl服药依从性;
        private DevExpress.XtraEditors.TextEdit txt实验室检查;
        private DevExpress.XtraEditors.LabelControl lbl实验室检查;
        private DevExpress.XtraLayout.LayoutControlItem lci实验室检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem lci服药依从性;
        private DevExpress.XtraEditors.TextEdit textEdit转诊机构;
        private DevExpress.XtraEditors.TextEdit textEdit转诊原因;
        private DevExpress.XtraEditors.LabelControl lbl是否转诊;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit此次随访分类;
        private DevExpress.XtraEditors.TextEdit txt药物不良反应;
        private DevExpress.XtraEditors.LabelControl lbl药物不良反应;
        private DevExpress.XtraLayout.LayoutControlItem lci药物不良反应;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem lci此次随访分类;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem lci转诊;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.LabelControl lbl康复措施;
        private DevExpress.XtraLayout.LayoutControlItem lci康复措施;
        private DevExpress.XtraEditors.SimpleButton btn添加随访;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraLayout.LayoutControlItem lci影响;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl轻度滋事;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl肇事;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl肇祸;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl自伤;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl自杀未遂;
        private DevExpress.XtraEditors.LabelControl lbl无影响;
        private DevExpress.XtraGrid.GridControl gridControl用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView用药情况;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit deleteHyperLinkEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem用药情况;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lbl末次住院时间;
        private DevExpress.XtraEditors.TextEdit txt下次随访日期;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.LabelControl labelControl考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit txt死亡原因;
        private DevExpress.XtraEditors.TextEdit txt死亡日期;
        private DevExpress.XtraEditors.TextEdit txt失访原因;
        private DevExpress.XtraEditors.TextEdit txt本次随访形式;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl其他危害行为;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.TextEdit txt患者或家属签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txt转诊科室;
        private DevExpress.XtraEditors.TextEdit txt转诊机构;
        private DevExpress.XtraEditors.TextEdit txt转诊是否成功;
        private DevExpress.XtraEditors.TextEdit txt转诊原因;
        private DevExpress.XtraEditors.TextEdit txt是否需要转诊;
        private DevExpress.XtraEditors.TextEdit txt受理人2电话;
        private DevExpress.XtraEditors.TextEdit txt受理人1电话;
        private DevExpress.XtraEditors.TextEdit txt受理人2姓名;
        private DevExpress.XtraEditors.TextEdit txt受理人1姓名;
        private DevExpress.XtraEditors.TextEdit txt是否通知联席部门;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit txt处置结果;
        private DevExpress.XtraEditors.TextEdit txt医师电话;
        private DevExpress.XtraEditors.TextEdit txt医师姓名;
        private DevExpress.XtraEditors.TextEdit txt是否联系精神专科医师;
        private DevExpress.XtraEditors.TextEdit txt转诊联系电话;
        private DevExpress.XtraEditors.TextEdit txt转诊联系人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
    }
}
