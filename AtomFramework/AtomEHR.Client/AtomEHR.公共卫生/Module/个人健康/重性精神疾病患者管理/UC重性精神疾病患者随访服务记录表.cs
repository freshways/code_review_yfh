﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Models;


namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class UC重性精神疾病患者随访服务记录表 : UserControlBase
    {
        private string m_Grdabh;
        private string m_ID;
        private UpdateType m_UpdateType = UpdateType.None;

        private DataSet m_ds随访 = null;
        private DataSet m_ds用药情况 = null;
        private int m_InitCount用药 = 0;

        private bll精神病随访记录 m_bll随访 = new bll精神病随访记录();
        private bll精神病_用药情况 m_bll用药情况 = new bll精神病_用药情况();

        //util.LayoutControlScrollHelper helper;
        public UC重性精神疾病患者随访服务记录表(string grdabh, string id, UpdateType updateType)
        {
            InitializeComponent();

            //helper = new util.LayoutControlScrollHelper(layoutControl1);
            //helper.EnableScrollOnMouseWheel();

            //添加事件
            SetEvent();

            m_Grdabh = grdabh;
            if (updateType == UpdateType.Add)
            {
                m_ID = "0";
            }
            else
            {
                m_ID = id;
            }

            m_UpdateType = updateType;

            if (m_UpdateType != UpdateType.Add && m_UpdateType != UpdateType.Modify)
            {
                throw new Exception("精神病随访传入参数错误。");
            }

            string strServerDateTime = m_bll随访.ServiceDateTime;

            BindOrginalDataForComboBox();

            //设置默认值
            SetDefaultValue();

            GetVisitDataFromDB(m_Grdabh, m_ID);

            if (m_UpdateType == UpdateType.Modify)
            {
                //BindDataFor用药情况();
                SetReadOnly();
            }
            else if (m_UpdateType == UpdateType.Add)
            {
                //m_bll随访.NewBusiness();
                labelControl1创建时间.Text = strServerDateTime;
                labelControl当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                labelControl创建机构.Text = Loginer.CurrentUser.所属机构名称;
                labelControl创建人.Text = Loginer.CurrentUser.AccountName;
            }

            labelControl最近更新时间.Text = strServerDateTime;
            labelControl最近修改人.Text = Loginer.CurrentUser.AccountName;
            this.textEdit随访医生.Text = Loginer.CurrentUser.AccountName;//默认随访医生为“用户名”

        }

        //给部分控件设置默认值
        private void SetDefaultValue()
        {
            util.ControlsHelper.SetComboxData("1", comboBoxEdit危险性);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit是否通知联席部门);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit是否需要转诊);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit转诊是否成功);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit是否联系精神专科医师);
        }

        private void SetReadOnly()
        {
            //dateEdit下次随访日期.BackColor = SystemColors.Control;
            //dateEdit下次随访日期.Properties.ReadOnly = true;
            //dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            //dateEdit随访日期.BackColor = SystemColors.Control;
            //dateEdit随访日期.Properties.ReadOnly = true;
            //dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
        }

        private void SetEvent()
        {
            ucLblTxtLbl轻度滋事.Txt1.Leave += textEditCheckNum_Leave;
            ucLblTxtLbl肇事.Txt1.Leave += textEditCheckNum_Leave;
            ucLblTxtLbl肇祸.Txt1.Leave += textEditCheckNum_Leave;
            ucLblTxtLbl自伤.Txt1.Leave += textEditCheckNum_Leave;
            ucLblTxtLbl自杀未遂.Txt1.Leave += textEditCheckNum_Leave;
            ucLblTxtLbl其他危害行为.Txt1.Leave += textEditCheckNum_Leave;
        }

        private void GetVisitDataFromDB(string grdabh, string id)
        {
            try
            {
                m_ds随访 = m_bll随访.GetBusinessByGrdabhAndID(m_Grdabh, m_ID, true);
                m_ds用药情况 = m_bll用药情况.GetBusinessByKey(m_ID, true);
                m_InitCount用药 = m_ds用药情况.Tables[0].Rows.Count;

                if (m_ds随访 == null || m_ds随访.Tables.Count == 0)
                {
                    Msg.ShowInformation("从数据库中获取随访记录时失败，请关闭窗口后重试。");
                }
                else if (m_UpdateType == UpdateType.Modify)
                {
                    ShowPatientInfo();
                    ShowDetailInfo(m_ds随访);
                    ShowMediniceList();
                    Set变色();
                }
                else if (m_UpdateType == UpdateType.Add)
                {
                    m_bll随访.NewBusiness();
                    ShowPatientInfo();
                    ShowMediniceList();
                }
            }
            catch (Exception ex)
            {
                sbtn保存.Enabled = false;
                Msg.ShowInformation(ex.Message);
            }
        }

        private void ShowMediniceList()
        {
            if (m_ds用药情况 != null && m_ds用药情况.Tables.Count > 0)
            {
                this.gridControl用药情况.DataSource = m_ds用药情况.Tables[tb_精神病_用药情况.__TableName];
                //调整宽度
                int newHeight = 30 + 24 * m_ds用药情况.Tables[0].Rows.Count;
                this.layoutControlItem用药情况.MaxSize = new Size(this.layoutControlItem用药情况.MaxSize.Width, newHeight);
                this.layoutControlItem用药情况.MinSize = new Size(this.layoutControlItem用药情况.MinSize.Width, newHeight);
            }
        }
        private void ShowPatientInfo()
        {
            labelControl个人档案号.Text = m_Grdabh;
            labelControl姓名.Text = DESEncrypt.DES解密(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.姓名].ToString());
            labelControl性别.Text = m_bll随访.ReturnDis字典显示("xb_xingbie", m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.性别].ToString());
            labelControl身份证号.Text = m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.身份证号].ToString();
            labelControl出生日期.Text = m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.出生日期].ToString();
            labelControl联系电话.Text = m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.联系电话].ToString();
            labelControl居住地址.Text = m_bll随访.Return地区名称(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.省].ToString())
                                      + m_bll随访.Return地区名称(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.市].ToString())
                                      + m_bll随访.Return地区名称(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.区].ToString())
                                      + m_bll随访.Return地区名称(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.街道].ToString())
                                      + m_bll随访.Return地区名称(m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居委会].ToString())
                                      + m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居住地址].ToString();
            labelControl婚姻状况.Text = m_bll随访.ReturnDis字典显示("hyzk", m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.婚姻状况].ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInfo">数据列的格式必须符合"m_ds随访"的结构</param>
        private void ShowDetailInfo(DataSet dsInfo)
        {
            //labelControl个人档案号.Text = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.个人档案编号].ToString();

            //string str姓名 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.姓名].ToString();
            //labelControl姓名.Text = DESEncrypt.DES解密(str姓名);

            //string str性别 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.性别].ToString();
            //labelControl性别.Text = m_bll随访.ReturnDis字典显示("xb_xingbie", str性别);

            //labelControl身份证号.Text = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.身份证号].ToString();

            //labelControl出生日期.Text = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.出生日期].ToString();

            //labelControl联系电话.Text = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.联系电话].ToString();

            //string str省 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.省].ToString();
            //string str市 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.市].ToString();
            //string str县 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.区].ToString();
            //string str镇 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.街道].ToString();
            //string str居委会 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居委会].ToString();
            //string str居住地址 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居住地址].ToString();
            //labelControl居住地址.Text = str省 + str市 + str县 + str镇 + str居委会 + str居住地址;

            //string str婚姻 = dsInfo.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.婚姻状况].ToString();
            //labelControl婚姻状况.Text = m_bll随访.ReturnDis字典显示("hyzk", str婚姻);

            dateEdit随访日期.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访日期].ToString();

            string str危险性 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.危险性].ToString();
            util.ControlsHelper.SetComboxData(str危险性, comboBoxEdit危险性);

            string str目前症状 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状].ToString();
            string str目前症状其他 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状其他].ToString();
            Set目前症状(str目前症状, str目前症状其他);

            radioGroup自知力.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.自知力].ToString();
            radioGroup睡眠情况.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.睡眠情况].ToString();
            radioGroup饮食情况.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.饮食情况].ToString();

            radioGroup个人生活料理.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.个人生活料理].ToString();
            radioGroup家务劳动.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.家务劳动].ToString();
            radioGroup生产劳动及工作.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.生产劳动及工作].ToString();
            radioGroup学习能力.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.学习能力].ToString();
            radioGroup社会人际交往.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.社会人际交往].ToString();

            string str影响 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响].ToString();
            char[] c分隔符 = { ',' };
            string[] str影响明细 = str影响.Split(c分隔符);
            for (int index = 0; index < str影响明细.Length; index++)
            {
                switch (str影响明细[index])
                {
                    case "1":
                        checkEdit轻度滋事.Checked = true;
                        ucLblTxtLbl轻度滋事.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响轻度滋事].ToString();
                        break;
                    case "2":
                        checkEdit肇事.Checked = true;
                        ucLblTxtLbl肇事.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇事].ToString();
                        break;
                    case "3":
                        checkEdit肇祸.Checked = true;
                        ucLblTxtLbl肇祸.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇祸].ToString();
                        break;
                    case "4":
                        checkEdit自伤.Checked = true;
                        ucLblTxtLbl自伤.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自伤].ToString();
                        break;
                    case "5":
                        checkEdit自杀未遂.Checked = true;
                        ucLblTxtLbl自杀未遂.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自杀未遂].ToString();
                        break;
                    case "6":
                        checkEdit无影响.Checked = true;
                        break;
                    case "7":
                        checkEdit其他危害行为.Checked = true;
                        ucLblTxtLbl其他危害行为.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响其他危害行为].ToString();
                        break;
                }
            }

            radioGroup关锁情况.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.关锁情况].ToString();
            string str住院情况 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.住院情况].ToString();
            radioGroup住院情况.EditValue = str住院情况;
            if (str住院情况 == "3")
            {
                ucLblDtp末次出院情况.Dtp1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.末次出院时间].ToString();
                ucLblDtp末次出院情况.Enabled = true;
            }

            string str实验室检查 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.实验室检查].ToString();
            radio实验室检查.EditValue = str实验室检查;
            if (str实验室检查 == "1")
            {
                textEdit实验室检查.Enabled = true;
                textEdit实验室检查.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.实验室检查其他].ToString();
            }

            radioGroup服药依从性.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.服药依从性].ToString();

            string str不良反应有无 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.药物不良反应有无].ToString();
            radio药物不良反应.EditValue = str不良反应有无;
            if (str不良反应有无 == "1")
            {
                textEdit药物不良反应.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.药物不良反应].ToString();
            }

            radioGroup治疗效果.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.治疗效果].ToString();

            string str随访分类 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.此次随访分类].ToString();
            util.ControlsHelper.SetComboxData(str随访分类, comboBoxEdit此次随访分类);

            //string str转诊 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否转诊].ToString();
            //radio是否转诊.EditValue = str转诊;

            //if (str转诊 == "1")
            //{
            //    textEdit转诊原因.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因].ToString();
            //    textEdit转诊科室.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室].ToString();
            //}

            string str康复措施 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施].ToString();
            string[] str措施明细 = str康复措施.Split(c分隔符);
            for (int index = 0; index < str措施明细.Length; index++)
            {
                switch (str措施明细[index])
                {
                    case "1":
                        checkEdit生活劳动.Checked = true;
                        break;
                    case "2":
                        checkEdit职业训练.Checked = true;
                        break;
                    case "3":
                        checkEdit学习能力.Checked = true;
                        break;
                    case "4":
                        checkEdit社会交往.Checked = true;
                        break;
                    case "99":
                        checkEdit康复措施其他.Checked = true;
                        textEdit康复措施其他.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施其他].ToString();
                        break;
                }
            }

            #region    新版本添加
            radio本次随访形式.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.本次随访形式].ToString();
            radio失访原因.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.若失访原因].ToString();
            txt死亡日期.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡日期].ToString();
            string str死亡原因 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因].ToString();
            if (string.IsNullOrEmpty(str死亡原因))
            {
                checkEdit躯体疾病.Checked = checkEdit自杀.Checked = checkEdit他杀.Checked = checkEdit意外.Checked = checkEdit精神疾病并发症.Checked = checkEdit死亡原因其他.Checked = false;
                radio躯体疾病.EditValue = "";
            }
            else
            {
                string[] str死亡原因明细 = str康复措施.Split(c分隔符);
                for (int index = 0; index < str死亡原因明细.Length; index++)
                {
                    switch (str死亡原因明细[index])
                    {
                        case "1":
                            checkEdit躯体疾病.Checked = true;
                            radio躯体疾病.EditValue = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因_躯体疾病].ToString();
                            break;
                        case "2":
                            checkEdit自杀.Checked = true;
                            break;
                        case "3":
                            checkEdit他杀.Checked = true;
                            break;
                        case "4":
                            checkEdit意外.Checked = true;
                            break;
                        case "5":
                            checkEdit精神疾病并发症.Checked = true;
                            break;
                        case "99":
                            checkEdit死亡原因其他.Checked = true;
                            break;
                    }
                }
            }
            
            string str是否通知联席部门 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否通知联席部门].ToString();
            util.ControlsHelper.SetComboxData(str是否通知联席部门, comboBoxEdit是否通知联席部门);
            if(str是否通知联席部门=="1")
            {
                txt受理人1姓名.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1姓名].ToString();
                txt受理人2姓名.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2姓名].ToString();
                txt受理人1电话.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1联系电话].ToString();
                txt受理人2电话.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2联系电话].ToString();
            }
            string str是否需要转诊 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否需要转诊].ToString();
            util.ControlsHelper.SetComboxData(str是否需要转诊, comboBoxEdit是否需要转诊);
            if (str是否需要转诊 == "1")
            {
                txt转诊原因.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因].ToString();
            }
            string str转诊是否成功 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否转诊].ToString();
            util.ControlsHelper.SetComboxData(str转诊是否成功, comboBoxEdit转诊是否成功);
            if (str转诊是否成功 == "1")
            {
                txt转诊机构.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊机构].ToString();
                txt转诊科室.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室].ToString();
                txt转诊联系人.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系人].ToString();
                txt转诊联系电话.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系电话].ToString();
            }
            string str是否联系精神专科医师 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否联系精神专科医师].ToString();
            util.ControlsHelper.SetComboxData(str是否联系精神专科医师, comboBoxEdit是否联系精神专科医师);
            if (str转诊是否成功 == "1")
            {
                txt精神专科医师姓名.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师姓名].ToString();
                txt精神专科医师电话.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师电话].ToString();
                txt处置结果.Txt1.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.处置结果].ToString();
            }
            textEdit患者或家属签名.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.患者或家属签名].ToString();
            #endregion

            dateEdit下次随访日期.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.下次随访日期].ToString();
            //this.textEdit随访医生.Text = Loginer.CurrentUser.AccountName;//默认随访医生为“用户名”
            //textEdit随访医生.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访医生].ToString();
            labelControl1创建时间.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建时间].ToString();

            string str所属机构 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.所属机构].ToString();
            labelControl当前所属机构.Text = m_bll随访.Return机构名称(str所属机构);
            //从个人档案中获取所属机构，“tb_精神病随访记录”表中的“所属机构”不再使用
            //DataRow[] dr = Common.dt个人档案.Select("个人档案编号='"+m_Grdabh+"'");
            //if(dr.Length>0)
            //{
            //    string str所属机构 = dr[0][tb_健康档案.所属机构].ToString();
            //    labelControl当前所属机构.Text = m_bll随访.Return机构名称(str所属机构);
            //}

            string str创建机构 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建机构].ToString();
            labelControl创建机构.Text = m_bll随访.Return机构名称(str创建机构);

            string str创建人 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建人].ToString();
            labelControl创建人.Text = m_bll随访.Return用户名称(str创建人);

            //labelControl最近更新时间.Text = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.更新时间].ToString();

            //string str修改人 = dsInfo.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.修改人].ToString();
            //labelControl最近更新人.Text = m_bll随访.Return用户名称(str修改人);
        }

        private void Set目前症状(string str症状, string str症状其他)
        {
            textEdit症状其他.Text = "";

            char[] c分隔符 = { ',' };
            string[] str症状明细 = str症状.Split(c分隔符);
            for (int index = 0; index < str症状明细.Length; index++)
            {
                switch (str症状明细[index])
                {
                    case "1":
                        checkEdit幻觉.Checked = true;
                        break;
                    case "2":
                        checkEdit交流困难.Checked = true;
                        break;
                    case "3":
                        checkEdit猜疑.Checked = true;
                        break;
                    case "4":
                        checkEdit喜怒无常.Checked = true;
                        break;
                    case "5":
                        checkEdit行为怪异.Checked = true;
                        break;
                    case "6":
                        checkEdit兴奋话多.Checked = true;
                        break;
                    case "7":
                        checkEdit伤人毁物.Checked = true;
                        break;
                    case "8":
                        checkEdit悲观厌世.Checked = true;
                        break;
                    case "9":
                        checkEdit无故外走.Checked = true;
                        break;
                    case "10":
                        checkEdit自语自笑.Checked = true;
                        break;
                    case "11":
                        checkEdit孤僻懒散.Checked = true;
                        break;
                    case "99":
                        checkEdit症状其他.Checked = true;
                        textEdit症状其他.Text = str症状其他;
                        break;
                    default:
                        break;
                }
            }
        }

        //private void BindDataFor用药情况()
        //{
        //    dt用药情况.Columns.Add("药物名称");
        //    dt用药情况.Columns.Add("剂量");
        //    dt用药情况.Columns.Add("用法");

        //    this.gridControl用药情况.DataSource = dt用药情况;
        //}

        private void BindOrginalDataForComboBox()
        {
            BindDataForComboBox("wxx", comboBoxEdit危险性);
            BindDataForComboBox("jsbsffl", comboBoxEdit此次随访分类);
            BindDataForComboBox("sf_shifou", comboBoxEdit是否通知联席部门);
            BindDataForComboBox("sf_shifou", comboBoxEdit是否需要转诊);
            BindDataForComboBox("sf_shifou", comboBoxEdit转诊是否成功);
            BindDataForComboBox("sf_shifou", comboBoxEdit是否联系精神专科医师);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }

        private void deleteHyperLinkEdit_Click(object sender, EventArgs e)
        {
            int index = this.gridView用药情况.GetFocusedDataSourceRowIndex();
            this.gridView用药情况.DeleteRow(index);
            int newHeight = this.gridView用药情况.RowCount * 24 + 27;
            this.layoutControlItem用药情况.MaxSize = new System.Drawing.Size(layoutControlItem饮食情况.MaxSize.Width, newHeight);
            this.layoutControlItem用药情况.MinSize = new System.Drawing.Size(layoutControlItem饮食情况.MinSize.Width, newHeight);
        }

        private void sbtn添加药物_Click(object sender, EventArgs e)
        {
            this.gridView用药情况.AddNewRow();
            int newHeight = this.gridView用药情况.RowCount * 24 + 27;
            this.layoutControlItem用药情况.MaxSize = new System.Drawing.Size(layoutControlItem饮食情况.MaxSize.Width, newHeight);
            this.layoutControlItem用药情况.MinSize = new System.Drawing.Size(layoutControlItem饮食情况.MinSize.Width, newHeight);
        }

        private bool Check()
        {
            //bool ret = true;
            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                Msg.ShowInformation("请填写随访日期。");
                dateEdit随访日期.Focus();
                return false;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit危险性)))
            {
                Msg.ShowInformation("请为“危险性”填写内容。");
                comboBoxEdit危险性.Focus();
                return false;
            }
            if (checkEdit幻觉.Checked || checkEdit交流困难.Checked || checkEdit猜疑.Checked || checkEdit喜怒无常.Checked
                || checkEdit行为怪异.Checked || checkEdit伤人毁物.Checked || checkEdit悲观厌世.Checked
                || checkEdit无故外走.Checked || checkEdit自语自笑.Checked
                || (checkEdit症状其他.Checked && !string.IsNullOrWhiteSpace(textEdit症状其他.Text)))
            {
            }
            else
            {
                Msg.ShowInformation("请确认“目前症状”是否填写完整。");
                return false;
            }

            if (radioGroup自知力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup自知力.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“自知力”设置选项。");
                return false;
            }

            if (radioGroup睡眠情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup睡眠情况.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“睡眠情况”设置选项。");
                return false;
            }

            if (radioGroup饮食情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup饮食情况.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“饮食情况”设置选项。");
                return false;
            }
            if (radioGroup个人生活料理.EditValue == null || string.IsNullOrWhiteSpace(radioGroup个人生活料理.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“个人生活料理”设置选项。");
                return false;
            }
            if (radioGroup家务劳动.EditValue == null || string.IsNullOrWhiteSpace(radioGroup家务劳动.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“家务劳动”设置选项。");
                return false;
            }
            if (radioGroup生产劳动及工作.EditValue == null || string.IsNullOrWhiteSpace(radioGroup生产劳动及工作.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“生产劳动及工作”设置选项。");
                return false;
            }
            if (radioGroup学习能力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup学习能力.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“学习能力”设置选项。");
                return false;
            }
            if (radioGroup社会人际交往.EditValue == null || string.IsNullOrWhiteSpace(radioGroup社会人际交往.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“社会人际交往”设置选项。");
                return false;
            }

            if ((checkEdit轻度滋事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl轻度滋事.Txt1.Text)))
                || (checkEdit肇事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇事.Txt1.Text)))
                || (checkEdit肇祸.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇祸.Txt1.Text))
                || (checkEdit自伤.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自伤.Txt1.Text))
                || (checkEdit自杀未遂.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自杀未遂.Txt1.Text)))
                || checkEdit无影响.Checked)
            { }
            else
            {
                Msg.ShowInformation("请填写“患者对家庭和社会的影响”。");
                return false;
            }

            if (radioGroup关锁情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup关锁情况.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“关锁情况”设置选项。");
                return false;
            }

            if (radioGroup住院情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup住院情况.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“住院情况”设置选项。");
                return false;
            }
            else if ((radioGroup住院情况.EditValue.ToString() == "3") && (string.IsNullOrWhiteSpace(ucLblDtp末次出院情况.Dtp1.Text)))
            {
                Msg.ShowInformation("请为“住院情况”填写“末次出院时间”");
                return false;
            }
            else { }

            if (radio实验室检查.EditValue == null || string.IsNullOrWhiteSpace(radio实验室检查.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“实验室检查”设置选项。");
                return false;
            }
            else if (radio实验室检查.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit实验室检查.Text))
            {
                Msg.ShowInformation("“实验室检查”的设置项为“有”,请填写实验室检查的具体内容。");
                return false;
            }
            else { }

            if (radioGroup服药依从性.EditValue == null || string.IsNullOrWhiteSpace(radioGroup服药依从性.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“服药依从性”设置选项。");
                return false;
            }

            if (radio药物不良反应.EditValue == null || string.IsNullOrWhiteSpace(radio药物不良反应.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“药物不良反应”设置选项。");
                return false;
            }
            else if (radio药物不良反应.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit药物不良反应.Text))
            {
                Msg.ShowInformation("“药物不良反应”的设定值为“有”，请填写药物不良反应的具体内容。");
            }
            else { }

            if (radioGroup治疗效果.EditValue == null || string.IsNullOrWhiteSpace(radioGroup治疗效果.EditValue.ToString()))
            {
                Msg.ShowInformation("请为“治疗效果”设置选项。");
                return false;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit此次随访分类)))
            {
                Msg.ShowInformation("请设置“此次随访分类”");
                return false;
            }

            //if (radio是否转诊.EditValue == null || string.IsNullOrWhiteSpace(radio是否转诊.EditValue.ToString()))
            //{
            //    Msg.ShowInformation("请为“是否转诊”设置选项。");
            //    return false;
            //}
            //else if (radio是否转诊.EditValue.ToString() == "1" && (string.IsNullOrWhiteSpace(textEdit转诊原因.Text) || string.IsNullOrWhiteSpace(textEdit转诊科室.Text)))
            //{
            //    Msg.ShowInformation("“是否转诊”的设定值为“是”，请填写转诊原因、转诊机构。");
            //    return false;
            //}
            //else { }

            if (checkEdit生活劳动.Checked || checkEdit职业训练.Checked || checkEdit学习能力.Checked
                || checkEdit社会交往.Checked || (checkEdit康复措施其他.Checked && !(string.IsNullOrWhiteSpace(textEdit康复措施其他.Text))))
            { }
            else
            {
                Msg.ShowInformation("请为“康复措施”设置选项(如果包含“其他”选项，请填写康复措施的其他内容)。");
                return false;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                Msg.ShowInformation("请填写“下次随访日期”");
                return false;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                Msg.ShowInformation("请填写“随访医生”");
                return false;
            }

            return true;
        }

        private void sbtn保存_Click(object sender, EventArgs e)
        {
            //delete by wjz 去掉填写时的验证 ▽
            //if (Check())
            //{
            //}
            //else
            //{
            //    return;
            //}
            //delete by wjz 去掉填写时的验证 △

            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                Msg.ShowInformation("请填写“随访日期”。");
                dateEdit随访日期.Focus();
                return;
            }
            else
            {
                try
                {
                    DateTime dtTemp = Convert.ToDateTime(dateEdit随访日期.Text);
                    dateEdit随访日期.Text = dtTemp.ToString("yyyy-MM-dd");
                }
                catch
                {
                    Msg.ShowInformation("请检查“随访日期”是否填写正确。");
                    return;
                }
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                Msg.ShowInformation("请填写“下次随访日期”。");
                dateEdit下次随访日期.Focus();
                return;
            }
            else
            {
                try
                {
                    DateTime dtTemp = Convert.ToDateTime(dateEdit下次随访日期.Text);
                    dateEdit下次随访日期.Text = dtTemp.ToString("yyyy-MM-dd");
                }
                catch
                {
                    Msg.ShowInformation("请检查填写“下次随访日期”是否填写正确。");
                    return;
                }
            }
            if (dateEdit随访日期.DateTime > Convert.ToDateTime(this.labelControl1创建时间.Text))
            {
                Msg.ShowInformation("随访日期不能大于填写日期");
                dateEdit随访日期.Focus();
                return;
            }
            //剔除用药信息的空白数据
            //ADD BY WJZ 修改随访记录时，如果只执行了删除药品信息的操作，保存将会失败，修正此问题 ▽
            m_ds用药情况.Tables[0].AcceptChanges();
            //ADD BY WJZ 修改随访记录时，如果只执行了删除药品信息的操作，保存将会失败，修正此问题 △

            int rowCount = m_ds用药情况.Tables[0].Rows.Count;
            for (int index = rowCount - 1; index >= 0; index--)
            {
                if (string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.药物名称].ToString())
                    && string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.剂量].ToString())
                    && string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.用法].ToString()))
                {
                    m_ds用药情况.Tables[0].Rows.RemoveAt(index);
                    //m_ds用药情况.Tables[0].Rows[index].Delete();
                    //m_ds用药情况.Tables[0].Rows[index].r
                }
                else if (!(string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.药物名称].ToString()))
                      && !(string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.剂量].ToString()))
                      && !(string.IsNullOrWhiteSpace(m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.用法].ToString())))
                {
                    //m_ds用药情况.Tables[0].Rows[index].AcceptChanges();
                }
                else
                {
                    Msg.ShowInformation("请确认用药情况中所有药品的“药物名称”、“剂量”、“用法”是否填写完整。");
                    return;
                }
            }

            #region 向“m_ds随访.Tables[tb_精神病随访记录.__TableName]”中传值
            if (m_UpdateType == UpdateType.Add)
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建人] = Loginer.CurrentUser.用户编码;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建时间] = labelControl1创建时间.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.创建机构] = Loginer.CurrentUser.所属机构;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.所属机构] = Loginer.CurrentUser.所属机构;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.个人档案编号] = m_Grdabh;

                //add by wjz ▽
                //m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访日期] = dateEdit随访日期.Text;
                //m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.下次随访日期] = dateEdit下次随访日期.Text;
                //add by wjz △

                bool bret = Msg.AskQuestion("信息保存后，‘随访日期’与‘下次随访日期’将不允许修改，确认保存信息？");
                if (!bret)
                {
                    return;
                }
            }
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访日期] = dateEdit随访日期.Text;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.下次随访日期] = dateEdit下次随访日期.Text;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.修改人] = Loginer.CurrentUser.用户编码;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.更新时间] = labelControl最近更新时间.Text;

            //m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访日期] = dateEdit随访日期.Text;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.危险性] = util.ControlsHelper.GetComboxKey(comboBoxEdit危险性);

            string str目前症状 = "";
            if (checkEdit幻觉.Checked)
            {
                str目前症状 += "1,";
            }
            if (checkEdit交流困难.Checked)
            {
                str目前症状 += "2,";
            }
            if (checkEdit猜疑.Checked)
            {
                str目前症状 += "3,";
            }
            if (checkEdit喜怒无常.Checked)
            {
                str目前症状 += "4,";
            }
            if (checkEdit行为怪异.Checked)
            {
                str目前症状 += "5,";
            }
            if (checkEdit兴奋话多.Checked)
            {
                str目前症状 += "6,";
            }
            if (checkEdit伤人毁物.Checked)
            {
                str目前症状 += "7,";
            }
            if (checkEdit悲观厌世.Checked)
            {
                str目前症状 += "8,";
            }
            if (checkEdit无故外走.Checked)
            {
                str目前症状 += "9,";
            }
            if (checkEdit自语自笑.Checked)
            {
                str目前症状 += "10,";
            }
            if (checkEdit孤僻懒散.Checked)
            {
                str目前症状 += "11,";
            }
            if (checkEdit症状其他.Checked)
            {
                str目前症状 += "99,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状其他] = textEdit症状其他.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状其他] = null;
            }
            if (str目前症状.Length > 0)
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状] = str目前症状.Substring(0, str目前症状.Length - 1); ;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.目前症状] = null;
            }

            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.自知力] = radioGroup自知力.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.睡眠情况] = radioGroup睡眠情况.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.饮食情况] = radioGroup饮食情况.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.个人生活料理] = radioGroup个人生活料理.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.家务劳动] = radioGroup家务劳动.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.生产劳动及工作] = radioGroup生产劳动及工作.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.学习能力] = radioGroup学习能力.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.社会人际交往] = radioGroup社会人际交往.EditValue;

            string str影响 = "";
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响轻度滋事] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇事] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇祸] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自伤] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自杀未遂] = null;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响其他危害行为] = null;
            if (checkEdit轻度滋事.Checked)
            {
                str影响 += "1,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响轻度滋事] = ucLblTxtLbl轻度滋事.Txt1.Text;
            }
            if (checkEdit肇事.Checked)
            {
                str影响 += "2,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇事] = ucLblTxtLbl肇事.Txt1.Text;
            }
            if (checkEdit肇祸.Checked)
            {
                str影响 += "3,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响肇祸] = ucLblTxtLbl肇祸.Txt1.Text;
            }
            if (checkEdit自伤.Checked)
            {
                str影响 += "4,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自伤] = ucLblTxtLbl自伤.Txt1.Text;
            }
            if (checkEdit自杀未遂.Checked)
            {
                str影响 += "5,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响自杀未遂] = ucLblTxtLbl自杀未遂.Txt1.Text;
            }
            if (checkEdit无影响.Checked)
            {
                str影响 += "6,";
            }
            if(checkEdit其他危害行为.Checked)
            {
                str影响 += "7,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响其他危害行为] = ucLblTxtLbl其他危害行为.Txt1.Text; 
            }
            if (str影响.Length > 0)
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.影响] = str影响.Substring(0, str影响.Length - 1);
            }

            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.关锁情况] = radioGroup关锁情况.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.住院情况] = radioGroup住院情况.EditValue;

            if (radioGroup住院情况.EditValue != null && radioGroup住院情况.EditValue.ToString() == "3")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.末次出院时间] = ucLblDtp末次出院情况.Dtp1.Text;
            }

            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.实验室检查] = radio实验室检查.EditValue;
            if (radio实验室检查.EditValue != null && radio实验室检查.EditValue.ToString() == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.实验室检查其他] = textEdit实验室检查.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.实验室检查其他] = null;
            }
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.服药依从性] = radioGroup服药依从性.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.药物不良反应有无] = radio药物不良反应.EditValue;
            if (radio药物不良反应.EditValue == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.药物不良反应] = textEdit药物不良反应.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.药物不良反应] = null;
            }
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.治疗效果] = radioGroup治疗效果.EditValue;

            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.此次随访分类] = util.ControlsHelper.GetComboxKey(comboBoxEdit此次随访分类);

            //m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否转诊] = radio是否转诊.EditValue;
            //if (radio是否转诊.EditValue == "1")
            //{
            //    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因] = textEdit转诊原因.Text;
            //    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室] = textEdit转诊科室.Text;
            //}
            //else
            //{
            //    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因] = null;
            //    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室] = null;
            //}

            string str康复措施 = "";
            if (checkEdit生活劳动.Checked)
            {
                str康复措施 += "1,";
            }
            if (checkEdit职业训练.Checked)
            {
                str康复措施 += "2,";
            }
            if (checkEdit学习能力.Checked)
            {
                str康复措施 += "3,";
            }
            if (checkEdit社会交往.Checked)
            {
                str康复措施 += "4,";
            }
            if (checkEdit康复措施其他.Checked)
            {
                str康复措施 += "99,";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施其他] = textEdit康复措施其他.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施其他] = null;
            }
            if (str康复措施.Length > 0)
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施] = str康复措施.Substring(0, str康复措施.Length - 1);
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.康复措施] = null;
            }

            #region     新版本添加
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.本次随访形式] = radio本次随访形式.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.若失访原因] = radio失访原因.EditValue;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡日期] = txt死亡日期.Text;
            string str死亡原因 = "";
            if(checkEdit躯体疾病.Checked)
            {
                str死亡原因 = "1";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因_躯体疾病] = radio躯体疾病.EditValue;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else if (checkEdit自杀.Checked)
            {
                str死亡原因 = "2";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else if (checkEdit他杀.Checked)
            {
                str死亡原因 = "3";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else if (checkEdit意外.Checked)
            {
                str死亡原因 = "4";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else if (checkEdit精神疾病并发症.Checked)
            {
                str死亡原因 = "5";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else if (checkEdit死亡原因其他.Checked)
            {
                str死亡原因 = "99";
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = str死亡原因;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因_躯体疾病] = null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.死亡原因] = null;
            }
            string str是否通知联席部门 = util.ControlsHelper.GetComboxKey(comboBoxEdit是否通知联席部门);
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否通知联席部门] = str是否通知联席部门;
            if (str是否通知联席部门 == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1姓名] = txt受理人1姓名.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2姓名] = txt受理人2姓名.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1联系电话] = txt受理人1电话.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2联系电话] = txt受理人2电话.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1姓名] =null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2姓名] = null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人1联系电话] = null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.受理人2联系电话] =null;
            }
            string str是否需要转诊 = util.ControlsHelper.GetComboxKey(comboBoxEdit是否需要转诊);
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否需要转诊] = str是否需要转诊;
            if (str是否需要转诊 == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因] = txt转诊原因.Txt1.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊原因] = null;
            }
            string str转诊是否成功 = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊是否成功);
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否转诊] = str转诊是否成功;
            if (str转诊是否成功 == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊机构] = txt转诊机构.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室] = txt转诊科室.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系人] = txt转诊联系人.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系电话] = txt转诊联系电话.Txt1.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊机构] = null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊科室] =null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系人] =null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.转诊联系电话] =null;
            }
            string str是否联系精神专科医师 = util.ControlsHelper.GetComboxKey(comboBoxEdit是否联系精神专科医师);
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.是否联系精神专科医师] = str是否联系精神专科医师;
            if (str是否联系精神专科医师 == "1")
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师姓名] = txt精神专科医师姓名.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师电话] = txt精神专科医师电话.Txt1.Text;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.处置结果] = txt处置结果.Txt1.Text;
            }
            else
            {
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师姓名] =null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.精神专科医师电话] = null;
                m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.处置结果] =null;
            }
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.患者或家属签名] = textEdit患者或家属签名.Text;
            #endregion

            //m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.下次随访日期] = dateEdit下次随访日期.Text;
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.随访医生] = textEdit随访医生.Text;

            if (m_UpdateType == UpdateType.Add)
            {
                //设置随访次数
                int sfcount = m_bll随访.GetVisitCount(m_Grdabh, dateEdit随访日期.Text.Substring(0, 4));
                if (sfcount == 0)
                {
                    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.SFCS] = "1";
                }
                else
                {
                    m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.SFCS] = (sfcount + 1).ToString();
                }
            }
            #endregion

            #region 计算缺项、完整度
            int i缺项 = 0;
            int i完整度 = 0;
            Cal缺项完整度(out i缺项, out i完整度);
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.缺项] = i缺项.ToString();
            m_ds随访.Tables[tb_精神病随访记录.__TableName].Rows[0][tb_精神病随访记录.完整度] = i完整度.ToString();
            #endregion

            #region 设置随访的关联表
            string strXcsfsj1 = m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.下次随访时间].ToString();

            if (m_UpdateType == UpdateType.Add)
            {
                //TODO: 添加用药明细

                if (string.IsNullOrWhiteSpace(strXcsfsj1))
                {
                    m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.下次随访时间] = dateEdit下次随访日期.Text;
                    try
                    {
                        DateTime dtXcsfsj_BenCi = Convert.ToDateTime(dateEdit下次随访日期.Text);
                        DateTime dtServerTime = Convert.ToDateTime(labelControl最近更新时间.Text);
                        DateTime dtXcsfsj = Convert.ToDateTime(strXcsfsj1);
                        if (dtXcsfsj_BenCi >= dtServerTime)
                        {
                            m_ds随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = i缺项.ToString() + "," + i完整度.ToString();
                        }
                    }
                    catch
                    { }
                }
                else if (Convert.ToDateTime(dateEdit下次随访日期.Text) > Convert.ToDateTime(strXcsfsj1))
                {
                    m_ds随访.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.下次随访时间] = dateEdit下次随访日期.Text;
                    if (Convert.ToDateTime(dateEdit下次随访日期.Text) > Convert.ToDateTime(labelControl最近更新时间.Text))//应该是大于当前时间
                    {
                        m_ds随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = i缺项.ToString() + "," + i完整度.ToString();
                    }
                }
            }
            else if (m_UpdateType == UpdateType.Modify)
            {
                //更新个人体征和基本信息补充表

                if (string.IsNullOrWhiteSpace(strXcsfsj1) || strXcsfsj1.Equals("1900-01-01") || strXcsfsj1.Equals(dateEdit下次随访日期.Text))
                {
                    #region 修改个人体征
                    try
                    {
                        DateTime dtUpdateTime = Convert.ToDateTime(labelControl最近更新时间.Text);
                        DateTime dtxcsfsj = Convert.ToDateTime(strXcsfsj1);
                        if (dtxcsfsj >= dtUpdateTime)
                        {
                            m_ds随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = i缺项.ToString() + "," + i完整度.ToString();
                        }
                        else
                        {
                            m_ds随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                        }
                    }
                    catch
                    { }
                    #endregion
                }
            }
            #endregion

            SaveResult result = m_bll随访.Save(m_ds随访);
            if (result.Success)
            {
                if (m_UpdateType == UpdateType.Modify && m_InitCount用药 > 0)//修改随访记录前，药品数量不是0的情况下，删除旧的药品信息
                {
                    //更新用药情况
                    m_bll用药情况.DeleteBySFID(m_ID);
                }

                if (m_ds用药情况.Tables[0].Rows.Count > 0)//如果新增药品信息的数量不是0的情况下，执行添加药品的操作
                {
                    if (m_UpdateType == UpdateType.Add)
                    {
                        m_ID = m_bll随访.GetIdByGrdabhAndUpdateUserTime(m_Grdabh, Loginer.CurrentUser.用户编码, labelControl最近更新时间.Text);
                    }

                    if ("0".Equals(m_ID))//如果m_ID是0，这说明从数据库中获取ID失败，在获取ID失败的情况下，不向药品信息表中添加数据
                    { }
                    else
                    {
                        m_ds用药情况.Tables[0].AcceptChanges();
                        for (int index = 0; index < m_ds用药情况.Tables[0].Rows.Count; index++)
                        {
                            m_ds用药情况.Tables[0].Rows[index].SetAdded();//更新每一行的状况，重置为Added，目的：通过Save将数据插入药品数据表
                            m_ds用药情况.Tables[0].Rows[index][tb_精神病_用药情况.C_JRID] = m_ID.Trim();
                        }
                        //添加用药信息
                        m_bll用药情况.Save(m_ds用药情况);
                    }
                }

                UC重性精神疾病患者随访服务记录表_显示 ctrl = new UC重性精神疾病患者随访服务记录表_显示(m_Grdabh, m_ID);
                ShowControl(ctrl, DockStyle.Fill);
            }
            else
            {
                Msg.ShowError("保存失败！");
            }
        }

        private void Cal缺项完整度(out int i缺项, out int i完整度)
        {
            int i考核项 = 23;
            i缺项 = 0;
            i完整度 = 0;
            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit危险性)))
            {
                i缺项++;
            }
            if (checkEdit幻觉.Checked || checkEdit交流困难.Checked || checkEdit猜疑.Checked || checkEdit喜怒无常.Checked
                || checkEdit行为怪异.Checked || checkEdit伤人毁物.Checked || checkEdit悲观厌世.Checked
                || checkEdit无故外走.Checked || checkEdit自语自笑.Checked
                || (checkEdit症状其他.Checked && !string.IsNullOrWhiteSpace(textEdit症状其他.Text)))
            {
            }
            else
            {
                i缺项++;
            }

            if (radioGroup自知力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup自知力.EditValue.ToString()))
            {
                i缺项++;
            }

            if (radioGroup睡眠情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup睡眠情况.EditValue.ToString()))
            {
                i缺项++;
            }

            if (radioGroup饮食情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup饮食情况.EditValue.ToString()))
            {
                i缺项++;
            }
            if (radioGroup个人生活料理.EditValue == null || string.IsNullOrWhiteSpace(radioGroup个人生活料理.EditValue.ToString()))
            {
                i缺项++;
            }
            if (radioGroup家务劳动.EditValue == null || string.IsNullOrWhiteSpace(radioGroup家务劳动.EditValue.ToString()))
            {
                i缺项++;
            }
            if (radioGroup生产劳动及工作.EditValue == null || string.IsNullOrWhiteSpace(radioGroup生产劳动及工作.EditValue.ToString()))
            {
                i缺项++;
            }
            if (radioGroup学习能力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup学习能力.EditValue.ToString()))
            {
                i缺项++;
            }
            if (radioGroup社会人际交往.EditValue == null || string.IsNullOrWhiteSpace(radioGroup社会人际交往.EditValue.ToString()))
            {
                i缺项++;
            }

            if ((checkEdit轻度滋事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl轻度滋事.Txt1.Text)))
                || (checkEdit肇事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇事.Txt1.Text)))
                || (checkEdit肇祸.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇祸.Txt1.Text))
                || (checkEdit自伤.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自伤.Txt1.Text))
                || (checkEdit自杀未遂.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自杀未遂.Txt1.Text)))
                || checkEdit无影响.Checked)
            { }
            else
            {
                i缺项++;
            }

            if (radioGroup关锁情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup关锁情况.EditValue.ToString()))
            {
                i缺项++;
            }

            if (radioGroup住院情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup住院情况.EditValue.ToString()))
            {
                i缺项++;
            }
            //else if ((radioGroup住院情况.EditValue.ToString() == "3") && (string.IsNullOrWhiteSpace(ucLblDtp末次出院情况.Dtp1.Text)))
            //{
            //    i缺项++;
            //}
            else { }

            if (radio实验室检查.EditValue == null || string.IsNullOrWhiteSpace(radio实验室检查.EditValue.ToString()))
            {
                i缺项++;
            }
            else if (radio实验室检查.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit实验室检查.Text))
            {
                i缺项++;
            }
            else { }

            if (radioGroup服药依从性.EditValue == null || string.IsNullOrWhiteSpace(radioGroup服药依从性.EditValue.ToString()))
            {
                i缺项++;
            }

            if (radio药物不良反应.EditValue == null || string.IsNullOrWhiteSpace(radio药物不良反应.EditValue.ToString()))
            {
                i缺项++;
            }
            else if (radio药物不良反应.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit药物不良反应.Text))
            {
                i缺项++;
            }
            else { }

            if (radioGroup治疗效果.EditValue == null || string.IsNullOrWhiteSpace(radioGroup治疗效果.EditValue.ToString()))
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit此次随访分类)))
            {
                i缺项++;
            }

            if (radio是否转诊.EditValue == null || string.IsNullOrWhiteSpace(radio是否转诊.EditValue.ToString()))
            {
                i缺项++;
            }
            else if (radio是否转诊.EditValue.ToString() == "1" && (string.IsNullOrWhiteSpace(textEdit转诊原因.Text) || string.IsNullOrWhiteSpace(textEdit转诊科室.Text)))
            {
                i缺项++;
            }
            else { }

            if (checkEdit生活劳动.Checked || checkEdit职业训练.Checked || checkEdit学习能力.Checked
                || checkEdit社会交往.Checked || checkEdit康复措施其他.Checked)
            { }
            else
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                i缺项++;
            }

            i完整度 = (i考核项 - i缺项) * 100 / i考核项;
        }

        private void Set变色()
        {
            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                layoutControlItem随访日期.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit危险性)))
            {
                layoutControlItem危险性.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (!checkEdit幻觉.Checked && !checkEdit交流困难.Checked && !checkEdit猜疑.Checked && !checkEdit喜怒无常.Checked
                && !checkEdit行为怪异.Checked && !checkEdit伤人毁物.Checked && !checkEdit悲观厌世.Checked
                && !checkEdit无故外走.Checked && !checkEdit自语自笑.Checked && (!checkEdit症状其他.Checked))
            {
                layoutControlItem目前症状.AppearanceItemCaption.ForeColor = Color.Red;
            }


            //if (checkEdit症状其他.Checked && string.IsNullOrWhiteSpace(textEdit症状其他.Text))
            //{
            //    layoutControlItem目前症状.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else if (checkEdit幻觉.Checked || checkEdit交流困难.Checked || checkEdit猜疑.Checked || checkEdit喜怒无常.Checked
            //    || checkEdit行为怪异.Checked || checkEdit伤人毁物.Checked || checkEdit悲观厌世.Checked
            //    || checkEdit无故外走.Checked || checkEdit自语自笑.Checked)
            //{
            //}
            //else
            //{
            //    layoutControlItem目前症状.AppearanceItemCaption.ForeColor = Color.Red;
            //}

            if (radioGroup自知力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup自知力.EditValue.ToString()))
            {
                layoutControlItem自知力.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radioGroup睡眠情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup睡眠情况.EditValue.ToString()))
            {
                layoutControlItem睡眠情况.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radioGroup饮食情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup饮食情况.EditValue.ToString()))
            {
                layoutControlItem饮食情况.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (radioGroup个人生活料理.EditValue == null || string.IsNullOrWhiteSpace(radioGroup个人生活料理.EditValue.ToString()))
            {
                layoutControlItem个人生活料理.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (radioGroup家务劳动.EditValue == null || string.IsNullOrWhiteSpace(radioGroup家务劳动.EditValue.ToString()))
            {
                layoutControlItem家务劳动.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (radioGroup生产劳动及工作.EditValue == null || string.IsNullOrWhiteSpace(radioGroup生产劳动及工作.EditValue.ToString()))
            {
                layoutControlItem生产劳动及工作.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (radioGroup学习能力.EditValue == null || string.IsNullOrWhiteSpace(radioGroup学习能力.EditValue.ToString()))
            {
                layoutControlItem学习能力.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (radioGroup社会人际交往.EditValue == null || string.IsNullOrWhiteSpace(radioGroup社会人际交往.EditValue.ToString()))
            {
                layoutControlItem社会人际交往.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if ((checkEdit轻度滋事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl轻度滋事.Txt1.Text)))
                || (checkEdit肇事.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇事.Txt1.Text)))
                || (checkEdit肇祸.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl肇祸.Txt1.Text))
                || (checkEdit自伤.Checked) && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自伤.Txt1.Text))
                || (checkEdit自杀未遂.Checked && !(string.IsNullOrWhiteSpace(ucLblTxtLbl自杀未遂.Txt1.Text)))
                || checkEdit无影响.Checked)
            { }
            else
            {
                emptySpaceItem影响.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radioGroup关锁情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup关锁情况.EditValue.ToString()))
            {
                layoutControlItem关锁情况.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radioGroup住院情况.EditValue == null || string.IsNullOrWhiteSpace(radioGroup住院情况.EditValue.ToString()))
            {
                layoutControlItem住院情况.AppearanceItemCaption.ForeColor = Color.Red;
            }
            //else if ((radioGroup住院情况.EditValue.ToString() == "3") && (string.IsNullOrWhiteSpace(ucLblDtp末次出院情况.Dtp1.Text)))
            //{
            //    layoutControlItem住院情况.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            else { }

            if (radio实验室检查.EditValue == null || string.IsNullOrWhiteSpace(radio实验室检查.EditValue.ToString()))
            {
                layoutControlItem实验室检查.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (radio实验室检查.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit实验室检查.Text))
            {
                layoutControlItem实验室检查.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else { }

            if (radioGroup服药依从性.EditValue == null || string.IsNullOrWhiteSpace(radioGroup服药依从性.EditValue.ToString()))
            {
                layoutControlItem服药依从性.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radio药物不良反应.EditValue == null || string.IsNullOrWhiteSpace(radio药物不良反应.EditValue.ToString()))
            {
                layoutControlItem药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (radio药物不良反应.EditValue.ToString() == "1" && string.IsNullOrWhiteSpace(textEdit药物不良反应.Text))
            {
                layoutControlItem药物不良反应.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else { }

            if (radioGroup治疗效果.EditValue == null || string.IsNullOrWhiteSpace(radioGroup治疗效果.EditValue.ToString()))
            {
                layoutControlItem治疗效果.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit此次随访分类)))
            {
                layoutControlItem此次随访分类.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (radio是否转诊.EditValue == null || string.IsNullOrWhiteSpace(radio是否转诊.EditValue.ToString()))
            {
                layoutControlItem是否转诊.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (radio是否转诊.EditValue.ToString() == "1" && (string.IsNullOrWhiteSpace(textEdit转诊原因.Text) || string.IsNullOrWhiteSpace(textEdit转诊科室.Text)))
            {
                layoutControlItem是否转诊.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else { }

            if (checkEdit生活劳动.Checked || checkEdit职业训练.Checked || checkEdit学习能力.Checked
                || checkEdit社会交往.Checked || checkEdit康复措施其他.Checked)
            { }
            else
            {
                layoutControlItem康复措施.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                layoutControlItem下次随访日期.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                layoutControlItem随访医生.AppearanceItemCaption.ForeColor = Color.Red;
            }
        }

        private void checkEdit症状其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit症状其他.Checked)
            {
                textEdit症状其他.Enabled = true;
            }
            else
            {
                textEdit症状其他.Enabled = false;
                textEdit症状其他.Text = "";
            }
        }

        private void checkEdit轻度滋事_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit轻度滋事.Checked)
            {
                ucLblTxtLbl轻度滋事.Enabled = true;
            }
            else
            {
                ucLblTxtLbl轻度滋事.Enabled = false;
                ucLblTxtLbl轻度滋事.Txt1.Text = "";
            }
        }

        private void checkEdit肇事_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit肇事.Checked)
            {
                ucLblTxtLbl肇事.Enabled = true;
            }
            else
            {
                ucLblTxtLbl肇事.Enabled = false;
                ucLblTxtLbl肇事.Txt1.Text = "";
            }
        }

        private void checkEdit肇祸_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit肇祸.Checked)
            {
                ucLblTxtLbl肇祸.Enabled = true;
            }
            else
            {
                ucLblTxtLbl肇祸.Enabled = false;
                ucLblTxtLbl肇祸.Txt1.Text = "";
            }
        }

        private void checkEdit自伤_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit自伤.Checked)
            {
                ucLblTxtLbl自伤.Enabled = true;
            }
            else
            {
                ucLblTxtLbl自伤.Enabled = false;
                ucLblTxtLbl自伤.Txt1.Text = "";
            }
        }

        private void checkEdit自杀未遂_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit自杀未遂.Checked)
            {
                ucLblTxtLbl自杀未遂.Enabled = true;
            }
            else
            {
                ucLblTxtLbl自杀未遂.Enabled = false;
                ucLblTxtLbl自杀未遂.Txt1.Text = "";
            }
        }

        private void checkEdit其他危害行为_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit其他危害行为.Checked)
            {
                ucLblTxtLbl其他危害行为.Enabled = true;
            }
            else
            {
                ucLblTxtLbl其他危害行为.Enabled = false;
                ucLblTxtLbl其他危害行为.Txt1.Text = "";
            }
        }

        private void radioGroup住院情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroup住院情况.EditValue != null && radioGroup住院情况.EditValue.ToString() == "3")
            {
                ucLblDtp末次出院情况.Enabled = true;
            }
            else
            {
                ucLblDtp末次出院情况.Enabled = false;
                ucLblDtp末次出院情况.Dtp1.Text = "";
            }
        }

        private void radio药物不良反应_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio药物不良反应.EditValue != null && radio药物不良反应.EditValue.ToString() == "1")
            {
                textEdit药物不良反应.Enabled = true;
            }
            else
            {
                textEdit药物不良反应.Enabled = false;
                textEdit药物不良反应.Text = "";
            }
        }

        private void radio实验室检查_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio实验室检查.EditValue != null && radio实验室检查.EditValue.ToString() == "1")
            {
                textEdit实验室检查.Enabled = true;
            }
            else
            {
                textEdit实验室检查.Enabled = false;
                textEdit实验室检查.Text = "";
            }
        }

        private void radio是否转诊_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否转诊.EditValue != null && radio是否转诊.EditValue.ToString() == "1")
            {
                textEdit转诊原因.Enabled = true;
                textEdit转诊科室.Enabled = true;
            }
            else
            {
                textEdit转诊原因.Enabled = false;
                textEdit转诊科室.Enabled = false;
                textEdit转诊原因.Text = "";
                textEdit转诊科室.Text = "";
            }
        }

        private void checkEdit康复措施其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit康复措施其他.Checked)
            {
                textEdit康复措施其他.Enabled = true;
            }
            else
            {
                textEdit康复措施其他.Enabled = false;
                textEdit康复措施其他.Text = "";
            }
        }

        private void textEditCheckNum_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit textedit = sender as DevExpress.XtraEditors.TextEdit;
            if (string.IsNullOrWhiteSpace(textedit.Text))
            {
                return;
            }

            try
            {
                int count = Convert.ToInt32(textedit.Text);
                if (count > 10000)
                {
                    Msg.ShowInformation("请属于小于10000的整数");
                    textedit.Focus();
                }
            }
            catch
            {
                Msg.ShowInformation("请属于小于10000的整数");
                textedit.Focus();
            }
        }

        private void comboBoxEdit是否通知联席部门_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit是否通知联席部门);

            //1是“是”的编码
            if (value != null && value.Equals("1"))
            {
                txt受理人1姓名.Enabled = true;
                txt受理人2姓名.Enabled = true;
                txt受理人1电话.Enabled = true;
                txt受理人2电话.Enabled = true;
            }
            else
            {
                txt受理人1姓名.Enabled = false;
                txt受理人2姓名.Enabled = false;
                txt受理人1电话.Enabled = false;
                txt受理人2电话.Enabled = false;
                txt受理人1姓名.Txt1.Text = "";
                txt受理人2姓名.Text = "";
                txt受理人1电话.Txt1.Text = "";
                txt受理人2电话.Text = "";
            }
        }

        private void comboBoxEdit是否需要转诊_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit是否需要转诊);
             //1是“是”的编码
            if (value != null && value.Equals("1"))
            {
                txt转诊原因.Enabled = true;
            }
            else
            {
                txt转诊原因.Enabled = false;
                txt转诊原因.Txt1.Text = "";
            }
        }

        private void comboBoxEdit转诊是否成功_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊是否成功);
            //1是“是”的编码
            if (value != null && value.Equals("1"))
            {
                txt转诊机构.Enabled = true;
                txt转诊科室.Enabled = true;
                txt转诊联系人.Enabled = true;
                txt转诊联系电话.Enabled = true;
            }
            else
            {
                txt转诊机构.Enabled = false;
                txt转诊科室.Enabled = false;
                txt转诊联系人.Enabled = false;
                txt转诊联系电话.Enabled = false;
                txt转诊机构.Txt1.Text = "";
                txt转诊科室.Txt1.Text = "";
                txt转诊联系人.Txt1.Text = "";
                txt转诊联系电话.Txt1.Text = "";
            }
        }

        private void comboBoxEdit是否联系精神专科医师_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit是否联系精神专科医师);
            //1是“是”的编码
            if (value != null && value.Equals("1"))
            {
                txt精神专科医师姓名.Enabled = true;
                txt精神专科医师电话.Enabled = true;
                txt处置结果.Enabled = true;
            }
            else
            {
                txt精神专科医师姓名.Enabled = false;
                txt精神专科医师电话.Enabled = false;
                txt处置结果.Enabled = false;
                txt精神专科医师姓名.Txt1.Text = "";
                txt精神专科医师电话.Txt1.Text = "";
                txt处置结果.Txt1.Text = "";
            }
        }

        private void checkEdit躯体疾病_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit躯体疾病.Checked)
            {
                radio躯体疾病.Enabled = true;
                checkEdit自杀.Enabled = checkEdit他杀.Enabled = checkEdit意外.Enabled = checkEdit精神疾病并发症.Enabled = checkEdit死亡原因其他.Enabled = false;
                checkEdit自杀.Checked = checkEdit他杀.Checked = checkEdit意外.Checked = checkEdit精神疾病并发症.Checked = checkEdit死亡原因其他.Checked = false;
            }
            else
            {
                radio躯体疾病.Enabled = false;
                radio躯体疾病.EditValue = "";
                checkEdit自杀.Enabled = checkEdit他杀.Enabled = checkEdit意外.Enabled = checkEdit精神疾病并发症.Enabled = checkEdit死亡原因其他.Enabled = true;
            }
        }




    }
}
