﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.Common;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class UC重性精神疾病患者个人信息补充表 : UserControlBase
    {
        #region Fields
        AtomEHR.Business.bll精神疾病信息补充表 _Bll = new Business.bll精神疾病信息补充表();
        DataSet _ds精神疾病信息补充表;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion
        public UC重性精神疾病患者个人信息补充表()
        {
            InitializeComponent();
        }

        public UC重性精神疾病患者个人信息补充表(frm个人健康 frm, AtomEHR.Common.UpdateType updateType)
        {
            InitializeComponent();
            _frm = frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = updateType;
            _id = _frm._param as string;
            _ds精神疾病信息补充表 = _Bll.GetBusinessByKey(_docNo, true);
        }

        private void UC重性精神疾病患者个人信息补充表_Load(object sender, EventArgs e)
        {
            InitView();

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t与户主关系, cbo与患者关系, false);
            //this.cbo与患者关系.SelectedIndex = 2;
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t知情同意, cbo知情同意, false);
            this.cbo知情同意.SelectedIndex = 0;

            DoBindingSummaryEditor(_ds精神疾病信息补充表);//绑定数据

        }

        private void InitView()
        {
            this.uc轻度滋事.Txt1.Enabled = false;
            this.uc肇祸.Txt1.Enabled = false;
            this.uc肇事.Txt1.Enabled = false;
            this.uc自杀未遂.Txt1.Enabled = false;
            this.uc自伤.Txt1.Enabled = false;
        }

        protected void DoBindingSummaryEditor(DataSet _ds精神疾病信息补充表)
        {
            DataTable dt精神疾病 = _ds精神疾病信息补充表.Tables[Models.tb_精神疾病信息补充表.__TableName];
            if (dt精神疾病 == null || dt精神疾病.Rows.Count == 0) return;
            DataTable dt健康档案 = _ds精神疾病信息补充表.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;
            this.lbl个人档案编号.Text = dt健康档案.Rows[0][tb_健康档案.个人档案编号].ToString();
            this.lbl姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.lbl性别.Text = _Bll.ReturnDis字典显示("xb_xingbie", dt健康档案.Rows[0][tb_健康档案.性别].ToString());
            this.lbl身份证号.Text = dt健康档案.Rows[0][tb_健康档案.身份证号].ToString();
            this.lbl出生日期.Text = dt健康档案.Rows[0][tb_健康档案.出生日期].ToString();
            this.lbl联系电话.Text = dt健康档案.Rows[0][tb_健康档案.联系人电话].ToString();
            this.lbl居住地址.Text = _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.市].ToString()) + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.区].ToString()) + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.街道].ToString()) + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.居委会].ToString()) + dt健康档案.Rows[0][tb_健康档案.居住地址].ToString();
            this.lbl婚姻状况.Text = _Bll.ReturnDis字典显示(Business.enumBase.hyzk.ToString(), dt健康档案.Rows[0][tb_健康档案.婚姻状况].ToString());
            this.textEdit档案类别.Text = _Bll.ReturnDis字典显示(Business.enumBase.dalb.ToString(),dt健康档案.Rows[0][tb_健康档案.档案类别].ToString());

            this.lbl创建时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建时间].ToString();
            this.lbl最近更新时间.Text = _serverDateTime;
            this.lbl当前所属机构.Text = _Bll.Return机构名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.所属机构].ToString());
            this.lbl创建机构.Text = _Bll.Return机构名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建机构].ToString());
            this.lbl创建人.Text = _Bll.Return用户名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建人].ToString());
            this.lbl最近更新人.Text = Loginer.CurrentUser.用户编码;
            //this.lbl创建机构.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.出生日期].ToString();
            //this.lbl联系电话.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.联系电话].ToString();
            //this.lbl居住地址.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.居住地址].ToString();
            //this.lbl婚姻状况.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.婚姻状况].ToString();

            util.ControlsHelper.SetComboxData(dt精神疾病.Rows[0][tb_精神疾病信息补充表.与患者关系].ToString(), cbo与患者关系);
            util.ControlsHelper.SetComboxData(dt精神疾病.Rows[0][tb_精神疾病信息补充表.知情同意].ToString(), cbo知情同意);

            //this.lbl个人档案编号.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.个人档案编号].ToString();
            DataBinder.BindingTextEdit(txt监护人姓名, dt精神疾病, tb_精神疾病信息补充表.监护人姓名);
            DataBinder.BindingTextEdit(txt监护人地址, dt精神疾病, tb_精神疾病信息补充表.监护人住址);
            DataBinder.BindingTextEdit(txt监护人电话, dt精神疾病, tb_精神疾病信息补充表.监护人电话);
            DataBinder.BindingTextEdit(txt联系人电话, dt精神疾病, tb_精神疾病信息补充表.联系人电话);
            DataBinder.BindingTextEditDateTime(dte初次发病时间, dt精神疾病, tb_精神疾病信息补充表.初次发病时间);
            DataBinder.BindingTextEdit(txt知情同意签字, dt精神疾病, tb_精神疾病信息补充表.签字);
            DataBinder.BindingTextEditDateTime(dte知情同意签字时间, dt精神疾病, tb_精神疾病信息补充表.签字时间);
            SetFlowLayoutResult(dt精神疾病.Rows[0][tb_精神疾病信息补充表.既往主要症状].ToString(), flow既往主要症状);
            DataBinder.BindingTextEdit(txt症状其他, dt精神疾病, tb_精神疾病信息补充表.既往主要症状其他);
            DataBinder.BindingRadioEdit(radio门诊, dt精神疾病, tb_精神疾病信息补充表.门诊状况);
            DataBinder.BindingTextEditDateTime(dte首次治疗时间.Dtp1, dt精神疾病, tb_精神疾病信息补充表.首次抗精神病药治疗时间);
            DataBinder.BindingTextEdit(txt住院此次.Txt1, dt精神疾病, tb_精神疾病信息补充表.住院次数);
            DataBinder.BindingTextEdit(txt诊断.Txt1, dt精神疾病, tb_精神疾病信息补充表.诊断);
            DataBinder.BindingTextEdit(txt住院此次.Txt1, dt精神疾病, tb_精神疾病信息补充表.住院次数);
            DataBinder.BindingTextEdit(txt确诊医院.Txt1, dt精神疾病, tb_精神疾病信息补充表.确诊医院);
            DataBinder.BindingTextEdit(dte确诊日期.Dtp1, dt精神疾病, tb_精神疾病信息补充表.确诊日期);
            DataBinder.BindingRadioEdit(radio最后一次治疗效果, dt精神疾病, tb_精神疾病信息补充表.治疗效果);
            //DataBinder.BindingTextEditDateTime(dte末次月经, dataTable, tb_孕妇_产前随访1次.末次月经);
            DataBinder.BindingTextEdit(uc自伤.Txt1, dt精神疾病, tb_精神疾病信息补充表.自伤);
            DataBinder.BindingTextEdit(uc自杀未遂.Txt1, dt精神疾病, tb_精神疾病信息补充表.自杀未遂);
            DataBinder.BindingTextEdit(uc肇事.Txt1, dt精神疾病, tb_精神疾病信息补充表.肇事);
            DataBinder.BindingTextEdit(uc肇祸.Txt1, dt精神疾病, tb_精神疾病信息补充表.肇祸);
            DataBinder.BindingTextEdit(uc轻度滋事.Txt1, dt精神疾病, tb_精神疾病信息补充表.轻度滋事);
            //add 公卫改版升级 ▽
            //Set户别(dt精神疾病);
            DataBinder.BindingRadioEdit(radioGroup就业情况, dt精神疾病, tb_精神疾病信息补充表.就业情况);
            DataBinder.BindingTextEdit(uc其他危害行为.Txt1, dt精神疾病, tb_精神疾病信息补充表.危险行为_其他危害);
            DataBinder.BindingTextEdit(txt患者签名, dt精神疾病, tb_精神疾病信息补充表.患者签字);
            //add 公卫改版升级 △
            Set影响(dt精神疾病.Rows[0][tb_精神疾病信息补充表.对家庭和社会的影响].ToString());
            DataBinder.BindingRadioEdit(this.radio关锁情况, dt精神疾病, tb_精神疾病信息补充表.关锁情况);
            DataBinder.BindingRadioEdit(this.radio经济状况, dt精神疾病, tb_精神疾病信息补充表.经济状况);
            DataBinder.BindingTextEdit(this.txt医生意见, dt精神疾病, tb_精神疾病信息补充表.医生意见);
            DataBinder.BindingTextEditDateTime(dte检查日期, dt精神疾病, tb_精神疾病信息补充表.检查日期);
            DataBinder.BindingTextEdit(this.txt医生签字, dt精神疾病, tb_精神疾病信息补充表.医生签名);
        }

        private void Set影响(string index)
        {
            if (string.IsNullOrEmpty(index)) return;
            string[] a = index.Split(',');
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < flow影响.Controls.Count; j++)
                {
                    if (flow影响.Controls[j] is CheckEdit)
                    {
                        CheckEdit chk = (CheckEdit)flow影响.Controls[j];
                        if (chk.Tag != null && !string.IsNullOrEmpty(chk.Tag.ToString()) && !string.IsNullOrEmpty(a[i]) && a[i] == chk.Tag.ToString())
                        {
                            chk.Checked = true;
                        }
                    }
                }
            }
        }
        private string Get影响()
        {
            string result = string.Empty;
            for (int j = 0; j < flow影响.Controls.Count; j++)
            {
                if (flow影响.Controls[j] is CheckEdit)
                {
                    CheckEdit chk = (CheckEdit)flow影响.Controls[j];
                    if (chk.Checked)
                    {
                        result += chk.Tag + ",";
                    }
                }
            }
            return result;
        }

        private void chk影响无_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk影响无.Checked)
            {
                this.chk影响1.Enabled = this.chk影响2.Enabled = this.chk影响3.Enabled = this.chk影响4.Enabled = this.chk影响5.Enabled = this.uc轻度滋事.Txt1.Enabled = this.uc肇祸.Txt1.Enabled = this.uc肇事.Txt1.Enabled = this.uc自杀未遂.Txt1.Enabled = this.uc自伤.Txt1.Enabled = false;

                this.chk影响1.Checked = this.chk影响2.Checked = this.chk影响3.Checked = this.chk影响4.Checked = this.chk影响5.Checked = false;
                this.uc轻度滋事.Txt1.Text = this.uc肇祸.Txt1.Text = this.uc肇事.Txt1.Text = this.uc自杀未遂.Txt1.Text = this.uc自伤.Txt1.Text = "";
            }
            else
            {
                this.chk影响1.Enabled = this.chk影响2.Enabled = this.chk影响3.Enabled = this.chk影响4.Enabled = this.chk影响5.Enabled = this.uc轻度滋事.Txt1.Enabled = this.uc肇祸.Txt1.Enabled = this.uc肇事.Txt1.Enabled = this.uc自杀未遂.Txt1.Enabled = this.uc自伤.Txt1.Enabled = true;
            }
        }

        private void chk影响5_CheckedChanged(object sender, EventArgs e)
        {
            if (chk影响5.Checked)
            {
                this.uc自杀未遂.Txt1.Enabled = true;
            }
            else
            {
                this.uc自杀未遂.Txt1.Enabled = false;
                this.uc自杀未遂.Txt1.Text = "";
            }
        }

        private void chk影响4_CheckedChanged(object sender, EventArgs e)
        {
            if (chk影响4.Checked)
            {
                this.uc自伤.Txt1.Enabled = true;
            }
            else
            {
                this.uc自伤.Txt1.Enabled = false;
                this.uc自伤.Txt1.Text = "";
            }
        }

        private void chk影响3_CheckedChanged(object sender, EventArgs e)
        {
            if (chk影响3.Checked)
            {
                this.uc肇祸.Txt1.Enabled = true;
            }
            else
            {
                this.uc肇祸.Txt1.Enabled = false;
                this.uc肇祸.Txt1.Text = "";
            }
        }

        private void chk影响2_CheckedChanged(object sender, EventArgs e)
        {
            if (chk影响2.Checked)
            {
                this.uc肇事.Txt1.Enabled = true;
            }
            else
            {
                this.uc肇事.Txt1.Enabled = false;
                this.uc肇事.Txt1.Text = "";
            }
        }

        private void chk影响1_CheckedChanged(object sender, EventArgs e)
        {
            if (chk影响1.Checked)
            {
                this.uc轻度滋事.Txt1.Enabled = true;
            }
            else
            {
                this.uc轻度滋事.Txt1.Enabled = false;
                this.uc轻度滋事.Txt1.Text = "";
            }
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.与患者关系] = util.ControlsHelper.GetComboxKey(cbo与患者关系);
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.知情同意] = util.ControlsHelper.GetComboxKey(cbo知情同意);
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.初次发病时间] = this.dte初次发病时间.Text;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.签字时间] = this.dte知情同意签字时间.Text;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.既往主要症状] = GetFlowLayoutResult(flow既往主要症状);
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.确诊日期] = this.dte确诊日期.Dtp1.Text;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.检查日期] = this.dte检查日期.Text;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.对家庭和社会的影响] = Get影响();
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.修改时间] = _serverDateTime;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.修改人] = Loginer.CurrentUser.用户编码;
            int i = Get缺项();
            int j = Get完整度(i);
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.缺项] = i;
            _ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.完整度] = j;
            _ds精神疾病信息补充表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = i + "," + j;

            _Bll.WriteLog();//写日志
            DataSet dsTemp = _Bll.CreateSaveData(_Bll.CurrentBusiness, UpdateType.Modify);
            SaveResult result = _Bll.Save(dsTemp);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功！");
                UC重性精神疾病患者个人信息补充表_显示 uc = new UC重性精神疾病患者个人信息补充表_显示(_Bll.CurrentBusiness, _frm);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.ShowInformation("保存失败！");
            }
        }

        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((18 - i) * 100 / 18.0);
            return k;
        }

        private int Get缺项()
        {
            int i = 0;
            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人姓名] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人姓名].ToString())) i++;
            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.与患者关系] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.与患者关系].ToString())) i++;
            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人住址] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人住址].ToString())) i++;
            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人电话] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.监护人电话].ToString())) i++;
            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.联系人电话] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.联系人电话].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.初次发病时间] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.初次发病时间].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.既往主要症状] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.既往主要症状].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.门诊状况] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.门诊状况].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.住院次数] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.住院次数].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.诊断] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.诊断].ToString()) && (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.确诊医院] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.确诊医院].ToString())) && (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.确诊日期] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.确诊日期].ToString()))) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.知情同意] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.知情同意].ToString()) && (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.签字] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.签字].ToString())) && (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.签字时间] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.签字时间].ToString()))) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.治疗效果] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.治疗效果].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.对家庭和社会的影响] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.对家庭和社会的影响].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.关锁情况] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.关锁情况].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.经济状况] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.经济状况].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.医生意见] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.医生意见].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.检查日期] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.检查日期].ToString())) i++;

            if (_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.医生签名] == null || string.IsNullOrEmpty(_ds精神疾病信息补充表.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.医生签名].ToString())) i++;

            return i;
        }
    }
}
