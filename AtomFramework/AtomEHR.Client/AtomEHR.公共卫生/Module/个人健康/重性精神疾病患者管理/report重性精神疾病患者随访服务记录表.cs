﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class report重性精神疾病患者随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds精神疾病;
        DataSet _ds用药情况;
        AtomEHR.Business.bll精神病随访记录 _bll随访 = new Business.bll精神病随访记录();
        string docNo;
        string year;
        #endregion
        public report重性精神疾病患者随访服务记录表()
        {
            InitializeComponent();
        }

        public report重性精神疾病患者随访服务记录表(string docNo, string year)
        {
            InitializeComponent();
            this.docNo = docNo;
            this.year = year;
            _ds精神疾病 = _bll随访.GetInfoBySuiFang(docNo, year, true);
            DoBindingDataSource(_ds精神疾病);
        }

        private void DoBindingDataSource(DataSet _ds精神疾病)
        {
            DataTable dt精神疾病 = _ds精神疾病.Tables[Models.tb_精神病随访记录.__TableName];
            
            //用药情况
            if (dt精神疾病.Rows.Count == 1)
            {
                string id = dt精神疾病.Rows[0][tb_精神病随访记录.ID].ToString();
                _ds用药情况 = new bll精神病_用药情况().GetBusinessByKey(id, false);
            }

            DataTable dt健康档案 = _ds精神疾病.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());

            if (dt精神疾病 != null && dt精神疾病.Rows.Count > 0)
            {
                this.txt随访日期.Text = Convert.ToDateTime(dt精神疾病.Rows[0][tb_精神病随访记录.随访日期]).ToLongDateString().ToString();//将时间转换成年月日格式
                //由于选项是从0开始的，而下标是从1开始的，所以强转后-1就能获得从0开始的选项
                this.txt危险性.Text = (Convert.ToInt32(dt精神疾病.Rows[0][tb_精神病随访记录.危险性]) - 1).ToString();
                string 目前症状 = dt精神疾病.Rows[0][tb_精神病随访记录.目前症状].ToString();
                if (!string.IsNullOrEmpty(目前症状))
                {
                    string[] a = 目前症状.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt目前症状" + a[i];
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99") lbl.Text = "12";
                        else lbl.Text = a[i];
                    }
                }
                this.txt目前症状其他.Text = dt精神疾病.Rows[0][tb_精神病随访记录.目前症状其他].ToString();
                this.txt自知力.Text = dt精神疾病.Rows[0][tb_精神病随访记录.自知力].ToString();
                this.txt睡眠情况.Text = dt精神疾病.Rows[0][tb_精神病随访记录.睡眠情况].ToString();
                this.txt饮食情况.Text = dt精神疾病.Rows[0][tb_精神病随访记录.饮食情况].ToString();
                this.txt个人生活料理.Text = dt精神疾病.Rows[0][tb_精神病随访记录.个人生活料理].ToString();
                this.txt家务劳动.Text = dt精神疾病.Rows[0][tb_精神病随访记录.家务劳动].ToString();
                this.txt生产劳动及工作.Text = dt精神疾病.Rows[0][tb_精神病随访记录.生产劳动及工作].ToString();
                this.txt学习能力.Text = dt精神疾病.Rows[0][tb_精神病随访记录.学习能力].ToString();
                this.txt社会人际交往.Text = dt精神疾病.Rows[0][tb_精神病随访记录.社会人际交往].ToString();
                this.txt轻度滋事次数.Text = dt精神疾病.Rows[0][tb_精神病随访记录.影响轻度滋事].ToString();
                this.txt肇事次数.Text = dt精神疾病.Rows[0][tb_精神病随访记录.影响肇事].ToString();
                this.txt肇祸次数.Text = dt精神疾病.Rows[0][tb_精神病随访记录.影响肇祸].ToString();
                this.txt自伤次数.Text = dt精神疾病.Rows[0][tb_精神病随访记录.影响自伤].ToString();
                this.txt自杀未遂次数.Text = dt精神疾病.Rows[0][tb_精神病随访记录.影响自杀未遂].ToString();
                this.txt关锁情况.Text = dt精神疾病.Rows[0][tb_精神病随访记录.关锁情况].ToString();
                this.txt住院情况.Text = (Convert.ToInt32(dt精神疾病.Rows[0][tb_精神病随访记录.住院情况]) - 1).ToString();
                this.txt末次出院时间.Text = dt精神疾病.Rows[0][tb_精神病随访记录.末次出院时间].ToString();
                this.txt实验室检查.Text = dt精神疾病.Rows[0][tb_精神病随访记录.实验室检查].ToString();
                //由于该选项中数据与显示页面中的数据顺序不一致 ，所以需要定义一下
                if (this.txt实验室检查.Text == "1") this.txt实验室检查.Text = "2";
                else this.txt实验室检查.Text = "1";
                this.txt实验室检查有.Text = dt精神疾病.Rows[0][tb_精神病随访记录.实验室检查其他].ToString();
                this.txt服药依从性.Text = dt精神疾病.Rows[0][tb_精神病随访记录.服药依从性].ToString();
                this.txt药物不良反应.Text = dt精神疾病.Rows[0][tb_精神病随访记录.药物不良反应有无].ToString();
                if (this.txt药物不良反应.Text == "1") this.txt药物不良反应.Text = "2";
                else this.txt药物不良反应.Text = "1";
                this.txt药物不良反应有.Text = dt精神疾病.Rows[0][tb_精神病随访记录.药物不良反应].ToString();
                this.txt治疗效果.Text = dt精神疾病.Rows[0][tb_精神病随访记录.治疗效果].ToString();
                this.txt是否转诊.Text = dt精神疾病.Rows[0][tb_精神病随访记录.是否转诊].ToString();
                if (this.txt是否转诊.Text == "1") this.txt是否转诊.Text = "2";
                else this.txt是否转诊.Text = "1";
                this.txt转诊原因.Text = dt精神疾病.Rows[0][tb_精神病随访记录.转诊原因].ToString();
                this.txt转诊至机构及科室.Text = dt精神疾病.Rows[0][tb_精神病随访记录.转诊科室].ToString();

                if (_ds用药情况 !=null && _ds用药情况.Tables[0].Rows.Count != 0)
                {
                    for (int i = 0; i < _ds用药情况.Tables[0].Rows.Count; i++)
                    {
                        //最多显示3行，超过3行以后不显示
                        if (i > 2)
                        {
                            break;
                        }
                        string ctr药物 = "txt药物" + (i + 1);
                        string ctr用法 = "txt用法" + (i + 1);
                        string ctr剂量 = "txt剂量" + (i + 1);

                        XRLabel lbl药物 = (XRLabel)FindControl(ctr药物, false);
                        XRLabel lbl用法 = (XRLabel)FindControl(ctr用法, false);
                        XRLabel lbl剂量 = (XRLabel)FindControl(ctr剂量, false);

                        lbl药物.Text = _ds用药情况.Tables[0].Rows[i][tb_精神病_用药情况.药物名称].ToString();
                        lbl用法.Text = _ds用药情况.Tables[0].Rows[i][tb_精神病_用药情况.用法].ToString();
                        lbl剂量.Text = _ds用药情况.Tables[0].Rows[i][tb_精神病_用药情况.剂量].ToString();
                    }
                }

                string 康复措施 = dt精神疾病.Rows[0][tb_精神病随访记录.康复措施].ToString();
                if (!string.IsNullOrEmpty(康复措施))
                {
                    string[] a = 康复措施.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt康复措施" + a[i];
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (a[i] == "99") lbl.Text = "5";
                        else lbl.Text = a[i];
                    }
                }
                this.txt康复措施其他.Text = dt精神疾病.Rows[0][tb_精神病随访记录.康复措施其他].ToString();
                this.txt本次随访分类.Text = dt精神疾病.Rows[0][tb_精神病随访记录.此次随访分类].ToString();
                this.txt下次随访日期.Text = Convert.ToDateTime(dt精神疾病.Rows[0][tb_精神病随访记录.下次随访日期]).ToLongDateString().ToString();//将时间转换成年月日格式
                this.txt随访医生签名.Text = dt精神疾病.Rows[0][tb_精神病随访记录.随访医生].ToString();
            }
        }

    }
}
