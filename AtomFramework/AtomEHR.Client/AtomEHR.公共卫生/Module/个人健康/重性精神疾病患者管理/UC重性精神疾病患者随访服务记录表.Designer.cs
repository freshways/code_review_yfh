﻿namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    partial class UC重性精神疾病患者随访服务记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.sbtn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt死亡日期 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit躯体疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.radio躯体疾病 = new DevExpress.XtraEditors.RadioGroup();
            this.checkEdit自杀 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit他杀 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit意外 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit精神疾病并发症 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit死亡原因其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit患者或家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxEdit是否需要转诊 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxEdit转诊是否成功 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt转诊机构 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt转诊科室 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt转诊联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt转诊联系电话 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxEdit是否联系精神专科医师 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt精神专科医师姓名 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt精神专科医师电话 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt处置结果 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxEdit是否通知联席部门 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt受理人1姓名 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt受理人2姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt受理人1电话 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt受理人2电话 = new DevExpress.XtraEditors.TextEdit();
            this.radio失访原因 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.radio本次随访形式 = new DevExpress.XtraEditors.RadioGroup();
            this.gridControl用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gridView用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.deleteHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit生活劳动 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit职业训练 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit学习能力 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit社会交往 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit康复措施其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit康复措施其他 = new DevExpress.XtraEditors.TextEdit();
            this.sbtn添加药物 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit转诊科室 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.radio是否转诊 = new DevExpress.XtraEditors.RadioGroup();
            this.comboBoxEdit此次随访分类 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit药物不良反应 = new DevExpress.XtraEditors.TextEdit();
            this.radio药物不良反应 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup服药依从性 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit实验室检查 = new DevExpress.XtraEditors.TextEdit();
            this.radio实验室检查 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup社会人际交往 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup学习能力 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup生产劳动及工作 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup家务劳动 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup个人生活料理 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup饮食情况 = new DevExpress.XtraEditors.RadioGroup();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.radioGroup关锁情况 = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.ucLblTxtLbl其他危害行为 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit其他危害行为 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit无影响 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxtLbl自杀未遂 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit自杀未遂 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxtLbl自伤 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit自伤 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxtLbl肇祸 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit肇祸 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxtLbl肇事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit肇事 = new DevExpress.XtraEditors.CheckEdit();
            this.ucLblTxtLbl轻度滋事 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.checkEdit轻度滋事 = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.ucLblDtp末次出院情况 = new AtomEHR.Library.UserControls.UCLblDtp();
            this.radioGroup住院情况 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.radioGroup治疗效果 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit幻觉 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit交流困难 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit猜疑 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit喜怒无常 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit行为怪异 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit兴奋话多 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit伤人毁物 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit悲观厌世 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit无故外走 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit自语自笑 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit孤僻懒散 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit症状其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit症状其他 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl婚姻状况 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl性别 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit危险性 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.radioGroup自知力 = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroup睡眠情况 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem个人生活料理 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem家务劳动 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem生产劳动及工作 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem学习能力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem社会人际交往 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem关锁情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem住院情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem实验室检查 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem服药依从性 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem药物不良反应 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem治疗效果 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem此次随访分类 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem是否转诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem康复措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem影响 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem用药情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem危险性 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem自知力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem目前症状 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem睡眠情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem饮食情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡日期.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit躯体疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio躯体疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自杀.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit他杀.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit意外.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit精神疾病并发症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit死亡原因其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患者或家属签名.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否需要转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊是否成功.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否联系精神专科医师.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否通知联席部门.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio失访原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio本次随访形式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteHyperLinkEdit)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit生活劳动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit职业训练.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit学习能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit社会交往.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit康复措施其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复措施其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit此次随访分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit药物不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物不良反应.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药依从性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit实验室检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio实验室检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup社会人际交往.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup学习能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup生产劳动及工作.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup家务劳动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup个人生活料理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup饮食情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup关锁情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他危害行为.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit无影响.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自杀未遂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自伤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit肇祸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit肇事.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit轻度滋事.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup住院情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup治疗效果.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit幻觉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit交流困难.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit猜疑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit喜怒无常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit行为怪异.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit兴奋话多.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伤人毁物.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit悲观厌世.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit无故外走.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自语自笑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit孤僻懒散.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit症状其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit危险性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup自知力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup睡眠情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem个人生活料理)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem家务劳动)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem生产劳动及工作)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem学习能力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem社会人际交往)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem关锁情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住院情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem实验室检查)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem服药依从性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem药物不良反应)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem治疗效果)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem此次随访分类)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem是否转诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem康复措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem影响)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem危险性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem自知力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem目前症状)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem睡眠情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem饮食情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.sbtn保存);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // sbtn保存
            // 
            this.sbtn保存.Location = new System.Drawing.Point(3, 3);
            this.sbtn保存.Name = "sbtn保存";
            this.sbtn保存.Size = new System.Drawing.Size(75, 23);
            this.sbtn保存.TabIndex = 0;
            this.sbtn保存.Text = "保存";
            this.sbtn保存.Click += new System.EventHandler(this.sbtn保存_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt死亡日期);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.textEdit患者或家属签名);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.radio失访原因);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.radio本次随访形式);
            this.layoutControl1.Controls.Add(this.gridControl用药情况);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.sbtn添加药物);
            this.layoutControl1.Controls.Add(this.textEdit转诊科室);
            this.layoutControl1.Controls.Add(this.textEdit转诊原因);
            this.layoutControl1.Controls.Add(this.radio是否转诊);
            this.layoutControl1.Controls.Add(this.comboBoxEdit此次随访分类);
            this.layoutControl1.Controls.Add(this.textEdit药物不良反应);
            this.layoutControl1.Controls.Add(this.radio药物不良反应);
            this.layoutControl1.Controls.Add(this.radioGroup服药依从性);
            this.layoutControl1.Controls.Add(this.textEdit实验室检查);
            this.layoutControl1.Controls.Add(this.radio实验室检查);
            this.layoutControl1.Controls.Add(this.radioGroup社会人际交往);
            this.layoutControl1.Controls.Add(this.radioGroup学习能力);
            this.layoutControl1.Controls.Add(this.radioGroup生产劳动及工作);
            this.layoutControl1.Controls.Add(this.radioGroup家务劳动);
            this.layoutControl1.Controls.Add(this.radioGroup个人生活料理);
            this.layoutControl1.Controls.Add(this.radioGroup饮食情况);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.radioGroup关锁情况);
            this.layoutControl1.Controls.Add(this.panelControl5);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.labelControl当前所属机构);
            this.layoutControl1.Controls.Add(this.labelControl创建机构);
            this.layoutControl1.Controls.Add(this.labelControl最近修改人);
            this.layoutControl1.Controls.Add(this.labelControl创建人);
            this.layoutControl1.Controls.Add(this.labelControl最近更新时间);
            this.layoutControl1.Controls.Add(this.labelControl1创建时间);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.radioGroup治疗效果);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.labelControl居住地址);
            this.layoutControl1.Controls.Add(this.labelControl联系电话);
            this.layoutControl1.Controls.Add(this.labelControl婚姻状况);
            this.layoutControl1.Controls.Add(this.labelControl身份证号);
            this.layoutControl1.Controls.Add(this.labelControl出生日期);
            this.layoutControl1.Controls.Add(this.labelControl性别);
            this.layoutControl1.Controls.Add(this.labelControl姓名);
            this.layoutControl1.Controls.Add(this.labelControl个人档案号);
            this.layoutControl1.Controls.Add(this.comboBoxEdit危险性);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Controls.Add(this.radioGroup自知力);
            this.layoutControl1.Controls.Add(this.radioGroup睡眠情况);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 468);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt死亡日期
            // 
            this.txt死亡日期.Location = new System.Drawing.Point(158, 233);
            this.txt死亡日期.Name = "txt死亡日期";
            this.txt死亡日期.Size = new System.Drawing.Size(147, 20);
            this.txt死亡日期.StyleController = this.layoutControl1;
            this.txt死亡日期.TabIndex = 109;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.checkEdit躯体疾病);
            this.flowLayoutPanel5.Controls.Add(this.radio躯体疾病);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit自杀);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit他杀);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit意外);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit精神疾病并发症);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit死亡原因其他);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(158, 257);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(572, 116);
            this.flowLayoutPanel5.TabIndex = 107;
            // 
            // checkEdit躯体疾病
            // 
            this.checkEdit躯体疾病.Location = new System.Drawing.Point(3, 3);
            this.checkEdit躯体疾病.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.checkEdit躯体疾病.Name = "checkEdit躯体疾病";
            this.checkEdit躯体疾病.Properties.Caption = "躯体疾病";
            this.checkEdit躯体疾病.Size = new System.Drawing.Size(75, 19);
            this.checkEdit躯体疾病.TabIndex = 0;
            this.checkEdit躯体疾病.Tag = "1";
            this.checkEdit躯体疾病.CheckedChanged += new System.EventHandler(this.checkEdit躯体疾病_CheckedChanged);
            // 
            // radio躯体疾病
            // 
            this.radio躯体疾病.Enabled = false;
            this.radio躯体疾病.Location = new System.Drawing.Point(78, 3);
            this.radio躯体疾病.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.radio躯体疾病.Name = "radio躯体疾病";
            this.radio躯体疾病.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "传染病和寄生虫病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "肿瘤"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "心脏病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "脑血管病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "呼吸系统疾病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "消化系统疾病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("7", "其他疾病"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("8", "不详")});
            this.radio躯体疾病.Size = new System.Drawing.Size(465, 50);
            this.radio躯体疾病.TabIndex = 6;
            // 
            // checkEdit自杀
            // 
            this.checkEdit自杀.Location = new System.Drawing.Point(3, 59);
            this.checkEdit自杀.Name = "checkEdit自杀";
            this.checkEdit自杀.Properties.Caption = "自杀";
            this.checkEdit自杀.Size = new System.Drawing.Size(75, 19);
            this.checkEdit自杀.TabIndex = 1;
            this.checkEdit自杀.Tag = "2";
            // 
            // checkEdit他杀
            // 
            this.checkEdit他杀.Location = new System.Drawing.Point(84, 59);
            this.checkEdit他杀.Name = "checkEdit他杀";
            this.checkEdit他杀.Properties.Caption = "他杀";
            this.checkEdit他杀.Size = new System.Drawing.Size(75, 19);
            this.checkEdit他杀.TabIndex = 2;
            this.checkEdit他杀.Tag = "3";
            // 
            // checkEdit意外
            // 
            this.checkEdit意外.Location = new System.Drawing.Point(165, 59);
            this.checkEdit意外.Name = "checkEdit意外";
            this.checkEdit意外.Properties.Caption = "意外";
            this.checkEdit意外.Size = new System.Drawing.Size(75, 19);
            this.checkEdit意外.TabIndex = 3;
            this.checkEdit意外.Tag = "4";
            // 
            // checkEdit精神疾病并发症
            // 
            this.checkEdit精神疾病并发症.Location = new System.Drawing.Point(246, 59);
            this.checkEdit精神疾病并发症.Name = "checkEdit精神疾病并发症";
            this.checkEdit精神疾病并发症.Properties.Caption = "精神疾病相关并发症";
            this.checkEdit精神疾病并发症.Size = new System.Drawing.Size(156, 19);
            this.checkEdit精神疾病并发症.TabIndex = 4;
            this.checkEdit精神疾病并发症.Tag = "5";
            // 
            // checkEdit死亡原因其他
            // 
            this.checkEdit死亡原因其他.Location = new System.Drawing.Point(408, 59);
            this.checkEdit死亡原因其他.Name = "checkEdit死亡原因其他";
            this.checkEdit死亡原因其他.Properties.Caption = "其他";
            this.checkEdit死亡原因其他.Size = new System.Drawing.Size(75, 19);
            this.checkEdit死亡原因其他.TabIndex = 5;
            this.checkEdit死亡原因其他.Tag = "99";
            // 
            // textEdit患者或家属签名
            // 
            this.textEdit患者或家属签名.Location = new System.Drawing.Point(606, 1257);
            this.textEdit患者或家属签名.Name = "textEdit患者或家属签名";
            this.textEdit患者或家属签名.Size = new System.Drawing.Size(124, 20);
            this.textEdit患者或家属签名.StyleController = this.layoutControl1;
            this.textEdit患者或家属签名.TabIndex = 106;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.label2);
            this.flowLayoutPanel4.Controls.Add(this.comboBoxEdit是否需要转诊);
            this.flowLayoutPanel4.Controls.Add(this.txt转诊原因);
            this.flowLayoutPanel4.Controls.Add(this.label3);
            this.flowLayoutPanel4.Controls.Add(this.comboBoxEdit转诊是否成功);
            this.flowLayoutPanel4.Controls.Add(this.txt转诊机构);
            this.flowLayoutPanel4.Controls.Add(this.txt转诊科室);
            this.flowLayoutPanel4.Controls.Add(this.txt转诊联系人);
            this.flowLayoutPanel4.Controls.Add(this.txt转诊联系电话);
            this.flowLayoutPanel4.Controls.Add(this.label4);
            this.flowLayoutPanel4.Controls.Add(this.comboBoxEdit是否联系精神专科医师);
            this.flowLayoutPanel4.Controls.Add(this.txt精神专科医师姓名);
            this.flowLayoutPanel4.Controls.Add(this.txt精神专科医师电话);
            this.flowLayoutPanel4.Controls.Add(this.txt处置结果);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(118, 1031);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(612, 166);
            this.flowLayoutPanel4.TabIndex = 105;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "是否需要转诊：";
            // 
            // comboBoxEdit是否需要转诊
            // 
            this.comboBoxEdit是否需要转诊.Location = new System.Drawing.Point(98, 3);
            this.comboBoxEdit是否需要转诊.Name = "comboBoxEdit是否需要转诊";
            this.comboBoxEdit是否需要转诊.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否需要转诊.Size = new System.Drawing.Size(59, 20);
            this.comboBoxEdit是否需要转诊.TabIndex = 3;
            this.comboBoxEdit是否需要转诊.TextChanged += new System.EventHandler(this.comboBoxEdit是否需要转诊_TextChanged);
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt转诊原因.Lbl1Text = "转诊原因：";
            this.txt转诊原因.Location = new System.Drawing.Point(180, 3);
            this.txt转诊原因.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Size = new System.Drawing.Size(351, 22);
            this.txt转诊原因.TabIndex = 6;
            this.txt转诊原因.Txt1Size = new System.Drawing.Size(200, 20);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 35);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "转诊是否成功：";
            // 
            // comboBoxEdit转诊是否成功
            // 
            this.comboBoxEdit转诊是否成功.Location = new System.Drawing.Point(98, 31);
            this.comboBoxEdit转诊是否成功.Name = "comboBoxEdit转诊是否成功";
            this.comboBoxEdit转诊是否成功.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit转诊是否成功.Size = new System.Drawing.Size(59, 20);
            this.comboBoxEdit转诊是否成功.TabIndex = 4;
            this.comboBoxEdit转诊是否成功.TextChanged += new System.EventHandler(this.comboBoxEdit转诊是否成功_TextChanged);
            // 
            // txt转诊机构
            // 
            this.txt转诊机构.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt转诊机构.Lbl1Text = "转诊机构：";
            this.txt转诊机构.Location = new System.Drawing.Point(180, 31);
            this.txt转诊机构.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.txt转诊机构.Name = "txt转诊机构";
            this.txt转诊机构.Size = new System.Drawing.Size(181, 22);
            this.txt转诊机构.TabIndex = 7;
            this.txt转诊机构.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt转诊科室
            // 
            this.txt转诊科室.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt转诊科室.Lbl1Text = "转诊科室：";
            this.txt转诊科室.Location = new System.Drawing.Point(367, 31);
            this.txt转诊科室.Name = "txt转诊科室";
            this.txt转诊科室.Size = new System.Drawing.Size(181, 22);
            this.txt转诊科室.TabIndex = 8;
            this.txt转诊科室.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt转诊联系人
            // 
            this.txt转诊联系人.Lbl1Size = new System.Drawing.Size(90, 18);
            this.txt转诊联系人.Lbl1Text = "转诊联系人：";
            this.txt转诊联系人.Location = new System.Drawing.Point(6, 59);
            this.txt转诊联系人.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.txt转诊联系人.Name = "txt转诊联系人";
            this.txt转诊联系人.Size = new System.Drawing.Size(165, 22);
            this.txt转诊联系人.TabIndex = 9;
            this.txt转诊联系人.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt转诊联系电话
            // 
            this.txt转诊联系电话.Lbl1Size = new System.Drawing.Size(80, 18);
            this.txt转诊联系电话.Lbl1Text = "转诊联系电话：";
            this.txt转诊联系电话.Location = new System.Drawing.Point(177, 59);
            this.txt转诊联系电话.Name = "txt转诊联系电话";
            this.txt转诊联系电话.Size = new System.Drawing.Size(292, 22);
            this.txt转诊联系电话.TabIndex = 10;
            this.txt转诊联系电话.Txt1Size = new System.Drawing.Size(150, 20);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 7, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "是否联系精神专科医师：";
            // 
            // comboBoxEdit是否联系精神专科医师
            // 
            this.comboBoxEdit是否联系精神专科医师.Location = new System.Drawing.Point(149, 87);
            this.comboBoxEdit是否联系精神专科医师.Name = "comboBoxEdit是否联系精神专科医师";
            this.comboBoxEdit是否联系精神专科医师.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否联系精神专科医师.Size = new System.Drawing.Size(60, 20);
            this.comboBoxEdit是否联系精神专科医师.TabIndex = 5;
            this.comboBoxEdit是否联系精神专科医师.TextChanged += new System.EventHandler(this.comboBoxEdit是否联系精神专科医师_TextChanged);
            // 
            // txt精神专科医师姓名
            // 
            this.txt精神专科医师姓名.Lbl1Size = new System.Drawing.Size(100, 18);
            this.txt精神专科医师姓名.Lbl1Text = "精神专科医师姓名：";
            this.txt精神专科医师姓名.Location = new System.Drawing.Point(215, 87);
            this.txt精神专科医师姓名.Name = "txt精神专科医师姓名";
            this.txt精神专科医师姓名.Size = new System.Drawing.Size(176, 22);
            this.txt精神专科医师姓名.TabIndex = 11;
            this.txt精神专科医师姓名.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt精神专科医师电话
            // 
            this.txt精神专科医师电话.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt精神专科医师电话.Lbl1Text = "医师电话：";
            this.txt精神专科医师电话.Location = new System.Drawing.Point(397, 87);
            this.txt精神专科医师电话.Name = "txt精神专科医师电话";
            this.txt精神专科医师电话.Size = new System.Drawing.Size(187, 22);
            this.txt精神专科医师电话.TabIndex = 12;
            this.txt精神专科医师电话.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // txt处置结果
            // 
            this.txt处置结果.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt处置结果.Lbl1Text = "处置结果：";
            this.txt处置结果.Location = new System.Drawing.Point(3, 115);
            this.txt处置结果.Name = "txt处置结果";
            this.txt处置结果.Size = new System.Drawing.Size(216, 22);
            this.txt处置结果.TabIndex = 13;
            this.txt处置结果.Txt1Size = new System.Drawing.Size(140, 20);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label1);
            this.flowLayoutPanel3.Controls.Add(this.comboBoxEdit是否通知联席部门);
            this.flowLayoutPanel3.Controls.Add(this.txt受理人1姓名);
            this.flowLayoutPanel3.Controls.Add(this.txt受理人2姓名);
            this.flowLayoutPanel3.Controls.Add(this.txt受理人1电话);
            this.flowLayoutPanel3.Controls.Add(this.txt受理人2电话);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(118, 971);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(612, 56);
            this.flowLayoutPanel3.TabIndex = 104;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "是否通知：";
            // 
            // comboBoxEdit是否通知联席部门
            // 
            this.comboBoxEdit是否通知联席部门.Location = new System.Drawing.Point(74, 3);
            this.comboBoxEdit是否通知联席部门.Name = "comboBoxEdit是否通知联席部门";
            this.comboBoxEdit是否通知联席部门.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否通知联席部门.Size = new System.Drawing.Size(60, 20);
            this.comboBoxEdit是否通知联席部门.TabIndex = 0;
            this.comboBoxEdit是否通知联席部门.TextChanged += new System.EventHandler(this.comboBoxEdit是否通知联席部门_TextChanged);
            // 
            // txt受理人1姓名
            // 
            this.txt受理人1姓名.Lbl1Size = new System.Drawing.Size(200, 18);
            this.txt受理人1姓名.Lbl1Text = "公安部门/社区综治中心受理人姓名：";
            this.txt受理人1姓名.Location = new System.Drawing.Point(140, 3);
            this.txt受理人1姓名.Name = "txt受理人1姓名";
            this.txt受理人1姓名.Size = new System.Drawing.Size(291, 22);
            this.txt受理人1姓名.TabIndex = 2;
            this.txt受理人1姓名.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt受理人2姓名
            // 
            this.txt受理人2姓名.Location = new System.Drawing.Point(437, 3);
            this.txt受理人2姓名.Name = "txt受理人2姓名";
            this.txt受理人2姓名.Size = new System.Drawing.Size(82, 20);
            this.txt受理人2姓名.TabIndex = 4;
            // 
            // txt受理人1电话
            // 
            this.txt受理人1电话.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt受理人1电话.Lbl1Text = "受理人电话：";
            this.txt受理人1电话.Location = new System.Drawing.Point(6, 31);
            this.txt受理人1电话.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.txt受理人1电话.Name = "txt受理人1电话";
            this.txt受理人1电话.Size = new System.Drawing.Size(203, 22);
            this.txt受理人1电话.TabIndex = 3;
            this.txt受理人1电话.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // txt受理人2电话
            // 
            this.txt受理人2电话.Location = new System.Drawing.Point(215, 31);
            this.txt受理人2电话.Name = "txt受理人2电话";
            this.txt受理人2电话.Size = new System.Drawing.Size(121, 20);
            this.txt受理人2电话.TabIndex = 5;
            // 
            // radio失访原因
            // 
            this.radio失访原因.Location = new System.Drawing.Point(118, 203);
            this.radio失访原因.Name = "radio失访原因";
            this.radio失访原因.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "外出打工"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "迁居他处"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "走失"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "连续3次未访到"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("99", "其他")});
            this.radio失访原因.Size = new System.Drawing.Size(612, 26);
            this.radio失访原因.StyleController = this.layoutControl1;
            this.radio失访原因.TabIndex = 103;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(370, 173);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(360, 26);
            this.layoutControl2.TabIndex = 102;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(360, 26);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // radio本次随访形式
            // 
            this.radio本次随访形式.Location = new System.Drawing.Point(118, 173);
            this.radio本次随访形式.Name = "radio本次随访形式";
            this.radio本次随访形式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭访视"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话")});
            this.radio本次随访形式.Size = new System.Drawing.Size(248, 26);
            this.radio本次随访形式.StyleController = this.layoutControl1;
            this.radio本次随访形式.TabIndex = 101;
            // 
            // gridControl用药情况
            // 
            this.gridControl用药情况.Location = new System.Drawing.Point(122, 918);
            this.gridControl用药情况.MainView = this.gridView用药情况;
            this.gridControl用药情况.Name = "gridControl用药情况";
            this.gridControl用药情况.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.deleteHyperLinkEdit});
            this.gridControl用药情况.Size = new System.Drawing.Size(608, 23);
            this.gridControl用药情况.TabIndex = 100;
            this.gridControl用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView用药情况});
            // 
            // gridView用药情况
            // 
            this.gridView用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView用药情况.GridControl = this.gridControl用药情况;
            this.gridView用药情况.Name = "gridView用药情况";
            this.gridView用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "药物名称";
            this.gridColumn1.FieldName = "药物名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "剂量";
            this.gridColumn2.FieldName = "剂量";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "用法";
            this.gridColumn3.FieldName = "用法";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "操作";
            this.gridColumn4.ColumnEdit = this.deleteHyperLinkEdit;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // deleteHyperLinkEdit
            // 
            this.deleteHyperLinkEdit.AutoHeight = false;
            this.deleteHyperLinkEdit.Name = "deleteHyperLinkEdit";
            this.deleteHyperLinkEdit.NullText = "删除";
            this.deleteHyperLinkEdit.Click += new System.EventHandler(this.deleteHyperLinkEdit_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.checkEdit生活劳动);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit职业训练);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit学习能力);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit社会交往);
            this.flowLayoutPanel2.Controls.Add(this.checkEdit康复措施其他);
            this.flowLayoutPanel2.Controls.Add(this.textEdit康复措施其他);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(118, 1230);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(612, 23);
            this.flowLayoutPanel2.TabIndex = 58;
            // 
            // checkEdit生活劳动
            // 
            this.checkEdit生活劳动.Location = new System.Drawing.Point(3, 1);
            this.checkEdit生活劳动.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit生活劳动.Name = "checkEdit生活劳动";
            this.checkEdit生活劳动.Properties.Caption = "生活劳动能力";
            this.checkEdit生活劳动.Size = new System.Drawing.Size(101, 19);
            this.checkEdit生活劳动.TabIndex = 1;
            // 
            // checkEdit职业训练
            // 
            this.checkEdit职业训练.Location = new System.Drawing.Point(110, 1);
            this.checkEdit职业训练.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit职业训练.Name = "checkEdit职业训练";
            this.checkEdit职业训练.Properties.Caption = "职业训练";
            this.checkEdit职业训练.Size = new System.Drawing.Size(75, 19);
            this.checkEdit职业训练.TabIndex = 2;
            // 
            // checkEdit学习能力
            // 
            this.checkEdit学习能力.Location = new System.Drawing.Point(191, 1);
            this.checkEdit学习能力.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit学习能力.Name = "checkEdit学习能力";
            this.checkEdit学习能力.Properties.Caption = "学习能力";
            this.checkEdit学习能力.Size = new System.Drawing.Size(75, 19);
            this.checkEdit学习能力.TabIndex = 3;
            // 
            // checkEdit社会交往
            // 
            this.checkEdit社会交往.Location = new System.Drawing.Point(272, 1);
            this.checkEdit社会交往.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit社会交往.Name = "checkEdit社会交往";
            this.checkEdit社会交往.Properties.Caption = "社会交往";
            this.checkEdit社会交往.Size = new System.Drawing.Size(75, 19);
            this.checkEdit社会交往.TabIndex = 4;
            // 
            // checkEdit康复措施其他
            // 
            this.checkEdit康复措施其他.Location = new System.Drawing.Point(353, 1);
            this.checkEdit康复措施其他.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit康复措施其他.Name = "checkEdit康复措施其他";
            this.checkEdit康复措施其他.Properties.Caption = "其他";
            this.checkEdit康复措施其他.Size = new System.Drawing.Size(54, 19);
            this.checkEdit康复措施其他.TabIndex = 5;
            this.checkEdit康复措施其他.CheckedChanged += new System.EventHandler(this.checkEdit康复措施其他_CheckedChanged);
            // 
            // textEdit康复措施其他
            // 
            this.textEdit康复措施其他.Enabled = false;
            this.textEdit康复措施其他.Location = new System.Drawing.Point(413, 1);
            this.textEdit康复措施其他.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.textEdit康复措施其他.Name = "textEdit康复措施其他";
            this.textEdit康复措施其他.Size = new System.Drawing.Size(106, 20);
            this.textEdit康复措施其他.TabIndex = 6;
            // 
            // sbtn添加药物
            // 
            this.sbtn添加药物.Location = new System.Drawing.Point(122, 945);
            this.sbtn添加药物.Name = "sbtn添加药物";
            this.sbtn添加药物.Size = new System.Drawing.Size(159, 22);
            this.sbtn添加药物.StyleController = this.layoutControl1;
            this.sbtn添加药物.TabIndex = 57;
            this.sbtn添加药物.Text = "添加药物";
            this.sbtn添加药物.Click += new System.EventHandler(this.sbtn添加药物_Click);
            // 
            // textEdit转诊科室
            // 
            this.textEdit转诊科室.Enabled = false;
            this.textEdit转诊科室.Location = new System.Drawing.Point(569, 1201);
            this.textEdit转诊科室.Name = "textEdit转诊科室";
            this.textEdit转诊科室.Size = new System.Drawing.Size(161, 20);
            this.textEdit转诊科室.StyleController = this.layoutControl1;
            this.textEdit转诊科室.TabIndex = 55;
            // 
            // textEdit转诊原因
            // 
            this.textEdit转诊原因.Enabled = false;
            this.textEdit转诊原因.Location = new System.Drawing.Point(241, 1201);
            this.textEdit转诊原因.Name = "textEdit转诊原因";
            this.textEdit转诊原因.Size = new System.Drawing.Size(247, 20);
            this.textEdit转诊原因.StyleController = this.layoutControl1;
            this.textEdit转诊原因.TabIndex = 54;
            // 
            // radio是否转诊
            // 
            this.radio是否转诊.Location = new System.Drawing.Point(118, 1201);
            this.radio是否转诊.Name = "radio是否转诊";
            this.radio是否转诊.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否转诊.Size = new System.Drawing.Size(78, 25);
            this.radio是否转诊.StyleController = this.layoutControl1;
            this.radio是否转诊.TabIndex = 53;
            this.radio是否转诊.SelectedIndexChanged += new System.EventHandler(this.radio是否转诊_SelectedIndexChanged);
            // 
            // comboBoxEdit此次随访分类
            // 
            this.comboBoxEdit此次随访分类.Location = new System.Drawing.Point(118, 894);
            this.comboBoxEdit此次随访分类.Name = "comboBoxEdit此次随访分类";
            this.comboBoxEdit此次随访分类.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit此次随访分类.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit此次随访分类.Size = new System.Drawing.Size(150, 20);
            this.comboBoxEdit此次随访分类.StyleController = this.layoutControl1;
            this.comboBoxEdit此次随访分类.TabIndex = 52;
            // 
            // textEdit药物不良反应
            // 
            this.textEdit药物不良反应.Enabled = false;
            this.textEdit药物不良反应.Location = new System.Drawing.Point(200, 844);
            this.textEdit药物不良反应.Name = "textEdit药物不良反应";
            this.textEdit药物不良反应.Size = new System.Drawing.Size(530, 20);
            this.textEdit药物不良反应.StyleController = this.layoutControl1;
            this.textEdit药物不良反应.TabIndex = 50;
            // 
            // radio药物不良反应
            // 
            this.radio药物不良反应.Location = new System.Drawing.Point(118, 844);
            this.radio药物不良反应.Name = "radio药物不良反应";
            this.radio药物不良反应.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio药物不良反应.Size = new System.Drawing.Size(78, 22);
            this.radio药物不良反应.StyleController = this.layoutControl1;
            this.radio药物不良反应.TabIndex = 49;
            this.radio药物不良反应.SelectedIndexChanged += new System.EventHandler(this.radio药物不良反应_SelectedIndexChanged);
            // 
            // radioGroup服药依从性
            // 
            this.radioGroup服药依从性.Location = new System.Drawing.Point(118, 815);
            this.radioGroup服药依从性.Name = "radioGroup服药依从性";
            this.radioGroup服药依从性.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "规律"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "间断"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "不服药")});
            this.radioGroup服药依从性.Size = new System.Drawing.Size(246, 25);
            this.radioGroup服药依从性.StyleController = this.layoutControl1;
            this.radioGroup服药依从性.TabIndex = 48;
            // 
            // textEdit实验室检查
            // 
            this.textEdit实验室检查.Enabled = false;
            this.textEdit实验室检查.Location = new System.Drawing.Point(199, 791);
            this.textEdit实验室检查.Name = "textEdit实验室检查";
            this.textEdit实验室检查.Size = new System.Drawing.Size(531, 20);
            this.textEdit实验室检查.StyleController = this.layoutControl1;
            this.textEdit实验室检查.TabIndex = 47;
            // 
            // radio实验室检查
            // 
            this.radio实验室检查.Location = new System.Drawing.Point(118, 791);
            this.radio实验室检查.Name = "radio实验室检查";
            this.radio实验室检查.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio实验室检查.Size = new System.Drawing.Size(77, 20);
            this.radio实验室检查.StyleController = this.layoutControl1;
            this.radio实验室检查.TabIndex = 46;
            this.radio实验室检查.SelectedIndexChanged += new System.EventHandler(this.radio实验室检查_SelectedIndexChanged);
            // 
            // radioGroup社会人际交往
            // 
            this.radioGroup社会人际交往.Location = new System.Drawing.Point(218, 610);
            this.radioGroup社会人际交往.Name = "radioGroup社会人际交往";
            this.radioGroup社会人际交往.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差")});
            this.radioGroup社会人际交往.Size = new System.Drawing.Size(385, 22);
            this.radioGroup社会人际交往.StyleController = this.layoutControl1;
            this.radioGroup社会人际交往.TabIndex = 31;
            // 
            // radioGroup学习能力
            // 
            this.radioGroup学习能力.Location = new System.Drawing.Point(218, 584);
            this.radioGroup学习能力.Name = "radioGroup学习能力";
            this.radioGroup学习能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差")});
            this.radioGroup学习能力.Size = new System.Drawing.Size(385, 22);
            this.radioGroup学习能力.StyleController = this.layoutControl1;
            this.radioGroup学习能力.TabIndex = 30;
            // 
            // radioGroup生产劳动及工作
            // 
            this.radioGroup生产劳动及工作.Location = new System.Drawing.Point(218, 558);
            this.radioGroup生产劳动及工作.Name = "radioGroup生产劳动及工作";
            this.radioGroup生产劳动及工作.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "此项不适用")});
            this.radioGroup生产劳动及工作.Size = new System.Drawing.Size(512, 22);
            this.radioGroup生产劳动及工作.StyleController = this.layoutControl1;
            this.radioGroup生产劳动及工作.TabIndex = 29;
            // 
            // radioGroup家务劳动
            // 
            this.radioGroup家务劳动.Location = new System.Drawing.Point(218, 532);
            this.radioGroup家务劳动.Name = "radioGroup家务劳动";
            this.radioGroup家务劳动.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差")});
            this.radioGroup家务劳动.Size = new System.Drawing.Size(385, 22);
            this.radioGroup家务劳动.StyleController = this.layoutControl1;
            this.radioGroup家务劳动.TabIndex = 28;
            // 
            // radioGroup个人生活料理
            // 
            this.radioGroup个人生活料理.Location = new System.Drawing.Point(218, 506);
            this.radioGroup个人生活料理.Name = "radioGroup个人生活料理";
            this.radioGroup个人生活料理.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差")});
            this.radioGroup个人生活料理.Size = new System.Drawing.Size(385, 22);
            this.radioGroup个人生活料理.StyleController = this.layoutControl1;
            this.radioGroup个人生活料理.TabIndex = 27;
            // 
            // radioGroup饮食情况
            // 
            this.radioGroup饮食情况.Location = new System.Drawing.Point(118, 477);
            this.radioGroup饮食情况.Name = "radioGroup饮食情况";
            this.radioGroup饮食情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("9", "此项不适用")});
            this.radioGroup饮食情况.Size = new System.Drawing.Size(612, 25);
            this.radioGroup饮食情况.StyleController = this.layoutControl1;
            this.radioGroup饮食情况.TabIndex = 26;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(365, 1257);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(132, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 65;
            // 
            // radioGroup关锁情况
            // 
            this.radioGroup关锁情况.Location = new System.Drawing.Point(118, 732);
            this.radioGroup关锁情况.Name = "radioGroup关锁情况";
            this.radioGroup关锁情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无关锁"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "关锁"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "关锁已解除")});
            this.radioGroup关锁情况.Size = new System.Drawing.Size(245, 25);
            this.radioGroup关锁情况.StyleController = this.layoutControl1;
            this.radioGroup关锁情况.TabIndex = 43;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.ucLblTxtLbl其他危害行为);
            this.panelControl5.Controls.Add(this.checkEdit其他危害行为);
            this.panelControl5.Controls.Add(this.checkEdit无影响);
            this.panelControl5.Controls.Add(this.ucLblTxtLbl自杀未遂);
            this.panelControl5.Controls.Add(this.checkEdit自杀未遂);
            this.panelControl5.Controls.Add(this.ucLblTxtLbl自伤);
            this.panelControl5.Controls.Add(this.checkEdit自伤);
            this.panelControl5.Controls.Add(this.ucLblTxtLbl肇祸);
            this.panelControl5.Controls.Add(this.checkEdit肇祸);
            this.panelControl5.Controls.Add(this.ucLblTxtLbl肇事);
            this.panelControl5.Controls.Add(this.checkEdit肇事);
            this.panelControl5.Controls.Add(this.ucLblTxtLbl轻度滋事);
            this.panelControl5.Controls.Add(this.checkEdit轻度滋事);
            this.panelControl5.Location = new System.Drawing.Point(113, 636);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(617, 92);
            this.panelControl5.TabIndex = 72;
            // 
            // ucLblTxtLbl其他危害行为
            // 
            this.ucLblTxtLbl其他危害行为.Enabled = false;
            this.ucLblTxtLbl其他危害行为.Lbl1Size = new System.Drawing.Size(90, 18);
            this.ucLblTxtLbl其他危害行为.Lbl1Text = "其他危害行为：";
            this.ucLblTxtLbl其他危害行为.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl其他危害行为.Lbl2Text = "次";
            this.ucLblTxtLbl其他危害行为.Location = new System.Drawing.Point(34, 27);
            this.ucLblTxtLbl其他危害行为.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl其他危害行为.Name = "ucLblTxtLbl其他危害行为";
            this.ucLblTxtLbl其他危害行为.Size = new System.Drawing.Size(180, 22);
            this.ucLblTxtLbl其他危害行为.TabIndex = 44;
            this.ucLblTxtLbl其他危害行为.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit其他危害行为
            // 
            this.checkEdit其他危害行为.Location = new System.Drawing.Point(7, 29);
            this.checkEdit其他危害行为.Name = "checkEdit其他危害行为";
            this.checkEdit其他危害行为.Properties.Caption = "";
            this.checkEdit其他危害行为.Size = new System.Drawing.Size(20, 19);
            this.checkEdit其他危害行为.TabIndex = 43;
            this.checkEdit其他危害行为.CheckedChanged += new System.EventHandler(this.checkEdit其他危害行为_CheckedChanged);
            // 
            // checkEdit无影响
            // 
            this.checkEdit无影响.Location = new System.Drawing.Point(9, 54);
            this.checkEdit无影响.Name = "checkEdit无影响";
            this.checkEdit无影响.Properties.Caption = "无";
            this.checkEdit无影响.Size = new System.Drawing.Size(56, 19);
            this.checkEdit无影响.TabIndex = 42;
            // 
            // ucLblTxtLbl自杀未遂
            // 
            this.ucLblTxtLbl自杀未遂.Enabled = false;
            this.ucLblTxtLbl自杀未遂.Lbl1Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl自杀未遂.Lbl1Text = "自杀未遂";
            this.ucLblTxtLbl自杀未遂.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl自杀未遂.Lbl2Text = "次";
            this.ucLblTxtLbl自杀未遂.Location = new System.Drawing.Point(386, 30);
            this.ucLblTxtLbl自杀未遂.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl自杀未遂.Name = "ucLblTxtLbl自杀未遂";
            this.ucLblTxtLbl自杀未遂.Size = new System.Drawing.Size(130, 22);
            this.ucLblTxtLbl自杀未遂.TabIndex = 41;
            this.ucLblTxtLbl自杀未遂.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit自杀未遂
            // 
            this.checkEdit自杀未遂.Location = new System.Drawing.Point(363, 31);
            this.checkEdit自杀未遂.Name = "checkEdit自杀未遂";
            this.checkEdit自杀未遂.Properties.Caption = "";
            this.checkEdit自杀未遂.Size = new System.Drawing.Size(19, 19);
            this.checkEdit自杀未遂.TabIndex = 40;
            this.checkEdit自杀未遂.CheckedChanged += new System.EventHandler(this.checkEdit自杀未遂_CheckedChanged);
            // 
            // ucLblTxtLbl自伤
            // 
            this.ucLblTxtLbl自伤.Enabled = false;
            this.ucLblTxtLbl自伤.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl自伤.Lbl1Text = "自伤";
            this.ucLblTxtLbl自伤.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl自伤.Lbl2Text = "次";
            this.ucLblTxtLbl自伤.Location = new System.Drawing.Point(242, 30);
            this.ucLblTxtLbl自伤.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl自伤.Name = "ucLblTxtLbl自伤";
            this.ucLblTxtLbl自伤.Size = new System.Drawing.Size(113, 22);
            this.ucLblTxtLbl自伤.TabIndex = 39;
            this.ucLblTxtLbl自伤.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit自伤
            // 
            this.checkEdit自伤.Location = new System.Drawing.Point(217, 31);
            this.checkEdit自伤.Name = "checkEdit自伤";
            this.checkEdit自伤.Properties.Caption = "";
            this.checkEdit自伤.Size = new System.Drawing.Size(19, 19);
            this.checkEdit自伤.TabIndex = 38;
            this.checkEdit自伤.CheckedChanged += new System.EventHandler(this.checkEdit自伤_CheckedChanged);
            // 
            // ucLblTxtLbl肇祸
            // 
            this.ucLblTxtLbl肇祸.Enabled = false;
            this.ucLblTxtLbl肇祸.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl肇祸.Lbl1Text = "肇祸";
            this.ucLblTxtLbl肇祸.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl肇祸.Lbl2Text = "次";
            this.ucLblTxtLbl肇祸.Location = new System.Drawing.Point(385, 2);
            this.ucLblTxtLbl肇祸.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl肇祸.Name = "ucLblTxtLbl肇祸";
            this.ucLblTxtLbl肇祸.Size = new System.Drawing.Size(137, 22);
            this.ucLblTxtLbl肇祸.TabIndex = 37;
            this.ucLblTxtLbl肇祸.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit肇祸
            // 
            this.checkEdit肇祸.Location = new System.Drawing.Point(363, 3);
            this.checkEdit肇祸.Name = "checkEdit肇祸";
            this.checkEdit肇祸.Properties.Caption = "";
            this.checkEdit肇祸.Size = new System.Drawing.Size(19, 19);
            this.checkEdit肇祸.TabIndex = 36;
            this.checkEdit肇祸.CheckedChanged += new System.EventHandler(this.checkEdit肇祸_CheckedChanged);
            // 
            // ucLblTxtLbl肇事
            // 
            this.ucLblTxtLbl肇事.Enabled = false;
            this.ucLblTxtLbl肇事.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucLblTxtLbl肇事.Lbl1Text = "肇事";
            this.ucLblTxtLbl肇事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl肇事.Lbl2Text = "次";
            this.ucLblTxtLbl肇事.Location = new System.Drawing.Point(242, 3);
            this.ucLblTxtLbl肇事.Margin = new System.Windows.Forms.Padding(1);
            this.ucLblTxtLbl肇事.Name = "ucLblTxtLbl肇事";
            this.ucLblTxtLbl肇事.Size = new System.Drawing.Size(114, 22);
            this.ucLblTxtLbl肇事.TabIndex = 35;
            this.ucLblTxtLbl肇事.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit肇事
            // 
            this.checkEdit肇事.Location = new System.Drawing.Point(217, 4);
            this.checkEdit肇事.Name = "checkEdit肇事";
            this.checkEdit肇事.Properties.Caption = "";
            this.checkEdit肇事.Size = new System.Drawing.Size(19, 19);
            this.checkEdit肇事.TabIndex = 34;
            this.checkEdit肇事.CheckedChanged += new System.EventHandler(this.checkEdit肇事_CheckedChanged);
            // 
            // ucLblTxtLbl轻度滋事
            // 
            this.ucLblTxtLbl轻度滋事.Enabled = false;
            this.ucLblTxtLbl轻度滋事.Lbl1Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl轻度滋事.Lbl1Text = "轻度滋事";
            this.ucLblTxtLbl轻度滋事.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl轻度滋事.Lbl2Text = "次";
            this.ucLblTxtLbl轻度滋事.Location = new System.Drawing.Point(32, 3);
            this.ucLblTxtLbl轻度滋事.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl轻度滋事.Name = "ucLblTxtLbl轻度滋事";
            this.ucLblTxtLbl轻度滋事.Size = new System.Drawing.Size(136, 22);
            this.ucLblTxtLbl轻度滋事.TabIndex = 33;
            this.ucLblTxtLbl轻度滋事.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // checkEdit轻度滋事
            // 
            this.checkEdit轻度滋事.Location = new System.Drawing.Point(7, 4);
            this.checkEdit轻度滋事.Name = "checkEdit轻度滋事";
            this.checkEdit轻度滋事.Properties.Caption = "";
            this.checkEdit轻度滋事.Size = new System.Drawing.Size(19, 19);
            this.checkEdit轻度滋事.TabIndex = 32;
            this.checkEdit轻度滋事.CheckedChanged += new System.EventHandler(this.checkEdit轻度滋事_CheckedChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.ucLblDtp末次出院情况);
            this.panelControl2.Controls.Add(this.radioGroup住院情况);
            this.panelControl2.Location = new System.Drawing.Point(118, 761);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(612, 26);
            this.panelControl2.TabIndex = 45;
            // 
            // ucLblDtp末次出院情况
            // 
            this.ucLblDtp末次出院情况.AutoSize = true;
            this.ucLblDtp末次出院情况.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ucLblDtp末次出院情况.Dtp1Size = new System.Drawing.Size(100, 20);
            this.ucLblDtp末次出院情况.Enabled = false;
            this.ucLblDtp末次出院情况.Lbl1Size = new System.Drawing.Size(90, 16);
            this.ucLblDtp末次出院情况.Lbl1Text = "末次出院时间：";
            this.ucLblDtp末次出院情况.Location = new System.Drawing.Point(397, 2);
            this.ucLblDtp末次出院情况.Name = "ucLblDtp末次出院情况";
            this.ucLblDtp末次出院情况.Size = new System.Drawing.Size(196, 22);
            this.ucLblDtp末次出院情况.TabIndex = 2;
            // 
            // radioGroup住院情况
            // 
            this.radioGroup住院情况.Location = new System.Drawing.Point(2, 2);
            this.radioGroup住院情况.Name = "radioGroup住院情况";
            this.radioGroup住院情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "从未住院"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "目前正在住院"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "既往住院，现未住院")});
            this.radioGroup住院情况.Size = new System.Drawing.Size(389, 23);
            this.radioGroup住院情况.TabIndex = 1;
            this.radioGroup住院情况.SelectedIndexChanged += new System.EventHandler(this.radioGroup住院情况_SelectedIndexChanged);
            // 
            // labelControl当前所属机构
            // 
            this.labelControl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl当前所属机构.Location = new System.Drawing.Point(118, 1305);
            this.labelControl当前所属机构.Name = "labelControl当前所属机构";
            this.labelControl当前所属机构.Size = new System.Drawing.Size(236, 20);
            this.labelControl当前所属机构.StyleController = this.layoutControl1;
            this.labelControl当前所属机构.TabIndex = 68;
            this.labelControl当前所属机构.Text = "labelControl16";
            // 
            // labelControl创建机构
            // 
            this.labelControl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl创建机构.Location = new System.Drawing.Point(473, 1305);
            this.labelControl创建机构.Name = "labelControl创建机构";
            this.labelControl创建机构.Size = new System.Drawing.Size(257, 20);
            this.labelControl创建机构.StyleController = this.layoutControl1;
            this.labelControl创建机构.TabIndex = 69;
            this.labelControl创建机构.Text = "labelControl15";
            // 
            // labelControl最近修改人
            // 
            this.labelControl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl最近修改人.Location = new System.Drawing.Point(473, 1329);
            this.labelControl最近修改人.Name = "labelControl最近修改人";
            this.labelControl最近修改人.Size = new System.Drawing.Size(257, 20);
            this.labelControl最近修改人.StyleController = this.layoutControl1;
            this.labelControl最近修改人.TabIndex = 71;
            this.labelControl最近修改人.Text = "labelControl14";
            // 
            // labelControl创建人
            // 
            this.labelControl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl创建人.Location = new System.Drawing.Point(118, 1329);
            this.labelControl创建人.Name = "labelControl创建人";
            this.labelControl创建人.Size = new System.Drawing.Size(236, 20);
            this.labelControl创建人.StyleController = this.layoutControl1;
            this.labelControl创建人.TabIndex = 70;
            this.labelControl创建人.Text = "labelControl13";
            // 
            // labelControl最近更新时间
            // 
            this.labelControl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl最近更新时间.Location = new System.Drawing.Point(473, 1281);
            this.labelControl最近更新时间.Name = "labelControl最近更新时间";
            this.labelControl最近更新时间.Size = new System.Drawing.Size(257, 20);
            this.labelControl最近更新时间.StyleController = this.layoutControl1;
            this.labelControl最近更新时间.TabIndex = 67;
            this.labelControl最近更新时间.Text = "labelControl12";
            // 
            // labelControl1创建时间
            // 
            this.labelControl1创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1创建时间.Location = new System.Drawing.Point(118, 1281);
            this.labelControl1创建时间.Name = "labelControl1创建时间";
            this.labelControl1创建时间.Size = new System.Drawing.Size(236, 20);
            this.labelControl1创建时间.StyleController = this.layoutControl1;
            this.labelControl1创建时间.TabIndex = 66;
            this.labelControl1创建时间.Text = "labelControl11";
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(118, 1257);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(128, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 64;
            // 
            // radioGroup治疗效果
            // 
            this.radioGroup治疗效果.Location = new System.Drawing.Point(118, 870);
            this.radioGroup治疗效果.Name = "radioGroup治疗效果";
            this.radioGroup治疗效果.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "痊愈"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "好转"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "无变化"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "加重")});
            this.radioGroup治疗效果.Size = new System.Drawing.Size(321, 20);
            this.radioGroup治疗效果.StyleController = this.layoutControl1;
            this.radioGroup治疗效果.TabIndex = 51;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit幻觉);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit交流困难);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit猜疑);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit喜怒无常);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit行为怪异);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit兴奋话多);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit伤人毁物);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit悲观厌世);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit无故外走);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit自语自笑);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit孤僻懒散);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit症状其他);
            this.flowLayoutPanel6.Controls.Add(this.textEdit症状其他);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(118, 377);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(612, 46);
            this.flowLayoutPanel6.TabIndex = 10;
            // 
            // checkEdit幻觉
            // 
            this.checkEdit幻觉.Location = new System.Drawing.Point(1, 1);
            this.checkEdit幻觉.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit幻觉.Name = "checkEdit幻觉";
            this.checkEdit幻觉.Properties.Caption = "幻觉";
            this.checkEdit幻觉.Size = new System.Drawing.Size(51, 19);
            this.checkEdit幻觉.TabIndex = 11;
            // 
            // checkEdit交流困难
            // 
            this.checkEdit交流困难.Location = new System.Drawing.Point(54, 1);
            this.checkEdit交流困难.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit交流困难.Name = "checkEdit交流困难";
            this.checkEdit交流困难.Properties.Caption = "交流困难";
            this.checkEdit交流困难.Size = new System.Drawing.Size(80, 19);
            this.checkEdit交流困难.TabIndex = 12;
            // 
            // checkEdit猜疑
            // 
            this.checkEdit猜疑.Location = new System.Drawing.Point(136, 1);
            this.checkEdit猜疑.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit猜疑.Name = "checkEdit猜疑";
            this.checkEdit猜疑.Properties.Caption = "猜疑";
            this.checkEdit猜疑.Size = new System.Drawing.Size(54, 19);
            this.checkEdit猜疑.TabIndex = 13;
            // 
            // checkEdit喜怒无常
            // 
            this.checkEdit喜怒无常.Location = new System.Drawing.Point(192, 1);
            this.checkEdit喜怒无常.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit喜怒无常.Name = "checkEdit喜怒无常";
            this.checkEdit喜怒无常.Properties.Caption = "喜怒无常";
            this.checkEdit喜怒无常.Size = new System.Drawing.Size(81, 19);
            this.checkEdit喜怒无常.TabIndex = 14;
            // 
            // checkEdit行为怪异
            // 
            this.checkEdit行为怪异.Location = new System.Drawing.Point(275, 1);
            this.checkEdit行为怪异.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit行为怪异.Name = "checkEdit行为怪异";
            this.checkEdit行为怪异.Properties.Caption = "行为怪异";
            this.checkEdit行为怪异.Size = new System.Drawing.Size(72, 19);
            this.checkEdit行为怪异.TabIndex = 15;
            // 
            // checkEdit兴奋话多
            // 
            this.checkEdit兴奋话多.Location = new System.Drawing.Point(349, 1);
            this.checkEdit兴奋话多.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit兴奋话多.Name = "checkEdit兴奋话多";
            this.checkEdit兴奋话多.Properties.Caption = "兴奋话多";
            this.checkEdit兴奋话多.Size = new System.Drawing.Size(82, 19);
            this.checkEdit兴奋话多.TabIndex = 16;
            // 
            // checkEdit伤人毁物
            // 
            this.checkEdit伤人毁物.Location = new System.Drawing.Point(433, 1);
            this.checkEdit伤人毁物.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit伤人毁物.Name = "checkEdit伤人毁物";
            this.checkEdit伤人毁物.Properties.Caption = "伤人毁物";
            this.checkEdit伤人毁物.Size = new System.Drawing.Size(79, 19);
            this.checkEdit伤人毁物.TabIndex = 17;
            // 
            // checkEdit悲观厌世
            // 
            this.checkEdit悲观厌世.Location = new System.Drawing.Point(514, 1);
            this.checkEdit悲观厌世.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit悲观厌世.Name = "checkEdit悲观厌世";
            this.checkEdit悲观厌世.Properties.Caption = "悲观厌世";
            this.checkEdit悲观厌世.Size = new System.Drawing.Size(75, 19);
            this.checkEdit悲观厌世.TabIndex = 18;
            // 
            // checkEdit无故外走
            // 
            this.checkEdit无故外走.Location = new System.Drawing.Point(1, 22);
            this.checkEdit无故外走.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit无故外走.Name = "checkEdit无故外走";
            this.checkEdit无故外走.Properties.Caption = "无故外走";
            this.checkEdit无故外走.Size = new System.Drawing.Size(84, 19);
            this.checkEdit无故外走.TabIndex = 19;
            // 
            // checkEdit自语自笑
            // 
            this.checkEdit自语自笑.Location = new System.Drawing.Point(87, 22);
            this.checkEdit自语自笑.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit自语自笑.Name = "checkEdit自语自笑";
            this.checkEdit自语自笑.Properties.Caption = "自语自笑";
            this.checkEdit自语自笑.Size = new System.Drawing.Size(84, 19);
            this.checkEdit自语自笑.TabIndex = 20;
            // 
            // checkEdit孤僻懒散
            // 
            this.checkEdit孤僻懒散.Location = new System.Drawing.Point(173, 22);
            this.checkEdit孤僻懒散.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit孤僻懒散.Name = "checkEdit孤僻懒散";
            this.checkEdit孤僻懒散.Properties.Caption = "孤僻懒散";
            this.checkEdit孤僻懒散.Size = new System.Drawing.Size(84, 19);
            this.checkEdit孤僻懒散.TabIndex = 21;
            // 
            // checkEdit症状其他
            // 
            this.checkEdit症状其他.Location = new System.Drawing.Point(259, 22);
            this.checkEdit症状其他.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit症状其他.Name = "checkEdit症状其他";
            this.checkEdit症状其他.Properties.Caption = "其他";
            this.checkEdit症状其他.Size = new System.Drawing.Size(51, 19);
            this.checkEdit症状其他.TabIndex = 22;
            this.checkEdit症状其他.CheckedChanged += new System.EventHandler(this.checkEdit症状其他_CheckedChanged);
            // 
            // textEdit症状其他
            // 
            this.textEdit症状其他.Enabled = false;
            this.textEdit症状其他.Location = new System.Drawing.Point(312, 22);
            this.textEdit症状其他.Margin = new System.Windows.Forms.Padding(1);
            this.textEdit症状其他.Name = "textEdit症状其他";
            this.textEdit症状其他.Size = new System.Drawing.Size(125, 20);
            this.textEdit症状其他.TabIndex = 23;
            // 
            // labelControl居住地址
            // 
            this.labelControl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl居住地址.Location = new System.Drawing.Point(118, 108);
            this.labelControl居住地址.Name = "labelControl居住地址";
            this.labelControl居住地址.Size = new System.Drawing.Size(248, 36);
            this.labelControl居住地址.StyleController = this.layoutControl1;
            this.labelControl居住地址.TabIndex = 6;
            // 
            // labelControl联系电话
            // 
            this.labelControl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl联系电话.Location = new System.Drawing.Point(465, 82);
            this.labelControl联系电话.Name = "labelControl联系电话";
            this.labelControl联系电话.Size = new System.Drawing.Size(265, 22);
            this.labelControl联系电话.StyleController = this.layoutControl1;
            this.labelControl联系电话.TabIndex = 5;
            // 
            // labelControl婚姻状况
            // 
            this.labelControl婚姻状况.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl婚姻状况.Location = new System.Drawing.Point(465, 108);
            this.labelControl婚姻状况.Name = "labelControl婚姻状况";
            this.labelControl婚姻状况.Size = new System.Drawing.Size(265, 36);
            this.labelControl婚姻状况.StyleController = this.layoutControl1;
            this.labelControl婚姻状况.TabIndex = 7;
            // 
            // labelControl身份证号
            // 
            this.labelControl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl身份证号.Location = new System.Drawing.Point(465, 56);
            this.labelControl身份证号.Name = "labelControl身份证号";
            this.labelControl身份证号.Size = new System.Drawing.Size(265, 22);
            this.labelControl身份证号.StyleController = this.layoutControl1;
            this.labelControl身份证号.TabIndex = 3;
            // 
            // labelControl出生日期
            // 
            this.labelControl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl出生日期.Location = new System.Drawing.Point(118, 82);
            this.labelControl出生日期.Name = "labelControl出生日期";
            this.labelControl出生日期.Size = new System.Drawing.Size(248, 22);
            this.labelControl出生日期.StyleController = this.layoutControl1;
            this.labelControl出生日期.TabIndex = 4;
            // 
            // labelControl性别
            // 
            this.labelControl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl性别.Location = new System.Drawing.Point(118, 56);
            this.labelControl性别.Name = "labelControl性别";
            this.labelControl性别.Size = new System.Drawing.Size(248, 22);
            this.labelControl性别.StyleController = this.layoutControl1;
            this.labelControl性别.TabIndex = 2;
            // 
            // labelControl姓名
            // 
            this.labelControl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl姓名.Location = new System.Drawing.Point(465, 30);
            this.labelControl姓名.Name = "labelControl姓名";
            this.labelControl姓名.Size = new System.Drawing.Size(265, 22);
            this.labelControl姓名.StyleController = this.layoutControl1;
            this.labelControl姓名.TabIndex = 1;
            // 
            // labelControl个人档案号
            // 
            this.labelControl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl个人档案号.Location = new System.Drawing.Point(118, 30);
            this.labelControl个人档案号.Name = "labelControl个人档案号";
            this.labelControl个人档案号.Size = new System.Drawing.Size(248, 22);
            this.labelControl个人档案号.StyleController = this.layoutControl1;
            this.labelControl个人档案号.TabIndex = 0;
            // 
            // comboBoxEdit危险性
            // 
            this.comboBoxEdit危险性.Location = new System.Drawing.Point(465, 148);
            this.comboBoxEdit危险性.Name = "comboBoxEdit危险性";
            this.comboBoxEdit危险性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit危险性.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit危险性.Size = new System.Drawing.Size(265, 20);
            this.comboBoxEdit危险性.StyleController = this.layoutControl1;
            this.comboBoxEdit危险性.TabIndex = 9;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(118, 148);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.AutoHeight = false;
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit随访日期.Size = new System.Drawing.Size(248, 21);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 8;
            // 
            // radioGroup自知力
            // 
            this.radioGroup自知力.Location = new System.Drawing.Point(118, 427);
            this.radioGroup自知力.Name = "radioGroup自知力";
            this.radioGroup自知力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "自知力完全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "自知力不全"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "自知力缺失")});
            this.radioGroup自知力.Size = new System.Drawing.Size(462, 21);
            this.radioGroup自知力.StyleController = this.layoutControl1;
            this.radioGroup自知力.TabIndex = 24;
            // 
            // radioGroup睡眠情况
            // 
            this.radioGroup睡眠情况.Location = new System.Drawing.Point(118, 452);
            this.radioGroup睡眠情况.Name = "radioGroup睡眠情况";
            this.radioGroup睡眠情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "良好"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "一般"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "较差"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("9", "此项不适用")});
            this.radioGroup睡眠情况.Size = new System.Drawing.Size(612, 21);
            this.radioGroup睡眠情况.StyleController = this.layoutControl1;
            this.radioGroup睡眠情况.TabIndex = 25;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "冠心病患者管理卡";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 1352);
            this.layoutControlGroup1.Text = "重性精神疾病患者随访服务记录表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem随访日期,
            this.layoutControlGroup5,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem危险性,
            this.layoutControlItem自知力,
            this.layoutControlItem目前症状,
            this.layoutControlItem睡眠情况,
            this.layoutControlItem饮食情况,
            this.emptySpaceItem10,
            this.layoutControlItem1,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem15,
            this.emptySpaceItem9,
            this.emptySpaceItem14,
            this.layoutControlItem18});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(731, 1323);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.labelControl居住地址;
            this.layoutControlItem9.CustomizationFormText = "居住地址";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(111, 40);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(367, 40);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "居住地址：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.labelControl姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(367, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.labelControl个人档案号;
            this.layoutControlItem2.CustomizationFormText = "个人档案号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "个人档案号：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.labelControl性别;
            this.layoutControlItem4.CustomizationFormText = "些别";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "性别：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem随访日期
            // 
            this.layoutControlItem随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem随访日期.Control = this.dateEdit随访日期;
            this.layoutControlItem随访日期.CustomizationFormText = "随访日期*";
            this.layoutControlItem随访日期.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem随访日期.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem随访日期.Name = "layoutControlItem随访日期";
            this.layoutControlItem随访日期.Size = new System.Drawing.Size(367, 25);
            this.layoutControlItem随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem随访日期.Text = "随访日期*";
            this.layoutControlItem随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem随访日期.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem随访日期.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem随访医生,
            this.layoutControlItem48,
            this.layoutControlItem47,
            this.layoutControlItem46,
            this.layoutControlItem45,
            this.layoutControlItem个人生活料理,
            this.layoutControlItem家务劳动,
            this.layoutControlItem生产劳动及工作,
            this.layoutControlItem学习能力,
            this.layoutControlItem社会人际交往,
            this.layoutControlItem22,
            this.layoutControlItem关锁情况,
            this.emptySpaceItem1,
            this.layoutControlItem住院情况,
            this.layoutControlItem实验室检查,
            this.layoutControlItem16,
            this.layoutControlItem服药依从性,
            this.layoutControlItem药物不良反应,
            this.layoutControlItem29,
            this.layoutControlItem治疗效果,
            this.layoutControlItem此次随访分类,
            this.emptySpaceItem3,
            this.layoutControlItem是否转诊,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem康复措施,
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem7,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.emptySpaceItem13,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem下次随访日期,
            this.layoutControlItem用药情况,
            this.layoutControlItem34,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 476);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(731, 847);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.labelControl1创建时间;
            this.layoutControlItem43.CustomizationFormText = "录入时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 775);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "创建时间：";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.labelControl最近更新时间;
            this.layoutControlItem44.CustomizationFormText = "最近更新时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(355, 775);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "最近更新时间：";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem随访医生
            // 
            this.layoutControlItem随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem随访医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem随访医生.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem随访医生.Control = this.textEdit随访医生;
            this.layoutControlItem随访医生.CustomizationFormText = "随访医生签名";
            this.layoutControlItem随访医生.Location = new System.Drawing.Point(247, 751);
            this.layoutControlItem随访医生.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem随访医生.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem随访医生.Name = "layoutControlItem随访医生";
            this.layoutControlItem随访医生.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem随访医生.Text = "随访医生签名";
            this.layoutControlItem随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem随访医生.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem随访医生.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.labelControl当前所属机构;
            this.layoutControlItem48.CustomizationFormText = "当前所属机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 799);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "当前所属机构：";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.labelControl创建机构;
            this.layoutControlItem47.CustomizationFormText = "创建机构";
            this.layoutControlItem47.Location = new System.Drawing.Point(355, 799);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "创建机构：";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.labelControl最近修改人;
            this.layoutControlItem46.CustomizationFormText = "最近更新人";
            this.layoutControlItem46.Location = new System.Drawing.Point(355, 823);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(376, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "最近修改人：";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.labelControl创建人;
            this.layoutControlItem45.CustomizationFormText = "录入人";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 823);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(92, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "创建人：";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem个人生活料理
            // 
            this.layoutControlItem个人生活料理.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem个人生活料理.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem个人生活料理.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem个人生活料理.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem个人生活料理.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem个人生活料理.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem个人生活料理.Control = this.radioGroup个人生活料理;
            this.layoutControlItem个人生活料理.CustomizationFormText = "个人生活料理";
            this.layoutControlItem个人生活料理.Location = new System.Drawing.Point(110, 0);
            this.layoutControlItem个人生活料理.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem个人生活料理.Name = "layoutControlItem个人生活料理";
            this.layoutControlItem个人生活料理.Size = new System.Drawing.Size(494, 26);
            this.layoutControlItem个人生活料理.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem个人生活料理.Text = "个人生活料理";
            this.layoutControlItem个人生活料理.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem个人生活料理.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem个人生活料理.TextToControlDistance = 5;
            // 
            // layoutControlItem家务劳动
            // 
            this.layoutControlItem家务劳动.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem家务劳动.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem家务劳动.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem家务劳动.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem家务劳动.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem家务劳动.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem家务劳动.Control = this.radioGroup家务劳动;
            this.layoutControlItem家务劳动.CustomizationFormText = "家务劳动";
            this.layoutControlItem家务劳动.Location = new System.Drawing.Point(110, 26);
            this.layoutControlItem家务劳动.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem家务劳动.Name = "layoutControlItem家务劳动";
            this.layoutControlItem家务劳动.Size = new System.Drawing.Size(494, 26);
            this.layoutControlItem家务劳动.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem家务劳动.Text = "家务劳动";
            this.layoutControlItem家务劳动.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem家务劳动.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem家务劳动.TextToControlDistance = 5;
            // 
            // layoutControlItem生产劳动及工作
            // 
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem生产劳动及工作.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem生产劳动及工作.Control = this.radioGroup生产劳动及工作;
            this.layoutControlItem生产劳动及工作.CustomizationFormText = "生产劳动及工作";
            this.layoutControlItem生产劳动及工作.Location = new System.Drawing.Point(110, 52);
            this.layoutControlItem生产劳动及工作.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem生产劳动及工作.Name = "layoutControlItem生产劳动及工作";
            this.layoutControlItem生产劳动及工作.Size = new System.Drawing.Size(621, 26);
            this.layoutControlItem生产劳动及工作.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem生产劳动及工作.Text = "生产劳动及工作";
            this.layoutControlItem生产劳动及工作.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem生产劳动及工作.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem生产劳动及工作.TextToControlDistance = 5;
            // 
            // layoutControlItem学习能力
            // 
            this.layoutControlItem学习能力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem学习能力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem学习能力.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem学习能力.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem学习能力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem学习能力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem学习能力.Control = this.radioGroup学习能力;
            this.layoutControlItem学习能力.CustomizationFormText = "学习能力";
            this.layoutControlItem学习能力.Location = new System.Drawing.Point(110, 78);
            this.layoutControlItem学习能力.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem学习能力.Name = "layoutControlItem学习能力";
            this.layoutControlItem学习能力.Size = new System.Drawing.Size(494, 26);
            this.layoutControlItem学习能力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem学习能力.Text = "学习能力";
            this.layoutControlItem学习能力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem学习能力.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem学习能力.TextToControlDistance = 5;
            // 
            // layoutControlItem社会人际交往
            // 
            this.layoutControlItem社会人际交往.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem社会人际交往.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem社会人际交往.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem社会人际交往.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem社会人际交往.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem社会人际交往.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem社会人际交往.Control = this.radioGroup社会人际交往;
            this.layoutControlItem社会人际交往.CustomizationFormText = "社会人际交往";
            this.layoutControlItem社会人际交往.Location = new System.Drawing.Point(110, 104);
            this.layoutControlItem社会人际交往.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem社会人际交往.Name = "layoutControlItem社会人际交往";
            this.layoutControlItem社会人际交往.Size = new System.Drawing.Size(494, 26);
            this.layoutControlItem社会人际交往.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem社会人际交往.Text = "社会人际交往";
            this.layoutControlItem社会人际交往.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem社会人际交往.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem社会人际交往.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.panelControl5;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(110, 130);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(621, 96);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem关锁情况
            // 
            this.layoutControlItem关锁情况.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem关锁情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem关锁情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem关锁情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem关锁情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem关锁情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem关锁情况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem关锁情况.Control = this.radioGroup关锁情况;
            this.layoutControlItem关锁情况.CustomizationFormText = "关锁情况";
            this.layoutControlItem关锁情况.Location = new System.Drawing.Point(0, 226);
            this.layoutControlItem关锁情况.MinSize = new System.Drawing.Size(117, 29);
            this.layoutControlItem关锁情况.Name = "layoutControlItem关锁情况";
            this.layoutControlItem关锁情况.Size = new System.Drawing.Size(364, 29);
            this.layoutControlItem关锁情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem关锁情况.Text = "关锁情况";
            this.layoutControlItem关锁情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem关锁情况.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem关锁情况.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(364, 226);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(367, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem住院情况
            // 
            this.layoutControlItem住院情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem住院情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem住院情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem住院情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem住院情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem住院情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem住院情况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem住院情况.Control = this.panelControl2;
            this.layoutControlItem住院情况.CustomizationFormText = "门诊";
            this.layoutControlItem住院情况.Location = new System.Drawing.Point(0, 255);
            this.layoutControlItem住院情况.MinSize = new System.Drawing.Size(275, 30);
            this.layoutControlItem住院情况.Name = "layoutControlItem住院情况";
            this.layoutControlItem住院情况.Size = new System.Drawing.Size(731, 30);
            this.layoutControlItem住院情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem住院情况.Text = "住院情况";
            this.layoutControlItem住院情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem住院情况.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem住院情况.TextToControlDistance = 5;
            // 
            // layoutControlItem实验室检查
            // 
            this.layoutControlItem实验室检查.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem实验室检查.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem实验室检查.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem实验室检查.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem实验室检查.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem实验室检查.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem实验室检查.Control = this.radio实验室检查;
            this.layoutControlItem实验室检查.CustomizationFormText = "实验室检查";
            this.layoutControlItem实验室检查.Location = new System.Drawing.Point(0, 285);
            this.layoutControlItem实验室检查.MaxSize = new System.Drawing.Size(196, 0);
            this.layoutControlItem实验室检查.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem实验室检查.Name = "layoutControlItem实验室检查";
            this.layoutControlItem实验室检查.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem实验室检查.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem实验室检查.Text = "实验室检查";
            this.layoutControlItem实验室检查.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem实验室检查.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem实验室检查.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.textEdit实验室检查;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(196, 285);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(535, 24);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem服药依从性
            // 
            this.layoutControlItem服药依从性.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem服药依从性.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem服药依从性.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem服药依从性.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem服药依从性.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem服药依从性.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem服药依从性.Control = this.radioGroup服药依从性;
            this.layoutControlItem服药依从性.CustomizationFormText = "服药依从性";
            this.layoutControlItem服药依从性.Location = new System.Drawing.Point(0, 309);
            this.layoutControlItem服药依从性.MinSize = new System.Drawing.Size(169, 29);
            this.layoutControlItem服药依从性.Name = "layoutControlItem服药依从性";
            this.layoutControlItem服药依从性.Size = new System.Drawing.Size(365, 29);
            this.layoutControlItem服药依从性.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem服药依从性.Text = "用药依从性";
            this.layoutControlItem服药依从性.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem服药依从性.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem服药依从性.TextToControlDistance = 5;
            // 
            // layoutControlItem药物不良反应
            // 
            this.layoutControlItem药物不良反应.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem药物不良反应.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem药物不良反应.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem药物不良反应.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem药物不良反应.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem药物不良反应.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem药物不良反应.Control = this.radio药物不良反应;
            this.layoutControlItem药物不良反应.CustomizationFormText = "药物不良反应";
            this.layoutControlItem药物不良反应.Location = new System.Drawing.Point(0, 338);
            this.layoutControlItem药物不良反应.MaxSize = new System.Drawing.Size(197, 0);
            this.layoutControlItem药物不良反应.MinSize = new System.Drawing.Size(197, 26);
            this.layoutControlItem药物不良反应.Name = "layoutControlItem药物不良反应";
            this.layoutControlItem药物不良反应.Size = new System.Drawing.Size(197, 26);
            this.layoutControlItem药物不良反应.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem药物不良反应.Text = "药物不良反应";
            this.layoutControlItem药物不良反应.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem药物不良反应.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem药物不良反应.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit药物不良反应;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(197, 338);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(534, 26);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem治疗效果
            // 
            this.layoutControlItem治疗效果.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem治疗效果.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem治疗效果.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem治疗效果.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem治疗效果.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem治疗效果.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem治疗效果.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem治疗效果.Control = this.radioGroup治疗效果;
            this.layoutControlItem治疗效果.CustomizationFormText = "是否终止管理";
            this.layoutControlItem治疗效果.Location = new System.Drawing.Point(0, 364);
            this.layoutControlItem治疗效果.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem治疗效果.Name = "layoutControlItem治疗效果";
            this.layoutControlItem治疗效果.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem治疗效果.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem治疗效果.Text = "治疗效果";
            this.layoutControlItem治疗效果.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem治疗效果.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem治疗效果.TextToControlDistance = 5;
            // 
            // layoutControlItem此次随访分类
            // 
            this.layoutControlItem此次随访分类.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem此次随访分类.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem此次随访分类.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem此次随访分类.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem此次随访分类.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem此次随访分类.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem此次随访分类.Control = this.comboBoxEdit此次随访分类;
            this.layoutControlItem此次随访分类.CustomizationFormText = "此次随访分类";
            this.layoutControlItem此次随访分类.Location = new System.Drawing.Point(0, 388);
            this.layoutControlItem此次随访分类.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem此次随访分类.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem此次随访分类.Name = "layoutControlItem此次随访分类";
            this.layoutControlItem此次随访分类.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem此次随访分类.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem此次随访分类.Text = "此次随访分类";
            this.layoutControlItem此次随访分类.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem此次随访分类.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem此次随访分类.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(269, 388);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(462, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem是否转诊
            // 
            this.layoutControlItem是否转诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem是否转诊.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem是否转诊.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem是否转诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem是否转诊.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem是否转诊.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem是否转诊.Control = this.radio是否转诊;
            this.layoutControlItem是否转诊.CustomizationFormText = "是否转诊";
            this.layoutControlItem是否转诊.Location = new System.Drawing.Point(0, 695);
            this.layoutControlItem是否转诊.MaxSize = new System.Drawing.Size(197, 0);
            this.layoutControlItem是否转诊.MinSize = new System.Drawing.Size(197, 29);
            this.layoutControlItem是否转诊.Name = "layoutControlItem是否转诊";
            this.layoutControlItem是否转诊.Size = new System.Drawing.Size(197, 29);
            this.layoutControlItem是否转诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem是否转诊.Text = "是否转诊";
            this.layoutControlItem是否转诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem是否转诊.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem是否转诊.TextToControlDistance = 5;
            this.layoutControlItem是否转诊.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.textEdit转诊原因;
            this.layoutControlItem32.CustomizationFormText = "原因：";
            this.layoutControlItem32.Location = new System.Drawing.Point(197, 695);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(292, 29);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "原因：";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            this.layoutControlItem32.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.textEdit转诊科室;
            this.layoutControlItem33.CustomizationFormText = "机构及科室：";
            this.layoutControlItem33.Location = new System.Drawing.Point(489, 695);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(242, 29);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "机构及科室：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem33.TextToControlDistance = 5;
            this.layoutControlItem33.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem康复措施
            // 
            this.layoutControlItem康复措施.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem康复措施.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem康复措施.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem康复措施.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem康复措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem康复措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem康复措施.Control = this.flowLayoutPanel2;
            this.layoutControlItem康复措施.CustomizationFormText = "康复措施";
            this.layoutControlItem康复措施.Location = new System.Drawing.Point(0, 724);
            this.layoutControlItem康复措施.MinSize = new System.Drawing.Size(219, 27);
            this.layoutControlItem康复措施.Name = "layoutControlItem康复措施";
            this.layoutControlItem康复措施.Size = new System.Drawing.Size(731, 27);
            this.layoutControlItem康复措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem康复措施.Text = "康复措施";
            this.layoutControlItem康复措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem康复措施.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem康复措施.TextToControlDistance = 5;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(365, 309);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(366, 29);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(440, 364);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(291, 24);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(110, 130);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem2.CustomizationFormText = "社会功能情况";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem2.Size = new System.Drawing.Size(104, 124);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "社会功能情况";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(110, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem影响});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 130);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(110, 96);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // emptySpaceItem影响
            // 
            this.emptySpaceItem影响.AllowHotTrack = false;
            this.emptySpaceItem影响.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.emptySpaceItem影响.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem影响.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem影响.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem影响.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem影响.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem影响.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem影响.CustomizationFormText = "患者对家庭和社会的影响";
            this.emptySpaceItem影响.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem影响.MinSize = new System.Drawing.Size(10, 90);
            this.emptySpaceItem影响.Name = "emptySpaceItem影响";
            this.emptySpaceItem影响.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem影响.Size = new System.Drawing.Size(104, 90);
            this.emptySpaceItem影响.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem影响.Text = "危险行为";
            this.emptySpaceItem影响.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem影响.TextSize = new System.Drawing.Size(110, 20);
            this.emptySpaceItem影响.TextVisible = true;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(604, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(127, 26);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(604, 26);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(127, 26);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(604, 78);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(127, 26);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(604, 104);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(127, 26);
            this.emptySpaceItem13.Text = "emptySpaceItem13";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.flowLayoutPanel3;
            this.layoutControlItem12.CustomizationFormText = "通知联席部门";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 465);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(179, 60);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(731, 60);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "通知联席部门";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.flowLayoutPanel4;
            this.layoutControlItem13.CustomizationFormText = "转诊";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 525);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(219, 170);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(731, 170);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "转诊";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.textEdit患者或家属签名;
            this.layoutControlItem14.CustomizationFormText = "患者或家属签名";
            this.layoutControlItem14.Location = new System.Drawing.Point(498, 751);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem14.Text = "患者或家属签名";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem下次随访日期
            // 
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem下次随访日期.Control = this.dateEdit下次随访日期;
            this.layoutControlItem下次随访日期.CustomizationFormText = "下次随访日期*";
            this.layoutControlItem下次随访日期.Location = new System.Drawing.Point(0, 751);
            this.layoutControlItem下次随访日期.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem下次随访日期.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem下次随访日期.Name = "layoutControlItem下次随访日期";
            this.layoutControlItem下次随访日期.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem下次随访日期.Text = "下次随访日期*";
            this.layoutControlItem下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem下次随访日期.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem下次随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem用药情况
            // 
            this.layoutControlItem用药情况.Control = this.gridControl用药情况;
            this.layoutControlItem用药情况.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem用药情况.Location = new System.Drawing.Point(119, 412);
            this.layoutControlItem用药情况.MaxSize = new System.Drawing.Size(0, 27);
            this.layoutControlItem用药情况.MinSize = new System.Drawing.Size(150, 27);
            this.layoutControlItem用药情况.Name = "layoutControlItem用药情况";
            this.layoutControlItem用药情况.Size = new System.Drawing.Size(612, 27);
            this.layoutControlItem用药情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem用药情况.Text = "layoutControlItem用药情况";
            this.layoutControlItem用药情况.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem用药情况.TextToControlDistance = 0;
            this.layoutControlItem用药情况.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.sbtn添加药物;
            this.layoutControlItem34.CustomizationFormText = "用药情况";
            this.layoutControlItem34.Location = new System.Drawing.Point(119, 439);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(163, 26);
            this.layoutControlItem34.Text = "用药情况";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(282, 439);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(449, 26);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "用药情况";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 412);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(119, 53);
            this.emptySpaceItem5.Text = "用药情况";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.labelControl出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.labelControl身份证号;
            this.layoutControlItem6.CustomizationFormText = "身份证号";
            this.layoutControlItem6.Location = new System.Drawing.Point(367, 26);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "身份证号：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.labelControl联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(367, 52);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(111, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(364, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "联系电话：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.labelControl婚姻状况;
            this.layoutControlItem7.CustomizationFormText = "职业";
            this.layoutControlItem7.Location = new System.Drawing.Point(367, 78);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(111, 40);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(364, 40);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "婚姻状况 ：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem危险性
            // 
            this.layoutControlItem危险性.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem危险性.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem危险性.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem危险性.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem危险性.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem危险性.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem危险性.Control = this.comboBoxEdit危险性;
            this.layoutControlItem危险性.CustomizationFormText = "危险性";
            this.layoutControlItem危险性.Location = new System.Drawing.Point(367, 118);
            this.layoutControlItem危险性.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem危险性.Name = "layoutControlItem危险性";
            this.layoutControlItem危险性.Size = new System.Drawing.Size(364, 25);
            this.layoutControlItem危险性.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem危险性.Text = "危险性";
            this.layoutControlItem危险性.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem危险性.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem危险性.TextToControlDistance = 5;
            // 
            // layoutControlItem自知力
            // 
            this.layoutControlItem自知力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem自知力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem自知力.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem自知力.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem自知力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem自知力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem自知力.Control = this.radioGroup自知力;
            this.layoutControlItem自知力.CustomizationFormText = "自知力";
            this.layoutControlItem自知力.Location = new System.Drawing.Point(0, 397);
            this.layoutControlItem自知力.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem自知力.Name = "layoutControlItem自知力";
            this.layoutControlItem自知力.Size = new System.Drawing.Size(581, 25);
            this.layoutControlItem自知力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem自知力.Text = "自知力";
            this.layoutControlItem自知力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem自知力.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem自知力.TextToControlDistance = 5;
            // 
            // layoutControlItem目前症状
            // 
            this.layoutControlItem目前症状.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem目前症状.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem目前症状.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem目前症状.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem目前症状.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem目前症状.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem目前症状.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem目前症状.Control = this.flowLayoutPanel6;
            this.layoutControlItem目前症状.CustomizationFormText = "目前症状";
            this.layoutControlItem目前症状.Location = new System.Drawing.Point(0, 347);
            this.layoutControlItem目前症状.MinSize = new System.Drawing.Size(192, 50);
            this.layoutControlItem目前症状.Name = "layoutControlItem目前症状";
            this.layoutControlItem目前症状.Size = new System.Drawing.Size(731, 50);
            this.layoutControlItem目前症状.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem目前症状.Text = "目前症状";
            this.layoutControlItem目前症状.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem目前症状.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem目前症状.TextToControlDistance = 5;
            // 
            // layoutControlItem睡眠情况
            // 
            this.layoutControlItem睡眠情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem睡眠情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem睡眠情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem睡眠情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem睡眠情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem睡眠情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem睡眠情况.Control = this.radioGroup睡眠情况;
            this.layoutControlItem睡眠情况.CustomizationFormText = "睡眠情况";
            this.layoutControlItem睡眠情况.Location = new System.Drawing.Point(0, 422);
            this.layoutControlItem睡眠情况.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem睡眠情况.Name = "layoutControlItem睡眠情况";
            this.layoutControlItem睡眠情况.Size = new System.Drawing.Size(731, 25);
            this.layoutControlItem睡眠情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem睡眠情况.Text = "睡眠情况";
            this.layoutControlItem睡眠情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem睡眠情况.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem睡眠情况.TextToControlDistance = 5;
            // 
            // layoutControlItem饮食情况
            // 
            this.layoutControlItem饮食情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem饮食情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem饮食情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem饮食情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem饮食情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem饮食情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem饮食情况.Control = this.radioGroup饮食情况;
            this.layoutControlItem饮食情况.CustomizationFormText = "饮食情况";
            this.layoutControlItem饮食情况.Location = new System.Drawing.Point(0, 447);
            this.layoutControlItem饮食情况.MinSize = new System.Drawing.Size(169, 29);
            this.layoutControlItem饮食情况.Name = "layoutControlItem饮食情况";
            this.layoutControlItem饮食情况.Size = new System.Drawing.Size(731, 29);
            this.layoutControlItem饮食情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem饮食情况.Text = "饮食情况";
            this.layoutControlItem饮食情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem饮食情况.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem饮食情况.TextToControlDistance = 5;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(581, 397);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(150, 25);
            this.emptySpaceItem10.Text = "emptySpaceItem10";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.radio本次随访形式;
            this.layoutControlItem1.CustomizationFormText = "本次随访形式";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(367, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "本次随访形式";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.layoutControl2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(367, 143);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(364, 30);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.radio失访原因;
            this.layoutControlItem11.CustomizationFormText = "若失访，原因";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(731, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "若失访，原因";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.flowLayoutPanel5;
            this.layoutControlItem15.CustomizationFormText = "死亡原因";
            this.layoutControlItem15.Location = new System.Drawing.Point(100, 227);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(215, 120);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(631, 120);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "死亡原因";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 203);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(100, 144);
            this.emptySpaceItem9.Text = "如死亡日期和原因";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(306, 203);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(425, 24);
            this.emptySpaceItem14.Text = "emptySpaceItem14";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txt死亡日期;
            this.layoutControlItem18.CustomizationFormText = "死亡日期";
            this.layoutControlItem18.Location = new System.Drawing.Point(100, 203);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItem18.Text = "死亡日期";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // UC重性精神疾病患者随访服务记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC重性精神疾病患者随访服务记录表";
            this.Size = new System.Drawing.Size(750, 500);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt死亡日期.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit躯体疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio躯体疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自杀.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit他杀.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit意外.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit精神疾病并发症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit死亡原因其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患者或家属签名.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否需要转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊是否成功.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否联系精神专科医师.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否通知联席部门.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt受理人2电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio失访原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio本次随访形式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteHyperLinkEdit)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit生活劳动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit职业训练.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit学习能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit社会交往.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit康复措施其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit康复措施其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit此次随访分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit药物不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio药物不良反应.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup服药依从性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit实验室检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio实验室检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup社会人际交往.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup学习能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup生产劳动及工作.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup家务劳动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup个人生活料理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup饮食情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup关锁情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他危害行为.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit无影响.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自杀未遂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自伤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit肇祸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit肇事.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit轻度滋事.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup住院情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup治疗效果.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit幻觉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit交流困难.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit猜疑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit喜怒无常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit行为怪异.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit兴奋话多.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit伤人毁物.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit悲观厌世.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit无故外走.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自语自笑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit孤僻懒散.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit症状其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit危险性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup自知力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup睡眠情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem个人生活料理)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem家务劳动)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem生产劳动及工作)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem学习能力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem社会人际交往)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem关锁情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住院情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem实验室检查)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem服药依从性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem药物不良反应)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem治疗效果)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem此次随访分类)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem是否转诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem康复措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem影响)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem危险性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem自知力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem目前症状)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem睡眠情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem饮食情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton sbtn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl labelControl居住地址;
        private DevExpress.XtraEditors.LabelControl labelControl联系电话;
        private DevExpress.XtraEditors.LabelControl labelControl婚姻状况;
        private DevExpress.XtraEditors.LabelControl labelControl身份证号;
        private DevExpress.XtraEditors.LabelControl labelControl出生日期;
        private DevExpress.XtraEditors.LabelControl labelControl性别;
        private DevExpress.XtraEditors.LabelControl labelControl姓名;
        private DevExpress.XtraEditors.LabelControl labelControl个人档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem危险性;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem自知力;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem随访日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit幻觉;
        private DevExpress.XtraEditors.CheckEdit checkEdit交流困难;
        private DevExpress.XtraEditors.CheckEdit checkEdit猜疑;
        private DevExpress.XtraEditors.CheckEdit checkEdit喜怒无常;
        private DevExpress.XtraEditors.CheckEdit checkEdit行为怪异;
        private DevExpress.XtraEditors.CheckEdit checkEdit兴奋话多;
        private DevExpress.XtraEditors.CheckEdit checkEdit伤人毁物;
        private DevExpress.XtraEditors.CheckEdit checkEdit悲观厌世;
        private DevExpress.XtraEditors.CheckEdit checkEdit无故外走;
        private DevExpress.XtraEditors.RadioGroup radioGroup关锁情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem目前症状;
        private DevExpress.XtraEditors.LabelControl labelControl当前所属机构;
        private DevExpress.XtraEditors.LabelControl labelControl创建机构;
        private DevExpress.XtraEditors.LabelControl labelControl最近修改人;
        private DevExpress.XtraEditors.LabelControl labelControl创建人;
        private DevExpress.XtraEditors.LabelControl labelControl最近更新时间;
        private DevExpress.XtraEditors.LabelControl labelControl1创建时间;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private DevExpress.XtraEditors.RadioGroup radioGroup治疗效果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem治疗效果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit危险性;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem睡眠情况;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.RadioGroup radioGroup住院情况;
        private DevExpress.XtraEditors.CheckEdit checkEdit自语自笑;
        private DevExpress.XtraEditors.CheckEdit checkEdit孤僻懒散;
        private DevExpress.XtraEditors.CheckEdit checkEdit症状其他;
        private DevExpress.XtraEditors.TextEdit textEdit症状其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem住院情况;
        private Library.UserControls.UCLblDtp ucLblDtp末次出院情况;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.CheckEdit checkEdit无影响;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl自杀未遂;
        private DevExpress.XtraEditors.CheckEdit checkEdit自杀未遂;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl自伤;
        private DevExpress.XtraEditors.CheckEdit checkEdit自伤;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl肇祸;
        private DevExpress.XtraEditors.CheckEdit checkEdit肇祸;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl肇事;
        private DevExpress.XtraEditors.CheckEdit checkEdit肇事;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl轻度滋事;
        private DevExpress.XtraEditors.CheckEdit checkEdit轻度滋事;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem影响;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem关锁情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem随访医生;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private DevExpress.XtraEditors.RadioGroup radioGroup自知力;
        private DevExpress.XtraEditors.RadioGroup radioGroup社会人际交往;
        private DevExpress.XtraEditors.RadioGroup radioGroup学习能力;
        private DevExpress.XtraEditors.RadioGroup radioGroup生产劳动及工作;
        private DevExpress.XtraEditors.RadioGroup radioGroup家务劳动;
        private DevExpress.XtraEditors.RadioGroup radioGroup个人生活料理;
        private DevExpress.XtraEditors.RadioGroup radioGroup饮食情况;
        private DevExpress.XtraEditors.RadioGroup radioGroup睡眠情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem个人生活料理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem家务劳动;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem生产劳动及工作;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem学习能力;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem社会人际交往;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem饮食情况;
        private DevExpress.XtraEditors.RadioGroup radioGroup服药依从性;
        private DevExpress.XtraEditors.TextEdit textEdit实验室检查;
        private DevExpress.XtraEditors.RadioGroup radio实验室检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem实验室检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem服药依从性;
        private DevExpress.XtraEditors.TextEdit textEdit转诊科室;
        private DevExpress.XtraEditors.TextEdit textEdit转诊原因;
        private DevExpress.XtraEditors.RadioGroup radio是否转诊;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit此次随访分类;
        private DevExpress.XtraEditors.TextEdit textEdit药物不良反应;
        private DevExpress.XtraEditors.RadioGroup radio药物不良反应;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem药物不良反应;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem此次随访分类;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem是否转诊;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit checkEdit生活劳动;
        private DevExpress.XtraEditors.CheckEdit checkEdit职业训练;
        private DevExpress.XtraEditors.CheckEdit checkEdit学习能力;
        private DevExpress.XtraEditors.CheckEdit checkEdit社会交往;
        private DevExpress.XtraEditors.CheckEdit checkEdit康复措施其他;
        private DevExpress.XtraEditors.TextEdit textEdit康复措施其他;
        private DevExpress.XtraEditors.SimpleButton sbtn添加药物;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem康复措施;
        private DevExpress.XtraGrid.GridControl gridControl用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView用药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem用药情况;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit deleteHyperLinkEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.RadioGroup radio失访原因;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.RadioGroup radio本次随访形式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否通知联席部门;
        private Library.UserControls.UCLblTxt txt受理人1姓名;
        private DevExpress.XtraEditors.TextEdit txt受理人2姓名;
        private Library.UserControls.UCLblTxt txt受理人1电话;
        private DevExpress.XtraEditors.TextEdit txt受理人2电话;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl其他危害行为;
        private DevExpress.XtraEditors.CheckEdit checkEdit其他危害行为;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit textEdit患者或家属签名;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否需要转诊;
        private Library.UserControls.UCLblTxt txt转诊原因;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit转诊是否成功;
        private Library.UserControls.UCLblTxt txt转诊机构;
        private Library.UserControls.UCLblTxt txt转诊科室;
        private Library.UserControls.UCLblTxt txt转诊联系人;
        private Library.UserControls.UCLblTxt txt转诊联系电话;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否联系精神专科医师;
        private Library.UserControls.UCLblTxt txt精神专科医师姓名;
        private Library.UserControls.UCLblTxt txt精神专科医师电话;
        private Library.UserControls.UCLblTxt txt处置结果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit checkEdit躯体疾病;
        private DevExpress.XtraEditors.RadioGroup radio躯体疾病;
        private DevExpress.XtraEditors.CheckEdit checkEdit自杀;
        private DevExpress.XtraEditors.CheckEdit checkEdit他杀;
        private DevExpress.XtraEditors.CheckEdit checkEdit意外;
        private DevExpress.XtraEditors.CheckEdit checkEdit精神疾病并发症;
        private DevExpress.XtraEditors.CheckEdit checkEdit死亡原因其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.TextEdit txt死亡日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
    }
}
