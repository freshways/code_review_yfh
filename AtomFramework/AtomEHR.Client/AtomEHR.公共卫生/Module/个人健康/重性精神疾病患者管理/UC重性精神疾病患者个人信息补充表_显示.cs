﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理
{
    public partial class UC重性精神疾病患者个人信息补充表_显示 : UserControlBase
    {
        #region Fields
        DataSet _ds精神疾病;
        AtomEHR.Business.bll精神疾病信息补充表 _Bll = new Business.bll精神疾病信息补充表();
        public frm个人健康 _frm = null;
        string _docNo;
        string _familyNo;
        #endregion
        public UC重性精神疾病患者个人信息补充表_显示()
        {
            InitializeComponent();
        }
        public UC重性精神疾病患者个人信息补充表_显示(DataSet ds精神疾病, Form parentForm)
        {
            InitializeComponent();
            _ds精神疾病 = ds精神疾病;
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            //根据个人档案编号 查询全部的  体检数据
            //_ds老年人随访 = _Bll.GetAllDataByKey(_docNo);
            DoBindingDataSource(_ds精神疾病);//绑定数据
        }
        public UC重性精神疾病患者个人信息补充表_显示(Form parentForm)
        {
            InitializeComponent();
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            _ds精神疾病 = _Bll.GetBusinessByKey(_docNo, true);
            DoBindingDataSource(_ds精神疾病);
        }

        private void DoBindingDataSource(DataSet _ds精神疾病)
        {

            DataTable dt精神疾病 = _ds精神疾病.Tables[Models.tb_精神疾病信息补充表.__TableName];
            if (dt精神疾病 == null || dt精神疾病.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds精神疾病.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.lbl个人档案编号.Text = dt健康档案.Rows[0][tb_健康档案.个人档案编号].ToString();
            this.lbl姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.lbl性别.Text = _Bll.ReturnDis字典显示("xb_xingbie", dt健康档案.Rows[0][tb_健康档案.性别].ToString());
            this.lbl身份证号.Text = dt健康档案.Rows[0][tb_健康档案.身份证号].ToString();
            this.lbl出生日期.Text = dt健康档案.Rows[0][tb_健康档案.出生日期].ToString();
            this.lbl联系电话.Text = dt健康档案.Rows[0][tb_健康档案.联系人电话].ToString();
            this.lbl居住地址.Text = _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.市].ToString()) + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.区].ToString()) + _Bll.Return地区名称(dt精神疾病.Rows[0][tb_健康档案.街道].ToString()) + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.居委会].ToString()) + dt健康档案.Rows[0][tb_健康档案.居住地址].ToString();
            this.lbl婚姻状况.Text = _Bll.ReturnDis字典显示(Business.enumBase.hyzk.ToString(), dt健康档案.Rows[0][tb_健康档案.婚姻状况].ToString());

            this.textEdit档案类别.Text = _Bll.ReturnDis字典显示(Business.enumBase.dalb.ToString(), dt健康档案.Rows[0][tb_健康档案.档案类别].ToString());
            this.lbl创建时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建时间].ToString();
            this.lbl最近更新时间.Text = dt精神疾病.Rows[0][tb_精神疾病信息补充表.修改时间].ToString();
            this.lbl当前所属机构.Text = _Bll.Return机构名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.所属机构].ToString());
            this.lbl创建机构.Text = _Bll.Return机构名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建机构].ToString());
            this.lbl创建人.Text = _Bll.Return用户名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.创建人].ToString());
            this.lbl最近更新人.Text = _Bll.Return用户名称(dt精神疾病.Rows[0][tb_精神疾病信息补充表.修改人].ToString());

            this.txt与患者关系.Text = _Bll.ReturnDis字典显示("yhzgx", dt精神疾病.Rows[0][tb_精神疾病信息补充表.与患者关系].ToString());
            this.txt知情同意.Text = _Bll.ReturnDis字典显示("zhiqingTY", dt精神疾病.Rows[0][tb_精神疾病信息补充表.知情同意].ToString());

            DataBinder.BindingTextEdit(txt监护人姓名, dt精神疾病, tb_精神疾病信息补充表.监护人姓名);
            DataBinder.BindingTextEdit(txt监护人地址, dt精神疾病, tb_精神疾病信息补充表.监护人住址);
            DataBinder.BindingTextEdit(txt监护人电话, dt精神疾病, tb_精神疾病信息补充表.监护人电话);
            DataBinder.BindingTextEdit(txt联系人电话, dt精神疾病, tb_精神疾病信息补充表.联系人电话);
            DataBinder.BindingTextEdit(dte初次发病时间, dt精神疾病, tb_精神疾病信息补充表.初次发病时间);

            DataBinder.BindingTextEdit(txt知情同意签字, dt精神疾病, tb_精神疾病信息补充表.签字);
            DataBinder.BindingTextEdit(dte知情同意签字时间, dt精神疾病, tb_精神疾病信息补充表.签字时间);
            this.txt既往主要症状.Text = SetFlowLayoutValues("jwzyzz", dt精神疾病.Rows[0][tb_精神疾病信息补充表.既往主要症状].ToString()) + dt精神疾病.Rows[0][tb_精神疾病信息补充表.既往主要症状其他].ToString();
            this.txt门诊.Text = _Bll.ReturnDis字典显示("mzzlqk", dt精神疾病.Rows[0][tb_精神疾病信息补充表.门诊状况].ToString());
            this.txt最后一次治疗效果.Text = _Bll.ReturnDis字典显示("zlxg", dt精神疾病.Rows[0][tb_精神疾病信息补充表.治疗效果].ToString());
            DataBinder.BindingTextEditDateTime(dte首次治疗时间.Dtp1, dt精神疾病, tb_精神疾病信息补充表.首次抗精神病药治疗时间);
            DataBinder.BindingTextEdit(txt住院此次.Txt1, dt精神疾病, tb_精神疾病信息补充表.住院次数);
            DataBinder.BindingTextEdit(txt诊断.Txt1, dt精神疾病, tb_精神疾病信息补充表.诊断);
            DataBinder.BindingTextEdit(txt住院此次.Txt1, dt精神疾病, tb_精神疾病信息补充表.住院次数);
            DataBinder.BindingTextEdit(txt确诊医院.Txt1, dt精神疾病, tb_精神疾病信息补充表.确诊医院);
            DataBinder.BindingTextEdit(dte确诊日期.Dtp1, dt精神疾病, tb_精神疾病信息补充表.确诊日期);

            string 对家庭和社会的影响 = SetFlowLayoutValues("djtshyx", dt精神疾病.Rows[0][tb_精神疾病信息补充表.对家庭和社会的影响].ToString());
            string result = string.Empty;
            if (!string.IsNullOrEmpty(对家庭和社会的影响))
            {
                string[] a = 对家庭和社会的影响.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] == "轻度滋事")
                    {
                        result += a[i] + dt精神疾病.Rows[0][tb_精神疾病信息补充表.轻度滋事].ToString() + "次   ";
                    }
                    else if (a[i] == "肇事")
                    {
                        result += a[i] + dt精神疾病.Rows[0][tb_精神疾病信息补充表.肇事].ToString() + "次   ";
                    }
                    else if (a[i] == "肇祸")
                    {
                        result += a[i] + dt精神疾病.Rows[0][tb_精神疾病信息补充表.肇祸].ToString() + "次   ";
                    }
                    else if (a[i] == "自伤")
                    {
                        result += a[i] + dt精神疾病.Rows[0][tb_精神疾病信息补充表.自伤].ToString() + "次   ";
                    }
                    else if (a[i] == "自杀未遂")
                    {
                        result += a[i] + dt精神疾病.Rows[0][tb_精神疾病信息补充表.自杀未遂].ToString() + "次   ";
                    }
                    else
                    {
                        result += a[i];
                    }
                }
            }
            this.txt对家庭和社会的影响.Text = result;
            this.txt关锁情况.Text = _Bll.ReturnDis字典显示("gsqk", dt精神疾病.Rows[0][tb_精神疾病信息补充表.关锁情况].ToString());
            this.txt经济状况.Text = _Bll.ReturnDis字典显示("jj_zk", dt精神疾病.Rows[0][tb_精神疾病信息补充表.经济状况].ToString());

            DataBinder.BindingTextEdit(this.txt医生意见, dt精神疾病, tb_精神疾病信息补充表.医生意见);
            DataBinder.BindingTextEdit(dte检查日期, dt精神疾病, tb_精神疾病信息补充表.检查日期);
            DataBinder.BindingTextEdit(this.txt医生签字, dt精神疾病, tb_精神疾病信息补充表.医生签名);
            labelControl1.Text = string.Format("考核项：17     缺项：{0} 完整度：{1}% ", dt精神疾病.Rows[0][tb_精神疾病信息补充表.缺项].ToString(), dt精神疾病.Rows[0][tb_精神疾病信息补充表.完整度].ToString());

            //2017版新公卫标准添加 ▽            
            DataBinder.BindingRadioEdit(radioGroup就业情况,dt精神疾病,tb_精神疾病信息补充表.就业情况);
            DataBinder.BindingTextEdit(this.txt患者签名, dt精神疾病, tb_精神疾病信息补充表.患者签字);
            //2017版新公卫标准添加 △
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds精神疾病.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.所属机构].ToString()))
            {
                UC重性精神疾病患者个人信息补充表 uc = new UC重性精神疾病患者个人信息补充表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void UC重性精神疾病患者个人信息补充表_显示_Load(object sender, EventArgs e)
        {
            dte首次治疗时间.Dtp1.Enabled = false;
            txt住院此次.Txt1.Enabled = false;
            txt诊断.Txt1.Enabled = false;
            txt确诊医院.Txt1.Enabled = false;
            dte确诊日期.Dtp1.Enabled = false;
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds精神疾病.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.所属机构].ToString()))
            {
                if (Msg.AskQuestion("删除”重性精神疾病患者个人信息补充表“会把“重性精神疾病患者个人信息补充表”及“重性精神疾病患者随访表”信息一并删除，确定要删除么 ？"))
                {
                    if (_Bll.Delete(_docNo))
                    {
                        Msg.ShowInformation("精神病人信息删除成功！");
                        UC个人基本信息表_显示 uc = new UC个人基本信息表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.lbl个人档案编号.Text.Trim();
            if (!string.IsNullOrEmpty(docNo))
            {
                report重性精神疾病患者个人信息补充表 report = new report重性精神疾病患者个人信息补充表(docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }
    }
}
