﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class UC儿童基本信息_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC儿童基本信息_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit托幼机构电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit托幼机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit建档单位电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit建档单位 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母亲身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit邮政编码 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母亲职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父亲职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit住址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父亲姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母亲姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit发生时间 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem父亲姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母职业 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父职业 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem邮政编码 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem建档单位 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem托幼机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem托幼机构电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem建档单位电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母身份证号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem发生时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem考核项 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit邮政编码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母职业)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父职业)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem邮政编码)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母身份证号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem发生时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem考核项)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.sbtn删除);
            this.panelControl1.Controls.Add(this.sbtn修改);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 35);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(194, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "导出";
            // 
            // sbtn删除
            // 
            this.sbtn删除.Image = ((System.Drawing.Image)(resources.GetObject("sbtn删除.Image")));
            this.sbtn删除.Location = new System.Drawing.Point(100, 5);
            this.sbtn删除.Name = "sbtn删除";
            this.sbtn删除.Size = new System.Drawing.Size(75, 23);
            this.sbtn删除.TabIndex = 1;
            this.sbtn删除.Text = "删除";
            this.sbtn删除.Click += new System.EventHandler(this.sbtn删除_Click);
            // 
            // sbtn修改
            // 
            this.sbtn修改.Image = ((System.Drawing.Image)(resources.GetObject("sbtn修改.Image")));
            this.sbtn修改.Location = new System.Drawing.Point(6, 5);
            this.sbtn修改.Name = "sbtn修改";
            this.sbtn修改.Size = new System.Drawing.Size(75, 23);
            this.sbtn修改.TabIndex = 0;
            this.sbtn修改.Text = "修改";
            this.sbtn修改.Click += new System.EventHandler(this.sbtn修改_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改时间);
            this.layoutControl1.Controls.Add(this.textEdit录入时间);
            this.layoutControl1.Controls.Add(this.textEdit托幼机构电话);
            this.layoutControl1.Controls.Add(this.textEdit托幼机构);
            this.layoutControl1.Controls.Add(this.textEdit建档单位电话);
            this.layoutControl1.Controls.Add(this.textEdit建档单位);
            this.layoutControl1.Controls.Add(this.textEdit母亲身份证号);
            this.layoutControl1.Controls.Add(this.textEdit邮政编码);
            this.layoutControl1.Controls.Add(this.textEdit母联系电话);
            this.layoutControl1.Controls.Add(this.textEdit母亲职业);
            this.layoutControl1.Controls.Add(this.textEdit父联系电话);
            this.layoutControl1.Controls.Add(this.textEdit父亲职业);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit住址);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit儿童姓名);
            this.layoutControl1.Controls.Add(this.textEdit儿童档案号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.textEdit父亲姓名);
            this.layoutControl1.Controls.Add(this.textEdit母亲姓名);
            this.layoutControl1.Controls.Add(this.textEdit父出生日期);
            this.layoutControl1.Controls.Add(this.textEdit母出生日期);
            this.layoutControl1.Controls.Add(this.textEdit发生时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 35);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 417);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(106, 365);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(627, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 28;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(585, 341);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(148, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 27;
            // 
            // textEdit录入人
            // 
            this.textEdit录入人.Location = new System.Drawing.Point(342, 341);
            this.textEdit录入人.Name = "textEdit录入人";
            this.textEdit录入人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入人.Properties.ReadOnly = true;
            this.textEdit录入人.Size = new System.Drawing.Size(150, 20);
            this.textEdit录入人.StyleController = this.layoutControl1;
            this.textEdit录入人.TabIndex = 26;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(106, 341);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(167, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 25;
            // 
            // textEdit最近修改时间
            // 
            this.textEdit最近修改时间.Location = new System.Drawing.Point(585, 317);
            this.textEdit最近修改时间.Name = "textEdit最近修改时间";
            this.textEdit最近修改时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改时间.Properties.ReadOnly = true;
            this.textEdit最近修改时间.Size = new System.Drawing.Size(148, 20);
            this.textEdit最近修改时间.StyleController = this.layoutControl1;
            this.textEdit最近修改时间.TabIndex = 7;
            // 
            // textEdit录入时间
            // 
            this.textEdit录入时间.Location = new System.Drawing.Point(342, 317);
            this.textEdit录入时间.Name = "textEdit录入时间";
            this.textEdit录入时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入时间.Properties.ReadOnly = true;
            this.textEdit录入时间.Size = new System.Drawing.Size(150, 20);
            this.textEdit录入时间.StyleController = this.layoutControl1;
            this.textEdit录入时间.TabIndex = 7;
            // 
            // textEdit托幼机构电话
            // 
            this.textEdit托幼机构电话.Location = new System.Drawing.Point(471, 283);
            this.textEdit托幼机构电话.Name = "textEdit托幼机构电话";
            this.textEdit托幼机构电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit托幼机构电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit托幼机构电话.Properties.ReadOnly = true;
            this.textEdit托幼机构电话.Size = new System.Drawing.Size(262, 20);
            this.textEdit托幼机构电话.StyleController = this.layoutControl1;
            this.textEdit托幼机构电话.TabIndex = 23;
            // 
            // textEdit托幼机构
            // 
            this.textEdit托幼机构.Location = new System.Drawing.Point(97, 283);
            this.textEdit托幼机构.Name = "textEdit托幼机构";
            this.textEdit托幼机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit托幼机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit托幼机构.Properties.ReadOnly = true;
            this.textEdit托幼机构.Size = new System.Drawing.Size(276, 20);
            this.textEdit托幼机构.StyleController = this.layoutControl1;
            this.textEdit托幼机构.TabIndex = 22;
            // 
            // textEdit建档单位电话
            // 
            this.textEdit建档单位电话.Location = new System.Drawing.Point(471, 259);
            this.textEdit建档单位电话.Name = "textEdit建档单位电话";
            this.textEdit建档单位电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit建档单位电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit建档单位电话.Properties.ReadOnly = true;
            this.textEdit建档单位电话.Size = new System.Drawing.Size(262, 20);
            this.textEdit建档单位电话.StyleController = this.layoutControl1;
            this.textEdit建档单位电话.TabIndex = 21;
            // 
            // textEdit建档单位
            // 
            this.textEdit建档单位.Location = new System.Drawing.Point(97, 259);
            this.textEdit建档单位.Name = "textEdit建档单位";
            this.textEdit建档单位.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit建档单位.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit建档单位.Properties.ReadOnly = true;
            this.textEdit建档单位.Size = new System.Drawing.Size(276, 20);
            this.textEdit建档单位.StyleController = this.layoutControl1;
            this.textEdit建档单位.TabIndex = 20;
            // 
            // textEdit母亲身份证号
            // 
            this.textEdit母亲身份证号.Location = new System.Drawing.Point(471, 235);
            this.textEdit母亲身份证号.Name = "textEdit母亲身份证号";
            this.textEdit母亲身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母亲身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母亲身份证号.Properties.ReadOnly = true;
            this.textEdit母亲身份证号.Size = new System.Drawing.Size(262, 20);
            this.textEdit母亲身份证号.StyleController = this.layoutControl1;
            this.textEdit母亲身份证号.TabIndex = 19;
            // 
            // textEdit邮政编码
            // 
            this.textEdit邮政编码.Location = new System.Drawing.Point(97, 235);
            this.textEdit邮政编码.Name = "textEdit邮政编码";
            this.textEdit邮政编码.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit邮政编码.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit邮政编码.Properties.ReadOnly = true;
            this.textEdit邮政编码.Size = new System.Drawing.Size(276, 20);
            this.textEdit邮政编码.StyleController = this.layoutControl1;
            this.textEdit邮政编码.TabIndex = 18;
            // 
            // textEdit母联系电话
            // 
            this.textEdit母联系电话.Location = new System.Drawing.Point(446, 201);
            this.textEdit母联系电话.Name = "textEdit母联系电话";
            this.textEdit母联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母联系电话.Properties.ReadOnly = true;
            this.textEdit母联系电话.Size = new System.Drawing.Size(106, 20);
            this.textEdit母联系电话.StyleController = this.layoutControl1;
            this.textEdit母联系电话.TabIndex = 16;
            // 
            // textEdit母亲职业
            // 
            this.textEdit母亲职业.Location = new System.Drawing.Point(247, 201);
            this.textEdit母亲职业.Name = "textEdit母亲职业";
            this.textEdit母亲职业.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母亲职业.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母亲职业.Properties.ReadOnly = true;
            this.textEdit母亲职业.Size = new System.Drawing.Size(125, 20);
            this.textEdit母亲职业.StyleController = this.layoutControl1;
            this.textEdit母亲职业.TabIndex = 15;
            // 
            // textEdit父联系电话
            // 
            this.textEdit父联系电话.Location = new System.Drawing.Point(446, 177);
            this.textEdit父联系电话.Name = "textEdit父联系电话";
            this.textEdit父联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父联系电话.Properties.ReadOnly = true;
            this.textEdit父联系电话.Size = new System.Drawing.Size(106, 20);
            this.textEdit父联系电话.StyleController = this.layoutControl1;
            this.textEdit父联系电话.TabIndex = 12;
            // 
            // textEdit父亲职业
            // 
            this.textEdit父亲职业.Location = new System.Drawing.Point(247, 177);
            this.textEdit父亲职业.Name = "textEdit父亲职业";
            this.textEdit父亲职业.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父亲职业.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父亲职业.Properties.ReadOnly = true;
            this.textEdit父亲职业.Size = new System.Drawing.Size(125, 20);
            this.textEdit父亲职业.StyleController = this.layoutControl1;
            this.textEdit父亲职业.TabIndex = 11;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(455, 144);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(279, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 9;
            // 
            // textEdit住址
            // 
            this.textEdit住址.Location = new System.Drawing.Point(96, 144);
            this.textEdit住址.Name = "textEdit住址";
            this.textEdit住址.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit住址.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit住址.Properties.ReadOnly = true;
            this.textEdit住址.Size = new System.Drawing.Size(277, 20);
            this.textEdit住址.StyleController = this.layoutControl1;
            this.textEdit住址.TabIndex = 8;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(455, 122);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(279, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 7;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(96, 122);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit性别.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(277, 20);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 7;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(455, 100);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(279, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 6;
            // 
            // textEdit儿童姓名
            // 
            this.textEdit儿童姓名.Location = new System.Drawing.Point(96, 100);
            this.textEdit儿童姓名.Name = "textEdit儿童姓名";
            this.textEdit儿童姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童姓名.Properties.ReadOnly = true;
            this.textEdit儿童姓名.Size = new System.Drawing.Size(277, 20);
            this.textEdit儿童姓名.StyleController = this.layoutControl1;
            this.textEdit儿童姓名.TabIndex = 6;
            // 
            // textEdit儿童档案号
            // 
            this.textEdit儿童档案号.Location = new System.Drawing.Point(455, 78);
            this.textEdit儿童档案号.Name = "textEdit儿童档案号";
            this.textEdit儿童档案号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童档案号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童档案号.Properties.ReadOnly = true;
            this.textEdit儿童档案号.Size = new System.Drawing.Size(279, 20);
            this.textEdit儿童档案号.StyleController = this.layoutControl1;
            this.textEdit儿童档案号.TabIndex = 5;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(96, 78);
            this.textEdit卡号.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit卡号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit卡号.Properties.ReadOnly = true;
            this.textEdit卡号.Size = new System.Drawing.Size(277, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 4;
            // 
            // textEdit父亲姓名
            // 
            this.textEdit父亲姓名.Location = new System.Drawing.Point(87, 177);
            this.textEdit父亲姓名.Name = "textEdit父亲姓名";
            this.textEdit父亲姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父亲姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父亲姓名.Properties.ReadOnly = true;
            this.textEdit父亲姓名.Size = new System.Drawing.Size(112, 20);
            this.textEdit父亲姓名.StyleController = this.layoutControl1;
            this.textEdit父亲姓名.TabIndex = 10;
            // 
            // textEdit母亲姓名
            // 
            this.textEdit母亲姓名.Location = new System.Drawing.Point(87, 201);
            this.textEdit母亲姓名.Name = "textEdit母亲姓名";
            this.textEdit母亲姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母亲姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母亲姓名.Properties.ReadOnly = true;
            this.textEdit母亲姓名.Size = new System.Drawing.Size(112, 20);
            this.textEdit母亲姓名.StyleController = this.layoutControl1;
            this.textEdit母亲姓名.TabIndex = 14;
            // 
            // textEdit父出生日期
            // 
            this.textEdit父出生日期.Location = new System.Drawing.Point(626, 177);
            this.textEdit父出生日期.Name = "textEdit父出生日期";
            this.textEdit父出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父出生日期.Properties.ReadOnly = true;
            this.textEdit父出生日期.Size = new System.Drawing.Size(107, 20);
            this.textEdit父出生日期.StyleController = this.layoutControl1;
            this.textEdit父出生日期.TabIndex = 13;
            // 
            // textEdit母出生日期
            // 
            this.textEdit母出生日期.Location = new System.Drawing.Point(626, 201);
            this.textEdit母出生日期.Name = "textEdit母出生日期";
            this.textEdit母出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母出生日期.Properties.ReadOnly = true;
            this.textEdit母出生日期.Size = new System.Drawing.Size(107, 20);
            this.textEdit母出生日期.StyleController = this.layoutControl1;
            this.textEdit母出生日期.TabIndex = 17;
            // 
            // textEdit发生时间
            // 
            this.textEdit发生时间.Location = new System.Drawing.Point(106, 317);
            this.textEdit发生时间.Name = "textEdit发生时间";
            this.textEdit发生时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit发生时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit发生时间.Properties.ReadOnly = true;
            this.textEdit发生时间.Size = new System.Drawing.Size(167, 20);
            this.textEdit发生时间.StyleController = this.layoutControl1;
            this.textEdit发生时间.TabIndex = 24;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.emptySpaceItem考核项,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(750, 417);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(81)))), ((int)(((byte)(93)))));
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "更新儿童基本信息";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 36);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 36);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(730, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "儿童基本信息表";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(91, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 62);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(730, 98);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit卡号;
            this.layoutControlItem1.CustomizationFormText = "卡号：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem1.Size = new System.Drawing.Size(359, 22);
            this.layoutControlItem1.Text = "卡号：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit儿童姓名;
            this.layoutControlItem3.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 22);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem3.Size = new System.Drawing.Size(359, 22);
            this.layoutControlItem3.Text = "儿童姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit性别;
            this.layoutControlItem4.CustomizationFormText = "性别：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem4.Size = new System.Drawing.Size(359, 22);
            this.layoutControlItem4.Text = "性别：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit住址;
            this.layoutControlItem7.CustomizationFormText = "住址：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem7.Size = new System.Drawing.Size(359, 22);
            this.layoutControlItem7.Text = "住址：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话：";
            this.layoutControlItem8.Location = new System.Drawing.Point(359, 66);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem8.Size = new System.Drawing.Size(361, 22);
            this.layoutControlItem8.Text = "联系电话：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(359, 44);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem6.Size = new System.Drawing.Size(361, 22);
            this.layoutControlItem6.Text = "出生日期：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(359, 22);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem5.Size = new System.Drawing.Size(361, 22);
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit儿童档案号;
            this.layoutControlItem2.CustomizationFormText = "儿童档案号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(359, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem2.Size = new System.Drawing.Size(361, 22);
            this.layoutControlItem2.Text = "儿童档案号：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem父亲姓名,
            this.layoutControlItem母姓名,
            this.layoutControlItem母职业,
            this.layoutControlItem父职业,
            this.layoutControlItem父电话,
            this.layoutControlItem母电话,
            this.layoutControlItem母出生日期,
            this.layoutControlItem父出生日期});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 160);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(730, 58);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem父亲姓名
            // 
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父亲姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父亲姓名.Control = this.textEdit父亲姓名;
            this.layoutControlItem父亲姓名.CustomizationFormText = "父亲姓名：";
            this.layoutControlItem父亲姓名.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem父亲姓名.Name = "layoutControlItem父亲姓名";
            this.layoutControlItem父亲姓名.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem父亲姓名.Text = "父亲姓名：";
            this.layoutControlItem父亲姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父亲姓名.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父亲姓名.TextToControlDistance = 5;
            // 
            // layoutControlItem母姓名
            // 
            this.layoutControlItem母姓名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母姓名.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母姓名.Control = this.textEdit母亲姓名;
            this.layoutControlItem母姓名.CustomizationFormText = "母亲姓名：";
            this.layoutControlItem母姓名.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem母姓名.Name = "layoutControlItem母姓名";
            this.layoutControlItem母姓名.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem母姓名.Text = "母亲姓名：";
            this.layoutControlItem母姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母姓名.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母姓名.TextToControlDistance = 5;
            // 
            // layoutControlItem母职业
            // 
            this.layoutControlItem母职业.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母职业.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母职业.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母职业.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem母职业.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem母职业.Control = this.textEdit母亲职业;
            this.layoutControlItem母职业.CustomizationFormText = "职业：";
            this.layoutControlItem母职业.Location = new System.Drawing.Point(186, 24);
            this.layoutControlItem母职业.Name = "layoutControlItem母职业";
            this.layoutControlItem母职业.Size = new System.Drawing.Size(173, 24);
            this.layoutControlItem母职业.Text = "职业：";
            this.layoutControlItem母职业.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母职业.TextSize = new System.Drawing.Size(39, 14);
            this.layoutControlItem母职业.TextToControlDistance = 5;
            // 
            // layoutControlItem父职业
            // 
            this.layoutControlItem父职业.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父职业.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父职业.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父职业.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem父职业.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem父职业.Control = this.textEdit父亲职业;
            this.layoutControlItem父职业.CustomizationFormText = "职业：";
            this.layoutControlItem父职业.Location = new System.Drawing.Point(186, 0);
            this.layoutControlItem父职业.Name = "layoutControlItem父职业";
            this.layoutControlItem父职业.Size = new System.Drawing.Size(173, 24);
            this.layoutControlItem父职业.Text = "职业：";
            this.layoutControlItem父职业.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父职业.TextSize = new System.Drawing.Size(39, 14);
            this.layoutControlItem父职业.TextToControlDistance = 5;
            // 
            // layoutControlItem父电话
            // 
            this.layoutControlItem父电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父电话.Control = this.textEdit父联系电话;
            this.layoutControlItem父电话.CustomizationFormText = "联系电话：";
            this.layoutControlItem父电话.Location = new System.Drawing.Point(359, 0);
            this.layoutControlItem父电话.Name = "layoutControlItem父电话";
            this.layoutControlItem父电话.Size = new System.Drawing.Size(180, 24);
            this.layoutControlItem父电话.Text = "联系电话：";
            this.layoutControlItem父电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父电话.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父电话.TextToControlDistance = 5;
            // 
            // layoutControlItem母电话
            // 
            this.layoutControlItem母电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母电话.Control = this.textEdit母联系电话;
            this.layoutControlItem母电话.CustomizationFormText = "联系电话：";
            this.layoutControlItem母电话.Location = new System.Drawing.Point(359, 24);
            this.layoutControlItem母电话.Name = "layoutControlItem母电话";
            this.layoutControlItem母电话.Size = new System.Drawing.Size(180, 24);
            this.layoutControlItem母电话.Text = "联系电话：";
            this.layoutControlItem母电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母电话.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母电话.TextToControlDistance = 5;
            // 
            // layoutControlItem母出生日期
            // 
            this.layoutControlItem母出生日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母出生日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母出生日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母出生日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母出生日期.Control = this.textEdit母出生日期;
            this.layoutControlItem母出生日期.CustomizationFormText = "出生日期：";
            this.layoutControlItem母出生日期.Location = new System.Drawing.Point(539, 24);
            this.layoutControlItem母出生日期.Name = "layoutControlItem母出生日期";
            this.layoutControlItem母出生日期.Size = new System.Drawing.Size(181, 24);
            this.layoutControlItem母出生日期.Text = "出生日期：";
            this.layoutControlItem母出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母出生日期.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母出生日期.TextToControlDistance = 5;
            // 
            // layoutControlItem父出生日期
            // 
            this.layoutControlItem父出生日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父出生日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父出生日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父出生日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父出生日期.Control = this.textEdit父出生日期;
            this.layoutControlItem父出生日期.CustomizationFormText = "出生日期：";
            this.layoutControlItem父出生日期.Location = new System.Drawing.Point(539, 0);
            this.layoutControlItem父出生日期.Name = "layoutControlItem父出生日期";
            this.layoutControlItem父出生日期.Size = new System.Drawing.Size(181, 24);
            this.layoutControlItem父出生日期.Text = "出生日期：";
            this.layoutControlItem父出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父出生日期.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父出生日期.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem邮政编码,
            this.layoutControlItem建档单位,
            this.layoutControlItem托幼机构,
            this.layoutControlItem托幼机构电话,
            this.layoutControlItem建档单位电话,
            this.layoutControlItem母身份证号});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 218);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(730, 82);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem邮政编码
            // 
            this.layoutControlItem邮政编码.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem邮政编码.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem邮政编码.Control = this.textEdit邮政编码;
            this.layoutControlItem邮政编码.CustomizationFormText = "邮政编码：";
            this.layoutControlItem邮政编码.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem邮政编码.Name = "layoutControlItem邮政编码";
            this.layoutControlItem邮政编码.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem邮政编码.Text = "邮政编码：";
            this.layoutControlItem邮政编码.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem邮政编码.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem邮政编码.TextToControlDistance = 5;
            // 
            // layoutControlItem建档单位
            // 
            this.layoutControlItem建档单位.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem建档单位.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem建档单位.Control = this.textEdit建档单位;
            this.layoutControlItem建档单位.CustomizationFormText = "建档单位：";
            this.layoutControlItem建档单位.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem建档单位.Name = "layoutControlItem建档单位";
            this.layoutControlItem建档单位.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem建档单位.Text = "建档单位：";
            this.layoutControlItem建档单位.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem建档单位.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem建档单位.TextToControlDistance = 5;
            // 
            // layoutControlItem托幼机构
            // 
            this.layoutControlItem托幼机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem托幼机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem托幼机构.Control = this.textEdit托幼机构;
            this.layoutControlItem托幼机构.CustomizationFormText = "托幼机构：";
            this.layoutControlItem托幼机构.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem托幼机构.Name = "layoutControlItem托幼机构";
            this.layoutControlItem托幼机构.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem托幼机构.Text = "托幼机构：";
            this.layoutControlItem托幼机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem托幼机构.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem托幼机构.TextToControlDistance = 5;
            // 
            // layoutControlItem托幼机构电话
            // 
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem托幼机构电话.Control = this.textEdit托幼机构电话;
            this.layoutControlItem托幼机构电话.CustomizationFormText = "电话：";
            this.layoutControlItem托幼机构电话.Location = new System.Drawing.Point(360, 48);
            this.layoutControlItem托幼机构电话.Name = "layoutControlItem托幼机构电话";
            this.layoutControlItem托幼机构电话.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem托幼机构电话.Text = "电话：";
            this.layoutControlItem托幼机构电话.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem建档单位电话
            // 
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem建档单位电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem建档单位电话.Control = this.textEdit建档单位电话;
            this.layoutControlItem建档单位电话.CustomizationFormText = "电话：";
            this.layoutControlItem建档单位电话.Location = new System.Drawing.Point(360, 24);
            this.layoutControlItem建档单位电话.Name = "layoutControlItem建档单位电话";
            this.layoutControlItem建档单位电话.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem建档单位电话.Text = "电话：";
            this.layoutControlItem建档单位电话.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem母身份证号
            // 
            this.layoutControlItem母身份证号.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母身份证号.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母身份证号.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母身份证号.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母身份证号.Control = this.textEdit母亲身份证号;
            this.layoutControlItem母身份证号.CustomizationFormText = "母亲身份证号：";
            this.layoutControlItem母身份证号.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem母身份证号.Name = "layoutControlItem母身份证号";
            this.layoutControlItem母身份证号.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem母身份证号.Text = "母亲身份证号：";
            this.layoutControlItem母身份证号.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29,
            this.layoutControlItem26,
            this.layoutControlItem发生时间,
            this.layoutControlItem24,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem25});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 300);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(730, 82);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem29.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem29.Control = this.textEdit当前所属机构;
            this.layoutControlItem29.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(720, 24);
            this.layoutControlItem29.Text = "当前所属机构：";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem26.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit创建机构;
            this.layoutControlItem26.CustomizationFormText = "创建机构：";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem26.Text = "创建机构：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem发生时间
            // 
            this.layoutControlItem发生时间.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem发生时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem发生时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem发生时间.Control = this.textEdit发生时间;
            this.layoutControlItem发生时间.CustomizationFormText = "发生时间：";
            this.layoutControlItem发生时间.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem发生时间.Name = "layoutControlItem发生时间";
            this.layoutControlItem发生时间.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem发生时间.Text = "发生时间：";
            this.layoutControlItem发生时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem发生时间.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem发生时间.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem24.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem24.Control = this.textEdit录入时间;
            this.layoutControlItem24.CustomizationFormText = "录入时间：";
            this.layoutControlItem24.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem24.Text = "录入时间：";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem27.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem27.Control = this.textEdit录入人;
            this.layoutControlItem27.CustomizationFormText = "录入人：";
            this.layoutControlItem27.Location = new System.Drawing.Point(260, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem27.Text = "录入人：";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem28.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem28.Control = this.textEdit最近修改人;
            this.layoutControlItem28.CustomizationFormText = "最近修改人：";
            this.layoutControlItem28.Location = new System.Drawing.Point(479, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem28.Text = "最近修改人：";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControlItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem25.Control = this.textEdit最近修改时间;
            this.layoutControlItem25.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem25.Location = new System.Drawing.Point(479, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem25.Text = "最近更新时间：";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // emptySpaceItem考核项
            // 
            this.emptySpaceItem考核项.AllowHotTrack = false;
            this.emptySpaceItem考核项.CustomizationFormText = "  考核项：  缺项：   完整度： % ";
            this.emptySpaceItem考核项.Location = new System.Drawing.Point(0, 36);
            this.emptySpaceItem考核项.MaxSize = new System.Drawing.Size(0, 26);
            this.emptySpaceItem考核项.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem考核项.Name = "emptySpaceItem考核项";
            this.emptySpaceItem考核项.Size = new System.Drawing.Size(730, 26);
            this.emptySpaceItem考核项.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem考核项.Text = "  考核项：  缺项：   完整度： % ";
            this.emptySpaceItem考核项.TextSize = new System.Drawing.Size(91, 0);
            this.emptySpaceItem考核项.TextVisible = true;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 382);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(730, 15);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // UC儿童基本信息_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC儿童基本信息_显示";
            this.Size = new System.Drawing.Size(750, 452);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit邮政编码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母职业)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父职业)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem邮政编码)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母身份证号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem发生时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem考核项)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton sbtn删除;
        private DevExpress.XtraEditors.SimpleButton sbtn修改;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit录入人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改时间;
        private DevExpress.XtraEditors.TextEdit textEdit录入时间;
        private DevExpress.XtraEditors.TextEdit textEdit托幼机构电话;
        private DevExpress.XtraEditors.TextEdit textEdit托幼机构;
        private DevExpress.XtraEditors.TextEdit textEdit建档单位电话;
        private DevExpress.XtraEditors.TextEdit textEdit建档单位;
        private DevExpress.XtraEditors.TextEdit textEdit母亲身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit邮政编码;
        private DevExpress.XtraEditors.TextEdit textEdit母联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit母亲职业;
        private DevExpress.XtraEditors.TextEdit textEdit父联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit父亲职业;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit住址;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit儿童姓名;
        private DevExpress.XtraEditors.TextEdit textEdit儿童档案号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父亲姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母出生日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父出生日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem邮政编码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem建档单位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem托幼机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem托幼机构电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem建档单位电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母身份证号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem发生时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit textEdit父亲姓名;
        private DevExpress.XtraEditors.TextEdit textEdit母亲姓名;
        private DevExpress.XtraEditors.TextEdit textEdit父出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit母出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit发生时间;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem考核项;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}
