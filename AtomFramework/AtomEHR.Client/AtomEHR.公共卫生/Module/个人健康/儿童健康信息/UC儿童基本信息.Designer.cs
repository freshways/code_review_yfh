﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class UC儿童基本信息
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit发生时间 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit托幼机构电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit托幼机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit建档单位电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit建档单位 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母亲身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit邮政编码 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母亲职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父亲职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit住址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit母亲姓名 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit父亲姓名 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEdit父出生日期 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit母出生日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItemHeader = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem卡号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem父亲姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母亲姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母亲职业 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父亲职业 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父亲联系电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母亲联系电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母亲出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem父亲出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem邮政编码 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem建档单位 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem托幼机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem托幼机构电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem建档单位电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母亲身份证号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem发生时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit发生时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit发生时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit邮政编码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit母亲姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit父亲姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit父出生日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit父出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit母出生日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit母出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem卡号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲职业)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲职业)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲联系电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲联系电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem邮政编码)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲身份证号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem发生时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sbtn保存);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(804, 35);
            this.panelControl1.TabIndex = 0;
            // 
            // sbtn保存
            // 
            this.sbtn保存.Location = new System.Drawing.Point(14, 7);
            this.sbtn保存.Name = "sbtn保存";
            this.sbtn保存.Size = new System.Drawing.Size(75, 23);
            this.sbtn保存.TabIndex = 0;
            this.sbtn保存.Text = "保存";
            this.sbtn保存.Click += new System.EventHandler(this.sbtn保存_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改时间);
            this.layoutControl1.Controls.Add(this.textEdit录入时间);
            this.layoutControl1.Controls.Add(this.dateEdit发生时间);
            this.layoutControl1.Controls.Add(this.textEdit托幼机构电话);
            this.layoutControl1.Controls.Add(this.textEdit托幼机构);
            this.layoutControl1.Controls.Add(this.textEdit建档单位电话);
            this.layoutControl1.Controls.Add(this.textEdit建档单位);
            this.layoutControl1.Controls.Add(this.textEdit母亲身份证号);
            this.layoutControl1.Controls.Add(this.textEdit邮政编码);
            this.layoutControl1.Controls.Add(this.textEdit母联系电话);
            this.layoutControl1.Controls.Add(this.textEdit母亲职业);
            this.layoutControl1.Controls.Add(this.textEdit父联系电话);
            this.layoutControl1.Controls.Add(this.textEdit父亲职业);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit住址);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit儿童姓名);
            this.layoutControl1.Controls.Add(this.textEdit儿童档案号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.comboBoxEdit母亲姓名);
            this.layoutControl1.Controls.Add(this.comboBoxEdit父亲姓名);
            this.layoutControl1.Controls.Add(this.dateEdit父出生日期);
            this.layoutControl1.Controls.Add(this.dateEdit母出生日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 35);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(240, 135, 628, 446);
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(804, 427);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(106, 339);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit当前所属机构.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(681, 18);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 28;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(621, 315);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改人.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(166, 18);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 27;
            // 
            // textEdit录入人
            // 
            this.textEdit录入人.Location = new System.Drawing.Point(361, 315);
            this.textEdit录入人.Name = "textEdit录入人";
            this.textEdit录入人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit录入人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入人.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit录入人.Properties.ReadOnly = true;
            this.textEdit录入人.Size = new System.Drawing.Size(167, 18);
            this.textEdit录入人.StyleController = this.layoutControl1;
            this.textEdit录入人.TabIndex = 26;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(106, 315);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit创建机构.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(186, 18);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 25;
            // 
            // textEdit最近修改时间
            // 
            this.textEdit最近修改时间.Location = new System.Drawing.Point(621, 291);
            this.textEdit最近修改时间.Name = "textEdit最近修改时间";
            this.textEdit最近修改时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit最近修改时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改时间.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit最近修改时间.Properties.ReadOnly = true;
            this.textEdit最近修改时间.Size = new System.Drawing.Size(166, 18);
            this.textEdit最近修改时间.StyleController = this.layoutControl1;
            this.textEdit最近修改时间.TabIndex = 7;
            // 
            // textEdit录入时间
            // 
            this.textEdit录入时间.Location = new System.Drawing.Point(361, 291);
            this.textEdit录入时间.Name = "textEdit录入时间";
            this.textEdit录入时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit录入时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入时间.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit录入时间.Properties.ReadOnly = true;
            this.textEdit录入时间.Size = new System.Drawing.Size(167, 18);
            this.textEdit录入时间.StyleController = this.layoutControl1;
            this.textEdit录入时间.TabIndex = 7;
            // 
            // dateEdit发生时间
            // 
            this.dateEdit发生时间.EditValue = null;
            this.dateEdit发生时间.Location = new System.Drawing.Point(106, 291);
            this.dateEdit发生时间.Name = "dateEdit发生时间";
            this.dateEdit发生时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit发生时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit发生时间.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit发生时间.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit发生时间.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit发生时间.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit发生时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit发生时间.Size = new System.Drawing.Size(186, 20);
            this.dateEdit发生时间.StyleController = this.layoutControl1;
            this.dateEdit发生时间.TabIndex = 24;
            // 
            // textEdit托幼机构电话
            // 
            this.textEdit托幼机构电话.Location = new System.Drawing.Point(498, 257);
            this.textEdit托幼机构电话.Name = "textEdit托幼机构电话";
            this.textEdit托幼机构电话.Size = new System.Drawing.Size(289, 20);
            this.textEdit托幼机构电话.StyleController = this.layoutControl1;
            this.textEdit托幼机构电话.TabIndex = 23;
            // 
            // textEdit托幼机构
            // 
            this.textEdit托幼机构.Location = new System.Drawing.Point(97, 257);
            this.textEdit托幼机构.Name = "textEdit托幼机构";
            this.textEdit托幼机构.Size = new System.Drawing.Size(303, 20);
            this.textEdit托幼机构.StyleController = this.layoutControl1;
            this.textEdit托幼机构.TabIndex = 22;
            // 
            // textEdit建档单位电话
            // 
            this.textEdit建档单位电话.Location = new System.Drawing.Point(498, 233);
            this.textEdit建档单位电话.Name = "textEdit建档单位电话";
            this.textEdit建档单位电话.Size = new System.Drawing.Size(289, 20);
            this.textEdit建档单位电话.StyleController = this.layoutControl1;
            this.textEdit建档单位电话.TabIndex = 21;
            // 
            // textEdit建档单位
            // 
            this.textEdit建档单位.Location = new System.Drawing.Point(97, 233);
            this.textEdit建档单位.Name = "textEdit建档单位";
            this.textEdit建档单位.Size = new System.Drawing.Size(303, 20);
            this.textEdit建档单位.StyleController = this.layoutControl1;
            this.textEdit建档单位.TabIndex = 20;
            // 
            // textEdit母亲身份证号
            // 
            this.textEdit母亲身份证号.Location = new System.Drawing.Point(498, 209);
            this.textEdit母亲身份证号.Name = "textEdit母亲身份证号";
            this.textEdit母亲身份证号.Size = new System.Drawing.Size(289, 20);
            this.textEdit母亲身份证号.StyleController = this.layoutControl1;
            this.textEdit母亲身份证号.TabIndex = 19;
            // 
            // textEdit邮政编码
            // 
            this.textEdit邮政编码.Location = new System.Drawing.Point(97, 209);
            this.textEdit邮政编码.Name = "textEdit邮政编码";
            this.textEdit邮政编码.Size = new System.Drawing.Size(303, 20);
            this.textEdit邮政编码.StyleController = this.layoutControl1;
            this.textEdit邮政编码.TabIndex = 18;
            // 
            // textEdit母联系电话
            // 
            this.textEdit母联系电话.Location = new System.Drawing.Point(473, 175);
            this.textEdit母联系电话.Name = "textEdit母联系电话";
            this.textEdit母联系电话.Size = new System.Drawing.Size(119, 20);
            this.textEdit母联系电话.StyleController = this.layoutControl1;
            this.textEdit母联系电话.TabIndex = 16;
            // 
            // textEdit母亲职业
            // 
            this.textEdit母亲职业.Location = new System.Drawing.Point(253, 175);
            this.textEdit母亲职业.Name = "textEdit母亲职业";
            this.textEdit母亲职业.Size = new System.Drawing.Size(146, 20);
            this.textEdit母亲职业.StyleController = this.layoutControl1;
            this.textEdit母亲职业.TabIndex = 15;
            // 
            // textEdit父联系电话
            // 
            this.textEdit父联系电话.Location = new System.Drawing.Point(473, 151);
            this.textEdit父联系电话.Name = "textEdit父联系电话";
            this.textEdit父联系电话.Size = new System.Drawing.Size(119, 20);
            this.textEdit父联系电话.StyleController = this.layoutControl1;
            this.textEdit父联系电话.TabIndex = 12;
            // 
            // textEdit父亲职业
            // 
            this.textEdit父亲职业.Location = new System.Drawing.Point(253, 151);
            this.textEdit父亲职业.Name = "textEdit父亲职业";
            this.textEdit父亲职业.Size = new System.Drawing.Size(146, 20);
            this.textEdit父亲职业.StyleController = this.layoutControl1;
            this.textEdit父亲职业.TabIndex = 11;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(482, 118);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit联系电话.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(306, 18);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 9;
            // 
            // textEdit住址
            // 
            this.textEdit住址.Location = new System.Drawing.Point(96, 118);
            this.textEdit住址.Name = "textEdit住址";
            this.textEdit住址.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit住址.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit住址.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit住址.Properties.ReadOnly = true;
            this.textEdit住址.Size = new System.Drawing.Size(304, 18);
            this.textEdit住址.StyleController = this.layoutControl1;
            this.textEdit住址.TabIndex = 8;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(482, 96);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(306, 18);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 7;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(96, 96);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit性别.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit性别.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(304, 18);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 7;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(482, 74);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(306, 18);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 6;
            // 
            // textEdit儿童姓名
            // 
            this.textEdit儿童姓名.Location = new System.Drawing.Point(96, 74);
            this.textEdit儿童姓名.Name = "textEdit儿童姓名";
            this.textEdit儿童姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit儿童姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童姓名.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit儿童姓名.Properties.ReadOnly = true;
            this.textEdit儿童姓名.Size = new System.Drawing.Size(304, 18);
            this.textEdit儿童姓名.StyleController = this.layoutControl1;
            this.textEdit儿童姓名.TabIndex = 6;
            // 
            // textEdit儿童档案号
            // 
            this.textEdit儿童档案号.Location = new System.Drawing.Point(482, 52);
            this.textEdit儿童档案号.Name = "textEdit儿童档案号";
            this.textEdit儿童档案号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.textEdit儿童档案号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童档案号.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit儿童档案号.Properties.ReadOnly = true;
            this.textEdit儿童档案号.Size = new System.Drawing.Size(306, 18);
            this.textEdit儿童档案号.StyleController = this.layoutControl1;
            this.textEdit儿童档案号.TabIndex = 5;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(96, 52);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Size = new System.Drawing.Size(304, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 4;
            // 
            // comboBoxEdit母亲姓名
            // 
            this.comboBoxEdit母亲姓名.Location = new System.Drawing.Point(87, 175);
            this.comboBoxEdit母亲姓名.Name = "comboBoxEdit母亲姓名";
            this.comboBoxEdit母亲姓名.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit母亲姓名.Size = new System.Drawing.Size(118, 20);
            this.comboBoxEdit母亲姓名.StyleController = this.layoutControl1;
            this.comboBoxEdit母亲姓名.TabIndex = 14;
            this.comboBoxEdit母亲姓名.SelectedIndexChanged += new System.EventHandler(this.comboBox母亲姓名_SelectedIndexChanged);
            // 
            // comboBoxEdit父亲姓名
            // 
            this.comboBoxEdit父亲姓名.Location = new System.Drawing.Point(87, 151);
            this.comboBoxEdit父亲姓名.Name = "comboBoxEdit父亲姓名";
            this.comboBoxEdit父亲姓名.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit父亲姓名.Size = new System.Drawing.Size(118, 20);
            this.comboBoxEdit父亲姓名.StyleController = this.layoutControl1;
            this.comboBoxEdit父亲姓名.TabIndex = 10;
            this.comboBoxEdit父亲姓名.SelectedIndexChanged += new System.EventHandler(this.comboBox父亲姓名_SelectedIndexChanged);
            // 
            // dateEdit父出生日期
            // 
            this.dateEdit父出生日期.EditValue = null;
            this.dateEdit父出生日期.Location = new System.Drawing.Point(666, 151);
            this.dateEdit父出生日期.Name = "dateEdit父出生日期";
            this.dateEdit父出生日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit父出生日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit父出生日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit父出生日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit父出生日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit父出生日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit父出生日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit父出生日期.Size = new System.Drawing.Size(121, 20);
            this.dateEdit父出生日期.StyleController = this.layoutControl1;
            this.dateEdit父出生日期.TabIndex = 13;
            // 
            // dateEdit母出生日期
            // 
            this.dateEdit母出生日期.EditValue = null;
            this.dateEdit母出生日期.Location = new System.Drawing.Point(666, 175);
            this.dateEdit母出生日期.Name = "dateEdit母出生日期";
            this.dateEdit母出生日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit母出生日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit母出生日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit母出生日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit母出生日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit母出生日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit母出生日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit母出生日期.Size = new System.Drawing.Size(121, 20);
            this.dateEdit母出生日期.StyleController = this.layoutControl1;
            this.dateEdit母出生日期.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItemHeader,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.emptySpaceItem4,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(804, 427);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItemHeader
            // 
            this.emptySpaceItemHeader.AllowHotTrack = false;
            this.emptySpaceItemHeader.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItemHeader.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(81)))), ((int)(((byte)(93)))));
            this.emptySpaceItemHeader.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItemHeader.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItemHeader.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItemHeader.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItemHeader.CustomizationFormText = "更新儿童基本信息";
            this.emptySpaceItemHeader.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItemHeader.MaxSize = new System.Drawing.Size(0, 36);
            this.emptySpaceItemHeader.MinSize = new System.Drawing.Size(10, 36);
            this.emptySpaceItemHeader.Name = "emptySpaceItemHeader";
            this.emptySpaceItemHeader.Size = new System.Drawing.Size(784, 36);
            this.emptySpaceItemHeader.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItemHeader.Text = "更新儿童基本信息";
            this.emptySpaceItemHeader.TextSize = new System.Drawing.Size(91, 0);
            this.emptySpaceItemHeader.TextVisible = true;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem卡号,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 36);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(784, 98);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem卡号
            // 
            this.layoutControlItem卡号.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem卡号.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem卡号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem卡号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem卡号.Control = this.textEdit卡号;
            this.layoutControlItem卡号.CustomizationFormText = "卡号：";
            this.layoutControlItem卡号.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem卡号.Name = "layoutControlItem卡号";
            this.layoutControlItem卡号.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem卡号.Size = new System.Drawing.Size(386, 22);
            this.layoutControlItem卡号.Text = "卡号：";
            this.layoutControlItem卡号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem卡号.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem卡号.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit儿童姓名;
            this.layoutControlItem3.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 22);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem3.Size = new System.Drawing.Size(386, 22);
            this.layoutControlItem3.Text = "儿童姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit性别;
            this.layoutControlItem4.CustomizationFormText = "性别：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem4.Size = new System.Drawing.Size(386, 22);
            this.layoutControlItem4.Text = "性别：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit住址;
            this.layoutControlItem7.CustomizationFormText = "住址：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem7.Size = new System.Drawing.Size(386, 22);
            this.layoutControlItem7.Text = "住址：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话：";
            this.layoutControlItem8.Location = new System.Drawing.Point(386, 66);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem8.Size = new System.Drawing.Size(388, 22);
            this.layoutControlItem8.Text = "联系电话：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(386, 44);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem6.Size = new System.Drawing.Size(388, 22);
            this.layoutControlItem6.Text = "出生日期：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(386, 22);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem5.Size = new System.Drawing.Size(388, 22);
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit儿童档案号;
            this.layoutControlItem2.CustomizationFormText = "儿童档案号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem2.Size = new System.Drawing.Size(388, 22);
            this.layoutControlItem2.Text = "儿童档案号：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem父亲姓名,
            this.layoutControlItem母亲姓名,
            this.layoutControlItem母亲职业,
            this.layoutControlItem父亲职业,
            this.layoutControlItem父亲联系电话,
            this.layoutControlItem母亲联系电话,
            this.layoutControlItem母亲出生日期,
            this.layoutControlItem父亲出生日期});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 134);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(784, 58);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem父亲姓名
            // 
            this.layoutControlItem父亲姓名.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父亲姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父亲姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父亲姓名.Control = this.comboBoxEdit父亲姓名;
            this.layoutControlItem父亲姓名.CustomizationFormText = "父亲姓名：";
            this.layoutControlItem父亲姓名.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem父亲姓名.Name = "layoutControlItem父亲姓名";
            this.layoutControlItem父亲姓名.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem父亲姓名.Text = "父亲姓名：";
            this.layoutControlItem父亲姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父亲姓名.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父亲姓名.TextToControlDistance = 5;
            // 
            // layoutControlItem母亲姓名
            // 
            this.layoutControlItem母亲姓名.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem母亲姓名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母亲姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母亲姓名.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem母亲姓名.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母亲姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母亲姓名.Control = this.comboBoxEdit母亲姓名;
            this.layoutControlItem母亲姓名.CustomizationFormText = "母亲姓名：";
            this.layoutControlItem母亲姓名.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem母亲姓名.Name = "layoutControlItem母亲姓名";
            this.layoutControlItem母亲姓名.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem母亲姓名.Text = "母亲姓名：";
            this.layoutControlItem母亲姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母亲姓名.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母亲姓名.TextToControlDistance = 5;
            // 
            // layoutControlItem母亲职业
            // 
            this.layoutControlItem母亲职业.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem母亲职业.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母亲职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母亲职业.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem母亲职业.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母亲职业.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母亲职业.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem母亲职业.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem母亲职业.Control = this.textEdit母亲职业;
            this.layoutControlItem母亲职业.CustomizationFormText = "职业：";
            this.layoutControlItem母亲职业.Location = new System.Drawing.Point(192, 24);
            this.layoutControlItem母亲职业.Name = "layoutControlItem母亲职业";
            this.layoutControlItem母亲职业.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem母亲职业.Text = "职业：";
            this.layoutControlItem母亲职业.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母亲职业.TextSize = new System.Drawing.Size(39, 14);
            this.layoutControlItem母亲职业.TextToControlDistance = 5;
            // 
            // layoutControlItem父亲职业
            // 
            this.layoutControlItem父亲职业.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem父亲职业.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父亲职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父亲职业.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem父亲职业.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父亲职业.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父亲职业.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem父亲职业.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem父亲职业.Control = this.textEdit父亲职业;
            this.layoutControlItem父亲职业.CustomizationFormText = "职业：";
            this.layoutControlItem父亲职业.Location = new System.Drawing.Point(192, 0);
            this.layoutControlItem父亲职业.Name = "layoutControlItem父亲职业";
            this.layoutControlItem父亲职业.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem父亲职业.Text = "职业：";
            this.layoutControlItem父亲职业.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父亲职业.TextSize = new System.Drawing.Size(39, 14);
            this.layoutControlItem父亲职业.TextToControlDistance = 5;
            // 
            // layoutControlItem父亲联系电话
            // 
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父亲联系电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父亲联系电话.Control = this.textEdit父联系电话;
            this.layoutControlItem父亲联系电话.CustomizationFormText = "联系电话：";
            this.layoutControlItem父亲联系电话.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem父亲联系电话.Name = "layoutControlItem父亲联系电话";
            this.layoutControlItem父亲联系电话.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem父亲联系电话.Text = "联系电话：";
            this.layoutControlItem父亲联系电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父亲联系电话.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父亲联系电话.TextToControlDistance = 5;
            // 
            // layoutControlItem母亲联系电话
            // 
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母亲联系电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母亲联系电话.Control = this.textEdit母联系电话;
            this.layoutControlItem母亲联系电话.CustomizationFormText = "联系电话：";
            this.layoutControlItem母亲联系电话.Location = new System.Drawing.Point(386, 24);
            this.layoutControlItem母亲联系电话.Name = "layoutControlItem母亲联系电话";
            this.layoutControlItem母亲联系电话.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem母亲联系电话.Text = "联系电话：";
            this.layoutControlItem母亲联系电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母亲联系电话.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母亲联系电话.TextToControlDistance = 5;
            // 
            // layoutControlItem母亲出生日期
            // 
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母亲出生日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母亲出生日期.Control = this.dateEdit母出生日期;
            this.layoutControlItem母亲出生日期.CustomizationFormText = "出生日期：";
            this.layoutControlItem母亲出生日期.Location = new System.Drawing.Point(579, 24);
            this.layoutControlItem母亲出生日期.Name = "layoutControlItem母亲出生日期";
            this.layoutControlItem母亲出生日期.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem母亲出生日期.Text = "出生日期：";
            this.layoutControlItem母亲出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem母亲出生日期.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem母亲出生日期.TextToControlDistance = 5;
            // 
            // layoutControlItem父亲出生日期
            // 
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem父亲出生日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem父亲出生日期.Control = this.dateEdit父出生日期;
            this.layoutControlItem父亲出生日期.CustomizationFormText = "出生日期：";
            this.layoutControlItem父亲出生日期.Location = new System.Drawing.Point(579, 0);
            this.layoutControlItem父亲出生日期.Name = "layoutControlItem父亲出生日期";
            this.layoutControlItem父亲出生日期.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem父亲出生日期.Text = "出生日期：";
            this.layoutControlItem父亲出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem父亲出生日期.TextSize = new System.Drawing.Size(65, 14);
            this.layoutControlItem父亲出生日期.TextToControlDistance = 5;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 354);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(784, 53);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem邮政编码,
            this.layoutControlItem建档单位,
            this.layoutControlItem托幼机构,
            this.layoutControlItem托幼机构电话,
            this.layoutControlItem建档单位电话,
            this.layoutControlItem母亲身份证号});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 192);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(784, 82);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem邮政编码
            // 
            this.layoutControlItem邮政编码.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem邮政编码.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem邮政编码.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem邮政编码.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem邮政编码.Control = this.textEdit邮政编码;
            this.layoutControlItem邮政编码.CustomizationFormText = "邮政编码：";
            this.layoutControlItem邮政编码.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem邮政编码.Name = "layoutControlItem邮政编码";
            this.layoutControlItem邮政编码.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem邮政编码.Text = "邮政编码：";
            this.layoutControlItem邮政编码.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem邮政编码.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem邮政编码.TextToControlDistance = 5;
            // 
            // layoutControlItem建档单位
            // 
            this.layoutControlItem建档单位.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem建档单位.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem建档单位.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem建档单位.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem建档单位.Control = this.textEdit建档单位;
            this.layoutControlItem建档单位.CustomizationFormText = "建档单位：";
            this.layoutControlItem建档单位.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem建档单位.Name = "layoutControlItem建档单位";
            this.layoutControlItem建档单位.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem建档单位.Text = "建档单位：";
            this.layoutControlItem建档单位.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem建档单位.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem建档单位.TextToControlDistance = 5;
            // 
            // layoutControlItem托幼机构
            // 
            this.layoutControlItem托幼机构.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem托幼机构.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem托幼机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem托幼机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem托幼机构.Control = this.textEdit托幼机构;
            this.layoutControlItem托幼机构.CustomizationFormText = "托幼机构：";
            this.layoutControlItem托幼机构.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem托幼机构.Name = "layoutControlItem托幼机构";
            this.layoutControlItem托幼机构.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem托幼机构.Text = "托幼机构：";
            this.layoutControlItem托幼机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem托幼机构.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem托幼机构.TextToControlDistance = 5;
            // 
            // layoutControlItem托幼机构电话
            // 
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem托幼机构电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem托幼机构电话.Control = this.textEdit托幼机构电话;
            this.layoutControlItem托幼机构电话.CustomizationFormText = "电话：";
            this.layoutControlItem托幼机构电话.Location = new System.Drawing.Point(387, 48);
            this.layoutControlItem托幼机构电话.Name = "layoutControlItem托幼机构电话";
            this.layoutControlItem托幼机构电话.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem托幼机构电话.Text = "电话：";
            this.layoutControlItem托幼机构电话.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem建档单位电话
            // 
            this.layoutControlItem建档单位电话.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem建档单位电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem建档单位电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem建档单位电话.Control = this.textEdit建档单位电话;
            this.layoutControlItem建档单位电话.CustomizationFormText = "电话：";
            this.layoutControlItem建档单位电话.Location = new System.Drawing.Point(387, 24);
            this.layoutControlItem建档单位电话.Name = "layoutControlItem建档单位电话";
            this.layoutControlItem建档单位电话.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem建档单位电话.Text = "电话：";
            this.layoutControlItem建档单位电话.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlItem母亲身份证号
            // 
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母亲身份证号.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母亲身份证号.Control = this.textEdit母亲身份证号;
            this.layoutControlItem母亲身份证号.CustomizationFormText = "母亲身份证号：";
            this.layoutControlItem母亲身份证号.Location = new System.Drawing.Point(387, 0);
            this.layoutControlItem母亲身份证号.Name = "layoutControlItem母亲身份证号";
            this.layoutControlItem母亲身份证号.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem母亲身份证号.Text = "母亲身份证号：";
            this.layoutControlItem母亲身份证号.TextSize = new System.Drawing.Size(91, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29,
            this.layoutControlItem26,
            this.layoutControlItem发生时间,
            this.layoutControlItem24,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem25});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 274);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(784, 80);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem29.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem29.Control = this.textEdit当前所属机构;
            this.layoutControlItem29.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(774, 22);
            this.layoutControlItem29.Text = "当前所属机构：";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem26.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit创建机构;
            this.layoutControlItem26.CustomizationFormText = "创建机构：";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem26.Text = "创建机构：";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem发生时间
            // 
            this.layoutControlItem发生时间.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem发生时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem发生时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem发生时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem发生时间.Control = this.dateEdit发生时间;
            this.layoutControlItem发生时间.CustomizationFormText = "发生时间：";
            this.layoutControlItem发生时间.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem发生时间.Name = "layoutControlItem发生时间";
            this.layoutControlItem发生时间.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem发生时间.Text = "发生时间：";
            this.layoutControlItem发生时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem发生时间.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem发生时间.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem24.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem24.Control = this.textEdit录入时间;
            this.layoutControlItem24.CustomizationFormText = "录入时间：";
            this.layoutControlItem24.Location = new System.Drawing.Point(279, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem24.Text = "录入时间：";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem27.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem27.Control = this.textEdit录入人;
            this.layoutControlItem27.CustomizationFormText = "录入人：";
            this.layoutControlItem27.Location = new System.Drawing.Point(279, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem27.Text = "录入人：";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem28.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem28.Control = this.textEdit最近修改人;
            this.layoutControlItem28.CustomizationFormText = "最近修改人：";
            this.layoutControlItem28.Location = new System.Drawing.Point(515, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(259, 24);
            this.layoutControlItem28.Text = "最近修改人：";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(84, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(244)))), ((int)(((byte)(246)))));
            this.layoutControlItem25.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem25.Control = this.textEdit最近修改时间;
            this.layoutControlItem25.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem25.Location = new System.Drawing.Point(515, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(259, 24);
            this.layoutControlItem25.Text = "最近更新时间：";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(84, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // UC儿童基本信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC儿童基本信息";
            this.Size = new System.Drawing.Size(804, 462);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit发生时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit发生时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit托幼机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit建档单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit邮政编码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母亲职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父亲职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit母亲姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit父亲姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit父出生日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit父出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit母出生日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit母出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem卡号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲职业)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲职业)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲联系电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲联系电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem父亲出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem邮政编码)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem托幼机构电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem建档单位电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母亲身份证号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem发生时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtn保存;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemHeader;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem卡号;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit textEdit儿童档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit textEdit儿童姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit textEdit住址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit textEdit父联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit父亲职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父亲姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父亲联系电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父亲职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem父亲出生日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母亲姓名;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit textEdit母联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit母亲职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母亲职业;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母亲联系电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母亲出生日期;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit母亲姓名;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit父亲姓名;
        private DevExpress.XtraEditors.DateEdit dateEdit父出生日期;
        private DevExpress.XtraEditors.DateEdit dateEdit母出生日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.TextEdit textEdit邮政编码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem邮政编码;
        private DevExpress.XtraEditors.TextEdit textEdit母亲身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母亲身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit建档单位;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem建档单位;
        private DevExpress.XtraEditors.TextEdit textEdit托幼机构;
        private DevExpress.XtraEditors.TextEdit textEdit建档单位电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem建档单位电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem托幼机构;
        private DevExpress.XtraEditors.TextEdit textEdit托幼机构电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem托幼机构电话;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.DateEdit dateEdit发生时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem发生时间;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改时间;
        private DevExpress.XtraEditors.TextEdit textEdit录入时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.TextEdit textEdit录入人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
    }
}
