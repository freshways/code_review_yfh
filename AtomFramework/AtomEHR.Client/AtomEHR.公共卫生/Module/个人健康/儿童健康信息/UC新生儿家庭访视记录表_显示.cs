﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Business.Security;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC新生儿家庭访视记录表_显示 : UserControlBase
    {
        private string m_grdabh = null;
        private string m_prgid = null; //当前所属机构

        //包含儿童基本信息
        private DataSet m_ds新生儿 = null;
        private DataSet m_ds儿童基本信息 = null;
        private bll儿童新生儿访视记录 m_bll新生儿 = new bll儿童新生儿访视记录();
        private bll儿童基本信息 m_bll儿童基本信息 = new bll儿童基本信息();
        public UC新生儿家庭访视记录表_显示(string grdabh)
        {
            InitializeComponent();
            m_grdabh = grdabh;
            
            BindData();
        }

        #region 为控件绑定数据
        private void BindData()
        {
            //母亲妊娠期患病情况
            //DataView dv母妊娠期 = new DataView(DataDictCache.Cache.t常用字典);
            //dv母妊娠期.RowFilter = "P_FUN_CODE = 'rcqhbqk'";
            //dv母妊娠期.Sort = "P_CODE";
            //util.ControlsHelper.BindComboxData(dv母妊娠期.ToTable(), comboBoxEdit母妊娠患病情况, "P_CODE", "P_DESC");
            //util.ControlsHelper.SetComboxData("", comboBoxEdit母妊娠患病情况);
            BindDataForComboBox("rcqhbqk", comboBoxEdit母妊娠患病情况);

            //是否有畸形
            //DataView dv畸形 = new DataView(DataDictCache.Cache.t常用字典);
            //dv畸形.RowFilter = "P_FUN_CODE = 'yw_youwu'";
            //dv畸形.Sort = "P_CODE";
            //util.ControlsHelper.BindComboxData(dv畸形.ToTable(), comboBoxEdit是否畸形, "P_CODE", "P_DESC");
            //util.ControlsHelper.SetComboxData("", comboBoxEdit是否畸形);
            BindDataForComboBox("yw_youwu", comboBoxEdit是否畸形);

            //新生儿窒息
            BindDataForComboBox("yw_youwu", comboBoxEdit新生儿窒息);

            //新生儿听力筛查
            BindDataForComboBox("tl_tingli", comboBoxEdit听力筛查);

            //喂养方式
            BindDataForComboBox("wyfs", comboBoxEdit喂养方式);

            //呕吐
            BindDataForComboBox("yw_youwu", comboBoxEdit呕吐);

            //大便
            BindDataForComboBox("dabian", comboBoxEdit大便);

            //黄疸部位
            BindDataForComboBox("hd_location", comboBoxEdit黄疸部位);

            //前卤情况
            BindDataForComboBox("qxzk", comboBoxEdit前卤情况);

            //眼外观
            BindDataForComboBox("ywyc", comboBoxEdit眼外观);

            //四肢活动度
            BindDataForComboBox("ywyc", comboBoxEdit四肢活动度);

            //耳外观
            BindDataForComboBox("ywyc", comboBoxEdit耳外观);

            //颈部包块
            BindDataForComboBox("yw_youwu", comboBoxEdit颈部包块);

            //鼻
            BindDataForComboBox("ywyc", comboBoxEdit鼻);

            //口腔
            BindDataForComboBox("ywyc", comboBoxEdit口腔);

            //肛门
            BindDataForComboBox("ywyc", comboBoxEdit肛门);

            //心肺
            BindDataForComboBox("ywyc", comboBoxEdit心肺);

            //外生殖器官
            BindDataForComboBox("ywyc", comboBoxEdit外生殖器);

            //腹部
            BindDataForComboBox("ywyc", comboBoxEdit腹部);

            //脊柱
            BindDataForComboBox("ywyc", comboBoxEdit脊柱);

            //脐带
            BindDataForComboBox("jd_jidai", comboBoxEdit脐带);

            //转诊
            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);

            //胸部
            BindDataForComboBox("ywyc", comboBoxEdit胸部);

            //结果
            BindDataForComboBox("sfdw", comboBoxEdit是否到位);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }
        #endregion

        private void UC新生儿家庭访视记录表_显示_Load(object sender, EventArgs e)
        {
            m_ds儿童基本信息 = m_bll儿童基本信息.Get儿童基本信息ByGrdabh(m_grdabh);
            m_ds新生儿 = m_bll新生儿.GetBusinessByGrdabh2(m_grdabh);
            m_prgid = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.所属机构].ToString();

            //显示儿童基本信息
            textEdit儿童档案号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();

            string str姓名 = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = DESEncrypt.DES解密(str姓名);
            textEdit身份证号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号].ToString();
            textEdit性别.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别].ToString(); ;
            textEdit出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();
            textEdit住址.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0]["居住地址"].ToString();
            textEdit联系电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.联系电话].ToString();

            textEdit父姓名.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲姓名].ToString();
            textEdit父职业.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲工作单位].ToString();
            textEdit父联系电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲联系电话].ToString();
            textEdit父出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲出生日期].ToString();

            textEdit母姓名.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲姓名].ToString();
            textEdit母职业.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲工作单位].ToString();
            textEdit母联系电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲联系电话].ToString();
            textEdit母出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲出生日期].ToString();
            textEdit母亲身份证号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲身份证号].ToString();
            //考核项，缺项
            string str缺项 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.缺项].ToString();
            string str完整度 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.完整度].ToString();
            labelControlHeader.Text = "考核项：40     缺项：" + str缺项 + " 完整度：" + str完整度 + "% ";
            textEdit卡号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.卡号].ToString();
            textEdit出生孕周.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生孕周].ToString();

            //修改时随访日期，下次随访日期不允许在修改
            dateEdit随访日期.Properties.ReadOnly = true;
            dateEdit随访日期.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            dateEdit下次随访日期.Properties.ReadOnly = true;
            dateEdit下次随访日期.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));

            //修改的情况下，将值赋给控件

            #region    新版本添加
            textEdit父亲身份证号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.父亲身份证号].ToString();
            //textEdit母亲身份证号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.母亲身份证号].ToString();
            string str胸部 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.胸部].ToString();
            string str胸部其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.胸部其他].ToString();
            SetComboBoxAndTextEditValue(str胸部, comboBoxEdit胸部, "99", textEdit胸部其他, str胸部其他);
            SetLayoutControlItemFontColor(layoutControlItem胸部, str胸部, "99", str胸部其他);
            textEdit家长签名.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.家长签名].ToString();
            SetLayoutControlItemFontColor(layoutControlItem家长签名, textEdit家长签名.Text);
            #endregion

            textEdit卡号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.卡号].ToString();
            
            textEdit出生孕周.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生孕周].ToString();
            SetLayoutControlItemFontColor(layoutControlItem出生孕周, textEdit出生孕周.Text);

            string str母亲妊娠 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况].ToString();
            //util.ControlsHelper.SetComboxData(str母亲妊娠, comboBoxEdit母妊娠患病情况);
            //if (str母亲妊娠.Equals("99"))
            //{
            //    textEdit母妊娠患病其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他].ToString();
            //}
            string str母亲妊娠其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他].ToString();
            SetComboBoxAndTextEditValue(str母亲妊娠, comboBoxEdit母妊娠患病情况, "99", textEdit母妊娠患病其他, str母亲妊娠其他);
            SetLayoutControlItemFontColor(layoutControlItem母妊娠疾病, str母亲妊娠, "99", textEdit母妊娠患病其他.Text);

            string str是否畸形 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.是否有畸型].ToString();
            string str是否畸形其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.是否有畸型其他].ToString();
            SetComboBoxAndTextEditValue(str是否畸形, comboBoxEdit是否畸形, "1", textEdit是否畸形其他, str是否畸形其他);
            SetLayoutControlItemFontColor(layoutControlItem是否畸形, str是否畸形, "1", textEdit是否畸形其他.Text);

            textEdit助产机构.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.助产机构名称].ToString();
            SetLayoutControlItemFontColor(layoutControlItem助产机构, textEdit助产机构.Text);
            char[] c分隔符 = { ',' };

            #region 设定"出生情况"
            string str出生情况 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况].ToString();
            string[] str出生情况Values = str出生情况.Split(c分隔符);
            for (int index = 0; index < str出生情况Values.Length; index++)
            {
                switch (str出生情况Values[index])
                {
                    case "1":
                        checkEdit顺产.Checked = true;
                        break;
                    case "2":
                        checkEdit头吸.Checked = true;
                        break;
                    case "3":
                        checkEdit产钳.Checked = true;
                        break;
                    case "4":
                        checkEdit剖宫.Checked = true;
                        break;
                    case "5":
                        checkEdit双多胎.Checked = true;
                        break;
                    case "6":
                        checkEdit臀位.Checked = true;
                        break;
                    case "99":
                        checkEdit其他.Checked = true;
                        textEdit出生情况其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况其他].ToString();
                        break;
                    default:
                        break;
                }
            }
            if (checkEdit顺产.Checked || checkEdit头吸.Checked || checkEdit产钳.Checked || checkEdit剖宫.Checked || checkEdit双多胎.Checked
                || checkEdit臀位.Checked || (checkEdit其他.Checked && !(string.IsNullOrWhiteSpace(textEdit出生情况其他.Text))))
            {
                layoutControlItem出生情况.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                layoutControlItem出生情况.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion

            #region 设定"新生儿窒息"及"Apgar评分"
            string str新生儿窒息 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿窒息].ToString();
            util.ControlsHelper.SetComboxData(str新生儿窒息, comboBoxEdit新生儿窒息);
            SetLayoutControlItemFontColor(layoutControlItem新生儿窒息, str新生儿窒息);

            if (str新生儿窒息.Equals("1"))
            {
                string strApgar评分 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分].ToString();
                string[] strApgar评分Values = strApgar评分.Split(c分隔符);
                for (int index = 0; index < strApgar评分Values.Length; index++)
                {
                    switch (strApgar评分Values[index])
                    {
                        case "1":
                            checkEdit1分钟.Checked = true;
                            textEdit1分钟得分.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分1].ToString();
                            break;
                        case "2":
                            checkEdit5分钟.Checked = true;
                            textEdit5分钟得分.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分5].ToString();
                            break;
                        case "3":
                            checkEditApgar不详.Checked = true;
                            break;
                        default:
                            break;
                    }
                }
            }
            #endregion

            string str听力筛查 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿听力筛查].ToString();
            util.ControlsHelper.SetComboxData(str听力筛查, comboBoxEdit听力筛查);
            SetLayoutControlItemFontColor(layoutControlItem听力筛查, str听力筛查);

            #region 新生儿疾病筛查
            string str疾病筛查 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查].ToString();
            string[] str疾病筛查Values = str疾病筛查.Split(c分隔符);
            for (int index = 0; index < str疾病筛查Values.Length; index++)
            {
                if (str疾病筛查Values[index].Equals("0"))
                {
                    checkEdit疾病筛查无.Checked = true;
                    break;
                }
                else if (str疾病筛查Values[index].Equals("1"))
                {
                    checkEdit疾病筛查甲低.Checked = true;
                }
                else if (str疾病筛查Values[index].Equals("2"))
                {
                    checkEdit疾病筛查苯丙酮尿.Checked = true;
                }
                else if (str疾病筛查Values[index].Equals("3"))
                {
                    checkEdit疾病_先天肾病.Checked = true;
                }
                else if (str疾病筛查Values[index].Equals("4"))
                {
                    checkEdit疾病_葡萄糖症.Checked = true;
                }
                else if (str疾病筛查Values[index].Equals("99"))
                {
                    checkEdit疾病_其他.Checked = true;
                    textEdit疾病_其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他].ToString();
                }
                else
                { }
            }

            if(checkEdit疾病筛查无.Checked || checkEdit疾病筛查甲低.Checked || checkEdit疾病筛查苯丙酮尿.Checked || checkEdit疾病_先天肾病.Checked 
                || checkEdit疾病_葡萄糖症.Checked || (checkEdit疾病_其他.Checked && !(string.IsNullOrWhiteSpace(textEdit疾病_其他.Text))))
            {
                layoutControlItem疾病筛查.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                layoutControlItem疾病筛查.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion

            textEdit出生体重.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿出生体重].ToString();
            SetLayoutControlItemFontColor(layoutControlItem出生体重, textEdit出生体重.Text);

            textEdit目前体重.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.目前体重].ToString();
            SetLayoutControlItemFontColor(layoutControlItem目前体重, textEdit目前体重.Text);

            textEdit出生身长.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生身长].ToString();
            SetLayoutControlItemFontColor(layoutControlItem出生身长, textEdit出生身长.Text);

            string str喂养方式 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.喂养方式].ToString();
            util.ControlsHelper.SetComboxData(str喂养方式, comboBoxEdit喂养方式);
            SetLayoutControlItemFontColor(layoutControlItem喂养方式, str喂养方式);

            textEdit吃奶量.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶量].ToString();

            textEdit吃奶次数.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶次数].ToString();

            string str呕吐 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呕吐].ToString();
            util.ControlsHelper.SetComboxData(str呕吐, comboBoxEdit呕吐);

            string str大便 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便].ToString();
            util.ControlsHelper.SetComboxData(str大便, comboBoxEdit大便);

            textEdit大便次数.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便次数].ToString();

            textEdit体温.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.体温].ToString();
            SetLayoutControlItemFontColor(layoutControlItem体温, textEdit体温.Text);

            textEdit呼吸频率.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呼吸频率].ToString();
            SetLayoutControlItemFontColor(layoutControlItem呼吸频率, textEdit呼吸频率.Text);

            textEdit脉率.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脉率].ToString();
            SetLayoutControlItemFontColor(layoutControlItem脉率, textEdit脉率.Text);

            #region 设置"面色"
            string str面色 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色].ToString();
            string[] str面色Values = str面色.Split(c分隔符);
            for (int index = 0; index < str面色Values.Length; index++)
            {
                if (str面色Values[index].Equals("1"))
                {
                    checkEdit面色未检.Checked = true;
                    break;
                }
                else if (str面色Values[index].Equals("2"))
                {
                    checkEdit面色红润.Checked = true;
                }
                else if (str面色Values[index].Equals("3"))
                {
                    checkEdit面色黄染.Checked = true;
                }
                else if (str面色Values[index].Equals("99"))
                {
                    checkEdit面色其他.Checked = true;
                    textEdit面色其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色其他].ToString();
                }
                else
                { }
            }
            if(checkEdit面色未检.Checked || checkEdit面色红润.Checked || checkEdit面色黄染.Checked 
                || (checkEdit面色其他.Checked && !(string.IsNullOrWhiteSpace(textEdit面色其他.Text))))
            {
                layoutControlItem面色.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                layoutControlItem面色.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion

            string str黄疸部位 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.黄疸部位].ToString();
            util.ControlsHelper.SetComboxData(str黄疸部位, comboBoxEdit黄疸部位);
            SetLayoutControlItemFontColor(layoutControlItem黄疸部位, str黄疸部位);

            textEdit前卤1.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟1].ToString();
            textEdit前卤2.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟2].ToString();
            if(string.IsNullOrWhiteSpace(textEdit前卤1.Text) || string.IsNullOrWhiteSpace(textEdit前卤2.Text))
            {
                layoutControlItem前卤.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem前卤.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            string str前卤情况 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟状况].ToString();
            string str前卤情况其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟状况其他].ToString();
            SetComboBoxAndTextEditValue(str前卤情况, comboBoxEdit前卤情况, "99", textEdit前卤情况其他, str前卤情况其他);
            SetLayoutControlItemFontColor(layoutControlItem前卤情况, str前卤情况, "99", str前卤情况其他);

            string str眼外观 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.眼外观].ToString();
            string str眼外观其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.眼外观其他].ToString();
            SetComboBoxAndTextEditValue(str眼外观, comboBoxEdit眼外观, "99", textEdit眼外观其他, str眼外观其他);
            SetLayoutControlItemFontColor(layoutControlItem眼外观, str眼外观, "99", str眼外观其他);

            string str四肢活动度 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.四肢活动度].ToString();
            string str四肢活动度其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.四肢活动度其他].ToString();
            SetComboBoxAndTextEditValue(str四肢活动度, comboBoxEdit四肢活动度, "99", textEdit四肢活动度其他, str四肢活动度其他);
            SetLayoutControlItemFontColor(layoutControlItem四肢活动度, str四肢活动度, "99", str四肢活动度其他);

            string str耳外观 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.耳外观].ToString();
            string str耳外观其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.耳外观其他].ToString();
            SetComboBoxAndTextEditValue(str耳外观, comboBoxEdit耳外观, "99", textEdit耳外观其他, str耳外观其他);
            SetLayoutControlItemFontColor(layoutControlItem耳外观, str耳外观, "99", str耳外观其他);

            string str颈部包块 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.颈部包块].ToString();
            string str颈部包块其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.颈部包块其他].ToString();
            SetComboBoxAndTextEditValue(str颈部包块, comboBoxEdit颈部包块, "1", textEdit颈部包块其他, str颈部包块其他);
            SetLayoutControlItemFontColor(layoutControlItem颈部包块, str颈部包块, "1", str颈部包块其他);

            string str鼻 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.鼻].ToString();
            string str鼻其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.鼻其他].ToString();
            SetComboBoxAndTextEditValue(str鼻, comboBoxEdit鼻, "99", textEdit鼻其他, str鼻其他);
            SetLayoutControlItemFontColor(layoutControlItem鼻, str鼻, "99", str鼻其他);

            #region 设置"皮肤"
            string str皮肤 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤].ToString();
            string[] str皮肤Values = str皮肤.Split(c分隔符);
            for (int index = 0; index < str皮肤Values.Length; index++)
            {
                if (str皮肤Values[index].Equals("1"))
                {
                    checkEdit皮肤未见异常.Checked = true;
                    break;
                }
                else if (str皮肤Values[index].Equals("2"))
                {
                    checkEdit皮肤湿疹.Checked = true;
                }
                else if (str皮肤Values[index].Equals("3"))
                {
                    checkEdit皮肤糜烂.Checked = true;
                }
                else if (str皮肤Values[index].Equals("99"))
                {
                    checkEdit皮肤其他.Checked = true;
                    textEdit皮肤其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤其他].ToString();
                }
                else { }
            }
            if(checkEdit皮肤未见异常.Checked || checkEdit皮肤湿疹.Checked || checkEdit皮肤糜烂.Checked 
                || (checkEdit皮肤其他.Checked && !(string.IsNullOrWhiteSpace(textEdit皮肤其他.Text))))
            {
                layoutControlItem皮肤.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                layoutControlItem皮肤.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion

            string str口腔 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.口腔].ToString();
            string str口腔其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.口腔其他].ToString();
            SetComboBoxAndTextEditValue(str口腔, comboBoxEdit口腔, "99", textEdit口腔其他, str口腔其他);
            SetLayoutControlItemFontColor(layoutControlItem口腔, str口腔, "99", str口腔其他);

            string str肛门 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.肛门].ToString();
            string str肛门其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.肛门其他].ToString();
            SetComboBoxAndTextEditValue(str肛门, comboBoxEdit肛门, "99", textEdit肛门其他, str肛门其他);
            SetLayoutControlItemFontColor(layoutControlItem肛门, str肛门, "99", str肛门其他);

            string str心肺 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.心肺].ToString();
            string str心肺其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.心肺其他].ToString();
            SetComboBoxAndTextEditValue(str心肺, comboBoxEdit心肺, "99", textEdit心肺其他, str心肺其他);
            SetLayoutControlItemFontColor(layoutControlItem心肺, str心肺, "99", str心肺其他);

            string str外生殖器 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.外生殖器官].ToString();
            string str外生殖器其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.外生殖器官其他].ToString();
            SetComboBoxAndTextEditValue(str外生殖器, comboBoxEdit外生殖器, "99", textEdit外生殖器其他, str外生殖器其他);
            SetLayoutControlItemFontColor(layoutControlItem外生殖器官, str外生殖器, "99", str外生殖器其他);

            string str腹部 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.腹部].ToString();
            string str腹部其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.腹部其他].ToString();
            SetComboBoxAndTextEditValue(str腹部, comboBoxEdit腹部, "99", textEdit腹部其他, str腹部其他);
            SetLayoutControlItemFontColor(layoutControlItem腹部, str腹部,"99", str腹部其他);

            string str脊柱 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脊柱].ToString();
            string str脊柱其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脊柱其他].ToString();
            SetComboBoxAndTextEditValue(str脊柱, comboBoxEdit脊柱, "99", textEdit脊柱其他, str脊柱其他);
            SetLayoutControlItemFontColor(layoutControlItem脊柱, str脊柱, "99", str脊柱其他);

            string str脐带 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脐带].ToString();
            string str脐带其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脐带其他].ToString();
            SetComboBoxAndTextEditValue(str脐带, comboBoxEdit脐带, "99", textEdit脐带其他, str脐带其他);
            SetLayoutControlItemFontColor(layoutControlItem脐带, str脐带, "99", str脐带其他);

            string str转诊 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊].ToString();
            util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);
            if (str转诊.Equals("1"))
            {
                textEdit转诊原因.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊原因].ToString();
                textEdit转诊机构.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊机构及科室].ToString();
                textEdit联系人.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系人].ToString();
                textEdit转诊联系电话.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系电话].ToString();
                string str结果 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.结果].ToString();
                util.ControlsHelper.SetComboxData(str结果, comboBoxEdit是否到位);
            }
            if(string.IsNullOrWhiteSpace(str转诊) 
                || (str转诊.Equals("1") && (string.IsNullOrWhiteSpace(textEdit转诊原因.Text) || string.IsNullOrWhiteSpace(textEdit转诊机构.Text)
                || string.IsNullOrWhiteSpace(textEdit联系人.Text) || string.IsNullOrWhiteSpace(textEdit转诊联系电话.Text) || string.IsNullOrWhiteSpace(comboBoxEdit是否到位.EditValue.ToString())
                )))
            {
                layoutControlItem转诊.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem转诊.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            #region 设置"指导"
            string str指导 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导].ToString();
            string[] str指导Values = str指导.Split(c分隔符);
            for (int index = 0; index < str指导Values.Length; index++)
            {
                switch (str指导Values[index])
                {
                    case "1":
                        checkEdit指导科学喂养.Checked = true;
                        break;
                    case "5":
                        checkEdit指导生长发育.Checked = true;
                        break;
                    case "6":
                        checkEdit指导疾病预防.Checked = true;
                        break;
                    case "7":
                        checkEdit指导预防意外.Checked = true;
                        break;
                    case "8":
                        checkEdit指导口腔保健.Checked = true;
                        break;
                    case "9":
                        chk口服维生素D.Checked = true;
                        break;
                    case "100":
                        checkEdit指导其他.Checked = true;
                        textEdit指导其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导其他].ToString();
                        break;
                    default:
                        break;
                }
            }
            if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (checkEdit指导科学喂养.Checked || checkEdit指导生长发育.Checked || checkEdit指导疾病预防.Checked
                || checkEdit指导预防意外.Checked || checkEdit指导口腔保健.Checked || chk口服维生素D.Checked)
            {
                layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion

            dateEdit下次随访日期.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访日期].ToString();
            SetLayoutControlItemFontColor(layoutControlItem下次随访日期, dateEdit下次随访日期.Text);

            textEdit下次随访地点.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访地点].ToString();
            SetLayoutControlItemFontColor(layoutControlItem下次随访地点, textEdit下次随访地点.Text);

            textEdit随访医生.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.随访医生签名].ToString();
            SetLayoutControlItemFontColor(layoutControlItem随访医生, textEdit随访医生.Text);

            dateEdit随访日期.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.发生时间].ToString();
            SetLayoutControlItemFontColor(layoutControlItem随访日期, dateEdit随访日期.Text);

            textEdit录入时间.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建时间].ToString();

            string str录入人 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建人].ToString();

            bllUser blluser = new bllUser();
            textEdit录入人.Text = blluser.Return用户名称(str录入人);

            bll机构信息 bll机构 = new bll机构信息();
            string str创建机构 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建机构].ToString();
            textEdit创建机构.Text = bll机构.Return机构名称(str创建机构);

            string str当前所属机构 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.所属机构].ToString();
            textEdit当前所属机构.Text = bll机构.Return机构名称(str当前所属机构);

            textEdit最近更新时间.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.修改时间].ToString();

            string str修改人 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.修改人].ToString();
            textEdit最近修改人.Text = blluser.Return用户名称(str修改人);

            #region 变色控制
            //Set考核项颜色_new(layoutControl1, null);
            
            #endregion
        }

        private void SetLayoutControlItemFontColor(DevExpress.XtraLayout.LayoutControlItem item, string value)
        {
            if(string.IsNullOrWhiteSpace(value))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        private void SetLayoutControlItemFontColor(DevExpress.XtraLayout.LayoutControlItem item, string value, string other, string otherValue)
        {
            if (string.IsNullOrWhiteSpace(value) || (value.Equals(other) && string.IsNullOrWhiteSpace(otherValue)))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        private void SetComboBoxAndTextEditValue(string value, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit,
                                                 string str其他编码, DevExpress.XtraEditors.TextEdit textEdit, string str其他值)
        {
            util.ControlsHelper.SetComboxData(value, comboBoxEdit);
            if (value.Equals(str其他编码))
            {
                textEdit.Text = str其他值;
            }
            else
            {
                textEdit.Visible = false;
            }
        }

        private void sbtn修改_Click(object sender, EventArgs e)
        {
            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            //if (isallow == false && m_prgid != Loginer.CurrentUser.所属机构)
            if (!isallow)
            {
                if (m_prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }

            UC新生儿家庭访视记录表 ctl = new UC新生儿家庭访视记录表(m_grdabh, UpdateType.Modify, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }

        private void sbtn删除_Click(object sender, EventArgs e)
        {
            if(Msg.AskQuestion("您是否确实要删除新生儿家庭访视记录表吗？"))
            { }
            else
            {
                return;
            }

            ////儿童时不能删除新生儿访视记录表
            //if(new bllCom().GetAgeByBirthDay(textEdit出生日期.Text) > 6)
            //{

            //}

            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            //if (isallow == false && m_prgid != Loginer.CurrentUser.所属机构)
            if (isallow)
            {
                if (m_prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }
            string strBirth = textEdit出生日期.Text;
            string strCurrent = m_bll儿童基本信息.ServiceDateTime;

            try
            {
                DateTime dtBirth = Convert.ToDateTime(strBirth);
                TimeSpan tsp = Convert.ToDateTime(strCurrent) - dtBirth;
                if(tsp.Days > 0 && tsp.Days <= 28)
                {
                    //更新个人健康特征、儿童基本信息表
                    string xcsfsj = dtBirth.AddDays(28).ToString("yyyy-MM-dd");
                    m_bll新生儿.UpdateRelatedInfo(m_grdabh, xcsfsj);
                }
            }
            catch { }//日期转换过程中的异常不做任何处理

            bool bRet = m_bll新生儿.DeleteByGrdabh(m_grdabh);
            if(bRet)
            {
                Msg.ShowInformation("删除成功！");

                //向儿童基本信息画面跳转
                UC儿童基本信息_显示 ctl = new UC儿童基本信息_显示(Common.frm个人健康 as frm个人健康, m_grdabh);
                ctl.Dock = DockStyle.Fill;
                ShowControl(ctl);
            }
            else
            {
                Msg.ShowInformation("删除失败");
            }

        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit指导其他.Checked)
            {
                textEdit指导其他.Enabled = true;
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        private void sbtn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.textEdit儿童档案号.Text.Trim();
            if (!string.IsNullOrEmpty(docNo))
            {
                report新生儿家庭访视记录表 report = new report新生儿家庭访视记录表(docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }
    }
}

