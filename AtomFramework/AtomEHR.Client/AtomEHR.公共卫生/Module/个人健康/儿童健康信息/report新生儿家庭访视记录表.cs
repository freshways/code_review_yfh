﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class report新生儿家庭访视记录表 : DevExpress.XtraReports.UI.XtraReport
    {

        string docNo;
        DataSet _ds新生儿;
        DataSet _ds儿童基本;
        //bll老年人随访 _bll = new bll老年人随访();
        bll儿童新生儿访视记录 _bll = new bll儿童新生儿访视记录();
        bll儿童基本信息 _bll儿童基本 = new bll儿童基本信息();
        public report新生儿家庭访视记录表()
        {
            InitializeComponent();
        }

        public report新生儿家庭访视记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds新生儿 = _bll.GetInfoByFangshi(_docNo, true);
            _ds儿童基本 = _bll儿童基本.GetInfoByErTong(_docNo,true);
            DoBindingDataSource(_ds新生儿,_ds儿童基本);
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="_ds新生儿"></param>
        /// <param name="_ds儿童基本"></param>
        private void DoBindingDataSource(DataSet _ds新生儿,DataSet _ds儿童基本)
        {
            DataTable dt新生儿 = _ds新生儿.Tables[Models.tb_儿童新生儿访视记录.__TableName];
            if (dt新生儿 == null || dt新生儿.Rows.Count == 0) return;

            DataTable dt儿童基本 = _ds儿童基本.Tables[Models.tb_儿童基本信息.__TableName];
            if (dt儿童基本 == null || dt儿童基本.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds新生儿.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;
            //新生儿访视记录绑定数据
            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt姓别.Text = dt儿童基本.Rows[0][tb_儿童基本信息.性别].ToString();
            this.txt身份证号.Text = dt儿童基本.Rows[0][tb_儿童基本信息.身份证号].ToString();
            this.txt家庭住址.Text = dt儿童基本.Rows[0][tb_儿童基本信息.居住地址].ToString();
            this.txt出生日期.Text = Convert.ToDateTime(dt儿童基本.Rows[0][tb_儿童基本信息.出生日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            
            this.txt父亲姓名.Text = dt儿童基本.Rows[0][tb_儿童基本信息.父亲姓名].ToString();
            this.txt母亲姓名.Text = dt儿童基本.Rows[0][tb_儿童基本信息.母亲姓名].ToString();
            this.txt父亲职业.Text = dt儿童基本.Rows[0][tb_儿童基本信息.父亲工作单位].ToString();
            this.txt母亲职业.Text = dt儿童基本.Rows[0][tb_儿童基本信息.母亲工作单位].ToString();
            this.txt父电话.Text = dt儿童基本.Rows[0][tb_儿童基本信息.父亲联系电话].ToString();
            this.txt母电话.Text = dt儿童基本.Rows[0][tb_儿童基本信息.母亲联系电话].ToString();
            this.txt父出生日期.Text = dt儿童基本.Rows[0][tb_儿童基本信息.父亲出生日期].ToString();
            this.txt母出生日期.Text = dt儿童基本.Rows[0][tb_儿童基本信息.母亲出生日期].ToString();
            
            this.txt孕周.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.出生孕周].ToString();

            this.txt妊娠其他.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他].ToString();
            this.txt妊娠情况.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况].ToString();
            if (txt妊娠情况.Text == "0")
            {
                this.txt妊娠情况.Text = "";
            }
            else if (txt妊娠情况.Text == "99")
            {
                this.txt妊娠情况.Text = "3";
            }
            this.txt助产机构.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.助产机构名称].ToString();

            this.txt出生情况其他.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.出生情况其他].ToString();

            string txt出生情况 = dt新生儿.Rows[0][tb_儿童新生儿访视记录.出生情况].ToString();

            string[] arr出生情况 = txt出生情况.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr出生情况.Length; i++)
            {
                switch (arr出生情况[i])
                {
                    case "1":
                        this.txt出生情况1.Text = arr出生情况[i];
                        break;
                    case "2":
                        this.txt出生情况2.Text = arr出生情况[i];
                        break;
                    case "3":
                        this.txt出生情况2.Text = arr出生情况[i];
                        break;
                    case "4":
                        if (txt出生情况1.Text == "")
                        {
                            this.txt出生情况1.Text = arr出生情况[i];
                        }
                        else if (txt出生情况1.Text == "1")
                        {
                            this.txt出生情况2.Text = arr出生情况[i];
                        }
                        break;
                    case "5":
                        
                        this.txt出生情况2.Text = arr出生情况[i];
                        break;
                    case "6":
                        this.txt出生情况2.Text = arr出生情况[i];
                        break;
                    case "99":
                        this.txt防病.Text = "7";
                        break;

                    default:
                        break;
                }
            }

            this.txt新生儿窒息.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.新生儿窒息].ToString();
            if (txt新生儿窒息.Text == "2")
            {
                this.txt新生儿窒息.Text = "1";
            }
            else if (txt新生儿窒息.Text == "1")
            {
                this.txt新生儿窒息.Text = "2";
            }
            this.txt有畸形.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.是否有畸型其他].ToString();
            this.txt畸形.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.是否有畸型].ToString();
            if (txt畸形.Text == "2")
            {
                this.txt畸形.Text = "1";
            }
            else if (txt畸形.Text == "1")
            {
                this.txt畸形.Text = "2";
            }
            this.txt听力.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.新生儿听力筛查].ToString();
            if (txt听力.Text == "2")
            {
                this.txt听力.Text = "1";
            }
            else if (txt听力.Text == "3")
            {
                this.txt听力.Text = "2";
            }
            else if (txt听力.Text == "1")
            {
                this.txt听力.Text = "3";
            }
            
            this.txt其他代谢病.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他].ToString();

            string txt疾病 = dt新生儿.Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查].ToString();

            string[] arr疾病 = txt疾病.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr疾病.Length; i++)
            {
                switch (arr疾病[i])
                {
                    case "1":
                        this.txt疾病甲低.Text = arr疾病[i];
                        break;
                    case "2":
                        this.txt疾病苯丙酮.Text = arr疾病[2];
                        break;
                    case "99":
                        this.txt疾病其他.Text = "3";
                        break;
                    
                    default:
                        break;
                }
            }

            this.txt出生体重.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.新生儿出生体重].ToString();
            this.txt目前体重.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.目前体重].ToString();
            this.txt出生身长.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.出生身长].ToString();

            this.txt喂养方式.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.喂养方式].ToString();
            this.txt吃奶量.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.吃奶量].ToString();
            this.txt吃奶次数.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.吃奶次数].ToString();
            this.txt呕吐.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.呕吐].ToString();
            this.txt大便.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.大便].ToString();
            this.txt大便次数.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.大便次数].ToString();

            this.txt体温.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.体温].ToString();
            this.txt脉率.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.脉率].ToString();
            this.txt呼吸频率.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.呼吸频率].ToString();
            this.txt面色.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.面色].ToString();
            if (txt面色.Text == "2")
            {
                this.txt面色.Text = "1";
            }
            else if (txt面色.Text == "3")
            {
                this.txt面色.Text = "2";
            }
            this.txt黄疸.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.黄疸部位].ToString();
            if (txt黄疸.Text == "99")
            {
                txt黄疸.Text = "";
            }

            this.txt前囟1.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.前囟1].ToString();
            this.txt前囟2.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.前囟2].ToString();
            this.txt前囟其他.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.前囟状况其他].ToString();
            this.txt前囟状态.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.前囟状况].ToString();

            this.txt眼外观异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.眼外观其他].ToString();
            this.txt眼外观.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.眼外观].ToString();
            this.txt耳外观异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.耳外观其他].ToString();
            this.txt耳外观.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.耳外观].ToString();

            this.txt鼻异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.鼻其他].ToString();
            this.txt鼻.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.鼻].ToString();
            this.txt口腔异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.口腔其他].ToString();
            this.txt口腔.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.口腔].ToString();

            this.txt心肺听诊异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.心肺其他].ToString();
            this.txt心肺听诊.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.心肺].ToString();

            this.txt腹部触诊异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.腹部其他].ToString();
            this.txt腹部触诊.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.腹部].ToString();

            this.txt四肢活动度异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.四肢活动度其他].ToString();
            this.txt四肢活动度.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.四肢活动度].ToString();

            this.txt颈部包块有.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.颈部包块其他].ToString();
            this.txt颈部包块.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.颈部包块].ToString();
            if (txt颈部包块.Text == "2")
            {
                this.txt颈部包块.Text = "1";
            }
            else if (txt颈部包块.Text == "1")
            {
                this.txt颈部包块.Text = "2";
            }
            this.txt皮肤其他.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.皮肤其他].ToString();
            this.txt皮肤.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.皮肤].ToString();

            this.txt肛门异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.肛门其他].ToString();
            this.txt肛门.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.肛门].ToString();

            this.txt外生殖器异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.外生殖器官其他].ToString();
            this.txt外生殖器.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.外生殖器官].ToString();
            this.txt脊柱异常.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.脊柱其他].ToString();
            this.txt脊柱.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.脊柱].ToString();

            this.txt脐带其他.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.脐带其他].ToString();
            this.txt脐带.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.脐带].ToString();

            this.txt原因.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.转诊原因].ToString();
            this.txt机构及科室.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.转诊机构及科室].ToString();
            this.txt转诊建议.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.转诊].ToString();

            string txt指导 = dt新生儿.Rows[0][tb_儿童新生儿访视记录.指导].ToString();

            string[] arr = txt指导.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr.Length; i++)
            {
                switch (arr[i])
                {
                    case "1":
                        this.txt喂养.Text = arr[i];
                        break;
                    case "5":
                        this.txt发育.Text = "2";
                        break;
                    case "6":
                        this.txt防病.Text = "3";
                        break;
                    case "7":
                        this.txt预防.Text = "4";
                        break;
                    case "8":
                        this.txt口腔保健.Text ="5";
                        break;                 
                    default:
                        break;
                }
            }
            this.txt本次访视日期.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.发生时间].ToString();
            this.txt下次随访日期.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.下次随访日期].ToString();
            this.txt下次随访地点.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.下次随访地点].ToString();
            this.txt随访医生签名.Text = dt新生儿.Rows[0][tb_儿童新生儿访视记录.随访医生签名].ToString();            
        }
    }
}
