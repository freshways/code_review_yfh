﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class UC儿童健康检查_6岁_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC儿童健康检查_6岁_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.sbtnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit所在幼儿园名称 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit家长签名 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk发育评估无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估4 = new DevExpress.XtraEditors.CheckEdit();
            this.comboBoxEdit体重指数 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit查体机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit健康管理联系人 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl考核项 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit患病无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit患病肺炎 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit患病肺炎 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit患病腹泻 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit患病腹泻 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit患病外伤 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit患病外伤 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit患病其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit患病其他 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl血红蛋白值 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLblTxtLbl出牙龋齿 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit指导合理膳食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导预防意外 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导生长发育 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导口腔保健 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导疾病预防 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ucLblTxt结果 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt联系方式 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label1 = new System.Windows.Forms.Label();
            this.ucLblTxt转诊机构 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.comboBoxEdit转诊 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit腹部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit胸部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucTxtLbl体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit体重 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucTxtLbl身长 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit身长 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit体格 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit视力 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci身长 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTgfypj = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciXhdb = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciZd = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lci牙齿龋齿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl胸部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciBelly = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciQt = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSight = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciLcsfjhbqk = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl健康管理联系人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl联系电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重指数 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl发育评估 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSfys = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSfrq = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciZz = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家长签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl查体机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所在幼儿园名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重指数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit健康管理联系人.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病肺炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病肺炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病腹泻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病腹泻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病外伤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病外伤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导合理膳食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit身长.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体格.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci身长)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTgfypj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXhdb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci牙齿龋齿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBelly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLcsfjhbqk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康管理联系人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl发育评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfys)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfrq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家长签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl查体机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(739, 32);
            this.panelControl1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.sbtnUpdate);
            this.flowLayoutPanel1.Controls.Add(this.sbtnDelete);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(735, 28);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // sbtnUpdate
            // 
            this.sbtnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("sbtnUpdate.Image")));
            this.sbtnUpdate.Location = new System.Drawing.Point(3, 3);
            this.sbtnUpdate.Name = "sbtnUpdate";
            this.sbtnUpdate.Size = new System.Drawing.Size(75, 23);
            this.sbtnUpdate.TabIndex = 0;
            this.sbtnUpdate.Text = "修改";
            this.sbtnUpdate.Click += new System.EventHandler(this.sbtnUpdate_Click);
            // 
            // sbtnDelete
            // 
            this.sbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("sbtnDelete.Image")));
            this.sbtnDelete.Location = new System.Drawing.Point(84, 3);
            this.sbtnDelete.Name = "sbtnDelete";
            this.sbtnDelete.Size = new System.Drawing.Size(75, 23);
            this.sbtnDelete.TabIndex = 1;
            this.sbtnDelete.Text = "删除";
            this.sbtnDelete.Click += new System.EventHandler(this.sbtnDelete_Click);
            // 
            // sbtnExport
            // 
            this.sbtnExport.Image = ((System.Drawing.Image)(resources.GetObject("sbtnExport.Image")));
            this.sbtnExport.Location = new System.Drawing.Point(165, 3);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport.TabIndex = 3;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(359, 24);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControl1.Controls.Add(this.textEdit所在幼儿园名称);
            this.layoutControl1.Controls.Add(this.textEdit家长签名);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.comboBoxEdit体重指数);
            this.layoutControl1.Controls.Add(this.textEdit查体机构);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit健康管理联系人);
            this.layoutControl1.Controls.Add(this.labelControl考核项);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.ucTxtLbl血红蛋白值);
            this.layoutControl1.Controls.Add(this.ucTxtLblTxtLbl出牙龋齿);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改时间);
            this.layoutControl1.Controls.Add(this.textEdit录入时间);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit儿童姓名);
            this.layoutControl1.Controls.Add(this.textEdit儿童档案号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit其他);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.panel1);
            this.layoutControl1.Controls.Add(this.comboBoxEdit腹部);
            this.layoutControl1.Controls.Add(this.comboBoxEdit胸部);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.comboBoxEdit体格);
            this.layoutControl1.Controls.Add(this.textEdit视力);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.Teal;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(739, 487);
            this.layoutControl1.TabIndex = 5;
            // 
            // textEdit所在幼儿园名称
            // 
            this.textEdit所在幼儿园名称.Location = new System.Drawing.Point(134, 134);
            this.textEdit所在幼儿园名称.Name = "textEdit所在幼儿园名称";
            this.textEdit所在幼儿园名称.Properties.ReadOnly = true;
            this.textEdit所在幼儿园名称.Size = new System.Drawing.Size(130, 20);
            this.textEdit所在幼儿园名称.StyleController = this.layoutControl1;
            this.textEdit所在幼儿园名称.TabIndex = 126;
            // 
            // textEdit家长签名
            // 
            this.textEdit家长签名.Location = new System.Drawing.Point(560, 612);
            this.textEdit家长签名.Name = "textEdit家长签名";
            this.textEdit家长签名.Properties.ReadOnly = true;
            this.textEdit家长签名.Size = new System.Drawing.Size(158, 20);
            this.textEdit家长签名.StyleController = this.layoutControl1;
            this.textEdit家长签名.TabIndex = 115;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.chk发育评估无);
            this.flowLayoutPanel5.Controls.Add(this.chk发育评估1);
            this.flowLayoutPanel5.Controls.Add(this.chk发育评估2);
            this.flowLayoutPanel5.Controls.Add(this.chk发育评估3);
            this.flowLayoutPanel5.Controls.Add(this.chk发育评估4);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(128, 350);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(590, 56);
            this.flowLayoutPanel5.TabIndex = 114;
            // 
            // chk发育评估无
            // 
            this.chk发育评估无.Location = new System.Drawing.Point(3, 3);
            this.chk发育评估无.Name = "chk发育评估无";
            this.chk发育评估无.Properties.Caption = "无";
            this.chk发育评估无.Properties.ReadOnly = true;
            this.chk发育评估无.Size = new System.Drawing.Size(104, 19);
            this.chk发育评估无.TabIndex = 0;
            // 
            // chk发育评估1
            // 
            this.chk发育评估1.Location = new System.Drawing.Point(113, 3);
            this.chk发育评估1.Name = "chk发育评估1";
            this.chk发育评估1.Properties.Caption = "不会表达自己的感受或想法";
            this.chk发育评估1.Properties.ReadOnly = true;
            this.chk发育评估1.Size = new System.Drawing.Size(208, 19);
            this.chk发育评估1.TabIndex = 1;
            // 
            // chk发育评估2
            // 
            this.chk发育评估2.Location = new System.Drawing.Point(327, 3);
            this.chk发育评估2.Name = "chk发育评估2";
            this.chk发育评估2.Properties.Caption = "不会玩角色扮演的集体游戏";
            this.chk发育评估2.Properties.ReadOnly = true;
            this.chk发育评估2.Size = new System.Drawing.Size(179, 19);
            this.chk发育评估2.TabIndex = 2;
            // 
            // chk发育评估3
            // 
            this.chk发育评估3.Location = new System.Drawing.Point(3, 28);
            this.chk发育评估3.Name = "chk发育评估3";
            this.chk发育评估3.Properties.Caption = "不会画方形";
            this.chk发育评估3.Properties.ReadOnly = true;
            this.chk发育评估3.Size = new System.Drawing.Size(104, 19);
            this.chk发育评估3.TabIndex = 3;
            // 
            // chk发育评估4
            // 
            this.chk发育评估4.Location = new System.Drawing.Point(113, 28);
            this.chk发育评估4.Name = "chk发育评估4";
            this.chk发育评估4.Properties.Caption = "不会奔跑";
            this.chk发育评估4.Properties.ReadOnly = true;
            this.chk发育评估4.Size = new System.Drawing.Size(75, 19);
            this.chk发育评估4.TabIndex = 4;
            // 
            // comboBoxEdit体重指数
            // 
            this.comboBoxEdit体重指数.Location = new System.Drawing.Point(128, 230);
            this.comboBoxEdit体重指数.Name = "comboBoxEdit体重指数";
            this.comboBoxEdit体重指数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit体重指数.Size = new System.Drawing.Size(231, 20);
            this.comboBoxEdit体重指数.StyleController = this.layoutControl1;
            this.comboBoxEdit体重指数.TabIndex = 113;
            // 
            // textEdit查体机构
            // 
            this.textEdit查体机构.Location = new System.Drawing.Point(89, 158);
            this.textEdit查体机构.Name = "textEdit查体机构";
            this.textEdit查体机构.Properties.ReadOnly = true;
            this.textEdit查体机构.Size = new System.Drawing.Size(629, 20);
            this.textEdit查体机构.StyleController = this.layoutControl1;
            this.textEdit查体机构.TabIndex = 112;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(557, 134);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(161, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 111;
            // 
            // textEdit健康管理联系人
            // 
            this.textEdit健康管理联系人.Location = new System.Drawing.Point(369, 134);
            this.textEdit健康管理联系人.Name = "textEdit健康管理联系人";
            this.textEdit健康管理联系人.Properties.ReadOnly = true;
            this.textEdit健康管理联系人.Size = new System.Drawing.Size(99, 20);
            this.textEdit健康管理联系人.StyleController = this.layoutControl1;
            this.textEdit健康管理联系人.TabIndex = 110;
            // 
            // labelControl考核项
            // 
            this.labelControl考核项.Location = new System.Drawing.Point(4, 44);
            this.labelControl考核项.Name = "labelControl考核项";
            this.labelControl考核项.Size = new System.Drawing.Size(714, 14);
            this.labelControl考核项.StyleController = this.layoutControl1;
            this.labelControl考核项.TabIndex = 109;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.checkEdit患病无);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit患病肺炎);
            this.flowLayoutPanel4.Controls.Add(this.textEdit患病肺炎);
            this.flowLayoutPanel4.Controls.Add(this.labelControl1);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit患病腹泻);
            this.flowLayoutPanel4.Controls.Add(this.textEdit患病腹泻);
            this.flowLayoutPanel4.Controls.Add(this.labelControl2);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit患病外伤);
            this.flowLayoutPanel4.Controls.Add(this.textEdit患病外伤);
            this.flowLayoutPanel4.Controls.Add(this.labelControl4);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit患病其他);
            this.flowLayoutPanel4.Controls.Add(this.textEdit患病其他);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(128, 410);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(590, 56);
            this.flowLayoutPanel4.TabIndex = 108;
            // 
            // checkEdit患病无
            // 
            this.checkEdit患病无.Location = new System.Drawing.Point(3, 1);
            this.checkEdit患病无.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit患病无.Name = "checkEdit患病无";
            this.checkEdit患病无.Properties.Caption = "无";
            this.checkEdit患病无.Properties.ReadOnly = true;
            this.checkEdit患病无.Size = new System.Drawing.Size(75, 19);
            this.checkEdit患病无.TabIndex = 0;
            this.checkEdit患病无.CheckedChanged += new System.EventHandler(this.checkEdit患病无_CheckedChanged);
            // 
            // checkEdit患病肺炎
            // 
            this.checkEdit患病肺炎.Location = new System.Drawing.Point(84, 1);
            this.checkEdit患病肺炎.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.checkEdit患病肺炎.Name = "checkEdit患病肺炎";
            this.checkEdit患病肺炎.Properties.Caption = "肺炎：";
            this.checkEdit患病肺炎.Properties.ReadOnly = true;
            this.checkEdit患病肺炎.Size = new System.Drawing.Size(66, 19);
            this.checkEdit患病肺炎.TabIndex = 1;
            this.checkEdit患病肺炎.CheckedChanged += new System.EventHandler(this.checkEdit患病肺炎_CheckedChanged);
            // 
            // textEdit患病肺炎
            // 
            this.textEdit患病肺炎.Location = new System.Drawing.Point(156, 1);
            this.textEdit患病肺炎.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.textEdit患病肺炎.Name = "textEdit患病肺炎";
            this.textEdit患病肺炎.Properties.ReadOnly = true;
            this.textEdit患病肺炎.Size = new System.Drawing.Size(71, 20);
            this.textEdit患病肺炎.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(233, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(12, 14);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "次";
            // 
            // checkEdit患病腹泻
            // 
            this.checkEdit患病腹泻.Location = new System.Drawing.Point(251, 0);
            this.checkEdit患病腹泻.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.checkEdit患病腹泻.Name = "checkEdit患病腹泻";
            this.checkEdit患病腹泻.Properties.Caption = "腹泻：";
            this.checkEdit患病腹泻.Properties.ReadOnly = true;
            this.checkEdit患病腹泻.Size = new System.Drawing.Size(59, 19);
            this.checkEdit患病腹泻.TabIndex = 4;
            this.checkEdit患病腹泻.CheckedChanged += new System.EventHandler(this.checkEdit患病腹泻_CheckedChanged);
            // 
            // textEdit患病腹泻
            // 
            this.textEdit患病腹泻.Location = new System.Drawing.Point(316, 0);
            this.textEdit患病腹泻.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.textEdit患病腹泻.Name = "textEdit患病腹泻";
            this.textEdit患病腹泻.Properties.ReadOnly = true;
            this.textEdit患病腹泻.Size = new System.Drawing.Size(66, 20);
            this.textEdit患病腹泻.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(388, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "次";
            // 
            // checkEdit患病外伤
            // 
            this.checkEdit患病外伤.Location = new System.Drawing.Point(406, 0);
            this.checkEdit患病外伤.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.checkEdit患病外伤.Name = "checkEdit患病外伤";
            this.checkEdit患病外伤.Properties.Caption = "外伤：";
            this.checkEdit患病外伤.Properties.ReadOnly = true;
            this.checkEdit患病外伤.Size = new System.Drawing.Size(61, 19);
            this.checkEdit患病外伤.TabIndex = 4;
            this.checkEdit患病外伤.CheckedChanged += new System.EventHandler(this.checkEdit患病外伤_CheckedChanged);
            // 
            // textEdit患病外伤
            // 
            this.textEdit患病外伤.Location = new System.Drawing.Point(473, 0);
            this.textEdit患病外伤.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.textEdit患病外伤.Name = "textEdit患病外伤";
            this.textEdit患病外伤.Properties.ReadOnly = true;
            this.textEdit患病外伤.Size = new System.Drawing.Size(66, 20);
            this.textEdit患病外伤.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(545, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(12, 14);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "次";
            // 
            // checkEdit患病其他
            // 
            this.checkEdit患病其他.Location = new System.Drawing.Point(3, 24);
            this.checkEdit患病其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.checkEdit患病其他.Name = "checkEdit患病其他";
            this.checkEdit患病其他.Properties.Caption = "其他";
            this.checkEdit患病其他.Properties.ReadOnly = true;
            this.checkEdit患病其他.Size = new System.Drawing.Size(63, 19);
            this.checkEdit患病其他.TabIndex = 4;
            this.checkEdit患病其他.CheckedChanged += new System.EventHandler(this.checkEdit患病其他_CheckedChanged);
            // 
            // textEdit患病其他
            // 
            this.textEdit患病其他.Location = new System.Drawing.Point(72, 24);
            this.textEdit患病其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.textEdit患病其他.Name = "textEdit患病其他";
            this.textEdit患病其他.Properties.ReadOnly = true;
            this.textEdit患病其他.Size = new System.Drawing.Size(85, 20);
            this.textEdit患病其他.TabIndex = 5;
            // 
            // ucTxtLbl血红蛋白值
            // 
            this.ucTxtLbl血红蛋白值.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl血红蛋白值.Lbl1Text = "g/L ";
            this.ucTxtLbl血红蛋白值.Location = new System.Drawing.Point(128, 326);
            this.ucTxtLbl血红蛋白值.Name = "ucTxtLbl血红蛋白值";
            this.ucTxtLbl血红蛋白值.Size = new System.Drawing.Size(231, 20);
            this.ucTxtLbl血红蛋白值.TabIndex = 76;
            this.ucTxtLbl血红蛋白值.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLblTxtLbl出牙龋齿
            // 
            this.ucTxtLblTxtLbl出牙龋齿.Lbl1Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl出牙龋齿.Lbl1Text = "/";
            this.ucTxtLblTxtLbl出牙龋齿.Lbl2Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl出牙龋齿.Lbl2Text = "颗";
            this.ucTxtLblTxtLbl出牙龋齿.Location = new System.Drawing.Point(487, 278);
            this.ucTxtLblTxtLbl出牙龋齿.Name = "ucTxtLblTxtLbl出牙龋齿";
            this.ucTxtLblTxtLbl出牙龋齿.Size = new System.Drawing.Size(231, 20);
            this.ucTxtLblTxtLbl出牙龋齿.TabIndex = 106;
            this.ucTxtLblTxtLbl出牙龋齿.Txt1EditValue = null;
            this.ucTxtLblTxtLbl出牙龋齿.Txt1Size = new System.Drawing.Size(50, 20);
            this.ucTxtLblTxtLbl出牙龋齿.Txt2EditValue = null;
            this.ucTxtLblTxtLbl出牙龋齿.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(561, 660);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(157, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 101;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(345, 660);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(117, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 100;
            // 
            // textEdit录入人
            // 
            this.textEdit录入人.Location = new System.Drawing.Point(345, 636);
            this.textEdit录入人.Name = "textEdit录入人";
            this.textEdit录入人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入人.Properties.ReadOnly = true;
            this.textEdit录入人.Size = new System.Drawing.Size(116, 20);
            this.textEdit录入人.StyleController = this.layoutControl1;
            this.textEdit录入人.TabIndex = 99;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(560, 636);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(158, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 98;
            // 
            // textEdit最近修改时间
            // 
            this.textEdit最近修改时间.Location = new System.Drawing.Point(129, 660);
            this.textEdit最近修改时间.Name = "textEdit最近修改时间";
            this.textEdit最近修改时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改时间.Properties.ReadOnly = true;
            this.textEdit最近修改时间.Size = new System.Drawing.Size(117, 20);
            this.textEdit最近修改时间.StyleController = this.layoutControl1;
            this.textEdit最近修改时间.TabIndex = 97;
            // 
            // textEdit录入时间
            // 
            this.textEdit录入时间.Location = new System.Drawing.Point(129, 636);
            this.textEdit录入时间.Name = "textEdit录入时间";
            this.textEdit录入时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入时间.Properties.ReadOnly = true;
            this.textEdit录入时间.Size = new System.Drawing.Size(117, 20);
            this.textEdit录入时间.StyleController = this.layoutControl1;
            this.textEdit录入时间.TabIndex = 96;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(440, 110);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(278, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 95;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(128, 110);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit性别.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(184, 20);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 94;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(440, 86);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(278, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 93;
            // 
            // textEdit儿童姓名
            // 
            this.textEdit儿童姓名.Location = new System.Drawing.Point(128, 86);
            this.textEdit儿童姓名.Name = "textEdit儿童姓名";
            this.textEdit儿童姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童姓名.Properties.ReadOnly = true;
            this.textEdit儿童姓名.Size = new System.Drawing.Size(184, 20);
            this.textEdit儿童姓名.StyleController = this.layoutControl1;
            this.textEdit儿童姓名.TabIndex = 92;
            // 
            // textEdit儿童档案号
            // 
            this.textEdit儿童档案号.Location = new System.Drawing.Point(440, 62);
            this.textEdit儿童档案号.Name = "textEdit儿童档案号";
            this.textEdit儿童档案号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童档案号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童档案号.Properties.ReadOnly = true;
            this.textEdit儿童档案号.Size = new System.Drawing.Size(278, 20);
            this.textEdit儿童档案号.StyleController = this.layoutControl1;
            this.textEdit儿童档案号.TabIndex = 91;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(128, 62);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit卡号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit卡号.Properties.ReadOnly = true;
            this.textEdit卡号.Size = new System.Drawing.Size(184, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 90;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(129, 612);
            this.dateEdit随访日期.Margin = new System.Windows.Forms.Padding(0);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.ReadOnly = true;
            this.dateEdit随访日期.Size = new System.Drawing.Size(117, 20);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 14;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(345, 612);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Properties.ReadOnly = true;
            this.textEdit随访医生.Size = new System.Drawing.Size(116, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 89;
            // 
            // textEdit其他
            // 
            this.textEdit其他.Location = new System.Drawing.Point(487, 326);
            this.textEdit其他.Margin = new System.Windows.Forms.Padding(3, 22, 3, 3);
            this.textEdit其他.Name = "textEdit其他";
            this.textEdit其他.Properties.ReadOnly = true;
            this.textEdit其他.Size = new System.Drawing.Size(231, 20);
            this.textEdit其他.StyleController = this.layoutControl1;
            this.textEdit其他.TabIndex = 85;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导合理膳食);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导预防意外);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导生长发育);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导口腔保健);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导疾病预防);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导其他);
            this.flowLayoutPanel6.Controls.Add(this.textEdit指导其他);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(128, 470);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(590, 56);
            this.flowLayoutPanel6.TabIndex = 83;
            // 
            // checkEdit指导合理膳食
            // 
            this.checkEdit指导合理膳食.Location = new System.Drawing.Point(3, 3);
            this.checkEdit指导合理膳食.Name = "checkEdit指导合理膳食";
            this.checkEdit指导合理膳食.Properties.Caption = "合理膳食";
            this.checkEdit指导合理膳食.Properties.ReadOnly = true;
            this.checkEdit指导合理膳食.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导合理膳食.TabIndex = 0;
            // 
            // checkEdit指导预防意外
            // 
            this.checkEdit指导预防意外.Location = new System.Drawing.Point(85, 3);
            this.checkEdit指导预防意外.Name = "checkEdit指导预防意外";
            this.checkEdit指导预防意外.Properties.Caption = "预防意外伤害";
            this.checkEdit指导预防意外.Properties.ReadOnly = true;
            this.checkEdit指导预防意外.Size = new System.Drawing.Size(103, 19);
            this.checkEdit指导预防意外.TabIndex = 1;
            // 
            // checkEdit指导生长发育
            // 
            this.checkEdit指导生长发育.Location = new System.Drawing.Point(194, 3);
            this.checkEdit指导生长发育.Name = "checkEdit指导生长发育";
            this.checkEdit指导生长发育.Properties.Caption = "生长发育";
            this.checkEdit指导生长发育.Properties.ReadOnly = true;
            this.checkEdit指导生长发育.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导生长发育.TabIndex = 2;
            // 
            // checkEdit指导口腔保健
            // 
            this.checkEdit指导口腔保健.Location = new System.Drawing.Point(276, 3);
            this.checkEdit指导口腔保健.Name = "checkEdit指导口腔保健";
            this.checkEdit指导口腔保健.Properties.Caption = "口腔保健";
            this.checkEdit指导口腔保健.Properties.ReadOnly = true;
            this.checkEdit指导口腔保健.Size = new System.Drawing.Size(78, 19);
            this.checkEdit指导口腔保健.TabIndex = 3;
            // 
            // checkEdit指导疾病预防
            // 
            this.checkEdit指导疾病预防.Location = new System.Drawing.Point(360, 3);
            this.checkEdit指导疾病预防.Name = "checkEdit指导疾病预防";
            this.checkEdit指导疾病预防.Properties.Caption = "疾病预防";
            this.checkEdit指导疾病预防.Properties.ReadOnly = true;
            this.checkEdit指导疾病预防.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导疾病预防.TabIndex = 4;
            // 
            // checkEdit指导其他
            // 
            this.checkEdit指导其他.Location = new System.Drawing.Point(442, 3);
            this.checkEdit指导其他.Name = "checkEdit指导其他";
            this.checkEdit指导其他.Properties.Caption = "其他";
            this.checkEdit指导其他.Properties.ReadOnly = true;
            this.checkEdit指导其他.Size = new System.Drawing.Size(53, 19);
            this.checkEdit指导其他.TabIndex = 21;
            this.checkEdit指导其他.CheckedChanged += new System.EventHandler(this.checkEdit指导其他_CheckedChanged);
            // 
            // textEdit指导其他
            // 
            this.textEdit指导其他.Enabled = false;
            this.textEdit指导其他.Location = new System.Drawing.Point(3, 28);
            this.textEdit指导其他.Name = "textEdit指导其他";
            this.textEdit指导其他.Properties.ReadOnly = true;
            this.textEdit指导其他.Size = new System.Drawing.Size(182, 20);
            this.textEdit指导其他.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ucLblTxt结果);
            this.panel1.Controls.Add(this.ucLblTxt联系方式);
            this.panel1.Controls.Add(this.ucLblTxt联系人);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ucLblTxt转诊机构);
            this.panel1.Controls.Add(this.ucLblTxt转诊原因);
            this.panel1.Controls.Add(this.comboBoxEdit转诊);
            this.panel1.Location = new System.Drawing.Point(128, 530);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 78);
            this.panel1.TabIndex = 81;
            // 
            // ucLblTxt结果
            // 
            this.ucLblTxt结果.Enabled = false;
            this.ucLblTxt结果.Lbl1Size = new System.Drawing.Size(60, 18);
            this.ucLblTxt结果.Lbl1Text = "结果：";
            this.ucLblTxt结果.Location = new System.Drawing.Point(363, 42);
            this.ucLblTxt结果.Name = "ucLblTxt结果";
            this.ucLblTxt结果.Size = new System.Drawing.Size(136, 22);
            this.ucLblTxt结果.TabIndex = 86;
            this.ucLblTxt结果.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxt联系方式
            // 
            this.ucLblTxt联系方式.Enabled = false;
            this.ucLblTxt联系方式.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt联系方式.Lbl1Text = "联系方式：";
            this.ucLblTxt联系方式.Location = new System.Drawing.Point(153, 43);
            this.ucLblTxt联系方式.Name = "ucLblTxt联系方式";
            this.ucLblTxt联系方式.Size = new System.Drawing.Size(196, 22);
            this.ucLblTxt联系方式.TabIndex = 85;
            this.ucLblTxt联系方式.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // ucLblTxt联系人
            // 
            this.ucLblTxt联系人.Enabled = false;
            this.ucLblTxt联系人.Lbl1Size = new System.Drawing.Size(60, 18);
            this.ucLblTxt联系人.Lbl1Text = "联系人：";
            this.ucLblTxt联系人.Location = new System.Drawing.Point(18, 43);
            this.ucLblTxt联系人.Name = "ucLblTxt联系人";
            this.ucLblTxt联系人.Size = new System.Drawing.Size(133, 22);
            this.ucLblTxt联系人.TabIndex = 84;
            this.ucLblTxt联系人.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 83;
            this.label1.Text = "有无转诊：";
            // 
            // ucLblTxt转诊机构
            // 
            this.ucLblTxt转诊机构.Enabled = false;
            this.ucLblTxt转诊机构.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt转诊机构.Lbl1Text = "机构及科室：";
            this.ucLblTxt转诊机构.Location = new System.Drawing.Point(354, 9);
            this.ucLblTxt转诊机构.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt转诊机构.Name = "ucLblTxt转诊机构";
            this.ucLblTxt转诊机构.Size = new System.Drawing.Size(196, 22);
            this.ucLblTxt转诊机构.TabIndex = 82;
            this.ucLblTxt转诊机构.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // ucLblTxt转诊原因
            // 
            this.ucLblTxt转诊原因.Enabled = false;
            this.ucLblTxt转诊原因.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt转诊原因.Lbl1Text = "原  因：";
            this.ucLblTxt转诊原因.Location = new System.Drawing.Point(152, 9);
            this.ucLblTxt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt转诊原因.Name = "ucLblTxt转诊原因";
            this.ucLblTxt转诊原因.Size = new System.Drawing.Size(197, 22);
            this.ucLblTxt转诊原因.TabIndex = 81;
            this.ucLblTxt转诊原因.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // comboBoxEdit转诊
            // 
            this.comboBoxEdit转诊.Location = new System.Drawing.Point(78, 9);
            this.comboBoxEdit转诊.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit转诊.Name = "comboBoxEdit转诊";
            this.comboBoxEdit转诊.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit转诊.Properties.ReadOnly = true;
            this.comboBoxEdit转诊.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit转诊.Size = new System.Drawing.Size(60, 20);
            this.comboBoxEdit转诊.TabIndex = 80;
            // 
            // comboBoxEdit腹部
            // 
            this.comboBoxEdit腹部.Location = new System.Drawing.Point(487, 302);
            this.comboBoxEdit腹部.Name = "comboBoxEdit腹部";
            this.comboBoxEdit腹部.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit腹部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit腹部.Properties.ReadOnly = true;
            this.comboBoxEdit腹部.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit腹部.Size = new System.Drawing.Size(231, 20);
            this.comboBoxEdit腹部.StyleController = this.layoutControl1;
            this.comboBoxEdit腹部.TabIndex = 62;
            // 
            // comboBoxEdit胸部
            // 
            this.comboBoxEdit胸部.Location = new System.Drawing.Point(128, 302);
            this.comboBoxEdit胸部.Name = "comboBoxEdit胸部";
            this.comboBoxEdit胸部.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit胸部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit胸部.Properties.ReadOnly = true;
            this.comboBoxEdit胸部.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit胸部.Size = new System.Drawing.Size(231, 20);
            this.comboBoxEdit胸部.StyleController = this.layoutControl1;
            this.comboBoxEdit胸部.TabIndex = 61;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.ucTxtLbl体重);
            this.flowLayoutPanel2.Controls.Add(this.comboBoxEdit体重);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(128, 206);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(231, 20);
            this.flowLayoutPanel2.TabIndex = 52;
            // 
            // ucTxtLbl体重
            // 
            this.ucTxtLbl体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl体重.Lbl1Text = "kg";
            this.ucTxtLbl体重.Location = new System.Drawing.Point(0, 0);
            this.ucTxtLbl体重.Margin = new System.Windows.Forms.Padding(0);
            this.ucTxtLbl体重.Name = "ucTxtLbl体重";
            this.ucTxtLbl体重.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl体重.TabIndex = 0;
            this.ucTxtLbl体重.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // comboBoxEdit体重
            // 
            this.comboBoxEdit体重.Location = new System.Drawing.Point(107, 0);
            this.comboBoxEdit体重.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit体重.Name = "comboBoxEdit体重";
            this.comboBoxEdit体重.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit体重.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit体重.Properties.ReadOnly = true;
            this.comboBoxEdit体重.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit体重.Size = new System.Drawing.Size(55, 20);
            this.comboBoxEdit体重.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.ucTxtLbl身长);
            this.flowLayoutPanel3.Controls.Add(this.comboBoxEdit身长);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(487, 206);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(231, 20);
            this.flowLayoutPanel3.TabIndex = 53;
            // 
            // ucTxtLbl身长
            // 
            this.ucTxtLbl身长.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl身长.Lbl1Text = "cm";
            this.ucTxtLbl身长.Location = new System.Drawing.Point(0, 0);
            this.ucTxtLbl身长.Margin = new System.Windows.Forms.Padding(0);
            this.ucTxtLbl身长.Name = "ucTxtLbl身长";
            this.ucTxtLbl身长.Size = new System.Drawing.Size(117, 22);
            this.ucTxtLbl身长.TabIndex = 0;
            this.ucTxtLbl身长.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // comboBoxEdit身长
            // 
            this.comboBoxEdit身长.Location = new System.Drawing.Point(117, 0);
            this.comboBoxEdit身长.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit身长.Name = "comboBoxEdit身长";
            this.comboBoxEdit身长.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit身长.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit身长.Properties.ReadOnly = true;
            this.comboBoxEdit身长.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit身长.Size = new System.Drawing.Size(55, 20);
            this.comboBoxEdit身长.TabIndex = 1;
            // 
            // comboBoxEdit体格
            // 
            this.comboBoxEdit体格.Location = new System.Drawing.Point(487, 230);
            this.comboBoxEdit体格.Name = "comboBoxEdit体格";
            this.comboBoxEdit体格.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEdit体格.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit体格.Properties.ReadOnly = true;
            this.comboBoxEdit体格.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit体格.Size = new System.Drawing.Size(231, 20);
            this.comboBoxEdit体格.StyleController = this.layoutControl1;
            this.comboBoxEdit体格.TabIndex = 56;
            // 
            // textEdit视力
            // 
            this.textEdit视力.Location = new System.Drawing.Point(128, 278);
            this.textEdit视力.Name = "textEdit视力";
            this.textEdit视力.Properties.ReadOnly = true;
            this.textEdit视力.Size = new System.Drawing.Size(231, 20);
            this.textEdit视力.StyleController = this.layoutControl1;
            this.textEdit视力.TabIndex = 107;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem52,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.emptySpaceItem1,
            this.lci体重,
            this.lci身长,
            this.lciTgfypj,
            this.lciXhdb,
            this.lciZd,
            this.emptySpaceItem5,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.lci牙齿龋齿,
            this.lbl胸部,
            this.lciBelly,
            this.lciQt,
            this.lciSight,
            this.lciLcsfjhbqk,
            this.layoutControlItem19,
            this.lbl健康管理联系人,
            this.lbl联系电话,
            this.lbl体重指数,
            this.lbl发育评估,
            this.lciSfys,
            this.lciSfrq,
            this.lciZz,
            this.lbl家长签名,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem12,
            this.layoutControlItem1,
            this.lbl查体机构,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(722, 694);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.textEdit卡号;
            this.layoutControlItem52.CustomizationFormText = "卡号：";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem52.Text = "卡号：";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit儿童档案号;
            this.layoutControlItem2.CustomizationFormText = "儿童档案号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(312, 58);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem2.Text = "儿童档案号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit儿童姓名;
            this.layoutControlItem6.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem6.Text = "儿童姓名：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit身份证号;
            this.layoutControlItem3.CustomizationFormText = "身份证号：";
            this.layoutControlItem3.Location = new System.Drawing.Point(312, 82);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem3.Text = "身份证号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit性别;
            this.layoutControlItem7.CustomizationFormText = "性别：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 106);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem7.Text = "性别：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit出生日期;
            this.layoutControlItem4.CustomizationFormText = "出生日期：";
            this.layoutControlItem4.Location = new System.Drawing.Point(312, 106);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(406, 24);
            this.layoutControlItem4.Text = "出生日期：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit录入时间;
            this.layoutControlItem5.CustomizationFormText = "录入时间：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 632);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem5.Text = "录入时间：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(120, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit最近修改人;
            this.layoutControlItem11.CustomizationFormText = "最近修改人：";
            this.layoutControlItem11.Location = new System.Drawing.Point(246, 656);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem11.Text = "最近修改人：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 40);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 40);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(718, 40);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "儿童健康检查记录表 （6岁）";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(121, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // lci体重
            // 
            this.lci体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci体重.AppearanceItemCaption.Options.UseFont = true;
            this.lci体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci体重.Control = this.flowLayoutPanel2;
            this.lci体重.CustomizationFormText = "体重：";
            this.lci体重.Location = new System.Drawing.Point(0, 202);
            this.lci体重.Name = "lci体重";
            this.lci体重.Size = new System.Drawing.Size(359, 24);
            this.lci体重.Tag = "check";
            this.lci体重.Text = "体重：";
            this.lci体重.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lci身长
            // 
            this.lci身长.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci身长.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci身长.AppearanceItemCaption.Options.UseFont = true;
            this.lci身长.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci身长.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci身长.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci身长.Control = this.flowLayoutPanel3;
            this.lci身长.CustomizationFormText = "身长：";
            this.lci身长.Location = new System.Drawing.Point(359, 202);
            this.lci身长.Name = "lci身长";
            this.lci身长.Size = new System.Drawing.Size(359, 24);
            this.lci身长.Tag = "check";
            this.lci身长.Text = "身高：";
            this.lci身长.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciTgfypj
            // 
            this.lciTgfypj.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciTgfypj.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciTgfypj.AppearanceItemCaption.Options.UseFont = true;
            this.lciTgfypj.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciTgfypj.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciTgfypj.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciTgfypj.Control = this.comboBoxEdit体格;
            this.lciTgfypj.CustomizationFormText = "皮肤：";
            this.lciTgfypj.Location = new System.Drawing.Point(359, 226);
            this.lciTgfypj.Name = "lciTgfypj";
            this.lciTgfypj.Size = new System.Drawing.Size(359, 24);
            this.lciTgfypj.Tag = "check";
            this.lciTgfypj.Text = "体格发育评价：";
            this.lciTgfypj.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciXhdb
            // 
            this.lciXhdb.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciXhdb.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciXhdb.AppearanceItemCaption.Options.UseFont = true;
            this.lciXhdb.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciXhdb.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciXhdb.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciXhdb.Control = this.ucTxtLbl血红蛋白值;
            this.lciXhdb.CustomizationFormText = "血红蛋白值：";
            this.lciXhdb.Location = new System.Drawing.Point(0, 322);
            this.lciXhdb.Name = "lciXhdb";
            this.lciXhdb.Size = new System.Drawing.Size(359, 24);
            this.lciXhdb.Text = "血红蛋白值：";
            this.lciXhdb.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciZd
            // 
            this.lciZd.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciZd.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciZd.AppearanceItemCaption.Options.UseFont = true;
            this.lciZd.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciZd.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciZd.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciZd.Control = this.flowLayoutPanel6;
            this.lciZd.CustomizationFormText = "指导:";
            this.lciZd.Location = new System.Drawing.Point(0, 466);
            this.lciZd.MinSize = new System.Drawing.Size(228, 60);
            this.lciZd.Name = "lciZd";
            this.lciZd.Size = new System.Drawing.Size(718, 60);
            this.lciZd.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciZd.Tag = "check";
            this.lciZd.Text = "指导:";
            this.lciZd.TextSize = new System.Drawing.Size(121, 14);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 680);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(718, 10);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "体格发育:";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 178);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(718, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "体格发育:";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(121, 0);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "体格检查: ";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 250);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(718, 24);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "体格检查: ";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(121, 0);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // lci牙齿龋齿
            // 
            this.lci牙齿龋齿.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci牙齿龋齿.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci牙齿龋齿.AppearanceItemCaption.Options.UseFont = true;
            this.lci牙齿龋齿.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci牙齿龋齿.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci牙齿龋齿.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci牙齿龋齿.Control = this.ucTxtLblTxtLbl出牙龋齿;
            this.lci牙齿龋齿.CustomizationFormText = "出牙/龋齿数：";
            this.lci牙齿龋齿.Location = new System.Drawing.Point(359, 274);
            this.lci牙齿龋齿.Name = "lci牙齿龋齿";
            this.lci牙齿龋齿.Size = new System.Drawing.Size(359, 24);
            this.lci牙齿龋齿.Text = "牙齿/龋齿数：";
            this.lci牙齿龋齿.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lbl胸部
            // 
            this.lbl胸部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl胸部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl胸部.AppearanceItemCaption.Options.UseFont = true;
            this.lbl胸部.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl胸部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl胸部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl胸部.Control = this.comboBoxEdit胸部;
            this.lbl胸部.CustomizationFormText = "胸部：";
            this.lbl胸部.Location = new System.Drawing.Point(0, 298);
            this.lbl胸部.Name = "lbl胸部";
            this.lbl胸部.Size = new System.Drawing.Size(359, 24);
            this.lbl胸部.Tag = "check";
            this.lbl胸部.Text = "胸部：";
            this.lbl胸部.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciBelly
            // 
            this.lciBelly.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciBelly.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciBelly.AppearanceItemCaption.Options.UseFont = true;
            this.lciBelly.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciBelly.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciBelly.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciBelly.Control = this.comboBoxEdit腹部;
            this.lciBelly.CustomizationFormText = "腹部：";
            this.lciBelly.Location = new System.Drawing.Point(359, 298);
            this.lciBelly.Name = "lciBelly";
            this.lciBelly.Size = new System.Drawing.Size(359, 24);
            this.lciBelly.Tag = "check";
            this.lciBelly.Text = "腹部：";
            this.lciBelly.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciQt
            // 
            this.lciQt.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciQt.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciQt.AppearanceItemCaption.Options.UseFont = true;
            this.lciQt.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciQt.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciQt.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciQt.Control = this.textEdit其他;
            this.lciQt.CustomizationFormText = "其他:";
            this.lciQt.Location = new System.Drawing.Point(359, 322);
            this.lciQt.Name = "lciQt";
            this.lciQt.Size = new System.Drawing.Size(359, 24);
            this.lciQt.Tag = "check";
            this.lciQt.Text = "其他:";
            this.lciQt.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciSight
            // 
            this.lciSight.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciSight.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciSight.AppearanceItemCaption.Options.UseFont = true;
            this.lciSight.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciSight.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciSight.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciSight.Control = this.textEdit视力;
            this.lciSight.CustomizationFormText = "听力：";
            this.lciSight.Location = new System.Drawing.Point(0, 274);
            this.lciSight.Name = "lciSight";
            this.lciSight.Size = new System.Drawing.Size(359, 24);
            this.lciSight.Tag = "check";
            this.lciSight.Text = "视力：";
            this.lciSight.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciLcsfjhbqk
            // 
            this.lciLcsfjhbqk.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciLcsfjhbqk.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciLcsfjhbqk.AppearanceItemCaption.Options.UseFont = true;
            this.lciLcsfjhbqk.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciLcsfjhbqk.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciLcsfjhbqk.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciLcsfjhbqk.Control = this.flowLayoutPanel4;
            this.lciLcsfjhbqk.CustomizationFormText = "两次随访间患病情况:";
            this.lciLcsfjhbqk.Location = new System.Drawing.Point(0, 406);
            this.lciLcsfjhbqk.MinSize = new System.Drawing.Size(228, 60);
            this.lciLcsfjhbqk.Name = "lciLcsfjhbqk";
            this.lciLcsfjhbqk.Size = new System.Drawing.Size(718, 60);
            this.lciLcsfjhbqk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciLcsfjhbqk.Tag = "check";
            this.lciLcsfjhbqk.Text = "两次随访间患病情况:";
            this.lciLcsfjhbqk.TextSize = new System.Drawing.Size(121, 14);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.labelControl考核项;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(718, 18);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // lbl健康管理联系人
            // 
            this.lbl健康管理联系人.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl健康管理联系人.AppearanceItemCaption.Options.UseFont = true;
            this.lbl健康管理联系人.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl健康管理联系人.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl健康管理联系人.Control = this.textEdit健康管理联系人;
            this.lbl健康管理联系人.CustomizationFormText = "健康管理联系人：";
            this.lbl健康管理联系人.Location = new System.Drawing.Point(264, 130);
            this.lbl健康管理联系人.Name = "lbl健康管理联系人";
            this.lbl健康管理联系人.Size = new System.Drawing.Size(204, 24);
            this.lbl健康管理联系人.Tag = "";
            this.lbl健康管理联系人.Text = "健康管理联系人：";
            this.lbl健康管理联系人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lbl健康管理联系人.TextSize = new System.Drawing.Size(96, 14);
            this.lbl健康管理联系人.TextToControlDistance = 5;
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl联系电话.AppearanceItemCaption.Options.UseFont = true;
            this.lbl联系电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl联系电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl联系电话.Control = this.textEdit联系电话;
            this.lbl联系电话.CustomizationFormText = "联系电话：";
            this.lbl联系电话.Location = new System.Drawing.Point(468, 130);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(250, 24);
            this.lbl联系电话.Tag = "";
            this.lbl联系电话.Text = "联系电话：";
            this.lbl联系电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl联系电话.TextSize = new System.Drawing.Size(80, 20);
            this.lbl联系电话.TextToControlDistance = 5;
            // 
            // lbl体重指数
            // 
            this.lbl体重指数.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl体重指数.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl体重指数.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体重指数.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体重指数.Control = this.comboBoxEdit体重指数;
            this.lbl体重指数.CustomizationFormText = "体重/身高：";
            this.lbl体重指数.Location = new System.Drawing.Point(0, 226);
            this.lbl体重指数.Name = "lbl体重指数";
            this.lbl体重指数.Size = new System.Drawing.Size(359, 24);
            this.lbl体重指数.Tag = "check";
            this.lbl体重指数.Text = "体重/身高：";
            this.lbl体重指数.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lbl发育评估
            // 
            this.lbl发育评估.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl发育评估.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl发育评估.AppearanceItemCaption.Options.UseFont = true;
            this.lbl发育评估.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl发育评估.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl发育评估.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl发育评估.Control = this.flowLayoutPanel5;
            this.lbl发育评估.CustomizationFormText = "发育评估：";
            this.lbl发育评估.Location = new System.Drawing.Point(0, 346);
            this.lbl发育评估.MinSize = new System.Drawing.Size(228, 60);
            this.lbl发育评估.Name = "lbl发育评估";
            this.lbl发育评估.Size = new System.Drawing.Size(718, 60);
            this.lbl发育评估.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl发育评估.Tag = "check";
            this.lbl发育评估.Text = "发育评估：";
            this.lbl发育评估.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lciSfys
            // 
            this.lciSfys.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciSfys.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciSfys.AppearanceItemCaption.Options.UseFont = true;
            this.lciSfys.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciSfys.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciSfys.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciSfys.Control = this.textEdit随访医生;
            this.lciSfys.CustomizationFormText = "随访医生签名:";
            this.lciSfys.Location = new System.Drawing.Point(246, 608);
            this.lciSfys.Name = "lciSfys";
            this.lciSfys.Size = new System.Drawing.Size(215, 24);
            this.lciSfys.Tag = "check";
            this.lciSfys.Text = "随访医生签名:";
            this.lciSfys.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciSfys.TextSize = new System.Drawing.Size(90, 14);
            this.lciSfys.TextToControlDistance = 5;
            // 
            // lciSfrq
            // 
            this.lciSfrq.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciSfrq.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciSfrq.AppearanceItemCaption.Options.UseFont = true;
            this.lciSfrq.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciSfrq.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciSfrq.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciSfrq.Control = this.dateEdit随访日期;
            this.lciSfrq.CustomizationFormText = "随访日期:";
            this.lciSfrq.Location = new System.Drawing.Point(0, 608);
            this.lciSfrq.Name = "lciSfrq";
            this.lciSfrq.Size = new System.Drawing.Size(246, 24);
            this.lciSfrq.Tag = "check";
            this.lciSfrq.Text = "随访日期:";
            this.lciSfrq.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciSfrq.TextSize = new System.Drawing.Size(120, 14);
            this.lciSfrq.TextToControlDistance = 5;
            // 
            // lciZz
            // 
            this.lciZz.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciZz.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciZz.AppearanceItemCaption.Options.UseFont = true;
            this.lciZz.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciZz.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciZz.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciZz.Control = this.panel1;
            this.lciZz.CustomizationFormText = "转诊:";
            this.lciZz.Location = new System.Drawing.Point(0, 526);
            this.lciZz.MinSize = new System.Drawing.Size(228, 82);
            this.lciZz.Name = "lciZz";
            this.lciZz.Size = new System.Drawing.Size(718, 82);
            this.lciZz.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciZz.Tag = "check";
            this.lciZz.Text = "转诊建议:";
            this.lciZz.TextSize = new System.Drawing.Size(121, 14);
            // 
            // lbl家长签名
            // 
            this.lbl家长签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl家长签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl家长签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家长签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家长签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家长签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家长签名.Control = this.textEdit家长签名;
            this.lbl家长签名.CustomizationFormText = "家长签名：";
            this.lbl家长签名.Location = new System.Drawing.Point(461, 608);
            this.lbl家长签名.Name = "lbl家长签名";
            this.lbl家长签名.Size = new System.Drawing.Size(257, 24);
            this.lbl家长签名.Tag = "check";
            this.lbl家长签名.Text = "家长签名：";
            this.lbl家长签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl家长签名.TextSize = new System.Drawing.Size(90, 20);
            this.lbl家长签名.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit录入人;
            this.layoutControlItem10.CustomizationFormText = "录入人：";
            this.layoutControlItem10.Location = new System.Drawing.Point(246, 632);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit创建机构;
            this.layoutControlItem9.CustomizationFormText = "创建机构：";
            this.layoutControlItem9.Location = new System.Drawing.Point(461, 632);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(257, 24);
            this.layoutControlItem9.Text = "创建机构：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit最近修改时间;
            this.layoutControlItem8.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 656);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem8.Text = "最近更新时间：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(120, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit当前所属机构;
            this.layoutControlItem12.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem12.Location = new System.Drawing.Point(462, 656);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem12.Text = "当前所属机构：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit所在幼儿园名称;
            this.layoutControlItem1.CustomizationFormText = "名称：";
            this.layoutControlItem1.Location = new System.Drawing.Point(89, 130);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(175, 24);
            this.layoutControlItem1.Text = "名称：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // lbl查体机构
            // 
            this.lbl查体机构.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl查体机构.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl查体机构.AppearanceItemCaption.Options.UseFont = true;
            this.lbl查体机构.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl查体机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl查体机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl查体机构.Control = this.textEdit查体机构;
            this.lbl查体机构.CustomizationFormText = "查体机构：";
            this.lbl查体机构.Location = new System.Drawing.Point(0, 154);
            this.lbl查体机构.Name = "lbl查体机构";
            this.lbl查体机构.Size = new System.Drawing.Size(718, 24);
            this.lbl查体机构.Tag = "check";
            this.lbl查体机构.Text = "查体机构：";
            this.lbl查体机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl查体机构.TextSize = new System.Drawing.Size(80, 14);
            this.lbl查体机构.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "所在幼儿园：";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(89, 24);
            this.emptySpaceItem2.Text = "所在幼儿园：";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(121, 0);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // UC儿童健康检查_6岁_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC儿童健康检查_6岁_显示";
            this.Size = new System.Drawing.Size(739, 519);
            this.Load += new System.EventHandler(this.UC儿童健康检查_6岁_显示_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所在幼儿园名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重指数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit健康管理联系人.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病肺炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病肺炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病腹泻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病腹泻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病外伤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病外伤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit患病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit患病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导合理膳食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit身长.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体格.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci身长)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTgfypj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXhdb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci牙齿龋齿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBelly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLcsfjhbqk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康管理联系人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl发育评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfys)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfrq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家长签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl查体机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton sbtnUpdate;
        private DevExpress.XtraEditors.SimpleButton sbtnDelete;
        private DevExpress.XtraEditors.SimpleButton sbtnExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.CheckEdit checkEdit患病无;
        private DevExpress.XtraEditors.CheckEdit checkEdit患病肺炎;
        private DevExpress.XtraEditors.TextEdit textEdit患病肺炎;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit患病腹泻;
        private DevExpress.XtraEditors.TextEdit textEdit患病腹泻;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkEdit患病外伤;
        private DevExpress.XtraEditors.TextEdit textEdit患病外伤;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit checkEdit患病其他;
        private DevExpress.XtraEditors.TextEdit textEdit患病其他;
        private Library.UserControls.UCTxtLbl ucTxtLbl血红蛋白值;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl出牙龋齿;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit录入人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改时间;
        private DevExpress.XtraEditors.TextEdit textEdit录入时间;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit儿童姓名;
        private DevExpress.XtraEditors.TextEdit textEdit儿童档案号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导合理膳食;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导预防意外;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导口腔保健;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导生长发育;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导疾病预防;
        private System.Windows.Forms.Panel panel1;
        private Library.UserControls.UCLblTxt ucLblTxt转诊机构;
        private Library.UserControls.UCLblTxt ucLblTxt转诊原因;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit转诊;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit腹部;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit胸部;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Library.UserControls.UCTxtLbl ucTxtLbl体重;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit体重;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private Library.UserControls.UCTxtLbl ucTxtLbl身长;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit身长;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit体格;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem lciSfrq;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem lci体重;
        private DevExpress.XtraLayout.LayoutControlItem lci身长;
        private DevExpress.XtraLayout.LayoutControlItem lciTgfypj;
        private DevExpress.XtraLayout.LayoutControlItem lciXhdb;
        private DevExpress.XtraLayout.LayoutControlItem lciZd;
        private DevExpress.XtraLayout.LayoutControlItem lciSfys;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem lci牙齿龋齿;
        private DevExpress.XtraLayout.LayoutControlItem lbl胸部;
        private DevExpress.XtraLayout.LayoutControlItem lciBelly;
        private DevExpress.XtraLayout.LayoutControlItem lciQt;
        private DevExpress.XtraLayout.LayoutControlItem lciZz;
        private DevExpress.XtraLayout.LayoutControlItem lciSight;
        private DevExpress.XtraLayout.LayoutControlItem lciLcsfjhbqk;
        private DevExpress.XtraEditors.LabelControl labelControl考核项;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.TextEdit textEdit视力;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导其他;
        private DevExpress.XtraEditors.TextEdit textEdit指导其他;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit体重指数;
        private DevExpress.XtraEditors.TextEdit textEdit查体机构;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit健康管理联系人;
        private DevExpress.XtraLayout.LayoutControlItem lbl健康管理联系人;
        private DevExpress.XtraLayout.LayoutControlItem lbl联系电话;
        private DevExpress.XtraLayout.LayoutControlItem lbl查体机构;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重指数;
        private DevExpress.XtraEditors.TextEdit textEdit家长签名;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit chk发育评估无;
        private DevExpress.XtraEditors.CheckEdit chk发育评估1;
        private DevExpress.XtraEditors.CheckEdit chk发育评估2;
        private DevExpress.XtraEditors.CheckEdit chk发育评估3;
        private DevExpress.XtraEditors.CheckEdit chk发育评估4;
        private DevExpress.XtraLayout.LayoutControlItem lbl发育评估;
        private DevExpress.XtraLayout.LayoutControlItem lbl家长签名;
        private Library.UserControls.UCLblTxt ucLblTxt结果;
        private Library.UserControls.UCLblTxt ucLblTxt联系方式;
        private Library.UserControls.UCLblTxt ucLblTxt联系人;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit所在幼儿园名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}
