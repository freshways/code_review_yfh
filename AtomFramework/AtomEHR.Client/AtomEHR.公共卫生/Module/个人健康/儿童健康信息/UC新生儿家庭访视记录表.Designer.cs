﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class UC新生儿家庭访视记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textEdit家长签名 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit胸部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit胸部其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt母亲身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt父亲身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit脉率 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit呼吸频率 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit体温 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit吃奶次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit大便次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit吃奶量 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl吃奶量 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit出生身长 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit目前体重 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl目前体重 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit出生体重 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl出生体重 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit脐带 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit脐带其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit脊柱 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit脊柱其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit腹部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit腹部其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit外生殖器 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit外生殖器其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit肛门 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit肛门其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit心肺 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit心肺其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit口腔 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit口腔其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit鼻 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit鼻其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit颈部包块 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit颈部包块其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit耳外观 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit耳外观其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit四肢活动度 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit四肢活动度其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit眼外观 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit眼外观其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit前卤情况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit前卤情况其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit前卤1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit前卤2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit是否畸形 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit是否畸形其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit母妊娠患病情况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit母妊娠患病其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit下次随访地点 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit指导科学喂养 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导生长发育 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导疾病预防 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导预防意外 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导口腔保健 = new DevExpress.XtraEditors.CheckEdit();
            this.chk口服维生素D = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit是否到位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit转诊联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系人 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit转诊机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转诊原因 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit转诊 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit皮肤未见异常 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit皮肤湿疹 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit皮肤糜烂 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit皮肤其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit皮肤其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit面色未检 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit面色红润 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit面色黄染 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit面色其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit面色其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit疾病筛查无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit疾病筛查甲低 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit疾病筛查苯丙酮尿 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit疾病_先天肾病 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit疾病_葡萄糖症 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit1分钟 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit1分钟得分 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit5分钟 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit5分钟得分 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditApgar不详 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit顺产 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit头吸 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit产钳 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit剖宫 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit双多胎 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit臀位 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit出生情况其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit助产机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生孕周 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit母姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父职业 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit父姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit住址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童档案号 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit新生儿窒息 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit听力筛查 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit喂养方式 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit呕吐 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit大便 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit黄疸部位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem出生孕周 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem助产机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem出生情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem新生儿窒息 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem听力筛查 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem疾病筛查 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem喂养方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem面色 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem黄疸部位 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem皮肤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem指导 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem母妊娠疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem是否畸形 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem前卤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem前卤情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem眼外观 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem四肢活动度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem耳外观 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem颈部包块 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem鼻 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem口腔 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem肛门 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem心肺 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem外生殖器官 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem腹部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem脊柱 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem脐带 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem出生体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem目前体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem出生身长 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem体温 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem呼吸频率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem脉率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem胸部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem下次随访地点 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem家长签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem转诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtn说明 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn保存 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).BeginInit();
            this.flowLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit胸部其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt母亲身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt父亲身份证号.Properties)).BeginInit();
            this.flowLayoutPanel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脉率.Properties)).BeginInit();
            this.flowLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit呼吸频率.Properties)).BeginInit();
            this.flowLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体温.Properties)).BeginInit();
            this.flowLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit吃奶次数.Properties)).BeginInit();
            this.flowLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit大便次数.Properties)).BeginInit();
            this.flowLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit吃奶量.Properties)).BeginInit();
            this.flowLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生身长.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit目前体重.Properties)).BeginInit();
            this.flowLayoutPanel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生体重.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit脐带.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脐带其他.Properties)).BeginInit();
            this.flowLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit脊柱.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脊柱其他.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit腹部其他.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit外生殖器.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外生殖器其他.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit肛门.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肛门其他.Properties)).BeginInit();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit心肺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心肺其他.Properties)).BeginInit();
            this.flowLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit口腔.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit口腔其他.Properties)).BeginInit();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit鼻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit鼻其他.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit颈部包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit颈部包块其他.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit耳外观.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit耳外观其他.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit四肢活动度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit四肢活动度其他.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit眼外观.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit眼外观其他.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit前卤情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤情况其他.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤2.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否畸形.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit是否畸形其他.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit母妊娠患病情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母妊娠患病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访地点.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            this.flowLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导科学喂养.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk口服维生素D.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否到位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).BeginInit();
            this.flowLayoutPanel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤未见异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤湿疹.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤糜烂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit皮肤其他.Properties)).BeginInit();
            this.flowLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色未检.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色红润.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色黄染.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit面色其他.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查甲低.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查苯丙酮尿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_先天肾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_葡萄糖症.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit疾病_其他.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1分钟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1分钟得分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5分钟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5分钟得分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApgar不详.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit顺产.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit头吸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit产钳.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit剖宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit双多胎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit臀位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生情况其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit助产机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生孕周.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit新生儿窒息.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit听力筛查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit喂养方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit呕吐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit大便.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit黄疸部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生孕周)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem助产机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem新生儿窒息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem听力筛查)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem疾病筛查)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem喂养方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem面色)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem黄疸部位)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem皮肤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem指导)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母妊娠疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem是否畸形)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem前卤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem前卤情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem眼外观)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem四肢活动度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem耳外观)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem颈部包块)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem鼻)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem口腔)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem肛门)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem心肺)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem外生殖器官)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem腹部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脊柱)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脐带)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem目前体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生身长)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem呼吸频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脉率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem胸部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访地点)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem家长签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem转诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.textEdit家长签名);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel32);
            this.layoutControl1.Controls.Add(this.txt母亲身份证号);
            this.layoutControl1.Controls.Add(this.txt父亲身份证号);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel29);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel27);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel26);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel24);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel25);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel22);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel21);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel20);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel31);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel19);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel17);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel15);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel14);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel18);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit录入时间);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit下次随访地点);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel30);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel28);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel23);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.textEdit助产机构);
            this.layoutControl1.Controls.Add(this.textEdit出生孕周);
            this.layoutControl1.Controls.Add(this.textEdit母出生日期);
            this.layoutControl1.Controls.Add(this.textEdit母联系电话);
            this.layoutControl1.Controls.Add(this.textEdit母职业);
            this.layoutControl1.Controls.Add(this.textEdit母姓名);
            this.layoutControl1.Controls.Add(this.textEdit父出生日期);
            this.layoutControl1.Controls.Add(this.textEdit父联系电话);
            this.layoutControl1.Controls.Add(this.textEdit父职业);
            this.layoutControl1.Controls.Add(this.textEdit父姓名);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit住址);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit儿童姓名);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.textEdit儿童档案号);
            this.layoutControl1.Controls.Add(this.comboBoxEdit新生儿窒息);
            this.layoutControl1.Controls.Add(this.comboBoxEdit听力筛查);
            this.layoutControl1.Controls.Add(this.comboBoxEdit喂养方式);
            this.layoutControl1.Controls.Add(this.comboBoxEdit呕吐);
            this.layoutControl1.Controls.Add(this.comboBoxEdit大便);
            this.layoutControl1.Controls.Add(this.comboBoxEdit黄疸部位);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 34);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.LookAndFeel.UseWindowsXPTheme = true;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.Teal;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(815, 466);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(522, 931);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(260, 15);
            this.layoutControl2.TabIndex = 71;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.OptionsItemText.TextToControlDistance = 5;
            this.Root.Size = new System.Drawing.Size(260, 15);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // textEdit家长签名
            // 
            this.textEdit家长签名.Location = new System.Drawing.Point(614, 903);
            this.textEdit家长签名.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit家长签名.Name = "textEdit家长签名";
            this.textEdit家长签名.Size = new System.Drawing.Size(171, 20);
            this.textEdit家长签名.StyleController = this.layoutControl1;
            this.textEdit家长签名.TabIndex = 61;
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.Controls.Add(this.comboBoxEdit胸部);
            this.flowLayoutPanel32.Controls.Add(this.textEdit胸部其他);
            this.flowLayoutPanel32.Location = new System.Drawing.Point(370, 754);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(412, 20);
            this.flowLayoutPanel32.TabIndex = 56;
            // 
            // comboBoxEdit胸部
            // 
            this.comboBoxEdit胸部.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit胸部.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit胸部.Name = "comboBoxEdit胸部";
            this.comboBoxEdit胸部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit胸部.Size = new System.Drawing.Size(96, 20);
            this.comboBoxEdit胸部.TabIndex = 0;
            this.comboBoxEdit胸部.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit胸部_SelectedIndexChanged);
            // 
            // textEdit胸部其他
            // 
            this.textEdit胸部其他.Location = new System.Drawing.Point(96, 0);
            this.textEdit胸部其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit胸部其他.Name = "textEdit胸部其他";
            this.textEdit胸部其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit胸部其他.TabIndex = 1;
            // 
            // txt母亲身份证号
            // 
            this.txt母亲身份证号.Location = new System.Drawing.Point(469, 190);
            this.txt母亲身份证号.Name = "txt母亲身份证号";
            this.txt母亲身份证号.Size = new System.Drawing.Size(313, 20);
            this.txt母亲身份证号.StyleController = this.layoutControl1;
            this.txt母亲身份证号.TabIndex = 17;
            // 
            // txt父亲身份证号
            // 
            this.txt父亲身份证号.Location = new System.Drawing.Point(103, 190);
            this.txt父亲身份证号.Name = "txt父亲身份证号";
            this.txt父亲身份证号.Size = new System.Drawing.Size(269, 20);
            this.txt父亲身份证号.StyleController = this.layoutControl1;
            this.txt父亲身份证号.TabIndex = 16;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.Controls.Add(this.textEdit脉率);
            this.flowLayoutPanel29.Controls.Add(this.labelControl6);
            this.flowLayoutPanel29.Location = new System.Drawing.Point(101, 519);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(186, 60);
            this.flowLayoutPanel29.TabIndex = 38;
            // 
            // textEdit脉率
            // 
            this.textEdit脉率.Location = new System.Drawing.Point(0, 0);
            this.textEdit脉率.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit脉率.Name = "textEdit脉率";
            this.textEdit脉率.Size = new System.Drawing.Size(79, 20);
            this.textEdit脉率.TabIndex = 0;
            this.textEdit脉率.Leave += new System.EventHandler(this.textEdit脉率_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(82, 3);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(41, 14);
            this.labelControl6.TabIndex = 1;
            this.labelControl6.Text = "次/分钟";
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.Controls.Add(this.textEdit呼吸频率);
            this.flowLayoutPanel27.Controls.Add(this.labelControl5);
            this.flowLayoutPanel27.Location = new System.Drawing.Point(366, 495);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel27.TabIndex = 37;
            // 
            // textEdit呼吸频率
            // 
            this.textEdit呼吸频率.Location = new System.Drawing.Point(0, 0);
            this.textEdit呼吸频率.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit呼吸频率.Name = "textEdit呼吸频率";
            this.textEdit呼吸频率.Size = new System.Drawing.Size(100, 20);
            this.textEdit呼吸频率.TabIndex = 0;
            this.textEdit呼吸频率.Leave += new System.EventHandler(this.textEdit呼吸频率_Leave);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(103, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(41, 14);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "次/分钟";
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.Controls.Add(this.textEdit体温);
            this.flowLayoutPanel26.Controls.Add(this.labelControl4);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(101, 495);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel26.TabIndex = 36;
            // 
            // textEdit体温
            // 
            this.textEdit体温.Location = new System.Drawing.Point(0, 0);
            this.textEdit体温.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit体温.Name = "textEdit体温";
            this.textEdit体温.Size = new System.Drawing.Size(79, 20);
            this.textEdit体温.TabIndex = 0;
            this.textEdit体温.Leave += new System.EventHandler(this.textEdit体温_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(82, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(13, 14);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "℃";
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.Controls.Add(this.textEdit吃奶次数);
            this.flowLayoutPanel24.Controls.Add(this.labelControl2);
            this.flowLayoutPanel24.Location = new System.Drawing.Point(633, 447);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(152, 20);
            this.flowLayoutPanel24.TabIndex = 32;
            // 
            // textEdit吃奶次数
            // 
            this.textEdit吃奶次数.Location = new System.Drawing.Point(0, 0);
            this.textEdit吃奶次数.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit吃奶次数.Name = "textEdit吃奶次数";
            this.textEdit吃奶次数.Size = new System.Drawing.Size(72, 20);
            this.textEdit吃奶次数.TabIndex = 0;
            this.textEdit吃奶次数.Leave += new System.EventHandler(this.textEdit吃奶次数_Leave);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(75, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "次/每日";
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.Controls.Add(this.textEdit大便次数);
            this.flowLayoutPanel25.Controls.Add(this.labelControl3);
            this.flowLayoutPanel25.Location = new System.Drawing.Point(633, 471);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(152, 20);
            this.flowLayoutPanel25.TabIndex = 35;
            // 
            // textEdit大便次数
            // 
            this.textEdit大便次数.Location = new System.Drawing.Point(0, 0);
            this.textEdit大便次数.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit大便次数.Name = "textEdit大便次数";
            this.textEdit大便次数.Size = new System.Drawing.Size(72, 20);
            this.textEdit大便次数.TabIndex = 0;
            this.textEdit大便次数.Leave += new System.EventHandler(this.textEdit大便次数_Leave);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(75, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 14);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "次/每日";
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.Controls.Add(this.textEdit吃奶量);
            this.flowLayoutPanel22.Controls.Add(this.labelControl吃奶量);
            this.flowLayoutPanel22.Location = new System.Drawing.Point(366, 447);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(188, 20);
            this.flowLayoutPanel22.TabIndex = 31;
            // 
            // textEdit吃奶量
            // 
            this.textEdit吃奶量.Location = new System.Drawing.Point(0, 0);
            this.textEdit吃奶量.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit吃奶量.Name = "textEdit吃奶量";
            this.textEdit吃奶量.Size = new System.Drawing.Size(89, 20);
            this.textEdit吃奶量.TabIndex = 0;
            this.textEdit吃奶量.Leave += new System.EventHandler(this.textEdit吃奶量_Leave);
            // 
            // labelControl吃奶量
            // 
            this.labelControl吃奶量.Location = new System.Drawing.Point(92, 3);
            this.labelControl吃奶量.Name = "labelControl吃奶量";
            this.labelControl吃奶量.Size = new System.Drawing.Size(29, 14);
            this.labelControl吃奶量.TabIndex = 1;
            this.labelControl吃奶量.Text = "ml/次";
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.Controls.Add(this.textEdit出生身长);
            this.flowLayoutPanel21.Controls.Add(this.labelControl1);
            this.flowLayoutPanel21.Location = new System.Drawing.Point(633, 423);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(152, 20);
            this.flowLayoutPanel21.TabIndex = 29;
            // 
            // textEdit出生身长
            // 
            this.textEdit出生身长.Location = new System.Drawing.Point(0, 0);
            this.textEdit出生身长.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit出生身长.Name = "textEdit出生身长";
            this.textEdit出生身长.Size = new System.Drawing.Size(72, 20);
            this.textEdit出生身长.TabIndex = 0;
            this.textEdit出生身长.Leave += new System.EventHandler(this.textEdit出生生长_Leave);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(75, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(16, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "cm";
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.Controls.Add(this.textEdit目前体重);
            this.flowLayoutPanel20.Controls.Add(this.labelControl目前体重);
            this.flowLayoutPanel20.Location = new System.Drawing.Point(366, 423);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(188, 20);
            this.flowLayoutPanel20.TabIndex = 28;
            // 
            // textEdit目前体重
            // 
            this.textEdit目前体重.Location = new System.Drawing.Point(0, 0);
            this.textEdit目前体重.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit目前体重.Name = "textEdit目前体重";
            this.textEdit目前体重.Size = new System.Drawing.Size(89, 20);
            this.textEdit目前体重.TabIndex = 0;
            this.textEdit目前体重.Leave += new System.EventHandler(this.textEdit目前体重_Leave);
            // 
            // labelControl目前体重
            // 
            this.labelControl目前体重.Location = new System.Drawing.Point(92, 3);
            this.labelControl目前体重.Name = "labelControl目前体重";
            this.labelControl目前体重.Size = new System.Drawing.Size(13, 14);
            this.labelControl目前体重.TabIndex = 1;
            this.labelControl目前体重.Text = "kg";
            // 
            // flowLayoutPanel31
            // 
            this.flowLayoutPanel31.Controls.Add(this.textEdit出生体重);
            this.flowLayoutPanel31.Controls.Add(this.labelControl出生体重);
            this.flowLayoutPanel31.Location = new System.Drawing.Point(101, 423);
            this.flowLayoutPanel31.Name = "flowLayoutPanel31";
            this.flowLayoutPanel31.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel31.TabIndex = 27;
            // 
            // textEdit出生体重
            // 
            this.textEdit出生体重.Location = new System.Drawing.Point(0, 0);
            this.textEdit出生体重.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit出生体重.Name = "textEdit出生体重";
            this.textEdit出生体重.Size = new System.Drawing.Size(79, 20);
            this.textEdit出生体重.TabIndex = 1;
            this.textEdit出生体重.Leave += new System.EventHandler(this.textEdit出生体重_Leave);
            // 
            // labelControl出生体重
            // 
            this.labelControl出生体重.Location = new System.Drawing.Point(82, 3);
            this.labelControl出生体重.Name = "labelControl出生体重";
            this.labelControl出生体重.Size = new System.Drawing.Size(13, 14);
            this.labelControl出生体重.TabIndex = 2;
            this.labelControl出生体重.Text = "kg";
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.Controls.Add(this.comboBoxEdit脐带);
            this.flowLayoutPanel19.Controls.Add(this.textEdit脐带其他);
            this.flowLayoutPanel19.Location = new System.Drawing.Point(101, 751);
            this.flowLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(187, 26);
            this.flowLayoutPanel19.TabIndex = 55;
            // 
            // comboBoxEdit脐带
            // 
            this.comboBoxEdit脐带.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit脐带.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit脐带.Name = "comboBoxEdit脐带";
            this.comboBoxEdit脐带.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit脐带.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit脐带.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit脐带.TabIndex = 0;
            this.comboBoxEdit脐带.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit脐带_SelectedIndexChanged);
            // 
            // textEdit脐带其他
            // 
            this.textEdit脐带其他.Enabled = false;
            this.textEdit脐带其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit脐带其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit脐带其他.Name = "textEdit脐带其他";
            this.textEdit脐带其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit脐带其他.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.Controls.Add(this.comboBoxEdit脊柱);
            this.flowLayoutPanel17.Controls.Add(this.textEdit脊柱其他);
            this.flowLayoutPanel17.Location = new System.Drawing.Point(366, 727);
            this.flowLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel17.TabIndex = 54;
            // 
            // comboBoxEdit脊柱
            // 
            this.comboBoxEdit脊柱.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit脊柱.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit脊柱.Name = "comboBoxEdit脊柱";
            this.comboBoxEdit脊柱.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit脊柱.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit脊柱.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit脊柱.TabIndex = 0;
            this.comboBoxEdit脊柱.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit脊柱_SelectedIndexChanged);
            // 
            // textEdit脊柱其他
            // 
            this.textEdit脊柱其他.Enabled = false;
            this.textEdit脊柱其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit脊柱其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit脊柱其他.Name = "textEdit脊柱其他";
            this.textEdit脊柱其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit脊柱其他.TabIndex = 1;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.Controls.Add(this.comboBoxEdit腹部);
            this.flowLayoutPanel16.Controls.Add(this.textEdit腹部其他);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(101, 727);
            this.flowLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel16.TabIndex = 53;
            // 
            // comboBoxEdit腹部
            // 
            this.comboBoxEdit腹部.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit腹部.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit腹部.Name = "comboBoxEdit腹部";
            this.comboBoxEdit腹部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit腹部.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit腹部.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit腹部.TabIndex = 0;
            this.comboBoxEdit腹部.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit腹部_SelectedIndexChanged);
            // 
            // textEdit腹部其他
            // 
            this.textEdit腹部其他.Enabled = false;
            this.textEdit腹部其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit腹部其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit腹部其他.Name = "textEdit腹部其他";
            this.textEdit腹部其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit腹部其他.TabIndex = 1;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.comboBoxEdit外生殖器);
            this.flowLayoutPanel15.Controls.Add(this.textEdit外生殖器其他);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(366, 703);
            this.flowLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel15.TabIndex = 52;
            // 
            // comboBoxEdit外生殖器
            // 
            this.comboBoxEdit外生殖器.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit外生殖器.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit外生殖器.Name = "comboBoxEdit外生殖器";
            this.comboBoxEdit外生殖器.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit外生殖器.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit外生殖器.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit外生殖器.TabIndex = 0;
            this.comboBoxEdit外生殖器.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit外生殖器_SelectedIndexChanged);
            // 
            // textEdit外生殖器其他
            // 
            this.textEdit外生殖器其他.Enabled = false;
            this.textEdit外生殖器其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit外生殖器其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit外生殖器其他.Name = "textEdit外生殖器其他";
            this.textEdit外生殖器其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit外生殖器其他.TabIndex = 1;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.comboBoxEdit肛门);
            this.flowLayoutPanel13.Controls.Add(this.textEdit肛门其他);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(366, 679);
            this.flowLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel13.TabIndex = 50;
            // 
            // comboBoxEdit肛门
            // 
            this.comboBoxEdit肛门.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit肛门.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit肛门.Name = "comboBoxEdit肛门";
            this.comboBoxEdit肛门.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit肛门.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit肛门.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit肛门.TabIndex = 0;
            this.comboBoxEdit肛门.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit肛门_SelectedIndexChanged);
            // 
            // textEdit肛门其他
            // 
            this.textEdit肛门其他.Enabled = false;
            this.textEdit肛门其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit肛门其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit肛门其他.Name = "textEdit肛门其他";
            this.textEdit肛门其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit肛门其他.TabIndex = 1;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.Controls.Add(this.comboBoxEdit心肺);
            this.flowLayoutPanel14.Controls.Add(this.textEdit心肺其他);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(101, 703);
            this.flowLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel14.TabIndex = 51;
            // 
            // comboBoxEdit心肺
            // 
            this.comboBoxEdit心肺.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit心肺.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit心肺.Name = "comboBoxEdit心肺";
            this.comboBoxEdit心肺.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit心肺.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit心肺.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit心肺.TabIndex = 0;
            this.comboBoxEdit心肺.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit心肺_SelectedIndexChanged);
            // 
            // textEdit心肺其他
            // 
            this.textEdit心肺其他.Enabled = false;
            this.textEdit心肺其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit心肺其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit心肺其他.Name = "textEdit心肺其他";
            this.textEdit心肺其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit心肺其他.TabIndex = 1;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.Controls.Add(this.comboBoxEdit口腔);
            this.flowLayoutPanel18.Controls.Add(this.textEdit口腔其他);
            this.flowLayoutPanel18.Location = new System.Drawing.Point(101, 679);
            this.flowLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel18.TabIndex = 49;
            // 
            // comboBoxEdit口腔
            // 
            this.comboBoxEdit口腔.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit口腔.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit口腔.Name = "comboBoxEdit口腔";
            this.comboBoxEdit口腔.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit口腔.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit口腔.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit口腔.TabIndex = 0;
            this.comboBoxEdit口腔.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit口腔_SelectedIndexChanged);
            // 
            // textEdit口腔其他
            // 
            this.textEdit口腔其他.Enabled = false;
            this.textEdit口腔其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit口腔其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit口腔其他.Name = "textEdit口腔其他";
            this.textEdit口腔其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit口腔其他.TabIndex = 1;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.comboBoxEdit鼻);
            this.flowLayoutPanel12.Controls.Add(this.textEdit鼻其他);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(101, 655);
            this.flowLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel12.TabIndex = 47;
            // 
            // comboBoxEdit鼻
            // 
            this.comboBoxEdit鼻.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit鼻.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit鼻.Name = "comboBoxEdit鼻";
            this.comboBoxEdit鼻.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit鼻.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit鼻.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit鼻.TabIndex = 0;
            this.comboBoxEdit鼻.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit鼻_SelectedIndexChanged);
            // 
            // textEdit鼻其他
            // 
            this.textEdit鼻其他.Enabled = false;
            this.textEdit鼻其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit鼻其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit鼻其他.Name = "textEdit鼻其他";
            this.textEdit鼻其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit鼻其他.TabIndex = 1;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.comboBoxEdit颈部包块);
            this.flowLayoutPanel11.Controls.Add(this.textEdit颈部包块其他);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(366, 631);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel11.TabIndex = 46;
            // 
            // comboBoxEdit颈部包块
            // 
            this.comboBoxEdit颈部包块.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit颈部包块.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit颈部包块.Name = "comboBoxEdit颈部包块";
            this.comboBoxEdit颈部包块.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit颈部包块.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit颈部包块.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit颈部包块.TabIndex = 0;
            this.comboBoxEdit颈部包块.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit颈部包块_SelectedIndexChanged);
            // 
            // textEdit颈部包块其他
            // 
            this.textEdit颈部包块其他.Enabled = false;
            this.textEdit颈部包块其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit颈部包块其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit颈部包块其他.Name = "textEdit颈部包块其他";
            this.textEdit颈部包块其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit颈部包块其他.TabIndex = 1;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.comboBoxEdit耳外观);
            this.flowLayoutPanel10.Controls.Add(this.textEdit耳外观其他);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(101, 631);
            this.flowLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel10.TabIndex = 45;
            // 
            // comboBoxEdit耳外观
            // 
            this.comboBoxEdit耳外观.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit耳外观.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit耳外观.Name = "comboBoxEdit耳外观";
            this.comboBoxEdit耳外观.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit耳外观.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit耳外观.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit耳外观.TabIndex = 0;
            this.comboBoxEdit耳外观.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit耳外观_SelectedIndexChanged);
            // 
            // textEdit耳外观其他
            // 
            this.textEdit耳外观其他.Enabled = false;
            this.textEdit耳外观其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit耳外观其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit耳外观其他.Name = "textEdit耳外观其他";
            this.textEdit耳外观其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit耳外观其他.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.comboBoxEdit四肢活动度);
            this.flowLayoutPanel6.Controls.Add(this.textEdit四肢活动度其他);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(366, 607);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel6.TabIndex = 44;
            // 
            // comboBoxEdit四肢活动度
            // 
            this.comboBoxEdit四肢活动度.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit四肢活动度.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit四肢活动度.Name = "comboBoxEdit四肢活动度";
            this.comboBoxEdit四肢活动度.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit四肢活动度.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit四肢活动度.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit四肢活动度.TabIndex = 0;
            this.comboBoxEdit四肢活动度.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit四肢活动度_SelectedIndexChanged);
            // 
            // textEdit四肢活动度其他
            // 
            this.textEdit四肢活动度其他.Enabled = false;
            this.textEdit四肢活动度其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit四肢活动度其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit四肢活动度其他.Name = "textEdit四肢活动度其他";
            this.textEdit四肢活动度其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit四肢活动度其他.TabIndex = 1;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.comboBoxEdit眼外观);
            this.flowLayoutPanel9.Controls.Add(this.textEdit眼外观其他);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(101, 607);
            this.flowLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel9.TabIndex = 43;
            // 
            // comboBoxEdit眼外观
            // 
            this.comboBoxEdit眼外观.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit眼外观.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit眼外观.Name = "comboBoxEdit眼外观";
            this.comboBoxEdit眼外观.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit眼外观.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit眼外观.Size = new System.Drawing.Size(81, 20);
            this.comboBoxEdit眼外观.TabIndex = 0;
            this.comboBoxEdit眼外观.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit眼外观_SelectedIndexChanged);
            // 
            // textEdit眼外观其他
            // 
            this.textEdit眼外观其他.Enabled = false;
            this.textEdit眼外观其他.Location = new System.Drawing.Point(81, 0);
            this.textEdit眼外观其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit眼外观其他.Name = "textEdit眼外观其他";
            this.textEdit眼外观其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit眼外观其他.TabIndex = 1;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.comboBoxEdit前卤情况);
            this.flowLayoutPanel8.Controls.Add(this.textEdit前卤情况其他);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(366, 583);
            this.flowLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel8.TabIndex = 42;
            // 
            // comboBoxEdit前卤情况
            // 
            this.comboBoxEdit前卤情况.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit前卤情况.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit前卤情况.Name = "comboBoxEdit前卤情况";
            this.comboBoxEdit前卤情况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit前卤情况.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit前卤情况.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit前卤情况.TabIndex = 0;
            this.comboBoxEdit前卤情况.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit前卤情况_SelectedIndexChanged);
            // 
            // textEdit前卤情况其他
            // 
            this.textEdit前卤情况其他.Enabled = false;
            this.textEdit前卤情况其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit前卤情况其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit前卤情况其他.Name = "textEdit前卤情况其他";
            this.textEdit前卤情况其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit前卤情况其他.TabIndex = 1;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.textEdit前卤1);
            this.flowLayoutPanel7.Controls.Add(this.labelControl47);
            this.flowLayoutPanel7.Controls.Add(this.textEdit前卤2);
            this.flowLayoutPanel7.Controls.Add(this.labelControl46);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(101, 583);
            this.flowLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel7.TabIndex = 41;
            // 
            // textEdit前卤1
            // 
            this.textEdit前卤1.Location = new System.Drawing.Point(0, 0);
            this.textEdit前卤1.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit前卤1.Name = "textEdit前卤1";
            this.textEdit前卤1.Size = new System.Drawing.Size(59, 20);
            this.textEdit前卤1.TabIndex = 0;
            this.textEdit前卤1.Leave += new System.EventHandler(this.textEdit前卤1_Leave);
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(59, 3);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(23, 14);
            this.labelControl47.TabIndex = 1;
            this.labelControl47.Text = "cm*";
            // 
            // textEdit前卤2
            // 
            this.textEdit前卤2.Location = new System.Drawing.Point(82, 0);
            this.textEdit前卤2.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit前卤2.Name = "textEdit前卤2";
            this.textEdit前卤2.Size = new System.Drawing.Size(59, 20);
            this.textEdit前卤2.TabIndex = 2;
            this.textEdit前卤2.Leave += new System.EventHandler(this.textEdit前卤2_Leave);
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(144, 3);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(16, 14);
            this.labelControl46.TabIndex = 3;
            this.labelControl46.Text = "cm";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.comboBoxEdit是否畸形);
            this.flowLayoutPanel2.Controls.Add(this.textEdit是否畸形其他);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(631, 230);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(154, 56);
            this.flowLayoutPanel2.TabIndex = 20;
            // 
            // comboBoxEdit是否畸形
            // 
            this.comboBoxEdit是否畸形.Location = new System.Drawing.Point(1, 1);
            this.comboBoxEdit是否畸形.Margin = new System.Windows.Forms.Padding(1);
            this.comboBoxEdit是否畸形.Name = "comboBoxEdit是否畸形";
            this.comboBoxEdit是否畸形.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否畸形.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit是否畸形.Size = new System.Drawing.Size(71, 20);
            this.comboBoxEdit是否畸形.TabIndex = 0;
            this.comboBoxEdit是否畸形.EditValueChanged += new System.EventHandler(this.comboBoxEdit是否畸形_EditValueChanged);
            // 
            // textEdit是否畸形其他
            // 
            this.textEdit是否畸形其他.Enabled = false;
            this.textEdit是否畸形其他.Location = new System.Drawing.Point(74, 1);
            this.textEdit是否畸形其他.Margin = new System.Windows.Forms.Padding(1);
            this.textEdit是否畸形其他.Name = "textEdit是否畸形其他";
            this.textEdit是否畸形其他.Size = new System.Drawing.Size(67, 20);
            this.textEdit是否畸形其他.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.comboBoxEdit母妊娠患病情况);
            this.flowLayoutPanel1.Controls.Add(this.textEdit母妊娠患病其他);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(366, 230);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(186, 56);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // comboBoxEdit母妊娠患病情况
            // 
            this.comboBoxEdit母妊娠患病情况.Location = new System.Drawing.Point(1, 1);
            this.comboBoxEdit母妊娠患病情况.Margin = new System.Windows.Forms.Padding(1);
            this.comboBoxEdit母妊娠患病情况.Name = "comboBoxEdit母妊娠患病情况";
            this.comboBoxEdit母妊娠患病情况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit母妊娠患病情况.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit母妊娠患病情况.Size = new System.Drawing.Size(92, 20);
            this.comboBoxEdit母妊娠患病情况.TabIndex = 0;
            this.comboBoxEdit母妊娠患病情况.EditValueChanged += new System.EventHandler(this.comboBoxEdit母妊娠患病情况_EditValueChanged);
            // 
            // textEdit母妊娠患病其他
            // 
            this.textEdit母妊娠患病其他.Enabled = false;
            this.textEdit母妊娠患病其他.Location = new System.Drawing.Point(95, 1);
            this.textEdit母妊娠患病其他.Margin = new System.Windows.Forms.Padding(1);
            this.textEdit母妊娠患病其他.Name = "textEdit母妊娠患病其他";
            this.textEdit母妊娠患病其他.Size = new System.Drawing.Size(79, 20);
            this.textEdit母妊娠患病其他.TabIndex = 1;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(349, 977);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(169, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 68;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(617, 977);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(168, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 69;
            // 
            // textEdit录入人
            // 
            this.textEdit录入人.Location = new System.Drawing.Point(351, 953);
            this.textEdit录入人.Name = "textEdit录入人";
            this.textEdit录入人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入人.Properties.ReadOnly = true;
            this.textEdit录入人.Size = new System.Drawing.Size(165, 20);
            this.textEdit录入人.StyleController = this.layoutControl1;
            this.textEdit录入人.TabIndex = 65;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(113, 977);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(133, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 67;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(615, 953);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近更新时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(170, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 66;
            // 
            // textEdit录入时间
            // 
            this.textEdit录入时间.Location = new System.Drawing.Point(113, 953);
            this.textEdit录入时间.Name = "textEdit录入时间";
            this.textEdit录入时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入时间.Properties.ReadOnly = true;
            this.textEdit录入时间.Size = new System.Drawing.Size(134, 20);
            this.textEdit录入时间.StyleController = this.layoutControl1;
            this.textEdit录入时间.TabIndex = 64;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(113, 903);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.Mask.ShowPlaceHolders = false;
            this.dateEdit随访日期.Size = new System.Drawing.Size(133, 20);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 59;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(350, 903);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(165, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 60;
            // 
            // textEdit下次随访地点
            // 
            this.textEdit下次随访地点.Location = new System.Drawing.Point(351, 928);
            this.textEdit下次随访地点.Name = "textEdit下次随访地点";
            this.textEdit下次随访地点.Size = new System.Drawing.Size(164, 20);
            this.textEdit下次随访地点.StyleController = this.layoutControl1;
            this.textEdit下次随访地点.TabIndex = 63;
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(113, 928);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.Mask.ShowPlaceHolders = false;
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(134, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 62;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导科学喂养);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导生长发育);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导疾病预防);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导预防意外);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导口腔保健);
            this.flowLayoutPanel30.Controls.Add(this.chk口服维生素D);
            this.flowLayoutPanel30.Controls.Add(this.checkEdit指导其他);
            this.flowLayoutPanel30.Controls.Add(this.textEdit指导其他);
            this.flowLayoutPanel30.Location = new System.Drawing.Point(101, 781);
            this.flowLayoutPanel30.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(684, 48);
            this.flowLayoutPanel30.TabIndex = 57;
            // 
            // checkEdit指导科学喂养
            // 
            this.checkEdit指导科学喂养.Location = new System.Drawing.Point(0, 0);
            this.checkEdit指导科学喂养.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导科学喂养.Name = "checkEdit指导科学喂养";
            this.checkEdit指导科学喂养.Properties.Caption = "科学喂养";
            this.checkEdit指导科学喂养.Size = new System.Drawing.Size(79, 19);
            this.checkEdit指导科学喂养.TabIndex = 0;
            // 
            // checkEdit指导生长发育
            // 
            this.checkEdit指导生长发育.Location = new System.Drawing.Point(79, 0);
            this.checkEdit指导生长发育.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导生长发育.Name = "checkEdit指导生长发育";
            this.checkEdit指导生长发育.Properties.Caption = "生长发育";
            this.checkEdit指导生长发育.Size = new System.Drawing.Size(72, 19);
            this.checkEdit指导生长发育.TabIndex = 1;
            // 
            // checkEdit指导疾病预防
            // 
            this.checkEdit指导疾病预防.Location = new System.Drawing.Point(151, 0);
            this.checkEdit指导疾病预防.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导疾病预防.Name = "checkEdit指导疾病预防";
            this.checkEdit指导疾病预防.Properties.Caption = "疾病预防";
            this.checkEdit指导疾病预防.Size = new System.Drawing.Size(75, 19);
            this.checkEdit指导疾病预防.TabIndex = 2;
            // 
            // checkEdit指导预防意外
            // 
            this.checkEdit指导预防意外.Location = new System.Drawing.Point(226, 0);
            this.checkEdit指导预防意外.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导预防意外.Name = "checkEdit指导预防意外";
            this.checkEdit指导预防意外.Properties.Caption = "预防意外伤害";
            this.checkEdit指导预防意外.Size = new System.Drawing.Size(99, 19);
            this.checkEdit指导预防意外.TabIndex = 3;
            // 
            // checkEdit指导口腔保健
            // 
            this.checkEdit指导口腔保健.Location = new System.Drawing.Point(325, 0);
            this.checkEdit指导口腔保健.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导口腔保健.Name = "checkEdit指导口腔保健";
            this.checkEdit指导口腔保健.Properties.Caption = "口腔保健";
            this.checkEdit指导口腔保健.Size = new System.Drawing.Size(83, 19);
            this.checkEdit指导口腔保健.TabIndex = 4;
            // 
            // chk口服维生素D
            // 
            this.chk口服维生素D.Location = new System.Drawing.Point(408, 0);
            this.chk口服维生素D.Margin = new System.Windows.Forms.Padding(0);
            this.chk口服维生素D.Name = "chk口服维生素D";
            this.chk口服维生素D.Properties.Caption = "口服维生素D";
            this.chk口服维生素D.Size = new System.Drawing.Size(100, 19);
            this.chk口服维生素D.TabIndex = 7;
            // 
            // checkEdit指导其他
            // 
            this.checkEdit指导其他.Location = new System.Drawing.Point(508, 0);
            this.checkEdit指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导其他.Name = "checkEdit指导其他";
            this.checkEdit指导其他.Properties.Caption = "其他";
            this.checkEdit指导其他.Size = new System.Drawing.Size(53, 19);
            this.checkEdit指导其他.TabIndex = 5;
            this.checkEdit指导其他.CheckedChanged += new System.EventHandler(this.checkEdit指导其他_CheckedChanged);
            // 
            // textEdit指导其他
            // 
            this.textEdit指导其他.Enabled = false;
            this.textEdit指导其他.Location = new System.Drawing.Point(0, 19);
            this.textEdit指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit指导其他.Name = "textEdit指导其他";
            this.textEdit指导其他.Size = new System.Drawing.Size(136, 20);
            this.textEdit指导其他.TabIndex = 6;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.comboBoxEdit是否到位);
            this.panelControl2.Controls.Add(this.textEdit转诊联系电话);
            this.panelControl2.Controls.Add(this.textEdit联系人);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.textEdit转诊机构);
            this.panelControl2.Controls.Add(this.textEdit转诊原因);
            this.panelControl2.Controls.Add(this.labelControl80);
            this.panelControl2.Controls.Add(this.labelControl79);
            this.panelControl2.Controls.Add(this.comboBoxEdit转诊);
            this.panelControl2.Location = new System.Drawing.Point(101, 833);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(684, 66);
            this.panelControl2.TabIndex = 58;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(364, 38);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(36, 14);
            this.labelControl10.TabIndex = 9;
            this.labelControl10.Text = "结果：";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(159, 38);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "联系电话：";
            // 
            // comboBoxEdit是否到位
            // 
            this.comboBoxEdit是否到位.Location = new System.Drawing.Point(411, 34);
            this.comboBoxEdit是否到位.Name = "comboBoxEdit是否到位";
            this.comboBoxEdit是否到位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否到位.Size = new System.Drawing.Size(72, 20);
            this.comboBoxEdit是否到位.TabIndex = 5;
            // 
            // textEdit转诊联系电话
            // 
            this.textEdit转诊联系电话.Location = new System.Drawing.Point(217, 35);
            this.textEdit转诊联系电话.Name = "textEdit转诊联系电话";
            this.textEdit转诊联系电话.Size = new System.Drawing.Size(140, 20);
            this.textEdit转诊联系电话.TabIndex = 4;
            // 
            // textEdit联系人
            // 
            this.textEdit联系人.Location = new System.Drawing.Point(64, 35);
            this.textEdit联系人.Name = "textEdit联系人";
            this.textEdit联系人.Size = new System.Drawing.Size(87, 20);
            this.textEdit联系人.TabIndex = 3;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(11, 38);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(48, 14);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "联系人：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(5, 7);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 14);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "有无转诊：";
            // 
            // textEdit转诊机构
            // 
            this.textEdit转诊机构.Enabled = false;
            this.textEdit转诊机构.Location = new System.Drawing.Point(448, 6);
            this.textEdit转诊机构.Name = "textEdit转诊机构";
            this.textEdit转诊机构.Size = new System.Drawing.Size(149, 20);
            this.textEdit转诊机构.TabIndex = 2;
            // 
            // textEdit转诊原因
            // 
            this.textEdit转诊原因.Enabled = false;
            this.textEdit转诊原因.Location = new System.Drawing.Point(208, 6);
            this.textEdit转诊原因.Name = "textEdit转诊原因";
            this.textEdit转诊原因.Size = new System.Drawing.Size(160, 20);
            this.textEdit转诊原因.TabIndex = 1;
            // 
            // labelControl80
            // 
            this.labelControl80.Location = new System.Drawing.Point(374, 8);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(72, 14);
            this.labelControl80.TabIndex = 1;
            this.labelControl80.Text = "机构及科室：";
            // 
            // labelControl79
            // 
            this.labelControl79.Location = new System.Drawing.Point(134, 8);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(72, 14);
            this.labelControl79.TabIndex = 1;
            this.labelControl79.Text = "原　　　因：";
            // 
            // comboBoxEdit转诊
            // 
            this.comboBoxEdit转诊.Location = new System.Drawing.Point(64, 5);
            this.comboBoxEdit转诊.Name = "comboBoxEdit转诊";
            this.comboBoxEdit转诊.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit转诊.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit转诊.Size = new System.Drawing.Size(64, 20);
            this.comboBoxEdit转诊.TabIndex = 0;
            this.comboBoxEdit转诊.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit转诊_SelectedIndexChanged);
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.Controls.Add(this.checkEdit皮肤未见异常);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit皮肤湿疹);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit皮肤糜烂);
            this.flowLayoutPanel28.Controls.Add(this.checkEdit皮肤其他);
            this.flowLayoutPanel28.Controls.Add(this.textEdit皮肤其他);
            this.flowLayoutPanel28.Location = new System.Drawing.Point(366, 655);
            this.flowLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(419, 20);
            this.flowLayoutPanel28.TabIndex = 48;
            // 
            // checkEdit皮肤未见异常
            // 
            this.checkEdit皮肤未见异常.Location = new System.Drawing.Point(0, 0);
            this.checkEdit皮肤未见异常.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit皮肤未见异常.Name = "checkEdit皮肤未见异常";
            this.checkEdit皮肤未见异常.Properties.Caption = "未见异常";
            this.checkEdit皮肤未见异常.Size = new System.Drawing.Size(79, 19);
            this.checkEdit皮肤未见异常.TabIndex = 0;
            this.checkEdit皮肤未见异常.CheckedChanged += new System.EventHandler(this.checkEdit皮肤未见异常_CheckedChanged);
            // 
            // checkEdit皮肤湿疹
            // 
            this.checkEdit皮肤湿疹.Location = new System.Drawing.Point(79, 0);
            this.checkEdit皮肤湿疹.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit皮肤湿疹.Name = "checkEdit皮肤湿疹";
            this.checkEdit皮肤湿疹.Properties.Caption = "湿疹";
            this.checkEdit皮肤湿疹.Size = new System.Drawing.Size(52, 19);
            this.checkEdit皮肤湿疹.TabIndex = 1;
            // 
            // checkEdit皮肤糜烂
            // 
            this.checkEdit皮肤糜烂.Location = new System.Drawing.Point(131, 0);
            this.checkEdit皮肤糜烂.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit皮肤糜烂.Name = "checkEdit皮肤糜烂";
            this.checkEdit皮肤糜烂.Properties.Caption = "糜烂";
            this.checkEdit皮肤糜烂.Size = new System.Drawing.Size(57, 19);
            this.checkEdit皮肤糜烂.TabIndex = 2;
            // 
            // checkEdit皮肤其他
            // 
            this.checkEdit皮肤其他.Location = new System.Drawing.Point(188, 0);
            this.checkEdit皮肤其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit皮肤其他.Name = "checkEdit皮肤其他";
            this.checkEdit皮肤其他.Properties.Caption = "其他";
            this.checkEdit皮肤其他.Size = new System.Drawing.Size(54, 19);
            this.checkEdit皮肤其他.TabIndex = 3;
            this.checkEdit皮肤其他.CheckedChanged += new System.EventHandler(this.checkEdit皮肤其他_CheckedChanged);
            // 
            // textEdit皮肤其他
            // 
            this.textEdit皮肤其他.Enabled = false;
            this.textEdit皮肤其他.Location = new System.Drawing.Point(242, 0);
            this.textEdit皮肤其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit皮肤其他.Name = "textEdit皮肤其他";
            this.textEdit皮肤其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit皮肤其他.TabIndex = 4;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.Controls.Add(this.checkEdit面色未检);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit面色红润);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit面色黄染);
            this.flowLayoutPanel23.Controls.Add(this.checkEdit面色其他);
            this.flowLayoutPanel23.Controls.Add(this.textEdit面色其他);
            this.flowLayoutPanel23.Location = new System.Drawing.Point(366, 519);
            this.flowLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(190, 60);
            this.flowLayoutPanel23.TabIndex = 39;
            // 
            // checkEdit面色未检
            // 
            this.checkEdit面色未检.Location = new System.Drawing.Point(0, 0);
            this.checkEdit面色未检.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色未检.Name = "checkEdit面色未检";
            this.checkEdit面色未检.Properties.Caption = "未检";
            this.checkEdit面色未检.Size = new System.Drawing.Size(51, 19);
            this.checkEdit面色未检.TabIndex = 0;
            this.checkEdit面色未检.CheckedChanged += new System.EventHandler(this.checkEdit面色未检_CheckedChanged);
            // 
            // checkEdit面色红润
            // 
            this.checkEdit面色红润.Location = new System.Drawing.Point(51, 0);
            this.checkEdit面色红润.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色红润.Name = "checkEdit面色红润";
            this.checkEdit面色红润.Properties.Caption = "红润";
            this.checkEdit面色红润.Size = new System.Drawing.Size(52, 19);
            this.checkEdit面色红润.TabIndex = 1;
            // 
            // checkEdit面色黄染
            // 
            this.checkEdit面色黄染.Location = new System.Drawing.Point(103, 0);
            this.checkEdit面色黄染.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色黄染.Name = "checkEdit面色黄染";
            this.checkEdit面色黄染.Properties.Caption = "黄染";
            this.checkEdit面色黄染.Size = new System.Drawing.Size(59, 19);
            this.checkEdit面色黄染.TabIndex = 2;
            // 
            // checkEdit面色其他
            // 
            this.checkEdit面色其他.Location = new System.Drawing.Point(0, 19);
            this.checkEdit面色其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色其他.Name = "checkEdit面色其他";
            this.checkEdit面色其他.Properties.Caption = "其他";
            this.checkEdit面色其他.Size = new System.Drawing.Size(54, 19);
            this.checkEdit面色其他.TabIndex = 3;
            this.checkEdit面色其他.CheckedChanged += new System.EventHandler(this.checkEdit面色其他_CheckedChanged);
            // 
            // textEdit面色其他
            // 
            this.textEdit面色其他.Enabled = false;
            this.textEdit面色其他.Location = new System.Drawing.Point(54, 19);
            this.textEdit面色其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit面色其他.Name = "textEdit面色其他";
            this.textEdit面色其他.Size = new System.Drawing.Size(53, 20);
            this.textEdit面色其他.TabIndex = 4;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病筛查无);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病筛查甲低);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病筛查苯丙酮尿);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病_先天肾病);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病_葡萄糖症);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit疾病_其他);
            this.flowLayoutPanel5.Controls.Add(this.textEdit疾病_其他);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(366, 378);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(419, 41);
            this.flowLayoutPanel5.TabIndex = 26;
            // 
            // checkEdit疾病筛查无
            // 
            this.checkEdit疾病筛查无.Location = new System.Drawing.Point(0, 0);
            this.checkEdit疾病筛查无.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病筛查无.Name = "checkEdit疾病筛查无";
            this.checkEdit疾病筛查无.Properties.Caption = "无";
            this.checkEdit疾病筛查无.Size = new System.Drawing.Size(40, 19);
            this.checkEdit疾病筛查无.TabIndex = 0;
            this.checkEdit疾病筛查无.CheckedChanged += new System.EventHandler(this.checkEdit疾病筛查无_CheckedChanged);
            // 
            // checkEdit疾病筛查甲低
            // 
            this.checkEdit疾病筛查甲低.Location = new System.Drawing.Point(40, 0);
            this.checkEdit疾病筛查甲低.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病筛查甲低.Name = "checkEdit疾病筛查甲低";
            this.checkEdit疾病筛查甲低.Properties.Caption = "甲低";
            this.checkEdit疾病筛查甲低.Size = new System.Drawing.Size(52, 19);
            this.checkEdit疾病筛查甲低.TabIndex = 1;
            // 
            // checkEdit疾病筛查苯丙酮尿
            // 
            this.checkEdit疾病筛查苯丙酮尿.Location = new System.Drawing.Point(92, 0);
            this.checkEdit疾病筛查苯丙酮尿.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病筛查苯丙酮尿.Name = "checkEdit疾病筛查苯丙酮尿";
            this.checkEdit疾病筛查苯丙酮尿.Properties.Caption = "苯丙酮尿症";
            this.checkEdit疾病筛查苯丙酮尿.Size = new System.Drawing.Size(84, 19);
            this.checkEdit疾病筛查苯丙酮尿.TabIndex = 2;
            // 
            // checkEdit疾病_先天肾病
            // 
            this.checkEdit疾病_先天肾病.Location = new System.Drawing.Point(176, 0);
            this.checkEdit疾病_先天肾病.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病_先天肾病.Name = "checkEdit疾病_先天肾病";
            this.checkEdit疾病_先天肾病.Properties.Caption = "先天性肾上腺皮质激素增生症";
            this.checkEdit疾病_先天肾病.Size = new System.Drawing.Size(177, 19);
            this.checkEdit疾病_先天肾病.TabIndex = 3;
            // 
            // checkEdit疾病_葡萄糖症
            // 
            this.checkEdit疾病_葡萄糖症.Location = new System.Drawing.Point(0, 19);
            this.checkEdit疾病_葡萄糖症.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病_葡萄糖症.Name = "checkEdit疾病_葡萄糖症";
            this.checkEdit疾病_葡萄糖症.Properties.Caption = "葡萄糖-6-磷酸脱氢酶缺乏症";
            this.checkEdit疾病_葡萄糖症.Size = new System.Drawing.Size(172, 19);
            this.checkEdit疾病_葡萄糖症.TabIndex = 4;
            // 
            // checkEdit疾病_其他
            // 
            this.checkEdit疾病_其他.Location = new System.Drawing.Point(172, 19);
            this.checkEdit疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit疾病_其他.Name = "checkEdit疾病_其他";
            this.checkEdit疾病_其他.Properties.Caption = "其他";
            this.checkEdit疾病_其他.Size = new System.Drawing.Size(60, 19);
            this.checkEdit疾病_其他.TabIndex = 5;
            this.checkEdit疾病_其他.CheckedChanged += new System.EventHandler(this.checkEdit疾病_其他_CheckedChanged);
            // 
            // textEdit疾病_其他
            // 
            this.textEdit疾病_其他.Enabled = false;
            this.textEdit疾病_其他.Location = new System.Drawing.Point(232, 19);
            this.textEdit疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit疾病_其他.Name = "textEdit疾病_其他";
            this.textEdit疾病_其他.Size = new System.Drawing.Size(100, 20);
            this.textEdit疾病_其他.TabIndex = 6;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.checkEdit1分钟);
            this.flowLayoutPanel3.Controls.Add(this.textEdit1分钟得分);
            this.flowLayoutPanel3.Controls.Add(this.labelControl21);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit5分钟);
            this.flowLayoutPanel3.Controls.Add(this.textEdit5分钟得分);
            this.flowLayoutPanel3.Controls.Add(this.labelControl20);
            this.flowLayoutPanel3.Controls.Add(this.checkEditApgar不详);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(366, 334);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(419, 40);
            this.flowLayoutPanel3.TabIndex = 24;
            // 
            // checkEdit1分钟
            // 
            this.checkEdit1分钟.Enabled = false;
            this.checkEdit1分钟.Location = new System.Drawing.Point(1, 1);
            this.checkEdit1分钟.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit1分钟.Name = "checkEdit1分钟";
            this.checkEdit1分钟.Properties.Caption = "1分钟";
            this.checkEdit1分钟.Size = new System.Drawing.Size(53, 19);
            this.checkEdit1分钟.TabIndex = 0;
            this.checkEdit1分钟.CheckedChanged += new System.EventHandler(this.checkEdit1分钟_CheckedChanged);
            // 
            // textEdit1分钟得分
            // 
            this.textEdit1分钟得分.Enabled = false;
            this.textEdit1分钟得分.Location = new System.Drawing.Point(56, 1);
            this.textEdit1分钟得分.Margin = new System.Windows.Forms.Padding(1);
            this.textEdit1分钟得分.Name = "textEdit1分钟得分";
            this.textEdit1分钟得分.Size = new System.Drawing.Size(51, 20);
            this.textEdit1分钟得分.TabIndex = 1;
            this.textEdit1分钟得分.Leave += new System.EventHandler(this.textEdit1分钟得分_Leave);
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(109, 1);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(12, 14);
            this.labelControl21.TabIndex = 7;
            this.labelControl21.Text = "分";
            // 
            // checkEdit5分钟
            // 
            this.checkEdit5分钟.Enabled = false;
            this.checkEdit5分钟.Location = new System.Drawing.Point(123, 1);
            this.checkEdit5分钟.Margin = new System.Windows.Forms.Padding(1);
            this.checkEdit5分钟.Name = "checkEdit5分钟";
            this.checkEdit5分钟.Properties.Caption = "5分钟";
            this.checkEdit5分钟.Size = new System.Drawing.Size(53, 19);
            this.checkEdit5分钟.TabIndex = 2;
            this.checkEdit5分钟.CheckedChanged += new System.EventHandler(this.checkEdit5分钟_CheckedChanged);
            // 
            // textEdit5分钟得分
            // 
            this.textEdit5分钟得分.Enabled = false;
            this.textEdit5分钟得分.Location = new System.Drawing.Point(178, 1);
            this.textEdit5分钟得分.Margin = new System.Windows.Forms.Padding(1);
            this.textEdit5分钟得分.Name = "textEdit5分钟得分";
            this.textEdit5分钟得分.Size = new System.Drawing.Size(54, 20);
            this.textEdit5分钟得分.TabIndex = 3;
            this.textEdit5分钟得分.Leave += new System.EventHandler(this.textEdit5分钟得分_Leave);
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(234, 1);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(12, 14);
            this.labelControl20.TabIndex = 6;
            this.labelControl20.Text = "分";
            // 
            // checkEditApgar不详
            // 
            this.checkEditApgar不详.Enabled = false;
            this.checkEditApgar不详.Location = new System.Drawing.Point(248, 1);
            this.checkEditApgar不详.Margin = new System.Windows.Forms.Padding(1);
            this.checkEditApgar不详.Name = "checkEditApgar不详";
            this.checkEditApgar不详.Properties.Caption = "不详";
            this.checkEditApgar不详.Size = new System.Drawing.Size(51, 19);
            this.checkEditApgar不详.TabIndex = 4;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.checkEdit顺产);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit头吸);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit产钳);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit剖宫);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit双多胎);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit臀位);
            this.flowLayoutPanel4.Controls.Add(this.checkEdit其他);
            this.flowLayoutPanel4.Controls.Add(this.textEdit出生情况其他);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(366, 290);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(419, 40);
            this.flowLayoutPanel4.TabIndex = 22;
            // 
            // checkEdit顺产
            // 
            this.checkEdit顺产.Location = new System.Drawing.Point(0, 0);
            this.checkEdit顺产.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit顺产.Name = "checkEdit顺产";
            this.checkEdit顺产.Properties.Caption = "顺产";
            this.checkEdit顺产.Size = new System.Drawing.Size(45, 19);
            this.checkEdit顺产.TabIndex = 0;
            // 
            // checkEdit头吸
            // 
            this.checkEdit头吸.Location = new System.Drawing.Point(45, 0);
            this.checkEdit头吸.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit头吸.Name = "checkEdit头吸";
            this.checkEdit头吸.Properties.Caption = "头吸";
            this.checkEdit头吸.Size = new System.Drawing.Size(47, 19);
            this.checkEdit头吸.TabIndex = 1;
            // 
            // checkEdit产钳
            // 
            this.checkEdit产钳.Location = new System.Drawing.Point(92, 0);
            this.checkEdit产钳.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit产钳.Name = "checkEdit产钳";
            this.checkEdit产钳.Properties.Caption = "产钳";
            this.checkEdit产钳.Size = new System.Drawing.Size(47, 19);
            this.checkEdit产钳.TabIndex = 2;
            // 
            // checkEdit剖宫
            // 
            this.checkEdit剖宫.Location = new System.Drawing.Point(139, 0);
            this.checkEdit剖宫.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit剖宫.Name = "checkEdit剖宫";
            this.checkEdit剖宫.Properties.Caption = "剖宫";
            this.checkEdit剖宫.Size = new System.Drawing.Size(47, 19);
            this.checkEdit剖宫.TabIndex = 3;
            // 
            // checkEdit双多胎
            // 
            this.checkEdit双多胎.Location = new System.Drawing.Point(186, 0);
            this.checkEdit双多胎.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit双多胎.Name = "checkEdit双多胎";
            this.checkEdit双多胎.Properties.Caption = "双多胎";
            this.checkEdit双多胎.Size = new System.Drawing.Size(60, 19);
            this.checkEdit双多胎.TabIndex = 4;
            // 
            // checkEdit臀位
            // 
            this.checkEdit臀位.Location = new System.Drawing.Point(246, 0);
            this.checkEdit臀位.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit臀位.Name = "checkEdit臀位";
            this.checkEdit臀位.Properties.Caption = "臀位";
            this.checkEdit臀位.Size = new System.Drawing.Size(47, 19);
            this.checkEdit臀位.TabIndex = 5;
            // 
            // checkEdit其他
            // 
            this.checkEdit其他.Location = new System.Drawing.Point(293, 0);
            this.checkEdit其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit其他.Name = "checkEdit其他";
            this.checkEdit其他.Properties.Caption = "其他";
            this.checkEdit其他.Size = new System.Drawing.Size(47, 19);
            this.checkEdit其他.TabIndex = 6;
            this.checkEdit其他.CheckedChanged += new System.EventHandler(this.checkEdit其他_CheckedChanged);
            // 
            // textEdit出生情况其他
            // 
            this.textEdit出生情况其他.Enabled = false;
            this.textEdit出生情况其他.Location = new System.Drawing.Point(340, 0);
            this.textEdit出生情况其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit出生情况其他.Name = "textEdit出生情况其他";
            this.textEdit出生情况其他.Size = new System.Drawing.Size(48, 20);
            this.textEdit出生情况其他.TabIndex = 7;
            // 
            // textEdit助产机构
            // 
            this.textEdit助产机构.Location = new System.Drawing.Point(101, 290);
            this.textEdit助产机构.Name = "textEdit助产机构";
            this.textEdit助产机构.Size = new System.Drawing.Size(186, 20);
            this.textEdit助产机构.StyleController = this.layoutControl1;
            this.textEdit助产机构.TabIndex = 21;
            // 
            // textEdit出生孕周
            // 
            this.textEdit出生孕周.Location = new System.Drawing.Point(101, 230);
            this.textEdit出生孕周.Name = "textEdit出生孕周";
            this.textEdit出生孕周.Size = new System.Drawing.Size(186, 20);
            this.textEdit出生孕周.StyleController = this.layoutControl1;
            this.textEdit出生孕周.TabIndex = 18;
            // 
            // textEdit母出生日期
            // 
            this.textEdit母出生日期.Location = new System.Drawing.Point(647, 163);
            this.textEdit母出生日期.Name = "textEdit母出生日期";
            this.textEdit母出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母出生日期.Properties.ReadOnly = true;
            this.textEdit母出生日期.Size = new System.Drawing.Size(138, 20);
            this.textEdit母出生日期.StyleController = this.layoutControl1;
            this.textEdit母出生日期.TabIndex = 15;
            // 
            // textEdit母联系电话
            // 
            this.textEdit母联系电话.Location = new System.Drawing.Point(455, 163);
            this.textEdit母联系电话.Name = "textEdit母联系电话";
            this.textEdit母联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母联系电话.Properties.ReadOnly = true;
            this.textEdit母联系电话.Size = new System.Drawing.Size(123, 20);
            this.textEdit母联系电话.StyleController = this.layoutControl1;
            this.textEdit母联系电话.TabIndex = 14;
            // 
            // textEdit母职业
            // 
            this.textEdit母职业.Location = new System.Drawing.Point(254, 163);
            this.textEdit母职业.Name = "textEdit母职业";
            this.textEdit母职业.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母职业.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母职业.Properties.ReadOnly = true;
            this.textEdit母职业.Size = new System.Drawing.Size(120, 20);
            this.textEdit母职业.StyleController = this.layoutControl1;
            this.textEdit母职业.TabIndex = 13;
            // 
            // textEdit母姓名
            // 
            this.textEdit母姓名.Location = new System.Drawing.Point(90, 163);
            this.textEdit母姓名.Name = "textEdit母姓名";
            this.textEdit母姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit母姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit母姓名.Properties.ReadOnly = true;
            this.textEdit母姓名.Size = new System.Drawing.Size(119, 20);
            this.textEdit母姓名.StyleController = this.layoutControl1;
            this.textEdit母姓名.TabIndex = 12;
            // 
            // textEdit父出生日期
            // 
            this.textEdit父出生日期.Location = new System.Drawing.Point(647, 139);
            this.textEdit父出生日期.Name = "textEdit父出生日期";
            this.textEdit父出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父出生日期.Properties.ReadOnly = true;
            this.textEdit父出生日期.Size = new System.Drawing.Size(138, 20);
            this.textEdit父出生日期.StyleController = this.layoutControl1;
            this.textEdit父出生日期.TabIndex = 11;
            // 
            // textEdit父联系电话
            // 
            this.textEdit父联系电话.Location = new System.Drawing.Point(455, 139);
            this.textEdit父联系电话.Name = "textEdit父联系电话";
            this.textEdit父联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父联系电话.Properties.ReadOnly = true;
            this.textEdit父联系电话.Size = new System.Drawing.Size(123, 20);
            this.textEdit父联系电话.StyleController = this.layoutControl1;
            this.textEdit父联系电话.TabIndex = 10;
            // 
            // textEdit父职业
            // 
            this.textEdit父职业.Location = new System.Drawing.Point(254, 139);
            this.textEdit父职业.Name = "textEdit父职业";
            this.textEdit父职业.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父职业.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父职业.Properties.ReadOnly = true;
            this.textEdit父职业.Size = new System.Drawing.Size(120, 20);
            this.textEdit父职业.StyleController = this.layoutControl1;
            this.textEdit父职业.TabIndex = 9;
            // 
            // textEdit父姓名
            // 
            this.textEdit父姓名.Location = new System.Drawing.Point(90, 139);
            this.textEdit父姓名.Name = "textEdit父姓名";
            this.textEdit父姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit父姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit父姓名.Properties.ReadOnly = true;
            this.textEdit父姓名.Size = new System.Drawing.Size(119, 20);
            this.textEdit父姓名.StyleController = this.layoutControl1;
            this.textEdit父姓名.TabIndex = 8;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(455, 115);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(330, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 7;
            // 
            // textEdit住址
            // 
            this.textEdit住址.Location = new System.Drawing.Point(90, 115);
            this.textEdit住址.Name = "textEdit住址";
            this.textEdit住址.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit住址.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit住址.Properties.ReadOnly = true;
            this.textEdit住址.Size = new System.Drawing.Size(284, 20);
            this.textEdit住址.StyleController = this.layoutControl1;
            this.textEdit住址.TabIndex = 6;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(455, 91);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(330, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 5;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(90, 91);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit性别.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(284, 20);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 4;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(455, 67);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(330, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 3;
            // 
            // textEdit儿童姓名
            // 
            this.textEdit儿童姓名.Location = new System.Drawing.Point(90, 67);
            this.textEdit儿童姓名.Name = "textEdit儿童姓名";
            this.textEdit儿童姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童姓名.Properties.ReadOnly = true;
            this.textEdit儿童姓名.Size = new System.Drawing.Size(284, 20);
            this.textEdit儿童姓名.StyleController = this.layoutControl1;
            this.textEdit儿童姓名.TabIndex = 2;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(90, 43);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Size = new System.Drawing.Size(284, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 0;
            // 
            // textEdit儿童档案号
            // 
            this.textEdit儿童档案号.Location = new System.Drawing.Point(455, 43);
            this.textEdit儿童档案号.Name = "textEdit儿童档案号";
            this.textEdit儿童档案号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童档案号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童档案号.Properties.ReadOnly = true;
            this.textEdit儿童档案号.Size = new System.Drawing.Size(330, 20);
            this.textEdit儿童档案号.StyleController = this.layoutControl1;
            this.textEdit儿童档案号.TabIndex = 1;
            // 
            // comboBoxEdit新生儿窒息
            // 
            this.comboBoxEdit新生儿窒息.Location = new System.Drawing.Point(101, 334);
            this.comboBoxEdit新生儿窒息.Name = "comboBoxEdit新生儿窒息";
            this.comboBoxEdit新生儿窒息.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit新生儿窒息.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit新生儿窒息.Size = new System.Drawing.Size(186, 20);
            this.comboBoxEdit新生儿窒息.StyleController = this.layoutControl1;
            this.comboBoxEdit新生儿窒息.TabIndex = 23;
            this.comboBoxEdit新生儿窒息.EditValueChanged += new System.EventHandler(this.comboBoxEdit新生儿窒息_EditValueChanged);
            // 
            // comboBoxEdit听力筛查
            // 
            this.comboBoxEdit听力筛查.Location = new System.Drawing.Point(101, 378);
            this.comboBoxEdit听力筛查.Name = "comboBoxEdit听力筛查";
            this.comboBoxEdit听力筛查.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit听力筛查.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit听力筛查.Size = new System.Drawing.Size(186, 20);
            this.comboBoxEdit听力筛查.StyleController = this.layoutControl1;
            this.comboBoxEdit听力筛查.TabIndex = 25;
            // 
            // comboBoxEdit喂养方式
            // 
            this.comboBoxEdit喂养方式.Location = new System.Drawing.Point(101, 447);
            this.comboBoxEdit喂养方式.Name = "comboBoxEdit喂养方式";
            this.comboBoxEdit喂养方式.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit喂养方式.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit喂养方式.Size = new System.Drawing.Size(186, 20);
            this.comboBoxEdit喂养方式.StyleController = this.layoutControl1;
            this.comboBoxEdit喂养方式.TabIndex = 30;
            this.comboBoxEdit喂养方式.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit喂养方式_SelectedIndexChanged);
            // 
            // comboBoxEdit呕吐
            // 
            this.comboBoxEdit呕吐.Location = new System.Drawing.Point(101, 471);
            this.comboBoxEdit呕吐.Name = "comboBoxEdit呕吐";
            this.comboBoxEdit呕吐.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit呕吐.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit呕吐.Size = new System.Drawing.Size(186, 20);
            this.comboBoxEdit呕吐.StyleController = this.layoutControl1;
            this.comboBoxEdit呕吐.TabIndex = 33;
            // 
            // comboBoxEdit大便
            // 
            this.comboBoxEdit大便.Location = new System.Drawing.Point(366, 471);
            this.comboBoxEdit大便.Name = "comboBoxEdit大便";
            this.comboBoxEdit大便.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit大便.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit大便.Size = new System.Drawing.Size(188, 20);
            this.comboBoxEdit大便.StyleController = this.layoutControl1;
            this.comboBoxEdit大便.TabIndex = 34;
            // 
            // comboBoxEdit黄疸部位
            // 
            this.comboBoxEdit黄疸部位.Location = new System.Drawing.Point(638, 522);
            this.comboBoxEdit黄疸部位.Name = "comboBoxEdit黄疸部位";
            this.comboBoxEdit黄疸部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit黄疸部位.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit黄疸部位.Size = new System.Drawing.Size(144, 20);
            this.comboBoxEdit黄疸部位.StyleController = this.layoutControl1;
            this.comboBoxEdit黄疸部位.TabIndex = 40;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup1,
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(798, 1021);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "新生儿家庭访视记录表";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 30);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.emptySpaceItem1.Size = new System.Drawing.Size(788, 30);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "新生儿家庭访视记录表";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem10,
            this.layoutControlItem15,
            this.layoutControlItem11,
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem12,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(788, 187);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit卡号;
            this.layoutControlItem2.CustomizationFormText = "卡号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem2.Text = "卡号：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit儿童档案号;
            this.layoutControlItem1.CustomizationFormText = "儿童档案号：";
            this.layoutControlItem1.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem1.Text = "儿童档案号：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(365, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem4.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit儿童姓名;
            this.layoutControlItem3.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem3.Text = "儿童姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit性别;
            this.layoutControlItem5.CustomizationFormText = "性别：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem5.Text = "性别：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit住址;
            this.layoutControlItem7.CustomizationFormText = "住址：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem7.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem7.Text = "住址：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit父姓名;
            this.layoutControlItem9.CustomizationFormText = "父亲姓名：";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem9.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem9.Text = "父亲姓名：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit母姓名;
            this.layoutControlItem13.CustomizationFormText = "母亲姓名：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem13.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem13.Text = "母亲姓名：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.textEdit母职业;
            this.layoutControlItem14.CustomizationFormText = "职业：";
            this.layoutControlItem14.Location = new System.Drawing.Point(200, 120);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem14.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem14.Text = "职业：";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit父职业;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(200, 96);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem10.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem10.Text = "职业：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.textEdit母联系电话;
            this.layoutControlItem15.CustomizationFormText = "联系电话：";
            this.layoutControlItem15.Location = new System.Drawing.Point(365, 120);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem15.Size = new System.Drawing.Size(204, 24);
            this.layoutControlItem15.Text = "联系电话：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit父联系电话;
            this.layoutControlItem11.CustomizationFormText = "联系电话：";
            this.layoutControlItem11.Location = new System.Drawing.Point(365, 96);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem11.Size = new System.Drawing.Size(204, 24);
            this.layoutControlItem11.Text = "联系电话：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话：";
            this.layoutControlItem8.Location = new System.Drawing.Point(365, 72);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem8.Text = "联系电话：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(365, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem6.Size = new System.Drawing.Size(411, 24);
            this.layoutControlItem6.Text = "出生日期：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit父出生日期;
            this.layoutControlItem12.CustomizationFormText = "出生日期：";
            this.layoutControlItem12.Location = new System.Drawing.Point(569, 96);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem12.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem12.Text = "出生日期：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.textEdit母出生日期;
            this.layoutControlItem16.CustomizationFormText = "出生日期：";
            this.layoutControlItem16.Location = new System.Drawing.Point(569, 120);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem16.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem16.Text = "出生日期：";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt父亲身份证号;
            this.layoutControlItem17.CustomizationFormText = "父亲身份证号：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(366, 30);
            this.layoutControlItem17.Text = "父亲身份证号：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.txt母亲身份证号;
            this.layoutControlItem18.CustomizationFormText = "母亲身份证号：";
            this.layoutControlItem18.Location = new System.Drawing.Point(366, 144);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(410, 30);
            this.layoutControlItem18.Text = "母亲身份证号：";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem出生孕周,
            this.layoutControlItem助产机构,
            this.layoutControlItem出生情况,
            this.layoutControlItem新生儿窒息,
            this.layoutControlItem28,
            this.layoutControlItem听力筛查,
            this.layoutControlItem疾病筛查,
            this.layoutControlItem喂养方式,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem面色,
            this.layoutControlItem黄疸部位,
            this.layoutControlItem皮肤,
            this.layoutControlItem指导,
            this.layoutControlItem下次随访日期,
            this.layoutControlItem77,
            this.layoutControlItem79,
            this.layoutControlItem母妊娠疾病,
            this.layoutControlItem是否畸形,
            this.layoutControlItem前卤,
            this.layoutControlItem前卤情况,
            this.layoutControlItem眼外观,
            this.layoutControlItem四肢活动度,
            this.layoutControlItem耳外观,
            this.layoutControlItem颈部包块,
            this.layoutControlItem鼻,
            this.layoutControlItem口腔,
            this.layoutControlItem肛门,
            this.layoutControlItem心肺,
            this.layoutControlItem外生殖器官,
            this.layoutControlItem腹部,
            this.layoutControlItem脊柱,
            this.layoutControlItem脐带,
            this.emptySpaceItem2,
            this.layoutControlItem出生体重,
            this.layoutControlItem目前体重,
            this.layoutControlItem出生身长,
            this.layoutControlItem25,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem体温,
            this.layoutControlItem呼吸频率,
            this.layoutControlItem脉率,
            this.layoutControlItem胸部,
            this.layoutControlItem随访日期,
            this.layoutControlItem78,
            this.layoutControlItem随访医生,
            this.layoutControlItem下次随访地点,
            this.layoutControlItem家长签名,
            this.layoutControlItem80,
            this.layoutControlItem82,
            this.layoutControlItem81,
            this.layoutControlItem19,
            this.layoutControlItem转诊});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 217);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(788, 794);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem出生孕周
            // 
            this.layoutControlItem出生孕周.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem出生孕周.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem出生孕周.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem出生孕周.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem出生孕周.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem出生孕周.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem出生孕周.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItem出生孕周.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem出生孕周.Control = this.textEdit出生孕周;
            this.layoutControlItem出生孕周.CustomizationFormText = "出生孕周：";
            this.layoutControlItem出生孕周.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem出生孕周.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem出生孕周.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem出生孕周.Name = "layoutControlItem出生孕周";
            this.layoutControlItem出生孕周.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem出生孕周.Size = new System.Drawing.Size(278, 60);
            this.layoutControlItem出生孕周.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem出生孕周.Tag = "check";
            this.layoutControlItem出生孕周.Text = "出生孕周：";
            this.layoutControlItem出生孕周.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem出生孕周.TextSize = new System.Drawing.Size(83, 14);
            this.layoutControlItem出生孕周.TextToControlDistance = 5;
            // 
            // layoutControlItem助产机构
            // 
            this.layoutControlItem助产机构.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem助产机构.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem助产机构.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem助产机构.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem助产机构.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem助产机构.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem助产机构.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem助产机构.Control = this.textEdit助产机构;
            this.layoutControlItem助产机构.CustomizationFormText = "助产机构名称：";
            this.layoutControlItem助产机构.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem助产机构.MaxSize = new System.Drawing.Size(0, 440);
            this.layoutControlItem助产机构.MinSize = new System.Drawing.Size(129, 44);
            this.layoutControlItem助产机构.Name = "layoutControlItem助产机构";
            this.layoutControlItem助产机构.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem助产机构.Size = new System.Drawing.Size(278, 44);
            this.layoutControlItem助产机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem助产机构.Tag = "check";
            this.layoutControlItem助产机构.Text = "助产机构名称：";
            this.layoutControlItem助产机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem助产机构.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem助产机构.TextToControlDistance = 5;
            // 
            // layoutControlItem出生情况
            // 
            this.layoutControlItem出生情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem出生情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem出生情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem出生情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem出生情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem出生情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem出生情况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem出生情况.Control = this.flowLayoutPanel4;
            this.layoutControlItem出生情况.CustomizationFormText = "出生情况：";
            this.layoutControlItem出生情况.Location = new System.Drawing.Point(278, 60);
            this.layoutControlItem出生情况.Name = "layoutControlItem出生情况";
            this.layoutControlItem出生情况.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem出生情况.Size = new System.Drawing.Size(498, 44);
            this.layoutControlItem出生情况.Tag = "check";
            this.layoutControlItem出生情况.Text = "出生情况：";
            this.layoutControlItem出生情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem出生情况.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem出生情况.TextToControlDistance = 5;
            // 
            // layoutControlItem新生儿窒息
            // 
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem新生儿窒息.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem新生儿窒息.Control = this.comboBoxEdit新生儿窒息;
            this.layoutControlItem新生儿窒息.CustomizationFormText = "新生儿窒息：";
            this.layoutControlItem新生儿窒息.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem新生儿窒息.MaxSize = new System.Drawing.Size(0, 44);
            this.layoutControlItem新生儿窒息.MinSize = new System.Drawing.Size(129, 44);
            this.layoutControlItem新生儿窒息.Name = "layoutControlItem新生儿窒息";
            this.layoutControlItem新生儿窒息.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem新生儿窒息.Size = new System.Drawing.Size(278, 44);
            this.layoutControlItem新生儿窒息.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem新生儿窒息.Tag = "check";
            this.layoutControlItem新生儿窒息.Text = "新生儿窒息：";
            this.layoutControlItem新生儿窒息.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem新生儿窒息.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem新生儿窒息.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.flowLayoutPanel3;
            this.layoutControlItem28.CustomizationFormText = "Apgar评分：";
            this.layoutControlItem28.Location = new System.Drawing.Point(278, 104);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem28.Size = new System.Drawing.Size(498, 44);
            this.layoutControlItem28.Text = "Apgar评分：";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem听力筛查
            // 
            this.layoutControlItem听力筛查.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem听力筛查.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem听力筛查.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem听力筛查.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem听力筛查.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem听力筛查.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem听力筛查.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem听力筛查.Control = this.comboBoxEdit听力筛查;
            this.layoutControlItem听力筛查.CustomizationFormText = "新生儿听力筛查：";
            this.layoutControlItem听力筛查.Location = new System.Drawing.Point(0, 148);
            this.layoutControlItem听力筛查.MaxSize = new System.Drawing.Size(0, 44);
            this.layoutControlItem听力筛查.MinSize = new System.Drawing.Size(129, 44);
            this.layoutControlItem听力筛查.Name = "layoutControlItem听力筛查";
            this.layoutControlItem听力筛查.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem听力筛查.Size = new System.Drawing.Size(278, 45);
            this.layoutControlItem听力筛查.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem听力筛查.Tag = "check";
            this.layoutControlItem听力筛查.Text = "新生儿听力筛查：";
            this.layoutControlItem听力筛查.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem听力筛查.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem听力筛查.TextToControlDistance = 5;
            // 
            // layoutControlItem疾病筛查
            // 
            this.layoutControlItem疾病筛查.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem疾病筛查.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem疾病筛查.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem疾病筛查.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem疾病筛查.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem疾病筛查.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem疾病筛查.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem疾病筛查.Control = this.flowLayoutPanel5;
            this.layoutControlItem疾病筛查.CustomizationFormText = "新生儿疾病筛查：";
            this.layoutControlItem疾病筛查.Location = new System.Drawing.Point(278, 148);
            this.layoutControlItem疾病筛查.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem疾病筛查.MinSize = new System.Drawing.Size(213, 45);
            this.layoutControlItem疾病筛查.Name = "layoutControlItem疾病筛查";
            this.layoutControlItem疾病筛查.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem疾病筛查.Size = new System.Drawing.Size(498, 45);
            this.layoutControlItem疾病筛查.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem疾病筛查.Tag = "check";
            this.layoutControlItem疾病筛查.Text = "新生儿疾病筛查：";
            this.layoutControlItem疾病筛查.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem疾病筛查.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem疾病筛查.TextToControlDistance = 5;
            // 
            // layoutControlItem喂养方式
            // 
            this.layoutControlItem喂养方式.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem喂养方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem喂养方式.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem喂养方式.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem喂养方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem喂养方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem喂养方式.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem喂养方式.Control = this.comboBoxEdit喂养方式;
            this.layoutControlItem喂养方式.CustomizationFormText = "喂养方式：";
            this.layoutControlItem喂养方式.Location = new System.Drawing.Point(0, 217);
            this.layoutControlItem喂养方式.Name = "layoutControlItem喂养方式";
            this.layoutControlItem喂养方式.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem喂养方式.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem喂养方式.Tag = "check";
            this.layoutControlItem喂养方式.Text = "喂养方式：";
            this.layoutControlItem喂养方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem喂养方式.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem喂养方式.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.comboBoxEdit呕吐;
            this.layoutControlItem34.CustomizationFormText = "呕吐：";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 241);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem34.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem34.Text = "呕吐：";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(83, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.comboBoxEdit大便;
            this.layoutControlItem35.CustomizationFormText = "大便：";
            this.layoutControlItem35.Location = new System.Drawing.Point(278, 241);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem35.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem35.Text = "大便：";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem面色
            // 
            this.layoutControlItem面色.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem面色.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem面色.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem面色.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem面色.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem面色.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem面色.Control = this.flowLayoutPanel23;
            this.layoutControlItem面色.CustomizationFormText = "面色：";
            this.layoutControlItem面色.Location = new System.Drawing.Point(278, 289);
            this.layoutControlItem面色.MinSize = new System.Drawing.Size(167, 64);
            this.layoutControlItem面色.Name = "layoutControlItem面色";
            this.layoutControlItem面色.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem面色.Size = new System.Drawing.Size(269, 64);
            this.layoutControlItem面色.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem面色.Tag = "check";
            this.layoutControlItem面色.Text = "面色：";
            this.layoutControlItem面色.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem面色.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem面色.TextToControlDistance = 5;
            // 
            // layoutControlItem黄疸部位
            // 
            this.layoutControlItem黄疸部位.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem黄疸部位.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem黄疸部位.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem黄疸部位.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem黄疸部位.Control = this.comboBoxEdit黄疸部位;
            this.layoutControlItem黄疸部位.CustomizationFormText = "黄疸部位：";
            this.layoutControlItem黄疸部位.Location = new System.Drawing.Point(547, 289);
            this.layoutControlItem黄疸部位.Name = "layoutControlItem黄疸部位";
            this.layoutControlItem黄疸部位.Size = new System.Drawing.Size(229, 64);
            this.layoutControlItem黄疸部位.Tag = "check";
            this.layoutControlItem黄疸部位.Text = "黄疸部位：";
            this.layoutControlItem黄疸部位.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem黄疸部位.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem黄疸部位.TextToControlDistance = 5;
            // 
            // layoutControlItem皮肤
            // 
            this.layoutControlItem皮肤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem皮肤.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem皮肤.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem皮肤.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem皮肤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem皮肤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem皮肤.Control = this.flowLayoutPanel28;
            this.layoutControlItem皮肤.CustomizationFormText = "皮肤：";
            this.layoutControlItem皮肤.Location = new System.Drawing.Point(278, 425);
            this.layoutControlItem皮肤.Name = "layoutControlItem皮肤";
            this.layoutControlItem皮肤.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem皮肤.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem皮肤.Tag = "check";
            this.layoutControlItem皮肤.Text = "皮肤：";
            this.layoutControlItem皮肤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem皮肤.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem皮肤.TextToControlDistance = 5;
            // 
            // layoutControlItem指导
            // 
            this.layoutControlItem指导.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem指导.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem指导.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem指导.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem指导.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem指导.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem指导.Control = this.flowLayoutPanel30;
            this.layoutControlItem指导.CustomizationFormText = "指导：";
            this.layoutControlItem指导.Location = new System.Drawing.Point(0, 551);
            this.layoutControlItem指导.MinSize = new System.Drawing.Size(192, 52);
            this.layoutControlItem指导.Name = "layoutControlItem指导";
            this.layoutControlItem指导.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem指导.Size = new System.Drawing.Size(776, 52);
            this.layoutControlItem指导.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem指导.Tag = "check";
            this.layoutControlItem指导.Text = "指导：";
            this.layoutControlItem指导.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem指导.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem指导.TextToControlDistance = 5;
            // 
            // layoutControlItem下次随访日期
            // 
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem下次随访日期.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem下次随访日期.Control = this.dateEdit下次随访日期;
            this.layoutControlItem下次随访日期.CustomizationFormText = "下次随访日期：";
            this.layoutControlItem下次随访日期.Location = new System.Drawing.Point(0, 698);
            this.layoutControlItem下次随访日期.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem下次随访日期.MinSize = new System.Drawing.Size(129, 25);
            this.layoutControlItem下次随访日期.Name = "layoutControlItem下次随访日期";
            this.layoutControlItem下次随访日期.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem下次随访日期.Size = new System.Drawing.Size(238, 25);
            this.layoutControlItem下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem下次随访日期.Tag = "check";
            this.layoutControlItem下次随访日期.Text = "下次随访日期：";
            this.layoutControlItem下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem下次随访日期.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem下次随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem77.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem77.Control = this.textEdit录入时间;
            this.layoutControlItem77.CustomizationFormText = "录入时间：";
            this.layoutControlItem77.Location = new System.Drawing.Point(0, 723);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem77.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem77.Text = "录入时间：";
            this.layoutControlItem77.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem77.TextSize = new System.Drawing.Size(95, 14);
            this.layoutControlItem77.TextToControlDistance = 5;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem79.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem79.Control = this.textEdit创建机构;
            this.layoutControlItem79.CustomizationFormText = "创建机构：";
            this.layoutControlItem79.Location = new System.Drawing.Point(0, 747);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem79.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem79.Text = "创建机构：";
            this.layoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem79.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem79.TextToControlDistance = 5;
            // 
            // layoutControlItem母妊娠疾病
            // 
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem母妊娠疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem母妊娠疾病.Control = this.flowLayoutPanel1;
            this.layoutControlItem母妊娠疾病.CustomizationFormText = "母亲妊娠期患病疾病情况：";
            this.layoutControlItem母妊娠疾病.Location = new System.Drawing.Point(278, 0);
            this.layoutControlItem母妊娠疾病.MinSize = new System.Drawing.Size(185, 60);
            this.layoutControlItem母妊娠疾病.Name = "layoutControlItem母妊娠疾病";
            this.layoutControlItem母妊娠疾病.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem母妊娠疾病.Size = new System.Drawing.Size(265, 60);
            this.layoutControlItem母妊娠疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem母妊娠疾病.Tag = "check";
            this.layoutControlItem母妊娠疾病.Text = "母亲妊娠期患病疾病情况：";
            this.layoutControlItem母妊娠疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem母妊娠疾病.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem母妊娠疾病.TextToControlDistance = 5;
            // 
            // layoutControlItem是否畸形
            // 
            this.layoutControlItem是否畸形.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem是否畸形.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem是否畸形.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem是否畸形.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem是否畸形.Control = this.flowLayoutPanel2;
            this.layoutControlItem是否畸形.CustomizationFormText = "是否畸型：";
            this.layoutControlItem是否畸形.Location = new System.Drawing.Point(543, 0);
            this.layoutControlItem是否畸形.Name = "layoutControlItem是否畸形";
            this.layoutControlItem是否畸形.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem是否畸形.Size = new System.Drawing.Size(233, 60);
            this.layoutControlItem是否畸形.Tag = "check";
            this.layoutControlItem是否畸形.Text = "是否有畸型：";
            this.layoutControlItem是否畸形.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem是否畸形.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem是否畸形.TextToControlDistance = 5;
            // 
            // layoutControlItem前卤
            // 
            this.layoutControlItem前卤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem前卤.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem前卤.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem前卤.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem前卤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem前卤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem前卤.Control = this.flowLayoutPanel7;
            this.layoutControlItem前卤.CustomizationFormText = "前卤：";
            this.layoutControlItem前卤.Location = new System.Drawing.Point(0, 353);
            this.layoutControlItem前卤.Name = "layoutControlItem前卤";
            this.layoutControlItem前卤.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem前卤.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem前卤.Tag = "check";
            this.layoutControlItem前卤.Text = "前囟：";
            this.layoutControlItem前卤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem前卤.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem前卤.TextToControlDistance = 5;
            // 
            // layoutControlItem前卤情况
            // 
            this.layoutControlItem前卤情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem前卤情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem前卤情况.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem前卤情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem前卤情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem前卤情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem前卤情况.Control = this.flowLayoutPanel8;
            this.layoutControlItem前卤情况.CustomizationFormText = "前卤情况：";
            this.layoutControlItem前卤情况.Location = new System.Drawing.Point(278, 353);
            this.layoutControlItem前卤情况.Name = "layoutControlItem前卤情况";
            this.layoutControlItem前卤情况.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem前卤情况.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem前卤情况.Tag = "check";
            this.layoutControlItem前卤情况.Text = "前囟情况：";
            this.layoutControlItem前卤情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem前卤情况.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem前卤情况.TextToControlDistance = 5;
            // 
            // layoutControlItem眼外观
            // 
            this.layoutControlItem眼外观.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem眼外观.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem眼外观.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem眼外观.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem眼外观.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem眼外观.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem眼外观.Control = this.flowLayoutPanel9;
            this.layoutControlItem眼外观.CustomizationFormText = "眼外观：";
            this.layoutControlItem眼外观.Location = new System.Drawing.Point(0, 377);
            this.layoutControlItem眼外观.Name = "layoutControlItem眼外观";
            this.layoutControlItem眼外观.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem眼外观.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem眼外观.Tag = "check";
            this.layoutControlItem眼外观.Text = "眼睛：";
            this.layoutControlItem眼外观.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem眼外观.TextSize = new System.Drawing.Size(83, 14);
            this.layoutControlItem眼外观.TextToControlDistance = 5;
            // 
            // layoutControlItem四肢活动度
            // 
            this.layoutControlItem四肢活动度.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem四肢活动度.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem四肢活动度.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem四肢活动度.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem四肢活动度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem四肢活动度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem四肢活动度.Control = this.flowLayoutPanel6;
            this.layoutControlItem四肢活动度.CustomizationFormText = "四肢活动度：";
            this.layoutControlItem四肢活动度.Location = new System.Drawing.Point(278, 377);
            this.layoutControlItem四肢活动度.Name = "layoutControlItem四肢活动度";
            this.layoutControlItem四肢活动度.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem四肢活动度.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem四肢活动度.Tag = "check";
            this.layoutControlItem四肢活动度.Text = "四肢活动度:";
            this.layoutControlItem四肢活动度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem四肢活动度.TextSize = new System.Drawing.Size(70, 14);
            this.layoutControlItem四肢活动度.TextToControlDistance = 5;
            // 
            // layoutControlItem耳外观
            // 
            this.layoutControlItem耳外观.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem耳外观.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem耳外观.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem耳外观.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem耳外观.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem耳外观.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem耳外观.Control = this.flowLayoutPanel10;
            this.layoutControlItem耳外观.CustomizationFormText = "耳外观：";
            this.layoutControlItem耳外观.Location = new System.Drawing.Point(0, 401);
            this.layoutControlItem耳外观.Name = "layoutControlItem耳外观";
            this.layoutControlItem耳外观.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem耳外观.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem耳外观.Tag = "check";
            this.layoutControlItem耳外观.Text = "耳外观：";
            this.layoutControlItem耳外观.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem耳外观.TextSize = new System.Drawing.Size(83, 14);
            this.layoutControlItem耳外观.TextToControlDistance = 5;
            // 
            // layoutControlItem颈部包块
            // 
            this.layoutControlItem颈部包块.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem颈部包块.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem颈部包块.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem颈部包块.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem颈部包块.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem颈部包块.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem颈部包块.Control = this.flowLayoutPanel11;
            this.layoutControlItem颈部包块.CustomizationFormText = "颈部包块：";
            this.layoutControlItem颈部包块.Location = new System.Drawing.Point(278, 401);
            this.layoutControlItem颈部包块.Name = "layoutControlItem颈部包块";
            this.layoutControlItem颈部包块.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem颈部包块.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem颈部包块.Tag = "check";
            this.layoutControlItem颈部包块.Text = "颈部包块：";
            this.layoutControlItem颈部包块.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem颈部包块.TextSize = new System.Drawing.Size(70, 14);
            this.layoutControlItem颈部包块.TextToControlDistance = 5;
            // 
            // layoutControlItem鼻
            // 
            this.layoutControlItem鼻.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem鼻.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem鼻.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem鼻.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem鼻.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem鼻.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem鼻.Control = this.flowLayoutPanel12;
            this.layoutControlItem鼻.CustomizationFormText = "鼻：";
            this.layoutControlItem鼻.Location = new System.Drawing.Point(0, 425);
            this.layoutControlItem鼻.Name = "layoutControlItem鼻";
            this.layoutControlItem鼻.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem鼻.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem鼻.Tag = "check";
            this.layoutControlItem鼻.Text = "鼻：";
            this.layoutControlItem鼻.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem鼻.TextSize = new System.Drawing.Size(83, 14);
            this.layoutControlItem鼻.TextToControlDistance = 5;
            // 
            // layoutControlItem口腔
            // 
            this.layoutControlItem口腔.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem口腔.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem口腔.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem口腔.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem口腔.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem口腔.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem口腔.Control = this.flowLayoutPanel18;
            this.layoutControlItem口腔.CustomizationFormText = "口腔：";
            this.layoutControlItem口腔.Location = new System.Drawing.Point(0, 449);
            this.layoutControlItem口腔.Name = "layoutControlItem口腔";
            this.layoutControlItem口腔.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem口腔.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem口腔.Tag = "check";
            this.layoutControlItem口腔.Text = "口腔：";
            this.layoutControlItem口腔.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem口腔.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem口腔.TextToControlDistance = 5;
            // 
            // layoutControlItem肛门
            // 
            this.layoutControlItem肛门.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem肛门.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem肛门.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem肛门.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem肛门.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem肛门.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem肛门.Control = this.flowLayoutPanel13;
            this.layoutControlItem肛门.CustomizationFormText = "肛门:";
            this.layoutControlItem肛门.Location = new System.Drawing.Point(278, 449);
            this.layoutControlItem肛门.Name = "layoutControlItem肛门";
            this.layoutControlItem肛门.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem肛门.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem肛门.Tag = "check";
            this.layoutControlItem肛门.Text = "肛门:";
            this.layoutControlItem肛门.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem肛门.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem肛门.TextToControlDistance = 5;
            // 
            // layoutControlItem心肺
            // 
            this.layoutControlItem心肺.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem心肺.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem心肺.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem心肺.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem心肺.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem心肺.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem心肺.Control = this.flowLayoutPanel14;
            this.layoutControlItem心肺.CustomizationFormText = "心肺：";
            this.layoutControlItem心肺.Location = new System.Drawing.Point(0, 473);
            this.layoutControlItem心肺.Name = "layoutControlItem心肺";
            this.layoutControlItem心肺.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem心肺.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem心肺.Tag = "check";
            this.layoutControlItem心肺.Text = "心肺：";
            this.layoutControlItem心肺.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem心肺.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem心肺.TextToControlDistance = 5;
            // 
            // layoutControlItem外生殖器官
            // 
            this.layoutControlItem外生殖器官.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem外生殖器官.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem外生殖器官.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem外生殖器官.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem外生殖器官.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem外生殖器官.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem外生殖器官.Control = this.flowLayoutPanel15;
            this.layoutControlItem外生殖器官.CustomizationFormText = "外生殖器官:";
            this.layoutControlItem外生殖器官.Location = new System.Drawing.Point(278, 473);
            this.layoutControlItem外生殖器官.Name = "layoutControlItem外生殖器官";
            this.layoutControlItem外生殖器官.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem外生殖器官.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem外生殖器官.Tag = "check";
            this.layoutControlItem外生殖器官.Text = "外生殖器官:";
            this.layoutControlItem外生殖器官.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem外生殖器官.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem外生殖器官.TextToControlDistance = 5;
            // 
            // layoutControlItem腹部
            // 
            this.layoutControlItem腹部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem腹部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem腹部.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem腹部.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem腹部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem腹部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem腹部.Control = this.flowLayoutPanel16;
            this.layoutControlItem腹部.CustomizationFormText = "腹部：";
            this.layoutControlItem腹部.Location = new System.Drawing.Point(0, 497);
            this.layoutControlItem腹部.Name = "layoutControlItem腹部";
            this.layoutControlItem腹部.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem腹部.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem腹部.Tag = "check";
            this.layoutControlItem腹部.Text = "腹部：";
            this.layoutControlItem腹部.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem腹部.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem腹部.TextToControlDistance = 5;
            // 
            // layoutControlItem脊柱
            // 
            this.layoutControlItem脊柱.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem脊柱.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem脊柱.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem脊柱.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem脊柱.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem脊柱.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem脊柱.Control = this.flowLayoutPanel17;
            this.layoutControlItem脊柱.CustomizationFormText = "脊柱:";
            this.layoutControlItem脊柱.Location = new System.Drawing.Point(278, 497);
            this.layoutControlItem脊柱.Name = "layoutControlItem脊柱";
            this.layoutControlItem脊柱.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem脊柱.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem脊柱.Tag = "check";
            this.layoutControlItem脊柱.Text = "脊柱:";
            this.layoutControlItem脊柱.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem脊柱.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem脊柱.TextToControlDistance = 5;
            // 
            // layoutControlItem脐带
            // 
            this.layoutControlItem脐带.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem脐带.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem脐带.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem脐带.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem脐带.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem脐带.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem脐带.Control = this.flowLayoutPanel19;
            this.layoutControlItem脐带.CustomizationFormText = "脐带：";
            this.layoutControlItem脐带.Location = new System.Drawing.Point(0, 521);
            this.layoutControlItem脐带.Name = "layoutControlItem脐带";
            this.layoutControlItem脐带.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem脐带.Size = new System.Drawing.Size(279, 30);
            this.layoutControlItem脐带.Tag = "check";
            this.layoutControlItem脐带.Text = "脐带：";
            this.layoutControlItem脐带.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem脐带.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem脐带.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 771);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(776, 10);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem出生体重
            // 
            this.layoutControlItem出生体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem出生体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem出生体重.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem出生体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem出生体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem出生体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem出生体重.Control = this.flowLayoutPanel31;
            this.layoutControlItem出生体重.CustomizationFormText = "出生体重：";
            this.layoutControlItem出生体重.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem出生体重.Name = "layoutControlItem出生体重";
            this.layoutControlItem出生体重.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem出生体重.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem出生体重.Tag = "check";
            this.layoutControlItem出生体重.Text = "出生体重：";
            this.layoutControlItem出生体重.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem出生体重.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem出生体重.TextToControlDistance = 5;
            // 
            // layoutControlItem目前体重
            // 
            this.layoutControlItem目前体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem目前体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem目前体重.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem目前体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem目前体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem目前体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem目前体重.Control = this.flowLayoutPanel20;
            this.layoutControlItem目前体重.CustomizationFormText = "目前体重：";
            this.layoutControlItem目前体重.Location = new System.Drawing.Point(278, 193);
            this.layoutControlItem目前体重.Name = "layoutControlItem目前体重";
            this.layoutControlItem目前体重.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem目前体重.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem目前体重.Tag = "check";
            this.layoutControlItem目前体重.Text = "目前体重：";
            this.layoutControlItem目前体重.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem目前体重.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem目前体重.TextToControlDistance = 5;
            // 
            // layoutControlItem出生身长
            // 
            this.layoutControlItem出生身长.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem出生身长.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem出生身长.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem出生身长.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem出生身长.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem出生身长.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem出生身长.Control = this.flowLayoutPanel21;
            this.layoutControlItem出生身长.CustomizationFormText = "出生身长:";
            this.layoutControlItem出生身长.Location = new System.Drawing.Point(545, 193);
            this.layoutControlItem出生身长.Name = "layoutControlItem出生身长";
            this.layoutControlItem出生身长.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem出生身长.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem出生身长.Tag = "check";
            this.layoutControlItem出生身长.Text = "出生身长:";
            this.layoutControlItem出生身长.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem出生身长.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem出生身长.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.flowLayoutPanel22;
            this.layoutControlItem25.CustomizationFormText = "吃奶量:";
            this.layoutControlItem25.Location = new System.Drawing.Point(278, 217);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem25.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem25.Text = "吃奶量:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.flowLayoutPanel24;
            this.layoutControlItem29.CustomizationFormText = "吃奶次数:";
            this.layoutControlItem29.Location = new System.Drawing.Point(545, 217);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem29.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem29.Text = "吃奶次数:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.flowLayoutPanel25;
            this.layoutControlItem30.CustomizationFormText = "大便次数：";
            this.layoutControlItem30.Location = new System.Drawing.Point(545, 241);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem30.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem30.Text = "大便次数：";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem体温
            // 
            this.layoutControlItem体温.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem体温.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem体温.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem体温.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem体温.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem体温.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem体温.Control = this.flowLayoutPanel26;
            this.layoutControlItem体温.CustomizationFormText = "体温：";
            this.layoutControlItem体温.Location = new System.Drawing.Point(0, 265);
            this.layoutControlItem体温.Name = "layoutControlItem体温";
            this.layoutControlItem体温.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem体温.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem体温.Tag = "check";
            this.layoutControlItem体温.Text = "体温：";
            this.layoutControlItem体温.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem体温.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem体温.TextToControlDistance = 5;
            // 
            // layoutControlItem呼吸频率
            // 
            this.layoutControlItem呼吸频率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem呼吸频率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem呼吸频率.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem呼吸频率.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem呼吸频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem呼吸频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem呼吸频率.Control = this.flowLayoutPanel27;
            this.layoutControlItem呼吸频率.CustomizationFormText = "呼吸频率：";
            this.layoutControlItem呼吸频率.Location = new System.Drawing.Point(278, 265);
            this.layoutControlItem呼吸频率.Name = "layoutControlItem呼吸频率";
            this.layoutControlItem呼吸频率.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem呼吸频率.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem呼吸频率.Tag = "check";
            this.layoutControlItem呼吸频率.Text = "呼吸频率：";
            this.layoutControlItem呼吸频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem呼吸频率.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem呼吸频率.TextToControlDistance = 5;
            // 
            // layoutControlItem脉率
            // 
            this.layoutControlItem脉率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem脉率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem脉率.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem脉率.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem脉率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem脉率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem脉率.Control = this.flowLayoutPanel29;
            this.layoutControlItem脉率.CustomizationFormText = "脉率：";
            this.layoutControlItem脉率.Location = new System.Drawing.Point(0, 289);
            this.layoutControlItem脉率.Name = "layoutControlItem脉率";
            this.layoutControlItem脉率.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem脉率.Size = new System.Drawing.Size(278, 64);
            this.layoutControlItem脉率.Tag = "check";
            this.layoutControlItem脉率.Text = "脉率：";
            this.layoutControlItem脉率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem脉率.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem脉率.TextToControlDistance = 5;
            // 
            // layoutControlItem胸部
            // 
            this.layoutControlItem胸部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem胸部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem胸部.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem胸部.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem胸部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem胸部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem胸部.Control = this.flowLayoutPanel32;
            this.layoutControlItem胸部.CustomizationFormText = "胸部：";
            this.layoutControlItem胸部.Location = new System.Drawing.Point(279, 521);
            this.layoutControlItem胸部.Name = "layoutControlItem胸部";
            this.layoutControlItem胸部.Size = new System.Drawing.Size(497, 30);
            this.layoutControlItem胸部.Tag = "check";
            this.layoutControlItem胸部.Text = "胸部：";
            this.layoutControlItem胸部.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem胸部.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem胸部.TextToControlDistance = 5;
            // 
            // layoutControlItem随访日期
            // 
            this.layoutControlItem随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem随访日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem随访日期.Control = this.dateEdit随访日期;
            this.layoutControlItem随访日期.CustomizationFormText = "随访日期：";
            this.layoutControlItem随访日期.Location = new System.Drawing.Point(0, 673);
            this.layoutControlItem随访日期.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItem随访日期.MinSize = new System.Drawing.Size(142, 20);
            this.layoutControlItem随访日期.Name = "layoutControlItem随访日期";
            this.layoutControlItem随访日期.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem随访日期.Size = new System.Drawing.Size(237, 25);
            this.layoutControlItem随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem随访日期.Tag = "check";
            this.layoutControlItem随访日期.Text = "随访日期：";
            this.layoutControlItem随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem随访日期.TextSize = new System.Drawing.Size(95, 20);
            this.layoutControlItem随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem78.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem78.Control = this.textEdit最近更新时间;
            this.layoutControlItem78.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem78.Location = new System.Drawing.Point(507, 723);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem78.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem78.Text = "最近更新时间：";
            this.layoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem78.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem78.TextToControlDistance = 5;
            // 
            // layoutControlItem随访医生
            // 
            this.layoutControlItem随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem随访医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem随访医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem随访医生.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem随访医生.Control = this.textEdit随访医生;
            this.layoutControlItem随访医生.CustomizationFormText = "随访医生签名：";
            this.layoutControlItem随访医生.Location = new System.Drawing.Point(237, 673);
            this.layoutControlItem随访医生.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem随访医生.MinSize = new System.Drawing.Size(139, 20);
            this.layoutControlItem随访医生.Name = "layoutControlItem随访医生";
            this.layoutControlItem随访医生.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem随访医生.Size = new System.Drawing.Size(269, 25);
            this.layoutControlItem随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem随访医生.Tag = "check";
            this.layoutControlItem随访医生.Text = "随访医生签名：";
            this.layoutControlItem随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem随访医生.TextSize = new System.Drawing.Size(95, 14);
            this.layoutControlItem随访医生.TextToControlDistance = 5;
            // 
            // layoutControlItem下次随访地点
            // 
            this.layoutControlItem下次随访地点.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem下次随访地点.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem下次随访地点.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem下次随访地点.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem下次随访地点.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem下次随访地点.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem下次随访地点.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem下次随访地点.Control = this.textEdit下次随访地点;
            this.layoutControlItem下次随访地点.CustomizationFormText = "下次随访地点：";
            this.layoutControlItem下次随访地点.Location = new System.Drawing.Point(238, 698);
            this.layoutControlItem下次随访地点.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem下次随访地点.MinSize = new System.Drawing.Size(129, 25);
            this.layoutControlItem下次随访地点.Name = "layoutControlItem下次随访地点";
            this.layoutControlItem下次随访地点.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem下次随访地点.Size = new System.Drawing.Size(268, 25);
            this.layoutControlItem下次随访地点.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem下次随访地点.Tag = "check";
            this.layoutControlItem下次随访地点.Text = "下次随访地点：";
            this.layoutControlItem下次随访地点.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem下次随访地点.TextSize = new System.Drawing.Size(95, 14);
            this.layoutControlItem下次随访地点.TextToControlDistance = 5;
            // 
            // layoutControlItem家长签名
            // 
            this.layoutControlItem家长签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem家长签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem家长签名.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem家长签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem家长签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem家长签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem家长签名.Control = this.textEdit家长签名;
            this.layoutControlItem家长签名.CustomizationFormText = "家长签名：";
            this.layoutControlItem家长签名.Location = new System.Drawing.Point(506, 673);
            this.layoutControlItem家长签名.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem家长签名.MinSize = new System.Drawing.Size(147, 25);
            this.layoutControlItem家长签名.Name = "layoutControlItem家长签名";
            this.layoutControlItem家长签名.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem家长签名.Size = new System.Drawing.Size(270, 25);
            this.layoutControlItem家长签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem家长签名.Tag = "check";
            this.layoutControlItem家长签名.Text = "家长签名：";
            this.layoutControlItem家长签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem家长签名.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem家长签名.TextToControlDistance = 5;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem80.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem80.Control = this.textEdit录入人;
            this.layoutControlItem80.CustomizationFormText = "录入人：";
            this.layoutControlItem80.Location = new System.Drawing.Point(238, 723);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem80.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem80.Text = "录入人：";
            this.layoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem80.TextSize = new System.Drawing.Size(95, 14);
            this.layoutControlItem80.TextToControlDistance = 5;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem82.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem82.Control = this.textEdit当前所属机构;
            this.layoutControlItem82.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem82.Location = new System.Drawing.Point(237, 747);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem82.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem82.Text = "当前所属机构：";
            this.layoutControlItem82.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem82.TextSize = new System.Drawing.Size(94, 20);
            this.layoutControlItem82.TextToControlDistance = 5;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem81.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem81.Control = this.textEdit最近修改人;
            this.layoutControlItem81.CustomizationFormText = "最近修改人：";
            this.layoutControlItem81.Location = new System.Drawing.Point(509, 747);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem81.Size = new System.Drawing.Size(267, 24);
            this.layoutControlItem81.Text = "最近修改人：";
            this.layoutControlItem81.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem81.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem81.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.layoutControl2;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(506, 698);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(270, 25);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem转诊
            // 
            this.layoutControlItem转诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem转诊.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem转诊.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem转诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem转诊.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem转诊.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem转诊.Control = this.panelControl2;
            this.layoutControlItem转诊.CustomizationFormText = "转诊：";
            this.layoutControlItem转诊.Location = new System.Drawing.Point(0, 603);
            this.layoutControlItem转诊.MinSize = new System.Drawing.Size(179, 70);
            this.layoutControlItem转诊.Name = "layoutControlItem转诊";
            this.layoutControlItem转诊.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem转诊.Size = new System.Drawing.Size(776, 70);
            this.layoutControlItem转诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem转诊.Tag = "check";
            this.layoutControlItem转诊.Text = "转诊：";
            this.layoutControlItem转诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem转诊.TextSize = new System.Drawing.Size(83, 20);
            this.layoutControlItem转诊.TextToControlDistance = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sbtn说明);
            this.panelControl1.Controls.Add(this.sbtn保存);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(815, 34);
            this.panelControl1.TabIndex = 0;
            // 
            // sbtn说明
            // 
            this.sbtn说明.Location = new System.Drawing.Point(102, 5);
            this.sbtn说明.Name = "sbtn说明";
            this.sbtn说明.Size = new System.Drawing.Size(75, 23);
            this.sbtn说明.TabIndex = 1;
            this.sbtn说明.Text = "填表说明";
            // 
            // sbtn保存
            // 
            this.sbtn保存.Location = new System.Drawing.Point(12, 5);
            this.sbtn保存.Name = "sbtn保存";
            this.sbtn保存.Size = new System.Drawing.Size(75, 23);
            this.sbtn保存.TabIndex = 0;
            this.sbtn保存.Text = "保存";
            this.sbtn保存.Click += new System.EventHandler(this.sbtn保存_Click);
            // 
            // UC新生儿家庭访视记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC新生儿家庭访视记录表";
            this.Size = new System.Drawing.Size(815, 500);
            this.Load += new System.EventHandler(this.UC新生儿家庭访视记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).EndInit();
            this.flowLayoutPanel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit胸部其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt母亲身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt父亲身份证号.Properties)).EndInit();
            this.flowLayoutPanel29.ResumeLayout(false);
            this.flowLayoutPanel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脉率.Properties)).EndInit();
            this.flowLayoutPanel27.ResumeLayout(false);
            this.flowLayoutPanel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit呼吸频率.Properties)).EndInit();
            this.flowLayoutPanel26.ResumeLayout(false);
            this.flowLayoutPanel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体温.Properties)).EndInit();
            this.flowLayoutPanel24.ResumeLayout(false);
            this.flowLayoutPanel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit吃奶次数.Properties)).EndInit();
            this.flowLayoutPanel25.ResumeLayout(false);
            this.flowLayoutPanel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit大便次数.Properties)).EndInit();
            this.flowLayoutPanel22.ResumeLayout(false);
            this.flowLayoutPanel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit吃奶量.Properties)).EndInit();
            this.flowLayoutPanel21.ResumeLayout(false);
            this.flowLayoutPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生身长.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            this.flowLayoutPanel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit目前体重.Properties)).EndInit();
            this.flowLayoutPanel31.ResumeLayout(false);
            this.flowLayoutPanel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生体重.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit脐带.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脐带其他.Properties)).EndInit();
            this.flowLayoutPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit脊柱.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit脊柱其他.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit腹部其他.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit外生殖器.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外生殖器其他.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit肛门.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肛门其他.Properties)).EndInit();
            this.flowLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit心肺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心肺其他.Properties)).EndInit();
            this.flowLayoutPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit口腔.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit口腔其他.Properties)).EndInit();
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit鼻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit鼻其他.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit颈部包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit颈部包块其他.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit耳外观.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit耳外观其他.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit四肢活动度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit四肢活动度其他.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit眼外观.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit眼外观其他.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit前卤情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤情况其他.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit前卤2.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否畸形.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit是否畸形其他.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit母妊娠患病情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母妊娠患病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访地点.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            this.flowLayoutPanel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导科学喂养.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk口服维生素D.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否到位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).EndInit();
            this.flowLayoutPanel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤未见异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤湿疹.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤糜烂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit皮肤其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit皮肤其他.Properties)).EndInit();
            this.flowLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色未检.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色红润.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色黄染.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit面色其他.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查甲低.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病筛查苯丙酮尿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_先天肾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_葡萄糖症.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit疾病_其他.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1分钟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1分钟得分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5分钟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5分钟得分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApgar不详.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit顺产.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit头吸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit产钳.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit剖宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit双多胎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit臀位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生情况其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit助产机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生孕周.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit母姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit父姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit住址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit新生儿窒息.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit听力筛查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit喂养方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit呕吐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit大便.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit黄疸部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生孕周)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem助产机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem新生儿窒息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem听力筛查)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem疾病筛查)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem喂养方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem面色)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem黄疸部位)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem皮肤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem指导)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem母妊娠疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem是否畸形)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem前卤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem前卤情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem眼外观)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem四肢活动度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem耳外观)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem颈部包块)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem鼻)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem口腔)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem肛门)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem心肺)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem外生殖器官)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem腹部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脊柱)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脐带)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem目前体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem出生身长)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem呼吸频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem脉率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem胸部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem下次随访地点)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem家长签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem转诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtn说明;
        private DevExpress.XtraEditors.SimpleButton sbtn保存;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit textEdit母出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit母联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit母职业;
        private DevExpress.XtraEditors.TextEdit textEdit母姓名;
        private DevExpress.XtraEditors.TextEdit textEdit父出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit父联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit父职业;
        private DevExpress.XtraEditors.TextEdit textEdit父姓名;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit住址;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit儿童姓名;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraEditors.TextEdit textEdit儿童档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEdit出生孕周;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem出生孕周;
        private DevExpress.XtraEditors.TextEdit textEdit助产机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem助产机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem新生儿窒息;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem听力筛查;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.CheckEdit checkEdit顺产;
        private DevExpress.XtraEditors.CheckEdit checkEdit头吸;
        private DevExpress.XtraEditors.CheckEdit checkEdit产钳;
        private DevExpress.XtraEditors.CheckEdit checkEdit剖宫;
        private DevExpress.XtraEditors.CheckEdit checkEdit双多胎;
        private DevExpress.XtraEditors.CheckEdit checkEdit臀位;
        private DevExpress.XtraEditors.CheckEdit checkEdit其他;
        private DevExpress.XtraEditors.TextEdit textEdit出生情况其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem出生情况;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1分钟;
        private DevExpress.XtraEditors.TextEdit textEdit1分钟得分;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.CheckEdit checkEdit5分钟;
        private DevExpress.XtraEditors.TextEdit textEdit5分钟得分;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.CheckEdit checkEditApgar不详;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病筛查无;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病筛查甲低;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病筛查苯丙酮尿;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病_先天肾病;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病_葡萄糖症;
        private DevExpress.XtraEditors.CheckEdit checkEdit疾病_其他;
        private DevExpress.XtraEditors.TextEdit textEdit疾病_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem疾病筛查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem喂养方式;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit新生儿窒息;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit听力筛查;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit喂养方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色未检;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色红润;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色黄染;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色其他;
        private DevExpress.XtraEditors.TextEdit textEdit面色其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem面色;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem黄疸部位;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private DevExpress.XtraEditors.CheckEdit checkEdit皮肤未见异常;
        private DevExpress.XtraEditors.CheckEdit checkEdit皮肤湿疹;
        private DevExpress.XtraEditors.CheckEdit checkEdit皮肤糜烂;
        private DevExpress.XtraEditors.CheckEdit checkEdit皮肤其他;
        private DevExpress.XtraEditors.TextEdit textEdit皮肤其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem皮肤;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit转诊机构;
        private DevExpress.XtraEditors.TextEdit textEdit转诊原因;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem转诊;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit录入人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit录入时间;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit下次随访地点;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导科学喂养;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导生长发育;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导疾病预防;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导预防意外;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导口腔保健;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem指导;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem下次随访地点;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem随访医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit呕吐;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit大便;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit黄疸部位;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit转诊;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit母妊娠患病情况;
        private DevExpress.XtraEditors.TextEdit textEdit母妊娠患病其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem母妊娠疾病;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否畸形;
        private DevExpress.XtraEditors.TextEdit textEdit是否畸形其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem是否畸形;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.TextEdit textEdit前卤1;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit textEdit前卤2;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem前卤;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit前卤情况;
        private DevExpress.XtraEditors.TextEdit textEdit前卤情况其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem前卤情况;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit眼外观;
        private DevExpress.XtraEditors.TextEdit textEdit眼外观其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem眼外观;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit四肢活动度;
        private DevExpress.XtraEditors.TextEdit textEdit四肢活动度其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem四肢活动度;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit颈部包块;
        private DevExpress.XtraEditors.TextEdit textEdit颈部包块其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit耳外观;
        private DevExpress.XtraEditors.TextEdit textEdit耳外观其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem耳外观;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem颈部包块;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit鼻;
        private DevExpress.XtraEditors.TextEdit textEdit鼻其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem鼻;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit口腔;
        private DevExpress.XtraEditors.TextEdit textEdit口腔其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem口腔;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit肛门;
        private DevExpress.XtraEditors.TextEdit textEdit肛门其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem肛门;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit心肺;
        private DevExpress.XtraEditors.TextEdit textEdit心肺其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem心肺;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit外生殖器;
        private DevExpress.XtraEditors.TextEdit textEdit外生殖器其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem外生殖器官;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit腹部;
        private DevExpress.XtraEditors.TextEdit textEdit腹部其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem腹部;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit脊柱;
        private DevExpress.XtraEditors.TextEdit textEdit脊柱其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem脊柱;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit脐带;
        private DevExpress.XtraEditors.TextEdit textEdit脐带其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem脐带;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
        private DevExpress.XtraEditors.TextEdit textEdit出生体重;
        private DevExpress.XtraEditors.LabelControl labelControl出生体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem出生体重;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.TextEdit textEdit目前体重;
        private DevExpress.XtraEditors.LabelControl labelControl目前体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem目前体重;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private DevExpress.XtraEditors.TextEdit textEdit出生身长;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem出生身长;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private DevExpress.XtraEditors.TextEdit textEdit吃奶量;
        private DevExpress.XtraEditors.LabelControl labelControl吃奶量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private DevExpress.XtraEditors.TextEdit textEdit吃奶次数;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private DevExpress.XtraEditors.TextEdit textEdit大便次数;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private DevExpress.XtraEditors.TextEdit textEdit体温;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem体温;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private DevExpress.XtraEditors.TextEdit textEdit呼吸频率;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem呼吸频率;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private DevExpress.XtraEditors.TextEdit textEdit脉率;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem脉率;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导其他;
        private DevExpress.XtraEditors.TextEdit textEdit指导其他;
        private DevExpress.XtraEditors.TextEdit txt母亲身份证号;
        private DevExpress.XtraEditors.TextEdit txt父亲身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit胸部;
        private DevExpress.XtraEditors.TextEdit textEdit胸部其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem胸部;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.TextEdit textEdit家长签名;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否到位;
        private DevExpress.XtraEditors.TextEdit textEdit转诊联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit联系人;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem家长签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.CheckEdit chk口服维生素D;
    }
}
