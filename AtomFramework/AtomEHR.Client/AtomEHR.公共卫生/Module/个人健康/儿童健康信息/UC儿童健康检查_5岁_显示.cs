﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC儿童健康检查_5岁_显示 : UserControlBase
    {
        private string m_grdabh = null;
        private string m_prgid = null;

        private DataSet m_ds5岁 = null;
        private bll儿童_健康检查_5岁 m_bll5岁 = new bll儿童_健康检查_5岁();
        public UC儿童健康检查_5岁_显示(string grdabh)
        {
            InitializeComponent();
            m_grdabh = grdabh;

            BindData();
        }

        #region 为控件绑定数据
        private void BindData()
        {
            BindDataForComboBox("hw_item", comboBoxEdit体重);

            BindDataForComboBox("hw_item", comboBoxEdit身长);

            BindDataForComboBox("ebjctgfypj", comboBoxEdit体格);

            //BindDataForComboBox("ssbtqk", comboBoxEdit步态);

            //BindDataForComboBox("ywyc", comboBoxEdit皮肤);

            //BindDataForComboBox("qx_qianxin", comboBoxEdit前囟);

            //BindDataForComboBox("yw_youwu", comboBoxEdit颈部包块);

            //BindDataForComboBox("ywyc", comboBoxEdit眼外观);

            //BindDataForComboBox("ywyc", comboBoxEdit耳外观);

            BindDataForComboBox("ywyc", comboBoxEdit胸部);

            BindDataForComboBox("ywyc", comboBoxEdit腹部);

            //BindDataForComboBox("jd_jidai", comboBoxEdit脐部);

            //BindDataForComboBox("ywyc", comboBoxEdit四肢);

            //BindDataForComboBox("glbtz", comboBoxEdit可疑佝偻);

            //BindDataForComboBox("ywyc", comboBoxEdit肛门外生殖器);

            //BindDataForComboBox("myfypg", comboBoxEdit发育评估);

            //BindDataForComboBox("mylchbqk", comboBoxEdit两次随访间患病情况);

            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);

            BindDataForComboBox("hw_item", comboBoxEdit体重指数);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }
        #endregion

        private void UC儿童健康检查_5岁_显示_Load(object sender, EventArgs e)
        {
            ucTxtLbl体重.Txt1.Properties.ReadOnly = true;
            ucTxtLbl身长.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl头围.Txt1.Properties.ReadOnly = true;
            //ucTxtLblTxtLbl前囟.Txt1.Properties.ReadOnly = true;
            //ucTxtLblTxtLbl前囟.Txt2.Properties.ReadOnly = true;
            ucTxtLbl血红蛋白值.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl户外活动.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl服用维生素.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊原因.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊机构.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl出牙数.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl出牙龋齿.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl出牙龋齿.Txt2.Properties.ReadOnly = true;
            

            m_ds5岁 = m_bll5岁.GetBusinessByGrdabh(m_grdabh, true);

            m_prgid = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所属机构].ToString();

            textEdit儿童档案号.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();

            string str姓名 = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = DESEncrypt.DES解密(str姓名);
            textEdit身份证号.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号].ToString();

            string str性别 = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别].ToString();
            DataRow[] dr性别 = DataDictCache.Cache.t性别.Select("P_CODE='" + str性别 + "'");
            if (dr性别.Length > 0)
            {
                textEdit性别.Text = dr性别[0]["P_DESC"].ToString();
            }
            textEdit出生日期.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();

            textEdit卡号.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.卡号].ToString();
            ucTxtLbl体重.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重].ToString();

            string str体重选项 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重选项].ToString();
            util.ControlsHelper.SetComboxData(str体重选项, comboBoxEdit体重);

            ucTxtLbl身长.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高].ToString();
            string str身长选项 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高选项].ToString();
            util.ControlsHelper.SetComboxData(str身长选项, comboBoxEdit身长);

            string strTgfypj = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体格发育评价].ToString();
            util.ControlsHelper.SetComboxData(strTgfypj, comboBoxEdit体格);

            textEdit视力.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.视力].ToString();

            //ucTxtLbl头围.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.头围].ToString();

            //string str步态 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.步态].ToString();
            //util.ControlsHelper.SetComboxData(str步态, comboBoxEdit步态);

            //string str皮肤 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.皮肤].ToString();
            //util.ControlsHelper.SetComboxData(str皮肤, comboBoxEdit皮肤);

            //string str前囟 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟].ToString();
            //util.ControlsHelper.SetComboxData(str前囟, comboBoxEdit前囟);

            //ucTxtLblTxtLbl前囟.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟值].ToString();
            //ucTxtLblTxtLbl前囟.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.后囟值].ToString();

            //ucTxtLbl出牙数.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.出牙数].ToString();
            ucTxtLblTxtLbl出牙龋齿.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.牙齿数目].ToString();
            ucTxtLblTxtLbl出牙龋齿.Txt2.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.龋齿数].ToString();
            //string str颈部包块 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.颈部包块].ToString();
            //util.ControlsHelper.SetComboxData(str颈部包块, comboBoxEdit颈部包块);

            //string str眼外观 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.眼部].ToString();
            //util.ControlsHelper.SetComboxData(str眼外观, comboBoxEdit眼外观);

            //string str耳外观 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.耳部].ToString();
            //util.ControlsHelper.SetComboxData(str耳外观, comboBoxEdit耳外观);

            //string str心肺 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.心肺].ToString();
            //util.ControlsHelper.SetComboxData(str心肺, comboBoxEdit胸部);

            string str腹部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.腹部].ToString();
            util.ControlsHelper.SetComboxData(str腹部, comboBoxEdit腹部);

            //string str脐部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.脐部].ToString();
            //util.ControlsHelper.SetComboxData(str脐部, comboBoxEdit脐部);

            //string str四肢 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.四肢].ToString();
            //util.ControlsHelper.SetComboxData(str四肢, comboBoxEdit四肢);

            //string str可疑佝偻 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病体征].ToString();
            //util.ControlsHelper.SetComboxData(str可疑佝偻, comboBoxEdit可疑佝偻);

            //string str佝偻病症状 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病症状].ToString();
            //util.ControlsHelper.SetComboxData(str佝偻病症状, comboBoxEdit佝偻病症状);

            //string str肛门 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.生殖器].ToString();
            //util.ControlsHelper.SetComboxData(str肛门, comboBoxEdit肛门外生殖器);

            ucTxtLbl血红蛋白值.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.血红蛋白值].ToString();

            //ucTxtLbl户外活动.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.户外活动].ToString();
            //ucTxtLbl服用维生素.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.服用维生素].ToString();

            //string str听力 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病体征].ToString();
            //util.ControlsHelper.SetComboxData(str听力, comboBoxEdit听力);

            //string str发育评估 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估].ToString();
            //util.ControlsHelper.SetComboxData(str发育评估, comboBoxEdit发育评估);

            //string str面色 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.面色].ToString();
            char[] c分隔符 = { ',' };
            //string[] str面色Value = str面色.Split(c分隔符);
            //for (int index = 0; index < str面色Value.Length; index++)
            //{
            //    //if (str面色Value[index].Equals("1"))
            //    //{
            //    //    checkEdit面色未检.Checked = true;
            //    //    break;
            //    //}
            //    //else 
            //    if (str面色Value[index].Equals("2"))
            //    {
            //        checkEdit面色红润.Checked = true;
            //    }
            //    //else if (str面色Value[index].Equals("3"))
            //    //{
            //    //    checkEdit面色黄染.Checked = true;
            //    //}
            //    else if (str面色Value[index].Equals("99"))
            //    {
            //        checkEdit面色其他.Checked = true;
            //    }
            //}

            //string str两次随访间患病情况 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.两次随访间患病情况].ToString();
            //util.ControlsHelper.SetComboxData(str两次随访间患病情况, comboBoxEdit两次随访间患病情况);

            string str患病Temp = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况].ToString();
            string[] str患病 = str患病Temp.Split(c分隔符);
            for (int index = 0; index < str患病.Length; index++)
            {
                if (str患病[index].Equals("1"))
                {
                    checkEdit患病无.Checked = true;
                    break;
                }
                else if (str患病[index].Equals("2"))
                {
                    checkEdit患病肺炎.Checked = true;
                    textEdit患病肺炎.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况肺炎].ToString();
                }
                else if (str患病[index].Equals("97"))
                {
                    checkEdit患病腹泻.Checked = true;
                    textEdit患病腹泻.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况腹泻].ToString();
                }
                else if (str患病[index].Equals("98"))
                {
                    checkEdit患病外伤.Checked = true;
                    textEdit患病外伤.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况外伤].ToString();
                }
                else if (str患病[index].Equals("99"))
                {
                    checkEdit患病其他.Checked = true;
                    textEdit患病其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况其他].ToString();
                }
            }


            string str转诊 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊状况].ToString();
            util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);

            ucLblTxt转诊原因.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊原因].ToString();
            ucLblTxt转诊机构.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊机构].ToString();

            string str指导 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.指导].ToString();
            string[] str指导Values = str指导.Split(c分隔符);
            for (int index = 0; index < str指导Values.Length; index++)
            {
                switch (str指导Values[index])
                {
                    case "1":
                        checkEdit指导合理膳食.Checked = true;
                        break;
                    case "2":
                        checkEdit指导预防意外.Checked = true;
                        break;
                    case "3":
                        checkEdit指导生长发育.Checked = true;
                        break;
                    case "4":
                        checkEdit指导口腔保健.Checked = true;
                        break;
                    case "99":
                        checkEdit指导疾病预防.Checked = true;
                        break;
                    case "100":
                        checkEdit指导其他.Checked = true;
                        textEdit指导其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.E_XSEZDQT].ToString();
                        break;
                    default:
                        break;
                }
            }

            //string str中医Temp = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理].ToString();
            //string[] str中医 = str中医Temp.Split(c分隔符);
            //for (int index = 0; index < str中医.Length; index++)
            //{
            //    switch (str中医[index])
            //    {
            //        case "1":
            //            checkEdit中医饮食.Checked = true;
            //            break;
            //        case "2":
            //            checkEdit中医起居.Checked = true;
            //            break;
            //        case "3":
            //            checkEdit传授摩捏.Checked = true;
            //            break;
            //        case "4":
            //            checkEdit中医其他.Checked = true;
            //            textEdit中医其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理其他].ToString();
            //            break;
            //    }
            //}

            #region 新版本添加
            textEdit所在幼儿园名称.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所在幼儿园名称].ToString();
            textEdit健康管理联系人.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.健康管理联系人].ToString();
            textEdit联系电话.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系电话].ToString();
            string str查体机构 = m_bll5岁.ReturnDis字典显示("etctjg", m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构].ToString());
            if (!string.IsNullOrEmpty(str查体机构) && str查体机构 == "其他医疗机构")
            {
                textEdit查体机构.Text = str查体机构 + ":" + m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构其他].ToString();
            }
            else
            {
                textEdit查体机构.Text = str查体机构;
            }
            string str体重指数 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重指数].ToString();
            util.ControlsHelper.SetComboxData(str体重指数, comboBoxEdit体重指数);
            string str胸部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.胸部].ToString();
            util.ControlsHelper.SetComboxData(str胸部, comboBoxEdit胸部);
            string str发育评估 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估].ToString();
            string[] strs发育评估 = str发育评估.Split(c分隔符);
            for (int index = 0; index < strs发育评估.Length; index++)
            {
                switch (strs发育评估[index])
                {
                    case "1":
                        chk发育评估无.Checked = true;
                        break;
                    case "2":
                        chk发育评估1.Checked = true;
                        break;
                    case "3":
                        chk发育评估2.Checked = true;
                        break;
                    case "4":
                        chk发育评估3.Checked = true;
                        break;
                    case "5":
                        chk发育评估4.Checked = true;
                        break;
                }
            }
            ucLblTxt联系人.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系人].ToString();
            ucLblTxt联系方式.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系方式].ToString();
            ucLblTxt结果.Txt1.Text = m_bll5岁.ReturnDis字典显示("sfdw", m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.结果].ToString());
            textEdit家长签名.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.家长签名].ToString();
            #endregion

            textEdit其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.其他].ToString();
            dateEdit下次随访日期.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.下次随访日期].ToString();
            textEdit随访医生.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.随访医生].ToString();
            dateEdit随访日期.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发生时间].ToString();


            textEdit录入时间.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建时间].ToString();
            textEdit录入人.Text = _bllUser.Return用户名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建人].ToString());
            textEdit创建机构.Text = _bll机构信息.Return机构名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建机构].ToString());
            textEdit当前所属机构.Text = _bll机构信息.Return机构名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所属机构].ToString());
            textEdit最近修改人.Text = _bllUser.Return用户名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改人].ToString());
            textEdit最近修改时间.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改时间].ToString();
            
            #region 字体变色
            //Set考核项颜色_new(layoutControl1, labelControl考核项);
            ChangeColorForLayoutItem();
            #endregion

            //labelControl考核项.Text = "考核项：15     缺项："
            labelControl考核项.Text = "考核项：21     缺项："
                + m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.缺项].ToString()
                + " 完整度："
                + m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.完整度].ToString()
                + "% ";
        }

        private void ChangeColorForLayoutItem()
        {
            if (string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit体重.Text))
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLbl身长.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit身长.Text))
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            SetComboBoxEditItemColor(comboBoxEdit体格, lciTgfypj);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl头围.Txt1.Text))
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //if (//checkEdit面色未检.Checked || 
            //    checkEdit面色红润.Checked
            //    //|| checkEdit面色黄染.Checked 
            //    || checkEdit面色其他.Checked)
            //{
            //    lciFace.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lciFace.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            if (string.IsNullOrWhiteSpace(textEdit视力.Text))
            {
                lciSight.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSight.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);
            //SetComboBoxEditItemColor(comboBoxEdit口腔, lci口腔);
            //SetComboBoxEditItemColor(comboBoxEdit皮肤, lci皮肤);

            //if (string.IsNullOrWhiteSpace(comboBoxEdit前囟.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt1.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt2.Text))
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit颈部包块, lci颈部包块);
            //SetComboBoxEditItemColor(comboBoxEdit眼外观, lciEye);
            //SetComboBoxEditItemColor(comboBoxEdit耳外观, lciEar);

            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt1.Text) || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt2.Text))
            {
                lci牙齿龋齿.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci牙齿龋齿.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            SetComboBoxEditItemColor(comboBoxEdit胸部, lbl胸部);
            SetComboBoxEditItemColor(comboBoxEdit腹部, lciBelly);

            if (string.IsNullOrWhiteSpace(ucTxtLbl血红蛋白值.Txt1.Text))
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(textEdit其他.Text))
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            //SetComboBoxEditItemColor(comboBoxEdit脐部, lciNavel);
            //SetComboBoxEditItemColor(comboBoxEdit四肢, lciFours);
            //SetComboBoxEditItemColor(comboBoxEdit佝偻病症状, lciGlbzz);
            //SetComboBoxEditItemColor(comboBoxEdit可疑佝偻, lciGlbtz);
            //SetComboBoxEditItemColor(comboBoxEdit肛门外生殖器, lciSzq);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl户外活动.Txt1.Text))
            //{
            //    lciHwhd.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciHwhd.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl服用维生素.Txt1.Text))
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit发育评估, lciFypg);

            //SetComboBoxEditItemColor(comboBoxEdit步态, lciBt);

            //SetComboBoxEditItemColor(comboBoxEdit两次随访间患病情况, lciLcsfjhbqk);
            //if (!(checkEdit患病无.Checked)
            //    && !(checkEdit患病肺炎.Checked && !(string.IsNullOrWhiteSpace(textEdit患病肺炎.Text.Trim())))
            //    && !(checkEdit患病腹泻.Checked && !(string.IsNullOrWhiteSpace(textEdit患病腹泻.Text.Trim())))
            //    && !(checkEdit患病外伤.Checked && !(string.IsNullOrWhiteSpace(textEdit患病外伤.Text.Trim())))
            //    && !(checkEdit患病其他.Checked && !(string.IsNullOrWhiteSpace(textEdit患病其他.Text.Trim())))
            //    )
            //{
            //    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            if (checkEdit患病无.Checked || checkEdit患病肺炎.Checked || checkEdit患病腹泻.Checked
                || checkEdit患病外伤.Checked || checkEdit患病其他.Checked)
            {
                //未选择肺炎、腹泻、患病、其他的情况下，即只勾选了“无”的情况下
                if (!(checkEdit患病肺炎.Checked) && !(checkEdit患病腹泻.Checked) && !(checkEdit患病外伤.Checked) && !(checkEdit患病其他.Checked))
                {
                    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    if (checkEdit患病肺炎.Checked && string.IsNullOrWhiteSpace(textEdit患病肺炎.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病腹泻.Checked && string.IsNullOrWhiteSpace(textEdit患病腹泻.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病外伤.Checked && string.IsNullOrWhiteSpace(textEdit患病外伤.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病其他.Checked && string.IsNullOrWhiteSpace(textEdit其他.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
            }
            else
            {
                lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(comboBoxEdit转诊.Text))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (comboBoxEdit转诊.Text == "有" && (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text)
                    || string.IsNullOrWhiteSpace(ucLblTxt联系人.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt联系方式.Txt1.Text)
                    || string.IsNullOrWhiteSpace(ucLblTxt结果.Txt1.Text)
                ))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (checkEdit指导合理膳食.Checked || checkEdit指导生长发育.Checked
                || checkEdit指导疾病预防.Checked || checkEdit指导预防意外.Checked
                || checkEdit指导口腔保健.Checked)
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            #region 新版本添加
            //if (!string.IsNullOrWhiteSpace(textEdit所在幼儿园名称.Text))
            //{
            //    lbl所在幼儿园名称.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl所在幼儿园名称.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //if (!string.IsNullOrWhiteSpace(textEdit健康管理联系人.Text))
            //{
            //    lbl健康管理联系人.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl健康管理联系人.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //if (!string.IsNullOrWhiteSpace(textEdit联系电话.Text))
            //{
            //    lbl联系电话.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl联系电话.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            if (!string.IsNullOrWhiteSpace(textEdit查体机构.Text))
            {
                lbl查体机构.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl查体机构.AppearanceItemCaption.ForeColor = Color.Red;
            }
            SetComboBoxEditItemColor(comboBoxEdit胸部, lbl胸部);

            if (chk发育评估无.Checked || chk发育评估1.Checked || chk发育评估2.Checked
                || chk发育评估3.Checked || chk发育评估4.Checked)
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (!string.IsNullOrWhiteSpace(textEdit家长签名.Text))
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion
        }

        private void SetComboBoxEditItemColor(DevExpress.XtraEditors.ComboBoxEdit combo, DevExpress.XtraLayout.LayoutControlItem item)
        {
            if (string.IsNullOrWhiteSpace(combo.Text))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }


        //private void comboBoxEdit前囟_TextChanged(object sender, EventArgs e)
        //{
        //    string value = util.ControlsHelper.GetComboxKey(comboBoxEdit前囟);
        //    if (value == "99")
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = true;
        //    }
        //    else
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = false;
        //    }
        //}

        private void comboBoxEdit转诊_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);

            //1是“有”的编码
            if (value != null && value.Equals("1"))
            {
                ucLblTxt转诊原因.Enabled = true;
                ucLblTxt转诊机构.Enabled = true;
                ucLblTxt联系人.Enabled = true;
                ucLblTxt联系方式.Enabled = true;
                ucLblTxt结果.Enabled = true;
            }
            else
            {
                ucLblTxt转诊原因.Enabled = false;
                ucLblTxt转诊原因.Txt1.Text = "";
                ucLblTxt转诊机构.Enabled = false;
                ucLblTxt转诊机构.Txt1.Text = "";
                ucLblTxt联系人.Enabled = false;
                ucLblTxt联系方式.Enabled = false;
                ucLblTxt结果.Enabled = false;
                ucLblTxt联系人.Txt1.Text = "";
                ucLblTxt联系方式.Txt1.Text = "";
                ucLblTxt结果.Txt1.Text = "";
            }
        }

        private void sbtnUpdate_Click(object sender, EventArgs e)
        {
            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            if (!isallow)
            {
                if (m_prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }

            UC儿童健康检查_5岁 ctl = new UC儿童健康检查_5岁(m_grdabh, UpdateType.Modify, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }

        private void sbtnDelete_Click(object sender, EventArgs e)
        {
            if (m_prgid != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                return;
            }

            if (Msg.AskQuestion("您是否确实要删除儿童健康检查记录表（5岁）吗？"))
            { }
            else
            {
                //不删除
                return;
            }

            string strID = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.ID].ToString();
            bool bRet = m_bll5岁.Delete(strID);

            string strCurrentServerDate = m_bll5岁.ServiceDateTime;
            try
            {
                string strBirth = textEdit出生日期.Text;

                DateTime dt出生日期 = Convert.ToDateTime(strBirth);
                //DateTime dt更新时间 = Convert.ToDateTime(strCurrentServerDate);
                //int month = (dt更新时间.Year - dt出生日期.Year) * 12 + (dt更新时间.Month - dt出生日期.Month);

                //if (month < 36 || month >= 48)
                int age = new bllCom().GetAgeByBirthDay(strBirth, strCurrentServerDate);
                if (age < 5 || age >= 6)
                {
                }
                else
                {
                    string strXcsfsj = dt出生日期.AddYears(5).ToString("yyyy-MM-dd");
                    m_bll5岁.UpdateRelatedInfo(m_grdabh, strXcsfsj);
                }
            }
            catch(Exception ex)
            {
                Msg.ShowException(ex);
            }

            if(bRet)
            {
                Msg.ShowInformation("删除成功！");
                UC儿童基本信息_显示 ctrl = new UC儿童基本信息_显示(Common.frm个人健康 as frm个人健康, m_grdabh);
                ctrl.Dock = DockStyle.Fill;
                ShowControl(ctrl);
            }
            else
            {
                Msg.ShowInformation("删除失败。");
            }
        }

        //private void checkEdit中医其他_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (checkEdit中医其他.Checked)
        //    {
        //        textEdit中医其他.Enabled = true;
        //    }
        //    else
        //    {
        //        textEdit中医其他.Enabled = true;
        //        textEdit中医其他.Text = "";
        //    }
        //}

        private void checkEdit患病无_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病无.Checked)
            {
                checkEdit患病肺炎.Checked = false;
                checkEdit患病腹泻.Checked = false;
                checkEdit患病外伤.Checked = false;
                checkEdit患病其他.Checked = false;
            }
            else
            {
                checkEdit患病肺炎.Checked = true;
                checkEdit患病腹泻.Checked = true;
                checkEdit患病外伤.Checked = true;
                checkEdit患病其他.Checked = true;
            }
        }

        private void checkEdit患病肺炎_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病肺炎.Checked)
            {
                textEdit患病肺炎.Enabled = true;
            }
            else
            {
                textEdit患病肺炎.Enabled = false;
                textEdit患病肺炎.Text = "";
            }
        }

        private void checkEdit患病腹泻_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病腹泻.Checked)
            {
                textEdit患病腹泻.Enabled = true;
            }
            else
            {
                textEdit患病腹泻.Enabled = false;
                textEdit患病腹泻.Text = "";
            }
        }

        private void checkEdit患病外伤_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病外伤.Checked)
            {
                textEdit患病外伤.Enabled = true;
            }
            else
            {
                textEdit患病外伤.Enabled = false;
                textEdit患病外伤.Text = "";
            }
        }

        private void checkEdit患病其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病其他.Checked)
            {
                textEdit患病其他.Enabled = true;
            }
            else
            {
                textEdit患病其他.Enabled = false;
                textEdit患病其他.Text = "";
            }
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            textEdit指导其他.Enabled = checkEdit指导其他.Checked;
            //if (checkEdit指导其他.Checked)
            //{
            //    textEdit指导其他.Enabled = true;
            //}
            //else
            //{
            //    textEdit指导其他.Enabled = false;
            //    textEdit指导其他.Text = "";
            //}
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            string _docNo = this.textEdit儿童档案号.Text.Trim();
            if (!string.IsNullOrEmpty(_docNo))
            {
                report3_6岁儿童健康检查记录表 report = new report3_6岁儿童健康检查记录表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }
    }
}
