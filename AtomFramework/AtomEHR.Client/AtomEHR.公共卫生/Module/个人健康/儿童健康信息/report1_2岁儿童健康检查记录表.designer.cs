﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class report1_2岁儿童健康检查记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine47 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine48 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine49 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine50 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine51 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine21 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月出牙数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月出牙数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月出牙数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月出牙数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月步态 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine37 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine38 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine39 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine40 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine41 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月步态 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月步大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月步态 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月步大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月步态 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月步大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月佝偻病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月佝偻病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月佝偻病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine22 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine27 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine28 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine29 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine30 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine31 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine32 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine33 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine34 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine35 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine36 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine42 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine43 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine44 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine45 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine46 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt12月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt12月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt12月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt12月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt12月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt12月服务其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt18月服务其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt24月服务其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt30月服务其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月下次随访 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月下次随访 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月下次随访 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月下次随访 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt12月随访医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt18月随访医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt24月随访医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt30月随访医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.txt姓名,
            this.xrLine1,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel12,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.HeightF = 1110.417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 107.2917F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow10,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow58,
            this.xrTableRow59});
            this.xrTable1.SizeF = new System.Drawing.SizeF(750F, 901.7129F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UsePadding = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 0.8D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "月（年）龄";
            this.xrTableCell1.Weight = 1.5000000966390006D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "12月龄";
            this.xrTableCell2.Weight = 1.5000000966390006D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "18月龄";
            this.xrTableCell4.Weight = 1.5000000406901042D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "24月龄";
            this.xrTableCell5.Weight = 1.5000001305474153D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "30月龄";
            this.xrTableCell3.Weight = 1.4999999406602607D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 0.8D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "随访日期";
            this.xrTableCell6.Weight = 1.5000000966390006D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月随访});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.5000000966390006D;
            // 
            // txt12月随访
            // 
            this.txt12月随访.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.txt12月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.589457E-05F);
            this.txt12月随访.Name = "txt12月随访";
            this.txt12月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt12月随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月随访});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1.5000000406901042D;
            // 
            // txt18月随访
            // 
            this.txt18月随访.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.txt18月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt18月随访.Name = "txt18月随访";
            this.txt18月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt18月随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月随访});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 1.5000001305474153D;
            // 
            // txt24月随访
            // 
            this.txt24月随访.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.txt24月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt24月随访.Name = "txt24月随访";
            this.txt24月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt24月随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月随访});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 1.4999999406602607D;
            // 
            // txt30月随访
            // 
            this.txt30月随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.txt30月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt30月随访.Name = "txt30月随访";
            this.txt30月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt30月随访.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell11,
            this.xrTableCell16,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell17,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell15});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 0.8D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "体重（kg)";
            this.xrTableCell19.Weight = 1.5000000788370782D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月体重});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.7500000483195004D;
            // 
            // txt12月体重
            // 
            this.txt12月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月体重.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 1.589457E-05F);
            this.txt12月体重.Name = "txt12月体重";
            this.txt12月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt12月体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "上 中 下";
            this.xrTableCell16.Weight = 0.75000004831950018D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月体重});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.75000004831950029D;
            // 
            // txt18月体重
            // 
            this.txt18月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月体重.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt18月体重.Name = "txt18月体重";
            this.txt18月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt18月体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "上 中 下";
            this.xrTableCell13.Weight = 0.75000001017252615D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月体重});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.75000006527370766D;
            // 
            // txt24月体重
            // 
            this.txt24月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月体重.LocationFloat = new DevExpress.Utils.PointFloat(5.000114F, 0F);
            this.txt24月体重.Name = "txt24月体重";
            this.txt24月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt24月体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "上 中 下";
            this.xrTableCell14.Weight = 0.75000006527370766D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月体重});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.74999997033013033D;
            // 
            // txt30月体重
            // 
            this.txt30月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月体重.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt30月体重.Name = "txt30月体重";
            this.txt30月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt30月体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "上 中 下";
            this.xrTableCell15.Weight = 0.74999997033013033D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.8D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Text = "身长（cm)";
            this.xrTableCell20.Weight = 1.5000000788370782D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月身长});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.7500000483195004D;
            // 
            // txt12月身长
            // 
            this.txt12月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 0F);
            this.txt12月身长.Name = "txt12月身长";
            this.txt12月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt12月身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "上 中 下";
            this.xrTableCell22.Weight = 0.75000004831950018D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月身长});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.75000004831950029D;
            // 
            // txt18月身长
            // 
            this.txt18月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月身长.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.txt18月身长.Name = "txt18月身长";
            this.txt18月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt18月身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "上 中 下";
            this.xrTableCell24.Weight = 0.75000001017252615D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月身长});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.75000006527370766D;
            // 
            // txt24月身长
            // 
            this.txt24月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt24月身长.Name = "txt24月身长";
            this.txt24月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt24月身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "上 中 下";
            this.xrTableCell26.Weight = 0.75000006527370766D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月身长});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Weight = 0.74999997033013033D;
            // 
            // txt30月身长
            // 
            this.txt30月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt30月身长.Name = "txt30月身长";
            this.txt30月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt30月身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Text = "上 中 下";
            this.xrTableCell28.Weight = 0.74999997033013033D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell34,
            this.xrTableCell138,
            this.xrTableCell32,
            this.xrTableCell139,
            this.xrTableCell33,
            this.xrTableCell140,
            this.xrTableCell36,
            this.xrTableCell238,
            this.xrTableCell31});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.8D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Weight = 0.28000001012166392D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "面色";
            this.xrTableCell34.Weight = 1.2200000642649336D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Text = "1红润 2其他";
            this.xrTableCell138.Weight = 1.2659012333664421D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell32.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月面色});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Weight = 0.23409882321823361D;
            // 
            // txt12月面色
            // 
            this.txt12月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月面色.BorderWidth = 1F;
            this.txt12月面色.CanGrow = false;
            this.txt12月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt12月面色.Name = "txt12月面色";
            this.txt12月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月面色.StylePriority.UseBorders = false;
            this.txt12月面色.StylePriority.UseBorderWidth = false;
            this.txt12月面色.StylePriority.UseFont = false;
            this.txt12月面色.StylePriority.UsePadding = false;
            this.txt12月面色.StylePriority.UseTextAlignment = false;
            this.txt12月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Text = "1红润 2其他";
            this.xrTableCell139.Weight = 1.2500000486373897D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月面色});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Weight = 0.25000000794728572D;
            // 
            // txt18月面色
            // 
            this.txt18月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月面色.BorderWidth = 1F;
            this.txt18月面色.CanGrow = false;
            this.txt18月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt18月面色.Name = "txt18月面色";
            this.txt18月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月面色.StylePriority.UseBorders = false;
            this.txt18月面色.StylePriority.UseBorderWidth = false;
            this.txt18月面色.StylePriority.UseFont = false;
            this.txt18月面色.StylePriority.UsePadding = false;
            this.txt18月面色.StylePriority.UseTextAlignment = false;
            this.txt18月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Text = "1红润 2其他";
            this.xrTableCell140.Weight = 1.2500006601015974D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月面色});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Weight = 0.25000000905990605D;
            // 
            // txt24月面色
            // 
            this.txt24月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月面色.BorderWidth = 1F;
            this.txt24月面色.CanGrow = false;
            this.txt24月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月面色.LocationFloat = new DevExpress.Utils.PointFloat(3.13F, 4F);
            this.txt24月面色.Name = "txt24月面色";
            this.txt24月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月面色.StylePriority.UseBorders = false;
            this.txt24月面色.StylePriority.UseBorderWidth = false;
            this.txt24月面色.StylePriority.UseFont = false;
            this.txt24月面色.StylePriority.UsePadding = false;
            this.txt24月面色.StylePriority.UseTextAlignment = false;
            this.txt24月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Text = "1红润 2其他";
            this.xrTableCell238.Weight = 1.2499997445742164D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月面色});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 0.24999970388411219D;
            // 
            // txt30月面色
            // 
            this.txt30月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月面色.BorderWidth = 1F;
            this.txt30月面色.CanGrow = false;
            this.txt30月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt30月面色.Name = "txt30月面色";
            this.txt30月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月面色.StylePriority.UseBorders = false;
            this.txt30月面色.StylePriority.UseBorderWidth = false;
            this.txt30月面色.StylePriority.UseFont = false;
            this.txt30月面色.StylePriority.UsePadding = false;
            this.txt30月面色.StylePriority.UseTextAlignment = false;
            this.txt30月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell242,
            this.xrTableCell39,
            this.xrTableCell243,
            this.xrTableCell40,
            this.xrTableCell244,
            this.xrTableCell41,
            this.xrTableCell245,
            this.xrTableCell42});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.Weight = 0.8D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.28000001012166392D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Text = "皮肤";
            this.xrTableCell38.Weight = 1.2200000642649336D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Text = "1未见异常 2异常";
            this.xrTableCell242.Weight = 1.2659009281906484D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月皮肤});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.23409912839402725D;
            // 
            // txt12月皮肤
            // 
            this.txt12月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月皮肤.BorderWidth = 1F;
            this.txt12月皮肤.CanGrow = false;
            this.txt12月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt12月皮肤.Name = "txt12月皮肤";
            this.txt12月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月皮肤.StylePriority.UseBorders = false;
            this.txt12月皮肤.StylePriority.UseBorderWidth = false;
            this.txt12月皮肤.StylePriority.UseFont = false;
            this.txt12月皮肤.StylePriority.UsePadding = false;
            this.txt12月皮肤.StylePriority.UseTextAlignment = false;
            this.txt12月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Text = "1未见异常 2异常";
            this.xrTableCell243.Weight = 1.25000004863739D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月皮肤});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 0.25000000794728577D;
            // 
            // txt18月皮肤
            // 
            this.txt18月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月皮肤.BorderWidth = 1F;
            this.txt18月皮肤.CanGrow = false;
            this.txt18月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt18月皮肤.Name = "txt18月皮肤";
            this.txt18月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月皮肤.StylePriority.UseBorders = false;
            this.txt18月皮肤.StylePriority.UseBorderWidth = false;
            this.txt18月皮肤.StylePriority.UseFont = false;
            this.txt18月皮肤.StylePriority.UsePadding = false;
            this.txt18月皮肤.StylePriority.UseTextAlignment = false;
            this.txt18月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Text = "1未见异常 2异常";
            this.xrTableCell244.Weight = 1.2500006601015974D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月皮肤});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.25000000905990605D;
            // 
            // txt24月皮肤
            // 
            this.txt24月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月皮肤.BorderWidth = 1F;
            this.txt24月皮肤.CanGrow = false;
            this.txt24月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(3.13F, 4F);
            this.txt24月皮肤.Name = "txt24月皮肤";
            this.txt24月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月皮肤.StylePriority.UseBorders = false;
            this.txt24月皮肤.StylePriority.UseBorderWidth = false;
            this.txt24月皮肤.StylePriority.UseFont = false;
            this.txt24月皮肤.StylePriority.UsePadding = false;
            this.txt24月皮肤.StylePriority.UseTextAlignment = false;
            this.txt24月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Text = "1未见异常 2异常";
            this.xrTableCell245.Weight = 1.2499997445742164D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月皮肤});
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.24999970388411219D;
            // 
            // txt30月皮肤
            // 
            this.txt30月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月皮肤.BorderWidth = 1F;
            this.txt30月皮肤.CanGrow = false;
            this.txt30月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt30月皮肤.Name = "txt30月皮肤";
            this.txt30月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月皮肤.StylePriority.UseBorders = false;
            this.txt30月皮肤.StylePriority.UseBorderWidth = false;
            this.txt30月皮肤.StylePriority.UseFont = false;
            this.txt30月皮肤.StylePriority.UsePadding = false;
            this.txt30月皮肤.StylePriority.UseTextAlignment = false;
            this.txt30月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell49,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 1.6D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Weight = 0.28000001012166392D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Text = "前囟";
            this.xrTableCell49.Weight = 1.2200000569534304D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 1.5000000638961788D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow16});
            this.xrTable5.SizeF = new System.Drawing.SizeF(148.5901F, 40F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell247,
            this.xrTableCell76});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Text = "1";
            this.xrTableCell73.Weight = 0.43308273856634294D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = "闭合";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell74.Weight = 0.9304203772183216D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "2";
            this.xrTableCell75.Weight = 0.30482012810836495D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Text = "未闭";
            this.xrTableCell247.Weight = 0.888801583277827D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月闭合});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell76.Weight = 0.44440157277723646D;
            // 
            // txt12月闭合
            // 
            this.txt12月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月闭合.BorderWidth = 1F;
            this.txt12月闭合.CanGrow = false;
            this.txt12月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月闭合.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.985535F);
            this.txt12月闭合.Name = "txt12月闭合";
            this.txt12月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月闭合.StylePriority.UseBorders = false;
            this.txt12月闭合.StylePriority.UseBorderWidth = false;
            this.txt12月闭合.StylePriority.UseFont = false;
            this.txt12月闭合.StylePriority.UsePadding = false;
            this.txt12月闭合.StylePriority.UseTextAlignment = false;
            this.txt12月闭合.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月前囟});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.9393014475208924D;
            // 
            // txt12月前囟
            // 
            this.txt12月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月前囟.LocationFloat = new DevExpress.Utils.PointFloat(2.5F, 0F);
            this.txt12月前囟.Name = "txt12月前囟";
            this.txt12月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月前囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt12月前囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.Text = "cm x";
            this.xrTableCell78.Weight = 0.7172400274953693D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月后囟});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.Weight = 0.900584189262272D;
            // 
            // txt12月后囟
            // 
            this.txt12月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.583252F, 0F);
            this.txt12月后囟.Name = "txt12月后囟";
            this.txt12月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月后囟.SizeF = new System.Drawing.SizeF(42.40991F, 18F);
            this.txt12月后囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "cm";
            this.xrTableCell80.Weight = 0.44440073566955912D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 1.5000000565846756D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1.083374F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable3.SizeF = new System.Drawing.SizeF(148.5901F, 40F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell248,
            this.xrTableCell60});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Text = "1";
            this.xrTableCell57.Weight = 0.43308273856634294D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "闭合";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell58.Weight = 0.90853710057693338D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Text = "2";
            this.xrTableCell59.Weight = 0.49853568773562085D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.StylePriority.UsePadding = false;
            this.xrTableCell248.Text = "未闭";
            this.xrTableCell248.Weight = 0.66296587243964067D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月闭合});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.49840500062955495D;
            // 
            // txt18月闭合
            // 
            this.txt18月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月闭合.BorderWidth = 1F;
            this.txt18月闭合.CanGrow = false;
            this.txt18月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月闭合.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.985535F);
            this.txt18月闭合.Name = "txt18月闭合";
            this.txt18月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月闭合.StylePriority.UseBorders = false;
            this.txt18月闭合.StylePriority.UseBorderWidth = false;
            this.txt18月闭合.StylePriority.UseFont = false;
            this.txt18月闭合.StylePriority.UsePadding = false;
            this.txt18月闭合.StylePriority.UseTextAlignment = false;
            this.txt18月闭合.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月前囟});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Weight = 0.9393014475208924D;
            // 
            // txt18月前囟
            // 
            this.txt18月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月前囟.LocationFloat = new DevExpress.Utils.PointFloat(3.916656F, 0F);
            this.txt18月前囟.Name = "txt18月前囟";
            this.txt18月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月前囟.SizeF = new System.Drawing.SizeF(42.58334F, 18F);
            this.txt18月前囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Text = "cm x";
            this.xrTableCell62.Weight = 0.7172400274953693D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell63.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月后囟});
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Weight = 0.84657891204082736D;
            // 
            // txt18月后囟
            // 
            this.txt18月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.5832214F, 0F);
            this.txt18月后囟.Name = "txt18月后囟";
            this.txt18月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月后囟.SizeF = new System.Drawing.SizeF(41.32654F, 18F);
            this.txt18月后囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Text = "cm";
            this.xrTableCell64.Weight = 0.49840601289100378D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 1.5000006691615035D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable4.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14});
            this.xrTable4.SizeF = new System.Drawing.SizeF(150.0001F, 40F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell249,
            this.xrTableCell68});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Text = "1";
            this.xrTableCell65.Weight = 0.43308273856634294D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "闭合";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell66.Weight = 0.91760468020535491D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Text = "2";
            this.xrTableCell67.Weight = 0.46273624048321765D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.StylePriority.UsePadding = false;
            this.xrTableCell249.Text = "未闭";
            this.xrTableCell249.Weight = 0.59405137034658873D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月闭合});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.59405137034658873D;
            // 
            // txt24月闭合
            // 
            this.txt24月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月闭合.BorderWidth = 1F;
            this.txt24月闭合.CanGrow = false;
            this.txt24月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月闭合.LocationFloat = new DevExpress.Utils.PointFloat(7.812373F, 2.985541F);
            this.txt24月闭合.Name = "txt24月闭合";
            this.txt24月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月闭合.StylePriority.UseBorders = false;
            this.txt24月闭合.StylePriority.UseBorderWidth = false;
            this.txt24月闭合.StylePriority.UseFont = false;
            this.txt24月闭合.StylePriority.UsePadding = false;
            this.txt24月闭合.StylePriority.UseTextAlignment = false;
            this.txt24月闭合.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月前囟});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Weight = 0.930472425439123D;
            // 
            // txt24月前囟
            // 
            this.txt24月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月前囟.LocationFloat = new DevExpress.Utils.PointFloat(5.000061F, 0F);
            this.txt24月前囟.Name = "txt24月前囟";
            this.txt24月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月前囟.SizeF = new System.Drawing.SizeF(41.49991F, 18F);
            this.txt24月前囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "cm x";
            this.xrTableCell70.Weight = 0.72606904957713869D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月后囟});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Weight = 0.900584189262272D;
            // 
            // txt24月后囟
            // 
            this.txt24月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.5832825F, 0F);
            this.txt24月后囟.Name = "txt24月后囟";
            this.txt24月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月后囟.SizeF = new System.Drawing.SizeF(41.63177F, 18F);
            this.txt24月后囟.StylePriority.UseBorders = false;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Text = "cm";
            this.xrTableCell72.Weight = 0.44440073566955912D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine47,
            this.xrLine48,
            this.xrLine49,
            this.xrLine50,
            this.xrLine51});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 1.4999994484583286D;
            // 
            // xrLine47
            // 
            this.xrLine47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine47.BorderWidth = 2F;
            this.xrLine47.LineWidth = 2;
            this.xrLine47.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 14.91379F);
            this.xrLine47.Name = "xrLine47";
            this.xrLine47.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine47.StylePriority.UseBorders = false;
            this.xrLine47.StylePriority.UseBorderWidth = false;
            // 
            // xrLine48
            // 
            this.xrLine48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine48.BorderWidth = 2F;
            this.xrLine48.LineWidth = 2;
            this.xrLine48.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 14.91379F);
            this.xrLine48.Name = "xrLine48";
            this.xrLine48.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine48.StylePriority.UseBorders = false;
            this.xrLine48.StylePriority.UseBorderWidth = false;
            // 
            // xrLine49
            // 
            this.xrLine49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine49.BorderWidth = 2F;
            this.xrLine49.LineWidth = 2;
            this.xrLine49.LocationFloat = new DevExpress.Utils.PointFloat(67.50001F, 14.91379F);
            this.xrLine49.Name = "xrLine49";
            this.xrLine49.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine49.StylePriority.UseBorders = false;
            this.xrLine49.StylePriority.UseBorderWidth = false;
            // 
            // xrLine50
            // 
            this.xrLine50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine50.BorderWidth = 2F;
            this.xrLine50.LineWidth = 2;
            this.xrLine50.LocationFloat = new DevExpress.Utils.PointFloat(82.50004F, 14.91379F);
            this.xrLine50.Name = "xrLine50";
            this.xrLine50.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine50.StylePriority.UseBorders = false;
            this.xrLine50.StylePriority.UseBorderWidth = false;
            // 
            // xrLine51
            // 
            this.xrLine51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine51.BorderWidth = 2F;
            this.xrLine51.LineWidth = 2;
            this.xrLine51.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 14.91379F);
            this.xrLine51.Name = "xrLine51";
            this.xrLine51.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine51.StylePriority.UseBorders = false;
            this.xrLine51.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell258,
            this.xrTableCell89,
            this.xrTableCell259,
            this.xrTableCell90,
            this.xrTableCell260,
            this.xrTableCell91,
            this.xrTableCell261,
            this.xrTableCell92});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 0.80000000000000016D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.Weight = 0.28000001012166392D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Text = "眼外观";
            this.xrTableCell56.Weight = 1.2200000569534304D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Text = "1未见异常 2异常";
            this.xrTableCell258.Weight = 1.2659009318463999D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月眼外观});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.23409913204977889D;
            // 
            // txt12月眼外观
            // 
            this.txt12月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月眼外观.BorderWidth = 1F;
            this.txt12月眼外观.CanGrow = false;
            this.txt12月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.874161F);
            this.txt12月眼外观.Name = "txt12月眼外观";
            this.txt12月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月眼外观.StylePriority.UseBorders = false;
            this.txt12月眼外观.StylePriority.UseBorderWidth = false;
            this.txt12月眼外观.StylePriority.UseFont = false;
            this.txt12月眼外观.StylePriority.UsePadding = false;
            this.txt12月眼外观.StylePriority.UseTextAlignment = false;
            this.txt12月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Text = "1未见异常 2异常";
            this.xrTableCell259.Weight = 1.25000004863739D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月眼外观});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Weight = 0.25000000794728572D;
            // 
            // txt18月眼外观
            // 
            this.txt18月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月眼外观.BorderWidth = 1F;
            this.txt18月眼外观.CanGrow = false;
            this.txt18月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.874161F);
            this.txt18月眼外观.Name = "txt18月眼外观";
            this.txt18月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月眼外观.StylePriority.UseBorders = false;
            this.txt18月眼外观.StylePriority.UseBorderWidth = false;
            this.txt18月眼外观.StylePriority.UseFont = false;
            this.txt18月眼外观.StylePriority.UsePadding = false;
            this.txt18月眼外观.StylePriority.UseTextAlignment = false;
            this.txt18月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Text = "1未见异常 2异常";
            this.xrTableCell260.Weight = 1.2812506613731631D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月眼外观});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.21875000778834031D;
            // 
            // txt24月眼外观
            // 
            this.txt24月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月眼外观.BorderWidth = 1F;
            this.txt24月眼外观.CanGrow = false;
            this.txt24月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.874161F);
            this.txt24月眼外观.Name = "txt24月眼外观";
            this.txt24月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月眼外观.StylePriority.UseBorders = false;
            this.txt24月眼外观.StylePriority.UseBorderWidth = false;
            this.txt24月眼外观.StylePriority.UseFont = false;
            this.txt24月眼外观.StylePriority.UsePadding = false;
            this.txt24月眼外观.StylePriority.UseTextAlignment = false;
            this.txt24月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Text = "1未见异常 2异常";
            this.xrTableCell261.Weight = 1.2437485236167287D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月眼外观});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.25625092484159995D;
            // 
            // txt30月眼外观
            // 
            this.txt30月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月眼外观.BorderWidth = 1F;
            this.txt30月眼外观.CanGrow = false;
            this.txt30月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0.6251221F, 1.874161F);
            this.txt30月眼外观.Name = "txt30月眼外观";
            this.txt30月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月眼外观.StylePriority.UseBorders = false;
            this.txt30月眼外观.StylePriority.UseBorderWidth = false;
            this.txt30月眼外观.StylePriority.UseFont = false;
            this.txt30月眼外观.StylePriority.UsePadding = false;
            this.txt30月眼外观.StylePriority.UseTextAlignment = false;
            this.txt30月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell262,
            this.xrTableCell95,
            this.xrTableCell263,
            this.xrTableCell96,
            this.xrTableCell264,
            this.xrTableCell97,
            this.xrTableCell265,
            this.xrTableCell98});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.80000000000000016D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.28000001012166392D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "耳外观";
            this.xrTableCell94.Weight = 1.2200000569534304D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Text = "1未见异常 2异常";
            this.xrTableCell262.Weight = 1.2659009318463999D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月耳外观});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.23409913204977884D;
            // 
            // txt12月耳外观
            // 
            this.txt12月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月耳外观.BorderWidth = 1F;
            this.txt12月耳外观.CanGrow = false;
            this.txt12月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.670364F);
            this.txt12月耳外观.Name = "txt12月耳外观";
            this.txt12月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月耳外观.StylePriority.UseBorders = false;
            this.txt12月耳外观.StylePriority.UseBorderWidth = false;
            this.txt12月耳外观.StylePriority.UseFont = false;
            this.txt12月耳外观.StylePriority.UsePadding = false;
            this.txt12月耳外观.StylePriority.UseTextAlignment = false;
            this.txt12月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Text = "1未见异常 2异常";
            this.xrTableCell263.Weight = 1.25000004863739D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月耳外观});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.25000000794728572D;
            // 
            // txt18月耳外观
            // 
            this.txt18月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月耳外观.BorderWidth = 1F;
            this.txt18月耳外观.CanGrow = false;
            this.txt18月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.670364F);
            this.txt18月耳外观.Name = "txt18月耳外观";
            this.txt18月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月耳外观.StylePriority.UseBorders = false;
            this.txt18月耳外观.StylePriority.UseBorderWidth = false;
            this.txt18月耳外观.StylePriority.UseFont = false;
            this.txt18月耳外观.StylePriority.UsePadding = false;
            this.txt18月耳外观.StylePriority.UseTextAlignment = false;
            this.txt18月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Text = "1未见异常 2异常";
            this.xrTableCell264.Weight = 1.2812506613731634D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月耳外观});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.21875000778834031D;
            // 
            // txt24月耳外观
            // 
            this.txt24月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月耳外观.BorderWidth = 1F;
            this.txt24月耳外观.CanGrow = false;
            this.txt24月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.670364F);
            this.txt24月耳外观.Name = "txt24月耳外观";
            this.txt24月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月耳外观.StylePriority.UseBorders = false;
            this.txt24月耳外观.StylePriority.UseBorderWidth = false;
            this.txt24月耳外观.StylePriority.UseFont = false;
            this.txt24月耳外观.StylePriority.UsePadding = false;
            this.txt24月耳外观.StylePriority.UseTextAlignment = false;
            this.txt24月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Text = "1未见异常 2异常";
            this.xrTableCell265.Weight = 1.2437485236167287D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月耳外观});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 0.25625092484159995D;
            // 
            // txt30月耳外观
            // 
            this.txt30月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月耳外观.BorderWidth = 1F;
            this.txt30月耳外观.CanGrow = false;
            this.txt30月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0.6251221F, 5.670364F);
            this.txt30月耳外观.Name = "txt30月耳外观";
            this.txt30月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月耳外观.StylePriority.UseBorders = false;
            this.txt30月耳外观.StylePriority.UseBorderWidth = false;
            this.txt30月耳外观.StylePriority.UseFont = false;
            this.txt30月耳外观.StylePriority.UsePadding = false;
            this.txt30月耳外观.StylePriority.UseTextAlignment = false;
            this.txt30月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell267,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell266,
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 0.80000000000000016D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.Text = "体";
            this.xrTableCell99.Weight = 0.28000001012166392D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Text = "听力";
            this.xrTableCell100.Weight = 1.2200000569534304D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Text = "1通过 2未通过";
            this.xrTableCell267.Weight = 1.2500000522931414D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月听力});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Weight = 0.25000001160303731D;
            // 
            // txt12月听力
            // 
            this.txt12月听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月听力.BorderWidth = 1F;
            this.txt12月听力.CanGrow = false;
            this.txt12月听力.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月听力.LocationFloat = new DevExpress.Utils.PointFloat(1.590093F, 3.710016F);
            this.txt12月听力.Name = "txt12月听力";
            this.txt12月听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月听力.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月听力.StylePriority.UseBorders = false;
            this.txt12月听力.StylePriority.UseBorderWidth = false;
            this.txt12月听力.StylePriority.UseFont = false;
            this.txt12月听力.StylePriority.UsePadding = false;
            this.txt12月听力.StylePriority.UseTextAlignment = false;
            this.txt12月听力.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine12,
            this.xrLine13,
            this.xrLine14,
            this.xrLine15,
            this.xrLine16});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 1.5000000565846756D;
            // 
            // xrLine12
            // 
            this.xrLine12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine12.BorderWidth = 2F;
            this.xrLine12.LineWidth = 2;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine12.StylePriority.UseBorders = false;
            this.xrLine12.StylePriority.UseBorderWidth = false;
            // 
            // xrLine13
            // 
            this.xrLine13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine13.BorderWidth = 2F;
            this.xrLine13.LineWidth = 2;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine13.StylePriority.UseBorders = false;
            this.xrLine13.StylePriority.UseBorderWidth = false;
            // 
            // xrLine14
            // 
            this.xrLine14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine14.BorderWidth = 2F;
            this.xrLine14.LineWidth = 2;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine14.StylePriority.UseBorders = false;
            this.xrLine14.StylePriority.UseBorderWidth = false;
            // 
            // xrLine15
            // 
            this.xrLine15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine15.BorderWidth = 2F;
            this.xrLine15.LineWidth = 2;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 3.71F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine15.StylePriority.UseBorders = false;
            this.xrLine15.StylePriority.UseBorderWidth = false;
            // 
            // xrLine16
            // 
            this.xrLine16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine16.BorderWidth = 2F;
            this.xrLine16.LineWidth = 2;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine16.StylePriority.UseBorders = false;
            this.xrLine16.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Text = "1通过 2未通过";
            this.xrTableCell266.Weight = 1.2779144795967885D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月听力});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.22208618956471493D;
            // 
            // txt24月听力
            // 
            this.txt24月听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月听力.BorderWidth = 1F;
            this.txt24月听力.CanGrow = false;
            this.txt24月听力.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月听力.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.706678F);
            this.txt24月听力.Name = "txt24月听力";
            this.txt24月听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月听力.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月听力.StylePriority.UseBorders = false;
            this.txt24月听力.StylePriority.UseBorderWidth = false;
            this.txt24月听力.StylePriority.UseFont = false;
            this.txt24月听力.StylePriority.UsePadding = false;
            this.txt24月听力.StylePriority.UseTextAlignment = false;
            this.txt24月听力.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine17,
            this.xrLine18,
            this.xrLine19,
            this.xrLine20,
            this.xrLine21});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 1.4999994484583286D;
            // 
            // xrLine17
            // 
            this.xrLine17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine17.BorderWidth = 2F;
            this.xrLine17.LineWidth = 2;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 3.71F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine17.StylePriority.UseBorders = false;
            this.xrLine17.StylePriority.UseBorderWidth = false;
            // 
            // xrLine18
            // 
            this.xrLine18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine18.BorderWidth = 2F;
            this.xrLine18.LineWidth = 2;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(82.50003F, 3.71F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine18.StylePriority.UseBorders = false;
            this.xrLine18.StylePriority.UseBorderWidth = false;
            // 
            // xrLine19
            // 
            this.xrLine19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine19.BorderWidth = 2F;
            this.xrLine19.LineWidth = 2;
            this.xrLine19.LocationFloat = new DevExpress.Utils.PointFloat(67.5F, 3.71F);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine19.StylePriority.UseBorders = false;
            this.xrLine19.StylePriority.UseBorderWidth = false;
            // 
            // xrLine20
            // 
            this.xrLine20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine20.BorderWidth = 2F;
            this.xrLine20.LineWidth = 2;
            this.xrLine20.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 3.71F);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine20.StylePriority.UseBorders = false;
            this.xrLine20.StylePriority.UseBorderWidth = false;
            // 
            // xrLine21
            // 
            this.xrLine21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine21.BorderWidth = 2F;
            this.xrLine21.LineWidth = 2;
            this.xrLine21.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 3.71F);
            this.xrLine21.Name = "xrLine21";
            this.xrLine21.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine21.StylePriority.UseBorders = false;
            this.xrLine21.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.txt12月出牙数,
            this.xrTableCell50,
            this.txt12月龋齿数,
            this.txt18月出牙数,
            this.xrTableCell52,
            this.txt18月龋齿数,
            this.txt24月出牙数,
            this.xrTableCell54,
            this.txt24月龋齿数,
            this.txt30月出牙数,
            this.xrTableCell82,
            this.txt30月龋齿数});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.StylePriority.UsePadding = false;
            this.xrTableRow21.Weight = 0.80000000000000016D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.StylePriority.UsePadding = false;
            this.xrTableCell105.Text = "格";
            this.xrTableCell105.Weight = 0.28000001012166392D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "出牙/龋齿数";
            this.xrTableCell106.Weight = 1.2200000569534304D;
            // 
            // txt12月出牙数
            // 
            this.txt12月出牙数.Name = "txt12月出牙数";
            this.txt12月出牙数.StylePriority.UseTextAlignment = false;
            this.txt12月出牙数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt12月出牙数.Weight = 0.70000002991358423D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UsePadding = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "/";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 0.10000000478426602D;
            // 
            // txt12月龋齿数
            // 
            this.txt12月龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月龋齿数.Name = "txt12月龋齿数";
            this.txt12月龋齿数.StylePriority.UseBorders = false;
            this.txt12月龋齿数.StylePriority.UseTextAlignment = false;
            this.txt12月龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt12月龋齿数.Weight = 0.7000000291983286D;
            // 
            // txt18月出牙数
            // 
            this.txt18月出牙数.Name = "txt18月出牙数";
            this.txt18月出牙数.StylePriority.UseTextAlignment = false;
            this.txt18月出牙数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt18月出牙数.Weight = 0.70000002625783264D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.Text = "/";
            this.xrTableCell52.Weight = 0.10000000295639022D;
            // 
            // txt18月龋齿数
            // 
            this.txt18月龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月龋齿数.Name = "txt18月龋齿数";
            this.txt18月龋齿数.StylePriority.UseBorders = false;
            this.txt18月龋齿数.StylePriority.UseTextAlignment = false;
            this.txt18月龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt18月龋齿数.Weight = 0.70000002737045275D;
            // 
            // txt24月出牙数
            // 
            this.txt24月出牙数.Name = "txt24月出牙数";
            this.txt24月出牙数.StylePriority.UseTextAlignment = false;
            this.txt24月出牙数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt24月出牙数.Weight = 0.70000002737045286D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.StylePriority.UsePadding = false;
            this.xrTableCell54.Text = "/";
            this.xrTableCell54.Weight = 0.10000000351270039D;
            // 
            // txt24月龋齿数
            // 
            this.txt24月龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月龋齿数.Name = "txt24月龋齿数";
            this.txt24月龋齿数.StylePriority.UseBorders = false;
            this.txt24月龋齿数.StylePriority.UseTextAlignment = false;
            this.txt24月龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt24月龋齿数.Weight = 0.70000063827835035D;
            // 
            // txt30月出牙数
            // 
            this.txt30月出牙数.Name = "txt30月出牙数";
            this.txt30月出牙数.StylePriority.UseTextAlignment = false;
            this.txt30月出牙数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt30月出牙数.Weight = 0.70000002737045275D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.StylePriority.UsePadding = false;
            this.xrTableCell82.Text = "/";
            this.xrTableCell82.Weight = 0.10000000351270033D;
            // 
            // txt30月龋齿数
            // 
            this.txt30月龋齿数.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月龋齿数.Name = "txt30月龋齿数";
            this.txt30月龋齿数.StylePriority.UseBorders = false;
            this.txt30月龋齿数.StylePriority.UseTextAlignment = false;
            this.txt30月龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt30月龋齿数.Weight = 0.69999941757517548D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell268,
            this.xrTableCell113,
            this.xrTableCell271,
            this.xrTableCell114,
            this.xrTableCell275,
            this.xrTableCell115,
            this.xrTableCell279,
            this.xrTableCell116});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.80000000000000016D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Text = "栓检";
            this.xrTableCell111.Weight = 0.28000001012166392D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Text = "心肺";
            this.xrTableCell112.Weight = 1.2200000569534304D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Text = "1未见异常 2异常";
            this.xrTableCell268.Weight = 1.2659012370221936D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月心肺});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Weight = 0.2340988268739852D;
            // 
            // txt12月心肺
            // 
            this.txt12月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月心肺.BorderWidth = 1F;
            this.txt12月心肺.CanGrow = false;
            this.txt12月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.558975F);
            this.txt12月心肺.Name = "txt12月心肺";
            this.txt12月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月心肺.StylePriority.UseBorders = false;
            this.txt12月心肺.StylePriority.UseBorderWidth = false;
            this.txt12月心肺.StylePriority.UseFont = false;
            this.txt12月心肺.StylePriority.UsePadding = false;
            this.txt12月心肺.StylePriority.UseTextAlignment = false;
            this.txt12月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Text = "1未见异常 2异常";
            this.xrTableCell271.Weight = 1.25000004863739D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月心肺});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.25000000794728572D;
            // 
            // txt18月心肺
            // 
            this.txt18月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月心肺.BorderWidth = 1F;
            this.txt18月心肺.CanGrow = false;
            this.txt18月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.558975F);
            this.txt18月心肺.Name = "txt18月心肺";
            this.txt18月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月心肺.StylePriority.UseBorders = false;
            this.txt18月心肺.StylePriority.UseBorderWidth = false;
            this.txt18月心肺.StylePriority.UseFont = false;
            this.txt18月心肺.StylePriority.UsePadding = false;
            this.txt18月心肺.StylePriority.UseTextAlignment = false;
            this.txt18月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Text = "1未见异常 2异常";
            this.xrTableCell275.Weight = 1.2812512717247506D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月心肺});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.21874939743675292D;
            // 
            // txt24月心肺
            // 
            this.txt24月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月心肺.BorderWidth = 1F;
            this.txt24月心肺.CanGrow = false;
            this.txt24月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt24月心肺.Name = "txt24月心肺";
            this.txt24月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月心肺.StylePriority.UseBorders = false;
            this.txt24月心肺.StylePriority.UseBorderWidth = false;
            this.txt24月心肺.StylePriority.UseFont = false;
            this.txt24月心肺.StylePriority.UsePadding = false;
            this.txt24月心肺.StylePriority.UseTextAlignment = false;
            this.txt24月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "1未见异常 2异常";
            this.xrTableCell279.Weight = 1.2437485236167285D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell116.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月心肺});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.25625092484160006D;
            // 
            // txt30月心肺
            // 
            this.txt30月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月心肺.BorderWidth = 1F;
            this.txt30月心肺.CanGrow = false;
            this.txt30月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0.6251221F, 4.558975F);
            this.txt30月心肺.Name = "txt30月心肺";
            this.txt30月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月心肺.StylePriority.UseBorders = false;
            this.txt30月心肺.StylePriority.UseBorderWidth = false;
            this.txt30月心肺.StylePriority.UseFont = false;
            this.txt30月心肺.StylePriority.UsePadding = false;
            this.txt30月心肺.StylePriority.UseTextAlignment = false;
            this.txt30月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell269,
            this.xrTableCell119,
            this.xrTableCell272,
            this.xrTableCell120,
            this.xrTableCell276,
            this.xrTableCell121,
            this.xrTableCell280,
            this.xrTableCell122});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.80000000000000016D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Text = "查";
            this.xrTableCell117.Weight = 0.28000001012166392D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "腹部";
            this.xrTableCell118.Weight = 1.2200000569534304D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "1未见异常 2异常";
            this.xrTableCell269.Weight = 1.2659012370221934D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月腹部});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.23409882687398523D;
            // 
            // txt12月腹部
            // 
            this.txt12月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月腹部.BorderWidth = 1F;
            this.txt12月腹部.CanGrow = false;
            this.txt12月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt12月腹部.Name = "txt12月腹部";
            this.txt12月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月腹部.StylePriority.UseBorders = false;
            this.txt12月腹部.StylePriority.UseBorderWidth = false;
            this.txt12月腹部.StylePriority.UseFont = false;
            this.txt12月腹部.StylePriority.UsePadding = false;
            this.txt12月腹部.StylePriority.UseTextAlignment = false;
            this.txt12月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Text = "1未见异常 2异常";
            this.xrTableCell272.Weight = 1.25000004863739D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月腹部});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.25000000794728572D;
            // 
            // txt18月腹部
            // 
            this.txt18月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月腹部.BorderWidth = 1F;
            this.txt18月腹部.CanGrow = false;
            this.txt18月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月腹部.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0F);
            this.txt18月腹部.Name = "txt18月腹部";
            this.txt18月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月腹部.StylePriority.UseBorders = false;
            this.txt18月腹部.StylePriority.UseBorderWidth = false;
            this.txt18月腹部.StylePriority.UseFont = false;
            this.txt18月腹部.StylePriority.UsePadding = false;
            this.txt18月腹部.StylePriority.UseTextAlignment = false;
            this.txt18月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Text = "1未见异常 2异常";
            this.xrTableCell276.Weight = 1.2779144795967887D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月腹部});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.22208618956471482D;
            // 
            // txt24月腹部
            // 
            this.txt24月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月腹部.BorderWidth = 1F;
            this.txt24月腹部.CanGrow = false;
            this.txt24月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月腹部.LocationFloat = new DevExpress.Utils.PointFloat(0.3336588F, 0F);
            this.txt24月腹部.Name = "txt24月腹部";
            this.txt24月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月腹部.StylePriority.UseBorders = false;
            this.txt24月腹部.StylePriority.UseBorderWidth = false;
            this.txt24月腹部.StylePriority.UseFont = false;
            this.txt24月腹部.StylePriority.UsePadding = false;
            this.txt24月腹部.StylePriority.UseTextAlignment = false;
            this.txt24月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Text = "1未见异常 2异常";
            this.xrTableCell280.Weight = 1.2437485236167287D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月腹部});
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Weight = 0.25625092484159995D;
            // 
            // txt30月腹部
            // 
            this.txt30月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月腹部.BorderWidth = 1F;
            this.txt30月腹部.CanGrow = false;
            this.txt30月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月腹部.LocationFloat = new DevExpress.Utils.PointFloat(0.6251017F, 3.178914E-05F);
            this.txt30月腹部.Name = "txt30月腹部";
            this.txt30月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月腹部.StylePriority.UseBorders = false;
            this.txt30月腹部.StylePriority.UseBorderWidth = false;
            this.txt30月腹部.StylePriority.UseFont = false;
            this.txt30月腹部.StylePriority.UsePadding = false;
            this.txt30月腹部.StylePriority.UseTextAlignment = false;
            this.txt30月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell270,
            this.xrTableCell131,
            this.xrTableCell273,
            this.xrTableCell132,
            this.xrTableCell277,
            this.xrTableCell133,
            this.xrTableCell281,
            this.xrTableCell134});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseBorders = false;
            this.xrTableRow25.Weight = 0.80000000000000027D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Weight = 0.28000001012166392D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "四肢";
            this.xrTableCell130.Weight = 1.2200000569534304D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Text = "1未见异常 2异常";
            this.xrTableCell270.Weight = 1.2659009318463999D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月四肢});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.23409913204977884D;
            // 
            // txt12月四肢
            // 
            this.txt12月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月四肢.BorderWidth = 1F;
            this.txt12月四肢.CanGrow = false;
            this.txt12月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月四肢.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt12月四肢.Name = "txt12月四肢";
            this.txt12月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月四肢.StylePriority.UseBorders = false;
            this.txt12月四肢.StylePriority.UseBorderWidth = false;
            this.txt12月四肢.StylePriority.UseFont = false;
            this.txt12月四肢.StylePriority.UsePadding = false;
            this.txt12月四肢.StylePriority.UseTextAlignment = false;
            this.txt12月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Text = "1未见异常 2异常";
            this.xrTableCell273.Weight = 1.25000004863739D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月四肢});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.25000000794728572D;
            // 
            // txt18月四肢
            // 
            this.txt18月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月四肢.BorderWidth = 1F;
            this.txt18月四肢.CanGrow = false;
            this.txt18月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月四肢.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt18月四肢.Name = "txt18月四肢";
            this.txt18月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月四肢.StylePriority.UseBorders = false;
            this.txt18月四肢.StylePriority.UseBorderWidth = false;
            this.txt18月四肢.StylePriority.UseFont = false;
            this.txt18月四肢.StylePriority.UsePadding = false;
            this.txt18月四肢.StylePriority.UseTextAlignment = false;
            this.txt18月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Text = "1未见异常 2异常";
            this.xrTableCell277.Weight = 1.2812506613731634D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月四肢});
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.21875000778834031D;
            // 
            // txt24月四肢
            // 
            this.txt24月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月四肢.BorderWidth = 1F;
            this.txt24月四肢.CanGrow = false;
            this.txt24月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月四肢.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt24月四肢.Name = "txt24月四肢";
            this.txt24月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月四肢.StylePriority.UseBorders = false;
            this.txt24月四肢.StylePriority.UseBorderWidth = false;
            this.txt24月四肢.StylePriority.UseFont = false;
            this.txt24月四肢.StylePriority.UsePadding = false;
            this.txt24月四肢.StylePriority.UseTextAlignment = false;
            this.txt24月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Text = "1未见异常 2异常";
            this.xrTableCell281.Weight = 1.2499997445742164D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月四肢});
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Weight = 0.24999970388411219D;
            // 
            // txt30月四肢
            // 
            this.txt30月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月四肢.BorderWidth = 1F;
            this.txt30月四肢.CanGrow = false;
            this.txt30月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月四肢.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt30月四肢.Name = "txt30月四肢";
            this.txt30月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月四肢.StylePriority.UseBorders = false;
            this.txt30月四肢.StylePriority.UseBorderWidth = false;
            this.txt30月四肢.StylePriority.UseFont = false;
            this.txt30月四肢.StylePriority.UsePadding = false;
            this.txt30月四肢.StylePriority.UseTextAlignment = false;
            this.txt30月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.txt12月步态,
            this.xrTableCell274,
            this.txt18月步态,
            this.xrTableCell278,
            this.txt24月步态,
            this.xrTableCell282,
            this.txt30月步态});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseBorders = false;
            this.xrTableRow26.Weight = 0.80000000000000027D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.Weight = 0.28000001012166392D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "步态";
            this.xrTableCell136.Weight = 1.2200000569534304D;
            // 
            // txt12月步态
            // 
            this.txt12月步态.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine37,
            this.xrLine38,
            this.xrLine39,
            this.xrLine40,
            this.xrLine41});
            this.txt12月步态.Name = "txt12月步态";
            this.txt12月步态.Weight = 1.5000000638961788D;
            // 
            // xrLine37
            // 
            this.xrLine37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine37.BorderWidth = 2F;
            this.xrLine37.LineWidth = 2;
            this.xrLine37.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 4.311896F);
            this.xrLine37.Name = "xrLine37";
            this.xrLine37.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine37.StylePriority.UseBorders = false;
            this.xrLine37.StylePriority.UseBorderWidth = false;
            // 
            // xrLine38
            // 
            this.xrLine38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine38.BorderWidth = 2F;
            this.xrLine38.LineWidth = 2;
            this.xrLine38.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 4.311896F);
            this.xrLine38.Name = "xrLine38";
            this.xrLine38.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine38.StylePriority.UseBorders = false;
            this.xrLine38.StylePriority.UseBorderWidth = false;
            // 
            // xrLine39
            // 
            this.xrLine39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine39.BorderWidth = 2F;
            this.xrLine39.LineWidth = 2;
            this.xrLine39.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 4.311896F);
            this.xrLine39.Name = "xrLine39";
            this.xrLine39.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine39.StylePriority.UseBorders = false;
            this.xrLine39.StylePriority.UseBorderWidth = false;
            // 
            // xrLine40
            // 
            this.xrLine40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine40.BorderWidth = 2F;
            this.xrLine40.LineWidth = 2;
            this.xrLine40.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 4.311896F);
            this.xrLine40.Name = "xrLine40";
            this.xrLine40.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine40.StylePriority.UseBorders = false;
            this.xrLine40.StylePriority.UseBorderWidth = false;
            // 
            // xrLine41
            // 
            this.xrLine41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine41.BorderWidth = 2F;
            this.xrLine41.LineWidth = 2;
            this.xrLine41.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 4.311896F);
            this.xrLine41.Name = "xrLine41";
            this.xrLine41.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine41.StylePriority.UseBorders = false;
            this.xrLine41.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Text = "1未见异常 2异常";
            this.xrTableCell274.Weight = 1.25000004863739D;
            // 
            // txt18月步态
            // 
            this.txt18月步态.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月步态.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月步大});
            this.txt18月步态.Multiline = true;
            this.txt18月步态.Name = "txt18月步态";
            this.txt18月步态.StylePriority.UseBorders = false;
            this.txt18月步态.Weight = 0.25000000794728572D;
            // 
            // txt18月步大
            // 
            this.txt18月步大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月步大.BorderWidth = 1F;
            this.txt18月步大.CanGrow = false;
            this.txt18月步大.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月步大.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.447586F);
            this.txt18月步大.Name = "txt18月步大";
            this.txt18月步大.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月步大.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月步大.StylePriority.UseBorders = false;
            this.txt18月步大.StylePriority.UseBorderWidth = false;
            this.txt18月步大.StylePriority.UseFont = false;
            this.txt18月步大.StylePriority.UsePadding = false;
            this.txt18月步大.StylePriority.UseTextAlignment = false;
            this.txt18月步大.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Text = "1未见异常 2异常";
            this.xrTableCell278.Weight = 1.2779144795967887D;
            // 
            // txt24月步态
            // 
            this.txt24月步态.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月步态.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月步大});
            this.txt24月步态.Multiline = true;
            this.txt24月步态.Name = "txt24月步态";
            this.txt24月步态.StylePriority.UseBorders = false;
            this.txt24月步态.Weight = 0.22208618956471482D;
            // 
            // txt24月步大
            // 
            this.txt24月步大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月步大.BorderWidth = 1F;
            this.txt24月步大.CanGrow = false;
            this.txt24月步大.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月步大.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.447586F);
            this.txt24月步大.Name = "txt24月步大";
            this.txt24月步大.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月步大.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月步大.StylePriority.UseBorders = false;
            this.txt24月步大.StylePriority.UseBorderWidth = false;
            this.txt24月步大.StylePriority.UseFont = false;
            this.txt24月步大.StylePriority.UsePadding = false;
            this.txt24月步大.StylePriority.UseTextAlignment = false;
            this.txt24月步大.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Text = "1未见异常 2异常";
            this.xrTableCell282.Weight = 1.2437485236167287D;
            // 
            // txt30月步态
            // 
            this.txt30月步态.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月步态.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月步大});
            this.txt30月步态.Multiline = true;
            this.txt30月步态.Name = "txt30月步态";
            this.txt30月步态.StylePriority.UseBorders = false;
            this.txt30月步态.Weight = 0.25625092484159995D;
            // 
            // txt30月步大
            // 
            this.txt30月步大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月步大.BorderWidth = 1F;
            this.txt30月步大.CanGrow = false;
            this.txt30月步大.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月步大.LocationFloat = new DevExpress.Utils.PointFloat(0.6251221F, 3.447586F);
            this.txt30月步大.Name = "txt30月步大";
            this.txt30月步大.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月步大.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月步大.StylePriority.UseBorders = false;
            this.txt30月步大.StylePriority.UseBorderWidth = false;
            this.txt30月步大.StylePriority.UseFont = false;
            this.txt30月步大.StylePriority.UsePadding = false;
            this.txt30月步大.StylePriority.UseTextAlignment = false;
            this.txt30月步大.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell283,
            this.xrTableCell143,
            this.xrTableCell284,
            this.xrTableCell144,
            this.xrTableCell285,
            this.xrTableCell145,
            this.xrTableCell146});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 2.4000000000000012D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.28000001012166392D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Multiline = true;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseTextAlignment = false;
            this.xrTableCell142.Text = "可疑佝偻病体征";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell142.Weight = 1.2200000569534304D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Multiline = true;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Text = "1 “O”型腿\r\n2 “X”型腿";
            this.xrTableCell283.Weight = 1.2500000522931414D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月佝偻病});
            this.xrTableCell143.Multiline = true;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.StylePriority.UseTextAlignment = false;
            this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell143.Weight = 0.25000001160303731D;
            // 
            // txt12月佝偻病
            // 
            this.txt12月佝偻病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月佝偻病.BorderWidth = 1F;
            this.txt12月佝偻病.CanGrow = false;
            this.txt12月佝偻病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月佝偻病.LocationFloat = new DevExpress.Utils.PointFloat(1.590093F, 19.74376F);
            this.txt12月佝偻病.Name = "txt12月佝偻病";
            this.txt12月佝偻病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月佝偻病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月佝偻病.StylePriority.UseBorders = false;
            this.txt12月佝偻病.StylePriority.UseBorderWidth = false;
            this.txt12月佝偻病.StylePriority.UseFont = false;
            this.txt12月佝偻病.StylePriority.UsePadding = false;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Multiline = true;
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Text = "1 “O”型腿\r\n2 “X”型腿";
            this.xrTableCell284.Weight = 1.2500000486373897D;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell144.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月佝偻病});
            this.xrTableCell144.Multiline = true;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseTextAlignment = false;
            this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell144.Weight = 0.25000000794728583D;
            // 
            // txt18月佝偻病
            // 
            this.txt18月佝偻病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月佝偻病.BorderWidth = 1F;
            this.txt18月佝偻病.CanGrow = false;
            this.txt18月佝偻病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月佝偻病.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.74379F);
            this.txt18月佝偻病.Name = "txt18月佝偻病";
            this.txt18月佝偻病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月佝偻病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月佝偻病.StylePriority.UseBorders = false;
            this.txt18月佝偻病.StylePriority.UseBorderWidth = false;
            this.txt18月佝偻病.StylePriority.UseFont = false;
            this.txt18月佝偻病.StylePriority.UsePadding = false;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Multiline = true;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Text = "1 “O”型腿\r\n2 “X”型腿";
            this.xrTableCell285.Weight = 1.2779144795967885D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell145.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月佝偻病});
            this.xrTableCell145.Multiline = true;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UseTextAlignment = false;
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell145.Weight = 0.22208618956471493D;
            // 
            // txt24月佝偻病
            // 
            this.txt24月佝偻病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月佝偻病.BorderWidth = 1F;
            this.txt24月佝偻病.CanGrow = false;
            this.txt24月佝偻病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月佝偻病.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.74379F);
            this.txt24月佝偻病.Name = "txt24月佝偻病";
            this.txt24月佝偻病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月佝偻病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月佝偻病.StylePriority.UseBorders = false;
            this.txt24月佝偻病.StylePriority.UseBorderWidth = false;
            this.txt24月佝偻病.StylePriority.UseFont = false;
            this.txt24月佝偻病.StylePriority.UsePadding = false;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine7,
            this.xrLine8,
            this.xrLine9,
            this.xrLine10,
            this.xrLine11});
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 1.4999994484583286D;
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.BorderWidth = 2F;
            this.xrLine7.LineWidth = 2;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 23.71F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine7.StylePriority.UseBorders = false;
            this.xrLine7.StylePriority.UseBorderWidth = false;
            // 
            // xrLine8
            // 
            this.xrLine8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine8.BorderWidth = 2F;
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 23.71F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine8.StylePriority.UseBorders = false;
            this.xrLine8.StylePriority.UseBorderWidth = false;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.BorderWidth = 2F;
            this.xrLine9.LineWidth = 2;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(67.5F, 23.71F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine9.StylePriority.UseBorders = false;
            this.xrLine9.StylePriority.UseBorderWidth = false;
            // 
            // xrLine10
            // 
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.BorderWidth = 2F;
            this.xrLine10.LineWidth = 2;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(82.50003F, 23.71F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine10.StylePriority.UseBorders = false;
            this.xrLine10.StylePriority.UseBorderWidth = false;
            // 
            // xrLine11
            // 
            this.xrLine11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine11.BorderWidth = 2F;
            this.xrLine11.LineWidth = 2;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 23.71F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine11.StylePriority.UseBorders = false;
            this.xrLine11.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell156,
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell158});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.Weight = 0.8D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.28000001012166392D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "血红蛋白值 ";
            this.xrTableCell154.Weight = 1.2200000569534304D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine22,
            this.xrLine23,
            this.xrLine24,
            this.xrLine25,
            this.xrLine26});
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Weight = 1.5000000624656678D;
            // 
            // xrLine22
            // 
            this.xrLine22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine22.BorderWidth = 2F;
            this.xrLine22.LineWidth = 2;
            this.xrLine22.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine22.Name = "xrLine22";
            this.xrLine22.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine22.StylePriority.UseBorders = false;
            this.xrLine22.StylePriority.UseBorderWidth = false;
            // 
            // xrLine23
            // 
            this.xrLine23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine23.BorderWidth = 2F;
            this.xrLine23.LineWidth = 2;
            this.xrLine23.LocationFloat = new DevExpress.Utils.PointFloat(52.50002F, 3.71F);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine23.StylePriority.UseBorders = false;
            this.xrLine23.StylePriority.UseBorderWidth = false;
            // 
            // xrLine24
            // 
            this.xrLine24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine24.BorderWidth = 2F;
            this.xrLine24.LineWidth = 2;
            this.xrLine24.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine24.StylePriority.UseBorders = false;
            this.xrLine24.StylePriority.UseBorderWidth = false;
            // 
            // xrLine25
            // 
            this.xrLine25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine25.BorderWidth = 2F;
            this.xrLine25.LineWidth = 2;
            this.xrLine25.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine25.StylePriority.UseBorders = false;
            this.xrLine25.StylePriority.UseBorderWidth = false;
            // 
            // xrLine26
            // 
            this.xrLine26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine26.BorderWidth = 2F;
            this.xrLine26.LineWidth = 2;
            this.xrLine26.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine26.StylePriority.UseBorders = false;
            this.xrLine26.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月血红});
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.83090213262413892D;
            // 
            // txt18月血红
            // 
            this.txt18月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月血红.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt18月血红.Name = "txt18月血红";
            this.txt18月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月血红.SizeF = new System.Drawing.SizeF(81.18011F, 18F);
            this.txt18月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Text = "g/L";
            this.xrTableCell156.Weight = 0.66909823056684148D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine27,
            this.xrLine28,
            this.xrLine29,
            this.xrLine30,
            this.xrLine31});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 1.5000000599225358D;
            // 
            // xrLine27
            // 
            this.xrLine27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine27.BorderWidth = 2F;
            this.xrLine27.LineWidth = 2;
            this.xrLine27.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine27.Name = "xrLine27";
            this.xrLine27.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine27.StylePriority.UseBorders = false;
            this.xrLine27.StylePriority.UseBorderWidth = false;
            // 
            // xrLine28
            // 
            this.xrLine28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine28.BorderWidth = 2F;
            this.xrLine28.LineWidth = 2;
            this.xrLine28.LocationFloat = new DevExpress.Utils.PointFloat(52.50002F, 3.71F);
            this.xrLine28.Name = "xrLine28";
            this.xrLine28.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine28.StylePriority.UseBorders = false;
            this.xrLine28.StylePriority.UseBorderWidth = false;
            // 
            // xrLine29
            // 
            this.xrLine29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine29.BorderWidth = 2F;
            this.xrLine29.LineWidth = 2;
            this.xrLine29.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine29.Name = "xrLine29";
            this.xrLine29.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine29.StylePriority.UseBorders = false;
            this.xrLine29.StylePriority.UseBorderWidth = false;
            // 
            // xrLine30
            // 
            this.xrLine30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine30.BorderWidth = 2F;
            this.xrLine30.LineWidth = 2;
            this.xrLine30.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine30.Name = "xrLine30";
            this.xrLine30.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine30.StylePriority.UseBorders = false;
            this.xrLine30.StylePriority.UseBorderWidth = false;
            // 
            // xrLine31
            // 
            this.xrLine31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine31.BorderWidth = 2F;
            this.xrLine31.LineWidth = 2;
            this.xrLine31.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine31.Name = "xrLine31";
            this.xrLine31.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine31.StylePriority.UseBorders = false;
            this.xrLine31.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月血红});
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.9259003040044258D;
            // 
            // txt30月血红
            // 
            this.txt30月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月血红.LocationFloat = new DevExpress.Utils.PointFloat(5.978584F, 0F);
            this.txt30月血红.Name = "txt30月血红";
            this.txt30月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月血红.SizeF = new System.Drawing.SizeF(81.18011F, 18F);
            this.txt30月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "g/L";
            this.xrTableCell158.Weight = 0.5740994485170765D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 0.80000000000000027D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "户外活动";
            this.xrTableCell164.Weight = 1.5000000670750944D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月户外});
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Weight = 0.92590107059966165D;
            // 
            // txt12月户外
            // 
            this.txt12月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月户外.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt12月户外.Name = "txt12月户外";
            this.txt12月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月户外.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt12月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Text = "小时/日";
            this.xrTableCell166.Weight = 0.57000002462387067D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月户外});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Weight = 0.92589999882863194D;
            // 
            // txt18月户外
            // 
            this.txt18月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.409973F, 0F);
            this.txt18月户外.Name = "txt18月户外";
            this.txt18月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月户外.SizeF = new System.Drawing.SizeF(87.18005F, 18F);
            this.txt18月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Text = "小时/日";
            this.xrTableCell168.Weight = 0.57493440837593091D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月户外});
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Weight = 0.925899999941252D;
            // 
            // txt24月户外
            // 
            this.txt24月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.32666F, 0F);
            this.txt24月户外.Name = "txt24月户外";
            this.txt24月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月户外.SizeF = new System.Drawing.SizeF(85.625F, 18F);
            this.txt24月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "小时/日";
            this.xrTableCell170.Weight = 0.57736437174562993D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月户外});
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 0.92589999994125194D;
            // 
            // txt30月户外
            // 
            this.txt30月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月户外.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt30月户外.Name = "txt30月户外";
            this.txt30月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月户外.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt30月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Text = "小时/日";
            this.xrTableCell172.Weight = 0.57410036404445752D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell180});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 0.80000000000000027D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "服用维生素D";
            this.xrTableCell163.Weight = 1.5000000670750944D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月维生素});
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Weight = 0.92590107059966165D;
            // 
            // txt12月维生素
            // 
            this.txt12月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 0F);
            this.txt12月维生素.Name = "txt12月维生素";
            this.txt12月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月维生素.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt12月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Text = "IU/日";
            this.xrTableCell174.Weight = 0.57000002462387067D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月维生素});
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.92589999882863194D;
            // 
            // txt18月维生素
            // 
            this.txt18月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.409973F, 0F);
            this.txt18月维生素.Name = "txt18月维生素";
            this.txt18月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月维生素.SizeF = new System.Drawing.SizeF(87.17993F, 18F);
            this.txt18月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Text = "IU/日";
            this.xrTableCell176.Weight = 0.57493440837593091D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月维生素});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.925899999941252D;
            // 
            // txt24月维生素
            // 
            this.txt24月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.326599F, 0F);
            this.txt24月维生素.Name = "txt24月维生素";
            this.txt24月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月维生素.SizeF = new System.Drawing.SizeF(87.26343F, 18F);
            this.txt24月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "IU/日";
            this.xrTableCell178.Weight = 0.57736437174562993D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell180.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine32,
            this.xrLine33,
            this.xrLine34,
            this.xrLine35,
            this.xrLine36});
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Weight = 1.5000003639857096D;
            // 
            // xrLine32
            // 
            this.xrLine32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine32.BorderWidth = 2F;
            this.xrLine32.LineWidth = 2;
            this.xrLine32.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine32.Name = "xrLine32";
            this.xrLine32.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine32.StylePriority.UseBorders = false;
            this.xrLine32.StylePriority.UseBorderWidth = false;
            // 
            // xrLine33
            // 
            this.xrLine33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine33.BorderWidth = 2F;
            this.xrLine33.LineWidth = 2;
            this.xrLine33.LocationFloat = new DevExpress.Utils.PointFloat(52.50002F, 3.71F);
            this.xrLine33.Name = "xrLine33";
            this.xrLine33.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine33.StylePriority.UseBorders = false;
            this.xrLine33.StylePriority.UseBorderWidth = false;
            // 
            // xrLine34
            // 
            this.xrLine34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine34.BorderWidth = 2F;
            this.xrLine34.LineWidth = 2;
            this.xrLine34.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine34.Name = "xrLine34";
            this.xrLine34.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine34.StylePriority.UseBorders = false;
            this.xrLine34.StylePriority.UseBorderWidth = false;
            // 
            // xrLine35
            // 
            this.xrLine35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine35.BorderWidth = 2F;
            this.xrLine35.LineWidth = 2;
            this.xrLine35.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine35.Name = "xrLine35";
            this.xrLine35.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine35.StylePriority.UseBorders = false;
            this.xrLine35.StylePriority.UseBorderWidth = false;
            // 
            // xrLine36
            // 
            this.xrLine36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine36.BorderWidth = 2F;
            this.xrLine36.LineWidth = 2;
            this.xrLine36.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine36.Name = "xrLine36";
            this.xrLine36.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine36.StylePriority.UseBorders = false;
            this.xrLine36.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181,
            this.xrTableCell286,
            this.xrTableCell182,
            this.xrTableCell287,
            this.xrTableCell184,
            this.xrTableCell288,
            this.xrTableCell186,
            this.xrTableCell188});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseBorders = false;
            this.xrTableRow32.Weight = 0.80000000000000027D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "发育评估";
            this.xrTableCell181.Weight = 1.5000000670750944D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Text = "1 通过   2 未过";
            this.xrTableCell286.Weight = 1.250000204165783D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell182.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月发育});
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Weight = 0.24590088962723822D;
            // 
            // txt12月发育
            // 
            this.txt12月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月发育.BorderWidth = 1F;
            this.txt12月发育.CanGrow = false;
            this.txt12月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.021011F);
            this.txt12月发育.Name = "txt12月发育";
            this.txt12月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月发育.StylePriority.UseBorders = false;
            this.txt12月发育.StylePriority.UseBorderWidth = false;
            this.txt12月发育.StylePriority.UseFont = false;
            this.txt12月发育.StylePriority.UsePadding = false;
            this.txt12月发育.StylePriority.UseTextAlignment = false;
            this.txt12月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Text = "1 通过   2 未过";
            this.xrTableCell287.Weight = 1.2540997820773967D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell184.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月发育});
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.24673432360712422D;
            // 
            // txt18月发育
            // 
            this.txt18月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月发育.BorderWidth = 1F;
            this.txt18月发育.CanGrow = false;
            this.txt18月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.021011F);
            this.txt18月发育.Name = "txt18月发育";
            this.txt18月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月发育.StylePriority.UseBorders = false;
            this.txt18月发育.StylePriority.UseBorderWidth = false;
            this.txt18月发育.StylePriority.UseFont = false;
            this.txt18月发育.StylePriority.UsePadding = false;
            this.txt18月发育.StylePriority.UseTextAlignment = false;
            this.txt18月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Text = "1 通过   2 未过";
            this.xrTableCell288.Weight = 1.2811786393295477D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell186.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月发育});
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.22208679935999209D;
            // 
            // txt24月发育
            // 
            this.txt24月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月发育.BorderWidth = 1F;
            this.txt24月发育.CanGrow = false;
            this.txt24月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.021011F);
            this.txt24月发育.Name = "txt24月发育";
            this.txt24月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月发育.StylePriority.UseBorders = false;
            this.txt24月发育.StylePriority.UseBorderWidth = false;
            this.txt24月发育.StylePriority.UseFont = false;
            this.txt24月发育.StylePriority.UsePadding = false;
            this.txt24月发育.StylePriority.UseTextAlignment = false;
            this.txt24月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell188.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine42,
            this.xrLine43,
            this.xrLine44,
            this.xrLine45,
            this.xrLine46});
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Text = "1 通过   2 未过";
            this.xrTableCell188.Weight = 1.4999995999336051D;
            // 
            // xrLine42
            // 
            this.xrLine42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine42.BorderWidth = 2F;
            this.xrLine42.LineWidth = 2;
            this.xrLine42.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine42.Name = "xrLine42";
            this.xrLine42.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine42.StylePriority.UseBorders = false;
            this.xrLine42.StylePriority.UseBorderWidth = false;
            // 
            // xrLine43
            // 
            this.xrLine43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine43.BorderWidth = 2F;
            this.xrLine43.LineWidth = 2;
            this.xrLine43.LocationFloat = new DevExpress.Utils.PointFloat(52.50002F, 3.71F);
            this.xrLine43.Name = "xrLine43";
            this.xrLine43.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine43.StylePriority.UseBorders = false;
            this.xrLine43.StylePriority.UseBorderWidth = false;
            // 
            // xrLine44
            // 
            this.xrLine44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine44.BorderWidth = 2F;
            this.xrLine44.LineWidth = 2;
            this.xrLine44.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine44.Name = "xrLine44";
            this.xrLine44.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine44.StylePriority.UseBorders = false;
            this.xrLine44.StylePriority.UseBorderWidth = false;
            // 
            // xrLine45
            // 
            this.xrLine45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine45.BorderWidth = 2F;
            this.xrLine45.LineWidth = 2;
            this.xrLine45.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine45.Name = "xrLine45";
            this.xrLine45.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine45.StylePriority.UseBorders = false;
            this.xrLine45.StylePriority.UseBorderWidth = false;
            // 
            // xrLine46
            // 
            this.xrLine46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine46.BorderWidth = 2F;
            this.xrLine46.LineWidth = 2;
            this.xrLine46.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine46.Name = "xrLine46";
            this.xrLine46.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine46.StylePriority.UseBorders = false;
            this.xrLine46.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell289,
            this.xrTableCell185,
            this.xrTableCell290,
            this.xrTableCell187,
            this.xrTableCell291,
            this.xrTableCell189,
            this.xrTableCell292,
            this.xrTableCell190});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 0.80000000000000027D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "两次随访间患病情况";
            this.xrTableCell183.Weight = 1.5000000670750944D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Text = "1 未患病 2 患病";
            this.xrTableCell289.Weight = 1.250000204165783D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell185.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月两次随访});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 0.24590088962723811D;
            // 
            // txt12月两次随访
            // 
            this.txt12月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月两次随访.BorderWidth = 1F;
            this.txt12月两次随访.CanGrow = false;
            this.txt12月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt12月两次随访.Name = "txt12月两次随访";
            this.txt12月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月两次随访.StylePriority.UseBorders = false;
            this.txt12月两次随访.StylePriority.UseBorderWidth = false;
            this.txt12月两次随访.StylePriority.UseFont = false;
            this.txt12月两次随访.StylePriority.UsePadding = false;
            this.txt12月两次随访.StylePriority.UseTextAlignment = false;
            this.txt12月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Text = "1 未患病 2 患病";
            this.xrTableCell290.Weight = 1.2540994769016032D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月两次随访});
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Weight = 0.24673462878291785D;
            // 
            // txt18月两次随访
            // 
            this.txt18月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月两次随访.BorderWidth = 1F;
            this.txt18月两次随访.CanGrow = false;
            this.txt18月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt18月两次随访.Name = "txt18月两次随访";
            this.txt18月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月两次随访.StylePriority.UseBorders = false;
            this.txt18月两次随访.StylePriority.UseBorderWidth = false;
            this.txt18月两次随访.StylePriority.UseFont = false;
            this.txt18月两次随访.StylePriority.UsePadding = false;
            this.txt18月两次随访.StylePriority.UseTextAlignment = false;
            this.txt18月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Text = "1 未患病 2 患病";
            this.xrTableCell291.Weight = 1.2811786393295477D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell189.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月两次随访});
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell189.Weight = 0.22208679935999198D;
            // 
            // txt24月两次随访
            // 
            this.txt24月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月两次随访.BorderWidth = 1F;
            this.txt24月两次随访.CanGrow = false;
            this.txt24月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt24月两次随访.Name = "txt24月两次随访";
            this.txt24月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月两次随访.StylePriority.UseBorders = false;
            this.txt24月两次随访.StylePriority.UseBorderWidth = false;
            this.txt24月两次随访.StylePriority.UseFont = false;
            this.txt24月两次随访.StylePriority.UsePadding = false;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Text = "1 未患病 2 患病";
            this.xrTableCell292.Weight = 1.2437479890027796D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月两次随访});
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Weight = 0.2562516109308256D;
            // 
            // txt30月两次随访
            // 
            this.txt30月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月两次随访.BorderWidth = 1F;
            this.txt30月两次随访.CanGrow = false;
            this.txt30月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt30月两次随访.Name = "txt30月两次随访";
            this.txt30月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月两次随访.StylePriority.UseBorders = false;
            this.txt30月两次随访.StylePriority.UseBorderWidth = false;
            this.txt30月两次随访.StylePriority.UseFont = false;
            this.txt30月两次随访.StylePriority.UsePadding = false;
            this.txt30月两次随访.StylePriority.UseTextAlignment = false;
            this.txt30月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell195});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.StylePriority.UseBorders = false;
            this.xrTableRow34.Weight = 0.80000000000000027D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "其他";
            this.xrTableCell191.Weight = 1.5000000670750944D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月其他});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 1.4959010937930211D;
            // 
            // txt12月其他
            // 
            this.txt12月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt12月其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt12月其他.Name = "txt12月其他";
            this.txt12月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月其他.SizeF = new System.Drawing.SizeF(148.5901F, 20F);
            this.txt12月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月其他});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 1.5008341056845209D;
            // 
            // txt18月其他
            // 
            this.txt18月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt18月其他.LocationFloat = new DevExpress.Utils.PointFloat(0.4099121F, 0F);
            this.txt18月其他.Name = "txt18月其他";
            this.txt18月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月其他.SizeF = new System.Drawing.SizeF(149.6735F, 20F);
            this.txt18月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月其他});
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Weight = 1.5032654386895397D;
            // 
            // txt24月其他
            // 
            this.txt24月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt24月其他.LocationFloat = new DevExpress.Utils.PointFloat(0.3265076F, 0F);
            this.txt24月其他.Name = "txt24月其他";
            this.txt24月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月其他.SizeF = new System.Drawing.SizeF(149.9999F, 20F);
            this.txt24月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell195.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月其他});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.Weight = 1.4999995999336051D;
            // 
            // txt30月其他
            // 
            this.txt30月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt30月其他.LocationFloat = new DevExpress.Utils.PointFloat(4.999817F, 6.103516E-05F);
            this.txt30月其他.Name = "txt30月其他";
            this.txt30月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月其他.SizeF = new System.Drawing.SizeF(140.4883F, 18F);
            this.txt30月其他.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.Weight = 3.2000000000000015D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "转诊建议";
            this.xrTableCell196.Weight = 1.5000000670750944D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 1.4959010937930211D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0.4100189F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow37});
            this.xrTable2.SizeF = new System.Drawing.SizeF(148.1801F, 80F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell203});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "1有 2 无";
            this.xrTableCell29.Weight = 2.32764391706795D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月转诊建议});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.67235608293204974D;
            // 
            // txt12月转诊建议
            // 
            this.txt12月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月转诊建议.BorderWidth = 1F;
            this.txt12月转诊建议.CanGrow = false;
            this.txt12月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(9.619812F, 3.909653F);
            this.txt12月转诊建议.Name = "txt12月转诊建议";
            this.txt12月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月转诊建议.StylePriority.UseBorders = false;
            this.txt12月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt12月转诊建议.StylePriority.UseFont = false;
            this.txt12月转诊建议.StylePriority.UsePadding = false;
            this.txt12月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt12月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Text = "原因：";
            this.xrTableCell205.Weight = 1.0122814590239646D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月转诊原因});
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Weight = 1.9877185409760354D;
            // 
            // txt12月转诊原因
            // 
            this.txt12月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504639F, 1.999939F);
            this.txt12月转诊原因.Name = "txt12月转诊原因";
            this.txt12月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月转诊原因.SizeF = new System.Drawing.SizeF(95.67531F, 18.00006F);
            this.txt12月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell207,
            this.xrTableCell208});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "机构及科室：";
            this.xrTableCell207.Weight = 1.903089105596592D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel52});
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Weight = 1.0969108944034081D;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel52.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月转诊机构});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 3D;
            // 
            // txt12月转诊机构
            // 
            this.txt12月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt12月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.txt12月转诊机构.Name = "txt12月转诊机构";
            this.txt12月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt12月转诊机构.SizeF = new System.Drawing.SizeF(148.18F, 15F);
            this.txt12月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 1.5008341056845209D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(1.493327F, 3.178914E-05F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43});
            this.xrTable7.SizeF = new System.Drawing.SizeF(147.8536F, 80F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell201});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "1有 2 无";
            this.xrTableCell35.Weight = 2.2820242537101638D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月转诊建议});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.71797574628983629D;
            // 
            // txt18月转诊建议
            // 
            this.txt18月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月转诊建议.BorderWidth = 1F;
            this.txt18月转诊建议.CanGrow = false;
            this.txt18月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(11.44809F, 3.909592F);
            this.txt18月转诊建议.Name = "txt18月转诊建议";
            this.txt18月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月转诊建议.StylePriority.UseBorders = false;
            this.txt18月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt18月转诊建议.StylePriority.UseFont = false;
            this.txt18月转诊建议.StylePriority.UsePadding = false;
            this.txt18月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt18月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell209});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Text = "原因：";
            this.xrTableCell202.Weight = 1.0122814590239646D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月转诊原因});
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 1.9877185409760354D;
            // 
            // txt18月转诊原因
            // 
            this.txt18月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt18月转诊原因.Name = "txt18月转诊原因";
            this.txt18月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt18月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.xrTableCell211});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "机构及科室：";
            this.xrTableCell210.Weight = 1.903089105596592D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel54});
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 1.0969108944034081D;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel54.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月转诊机构});
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 3D;
            // 
            // txt18月转诊机构
            // 
            this.txt18月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt18月转诊机构.Name = "txt18月转诊机构";
            this.txt18月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt18月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 1.5032648283379522D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable8.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(2.826538F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(147.4999F, 80F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTableCell213});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Text = "1有 2 无";
            this.xrTableCell128.Weight = 2.2838994417141247D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月转诊建议});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 0.716100558285875D;
            // 
            // txt24月转诊建议
            // 
            this.txt24月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月转诊建议.BorderWidth = 1F;
            this.txt24月转诊建议.CanGrow = false;
            this.txt24月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(13.33345F, 3.909652F);
            this.txt24月转诊建议.Name = "txt24月转诊建议";
            this.txt24月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月转诊建议.StylePriority.UseBorders = false;
            this.txt24月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt24月转诊建议.StylePriority.UseFont = false;
            this.txt24月转诊建议.StylePriority.UsePadding = false;
            this.txt24月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt24月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "原因：";
            this.xrTableCell214.Weight = 1.0122814590239646D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月转诊原因});
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 1.9877185409760354D;
            // 
            // txt24月转诊原因
            // 
            this.txt24月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt24月转诊原因.Name = "txt24月转诊原因";
            this.txt24月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt24月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell217});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Text = "机构及科室：";
            this.xrTableCell216.Weight = 1.903089105596592D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel57});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 1.0969108944034081D;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel57.StylePriority.UseBorders = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月转诊机构});
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 3D;
            // 
            // txt24月转诊机构
            // 
            this.txt24月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt24月转诊机构.Name = "txt24月转诊机构";
            this.txt24月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt24月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 1.5000002102851926D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable9.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(4.999817F, 0.0001220703F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51});
            this.xrTable9.SizeF = new System.Drawing.SizeF(143.4101F, 76.99982F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell219});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "1有 2 无";
            this.xrTableCell137.Weight = 2.2844629646101877D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月转诊建议});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.71553703538981228D;
            // 
            // txt30月转诊建议
            // 
            this.txt30月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月转诊建议.BorderWidth = 1F;
            this.txt30月转诊建议.CanGrow = false;
            this.txt30月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(10.79517F, 3.909531F);
            this.txt30月转诊建议.Name = "txt30月转诊建议";
            this.txt30月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月转诊建议.StylePriority.UseBorders = false;
            this.txt30月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt30月转诊建议.StylePriority.UseFont = false;
            this.txt30月转诊建议.StylePriority.UsePadding = false;
            this.txt30月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt30月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell220,
            this.xrTableCell221});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Text = "原因：";
            this.xrTableCell220.Weight = 1.0911104591946297D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月转诊原因});
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 1.9088895408053703D;
            // 
            // txt30月转诊原因
            // 
            this.txt30月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt30月转诊原因.Name = "txt30月转诊原因";
            this.txt30月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月转诊原因.SizeF = new System.Drawing.SizeF(86.84119F, 18.00006F);
            this.txt30月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "机构及科室：";
            this.xrTableCell222.Weight = 1.9663888322345193D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel60});
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 1.0336111677654807D;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.249802F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(45F, 18.00012F);
            this.xrLabel60.StylePriority.UseBorders = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月转诊机构});
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Weight = 3D;
            // 
            // txt30月转诊机构
            // 
            this.txt30月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt30月转诊机构.Name = "txt30月转诊机构";
            this.txt30月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月转诊机构.SizeF = new System.Drawing.SizeF(139F, 15F);
            this.txt30月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell293,
            this.xrTableCell226,
            this.xrTableCell294,
            this.xrTableCell227,
            this.xrTableCell295,
            this.xrTableCell228,
            this.xrTableCell296,
            this.xrTableCell229});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.StylePriority.UseBorders = false;
            this.xrTableRow52.Weight = 4.4000000000000021D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Text = "指导";
            this.xrTableCell225.Weight = 1.5000000670750944D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Multiline = true;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell293.Weight = 1.2500005093415765D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell226.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt12月科学,
            this.txt12月生长,
            this.txt12月低盐,
            this.txt12月口腔保健,
            this.txt12月疾病,
            this.txt12月预防});
            this.xrTableCell226.Multiline = true;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.24590058445144447D;
            // 
            // txt12月科学
            // 
            this.txt12月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月科学.BorderWidth = 1F;
            this.txt12月科学.CanGrow = false;
            this.txt12月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月科学.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 7F);
            this.txt12月科学.Name = "txt12月科学";
            this.txt12月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月科学.StylePriority.UseBorders = false;
            this.txt12月科学.StylePriority.UseBorderWidth = false;
            this.txt12月科学.StylePriority.UseFont = false;
            this.txt12月科学.StylePriority.UsePadding = false;
            this.txt12月科学.StylePriority.UseTextAlignment = false;
            this.txt12月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt12月生长
            // 
            this.txt12月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月生长.BorderWidth = 1F;
            this.txt12月生长.CanGrow = false;
            this.txt12月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月生长.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 25F);
            this.txt12月生长.Name = "txt12月生长";
            this.txt12月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月生长.StylePriority.UseBorders = false;
            this.txt12月生长.StylePriority.UseBorderWidth = false;
            this.txt12月生长.StylePriority.UseFont = false;
            this.txt12月生长.StylePriority.UsePadding = false;
            this.txt12月生长.StylePriority.UseTextAlignment = false;
            this.txt12月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt12月低盐
            // 
            this.txt12月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月低盐.BorderWidth = 1F;
            this.txt12月低盐.CanGrow = false;
            this.txt12月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月低盐.LocationFloat = new DevExpress.Utils.PointFloat(1.590061F, 94.03754F);
            this.txt12月低盐.Name = "txt12月低盐";
            this.txt12月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月低盐.StylePriority.UseBorders = false;
            this.txt12月低盐.StylePriority.UseBorderWidth = false;
            this.txt12月低盐.StylePriority.UseFont = false;
            this.txt12月低盐.StylePriority.UsePadding = false;
            this.txt12月低盐.StylePriority.UseTextAlignment = false;
            this.txt12月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt12月口腔保健
            // 
            this.txt12月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月口腔保健.BorderWidth = 1F;
            this.txt12月口腔保健.CanGrow = false;
            this.txt12月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 75F);
            this.txt12月口腔保健.Name = "txt12月口腔保健";
            this.txt12月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月口腔保健.StylePriority.UseBorders = false;
            this.txt12月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt12月口腔保健.StylePriority.UseFont = false;
            this.txt12月口腔保健.StylePriority.UsePadding = false;
            this.txt12月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt12月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt12月疾病
            // 
            this.txt12月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月疾病.BorderWidth = 1F;
            this.txt12月疾病.CanGrow = false;
            this.txt12月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月疾病.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 40F);
            this.txt12月疾病.Name = "txt12月疾病";
            this.txt12月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月疾病.StylePriority.UseBorders = false;
            this.txt12月疾病.StylePriority.UseBorderWidth = false;
            this.txt12月疾病.StylePriority.UseFont = false;
            this.txt12月疾病.StylePriority.UsePadding = false;
            this.txt12月疾病.StylePriority.UseTextAlignment = false;
            this.txt12月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt12月预防
            // 
            this.txt12月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt12月预防.BorderWidth = 1F;
            this.txt12月预防.CanGrow = false;
            this.txt12月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12月预防.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 60F);
            this.txt12月预防.Name = "txt12月预防";
            this.txt12月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt12月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt12月预防.StylePriority.UseBorders = false;
            this.txt12月预防.StylePriority.UseBorderWidth = false;
            this.txt12月预防.StylePriority.UseFont = false;
            this.txt12月预防.StylePriority.UsePadding = false;
            this.txt12月预防.StylePriority.UseTextAlignment = false;
            this.txt12月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Multiline = true;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell294.Weight = 1.2540988665500157D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell227.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt18月科学,
            this.txt18月生长,
            this.txt18月低盐,
            this.txt18月口腔保健,
            this.txt18月疾病,
            this.txt18月预防});
            this.xrTableCell227.Multiline = true;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.24673523913450524D;
            // 
            // txt18月科学
            // 
            this.txt18月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月科学.BorderWidth = 1F;
            this.txt18月科学.CanGrow = false;
            this.txt18月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月科学.LocationFloat = new DevExpress.Utils.PointFloat(0F, 6.999969F);
            this.txt18月科学.Name = "txt18月科学";
            this.txt18月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月科学.StylePriority.UseBorders = false;
            this.txt18月科学.StylePriority.UseBorderWidth = false;
            this.txt18月科学.StylePriority.UseFont = false;
            this.txt18月科学.StylePriority.UsePadding = false;
            this.txt18月科学.StylePriority.UseTextAlignment = false;
            this.txt18月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt18月生长
            // 
            this.txt18月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月生长.BorderWidth = 1F;
            this.txt18月生长.CanGrow = false;
            this.txt18月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月生长.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 25F);
            this.txt18月生长.Name = "txt18月生长";
            this.txt18月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月生长.StylePriority.UseBorders = false;
            this.txt18月生长.StylePriority.UseBorderWidth = false;
            this.txt18月生长.StylePriority.UseFont = false;
            this.txt18月生长.StylePriority.UsePadding = false;
            this.txt18月生长.StylePriority.UseTextAlignment = false;
            this.txt18月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt18月低盐
            // 
            this.txt18月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月低盐.BorderWidth = 1F;
            this.txt18月低盐.CanGrow = false;
            this.txt18月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月低盐.LocationFloat = new DevExpress.Utils.PointFloat(0F, 94.0375F);
            this.txt18月低盐.Name = "txt18月低盐";
            this.txt18月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月低盐.StylePriority.UseBorders = false;
            this.txt18月低盐.StylePriority.UseBorderWidth = false;
            this.txt18月低盐.StylePriority.UseFont = false;
            this.txt18月低盐.StylePriority.UsePadding = false;
            this.txt18月低盐.StylePriority.UseTextAlignment = false;
            this.txt18月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt18月口腔保健
            // 
            this.txt18月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月口腔保健.BorderWidth = 1F;
            this.txt18月口腔保健.CanGrow = false;
            this.txt18月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(0F, 75F);
            this.txt18月口腔保健.Name = "txt18月口腔保健";
            this.txt18月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月口腔保健.StylePriority.UseBorders = false;
            this.txt18月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt18月口腔保健.StylePriority.UseFont = false;
            this.txt18月口腔保健.StylePriority.UsePadding = false;
            this.txt18月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt18月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt18月疾病
            // 
            this.txt18月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月疾病.BorderWidth = 1F;
            this.txt18月疾病.CanGrow = false;
            this.txt18月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月疾病.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 39.99999F);
            this.txt18月疾病.Name = "txt18月疾病";
            this.txt18月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月疾病.StylePriority.UseBorders = false;
            this.txt18月疾病.StylePriority.UseBorderWidth = false;
            this.txt18月疾病.StylePriority.UseFont = false;
            this.txt18月疾病.StylePriority.UsePadding = false;
            this.txt18月疾病.StylePriority.UseTextAlignment = false;
            this.txt18月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt18月预防
            // 
            this.txt18月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt18月预防.BorderWidth = 1F;
            this.txt18月预防.CanGrow = false;
            this.txt18月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18月预防.LocationFloat = new DevExpress.Utils.PointFloat(0F, 59.99997F);
            this.txt18月预防.Name = "txt18月预防";
            this.txt18月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt18月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt18月预防.StylePriority.UseBorders = false;
            this.txt18月预防.StylePriority.UseBorderWidth = false;
            this.txt18月预防.StylePriority.UseFont = false;
            this.txt18月预防.StylePriority.UsePadding = false;
            this.txt18月预防.StylePriority.UseTextAlignment = false;
            this.txt18月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Multiline = true;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell295.Weight = 1.1511816857977706D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell228.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt24月合理,
            this.txt24月低盐,
            this.txt24月生长,
            this.txt24月口腔保健,
            this.txt24月疾病,
            this.txt24月预防});
            this.xrTableCell228.Multiline = true;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Weight = 0.35208314254018153D;
            // 
            // txt24月合理
            // 
            this.txt24月合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月合理.BorderWidth = 1F;
            this.txt24月合理.CanGrow = false;
            this.txt24月合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月合理.LocationFloat = new DevExpress.Utils.PointFloat(9.583156F, 6.999938F);
            this.txt24月合理.Name = "txt24月合理";
            this.txt24月合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月合理.StylePriority.UseBorders = false;
            this.txt24月合理.StylePriority.UseBorderWidth = false;
            this.txt24月合理.StylePriority.UseFont = false;
            this.txt24月合理.StylePriority.UsePadding = false;
            this.txt24月合理.StylePriority.UseTextAlignment = false;
            this.txt24月合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt24月低盐
            // 
            this.txt24月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月低盐.BorderWidth = 1F;
            this.txt24月低盐.CanGrow = false;
            this.txt24月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月低盐.LocationFloat = new DevExpress.Utils.PointFloat(9.583092F, 94.03754F);
            this.txt24月低盐.Name = "txt24月低盐";
            this.txt24月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月低盐.StylePriority.UseBorders = false;
            this.txt24月低盐.StylePriority.UseBorderWidth = false;
            this.txt24月低盐.StylePriority.UseFont = false;
            this.txt24月低盐.StylePriority.UsePadding = false;
            this.txt24月低盐.StylePriority.UseTextAlignment = false;
            this.txt24月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt24月生长
            // 
            this.txt24月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月生长.BorderWidth = 1F;
            this.txt24月生长.CanGrow = false;
            this.txt24月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月生长.LocationFloat = new DevExpress.Utils.PointFloat(10.20838F, 25F);
            this.txt24月生长.Name = "txt24月生长";
            this.txt24月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月生长.StylePriority.UseBorders = false;
            this.txt24月生长.StylePriority.UseBorderWidth = false;
            this.txt24月生长.StylePriority.UseFont = false;
            this.txt24月生长.StylePriority.UsePadding = false;
            this.txt24月生长.StylePriority.UseTextAlignment = false;
            this.txt24月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt24月口腔保健
            // 
            this.txt24月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月口腔保健.BorderWidth = 1F;
            this.txt24月口腔保健.CanGrow = false;
            this.txt24月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(10.20838F, 75F);
            this.txt24月口腔保健.Name = "txt24月口腔保健";
            this.txt24月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月口腔保健.StylePriority.UseBorders = false;
            this.txt24月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt24月口腔保健.StylePriority.UseFont = false;
            this.txt24月口腔保健.StylePriority.UsePadding = false;
            this.txt24月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt24月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt24月疾病
            // 
            this.txt24月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月疾病.BorderWidth = 1F;
            this.txt24月疾病.CanGrow = false;
            this.txt24月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月疾病.LocationFloat = new DevExpress.Utils.PointFloat(9.583092F, 40.00003F);
            this.txt24月疾病.Name = "txt24月疾病";
            this.txt24月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月疾病.StylePriority.UseBorders = false;
            this.txt24月疾病.StylePriority.UseBorderWidth = false;
            this.txt24月疾病.StylePriority.UseFont = false;
            this.txt24月疾病.StylePriority.UsePadding = false;
            this.txt24月疾病.StylePriority.UseTextAlignment = false;
            this.txt24月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt24月预防
            // 
            this.txt24月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt24月预防.BorderWidth = 1F;
            this.txt24月预防.CanGrow = false;
            this.txt24月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt24月预防.LocationFloat = new DevExpress.Utils.PointFloat(9.583092F, 56.5945F);
            this.txt24月预防.Name = "txt24月预防";
            this.txt24月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt24月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt24月预防.StylePriority.UseBorders = false;
            this.txt24月预防.StylePriority.UseBorderWidth = false;
            this.txt24月预防.StylePriority.UseFont = false;
            this.txt24月预防.StylePriority.UsePadding = false;
            this.txt24月预防.StylePriority.UseTextAlignment = false;
            this.txt24月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Multiline = true;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell296.Weight = 1.1420478505872467D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell229.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt30月合理,
            this.txt30月低盐,
            this.txt30月生长,
            this.txt30月口腔保健,
            this.txt30月疾病,
            this.txt30月预防});
            this.xrTableCell229.Multiline = true;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.35795235969794587D;
            // 
            // txt30月合理
            // 
            this.txt30月合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月合理.BorderWidth = 1F;
            this.txt30月合理.CanGrow = false;
            this.txt30月合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月合理.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 6.999969F);
            this.txt30月合理.Name = "txt30月合理";
            this.txt30月合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月合理.StylePriority.UseBorders = false;
            this.txt30月合理.StylePriority.UseBorderWidth = false;
            this.txt30月合理.StylePriority.UseFont = false;
            this.txt30月合理.StylePriority.UsePadding = false;
            this.txt30月合理.StylePriority.UseTextAlignment = false;
            this.txt30月合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt30月低盐
            // 
            this.txt30月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月低盐.BorderWidth = 1F;
            this.txt30月低盐.CanGrow = false;
            this.txt30月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月低盐.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 94.03747F);
            this.txt30月低盐.Name = "txt30月低盐";
            this.txt30月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月低盐.StylePriority.UseBorders = false;
            this.txt30月低盐.StylePriority.UseBorderWidth = false;
            this.txt30月低盐.StylePriority.UseFont = false;
            this.txt30月低盐.StylePriority.UsePadding = false;
            this.txt30月低盐.StylePriority.UseTextAlignment = false;
            this.txt30月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt30月生长
            // 
            this.txt30月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月生长.BorderWidth = 1F;
            this.txt30月生长.CanGrow = false;
            this.txt30月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月生长.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 25F);
            this.txt30月生长.Name = "txt30月生长";
            this.txt30月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月生长.StylePriority.UseBorders = false;
            this.txt30月生长.StylePriority.UseBorderWidth = false;
            this.txt30月生长.StylePriority.UseFont = false;
            this.txt30月生长.StylePriority.UsePadding = false;
            this.txt30月生长.StylePriority.UseTextAlignment = false;
            this.txt30月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt30月口腔保健
            // 
            this.txt30月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月口腔保健.BorderWidth = 1F;
            this.txt30月口腔保健.CanGrow = false;
            this.txt30月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(10.79521F, 74.99997F);
            this.txt30月口腔保健.Name = "txt30月口腔保健";
            this.txt30月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月口腔保健.StylePriority.UseBorders = false;
            this.txt30月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt30月口腔保健.StylePriority.UseFont = false;
            this.txt30月口腔保健.StylePriority.UsePadding = false;
            this.txt30月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt30月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt30月疾病
            // 
            this.txt30月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月疾病.BorderWidth = 1F;
            this.txt30月疾病.CanGrow = false;
            this.txt30月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月疾病.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 44.01115F);
            this.txt30月疾病.Name = "txt30月疾病";
            this.txt30月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月疾病.StylePriority.UseBorders = false;
            this.txt30月疾病.StylePriority.UseBorderWidth = false;
            this.txt30月疾病.StylePriority.UseFont = false;
            this.txt30月疾病.StylePriority.UsePadding = false;
            this.txt30月疾病.StylePriority.UseTextAlignment = false;
            this.txt30月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt30月预防
            // 
            this.txt30月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月预防.BorderWidth = 1F;
            this.txt30月预防.CanGrow = false;
            this.txt30月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt30月预防.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 59.99997F);
            this.txt30月预防.Name = "txt30月预防";
            this.txt30月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt30月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt30月预防.StylePriority.UseBorders = false;
            this.txt30月预防.StylePriority.UseBorderWidth = false;
            this.txt30月预防.StylePriority.UseFont = false;
            this.txt30月预防.StylePriority.UsePadding = false;
            this.txt30月预防.StylePriority.UseTextAlignment = false;
            this.txt30月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233,
            this.xrTableCell234});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.StylePriority.UseBorders = false;
            this.xrTableRow53.Weight = 4.4841925454132392D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Text = "中医药健康服务";
            this.xrTableCell230.Weight = 1.5000000670750944D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11,
            this.xrLabel36});
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 1.4959010937930211D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable11.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.6249237F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow28,
            this.xrTableRow60,
            this.xrTableRow61});
            this.xrTable11.SizeF = new System.Drawing.SizeF(146.42F, 99.37F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1.9361006289308176D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授摩腹、捏脊方法";
            this.xrTableCell123.Weight = 3.0054574798513976D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124,
            this.xrTableCell125});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.063899371069182365D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Weight = 1D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 2.0054574798513976D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 0.72201255942290687D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "4其他";
            this.xrTableCell126.Weight = 1D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel27});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 2.0054574798513976D;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 4.577637E-05F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(97.11517F, 18F);
            this.xrLabel27.StylePriority.UseBorders = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt12月服务其他});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 0.73031444813470414D;
            // 
            // txt12月服务其他
            // 
            this.txt12月服务其他.Name = "txt12月服务其他";
            this.txt12月服务其他.Weight = 3.0054574798513976D;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(0F, 99.99492F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(148.59F, 15F);
            this.xrLabel36.StylePriority.UseBorders = false;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12,
            this.txt18月服务其他});
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 1.5008341056845209D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable12.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.2082977F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65});
            this.xrTable12.SizeF = new System.Drawing.SizeF(147.3F, 99.79F);
            this.xrTable12.StylePriority.UseBorders = false;
            this.xrTable12.StylePriority.UseFont = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1.9361006289308176D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Multiline = true;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授按揉、迎香穴、";
            this.xrTableCell147.Weight = 3.0054574798513976D;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell149});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 0.063899371069182365D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 1D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 2.0054574798513976D;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 0.72201255942290687D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Text = "   足三里穴方法";
            this.xrTableCell150.Weight = 3.0054574798513976D;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell152});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 0.73031444813470414D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Text = "4其他";
            this.xrTableCell254.Weight = 1.000002001521173D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel77});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 2.0054554783302248D;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(1.398987F, 0F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(96.88565F, 18.00006F);
            this.xrLabel77.StylePriority.UseBorders = false;
            // 
            // txt18月服务其他
            // 
            this.txt18月服务其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt18月服务其他.LocationFloat = new DevExpress.Utils.PointFloat(1.493286F, 99.99828F);
            this.txt18月服务其他.Name = "txt18月服务其他";
            this.txt18月服务其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt18月服务其他.SizeF = new System.Drawing.SizeF(145.8F, 15F);
            this.txt18月服务其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.txt24月服务其他});
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Weight = 1.5032648283379522D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable10.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54,
            this.xrTableRow56,
            this.xrTableRow55,
            this.xrTableRow57});
            this.xrTable10.SizeF = new System.Drawing.SizeF(150.3265F, 100F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell237});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1.9361006289308176D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Multiline = true;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授按揉、迎香穴、";
            this.xrTableCell237.Weight = 3.0054574798513976D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 0.063899371069182365D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Weight = 1D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 2.0054574798513976D;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.72201255942290687D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Text = "   足三里穴方法";
            this.xrTableCell236.Weight = 3.0054574798513976D;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell241});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 0.73031444813470414D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Text = "4其他";
            this.xrTableCell151.Weight = 1.0000007578457704D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel45});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Weight = 2.005456722005627D;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0.3085327F, 0F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(99.99988F, 18.00006F);
            this.xrLabel45.StylePriority.UseBorders = false;
            // 
            // txt24月服务其他
            // 
            this.txt24月服务其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt24月服务其他.LocationFloat = new DevExpress.Utils.PointFloat(0.9966125F, 101F);
            this.txt24月服务其他.Name = "txt24月服务其他";
            this.txt24月服务其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt24月服务其他.SizeF = new System.Drawing.SizeF(149.33F, 15F);
            this.txt24月服务其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13,
            this.txt30月服务其他});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 1.5000002102851926D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable13.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(1.409973F, 0.6299896F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69});
            this.xrTable13.SizeF = new System.Drawing.SizeF(146.9999F, 99.36993F);
            this.xrTable13.StylePriority.UseBorders = false;
            this.xrTable13.StylePriority.UseFont = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1.9361006289308176D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Multiline = true;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授按揉、四神聪穴";
            this.xrTableCell155.Weight = 3.0054574798513976D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.xrTableCell179});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 0.063899371069182365D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 1D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 2.0054574798513976D;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 0.72201255942290687D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Text = "   方法法";
            this.xrTableCell251.Weight = 3.0054574798513976D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell253});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 0.73031444813470414D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Text = "4其他";
            this.xrTableCell255.Weight = 0.99999933138682628D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel78});
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Weight = 2.0054581484645713D;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(98.08887F, 18.00006F);
            this.xrLabel78.StylePriority.UseBorders = false;
            // 
            // txt30月服务其他
            // 
            this.txt30月服务其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt30月服务其他.LocationFloat = new DevExpress.Utils.PointFloat(1.409912F, 100.9999F);
            this.txt30月服务其他.Name = "txt30月服务其他";
            this.txt30月服务其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt30月服务其他.SizeF = new System.Drawing.SizeF(146.9999F, 15F);
            this.txt30月服务其他.StylePriority.UseBorders = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.txt12月下次随访,
            this.txt18月下次随访,
            this.txt24月下次随访,
            this.txt30月下次随访});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.StylePriority.UseBorders = false;
            this.xrTableRow58.Weight = 0.90532573124093019D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Text = "下次随访日期";
            this.xrTableCell235.Weight = 1.5000000670750944D;
            // 
            // txt12月下次随访
            // 
            this.txt12月下次随访.Name = "txt12月下次随访";
            this.txt12月下次随访.Weight = 1.4959010937930211D;
            // 
            // txt18月下次随访
            // 
            this.txt18月下次随访.Name = "txt18月下次随访";
            this.txt18月下次随访.Weight = 1.5008341056845209D;
            // 
            // txt24月下次随访
            // 
            this.txt24月下次随访.Name = "txt24月下次随访";
            this.txt24月下次随访.Weight = 1.5032648283379522D;
            // 
            // txt30月下次随访
            // 
            this.txt30月下次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月下次随访.Name = "txt30月下次随访";
            this.txt30月下次随访.StylePriority.UseBorders = false;
            this.txt30月下次随访.Weight = 1.5000002102851926D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.txt12月随访医生,
            this.txt18月随访医生,
            this.txt24月随访医生,
            this.txt30月随访医生});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.StylePriority.UseBorders = false;
            this.xrTableRow59.Weight = 1.0312982731678162D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Text = "随访医生签名";
            this.xrTableCell246.Weight = 1.5000000670750944D;
            // 
            // txt12月随访医生
            // 
            this.txt12月随访医生.Name = "txt12月随访医生";
            this.txt12月随访医生.Weight = 1.4959010937930211D;
            // 
            // txt18月随访医生
            // 
            this.txt18月随访医生.Name = "txt18月随访医生";
            this.txt18月随访医生.Weight = 1.5008341056845209D;
            // 
            // txt24月随访医生
            // 
            this.txt24月随访医生.Name = "txt24月随访医生";
            this.txt24月随访医生.Weight = 1.5032648283379522D;
            // 
            // txt30月随访医生
            // 
            this.txt30月随访医生.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt30月随访医生.Name = "txt30月随访医生";
            this.txt30月随访医生.StylePriority.UseBorders = false;
            this.txt30月随访医生.Weight = 1.5000002102851926D;
            // 
            // txt姓名
            // 
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(143.75F, 80.16666F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(152.08F, 20F);
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 2F;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(596.6667F, 82.58667F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.BorderWidth = 2F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(551.0417F, 82.58333F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseBorderWidth = false;
            this.xrLabel11.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 2F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(578.125F, 82.58333F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(616.6667F, 82.58333F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 2F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(642.7083F, 82.58333F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.BorderWidth = 2F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(668.75F, 82.58333F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseBorderWidth = false;
            this.xrLabel7.StylePriority.UseFont = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.BorderWidth = 2F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(695.8333F, 82.58333F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseBorderWidth = false;
            this.xrLabel6.StylePriority.UseFont = false;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 2F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(724.3749F, 82.58333F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.BorderWidth = 2F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(525F, 82.58333F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseBorderWidth = false;
            this.xrLabel12.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(455.0002F, 80.16666F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(57.50482F, 20F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "编号";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(45.83333F, 80.16666F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(75.005F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "姓名：";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(225F, 32.25001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(300F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "1~2岁儿童健康检查记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 19.00001F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report1_2岁儿童健康检查记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(55, 45, 19, 12);
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel txt12月随访;
        private DevExpress.XtraReports.UI.XRLabel txt18月随访;
        private DevExpress.XtraReports.UI.XRLabel txt24月随访;
        private DevExpress.XtraReports.UI.XRLabel txt30月随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRLabel txt12月体重;
        private DevExpress.XtraReports.UI.XRLabel txt18月体重;
        private DevExpress.XtraReports.UI.XRLabel txt24月体重;
        private DevExpress.XtraReports.UI.XRLabel txt30月体重;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRLabel txt12月身长;
        private DevExpress.XtraReports.UI.XRLabel txt18月身长;
        private DevExpress.XtraReports.UI.XRLabel txt24月身长;
        private DevExpress.XtraReports.UI.XRLabel txt30月身长;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel txt18月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRLabel txt18月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel txt24月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRLabel txt24月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel txt12月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel txt12月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLine xrLine20;
        private DevExpress.XtraReports.UI.XRLine xrLine21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell txt12月龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell txt18月龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell txt24月龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell txt30月龋齿数;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell txt12月步态;
        private DevExpress.XtraReports.UI.XRTableCell txt18月步态;
        private DevExpress.XtraReports.UI.XRTableCell txt24月步态;
        private DevExpress.XtraReports.UI.XRTableCell txt30月步态;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRLabel txt12月户外;
        private DevExpress.XtraReports.UI.XRLabel txt18月户外;
        private DevExpress.XtraReports.UI.XRLabel txt24月户外;
        private DevExpress.XtraReports.UI.XRLabel txt30月户外;
        private DevExpress.XtraReports.UI.XRLabel txt12月维生素;
        private DevExpress.XtraReports.UI.XRLabel txt18月维生素;
        private DevExpress.XtraReports.UI.XRLabel txt24月维生素;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRLabel txt12月其他;
        private DevExpress.XtraReports.UI.XRLabel txt18月其他;
        private DevExpress.XtraReports.UI.XRLabel txt24月其他;
        private DevExpress.XtraReports.UI.XRLabel txt30月其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel txt12月转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel txt12月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRLabel txt18月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRLabel txt18月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel txt24月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRLabel txt24月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRLabel txt30月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRLabel txt30月转诊机构;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRLabel txt24月服务其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell txt12月下次随访;
        private DevExpress.XtraReports.UI.XRTableCell txt18月下次随访;
        private DevExpress.XtraReports.UI.XRTableCell txt24月下次随访;
        private DevExpress.XtraReports.UI.XRTableCell txt30月下次随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell txt12月随访医生;
        private DevExpress.XtraReports.UI.XRTableCell txt18月随访医生;
        private DevExpress.XtraReports.UI.XRTableCell txt24月随访医生;
        private DevExpress.XtraReports.UI.XRTableCell txt30月随访医生;
        private DevExpress.XtraReports.UI.XRLabel txt18月服务其他;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRLabel txt18月血红;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRLabel txt30月血红;
        private DevExpress.XtraReports.UI.XRLine xrLine22;
        private DevExpress.XtraReports.UI.XRLine xrLine23;
        private DevExpress.XtraReports.UI.XRLine xrLine24;
        private DevExpress.XtraReports.UI.XRLine xrLine25;
        private DevExpress.XtraReports.UI.XRLine xrLine26;
        private DevExpress.XtraReports.UI.XRLine xrLine27;
        private DevExpress.XtraReports.UI.XRLine xrLine28;
        private DevExpress.XtraReports.UI.XRLine xrLine29;
        private DevExpress.XtraReports.UI.XRLine xrLine30;
        private DevExpress.XtraReports.UI.XRLine xrLine31;
        private DevExpress.XtraReports.UI.XRLine xrLine32;
        private DevExpress.XtraReports.UI.XRLine xrLine33;
        private DevExpress.XtraReports.UI.XRLine xrLine34;
        private DevExpress.XtraReports.UI.XRLine xrLine35;
        private DevExpress.XtraReports.UI.XRLine xrLine36;
        private DevExpress.XtraReports.UI.XRLine xrLine42;
        private DevExpress.XtraReports.UI.XRLine xrLine43;
        private DevExpress.XtraReports.UI.XRLine xrLine44;
        private DevExpress.XtraReports.UI.XRLine xrLine45;
        private DevExpress.XtraReports.UI.XRLine xrLine46;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell txt12月服务其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRLabel txt30月服务其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLine xrLine37;
        private DevExpress.XtraReports.UI.XRLine xrLine38;
        private DevExpress.XtraReports.UI.XRLine xrLine39;
        private DevExpress.XtraReports.UI.XRLine xrLine40;
        private DevExpress.XtraReports.UI.XRLine xrLine41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRLabel txt12月转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt18月转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt24月转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt30月转诊建议;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRLabel txt12月面色;
        private DevExpress.XtraReports.UI.XRLabel txt18月面色;
        private DevExpress.XtraReports.UI.XRLabel txt24月面色;
        private DevExpress.XtraReports.UI.XRLabel txt30月面色;
        private DevExpress.XtraReports.UI.XRLabel txt12月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt18月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt24月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt30月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt12月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt18月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt24月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt12月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt18月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt24月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt30月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt12月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt18月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt24月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt30月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt12月听力;
        private DevExpress.XtraReports.UI.XRLabel txt24月听力;
        private DevExpress.XtraReports.UI.XRLabel txt12月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt18月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt24月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt30月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt12月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt18月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt24月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt30月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt12月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt18月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt24月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt30月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt18月步大;
        private DevExpress.XtraReports.UI.XRLabel txt24月步大;
        private DevExpress.XtraReports.UI.XRLabel txt30月步大;
        private DevExpress.XtraReports.UI.XRLabel txt12月佝偻病;
        private DevExpress.XtraReports.UI.XRLabel txt18月佝偻病;
        private DevExpress.XtraReports.UI.XRLabel txt24月佝偻病;
        private DevExpress.XtraReports.UI.XRLabel txt12月预防;
        private DevExpress.XtraReports.UI.XRLabel txt18月预防;
        private DevExpress.XtraReports.UI.XRLabel txt24月预防;
        private DevExpress.XtraReports.UI.XRLabel txt30月预防;
        private DevExpress.XtraReports.UI.XRLabel txt12月发育;
        private DevExpress.XtraReports.UI.XRLabel txt18月发育;
        private DevExpress.XtraReports.UI.XRLabel txt24月发育;
        private DevExpress.XtraReports.UI.XRLabel txt12月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt18月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt24月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt30月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt12月科学;
        private DevExpress.XtraReports.UI.XRLabel txt12月生长;
        private DevExpress.XtraReports.UI.XRLabel txt12月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt12月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt12月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt18月科学;
        private DevExpress.XtraReports.UI.XRLabel txt18月生长;
        private DevExpress.XtraReports.UI.XRLabel txt18月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt18月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt18月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt24月合理;
        private DevExpress.XtraReports.UI.XRLabel txt24月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt24月生长;
        private DevExpress.XtraReports.UI.XRLabel txt24月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt24月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt30月合理;
        private DevExpress.XtraReports.UI.XRLabel txt30月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt30月生长;
        private DevExpress.XtraReports.UI.XRLabel txt30月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt30月疾病;
        private DevExpress.XtraReports.UI.XRLine xrLine47;
        private DevExpress.XtraReports.UI.XRLine xrLine48;
        private DevExpress.XtraReports.UI.XRLine xrLine49;
        private DevExpress.XtraReports.UI.XRLine xrLine50;
        private DevExpress.XtraReports.UI.XRLine xrLine51;
        private DevExpress.XtraReports.UI.XRTableCell txt12月出牙数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell txt18月出牙数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell txt24月出牙数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell txt30月出牙数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
    }
}
