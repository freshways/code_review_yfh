﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC儿童健康检查_5岁 : UserControlChild
    {
        private string m_grdabh = null;
        private string m_closeType = null;
        private UpdateType m_updateType = UpdateType.None;
        

        private DataSet m_ds5岁 = null;
        private bll儿童_健康检查_5岁 m_bll5岁 = new bll儿童_健康检查_5岁();
        private string strCurrentServerDate = null;

        public UC儿童健康检查_5岁(string grdabh, UpdateType updateType, string colseType)
        {
            InitializeComponent();

            ucTxtLbl体重.Txt1.Leave += textEdit_Leave;
            ucTxtLbl身长.Txt1.Leave += textEdit_Leave;
            //ucTxtLbl头围.Txt1.Leave += textEdit_Leave;
            //ucTxtLblTxtLbl前囟.Txt1.Leave += textEdit_Leave;
            //ucTxtLblTxtLbl前囟.Txt2.Leave += textEdit_Leave;
            ucTxtLbl血红蛋白值.Txt1.Leave += textEdit_Leave;
            //ucTxtLbl服用维生素.Txt1.Leave += textEdit_Leave;
            //ucTxtLbl户外活动.Txt1.Leave += textEdit2_Leave;
            //ucTxtLbl出牙数.Txt1.Leave += textEdit100_Leave;
            ucTxtLblTxtLbl出牙龋齿.Txt1.Leave += textEdit100_Leave;
            ucTxtLblTxtLbl出牙龋齿.Txt2.Leave += textEdit100_Leave;

            m_grdabh = grdabh;
            m_updateType = updateType;
            m_closeType = colseType;
            strCurrentServerDate = m_bll5岁.ServiceDateTime;

            BindData();
        }

        //private void Binder

        #region 控件关联
        //private void comboBoxEdit前囟_TextChanged(object sender, EventArgs e)
        //{
        //    string value = util.ControlsHelper.GetComboxKey(comboBoxEdit前囟);
        //    if (value == "99")
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = true;
        //    }
        //    else
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = false;
        //    }
        //}

        private void comboBoxEdit转诊_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);

            //1是“有”的编码
            if (value != null && value.Equals("1"))
            {
                ucLblTxt转诊原因.Enabled = true;
                ucLblTxt转诊机构.Enabled = true;
                ucLblTxt联系人.Enabled = true;
                ucLblTxt联系方式.Enabled = true;
                comboBoxEdit是否到位.Enabled = true;
            }
            else
            {
                ucLblTxt转诊原因.Enabled = false;
                ucLblTxt转诊原因.Txt1.Text = "";
                ucLblTxt转诊机构.Enabled = false;
                ucLblTxt转诊机构.Txt1.Text = "";
                ucLblTxt联系人.Enabled = false;
                ucLblTxt联系方式.Enabled = false;
                comboBoxEdit是否到位.Enabled = false;
                ucLblTxt联系人.Txt1.Text = "";
                ucLblTxt联系方式.Txt1.Text = "";
                comboBoxEdit是否到位.EditValue = "";
            }
        }
        #endregion

        private void SetDefaultValue()
        {
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit口腔);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit皮肤);
            //util.ControlsHelper.SetComboxData("2", comboBoxEdit颈部包块);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit眼外观);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit耳外观);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit胸部);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit腹部);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit脐部);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit四肢);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit佝偻病症状);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit可疑佝偻);
            //util.ControlsHelper.SetComboxData("1", ); 听力
            util.ControlsHelper.SetComboxData("2", comboBoxEdit转诊);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit步态);
        }

        private void UC儿童健康检查_5岁_Load(object sender, EventArgs e)
        {
            m_ds5岁 = m_bll5岁.GetBusinessByGrdabh(m_grdabh,true);

            textEdit儿童档案号.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();

            string str姓名 = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = DESEncrypt.DES解密(str姓名);
            textEdit身份证号.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号].ToString();

            string str性别 = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别].ToString();
            DataRow[] dr性别 = DataDictCache.Cache.t性别.Select("P_CODE='"+str性别+"'");
            if(dr性别.Length >0)
            {
                textEdit性别.Text = dr性别[0]["P_DESC"].ToString();
            }
            textEdit出生日期.Text = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();

            textEdit最近修改时间.Text = strCurrentServerDate;
            textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;

            if (m_updateType == UpdateType.Add)
            {
                //作用：为 m_ds5岁 添加“健康检查_5岁”数据
                m_bll5岁.NewBusiness();

                textEdit录入人.Text = Loginer.CurrentUser.AccountName;
                textEdit录入时间.Text = strCurrentServerDate;

                textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;
                textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;

                SetDefaultValue();
            }
            else if (m_updateType == UpdateType.Modify)
            {
                dateEdit随访日期.Properties.ReadOnly = true;
                dateEdit随访日期.BackColor = SystemColors.Control;
                dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
                dateEdit下次随访日期.Properties.ReadOnly = true;
                dateEdit下次随访日期.BackColor = SystemColors.Control;
                dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;

                textEdit卡号.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.卡号].ToString();
                ucTxtLbl体重.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重].ToString();

                string str体重选项 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重选项].ToString();
                util.ControlsHelper.SetComboxData(str体重选项, comboBoxEdit体重);
                
                ucTxtLbl身长.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高].ToString();
                string str身长选项 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高选项].ToString();
                util.ControlsHelper.SetComboxData(str身长选项, comboBoxEdit身长);

                //ucTxtLbl头围.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.头围].ToString();

                //string str步态 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.步态].ToString();
                //util.ControlsHelper.SetComboxData(str步态, comboBoxEdit步态);

                string str体格 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体格发育评价].ToString();
                util.ControlsHelper.SetComboxData(str体格, comboBoxEdit体格);

                //string str前囟 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟].ToString();
                //util.ControlsHelper.SetComboxData(str前囟, comboBoxEdit前囟);

                //ucTxtLblTxtLbl前囟.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟值].ToString();
                //ucTxtLblTxtLbl前囟.Txt2.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.后囟值].ToString();

                //ucTxtLbl出牙数.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.出牙数].ToString();

                ucTxtLblTxtLbl出牙龋齿.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.牙齿数目].ToString();
                ucTxtLblTxtLbl出牙龋齿.Txt2.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.龋齿数].ToString();

                //string str颈部包块 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.颈部包块].ToString();
                //util.ControlsHelper.SetComboxData(str颈部包块, comboBoxEdit颈部包块);

                //string str眼外观 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.眼部].ToString();
                //util.ControlsHelper.SetComboxData(str眼外观, comboBoxEdit眼外观);

                //string str耳外观 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.耳部].ToString();
                //util.ControlsHelper.SetComboxData(str耳外观, comboBoxEdit耳外观);

                //string str心肺 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.心肺].ToString();
                //util.ControlsHelper.SetComboxData(str心肺, comboBoxEdit胸部);

                string str腹部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.腹部].ToString();
                util.ControlsHelper.SetComboxData(str腹部, comboBoxEdit腹部);

                //string str脐部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.脐部].ToString();
                //util.ControlsHelper.SetComboxData(str脐部, comboBoxEdit脐部);

                //string str四肢 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.四肢].ToString();
                //util.ControlsHelper.SetComboxData(str四肢, comboBoxEdit四肢);

                //string str可疑佝偻 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病体征].ToString();
                //util.ControlsHelper.SetComboxData(str可疑佝偻, comboBoxEdit可疑佝偻);

                //string str佝偻病症状 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病症状].ToString();
                //util.ControlsHelper.SetComboxData(str佝偻病症状, comboBoxEdit佝偻病症状);

                //string str肛门 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.生殖器].ToString();
                //util.ControlsHelper.SetComboxData(str肛门, comboBoxEdit肛门外生殖器);

                ucTxtLbl血红蛋白值.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.血红蛋白值].ToString();

                //string str听力 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.听力].ToString();
                //util.ControlsHelper.SetComboxData(str听力, textEdit视力);
                textEdit视力.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.视力].ToString();

                //ucTxtLbl户外活动.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.户外活动].ToString();
                //ucTxtLbl服用维生素.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.服用维生素].ToString();

                //string str发育评估 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估].ToString();
                //util.ControlsHelper.SetComboxData(str发育评估, comboBoxEdit发育评估);

                //string str面色 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.面色].ToString();
                char[] c分隔符 = {',' };
                //string[] str面色Value = str面色.Split(c分隔符);
                //for (int index = 0; index < str面色Value.Length; index++ )
                //{
                //    //if(str面色Value[index].Equals("1"))
                //    //{
                //    //    checkEdit面色未检.Checked = true;
                //    //    break;
                //    //}
                //    //else 
                //    if(str面色Value[index].Equals("2"))
                //    {
                //        checkEdit面色红润.Checked = true;
                //    }
                //    //else if(str面色Value[index].Equals("3"))
                //    //{
                //    //    checkEdit面色黄染.Checked = true;
                //    //}
                //    else if(str面色Value[index].Equals("99"))
                //    {
                //        checkEdit面色其他.Checked = true;
                //    }
                //}

                //string str两次随访间患病情况 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.两次随访间患病情况].ToString();
                //util.ControlsHelper.SetComboxData(str两次随访间患病情况, comboBoxEdit两次随访间患病情况);

                string str患病Temp = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况].ToString();
                string[] str患病 = str患病Temp.Split(c分隔符);
                for(int index = 0; index < str患病.Length; index++)
                {
                    if(str患病[index].Equals("1"))
                    {
                        checkEdit患病无.Checked = true;
                        break;
                    }
                    else if(str患病[index].Equals("2"))
                    {
                        checkEdit患病肺炎.Checked = true;
                        textEdit患病肺炎.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况肺炎].ToString();
                    }
                    else if (str患病[index].Equals("97"))
                    {
                        checkEdit患病腹泻.Checked = true;
                        textEdit患病腹泻.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况腹泻].ToString();
                    }
                    else if(str患病[index].Equals("98"))
                    {
                        checkEdit患病外伤.Checked = true;
                        textEdit患病外伤.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况外伤].ToString();
                    }
                    else if(str患病[index].Equals("99"))
                    {
                        checkEdit患病其他.Checked = true;
                        textEdit患病其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况其他].ToString();
                    }
                }


                string str转诊 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊状况].ToString();
                util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);

                ucLblTxt转诊原因.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊原因].ToString();
                ucLblTxt转诊机构.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊机构].ToString();

                string str指导 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.指导].ToString();
                string[] str指导Values = str指导.Split(c分隔符);
                for (int index = 0; index < str指导Values.Length; index++)
                {
                    switch(str指导Values[index])
                    {
                        case "1":
                            checkEdit指导合理膳食.Checked = true;
                            break;
                        case "2":
                            checkEdit指导预防意外1.Checked = true;
                            break;
                        case "3":
                            checkEdit指导生长发育.Checked = true;
                            break;
                        case "4":
                            checkEdit指导口腔保健.Checked = true;
                            break;
                        case "99":
                            checkEdit指导疾病预防.Checked = true;
                            break;
                        case "100":
                            checkEdit指导其他.Checked = true;
                            textEdit指导其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.E_XSEZDQT].ToString();
                            break;
                        default:
                            break;
                    }
                }

                //string str中医Temp = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理].ToString();
                //string[] str中医 = str中医Temp.Split(c分隔符);
                //for (int index = 0; index < str中医.Length; index++ )
                //{
                //    switch (str中医[index])
                //    {
                //        case "1":
                //            checkEdit中医饮食.Checked = true;
                //            break;
                //        case "2":
                //            checkEdit中医起居.Checked = true;
                //            break;
                //        case "3":
                //            checkEdit传授摩捏.Checked = true;
                //            break;
                //        case "4":
                //            checkEdit中医其他.Checked = true;
                //            textEdit中医其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理其他].ToString();
                //            break;
                //    }
                //}

                #region 新版本添加
                textEdit健康管理联系人.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.健康管理联系人].ToString();
                textEdit联系电话.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系电话].ToString();
                textEdit所在幼儿园名称.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所在幼儿园名称].ToString();
                string str查体机构 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构].ToString();
                if (!string.IsNullOrEmpty(str查体机构) && str查体机构 == "3")
                {
                    radio查体机构.EditValue = "3";
                    textEdit查体机构其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构其他].ToString();
                }
                else
                {
                    radio查体机构.EditValue = str查体机构;
                }
                string str体重指数 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重指数].ToString();
                util.ControlsHelper.SetComboxData(str体重指数, comboBoxEdit体重指数);
                string str胸部 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.胸部].ToString();
                util.ControlsHelper.SetComboxData(str胸部, comboBoxEdit胸部);
                string str发育评估 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估].ToString();
                string[] strs发育评估 = str发育评估.Split(c分隔符);
                for (int index = 0; index < strs发育评估.Length; index++)
                {
                    switch (strs发育评估[index])
                    {
                        case "1":
                            chk发育评估无.Checked = true;
                            break;
                        case "2":
                            chk发育评估1.Checked = true;
                            break;
                        case "3":
                            chk发育评估2.Checked = true;
                            break;
                        case "4":
                            chk发育评估3.Checked = true;
                            break;
                        case "5":
                            chk发育评估4.Checked = true;
                            break;
                    }
                }
                ucLblTxt联系人.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系人].ToString();
                ucLblTxt联系方式.Txt1.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系方式].ToString();
                string str结果 = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.结果].ToString();
                util.ControlsHelper.SetComboxData(str结果, comboBoxEdit是否到位);
                textEdit家长签名.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.家长签名].ToString();
                #endregion

                textEdit其他.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.其他].ToString();
                dateEdit下次随访日期.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.下次随访日期].ToString();
                dateEdit随访日期.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发生时间].ToString();
                textEdit随访医生.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.随访医生].ToString();
                
                textEdit录入时间.Text = m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建时间].ToString();
                textEdit录入人.Text = _bllUser.Return用户名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建人].ToString());
                textEdit创建机构.Text = _bll机构信息.Return机构名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建机构].ToString());
                textEdit当前所属机构.Text = _bll机构信息.Return机构名称(m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所属机构].ToString());

                #region 字体变色
                //Set考核项颜色_new(layoutControl1, null);
                ChangeColorForLayoutItem();
                #endregion
            }

        }

        private void ChangeColorForLayoutItem()
        {
            if (string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit体重.Text))
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLbl身长.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit身长.Text))
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            SetComboBoxEditItemColor(comboBoxEdit体格, lciTgfypj);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl头围.Txt1.Text))
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //if (//checkEdit面色未检.Checked || 
            //    checkEdit面色红润.Checked
            //    //|| checkEdit面色黄染.Checked 
            //    || checkEdit面色其他.Checked)
            //{
            //    lciFace.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lciFace.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            if (string.IsNullOrWhiteSpace(textEdit视力.Text))
            {
                lciSight.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSight.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);
            //SetComboBoxEditItemColor(comboBoxEdit口腔, lci口腔);
            //SetComboBoxEditItemColor(comboBoxEdit皮肤, lci皮肤);

            //if (string.IsNullOrWhiteSpace(comboBoxEdit前囟.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt1.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt2.Text))
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit颈部包块, lci颈部包块);
            //SetComboBoxEditItemColor(comboBoxEdit眼外观, lciEye);
            //SetComboBoxEditItemColor(comboBoxEdit耳外观, lciEar);

            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt1.Text) || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt2.Text))
            {
                lci牙齿龋齿.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci牙齿龋齿.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            SetComboBoxEditItemColor(comboBoxEdit胸部, lbl胸部);
            SetComboBoxEditItemColor(comboBoxEdit腹部, lciBelly);

            if (string.IsNullOrWhiteSpace(ucTxtLbl血红蛋白值.Txt1.Text))
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(textEdit其他.Text))
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            //SetComboBoxEditItemColor(comboBoxEdit脐部, lciNavel);
            //SetComboBoxEditItemColor(comboBoxEdit四肢, lciFours);
            //SetComboBoxEditItemColor(comboBoxEdit佝偻病症状, lciGlbzz);
            //SetComboBoxEditItemColor(comboBoxEdit可疑佝偻, lciGlbtz);
            //SetComboBoxEditItemColor(comboBoxEdit肛门外生殖器, lciSzq);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl户外活动.Txt1.Text))
            //{
            //    lciHwhd.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciHwhd.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl服用维生素.Txt1.Text))
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit发育评估, lciFypg);

            //SetComboBoxEditItemColor(comboBoxEdit步态, lciBt);

            //SetComboBoxEditItemColor(comboBoxEdit两次随访间患病情况, lciLcsfjhbqk);
            //if (!(checkEdit患病无.Checked)
            //    && !(checkEdit患病肺炎.Checked && !(string.IsNullOrWhiteSpace(textEdit患病肺炎.Text.Trim())))
            //    && !(checkEdit患病腹泻.Checked && !(string.IsNullOrWhiteSpace(textEdit患病腹泻.Text.Trim())))
            //    && !(checkEdit患病外伤.Checked && !(string.IsNullOrWhiteSpace(textEdit患病外伤.Text.Trim())))
            //    && !(checkEdit患病其他.Checked && !(string.IsNullOrWhiteSpace(textEdit患病其他.Text.Trim())))
            //    )
            //{
            //    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            if (checkEdit患病无.Checked || checkEdit患病肺炎.Checked || checkEdit患病腹泻.Checked
                || checkEdit患病外伤.Checked || checkEdit患病其他.Checked)
            {
                //未选择肺炎、腹泻、患病、其他的情况下，即只勾选了“无”的情况下
                if (!(checkEdit患病肺炎.Checked) && !(checkEdit患病腹泻.Checked) && !(checkEdit患病外伤.Checked) && !(checkEdit患病其他.Checked))
                {
                    lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    if (checkEdit患病肺炎.Checked && string.IsNullOrWhiteSpace(textEdit患病肺炎.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病腹泻.Checked && string.IsNullOrWhiteSpace(textEdit患病腹泻.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病外伤.Checked && string.IsNullOrWhiteSpace(textEdit患病外伤.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else if (checkEdit患病其他.Checked && string.IsNullOrWhiteSpace(textEdit其他.Text))
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
                    }
                    else
                    {
                        lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Blue;
                    }
                }
            }
            else
            {
                lciLcsfjhbqk.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(comboBoxEdit转诊.Text))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (comboBoxEdit转诊.Text == "有" && (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text)
                      || string.IsNullOrWhiteSpace(ucLblTxt联系人.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt联系方式.Txt1.Text)
                      || string.IsNullOrWhiteSpace(comboBoxEdit是否到位.EditValue.ToString())
                ))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (checkEdit指导合理膳食.Checked || checkEdit指导生长发育.Checked
                || checkEdit指导疾病预防.Checked || checkEdit指导预防意外1.Checked
                || checkEdit指导口腔保健.Checked)
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            #region 新版本添加（考核项是否填写判断）
            //if (!string.IsNullOrWhiteSpace(textEdit健康管理联系人.Text))
            //{
            //    lbl健康管理联系人.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl健康管理联系人.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //if (!string.IsNullOrWhiteSpace(textEdit联系电话.Text))
            //{
            //    lbl联系电话.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl联系电话.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //if (!string.IsNullOrWhiteSpace(textEdit所在幼儿园名称.Text))
            //{
            //    lbl所在幼儿园名称.AppearanceItemCaption.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lbl所在幼儿园名称.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            if (!string.IsNullOrWhiteSpace(radio查体机构.EditValue.ToString())
                || (radio查体机构.EditValue.ToString() == "3" && !string.IsNullOrWhiteSpace(textEdit查体机构其他.Text))
                )
            {
                lbl查体机构.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl查体机构.AppearanceItemCaption.ForeColor = Color.Red;
            }
            if (chk发育评估无.Checked || chk发育评估1.Checked || chk发育评估2.Checked
                || chk发育评估3.Checked || chk发育评估4.Checked)
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (!string.IsNullOrWhiteSpace(textEdit家长签名.Text))
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Red;
            }
            #endregion
        }

        private void SetComboBoxEditItemColor(DevExpress.XtraEditors.ComboBoxEdit combo, DevExpress.XtraLayout.LayoutControlItem item)
        {
            if (string.IsNullOrWhiteSpace(combo.Text))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }


        #region 为控件绑定数据
        private void BindData()
        {
            BindDataForComboBox("hw_item", comboBoxEdit体重);

            BindDataForComboBox("hw_item", comboBoxEdit身长);

            //BindDataForComboBox("ssbtqk", comboBoxEdit步态);

            BindDataForComboBox("ebjctgfypj", comboBoxEdit体格);

            //BindDataForComboBox("tl_tingli", textEdit视力);

            //BindDataForComboBox("qx_qianxin", comboBoxEdit前囟);

            //BindDataForComboBox("yw_youwu", comboBoxEdit颈部包块);

            //BindDataForComboBox("ywyc", comboBoxEdit眼外观);

            //BindDataForComboBox("ywyc", comboBoxEdit耳外观);

            BindDataForComboBox("ywyc", comboBoxEdit胸部);

            BindDataForComboBox("ywyc", comboBoxEdit腹部);

            //BindDataForComboBox("jd_jidai", comboBoxEdit脐部);

            //BindDataForComboBox("ywyc", comboBoxEdit四肢);

            //BindDataForComboBox("glbtz", comboBoxEdit可疑佝偻);

            //BindDataForComboBox("myglbzz", comboBoxEdit佝偻病症状);

            //BindDataForComboBox("ywyc", comboBoxEdit肛门外生殖器);

            //BindDataForComboBox("myfypg", comboBoxEdit发育评估);

            //BindDataForComboBox("mylchbqk", comboBoxEdit两次随访间患病情况);

            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);

            BindDataForComboBox("hw_item", comboBoxEdit体重指数);

            BindDataForComboBox("sfdw", comboBoxEdit是否到位);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }
        #endregion

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            

            string str转诊 = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);
            if(str转诊.Equals("1") )
            {
                if (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text))
                {
                    Msg.ShowInformation("如果有转诊情况，请填写转诊原因。");
                    ucLblTxt转诊原因.Txt1.Focus();
                    return;
                }
                if(string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text))
                {
                    Msg.ShowInformation("如果有转诊情况，请填写转诊机构。");
                    ucLblTxt转诊机构.Txt1.Focus();
                    return;
                }
            }

            if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                Msg.ShowInformation("“指导”一栏如果勾选“其他”,请填写其他内容。");
                textEdit指导其他.Focus();
                return;
            }

            if(string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                Msg.ShowInformation("请填写下次随访日期");
                dateEdit下次随访日期.Focus();
                return;
            }

            if(string.IsNullOrWhiteSpace(dateEdit随访日期.Text) )
            {
                Msg.ShowInformation("请填写随访日期");
                dateEdit随访日期.Focus();
                return;
            }
            if (dateEdit随访日期.DateTime > Convert.ToDateTime(this.textEdit录入时间.Text))
            {
                Msg.ShowInformation("随访日期不能大于填写日期");
                dateEdit随访日期.Focus();
                return;
            }
            if (m_updateType == UpdateType.Add)
            {
                if (Msg.AskQuestion("信息保存后，‘随访日期’与‘下次随访日期’将不允许修改，确认保存信息？"))
                {
                }
                else
                {
                    return;
                }
            }

            if (m_updateType == UpdateType.Add)
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.个人档案编号] = m_grdabh;
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建机构] = Loginer.CurrentUser.所属机构;
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所属机构] = Loginer.CurrentUser.所属机构;
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建人] = Loginer.CurrentUser.用户编码;
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.创建时间] = strCurrentServerDate;

                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.下次随访日期] = dateEdit下次随访日期.Text;
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发生时间] = dateEdit随访日期.Text;
            }
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改人] = Loginer.CurrentUser.用户编码;
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改时间] = strCurrentServerDate;

            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.卡号] = textEdit卡号.Text;

            if (string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text))
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重] = DBNull.Value;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重] = ucTxtLbl体重.Txt1.Text;
            }
            
            string str体重选项 = util.ControlsHelper.GetComboxKey(comboBoxEdit体重);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重选项] = str体重选项;

            if (string.IsNullOrWhiteSpace(ucTxtLbl身长.Txt1.Text))
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高] = DBNull.Value;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高] = ucTxtLbl身长.Txt1.Text;
            }
            string str身长选项 = util.ControlsHelper.GetComboxKey(comboBoxEdit身长);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.身高选项] = str身长选项;

            //if (string.IsNullOrWhiteSpace(ucTxtLbl头围.Txt1.Text))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.头围] = DBNull.Value;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.头围] = ucTxtLbl头围.Txt1.Text;
            //}

            //string str步态 = util.ControlsHelper.GetComboxKey(comboBoxEdit步态);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.步态] = str步态;

            string str体格 = util.ControlsHelper.GetComboxKey(comboBoxEdit体格);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体格发育评价] = str体格;

            //string str前囟 = util.ControlsHelper.GetComboxKey(comboBoxEdit前囟);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟] = str前囟;


            //if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt1.Text))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟值] = DBNull.Value;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.前囟值] = ucTxtLblTxtLbl前囟.Txt1.Text;
            //}

            //if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt2.Text))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.后囟值] = DBNull.Value;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.后囟值] = ucTxtLblTxtLbl前囟.Txt2.Text;
            //}

            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.出牙数] = ucTxtLbl出牙数.Txt1.Text;
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.牙齿数目] = ucTxtLblTxtLbl出牙龋齿.Txt1.Text;
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.龋齿数] = ucTxtLblTxtLbl出牙龋齿.Txt2.Text;
            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt1.Text))
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.牙齿数目] = DBNull.Value;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.牙齿数目] = ucTxtLblTxtLbl出牙龋齿.Txt1.Text;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt2.Text))
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.龋齿数] = DBNull.Value;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.龋齿数] = ucTxtLblTxtLbl出牙龋齿.Txt2.Text;
            }
            //string str颈部包块 = util.ControlsHelper.GetComboxKey(comboBoxEdit颈部包块);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.颈部包块] = str颈部包块;

            //string str眼部 = util.ControlsHelper.GetComboxKey(comboBoxEdit眼外观);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.眼部] = str眼部;

            //string str耳部 = util.ControlsHelper.GetComboxKey(comboBoxEdit耳外观);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.耳部] = str耳部;

            string str心肺 = util.ControlsHelper.GetComboxKey(comboBoxEdit胸部);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.心肺] = str心肺;

            string str腹部 = util.ControlsHelper.GetComboxKey(comboBoxEdit腹部);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.腹部] = str腹部;

            //string str脐部 = util.ControlsHelper.GetComboxKey(comboBoxEdit脐部);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.脐部] = str脐部;

            //string str四肢 = util.ControlsHelper.GetComboxKey(comboBoxEdit四肢);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.四肢] = str四肢;

            //string str佝偻病体征 = util.ControlsHelper.GetComboxKey(comboBoxEdit可疑佝偻);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病体征] = str佝偻病体征;

            //string str佝偻病症状 = util.ControlsHelper.GetComboxKey(comboBoxEdit佝偻病症状);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.佝偻病症状] = str佝偻病症状;

            //string str肛门生殖器 = util.ControlsHelper.GetComboxKey(comboBoxEdit肛门外生殖器);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.生殖器] = str肛门生殖器;

            if (string.IsNullOrWhiteSpace(ucTxtLbl血红蛋白值.Txt1.Text))
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.血红蛋白值] = DBNull.Value;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.血红蛋白值] = ucTxtLbl血红蛋白值.Txt1.Text;
            }

            //if (string.IsNullOrWhiteSpace(ucTxtLbl户外活动.Txt1.Text))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.户外活动] = DBNull.Value;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.户外活动] = ucTxtLbl户外活动.Txt1.Text;
            //}

            //if (string.IsNullOrWhiteSpace(ucTxtLbl服用维生素.Txt1.Text))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.服用维生素] = DBNull.Value;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.服用维生素] = ucTxtLbl服用维生素.Txt1.Text;
            //}

            //string str发育评估 = util.ControlsHelper.GetComboxKey(comboBoxEdit发育评估);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估] = str发育评估;

            //string str听力 = util.ControlsHelper.GetComboxKey(textEdit视力);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.听力] = str听力;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.视力] = textEdit视力.Text;

            //if (checkEdit面色未检.Checked)
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.面色] = "1";
            //}
            //else
            //{
                ////string str面色 = "";
                ////if (checkEdit面色红润.Checked)
                ////{
                ////    str面色 += "2,";
                ////}
                //////if (checkEdit面色黄染.Checked)
                //////{
                //////    str面色 += "3,";
                //////}
                ////if (checkEdit面色其他.Checked)
                ////{
                ////    str面色 += "99,";
                ////}
                ////if (str面色.Length > 0)
                ////{
                ////    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.面色] = str面色.Substring(0, str面色.Length - 1);
                ////}
                ////else
                ////{
                ////    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.面色] = "";
                ////}
            //}

            //string str两次 = util.ControlsHelper.GetComboxKey(comboBoxEdit两次随访间患病情况);
            //m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.两次随访间患病情况] = str两次;
            if(checkEdit患病无.Checked)
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况] = "1";
            }
            else
            {
                string str患病 = "";
                if(checkEdit患病肺炎.Checked)
                {
                    str患病 += "2,";
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况肺炎] = textEdit患病肺炎.Text;
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况肺炎] = null;
                }

                if(checkEdit患病腹泻.Checked)
                {
                    str患病 += "97,";
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况腹泻] = textEdit患病腹泻.Text;
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况腹泻] = null;
                }

                if(checkEdit患病外伤.Checked)
                {
                    str患病 += "98,";
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况外伤] = textEdit患病外伤.Text;
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况外伤] = null;
                }

                if(checkEdit患病其他.Checked)
                {
                    str患病 += "99,";
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况其他] = textEdit患病其他.Text;
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况其他] = null;
                }

                if(str患病.Length > 0)
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况] = str患病;
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.患病情况] = null;
                }
            }

            
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊状况] = str转诊;

            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊原因] = ucLblTxt转诊原因.Txt1.Text;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.转诊机构] = ucLblTxt转诊机构.Txt1.Text;

            string str指导 = "";
            if (checkEdit指导合理膳食.Checked)
            {
                str指导 += "1,";
            }
            if (checkEdit指导预防意外1.Checked)
            {
                str指导 += "2,";
            }
            if (checkEdit指导生长发育.Checked)
            {
                str指导 += "3,";
            }
            if (checkEdit指导口腔保健.Checked)
            {
                str指导 += "4,";
            }
            if (checkEdit指导疾病预防.Checked)
            {
                str指导 += "99,";
            }
            if (checkEdit指导其他.Checked)
            {
                str指导 += "100,";
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.E_XSEZDQT] = textEdit指导其他.Text;
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.E_XSEZDQT] = DBNull.Value;
            }
            if (str指导.Length > 0)
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.指导] = str指导.Substring(0, str指导.Length - 1);
            }
            else
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.指导] = "";
            }

            //string str中医 = "";
            //if(checkEdit中医饮食.Checked)
            //{
            //    str中医 += "1,";
            //}
            //if(checkEdit中医起居.Checked)
            //{
            //    str中医 += "2,";
            //}
            //if(checkEdit传授摩捏.Checked)
            //{
            //    str中医 += "3,";
            //}
            //if(checkEdit中医其他.Checked)
            //{
            //    str中医 += "4,";
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理其他] = textEdit中医其他.Text;
            //}
            //if(string.IsNullOrWhiteSpace(str中医))
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理] = null;
            //}
            //else
            //{
            //    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.中医药管理] = str中医.Substring(0, str中医.Length - 1);
            //}

            #region 新版本添加（选择项和填空项数据保存）
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.健康管理联系人] = textEdit健康管理联系人.Text.Trim();
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系电话] = textEdit联系电话.Text.Trim();
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.所在幼儿园名称] = textEdit所在幼儿园名称.Text.Trim();
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构] = radio查体机构.EditValue;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.查体机构其他] = textEdit查体机构其他.Text.Trim();
            string str体重指数 = util.ControlsHelper.GetComboxKey(comboBoxEdit体重指数);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.体重指数] = str体重指数;
            string str胸部 = util.ControlsHelper.GetComboxKey(comboBoxEdit胸部);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.胸部] = str胸部;
            string str发育评估 = "";
            if (chk发育评估无.Checked)
            {
                m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估] = "1";
            }
            else
            {
                if (chk发育评估1.Checked)
                {
                    str发育评估 += "2,";
                }
                if (chk发育评估2.Checked)
                {
                    str发育评估 += "3,";
                }
                if (chk发育评估3.Checked)
                {
                    str发育评估 += "4,";
                }
                if (chk发育评估4.Checked)
                {
                    str发育评估 += "5,";
                }
                if (str发育评估.Length > 0)
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估] = str发育评估.Substring(0, str发育评估.Length - 1);
                }
                else
                {
                    m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.发育评估] = null;
                }
            }
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系人] = ucLblTxt联系人.Txt1.Text;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.联系方式] = ucLblTxt联系方式.Txt1.Text;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.结果] = util.ControlsHelper.GetComboxKey(comboBoxEdit是否到位);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.家长签名] = textEdit家长签名.Text;
            #endregion

            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.其他] = textEdit其他.Text;
            
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.随访医生] = textEdit随访医生.Text;
            
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改时间] = textEdit最近修改时间.Text;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.修改人] = Loginer.CurrentUser.用户编码;

            //this.Set考核项颜色_new(layoutControl1, null);
            this.CalQXandWZD(layoutControlGroup1);
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.缺项] = _base缺项;
            m_ds5岁.Tables[tb_儿童_健康检查_5岁.__TableName].Rows[0][tb_儿童_健康检查_5岁.完整度] = _base完整度;


            string strBirth = m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();
            if(string.IsNullOrWhiteSpace(strBirth))
            {
            }
            else
            {
                try
                {
                    //DateTime dt出生日期 = Convert.ToDateTime(strBirth);
                    //DateTime dt更新时间 = Convert.ToDateTime(textEdit最近修改时间.Text);
                    //int month = (dt更新时间.Year - dt出生日期.Year) * 12 + (dt更新时间.Month - dt出生日期.Month);

                    //if (month < 36 || month >=48)
                    int age = new bllCom().GetAgeByBirthDay(strBirth, textEdit最近修改时间.Text);
                    if(age<5 || age >=6)
                    {
                    }
                    else
                    {
                        m_ds5岁.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.下次随访时间] = dateEdit下次随访日期.Text;

                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表5岁] = _base缺项.ToString() + "," + _base完整度.ToString();
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.新生儿随访记录] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表满月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表8月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表12月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表18月] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁半] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3岁] = "";
                        m_ds5岁.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表4岁] = "";
                    }
                }
                catch(Exception ex)
                {
                    Msg.ShowError("5岁保存：" + ex.Message);
                    return;
                }
            }

            m_bll5岁.Save(m_ds5岁);
            //m_bll5岁.WriteLog();

            if(string.IsNullOrWhiteSpace(m_closeType))
            {
                UC儿童健康检查_5岁_显示 ctrl = new UC儿童健康检查_5岁_显示(m_grdabh);
                ctrl.Dock = DockStyle.Fill;
                ShowControl(ctrl);
            }
            else
            {
                Common.frm个人健康.DialogResult = DialogResult.OK;
            }
        }
        private void textEdit_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit textEdit = sender as DevExpress.XtraEditors.TextEdit;
            if(string.IsNullOrWhiteSpace(textEdit.Text))
            {
                return;
            }
            try
            {
                double dTemp = Convert.ToDouble(textEdit.Text);
            }
            catch
            {
                Msg.ShowInformation("请输入数字");
                textEdit.Text = "";
                textEdit.Focus();
            }
        }

        private void textEdit2_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit textEdit = sender as DevExpress.XtraEditors.TextEdit;
            if (string.IsNullOrWhiteSpace(textEdit.Text))
            {
                return;
            }
            try
            {
                double dTemp = Convert.ToDouble(textEdit.Text);
                if(dTemp >=0 && dTemp <=24)
                { }
                else
                {
                    Msg.ShowInformation("请输入0--24之间的整数或小数");
                    textEdit.Text = "";
                    textEdit.Focus();
                }
            }
            catch
            {
                Msg.ShowInformation("请输入数字");
                textEdit.Text = "";
                textEdit.Focus();
            }
        }

        private void textEdit100_Leave(object sender , EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit textEdit = sender as DevExpress.XtraEditors.TextEdit;
            if (string.IsNullOrWhiteSpace(textEdit.Text))
            {
                return;
            }
            try
            {
                double dTemp = Convert.ToInt32(textEdit.Text);
                if (dTemp >= 0 && dTemp < 100)
                { }
                else
                {
                    Msg.ShowInformation("请输入1位或2位的整数");
                    textEdit.Text = "";
                    textEdit.Focus();
                }
            }
            catch
            {
                Msg.ShowInformation("请输入1位或2位的整数");
                textEdit.Text = "";
                textEdit.Focus();
            }
        }

        //private void checkEdit中医其他_CheckedChanged(object sender, EventArgs e)
        //{
        //    if(checkEdit中医其他.Checked)
        //    {
        //        textEdit中医其他.Enabled = true;
        //    }
        //    else
        //    {
        //        textEdit中医其他.Enabled = true;
        //        textEdit中医其他.Text = "";
        //    }
        //}

        private void checkEdit患病无_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病无.Checked)
            {
                checkEdit患病肺炎.Checked = false;
                checkEdit患病腹泻.Checked = false;
                checkEdit患病外伤.Checked = false;
                checkEdit患病其他.Checked = false;
                checkEdit患病肺炎.Enabled = false;
                checkEdit患病腹泻.Enabled = false;
                checkEdit患病外伤.Enabled = false;
                checkEdit患病其他.Enabled = false;
            }
            else
            {
                checkEdit患病肺炎.Enabled = true;
                checkEdit患病腹泻.Enabled = true;
                checkEdit患病外伤.Enabled = true;
                checkEdit患病其他.Enabled = true;
            }
        }

        private void checkEdit患病肺炎_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病肺炎.Checked)
            {
                textEdit患病肺炎.Enabled = true;
            }
            else
            {
                textEdit患病肺炎.Enabled = false;
                textEdit患病肺炎.Text = "";
            }
        }

        private void checkEdit患病腹泻_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病腹泻.Checked)
            {
                textEdit患病腹泻.Enabled = true;
            }
            else
            {
                textEdit患病腹泻.Enabled = false;
                textEdit患病腹泻.Text = "";
            }
        }

        private void checkEdit患病外伤_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病外伤.Checked)
            {
                textEdit患病外伤.Enabled = true;
            }
            else
            {
                textEdit患病外伤.Enabled = false;
                textEdit患病外伤.Text = "";
            }
        }

        private void checkEdit患病其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit患病其他.Checked)
            {
                textEdit患病其他.Enabled = true;
            }
            else
            {
                textEdit患病其他.Enabled = false;
                textEdit患病其他.Text = "";
            }
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit指导其他.Checked)
            {
                textEdit指导其他.Enabled = true;
                textEdit指导其他.Text = "低盐饮食";
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        private void radio查体机构_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio查体机构, textEdit查体机构其他);
        }
        private void SetRadioGroupTxtEnable(RadioGroup radio, TextEdit txt)
        {
            string index = radio.EditValue == null ? "" : radio.EditValue.ToString();
            txt.Enabled = !string.IsNullOrEmpty(index) && index == "3" ? true : false;
            txt.Text = !string.IsNullOrEmpty(index) && index == "3" ? txt.Text : "";
        }

        private void chk发育评估无_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk发育评估无.Checked)
            {
                this.chk发育评估1.Enabled = this.chk发育评估2.Enabled = this.chk发育评估3.Enabled = this.chk发育评估4.Enabled = false;
                this.chk发育评估1.Checked = this.chk发育评估2.Checked = this.chk发育评估3.Checked = this.chk发育评估4.Checked = false;
            }
            else
            {
                this.chk发育评估1.Enabled = this.chk发育评估2.Enabled = this.chk发育评估3.Enabled = this.chk发育评估4.Enabled = true;
            }
        }
    }
}
 