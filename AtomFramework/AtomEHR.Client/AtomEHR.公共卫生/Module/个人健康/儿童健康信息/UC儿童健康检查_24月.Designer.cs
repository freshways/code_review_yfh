﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class UC儿童健康检查_24月
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.sbtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit家长签名 = new DevExpress.XtraEditors.TextEdit();
            this.flow患病情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk患病情况无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk患病情况1 = new DevExpress.XtraEditors.CheckEdit();
            this.uc肺炎 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk患病情况2 = new DevExpress.XtraEditors.CheckEdit();
            this.uc腹泻 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk患病情况3 = new DevExpress.XtraEditors.CheckEdit();
            this.uc外伤 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.chk患病情况99 = new DevExpress.XtraEditors.CheckEdit();
            this.uc患病情况其他 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk发育评估无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk发育评估4 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxEdit胸部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit听力 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucTxtLblTxtLbl出牙龋齿 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit中医饮食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit中医起居 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit传授摩捏 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit中医其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit中医其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit儿童档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit指导合理膳食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导预防意外1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导生长发育 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导口腔保健 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导疾病预防 = new DevExpress.XtraEditors.CheckEdit();
            this.chk口服维生素D = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxEdit是否到位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucLblTxt联系方式 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ucLblTxt转诊机构 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.comboBoxEdit转诊 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit面色红润 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit面色其他 = new DevExpress.XtraEditors.CheckEdit();
            this.ucTxtLbl服用维生素 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl户外活动 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit可疑佝偻 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit四肢 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit腹部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit心肺 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit耳外观 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucTxtLbl体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit体重 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit眼外观 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucTxtLbl身长 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.comboBoxEdit身长 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit前囟 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucTxtLblTxtLbl前囟 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.comboBoxEdit步态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit皮肤 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci身长 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciEye = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciEar = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciSfys = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciQt = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciZz = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFace = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci皮肤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci前囟 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lci出牙龋齿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciHeart = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciBelly = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFours = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGlbtz = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciHwhd = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciZd = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFywssd = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciListening = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciBt = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl胸部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl发育评估 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl患病情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家长签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciXcsfrq = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSfrq = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).BeginInit();
            this.flow患病情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况99.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit听力.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医饮食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医起居.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit传授摩捏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit中医其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导合理膳食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk口服维生素D.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否到位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色红润.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit可疑佝偻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit四肢.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit心肺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit耳外观.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit眼外观.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit身长.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit前囟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit步态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit皮肤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci身长)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfys)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci皮肤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci前囟)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci出牙龋齿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHeart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBelly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGlbtz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHwhd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFywssd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciListening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl发育评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl患病情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家长签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXcsfrq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfrq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.sbtnSave);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // sbtnSave
            // 
            this.sbtnSave.Location = new System.Drawing.Point(3, 3);
            this.sbtnSave.Name = "sbtnSave";
            this.sbtnSave.Size = new System.Drawing.Size(75, 23);
            this.sbtnSave.TabIndex = 0;
            this.sbtnSave.Text = "保存";
            this.sbtnSave.Click += new System.EventHandler(this.sbtnSave_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(84, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "填表说明";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 32);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(750, 3);
            this.panelControl2.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.layoutControl1.Controls.Add(this.textEdit家长签名);
            this.layoutControl1.Controls.Add(this.flow患病情况);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.comboBoxEdit胸部);
            this.layoutControl1.Controls.Add(this.comboBoxEdit听力);
            this.layoutControl1.Controls.Add(this.ucTxtLblTxtLbl出牙龋齿);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit录入人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit最近修改时间);
            this.layoutControl1.Controls.Add(this.textEdit录入时间);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit儿童姓名);
            this.layoutControl1.Controls.Add(this.textEdit儿童档案号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.dateEdit随访日期);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.dateEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit其他);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.panel1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.ucTxtLbl服用维生素);
            this.layoutControl1.Controls.Add(this.ucTxtLbl户外活动);
            this.layoutControl1.Controls.Add(this.comboBoxEdit可疑佝偻);
            this.layoutControl1.Controls.Add(this.comboBoxEdit四肢);
            this.layoutControl1.Controls.Add(this.comboBoxEdit腹部);
            this.layoutControl1.Controls.Add(this.comboBoxEdit心肺);
            this.layoutControl1.Controls.Add(this.comboBoxEdit耳外观);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.comboBoxEdit眼外观);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.comboBoxEdit步态);
            this.layoutControl1.Controls.Add(this.comboBoxEdit皮肤);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 35);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.Teal;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 500);
            this.layoutControl1.TabIndex = 4;
            // 
            // textEdit家长签名
            // 
            this.textEdit家长签名.Location = new System.Drawing.Point(501, 418);
            this.textEdit家长签名.Name = "textEdit家长签名";
            this.textEdit家长签名.Size = new System.Drawing.Size(228, 20);
            this.textEdit家长签名.StyleController = this.layoutControl1;
            this.textEdit家长签名.TabIndex = 32;
            // 
            // flow患病情况
            // 
            this.flow患病情况.Controls.Add(this.chk患病情况无);
            this.flow患病情况.Controls.Add(this.chk患病情况1);
            this.flow患病情况.Controls.Add(this.uc肺炎);
            this.flow患病情况.Controls.Add(this.chk患病情况2);
            this.flow患病情况.Controls.Add(this.uc腹泻);
            this.flow患病情况.Controls.Add(this.chk患病情况3);
            this.flow患病情况.Controls.Add(this.uc外伤);
            this.flow患病情况.Controls.Add(this.chk患病情况99);
            this.flow患病情况.Controls.Add(this.uc患病情况其他);
            this.flow患病情况.Location = new System.Drawing.Point(137, 120);
            this.flow患病情况.Name = "flow患病情况";
            this.flow患病情况.Size = new System.Drawing.Size(592, 56);
            this.flow患病情况.TabIndex = 24;
            // 
            // chk患病情况无
            // 
            this.chk患病情况无.Location = new System.Drawing.Point(3, 3);
            this.chk患病情况无.Name = "chk患病情况无";
            this.chk患病情况无.Properties.Caption = "无";
            this.chk患病情况无.Size = new System.Drawing.Size(49, 19);
            this.chk患病情况无.TabIndex = 0;
            this.chk患病情况无.Tag = "1";
            this.chk患病情况无.CheckedChanged += new System.EventHandler(this.chk患病情况无_CheckedChanged);
            // 
            // chk患病情况1
            // 
            this.chk患病情况1.Location = new System.Drawing.Point(58, 3);
            this.chk患病情况1.Name = "chk患病情况1";
            this.chk患病情况1.Properties.Caption = "";
            this.chk患病情况1.Size = new System.Drawing.Size(21, 19);
            this.chk患病情况1.TabIndex = 1;
            this.chk患病情况1.Tag = "2";
            this.chk患病情况1.CheckedChanged += new System.EventHandler(this.chk患病情况1_CheckedChanged);
            // 
            // uc肺炎
            // 
            this.uc肺炎.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc肺炎.Lbl1Text = "肺炎：";
            this.uc肺炎.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc肺炎.Lbl2Text = "次";
            this.uc肺炎.Location = new System.Drawing.Point(82, 3);
            this.uc肺炎.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.uc肺炎.Name = "uc肺炎";
            this.uc肺炎.Size = new System.Drawing.Size(128, 22);
            this.uc肺炎.TabIndex = 5;
            this.uc肺炎.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // chk患病情况2
            // 
            this.chk患病情况2.Location = new System.Drawing.Point(213, 3);
            this.chk患病情况2.Name = "chk患病情况2";
            this.chk患病情况2.Properties.Caption = "";
            this.chk患病情况2.Size = new System.Drawing.Size(22, 19);
            this.chk患病情况2.TabIndex = 2;
            this.chk患病情况2.Tag = "3";
            this.chk患病情况2.CheckedChanged += new System.EventHandler(this.chk患病情况2_CheckedChanged);
            // 
            // uc腹泻
            // 
            this.uc腹泻.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc腹泻.Lbl1Text = "腹泻：";
            this.uc腹泻.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc腹泻.Lbl2Text = "次";
            this.uc腹泻.Location = new System.Drawing.Point(238, 3);
            this.uc腹泻.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.uc腹泻.Name = "uc腹泻";
            this.uc腹泻.Size = new System.Drawing.Size(133, 22);
            this.uc腹泻.TabIndex = 6;
            this.uc腹泻.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // chk患病情况3
            // 
            this.chk患病情况3.Location = new System.Drawing.Point(374, 3);
            this.chk患病情况3.Name = "chk患病情况3";
            this.chk患病情况3.Properties.Caption = "";
            this.chk患病情况3.Size = new System.Drawing.Size(23, 19);
            this.chk患病情况3.TabIndex = 3;
            this.chk患病情况3.Tag = "4";
            this.chk患病情况3.CheckedChanged += new System.EventHandler(this.chk患病情况3_CheckedChanged);
            // 
            // uc外伤
            // 
            this.uc外伤.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc外伤.Lbl1Text = "外伤：";
            this.uc外伤.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc外伤.Lbl2Text = "次";
            this.uc外伤.Location = new System.Drawing.Point(400, 3);
            this.uc外伤.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.uc外伤.Name = "uc外伤";
            this.uc外伤.Size = new System.Drawing.Size(143, 22);
            this.uc外伤.TabIndex = 7;
            this.uc外伤.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // chk患病情况99
            // 
            this.chk患病情况99.Location = new System.Drawing.Point(546, 3);
            this.chk患病情况99.Name = "chk患病情况99";
            this.chk患病情况99.Properties.Caption = "";
            this.chk患病情况99.Size = new System.Drawing.Size(20, 19);
            this.chk患病情况99.TabIndex = 4;
            this.chk患病情况99.Tag = "99";
            this.chk患病情况99.CheckedChanged += new System.EventHandler(this.chk患病情况99_CheckedChanged);
            // 
            // uc患病情况其他
            // 
            this.uc患病情况其他.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc患病情况其他.Lbl1Text = "其他：";
            this.uc患病情况其他.Location = new System.Drawing.Point(1, 26);
            this.uc患病情况其他.Margin = new System.Windows.Forms.Padding(1);
            this.uc患病情况其他.Name = "uc患病情况其他";
            this.uc患病情况其他.Size = new System.Drawing.Size(142, 22);
            this.uc患病情况其他.TabIndex = 8;
            this.uc患病情况其他.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.chk发育评估无);
            this.flowLayoutPanel8.Controls.Add(this.chk发育评估1);
            this.flowLayoutPanel8.Controls.Add(this.chk发育评估2);
            this.flowLayoutPanel8.Controls.Add(this.chk发育评估3);
            this.flowLayoutPanel8.Controls.Add(this.chk发育评估4);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(137, 60);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(592, 56);
            this.flowLayoutPanel8.TabIndex = 23;
            // 
            // chk发育评估无
            // 
            this.chk发育评估无.Location = new System.Drawing.Point(3, 3);
            this.chk发育评估无.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.chk发育评估无.Name = "chk发育评估无";
            this.chk发育评估无.Properties.Caption = "无";
            this.chk发育评估无.Size = new System.Drawing.Size(131, 19);
            this.chk发育评估无.TabIndex = 0;
            this.chk发育评估无.CheckedChanged += new System.EventHandler(this.chk发育评估无_CheckedChanged);
            // 
            // chk发育评估1
            // 
            this.chk发育评估1.Location = new System.Drawing.Point(137, 3);
            this.chk发育评估1.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.chk发育评估1.Name = "chk发育评估1";
            this.chk发育评估1.Properties.Caption = "不会说3个物品的名称";
            this.chk发育评估1.Size = new System.Drawing.Size(197, 19);
            this.chk发育评估1.TabIndex = 1;
            // 
            // chk发育评估2
            // 
            this.chk发育评估2.Location = new System.Drawing.Point(334, 3);
            this.chk发育评估2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.chk发育评估2.Name = "chk发育评估2";
            this.chk发育评估2.Properties.Caption = "不会按吩咐做简单事情";
            this.chk发育评估2.Size = new System.Drawing.Size(174, 19);
            this.chk发育评估2.TabIndex = 2;
            // 
            // chk发育评估3
            // 
            this.chk发育评估3.Location = new System.Drawing.Point(0, 28);
            this.chk发育评估3.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.chk发育评估3.Name = "chk发育评估3";
            this.chk发育评估3.Properties.Caption = "不会用勺吃饭";
            this.chk发育评估3.Size = new System.Drawing.Size(134, 19);
            this.chk发育评估3.TabIndex = 3;
            // 
            // chk发育评估4
            // 
            this.chk发育评估4.Location = new System.Drawing.Point(134, 28);
            this.chk发育评估4.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.chk发育评估4.Name = "chk发育评估4";
            this.chk发育评估4.Properties.Caption = "不会扶栏上楼/台";
            this.chk发育评估4.Size = new System.Drawing.Size(114, 19);
            this.chk发育评估4.TabIndex = 4;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(368, 36);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(361, 20);
            this.layoutControl2.TabIndex = 38;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(361, 20);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // comboBoxEdit胸部
            // 
            this.comboBoxEdit胸部.Location = new System.Drawing.Point(137, 36);
            this.comboBoxEdit胸部.Name = "comboBoxEdit胸部";
            this.comboBoxEdit胸部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit胸部.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEdit胸部.StyleController = this.layoutControl1;
            this.comboBoxEdit胸部.TabIndex = 22;
            // 
            // comboBoxEdit听力
            // 
            this.comboBoxEdit听力.Location = new System.Drawing.Point(137, -12);
            this.comboBoxEdit听力.Name = "comboBoxEdit听力";
            this.comboBoxEdit听力.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit听力.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit听力.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEdit听力.StyleController = this.layoutControl1;
            this.comboBoxEdit听力.TabIndex = 18;
            // 
            // ucTxtLblTxtLbl出牙龋齿
            // 
            this.ucTxtLblTxtLbl出牙龋齿.Lbl1Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl出牙龋齿.Lbl1Text = "/";
            this.ucTxtLblTxtLbl出牙龋齿.Lbl2Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl出牙龋齿.Lbl2Text = "颗";
            this.ucTxtLblTxtLbl出牙龋齿.Location = new System.Drawing.Point(501, -84);
            this.ucTxtLblTxtLbl出牙龋齿.Name = "ucTxtLblTxtLbl出牙龋齿";
            this.ucTxtLblTxtLbl出牙龋齿.Size = new System.Drawing.Size(228, 20);
            this.ucTxtLblTxtLbl出牙龋齿.TabIndex = 13;
            this.ucTxtLblTxtLbl出牙龋齿.Txt1EditValue = null;
            this.ucTxtLblTxtLbl出牙龋齿.Txt1Size = new System.Drawing.Size(50, 20);
            this.ucTxtLblTxtLbl出牙龋齿.Txt2EditValue = null;
            this.ucTxtLblTxtLbl出牙龋齿.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.checkEdit中医饮食);
            this.flowLayoutPanel7.Controls.Add(this.checkEdit中医起居);
            this.flowLayoutPanel7.Controls.Add(this.checkEdit传授摩捏);
            this.flowLayoutPanel7.Controls.Add(this.checkEdit中医其他);
            this.flowLayoutPanel7.Controls.Add(this.textEdit中医其他);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(137, 322);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(592, 44);
            this.flowLayoutPanel7.TabIndex = 27;
            // 
            // checkEdit中医饮食
            // 
            this.checkEdit中医饮食.Location = new System.Drawing.Point(0, 0);
            this.checkEdit中医饮食.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit中医饮食.Name = "checkEdit中医饮食";
            this.checkEdit中医饮食.Properties.Caption = "中医饮食调养指导";
            this.checkEdit中医饮食.Size = new System.Drawing.Size(162, 19);
            this.checkEdit中医饮食.TabIndex = 0;
            // 
            // checkEdit中医起居
            // 
            this.checkEdit中医起居.Location = new System.Drawing.Point(162, 0);
            this.checkEdit中医起居.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit中医起居.Name = "checkEdit中医起居";
            this.checkEdit中医起居.Properties.Caption = " 中医起居调摄指导 ";
            this.checkEdit中医起居.Size = new System.Drawing.Size(168, 19);
            this.checkEdit中医起居.TabIndex = 1;
            // 
            // checkEdit传授摩捏
            // 
            this.checkEdit传授摩捏.Location = new System.Drawing.Point(330, 0);
            this.checkEdit传授摩捏.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit传授摩捏.Name = "checkEdit传授摩捏";
            this.checkEdit传授摩捏.Properties.Caption = "传授按揉迎香穴、足三里穴方法";
            this.checkEdit传授摩捏.Size = new System.Drawing.Size(252, 19);
            this.checkEdit传授摩捏.TabIndex = 2;
            // 
            // checkEdit中医其他
            // 
            this.checkEdit中医其他.Location = new System.Drawing.Point(0, 19);
            this.checkEdit中医其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit中医其他.Name = "checkEdit中医其他";
            this.checkEdit中医其他.Properties.Caption = "其他";
            this.checkEdit中医其他.Size = new System.Drawing.Size(52, 19);
            this.checkEdit中医其他.TabIndex = 3;
            this.checkEdit中医其他.CheckedChanged += new System.EventHandler(this.checkEdit中医其他_CheckedChanged);
            // 
            // textEdit中医其他
            // 
            this.textEdit中医其他.Enabled = false;
            this.textEdit中医其他.Location = new System.Drawing.Point(52, 19);
            this.textEdit中医其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit中医其他.Name = "textEdit中医其他";
            this.textEdit中医其他.Size = new System.Drawing.Size(110, 20);
            this.textEdit中医其他.TabIndex = 4;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(554, 466);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(175, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 36;
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(356, 466);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(99, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 35;
            // 
            // textEdit录入人
            // 
            this.textEdit录入人.Location = new System.Drawing.Point(356, 442);
            this.textEdit录入人.Name = "textEdit录入人";
            this.textEdit录入人.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入人.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入人.Properties.ReadOnly = true;
            this.textEdit录入人.Size = new System.Drawing.Size(99, 20);
            this.textEdit录入人.StyleController = this.layoutControl1;
            this.textEdit录入人.TabIndex = 34;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(554, 442);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(175, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 33;
            // 
            // textEdit最近修改时间
            // 
            this.textEdit最近修改时间.Location = new System.Drawing.Point(139, 466);
            this.textEdit最近修改时间.Name = "textEdit最近修改时间";
            this.textEdit最近修改时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit最近修改时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit最近修改时间.Properties.ReadOnly = true;
            this.textEdit最近修改时间.Size = new System.Drawing.Size(118, 20);
            this.textEdit最近修改时间.StyleController = this.layoutControl1;
            this.textEdit最近修改时间.TabIndex = 32;
            // 
            // textEdit录入时间
            // 
            this.textEdit录入时间.Location = new System.Drawing.Point(139, 442);
            this.textEdit录入时间.Name = "textEdit录入时间";
            this.textEdit录入时间.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit录入时间.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit录入时间.Properties.ReadOnly = true;
            this.textEdit录入时间.Size = new System.Drawing.Size(118, 20);
            this.textEdit录入时间.StyleController = this.layoutControl1;
            this.textEdit录入时间.TabIndex = 31;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(473, -180);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(256, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 5;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(137, -180);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit性别.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(199, 20);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 4;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(473, -204);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(256, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 3;
            // 
            // textEdit儿童姓名
            // 
            this.textEdit儿童姓名.Location = new System.Drawing.Point(137, -204);
            this.textEdit儿童姓名.Name = "textEdit儿童姓名";
            this.textEdit儿童姓名.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童姓名.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童姓名.Properties.ReadOnly = true;
            this.textEdit儿童姓名.Size = new System.Drawing.Size(199, 20);
            this.textEdit儿童姓名.StyleController = this.layoutControl1;
            this.textEdit儿童姓名.TabIndex = 2;
            // 
            // textEdit儿童档案号
            // 
            this.textEdit儿童档案号.Location = new System.Drawing.Point(473, -228);
            this.textEdit儿童档案号.Name = "textEdit儿童档案号";
            this.textEdit儿童档案号.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
            this.textEdit儿童档案号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit儿童档案号.Properties.ReadOnly = true;
            this.textEdit儿童档案号.Size = new System.Drawing.Size(256, 20);
            this.textEdit儿童档案号.StyleController = this.layoutControl1;
            this.textEdit儿童档案号.TabIndex = 1;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(137, -228);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit卡号.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit卡号.Size = new System.Drawing.Size(199, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 0;
            // 
            // dateEdit随访日期
            // 
            this.dateEdit随访日期.EditValue = null;
            this.dateEdit随访日期.Location = new System.Drawing.Point(139, 394);
            this.dateEdit随访日期.Margin = new System.Windows.Forms.Padding(0);
            this.dateEdit随访日期.Name = "dateEdit随访日期";
            this.dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit随访日期.Size = new System.Drawing.Size(225, 20);
            this.dateEdit随访日期.StyleController = this.layoutControl1;
            this.dateEdit随访日期.TabIndex = 29;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(501, 394);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(228, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 30;
            // 
            // dateEdit下次随访日期
            // 
            this.dateEdit下次随访日期.EditValue = null;
            this.dateEdit下次随访日期.Location = new System.Drawing.Point(137, 418);
            this.dateEdit下次随访日期.Name = "dateEdit下次随访日期";
            this.dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit下次随访日期.Size = new System.Drawing.Size(227, 20);
            this.dateEdit下次随访日期.StyleController = this.layoutControl1;
            this.dateEdit下次随访日期.TabIndex = 31;
            // 
            // textEdit其他
            // 
            this.textEdit其他.EditValue = "无";
            this.textEdit其他.Location = new System.Drawing.Point(137, 370);
            this.textEdit其他.Margin = new System.Windows.Forms.Padding(3, 22, 3, 3);
            this.textEdit其他.Name = "textEdit其他";
            this.textEdit其他.Size = new System.Drawing.Size(592, 20);
            this.textEdit其他.StyleController = this.layoutControl1;
            this.textEdit其他.TabIndex = 28;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导合理膳食);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导预防意外1);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导生长发育);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导口腔保健);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导疾病预防);
            this.flowLayoutPanel6.Controls.Add(this.chk口服维生素D);
            this.flowLayoutPanel6.Controls.Add(this.checkEdit指导其他);
            this.flowLayoutPanel6.Controls.Add(this.textEdit指导其他);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(137, 180);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(592, 56);
            this.flowLayoutPanel6.TabIndex = 25;
            // 
            // checkEdit指导合理膳食
            // 
            this.checkEdit指导合理膳食.Location = new System.Drawing.Point(3, 3);
            this.checkEdit指导合理膳食.Name = "checkEdit指导合理膳食";
            this.checkEdit指导合理膳食.Properties.Caption = "合理膳食";
            this.checkEdit指导合理膳食.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导合理膳食.TabIndex = 0;
            // 
            // checkEdit指导预防意外1
            // 
            this.checkEdit指导预防意外1.Location = new System.Drawing.Point(85, 3);
            this.checkEdit指导预防意外1.Name = "checkEdit指导预防意外1";
            this.checkEdit指导预防意外1.Properties.Caption = "预防意外伤害";
            this.checkEdit指导预防意外1.Size = new System.Drawing.Size(103, 19);
            this.checkEdit指导预防意外1.TabIndex = 1;
            // 
            // checkEdit指导生长发育
            // 
            this.checkEdit指导生长发育.Location = new System.Drawing.Point(194, 3);
            this.checkEdit指导生长发育.Name = "checkEdit指导生长发育";
            this.checkEdit指导生长发育.Properties.Caption = "生长发育";
            this.checkEdit指导生长发育.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导生长发育.TabIndex = 2;
            // 
            // checkEdit指导口腔保健
            // 
            this.checkEdit指导口腔保健.Location = new System.Drawing.Point(276, 3);
            this.checkEdit指导口腔保健.Name = "checkEdit指导口腔保健";
            this.checkEdit指导口腔保健.Properties.Caption = "口腔保健";
            this.checkEdit指导口腔保健.Size = new System.Drawing.Size(95, 19);
            this.checkEdit指导口腔保健.TabIndex = 3;
            // 
            // checkEdit指导疾病预防
            // 
            this.checkEdit指导疾病预防.Location = new System.Drawing.Point(377, 3);
            this.checkEdit指导疾病预防.Name = "checkEdit指导疾病预防";
            this.checkEdit指导疾病预防.Properties.Caption = "疾病预防";
            this.checkEdit指导疾病预防.Size = new System.Drawing.Size(76, 19);
            this.checkEdit指导疾病预防.TabIndex = 4;
            // 
            // chk口服维生素D
            // 
            this.chk口服维生素D.Location = new System.Drawing.Point(459, 3);
            this.chk口服维生素D.Name = "chk口服维生素D";
            this.chk口服维生素D.Properties.Caption = "口服维生素D";
            this.chk口服维生素D.Size = new System.Drawing.Size(100, 19);
            this.chk口服维生素D.TabIndex = 24;
            // 
            // checkEdit指导其他
            // 
            this.checkEdit指导其他.Location = new System.Drawing.Point(3, 28);
            this.checkEdit指导其他.Name = "checkEdit指导其他";
            this.checkEdit指导其他.Properties.Caption = "其他";
            this.checkEdit指导其他.Size = new System.Drawing.Size(53, 19);
            this.checkEdit指导其他.TabIndex = 9;
            this.checkEdit指导其他.CheckedChanged += new System.EventHandler(this.checkEdit指导其他_CheckedChanged);
            // 
            // textEdit指导其他
            // 
            this.textEdit指导其他.Enabled = false;
            this.textEdit指导其他.Location = new System.Drawing.Point(62, 28);
            this.textEdit指导其他.Name = "textEdit指导其他";
            this.textEdit指导其他.Size = new System.Drawing.Size(188, 20);
            this.textEdit指导其他.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBoxEdit是否到位);
            this.panel1.Controls.Add(this.ucLblTxt联系方式);
            this.panel1.Controls.Add(this.ucLblTxt联系人);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ucLblTxt转诊机构);
            this.panel1.Controls.Add(this.ucLblTxt转诊原因);
            this.panel1.Controls.Add(this.comboBoxEdit转诊);
            this.panel1.Location = new System.Drawing.Point(137, 240);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(592, 78);
            this.panel1.TabIndex = 26;
            // 
            // comboBoxEdit是否到位
            // 
            this.comboBoxEdit是否到位.Enabled = false;
            this.comboBoxEdit是否到位.Location = new System.Drawing.Point(426, 43);
            this.comboBoxEdit是否到位.Name = "comboBoxEdit是否到位";
            this.comboBoxEdit是否到位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否到位.Size = new System.Drawing.Size(59, 20);
            this.comboBoxEdit是否到位.TabIndex = 7;
            // 
            // ucLblTxt联系方式
            // 
            this.ucLblTxt联系方式.Enabled = false;
            this.ucLblTxt联系方式.Lbl1Size = new System.Drawing.Size(65, 18);
            this.ucLblTxt联系方式.Lbl1Text = "联系方式：";
            this.ucLblTxt联系方式.Location = new System.Drawing.Point(150, 43);
            this.ucLblTxt联系方式.Name = "ucLblTxt联系方式";
            this.ucLblTxt联系方式.Size = new System.Drawing.Size(188, 22);
            this.ucLblTxt联系方式.TabIndex = 6;
            this.ucLblTxt联系方式.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // ucLblTxt联系人
            // 
            this.ucLblTxt联系人.Enabled = false;
            this.ucLblTxt联系人.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt联系人.Lbl1Text = "联系人：";
            this.ucLblTxt联系人.Location = new System.Drawing.Point(11, 44);
            this.ucLblTxt联系人.Name = "ucLblTxt联系人";
            this.ucLblTxt联系人.Size = new System.Drawing.Size(132, 22);
            this.ucLblTxt联系人.TabIndex = 5;
            this.ucLblTxt联系人.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(379, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "结果：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "有无转诊：";
            // 
            // ucLblTxt转诊机构
            // 
            this.ucLblTxt转诊机构.Enabled = false;
            this.ucLblTxt转诊机构.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt转诊机构.Lbl1Text = "机构及科室：";
            this.ucLblTxt转诊机构.Location = new System.Drawing.Point(354, 8);
            this.ucLblTxt转诊机构.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt转诊机构.Name = "ucLblTxt转诊机构";
            this.ucLblTxt转诊机构.Size = new System.Drawing.Size(192, 22);
            this.ucLblTxt转诊机构.TabIndex = 2;
            this.ucLblTxt转诊机构.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // ucLblTxt转诊原因
            // 
            this.ucLblTxt转诊原因.Enabled = false;
            this.ucLblTxt转诊原因.Lbl1Size = new System.Drawing.Size(65, 18);
            this.ucLblTxt转诊原因.Lbl1Text = "原  因：";
            this.ucLblTxt转诊原因.Location = new System.Drawing.Point(150, 8);
            this.ucLblTxt转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt转诊原因.Name = "ucLblTxt转诊原因";
            this.ucLblTxt转诊原因.Size = new System.Drawing.Size(192, 22);
            this.ucLblTxt转诊原因.TabIndex = 1;
            this.ucLblTxt转诊原因.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // comboBoxEdit转诊
            // 
            this.comboBoxEdit转诊.Location = new System.Drawing.Point(79, 8);
            this.comboBoxEdit转诊.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit转诊.Name = "comboBoxEdit转诊";
            this.comboBoxEdit转诊.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit转诊.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit转诊.Size = new System.Drawing.Size(64, 20);
            this.comboBoxEdit转诊.TabIndex = 0;
            this.comboBoxEdit转诊.TextChanged += new System.EventHandler(this.comboBoxEdit转诊_TextChanged);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.checkEdit面色红润);
            this.flowLayoutPanel5.Controls.Add(this.checkEdit面色其他);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(137, -132);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(227, 20);
            this.flowLayoutPanel5.TabIndex = 8;
            // 
            // checkEdit面色红润
            // 
            this.checkEdit面色红润.Location = new System.Drawing.Point(0, 0);
            this.checkEdit面色红润.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色红润.Name = "checkEdit面色红润";
            this.checkEdit面色红润.Properties.Caption = "红润";
            this.checkEdit面色红润.Size = new System.Drawing.Size(45, 19);
            this.checkEdit面色红润.TabIndex = 1;
            // 
            // checkEdit面色其他
            // 
            this.checkEdit面色其他.Location = new System.Drawing.Point(45, 0);
            this.checkEdit面色其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit面色其他.Name = "checkEdit面色其他";
            this.checkEdit面色其他.Properties.Caption = "其他";
            this.checkEdit面色其他.Size = new System.Drawing.Size(49, 19);
            this.checkEdit面色其他.TabIndex = 3;
            // 
            // ucTxtLbl服用维生素
            // 
            this.ucTxtLbl服用维生素.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl服用维生素.Lbl1Text = "IU/日";
            this.ucTxtLbl服用维生素.Location = new System.Drawing.Point(501, 12);
            this.ucTxtLbl服用维生素.Name = "ucTxtLbl服用维生素";
            this.ucTxtLbl服用维生素.Size = new System.Drawing.Size(228, 20);
            this.ucTxtLbl服用维生素.TabIndex = 21;
            this.ucTxtLbl服用维生素.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl户外活动
            // 
            this.ucTxtLbl户外活动.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl户外活动.Lbl1Text = "小时/日 ";
            this.ucTxtLbl户外活动.Location = new System.Drawing.Point(137, 12);
            this.ucTxtLbl户外活动.Name = "ucTxtLbl户外活动";
            this.ucTxtLbl户外活动.Size = new System.Drawing.Size(227, 20);
            this.ucTxtLbl户外活动.TabIndex = 20;
            this.ucTxtLbl户外活动.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // comboBoxEdit可疑佝偻
            // 
            this.comboBoxEdit可疑佝偻.Location = new System.Drawing.Point(501, -36);
            this.comboBoxEdit可疑佝偻.Name = "comboBoxEdit可疑佝偻";
            this.comboBoxEdit可疑佝偻.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit可疑佝偻.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit可疑佝偻.Size = new System.Drawing.Size(228, 20);
            this.comboBoxEdit可疑佝偻.StyleController = this.layoutControl1;
            this.comboBoxEdit可疑佝偻.TabIndex = 17;
            // 
            // comboBoxEdit四肢
            // 
            this.comboBoxEdit四肢.Location = new System.Drawing.Point(137, -36);
            this.comboBoxEdit四肢.Name = "comboBoxEdit四肢";
            this.comboBoxEdit四肢.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit四肢.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit四肢.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEdit四肢.StyleController = this.layoutControl1;
            this.comboBoxEdit四肢.TabIndex = 16;
            // 
            // comboBoxEdit腹部
            // 
            this.comboBoxEdit腹部.Location = new System.Drawing.Point(501, -60);
            this.comboBoxEdit腹部.Name = "comboBoxEdit腹部";
            this.comboBoxEdit腹部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit腹部.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit腹部.Size = new System.Drawing.Size(228, 20);
            this.comboBoxEdit腹部.StyleController = this.layoutControl1;
            this.comboBoxEdit腹部.TabIndex = 15;
            // 
            // comboBoxEdit心肺
            // 
            this.comboBoxEdit心肺.Location = new System.Drawing.Point(137, -60);
            this.comboBoxEdit心肺.Name = "comboBoxEdit心肺";
            this.comboBoxEdit心肺.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit心肺.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit心肺.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEdit心肺.StyleController = this.layoutControl1;
            this.comboBoxEdit心肺.TabIndex = 14;
            // 
            // comboBoxEdit耳外观
            // 
            this.comboBoxEdit耳外观.Location = new System.Drawing.Point(137, -84);
            this.comboBoxEdit耳外观.Name = "comboBoxEdit耳外观";
            this.comboBoxEdit耳外观.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit耳外观.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit耳外观.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEdit耳外观.StyleController = this.layoutControl1;
            this.comboBoxEdit耳外观.TabIndex = 12;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.ucTxtLbl体重);
            this.flowLayoutPanel2.Controls.Add(this.comboBoxEdit体重);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(137, -156);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(227, 20);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // ucTxtLbl体重
            // 
            this.ucTxtLbl体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl体重.Lbl1Text = "kg";
            this.ucTxtLbl体重.Location = new System.Drawing.Point(0, 0);
            this.ucTxtLbl体重.Margin = new System.Windows.Forms.Padding(0);
            this.ucTxtLbl体重.Name = "ucTxtLbl体重";
            this.ucTxtLbl体重.Size = new System.Drawing.Size(107, 22);
            this.ucTxtLbl体重.TabIndex = 0;
            this.ucTxtLbl体重.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // comboBoxEdit体重
            // 
            this.comboBoxEdit体重.Location = new System.Drawing.Point(107, 0);
            this.comboBoxEdit体重.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit体重.Name = "comboBoxEdit体重";
            this.comboBoxEdit体重.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit体重.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit体重.Size = new System.Drawing.Size(55, 20);
            this.comboBoxEdit体重.TabIndex = 1;
            // 
            // comboBoxEdit眼外观
            // 
            this.comboBoxEdit眼外观.Location = new System.Drawing.Point(501, -108);
            this.comboBoxEdit眼外观.Name = "comboBoxEdit眼外观";
            this.comboBoxEdit眼外观.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit眼外观.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit眼外观.Size = new System.Drawing.Size(228, 20);
            this.comboBoxEdit眼外观.StyleController = this.layoutControl1;
            this.comboBoxEdit眼外观.TabIndex = 11;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.ucTxtLbl身长);
            this.flowLayoutPanel3.Controls.Add(this.comboBoxEdit身长);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(501, -156);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(228, 20);
            this.flowLayoutPanel3.TabIndex = 7;
            // 
            // ucTxtLbl身长
            // 
            this.ucTxtLbl身长.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl身长.Lbl1Text = "cm";
            this.ucTxtLbl身长.Location = new System.Drawing.Point(0, 0);
            this.ucTxtLbl身长.Margin = new System.Windows.Forms.Padding(0);
            this.ucTxtLbl身长.Name = "ucTxtLbl身长";
            this.ucTxtLbl身长.Size = new System.Drawing.Size(117, 22);
            this.ucTxtLbl身长.TabIndex = 0;
            this.ucTxtLbl身长.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // comboBoxEdit身长
            // 
            this.comboBoxEdit身长.Location = new System.Drawing.Point(117, 0);
            this.comboBoxEdit身长.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit身长.Name = "comboBoxEdit身长";
            this.comboBoxEdit身长.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit身长.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit身长.Size = new System.Drawing.Size(55, 20);
            this.comboBoxEdit身长.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.comboBoxEdit前囟);
            this.flowLayoutPanel4.Controls.Add(this.ucTxtLblTxtLbl前囟);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(137, -108);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(227, 20);
            this.flowLayoutPanel4.TabIndex = 10;
            // 
            // comboBoxEdit前囟
            // 
            this.comboBoxEdit前囟.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit前囟.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit前囟.Name = "comboBoxEdit前囟";
            this.comboBoxEdit前囟.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit前囟.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit前囟.Size = new System.Drawing.Size(49, 20);
            this.comboBoxEdit前囟.TabIndex = 0;
            this.comboBoxEdit前囟.TextChanged += new System.EventHandler(this.comboBoxEdit前囟_TextChanged);
            // 
            // ucTxtLblTxtLbl前囟
            // 
            this.ucTxtLblTxtLbl前囟.Enabled = false;
            this.ucTxtLblTxtLbl前囟.Lbl1Size = new System.Drawing.Size(25, 14);
            this.ucTxtLblTxtLbl前囟.Lbl1Text = "cm*";
            this.ucTxtLblTxtLbl前囟.Lbl2Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl前囟.Lbl2Text = "cm";
            this.ucTxtLblTxtLbl前囟.Location = new System.Drawing.Point(49, 0);
            this.ucTxtLblTxtLbl前囟.Margin = new System.Windows.Forms.Padding(0);
            this.ucTxtLblTxtLbl前囟.Name = "ucTxtLblTxtLbl前囟";
            this.ucTxtLblTxtLbl前囟.Size = new System.Drawing.Size(161, 22);
            this.ucTxtLblTxtLbl前囟.TabIndex = 2;
            this.ucTxtLblTxtLbl前囟.Txt1EditValue = null;
            this.ucTxtLblTxtLbl前囟.Txt1Size = new System.Drawing.Size(50, 20);
            this.ucTxtLblTxtLbl前囟.Txt2EditValue = null;
            this.ucTxtLblTxtLbl前囟.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // comboBoxEdit步态
            // 
            this.comboBoxEdit步态.Location = new System.Drawing.Point(501, -12);
            this.comboBoxEdit步态.Name = "comboBoxEdit步态";
            this.comboBoxEdit步态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit步态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit步态.Size = new System.Drawing.Size(228, 20);
            this.comboBoxEdit步态.StyleController = this.layoutControl1;
            this.comboBoxEdit步态.TabIndex = 19;
            // 
            // comboBoxEdit皮肤
            // 
            this.comboBoxEdit皮肤.Location = new System.Drawing.Point(501, -132);
            this.comboBoxEdit皮肤.Name = "comboBoxEdit皮肤";
            this.comboBoxEdit皮肤.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit皮肤.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit皮肤.Size = new System.Drawing.Size(228, 20);
            this.comboBoxEdit皮肤.StyleController = this.layoutControl1;
            this.comboBoxEdit皮肤.TabIndex = 9;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem52,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.lci体重,
            this.lci身长,
            this.lciEye,
            this.lciEar,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.emptySpaceItem1,
            this.lciSfys,
            this.lciQt,
            this.lciZz,
            this.emptySpaceItem3,
            this.layoutControlItem22,
            this.lciFace,
            this.lci皮肤,
            this.lci前囟,
            this.lci出牙龋齿,
            this.lciHeart,
            this.lciBelly,
            this.lciFours,
            this.lciGlbtz,
            this.lciHwhd,
            this.lciZd,
            this.lciFywssd,
            this.lciListening,
            this.lciBt,
            this.lbl胸部,
            this.layoutControlItem13,
            this.lbl发育评估,
            this.lbl患病情况,
            this.lbl家长签名,
            this.lciXcsfrq,
            this.lciSfrq,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem12});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -272);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 772);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.textEdit卡号;
            this.layoutControlItem52.CustomizationFormText = "卡号：";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(336, 24);
            this.layoutControlItem52.Text = "卡号：";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit儿童档案号;
            this.layoutControlItem2.CustomizationFormText = "儿童档案号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(336, 40);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem2.Text = "儿童档案号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit儿童姓名;
            this.layoutControlItem6.CustomizationFormText = "儿童姓名：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(336, 24);
            this.layoutControlItem6.Text = "儿童姓名：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit身份证号;
            this.layoutControlItem3.CustomizationFormText = "身份证号：";
            this.layoutControlItem3.Location = new System.Drawing.Point(336, 64);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem3.Text = "身份证号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit性别;
            this.layoutControlItem7.CustomizationFormText = "性别：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(336, 24);
            this.layoutControlItem7.Text = "性别：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit出生日期;
            this.layoutControlItem4.CustomizationFormText = "出生日期：";
            this.layoutControlItem4.Location = new System.Drawing.Point(336, 88);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem4.Text = "出生日期：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lci体重
            // 
            this.lci体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci体重.AppearanceItemCaption.Options.UseFont = true;
            this.lci体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci体重.Control = this.flowLayoutPanel2;
            this.lci体重.CustomizationFormText = "体重：";
            this.lci体重.Location = new System.Drawing.Point(0, 112);
            this.lci体重.Name = "lci体重";
            this.lci体重.Size = new System.Drawing.Size(364, 24);
            this.lci体重.Tag = "check";
            this.lci体重.Text = "体重：";
            this.lci体重.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lci身长
            // 
            this.lci身长.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci身长.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci身长.AppearanceItemCaption.Options.UseFont = true;
            this.lci身长.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci身长.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci身长.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci身长.Control = this.flowLayoutPanel3;
            this.lci身长.CustomizationFormText = "身长：";
            this.lci身长.Location = new System.Drawing.Point(364, 112);
            this.lci身长.Name = "lci身长";
            this.lci身长.Size = new System.Drawing.Size(365, 24);
            this.lci身长.Tag = "check";
            this.lci身长.Text = "身长：";
            this.lci身长.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciEye
            // 
            this.lciEye.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciEye.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciEye.AppearanceItemCaption.Options.UseFont = true;
            this.lciEye.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciEye.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciEye.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciEye.Control = this.comboBoxEdit眼外观;
            this.lciEye.CustomizationFormText = "眼外观：";
            this.lciEye.Location = new System.Drawing.Point(364, 160);
            this.lciEye.Name = "lciEye";
            this.lciEye.Size = new System.Drawing.Size(365, 24);
            this.lciEye.Tag = "check";
            this.lciEye.Text = "眼睛：";
            this.lciEye.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciEar
            // 
            this.lciEar.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciEar.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciEar.AppearanceItemCaption.Options.UseFont = true;
            this.lciEar.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciEar.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciEar.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciEar.Control = this.comboBoxEdit耳外观;
            this.lciEar.CustomizationFormText = "耳外观：";
            this.lciEar.Location = new System.Drawing.Point(0, 184);
            this.lciEar.Name = "lciEar";
            this.lciEar.Size = new System.Drawing.Size(364, 24);
            this.lciEar.Tag = "check";
            this.lciEar.Text = "耳外观：";
            this.lciEar.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit录入时间;
            this.layoutControlItem5.CustomizationFormText = "录入时间：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 710);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(257, 24);
            this.layoutControlItem5.Text = "录入时间：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(130, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit最近修改人;
            this.layoutControlItem11.CustomizationFormText = "最近修改人：";
            this.layoutControlItem11.Location = new System.Drawing.Point(257, 734);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem11.Text = "最近修改人：";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 40);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 40);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(729, 40);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "儿童健康检查记录表 （24月）";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(130, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // lciSfys
            // 
            this.lciSfys.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciSfys.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciSfys.AppearanceItemCaption.Options.UseFont = true;
            this.lciSfys.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciSfys.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciSfys.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciSfys.Control = this.textEdit随访医生;
            this.lciSfys.CustomizationFormText = "随访医生签名:";
            this.lciSfys.Location = new System.Drawing.Point(364, 662);
            this.lciSfys.Name = "lciSfys";
            this.lciSfys.Size = new System.Drawing.Size(365, 24);
            this.lciSfys.Tag = "check";
            this.lciSfys.Text = "随访医生签名:";
            this.lciSfys.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciQt
            // 
            this.lciQt.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciQt.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciQt.AppearanceItemCaption.Options.UseFont = true;
            this.lciQt.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciQt.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciQt.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciQt.Control = this.textEdit其他;
            this.lciQt.CustomizationFormText = "其他:";
            this.lciQt.Location = new System.Drawing.Point(0, 638);
            this.lciQt.Name = "lciQt";
            this.lciQt.Size = new System.Drawing.Size(729, 24);
            this.lciQt.Tag = "check";
            this.lciQt.Text = "其他:";
            this.lciQt.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciZz
            // 
            this.lciZz.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciZz.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciZz.AppearanceItemCaption.Options.UseFont = true;
            this.lciZz.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciZz.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciZz.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciZz.Control = this.panel1;
            this.lciZz.CustomizationFormText = "转诊:";
            this.lciZz.Location = new System.Drawing.Point(0, 508);
            this.lciZz.MinSize = new System.Drawing.Size(228, 82);
            this.lciZz.Name = "lciZz";
            this.lciZz.Size = new System.Drawing.Size(729, 82);
            this.lciZz.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciZz.Tag = "check";
            this.lciZz.Text = "转诊:";
            this.lciZz.TextSize = new System.Drawing.Size(130, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 758);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(729, 10);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.flowLayoutPanel7;
            this.layoutControlItem22.CustomizationFormText = "中医药健康管理服务:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 590);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(228, 48);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(729, 48);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "中医药健康管理服务:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciFace
            // 
            this.lciFace.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciFace.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciFace.AppearanceItemCaption.Options.UseFont = true;
            this.lciFace.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciFace.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciFace.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciFace.Control = this.flowLayoutPanel5;
            this.lciFace.CustomizationFormText = "面色:";
            this.lciFace.Location = new System.Drawing.Point(0, 136);
            this.lciFace.Name = "lciFace";
            this.lciFace.Size = new System.Drawing.Size(364, 24);
            this.lciFace.Tag = "check";
            this.lciFace.Text = "面色:";
            this.lciFace.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lci皮肤
            // 
            this.lci皮肤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci皮肤.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci皮肤.AppearanceItemCaption.Options.UseFont = true;
            this.lci皮肤.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci皮肤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci皮肤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci皮肤.Control = this.comboBoxEdit皮肤;
            this.lci皮肤.CustomizationFormText = "皮肤：";
            this.lci皮肤.Location = new System.Drawing.Point(364, 136);
            this.lci皮肤.Name = "lci皮肤";
            this.lci皮肤.Size = new System.Drawing.Size(365, 24);
            this.lci皮肤.Tag = "check";
            this.lci皮肤.Text = "皮肤：";
            this.lci皮肤.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lci前囟
            // 
            this.lci前囟.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci前囟.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci前囟.AppearanceItemCaption.Options.UseFont = true;
            this.lci前囟.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci前囟.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci前囟.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci前囟.Control = this.flowLayoutPanel4;
            this.lci前囟.CustomizationFormText = "前卤：";
            this.lci前囟.Location = new System.Drawing.Point(0, 160);
            this.lci前囟.Name = "lci前囟";
            this.lci前囟.Size = new System.Drawing.Size(364, 24);
            this.lci前囟.Tag = "check";
            this.lci前囟.Text = "前囟:";
            this.lci前囟.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lci出牙龋齿
            // 
            this.lci出牙龋齿.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lci出牙龋齿.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lci出牙龋齿.AppearanceItemCaption.Options.UseFont = true;
            this.lci出牙龋齿.AppearanceItemCaption.Options.UseForeColor = true;
            this.lci出牙龋齿.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lci出牙龋齿.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lci出牙龋齿.Control = this.ucTxtLblTxtLbl出牙龋齿;
            this.lci出牙龋齿.CustomizationFormText = "出牙/龋齿数：";
            this.lci出牙龋齿.Location = new System.Drawing.Point(364, 184);
            this.lci出牙龋齿.Name = "lci出牙龋齿";
            this.lci出牙龋齿.Size = new System.Drawing.Size(365, 24);
            this.lci出牙龋齿.Tag = "check";
            this.lci出牙龋齿.Text = "出牙/龋齿数：";
            this.lci出牙龋齿.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciHeart
            // 
            this.lciHeart.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciHeart.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciHeart.AppearanceItemCaption.Options.UseFont = true;
            this.lciHeart.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciHeart.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciHeart.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciHeart.Control = this.comboBoxEdit心肺;
            this.lciHeart.CustomizationFormText = "心肺：";
            this.lciHeart.Location = new System.Drawing.Point(0, 208);
            this.lciHeart.Name = "lciHeart";
            this.lciHeart.Size = new System.Drawing.Size(364, 24);
            this.lciHeart.Tag = "check";
            this.lciHeart.Text = "心肺：";
            this.lciHeart.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciBelly
            // 
            this.lciBelly.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciBelly.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciBelly.AppearanceItemCaption.Options.UseFont = true;
            this.lciBelly.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciBelly.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciBelly.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciBelly.Control = this.comboBoxEdit腹部;
            this.lciBelly.CustomizationFormText = "腹部：";
            this.lciBelly.Location = new System.Drawing.Point(364, 208);
            this.lciBelly.Name = "lciBelly";
            this.lciBelly.Size = new System.Drawing.Size(365, 24);
            this.lciBelly.Tag = "check";
            this.lciBelly.Text = "腹部：";
            this.lciBelly.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciFours
            // 
            this.lciFours.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciFours.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciFours.AppearanceItemCaption.Options.UseFont = true;
            this.lciFours.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciFours.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciFours.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciFours.Control = this.comboBoxEdit四肢;
            this.lciFours.CustomizationFormText = "四肢：";
            this.lciFours.Location = new System.Drawing.Point(0, 232);
            this.lciFours.Name = "lciFours";
            this.lciFours.Size = new System.Drawing.Size(364, 24);
            this.lciFours.Tag = "check";
            this.lciFours.Text = "四肢：";
            this.lciFours.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciGlbtz
            // 
            this.lciGlbtz.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciGlbtz.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciGlbtz.AppearanceItemCaption.Options.UseFont = true;
            this.lciGlbtz.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciGlbtz.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciGlbtz.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciGlbtz.Control = this.comboBoxEdit可疑佝偻;
            this.lciGlbtz.CustomizationFormText = "可疑佝偻病体征:";
            this.lciGlbtz.Location = new System.Drawing.Point(364, 232);
            this.lciGlbtz.Name = "lciGlbtz";
            this.lciGlbtz.Size = new System.Drawing.Size(365, 24);
            this.lciGlbtz.Tag = "check";
            this.lciGlbtz.Text = "可疑佝偻病体征:";
            this.lciGlbtz.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciHwhd
            // 
            this.lciHwhd.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciHwhd.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciHwhd.AppearanceItemCaption.Options.UseFont = true;
            this.lciHwhd.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciHwhd.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciHwhd.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciHwhd.Control = this.ucTxtLbl户外活动;
            this.lciHwhd.CustomizationFormText = "户外活动:";
            this.lciHwhd.Location = new System.Drawing.Point(0, 280);
            this.lciHwhd.Name = "lciHwhd";
            this.lciHwhd.Size = new System.Drawing.Size(364, 24);
            this.lciHwhd.Tag = "check";
            this.lciHwhd.Text = "户外活动:";
            this.lciHwhd.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciZd
            // 
            this.lciZd.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciZd.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciZd.AppearanceItemCaption.Options.UseFont = true;
            this.lciZd.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciZd.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciZd.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciZd.Control = this.flowLayoutPanel6;
            this.lciZd.CustomizationFormText = "指导:";
            this.lciZd.Location = new System.Drawing.Point(0, 448);
            this.lciZd.MinSize = new System.Drawing.Size(228, 60);
            this.lciZd.Name = "lciZd";
            this.lciZd.Size = new System.Drawing.Size(729, 60);
            this.lciZd.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciZd.Tag = "check";
            this.lciZd.Text = "指导:";
            this.lciZd.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciFywssd
            // 
            this.lciFywssd.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciFywssd.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciFywssd.AppearanceItemCaption.Options.UseFont = true;
            this.lciFywssd.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciFywssd.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciFywssd.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciFywssd.Control = this.ucTxtLbl服用维生素;
            this.lciFywssd.CustomizationFormText = "服用维生素D:";
            this.lciFywssd.Location = new System.Drawing.Point(364, 280);
            this.lciFywssd.Name = "lciFywssd";
            this.lciFywssd.Size = new System.Drawing.Size(365, 24);
            this.lciFywssd.Tag = "check";
            this.lciFywssd.Text = "服用维生素D:";
            this.lciFywssd.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciListening
            // 
            this.lciListening.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciListening.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciListening.AppearanceItemCaption.Options.UseFont = true;
            this.lciListening.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciListening.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciListening.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciListening.Control = this.comboBoxEdit听力;
            this.lciListening.CustomizationFormText = "听力：";
            this.lciListening.Location = new System.Drawing.Point(0, 256);
            this.lciListening.Name = "lciListening";
            this.lciListening.Size = new System.Drawing.Size(364, 24);
            this.lciListening.Tag = "check";
            this.lciListening.Text = "听力：";
            this.lciListening.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciBt
            // 
            this.lciBt.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciBt.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciBt.AppearanceItemCaption.Options.UseFont = true;
            this.lciBt.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciBt.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciBt.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciBt.Control = this.comboBoxEdit步态;
            this.lciBt.CustomizationFormText = "口腔：";
            this.lciBt.Location = new System.Drawing.Point(364, 256);
            this.lciBt.Name = "lciBt";
            this.lciBt.Size = new System.Drawing.Size(365, 24);
            this.lciBt.Tag = "check";
            this.lciBt.Text = "步态：";
            this.lciBt.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lbl胸部
            // 
            this.lbl胸部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl胸部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl胸部.AppearanceItemCaption.Options.UseFont = true;
            this.lbl胸部.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl胸部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl胸部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl胸部.Control = this.comboBoxEdit胸部;
            this.lbl胸部.CustomizationFormText = "胸部：";
            this.lbl胸部.Location = new System.Drawing.Point(0, 304);
            this.lbl胸部.Name = "lbl胸部";
            this.lbl胸部.Size = new System.Drawing.Size(364, 24);
            this.lbl胸部.Tag = "check";
            this.lbl胸部.Text = "胸部：";
            this.lbl胸部.TextSize = new System.Drawing.Size(130, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.layoutControl2;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(364, 304);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // lbl发育评估
            // 
            this.lbl发育评估.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl发育评估.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl发育评估.AppearanceItemCaption.Options.UseFont = true;
            this.lbl发育评估.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl发育评估.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl发育评估.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl发育评估.Control = this.flowLayoutPanel8;
            this.lbl发育评估.CustomizationFormText = "发育评估：";
            this.lbl发育评估.Location = new System.Drawing.Point(0, 328);
            this.lbl发育评估.MinSize = new System.Drawing.Size(237, 60);
            this.lbl发育评估.Name = "lbl发育评估";
            this.lbl发育评估.Size = new System.Drawing.Size(729, 60);
            this.lbl发育评估.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl发育评估.Tag = "check";
            this.lbl发育评估.Text = "发育评估：";
            this.lbl发育评估.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lbl患病情况
            // 
            this.lbl患病情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl患病情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl患病情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl患病情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl患病情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl患病情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl患病情况.Control = this.flow患病情况;
            this.lbl患病情况.CustomizationFormText = "两次随访间患病情况：";
            this.lbl患病情况.Location = new System.Drawing.Point(0, 388);
            this.lbl患病情况.MinSize = new System.Drawing.Size(237, 60);
            this.lbl患病情况.Name = "lbl患病情况";
            this.lbl患病情况.Size = new System.Drawing.Size(729, 60);
            this.lbl患病情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl患病情况.Tag = "check";
            this.lbl患病情况.Text = "两次随访间患病情况：";
            this.lbl患病情况.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lbl家长签名
            // 
            this.lbl家长签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl家长签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl家长签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家长签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家长签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家长签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家长签名.Control = this.textEdit家长签名;
            this.lbl家长签名.CustomizationFormText = "家长签名：";
            this.lbl家长签名.Location = new System.Drawing.Point(364, 686);
            this.lbl家长签名.Name = "lbl家长签名";
            this.lbl家长签名.Size = new System.Drawing.Size(365, 24);
            this.lbl家长签名.Tag = "check";
            this.lbl家长签名.Text = "家长签名：";
            this.lbl家长签名.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciXcsfrq
            // 
            this.lciXcsfrq.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciXcsfrq.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciXcsfrq.AppearanceItemCaption.Options.UseFont = true;
            this.lciXcsfrq.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciXcsfrq.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciXcsfrq.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciXcsfrq.Control = this.dateEdit下次随访日期;
            this.lciXcsfrq.CustomizationFormText = "下次随访日期:";
            this.lciXcsfrq.Location = new System.Drawing.Point(0, 686);
            this.lciXcsfrq.Name = "lciXcsfrq";
            this.lciXcsfrq.Size = new System.Drawing.Size(364, 24);
            this.lciXcsfrq.Tag = "check";
            this.lciXcsfrq.Text = "下次随访日期:";
            this.lciXcsfrq.TextSize = new System.Drawing.Size(130, 14);
            // 
            // lciSfrq
            // 
            this.lciSfrq.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lciSfrq.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lciSfrq.AppearanceItemCaption.Options.UseFont = true;
            this.lciSfrq.AppearanceItemCaption.Options.UseForeColor = true;
            this.lciSfrq.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciSfrq.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciSfrq.Control = this.dateEdit随访日期;
            this.lciSfrq.CustomizationFormText = "随访日期:";
            this.lciSfrq.Location = new System.Drawing.Point(0, 662);
            this.lciSfrq.Name = "lciSfrq";
            this.lciSfrq.Size = new System.Drawing.Size(364, 24);
            this.lciSfrq.Tag = "check";
            this.lciSfrq.Text = "随访日期:";
            this.lciSfrq.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciSfrq.TextSize = new System.Drawing.Size(130, 14);
            this.lciSfrq.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit录入人;
            this.layoutControlItem10.CustomizationFormText = "录入人：";
            this.layoutControlItem10.Location = new System.Drawing.Point(257, 710);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit创建机构;
            this.layoutControlItem9.CustomizationFormText = "创建机构：";
            this.layoutControlItem9.Location = new System.Drawing.Point(455, 710);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem9.Text = "创建机构：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit最近修改时间;
            this.layoutControlItem8.CustomizationFormText = "最近更新时间：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 734);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(257, 24);
            this.layoutControlItem8.Text = "最近更新时间：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(130, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit当前所属机构;
            this.layoutControlItem12.CustomizationFormText = "当前所属机构：";
            this.layoutControlItem12.Location = new System.Drawing.Point(455, 734);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem12.Text = "当前所属机构：";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // UC儿童健康检查_24月
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC儿童健康检查_24月";
            this.Size = new System.Drawing.Size(750, 535);
            this.Load += new System.EventHandler(this.UC儿童健康检查_24月_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家长签名.Properties)).EndInit();
            this.flow患病情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk患病情况99.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk发育评估4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit胸部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit听力.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医饮食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医起居.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit传授摩捏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit中医其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit中医其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit儿童档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导合理膳食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导预防意外1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导生长发育.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导口腔保健.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导疾病预防.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk口服维生素D.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否到位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色红润.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit面色其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit可疑佝偻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit四肢.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit腹部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit心肺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit耳外观.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit眼外观.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit身长.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit前囟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit步态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit皮肤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci身长)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfys)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci皮肤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci前囟)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lci出牙龋齿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHeart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBelly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGlbtz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHwhd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciZd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFywssd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciListening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl发育评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl患病情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家长签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXcsfrq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSfrq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton sbtnSave;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DateEdit dateEdit随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导合理膳食;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导预防意外1;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导生长发育;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导口腔保健;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导疾病预防;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色红润;
        private DevExpress.XtraEditors.CheckEdit checkEdit面色其他;
        private Library.UserControls.UCTxtLbl ucTxtLbl服用维生素;
        private Library.UserControls.UCTxtLbl ucTxtLbl户外活动;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit可疑佝偻;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit四肢;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit腹部;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit心肺;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit耳外观;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit眼外观;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit前囟;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl前囟;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit皮肤;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private Library.UserControls.UCTxtLbl ucTxtLbl身长;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit身长;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Library.UserControls.UCTxtLbl ucTxtLbl体重;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit体重;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit步态;
        private System.Windows.Forms.Panel panel1;
        private Library.UserControls.UCLblTxt ucLblTxt转诊机构;
        private Library.UserControls.UCLblTxt ucLblTxt转诊原因;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit转诊;
        private DevExpress.XtraEditors.TextEdit textEdit其他;
        private DevExpress.XtraEditors.DateEdit dateEdit下次随访日期;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem lci体重;
        private DevExpress.XtraLayout.LayoutControlItem lci身长;
        private DevExpress.XtraLayout.LayoutControlItem lciBt;
        private DevExpress.XtraLayout.LayoutControlItem lci皮肤;
        private DevExpress.XtraLayout.LayoutControlItem lci前囟;
        private DevExpress.XtraLayout.LayoutControlItem lciEye;
        private DevExpress.XtraLayout.LayoutControlItem lciEar;
        private DevExpress.XtraLayout.LayoutControlItem lciHeart;
        private DevExpress.XtraLayout.LayoutControlItem lciBelly;
        private DevExpress.XtraLayout.LayoutControlItem lciFours;
        private DevExpress.XtraLayout.LayoutControlItem lciGlbtz;
        private DevExpress.XtraLayout.LayoutControlItem lciHwhd;
        private DevExpress.XtraLayout.LayoutControlItem lciFywssd;
        private DevExpress.XtraLayout.LayoutControlItem lciFace;
        private DevExpress.XtraLayout.LayoutControlItem lciZz;
        private DevExpress.XtraLayout.LayoutControlItem lciZd;
        private DevExpress.XtraLayout.LayoutControlItem lciQt;
        private DevExpress.XtraLayout.LayoutControlItem lciXcsfrq;
        private DevExpress.XtraLayout.LayoutControlItem lciSfys;
        private DevExpress.XtraLayout.LayoutControlItem lciSfrq;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit录入人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改时间;
        private DevExpress.XtraEditors.TextEdit textEdit录入时间;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit儿童姓名;
        private DevExpress.XtraEditors.TextEdit textEdit儿童档案号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit checkEdit中医饮食;
        private DevExpress.XtraEditors.CheckEdit checkEdit中医起居;
        private DevExpress.XtraEditors.CheckEdit checkEdit传授摩捏;
        private DevExpress.XtraEditors.CheckEdit checkEdit中医其他;
        private DevExpress.XtraEditors.TextEdit textEdit中医其他;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl出牙龋齿;
        private DevExpress.XtraLayout.LayoutControlItem lci出牙龋齿;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit听力;
        private DevExpress.XtraLayout.LayoutControlItem lciListening;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导其他;
        private DevExpress.XtraEditors.TextEdit textEdit指导其他;
        private System.Windows.Forms.FlowLayoutPanel flow患病情况;
        private DevExpress.XtraEditors.CheckEdit chk患病情况无;
        private DevExpress.XtraEditors.CheckEdit chk患病情况1;
        private Library.UserControls.UCLblTxtLbl uc肺炎;
        private DevExpress.XtraEditors.CheckEdit chk患病情况2;
        private Library.UserControls.UCLblTxtLbl uc腹泻;
        private DevExpress.XtraEditors.CheckEdit chk患病情况3;
        private Library.UserControls.UCLblTxtLbl uc外伤;
        private DevExpress.XtraEditors.CheckEdit chk患病情况99;
        private Library.UserControls.UCLblTxt uc患病情况其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.CheckEdit chk发育评估无;
        private DevExpress.XtraEditors.CheckEdit chk发育评估1;
        private DevExpress.XtraEditors.CheckEdit chk发育评估2;
        private DevExpress.XtraEditors.CheckEdit chk发育评估3;
        private DevExpress.XtraEditors.CheckEdit chk发育评估4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit胸部;
        private DevExpress.XtraLayout.LayoutControlItem lbl胸部;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem lbl发育评估;
        private DevExpress.XtraLayout.LayoutControlItem lbl患病情况;
        private DevExpress.XtraEditors.TextEdit textEdit家长签名;
        private DevExpress.XtraLayout.LayoutControlItem lbl家长签名;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否到位;
        private Library.UserControls.UCLblTxt ucLblTxt联系方式;
        private Library.UserControls.UCLblTxt ucLblTxt联系人;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit chk口服维生素D;
    }
}
