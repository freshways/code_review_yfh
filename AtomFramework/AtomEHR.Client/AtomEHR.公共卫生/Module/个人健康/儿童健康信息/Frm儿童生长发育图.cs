﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public struct ImagePoint
    {
        public ImagePoint(int mon, decimal value)
        {
            Month = mon;
            dValue = value;
        }
        public int Month;
        public decimal dValue;
    }


    public partial class Frm儿童生长发育图 : XtraForm
    {
        private string m_sex = "";
        private string m_name = "";
        private string m_dah = "";
        public DataTable m_dtBase;

        public Frm儿童生长发育图(string name, string dah, string sex)
        {
            InitializeComponent();

            m_sex = sex;
            m_name = name;
            m_dah = dah;
            GetBaseData(sex);
            DataTable dtGrow = Get儿童发育数据(dah);
            List<个人健康.儿童健康信息.ImagePoint> list身高 = new List<个人健康.儿童健康信息.ImagePoint>();
            List<个人健康.儿童健康信息.ImagePoint> list体重 = new List<个人健康.儿童健康信息.ImagePoint>();
            for (int index = 0; index < dtGrow.Rows.Count; index++)
            {
                decimal d身高 = 0m;
                try
                {
                    d身高 = Convert.ToDecimal(dtGrow.Rows[index]["CM"].ToString());
                }
                catch
                { }
                decimal d体重 = 0m;
                try
                {
                    d体重 = Convert.ToDecimal(dtGrow.Rows[index]["KG"].ToString());
                }
                catch
                { }

                list身高.Add(new 个人健康.儿童健康信息.ImagePoint((int)(dtGrow.Rows[index]["Month"]), d身高));
                list体重.Add(new 个人健康.儿童健康信息.ImagePoint((int)(dtGrow.Rows[index]["Month"]), d体重));
            }

            DrawImage();

            Draw体重曲线(list体重);

            Draw身高曲线(list身高);

            DrawLine(1, 3, 1.4m, "抬头");
            DrawLine(4, 7, 1.5m, "翻身", 10, 2);
            DrawLine(5, 8, 2.4m, "独坐", 10, 2);
            DrawLine(7, 11, 3m, "爬行", 20, 2);
            DrawLine(10, 14, 3.8m, "独站", 20, 2);
            DrawLine(11, 15, 5m, "独走", 20, 2);
            DrawLine(14, 22, 5.6m, "扶栏上楼梯", 40, 2);
            DrawLine(21, 32, 6.6m, "双脚跳", 70, 2);
        }

        public Frm儿童生长发育图(DataTable dtBase, string name, string dah, string sex, List<ImagePoint> list体重, List<ImagePoint> list身高)
        {
            InitializeComponent();
            m_sex = sex;
            m_name = name;
            m_dah = dah;
            m_dtBase = dtBase;

            DrawImage();

            Draw体重曲线(list体重);

            Draw身高曲线(list身高);

            DrawLine(1, 3, 1.4m, "抬头");
            DrawLine(4, 7, 1.5m, "翻身", 10, 2);
            DrawLine(5, 8, 2.4m, "独坐", 10, 2);
            DrawLine(7, 11, 3m, "爬行", 20, 2);
            DrawLine(10, 14, 3.8m, "独站", 20, 2);
            DrawLine(11, 15, 5m, "独走", 20, 2);
            DrawLine(14, 22, 5.6m, "扶栏上楼梯", 40, 2);
            DrawLine(21, 32, 6.6m, "双脚跳", 70, 2);
        }

        private void DrawLine(int beginMonth, int endMonth, decimal kgLoc, string str, int offsetX = 0, int offsetY = 0)
        {
            System.Drawing.Drawing2D.AdjustableArrowCap lineCap =
                new System.Drawing.Drawing2D.AdjustableArrowCap(3, 3, true);
            Pen arrowPen = new Pen(Color.FromArgb(0x1F, 0x68, 0xAA), 1);
            arrowPen.CustomEndCap = lineCap;

            Graphics gfx = Graphics.FromImage(this.pictureEdit1.Image);

            int begin = Cal月份坐标(left, HorizontalSpacing, beginMonth);
            int end = Cal月份坐标(left, HorizontalSpacing, endMonth);

            int YY = Cal体重纵坐标(bottom, VerticalSpacing * 5, kgLoc);

            gfx.DrawLine(arrowPen, begin, YY, end, YY);
            gfx.DrawLine(arrowPen, end, YY, begin, YY);

            gfx.DrawString(str, new Font("宋体", 12, FontStyle.Bold), Brushes.Black, begin + offsetX, YY + offsetY);
        }

        private void Draw体重曲线(List<ImagePoint> list体重)
        {
            if(list体重==null || list体重.Count ==0)
            {
                return;
            }

            Graphics g = Graphics.FromImage(this.pictureEdit1.Image);

            List<Point> plist = new List<Point>();
            for(int index=0; index < list体重.Count; index++)
            {
                //plist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, list体重[index].Month), Cal体重纵坐标(bottom, VerticalSpacing * 5, list体重[index].value)));
                Point p = new Point(Cal月份坐标(left, HorizontalSpacing, list体重[index].Month), Cal体重纵坐标(bottom, VerticalSpacing * 5, list体重[index].dValue));
                plist.Add(p);

                g.FillEllipse(Brushes.DarkBlue, p.X-5, p.Y-5, 10,10);
            }

            if (plist.Count > 1)
            {
                g.DrawLines(new Pen(Brushes.DarkBlue), plist.ToArray());
            }
        }

        private void Draw身高曲线(List<ImagePoint> list身高)
        {
            if (list身高 == null || list身高.Count == 0)
            {
                return;
            }

            Graphics g = Graphics.FromImage(this.pictureEdit1.Image);
            List<Point> plist = new List<Point>();
            for (int index = 0; index < list身高.Count; index++)
            {
                //plist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, list身高[index].Month), Cal身高纵坐标(bottom, minLoc, min身高, list身高[index].value, VerticalSpacing)));
                Point p = new Point(Cal月份坐标(left, HorizontalSpacing, list身高[index].Month), Cal身高纵坐标(bottom, minLoc, min身高, list身高[index].dValue, VerticalSpacing));
                plist.Add(p);
                g.FillEllipse(Brushes.DarkGreen, p.X - 5, p.Y - 5, 10, 10);
            }
            if (plist.Count > 1)
            {
                g.DrawLines(new Pen(Brushes.DarkBlue), plist.ToArray());
            }
            
        }

        
        bll儿童基本信息 bll儿童2 = new bll儿童基本信息();
        private DataTable GetBaseData(string sex="女")
        {
            if (sex == "女")
            {
                if (m_dtBase == null)
                {
                    m_dtBase = bll儿童2.GetBaseData(sex);
                }
                return m_dtBase;
            }
            else
            {
                if (m_dtBase == null)
                {
                    m_dtBase = bll儿童2.GetBaseData("男");
                }
                return m_dtBase;
            }
            //return m_dtBase;
        }

        private DataTable Get儿童发育数据(string dah)
        {
            DataTable dtGrownData = bll儿童2.GetGrownData(dah);
            return dtGrownData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="top">图中最顶端的位置</param>
        /// <param name="ibeginLoc">起始位置的行数，从0开始计数</param>
        /// <param name="d身高"></param>
        /// <param name="unit">每厘米是几个像素点数</param>
        /// <returns></returns>
        private int Cal身高纵坐标(int bottom, int ibeginLoc, int iBegin身高, decimal d身高, int unit)
        {
            decimal ret = Math.Round((d身高 - Convert.ToDecimal(iBegin身高)) * Convert.ToDecimal(unit));
            return bottom - ibeginLoc * unit - Convert.ToInt32(ret);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left">图的左边界</param>
        /// <param name="unit">每月是多少像素点宽度</param>
        /// <param name="month">月份</param>
        /// <returns></returns>
        private int Cal月份坐标(int left , int unit, int month)
        {
            return left + month * unit;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bottom"></param>
        /// <param name="unit"></param>
        /// <param name="d体重"></param>
        /// <param name="iBegin体重">起始体重</param>
        /// <returns></returns>
        private int Cal体重纵坐标(int bottom, int unit, decimal d体重, int iBegin体重=1)
        {
            decimal dd = (d体重 - Convert.ToDecimal(iBegin体重)) * Convert.ToDecimal(unit);
            return bottom - Convert.ToInt32(Math.Round(dd));
        }

        //身高从第多少行开始算，从下往上数 //需要根据这个值来计算“身长/身高(cm)”的显示位置，以及最低身高的起始位置。一般情况都是5的整数倍，即从第多少行开始显示45cm坐标
        int minLoc = 150;
        int min身高 = 40;//与minLoc对应的身高

        static int TotalY = 52 * 5; // 
        static int TotalX = 84;

        static int HorizontalSpacing = 20;//两条竖线的水平间距
        static int VerticalSpacing = 8;//两条水平线的垂直间距



        static int maph = TotalY * VerticalSpacing + 300;
        static int mapwidth = TotalX * HorizontalSpacing + 400;

        int right = mapwidth - 200;
        int left = 200;

        int top = 200;
        int bottom = maph - 100;

        private void DrawImage()
        {
            Bitmap bitmap = new Bitmap(mapwidth, maph);

            Graphics g = Graphics.FromImage(bitmap);
            if (m_sex == "女")
            {
                g.Clear(Color.FromArgb(0xF9, 0xC8, 0xDD));
            }
            else
            {
                g.Clear(Color.FromArgb(0xA9, 0xE1, 0xFA));
            }

            g.DrawString("0--6岁生长发育监测图", new Font("宋体", 20, FontStyle.Bold), Brushes.Black, new Point(mapwidth / 2 - 200, 80));
            g.DrawString("姓名：" + m_name + "      档案号：" + m_dah, new Font("宋体", 20, FontStyle.Bold), Brushes.Black, new Point(mapwidth / 2 - 300, 120));

            //创建画笔(颜色)
            //Pen penBlack = new Pen(Color.FromArgb(0x76, 0x76, 0x76));
            //Pen penGray = new Pen(Color.FromArgb(0xCF, 0xCF, 0xCF));

            Pen penBlack = new Pen(Color.FromArgb(0x66, 0x66, 0x66));
            Pen penGray = new Pen(Color.FromArgb(0xAA, 0xAA, 0xAA));

            //画水平线
            for (int index = 0; index <= TotalY; index++)
            {
                if (index % 5 == 0)
                {
                    Point n1 = new Point(left - 10, bottom - VerticalSpacing * index);
                    Point n2 = new Point(right + 10, bottom - VerticalSpacing * index);
                    g.DrawLine(penBlack, n1, n2);

                    //左下角的体重坐标
                    if ((index / 5) + 1 < 10)
                    {
                        //坐标值
                        g.DrawString((index / 5 + 1).ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 35, bottom - VerticalSpacing * index - 8));

                        //左下角靠近竖线的小横线
                        g.DrawLine(penBlack, new Point(left - 60, bottom - VerticalSpacing * index), new Point(left - 50, bottom - VerticalSpacing * index));
                    }
                    else if ((index / 5) + 1 == 10)
                    {
                        g.DrawLine(penBlack, new Point(left - 60, bottom - VerticalSpacing * index), new Point(left, bottom - VerticalSpacing * index));
                    }
                    else if (index >= minLoc)
                    {
                        //坐标值
                        //g.DrawString((45 + index-50).ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 35, bottom - VerticalSpacing * index - 8));
                        g.DrawString((min身高 + (index - minLoc)).ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 45, bottom - VerticalSpacing * index - 8));

                        //g.DrawLine(penBlack, new Point(left - 60, bottom - VerticalSpacing * index), new Point(left, bottom - VerticalSpacing * index));
                        g.DrawLine(penBlack, new Point(left - 60, bottom - VerticalSpacing * index), new Point(left - 50, bottom - VerticalSpacing * index));
                    }
                    else
                    { }

                    if ((index / 5) < 40)
                    {
                        g.DrawString((index / 5 + 1).ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(right + 20, bottom - VerticalSpacing * index - 8));
                        g.DrawLine(penBlack, new Point(right + 50, bottom - VerticalSpacing * index), new Point(right + 60, bottom - VerticalSpacing * index));
                    }
                    else if (index >= 210)
                    {
                        g.DrawString((min身高 + (index - minLoc)).ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(right + 7, bottom - VerticalSpacing * index - 9));
                        g.DrawLine(penBlack, new Point(right + 50, bottom - VerticalSpacing * index), new Point(right + 60, bottom - VerticalSpacing * index));
                    }
                }
                else
                {
                    Point n1 = new Point(left - 5, bottom - VerticalSpacing * index);
                    Point n2 = new Point(right + 5, bottom - VerticalSpacing * index);
                    g.DrawLine(penGray, n1, n2);
                }
            }
            StringFormat StrF = new StringFormat();
            StrF.FormatFlags = StringFormatFlags.DirectionVertical; // 竖排

            //右下方的竖线
            g.DrawLine(penBlack, new Point(right + 60, bottom), new Point(right + 60, bottom - VerticalSpacing * 195));
            g.DrawString("体重(kg)", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(right + 70, bottom - VerticalSpacing * 195 / 2), StrF);

            //右上方的竖线
            g.DrawLine(penBlack, new Point(right + 60, top), new Point(right + 60, bottom - VerticalSpacing * 210));
            g.DrawString("身长/身高(cm)", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(right + 70, (bottom - VerticalSpacing * 210 + top) / 2 - 100), StrF);

            //左边上方的竖线
            g.DrawLine(penBlack, new Point(left - 60, top), new Point(left - 60, bottom - VerticalSpacing * minLoc));

            //左下角的竖线
            g.DrawLine(penBlack, new Point(left - 60, bottom), new Point(left - 60, bottom - VerticalSpacing * 45));
            //左下角的“体重（kg）”
            //g.DrawString("体\n重\nkg", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 90, bottom - VerticalSpacing * 45/2));

            g.DrawString("体重(kg)", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 90, bottom - VerticalSpacing * 45 / 2), StrF);

            g.DrawString("身长/身高(cm)", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, new Point(left - 90, (bottom - VerticalSpacing * minLoc + top) / 2 - 100), StrF);

            //画竖线
            for (int index = 0; index <= TotalX; index++)
            {
                if (index % 2 == 0)
                {
                    //创建两个点
                    Point n1 = new Point(left + HorizontalSpacing * index, top - 10);
                    Point n2 = new Point(left + HorizontalSpacing * index, bottom + 10);
                    g.DrawLine(penBlack, n1, n2);

                    Point fontp = new Point(left + HorizontalSpacing * index - 8, top - 30);
                    g.DrawString(index.ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, fontp);

                    Point fontp2 = new Point(left + HorizontalSpacing * index - 8, bottom + 10);
                    g.DrawString(index.ToString(), new Font("宋体", 14, FontStyle.Bold), Brushes.Black, fontp2);
                }
                else
                {
                    //创建两个点
                    Point n1 = new Point(left + HorizontalSpacing * index, top - 5);
                    Point n2 = new Point(left + HorizontalSpacing * index, bottom + 5);
                    g.DrawLine(penGray, n1, n2);
                }

                if (index == TotalX)
                {
                    Point fontp = new Point(left + HorizontalSpacing * index + 20, top - 30);
                    g.DrawString("月龄", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, fontp);

                    Point fontp2 = new Point(left + HorizontalSpacing * index + 20, bottom + 10);
                    g.DrawString("月龄", new Font("宋体", 14, FontStyle.Bold), Brushes.Black, fontp2);
                }
            }

            DataTable dtBase = m_dtBase;

            List<Point> CML3SDlist = new List<Point>();
            List<Point> CML2SDlist = new List<Point>();
            List<Point> CML1SDlist = new List<Point>();
            List<Point> CMlist = new List<Point>();
            List<Point> CMG1SDlist = new List<Point>();
            List<Point> CMG2SDlist = new List<Point>();
            List<Point> CMG3SDlist = new List<Point>();

            List<Point> KGL3SDlist = new List<Point>();
            List<Point> KGL2SDlist = new List<Point>();
            List<Point> KGL1SDlist = new List<Point>();
            List<Point> KGlist = new List<Point>();
            List<Point> KGG1SDlist = new List<Point>();
            List<Point> KGG2SDlist = new List<Point>();
            List<Point> KGG3SDlist = new List<Point>();

            for (int index = 0; index < dtBase.Rows.Count; index++)
            {
                CML3SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CML3SD"].ToString()), VerticalSpacing)));
                CML2SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CML2SD"].ToString()), VerticalSpacing)));
                CML1SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CML1SD"].ToString()), VerticalSpacing)));
                CMlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CM"].ToString()), VerticalSpacing)));
                CMG1SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CMG1SD"].ToString()), VerticalSpacing)));
                CMG2SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CMG2SD"].ToString()), VerticalSpacing)));
                CMG3SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal身高纵坐标(bottom, minLoc, min身高, Convert.ToDecimal(dtBase.Rows[index]["CMG3SD"].ToString()), VerticalSpacing)));

                KGL3SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGL3SD"].ToString()))));
                KGL2SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGL2SD"].ToString()))));
                KGL1SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGL1SD"].ToString()))));
                KGlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KG"].ToString()))));
                KGG1SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGG1SD"].ToString()))));
                KGG2SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGG2SD"].ToString()))));
                KGG3SDlist.Add(new Point(Cal月份坐标(left, HorizontalSpacing, Convert.ToInt32(dtBase.Rows[index]["Month"].ToString())), Cal体重纵坐标(bottom, VerticalSpacing * 5, Convert.ToDecimal(dtBase.Rows[index]["KGG3SD"].ToString()))));
            }

            Pen redPen = new Pen(Color.Red);
            Pen pinkPen = new Pen(Color.OrangeRed);
            Pen orangePen = new Pen(Color.Orange);
            Pen greenPen = new Pen(Color.Green);

            g.DrawLines(redPen, CML3SDlist.ToArray());
            g.DrawLines(pinkPen, CML2SDlist.ToArray());
            g.DrawLines(orangePen, CML1SDlist.ToArray());
            g.DrawLines(greenPen, CMlist.ToArray());
            g.DrawLines(orangePen, CMG1SDlist.ToArray());
            g.DrawLines(pinkPen, CMG2SDlist.ToArray());
            g.DrawLines(redPen, CMG3SDlist.ToArray());

            g.DrawLines(redPen, KGL3SDlist.ToArray());
            g.DrawLines(pinkPen, KGL2SDlist.ToArray());
            g.DrawLines(orangePen, KGL1SDlist.ToArray());
            g.DrawLines(greenPen, KGlist.ToArray());
            g.DrawLines(orangePen, KGG1SDlist.ToArray());
            g.DrawLines(pinkPen, KGG2SDlist.ToArray());
            g.DrawLines(redPen, KGG3SDlist.ToArray());

            this.pictureEdit1.Image = bitmap;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromImage(this.pictureEdit1.Image);
            g.FillEllipse(Brushes.Red, Cal月份坐标(left, HorizontalSpacing, 2) - 5, Cal体重纵坐标(bottom, VerticalSpacing * 5, 10m) - 5, 10, 10);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.pictureEdit1.Properties.SizeMode == DevExpress.XtraEditors.Controls.PictureSizeMode.Clip)
            {
                this.labelControl1.Text = "当前模式下不允许放大缩小图片";
                this.simpleButton1.Text = "切换到允许放大缩小模式";
                this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            }
            else
            {
                this.labelControl1.Text = "当前模式下可放大缩小图片。按住Ctr键，滚动鼠标滚轮可以放到或缩小图片";
                this.simpleButton1.Text = "切换到完全显示模式";
                this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Clip;
            }
        }
    }
}
