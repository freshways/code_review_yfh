﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC儿童基本信息_显示 : UserControlBase
    {
        private string id = null;
        private string prgid = null;
        private frm个人健康 frm = null;
        private string m_Grdabh = null;
        //public UC儿童基本信息_显示()
        //{
        //    InitializeComponent();
        //}

        public UC儿童基本信息_显示(frm个人健康 frm个人, string strGrdabh)
        {
            InitializeComponent();

            frm = frm个人;
            m_Grdabh = strGrdabh;

            bll儿童基本信息 bll儿童 = new bll儿童基本信息();
            DataSet ds儿童信息 = bll儿童.Get儿童基本信息ByKey(strGrdabh);
            DataTable dt儿童信息 = ds儿童信息.Tables[0];
            DataRow dr = dt儿童信息.Rows[0];
            //赋值操作
            id = dr[tb_儿童基本信息.ID].ToString();
            textEdit卡号.Text = dr[tb_儿童基本信息.卡号].ToString();
            textEdit儿童档案号.Text = dr[tb_儿童基本信息.个人档案编号].ToString();

            DataRow[] rows = DataDictCache.Cache.t考核项.Select("新表名='"+tb_儿童基本信息.__TableName+"'");


            emptySpaceItem考核项.Text = "   考核项：" + rows.Length.ToString()
                                        + "   缺项：" + dr[tb_儿童基本信息.缺项].ToString() 
                                        + "   完整度：" + dr[tb_儿童基本信息.完整度].ToString() + "% ";

            //姓名解密
            string str姓名 = dr[tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = util.DESEncrypt.DES解密(dr[tb_儿童基本信息.姓名].ToString());

            textEdit身份证号.Text = dr[tb_儿童基本信息.身份证号].ToString();
            textEdit性别.Text = dr["性别"].ToString();
            textEdit出生日期.Text = dr[tb_儿童基本信息.出生日期].ToString();
            textEdit住址.Text = dr["居住地址"].ToString();
            textEdit联系电话.Text = dr[tb_儿童基本信息.联系电话].ToString();

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.父亲姓名].ToString()))
            {
                layoutControlItem父亲姓名.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                textEdit父亲姓名.Text = dr[tb_儿童基本信息.父亲姓名].ToString();
                layoutControlItem父亲姓名.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.父亲工作单位].ToString()))
            {
                layoutControlItem父职业.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                textEdit父亲职业.Text = dr[tb_儿童基本信息.父亲工作单位].ToString();
                layoutControlItem父职业.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.父亲联系电话].ToString()))
            {
                layoutControlItem父电话.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                textEdit父联系电话.Text = dr[tb_儿童基本信息.父亲联系电话].ToString();
                layoutControlItem父电话.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.父亲出生日期].ToString()))
            {
                layoutControlItem父出生日期.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                textEdit父出生日期.Text = dr[tb_儿童基本信息.父亲出生日期].ToString();
                layoutControlItem父出生日期.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            //母亲信息设置
            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.母亲姓名].ToString()))
            {
                layoutControlItem母姓名.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem母姓名.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit母亲姓名.Text = dr[tb_儿童基本信息.母亲姓名].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.母亲工作单位].ToString()))
            {
                layoutControlItem母职业.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem母职业.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit母亲职业.Text = dr[tb_儿童基本信息.母亲工作单位].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.母亲联系电话].ToString()))
            {
                layoutControlItem母电话.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem母电话.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit母联系电话.Text = dr[tb_儿童基本信息.母亲联系电话].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.母亲出生日期].ToString()))
            {
                layoutControlItem母出生日期.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem母出生日期.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit母出生日期.Text = dr[tb_儿童基本信息.母亲出生日期].ToString();
            }

            ////////////////////
            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.邮政编码].ToString()))
            {
                layoutControlItem邮政编码.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem邮政编码.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit邮政编码.Text = dr[tb_儿童基本信息.邮政编码].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.母亲身份证号].ToString()))
            {
                layoutControlItem母身份证号.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem母身份证号.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit母亲身份证号.Text = dr[tb_儿童基本信息.母亲身份证号].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.建档单位].ToString()))
            {
                layoutControlItem建档单位.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem建档单位.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit建档单位.Text = dr[tb_儿童基本信息.建档单位].ToString();
            }

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.建档单位电话].ToString()))
            {
                layoutControlItem建档单位电话.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem建档单位电话.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit建档单位电话.Text = dr[tb_儿童基本信息.建档单位电话].ToString();
            }

            textEdit托幼机构.Text = dr[tb_儿童基本信息.托幼机构].ToString();
            textEdit托幼机构电话.Text = dr[tb_儿童基本信息.托幼机构电话].ToString();
            //if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.托幼机构].ToString()))
            //{
            //    layoutControlItem托幼机构.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    layoutControlItem托幼机构.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.托幼机构电话].ToString()))
            //{
            //    layoutControlItem托幼机构电话.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    layoutControlItem托幼机构电话.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            if(string.IsNullOrWhiteSpace(dr[tb_儿童基本信息.HAPPENTIME].ToString()))
            {
                layoutControlItem发生时间.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                layoutControlItem发生时间.AppearanceItemCaption.ForeColor = Color.Blue;
                textEdit发生时间.Text = dr[tb_儿童基本信息.HAPPENTIME].ToString();
            }

            textEdit录入时间.Text = dr[tb_儿童基本信息.创建日期].ToString();
            textEdit最近修改时间.Text = dr[tb_儿童基本信息.修改日期].ToString();
            textEdit创建机构.Text = dr["创建机构"].ToString();
            textEdit录入人.Text = dr["录入人"].ToString();
            textEdit最近修改人.Text = dr["修改人"].ToString();
            textEdit当前所属机构.Text = dr["当前机构"].ToString();
            prgid = dr[tb_儿童基本信息.所属机构].ToString();
        }

        private void sbtn删除_Click(object sender, EventArgs e)
        {
            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            //if (isallow ==false && prgid != Loginer.CurrentUser.所属机构)
            if (isallow)
            {
                if (isallow == false && prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
                if (Msg.AskQuestion("是否要删除儿童基本信息?若删除儿童基本信息将删除其相关的业务表单!"))
                {
                    //删除儿童基本信息
                    bll儿童基本信息 bll儿童 = new bll儿童基本信息();
                    string strRet = null;
                    int iRet = bll儿童.Delete(id, textEdit儿童档案号.Text, textEdit出生日期.Text, out strRet);
                    if (iRet == -1)
                    {
                        //删除异常，提示失败信息
                        Msg.ShowInformation(strRet);
                    }
                    else if (iRet == 0)
                    {
                        //删除失败，提示失败信息
                        Msg.ShowInformation(strRet);
                    }
                    else //else if(iRet == 1)
                    {
                        //删除成功的情况下，页面跳转至个人基本信息
                        Control control = new 家庭成员健康信息.UC个人基本信息表_显示(frm);
                        control.Dock = DockStyle.Fill;
                        ShowControl(control);
                    }

                }
            }
        }

        private void sbtn修改_Click(object sender, EventArgs e)
        {
            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            //if (isallow == false && prgid != Loginer.CurrentUser.所属机构)
            if (!isallow) 
            {
                if (prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }

            Control ctl = new UC儿童基本信息(m_Grdabh, Common.dt个人档案, UpdateType.Modify, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }
    }
}
