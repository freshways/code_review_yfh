﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraLayout;
using AtomEHR.Business.Security;


namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC儿童基本信息 : UserControlBase
    {
        private bll儿童基本信息 m_Bll = new bll儿童基本信息();
        private UpdateType m_isUpdate = UpdateType.None;
        private DataTable m_dt家庭成员档案 = null;
        private DataSet m_ds儿童基本信息;
        private string m_strGrdabh = null;
        private string m_CloseType = null;//关闭方式
         
        //public UC儿童基本信息()
        //{
        //    InitializeComponent();
        //}

        public UC儿童基本信息(string grdabh, DataTable dt家庭成员信息,UpdateType updatetype, string closeType)
        {
            InitializeComponent();

            m_strGrdabh = grdabh;
            m_dt家庭成员档案 = dt家庭成员信息;
            m_isUpdate = updatetype;

            //设定关闭方式
            m_CloseType = closeType;

            //获取系统时间
            string strDateTime = new bllCom().GetDateTimeFromDBServer();

            //个人信息显示
            DataRow[] drs = m_dt家庭成员档案.Select(tb_健康档案.个人档案编号 + "='" + grdabh + "'");
            if (drs.Length > 0)
            {
                textEdit儿童档案号.Text = drs[0][tb_健康档案.个人档案编号].ToString();
                textEdit儿童姓名.Text = util.DESEncrypt.DES解密(drs[0][tb_健康档案.姓名].ToString());
                textEdit身份证号.Text = drs[0][tb_健康档案.身份证号].ToString();
                textEdit性别.Text = drs[0][tb_健康档案.性别].ToString();
                textEdit出生日期.Text = drs[0][tb_健康档案.出生日期].ToString();

                string str省 = new bll地区档案().Get名称By地区ID(drs[0][tb_健康档案.省].ToString());

                textEdit住址.Text = str省 + drs[0][tb_健康档案.市].ToString()
                                    + drs[0][tb_健康档案.区].ToString() + drs[0][tb_健康档案.街道].ToString()
                                    + drs[0][tb_健康档案.居住地址].ToString();
                textEdit联系电话.Text = drs[0][tb_健康档案.本人电话].ToString();


                //DataView dv = dt个人档案.AsDataView();
                DataView dv父亲 = new DataView(m_dt家庭成员档案);

                //DataView dv父亲 = m_dt家庭成员档案.DefaultView;
                dv父亲.RowFilter = tb_健康档案.个人档案编号 + "<>'" + m_strGrdabh + "' AND (" + tb_健康档案.性别 + "='1' OR " + tb_健康档案.性别 + "='男')";
                DataTable dt父亲 = dv父亲.ToTable();
                //姓名解密
                for (int index = 0; index < dt父亲.Rows.Count; index++)
                {
                    dt父亲.Rows[index][tb_健康档案.姓名] = util.DESEncrypt.DES解密(dt父亲.Rows[index][tb_健康档案.姓名].ToString());
                }
                util.ControlsHelper.BindComboxData(dt父亲, comboBoxEdit父亲姓名, tb_健康档案.个人档案编号, tb_健康档案.姓名);


                //DataView dv母亲 = m_dt家庭成员档案.DefaultView;
                DataView dv母亲 = new DataView(m_dt家庭成员档案);

                dv母亲.RowFilter = tb_健康档案.个人档案编号 + "<>'" + m_strGrdabh + "' AND (" + tb_健康档案.性别 + "='2' OR " + tb_健康档案.性别 + "='女')";
                DataTable dt母亲 = dv母亲.ToTable();
                //姓名解密
                for (int index = 0; index < dt母亲.Rows.Count; index++)
                {
                    dt母亲.Rows[index][tb_健康档案.姓名] = util.DESEncrypt.DES解密(dt母亲.Rows[index][tb_健康档案.姓名].ToString());
                }
                util.ControlsHelper.BindComboxData(dt母亲, comboBoxEdit母亲姓名, tb_健康档案.个人档案编号, tb_健康档案.姓名);

            }
            else
            {
                Msg.ShowError("信息异常，根据个人档案号没有查询到个人基本信息");
                sbtn保存.Enabled = false;
            }

            m_ds儿童基本信息 = m_Bll.GetBusinessByKey(m_strGrdabh, true);
            //判断是新建还是修改
            if (m_isUpdate == UpdateType.Modify)//表示是修改
            {
                //m_ds儿童基本信息 = m_Bll.GetBusinessByKey(m_strGrdabh, true);
                //为控件赋值
                dateEdit发生时间.Text = strDateTime.Substring(0, 10);
                textEdit最近修改时间.Text = strDateTime;
                textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
                textEdit卡号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.卡号].ToString();

                comboBoxEdit父亲姓名.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲姓名].ToString();
                textEdit父亲职业.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲工作单位].ToString();
                textEdit父联系电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲联系电话].ToString();
                dateEdit父出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲出生日期].ToString();

                comboBoxEdit母亲姓名.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲姓名].ToString();
                textEdit母亲职业.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲工作单位].ToString();
                textEdit母联系电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲联系电话].ToString();
                dateEdit母出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲出生日期].ToString();

                textEdit邮政编码.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.邮政编码].ToString();
                textEdit母亲身份证号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲身份证号].ToString();
                textEdit建档单位.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.建档单位].ToString();
                textEdit建档单位电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.建档单位电话].ToString();
                textEdit托幼机构.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.托幼机构].ToString();
                textEdit托幼机构电话.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.托幼机构电话].ToString();

                dateEdit发生时间.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.HAPPENTIME].ToString();
                textEdit录入时间.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建日期].ToString();

                string str录入人 = new bllUser().Return用户名称(m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建人].ToString());
                textEdit录入人.Text = str录入人;

                bll机构信息 bll机构 = new bll机构信息();
                string str创建机构 =  bll机构.Return机构名称(m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建机构].ToString());
                textEdit创建机构.Text = str创建机构;

                string str所属机构 = bll机构.Return机构名称(m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.所属机构].ToString());
                textEdit当前所属机构.Text = str所属机构;

                textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
                textEdit最近修改时间.Text = strDateTime;

                emptySpaceItemHeader.Text = "更新儿童基本信息";
            }
            else if (m_isUpdate == UpdateType.Add)//新增
            {
                //m_Bll.GetBusinessByKey("-", true);//下载一个空业务单据
                m_Bll.CreateNew儿童基本信息();
                //m_ds儿童基本信息 = m_Bll.CurrentBusiness;

                emptySpaceItemHeader.Text = "建立儿童基本信息";
                textEdit录入时间.Text = strDateTime;
                textEdit录入人.Text = Loginer.CurrentUser.AccountName;
                textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;
                textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;

                dateEdit发生时间.Text = strDateTime.Substring(0, 10);
                textEdit最近修改时间.Text = strDateTime;
                textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
            }

            //颜色调整
            ChangedLayoutControlItemFontColor(layoutControlItem父亲姓名, comboBoxEdit父亲姓名);
            ChangedLayoutControlItemFontColor(layoutControlItem父亲职业, textEdit父亲职业);
            ChangedLayoutControlItemFontColor(layoutControlItem父亲联系电话, textEdit父联系电话);
            ChangedLayoutControlItemFontColor(layoutControlItem父亲出生日期, dateEdit父出生日期);
            ChangedLayoutControlItemFontColor(layoutControlItem母亲姓名, comboBoxEdit母亲姓名);
            ChangedLayoutControlItemFontColor(layoutControlItem母亲职业, textEdit母亲职业);
            ChangedLayoutControlItemFontColor(layoutControlItem母亲联系电话, textEdit母联系电话);
            ChangedLayoutControlItemFontColor(layoutControlItem母亲出生日期, dateEdit母出生日期);
            ChangedLayoutControlItemFontColor(layoutControlItem邮政编码, textEdit邮政编码);
            ChangedLayoutControlItemFontColor(layoutControlItem母亲身份证号, textEdit母亲身份证号);
            ChangedLayoutControlItemFontColor(layoutControlItem建档单位, textEdit建档单位);
            ChangedLayoutControlItemFontColor(layoutControlItem建档单位电话, textEdit建档单位电话);
            ChangedLayoutControlItemFontColor(layoutControlItem托幼机构, textEdit托幼机构);
            ChangedLayoutControlItemFontColor(layoutControlItem托幼机构电话, textEdit托幼机构电话);
            ChangedLayoutControlItemFontColor(layoutControlItem发生时间, dateEdit发生时间);
        }

        #region 颜色调整
        private void comboBox父亲姓名_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChangedLayoutControlItemFontColor(layoutControlItem父亲姓名, comboBoxEdit父亲姓名);
            if (string.IsNullOrWhiteSpace(comboBoxEdit父亲姓名.Text))
            { }
            else
            {
                string str父亲档案号 = util.ControlsHelper.GetComboxKey(comboBoxEdit父亲姓名);
                DataRow[] drs = m_dt家庭成员档案.Select(tb_健康档案.个人档案编号 + "='" + str父亲档案号 + "'");
                if (drs.Length > 0)
                {
                    textEdit父亲职业.Text = drs[0][tb_健康档案.职业].ToString();
                    textEdit父联系电话.Text = drs[0][tb_健康档案.本人电话].ToString();
                    dateEdit父出生日期.Text = drs[0][tb_健康档案.出生日期].ToString();
                }
            }
        }
        
        private void textEdit父亲职业_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem父亲职业, textEdit父亲职业);
        }

        private void textEdit父联系电话_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem父亲联系电话, textEdit父联系电话);
        }

        private void dateEdit父出生日期_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem父亲出生日期, dateEdit父出生日期);
        }

        private void comboBox母亲姓名_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChangedLayoutControlItemFontColor(layoutControlItem母亲姓名, comboBoxEdit母亲姓名);
            if (string.IsNullOrWhiteSpace(comboBoxEdit母亲姓名.Text))
            { }
            else
            {
                string str母亲档案号 = util.ControlsHelper.GetComboxKey(comboBoxEdit母亲姓名);
                DataRow[] drs = m_dt家庭成员档案.Select(tb_健康档案.个人档案编号 + "='" + str母亲档案号 + "'");
                if (drs.Length > 0)
                {
                    textEdit母亲职业.Text = drs[0][tb_健康档案.职业].ToString();
                    textEdit母联系电话.Text = drs[0][tb_健康档案.本人电话].ToString();
                    dateEdit母出生日期.Text = drs[0][tb_健康档案.出生日期].ToString();
                    textEdit母亲身份证号.Text = drs[0][tb_健康档案.身份证号].ToString();
                }
            }
        }

        private void textEdit母亲职业_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem母亲职业, textEdit母亲职业);
        }

        private void textEdit母联系电话_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem母亲联系电话, textEdit母联系电话);
        }

        private void dateEdit母出生日期_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem母亲出生日期, dateEdit母出生日期);
        }

        private void textEdit邮政编码_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem邮政编码, textEdit邮政编码);
        }

        private void textEdit母亲身份证号_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem母亲身份证号, textEdit母亲身份证号);
        }

        private void textEdit建档单位_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem建档单位, textEdit建档单位);
        }

        private void textEdit建档单位电话_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem建档单位电话, textEdit建档单位电话);
        }

        private void textEdit托幼机构_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem托幼机构, textEdit托幼机构);
        }

        private void textEdit托幼机构电话_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem托幼机构电话, textEdit托幼机构电话);
        }

        private void dateEdit发生时间_EditValueChanged(object sender, EventArgs e)
        {
            ChangedLayoutControlItemFontColor(layoutControlItem发生时间, dateEdit发生时间);
        }

        private void ChangedLayoutControlItemFontColor(DevExpress.XtraLayout.LayoutControlItem item, Control control)
        {
            Type type = control.GetType();

            if (type.Name == "ComboBoxEdit" || type.Name == "DateEdit" || type.Name == "TextEdit")
            {
                if (string.IsNullOrWhiteSpace(control.Text))
                {
                    item.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else
                {
                    item.AppearanceItemCaption.ForeColor = Color.Blue;
                }
            }
            else
            {

            }
        }
        #endregion

        private void sbtn保存_Click(object sender, EventArgs e)
        {
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号] = textEdit儿童档案号.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.卡号] = textEdit卡号.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲姓名] = comboBoxEdit父亲姓名.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲工作单位] = textEdit父亲职业.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲联系电话] = textEdit父联系电话.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲出生日期] = dateEdit父出生日期.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲姓名] = comboBoxEdit母亲姓名.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲工作单位] = textEdit母亲职业.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲联系电话] = textEdit母联系电话.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲出生日期] = dateEdit母出生日期.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.邮政编码] = textEdit邮政编码.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲身份证号] = textEdit母亲身份证号.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.建档单位] = textEdit建档单位.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.建档单位电话] = textEdit建档单位电话.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.托幼机构] = textEdit托幼机构.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.托幼机构电话] = textEdit托幼机构电话.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.HAPPENTIME] = dateEdit发生时间.Text;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.修改人] = Loginer.CurrentUser.用户编码;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.修改日期] = textEdit最近修改时间.Text;

            //完整度与缺项
            //m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.完整度] =  ;
            //m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.缺项] =  ;

            if (m_isUpdate == UpdateType.Modify)//修改时
            {

            }
            else if (m_isUpdate == UpdateType.Add)//添加时
            {

                //DataRow[] drs = m_dt家庭成员档案.Select(tb_健康档案.档案号码 + "='" + m_strGrdabh + "'");

                DataSet ds健康档案 = new bll健康档案().GetRowInfoByKey(m_strGrdabh);
                DataRowCollection drs = ds健康档案.Tables[0].Rows;

                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号] = drs[0][tb_健康档案.个人档案编号].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名] = drs[0][tb_健康档案.姓名].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号] = drs[0][tb_健康档案.身份证号].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别] = drs[0][tb_健康档案.性别].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期] = drs[0][tb_健康档案.出生日期].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.省] = drs[0][tb_健康档案.省].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.市] = drs[0][tb_健康档案.市].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.区] = drs[0][tb_健康档案.区].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.街道] = drs[0][tb_健康档案.街道].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.居委会] = drs[0][tb_健康档案.居委会].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.居住地址] = drs[0][tb_健康档案.居住地址].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建人] = Loginer.CurrentUser.用户编码;
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建日期] = textEdit录入时间.Text;
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.拼音简码] = drs[0][tb_健康档案.拼音简码].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.居住状况] = drs[0][tb_健康档案.常住类型].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.婚姻状况] = drs[0][tb_健康档案.婚姻状况].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.医疗保险号] = drs[0][tb_健康档案.医疗保险号].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.新农合号] = drs[0][tb_健康档案.新农合号].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.工作单位] = drs[0][tb_健康档案.工作单位].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.邮箱] = drs[0][tb_健康档案.邮箱].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.医疗费支付类型] = drs[0][tb_健康档案.医疗费支付类型].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.民族] = drs[0][tb_健康档案.民族].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.文化程度] = drs[0][tb_健康档案.文化程度].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.职业] = drs[0][tb_健康档案.职业].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.民族] = drs[0][tb_健康档案.民族].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.家庭档案编号] = drs[0][tb_健康档案.家庭档案编号].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.与户主关系] = drs[0][tb_健康档案.与户主关系].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.档案状态] = drs[0][tb_健康档案.档案状态].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.所属片区] = drs[0][tb_健康档案.所属片区].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.D_GRDABH_17] = drs[0][tb_健康档案.D_GRDABH_17].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.D_GRDABH_SHOW] = drs[0][tb_健康档案.D_GRDABH_SHOW].ToString();
                m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.联系电话] = drs[0][tb_健康档案.本人电话].ToString();
                //m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.FCYYID] = drs[0][tb_健康档案.FCYYID].ToString();
            }
            else
            { }

            //计算缺项、完整度
            Control[] controls = { this.comboBoxEdit父亲姓名,this.textEdit父亲职业, this.textEdit父联系电话, this.dateEdit父出生日期,
                                   this.comboBoxEdit母亲姓名, this.textEdit母亲职业, this.textEdit母联系电话,this.dateEdit母出生日期,
                                 this.textEdit邮政编码, this.textEdit母亲身份证号,this.textEdit建档单位, this.textEdit建档单位电话,
                                 //this.textEdit托幼机构, this.textEdit托幼机构电话, this.dateEdit发生时间};
                                 this.dateEdit发生时间};
            int i缺项 = 0;
            int i完整度 = 0;
            int i考核项数 = 0;

            //DataRow[] dr考核项s = DataDictCache.Cache.t考核项.Select("新表名='" + tb_儿童基本信息.__TableName + "'");

            //i考核项数 = dr考核项s.Length;

            for (int index = 0; index < controls.Length; index++)
            {
                if (string.IsNullOrWhiteSpace(controls[index].Text))
                {
                    i缺项++;
                }
            }

            //防止考核项数是0
            if(i考核项数 == 0)
            {
                //i考核项数 = 15;
                i考核项数 = controls.Length;
            }

            i完整度 = 100 - i缺项 * 100 / i考核项数;
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.完整度] = i完整度.ToString();
            m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.缺项] = i缺项.ToString();

            string 出生日期 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.出生日期].ToString();
            string strDateTime = new bllCom().GetDateTimeFromDBServer();
            if (util.ControlsHelper.GetAge(出生日期, strDateTime) <= 6)
            {
                //更新个人健康特征
                m_ds儿童基本信息.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = i缺项.ToString() + "," + i完整度.ToString();
            }

            m_Bll.Save(m_ds儿童基本信息);

            //向tb_妇幼数据中添加数据
            if (m_isUpdate == UpdateType.Add)
            {
                string mqsfzh = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.母亲身份证号].ToString();
                string ertongsfzh = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.身份证号].ToString();
                
                if (mqsfzh.Equals("") || ertongsfzh.Equals(""))
                {

                    string 卡号 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.个人档案编号].ToString();
                    string 姓名 = util.DESEncrypt.DES解密(m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.姓名].ToString());
                    string 性别 = DataDictCache.Cache.t性别.Select("P_CODE='" + m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.性别].ToString() + "'")[0]["P_DESC"].ToString();
                    //出生日期
                    string 身份证号 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.身份证号].ToString();
                    string 母亲姓名 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.母亲姓名].ToString();
                    string 母亲身份证号 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.母亲身份证号].ToString();
                    string JCRGID = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.所属机构].ToString();
                    string JCRGNAME = Loginer.CurrentUser.所属机构名称;
                    string FCYYRGID = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.FCYYID].ToString();
                    string 录入人 = Loginer.CurrentUser.AccountName;
                    string 录入时间 = m_ds儿童基本信息.Tables[0].Rows[0][tb_儿童基本信息.创建日期].ToString();

                    new bll妇幼数据().InsertData(卡号, 姓名, 性别, 出生日期, 身份证号,
                                 母亲姓名, 母亲身份证号, JCRGID, JCRGNAME, FCYYRGID, 录入人, 录入时间
                        //,身份, YUNCC, 表单号, SJTQFSQK, SJLY
                                 );
                }
            }

            if (m_CloseType != null)
            {
                Common.frm个人健康.DialogResult = DialogResult.OK;
            }
            else
            {
                //显示画面
                Show儿童基本信息显示();
            }
        }

        private void Show儿童基本信息显示()
        {
            Control ctl = new UC儿童基本信息_显示(Common.frm个人健康 as frm个人健康,m_strGrdabh);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }
    }
}
