﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using System.Text.RegularExpressions;
using AtomEHR.Models;
using AtomEHR.Business.Security;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC新生儿家庭访视记录表 : UserControlBase
    {
        private string m_grdabh = null;
        private string m_closeType = null;
        private UpdateType m_updateType = UpdateType.None;
        
        //包含儿童基本信息
        private DataSet m_ds新生儿=null;
        private DataSet m_ds儿童基本信息 = null;
        private bll儿童新生儿访视记录 m_bll新生儿 = new bll儿童新生儿访视记录();
        private bll儿童基本信息 m_bll儿童基本信息 = new bll儿童基本信息();


        public UC新生儿家庭访视记录表(string grdah, UpdateType updatetype, string closeType)
        {
            InitializeComponent();
            m_grdabh = grdah;
            m_closeType = closeType;
            m_updateType = updatetype;

            //为控件绑定数据
            BindData();
        }

        private void SetDefaultValue()
        {
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit口腔);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit皮肤);
            util.ControlsHelper.SetComboxData("0", comboBoxEdit母妊娠患病情况);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit是否畸形);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit新生儿窒息);
            util.ControlsHelper.SetComboxData("2", comboBoxEdit颈部包块);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit眼外观);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit耳外观);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit心肺);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit腹部);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit脐部);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit四肢活动度);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit佝偻病症状);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit可疑佝偻);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit肛门);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit外生殖器);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit脊柱);
            //util.ControlsHelper.SetComboxData("1", ); 听力
            util.ControlsHelper.SetComboxData("2", comboBoxEdit转诊);
            //util.ControlsHelper.SetComboxData("1", comboBoxEdit步态);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit胸部);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit鼻);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit口腔);
        }


        private void UC新生儿家庭访视记录表_Load(object sender, EventArgs e)
        {
            //从数据库中读取新生儿数据
            m_ds新生儿 = m_bll新生儿.GetBusinessByGrdabh(m_grdabh, true);
            m_ds儿童基本信息 = m_bll儿童基本信息.Get儿童基本信息ByGrdabh(m_grdabh);
            string strServerDate = m_bll儿童基本信息.ServiceDateTime;

            if (m_updateType == UpdateType.Add)
            {
                //新增记录的情况下
                m_bll新生儿.NewBusiness();
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.个人档案编号] = m_grdabh;

                //
                textEdit录入时间.Text = strServerDate;
                textEdit录入人.Text = Loginer.CurrentUser.AccountName;
                textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;
                textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                dateEdit随访日期.Text = strServerDate.Substring(0, 10);

                SetDefaultValue();
            }
            else
            {
                //修改时随访日期，下次随访日期不允许在修改
                dateEdit随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
                dateEdit随访日期.Properties.ReadOnly = true;
                dateEdit随访日期.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));
                dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
                dateEdit下次随访日期.Properties.ReadOnly = true;
                dateEdit下次随访日期.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(235)))), ((int)(((byte)(242)))));

                #region  新版本添加
                txt父亲身份证号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.父亲身份证号].ToString();
                //txt母亲身份证号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.母亲身份证号].ToString();
                string str胸部 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.胸部].ToString();
                string str胸部其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.胸部其他].ToString();
                SetComboBoxAndTextEditValue(str胸部, comboBoxEdit胸部, "99", textEdit胸部其他, str胸部其他);
                SetLayoutControlItemFontColor(layoutControlItem胸部, str胸部, "99", str胸部其他);
                textEdit家长签名.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.家长签名].ToString();
                SetLayoutControlItemFontColor(layoutControlItem家长签名, textEdit家长签名.Text);
                #endregion

                //修改的情况下，将值赋给控件
                textEdit卡号.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.卡号].ToString();

                textEdit出生孕周.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生孕周].ToString();
                SetLayoutControlItemFontColor(layoutControlItem出生孕周, textEdit出生孕周.Text);

                string str母亲妊娠 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况].ToString();
                util.ControlsHelper.SetComboxData(str母亲妊娠, comboBoxEdit母妊娠患病情况);
                if (str母亲妊娠.Equals("99"))
                {
                    textEdit母妊娠患病其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他].ToString();
                }
                SetLayoutControlItemFontColor(layoutControlItem母妊娠疾病, str母亲妊娠, "99", textEdit母妊娠患病其他.Text);

                string str是否畸形 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.是否有畸型].ToString();
                string str是否畸形其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.是否有畸型其他].ToString();
                SetComboBoxAndTextEditValue(str是否畸形, comboBoxEdit是否畸形, "1", textEdit是否畸形其他, str是否畸形其他);
                SetLayoutControlItemFontColor(layoutControlItem是否畸形, str是否畸形, "1", textEdit是否畸形其他.Text);

                textEdit助产机构.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.助产机构名称].ToString();
                SetLayoutControlItemFontColor(layoutControlItem助产机构, textEdit助产机构.Text);
                char[] c分隔符 = { ',' };

                #region 设定"出生情况"
                string str出生情况 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况].ToString();
                string[] str出生情况Values = str出生情况.Split(c分隔符);
                for (int index = 0; index < str出生情况Values.Length; index++)
                {
                    switch (str出生情况Values[index])
                    {
                        case "1":
                            checkEdit顺产.Checked = true;
                            break;
                        case "2":
                            checkEdit头吸.Checked = true;
                            break;
                        case "3":
                            checkEdit产钳.Checked = true;
                            break;
                        case "4":
                            checkEdit剖宫.Checked = true;
                            break;
                        case "5":
                            checkEdit双多胎.Checked = true;
                            break;
                        case "6":
                            checkEdit臀位.Checked = true;
                            break;
                        case "99":
                            checkEdit其他.Checked = true;
                            textEdit出生情况其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况其他].ToString();
                            break;
                        default:
                            break;
                    }
                }
                if (checkEdit顺产.Checked || checkEdit头吸.Checked || checkEdit产钳.Checked || checkEdit剖宫.Checked || checkEdit双多胎.Checked
                    || checkEdit臀位.Checked || (checkEdit其他.Checked && !(string.IsNullOrWhiteSpace(textEdit出生情况其他.Text))))
                {
                    layoutControlItem出生情况.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    layoutControlItem出生情况.AppearanceItemCaption.ForeColor = Color.Red;
                }
                #endregion

                #region 设定"新生儿窒息"及"Apgar评分"
                string str新生儿窒息 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿窒息].ToString();
                util.ControlsHelper.SetComboxData(str新生儿窒息, comboBoxEdit新生儿窒息);
                SetLayoutControlItemFontColor(layoutControlItem新生儿窒息, str新生儿窒息);

                if (str新生儿窒息.Equals("1"))
                {
                    string strApgar评分 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分].ToString();
                    string[] strApgar评分Values = strApgar评分.Split(c分隔符);
                    for (int index = 0; index < strApgar评分Values.Length; index++)
                    {
                        switch (strApgar评分Values[index])
                        {
                            case "1":
                                checkEdit1分钟.Checked = true;
                                textEdit1分钟得分.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分1].ToString();
                                break;
                            case "2":
                                checkEdit5分钟.Checked = true;
                                textEdit5分钟得分.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分5].ToString();
                                break;
                            case "3":
                                checkEditApgar不详.Checked = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
                #endregion

                string str听力筛查 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿听力筛查].ToString();
                util.ControlsHelper.SetComboxData(str听力筛查, comboBoxEdit听力筛查);
                SetLayoutControlItemFontColor(layoutControlItem听力筛查, str听力筛查);

                #region 新生儿疾病筛查
                string str疾病筛查 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查].ToString();
                string[] str疾病筛查Values = str疾病筛查.Split(c分隔符);
                for (int index = 0; index < str疾病筛查Values.Length; index++)
                {
                    if (str疾病筛查Values[index].Equals("0"))
                    {
                        checkEdit疾病筛查无.Checked = true;
                        break;
                    }
                    else if (str疾病筛查Values[index].Equals("1"))
                    {
                        checkEdit疾病筛查甲低.Checked = true;
                    }
                    else if (str疾病筛查Values[index].Equals("2"))
                    {
                        checkEdit疾病筛查苯丙酮尿.Checked = true;
                    }
                    else if (str疾病筛查Values[index].Equals("3"))
                    {
                        checkEdit疾病_先天肾病.Checked = true;
                    }
                    else if (str疾病筛查Values[index].Equals("4"))
                    {
                        checkEdit疾病_葡萄糖症.Checked = true;
                    }
                    else if (str疾病筛查Values[index].Equals("99"))
                    {
                        checkEdit疾病_其他.Checked = true;
                        textEdit疾病_其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他].ToString();
                    }
                    else
                    { }
                }

                if (checkEdit疾病筛查无.Checked || checkEdit疾病筛查甲低.Checked || checkEdit疾病筛查苯丙酮尿.Checked || checkEdit疾病_先天肾病.Checked
                    || checkEdit疾病_葡萄糖症.Checked || (checkEdit疾病_其他.Checked && !(string.IsNullOrWhiteSpace(textEdit疾病_其他.Text))))
                {
                    layoutControlItem疾病筛查.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    layoutControlItem疾病筛查.AppearanceItemCaption.ForeColor = Color.Red;
                }
                #endregion

                textEdit出生体重.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿出生体重].ToString();
                SetLayoutControlItemFontColor(layoutControlItem出生体重, textEdit出生体重.Text);

                textEdit目前体重.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.目前体重].ToString();
                SetLayoutControlItemFontColor(layoutControlItem目前体重, textEdit目前体重.Text);

                textEdit出生身长.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生身长].ToString();
                SetLayoutControlItemFontColor(layoutControlItem出生身长, textEdit出生身长.Text);

                string str喂养方式 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.喂养方式].ToString();
                util.ControlsHelper.SetComboxData(str喂养方式, comboBoxEdit喂养方式);
                SetLayoutControlItemFontColor(layoutControlItem喂养方式, str喂养方式);

                textEdit吃奶量.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶量].ToString();

                textEdit吃奶次数.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶次数].ToString();

                string str呕吐 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呕吐].ToString();
                util.ControlsHelper.SetComboxData(str呕吐, comboBoxEdit呕吐);

                string str大便 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便].ToString();
                util.ControlsHelper.SetComboxData(str大便, comboBoxEdit大便);

                textEdit大便次数.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便次数].ToString();

                textEdit体温.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.体温].ToString();
                SetLayoutControlItemFontColor(layoutControlItem体温, textEdit体温.Text);

                textEdit呼吸频率.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呼吸频率].ToString();
                SetLayoutControlItemFontColor(layoutControlItem呼吸频率, textEdit呼吸频率.Text);

                textEdit脉率.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脉率].ToString();
                SetLayoutControlItemFontColor(layoutControlItem脉率, textEdit脉率.Text);

                #region 设置"面色"
                string str面色 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色].ToString();
                string[] str面色Values = str面色.Split(c分隔符);
                for (int index = 0; index < str面色Values.Length; index++)
                {
                    if (str面色Values[index].Equals("1"))
                    {
                        checkEdit面色未检.Checked = true;
                        break;
                    }
                    else if (str面色Values[index].Equals("2"))
                    {
                        checkEdit面色红润.Checked = true;
                    }
                    else if (str面色Values[index].Equals("3"))
                    {
                        checkEdit面色黄染.Checked = true;
                    }
                    else if (str面色Values[index].Equals("99"))
                    {
                        checkEdit面色其他.Checked = true;
                        textEdit面色其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色其他].ToString();
                    }
                    else
                    { }
                }
                if (checkEdit面色未检.Checked || checkEdit面色红润.Checked || checkEdit面色黄染.Checked
                    || (checkEdit面色其他.Checked && !(string.IsNullOrWhiteSpace(textEdit面色其他.Text))))
                {
                    layoutControlItem面色.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    layoutControlItem面色.AppearanceItemCaption.ForeColor = Color.Red;
                }
                #endregion

                string str黄疸部位 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.黄疸部位].ToString();
                util.ControlsHelper.SetComboxData(str黄疸部位, comboBoxEdit黄疸部位);
                SetLayoutControlItemFontColor(layoutControlItem黄疸部位, str黄疸部位);

                textEdit前卤1.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟1].ToString();
                textEdit前卤2.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟2].ToString();
                if (string.IsNullOrWhiteSpace(textEdit前卤1.Text) || string.IsNullOrWhiteSpace(textEdit前卤2.Text))
                {
                    layoutControlItem前卤.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else
                {
                    layoutControlItem前卤.AppearanceItemCaption.ForeColor = Color.Blue;
                }

                string str前卤情况 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟状况].ToString();
                string str前卤情况其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟状况其他].ToString();
                SetComboBoxAndTextEditValue(str前卤情况, comboBoxEdit前卤情况, "99", textEdit前卤情况其他, str前卤情况其他);
                SetLayoutControlItemFontColor(layoutControlItem前卤情况, str前卤情况, "99", str前卤情况其他);

                string str眼外观 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.眼外观].ToString();
                string str眼外观其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.眼外观其他].ToString();
                SetComboBoxAndTextEditValue(str眼外观, comboBoxEdit眼外观, "99", textEdit眼外观其他, str眼外观其他);
                SetLayoutControlItemFontColor(layoutControlItem眼外观, str眼外观, "99", str眼外观其他);

                string str四肢活动度 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.四肢活动度].ToString();
                string str四肢活动度其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.四肢活动度其他].ToString();
                SetComboBoxAndTextEditValue(str四肢活动度, comboBoxEdit四肢活动度, "99", textEdit四肢活动度其他, str四肢活动度其他);
                SetLayoutControlItemFontColor(layoutControlItem四肢活动度, str四肢活动度, "99", str四肢活动度其他);

                string str耳外观 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.耳外观].ToString();
                string str耳外观其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.耳外观其他].ToString();
                SetComboBoxAndTextEditValue(str耳外观, comboBoxEdit耳外观, "99", textEdit耳外观其他, str耳外观其他);
                SetLayoutControlItemFontColor(layoutControlItem耳外观, str耳外观, "99", str耳外观其他);

                string str颈部包块 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.颈部包块].ToString();
                string str颈部包块其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.颈部包块其他].ToString();
                SetComboBoxAndTextEditValue(str颈部包块, comboBoxEdit颈部包块, "1", textEdit颈部包块其他, str颈部包块其他);
                SetLayoutControlItemFontColor(layoutControlItem颈部包块, str颈部包块, "1", str颈部包块其他);

                string str鼻 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.鼻].ToString();
                string str鼻其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.鼻其他].ToString();
                SetComboBoxAndTextEditValue(str鼻, comboBoxEdit鼻, "99", textEdit鼻其他, str鼻其他);
                SetLayoutControlItemFontColor(layoutControlItem鼻, str鼻, "99", str鼻其他);

                #region 设置"皮肤"
                string str皮肤 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤].ToString();
                string[] str皮肤Values = str皮肤.Split(c分隔符);
                for (int index = 0; index < str皮肤Values.Length; index++)
                {
                    if (str皮肤Values[index].Equals("1"))
                    {
                        checkEdit皮肤未见异常.Checked = true;
                        break;
                    }
                    else if (str皮肤Values[index].Equals("2"))
                    {
                        checkEdit皮肤湿疹.Checked = true;
                    }
                    else if (str皮肤Values[index].Equals("3"))
                    {
                        checkEdit皮肤糜烂.Checked = true;
                    }
                    else if (str皮肤Values[index].Equals("99"))
                    {
                        checkEdit皮肤其他.Checked = true;
                        textEdit皮肤其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤其他].ToString();
                    }
                    else { }
                }
                if (checkEdit皮肤未见异常.Checked || checkEdit皮肤湿疹.Checked || checkEdit皮肤糜烂.Checked
                    || (checkEdit皮肤其他.Checked && !(string.IsNullOrWhiteSpace(textEdit皮肤其他.Text))))
                {
                    layoutControlItem皮肤.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    layoutControlItem皮肤.AppearanceItemCaption.ForeColor = Color.Red;
                }
                #endregion

                string str口腔 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.口腔].ToString();
                string str口腔其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.口腔其他].ToString();
                SetComboBoxAndTextEditValue(str口腔, comboBoxEdit口腔, "99", textEdit口腔其他, str口腔其他);
                SetLayoutControlItemFontColor(layoutControlItem口腔, str口腔, "99", str口腔其他);

                string str肛门 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.肛门].ToString();
                string str肛门其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.肛门其他].ToString();
                SetComboBoxAndTextEditValue(str肛门, comboBoxEdit肛门, "99", textEdit肛门其他, str肛门其他);
                SetLayoutControlItemFontColor(layoutControlItem肛门, str肛门, "99", str肛门其他);

                string str心肺 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.心肺].ToString();
                string str心肺其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.心肺其他].ToString();
                SetComboBoxAndTextEditValue(str心肺, comboBoxEdit心肺, "99", textEdit心肺其他, str心肺其他);
                SetLayoutControlItemFontColor(layoutControlItem心肺, str心肺, "99", str心肺其他);

                string str外生殖器 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.外生殖器官].ToString();
                string str外生殖器其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.外生殖器官其他].ToString();
                SetComboBoxAndTextEditValue(str外生殖器, comboBoxEdit外生殖器, "99", textEdit外生殖器其他, str外生殖器其他);
                SetLayoutControlItemFontColor(layoutControlItem外生殖器官, str外生殖器, "99", str外生殖器其他);

                string str腹部 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.腹部].ToString();
                string str腹部其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.腹部其他].ToString();
                SetComboBoxAndTextEditValue(str腹部, comboBoxEdit腹部, "99", textEdit腹部其他, str腹部其他);
                SetLayoutControlItemFontColor(layoutControlItem腹部, str腹部, "99", str腹部其他);

                string str脊柱 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脊柱].ToString();
                string str脊柱其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脊柱其他].ToString();
                SetComboBoxAndTextEditValue(str脊柱, comboBoxEdit脊柱, "99", textEdit脊柱其他, str脊柱其他);
                SetLayoutControlItemFontColor(layoutControlItem脊柱, str脊柱, "99", str脊柱其他);

                string str脐带 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脐带].ToString();
                string str脐带其他 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脐带其他].ToString();
                SetComboBoxAndTextEditValue(str脐带, comboBoxEdit脐带, "99", textEdit脐带其他, str脐带其他);
                SetLayoutControlItemFontColor(layoutControlItem脐带, str脐带, "99", str脐带其他);

                string str转诊 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊].ToString();
                util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);
                if (str转诊.Equals("1"))
                {
                    textEdit转诊原因.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊原因].ToString();
                    textEdit转诊机构.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊机构及科室].ToString();
                    textEdit联系人.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系人].ToString();
                    textEdit转诊联系电话.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系电话].ToString();
                    string str结果 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.结果].ToString();
                    util.ControlsHelper.SetComboxData(str结果, comboBoxEdit是否到位); 
                }
                if (string.IsNullOrWhiteSpace(str转诊)
                    || (str转诊.Equals("1") && (string.IsNullOrWhiteSpace(textEdit转诊原因.Text) || string.IsNullOrWhiteSpace(textEdit转诊机构.Text))))
                {
                    layoutControlItem转诊.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else
                {
                    layoutControlItem转诊.AppearanceItemCaption.ForeColor = Color.Blue;
                }

                #region 设置"指导"
                string str指导 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导].ToString();
                string[] str指导Values = str指导.Split(c分隔符);
                for (int index = 0; index < str指导Values.Length; index++)
                {
                    switch (str指导Values[index])
                    {
                        case "1":
                            checkEdit指导科学喂养.Checked = true;
                            break;
                        case "5":
                            checkEdit指导生长发育.Checked = true;
                            break;
                        case "6":
                            checkEdit指导疾病预防.Checked = true;
                            break;
                        case "7":
                            checkEdit指导预防意外.Checked = true;
                            break;
                        case "8":
                            checkEdit指导口腔保健.Checked = true;
                            break;
                        case "9":
                            chk口服维生素D.Checked = true;
                            break;
                        case "100":
                            checkEdit指导其他.Checked = true;
                            textEdit指导其他.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导其他].ToString();
                            break;
                        default:
                            break;
                    }
                }

                if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
                {
                    layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else if (checkEdit指导科学喂养.Checked || checkEdit指导生长发育.Checked || checkEdit指导疾病预防.Checked
                    || checkEdit指导预防意外.Checked || checkEdit指导口腔保健.Checked || chk口服维生素D.Checked)
                {
                    layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Blue;
                }
                else
                {
                    layoutControlItem指导.AppearanceItemCaption.ForeColor = Color.Red;
                }
                #endregion

                dateEdit下次随访日期.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访日期].ToString();
                SetLayoutControlItemFontColor(layoutControlItem下次随访日期, dateEdit下次随访日期.Text);

                textEdit下次随访地点.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访地点].ToString();
                SetLayoutControlItemFontColor(layoutControlItem下次随访地点, textEdit下次随访地点.Text);

                textEdit随访医生.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.随访医生签名].ToString();
                SetLayoutControlItemFontColor(layoutControlItem随访医生, textEdit随访医生.Text);

                dateEdit随访日期.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.发生时间].ToString();
                SetLayoutControlItemFontColor(layoutControlItem随访日期, dateEdit随访日期.Text);

                textEdit录入时间.Text = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建时间].ToString();

                string str录入人 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建人].ToString();
                textEdit录入人.Text = new bllUser().Return用户名称(str录入人);

                bll地区档案 bll地区 = new bll地区档案();
                string str创建机构 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建机构].ToString();
                textEdit创建机构.Text = bll地区.Get名称By地区ID(str创建机构);

                string str当前所属机构 = m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.所属机构].ToString();
                textEdit当前所属机构.Text = bll地区.Get名称By地区ID(str当前所属机构);

                #region 变色控制
                //Set考核项颜色_new(layoutControl1, null);
                #endregion

            }

            //显示儿童基本信息
            textEdit儿童档案号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();

            string str姓名 = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = DESEncrypt.DES解密(str姓名);
            textEdit身份证号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号].ToString();
            textEdit性别.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别].ToString(); ;
            textEdit出生日期.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();
            textEdit住址.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0]["居住地址"].ToString();
            textEdit联系电话.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.联系电话].ToString();

            textEdit父姓名.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲姓名].ToString();
            textEdit父职业.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲工作单位].ToString();
            textEdit父联系电话.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲联系电话].ToString();
            textEdit父出生日期.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.父亲出生日期].ToString();
            
            textEdit母姓名.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲姓名].ToString();
            textEdit母职业.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲工作单位].ToString();
            textEdit母联系电话.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲联系电话].ToString();
            textEdit母出生日期.Text =  m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲出生日期].ToString();
            txt母亲身份证号.Text = m_ds儿童基本信息.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.母亲身份证号].ToString();
            textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
            textEdit最近更新时间.Text = strServerDate;
        }

        private void SetLayoutControlItemFontColor(DevExpress.XtraLayout.LayoutControlItem item, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        private void SetLayoutControlItemFontColor(DevExpress.XtraLayout.LayoutControlItem item, string value, string other, string otherValue)
        {
            if (string.IsNullOrWhiteSpace(value) || (value.Equals(other) && string.IsNullOrWhiteSpace(otherValue)))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        private void SetComboBoxAndTextEditValue(string value, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit, 
                                                 string str其他编码, DevExpress.XtraEditors.TextEdit textEdit, string str其他值)
        {
            util.ControlsHelper.SetComboxData(value, comboBoxEdit);
            if (value.Equals(str其他编码))
            {
                textEdit.Text = str其他值;
            }
        }

        #region 为控件绑定数据
        private void BindData()
        {
            //母亲妊娠期患病情况
            //DataView dv母妊娠期 = new DataView(DataDictCache.Cache.t常用字典);
            //dv母妊娠期.RowFilter = "P_FUN_CODE = 'rcqhbqk'";
            //dv母妊娠期.Sort = "P_CODE";
            //util.ControlsHelper.BindComboxData(dv母妊娠期.ToTable(), comboBoxEdit母妊娠患病情况, "P_CODE", "P_DESC");
            //util.ControlsHelper.SetComboxData("", comboBoxEdit母妊娠患病情况);
            BindDataForComboBox("rcqhbqk", comboBoxEdit母妊娠患病情况);

            //是否有畸形
            //DataView dv畸形 = new DataView(DataDictCache.Cache.t常用字典);
            //dv畸形.RowFilter = "P_FUN_CODE = 'yw_youwu'";
            //dv畸形.Sort = "P_CODE";
            //util.ControlsHelper.BindComboxData(dv畸形.ToTable(), comboBoxEdit是否畸形, "P_CODE", "P_DESC");
            //util.ControlsHelper.SetComboxData("", comboBoxEdit是否畸形);
            BindDataForComboBox("yw_youwu", comboBoxEdit是否畸形);

            //新生儿窒息
            BindDataForComboBox("yw_youwu", comboBoxEdit新生儿窒息);

            //新生儿听力筛查
            BindDataForComboBox("tl_tingli", comboBoxEdit听力筛查);

            //喂养方式
            BindDataForComboBox("wyfs", comboBoxEdit喂养方式);

            //呕吐
            BindDataForComboBox("yw_youwu", comboBoxEdit呕吐);

            //大便
            BindDataForComboBox("dabian", comboBoxEdit大便);

            //黄疸部位
            BindDataForComboBox("hd_location", comboBoxEdit黄疸部位);

            //前卤情况
            BindDataForComboBox("qxzk", comboBoxEdit前卤情况);

            //眼外观
            BindDataForComboBox("ywyc", comboBoxEdit眼外观);

            //四肢活动度
            BindDataForComboBox("ywyc", comboBoxEdit四肢活动度);

            //耳外观
            BindDataForComboBox("ywyc", comboBoxEdit耳外观);

            //颈部包块
            BindDataForComboBox("yw_youwu", comboBoxEdit颈部包块);

            //鼻
            BindDataForComboBox("ywyc", comboBoxEdit鼻);

            //口腔
            BindDataForComboBox("ywyc", comboBoxEdit口腔);

            //肛门
            BindDataForComboBox("ywyc", comboBoxEdit肛门);

            //心肺
            BindDataForComboBox("ywyc", comboBoxEdit心肺);

            //外生殖器官
            BindDataForComboBox("ywyc", comboBoxEdit外生殖器);

            //腹部
            BindDataForComboBox("ywyc", comboBoxEdit腹部);

            //脊柱
            BindDataForComboBox("ywyc", comboBoxEdit脊柱);

            //脐带
            BindDataForComboBox("jd_jidai", comboBoxEdit脐带);

            //转诊
            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);

            //胸部
            BindDataForComboBox("ywyc", comboBoxEdit胸部);

            //结果
            BindDataForComboBox("sfdw", comboBoxEdit是否到位);

        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '"+p_fun_code+"'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }
        #endregion

        #region 控件间的关联控制
        private void comboBoxEdit母妊娠患病情况_EditValueChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit母妊娠患病情况);
            //当值为“其他”时
            if(value!= null && value.Equals("99"))
            {
                textEdit母妊娠患病其他.Enabled = true;
            }
            else
            {
                textEdit母妊娠患病其他.Text = "";
                textEdit母妊娠患病其他.Enabled = false;
            }
        }

        private void comboBoxEdit是否畸形_EditValueChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit是否畸形);
            //当值为“有”的情况下
            if(value!=null && value.Equals("1"))
            {
                textEdit是否畸形其他.Enabled = true;
            }
            else
            {
                textEdit是否畸形其他.Text = "";
                textEdit是否畸形其他.Enabled = false;
            }
        }

        private void comboBoxEdit新生儿窒息_EditValueChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit新生儿窒息);
            if(value!=null && value.Equals("1"))
            {
                checkEdit1分钟.Enabled = true;
                //textEdit1分钟得分.Enabled = true;
                checkEdit5分钟.Enabled = true;
                //textEdit5分钟得分.Enabled = true;
                checkEditApgar不详.Enabled = true;
            }
            else
            {
                checkEdit1分钟.Enabled = false;
                textEdit1分钟得分.Enabled = false;
                checkEdit5分钟.Enabled = false;
                textEdit5分钟得分.Enabled = false;
                checkEditApgar不详.Enabled = false;
            }
        }

        private void checkEdit疾病筛查无_CheckedChanged(object sender, EventArgs e)
        {
            //疾病筛查是“无”的情况下
            if(checkEdit疾病筛查无.Checked)
            {
                checkEdit疾病筛查甲低.Checked = false;
                checkEdit疾病筛查甲低.Enabled = false;

                checkEdit疾病筛查苯丙酮尿.Checked = false;
                checkEdit疾病筛查苯丙酮尿.Enabled = false;

                checkEdit疾病_先天肾病.Checked = false;
                checkEdit疾病_先天肾病.Enabled = false;

                checkEdit疾病_葡萄糖症.Checked = false;
                checkEdit疾病_葡萄糖症.Enabled = false;

                checkEdit疾病_其他.Checked = false;
                checkEdit疾病_其他.Enabled = false;

                //textEdit疾病_其他.Text = "";
                //textEdit疾病_其他.Enabled = false;
            }
            else
            {
                checkEdit疾病筛查甲低.Enabled = true;
                checkEdit疾病筛查苯丙酮尿.Enabled = true;
                checkEdit疾病_先天肾病.Enabled = true;
                checkEdit疾病_葡萄糖症.Enabled = true;
                checkEdit疾病_其他.Enabled = true;
                //textEdit疾病_其他.Enabled = true;
            }
        }

        private void checkEdit其他_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit其他.Checked)
            {
                textEdit出生情况其他.Enabled = true;
            }
            else
            {
                textEdit出生情况其他.Text = "";
                textEdit出生情况其他.Enabled = false;
            }
        }

        private void checkEdit1分钟_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit1分钟.Checked)
            {
                textEdit1分钟得分.Enabled = true;
            }
            else
            {
                textEdit1分钟得分.Enabled = false;
            }
        }

        private void checkEdit5分钟_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit5分钟.Checked)
            {
                textEdit5分钟得分.Enabled = true;
            }
            else
            {
                textEdit5分钟得分.Enabled = false;
            }
        }

        private void checkEdit疾病_其他_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit疾病_其他.Checked)
            {
                textEdit疾病_其他.Enabled = true;
            }
            else
            {
                textEdit疾病_其他.Text = "";
                textEdit疾病_其他.Enabled = false;
            }
        }

        private void checkEdit面色未检_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit面色未检.Checked)
            {
                checkEdit面色红润.Checked = false;
                checkEdit面色红润.Enabled = false;

                checkEdit面色黄染.Checked = false;
                checkEdit面色黄染.Enabled = false;

                checkEdit面色其他.Checked = false;
                checkEdit面色其他.Enabled = false;

                //textEdit面色其他.Text = "";
                //textEdit面色其他.Enabled = false;
            }
            else
            {
                checkEdit面色红润.Enabled = true;
                checkEdit面色黄染.Enabled = true;
                checkEdit面色其他.Enabled = true;
            }
        }

        private void checkEdit面色其他_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit面色其他.Checked)
            {
                textEdit面色其他.Enabled = true;
            }
            else
            {
                textEdit面色其他.Text = "";
                textEdit面色其他.Enabled = false;
            }
        }

        private void checkEdit皮肤其他_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit皮肤其他.Checked)
            {
                textEdit皮肤其他.Enabled = true;
            }
            else
            {
                textEdit皮肤其他.Text = "";
                textEdit皮肤其他.Enabled = false;
            }
        }

        private void checkEdit皮肤未见异常_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit皮肤未见异常.Checked)
            {
                checkEdit皮肤湿疹.Checked = false;
                checkEdit皮肤湿疹.Enabled = false;

                checkEdit皮肤糜烂.Checked = false;
                checkEdit皮肤糜烂.Enabled = false;

                checkEdit皮肤其他.Checked = false;
                checkEdit皮肤其他.Enabled = false;
            }
            else
            {
                checkEdit皮肤湿疹.Enabled = true;
                checkEdit皮肤糜烂.Enabled = true;
                checkEdit皮肤其他.Enabled = true;
            }
        }

        private void comboBoxEdit前卤情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            //“其他”时允许编辑:99为“其他”的编码
            SetControlEnable("99", comboBoxEdit前卤情况, textEdit前卤情况其他);
        }

        private void SetControlEnable(string emptyvalue,
                                DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit,
                                DevExpress.XtraEditors.TextEdit textEdit)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit);
            if (value != null && value.Equals(emptyvalue))
            {
                textEdit.Enabled = true;
            }
            else
            {
                textEdit.Text = "";
                textEdit.Enabled = false;
            }
        }

        private void comboBoxEdit眼外观_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit眼外观, textEdit眼外观其他);
        }

        private void comboBoxEdit四肢活动度_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit四肢活动度, textEdit四肢活动度其他);
        }

        private void comboBoxEdit耳外观_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit耳外观, textEdit耳外观其他);
        }

        private void comboBoxEdit颈部包块_SelectedIndexChanged(object sender, EventArgs e)
        {
            //1是“有”的编码
            SetControlEnable("1", comboBoxEdit颈部包块, textEdit颈部包块其他);
        }

        private void comboBoxEdit鼻_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit鼻, textEdit鼻其他);
        }

        private void comboBoxEdit口腔_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit口腔, textEdit口腔其他);
        }

        private void comboBoxEdit肛门_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit肛门, textEdit肛门其他);
        }

        private void comboBoxEdit心肺_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit心肺, textEdit心肺其他);
        }

        private void comboBoxEdit外生殖器_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit外生殖器, textEdit外生殖器其他);
        }

        private void comboBoxEdit腹部_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit腹部, textEdit腹部其他);
        }

        private void comboBoxEdit脊柱_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“异常”的编码
            SetControlEnable("99", comboBoxEdit脊柱, textEdit脊柱其他);
        }

        private void comboBoxEdit脐带_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“其他”的编码
            SetControlEnable("99", comboBoxEdit脐带, textEdit脐带其他);
        }
        private void comboBoxEdit胸部_SelectedIndexChanged(object sender, EventArgs e)
        {
            //99是“其他”的编码
            SetControlEnable("99", comboBoxEdit胸部, textEdit胸部其他);
        }

        private void comboBoxEdit转诊_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);

            //1是“有”的编码
            if(value!=null && value.Equals("1"))
            {
                textEdit转诊原因.Enabled = true;
                textEdit转诊机构.Enabled = true;
                textEdit联系人.Enabled = true;
                textEdit转诊联系电话.Enabled = true;
                comboBoxEdit是否到位.Enabled = true;
            }
            else
            {
                textEdit转诊原因.Text = "";
                textEdit转诊原因.Enabled = false;
                textEdit转诊机构.Text = "";
                textEdit转诊机构.Enabled = false;
                textEdit联系人.Enabled = false;
                textEdit转诊联系电话.Enabled = false;
                comboBoxEdit是否到位.Enabled = false;
                textEdit联系人.Text ="";
                textEdit转诊联系电话.Text ="";
                comboBoxEdit是否到位.EditValue ="";
            }
        }
        #endregion

        #region 输入格式的验证
        private void textEdit1分钟得分_Leave(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(textEdit1分钟得分.Text))
            {
            }
            else
            {
                if ((Regex.IsMatch(textEdit1分钟得分.Text, @"^\d{1}$|^10$")))
                {
                }
                else
                {
                    textEdit1分钟得分.Text = "";
                    textEdit1分钟得分.Focus();
                    Msg.ShowInformation("只能输入一个最大值为10的整数。");
                }
            }
        }

        private void textEdit5分钟得分_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit5分钟得分.Text))
            {
            }
            else
            {
                if ((Regex.IsMatch(textEdit5分钟得分.Text, @"^\d{1}$|^10$")))
                {
                }
                else
                {
                    textEdit5分钟得分.Text = "";
                    textEdit5分钟得分.Focus();
                    Msg.ShowInformation("只能输入一个最大值为10的整数。");
                }
            }
        }

        private void MatchValue(DevExpress.XtraEditors.TextEdit textEdit)
        {
            if (string.IsNullOrWhiteSpace(textEdit.Text))
            { }
            else
            {
                if ((Regex.IsMatch(textEdit.Text, @"^\d{1,3}\.\d{1,2}$|^\d{1,3}$")))
                {
                }
                else
                {
                    Msg.ShowInformation("输入的数字只能是3位整数或两位小数!");
                    textEdit.Text = "";
                    textEdit.Focus();
                }
            }
        }

        private void textEdit出生体重_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit出生体重);
        }

        private void textEdit目前体重_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit目前体重);
        }

        private void textEdit出生生长_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit出生身长);
        }

        private void textEdit吃奶量_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit吃奶量);
        }

        private void textEdit吃奶次数_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit吃奶次数);
        }

        private void textEdit大便次数_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit大便次数);
        }

        private void textEdit体温_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit体温);
        }

        private void textEdit呼吸频率_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit呼吸频率);
        }

        private void textEdit脉率_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit脉率);
        }

        private void textEdit前卤1_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit前卤1);
        }

        private void textEdit前卤2_Leave(object sender, EventArgs e)
        {
            MatchValue(textEdit前卤2);
        }

        #endregion

        private void sbtn保存_Click(object sender, EventArgs e)
        {
            #region 校验
            try
            {
                if (string.IsNullOrWhiteSpace(textEdit体温.Text))
                { }
                else
                {
                    decimal tw = Convert.ToDecimal(textEdit体温.Text);
                    if (tw > 42 || tw < 34)
                    {
                        Msg.ShowInformation("体温必须大于35℃小于41℃");
                        textEdit体温.Focus();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.ShowInformation("体温必须大于35℃小于41℃");
                textEdit体温.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                Msg.ShowInformation("必须填写“下次随访日期”");
                dateEdit下次随访日期.Focus();
                return;
            }

            if (checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                Msg.ShowInformation("“指导”一栏如果勾选“其他”,请填写其他内容。");
                textEdit指导其他.Focus();
                return;
            }
            if (dateEdit随访日期.DateTime > Convert.ToDateTime(this.textEdit录入时间.Text))
            {
                Msg.ShowInformation("随访日期不能大于填写日期");
                dateEdit随访日期.Focus();
                return;
            }
            #endregion

            #region 儿童访视记录
            if (m_updateType == UpdateType.Add)
            {
                //保存前的确认
                if (Msg.AskQuestion("信息保存后，‘随访日期’与‘下次随访日期’将不允许修改，确认保存信息？"))
                {
                }
                else
                {
                    return;
                }

                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建时间] = textEdit录入时间.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建人] = Loginer.CurrentUser.用户编码;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.创建机构] = Loginer.CurrentUser.所属机构;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.所属机构] = Loginer.CurrentUser.所属机构;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.发生时间] = dateEdit随访日期.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访日期] = dateEdit下次随访日期.Text;
            }
            //else {  }

            #region 新版本添加
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.父亲身份证号] = txt父亲身份证号.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.母亲身份证号] = txt母亲身份证号.Text;
            SetDataSetWithComboBox(comboBoxEdit胸部, tb_儿童新生儿访视记录.胸部, textEdit胸部其他, tb_儿童新生儿访视记录.胸部其他, "99");
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.家长签名] = textEdit家长签名.Text;
            #endregion

            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.卡号] = textEdit卡号.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.修改时间] = textEdit最近更新时间.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.修改人] = Loginer.CurrentUser.用户编码;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生孕周] = textEdit出生孕周.Text;

            string str母妊娠情况 = util.ControlsHelper.GetComboxKey(comboBoxEdit母妊娠患病情况);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况] = str母妊娠情况;
            if (str母妊娠情况.Equals("99"))
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他] = textEdit母妊娠患病其他.Text;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.妊娠期患病疾病情况其他] = null;
            }

            SetDataSetWithComboBox(comboBoxEdit是否畸形, tb_儿童新生儿访视记录.是否有畸型, textEdit是否畸形其他, tb_儿童新生儿访视记录.是否有畸型其他, "1");

            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.助产机构名称] = textEdit助产机构.Text;

            string str出生情况Temp = "";
            if (checkEdit顺产.Checked)
            {
                str出生情况Temp += "1,";
            }
            if (checkEdit头吸.Checked)
            {
                str出生情况Temp += "2,";
            }
            if (checkEdit产钳.Checked)
            {
                str出生情况Temp += "3,";
            }
            if (checkEdit剖宫.Checked)
            {
                str出生情况Temp += "4,";
            }
            if (checkEdit双多胎.Checked)
            {
                str出生情况Temp += "5,";
            }
            if (checkEdit臀位.Checked)
            {
                str出生情况Temp += "6,";
            }
            if (checkEdit其他.Checked)
            {
                str出生情况Temp += "99,";
            }
            if (str出生情况Temp.Length == 0)
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况] = null;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况] = str出生情况Temp.Substring(0, str出生情况Temp.Length - 1);//减1的目的：去掉最后一个逗号
            }
            if (str出生情况Temp.Contains("99"))
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况其他] = textEdit出生情况其他.Text;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生情况其他] = null;
            }

            string str窒息 = util.ControlsHelper.GetComboxKey(comboBoxEdit新生儿窒息);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿窒息] = str窒息;
            if (str窒息.Equals("1"))
            {
                string strApgar评分 = "";
                if (checkEdit1分钟.Checked)
                {
                    strApgar评分 += "1,";
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分1] = textEdit1分钟得分.Text;
                }
                if (checkEdit5分钟.Checked)
                {
                    strApgar评分 += "2,";
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分5] = textEdit5分钟得分.Text;
                }
                if (checkEditApgar不详.Checked)
                {
                    strApgar评分 += "3,";
                }
                if (strApgar评分.Length == 0)
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分] = null;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分] = strApgar评分.Substring(0, strApgar评分.Length - 1);//减1去掉最后一个逗号
                }
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分] = null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分1] = null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.Apgar评分5] = null;
            }

            string str听力 = util.ControlsHelper.GetComboxKey(comboBoxEdit听力筛查);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿听力筛查] = str听力;
            //util.ControlsHelper.SetComboxData(str听力, comboBoxEdit新生儿窒息);
            

            //新生儿基本筛查
            if (checkEdit疾病筛查无.Checked)
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查] = "0";
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他] = null;
            }
            else
            {
                string str疾病筛查Temp = "";
                if (checkEdit疾病筛查甲低.Checked)
                {
                    str疾病筛查Temp += "1,";
                }
                if (checkEdit疾病筛查苯丙酮尿.Checked)
                {
                    str疾病筛查Temp += "2,";
                }
                if (checkEdit疾病_先天肾病.Checked)
                {
                    str疾病筛查Temp += "3,";
                }
                if (checkEdit疾病_葡萄糖症.Checked)
                {
                    str疾病筛查Temp += "4,";
                }
                if (checkEdit疾病_其他.Checked)
                {
                    str疾病筛查Temp += "99,";

                }
                if (str疾病筛查Temp.Length == 0)
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查] = null;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查] = str疾病筛查Temp.Substring(0, str疾病筛查Temp.Length - 1);
                }
                if (str疾病筛查Temp.Contains("99"))
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他] = textEdit疾病_其他.Text;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿疾病筛查其他] = null;
                }
            }

            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.新生儿出生体重] = textEdit出生体重.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.目前体重] = textEdit目前体重.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.出生身长] = textEdit出生身长.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.喂养方式] = util.ControlsHelper.GetComboxKey(comboBoxEdit喂养方式);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶量] = textEdit吃奶量.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.吃奶次数] = textEdit吃奶次数.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呕吐] = util.ControlsHelper.GetComboxKey(comboBoxEdit呕吐);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便] = util.ControlsHelper.GetComboxKey(comboBoxEdit大便);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.大便次数] = textEdit大便次数.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.体温] = textEdit体温.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.呼吸频率] = textEdit呼吸频率.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.脉率] = textEdit脉率.Text;

            //面色
            if (checkEdit面色未检.Checked)
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色] = "1";
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色其他] = null;
            }
            else
            {
                string str面色Temp = "";
                if (checkEdit面色红润.Checked)
                {
                    str面色Temp += "2,";
                }
                if (checkEdit面色黄染.Checked)
                {
                    str面色Temp += "3,";
                }
                if (checkEdit面色其他.Checked)
                {
                    str面色Temp += "99,";

                }
                if (str面色Temp.Length == 0)
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色] = null;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色] = str面色Temp.Substring(0, str面色Temp.Length - 1);
                }

                if (str面色Temp.Contains("99"))
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色其他] = textEdit面色其他.Text;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.面色其他] = null;
                }
            }

            //黄疸部位
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.黄疸部位] = util.ControlsHelper.GetComboxKey(comboBoxEdit黄疸部位);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟1] = textEdit前卤1.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.前囟2] = textEdit前卤2.Text;
            SetDataSetWithComboBox(comboBoxEdit前卤情况, tb_儿童新生儿访视记录.前囟状况, textEdit前卤情况其他, tb_儿童新生儿访视记录.前囟状况其他, "99");
            SetDataSetWithComboBox(comboBoxEdit眼外观, tb_儿童新生儿访视记录.眼外观, textEdit眼外观其他, tb_儿童新生儿访视记录.眼外观其他, "99");
            SetDataSetWithComboBox(comboBoxEdit四肢活动度, tb_儿童新生儿访视记录.四肢活动度, textEdit四肢活动度其他, tb_儿童新生儿访视记录.四肢活动度其他, "99");
            SetDataSetWithComboBox(comboBoxEdit耳外观, tb_儿童新生儿访视记录.耳外观, textEdit耳外观其他, tb_儿童新生儿访视记录.耳外观其他, "99");
            SetDataSetWithComboBox(comboBoxEdit颈部包块, tb_儿童新生儿访视记录.颈部包块, textEdit颈部包块其他, tb_儿童新生儿访视记录.颈部包块其他, "1");
            SetDataSetWithComboBox(comboBoxEdit鼻, tb_儿童新生儿访视记录.鼻, textEdit鼻其他, tb_儿童新生儿访视记录.鼻其他, "99");

            //皮肤
            if (checkEdit皮肤未见异常.Checked)
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤] = "1";
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤其他] = null;
            }
            else
            {
                string str皮肤Temp = "";
                if (checkEdit皮肤湿疹.Checked)
                {
                    str皮肤Temp += "2,";
                }
                if (checkEdit皮肤糜烂.Checked)
                {
                    str皮肤Temp += "3,";
                }
                if (checkEdit皮肤其他.Checked)
                {
                    str皮肤Temp += "99,";
                }
                if (str皮肤Temp.Length == 0)
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤] = null;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤] = str皮肤Temp.Substring(0, str皮肤Temp.Length - 1);
                }
                if (str皮肤Temp.Contains("99"))
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤其他] = textEdit皮肤其他.Text;
                }
                else
                {
                    m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.皮肤其他] = null;
                }
            }

            SetDataSetWithComboBox(comboBoxEdit口腔, tb_儿童新生儿访视记录.口腔, textEdit口腔其他, tb_儿童新生儿访视记录.口腔其他, "99");
            SetDataSetWithComboBox(comboBoxEdit肛门, tb_儿童新生儿访视记录.肛门, textEdit肛门其他, tb_儿童新生儿访视记录.肛门其他, "99");
            SetDataSetWithComboBox(comboBoxEdit心肺, tb_儿童新生儿访视记录.心肺, textEdit心肺其他, tb_儿童新生儿访视记录.心肺其他, "99");
            SetDataSetWithComboBox(comboBoxEdit外生殖器, tb_儿童新生儿访视记录.外生殖器官, textEdit外生殖器其他, tb_儿童新生儿访视记录.外生殖器官其他, "99");
            SetDataSetWithComboBox(comboBoxEdit腹部, tb_儿童新生儿访视记录.腹部, textEdit腹部其他, tb_儿童新生儿访视记录.腹部其他, "99");
            SetDataSetWithComboBox(comboBoxEdit脊柱, tb_儿童新生儿访视记录.脊柱, textEdit脊柱其他, tb_儿童新生儿访视记录.脊柱其他, "99");
            SetDataSetWithComboBox(comboBoxEdit脐带, tb_儿童新生儿访视记录.脐带, textEdit脐带其他, tb_儿童新生儿访视记录.脐带其他, "99");
            SetDataSetWithComboBox(comboBoxEdit胸部, tb_儿童新生儿访视记录.胸部, textEdit胸部其他, tb_儿童新生儿访视记录.胸部其他, "99");

            string str转诊Temp = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊] = str转诊Temp;
            if (str转诊Temp.Equals("1"))
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊原因] = textEdit转诊原因.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊机构及科室] = textEdit转诊机构.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系人] = textEdit联系人.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系电话] = textEdit转诊联系电话.Text;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.结果] = util.ControlsHelper.GetComboxKey(comboBoxEdit是否到位);
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊原因] = null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.转诊机构及科室] = null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系人] =null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.联系电话] =null;
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.结果] = null;
            }


            string str指导Temp = "";
            if (checkEdit指导科学喂养.Checked)
            {
                str指导Temp += "1,";
            }
            if (checkEdit指导生长发育.Checked)
            {
                str指导Temp += "5,";
            }
            if (checkEdit指导疾病预防.Checked)
            {
                str指导Temp += "6,";
            }
            if (checkEdit指导预防意外.Checked)
            {
                str指导Temp += "7,";
            }
            if (checkEdit指导口腔保健.Checked)
            {
                str指导Temp += "8,";
            }
            if(chk口服维生素D.Checked)
            {
                str指导Temp += "9,";
            }

            if (checkEdit指导其他.Checked)
            {
                str指导Temp += "100,";
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导其他] = textEdit指导其他.Text;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导其他] = DBNull.Value;
            }
            if (str指导Temp.Length == 0)
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导] = null;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.指导] = str指导Temp.Substring(0, str指导Temp.Length - 1);
            }
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.下次随访地点] = textEdit下次随访地点.Text;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.随访医生签名] = textEdit随访医生.Text;
            #endregion

            #region 计算完整度、缺项
            int i缺项 = 0;
            if (string.IsNullOrWhiteSpace(textEdit出生孕周.Text)) { i缺项++; }
            string str母妊娠 = util.ControlsHelper.GetComboxKey(comboBoxEdit母妊娠患病情况);
            if (string.IsNullOrWhiteSpace(str母妊娠) || (str母妊娠.Equals("99") && string.IsNullOrWhiteSpace(textEdit母妊娠患病其他.Text))) { i缺项++; }

            string str是否畸形 = util.ControlsHelper.GetComboxKey(comboBoxEdit是否畸形);
            if (string.IsNullOrWhiteSpace(str是否畸形) || (str是否畸形.Equals("1") && string.IsNullOrWhiteSpace(textEdit是否畸形其他.Text))) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit助产机构.Text)) { i缺项++; }
            if (checkEdit顺产.Checked || checkEdit头吸.Checked || checkEdit产钳.Checked || checkEdit剖宫.Checked
                || checkEdit双多胎.Checked || checkEdit臀位.Checked || (checkEdit其他.Checked) && !(string.IsNullOrWhiteSpace(textEdit出生情况其他.Text)))
            { }
            else
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit新生儿窒息))) { i缺项++; }
            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit听力筛查))) { i缺项++; }

            if (checkEdit疾病筛查无.Checked || checkEdit疾病筛查甲低.Checked || checkEdit疾病筛查苯丙酮尿.Checked
                || checkEdit疾病_先天肾病.Checked || checkEdit疾病_葡萄糖症.Checked || (checkEdit疾病_其他.Checked) && !(string.IsNullOrWhiteSpace(textEdit疾病_其他.Text)))
            { }
            else
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(textEdit出生体重.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit目前体重.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit出生身长.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit喂养方式))) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit体温.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit呼吸频率.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit脉率.Text)) { i缺项++; }

            if (checkEdit面色未检.Checked || checkEdit面色红润.Checked || checkEdit面色黄染.Checked
                || (checkEdit面色其他.Checked) && !(string.IsNullOrWhiteSpace(textEdit面色其他.Text)))
            { }
            else
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit黄疸部位))) { i缺项++; }

            if (string.IsNullOrWhiteSpace(textEdit前卤1.Text) || string.IsNullOrWhiteSpace(textEdit前卤2.Text)) { i缺项++; }

            string str前卤情况 = util.ControlsHelper.GetComboxKey(comboBoxEdit前卤情况);
            if (string.IsNullOrWhiteSpace(str前卤情况) || (str前卤情况.Equals("99") && string.IsNullOrWhiteSpace(textEdit前卤情况其他.Text)))
            { i缺项++; }

            string str眼外观 = util.ControlsHelper.GetComboxKey(comboBoxEdit眼外观);
            if (string.IsNullOrWhiteSpace(str眼外观) || (str眼外观.Equals("99") && string.IsNullOrWhiteSpace(textEdit眼外观其他.Text))) { i缺项++; }

            string str四肢活动度 = util.ControlsHelper.GetComboxKey(comboBoxEdit四肢活动度);
            if (string.IsNullOrWhiteSpace(str四肢活动度) || (str四肢活动度.Equals("99") && string.IsNullOrWhiteSpace(textEdit四肢活动度其他.Text))) { i缺项++; }

            string str耳外观 = util.ControlsHelper.GetComboxKey(comboBoxEdit耳外观);
            if (string.IsNullOrWhiteSpace(str耳外观) || (str耳外观.Equals("99") && string.IsNullOrWhiteSpace(textEdit耳外观其他.Text))) { i缺项++; }

            string str颈部包块 = util.ControlsHelper.GetComboxKey(comboBoxEdit颈部包块);
            if (string.IsNullOrWhiteSpace(str颈部包块) || (str颈部包块.Equals("1") && string.IsNullOrWhiteSpace(textEdit颈部包块其他.Text))) { i缺项++; }

            string str鼻 = util.ControlsHelper.GetComboxKey(comboBoxEdit鼻);
            if (string.IsNullOrWhiteSpace(str鼻) || (str鼻.Equals("99") && string.IsNullOrWhiteSpace(textEdit鼻其他.Text))) { i缺项++; }
            //if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit皮肤))) { i缺项++; }
            if (checkEdit皮肤未见异常.Checked || checkEdit皮肤湿疹.Checked || checkEdit皮肤糜烂.Checked
                || (checkEdit皮肤其他.Checked && !(string.IsNullOrWhiteSpace(textEdit皮肤其他.Text))))
            { }
            else
            {
                i缺项++;
            }

            string str口腔 = util.ControlsHelper.GetComboxKey(comboBoxEdit口腔);
            if (string.IsNullOrWhiteSpace(str口腔) || (str口腔.Equals("99") && string.IsNullOrWhiteSpace(textEdit口腔其他.Text))) { i缺项++; }

            string str肛门 = util.ControlsHelper.GetComboxKey(comboBoxEdit肛门);
            if (string.IsNullOrWhiteSpace(str肛门) || (str肛门.Equals("99") && string.IsNullOrWhiteSpace(textEdit肛门其他.Text))) { i缺项++; }

            string str心肺 = util.ControlsHelper.GetComboxKey(comboBoxEdit心肺);
            if (string.IsNullOrWhiteSpace(str心肺) || (str心肺.Equals("99") && string.IsNullOrWhiteSpace(textEdit心肺其他.Text))) { i缺项++; }

            string str外生殖器 = util.ControlsHelper.GetComboxKey(comboBoxEdit外生殖器);
            if (string.IsNullOrWhiteSpace(str外生殖器) || (str外生殖器.Equals("99") && string.IsNullOrWhiteSpace(textEdit外生殖器其他.Text))) { i缺项++; }

            string str腹部 = util.ControlsHelper.GetComboxKey(comboBoxEdit腹部);
            if (string.IsNullOrWhiteSpace(str腹部) || (str腹部.Equals("99") && string.IsNullOrWhiteSpace(textEdit腹部其他.Text))) { i缺项++; }

            string str脊柱 = util.ControlsHelper.GetComboxKey(comboBoxEdit脊柱);
            if (string.IsNullOrWhiteSpace(str脊柱) || (str脊柱.Equals("99") && string.IsNullOrWhiteSpace(textEdit脊柱其他.Text))) { i缺项++; }

            string str脐带 = util.ControlsHelper.GetComboxKey(comboBoxEdit脐带);
            if (string.IsNullOrWhiteSpace(str脐带) || (str脐带.Equals("99") && string.IsNullOrWhiteSpace(textEdit脐带其他.Text))) { i缺项++; }

            string str胸部 = util.ControlsHelper.GetComboxKey(comboBoxEdit胸部);
            if (string.IsNullOrWhiteSpace(str胸部) || (str胸部.Equals("99") && string.IsNullOrWhiteSpace(textEdit胸部其他.Text))) { i缺项++; }

            string str转诊 = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);
            if (string.IsNullOrWhiteSpace(str转诊) || (str转诊.Equals("1") && (string.IsNullOrWhiteSpace(textEdit转诊机构.Text) || string.IsNullOrWhiteSpace(textEdit转诊原因.Text)
                || string.IsNullOrWhiteSpace(textEdit联系人.Text) || string.IsNullOrWhiteSpace(textEdit转诊联系电话.Text) || string.IsNullOrWhiteSpace(comboBoxEdit是否到位.EditValue.ToString())
                )))
            { i缺项++; }
            //if (string.IsNullOrWhiteSpace(util.ControlsHelper.GetComboxKey(comboBoxEdit指导))) { i缺项++; }
            if (checkEdit指导科学喂养.Checked || checkEdit指导生长发育.Checked || checkEdit指导疾病预防.Checked
                || checkEdit指导预防意外.Checked || checkEdit指导口腔保健.Checked || chk口服维生素D.Checked)
            { }
            else
            {
                i缺项++;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit下次随访地点.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text)) { i缺项++; }
            if (string.IsNullOrWhiteSpace(textEdit家长签名.Text)) { i缺项++; }

            int i考核项数 = 40;
            int i完整度 = 100 * (i考核项数 - i缺项) / i考核项数;
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.完整度] = i完整度.ToString();
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][tb_儿童新生儿访视记录.缺项] = i缺项.ToString();
            #endregion

            #region 个人健康特征
            m_ds新生儿.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.新生儿随访记录] = i缺项.ToString() + "," + i完整度.ToString();
            #endregion

            //m_bll新生儿.WriteLog();
            m_bll新生儿.Save(m_ds新生儿);

            if (string.IsNullOrWhiteSpace(m_closeType))
            {
                UC新生儿家庭访视记录表_显示 ctl = new UC新生儿家庭访视记录表_显示(m_grdabh);
                ctl.Dock = DockStyle.Fill;
                ShowControl(ctl);
            }
            else
            {
                Common.frm个人健康.DialogResult = DialogResult.OK;
            }
        }

        private void SetDataSetWithComboBox(DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit, string columnName,
                                       DevExpress.XtraEditors.TextEdit textEdit, string columnName其他, string otherValue)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit);
            m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][columnName] = value;
            if (value.Equals(otherValue))
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][columnName其他] = textEdit.Text;
            }
            else
            {
                m_ds新生儿.Tables[tb_儿童新生儿访视记录.__TableName].Rows[0][columnName其他] = null;
            }
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit指导其他.Checked)
            {
                textEdit指导其他.Enabled = true;
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        private void comboBoxEdit喂养方式_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxEdit喂养方式.Text=="混合喂养" || comboBoxEdit喂养方式.Text=="母乳")
            {
                textEdit吃奶量.Enabled = false;
                textEdit吃奶次数.Enabled = false;
            }
            else
            {
                textEdit吃奶量.Enabled = true;
                textEdit吃奶次数.Enabled = true;
            }
        }
    }
}
