﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class report1岁以内儿童健康检查记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月闭合 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月前囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月后囟 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月颈部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月颈部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月颈部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine21 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月口腔 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月口腔 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月出牙数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月出牙数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月脐部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月脐部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine22 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine27 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine28 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine29 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine30 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine31 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月四肢 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine32 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine33 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine34 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine35 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine36 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月佝偻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月佝偻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月佝偻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月佝偻体症 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月佝偻体症 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月佝偻体症 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月佝偻体症 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月肛门 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月肛门 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月肛门 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月肛门 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月户外 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月维生素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月两次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt满月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt满月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt满月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt满月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt满月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月科学 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt8月生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt8月低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt8月口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt8月疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt8月预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月服务 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月服务 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月服务其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月服务 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.txt姓名,
            this.xrLine1,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel12,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.HeightF = 1110.417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 107.2917F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow58,
            this.xrTableRow59});
            this.xrTable1.SizeF = new System.Drawing.SizeF(750F, 930F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UsePadding = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 0.8D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "月龄";
            this.xrTableCell1.Weight = 1.5000000966390006D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "满月";
            this.xrTableCell2.Weight = 1.5000000966390006D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "3月龄";
            this.xrTableCell4.Weight = 1.5000000406901042D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "6月龄";
            this.xrTableCell5.Weight = 1.5000001305474153D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "8月龄";
            this.xrTableCell3.Weight = 1.4999999406602607D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 0.8D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "随访日期";
            this.xrTableCell6.Weight = 1.5000000966390006D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月随访});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.5000000966390006D;
            // 
            // txt满月随访
            // 
            this.txt满月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.589457E-05F);
            this.txt满月随访.Name = "txt满月随访";
            this.txt满月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月随访});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1.5000000406901042D;
            // 
            // txt3月随访
            // 
            this.txt3月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt3月随访.Name = "txt3月随访";
            this.txt3月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月随访});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 1.5000001305474153D;
            // 
            // txt6月随访
            // 
            this.txt6月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt6月随访.Name = "txt6月随访";
            this.txt6月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月随访});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 1.4999999406602607D;
            // 
            // txt8月随访
            // 
            this.txt8月随访.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt8月随访.Name = "txt8月随访";
            this.txt8月随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt8月随访.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell11,
            this.xrTableCell16,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell17,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell15});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 0.8D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "体重（kg)";
            this.xrTableCell19.Weight = 1.5000000788370782D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月体重});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.7500000483195004D;
            // 
            // txt满月体重
            // 
            this.txt满月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月体重.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 1.589457E-05F);
            this.txt满月体重.Name = "txt满月体重";
            this.txt满月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt满月体重.StylePriority.UseBorders = false;
            this.txt满月体重.StylePriority.UsePadding = false;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "上 中 下";
            this.xrTableCell16.Weight = 0.75000004831950018D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月体重});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.75000004831950029D;
            // 
            // txt3月体重
            // 
            this.txt3月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月体重.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt3月体重.Name = "txt3月体重";
            this.txt3月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt3月体重.StylePriority.UseBorders = false;
            this.txt3月体重.StylePriority.UsePadding = false;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "上 中 下";
            this.xrTableCell13.Weight = 0.75000001017252615D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月体重});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.75000006527370766D;
            // 
            // txt6月体重
            // 
            this.txt6月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月体重.LocationFloat = new DevExpress.Utils.PointFloat(5.000114F, 0F);
            this.txt6月体重.Name = "txt6月体重";
            this.txt6月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt6月体重.StylePriority.UseBorders = false;
            this.txt6月体重.StylePriority.UsePadding = false;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "上 中 下";
            this.xrTableCell14.Weight = 0.75000006527370766D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月体重});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.74999997033013033D;
            // 
            // txt8月体重
            // 
            this.txt8月体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月体重.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
            this.txt8月体重.Name = "txt8月体重";
            this.txt8月体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt8月体重.StylePriority.UseBorders = false;
            this.txt8月体重.StylePriority.UsePadding = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "上 中 下";
            this.xrTableCell15.Weight = 0.74999997033013033D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.8D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Text = "身长（cm)";
            this.xrTableCell20.Weight = 1.5000000788370782D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月身长});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.7500000483195004D;
            // 
            // txt满月身长
            // 
            this.txt满月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 0F);
            this.txt满月身长.Name = "txt满月身长";
            this.txt满月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt满月身长.StylePriority.UseBorders = false;
            this.txt满月身长.StylePriority.UsePadding = false;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "上 中 下";
            this.xrTableCell22.Weight = 0.75000004831950018D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月身长});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.75000004831950029D;
            // 
            // txt3月身长
            // 
            this.txt3月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月身长.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.txt3月身长.Name = "txt3月身长";
            this.txt3月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt3月身长.StylePriority.UseBorders = false;
            this.txt3月身长.StylePriority.UsePadding = false;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "上 中 下";
            this.xrTableCell24.Weight = 0.75000001017252615D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月身长});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.75000006527370766D;
            // 
            // txt6月身长
            // 
            this.txt6月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt6月身长.Name = "txt6月身长";
            this.txt6月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt6月身长.StylePriority.UseBorders = false;
            this.txt6月身长.StylePriority.UsePadding = false;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "上 中 下";
            this.xrTableCell26.Weight = 0.75000006527370766D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月身长});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Weight = 0.74999997033013033D;
            // 
            // txt8月身长
            // 
            this.txt8月身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt8月身长.Name = "txt8月身长";
            this.txt8月身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt8月身长.StylePriority.UseBorders = false;
            this.txt8月身长.StylePriority.UsePadding = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Text = "上 中 下";
            this.xrTableCell28.Weight = 0.74999997033013033D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.txt满月头围,
            this.txt3月头围,
            this.txt6月头围,
            this.txt8月头围});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.Weight = 0.8D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "头围（cm)";
            this.xrTableCell29.Weight = 1.5000000788370782D;
            // 
            // txt满月头围
            // 
            this.txt满月头围.Name = "txt满月头围";
            this.txt满月头围.Weight = 1.5000000565846758D;
            // 
            // txt3月头围
            // 
            this.txt3月头围.Name = "txt3月头围";
            this.txt3月头围.Weight = 1.5000000565846758D;
            // 
            // txt6月头围
            // 
            this.txt6月头围.Name = "txt6月头围";
            this.txt6月头围.Weight = 1.5000000565846758D;
            // 
            // txt8月头围
            // 
            this.txt8月头围.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月头围.Name = "txt8月头围";
            this.txt8月头围.StylePriority.UseBorders = false;
            this.txt8月头围.Weight = 1.5000000565846758D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell34,
            this.xrTableCell254,
            this.xrTableCell32,
            this.xrTableCell255,
            this.xrTableCell33,
            this.xrTableCell256,
            this.xrTableCell36,
            this.xrTableCell257,
            this.xrTableCell31});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.8D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Weight = 0.28000001012166392D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "面色";
            this.xrTableCell34.Weight = 1.2200000642649336D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.StylePriority.UsePadding = false;
            this.xrTableCell254.Text = "1红润 2黄染 3其他";
            this.xrTableCell254.Weight = 1.3000000506718952D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell32.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月面色});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Weight = 0.20000000591278047D;
            // 
            // txt满月面色
            // 
            this.txt满月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月面色.BorderWidth = 1F;
            this.txt满月面色.CanGrow = false;
            this.txt满月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月面色.LocationFloat = new DevExpress.Utils.PointFloat(0.79F, 4F);
            this.txt满月面色.Name = "txt满月面色";
            this.txt满月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月面色.StylePriority.UseBorders = false;
            this.txt满月面色.StylePriority.UseBorderWidth = false;
            this.txt满月面色.StylePriority.UseFont = false;
            this.txt满月面色.StylePriority.UsePadding = false;
            this.txt满月面色.StylePriority.UseTextAlignment = false;
            this.txt满月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.StylePriority.UsePadding = false;
            this.xrTableCell255.Text = "1红润 2黄染 3其他";
            this.xrTableCell255.Weight = 1.3000000506718952D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月面色});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Weight = 0.20000000591278047D;
            // 
            // txt3月面色
            // 
            this.txt3月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月面色.BorderWidth = 1F;
            this.txt3月面色.CanGrow = false;
            this.txt3月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt3月面色.Name = "txt3月面色";
            this.txt3月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月面色.StylePriority.UseBorders = false;
            this.txt3月面色.StylePriority.UseBorderWidth = false;
            this.txt3月面色.StylePriority.UseFont = false;
            this.txt3月面色.StylePriority.UsePadding = false;
            this.txt3月面色.StylePriority.UseTextAlignment = false;
            this.txt3月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.StylePriority.UsePadding = false;
            this.xrTableCell256.Text = "1红润 2黄染 3其他";
            this.xrTableCell256.Weight = 1.3000000517845154D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月面色});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Weight = 0.20000061737698807D;
            // 
            // txt6月面色
            // 
            this.txt6月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月面色.BorderWidth = 1F;
            this.txt6月面色.CanGrow = false;
            this.txt6月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.999996F);
            this.txt6月面色.Name = "txt6月面色";
            this.txt6月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月面色.StylePriority.UseBorders = false;
            this.txt6月面色.StylePriority.UseBorderWidth = false;
            this.txt6月面色.StylePriority.UseFont = false;
            this.txt6月面色.StylePriority.UsePadding = false;
            this.txt6月面色.StylePriority.UseTextAlignment = false;
            this.txt6月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.StylePriority.UsePadding = false;
            this.xrTableCell257.Text = "1红润 2黄染 3其他";
            this.xrTableCell257.Weight = 1.3000000517845154D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月面色});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 0.19999939667381331D;
            // 
            // txt8月面色
            // 
            this.txt8月面色.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月面色.BorderWidth = 1F;
            this.txt8月面色.CanGrow = false;
            this.txt8月面色.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000019F);
            this.txt8月面色.Name = "txt8月面色";
            this.txt8月面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月面色.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月面色.StylePriority.UseBorders = false;
            this.txt8月面色.StylePriority.UseBorderWidth = false;
            this.txt8月面色.StylePriority.UseFont = false;
            this.txt8月面色.StylePriority.UsePadding = false;
            this.txt8月面色.StylePriority.UseTextAlignment = false;
            this.txt8月面色.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell258,
            this.xrTableCell39,
            this.xrTableCell259,
            this.xrTableCell40,
            this.xrTableCell260,
            this.xrTableCell41,
            this.xrTableCell261,
            this.xrTableCell42});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.Weight = 0.8D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.28000001012166392D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Text = "皮肤";
            this.xrTableCell38.Weight = 1.2200000642649336D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell258.StylePriority.UsePadding = false;
            this.xrTableCell258.Text = "1红润 2黄染 3其他";
            this.xrTableCell258.Weight = 1.3000000506718952D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月皮肤});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.20000000591278044D;
            // 
            // txt满月皮肤
            // 
            this.txt满月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月皮肤.BorderWidth = 1F;
            this.txt满月皮肤.CanGrow = false;
            this.txt满月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0.79F, 4F);
            this.txt满月皮肤.Name = "txt满月皮肤";
            this.txt满月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月皮肤.StylePriority.UseBorders = false;
            this.txt满月皮肤.StylePriority.UseBorderWidth = false;
            this.txt满月皮肤.StylePriority.UseFont = false;
            this.txt满月皮肤.StylePriority.UsePadding = false;
            this.txt满月皮肤.StylePriority.UseTextAlignment = false;
            this.txt满月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell259.StylePriority.UsePadding = false;
            this.xrTableCell259.Text = "1红润 2黄染 3其他";
            this.xrTableCell259.Weight = 1.300000050671895D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月皮肤});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 0.20000000591278055D;
            // 
            // txt3月皮肤
            // 
            this.txt3月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月皮肤.BorderWidth = 1F;
            this.txt3月皮肤.CanGrow = false;
            this.txt3月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.000028F);
            this.txt3月皮肤.Name = "txt3月皮肤";
            this.txt3月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月皮肤.StylePriority.UseBorders = false;
            this.txt3月皮肤.StylePriority.UseBorderWidth = false;
            this.txt3月皮肤.StylePriority.UseFont = false;
            this.txt3月皮肤.StylePriority.UsePadding = false;
            this.txt3月皮肤.StylePriority.UseTextAlignment = false;
            this.txt3月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell260.StylePriority.UsePadding = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "1红润 2黄染 3其他";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell260.Weight = 1.3000012724876902D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月皮肤});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.1999993966738135D;
            // 
            // txt6月皮肤
            // 
            this.txt6月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月皮肤.BorderWidth = 1F;
            this.txt6月皮肤.CanGrow = false;
            this.txt6月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.000028F);
            this.txt6月皮肤.Name = "txt6月皮肤";
            this.txt6月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月皮肤.StylePriority.UseBorders = false;
            this.txt6月皮肤.StylePriority.UseBorderWidth = false;
            this.txt6月皮肤.StylePriority.UseFont = false;
            this.txt6月皮肤.StylePriority.UsePadding = false;
            this.txt6月皮肤.StylePriority.UseTextAlignment = false;
            this.txt6月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell261.StylePriority.UsePadding = false;
            this.xrTableCell261.Text = "1红润 2黄染 3其他";
            this.xrTableCell261.Weight = 1.3000000517845152D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月皮肤});
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.19999939667381328D;
            // 
            // txt8月皮肤
            // 
            this.txt8月皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月皮肤.BorderWidth = 1F;
            this.txt8月皮肤.CanGrow = false;
            this.txt8月皮肤.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月皮肤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000051F);
            this.txt8月皮肤.Name = "txt8月皮肤";
            this.txt8月皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月皮肤.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月皮肤.StylePriority.UseBorders = false;
            this.txt8月皮肤.StylePriority.UseBorderWidth = false;
            this.txt8月皮肤.StylePriority.UseFont = false;
            this.txt8月皮肤.StylePriority.UsePadding = false;
            this.txt8月皮肤.StylePriority.UseTextAlignment = false;
            this.txt8月皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell49,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 1.6D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Weight = 0.28000001012166392D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Text = "前囟";
            this.xrTableCell49.Weight = 1.2200000569534304D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 1.5000000638961788D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow16});
            this.xrTable5.SizeF = new System.Drawing.SizeF(148.5901F, 40F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell315,
            this.xrTableCell76});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Text = "1";
            this.xrTableCell73.Weight = 0.43308273856634294D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UsePadding = false;
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = "闭合";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell74.Weight = 0.62741943136389278D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "2";
            this.xrTableCell75.Weight = 0.394039242860579D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBorders = false;
            this.xrTableCell315.Text = "未闭";
            this.xrTableCell315.Weight = 0.77349249357863914D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月闭合});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell76.Weight = 0.77349249357863914D;
            // 
            // txt满月闭合
            // 
            this.txt满月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月闭合.BorderWidth = 1F;
            this.txt满月闭合.CanGrow = false;
            this.txt满月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月闭合.LocationFloat = new DevExpress.Utils.PointFloat(20F, 4F);
            this.txt满月闭合.Name = "txt满月闭合";
            this.txt满月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月闭合.StylePriority.UseBorders = false;
            this.txt满月闭合.StylePriority.UseBorderWidth = false;
            this.txt满月闭合.StylePriority.UseFont = false;
            this.txt满月闭合.StylePriority.UsePadding = false;
            this.txt满月闭合.StylePriority.UseTextAlignment = false;
            this.txt满月闭合.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月前囟});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.9393014475208924D;
            // 
            // txt满月前囟
            // 
            this.txt满月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月前囟.LocationFloat = new DevExpress.Utils.PointFloat(2.499994F, 0F);
            this.txt满月前囟.Name = "txt满月前囟";
            this.txt满月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月前囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt满月前囟.StylePriority.UseBorders = false;
            this.txt满月前囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "cm x";
            this.xrTableCell78.Weight = 0.7172400274953693D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月后囟});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.900584189262272D;
            // 
            // txt满月后囟
            // 
            this.txt满月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.583252F, 0F);
            this.txt满月后囟.Name = "txt满月后囟";
            this.txt满月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月后囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt满月后囟.StylePriority.UseBorders = false;
            this.txt满月后囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "cm";
            this.xrTableCell80.Weight = 0.44440073566955912D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 1.5000000565846756D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1.083374F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable3.SizeF = new System.Drawing.SizeF(148.5901F, 40F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell316,
            this.xrTableCell60});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Text = "1";
            this.xrTableCell57.Weight = 0.43308273856634294D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "闭合";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell58.Weight = 0.60553523003794152D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Text = "2";
            this.xrTableCell59.Weight = 0.41592313595834263D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBorders = false;
            this.xrTableCell316.Text = "未闭";
            this.xrTableCell316.Weight = 0.77349264769273285D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月闭合});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.77349264769273285D;
            // 
            // txt3月闭合
            // 
            this.txt3月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月闭合.BorderWidth = 1F;
            this.txt3月闭合.CanGrow = false;
            this.txt3月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月闭合.LocationFloat = new DevExpress.Utils.PointFloat(18.62F, 4F);
            this.txt3月闭合.Name = "txt3月闭合";
            this.txt3月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月闭合.StylePriority.UseBorders = false;
            this.txt3月闭合.StylePriority.UseBorderWidth = false;
            this.txt3月闭合.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月前囟});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Weight = 0.9393014475208924D;
            // 
            // txt3月前囟
            // 
            this.txt3月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月前囟.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0F);
            this.txt3月前囟.Name = "txt3月前囟";
            this.txt3月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月前囟.SizeF = new System.Drawing.SizeF(36.5F, 18F);
            this.txt3月前囟.StylePriority.UseBorders = false;
            this.txt3月前囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "cm x";
            this.xrTableCell62.Weight = 0.7172400274953693D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月后囟});
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.900584189262272D;
            // 
            // txt3月后囟
            // 
            this.txt3月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.583252F, 0F);
            this.txt3月后囟.Name = "txt3月后囟";
            this.txt3月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月后囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt3月后囟.StylePriority.UseBorders = false;
            this.txt3月后囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Text = "cm";
            this.xrTableCell64.Weight = 0.44440073566955912D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 1.5000006691615035D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable4.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14});
            this.xrTable4.SizeF = new System.Drawing.SizeF(150.0001F, 40F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell317,
            this.xrTableCell68});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Text = "1";
            this.xrTableCell65.Weight = 0.43308273856634294D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UsePadding = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "闭合";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell66.Weight = 0.81758684718137153D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Text = "2";
            this.xrTableCell67.Weight = 0.40017028795793935D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseBorders = false;
            this.xrTableCell317.Text = "未闭";
            this.xrTableCell317.Weight = 0.87849483036345721D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月闭合});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.47219169587898185D;
            // 
            // txt6月闭合
            // 
            this.txt6月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月闭合.BorderWidth = 1F;
            this.txt6月闭合.CanGrow = false;
            this.txt6月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月闭合.LocationFloat = new DevExpress.Utils.PointFloat(3.59745F, 3.999996F);
            this.txt6月闭合.Name = "txt6月闭合";
            this.txt6月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月闭合.StylePriority.UseBorders = false;
            this.txt6月闭合.StylePriority.UseBorderWidth = false;
            this.txt6月闭合.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月前囟});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Weight = 0.930472425439123D;
            // 
            // txt6月前囟
            // 
            this.txt6月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月前囟.LocationFloat = new DevExpress.Utils.PointFloat(2.499994F, 0F);
            this.txt6月前囟.Name = "txt6月前囟";
            this.txt6月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月前囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt6月前囟.StylePriority.UseBorders = false;
            this.txt6月前囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Text = "cm x";
            this.xrTableCell70.Weight = 0.72606904957713869D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月后囟});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.900584189262272D;
            // 
            // txt6月后囟
            // 
            this.txt6月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.583252F, 0F);
            this.txt6月后囟.Name = "txt6月后囟";
            this.txt6月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月后囟.SizeF = new System.Drawing.SizeF(44F, 18F);
            this.txt6月后囟.StylePriority.UseBorders = false;
            this.txt6月后囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Text = "cm";
            this.xrTableCell72.Weight = 0.44440073566955912D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 1.4999994484583286D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable6.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(1.409912F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable6.SizeF = new System.Drawing.SizeF(147F, 38F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell318,
            this.xrTableCell84});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Text = "1";
            this.xrTableCell81.Weight = 0.43308273856634294D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.StylePriority.UsePadding = false;
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.Text = "闭合";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell82.Weight = 0.72921283667891923D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Text = "2";
            this.xrTableCell83.Weight = 0.50615539221298866D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.Text = "未闭";
            this.xrTableCell318.Weight = 0.86088373661086015D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月闭合});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell84.Weight = 0.47219169587898185D;
            // 
            // txt8月闭合
            // 
            this.txt8月闭合.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月闭合.BorderWidth = 1F;
            this.txt8月闭合.CanGrow = false;
            this.txt8月闭合.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月闭合.LocationFloat = new DevExpress.Utils.PointFloat(4.715729F, 4.000028F);
            this.txt8月闭合.Name = "txt8月闭合";
            this.txt8月闭合.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月闭合.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月闭合.StylePriority.UseBorders = false;
            this.txt8月闭合.StylePriority.UseBorderWidth = false;
            this.txt8月闭合.StylePriority.UseFont = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月前囟});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.9393014475208924D;
            // 
            // txt8月前囟
            // 
            this.txt8月前囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月前囟.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0.9999847F);
            this.txt8月前囟.Name = "txt8月前囟";
            this.txt8月前囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月前囟.SizeF = new System.Drawing.SizeF(35.06012F, 16F);
            this.txt8月前囟.StylePriority.UseBorders = false;
            this.txt8月前囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Text = "cm x";
            this.xrTableCell86.Weight = 0.7172400274953693D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月后囟});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.900584189262272D;
            // 
            // txt8月后囟
            // 
            this.txt8月后囟.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月后囟.LocationFloat = new DevExpress.Utils.PointFloat(0.583252F, 0F);
            this.txt8月后囟.Name = "txt8月后囟";
            this.txt8月后囟.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月后囟.SizeF = new System.Drawing.SizeF(42.62274F, 18F);
            this.txt8月后囟.StylePriority.UseBorders = false;
            this.txt8月后囟.StylePriority.UsePadding = false;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Text = "cm";
            this.xrTableCell88.Weight = 0.44440073566955912D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrTableCell50,
            this.xrTableCell262,
            this.xrTableCell51,
            this.xrTableCell263,
            this.xrTableCell52,
            this.xrTableCell264,
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 0.80000000000000016D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 0.28000001012166392D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Text = "颈部包块";
            this.xrTableCell50.Weight = 1.2200000569534304D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Text = "1 有    2 无";
            this.xrTableCell262.Weight = 1.3000003595034404D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月颈部});
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.Weight = 0.1999997043927384D;
            // 
            // txt满月颈部
            // 
            this.txt满月颈部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月颈部.BorderWidth = 1F;
            this.txt满月颈部.CanGrow = false;
            this.txt满月颈部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月颈部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.txt满月颈部.Name = "txt满月颈部";
            this.txt满月颈部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月颈部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月颈部.StylePriority.UseBorders = false;
            this.txt满月颈部.StylePriority.UseBorderWidth = false;
            this.txt满月颈部.StylePriority.UseFont = false;
            this.txt满月颈部.StylePriority.UsePadding = false;
            this.txt满月颈部.StylePriority.UseTextAlignment = false;
            this.txt满月颈部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Text = "1 有    2 无";
            this.xrTableCell263.Weight = 1.3000003558476889D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月颈部});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Weight = 0.19999970073698681D;
            // 
            // txt3月颈部
            // 
            this.txt3月颈部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月颈部.BorderWidth = 1F;
            this.txt3月颈部.CanGrow = false;
            this.txt3月颈部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月颈部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt3月颈部.Name = "txt3月颈部";
            this.txt3月颈部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月颈部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月颈部.StylePriority.UseBorders = false;
            this.txt3月颈部.StylePriority.UseBorderWidth = false;
            this.txt3月颈部.StylePriority.UseFont = false;
            this.txt3月颈部.StylePriority.UsePadding = false;
            this.txt3月颈部.StylePriority.UseTextAlignment = false;
            this.txt3月颈部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Text = "1 有    2 无";
            this.xrTableCell264.Weight = 1.3000067656519763D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月颈部});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.Weight = 0.19999390350952739D;
            // 
            // txt6月颈部
            // 
            this.txt6月颈部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月颈部.BorderWidth = 1F;
            this.txt6月颈部.CanGrow = false;
            this.txt6月颈部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月颈部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt6月颈部.Name = "txt6月颈部";
            this.txt6月颈部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月颈部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月颈部.StylePriority.UseBorders = false;
            this.txt6月颈部.StylePriority.UseBorderWidth = false;
            this.txt6月颈部.StylePriority.UseFont = false;
            this.txt6月颈部.StylePriority.UsePadding = false;
            this.txt6月颈部.StylePriority.UseTextAlignment = false;
            this.txt6月颈部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine6,
            this.xrLine5,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2});
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Weight = 1.4999994484583286D;
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.BorderWidth = 2F;
            this.xrLine6.LineWidth = 2;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(102.1586F, 6.00001F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine6.StylePriority.UseBorders = false;
            this.xrLine6.StylePriority.UseBorderWidth = false;
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.BorderWidth = 2F;
            this.xrLine5.LineWidth = 2;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(87.15865F, 6.00001F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine5.StylePriority.UseBorders = false;
            this.xrLine5.StylePriority.UseBorderWidth = false;
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.BorderWidth = 2F;
            this.xrLine4.LineWidth = 2;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(42.15857F, 6.00001F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine4.StylePriority.UseBorders = false;
            this.xrLine4.StylePriority.UseBorderWidth = false;
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.BorderWidth = 2F;
            this.xrLine3.LineWidth = 2;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(72.15862F, 6.00001F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine3.StylePriority.UseBorders = false;
            this.xrLine3.StylePriority.UseBorderWidth = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.BorderWidth = 2F;
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(57.1586F, 6.00001F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine2.StylePriority.UseBorders = false;
            this.xrLine2.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell265,
            this.xrTableCell89,
            this.xrTableCell267,
            this.xrTableCell90,
            this.xrTableCell269,
            this.xrTableCell91,
            this.xrTableCell271,
            this.xrTableCell92});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 0.80000000000000016D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.Weight = 0.28000001012166392D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Text = "眼外观";
            this.xrTableCell56.Weight = 1.2200000569534304D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Text = "1未见异常 2异常";
            this.xrTableCell265.Weight = 1.3000003595034404D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月眼外观});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.1999997043927384D;
            // 
            // txt满月眼外观
            // 
            this.txt满月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月眼外观.BorderWidth = 1F;
            this.txt满月眼外观.CanGrow = false;
            this.txt满月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.txt满月眼外观.Name = "txt满月眼外观";
            this.txt满月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月眼外观.StylePriority.UseBorders = false;
            this.txt满月眼外观.StylePriority.UseBorderWidth = false;
            this.txt满月眼外观.StylePriority.UseFont = false;
            this.txt满月眼外观.StylePriority.UsePadding = false;
            this.txt满月眼外观.StylePriority.UseTextAlignment = false;
            this.txt满月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Text = "1未见异常 2异常";
            this.xrTableCell267.Weight = 1.3000003558476889D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月眼外观});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Weight = 0.19999970073698681D;
            // 
            // txt3月眼外观
            // 
            this.txt3月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月眼外观.BorderWidth = 1F;
            this.txt3月眼外观.CanGrow = false;
            this.txt3月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt3月眼外观.Name = "txt3月眼外观";
            this.txt3月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月眼外观.StylePriority.UseBorders = false;
            this.txt3月眼外观.StylePriority.UseBorderWidth = false;
            this.txt3月眼外观.StylePriority.UseFont = false;
            this.txt3月眼外观.StylePriority.UsePadding = false;
            this.txt3月眼外观.StylePriority.UseTextAlignment = false;
            this.txt3月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "1未见异常 2异常";
            this.xrTableCell269.Weight = 1.3000018828392774D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月眼外观});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.19999878632222612D;
            // 
            // txt6月眼外观
            // 
            this.txt6月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月眼外观.BorderWidth = 1F;
            this.txt6月眼外观.CanGrow = false;
            this.txt6月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 4.999987F);
            this.txt6月眼外观.Name = "txt6月眼外观";
            this.txt6月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月眼外观.StylePriority.UseBorders = false;
            this.txt6月眼外观.StylePriority.UseBorderWidth = false;
            this.txt6月眼外观.StylePriority.UseFont = false;
            this.txt6月眼外观.StylePriority.UsePadding = false;
            this.txt6月眼外观.StylePriority.UseTextAlignment = false;
            this.txt6月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Text = "1未见异常 2异常";
            this.xrTableCell271.Weight = 1.3000003569603089D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月眼外观});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.19999909149801964D;
            // 
            // txt8月眼外观
            // 
            this.txt8月眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月眼外观.BorderWidth = 1F;
            this.txt8月眼外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月眼外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt8月眼外观.Name = "txt8月眼外观";
            this.txt8月眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月眼外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月眼外观.StylePriority.UseBorders = false;
            this.txt8月眼外观.StylePriority.UseBorderWidth = false;
            this.txt8月眼外观.StylePriority.UseFont = false;
            this.txt8月眼外观.StylePriority.UsePadding = false;
            this.txt8月眼外观.StylePriority.UseTextAlignment = false;
            this.txt8月眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell266,
            this.xrTableCell95,
            this.xrTableCell268,
            this.xrTableCell96,
            this.xrTableCell270,
            this.xrTableCell97,
            this.xrTableCell272,
            this.xrTableCell98});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.80000000000000016D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.28000001012166392D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "耳外观";
            this.xrTableCell94.Weight = 1.2200000569534304D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Text = "1未见异常 2异常";
            this.xrTableCell266.Weight = 1.3000003595034404D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月耳外观});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.1999997043927384D;
            // 
            // txt满月耳外观
            // 
            this.txt满月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月耳外观.BorderWidth = 1F;
            this.txt满月耳外观.CanGrow = false;
            this.txt满月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.txt满月耳外观.Name = "txt满月耳外观";
            this.txt满月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月耳外观.StylePriority.UseBorders = false;
            this.txt满月耳外观.StylePriority.UseBorderWidth = false;
            this.txt满月耳外观.StylePriority.UseFont = false;
            this.txt满月耳外观.StylePriority.UsePadding = false;
            this.txt满月耳外观.StylePriority.UseTextAlignment = false;
            this.txt满月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Text = "1未见异常 2异常";
            this.xrTableCell268.Weight = 1.3000003558476889D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月耳外观});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.19999970073698681D;
            // 
            // txt3月耳外观
            // 
            this.txt3月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月耳外观.BorderWidth = 1F;
            this.txt3月耳外观.CanGrow = false;
            this.txt3月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0.001780192F, 5.000019F);
            this.txt3月耳外观.Name = "txt3月耳外观";
            this.txt3月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月耳外观.StylePriority.UseBorders = false;
            this.txt3月耳外观.StylePriority.UseBorderWidth = false;
            this.txt3月耳外观.StylePriority.UseFont = false;
            this.txt3月耳外观.StylePriority.UsePadding = false;
            this.txt3月耳外观.StylePriority.UseTextAlignment = false;
            this.txt3月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Text = "1未见异常 2异常";
            this.xrTableCell270.Weight = 1.3000024931908647D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月耳外观});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.19999817597063874D;
            // 
            // txt6月耳外观
            // 
            this.txt6月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月耳外观.BorderWidth = 1F;
            this.txt6月耳外观.CanGrow = false;
            this.txt6月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000019F);
            this.txt6月耳外观.Name = "txt6月耳外观";
            this.txt6月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月耳外观.StylePriority.UseBorders = false;
            this.txt6月耳外观.StylePriority.UseBorderWidth = false;
            this.txt6月耳外观.StylePriority.UseFont = false;
            this.txt6月耳外观.StylePriority.UsePadding = false;
            this.txt6月耳外观.StylePriority.UseTextAlignment = false;
            this.txt6月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Text = "1未见异常 2异常";
            this.xrTableCell272.Weight = 1.3000009673118964D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月耳外观});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 0.19999848114643215D;
            // 
            // txt8月耳外观
            // 
            this.txt8月耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月耳外观.BorderWidth = 1F;
            this.txt8月耳外观.CanGrow = false;
            this.txt8月耳外观.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月耳外观.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000051F);
            this.txt8月耳外观.Name = "txt8月耳外观";
            this.txt8月耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月耳外观.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月耳外观.StylePriority.UseBorders = false;
            this.txt8月耳外观.StylePriority.UseBorderWidth = false;
            this.txt8月耳外观.StylePriority.UseFont = false;
            this.txt8月耳外观.StylePriority.UsePadding = false;
            this.txt8月耳外观.StylePriority.UseTextAlignment = false;
            this.txt8月耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell273,
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 0.80000000000000016D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.Weight = 0.28000001012166392D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Text = "听力";
            this.xrTableCell100.Weight = 1.2200000569534304D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine7,
            this.xrLine8,
            this.xrLine9,
            this.xrLine10,
            this.xrLine11});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 1.5000000638961788D;
            // 
            // xrLine7
            // 
            this.xrLine7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine7.BorderWidth = 2F;
            this.xrLine7.LineWidth = 2;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine7.StylePriority.UseBorders = false;
            this.xrLine7.StylePriority.UseBorderWidth = false;
            // 
            // xrLine8
            // 
            this.xrLine8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine8.BorderWidth = 2F;
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine8.StylePriority.UseBorders = false;
            this.xrLine8.StylePriority.UseBorderWidth = false;
            // 
            // xrLine9
            // 
            this.xrLine9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine9.BorderWidth = 2F;
            this.xrLine9.LineWidth = 2;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine9.StylePriority.UseBorders = false;
            this.xrLine9.StylePriority.UseBorderWidth = false;
            // 
            // xrLine10
            // 
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.BorderWidth = 2F;
            this.xrLine10.LineWidth = 2;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 3.71F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine10.StylePriority.UseBorders = false;
            this.xrLine10.StylePriority.UseBorderWidth = false;
            // 
            // xrLine11
            // 
            this.xrLine11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine11.BorderWidth = 2F;
            this.xrLine11.LineWidth = 2;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine11.StylePriority.UseBorders = false;
            this.xrLine11.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine12,
            this.xrLine13,
            this.xrLine14,
            this.xrLine15,
            this.xrLine16});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 1.5000000565846756D;
            // 
            // xrLine12
            // 
            this.xrLine12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine12.BorderWidth = 2F;
            this.xrLine12.LineWidth = 2;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine12.StylePriority.UseBorders = false;
            this.xrLine12.StylePriority.UseBorderWidth = false;
            // 
            // xrLine13
            // 
            this.xrLine13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine13.BorderWidth = 2F;
            this.xrLine13.LineWidth = 2;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine13.StylePriority.UseBorders = false;
            this.xrLine13.StylePriority.UseBorderWidth = false;
            // 
            // xrLine14
            // 
            this.xrLine14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine14.BorderWidth = 2F;
            this.xrLine14.LineWidth = 2;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine14.StylePriority.UseBorders = false;
            this.xrLine14.StylePriority.UseBorderWidth = false;
            // 
            // xrLine15
            // 
            this.xrLine15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine15.BorderWidth = 2F;
            this.xrLine15.LineWidth = 2;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 3.71F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine15.StylePriority.UseBorders = false;
            this.xrLine15.StylePriority.UseBorderWidth = false;
            // 
            // xrLine16
            // 
            this.xrLine16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine16.BorderWidth = 2F;
            this.xrLine16.LineWidth = 2;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine16.StylePriority.UseBorders = false;
            this.xrLine16.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Text = "1通过 2未通过";
            this.xrTableCell273.Weight = 1.3000006621361027D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月听力});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.20000000702540088D;
            // 
            // txt6月听力
            // 
            this.txt6月听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月听力.BorderWidth = 1F;
            this.txt6月听力.CanGrow = false;
            this.txt6月听力.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月听力.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.710016F);
            this.txt6月听力.Name = "txt6月听力";
            this.txt6月听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月听力.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月听力.StylePriority.UseBorders = false;
            this.txt6月听力.StylePriority.UseBorderWidth = false;
            this.txt6月听力.StylePriority.UseFont = false;
            this.txt6月听力.StylePriority.UsePadding = false;
            this.txt6月听力.StylePriority.UseTextAlignment = false;
            this.txt6月听力.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine17,
            this.xrLine18,
            this.xrLine19,
            this.xrLine20,
            this.xrLine21});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 1.4999994484583286D;
            // 
            // xrLine17
            // 
            this.xrLine17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine17.BorderWidth = 2F;
            this.xrLine17.LineWidth = 2;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 3.71F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine17.StylePriority.UseBorders = false;
            this.xrLine17.StylePriority.UseBorderWidth = false;
            // 
            // xrLine18
            // 
            this.xrLine18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine18.BorderWidth = 2F;
            this.xrLine18.LineWidth = 2;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(82.50003F, 3.71F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine18.StylePriority.UseBorders = false;
            this.xrLine18.StylePriority.UseBorderWidth = false;
            // 
            // xrLine19
            // 
            this.xrLine19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine19.BorderWidth = 2F;
            this.xrLine19.LineWidth = 2;
            this.xrLine19.LocationFloat = new DevExpress.Utils.PointFloat(67.5F, 3.71F);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine19.StylePriority.UseBorders = false;
            this.xrLine19.StylePriority.UseBorderWidth = false;
            // 
            // xrLine20
            // 
            this.xrLine20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine20.BorderWidth = 2F;
            this.xrLine20.LineWidth = 2;
            this.xrLine20.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 3.71F);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine20.StylePriority.UseBorders = false;
            this.xrLine20.StylePriority.UseBorderWidth = false;
            // 
            // xrLine21
            // 
            this.xrLine21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine21.BorderWidth = 2F;
            this.xrLine21.LineWidth = 2;
            this.xrLine21.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 3.71F);
            this.xrLine21.Name = "xrLine21";
            this.xrLine21.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine21.StylePriority.UseBorders = false;
            this.xrLine21.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell274,
            this.xrTableCell107,
            this.xrTableCell277,
            this.xrTableCell108,
            this.xrTableCell280,
            this.xrTableCell109,
            this.xrTableCell283,
            this.xrTableCell110});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 0.80000000000000016D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Text = "体";
            this.xrTableCell105.Weight = 0.28000001012166392D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "口腔";
            this.xrTableCell106.Weight = 1.2200000569534304D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Text = "1未见异常 2异常";
            this.xrTableCell274.Weight = 1.3000003595034404D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月口腔});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Weight = 0.1999997043927384D;
            // 
            // txt满月口腔
            // 
            this.txt满月口腔.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月口腔.BorderWidth = 1F;
            this.txt满月口腔.CanGrow = false;
            this.txt满月口腔.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月口腔.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt满月口腔.Name = "txt满月口腔";
            this.txt满月口腔.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月口腔.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月口腔.StylePriority.UseBorders = false;
            this.txt满月口腔.StylePriority.UseBorderWidth = false;
            this.txt满月口腔.StylePriority.UseFont = false;
            this.txt满月口腔.StylePriority.UsePadding = false;
            this.txt满月口腔.StylePriority.UseTextAlignment = false;
            this.txt满月口腔.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Text = "1未见异常 2异常";
            this.xrTableCell277.Weight = 1.3000000506718952D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月口腔});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.20000000591278047D;
            // 
            // txt3月口腔
            // 
            this.txt3月口腔.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月口腔.BorderWidth = 1F;
            this.txt3月口腔.CanGrow = false;
            this.txt3月口腔.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月口腔.LocationFloat = new DevExpress.Utils.PointFloat(0.00184377F, 3.999996F);
            this.txt3月口腔.Name = "txt3月口腔";
            this.txt3月口腔.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月口腔.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月口腔.StylePriority.UseBorders = false;
            this.txt3月口腔.StylePriority.UseBorderWidth = false;
            this.txt3月口腔.StylePriority.UseFont = false;
            this.txt3月口腔.StylePriority.UsePadding = false;
            this.txt3月口腔.StylePriority.UseTextAlignment = false;
            this.txt3月口腔.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Text = "出牙数（颗）";
            this.xrTableCell280.Weight = 0.97500125926340631D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月出牙数});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Weight = 0.52499940989809724D;
            // 
            // txt6月出牙数
            // 
            this.txt6月出牙数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月出牙数.BorderWidth = 1F;
            this.txt6月出牙数.CanGrow = false;
            this.txt6月出牙数.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月出牙数.LocationFloat = new DevExpress.Utils.PointFloat(9.999939F, 0F);
            this.txt6月出牙数.Name = "txt6月出牙数";
            this.txt6月出牙数.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月出牙数.SizeF = new System.Drawing.SizeF(38.12531F, 17.58334F);
            this.txt6月出牙数.StylePriority.UseBorders = false;
            this.txt6月出牙数.StylePriority.UseBorderWidth = false;
            this.txt6月出牙数.StylePriority.UseFont = false;
            this.txt6月出牙数.StylePriority.UsePadding = false;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Text = "出牙数（颗）";
            this.xrTableCell283.Weight = 0.9583322522374863D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月出牙数});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Weight = 0.54166719622084236D;
            // 
            // txt8月出牙数
            // 
            this.txt8月出牙数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月出牙数.BorderWidth = 1F;
            this.txt8月出牙数.CanGrow = false;
            this.txt8月出牙数.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月出牙数.LocationFloat = new DevExpress.Utils.PointFloat(4.166748F, 0F);
            this.txt8月出牙数.Name = "txt8月出牙数";
            this.txt8月出牙数.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月出牙数.SizeF = new System.Drawing.SizeF(38.12531F, 17.58334F);
            this.txt8月出牙数.StylePriority.UseBorders = false;
            this.txt8月出牙数.StylePriority.UseBorderWidth = false;
            this.txt8月出牙数.StylePriority.UseFont = false;
            this.txt8月出牙数.StylePriority.UsePadding = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell275,
            this.xrTableCell113,
            this.xrTableCell278,
            this.xrTableCell114,
            this.xrTableCell281,
            this.xrTableCell115,
            this.xrTableCell284,
            this.xrTableCell116});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.80000000000000016D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Text = "格";
            this.xrTableCell111.Weight = 0.28000001012166392D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Text = "心肺";
            this.xrTableCell112.Weight = 1.2200000569534304D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Text = "1未见异常 2异常";
            this.xrTableCell275.Weight = 1.3000003595034404D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月心肺});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Weight = 0.1999997043927384D;
            // 
            // txt满月心肺
            // 
            this.txt满月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月心肺.BorderWidth = 1F;
            this.txt满月心肺.CanGrow = false;
            this.txt满月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt满月心肺.Name = "txt满月心肺";
            this.txt满月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月心肺.StylePriority.UseBorders = false;
            this.txt满月心肺.StylePriority.UseBorderWidth = false;
            this.txt满月心肺.StylePriority.UseFont = false;
            this.txt满月心肺.StylePriority.UsePadding = false;
            this.txt满月心肺.StylePriority.UseTextAlignment = false;
            this.txt满月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Text = "1未见异常 2异常";
            this.xrTableCell278.Weight = 1.3000003558476889D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月心肺});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.19999970073698681D;
            // 
            // txt3月心肺
            // 
            this.txt3月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月心肺.BorderWidth = 1F;
            this.txt3月心肺.CanGrow = false;
            this.txt3月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0.001780192F, 3.999996F);
            this.txt3月心肺.Name = "txt3月心肺";
            this.txt3月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月心肺.StylePriority.UseBorders = false;
            this.txt3月心肺.StylePriority.UseBorderWidth = false;
            this.txt3月心肺.StylePriority.UseFont = false;
            this.txt3月心肺.StylePriority.UsePadding = false;
            this.txt3月心肺.StylePriority.UseTextAlignment = false;
            this.txt3月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Text = "1未见异常 2异常";
            this.xrTableCell281.Weight = 1.3000031035424522D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月心肺});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.19999756561905147D;
            // 
            // txt6月心肺
            // 
            this.txt6月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月心肺.BorderWidth = 1F;
            this.txt6月心肺.CanGrow = false;
            this.txt6月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt6月心肺.Name = "txt6月心肺";
            this.txt6月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月心肺.StylePriority.UseBorders = false;
            this.txt6月心肺.StylePriority.UseBorderWidth = false;
            this.txt6月心肺.StylePriority.UseFont = false;
            this.txt6月心肺.StylePriority.UsePadding = false;
            this.txt6月心肺.StylePriority.UseTextAlignment = false;
            this.txt6月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Text = "1未见异常 2异常";
            this.xrTableCell284.Weight = 1.2437485236167287D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell116.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月心肺});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.25625092484159989D;
            // 
            // txt8月心肺
            // 
            this.txt8月心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月心肺.BorderWidth = 1F;
            this.txt8月心肺.CanGrow = false;
            this.txt8月心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月心肺.LocationFloat = new DevExpress.Utils.PointFloat(5.625216F, 3.999996F);
            this.txt8月心肺.Name = "txt8月心肺";
            this.txt8月心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月心肺.StylePriority.UseBorders = false;
            this.txt8月心肺.StylePriority.UseBorderWidth = false;
            this.txt8月心肺.StylePriority.UseFont = false;
            this.txt8月心肺.StylePriority.UsePadding = false;
            this.txt8月心肺.StylePriority.UseTextAlignment = false;
            this.txt8月心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell276,
            this.xrTableCell119,
            this.xrTableCell279,
            this.xrTableCell120,
            this.xrTableCell282,
            this.xrTableCell121,
            this.xrTableCell285,
            this.xrTableCell122});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.80000000000000016D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Text = "检";
            this.xrTableCell117.Weight = 0.28000001012166392D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "腹部";
            this.xrTableCell118.Weight = 1.2200000569534304D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Text = "1未见异常 2异常";
            this.xrTableCell276.Weight = 1.2659012370221936D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月腹部});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.2340988268739852D;
            // 
            // txt满月腹部
            // 
            this.txt满月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月腹部.BorderWidth = 1F;
            this.txt满月腹部.CanGrow = false;
            this.txt满月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月腹部.LocationFloat = new DevExpress.Utils.PointFloat(3.41F, 4F);
            this.txt满月腹部.Name = "txt满月腹部";
            this.txt满月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月腹部.StylePriority.UseBorders = false;
            this.txt满月腹部.StylePriority.UseBorderWidth = false;
            this.txt满月腹部.StylePriority.UseFont = false;
            this.txt满月腹部.StylePriority.UsePadding = false;
            this.txt满月腹部.StylePriority.UseTextAlignment = false;
            this.txt满月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "1未见异常 2异常";
            this.xrTableCell279.Weight = 1.3000003558476889D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月腹部});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.19999970073698681D;
            // 
            // txt3月腹部
            // 
            this.txt3月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月腹部.BorderWidth = 1F;
            this.txt3月腹部.CanGrow = false;
            this.txt3月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月腹部.LocationFloat = new DevExpress.Utils.PointFloat(0.001780192F, 3.999964F);
            this.txt3月腹部.Name = "txt3月腹部";
            this.txt3月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月腹部.StylePriority.UseBorders = false;
            this.txt3月腹部.StylePriority.UseBorderWidth = false;
            this.txt3月腹部.StylePriority.UseFont = false;
            this.txt3月腹部.StylePriority.UsePadding = false;
            this.txt3月腹部.StylePriority.UseTextAlignment = false;
            this.txt3月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Text = "1未见异常 2异常";
            this.xrTableCell282.Weight = 1.3000037138940395D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月腹部});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.19999695526746408D;
            // 
            // txt6月腹部
            // 
            this.txt6月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月腹部.BorderWidth = 1F;
            this.txt6月腹部.CanGrow = false;
            this.txt6月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt6月腹部.Name = "txt6月腹部";
            this.txt6月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月腹部.StylePriority.UseBorders = false;
            this.txt6月腹部.StylePriority.UseBorderWidth = false;
            this.txt6月腹部.StylePriority.UseFont = false;
            this.txt6月腹部.StylePriority.UsePadding = false;
            this.txt6月腹部.StylePriority.UseTextAlignment = false;
            this.txt6月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Text = "1未见异常 2异常";
            this.xrTableCell285.Weight = 1.2437485236167285D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月腹部});
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Weight = 0.25625092484159995D;
            // 
            // txt8月腹部
            // 
            this.txt8月腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月腹部.BorderWidth = 1F;
            this.txt8月腹部.CanGrow = false;
            this.txt8月腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月腹部.LocationFloat = new DevExpress.Utils.PointFloat(5.625216F, 3.999996F);
            this.txt8月腹部.Name = "txt8月腹部";
            this.txt8月腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月腹部.StylePriority.UseBorders = false;
            this.txt8月腹部.StylePriority.UseBorderWidth = false;
            this.txt8月腹部.StylePriority.UseFont = false;
            this.txt8月腹部.StylePriority.UsePadding = false;
            this.txt8月腹部.StylePriority.UseTextAlignment = false;
            this.txt8月腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell286,
            this.xrTableCell125,
            this.xrTableCell287,
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 1.6000000000000005D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.StylePriority.UseTextAlignment = false;
            this.xrTableCell123.Text = "查";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell123.Weight = 0.28000001012166392D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "脐部";
            this.xrTableCell124.Weight = 1.2200000569534304D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Multiline = true;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Text = "1未脱 2脱落 3脐部有渗出 4其他";
            this.xrTableCell286.Weight = 1.2659012370221936D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell125.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月脐部});
            this.xrTableCell125.Multiline = true;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Weight = 0.2340988268739852D;
            // 
            // txt满月脐部
            // 
            this.txt满月脐部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月脐部.BorderWidth = 1F;
            this.txt满月脐部.CanGrow = false;
            this.txt满月脐部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月脐部.LocationFloat = new DevExpress.Utils.PointFloat(3.41F, 13F);
            this.txt满月脐部.Name = "txt满月脐部";
            this.txt满月脐部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月脐部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月脐部.StylePriority.UseBorders = false;
            this.txt满月脐部.StylePriority.UseBorderWidth = false;
            this.txt满月脐部.StylePriority.UseFont = false;
            this.txt满月脐部.StylePriority.UsePadding = false;
            this.txt满月脐部.StylePriority.UseTextAlignment = false;
            this.txt满月脐部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Multiline = true;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Text = "1未脱 2脱落 3脐部有渗出 4其他";
            this.xrTableCell287.Weight = 1.2767346688658494D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell126.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月脐部});
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Weight = 0.2232653877188262D;
            // 
            // txt3月脐部
            // 
            this.txt3月脐部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月脐部.BorderWidth = 1F;
            this.txt3月脐部.CanGrow = false;
            this.txt3月脐部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月脐部.LocationFloat = new DevExpress.Utils.PointFloat(2.32652F, 13.00001F);
            this.txt3月脐部.Name = "txt3月脐部";
            this.txt3月脐部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月脐部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月脐部.StylePriority.UseBorders = false;
            this.txt3月脐部.StylePriority.UseBorderWidth = false;
            this.txt3月脐部.StylePriority.UseFont = false;
            this.txt3月脐部.StylePriority.UsePadding = false;
            this.txt3月脐部.StylePriority.UseTextAlignment = false;
            this.txt3月脐部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine22,
            this.xrLine23,
            this.xrLine24,
            this.xrLine25,
            this.xrLine26});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 1.5000006691615035D;
            // 
            // xrLine22
            // 
            this.xrLine22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine22.BorderWidth = 2F;
            this.xrLine22.LineWidth = 2;
            this.xrLine22.LocationFloat = new DevExpress.Utils.PointFloat(37.50002F, 13.71F);
            this.xrLine22.Name = "xrLine22";
            this.xrLine22.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine22.StylePriority.UseBorders = false;
            this.xrLine22.StylePriority.UseBorderWidth = false;
            // 
            // xrLine23
            // 
            this.xrLine23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine23.BorderWidth = 2F;
            this.xrLine23.LineWidth = 2;
            this.xrLine23.LocationFloat = new DevExpress.Utils.PointFloat(52.50004F, 13.71F);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine23.StylePriority.UseBorders = false;
            this.xrLine23.StylePriority.UseBorderWidth = false;
            // 
            // xrLine24
            // 
            this.xrLine24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine24.BorderWidth = 2F;
            this.xrLine24.LineWidth = 2;
            this.xrLine24.LocationFloat = new DevExpress.Utils.PointFloat(67.50006F, 13.71F);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine24.StylePriority.UseBorders = false;
            this.xrLine24.StylePriority.UseBorderWidth = false;
            // 
            // xrLine25
            // 
            this.xrLine25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine25.BorderWidth = 2F;
            this.xrLine25.LineWidth = 2;
            this.xrLine25.LocationFloat = new DevExpress.Utils.PointFloat(82.50009F, 13.71F);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine25.StylePriority.UseBorders = false;
            this.xrLine25.StylePriority.UseBorderWidth = false;
            // 
            // xrLine26
            // 
            this.xrLine26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine26.BorderWidth = 2F;
            this.xrLine26.LineWidth = 2;
            this.xrLine26.LocationFloat = new DevExpress.Utils.PointFloat(97.50005F, 13.71F);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine26.StylePriority.UseBorders = false;
            this.xrLine26.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine27,
            this.xrLine28,
            this.xrLine29,
            this.xrLine30,
            this.xrLine31});
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.Weight = 1.4999994484583286D;
            // 
            // xrLine27
            // 
            this.xrLine27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine27.BorderWidth = 2F;
            this.xrLine27.LineWidth = 2;
            this.xrLine27.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 13.71F);
            this.xrLine27.Name = "xrLine27";
            this.xrLine27.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine27.StylePriority.UseBorders = false;
            this.xrLine27.StylePriority.UseBorderWidth = false;
            // 
            // xrLine28
            // 
            this.xrLine28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine28.BorderWidth = 2F;
            this.xrLine28.LineWidth = 2;
            this.xrLine28.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 13.71F);
            this.xrLine28.Name = "xrLine28";
            this.xrLine28.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine28.StylePriority.UseBorders = false;
            this.xrLine28.StylePriority.UseBorderWidth = false;
            // 
            // xrLine29
            // 
            this.xrLine29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine29.BorderWidth = 2F;
            this.xrLine29.LineWidth = 2;
            this.xrLine29.LocationFloat = new DevExpress.Utils.PointFloat(67.5F, 13.71F);
            this.xrLine29.Name = "xrLine29";
            this.xrLine29.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine29.StylePriority.UseBorders = false;
            this.xrLine29.StylePriority.UseBorderWidth = false;
            // 
            // xrLine30
            // 
            this.xrLine30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine30.BorderWidth = 2F;
            this.xrLine30.LineWidth = 2;
            this.xrLine30.LocationFloat = new DevExpress.Utils.PointFloat(82.50003F, 13.71F);
            this.xrLine30.Name = "xrLine30";
            this.xrLine30.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine30.StylePriority.UseBorders = false;
            this.xrLine30.StylePriority.UseBorderWidth = false;
            // 
            // xrLine31
            // 
            this.xrLine31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine31.BorderWidth = 2F;
            this.xrLine31.LineWidth = 2;
            this.xrLine31.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 13.71F);
            this.xrLine31.Name = "xrLine31";
            this.xrLine31.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine31.StylePriority.UseBorders = false;
            this.xrLine31.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell288,
            this.xrTableCell131,
            this.xrTableCell289,
            this.xrTableCell132,
            this.xrTableCell290,
            this.xrTableCell133,
            this.xrTableCell291,
            this.xrTableCell134});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseBorders = false;
            this.xrTableRow25.Weight = 0.80000000000000027D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Weight = 0.28000001012166392D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "四肢";
            this.xrTableCell130.Weight = 1.2200000569534304D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Text = "1未见异常 2异常";
            this.xrTableCell288.Weight = 1.2659012370221936D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月四肢});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.2340988268739852D;
            // 
            // txt满月四肢
            // 
            this.txt满月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月四肢.BorderWidth = 1F;
            this.txt满月四肢.CanGrow = false;
            this.txt满月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月四肢.LocationFloat = new DevExpress.Utils.PointFloat(3.41F, 5F);
            this.txt满月四肢.Name = "txt满月四肢";
            this.txt满月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月四肢.StylePriority.UseBorders = false;
            this.txt满月四肢.StylePriority.UseBorderWidth = false;
            this.txt满月四肢.StylePriority.UseFont = false;
            this.txt满月四肢.StylePriority.UsePadding = false;
            this.txt满月四肢.StylePriority.UseTextAlignment = false;
            this.txt满月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Text = "1未见异常 2异常";
            this.xrTableCell289.Weight = 1.2767346688658494D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月四肢});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.22326538771882626D;
            // 
            // txt3月四肢
            // 
            this.txt3月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月四肢.BorderWidth = 1F;
            this.txt3月四肢.CanGrow = false;
            this.txt3月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月四肢.LocationFloat = new DevExpress.Utils.PointFloat(2.32652F, 5.000051F);
            this.txt3月四肢.Name = "txt3月四肢";
            this.txt3月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月四肢.StylePriority.UseBorders = false;
            this.txt3月四肢.StylePriority.UseBorderWidth = false;
            this.txt3月四肢.StylePriority.UseFont = false;
            this.txt3月四肢.StylePriority.UsePadding = false;
            this.txt3月四肢.StylePriority.UseTextAlignment = false;
            this.txt3月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Text = "1未见异常 2异常";
            this.xrTableCell290.Weight = 1.3000043242456267D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月四肢});
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.1999963449158767D;
            // 
            // txt6月四肢
            // 
            this.txt6月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月四肢.BorderWidth = 1F;
            this.txt6月四肢.CanGrow = false;
            this.txt6月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月四肢.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 5.000019F);
            this.txt6月四肢.Name = "txt6月四肢";
            this.txt6月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月四肢.StylePriority.UseBorders = false;
            this.txt6月四肢.StylePriority.UseBorderWidth = false;
            this.txt6月四肢.StylePriority.UseFont = false;
            this.txt6月四肢.StylePriority.UsePadding = false;
            this.txt6月四肢.StylePriority.UseTextAlignment = false;
            this.txt6月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Text = "1未见异常 2异常";
            this.xrTableCell291.Weight = 1.2437479132651412D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月四肢});
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Weight = 0.25625153519318722D;
            // 
            // txt8月四肢
            // 
            this.txt8月四肢.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月四肢.BorderWidth = 1F;
            this.txt8月四肢.CanGrow = false;
            this.txt8月四肢.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月四肢.LocationFloat = new DevExpress.Utils.PointFloat(5.625216F, 5.000082F);
            this.txt8月四肢.Name = "txt8月四肢";
            this.txt8月四肢.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月四肢.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月四肢.StylePriority.UseBorders = false;
            this.txt8月四肢.StylePriority.UseBorderWidth = false;
            this.txt8月四肢.StylePriority.UseFont = false;
            this.txt8月四肢.StylePriority.UsePadding = false;
            this.txt8月四肢.StylePriority.UseTextAlignment = false;
            this.txt8月四肢.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell292,
            this.xrTableCell138,
            this.xrTableCell293,
            this.xrTableCell139,
            this.xrTableCell294,
            this.xrTableCell140});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseBorders = false;
            this.xrTableRow26.Weight = 1.6000000000000005D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.Weight = 0.28000001012166392D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "可疑佝偻病症\r\n状";
            this.xrTableCell136.Weight = 1.2200000569534304D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine32,
            this.xrLine33,
            this.xrLine34,
            this.xrLine35,
            this.xrLine36});
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 1.5000000638961788D;
            // 
            // xrLine32
            // 
            this.xrLine32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine32.BorderWidth = 2F;
            this.xrLine32.LineWidth = 2;
            this.xrLine32.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 13.71F);
            this.xrLine32.Name = "xrLine32";
            this.xrLine32.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine32.StylePriority.UseBorders = false;
            this.xrLine32.StylePriority.UseBorderWidth = false;
            // 
            // xrLine33
            // 
            this.xrLine33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine33.BorderWidth = 2F;
            this.xrLine33.LineWidth = 2;
            this.xrLine33.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 13.71F);
            this.xrLine33.Name = "xrLine33";
            this.xrLine33.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine33.StylePriority.UseBorders = false;
            this.xrLine33.StylePriority.UseBorderWidth = false;
            // 
            // xrLine34
            // 
            this.xrLine34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine34.BorderWidth = 2F;
            this.xrLine34.LineWidth = 2;
            this.xrLine34.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 13.71F);
            this.xrLine34.Name = "xrLine34";
            this.xrLine34.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine34.StylePriority.UseBorders = false;
            this.xrLine34.StylePriority.UseBorderWidth = false;
            // 
            // xrLine35
            // 
            this.xrLine35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine35.BorderWidth = 2F;
            this.xrLine35.LineWidth = 2;
            this.xrLine35.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 13.71F);
            this.xrLine35.Name = "xrLine35";
            this.xrLine35.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine35.StylePriority.UseBorders = false;
            this.xrLine35.StylePriority.UseBorderWidth = false;
            // 
            // xrLine36
            // 
            this.xrLine36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine36.BorderWidth = 2F;
            this.xrLine36.LineWidth = 2;
            this.xrLine36.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 13.71F);
            this.xrLine36.Name = "xrLine36";
            this.xrLine36.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine36.StylePriority.UseBorders = false;
            this.xrLine36.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Multiline = true;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Text = "1无      2夜惊\r\n3多汉    4烦燥";
            this.xrTableCell292.Weight = 1.2767349740416429D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell138.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月佝偻});
            this.xrTableCell138.Multiline = true;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Weight = 0.22326508254303268D;
            // 
            // txt3月佝偻
            // 
            this.txt3月佝偻.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月佝偻.BorderWidth = 1F;
            this.txt3月佝偻.CanGrow = false;
            this.txt3月佝偻.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月佝偻.LocationFloat = new DevExpress.Utils.PointFloat(2.32652F, 12.70828F);
            this.txt3月佝偻.Name = "txt3月佝偻";
            this.txt3月佝偻.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月佝偻.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月佝偻.StylePriority.UseBorders = false;
            this.txt3月佝偻.StylePriority.UseBorderWidth = false;
            this.txt3月佝偻.StylePriority.UseFont = false;
            this.txt3月佝偻.StylePriority.UsePadding = false;
            this.txt3月佝偻.StylePriority.UseTextAlignment = false;
            this.txt3月佝偻.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Multiline = true;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Text = "1无      2夜惊\r\n3多汉    4烦燥";
            this.xrTableCell293.Weight = 1.300004934597214D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月佝偻});
            this.xrTableCell139.Multiline = true;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.19999573456428954D;
            // 
            // txt6月佝偻
            // 
            this.txt6月佝偻.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月佝偻.BorderWidth = 1F;
            this.txt6月佝偻.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月佝偻.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12.70828F);
            this.txt6月佝偻.Name = "txt6月佝偻";
            this.txt6月佝偻.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月佝偻.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月佝偻.StylePriority.UseBorders = false;
            this.txt6月佝偻.StylePriority.UseBorderWidth = false;
            this.txt6月佝偻.StylePriority.UseFont = false;
            this.txt6月佝偻.StylePriority.UsePadding = false;
            this.txt6月佝偻.StylePriority.UseTextAlignment = false;
            this.txt6月佝偻.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Multiline = true;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Text = "1无      2夜惊\r\n3多汉    4烦燥";
            this.xrTableCell294.Weight = 1.2664529923140133D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月佝偻});
            this.xrTableCell140.Multiline = true;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Weight = 0.23354645614431535D;
            // 
            // txt8月佝偻
            // 
            this.txt8月佝偻.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月佝偻.BorderWidth = 1F;
            this.txt8月佝偻.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月佝偻.LocationFloat = new DevExpress.Utils.PointFloat(3.354708F, 12.70828F);
            this.txt8月佝偻.Name = "txt8月佝偻";
            this.txt8月佝偻.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月佝偻.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月佝偻.StylePriority.UseBorders = false;
            this.txt8月佝偻.StylePriority.UseBorderWidth = false;
            this.txt8月佝偻.StylePriority.UseFont = false;
            this.txt8月佝偻.StylePriority.UsePadding = false;
            this.txt8月佝偻.StylePriority.UseTextAlignment = false;
            this.txt8月佝偻.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell295,
            this.xrTableCell143,
            this.xrTableCell296,
            this.xrTableCell144,
            this.xrTableCell297,
            this.xrTableCell145,
            this.xrTableCell298,
            this.xrTableCell146});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 1.6000000000000005D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.28000001012166392D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Multiline = true;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Text = "可疑佝偻病体\r\n症";
            this.xrTableCell142.Weight = 1.2200000569534304D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Multiline = true;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Text = "1无 2颅骨软化 3方颅 4枕秃";
            this.xrTableCell295.Weight = 1.2659012370221936D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月佝偻体症});
            this.xrTableCell143.Multiline = true;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.2340988268739852D;
            // 
            // txt满月佝偻体症
            // 
            this.txt满月佝偻体症.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月佝偻体症.BorderWidth = 1F;
            this.txt满月佝偻体症.CanGrow = false;
            this.txt满月佝偻体症.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月佝偻体症.LocationFloat = new DevExpress.Utils.PointFloat(4.2F, 12F);
            this.txt满月佝偻体症.Name = "txt满月佝偻体症";
            this.txt满月佝偻体症.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月佝偻体症.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月佝偻体症.StylePriority.UseBorders = false;
            this.txt满月佝偻体症.StylePriority.UseBorderWidth = false;
            this.txt满月佝偻体症.StylePriority.UseFont = false;
            this.txt满月佝偻体症.StylePriority.UsePadding = false;
            this.txt满月佝偻体症.StylePriority.UseTextAlignment = false;
            this.txt满月佝偻体症.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Multiline = true;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Text = "1无 2颅骨软化 3方颅 4枕秃";
            this.xrTableCell296.Weight = 1.2767349740416432D;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell144.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月佝偻体症});
            this.xrTableCell144.Multiline = true;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.22326508254303257D;
            // 
            // txt3月佝偻体症
            // 
            this.txt3月佝偻体症.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月佝偻体症.BorderWidth = 1F;
            this.txt3月佝偻体症.CanGrow = false;
            this.txt3月佝偻体症.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月佝偻体症.LocationFloat = new DevExpress.Utils.PointFloat(2.32652F, 12.00002F);
            this.txt3月佝偻体症.Name = "txt3月佝偻体症";
            this.txt3月佝偻体症.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月佝偻体症.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月佝偻体症.StylePriority.UseBorders = false;
            this.txt3月佝偻体症.StylePriority.UseBorderWidth = false;
            this.txt3月佝偻体症.StylePriority.UseFont = false;
            this.txt3月佝偻体症.StylePriority.UsePadding = false;
            this.txt3月佝偻体症.StylePriority.UseTextAlignment = false;
            this.txt3月佝偻体症.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell297.Multiline = true;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell297.StylePriority.UseFont = false;
            this.xrTableCell297.StylePriority.UsePadding = false;
            this.xrTableCell297.Text = "1肋串珠 2肋外翻 3肋软骨沟 4鸡胸5 手镯征";
            this.xrTableCell297.Weight = 1.3000055449488013D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell145.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月佝偻体症});
            this.xrTableCell145.Multiline = true;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.Weight = 0.19999512421270216D;
            // 
            // txt6月佝偻体症
            // 
            this.txt6月佝偻体症.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月佝偻体症.BorderWidth = 1F;
            this.txt6月佝偻体症.CanGrow = false;
            this.txt6月佝偻体症.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月佝偻体症.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999974F);
            this.txt6月佝偻体症.Name = "txt6月佝偻体症";
            this.txt6月佝偻体症.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月佝偻体症.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月佝偻体症.StylePriority.UseBorders = false;
            this.txt6月佝偻体症.StylePriority.UseBorderWidth = false;
            this.txt6月佝偻体症.StylePriority.UseFont = false;
            this.txt6月佝偻体症.StylePriority.UsePadding = false;
            this.txt6月佝偻体症.StylePriority.UseTextAlignment = false;
            this.txt6月佝偻体症.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell298.Multiline = true;
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell298.StylePriority.UseFont = false;
            this.xrTableCell298.StylePriority.UsePadding = false;
            this.xrTableCell298.Text = "1肋串珠 2肋外翻 3肋软骨沟 4鸡胸5手镯征";
            this.xrTableCell298.Weight = 1.2664536026656004D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月佝偻体症});
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.23354584579272808D;
            // 
            // txt8月佝偻体症
            // 
            this.txt8月佝偻体症.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月佝偻体症.BorderWidth = 1F;
            this.txt8月佝偻体症.CanGrow = false;
            this.txt8月佝偻体症.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月佝偻体症.LocationFloat = new DevExpress.Utils.PointFloat(3.354708F, 12.00008F);
            this.txt8月佝偻体症.Name = "txt8月佝偻体症";
            this.txt8月佝偻体症.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月佝偻体症.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月佝偻体症.StylePriority.UseBorders = false;
            this.txt8月佝偻体症.StylePriority.UseBorderWidth = false;
            this.txt8月佝偻体症.StylePriority.UseFont = false;
            this.txt8月佝偻体症.StylePriority.UsePadding = false;
            this.txt8月佝偻体症.StylePriority.UseTextAlignment = false;
            this.txt8月佝偻体症.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell299,
            this.xrTableCell149,
            this.xrTableCell300,
            this.xrTableCell150,
            this.xrTableCell301,
            this.xrTableCell151,
            this.xrTableCell302,
            this.xrTableCell152});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.StylePriority.UseBorders = false;
            this.xrTableRow28.Weight = 0.80000000000000027D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.28000001012166392D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Text = "肛门/外生殖";
            this.xrTableCell148.Weight = 1.2200000569534304D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.Text = "1未见异常 2异常";
            this.xrTableCell299.Weight = 1.2659012370221936D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell149.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月肛门});
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 0.2340988268739852D;
            // 
            // txt满月肛门
            // 
            this.txt满月肛门.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月肛门.BorderWidth = 1F;
            this.txt满月肛门.CanGrow = false;
            this.txt满月肛门.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月肛门.LocationFloat = new DevExpress.Utils.PointFloat(4.2F, 3F);
            this.txt满月肛门.Name = "txt满月肛门";
            this.txt满月肛门.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月肛门.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月肛门.StylePriority.UseBorders = false;
            this.txt满月肛门.StylePriority.UseBorderWidth = false;
            this.txt满月肛门.StylePriority.UseFont = false;
            this.txt满月肛门.StylePriority.UsePadding = false;
            this.txt满月肛门.StylePriority.UseTextAlignment = false;
            this.txt满月肛门.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Text = "1未见异常 2异常";
            this.xrTableCell300.Weight = 1.2767349740416432D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell150.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月肛门});
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Weight = 0.22326508254303257D;
            // 
            // txt3月肛门
            // 
            this.txt3月肛门.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月肛门.BorderWidth = 1F;
            this.txt3月肛门.CanGrow = false;
            this.txt3月肛门.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月肛门.LocationFloat = new DevExpress.Utils.PointFloat(2.32652F, 2.999973F);
            this.txt3月肛门.Name = "txt3月肛门";
            this.txt3月肛门.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月肛门.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月肛门.StylePriority.UseBorders = false;
            this.txt3月肛门.StylePriority.UseBorderWidth = false;
            this.txt3月肛门.StylePriority.UseFont = false;
            this.txt3月肛门.StylePriority.UsePadding = false;
            this.txt3月肛门.StylePriority.UseTextAlignment = false;
            this.txt3月肛门.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.Text = "1未见异常 2异常";
            this.xrTableCell301.Weight = 1.3000061553003888D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月肛门});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 0.19999451386111478D;
            // 
            // txt6月肛门
            // 
            this.txt6月肛门.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月肛门.BorderWidth = 1F;
            this.txt6月肛门.CanGrow = false;
            this.txt6月肛门.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月肛门.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 3.000005F);
            this.txt6月肛门.Name = "txt6月肛门";
            this.txt6月肛门.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月肛门.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月肛门.StylePriority.UseBorders = false;
            this.txt6月肛门.StylePriority.UseBorderWidth = false;
            this.txt6月肛门.StylePriority.UseFont = false;
            this.txt6月肛门.StylePriority.UsePadding = false;
            this.txt6月肛门.StylePriority.UseTextAlignment = false;
            this.txt6月肛门.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Text = "1未见异常 2异常";
            this.xrTableCell302.Weight = 1.2664545181929816D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月肛门});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.23354493026534695D;
            // 
            // txt8月肛门
            // 
            this.txt8月肛门.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月肛门.BorderWidth = 1F;
            this.txt8月肛门.CanGrow = false;
            this.txt8月肛门.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月肛门.LocationFloat = new DevExpress.Utils.PointFloat(3.354581F, 2.999973F);
            this.txt8月肛门.Name = "txt8月肛门";
            this.txt8月肛门.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月肛门.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月肛门.StylePriority.UseBorders = false;
            this.txt8月肛门.StylePriority.UseBorderWidth = false;
            this.txt8月肛门.StylePriority.UseFont = false;
            this.txt8月肛门.StylePriority.UsePadding = false;
            this.txt8月肛门.StylePriority.UseTextAlignment = false;
            this.txt8月肛门.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell159,
            this.xrTableCell155,
            this.xrTableCell160,
            this.xrTableCell156,
            this.xrTableCell161,
            this.xrTableCell157,
            this.xrTableCell162,
            this.xrTableCell158});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.Weight = 0.80000000000000027D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.28000001012166392D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "血红蛋白值 ";
            this.xrTableCell154.Weight = 1.2200000569534304D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月血红});
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Weight = 0.92590107059966165D;
            // 
            // txt满月血红
            // 
            this.txt满月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月血红.LocationFloat = new DevExpress.Utils.PointFloat(4.999985F, 0F);
            this.txt满月血红.Name = "txt满月血红";
            this.txt满月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt满月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Text = "g/L";
            this.xrTableCell155.Weight = 0.57409868812072362D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月血红});
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.9218011827438819D;
            // 
            // txt3月血红
            // 
            this.txt3月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月血红.LocationFloat = new DevExpress.Utils.PointFloat(4.589971F, 0F);
            this.txt3月血红.Name = "txt3月血红";
            this.txt3月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt3月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Text = "g/L";
            this.xrTableCell156.Weight = 0.578199484192381D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月血红});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.92263553447638935D;
            // 
            // txt6月血红
            // 
            this.txt6月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月血红.LocationFloat = new DevExpress.Utils.PointFloat(4.673449F, 2.000046F);
            this.txt6月血红.Name = "txt6月血红";
            this.txt6月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt6月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Text = "g/L";
            this.xrTableCell157.Weight = 0.57736391398193943D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月血红});
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.92590091546863307D;
            // 
            // txt8月血红
            // 
            this.txt8月血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月血红.LocationFloat = new DevExpress.Utils.PointFloat(4.99986F, 0F);
            this.txt8月血红.Name = "txt8月血红";
            this.txt8月血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt8月血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "g/L";
            this.xrTableCell158.Weight = 0.5740994485170765D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 0.80000000000000027D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "户外活动";
            this.xrTableCell164.Weight = 1.5000000670750944D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月户外});
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Weight = 0.92590107059966165D;
            // 
            // txt满月户外
            // 
            this.txt满月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 2.000014F);
            this.txt满月户外.Name = "txt满月户外";
            this.txt满月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月户外.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt满月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Text = "小时/日";
            this.xrTableCell166.Weight = 0.57000002462387067D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月户外});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Weight = 0.92589999882863194D;
            // 
            // txt3月户外
            // 
            this.txt3月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.409973F, 0F);
            this.txt3月户外.Name = "txt3月户外";
            this.txt3月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月户外.SizeF = new System.Drawing.SizeF(87.18005F, 18F);
            this.txt3月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Text = "小时/日";
            this.xrTableCell168.Weight = 0.57493440837593091D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月户外});
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Weight = 0.925899999941252D;
            // 
            // txt6月户外
            // 
            this.txt6月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.326599F, 2F);
            this.txt6月户外.Name = "txt6月户外";
            this.txt6月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月户外.SizeF = new System.Drawing.SizeF(87.26349F, 18F);
            this.txt6月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "小时/日";
            this.xrTableCell170.Weight = 0.57736437174562993D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月户外});
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 0.92589999994125194D;
            // 
            // txt8月户外
            // 
            this.txt8月户外.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月户外.LocationFloat = new DevExpress.Utils.PointFloat(5.000114F, 2.000014F);
            this.txt8月户外.Name = "txt8月户外";
            this.txt8月户外.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月户外.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt8月户外.StylePriority.UseBorders = false;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Text = "小时/日";
            this.xrTableCell172.Weight = 0.57410036404445752D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell180});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 0.80000000000000027D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "服用维生素D";
            this.xrTableCell163.Weight = 1.5000000670750944D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月维生素});
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Weight = 0.92590107059966165D;
            // 
            // txt满月维生素
            // 
            this.txt满月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 0F);
            this.txt满月维生素.Name = "txt满月维生素";
            this.txt满月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月维生素.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt满月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Text = "IU/日";
            this.xrTableCell174.Weight = 0.57000002462387067D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月维生素});
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.92589999882863194D;
            // 
            // txt3月维生素
            // 
            this.txt3月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.409973F, 0F);
            this.txt3月维生素.Name = "txt3月维生素";
            this.txt3月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月维生素.SizeF = new System.Drawing.SizeF(87.17993F, 18F);
            this.txt3月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Text = "IU/日";
            this.xrTableCell176.Weight = 0.57493440837593091D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月维生素});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.925899999941252D;
            // 
            // txt6月维生素
            // 
            this.txt6月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.326599F, 0F);
            this.txt6月维生素.Name = "txt6月维生素";
            this.txt6月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月维生素.SizeF = new System.Drawing.SizeF(87.26343F, 18F);
            this.txt6月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "IU/日";
            this.xrTableCell178.Weight = 0.57736437174562993D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月维生素});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.92589999994125194D;
            // 
            // txt8月维生素
            // 
            this.txt8月维生素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月维生素.LocationFloat = new DevExpress.Utils.PointFloat(5.000114F, 1.99995F);
            this.txt8月维生素.Name = "txt8月维生素";
            this.txt8月维生素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月维生素.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt8月维生素.StylePriority.UseBorders = false;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Text = "IU/日";
            this.xrTableCell180.Weight = 0.57410036404445752D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181,
            this.xrTableCell303,
            this.xrTableCell182,
            this.xrTableCell304,
            this.xrTableCell184,
            this.xrTableCell305,
            this.xrTableCell186,
            this.xrTableCell306,
            this.xrTableCell188});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseBorders = false;
            this.xrTableRow32.Weight = 0.80000000000000027D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "发育评估";
            this.xrTableCell181.Weight = 1.5000000670750944D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.Text = "1 通过   2 未过";
            this.xrTableCell303.Weight = 1.250000204165783D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell182.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月发育});
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Weight = 0.24590088962723811D;
            // 
            // txt满月发育
            // 
            this.txt满月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月发育.BorderWidth = 1F;
            this.txt满月发育.CanGrow = false;
            this.txt满月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月发育.LocationFloat = new DevExpress.Utils.PointFloat(5F, 3F);
            this.txt满月发育.Name = "txt满月发育";
            this.txt满月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月发育.StylePriority.UseBorders = false;
            this.txt满月发育.StylePriority.UseBorderWidth = false;
            this.txt满月发育.StylePriority.UseFont = false;
            this.txt满月发育.StylePriority.UsePadding = false;
            this.txt满月发育.StylePriority.UseTextAlignment = false;
            this.txt满月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Text = "1 通过   2 未过";
            this.xrTableCell304.Weight = 1.2540994769016032D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell184.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月发育});
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.24673462878291785D;
            // 
            // txt3月发育
            // 
            this.txt3月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月发育.BorderWidth = 1F;
            this.txt3月发育.CanGrow = false;
            this.txt3月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月发育.LocationFloat = new DevExpress.Utils.PointFloat(5.001831F, 2.999973F);
            this.txt3月发育.Name = "txt3月发育";
            this.txt3月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月发育.StylePriority.UseBorders = false;
            this.txt3月发育.StylePriority.UseBorderWidth = false;
            this.txt3月发育.StylePriority.UseFont = false;
            this.txt3月发育.StylePriority.UsePadding = false;
            this.txt3月发育.StylePriority.UseTextAlignment = false;
            this.txt3月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.Text = "1 通过   2 未过";
            this.xrTableCell305.Weight = 1.2811786393295477D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell186.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月发育});
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.22208679935999198D;
            // 
            // txt6月发育
            // 
            this.txt6月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月发育.BorderWidth = 1F;
            this.txt6月发育.CanGrow = false;
            this.txt6月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月发育.LocationFloat = new DevExpress.Utils.PointFloat(2.208583F, 3.000005F);
            this.txt6月发育.Name = "txt6月发育";
            this.txt6月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月发育.StylePriority.UseBorders = false;
            this.txt6月发育.StylePriority.UseBorderWidth = false;
            this.txt6月发育.StylePriority.UseFont = false;
            this.txt6月发育.StylePriority.UsePadding = false;
            this.txt6月发育.StylePriority.UseTextAlignment = false;
            this.txt6月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Text = "1 通过   2 未过";
            this.xrTableCell306.Weight = 1.2837449388724469D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell188.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月发育});
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Weight = 0.21625466106115809D;
            // 
            // txt8月发育
            // 
            this.txt8月发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月发育.BorderWidth = 1F;
            this.txt8月发育.CanGrow = false;
            this.txt8月发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月发育.LocationFloat = new DevExpress.Utils.PointFloat(1.625633F, 3.000005F);
            this.txt8月发育.Name = "txt8月发育";
            this.txt8月发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月发育.StylePriority.UseBorders = false;
            this.txt8月发育.StylePriority.UseBorderWidth = false;
            this.txt8月发育.StylePriority.UseFont = false;
            this.txt8月发育.StylePriority.UsePadding = false;
            this.txt8月发育.StylePriority.UseTextAlignment = false;
            this.txt8月发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell307,
            this.xrTableCell185,
            this.xrTableCell308,
            this.xrTableCell187,
            this.xrTableCell309,
            this.xrTableCell189,
            this.xrTableCell310,
            this.xrTableCell190});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 0.80000000000000027D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "两次随访间患病情况";
            this.xrTableCell183.Weight = 1.5000000670750944D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.Text = "1 未患病 2 患病";
            this.xrTableCell307.Weight = 1.250000204165783D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell185.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月两次随访});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 0.24590088962723811D;
            // 
            // txt满月两次随访
            // 
            this.txt满月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月两次随访.BorderWidth = 1F;
            this.txt满月两次随访.CanGrow = false;
            this.txt满月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(5F, 3.5F);
            this.txt满月两次随访.Name = "txt满月两次随访";
            this.txt满月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月两次随访.StylePriority.UseBorders = false;
            this.txt满月两次随访.StylePriority.UseBorderWidth = false;
            this.txt满月两次随访.StylePriority.UseFont = false;
            this.txt满月两次随访.StylePriority.UsePadding = false;
            this.txt满月两次随访.StylePriority.UseTextAlignment = false;
            this.txt满月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Text = "1 未患病 2 患病";
            this.xrTableCell308.Weight = 1.2540997820773967D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月两次随访});
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Weight = 0.24673432360712411D;
            // 
            // txt3月两次随访
            // 
            this.txt3月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月两次随访.BorderWidth = 1F;
            this.txt3月两次随访.CanGrow = false;
            this.txt3月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(4.999924F, 3.499985F);
            this.txt3月两次随访.Name = "txt3月两次随访";
            this.txt3月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月两次随访.StylePriority.UseBorders = false;
            this.txt3月两次随访.StylePriority.UseBorderWidth = false;
            this.txt3月两次随访.StylePriority.UseFont = false;
            this.txt3月两次随访.StylePriority.UsePadding = false;
            this.txt3月两次随访.StylePriority.UseTextAlignment = false;
            this.txt3月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.Text = "1 未患病 2 患病";
            this.xrTableCell309.Weight = 1.2845148211059221D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell189.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月两次随访});
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.Weight = 0.21875061758361747D;
            // 
            // txt6月两次随访
            // 
            this.txt6月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月两次随访.BorderWidth = 1F;
            this.txt6月两次随访.CanGrow = false;
            this.txt6月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(1.88F, 3.5F);
            this.txt6月两次随访.Name = "txt6月两次随访";
            this.txt6月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月两次随访.StylePriority.UseBorders = false;
            this.txt6月两次随访.StylePriority.UseBorderWidth = false;
            this.txt6月两次随访.StylePriority.UseFont = false;
            this.txt6月两次随访.StylePriority.UsePadding = false;
            this.txt6月两次随访.StylePriority.UseTextAlignment = false;
            this.txt6月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.Text = "1 未患病 2 患病";
            this.xrTableCell310.Weight = 1.2837449388724469D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月两次随访});
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Weight = 0.2162546610611582D;
            // 
            // txt8月两次随访
            // 
            this.txt8月两次随访.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月两次随访.BorderWidth = 1F;
            this.txt8月两次随访.CanGrow = false;
            this.txt8月两次随访.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月两次随访.LocationFloat = new DevExpress.Utils.PointFloat(1.63F, 3.5F);
            this.txt8月两次随访.Name = "txt8月两次随访";
            this.txt8月两次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月两次随访.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月两次随访.StylePriority.UseBorders = false;
            this.txt8月两次随访.StylePriority.UseBorderWidth = false;
            this.txt8月两次随访.StylePriority.UseFont = false;
            this.txt8月两次随访.StylePriority.UsePadding = false;
            this.txt8月两次随访.StylePriority.UseTextAlignment = false;
            this.txt8月两次随访.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell195});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.StylePriority.UseBorders = false;
            this.xrTableRow34.Weight = 0.80000000000000027D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "其他";
            this.xrTableCell191.Weight = 1.5000000670750944D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月其他});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 1.4959010937930211D;
            // 
            // txt满月其他
            // 
            this.txt满月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt满月其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt满月其他.Name = "txt满月其他";
            this.txt满月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月其他.SizeF = new System.Drawing.SizeF(148.5901F, 20F);
            this.txt满月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月其他});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 1.5008341056845209D;
            // 
            // txt3月其他
            // 
            this.txt3月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt3月其他.LocationFloat = new DevExpress.Utils.PointFloat(0.4099121F, 0F);
            this.txt3月其他.Name = "txt3月其他";
            this.txt3月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月其他.SizeF = new System.Drawing.SizeF(149.6735F, 20F);
            this.txt3月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月其他});
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Weight = 1.5032654386895397D;
            // 
            // txt6月其他
            // 
            this.txt6月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt6月其他.LocationFloat = new DevExpress.Utils.PointFloat(0.3265076F, 0F);
            this.txt6月其他.Name = "txt6月其他";
            this.txt6月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月其他.SizeF = new System.Drawing.SizeF(149.9999F, 20F);
            this.txt6月其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell195.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月其他});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.Weight = 1.4999995999336051D;
            // 
            // txt8月其他
            // 
            this.txt8月其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt8月其他.LocationFloat = new DevExpress.Utils.PointFloat(1.549592F, 0.0001546499F);
            this.txt8月其他.Name = "txt8月其他";
            this.txt8月其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月其他.SizeF = new System.Drawing.SizeF(144.08F, 18F);
            this.txt8月其他.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.Weight = 3.2000000000000015D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "转诊建议";
            this.xrTableCell196.Weight = 1.5000000670750944D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 1.4959010937930211D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0.4100189F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow37});
            this.xrTable2.SizeF = new System.Drawing.SizeF(148.1801F, 80F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell203});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "1有 2 无";
            this.xrTableCell35.Weight = 2.2803001009872075D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月转诊建议});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.71969989901279263D;
            // 
            // txt满月转诊建议
            // 
            this.txt满月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月转诊建议.BorderWidth = 1F;
            this.txt满月转诊建议.CanGrow = false;
            this.txt满月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(16.96F, 4F);
            this.txt满月转诊建议.Name = "txt满月转诊建议";
            this.txt满月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月转诊建议.StylePriority.UseBorders = false;
            this.txt满月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt满月转诊建议.StylePriority.UseFont = false;
            this.txt满月转诊建议.StylePriority.UsePadding = false;
            this.txt满月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt满月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Text = "原因：";
            this.xrTableCell205.Weight = 1.0122814590239646D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月转诊原因});
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Weight = 1.9877185409760354D;
            // 
            // txt满月转诊原因
            // 
            this.txt满月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504639F, 1.999939F);
            this.txt满月转诊原因.Name = "txt满月转诊原因";
            this.txt满月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月转诊原因.SizeF = new System.Drawing.SizeF(95.67531F, 18.00006F);
            this.txt满月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell207,
            this.xrTableCell208});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "机构及科室：";
            this.xrTableCell207.Weight = 1.903089105596592D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel52});
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Weight = 1.0969108944034081D;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel52.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月转诊机构});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 3D;
            // 
            // txt满月转诊机构
            // 
            this.txt满月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.txt满月转诊机构.Name = "txt满月转诊机构";
            this.txt满月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月转诊机构.SizeF = new System.Drawing.SizeF(148.18F, 15F);
            this.txt满月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 1.5008341056845209D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(1.493327F, 3.178914E-05F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43});
            this.xrTable7.SizeF = new System.Drawing.SizeF(147.8536F, 80F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell201});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Text = "1有 2 无";
            this.xrTableCell251.Weight = 2.2820242537101638D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月转诊建议});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.71797574628983629D;
            // 
            // txt3月转诊建议
            // 
            this.txt3月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月转诊建议.BorderWidth = 1F;
            this.txt3月转诊建议.CanGrow = false;
            this.txt3月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(16.44993F, 3.999933F);
            this.txt3月转诊建议.Name = "txt3月转诊建议";
            this.txt3月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月转诊建议.StylePriority.UseBorders = false;
            this.txt3月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt3月转诊建议.StylePriority.UseFont = false;
            this.txt3月转诊建议.StylePriority.UsePadding = false;
            this.txt3月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt3月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell209});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Text = "原因：";
            this.xrTableCell202.Weight = 1.0122814590239646D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月转诊原因});
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 1.9877185409760354D;
            // 
            // txt3月转诊原因
            // 
            this.txt3月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt3月转诊原因.Name = "txt3月转诊原因";
            this.txt3月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt3月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.xrTableCell211});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "机构及科室：";
            this.xrTableCell210.Weight = 1.903089105596592D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel54});
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 1.0969108944034081D;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel54.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月转诊机构});
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 3D;
            // 
            // txt3月转诊机构
            // 
            this.txt3月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt3月转诊机构.Name = "txt3月转诊机构";
            this.txt3月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt3月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 1.5032648283379522D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable8.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(2.826538F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(147.4999F, 80F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell252,
            this.xrTableCell213});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Text = "1有 2 无";
            this.xrTableCell252.Weight = 2.3262719399562597D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月转诊建议});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 0.67372806004374053D;
            // 
            // txt6月转诊建议
            // 
            this.txt6月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月转诊建议.BorderWidth = 1F;
            this.txt6月转诊建议.CanGrow = false;
            this.txt6月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(13.13F, 4F);
            this.txt6月转诊建议.Name = "txt6月转诊建议";
            this.txt6月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月转诊建议.StylePriority.UseBorders = false;
            this.txt6月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt6月转诊建议.StylePriority.UseFont = false;
            this.txt6月转诊建议.StylePriority.UsePadding = false;
            this.txt6月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt6月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "原因：";
            this.xrTableCell214.Weight = 1.0122814590239646D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月转诊原因});
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 1.9877185409760354D;
            // 
            // txt6月转诊原因
            // 
            this.txt6月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt6月转诊原因.Name = "txt6月转诊原因";
            this.txt6月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt6月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell217});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Text = "机构及科室：";
            this.xrTableCell216.Weight = 1.903089105596592D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel57});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 1.0969108944034081D;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel57.StylePriority.UseBorders = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月转诊机构});
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 3D;
            // 
            // txt6月转诊机构
            // 
            this.txt6月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt6月转诊机构.Name = "txt6月转诊机构";
            this.txt6月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt6月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 1.5000002102851926D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable9.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(4.999817F, 0.0001220703F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51});
            this.xrTable9.SizeF = new System.Drawing.SizeF(143.4101F, 76.99982F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell253,
            this.xrTableCell219});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Text = "1有 2 无";
            this.xrTableCell253.Weight = 2.032469353698517D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月转诊建议});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.96753064630148322D;
            // 
            // txt8月转诊建议
            // 
            this.txt8月转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月转诊建议.BorderWidth = 1F;
            this.txt8月转诊建议.CanGrow = false;
            this.txt8月转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(27.84583F, 3.999901F);
            this.txt8月转诊建议.Name = "txt8月转诊建议";
            this.txt8月转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月转诊建议.StylePriority.UseBorders = false;
            this.txt8月转诊建议.StylePriority.UseBorderWidth = false;
            this.txt8月转诊建议.StylePriority.UseFont = false;
            this.txt8月转诊建议.StylePriority.UsePadding = false;
            this.txt8月转诊建议.StylePriority.UseTextAlignment = false;
            this.txt8月转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell220,
            this.xrTableCell221});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Text = "原因：";
            this.xrTableCell220.Weight = 1.0911104591946297D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月转诊原因});
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 1.9088895408053703D;
            // 
            // txt8月转诊原因
            // 
            this.txt8月转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt8月转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt8月转诊原因.Name = "txt8月转诊原因";
            this.txt8月转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月转诊原因.SizeF = new System.Drawing.SizeF(86.84119F, 18.00006F);
            this.txt8月转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "机构及科室：";
            this.xrTableCell222.Weight = 1.9663888322345193D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel60});
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 1.0336111677654807D;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.249878F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(46.48822F, 18.00012F);
            this.xrLabel60.StylePriority.UseBorders = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月转诊机构});
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.Weight = 3D;
            // 
            // txt8月转诊机构
            // 
            this.txt8月转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt8月转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999924F);
            this.txt8月转诊机构.Name = "txt8月转诊机构";
            this.txt8月转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月转诊机构.SizeF = new System.Drawing.SizeF(125.0003F, 15F);
            this.txt8月转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell311,
            this.xrTableCell226,
            this.xrTableCell312,
            this.xrTableCell227,
            this.xrTableCell313,
            this.xrTableCell228,
            this.xrTableCell314,
            this.xrTableCell229});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.StylePriority.UseBorders = false;
            this.xrTableRow52.Weight = 4.4000000000000021D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Text = "指导";
            this.xrTableCell225.Weight = 1.5000000670750944D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Multiline = true;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell311.Weight = 1.130417374592914D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell226.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月生长,
            this.txt满月低盐,
            this.txt满月口腔保健,
            this.txt满月疾病,
            this.txt满月科学,
            this.txt满月预防});
            this.xrTableCell226.Multiline = true;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.365483719200107D;
            // 
            // txt满月生长
            // 
            this.txt满月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月生长.BorderWidth = 1F;
            this.txt满月生长.CanGrow = false;
            this.txt满月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月生长.LocationFloat = new DevExpress.Utils.PointFloat(16.95817F, 19.99995F);
            this.txt满月生长.Name = "txt满月生长";
            this.txt满月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月生长.StylePriority.UseBorders = false;
            this.txt满月生长.StylePriority.UseBorderWidth = false;
            this.txt满月生长.StylePriority.UseFont = false;
            this.txt满月生长.StylePriority.UsePadding = false;
            this.txt满月生长.StylePriority.UseTextAlignment = false;
            this.txt满月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt满月低盐
            // 
            this.txt满月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月低盐.BorderWidth = 1F;
            this.txt满月低盐.CanGrow = false;
            this.txt满月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月低盐.LocationFloat = new DevExpress.Utils.PointFloat(16.95808F, 89.9999F);
            this.txt满月低盐.Name = "txt满月低盐";
            this.txt满月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月低盐.StylePriority.UseBorders = false;
            this.txt满月低盐.StylePriority.UseBorderWidth = false;
            this.txt满月低盐.StylePriority.UseFont = false;
            this.txt满月低盐.StylePriority.UsePadding = false;
            this.txt满月低盐.StylePriority.UseTextAlignment = false;
            this.txt满月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt满月口腔保健
            // 
            this.txt满月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月口腔保健.BorderWidth = 1F;
            this.txt满月口腔保健.CanGrow = false;
            this.txt满月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(16.95811F, 74.83311F);
            this.txt满月口腔保健.Name = "txt满月口腔保健";
            this.txt满月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月口腔保健.StylePriority.UseBorders = false;
            this.txt满月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt满月口腔保健.StylePriority.UseFont = false;
            this.txt满月口腔保健.StylePriority.UsePadding = false;
            this.txt满月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt满月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt满月疾病
            // 
            this.txt满月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月疾病.BorderWidth = 1F;
            this.txt满月疾病.CanGrow = false;
            this.txt满月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月疾病.LocationFloat = new DevExpress.Utils.PointFloat(16.95814F, 34.99994F);
            this.txt满月疾病.Name = "txt满月疾病";
            this.txt满月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月疾病.StylePriority.UseBorders = false;
            this.txt满月疾病.StylePriority.UseBorderWidth = false;
            this.txt满月疾病.StylePriority.UseFont = false;
            this.txt满月疾病.StylePriority.UsePadding = false;
            this.txt满月疾病.StylePriority.UseTextAlignment = false;
            this.txt满月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt满月科学
            // 
            this.txt满月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月科学.BorderWidth = 1F;
            this.txt满月科学.CanGrow = false;
            this.txt满月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月科学.LocationFloat = new DevExpress.Utils.PointFloat(16.9582F, 2.999942F);
            this.txt满月科学.Name = "txt满月科学";
            this.txt满月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月科学.StylePriority.UseBorders = false;
            this.txt满月科学.StylePriority.UseBorderWidth = false;
            this.txt满月科学.StylePriority.UseFont = false;
            this.txt满月科学.StylePriority.UsePadding = false;
            this.txt满月科学.StylePriority.UseTextAlignment = false;
            this.txt满月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt满月预防
            // 
            this.txt满月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt满月预防.BorderWidth = 1F;
            this.txt满月预防.CanGrow = false;
            this.txt满月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt满月预防.LocationFloat = new DevExpress.Utils.PointFloat(16.95811F, 54.99992F);
            this.txt满月预防.Name = "txt满月预防";
            this.txt满月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt满月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt满月预防.StylePriority.UseBorders = false;
            this.txt满月预防.StylePriority.UseBorderWidth = false;
            this.txt满月预防.StylePriority.UseFont = false;
            this.txt满月预防.StylePriority.UsePadding = false;
            this.txt满月预防.StylePriority.UseTextAlignment = false;
            this.txt满月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Multiline = true;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell312.Weight = 1.1396179658956824D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell227.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月科学,
            this.txt3月生长,
            this.txt3月低盐,
            this.txt3月口腔保健,
            this.txt3月疾病,
            this.txt3月预防});
            this.xrTableCell227.Multiline = true;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.36121613978883854D;
            // 
            // txt3月科学
            // 
            this.txt3月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月科学.BorderWidth = 1F;
            this.txt3月科学.CanGrow = false;
            this.txt3月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月科学.LocationFloat = new DevExpress.Utils.PointFloat(16.44808F, 2.999973F);
            this.txt3月科学.Name = "txt3月科学";
            this.txt3月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月科学.StylePriority.UseBorders = false;
            this.txt3月科学.StylePriority.UseBorderWidth = false;
            this.txt3月科学.StylePriority.UseFont = false;
            this.txt3月科学.StylePriority.UsePadding = false;
            this.txt3月科学.StylePriority.UseTextAlignment = false;
            this.txt3月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3月生长
            // 
            this.txt3月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月生长.BorderWidth = 1F;
            this.txt3月生长.CanGrow = false;
            this.txt3月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月生长.LocationFloat = new DevExpress.Utils.PointFloat(16.44808F, 19.99992F);
            this.txt3月生长.Name = "txt3月生长";
            this.txt3月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月生长.StylePriority.UseBorders = false;
            this.txt3月生长.StylePriority.UseBorderWidth = false;
            this.txt3月生长.StylePriority.UseFont = false;
            this.txt3月生长.StylePriority.UsePadding = false;
            this.txt3月生长.StylePriority.UseTextAlignment = false;
            this.txt3月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3月低盐
            // 
            this.txt3月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月低盐.BorderWidth = 1F;
            this.txt3月低盐.CanGrow = false;
            this.txt3月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月低盐.LocationFloat = new DevExpress.Utils.PointFloat(16.44993F, 89.99983F);
            this.txt3月低盐.Name = "txt3月低盐";
            this.txt3月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月低盐.StylePriority.UseBorders = false;
            this.txt3月低盐.StylePriority.UseBorderWidth = false;
            this.txt3月低盐.StylePriority.UseFont = false;
            this.txt3月低盐.StylePriority.UsePadding = false;
            this.txt3月低盐.StylePriority.UseTextAlignment = false;
            this.txt3月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3月口腔保健
            // 
            this.txt3月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月口腔保健.BorderWidth = 1F;
            this.txt3月口腔保健.CanGrow = false;
            this.txt3月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(16.44808F, 74.83311F);
            this.txt3月口腔保健.Name = "txt3月口腔保健";
            this.txt3月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月口腔保健.StylePriority.UseBorders = false;
            this.txt3月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt3月口腔保健.StylePriority.UseFont = false;
            this.txt3月口腔保健.StylePriority.UsePadding = false;
            this.txt3月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt3月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3月疾病
            // 
            this.txt3月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月疾病.BorderWidth = 1F;
            this.txt3月疾病.CanGrow = false;
            this.txt3月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月疾病.LocationFloat = new DevExpress.Utils.PointFloat(16.44993F, 34.99994F);
            this.txt3月疾病.Name = "txt3月疾病";
            this.txt3月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月疾病.StylePriority.UseBorders = false;
            this.txt3月疾病.StylePriority.UseBorderWidth = false;
            this.txt3月疾病.StylePriority.UseFont = false;
            this.txt3月疾病.StylePriority.UsePadding = false;
            this.txt3月疾病.StylePriority.UseTextAlignment = false;
            this.txt3月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3月预防
            // 
            this.txt3月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3月预防.BorderWidth = 1F;
            this.txt3月预防.CanGrow = false;
            this.txt3月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3月预防.LocationFloat = new DevExpress.Utils.PointFloat(16.44808F, 54.99992F);
            this.txt3月预防.Name = "txt3月预防";
            this.txt3月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3月预防.StylePriority.UseBorders = false;
            this.txt3月预防.StylePriority.UseBorderWidth = false;
            this.txt3月预防.StylePriority.UseFont = false;
            this.txt3月预防.StylePriority.UsePadding = false;
            this.txt3月预防.StylePriority.UseTextAlignment = false;
            this.txt3月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Multiline = true;
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell313.Weight = 1.1720148165282853D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell228.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月科学,
            this.txt6月生长,
            this.txt6月低盐,
            this.txt6月口腔保健,
            this.txt6月疾病,
            this.txt6月预防});
            this.xrTableCell228.Multiline = true;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Weight = 0.33125001180966673D;
            // 
            // txt6月科学
            // 
            this.txt6月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月科学.BorderWidth = 1F;
            this.txt6月科学.CanGrow = false;
            this.txt6月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月科学.LocationFloat = new DevExpress.Utils.PointFloat(13.12491F, 2.999878F);
            this.txt6月科学.Name = "txt6月科学";
            this.txt6月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月科学.StylePriority.UseBorders = false;
            this.txt6月科学.StylePriority.UseBorderWidth = false;
            this.txt6月科学.StylePriority.UseFont = false;
            this.txt6月科学.StylePriority.UsePadding = false;
            this.txt6月科学.StylePriority.UseTextAlignment = false;
            this.txt6月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6月生长
            // 
            this.txt6月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月生长.BorderWidth = 1F;
            this.txt6月生长.CanGrow = false;
            this.txt6月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月生长.LocationFloat = new DevExpress.Utils.PointFloat(13.12567F, 19.99995F);
            this.txt6月生长.Name = "txt6月生长";
            this.txt6月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月生长.StylePriority.UseBorders = false;
            this.txt6月生长.StylePriority.UseBorderWidth = false;
            this.txt6月生长.StylePriority.UseFont = false;
            this.txt6月生长.StylePriority.UsePadding = false;
            this.txt6月生长.StylePriority.UseTextAlignment = false;
            this.txt6月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6月低盐
            // 
            this.txt6月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月低盐.BorderWidth = 1F;
            this.txt6月低盐.CanGrow = false;
            this.txt6月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月低盐.LocationFloat = new DevExpress.Utils.PointFloat(13.12491F, 89.9999F);
            this.txt6月低盐.Name = "txt6月低盐";
            this.txt6月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月低盐.StylePriority.UseBorders = false;
            this.txt6月低盐.StylePriority.UseBorderWidth = false;
            this.txt6月低盐.StylePriority.UseFont = false;
            this.txt6月低盐.StylePriority.UsePadding = false;
            this.txt6月低盐.StylePriority.UseTextAlignment = false;
            this.txt6月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6月口腔保健
            // 
            this.txt6月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月口腔保健.BorderWidth = 1F;
            this.txt6月口腔保健.CanGrow = false;
            this.txt6月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(13.12567F, 74.83311F);
            this.txt6月口腔保健.Name = "txt6月口腔保健";
            this.txt6月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月口腔保健.StylePriority.UseBorders = false;
            this.txt6月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt6月口腔保健.StylePriority.UseFont = false;
            this.txt6月口腔保健.StylePriority.UsePadding = false;
            this.txt6月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt6月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6月疾病
            // 
            this.txt6月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月疾病.BorderWidth = 1F;
            this.txt6月疾病.CanGrow = false;
            this.txt6月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月疾病.LocationFloat = new DevExpress.Utils.PointFloat(13.12567F, 34.99994F);
            this.txt6月疾病.Name = "txt6月疾病";
            this.txt6月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月疾病.StylePriority.UseBorders = false;
            this.txt6月疾病.StylePriority.UseBorderWidth = false;
            this.txt6月疾病.StylePriority.UseFont = false;
            this.txt6月疾病.StylePriority.UsePadding = false;
            this.txt6月疾病.StylePriority.UseTextAlignment = false;
            this.txt6月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6月预防
            // 
            this.txt6月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6月预防.BorderWidth = 1F;
            this.txt6月预防.CanGrow = false;
            this.txt6月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6月预防.LocationFloat = new DevExpress.Utils.PointFloat(13.12567F, 54.99989F);
            this.txt6月预防.Name = "txt6月预防";
            this.txt6月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6月预防.StylePriority.UseBorders = false;
            this.txt6月预防.StylePriority.UseBorderWidth = false;
            this.txt6月预防.StylePriority.UseFont = false;
            this.txt6月预防.StylePriority.UsePadding = false;
            this.txt6月预防.StylePriority.UseTextAlignment = false;
            this.txt6月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Multiline = true;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.Text = "1 科学喂养\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell314.Weight = 1.1250001204013855D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell229.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月科学,
            this.txt8月生长,
            this.txt8月低盐,
            this.txt8月口腔保健,
            this.txt8月疾病,
            this.txt8月预防});
            this.xrTableCell229.Multiline = true;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.37500008988380723D;
            // 
            // txt8月科学
            // 
            this.txt8月科学.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月科学.BorderWidth = 1F;
            this.txt8月科学.CanGrow = false;
            this.txt8月科学.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月科学.LocationFloat = new DevExpress.Utils.PointFloat(15.87467F, 2.999973F);
            this.txt8月科学.Name = "txt8月科学";
            this.txt8月科学.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月科学.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月科学.StylePriority.UseBorders = false;
            this.txt8月科学.StylePriority.UseBorderWidth = false;
            this.txt8月科学.StylePriority.UseFont = false;
            this.txt8月科学.StylePriority.UsePadding = false;
            this.txt8月科学.StylePriority.UseTextAlignment = false;
            this.txt8月科学.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt8月生长
            // 
            this.txt8月生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月生长.BorderWidth = 1F;
            this.txt8月生长.CanGrow = false;
            this.txt8月生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月生长.LocationFloat = new DevExpress.Utils.PointFloat(15.87F, 20F);
            this.txt8月生长.Name = "txt8月生长";
            this.txt8月生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月生长.StylePriority.UseBorders = false;
            this.txt8月生长.StylePriority.UseBorderWidth = false;
            this.txt8月生长.StylePriority.UseFont = false;
            this.txt8月生长.StylePriority.UsePadding = false;
            this.txt8月生长.StylePriority.UseTextAlignment = false;
            this.txt8月生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt8月低盐
            // 
            this.txt8月低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月低盐.BorderWidth = 1F;
            this.txt8月低盐.CanGrow = false;
            this.txt8月低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月低盐.LocationFloat = new DevExpress.Utils.PointFloat(15.87448F, 89.99977F);
            this.txt8月低盐.Name = "txt8月低盐";
            this.txt8月低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月低盐.StylePriority.UseBorders = false;
            this.txt8月低盐.StylePriority.UseBorderWidth = false;
            this.txt8月低盐.StylePriority.UseFont = false;
            this.txt8月低盐.StylePriority.UsePadding = false;
            this.txt8月低盐.StylePriority.UseTextAlignment = false;
            this.txt8月低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt8月口腔保健
            // 
            this.txt8月口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月口腔保健.BorderWidth = 1F;
            this.txt8月口腔保健.CanGrow = false;
            this.txt8月口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(15.87F, 74.83F);
            this.txt8月口腔保健.Name = "txt8月口腔保健";
            this.txt8月口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月口腔保健.StylePriority.UseBorders = false;
            this.txt8月口腔保健.StylePriority.UseBorderWidth = false;
            this.txt8月口腔保健.StylePriority.UseFont = false;
            this.txt8月口腔保健.StylePriority.UsePadding = false;
            this.txt8月口腔保健.StylePriority.UseTextAlignment = false;
            this.txt8月口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt8月疾病
            // 
            this.txt8月疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月疾病.BorderWidth = 1F;
            this.txt8月疾病.CanGrow = false;
            this.txt8月疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月疾病.LocationFloat = new DevExpress.Utils.PointFloat(15.87467F, 34.99988F);
            this.txt8月疾病.Name = "txt8月疾病";
            this.txt8月疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月疾病.StylePriority.UseBorders = false;
            this.txt8月疾病.StylePriority.UseBorderWidth = false;
            this.txt8月疾病.StylePriority.UseFont = false;
            this.txt8月疾病.StylePriority.UsePadding = false;
            this.txt8月疾病.StylePriority.UseTextAlignment = false;
            this.txt8月疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt8月预防
            // 
            this.txt8月预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月预防.BorderWidth = 1F;
            this.txt8月预防.CanGrow = false;
            this.txt8月预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8月预防.LocationFloat = new DevExpress.Utils.PointFloat(15.87474F, 54.99986F);
            this.txt8月预防.Name = "txt8月预防";
            this.txt8月预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt8月预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt8月预防.StylePriority.UseBorders = false;
            this.txt8月预防.StylePriority.UseBorderWidth = false;
            this.txt8月预防.StylePriority.UseFont = false;
            this.txt8月预防.StylePriority.UsePadding = false;
            this.txt8月预防.StylePriority.UseTextAlignment = false;
            this.txt8月预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233,
            this.xrTableCell234});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.StylePriority.UseBorders = false;
            this.xrTableRow53.Weight = 4.0000000000000018D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Text = "中医药健康服务";
            this.xrTableCell230.Weight = 1.5000000670750944D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月服务});
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 1.4959010937930211D;
            // 
            // txt满月服务
            // 
            this.txt满月服务.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt满月服务.Name = "txt满月服务";
            this.txt满月服务.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月服务.SizeF = new System.Drawing.SizeF(148.59F, 100F);
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月服务});
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 1.5008341056845209D;
            // 
            // txt3月服务
            // 
            this.txt3月服务.LocationFloat = new DevExpress.Utils.PointFloat(0.4099121F, 0.625F);
            this.txt3月服务.Name = "txt3月服务";
            this.txt3月服务.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月服务.SizeF = new System.Drawing.SizeF(148.59F, 99.37494F);
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Weight = 1.5032648283379522D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable10.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54,
            this.xrTableRow56,
            this.xrTableRow55,
            this.xrTableRow57});
            this.xrTable10.SizeF = new System.Drawing.SizeF(150.3265F, 100F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell237});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1.9361006289308176D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Multiline = true;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授摩腹、捏脊方法";
            this.xrTableCell237.Weight = 3.0054574798513976D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 0.063899371069182365D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Weight = 1D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 2.0054574798513976D;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236,
            this.xrTableCell238});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.72201255942290687D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Text = "4其他";
            this.xrTableCell236.Weight = 1D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel64});
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Weight = 2.0054574798513976D;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 0F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(100.3083F, 18.00006F);
            this.xrLabel64.StylePriority.UseBorders = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell241});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 0.73031444813470414D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月服务其他});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Weight = 3.0054574798513976D;
            // 
            // txt6月服务其他
            // 
            this.txt6月服务其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月服务其他.LocationFloat = new DevExpress.Utils.PointFloat(5.326538F, 0F);
            this.txt6月服务其他.Name = "txt6月服务其他";
            this.txt6月服务其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月服务其他.SizeF = new System.Drawing.SizeF(143.0778F, 18.00006F);
            this.txt6月服务其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月服务});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 1.5000002102851926D;
            // 
            // txt8月服务
            // 
            this.txt8月服务.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月服务.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.625F);
            this.txt8月服务.Name = "txt8月服务";
            this.txt8月服务.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月服务.SizeF = new System.Drawing.SizeF(150F, 99.37494F);
            this.txt8月服务.StylePriority.UseBorders = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell245});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.StylePriority.UseBorders = false;
            this.xrTableRow58.Weight = 0.80000000000000027D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Text = "下次随访日期";
            this.xrTableCell235.Weight = 1.5000000670750944D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月下次随访});
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Weight = 1.4959010937930211D;
            // 
            // txt满月下次随访
            // 
            this.txt满月下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt满月下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.4100189F, 0.625F);
            this.txt满月下次随访.Name = "txt满月下次随访";
            this.txt满月下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt满月下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月下次随访});
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Weight = 1.5008341056845209D;
            // 
            // txt3月下次随访
            // 
            this.txt3月下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt3月下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.4099121F, 0.625F);
            this.txt3月下次随访.Name = "txt3月下次随访";
            this.txt3月下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt3月下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月下次随访});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 1.5032648283379522D;
            // 
            // txt6月下次随访
            // 
            this.txt6月下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt6月下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.3265076F, 0.625F);
            this.txt6月下次随访.Name = "txt6月下次随访";
            this.txt6月下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt6月下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月下次随访});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 1.5000002102851926D;
            // 
            // txt8月下次随访
            // 
            this.txt8月下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt8月下次随访.LocationFloat = new DevExpress.Utils.PointFloat(1.409912F, 0.624939F);
            this.txt8月下次随访.Name = "txt8月下次随访";
            this.txt8月下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月下次随访.SizeF = new System.Drawing.SizeF(147F, 17.00006F);
            this.txt8月下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.StylePriority.UseBorders = false;
            this.xrTableRow59.Weight = 0.80000000000000027D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Text = "随访医生签名";
            this.xrTableCell246.Weight = 1.5000000670750944D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt满月随访医生});
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Weight = 1.4959010937930211D;
            // 
            // txt满月随访医生
            // 
            this.txt满月随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt满月随访医生.LocationFloat = new DevExpress.Utils.PointFloat(0.4100482F, 0.6249745F);
            this.txt满月随访医生.Name = "txt满月随访医生";
            this.txt满月随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt满月随访医生.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt满月随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3月随访医生});
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 1.5008341056845209D;
            // 
            // txt3月随访医生
            // 
            this.txt3月随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt3月随访医生.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.625F);
            this.txt3月随访医生.Name = "txt3月随访医生";
            this.txt3月随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3月随访医生.SizeF = new System.Drawing.SizeF(149.347F, 19.37463F);
            this.txt3月随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6月随访医生});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Weight = 1.5032648283379522D;
            // 
            // txt6月随访医生
            // 
            this.txt6月随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt6月随访医生.LocationFloat = new DevExpress.Utils.PointFloat(2.146466F, 0.6250699F);
            this.txt6月随访医生.Name = "txt6月随访医生";
            this.txt6月随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6月随访医生.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt6月随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell250.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt8月随访医生});
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Weight = 1.5000002102851926D;
            // 
            // txt8月随访医生
            // 
            this.txt8月随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt8月随访医生.LocationFloat = new DevExpress.Utils.PointFloat(1.409973F, 0.6252441F);
            this.txt8月随访医生.Name = "txt8月随访医生";
            this.txt8月随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt8月随访医生.SizeF = new System.Drawing.SizeF(146.9999F, 17F);
            this.txt8月随访医生.StylePriority.UseBorders = false;
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(143.75F, 80.16666F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(152.08F, 20F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UsePadding = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 2F;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(596.6667F, 82.58667F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.BorderWidth = 2F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(551.0417F, 82.58333F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseBorderWidth = false;
            this.xrLabel11.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 2F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(578.125F, 82.58333F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(616.6667F, 82.58333F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 2F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(642.7083F, 82.58333F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.BorderWidth = 2F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(668.75F, 82.58333F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseBorderWidth = false;
            this.xrLabel7.StylePriority.UseFont = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.BorderWidth = 2F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(695.8333F, 82.58333F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseBorderWidth = false;
            this.xrLabel6.StylePriority.UseFont = false;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 2F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(724.3749F, 82.58333F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.BorderWidth = 2F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(525F, 82.58333F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseBorderWidth = false;
            this.xrLabel12.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(455.0001F, 80.16666F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(57.50488F, 20F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "编号";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(61.45833F, 80.16666F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(69.79668F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.Text = "姓名：";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(211.4583F, 35.37501F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(280.2083F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "1岁以内儿童健康检查记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 19F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report1岁以内儿童健康检查记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(55, 22, 19, 12);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel txt满月随访;
        private DevExpress.XtraReports.UI.XRLabel txt3月随访;
        private DevExpress.XtraReports.UI.XRLabel txt6月随访;
        private DevExpress.XtraReports.UI.XRLabel txt8月随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRLabel txt满月体重;
        private DevExpress.XtraReports.UI.XRLabel txt3月体重;
        private DevExpress.XtraReports.UI.XRLabel txt6月体重;
        private DevExpress.XtraReports.UI.XRLabel txt8月体重;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRLabel txt满月身长;
        private DevExpress.XtraReports.UI.XRLabel txt3月身长;
        private DevExpress.XtraReports.UI.XRLabel txt6月身长;
        private DevExpress.XtraReports.UI.XRLabel txt8月身长;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell txt8月头围;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel txt3月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRLabel txt3月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel txt6月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRLabel txt6月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel txt满月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel txt满月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRLabel txt8月前囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel txt8月后囟;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLine xrLine20;
        private DevExpress.XtraReports.UI.XRLine xrLine21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRLine xrLine22;
        private DevExpress.XtraReports.UI.XRLine xrLine23;
        private DevExpress.XtraReports.UI.XRLine xrLine24;
        private DevExpress.XtraReports.UI.XRLine xrLine25;
        private DevExpress.XtraReports.UI.XRLine xrLine26;
        private DevExpress.XtraReports.UI.XRLine xrLine27;
        private DevExpress.XtraReports.UI.XRLine xrLine28;
        private DevExpress.XtraReports.UI.XRLine xrLine29;
        private DevExpress.XtraReports.UI.XRLine xrLine30;
        private DevExpress.XtraReports.UI.XRLine xrLine31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRLine xrLine32;
        private DevExpress.XtraReports.UI.XRLine xrLine33;
        private DevExpress.XtraReports.UI.XRLine xrLine34;
        private DevExpress.XtraReports.UI.XRLine xrLine35;
        private DevExpress.XtraReports.UI.XRLine xrLine36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRLabel txt满月血红;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRLabel txt3月血红;
        private DevExpress.XtraReports.UI.XRLabel txt6月血红;
        private DevExpress.XtraReports.UI.XRLabel txt8月血红;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRLabel txt满月户外;
        private DevExpress.XtraReports.UI.XRLabel txt3月户外;
        private DevExpress.XtraReports.UI.XRLabel txt6月户外;
        private DevExpress.XtraReports.UI.XRLabel txt8月户外;
        private DevExpress.XtraReports.UI.XRLabel txt满月维生素;
        private DevExpress.XtraReports.UI.XRLabel txt3月维生素;
        private DevExpress.XtraReports.UI.XRLabel txt6月维生素;
        private DevExpress.XtraReports.UI.XRLabel txt8月维生素;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRLabel txt满月其他;
        private DevExpress.XtraReports.UI.XRLabel txt3月其他;
        private DevExpress.XtraReports.UI.XRLabel txt6月其他;
        private DevExpress.XtraReports.UI.XRLabel txt8月其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel txt满月转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel txt满月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRLabel txt3月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRLabel txt3月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel txt6月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRLabel txt6月转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRLabel txt8月转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRLabel txt8月转诊机构;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRLabel txt满月服务;
        private DevExpress.XtraReports.UI.XRLabel txt3月服务;
        private DevExpress.XtraReports.UI.XRLabel txt8月服务;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel txt6月服务其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRLabel txt满月下次随访;
        private DevExpress.XtraReports.UI.XRLabel txt3月下次随访;
        private DevExpress.XtraReports.UI.XRLabel txt6月下次随访;
        private DevExpress.XtraReports.UI.XRLabel txt8月下次随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRLabel txt满月随访医生;
        private DevExpress.XtraReports.UI.XRLabel txt3月随访医生;
        private DevExpress.XtraReports.UI.XRLabel txt6月随访医生;
        private DevExpress.XtraReports.UI.XRLabel txt8月随访医生;
        private DevExpress.XtraReports.UI.XRTableCell txt满月头围;
        private DevExpress.XtraReports.UI.XRTableCell txt3月头围;
        private DevExpress.XtraReports.UI.XRTableCell txt6月头围;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRLabel txt满月转诊建议;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRLabel txt3月转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt6月转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt8月转诊建议;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRLabel txt满月面色;
        private DevExpress.XtraReports.UI.XRLabel txt满月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt3月面色;
        private DevExpress.XtraReports.UI.XRLabel txt6月面色;
        private DevExpress.XtraReports.UI.XRLabel txt8月面色;
        private DevExpress.XtraReports.UI.XRLabel txt3月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt6月皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt8月皮肤;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRLabel txt满月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt满月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt3月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt6月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt8月眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt3月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt6月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt8月耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt6月听力;
        private DevExpress.XtraReports.UI.XRLabel txt满月口腔;
        private DevExpress.XtraReports.UI.XRLabel txt3月口腔;
        private DevExpress.XtraReports.UI.XRLabel txt6月出牙数;
        private DevExpress.XtraReports.UI.XRLabel txt8月出牙数;
        private DevExpress.XtraReports.UI.XRLabel txt满月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt3月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt6月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt8月心肺;
        private DevExpress.XtraReports.UI.XRLabel txt满月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt3月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt6月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt8月腹部;
        private DevExpress.XtraReports.UI.XRLabel txt满月脐部;
        private DevExpress.XtraReports.UI.XRLabel txt3月脐部;
        private DevExpress.XtraReports.UI.XRLabel txt满月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt3月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt6月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt8月四肢;
        private DevExpress.XtraReports.UI.XRLabel txt3月佝偻;
        private DevExpress.XtraReports.UI.XRLabel txt6月佝偻;
        private DevExpress.XtraReports.UI.XRLabel txt8月佝偻;
        private DevExpress.XtraReports.UI.XRLabel txt满月佝偻体症;
        private DevExpress.XtraReports.UI.XRLabel txt3月佝偻体症;
        private DevExpress.XtraReports.UI.XRLabel txt6月佝偻体症;
        private DevExpress.XtraReports.UI.XRLabel txt8月佝偻体症;
        private DevExpress.XtraReports.UI.XRLabel txt满月肛门;
        private DevExpress.XtraReports.UI.XRLabel txt3月肛门;
        private DevExpress.XtraReports.UI.XRLabel txt6月肛门;
        private DevExpress.XtraReports.UI.XRLabel txt8月肛门;
        private DevExpress.XtraReports.UI.XRLabel txt满月预防;
        private DevExpress.XtraReports.UI.XRLabel txt3月预防;
        private DevExpress.XtraReports.UI.XRLabel txt6月预防;
        private DevExpress.XtraReports.UI.XRLabel txt8月预防;
        private DevExpress.XtraReports.UI.XRLabel txt满月发育;
        private DevExpress.XtraReports.UI.XRLabel txt3月发育;
        private DevExpress.XtraReports.UI.XRLabel txt6月发育;
        private DevExpress.XtraReports.UI.XRLabel txt8月发育;
        private DevExpress.XtraReports.UI.XRLabel txt满月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt3月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt6月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt8月两次随访;
        private DevExpress.XtraReports.UI.XRLabel txt满月颈部;
        private DevExpress.XtraReports.UI.XRLabel txt3月颈部;
        private DevExpress.XtraReports.UI.XRLabel txt6月颈部;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRLabel txt满月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt3月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt6月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt8月闭合;
        private DevExpress.XtraReports.UI.XRLabel txt满月生长;
        private DevExpress.XtraReports.UI.XRLabel txt满月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt满月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt满月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt满月科学;
        private DevExpress.XtraReports.UI.XRLabel txt3月科学;
        private DevExpress.XtraReports.UI.XRLabel txt3月生长;
        private DevExpress.XtraReports.UI.XRLabel txt3月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt3月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt3月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt6月科学;
        private DevExpress.XtraReports.UI.XRLabel txt6月生长;
        private DevExpress.XtraReports.UI.XRLabel txt6月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt6月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt6月疾病;
        private DevExpress.XtraReports.UI.XRLabel txt8月科学;
        private DevExpress.XtraReports.UI.XRLabel txt8月生长;
        private DevExpress.XtraReports.UI.XRLabel txt8月低盐;
        private DevExpress.XtraReports.UI.XRLabel txt8月口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt8月疾病;
    }
}
