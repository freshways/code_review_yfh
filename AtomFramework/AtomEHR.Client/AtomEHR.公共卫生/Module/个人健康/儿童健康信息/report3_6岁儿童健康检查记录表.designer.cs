﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class report3_6岁儿童健康检查记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt满月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁体格发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁体格发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁体格发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt8月头围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁体格发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.txt4岁视力 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁视力 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁视力 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine37 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine38 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine39 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine40 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine41 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine42 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine43 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine44 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine45 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine46 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine47 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine48 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine49 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine50 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine51 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁牙齿数目 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁牙齿数目 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁牙齿数目 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁牙齿数目 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁龋齿数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁心肺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁腹部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁血红 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁无 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁肺炎 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁腹泻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁外伤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁两次其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁无 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁肺炎 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁腹泻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁外伤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁两次其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁无 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁肺炎 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁腹泻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁外伤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁两次其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁无 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁肺炎 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁腹泻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁外伤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁两次其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3岁低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3岁生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3岁口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3岁疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt3岁预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt5岁合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt5岁口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt5岁生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt5岁预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt5岁疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁合理 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6岁低盐 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6岁生长 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6岁口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6岁疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt6岁预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁服务其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt4岁服务 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁服务 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁服务 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁下次随访 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt3岁随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt4岁随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt5岁随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt6岁随访医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLine1,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel3,
            this.txt姓名,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.HeightF = 1079.042F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 110.5F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow10,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow33,
            this.xrTableRow35,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow58,
            this.xrTableRow59});
            this.xrTable1.SizeF = new System.Drawing.SizeF(750F, 937.7916F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UsePadding = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 0.8D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "月龄";
            this.xrTableCell1.Weight = 1.5000000966390006D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "3岁";
            this.xrTableCell2.Weight = 1.5000000966390006D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "4岁";
            this.xrTableCell4.Weight = 1.5000000406901042D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "5岁";
            this.xrTableCell5.Weight = 1.5000001305474153D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "6岁";
            this.xrTableCell3.Weight = 1.4999999406602607D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 0.8D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "随访日期";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 1.5000000966390006D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁随访});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.5000000966390006D;
            // 
            // txt3岁随访
            // 
            this.txt3岁随访.CanGrow = false;
            this.txt3岁随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.589457E-05F);
            this.txt3岁随访.Name = "txt3岁随访";
            this.txt3岁随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁随访});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1.5000000406901042D;
            // 
            // txt4岁随访
            // 
            this.txt4岁随访.CanGrow = false;
            this.txt4岁随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt4岁随访.Name = "txt4岁随访";
            this.txt4岁随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁随访});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 1.5000001305474153D;
            // 
            // txt5岁随访
            // 
            this.txt5岁随访.CanGrow = false;
            this.txt5岁随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt5岁随访.Name = "txt5岁随访";
            this.txt5岁随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁随访});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 1.4999999406602607D;
            // 
            // txt6岁随访
            // 
            this.txt6岁随访.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁随访.CanGrow = false;
            this.txt6岁随访.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt6岁随访.Name = "txt6岁随访";
            this.txt6岁随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁随访.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.txt6岁随访.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell11,
            this.xrTableCell16,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell17,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell15});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 0.8D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "体重（kg)";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 1.5000000788370782D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁体重});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.7500000483195004D;
            // 
            // txt3岁体重
            // 
            this.txt3岁体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁体重.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 1.589457E-05F);
            this.txt3岁体重.Name = "txt3岁体重";
            this.txt3岁体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt3岁体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "上 中 下";
            this.xrTableCell16.Weight = 0.75000004831950018D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁体重});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.75000004831950029D;
            // 
            // txt4岁体重
            // 
            this.txt4岁体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁体重.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 2.000014F);
            this.txt4岁体重.Name = "txt4岁体重";
            this.txt4岁体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt4岁体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "上 中 下";
            this.xrTableCell13.Weight = 0.75000001017252615D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁体重});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.75000006527370766D;
            // 
            // txt5岁体重
            // 
            this.txt5岁体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁体重.LocationFloat = new DevExpress.Utils.PointFloat(5.000114F, 0F);
            this.txt5岁体重.Name = "txt5岁体重";
            this.txt5岁体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt5岁体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "上 中 下";
            this.xrTableCell14.Weight = 0.75000006527370766D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁体重});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.74999997033013033D;
            // 
            // txt6岁体重
            // 
            this.txt6岁体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁体重.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 2.000014F);
            this.txt6岁体重.Name = "txt6岁体重";
            this.txt6岁体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁体重.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt6岁体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "上 中 下";
            this.xrTableCell15.Weight = 0.74999997033013033D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.8D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "身长（cm)";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell20.Weight = 1.5000000788370782D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁身长});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.7500000483195004D;
            // 
            // txt3岁身长
            // 
            this.txt3岁身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000019F, 0F);
            this.txt3岁身长.Name = "txt3岁身长";
            this.txt3岁身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt3岁身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "上 中 下";
            this.xrTableCell22.Weight = 0.75000004831950018D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁身长});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.75000004831950029D;
            // 
            // txt4岁身长
            // 
            this.txt4岁身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁身长.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.txt4岁身长.Name = "txt4岁身长";
            this.txt4岁身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt4岁身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "上 中 下";
            this.xrTableCell24.Weight = 0.75000001017252615D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁身长});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.75000006527370766D;
            // 
            // txt5岁身长
            // 
            this.txt5岁身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt5岁身长.Name = "txt5岁身长";
            this.txt5岁身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt5岁身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "上 中 下";
            this.xrTableCell26.Weight = 0.75000006527370766D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁身长});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Weight = 0.74999997033013033D;
            // 
            // txt6岁身长
            // 
            this.txt6岁身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁身长.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 0F);
            this.txt6岁身长.Name = "txt6岁身长";
            this.txt6岁身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁身长.SizeF = new System.Drawing.SizeF(70F, 18F);
            this.txt6岁身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Text = "上 中 下";
            this.xrTableCell28.Weight = 0.74999997033013033D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell90,
            this.txt满月头围,
            this.xrTableCell91,
            this.txt3月头围,
            this.xrTableCell92,
            this.txt6月头围,
            this.xrTableCell130,
            this.txt8月头围});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.Weight = 2.4000000000000004D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "体格发育评价";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 1.5000000788370782D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Multiline = true;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Text = "1正常 2低体重\r\n3消瘦 4发育迟缓 5\r\n超重";
            this.xrTableCell90.Weight = 1.3333331819110361D;
            // 
            // txt满月头围
            // 
            this.txt满月头围.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt满月头围.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁体格发育});
            this.txt满月头围.Multiline = true;
            this.txt满月头围.Name = "txt满月头围";
            this.txt满月头围.StylePriority.UseBorders = false;
            this.txt满月头围.Weight = 0.16666687467363961D;
            // 
            // txt3岁体格发育
            // 
            this.txt3岁体格发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁体格发育.BorderWidth = 1F;
            this.txt3岁体格发育.CanGrow = false;
            this.txt3岁体格发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁体格发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 30.35504F);
            this.txt3岁体格发育.Name = "txt3岁体格发育";
            this.txt3岁体格发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁体格发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁体格发育.StylePriority.UseBorders = false;
            this.txt3岁体格发育.StylePriority.UseBorderWidth = false;
            this.txt3岁体格发育.StylePriority.UseFont = false;
            this.txt3岁体格发育.StylePriority.UsePadding = false;
            this.txt3岁体格发育.StylePriority.UseTextAlignment = false;
            this.txt3岁体格发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Multiline = true;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "1正常 2低体重\r\n3消瘦 4发育迟缓 5\r\n超重";
            this.xrTableCell91.Weight = 1.333349966579688D;
            // 
            // txt3月头围
            // 
            this.txt3月头围.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3月头围.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁体格发育});
            this.txt3月头围.Multiline = true;
            this.txt3月头围.Name = "txt3月头围";
            this.txt3月头围.StylePriority.UseBorders = false;
            this.txt3月头围.Weight = 0.16665009000498787D;
            // 
            // txt4岁体格发育
            // 
            this.txt4岁体格发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁体格发育.BorderWidth = 1F;
            this.txt4岁体格发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁体格发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 30.35504F);
            this.txt4岁体格发育.Name = "txt4岁体格发育";
            this.txt4岁体格发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁体格发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁体格发育.StylePriority.UseBorders = false;
            this.txt4岁体格发育.StylePriority.UseBorderWidth = false;
            this.txt4岁体格发育.StylePriority.UseFont = false;
            this.txt4岁体格发育.StylePriority.UsePadding = false;
            this.txt4岁体格发育.StylePriority.UseTextAlignment = false;
            this.txt4岁体格发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Multiline = true;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Text = "1正常 2低体重\r\n3消瘦 4发育迟缓 5\r\n超重";
            this.xrTableCell92.Weight = 1.3400000522994995D;
            // 
            // txt6月头围
            // 
            this.txt6月头围.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6月头围.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁体格发育});
            this.txt6月头围.Multiline = true;
            this.txt6月头围.Name = "txt6月头围";
            this.txt6月头围.StylePriority.UseBorders = false;
            this.txt6月头围.Weight = 0.16004877519170183D;
            // 
            // txt5岁体格发育
            // 
            this.txt5岁体格发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁体格发育.BorderWidth = 1F;
            this.txt5岁体格发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁体格发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 30.35504F);
            this.txt5岁体格发育.Name = "txt5岁体格发育";
            this.txt5岁体格发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁体格发育.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁体格发育.StylePriority.UseBorders = false;
            this.txt5岁体格发育.StylePriority.UseBorderWidth = false;
            this.txt5岁体格发育.StylePriority.UseFont = false;
            this.txt5岁体格发育.StylePriority.UsePadding = false;
            this.txt5岁体格发育.StylePriority.UseTextAlignment = false;
            this.txt5岁体格发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Multiline = true;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "1正常 2低体重\r\n3消瘦 4发育迟缓 5\r\n超重";
            this.xrTableCell130.Weight = 1.3298176139884463D;
            // 
            // txt8月头围
            // 
            this.txt8月头围.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt8月头围.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁体格发育});
            this.txt8月头围.Multiline = true;
            this.txt8月头围.Name = "txt8月头围";
            this.txt8月头围.StylePriority.UseBorders = false;
            this.txt8月头围.Weight = 0.17013367168970389D;
            // 
            // txt6岁体格发育
            // 
            this.txt6岁体格发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁体格发育.BorderWidth = 1F;
            this.txt6岁体格发育.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁体格发育.LocationFloat = new DevExpress.Utils.PointFloat(1.388245F, 30.35504F);
            this.txt6岁体格发育.Name = "txt6岁体格发育";
            this.txt6岁体格发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁体格发育.SizeF = new System.Drawing.SizeF(14.03503F, 12.58331F);
            this.txt6岁体格发育.StylePriority.UseBorders = false;
            this.txt6岁体格发育.StylePriority.UseBorderWidth = false;
            this.txt6岁体格发育.StylePriority.UseFont = false;
            this.txt6岁体格发育.StylePriority.UsePadding = false;
            this.txt6岁体格发育.StylePriority.UseTextAlignment = false;
            this.txt6岁体格发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell89,
            this.txt4岁视力,
            this.txt5岁视力,
            this.txt6岁视力});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 0.80000000000000016D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.Weight = 0.28000001012166392D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "视力";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell56.Weight = 1.2200000569534304D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLine3,
            this.xrLine4,
            this.xrLine5,
            this.xrLine6});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 1.5000000638961788D;
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.BorderWidth = 2F;
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine2.StylePriority.UseBorders = false;
            this.xrLine2.StylePriority.UseBorderWidth = false;
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.BorderWidth = 2F;
            this.xrLine3.LineWidth = 2;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 3.71F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine3.StylePriority.UseBorders = false;
            this.xrLine3.StylePriority.UseBorderWidth = false;
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.BorderWidth = 2F;
            this.xrLine4.LineWidth = 2;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine4.StylePriority.UseBorders = false;
            this.xrLine4.StylePriority.UseBorderWidth = false;
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.BorderWidth = 2F;
            this.xrLine5.LineWidth = 2;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine5.StylePriority.UseBorders = false;
            this.xrLine5.StylePriority.UseBorderWidth = false;
            // 
            // xrLine6
            // 
            this.xrLine6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine6.BorderWidth = 2F;
            this.xrLine6.LineWidth = 2;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine6.StylePriority.UseBorders = false;
            this.xrLine6.StylePriority.UseBorderWidth = false;
            // 
            // txt4岁视力
            // 
            this.txt4岁视力.Name = "txt4岁视力";
            this.txt4岁视力.Weight = 1.5000000565846756D;
            // 
            // txt5岁视力
            // 
            this.txt5岁视力.Name = "txt5岁视力";
            this.txt5岁视力.Weight = 1.5000006691615035D;
            // 
            // txt6岁视力
            // 
            this.txt6岁视力.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁视力.Name = "txt6岁视力";
            this.txt6岁视力.StylePriority.UseBorders = false;
            this.txt6岁视力.Weight = 1.4999994484583286D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell131,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell98});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.80000000000000016D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.28000001012166392D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.StylePriority.UsePadding = false;
            this.xrTableCell94.StylePriority.UseTextAlignment = false;
            this.xrTableCell94.Text = "听力";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell94.Weight = 1.2200000569534304D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "1通过 2未过";
            this.xrTableCell131.Weight = 1.3333331855667878D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁听力});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.16666687832939098D;
            // 
            // txt3岁听力
            // 
            this.txt3岁听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁听力.BorderWidth = 1F;
            this.txt3岁听力.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁听力.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.709984F);
            this.txt3岁听力.Name = "txt3岁听力";
            this.txt3岁听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁听力.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁听力.StylePriority.UseBorders = false;
            this.txt3岁听力.StylePriority.UseBorderWidth = false;
            this.txt3岁听力.StylePriority.UseFont = false;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine37,
            this.xrLine38,
            this.xrLine39,
            this.xrLine40,
            this.xrLine41});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 1.5000000565846756D;
            // 
            // xrLine37
            // 
            this.xrLine37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine37.BorderWidth = 2F;
            this.xrLine37.LineWidth = 2;
            this.xrLine37.LocationFloat = new DevExpress.Utils.PointFloat(37.49998F, 3.71F);
            this.xrLine37.Name = "xrLine37";
            this.xrLine37.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine37.StylePriority.UseBorders = false;
            this.xrLine37.StylePriority.UseBorderWidth = false;
            // 
            // xrLine38
            // 
            this.xrLine38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine38.BorderWidth = 2F;
            this.xrLine38.LineWidth = 2;
            this.xrLine38.LocationFloat = new DevExpress.Utils.PointFloat(52.50001F, 3.71F);
            this.xrLine38.Name = "xrLine38";
            this.xrLine38.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine38.StylePriority.UseBorders = false;
            this.xrLine38.StylePriority.UseBorderWidth = false;
            // 
            // xrLine39
            // 
            this.xrLine39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine39.BorderWidth = 2F;
            this.xrLine39.LineWidth = 2;
            this.xrLine39.LocationFloat = new DevExpress.Utils.PointFloat(67.50003F, 3.71F);
            this.xrLine39.Name = "xrLine39";
            this.xrLine39.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine39.StylePriority.UseBorders = false;
            this.xrLine39.StylePriority.UseBorderWidth = false;
            // 
            // xrLine40
            // 
            this.xrLine40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine40.BorderWidth = 2F;
            this.xrLine40.LineWidth = 2;
            this.xrLine40.LocationFloat = new DevExpress.Utils.PointFloat(82.50006F, 3.71F);
            this.xrLine40.Name = "xrLine40";
            this.xrLine40.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine40.StylePriority.UseBorders = false;
            this.xrLine40.StylePriority.UseBorderWidth = false;
            // 
            // xrLine41
            // 
            this.xrLine41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine41.BorderWidth = 2F;
            this.xrLine41.LineWidth = 2;
            this.xrLine41.LocationFloat = new DevExpress.Utils.PointFloat(97.50002F, 3.71F);
            this.xrLine41.Name = "xrLine41";
            this.xrLine41.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine41.StylePriority.UseBorders = false;
            this.xrLine41.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine42,
            this.xrLine43,
            this.xrLine44,
            this.xrLine45,
            this.xrLine46});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 1.5000006691615035D;
            // 
            // xrLine42
            // 
            this.xrLine42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine42.BorderWidth = 2F;
            this.xrLine42.LineWidth = 2;
            this.xrLine42.LocationFloat = new DevExpress.Utils.PointFloat(37.50001F, 3.71F);
            this.xrLine42.Name = "xrLine42";
            this.xrLine42.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine42.StylePriority.UseBorders = false;
            this.xrLine42.StylePriority.UseBorderWidth = false;
            // 
            // xrLine43
            // 
            this.xrLine43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine43.BorderWidth = 2F;
            this.xrLine43.LineWidth = 2;
            this.xrLine43.LocationFloat = new DevExpress.Utils.PointFloat(52.50004F, 3.71F);
            this.xrLine43.Name = "xrLine43";
            this.xrLine43.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine43.StylePriority.UseBorders = false;
            this.xrLine43.StylePriority.UseBorderWidth = false;
            // 
            // xrLine44
            // 
            this.xrLine44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine44.BorderWidth = 2F;
            this.xrLine44.LineWidth = 2;
            this.xrLine44.LocationFloat = new DevExpress.Utils.PointFloat(67.50006F, 3.71F);
            this.xrLine44.Name = "xrLine44";
            this.xrLine44.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine44.StylePriority.UseBorders = false;
            this.xrLine44.StylePriority.UseBorderWidth = false;
            // 
            // xrLine45
            // 
            this.xrLine45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine45.BorderWidth = 2F;
            this.xrLine45.LineWidth = 2;
            this.xrLine45.LocationFloat = new DevExpress.Utils.PointFloat(82.50009F, 3.71F);
            this.xrLine45.Name = "xrLine45";
            this.xrLine45.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine45.StylePriority.UseBorders = false;
            this.xrLine45.StylePriority.UseBorderWidth = false;
            // 
            // xrLine46
            // 
            this.xrLine46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine46.BorderWidth = 2F;
            this.xrLine46.LineWidth = 2;
            this.xrLine46.LocationFloat = new DevExpress.Utils.PointFloat(97.50005F, 3.71F);
            this.xrLine46.Name = "xrLine46";
            this.xrLine46.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine46.StylePriority.UseBorders = false;
            this.xrLine46.StylePriority.UseBorderWidth = false;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine47,
            this.xrLine48,
            this.xrLine49,
            this.xrLine50,
            this.xrLine51});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 1.4999994484583286D;
            // 
            // xrLine47
            // 
            this.xrLine47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine47.BorderWidth = 2F;
            this.xrLine47.LineWidth = 2;
            this.xrLine47.LocationFloat = new DevExpress.Utils.PointFloat(37.49995F, 3.71F);
            this.xrLine47.Name = "xrLine47";
            this.xrLine47.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine47.StylePriority.UseBorders = false;
            this.xrLine47.StylePriority.UseBorderWidth = false;
            // 
            // xrLine48
            // 
            this.xrLine48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine48.BorderWidth = 2F;
            this.xrLine48.LineWidth = 2;
            this.xrLine48.LocationFloat = new DevExpress.Utils.PointFloat(52.49998F, 3.71F);
            this.xrLine48.Name = "xrLine48";
            this.xrLine48.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine48.StylePriority.UseBorders = false;
            this.xrLine48.StylePriority.UseBorderWidth = false;
            // 
            // xrLine49
            // 
            this.xrLine49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine49.BorderWidth = 2F;
            this.xrLine49.LineWidth = 2;
            this.xrLine49.LocationFloat = new DevExpress.Utils.PointFloat(67.5F, 3.71F);
            this.xrLine49.Name = "xrLine49";
            this.xrLine49.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine49.StylePriority.UseBorders = false;
            this.xrLine49.StylePriority.UseBorderWidth = false;
            // 
            // xrLine50
            // 
            this.xrLine50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine50.BorderWidth = 2F;
            this.xrLine50.LineWidth = 2;
            this.xrLine50.LocationFloat = new DevExpress.Utils.PointFloat(82.50003F, 3.71F);
            this.xrLine50.Name = "xrLine50";
            this.xrLine50.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine50.StylePriority.UseBorders = false;
            this.xrLine50.StylePriority.UseBorderWidth = false;
            // 
            // xrLine51
            // 
            this.xrLine51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine51.BorderWidth = 2F;
            this.xrLine51.LineWidth = 2;
            this.xrLine51.LocationFloat = new DevExpress.Utils.PointFloat(97.49998F, 3.71F);
            this.xrLine51.Name = "xrLine51";
            this.xrLine51.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine51.StylePriority.UseBorders = false;
            this.xrLine51.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.txt3岁牙齿数目,
            this.xrTableCell146,
            this.txt3岁龋齿数,
            this.txt4岁牙齿数目,
            this.xrTableCell156,
            this.txt4岁龋齿数,
            this.txt5岁牙齿数目,
            this.xrTableCell158,
            this.txt5岁龋齿数,
            this.txt6岁牙齿数目,
            this.xrTableCell160,
            this.txt6岁龋齿数});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.StylePriority.UsePadding = false;
            this.xrTableRow20.Weight = 1.5200000000000005D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell99.Multiline = true;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UsePadding = false;
            this.xrTableCell99.Text = "体\r\n格";
            this.xrTableCell99.Weight = 0.28000001012166392D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell100.Multiline = true;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UsePadding = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "牙数（颗）/\r\n龋齿数";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell100.Weight = 1.2200000569534304D;
            // 
            // txt3岁牙齿数目
            // 
            this.txt3岁牙齿数目.Name = "txt3岁牙齿数目";
            this.txt3岁牙齿数目.StylePriority.UseTextAlignment = false;
            this.txt3岁牙齿数目.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt3岁牙齿数目.Weight = 0.70000002991358423D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Text = "/";
            this.xrTableCell146.Weight = 0.10000000478426602D;
            // 
            // txt3岁龋齿数
            // 
            this.txt3岁龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁龋齿数.Name = "txt3岁龋齿数";
            this.txt3岁龋齿数.StylePriority.UseBorders = false;
            this.txt3岁龋齿数.StylePriority.UseTextAlignment = false;
            this.txt3岁龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt3岁龋齿数.Weight = 0.70000002919832849D;
            // 
            // txt4岁牙齿数目
            // 
            this.txt4岁牙齿数目.Name = "txt4岁牙齿数目";
            this.txt4岁牙齿数目.StylePriority.UseTextAlignment = false;
            this.txt4岁牙齿数目.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt4岁牙齿数目.Weight = 0.70000002625783264D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Text = "/";
            this.xrTableCell156.Weight = 0.10000000295639022D;
            // 
            // txt4岁龋齿数
            // 
            this.txt4岁龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁龋齿数.Name = "txt4岁龋齿数";
            this.txt4岁龋齿数.StylePriority.UseBorders = false;
            this.txt4岁龋齿数.StylePriority.UseTextAlignment = false;
            this.txt4岁龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt4岁龋齿数.Weight = 0.70000002737045275D;
            // 
            // txt5岁牙齿数目
            // 
            this.txt5岁牙齿数目.Name = "txt5岁牙齿数目";
            this.txt5岁牙齿数目.StylePriority.UseTextAlignment = false;
            this.txt5岁牙齿数目.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt5岁牙齿数目.Weight = 0.70000002737045286D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "/";
            this.xrTableCell158.Weight = 0.10000000351270039D;
            // 
            // txt5岁龋齿数
            // 
            this.txt5岁龋齿数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁龋齿数.Name = "txt5岁龋齿数";
            this.txt5岁龋齿数.StylePriority.UseBorders = false;
            this.txt5岁龋齿数.StylePriority.UseTextAlignment = false;
            this.txt5岁龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt5岁龋齿数.Weight = 0.70000063827835035D;
            // 
            // txt6岁牙齿数目
            // 
            this.txt6岁牙齿数目.Name = "txt6岁牙齿数目";
            this.txt6岁牙齿数目.StylePriority.UseTextAlignment = false;
            this.txt6岁牙齿数目.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txt6岁牙齿数目.Weight = 0.70000002737045275D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Text = "/";
            this.xrTableCell160.Weight = 0.10000000351270033D;
            // 
            // txt6岁龋齿数
            // 
            this.txt6岁龋齿数.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁龋齿数.Name = "txt6岁龋齿数";
            this.txt6岁龋齿数.StylePriority.UseBorders = false;
            this.txt6岁龋齿数.StylePriority.UseTextAlignment = false;
            this.txt6岁龋齿数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt6岁龋齿数.Weight = 0.69999941757517548D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell136,
            this.xrTableCell113,
            this.xrTableCell145,
            this.xrTableCell137,
            this.xrTableCell114,
            this.xrTableCell138,
            this.xrTableCell115,
            this.xrTableCell139,
            this.xrTableCell116});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.80000000000000016D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Text = "检";
            this.xrTableCell111.Weight = 0.28000001012166392D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UsePadding = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "心肺";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 1.2200000569534304D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "1未见异常 2异常";
            this.xrTableCell136.Weight = 1.3333331855667878D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁心肺});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Weight = 0.16666687832939098D;
            // 
            // txt3岁心肺
            // 
            this.txt3岁心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁心肺.BorderWidth = 1F;
            this.txt3岁心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.txt3岁心肺.Name = "txt3岁心肺";
            this.txt3岁心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁心肺.StylePriority.UseBorders = false;
            this.txt3岁心肺.StylePriority.UseBorderWidth = false;
            this.txt3岁心肺.StylePriority.UseFont = false;
            this.txt3岁心肺.StylePriority.UsePadding = false;
            this.txt3岁心肺.StylePriority.UseTextAlignment = false;
            this.txt3岁心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "xrTableCell145";
            this.xrTableCell145.Weight = 0.666674983289844D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "1未见异常 2异常";
            this.xrTableCell137.Weight = 0.666674983289844D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁心肺});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.16665009000498776D;
            // 
            // txt4岁心肺
            // 
            this.txt4岁心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁心肺.BorderWidth = 1F;
            this.txt4岁心肺.CanGrow = false;
            this.txt4岁心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt4岁心肺.Name = "txt4岁心肺";
            this.txt4岁心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁心肺.StylePriority.UseBorders = false;
            this.txt4岁心肺.StylePriority.UseBorderWidth = false;
            this.txt4岁心肺.StylePriority.UseFont = false;
            this.txt4岁心肺.StylePriority.UsePadding = false;
            this.txt4岁心肺.StylePriority.UseTextAlignment = false;
            this.txt4岁心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "1未见异常 2异常";
            this.xrTableCell138.Weight = 1.3400000534121195D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁心肺});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.16000061574938407D;
            // 
            // txt5岁心肺
            // 
            this.txt5岁心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁心肺.BorderWidth = 1F;
            this.txt5岁心肺.CanGrow = false;
            this.txt5岁心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt5岁心肺.Name = "txt5岁心肺";
            this.txt5岁心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁心肺.StylePriority.UseBorders = false;
            this.txt5岁心肺.StylePriority.UseBorderWidth = false;
            this.txt5岁心肺.StylePriority.UseFont = false;
            this.txt5岁心肺.StylePriority.UsePadding = false;
            this.txt5岁心肺.StylePriority.UseTextAlignment = false;
            this.txt5岁心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "1未见异常 2异常";
            this.xrTableCell139.Weight = 1.3298660808317984D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell116.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁心肺});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.1701333676265302D;
            // 
            // txt6岁心肺
            // 
            this.txt6岁心肺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁心肺.BorderWidth = 1F;
            this.txt6岁心肺.CanGrow = false;
            this.txt6岁心肺.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁心肺.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt6岁心肺.Name = "txt6岁心肺";
            this.txt6岁心肺.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁心肺.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁心肺.StylePriority.UseBorders = false;
            this.txt6岁心肺.StylePriority.UseBorderWidth = false;
            this.txt6岁心肺.StylePriority.UseFont = false;
            this.txt6岁心肺.StylePriority.UsePadding = false;
            this.txt6岁心肺.StylePriority.UseTextAlignment = false;
            this.txt6岁心肺.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell140,
            this.xrTableCell119,
            this.xrTableCell141,
            this.xrTableCell120,
            this.xrTableCell142,
            this.xrTableCell121,
            this.xrTableCell143,
            this.xrTableCell122});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.80000000000000016D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Text = "查";
            this.xrTableCell117.Weight = 0.28000001012166392D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.StylePriority.UsePadding = false;
            this.xrTableCell118.StylePriority.UseTextAlignment = false;
            this.xrTableCell118.Text = "腹部";
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell118.Weight = 1.2200000569534304D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "1未见异常 2异常";
            this.xrTableCell140.Weight = 1.3333331855667878D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁腹部});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.16666687832939098D;
            // 
            // txt3岁腹部
            // 
            this.txt3岁腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁腹部.BorderWidth = 1F;
            this.txt3岁腹部.CanGrow = false;
            this.txt3岁腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5F);
            this.txt3岁腹部.Name = "txt3岁腹部";
            this.txt3岁腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁腹部.StylePriority.UseBorders = false;
            this.txt3岁腹部.StylePriority.UseBorderWidth = false;
            this.txt3岁腹部.StylePriority.UseFont = false;
            this.txt3岁腹部.StylePriority.UsePadding = false;
            this.txt3岁腹部.StylePriority.UseTextAlignment = false;
            this.txt3岁腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "1未见异常 2异常";
            this.xrTableCell141.Weight = 1.333349966579688D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁腹部});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.16665009000498776D;
            // 
            // txt4岁腹部
            // 
            this.txt4岁腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁腹部.BorderWidth = 1F;
            this.txt4岁腹部.CanGrow = false;
            this.txt4岁腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999987F);
            this.txt4岁腹部.Name = "txt4岁腹部";
            this.txt4岁腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁腹部.StylePriority.UseBorders = false;
            this.txt4岁腹部.StylePriority.UseBorderWidth = false;
            this.txt4岁腹部.StylePriority.UseFont = false;
            this.txt4岁腹部.StylePriority.UsePadding = false;
            this.txt4岁腹部.StylePriority.UseTextAlignment = false;
            this.txt4岁腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Text = "1未见异常 2异常";
            this.xrTableCell142.Weight = 1.340000663763707D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁腹部});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.16000000539779669D;
            // 
            // txt5岁腹部
            // 
            this.txt5岁腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁腹部.BorderWidth = 1F;
            this.txt5岁腹部.CanGrow = false;
            this.txt5岁腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000019F);
            this.txt5岁腹部.Name = "txt5岁腹部";
            this.txt5岁腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁腹部.StylePriority.UseBorders = false;
            this.txt5岁腹部.StylePriority.UseBorderWidth = false;
            this.txt5岁腹部.StylePriority.UseFont = false;
            this.txt5岁腹部.StylePriority.UsePadding = false;
            this.txt5岁腹部.StylePriority.UseTextAlignment = false;
            this.txt5岁腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "1未见异常 2异常";
            this.xrTableCell143.Weight = 1.3298666911833856D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁腹部});
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Weight = 0.17013275727494293D;
            // 
            // txt6岁腹部
            // 
            this.txt6岁腹部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁腹部.BorderWidth = 1F;
            this.txt6岁腹部.CanGrow = false;
            this.txt6岁腹部.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁腹部.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.000019F);
            this.txt6岁腹部.Name = "txt6岁腹部";
            this.txt6岁腹部.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁腹部.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁腹部.StylePriority.UseBorders = false;
            this.txt6岁腹部.StylePriority.UseBorderWidth = false;
            this.txt6岁腹部.StylePriority.UseFont = false;
            this.txt6岁腹部.StylePriority.UsePadding = false;
            this.txt6岁腹部.StylePriority.UseTextAlignment = false;
            this.txt6岁腹部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell30,
            this.xrTableCell149,
            this.xrTableCell31,
            this.xrTableCell150,
            this.xrTableCell32,
            this.xrTableCell151,
            this.xrTableCell33,
            this.xrTableCell152});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.StylePriority.UseBorders = false;
            this.xrTableRow28.Weight = 0.80000000000000027D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.28000001012166392D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell148.StylePriority.UsePadding = false;
            this.xrTableCell148.StylePriority.UseTextAlignment = false;
            this.xrTableCell148.Text = "血红蛋白值";
            this.xrTableCell148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell148.Weight = 1.2200000569534304D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁血红});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 0.92590107059966131D;
            // 
            // txt3岁血红
            // 
            this.txt3岁血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁血红.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.txt3岁血红.Name = "txt3岁血红";
            this.txt3岁血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt3岁血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Text = "g/L";
            this.xrTableCell149.Weight = 0.57409899329651748D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁血红});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.92180057239229451D;
            // 
            // txt4岁血红
            // 
            this.txt4岁血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁血红.LocationFloat = new DevExpress.Utils.PointFloat(4.589876F, 0F);
            this.txt4岁血红.Name = "txt4岁血红";
            this.txt4岁血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt4岁血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Text = "g/L";
            this.xrTableCell150.Weight = 0.5781994841923811D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁血红});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.92263614482797685D;
            // 
            // txt5岁血红
            // 
            this.txt5岁血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁血红.LocationFloat = new DevExpress.Utils.PointFloat(4.673386F, 0F);
            this.txt5岁血红.Name = "txt5岁血红";
            this.txt5岁血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt5岁血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Text = "g/L";
            this.xrTableCell151.Weight = 0.5773645243335267D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁血红});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.925899999941252D;
            // 
            // txt6岁血红
            // 
            this.txt6岁血红.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁血红.LocationFloat = new DevExpress.Utils.PointFloat(5.000051F, 1.999982F);
            this.txt6岁血红.Name = "txt6岁血红";
            this.txt6岁血红.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁血红.SizeF = new System.Drawing.SizeF(87.59012F, 18F);
            this.txt6岁血红.StylePriority.UseBorders = false;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Text = "g/L";
            this.xrTableCell152.Weight = 0.5740994485170765D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.txt3岁其他,
            this.txt4岁其他,
            this.txt5岁其他,
            this.txt6岁其他});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.Weight = 0.80000000000000027D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.28000001012166392D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell154.StylePriority.UsePadding = false;
            this.xrTableCell154.StylePriority.UseTextAlignment = false;
            this.xrTableCell154.Text = "其他";
            this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell154.Weight = 1.2200000569534304D;
            // 
            // txt3岁其他
            // 
            this.txt3岁其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁其他.Name = "txt3岁其他";
            this.txt3岁其他.StylePriority.UseBorders = false;
            this.txt3岁其他.Weight = 1.4999997587203853D;
            // 
            // txt4岁其他
            // 
            this.txt4岁其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁其他.Name = "txt4岁其他";
            this.txt4岁其他.StylePriority.UseBorders = false;
            this.txt4岁其他.Weight = 1.5000006669362629D;
            // 
            // txt5岁其他
            // 
            this.txt5岁其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁其他.Name = "txt5岁其他";
            this.txt5岁其他.StylePriority.UseBorders = false;
            this.txt5岁其他.Weight = 1.4999994484583288D;
            // 
            // txt6岁其他
            // 
            this.txt6岁其他.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁其他.Name = "txt6岁其他";
            this.txt6岁其他.StylePriority.UseBorders = false;
            this.txt6岁其他.Weight = 1.5000003639857096D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell185,
            this.xrTableCell187,
            this.xrTableCell189,
            this.xrTableCell190});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 4.0000000000000009D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "两次随访间患病情况";
            this.xrTableCell183.Weight = 1.5000000670750944D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 1.4959010937930213D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.24F, 12F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16});
            this.xrTable4.SizeF = new System.Drawing.SizeF(149.347F, 100.3674F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.txt3岁无,
            this.xrTableCell51});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.8D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "1无";
            this.xrTableCell49.Weight = 0.88384761809987034D;
            // 
            // txt3岁无
            // 
            this.txt3岁无.Name = "txt3岁无";
            this.txt3岁无.Weight = 1.4603385393731623D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.65581384252696728D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.8D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "2肺炎";
            this.xrTableCell52.Weight = 0.88384761809987034D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁肺炎});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 1.4603385393731623D;
            // 
            // txt3岁肺炎
            // 
            this.txt3岁肺炎.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁肺炎.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt3岁肺炎.Name = "txt3岁肺炎";
            this.txt3岁肺炎.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁肺炎.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt3岁肺炎.StylePriority.UseBorders = false;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "次";
            this.xrTableCell54.Weight = 0.65581384252696728D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.8D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "3腹泻";
            this.xrTableCell57.Weight = 0.88384761809987034D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁腹泻});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 1.4603385393731623D;
            // 
            // txt3岁腹泻
            // 
            this.txt3岁腹泻.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁腹泻.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt3岁腹泻.Name = "txt3岁腹泻";
            this.txt3岁腹泻.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁腹泻.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt3岁腹泻.StylePriority.UseBorders = false;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Text = "次";
            this.xrTableCell59.Weight = 0.65581384252696728D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.8D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "4外伤";
            this.xrTableCell60.Weight = 0.88384761809987034D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁外伤});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 1.4603385393731623D;
            // 
            // txt3岁外伤
            // 
            this.txt3岁外伤.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁外伤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt3岁外伤.Name = "txt3岁外伤";
            this.txt3岁外伤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁外伤.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt3岁外伤.StylePriority.UseBorders = false;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "次";
            this.xrTableCell62.Weight = 0.65581384252696728D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.8D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Text = "5其他";
            this.xrTableCell63.Weight = 0.88384761809987034D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁两次其他});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 1.4603385393731623D;
            // 
            // txt3岁两次其他
            // 
            this.txt3岁两次其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁两次其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt3岁两次其他.Name = "txt3岁两次其他";
            this.txt3岁两次其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁两次其他.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt3岁两次其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.65581384252696728D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 1.5008341056845209D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow11});
            this.xrTable3.SizeF = new System.Drawing.SizeF(149.347F, 100F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.txt4岁无,
            this.xrTableCell36});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.8D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "1无";
            this.xrTableCell34.Weight = 0.88384761809987034D;
            // 
            // txt4岁无
            // 
            this.txt4岁无.Name = "txt4岁无";
            this.txt4岁无.Weight = 1.4603385393731623D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.65581384252696728D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.8D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "2肺炎";
            this.xrTableCell37.Weight = 0.88384761809987034D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁肺炎});
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 1.4603385393731623D;
            // 
            // txt4岁肺炎
            // 
            this.txt4岁肺炎.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁肺炎.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt4岁肺炎.Name = "txt4岁肺炎";
            this.txt4岁肺炎.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁肺炎.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt4岁肺炎.StylePriority.UseBorders = false;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "次";
            this.xrTableCell39.Weight = 0.65581384252696728D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.8D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "3腹泻";
            this.xrTableCell40.Weight = 0.88384761809987034D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁腹泻});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 1.4603385393731623D;
            // 
            // txt4岁腹泻
            // 
            this.txt4岁腹泻.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁腹泻.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt4岁腹泻.Name = "txt4岁腹泻";
            this.txt4岁腹泻.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁腹泻.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt4岁腹泻.StylePriority.UseBorders = false;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "次";
            this.xrTableCell42.Weight = 0.65581384252696728D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.8D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "4外伤";
            this.xrTableCell43.Weight = 0.88384761809987034D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁外伤});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 1.4603385393731623D;
            // 
            // txt4岁外伤
            // 
            this.txt4岁外伤.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁外伤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt4岁外伤.Name = "txt4岁外伤";
            this.txt4岁外伤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁外伤.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt4岁外伤.StylePriority.UseBorders = false;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "次";
            this.xrTableCell45.Weight = 0.65581384252696728D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.8D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "5其他";
            this.xrTableCell46.Weight = 0.88384761809987034D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁两次其他});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 1.4603385393731623D;
            // 
            // txt4岁两次其他
            // 
            this.txt4岁两次其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁两次其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt4岁两次其他.Name = "txt4岁两次其他";
            this.txt4岁两次其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁两次其他.SizeF = new System.Drawing.SizeF(72.69904F, 18.00003F);
            this.txt4岁两次其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.65581384252696728D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 1.5032654386895397D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(2.15F, 12F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow21,
            this.xrTableRow24,
            this.xrTableRow25});
            this.xrTable5.SizeF = new System.Drawing.SizeF(146.4825F, 100F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.txt5岁无,
            this.xrTableCell68});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.8D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "1无";
            this.xrTableCell66.Weight = 0.88384761809987034D;
            // 
            // txt5岁无
            // 
            this.txt5岁无.Name = "txt5岁无";
            this.txt5岁无.Weight = 1.4603385393731623D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.65581384252696728D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.8D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "2肺炎";
            this.xrTableCell69.Weight = 0.88384761809987034D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁肺炎});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 1.4603385393731623D;
            // 
            // txt5岁肺炎
            // 
            this.txt5岁肺炎.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁肺炎.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt5岁肺炎.Name = "txt5岁肺炎";
            this.txt5岁肺炎.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁肺炎.SizeF = new System.Drawing.SizeF(71.30466F, 18.00003F);
            this.txt5岁肺炎.StylePriority.UseBorders = false;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Text = "次";
            this.xrTableCell71.Weight = 0.65581384252696728D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.8D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "3腹泻";
            this.xrTableCell72.Weight = 0.88384761809987034D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁腹泻});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 1.4603385393731623D;
            // 
            // txt5岁腹泻
            // 
            this.txt5岁腹泻.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁腹泻.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt5岁腹泻.Name = "txt5岁腹泻";
            this.txt5岁腹泻.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁腹泻.SizeF = new System.Drawing.SizeF(71.3046F, 18.00003F);
            this.txt5岁腹泻.StylePriority.UseBorders = false;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "次";
            this.xrTableCell74.Weight = 0.65581384252696728D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.8D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "4外伤";
            this.xrTableCell75.Weight = 0.88384761809987034D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁外伤});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 1.4603385393731623D;
            // 
            // txt5岁外伤
            // 
            this.txt5岁外伤.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁外伤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt5岁外伤.Name = "txt5岁外伤";
            this.txt5岁外伤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁外伤.SizeF = new System.Drawing.SizeF(71.30447F, 18.00003F);
            this.txt5岁外伤.StylePriority.UseBorders = false;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Text = "次";
            this.xrTableCell77.Weight = 0.65581384252696728D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.8D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "5其他";
            this.xrTableCell78.Weight = 0.88384761809987034D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁两次其他});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 1.4603385393731623D;
            // 
            // txt5岁两次其他
            // 
            this.txt5岁两次其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁两次其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt5岁两次其他.Name = "txt5岁两次其他";
            this.txt5岁两次其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁两次其他.SizeF = new System.Drawing.SizeF(71.30441F, 18.00003F);
            this.txt5岁两次其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.65581384252696728D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Weight = 1.4999995999336051D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(1.59F, 12F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32});
            this.xrTable6.SizeF = new System.Drawing.SizeF(143.8979F, 98.36723F);
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.txt6岁无,
            this.xrTableCell83});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Text = "1无";
            this.xrTableCell81.Weight = 0.91731671951127014D;
            // 
            // txt6岁无
            // 
            this.txt6岁无.Name = "txt6岁无";
            this.txt6岁无.Weight = 1.3964087721967951D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.68627450829193481D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "2肺炎";
            this.xrTableCell84.Weight = 0.91731671951127014D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁肺炎});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 1.3964087721967951D;
            // 
            // txt6岁肺炎
            // 
            this.txt6岁肺炎.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁肺炎.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt6岁肺炎.Name = "txt6岁肺炎";
            this.txt6岁肺炎.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁肺炎.SizeF = new System.Drawing.SizeF(66.91003F, 18.00003F);
            this.txt6岁肺炎.StylePriority.UseBorders = false;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Text = "次";
            this.xrTableCell86.Weight = 0.68627450829193481D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell105});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Text = "3腹泻";
            this.xrTableCell87.Weight = 0.91731671951127014D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁腹泻});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 1.3964087721967951D;
            // 
            // txt6岁腹泻
            // 
            this.txt6岁腹泻.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁腹泻.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt6岁腹泻.Name = "txt6岁腹泻";
            this.txt6岁腹泻.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁腹泻.SizeF = new System.Drawing.SizeF(66.91003F, 18.00003F);
            this.txt6岁腹泻.StylePriority.UseBorders = false;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "次";
            this.xrTableCell105.Weight = 0.68627450829193481D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "4外伤";
            this.xrTableCell106.Weight = 0.91731671951127014D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁外伤});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 1.3964087721967951D;
            // 
            // txt6岁外伤
            // 
            this.txt6岁外伤.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁外伤.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.591843F);
            this.txt6岁外伤.Name = "txt6岁外伤";
            this.txt6岁外伤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁外伤.SizeF = new System.Drawing.SizeF(66.9801F, 18.00003F);
            this.txt6岁外伤.StylePriority.UseBorders = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "次";
            this.xrTableCell108.Weight = 0.68627450829193481D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell123});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1.0208351228000685D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Text = "5其他";
            this.xrTableCell109.Weight = 0.91731671951127014D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁两次其他});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 1.3964087721967951D;
            // 
            // txt6岁两次其他
            // 
            this.txt6岁两次其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁两次其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.632532F);
            this.txt6岁两次其他.Name = "txt6岁两次其他";
            this.txt6岁两次其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁两次其他.SizeF = new System.Drawing.SizeF(65F, 16F);
            this.txt6岁两次其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.68627450829193481D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.Weight = 4.4000000000000021D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell196.StylePriority.UsePadding = false;
            this.xrTableCell196.StylePriority.UseTextAlignment = false;
            this.xrTableCell196.Text = "转诊建议";
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell196.Weight = 1.5000000670750944D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 1.4959010937930211D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0.41F, 29F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow37});
            this.xrTable2.SizeF = new System.Drawing.SizeF(148.1801F, 80F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell203});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "1有 2 无";
            this.xrTableCell35.Weight = 2.3592035778324894D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁转诊建议});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.64079642216751065D;
            // 
            // txt3岁转诊建议
            // 
            this.txt3岁转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁转诊建议.BorderWidth = 1F;
            this.txt3岁转诊建议.CanGrow = false;
            this.txt3岁转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(7.025941F, 6.666756F);
            this.txt3岁转诊建议.Name = "txt3岁转诊建议";
            this.txt3岁转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁转诊建议.StylePriority.UseBorders = false;
            this.txt3岁转诊建议.StylePriority.UseBorderWidth = false;
            this.txt3岁转诊建议.StylePriority.UseFont = false;
            this.txt3岁转诊建议.StylePriority.UsePadding = false;
            this.txt3岁转诊建议.StylePriority.UseTextAlignment = false;
            this.txt3岁转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Text = "原因：";
            this.xrTableCell205.Weight = 1.0122814590239646D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁转诊原因});
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Weight = 1.9877185409760354D;
            // 
            // txt3岁转诊原因
            // 
            this.txt3岁转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504639F, 1.999939F);
            this.txt3岁转诊原因.Name = "txt3岁转诊原因";
            this.txt3岁转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁转诊原因.SizeF = new System.Drawing.SizeF(95.67531F, 18.00006F);
            this.txt3岁转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell207,
            this.xrTableCell208});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "机构及科室：";
            this.xrTableCell207.Weight = 1.903089105596592D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel52});
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Weight = 1.0969108944034081D;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel52.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁转诊机构});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 3D;
            // 
            // txt3岁转诊机构
            // 
            this.txt3岁转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.txt3岁转诊机构.Name = "txt3岁转诊机构";
            this.txt3岁转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁转诊机构.SizeF = new System.Drawing.SizeF(148.18F, 15F);
            this.txt3岁转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 1.5008341056845209D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable7.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(1.49339F, 28.9999F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43});
            this.xrTable7.SizeF = new System.Drawing.SizeF(147.8536F, 80F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell201});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "1有 2 无";
            this.xrTableCell50.Weight = 2.337562620398042D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁转诊建议});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.66243737960195814D;
            // 
            // txt4岁转诊建议
            // 
            this.txt4岁转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁转诊建议.BorderWidth = 1F;
            this.txt4岁转诊建议.CanGrow = false;
            this.txt4岁转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 6.666819F);
            this.txt4岁转诊建议.Name = "txt4岁转诊建议";
            this.txt4岁转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁转诊建议.StylePriority.UseBorders = false;
            this.txt4岁转诊建议.StylePriority.UseBorderWidth = false;
            this.txt4岁转诊建议.StylePriority.UseFont = false;
            this.txt4岁转诊建议.StylePriority.UsePadding = false;
            this.txt4岁转诊建议.StylePriority.UseTextAlignment = false;
            this.txt4岁转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell209});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Text = "原因：";
            this.xrTableCell202.Weight = 1.0122814590239646D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁转诊原因});
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 1.9877185409760354D;
            // 
            // txt4岁转诊原因
            // 
            this.txt4岁转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt4岁转诊原因.Name = "txt4岁转诊原因";
            this.txt4岁转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt4岁转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.xrTableCell211});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "机构及科室：";
            this.xrTableCell210.Weight = 1.903089105596592D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel54});
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 1.0969108944034081D;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel54.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁转诊机构});
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 3D;
            // 
            // txt4岁转诊机构
            // 
            this.txt4岁转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt4岁转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt4岁转诊机构.Name = "txt4岁转诊机构";
            this.txt4岁转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt4岁转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 1.5032648283379522D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable8.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 28.99987F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(147.4999F, 80F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell213});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "1有 2 无";
            this.xrTableCell67.Weight = 2.3717441987852341D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁转诊建议});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 0.6282558012147661D;
            // 
            // txt5岁转诊建议
            // 
            this.txt5岁转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁转诊建议.BorderWidth = 1F;
            this.txt5岁转诊建议.CanGrow = false;
            this.txt5岁转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(8.090718F, 6.666883F);
            this.txt5岁转诊建议.Name = "txt5岁转诊建议";
            this.txt5岁转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁转诊建议.StylePriority.UseBorders = false;
            this.txt5岁转诊建议.StylePriority.UseBorderWidth = false;
            this.txt5岁转诊建议.StylePriority.UseFont = false;
            this.txt5岁转诊建议.StylePriority.UsePadding = false;
            this.txt5岁转诊建议.StylePriority.UseTextAlignment = false;
            this.txt5岁转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "原因：";
            this.xrTableCell214.Weight = 1.0122814590239646D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁转诊原因});
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 1.9877185409760354D;
            // 
            // txt5岁转诊原因
            // 
            this.txt5岁转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(2.504608F, 1.999939F);
            this.txt5岁转诊原因.Name = "txt5岁转诊原因";
            this.txt5岁转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁转诊原因.SizeF = new System.Drawing.SizeF(93.40784F, 18.00006F);
            this.txt5岁转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell217});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Text = "机构及科室：";
            this.xrTableCell216.Weight = 1.903089105596592D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel57});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 1.0969108944034081D;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(52.00948F, 18.00006F);
            this.xrLabel57.StylePriority.UseBorders = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁转诊机构});
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 3D;
            // 
            // txt5岁转诊机构
            // 
            this.txt5岁转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt5岁转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt5岁转诊机构.Name = "txt5岁转诊机构";
            this.txt5岁转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁转诊机构.SizeF = new System.Drawing.SizeF(145.8023F, 15F);
            this.txt5岁转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 1.5000002102851926D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable9.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(1.409912F, 29.00009F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51});
            this.xrTable9.SizeF = new System.Drawing.SizeF(143.4101F, 76.99982F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82,
            this.xrTableCell219});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "1有 2 无";
            this.xrTableCell82.Weight = 2.32536130141286D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁转诊建议});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.67463869858713987D;
            // 
            // txt6岁转诊建议
            // 
            this.txt6岁转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁转诊建议.BorderWidth = 1F;
            this.txt6岁转诊建议.CanGrow = false;
            this.txt6岁转诊建议.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 6.666628F);
            this.txt6岁转诊建议.Name = "txt6岁转诊建议";
            this.txt6岁转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁转诊建议.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁转诊建议.StylePriority.UseBorders = false;
            this.txt6岁转诊建议.StylePriority.UseBorderWidth = false;
            this.txt6岁转诊建议.StylePriority.UseFont = false;
            this.txt6岁转诊建议.StylePriority.UsePadding = false;
            this.txt6岁转诊建议.StylePriority.UseTextAlignment = false;
            this.txt6岁转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell220,
            this.xrTableCell221});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Text = "原因：";
            this.xrTableCell220.Weight = 1.0911104591946297D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁转诊原因});
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 1.9088895408053703D;
            // 
            // txt6岁转诊原因
            // 
            this.txt6岁转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt6岁转诊原因.Name = "txt6岁转诊原因";
            this.txt6岁转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁转诊原因.SizeF = new System.Drawing.SizeF(86.84119F, 18.00006F);
            this.txt6岁转诊原因.StylePriority.UseBorders = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "机构及科室：";
            this.xrTableCell222.Weight = 1.9663888322345193D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel60});
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 1.0336111677654807D;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.249878F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(46.48822F, 18.00012F);
            this.xrLabel60.StylePriority.UseBorders = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁转诊机构});
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Weight = 3D;
            // 
            // txt6岁转诊机构
            // 
            this.txt6岁转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt6岁转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999939F);
            this.txt6岁转诊机构.Name = "txt6岁转诊机构";
            this.txt6岁转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁转诊机构.SizeF = new System.Drawing.SizeF(139F, 15F);
            this.txt6岁转诊机构.StylePriority.UseBorders = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell132,
            this.xrTableCell226,
            this.xrTableCell133,
            this.xrTableCell227,
            this.xrTableCell134,
            this.xrTableCell228,
            this.xrTableCell135,
            this.xrTableCell229});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.StylePriority.UseBorders = false;
            this.xrTableRow52.Weight = 4.4000000000000021D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell225.StylePriority.UsePadding = false;
            this.xrTableCell225.StylePriority.UseTextAlignment = false;
            this.xrTableCell225.Text = "指导";
            this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell225.Weight = 1.5000000670750944D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Multiline = true;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell132.Weight = 1.1693907648506203D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell226.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁合理,
            this.txt3岁低盐,
            this.txt3岁生长,
            this.txt3岁口腔保健,
            this.txt3岁疾病,
            this.txt3岁预防});
            this.xrTableCell226.Multiline = true;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.32651032894240078D;
            // 
            // txt3岁合理
            // 
            this.txt3岁合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁合理.BorderWidth = 1F;
            this.txt3岁合理.CanGrow = false;
            this.txt3岁合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁合理.LocationFloat = new DevExpress.Utils.PointFloat(7.03F, 18F);
            this.txt3岁合理.Name = "txt3岁合理";
            this.txt3岁合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁合理.StylePriority.UseBorders = false;
            this.txt3岁合理.StylePriority.UseBorderWidth = false;
            this.txt3岁合理.StylePriority.UseFont = false;
            this.txt3岁合理.StylePriority.UsePadding = false;
            this.txt3岁合理.StylePriority.UseTextAlignment = false;
            this.txt3岁合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3岁低盐
            // 
            this.txt3岁低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁低盐.BorderWidth = 1F;
            this.txt3岁低盐.CanGrow = false;
            this.txt3岁低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁低盐.LocationFloat = new DevExpress.Utils.PointFloat(7.03F, 102F);
            this.txt3岁低盐.Name = "txt3岁低盐";
            this.txt3岁低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁低盐.StylePriority.UseBorders = false;
            this.txt3岁低盐.StylePriority.UseBorderWidth = false;
            this.txt3岁低盐.StylePriority.UseFont = false;
            this.txt3岁低盐.StylePriority.UsePadding = false;
            this.txt3岁低盐.StylePriority.UseTextAlignment = false;
            this.txt3岁低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3岁生长
            // 
            this.txt3岁生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁生长.BorderWidth = 1F;
            this.txt3岁生长.CanGrow = false;
            this.txt3岁生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁生长.LocationFloat = new DevExpress.Utils.PointFloat(7.03F, 33F);
            this.txt3岁生长.Name = "txt3岁生长";
            this.txt3岁生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁生长.StylePriority.UseBorders = false;
            this.txt3岁生长.StylePriority.UseBorderWidth = false;
            this.txt3岁生长.StylePriority.UseFont = false;
            this.txt3岁生长.StylePriority.UseTextAlignment = false;
            this.txt3岁生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3岁口腔保健
            // 
            this.txt3岁口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁口腔保健.BorderWidth = 1F;
            this.txt3岁口腔保健.CanGrow = false;
            this.txt3岁口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(7.025877F, 85.85657F);
            this.txt3岁口腔保健.Name = "txt3岁口腔保健";
            this.txt3岁口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁口腔保健.StylePriority.UseBorders = false;
            this.txt3岁口腔保健.StylePriority.UseBorderWidth = false;
            this.txt3岁口腔保健.StylePriority.UseFont = false;
            this.txt3岁口腔保健.StylePriority.UsePadding = false;
            this.txt3岁口腔保健.StylePriority.UseTextAlignment = false;
            this.txt3岁口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3岁疾病
            // 
            this.txt3岁疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁疾病.BorderWidth = 1F;
            this.txt3岁疾病.CanGrow = false;
            this.txt3岁疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁疾病.LocationFloat = new DevExpress.Utils.PointFloat(7.03F, 50F);
            this.txt3岁疾病.Name = "txt3岁疾病";
            this.txt3岁疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁疾病.StylePriority.UseBorders = false;
            this.txt3岁疾病.StylePriority.UseBorderWidth = false;
            this.txt3岁疾病.StylePriority.UseFont = false;
            this.txt3岁疾病.StylePriority.UseTextAlignment = false;
            this.txt3岁疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt3岁预防
            // 
            this.txt3岁预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt3岁预防.BorderWidth = 1F;
            this.txt3岁预防.CanGrow = false;
            this.txt3岁预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3岁预防.LocationFloat = new DevExpress.Utils.PointFloat(7.03F, 67F);
            this.txt3岁预防.Name = "txt3岁预防";
            this.txt3岁预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt3岁预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt3岁预防.StylePriority.UseBorders = false;
            this.txt3岁预防.StylePriority.UseBorderWidth = false;
            this.txt3岁预防.StylePriority.UseFont = false;
            this.txt3岁预防.StylePriority.UsePadding = false;
            this.txt3岁预防.StylePriority.UseTextAlignment = false;
            this.txt3岁预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Multiline = true;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell133.Weight = 1.1669904035329073D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell227.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁合理,
            this.txt4岁低盐,
            this.txt4岁生长,
            this.txt4岁口腔保健,
            this.txt4岁预防,
            this.txt4岁疾病});
            this.xrTableCell227.Multiline = true;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.33384370215161369D;
            // 
            // txt4岁合理
            // 
            this.txt4岁合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁合理.BorderWidth = 1F;
            this.txt4岁合理.CanGrow = false;
            this.txt4岁合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁合理.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 18.00003F);
            this.txt4岁合理.Name = "txt4岁合理";
            this.txt4岁合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁合理.StylePriority.UseBorders = false;
            this.txt4岁合理.StylePriority.UseBorderWidth = false;
            this.txt4岁合理.StylePriority.UseFont = false;
            this.txt4岁合理.StylePriority.UsePadding = false;
            this.txt4岁合理.StylePriority.UseTextAlignment = false;
            this.txt4岁合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt4岁低盐
            // 
            this.txt4岁低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁低盐.BorderWidth = 1F;
            this.txt4岁低盐.CanGrow = false;
            this.txt4岁低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁低盐.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 102F);
            this.txt4岁低盐.Name = "txt4岁低盐";
            this.txt4岁低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁低盐.StylePriority.UseBorders = false;
            this.txt4岁低盐.StylePriority.UseBorderWidth = false;
            this.txt4岁低盐.StylePriority.UseFont = false;
            this.txt4岁低盐.StylePriority.UsePadding = false;
            this.txt4岁低盐.StylePriority.UseTextAlignment = false;
            this.txt4岁低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt4岁生长
            // 
            this.txt4岁生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁生长.BorderWidth = 1F;
            this.txt4岁生长.CanGrow = false;
            this.txt4岁生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁生长.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 32.99999F);
            this.txt4岁生长.Name = "txt4岁生长";
            this.txt4岁生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁生长.StylePriority.UseBorders = false;
            this.txt4岁生长.StylePriority.UseBorderWidth = false;
            this.txt4岁生长.StylePriority.UseFont = false;
            this.txt4岁生长.StylePriority.UsePadding = false;
            this.txt4岁生长.StylePriority.UseTextAlignment = false;
            this.txt4岁生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt4岁口腔保健
            // 
            this.txt4岁口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁口腔保健.BorderWidth = 1F;
            this.txt4岁口腔保健.CanGrow = false;
            this.txt4岁口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 85.85657F);
            this.txt4岁口腔保健.Name = "txt4岁口腔保健";
            this.txt4岁口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁口腔保健.StylePriority.UseBorders = false;
            this.txt4岁口腔保健.StylePriority.UseBorderWidth = false;
            this.txt4岁口腔保健.StylePriority.UseFont = false;
            this.txt4岁口腔保健.StylePriority.UsePadding = false;
            this.txt4岁口腔保健.StylePriority.UseTextAlignment = false;
            this.txt4岁口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt4岁预防
            // 
            this.txt4岁预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁预防.BorderWidth = 1F;
            this.txt4岁预防.CanGrow = false;
            this.txt4岁预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁预防.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 67.00001F);
            this.txt4岁预防.Name = "txt4岁预防";
            this.txt4岁预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁预防.StylePriority.UseBorders = false;
            this.txt4岁预防.StylePriority.UseBorderWidth = false;
            this.txt4岁预防.StylePriority.UseFont = false;
            this.txt4岁预防.StylePriority.UsePadding = false;
            this.txt4岁预防.StylePriority.UseTextAlignment = false;
            this.txt4岁预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt4岁疾病
            // 
            this.txt4岁疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt4岁疾病.BorderWidth = 1F;
            this.txt4岁疾病.CanGrow = false;
            this.txt4岁疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4岁疾病.LocationFloat = new DevExpress.Utils.PointFloat(7.759222F, 50F);
            this.txt4岁疾病.Name = "txt4岁疾病";
            this.txt4岁疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt4岁疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt4岁疾病.StylePriority.UseBorders = false;
            this.txt4岁疾病.StylePriority.UseBorderWidth = false;
            this.txt4岁疾病.StylePriority.UseFont = false;
            this.txt4岁疾病.StylePriority.UsePadding = false;
            this.txt4岁疾病.StylePriority.UseTextAlignment = false;
            this.txt4岁疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Multiline = true;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell134.Weight = 1.1661066131628801D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell228.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁低盐,
            this.txt5岁合理,
            this.txt5岁口腔保健,
            this.txt5岁生长,
            this.txt5岁预防,
            this.txt5岁疾病});
            this.xrTableCell228.Multiline = true;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Weight = 0.33715821517507216D;
            // 
            // txt5岁低盐
            // 
            this.txt5岁低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁低盐.BorderWidth = 1F;
            this.txt5岁低盐.CanGrow = false;
            this.txt5岁低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁低盐.LocationFloat = new DevExpress.Utils.PointFloat(8.090718F, 102F);
            this.txt5岁低盐.Name = "txt5岁低盐";
            this.txt5岁低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁低盐.StylePriority.UseBorders = false;
            this.txt5岁低盐.StylePriority.UseBorderWidth = false;
            this.txt5岁低盐.StylePriority.UseFont = false;
            this.txt5岁低盐.StylePriority.UsePadding = false;
            this.txt5岁低盐.StylePriority.UseTextAlignment = false;
            this.txt5岁低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt5岁合理
            // 
            this.txt5岁合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁合理.BorderWidth = 1F;
            this.txt5岁合理.CanGrow = false;
            this.txt5岁合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁合理.LocationFloat = new DevExpress.Utils.PointFloat(8.090718F, 18F);
            this.txt5岁合理.Name = "txt5岁合理";
            this.txt5岁合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁合理.StylePriority.UseBorders = false;
            this.txt5岁合理.StylePriority.UseBorderWidth = false;
            this.txt5岁合理.StylePriority.UseFont = false;
            this.txt5岁合理.StylePriority.UsePadding = false;
            this.txt5岁合理.StylePriority.UseTextAlignment = false;
            this.txt5岁合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt5岁口腔保健
            // 
            this.txt5岁口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁口腔保健.BorderWidth = 1F;
            this.txt5岁口腔保健.CanGrow = false;
            this.txt5岁口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(8.090718F, 85.8566F);
            this.txt5岁口腔保健.Name = "txt5岁口腔保健";
            this.txt5岁口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁口腔保健.StylePriority.UseBorders = false;
            this.txt5岁口腔保健.StylePriority.UseBorderWidth = false;
            this.txt5岁口腔保健.StylePriority.UseFont = false;
            this.txt5岁口腔保健.StylePriority.UsePadding = false;
            this.txt5岁口腔保健.StylePriority.UseTextAlignment = false;
            this.txt5岁口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt5岁生长
            // 
            this.txt5岁生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁生长.BorderWidth = 1F;
            this.txt5岁生长.CanGrow = false;
            this.txt5岁生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁生长.LocationFloat = new DevExpress.Utils.PointFloat(8.090718F, 32.99999F);
            this.txt5岁生长.Name = "txt5岁生长";
            this.txt5岁生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁生长.StylePriority.UseBorders = false;
            this.txt5岁生长.StylePriority.UseBorderWidth = false;
            this.txt5岁生长.StylePriority.UseFont = false;
            this.txt5岁生长.StylePriority.UsePadding = false;
            this.txt5岁生长.StylePriority.UseTextAlignment = false;
            this.txt5岁生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt5岁预防
            // 
            this.txt5岁预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁预防.BorderWidth = 1F;
            this.txt5岁预防.CanGrow = false;
            this.txt5岁预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁预防.LocationFloat = new DevExpress.Utils.PointFloat(8.090591F, 67.00001F);
            this.txt5岁预防.Name = "txt5岁预防";
            this.txt5岁预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁预防.StylePriority.UseBorders = false;
            this.txt5岁预防.StylePriority.UseBorderWidth = false;
            this.txt5岁预防.StylePriority.UseFont = false;
            this.txt5岁预防.StylePriority.UsePadding = false;
            this.txt5岁预防.StylePriority.UseTextAlignment = false;
            this.txt5岁预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt5岁疾病
            // 
            this.txt5岁疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt5岁疾病.BorderWidth = 1F;
            this.txt5岁疾病.CanGrow = false;
            this.txt5岁疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5岁疾病.LocationFloat = new DevExpress.Utils.PointFloat(8.090655F, 50.00003F);
            this.txt5岁疾病.Name = "txt5岁疾病";
            this.txt5岁疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt5岁疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt5岁疾病.StylePriority.UseBorders = false;
            this.txt5岁疾病.StylePriority.UseBorderWidth = false;
            this.txt5岁疾病.StylePriority.UseFont = false;
            this.txt5岁疾病.StylePriority.UsePadding = false;
            this.txt5岁疾病.StylePriority.UseTextAlignment = false;
            this.txt5岁疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "1 合理膳食\r\n2 生长发育\r\n3 疾病预防\r\n4 预防意外作害\r\n5 口腔保健\r\n6 低盐饮食";
            this.xrTableCell135.Weight = 1.125700193672059D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell229.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁合理,
            this.txt6岁低盐,
            this.txt6岁生长,
            this.txt6岁口腔保健,
            this.txt6岁疾病,
            this.txt6岁预防});
            this.xrTableCell229.Multiline = true;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.37430001661313372D;
            // 
            // txt6岁合理
            // 
            this.txt6岁合理.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁合理.BorderWidth = 1F;
            this.txt6岁合理.CanGrow = false;
            this.txt6岁合理.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁合理.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 18F);
            this.txt6岁合理.Name = "txt6岁合理";
            this.txt6岁合理.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁合理.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁合理.StylePriority.UseBorders = false;
            this.txt6岁合理.StylePriority.UseBorderWidth = false;
            this.txt6岁合理.StylePriority.UseFont = false;
            this.txt6岁合理.StylePriority.UsePadding = false;
            this.txt6岁合理.StylePriority.UseTextAlignment = false;
            this.txt6岁合理.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6岁低盐
            // 
            this.txt6岁低盐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁低盐.BorderWidth = 1F;
            this.txt6岁低盐.CanGrow = false;
            this.txt6岁低盐.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁低盐.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 102F);
            this.txt6岁低盐.Name = "txt6岁低盐";
            this.txt6岁低盐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁低盐.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁低盐.StylePriority.UseBorders = false;
            this.txt6岁低盐.StylePriority.UseBorderWidth = false;
            this.txt6岁低盐.StylePriority.UseFont = false;
            this.txt6岁低盐.StylePriority.UsePadding = false;
            this.txt6岁低盐.StylePriority.UseTextAlignment = false;
            this.txt6岁低盐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6岁生长
            // 
            this.txt6岁生长.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁生长.BorderWidth = 1F;
            this.txt6岁生长.CanGrow = false;
            this.txt6岁生长.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁生长.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 32.99999F);
            this.txt6岁生长.Name = "txt6岁生长";
            this.txt6岁生长.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁生长.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁生长.StylePriority.UseBorders = false;
            this.txt6岁生长.StylePriority.UseBorderWidth = false;
            this.txt6岁生长.StylePriority.UseFont = false;
            this.txt6岁生长.StylePriority.UsePadding = false;
            this.txt6岁生长.StylePriority.UseTextAlignment = false;
            this.txt6岁生长.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6岁口腔保健
            // 
            this.txt6岁口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁口腔保健.BorderWidth = 1F;
            this.txt6岁口腔保健.CanGrow = false;
            this.txt6岁口腔保健.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 85.8566F);
            this.txt6岁口腔保健.Name = "txt6岁口腔保健";
            this.txt6岁口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁口腔保健.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁口腔保健.StylePriority.UseBorders = false;
            this.txt6岁口腔保健.StylePriority.UseBorderWidth = false;
            this.txt6岁口腔保健.StylePriority.UseFont = false;
            this.txt6岁口腔保健.StylePriority.UsePadding = false;
            this.txt6岁口腔保健.StylePriority.UseTextAlignment = false;
            this.txt6岁口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6岁疾病
            // 
            this.txt6岁疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁疾病.BorderWidth = 1F;
            this.txt6岁疾病.CanGrow = false;
            this.txt6岁疾病.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁疾病.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 50F);
            this.txt6岁疾病.Name = "txt6岁疾病";
            this.txt6岁疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁疾病.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁疾病.StylePriority.UseBorders = false;
            this.txt6岁疾病.StylePriority.UseBorderWidth = false;
            this.txt6岁疾病.StylePriority.UseFont = false;
            this.txt6岁疾病.StylePriority.UsePadding = false;
            this.txt6岁疾病.StylePriority.UseTextAlignment = false;
            this.txt6岁疾病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt6岁预防
            // 
            this.txt6岁预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁预防.BorderWidth = 1F;
            this.txt6岁预防.CanGrow = false;
            this.txt6岁预防.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6岁预防.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 67.00004F);
            this.txt6岁预防.Name = "txt6岁预防";
            this.txt6岁预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt6岁预防.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.txt6岁预防.StylePriority.UseBorders = false;
            this.txt6岁预防.StylePriority.UseBorderWidth = false;
            this.txt6岁预防.StylePriority.UseFont = false;
            this.txt6岁预防.StylePriority.UsePadding = false;
            this.txt6岁预防.StylePriority.UseTextAlignment = false;
            this.txt6岁预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.txt4岁服务,
            this.txt5岁服务,
            this.txt6岁服务});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.StylePriority.UseBorders = false;
            this.xrTableRow53.Weight = 4.4000000000000021D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell230.StylePriority.UsePadding = false;
            this.xrTableCell230.StylePriority.UseTextAlignment = false;
            this.xrTableCell230.Text = "中医药健康服务";
            this.xrTableCell230.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell230.Weight = 1.5000000670750944D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 1.4959010937930211D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable11.Font = new System.Drawing.Font("仿宋", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62});
            this.xrTable11.SizeF = new System.Drawing.SizeF(148.5899F, 124.3191F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.StylePriority.UseFont = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1.9361006289308176D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Multiline = true;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "1中医饮食调养指导\r\n2中医起居调摄指导\r\n3传授按揉四神聪穴\r\n方法";
            this.xrTableCell124.Weight = 3.0054574798513976D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 0.063899371069182365D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 1D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 2.0054574798513976D;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 0.72201255942290687D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "4其他";
            this.xrTableCell127.Weight = 1D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel32});
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 2.0054574798513976D;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 0F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(99.14999F, 18.00006F);
            this.xrLabel32.StylePriority.UseBorders = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 0.73031444813470414D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁服务其他});
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 3.0054574798513976D;
            // 
            // txt3岁服务其他
            // 
            this.txt3岁服务其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt3岁服务其他.LocationFloat = new DevExpress.Utils.PointFloat(5.326538F, 0F);
            this.txt3岁服务其他.Name = "txt3岁服务其他";
            this.txt3岁服务其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁服务其他.SizeF = new System.Drawing.SizeF(143.0778F, 18.00006F);
            this.txt3岁服务其他.StylePriority.UseBorders = false;
            // 
            // txt4岁服务
            // 
            this.txt4岁服务.Name = "txt4岁服务";
            this.txt4岁服务.Weight = 1.5008341056845209D;
            // 
            // txt5岁服务
            // 
            this.txt5岁服务.Name = "txt5岁服务";
            this.txt5岁服务.Weight = 1.5032648283379522D;
            // 
            // txt6岁服务
            // 
            this.txt6岁服务.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt6岁服务.Name = "txt6岁服务";
            this.txt6岁服务.StylePriority.UseBorders = false;
            this.txt6岁服务.Weight = 1.5000002102851926D;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell245});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.StylePriority.UseBorders = false;
            this.xrTableRow58.Weight = 0.80000000000000027D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell235.StylePriority.UsePadding = false;
            this.xrTableCell235.StylePriority.UseTextAlignment = false;
            this.xrTableCell235.Text = "下次随访日期";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell235.Weight = 1.5000000670750944D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁下次随访});
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Weight = 1.4959010937930211D;
            // 
            // txt3岁下次随访
            // 
            this.txt3岁下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt3岁下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.4100189F, 0.625F);
            this.txt3岁下次随访.Name = "txt3岁下次随访";
            this.txt3岁下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt3岁下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁下次随访});
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Weight = 1.5008341056845209D;
            // 
            // txt4岁下次随访
            // 
            this.txt4岁下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt4岁下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.4099121F, 0.625F);
            this.txt4岁下次随访.Name = "txt4岁下次随访";
            this.txt4岁下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt4岁下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁下次随访});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 1.5032648283379522D;
            // 
            // txt5岁下次随访
            // 
            this.txt5岁下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt5岁下次随访.LocationFloat = new DevExpress.Utils.PointFloat(0.3265076F, 0.625F);
            this.txt5岁下次随访.Name = "txt5岁下次随访";
            this.txt5岁下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁下次随访.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt5岁下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁下次随访});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 1.5000002102851926D;
            // 
            // txt6岁下次随访
            // 
            this.txt6岁下次随访.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt6岁下次随访.LocationFloat = new DevExpress.Utils.PointFloat(1.409912F, 0.624939F);
            this.txt6岁下次随访.Name = "txt6岁下次随访";
            this.txt6岁下次随访.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁下次随访.SizeF = new System.Drawing.SizeF(147F, 17.00006F);
            this.txt6岁下次随访.StylePriority.UseBorders = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.StylePriority.UseBorders = false;
            this.xrTableRow59.Weight = 0.80000000000000027D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell246.StylePriority.UsePadding = false;
            this.xrTableCell246.StylePriority.UseTextAlignment = false;
            this.xrTableCell246.Text = "随访医生签名";
            this.xrTableCell246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell246.Weight = 1.5000000670750944D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt3岁随访医生});
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Weight = 1.4959010937930211D;
            // 
            // txt3岁随访医生
            // 
            this.txt3岁随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt3岁随访医生.LocationFloat = new DevExpress.Utils.PointFloat(0.4100482F, 0.6249745F);
            this.txt3岁随访医生.Name = "txt3岁随访医生";
            this.txt3岁随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt3岁随访医生.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt3岁随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt4岁随访医生});
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 1.5008341056845209D;
            // 
            // txt4岁随访医生
            // 
            this.txt4岁随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt4岁随访医生.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.625F);
            this.txt4岁随访医生.Name = "txt4岁随访医生";
            this.txt4岁随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt4岁随访医生.SizeF = new System.Drawing.SizeF(149.347F, 19.37463F);
            this.txt4岁随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt5岁随访医生});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Weight = 1.5032648283379522D;
            // 
            // txt5岁随访医生
            // 
            this.txt5岁随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt5岁随访医生.LocationFloat = new DevExpress.Utils.PointFloat(2.146466F, 0.6250699F);
            this.txt5岁随访医生.Name = "txt5岁随访医生";
            this.txt5岁随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt5岁随访医生.SizeF = new System.Drawing.SizeF(148.1799F, 19.375F);
            this.txt5岁随访医生.StylePriority.UseBorders = false;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell250.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt6岁随访医生});
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Weight = 1.5000002102851926D;
            // 
            // txt6岁随访医生
            // 
            this.txt6岁随访医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt6岁随访医生.LocationFloat = new DevExpress.Utils.PointFloat(1.409973F, 0.6252441F);
            this.txt6岁随访医生.Name = "txt6岁随访医生";
            this.txt6岁随访医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt6岁随访医生.SizeF = new System.Drawing.SizeF(146.9999F, 17F);
            this.txt6岁随访医生.StylePriority.UseBorders = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.BorderWidth = 2F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(449.375F, 83.66667F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseBorderWidth = false;
            this.xrLabel12.StylePriority.UseFont = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.BorderWidth = 2F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(475.4167F, 83.66667F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseBorderWidth = false;
            this.xrLabel11.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 2F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(502.5F, 83.66667F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderWidth = 2F;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(521.0417F, 83.67004F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 12.58F);
            this.xrLine1.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(541.0417F, 83.66667F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 2F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(567.0834F, 83.66667F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.BorderWidth = 2F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(593.125F, 83.66667F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseBorderWidth = false;
            this.xrLabel7.StylePriority.UseFont = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.BorderWidth = 2F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(620.2084F, 83.66667F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseBorderWidth = false;
            this.xrLabel6.StylePriority.UseFont = false;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 2F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(648.7499F, 83.66667F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(15.62512F, 12.58332F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(382.5F, 81.25F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(50.83496F, 20F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "编号";
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(131.25F, 81.25F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(152.08F, 20F);
            this.txt姓名.StylePriority.UseFont = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(48.95833F, 81.25F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(65.63F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("楷体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(171.875F, 35.41667F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(319.1668F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "3~6岁以内儿童健康检查记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 23F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 16F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report3_6岁儿童健康检查记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(44, 33, 23, 16);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRLabel txt3岁随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRLabel txt4岁随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel txt5岁随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel txt6岁随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel txt3岁体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRLabel txt4岁体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel txt5岁体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRLabel txt6岁体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRLabel txt3岁身长;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRLabel txt4岁身长;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRLabel txt5岁身长;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRLabel txt6岁身长;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell txt满月头围;
        private DevExpress.XtraReports.UI.XRTableCell txt3月头围;
        private DevExpress.XtraReports.UI.XRTableCell txt6月头围;
        private DevExpress.XtraReports.UI.XRTableCell txt8月头围;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁视力;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁视力;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁视力;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell txt3岁龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁龋齿数;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁其他;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁其他;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRLabel txt3岁转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel txt3岁转诊机构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRLabel txt4岁转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRLabel txt4岁转诊机构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel txt5岁转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRLabel txt5岁转诊机构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRLabel txt6岁转诊原因;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRLabel txt6岁转诊机构;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁服务;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁服务;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁服务;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRLabel txt3岁下次随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRLabel txt4岁下次随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRLabel txt5岁下次随访;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRLabel txt6岁下次随访;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRLabel txt3岁随访医生;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRLabel txt4岁随访医生;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRLabel txt5岁随访医生;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRLabel txt6岁随访医生;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine37;
        private DevExpress.XtraReports.UI.XRLine xrLine38;
        private DevExpress.XtraReports.UI.XRLine xrLine39;
        private DevExpress.XtraReports.UI.XRLine xrLine40;
        private DevExpress.XtraReports.UI.XRLine xrLine41;
        private DevExpress.XtraReports.UI.XRLine xrLine42;
        private DevExpress.XtraReports.UI.XRLine xrLine43;
        private DevExpress.XtraReports.UI.XRLine xrLine44;
        private DevExpress.XtraReports.UI.XRLine xrLine45;
        private DevExpress.XtraReports.UI.XRLine xrLine46;
        private DevExpress.XtraReports.UI.XRLine xrLine47;
        private DevExpress.XtraReports.UI.XRLine xrLine48;
        private DevExpress.XtraReports.UI.XRLine xrLine49;
        private DevExpress.XtraReports.UI.XRLine xrLine50;
        private DevExpress.XtraReports.UI.XRLine xrLine51;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁龋齿数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRLabel txt3岁血红;
        private DevExpress.XtraReports.UI.XRLabel txt4岁血红;
        private DevExpress.XtraReports.UI.XRLabel txt5岁血红;
        private DevExpress.XtraReports.UI.XRLabel txt6岁血红;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell txt3岁其他;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRLabel txt4岁肺炎;
        private DevExpress.XtraReports.UI.XRLabel txt4岁腹泻;
        private DevExpress.XtraReports.UI.XRLabel txt4岁外伤;
        private DevExpress.XtraReports.UI.XRLabel txt4岁两次其他;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell txt3岁无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRLabel txt3岁肺炎;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRLabel txt3岁腹泻;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel txt3岁外伤;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel txt3岁两次其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRLabel txt5岁肺炎;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel txt5岁腹泻;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRLabel txt5岁外伤;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel txt5岁两次其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRLabel txt6岁肺炎;
        private DevExpress.XtraReports.UI.XRLabel txt6岁腹泻;
        private DevExpress.XtraReports.UI.XRLabel txt6岁外伤;
        private DevExpress.XtraReports.UI.XRLabel txt6岁两次其他;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRLabel txt3岁服务其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRLabel txt3岁转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt4岁转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt5岁转诊建议;
        private DevExpress.XtraReports.UI.XRLabel txt6岁转诊建议;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRLabel txt3岁体格发育;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRLabel txt4岁体格发育;
        private DevExpress.XtraReports.UI.XRLabel txt5岁体格发育;
        private DevExpress.XtraReports.UI.XRLabel txt6岁体格发育;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRLabel txt3岁听力;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRLabel txt3岁心肺;
        private DevExpress.XtraReports.UI.XRLabel txt3岁腹部;
        private DevExpress.XtraReports.UI.XRLabel txt4岁心肺;
        private DevExpress.XtraReports.UI.XRLabel txt5岁心肺;
        private DevExpress.XtraReports.UI.XRLabel txt6岁心肺;
        private DevExpress.XtraReports.UI.XRLabel txt4岁腹部;
        private DevExpress.XtraReports.UI.XRLabel txt5岁腹部;
        private DevExpress.XtraReports.UI.XRLabel txt6岁腹部;
        private DevExpress.XtraReports.UI.XRLabel txt3岁预防;
        private DevExpress.XtraReports.UI.XRLabel txt4岁疾病;
        private DevExpress.XtraReports.UI.XRLabel txt5岁疾病;
        private DevExpress.XtraReports.UI.XRLabel txt6岁预防;
        private DevExpress.XtraReports.UI.XRLabel txt3岁合理;
        private DevExpress.XtraReports.UI.XRLabel txt3岁低盐;
        private DevExpress.XtraReports.UI.XRLabel txt3岁生长;
        private DevExpress.XtraReports.UI.XRLabel txt3岁口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt3岁疾病;
        private DevExpress.XtraReports.UI.XRLabel txt4岁合理;
        private DevExpress.XtraReports.UI.XRLabel txt4岁低盐;
        private DevExpress.XtraReports.UI.XRLabel txt4岁生长;
        private DevExpress.XtraReports.UI.XRLabel txt4岁口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt4岁预防;
        private DevExpress.XtraReports.UI.XRLabel txt5岁低盐;
        private DevExpress.XtraReports.UI.XRLabel txt5岁合理;
        private DevExpress.XtraReports.UI.XRLabel txt5岁口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt5岁生长;
        private DevExpress.XtraReports.UI.XRLabel txt5岁预防;
        private DevExpress.XtraReports.UI.XRLabel txt6岁合理;
        private DevExpress.XtraReports.UI.XRLabel txt6岁低盐;
        private DevExpress.XtraReports.UI.XRLabel txt6岁生长;
        private DevExpress.XtraReports.UI.XRLabel txt6岁口腔保健;
        private DevExpress.XtraReports.UI.XRLabel txt6岁疾病;
        private DevExpress.XtraReports.UI.XRTableCell txt3岁牙齿数目;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell txt4岁牙齿数目;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell txt5岁牙齿数目;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell txt6岁牙齿数目;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
    }
}
