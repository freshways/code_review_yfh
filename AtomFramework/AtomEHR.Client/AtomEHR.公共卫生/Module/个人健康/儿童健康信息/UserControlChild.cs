﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UserControlChild : UserControlBase
    {
        public UserControlChild()
        {
            InitializeComponent();
        }
        public void CalQXandWZD(DevExpress.XtraLayout.LayoutControlGroup group)
        {
            int iAllCount = 0;
            _base缺项 = 0;
            _base完整度 = 0;
            for (int index = 0; index < group.Items.Count; index++)
            {
                if ((group.Items[index].Tag != null) && (group.Items[index].Tag.ToString() == "check"))
                {
                    iAllCount++;
                    CalItem(group.Items[index] as DevExpress.XtraLayout.LayoutControlItem, ref _base缺项);
                }
            }
            if (iAllCount == 0)
            {
                iAllCount = 1;
            }

            _base完整度 = (iAllCount - _base缺项) * 100 / iAllCount;
        }

        private void CalItem(DevExpress.XtraLayout.LayoutControlItem item, ref int paramQX)
        {
            Control innerCtrl = item.Control;
            if ((innerCtrl is DevExpress.XtraEditors.ComboBoxEdit)
                || (innerCtrl is DevExpress.XtraEditors.TextEdit)
                || (innerCtrl is DevExpress.XtraEditors.DateEdit)
                || (innerCtrl is DevExpress.XtraEditors.LookUpEdit))
            {
                if (string.IsNullOrWhiteSpace(innerCtrl.Text))
                {
                    paramQX++;
                }
            }
            else if (innerCtrl is UserControl)
            {
                if (innerCtrl is Library.UserControls.UCTxtLbl)
                {
                    Library.UserControls.UCTxtLbl temp = innerCtrl as Library.UserControls.UCTxtLbl;
                    if (string.IsNullOrWhiteSpace(temp.Txt1.Text))
                    {
                        paramQX++;
                    }
                }
                else if (innerCtrl is Library.UserControls.UCTxtLblTxtLbl)
                {
                    Library.UserControls.UCTxtLblTxtLbl temp = innerCtrl as Library.UserControls.UCTxtLblTxtLbl;
                    if (string.IsNullOrWhiteSpace(temp.Txt1.Text)
                        || string.IsNullOrWhiteSpace(temp.Txt2.Text))
                    {
                        paramQX++;
                    }
                }
            }

            else if (innerCtrl is FlowLayoutPanel)
            {
                bool isExistCheckBox = false;


                for (int index = 0; index < innerCtrl.Controls.Count; index++)
                {
                    if (innerCtrl.Controls[index] is DevExpress.XtraEditors.CheckEdit)
                    {
                        isExistCheckBox = true;
                        break;
                    }
                }

                if (isExistCheckBox)
                {
                    //找出checkBox其他 和 textEdit其他两个
                    DevExpress.XtraEditors.CheckEdit check其他 = null;
                    DevExpress.XtraEditors.TextEdit text其他 = null;
                    int indexCheck其他 = -1;
                    int indexText其他 = -1;
                    for (int index = 0; index < innerCtrl.Controls.Count; index++)
                    {
                        if ((innerCtrl.Controls[index] is DevExpress.XtraEditors.CheckEdit)
                            && (innerCtrl.Controls[index].Name.Contains("其他")))
                        {
                            indexCheck其他 = index;
                            check其他 = innerCtrl.Controls[index] as DevExpress.XtraEditors.CheckEdit;
                        }
                        else if (innerCtrl.Controls[index] is DevExpress.XtraEditors.TextEdit)
                        {
                            indexText其他 = index;
                            text其他 = innerCtrl.Controls[index] as DevExpress.XtraEditors.TextEdit;
                        }
                    }

                    if (check其他 != null && text其他 != null && check其他.Checked && string.IsNullOrWhiteSpace(text其他.Text))
                    {
                        paramQX++;
                    }
                    else
                    {
                        bool isNOTQX = false;
                        for (int index = 0; index < innerCtrl.Controls.Count; index++)
                        {
                            if (innerCtrl.Controls[index] is DevExpress.XtraEditors.CheckEdit)
                            {
                                DevExpress.XtraEditors.CheckEdit temp = innerCtrl.Controls[index] as DevExpress.XtraEditors.CheckEdit;
                                isNOTQX = isNOTQX || temp.Checked;
                                if (isNOTQX)
                                {
                                    break;
                                }
                            }
                        }
                        if (isNOTQX)
                        {
                        }
                        else
                        {
                            paramQX++;
                        }
                    }
                }
                else
                {
                    #region 不包含CheckBox的情况下

                    //判断是否包含
                    Library.UserControls.UCTxtLblTxtLbl uctemp = null;
                    DevExpress.XtraEditors.ComboBoxEdit comTemp = null;
                    //DevExpress.XtraEditors.TextEdit textTemp = null;
                    for (int index = 0; index < innerCtrl.Controls.Count; index++)
                    {
                        if (innerCtrl.Controls[index] is Library.UserControls.UCTxtLblTxtLbl)
                        {
                            uctemp = innerCtrl.Controls[index] as Library.UserControls.UCTxtLblTxtLbl;
                        }
                        else if(innerCtrl.Controls[index] is DevExpress.XtraEditors.ComboBoxEdit)
                        {
                            comTemp = innerCtrl.Controls[index] as DevExpress.XtraEditors.ComboBoxEdit;
                        }
                        //else if(innerCtrl.Controls[index] is DevExpress.XtraEditors.TextEdit)
                        //{
                        //    textTemp = innerCtrl.Controls[index] as DevExpress.XtraEditors.TextEdit;
                        //}
                    }

                    if (uctemp!=null && comTemp!=null)
                    {
                        //针对“前囟”的判断
                        if (("未闭".Equals(comTemp.Text)) && (string.IsNullOrWhiteSpace(uctemp.Txt1.Text) || string.IsNullOrWhiteSpace(uctemp.Txt2.Text)))
                        {
                            paramQX++;
                        }
                    }
                    else
                    {
                        for (int index = 0; index < innerCtrl.Controls.Count; index++)
                        {
                            if (innerCtrl.Controls[index] is Library.UserControls.UCTxtLbl)
                            {
                                Library.UserControls.UCTxtLbl temp = innerCtrl.Controls[index] as Library.UserControls.UCTxtLbl;
                                if (string.IsNullOrWhiteSpace(temp.Txt1.Text))
                                {
                                    paramQX++;
                                    break;
                                }
                            }
                            else if (innerCtrl.Controls[index] is Library.UserControls.UCTxtLblTxtLbl)
                            {
                                Library.UserControls.UCTxtLblTxtLbl temp = innerCtrl.Controls[index] as Library.UserControls.UCTxtLblTxtLbl;
                                if (string.IsNullOrWhiteSpace(temp.Txt1.Text) || string.IsNullOrWhiteSpace(temp.Txt2.Text))
                                {
                                    paramQX++;
                                    break;
                                }
                            }
                            else if (innerCtrl.Controls[index] is DevExpress.XtraEditors.ComboBoxEdit)
                            {
                                if (string.IsNullOrWhiteSpace(innerCtrl.Controls[index].Text))
                                {
                                    paramQX++;
                                    break;
                                }
                            }
                        }
                    }

                    
                    #endregion
                }
            }
            else if (innerCtrl is Panel)
            {
                int comIndex = -1;
                for (int index = 0; index < innerCtrl.Controls.Count; index++)
                {
                    if (innerCtrl.Controls[index] is DevExpress.XtraEditors.ComboBoxEdit)
                    {
                        comIndex = index;
                        break;
                    }
                }
                if (string.IsNullOrWhiteSpace(innerCtrl.Controls[comIndex].Text))
                {
                    paramQX++;
                }
                else if (innerCtrl.Controls[comIndex].Text == "有")
                {
                    for (int index = 0; index < innerCtrl.Controls.Count; index++)
                    {
                        if (innerCtrl.Controls[index] is Library.UserControls.UCLblTxt)
                        {
                            Library.UserControls.UCLblTxt temp = innerCtrl.Controls[index] as Library.UserControls.UCLblTxt;
                            if (string.IsNullOrWhiteSpace(temp.Txt1.Text))
                            {
                                paramQX++;
                                break;
                            }
                        }
                    }
                }
            }

        }
    
    }
}
