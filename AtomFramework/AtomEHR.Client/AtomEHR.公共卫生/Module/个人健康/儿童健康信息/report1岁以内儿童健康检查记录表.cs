﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class report1岁以内儿童健康检查记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        DataSet _ds满月;
        DataSet _ds3月;
        DataSet _ds6月;
        DataSet _ds8月;
        //bll老年人随访 _bll = new bll老年人随访();
        bll儿童_健康检查_满月 _bll满月 = new bll儿童_健康检查_满月();
        bll儿童_健康检查_3月 _bll3月 = new bll儿童_健康检查_3月();
        bll儿童_健康检查_6月 _bll6月 = new bll儿童_健康检查_6月();
        bll儿童_健康检查_8月 _bll8月 = new bll儿童_健康检查_8月();

        public report1岁以内儿童健康检查记录表()
        {
            InitializeComponent();
        }

        public report1岁以内儿童健康检查记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds满月 = _bll满月.GetInfoByFangshi(_docNo, true);
            _ds3月 = _bll3月.GetInfoByFangshi(_docNo, true);
            _ds6月 = _bll6月.GetInfoByFangshi(_docNo, true);
            _ds8月 = _bll8月.GetInfoByFangshi(_docNo, true);

            DoBindingDataSource(_ds满月, _ds3月, _ds6月, _ds8月);
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="_ds满月"></param>
        /// <param name="_ds3月"></param>
        /// <param name="_ds6月"></param>
        /// <param name="_ds8月"></param>
        private void DoBindingDataSource(DataSet _ds满月, DataSet _ds3月, DataSet _ds6月, DataSet _ds8月)
        {
            //throw new NotImplementedException();
            DataTable dt满月 = _ds满月.Tables[Models.tb_儿童_健康检查_满月.__TableName];
            if (dt满月 == null || dt满月.Rows.Count == 0) return;

            DataTable dt3月 = _ds3月.Tables[Models.tb_儿童_健康检查_3月.__TableName];


            DataTable dt6月 = _ds6月.Tables[Models.tb_儿童_健康检查_6月.__TableName];


            DataTable dt8月 = _ds8月.Tables[Models.tb_儿童_健康检查_8月.__TableName];


            DataTable dt健康档案 = _ds满月.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;
            //满月数据
            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt满月随访.Text = Convert.ToDateTime(dt满月.Rows[0][tb_儿童_健康检查_满月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt满月体重.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.体重].ToString();
            this.txt满月身长.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.身长].ToString();
            this.txt满月头围.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.头围].ToString();

            this.txt满月面色.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.面色].ToString();
            if (txt满月面色.Text == "2")
            {
                this.txt满月面色.Text = "1";
            }
            else if (txt满月面色.Text == "3")
            {
                this.txt满月面色.Text = "2";
            }
            else if (txt满月面色.Text == "99")
            {
                this.txt满月面色.Text = "3";
            }
            this.txt满月皮肤.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.皮肤].ToString();
            this.txt满月闭合.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.前囟].ToString();

            if (txt满月闭合.Text == "99")
            {
                this.txt满月闭合.Text = "2";
            }

            this.txt满月颈部.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.颈部包块].ToString();
            
            this.txt满月眼外观.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.眼部].ToString();
            this.txt满月耳外观.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.耳部].ToString();
            this.txt满月口腔.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.口腔].ToString();
            this.txt满月心肺.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.心肺].ToString();
            this.txt满月腹部.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.腹部].ToString();
            this.txt满月脐部.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.脐部].ToString();
            this.txt满月四肢.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.四肢].ToString();
            this.txt满月佝偻体症.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.佝偻病体征].ToString();
            this.txt满月肛门.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.生殖器].ToString();
            this.txt满月发育.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.发育评估].ToString();
            this.txt满月两次随访.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.两次随访间患病情况].ToString();
            string txt指导 = dt满月.Rows[0][tb_儿童_健康检查_满月.指导].ToString();

            string[] arr=txt指导.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr.Length; i++)
            {
                switch (arr[i])
                {
                    case "1":
                        this.txt满月科学.Text = arr[i];
                        break;
                    case "5":
                        this.txt满月生长.Text = "2";
                        break;
                    case "6":
                        this.txt满月疾病.Text = "3";
                        break;
                    case "7":
                        this.txt满月预防.Text = "4";
                        break;
                    case "8":
                        this.txt满月口腔保健.Text = "5";
                        break;
                    case "100":
                        this.txt满月低盐.Text = "6";
                        break;
                    default:
                        break;
                }
            }



            this.txt满月前囟.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.前囟值].ToString();
            this.txt满月后囟.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.后囟值].ToString();
            this.txt满月血红.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.血红蛋白值].ToString();
            this.txt满月户外.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.户外活动].ToString();
            this.txt满月维生素.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.服用维生素].ToString();
            this.txt满月其他.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.其他].ToString();
            this.txt满月转诊建议.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.转诊状况].ToString();
            this.txt满月转诊原因.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.转诊原因].ToString();
            this.txt满月转诊机构.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.转诊机构].ToString();
            this.txt满月下次随访.Text = Convert.ToDateTime(dt满月.Rows[0][tb_儿童_健康检查_满月.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt满月随访医生.Text = dt满月.Rows[0][tb_儿童_健康检查_满月.随访医生].ToString();
            //3月数据
            if (dt3月 != null && dt3月.Rows.Count > 0)
            {
                this.txt3月随访.Text = Convert.ToDateTime(dt3月.Rows[0][tb_儿童_健康检查_3月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt3月体重.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.体重].ToString();
                this.txt3月身长.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.身长].ToString();
                this.txt3月头围.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.头围].ToString();

                this.txt3月面色.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.面色].ToString();
                if (txt3月面色.Text == "2")
                {
                    this.txt3月面色.Text = "1";
                }
                else if (txt3月面色.Text == "3")
                {
                    this.txt3月面色.Text = "2";
                }
                else if (txt3月面色.Text == "99")
                {
                    this.txt3月面色.Text = "3";
                }
                this.txt3月皮肤.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.皮肤].ToString();
                this.txt3月闭合.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.前囟].ToString();

                if (txt3月闭合.Text == "99")
                {
                    this.txt3月闭合.Text = "2";
                }

                this.txt3月颈部.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.颈部包块].ToString();
                this.txt3月眼外观.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.眼部].ToString();
                this.txt3月耳外观.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.耳部].ToString();
                this.txt3月口腔.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.口腔].ToString();
                this.txt3月心肺.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.心肺].ToString();
                this.txt3月腹部.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.腹部].ToString();
                this.txt3月脐部.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.脐部].ToString();
                this.txt3月四肢.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.四肢].ToString();
                this.txt3月佝偻.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.佝偻病症状].ToString();
                this.txt3月佝偻体症.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.佝偻病体征].ToString();
                this.txt3月肛门.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.生殖器].ToString();
                this.txt3月发育.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.发育评估].ToString();
                this.txt3月两次随访.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.两次随访间患病情况].ToString();
                string txt3月指导 = dt3月.Rows[0][tb_儿童_健康检查_3月.指导].ToString();

                string[] arr3月 = txt3月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr3月.Length; i++)
                {
                    switch (arr3月[i])
                    {
                        case "1":
                            this.txt3月科学.Text = arr3月[i];
                            break;  
                        case "5":   
                            this.txt3月生长.Text = "2";
                            break;  
                        case "6":   
                            this.txt3月疾病.Text = "3";
                            break;  
                        case "7":   
                            this.txt3月预防.Text = "4";
                            break;  
                        case "8":   
                            this.txt3月口腔保健.Text = "5";
                            break;  
                        case "100":   
                            this.txt3月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt3月前囟.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.前囟值].ToString();
                this.txt3月后囟.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.后囟值].ToString();
                this.txt3月血红.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.血红蛋白值].ToString();
                this.txt3月户外.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.户外活动].ToString();
                this.txt3月维生素.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.服用维生素].ToString();
                this.txt3月其他.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.其他].ToString();
                this.txt3月转诊建议.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.转诊状况].ToString();
                this.txt3月转诊原因.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.转诊原因].ToString();
                this.txt3月转诊机构.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.转诊机构].ToString();
                this.txt3月下次随访.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.下次随访日期].ToString();
                this.txt3月随访医生.Text = dt3月.Rows[0][tb_儿童_健康检查_3月.随访医生].ToString();
            }
            //6月数据
            if (dt6月 != null && dt6月.Rows.Count > 0)
            {
                this.txt6月随访.Text = Convert.ToDateTime(dt6月.Rows[0][tb_儿童_健康检查_6月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt6月体重.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.体重].ToString();
                this.txt6月身长.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.身长].ToString();
                this.txt6月头围.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.头围].ToString();

                this.txt6月面色.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.面色].ToString();
                if (txt6月面色.Text == "2")
                {
                    this.txt6月面色.Text = "1";
                }
                else if (txt6月面色.Text == "3")
                {
                    this.txt6月面色.Text = "2";
                }
                else if (txt6月面色.Text == "99")
                {
                    this.txt6月面色.Text = "3";
                }
                this.txt6月皮肤.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.皮肤].ToString();
                this.txt6月闭合.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.前囟].ToString();

                if (txt6月闭合.Text == "99")
                {
                    this.txt6月闭合.Text = "2";
                }

                this.txt6月颈部.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.颈部包块].ToString();
                this.txt6月眼外观.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.眼部].ToString();
                this.txt6月耳外观.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.耳部].ToString();
                this.txt6月听力.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.听力].ToString();
                this.txt6月出牙数.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.出牙数].ToString();
                this.txt6月心肺.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.心肺].ToString();
                this.txt6月腹部.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.腹部].ToString();                
                this.txt6月四肢.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.四肢].ToString();
                this.txt6月佝偻.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.佝偻病症状].ToString();
                this.txt6月佝偻体症.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.佝偻病体征].ToString();
                this.txt6月肛门.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.生殖器].ToString();
                this.txt6月发育.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.发育评估].ToString();
                this.txt6月两次随访.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.两次随访间患病情况].ToString();
                string txt6月指导 = dt6月.Rows[0][tb_儿童_健康检查_6月.指导].ToString();
                
                string[] arr6月 = txt6月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr6月.Length; i++)
                {
                    switch (arr6月[i])
                    {
                        case "1":
                            this.txt6月科学.Text = arr6月[i];
                            break;
                        case "5":
                            this.txt6月生长.Text = "2";
                            break;
                        case "6":
                            this.txt6月疾病.Text = "3";
                            break;
                        case "7":
                            this.txt6月预防.Text = "4";
                            break;
                        case "8":
                            this.txt6月口腔保健.Text = "5";
                            break;
                        case "100":
                            this.txt6月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt6月前囟.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.前囟值].ToString();
                this.txt6月后囟.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.后囟值].ToString();
                this.txt6月血红.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.血红蛋白值].ToString();
                this.txt6月户外.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.户外活动].ToString();
                this.txt6月维生素.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.服用维生素].ToString();
                this.txt6月转诊建议.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.转诊状况].ToString();
                this.txt6月转诊原因.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.转诊原因].ToString();
                this.txt6月转诊机构.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.转诊机构].ToString();
                this.txt6月下次随访.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.下次随访日期].ToString();
                this.txt6月随访医生.Text = dt6月.Rows[0][tb_儿童_健康检查_6月.随访医生].ToString();
            }
            //8月数据
            if (dt8月 != null && dt8月.Rows.Count > 0)
            {
                this.txt8月随访.Text = Convert.ToDateTime(dt8月.Rows[0][tb_儿童_健康检查_8月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt8月体重.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.体重].ToString();
                this.txt8月身长.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.身长].ToString();
                this.txt8月头围.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.头围].ToString();

                this.txt8月面色.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.面色].ToString();
                if (txt8月面色.Text == "2")
                {
                    this.txt8月面色.Text = "1";
                }
                else if (txt8月面色.Text == "3")
                {
                    this.txt8月面色.Text = "2";
                }
                else if (txt8月面色.Text == "99")
                {
                    this.txt8月面色.Text = "3";
                }
                this.txt8月皮肤.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.皮肤].ToString();
                this.txt8月闭合.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.前囟].ToString();

                if (txt8月闭合.Text == "99")
                {
                    this.txt8月闭合.Text = "2";
                }

                //this.txt8月颈部.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.颈部包块].ToString();
                this.txt8月眼外观.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.眼部].ToString();
                this.txt8月耳外观.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.耳部].ToString();
                //this.txt8月听力.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.听力].ToString();
                this.txt8月出牙数.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.出牙数].ToString();
                this.txt8月心肺.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.心肺].ToString();
                this.txt8月腹部.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.腹部].ToString();
                this.txt8月四肢.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.四肢].ToString();
                this.txt8月佝偻.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.佝偻病症状].ToString();
                this.txt8月佝偻体症.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.佝偻病体征].ToString();
                this.txt8月肛门.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.生殖器].ToString();
                this.txt8月发育.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.发育评估].ToString();
                this.txt8月两次随访.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.两次随访间患病情况].ToString();
                string txt8月指导 = dt8月.Rows[0][tb_儿童_健康检查_8月.指导].ToString();
                string[] arr8月 = txt8月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr8月.Length; i++)
                {
                    switch (arr8月[i])
                    {
                        case "1":
                            this.txt8月科学.Text = arr8月[i];
                            break;
                        case "5":
                            this.txt8月生长.Text = "2";
                            break;
                        case "6":
                            this.txt8月疾病.Text = "3";
                            break;
                        case "7":
                            this.txt8月预防.Text = "4";
                            break;
                        case "8":
                            this.txt8月口腔保健.Text = "5";
                            break;
                        case "100":
                            this.txt8月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt8月前囟.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.前囟值].ToString();
                this.txt8月后囟.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.后囟值].ToString();
                this.txt8月血红.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.血红蛋白值].ToString();
                this.txt8月户外.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.户外活动].ToString();
                this.txt8月维生素.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.服用维生素].ToString();
                this.txt8月其他.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.其他].ToString();
                this.txt8月转诊建议.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.转诊状况].ToString();
                this.txt8月转诊原因.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.转诊原因].ToString();
                this.txt8月转诊机构.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.转诊机构].ToString();
                this.txt8月下次随访.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.下次随访日期].ToString();
                this.txt8月随访医生.Text = dt8月.Rows[0][tb_儿童_健康检查_8月.随访医生].ToString();
            }
        }
    }


}