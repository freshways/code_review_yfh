﻿namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    partial class report新生儿家庭访视记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身份证号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家庭住址 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt父亲姓名1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt父亲姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt父亲职业 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt父电话 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt父出生日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt母亲姓名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt母亲职业 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt母电话 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt母出生日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt孕周 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt妊娠其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt妊娠情况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt助产机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生情况其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt新生儿窒息 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt有畸形 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt畸形 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt其他代谢病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt疾病甲低 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt疾病苯丙酮 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt目前体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生身长 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt喂养方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt吃奶量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt吃奶次数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt呕吐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt大便 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt大便次数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt体温 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脉率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt呼吸频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt面色 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt黄疸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt前囟1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt前囟2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt前囟其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt四肢活动度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt眼外观异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt眼外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt四肢活动度异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt前囟状态 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt耳外观异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt耳外观 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt颈部包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt颈部包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt鼻异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt鼻 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt皮肤其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt口腔异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt口腔 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肛门异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt肛门 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心肺听诊异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心肺听诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt外生殖器异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt外生殖器 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腹部触诊异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt腹部触诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脊柱异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脊柱 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脐带其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt脐带 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt转诊建议 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt机构及科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt喂养 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt发育 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt防病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt预防 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt口腔保健 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt本次访视日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访地点 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt姓名,
            this.xrLine1,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable1});
            this.Detail.HeightF = 990.625F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // txt姓名
            // 
            this.txt姓名.CanGrow = false;
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(119.5197F, 87.38F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(580.2083F, 87.37501F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(12.5F, 15.00003F);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(525F, 87.37501F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(553.125F, 87.37501F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(628.7501F, 87.38F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(655.915F, 87.38F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(681F, 87.38F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(703.9982F, 87.37504F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(603.125F, 87.37504F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(498.545F, 87.37501F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(453.125F, 86.37502F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(45.42F, 20F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "编号";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(49.58332F, 86.37502F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(69.93332F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "姓名：";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(216.25F, 37.45834F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(237.5F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "新生儿家庭访视记录表";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 127.0833F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30});
            this.xrTable1.SizeF = new System.Drawing.SizeF(723.9982F, 853.5417F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell5,
            this.xrTableCell2,
            this.xrTableCell4,
            this.txt出生日期});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.81499772123641256D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "姓   别";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.76592998326483341D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "0 未知的性别  1 男  2 女  \r\n9  未说明的性别";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 2.7306209788210536D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt姓别});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 0.847708404843374D;
            // 
            // txt姓别
            // 
            this.txt姓别.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt姓别.CanGrow = false;
            this.txt姓别.LocationFloat = new DevExpress.Utils.PointFloat(64.79F, 16F);
            this.txt姓别.Name = "txt姓别";
            this.txt姓别.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt姓别.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt姓别.StylePriority.UseBorders = false;
            this.txt姓别.StylePriority.UsePadding = false;
            this.txt姓别.StylePriority.UseTextAlignment = false;
            this.txt姓别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "出 生 日 期";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 1.1177796505055833D;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.txt出生日期.StylePriority.UseBorders = false;
            this.txt出生日期.StylePriority.UsePadding = false;
            this.txt出生日期.StylePriority.UseTextAlignment = false;
            this.txt出生日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt出生日期.Weight = 1.4696113786161158D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.txt身份证号,
            this.xrTableCell9,
            this.txt家庭住址});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.40749886061820628D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "身份证号";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.76592998326483341D;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.StylePriority.UseBorders = false;
            this.txt身份证号.Weight = 2.9260923249138404D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "家庭住址";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.65223762574627941D;
            // 
            // txt家庭住址
            // 
            this.txt家庭住址.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家庭住址.Name = "txt家庭住址";
            this.txt家庭住址.StylePriority.UseBorders = false;
            this.txt家庭住址.Weight = 2.5873904621260086D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell17,
            this.txt父亲姓名1,
            this.xrTableCell12,
            this.txt父亲职业,
            this.xrTableCell16,
            this.txt父电话,
            this.xrTableCell15,
            this.txt父出生日期});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.40749886061820628D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "父亲";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.38695421205955116D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "姓名";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.37897577476679933D;
            // 
            // txt父亲姓名1
            // 
            this.txt父亲姓名1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt父亲姓名1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt父亲姓名});
            this.txt父亲姓名1.Name = "txt父亲姓名1";
            this.txt父亲姓名1.StylePriority.UseBorders = false;
            this.txt父亲姓名1.Weight = 0.64425899111233365D;
            // 
            // txt父亲姓名
            // 
            this.txt父亲姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt父亲姓名.CanGrow = false;
            this.txt父亲姓名.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.815887F);
            this.txt父亲姓名.Name = "txt父亲姓名";
            this.txt父亲姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt父亲姓名.SizeF = new System.Drawing.SizeF(67.29169F, 20F);
            this.txt父亲姓名.StylePriority.UseBorders = false;
            this.txt父亲姓名.StylePriority.UsePadding = false;
            this.txt父亲姓名.StylePriority.UseTextAlignment = false;
            this.txt父亲姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "职业";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.43501661863086749D;
            // 
            // txt父亲职业
            // 
            this.txt父亲职业.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt父亲职业.Name = "txt父亲职业";
            this.txt父亲职业.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt父亲职业.StylePriority.UseBorders = false;
            this.txt父亲职业.StylePriority.UsePadding = false;
            this.txt父亲职业.Weight = 1.8468168325998777D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "联系电话";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.65223676325867752D;
            // 
            // txt父电话
            // 
            this.txt父电话.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt父电话.Name = "txt父电话";
            this.txt父电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt父电话.StylePriority.UseBorders = false;
            this.txt父电话.StylePriority.UsePadding = false;
            this.txt父电话.StylePriority.UseTextAlignment = false;
            this.txt父电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt父电话.Weight = 0.95142941108032519D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "出生日期";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.72404446282938473D;
            // 
            // txt父出生日期
            // 
            this.txt父出生日期.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt父出生日期.Name = "txt父出生日期";
            this.txt父出生日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt父出生日期.StylePriority.UseBorders = false;
            this.txt父出生日期.StylePriority.UsePadding = false;
            this.txt父出生日期.StylePriority.UseTextAlignment = false;
            this.txt父出生日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt父出生日期.Weight = 0.91191732971314443D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.txt母亲姓名,
            this.xrTableCell22,
            this.txt母亲职业,
            this.xrTableCell24,
            this.txt母电话,
            this.xrTableCell26,
            this.txt母出生日期});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseTextAlignment = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow4.Weight = 0.40749886061820628D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Text = "母亲";
            this.xrTableCell19.Weight = 0.38695421205955116D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Text = "姓名";
            this.xrTableCell20.Weight = 0.37897577476679933D;
            // 
            // txt母亲姓名
            // 
            this.txt母亲姓名.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt母亲姓名.Name = "txt母亲姓名";
            this.txt母亲姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt母亲姓名.StylePriority.UseBorders = false;
            this.txt母亲姓名.StylePriority.UsePadding = false;
            this.txt母亲姓名.StylePriority.UseTextAlignment = false;
            this.txt母亲姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt母亲姓名.Weight = 0.64425895458994076D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "职业";
            this.xrTableCell22.Weight = 0.43501647254129583D;
            // 
            // txt母亲职业
            // 
            this.txt母亲职业.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt母亲职业.Name = "txt母亲职业";
            this.txt母亲职业.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt母亲职业.StylePriority.UseBorders = false;
            this.txt母亲职业.StylePriority.UsePadding = false;
            this.txt母亲职业.StylePriority.UseTextAlignment = false;
            this.txt母亲职业.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt母亲职业.Weight = 1.8468170152118422D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.Text = "联系电话";
            this.xrTableCell24.Weight = 0.65223676325867752D;
            // 
            // txt母电话
            // 
            this.txt母电话.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt母电话.Name = "txt母电话";
            this.txt母电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt母电话.StylePriority.UseBorders = false;
            this.txt母电话.StylePriority.UsePadding = false;
            this.txt母电话.Weight = 0.95142875367725321D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.Text = "出生日期";
            this.xrTableCell26.Weight = 0.72404504718767115D;
            // 
            // txt母出生日期
            // 
            this.txt母出生日期.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt母出生日期.Name = "txt母出生日期";
            this.txt母出生日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt母出生日期.StylePriority.UseBorders = false;
            this.txt母出生日期.StylePriority.UsePadding = false;
            this.txt母出生日期.StylePriority.UseTextAlignment = false;
            this.txt母出生日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt母出生日期.Weight = 0.91191740275793021D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell30,
            this.xrTableCell32,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow5.Weight = 0.40749886061820628D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.Text = "出生孕周";
            this.xrTableCell28.Weight = 0.67018853901696973D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt孕周});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Weight = 0.5935958276304385D;
            // 
            // txt孕周
            // 
            this.txt孕周.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt孕周.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.txt孕周.Name = "txt孕周";
            this.txt孕周.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt孕周.SizeF = new System.Drawing.SizeF(62F, 24F);
            this.txt孕周.StylePriority.UseBorders = false;
            this.txt孕周.StylePriority.UsePadding = false;
            this.txt孕周.StylePriority.UseTextAlignment = false;
            this.txt孕周.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell32.CanGrow = false;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "周";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.14640523415513507D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "母亲妊娠期患病情况  1 糖尿病  2 妊娠期高血压  3 其他";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell34.Weight = 4.5078163568291245D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt妊娠其他});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Weight = 0.76788183975713142D;
            // 
            // txt妊娠其他
            // 
            this.txt妊娠其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt妊娠其他.CanGrow = false;
            this.txt妊娠其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt妊娠其他.Name = "txt妊娠其他";
            this.txt妊娠其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.txt妊娠其他.SizeF = new System.Drawing.SizeF(77.58F, 24F);
            this.txt妊娠其他.StylePriority.UseBorders = false;
            this.txt妊娠其他.StylePriority.UsePadding = false;
            this.txt妊娠其他.StylePriority.UseTextAlignment = false;
            this.txt妊娠其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt妊娠情况});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Weight = 0.24576259866216227D;
            // 
            // txt妊娠情况
            // 
            this.txt妊娠情况.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt妊娠情况.CanGrow = false;
            this.txt妊娠情况.LocationFloat = new DevExpress.Utils.PointFloat(2.37929F, 3.999996F);
            this.txt妊娠情况.Name = "txt妊娠情况";
            this.txt妊娠情况.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt妊娠情况.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt妊娠情况.StylePriority.UseBorders = false;
            this.txt妊娠情况.StylePriority.UsePadding = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell31,
            this.xrTableCell33,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.81499772123641256D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "助产机\r\n构名称";
            this.xrTableCell29.Weight = 0.53814579562839637D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt助产机构});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 0.8720432553550308D;
            // 
            // txt助产机构
            // 
            this.txt助产机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt助产机构.LocationFloat = new DevExpress.Utils.PointFloat(7.629395E-06F, 0F);
            this.txt助产机构.Name = "txt助产机构";
            this.txt助产机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.txt助产机构.SizeF = new System.Drawing.SizeF(91.0833F, 50F);
            this.txt助产机构.StylePriority.UseBorders = false;
            this.txt助产机构.StylePriority.UsePadding = false;
            this.txt助产机构.StylePriority.UseTextAlignment = false;
            this.txt助产机构.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Weight = 4.9059403973445557D;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow8});
            this.xrTable2.SizeF = new System.Drawing.SizeF(477.0834F, 55.06723F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Text = "出生情况 1 顺产 2 胎头吸引 3 产钳 4 剖宫 5 双多胎 6 臀位";
            this.xrTableCell41.Weight = 3D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell40});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Text = "其他";
            this.xrTableCell42.Weight = 0.30066210920371761D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生情况其他});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 2.6993378907962824D;
            // 
            // txt出生情况其他
            // 
            this.txt出生情况其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt出生情况其他.LocationFloat = new DevExpress.Utils.PointFloat(0.7488556F, 1.525879E-05F);
            this.txt出生情况其他.Name = "txt出生情况其他";
            this.txt出生情况其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt出生情况其他.SizeF = new System.Drawing.SizeF(410.2928F, 25F);
            this.txt出生情况其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生情况1});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22020489646090041D;
            // 
            // txt出生情况1
            // 
            this.txt出生情况1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt出生情况1.CanGrow = false;
            this.txt出生情况1.LocationFloat = new DevExpress.Utils.PointFloat(2.791809F, 20.21509F);
            this.txt出生情况1.Name = "txt出生情况1";
            this.txt出生情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt出生情况1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt出生情况1.StylePriority.UseBorders = false;
            this.txt出生情况1.StylePriority.UsePadding = false;
            this.txt出生情况1.StylePriority.UseTextAlignment = false;
            this.txt出生情况1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "/";
            this.xrTableCell38.Weight = 0.14955365421296604D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生情况2});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.24576239704911179D;
            // 
            // txt出生情况2
            // 
            this.txt出生情况2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt出生情况2.CanGrow = false;
            this.txt出生情况2.LocationFloat = new DevExpress.Utils.PointFloat(2.37929F, 20.21508F);
            this.txt出生情况2.Name = "txt出生情况2";
            this.txt出生情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt出生情况2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt出生情况2.StylePriority.UseBorders = false;
            this.txt出生情况2.StylePriority.UsePadding = false;
            this.txt出生情况2.StylePriority.UseTextAlignment = false;
            this.txt出生情况2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 0.81499772123641256D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Text = "新生儿窒息  1 无  2 有                                                    （Apgar 评分：1分钟 " +
    " 5分钟  不详）";
            this.xrTableCell43.Weight = 2.6458422276291262D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt新生儿窒息});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.26469181133135888D;
            // 
            // txt新生儿窒息
            // 
            this.txt新生儿窒息.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt新生儿窒息.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 22.9167F);
            this.txt新生儿窒息.Name = "txt新生儿窒息";
            this.txt新生儿窒息.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt新生儿窒息.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt新生儿窒息.StylePriority.UseBorders = false;
            this.txt新生儿窒息.StylePriority.UsePadding = false;
            this.txt新生儿窒息.StylePriority.UseTextAlignment = false;
            this.txt新生儿窒息.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Text = "是否有畸形  1 无  2 有";
            this.xrTableCell45.Weight = 1.7967757411191783D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt有畸形});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 1.9785781458774008D;
            // 
            // txt有畸形
            // 
            this.txt有畸形.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt有畸形.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt有畸形.Name = "txt有畸形";
            this.txt有畸形.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt有畸形.SizeF = new System.Drawing.SizeF(186.8301F, 50.00003F);
            this.txt有畸形.StylePriority.UseBorders = false;
            this.txt有畸形.StylePriority.UseTextAlignment = false;
            this.txt有畸形.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt畸形});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell48.Weight = 0.2457624700938976D;
            // 
            // txt畸形
            // 
            this.txt畸形.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt畸形.CanGrow = false;
            this.txt畸形.LocationFloat = new DevExpress.Utils.PointFloat(2.37929F, 22.9167F);
            this.txt畸形.Name = "txt畸形";
            this.txt畸形.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt畸形.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt畸形.StylePriority.UseBorders = false;
            this.txt畸形.StylePriority.UsePadding = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell51});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.40749886061820623D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Text = "新生儿听力筛查  1 通过  2 未通过  3 未筛查  4 不详";
            this.xrTableCell49.Weight = 6.6859778115711714D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt听力});
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.Weight = 0.24567258447979062D;
            // 
            // txt听力
            // 
            this.txt听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt听力.LocationFloat = new DevExpress.Utils.PointFloat(2.37F, 4F);
            this.txt听力.Name = "txt听力";
            this.txt听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt听力.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt听力.StylePriority.UseBorders = false;
            this.txt听力.StylePriority.UsePadding = false;
            this.txt听力.StylePriority.UseTextAlignment = false;
            this.txt听力.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell46,
            this.xrTableCell148,
            this.xrTableCell146,
            this.xrTableCell25,
            this.xrTableCell147,
            this.xrTableCell50});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 0.40749886061820623D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Text = "新生儿疾病筛查  1 通过  2 苯丙酮尿症  3 其他遗传代谢病";
            this.xrTableCell52.Weight = 4.3442595466421947D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt其他代谢病});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 1.244236393104375D;
            // 
            // txt其他代谢病
            // 
            this.txt其他代谢病.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt其他代谢病.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt其他代谢病.Name = "txt其他代谢病";
            this.txt其他代谢病.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.txt其他代谢病.SizeF = new System.Drawing.SizeF(126.88F, 24F);
            this.txt其他代谢病.StylePriority.UseBorders = false;
            this.txt其他代谢病.StylePriority.UsePadding = false;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt疾病甲低});
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.26807552859451905D;
            // 
            // txt疾病甲低
            // 
            this.txt疾病甲低.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病甲低.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt疾病甲低.Name = "txt疾病甲低";
            this.txt疾病甲低.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt疾病甲低.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病甲低.StylePriority.UseBorders = false;
            this.txt疾病甲低.StylePriority.UsePadding = false;
            this.txt疾病甲低.StylePriority.UseTextAlignment = false;
            this.txt疾病甲低.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell146.StylePriority.UsePadding = false;
            this.xrTableCell146.StylePriority.UseTextAlignment = false;
            this.xrTableCell146.Text = "/";
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell146.Weight = 0.26807553772511761D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt疾病苯丙酮});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.26807552425534104D;
            // 
            // txt疾病苯丙酮
            // 
            this.txt疾病苯丙酮.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病苯丙酮.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.txt疾病苯丙酮.Name = "txt疾病苯丙酮";
            this.txt疾病苯丙酮.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt疾病苯丙酮.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病苯丙酮.StylePriority.UseBorders = false;
            this.txt疾病苯丙酮.StylePriority.UsePadding = false;
            this.txt疾病苯丙酮.StylePriority.UseTextAlignment = false;
            this.txt疾病苯丙酮.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell147.StylePriority.UsePadding = false;
            this.xrTableCell147.StylePriority.UseTextAlignment = false;
            this.xrTableCell147.Text = "/";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell147.Weight = 0.29316613022113247D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt疾病其他});
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Weight = 0.24576173550828234D;
            // 
            // txt疾病其他
            // 
            this.txt疾病其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(2.63F, 4F);
            this.txt疾病其他.Name = "txt疾病其他";
            this.txt疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt疾病其他.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病其他.StylePriority.UseBorders = false;
            this.txt疾病其他.StylePriority.UsePadding = false;
            this.txt疾病其他.StylePriority.UseTextAlignment = false;
            this.txt疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell59,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell53,
            this.xrTableCell58,
            this.xrTableCell55,
            this.xrTableCell54});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 0.40749886061820623D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Text = "新生儿出生体重";
            this.xrTableCell60.Weight = 1.4101891052924165D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生体重});
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.801223342956729D;
            // 
            // txt出生体重
            // 
            this.txt出生体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt出生体重.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt出生体重.Name = "txt出生体重";
            this.txt出生体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt出生体重.SizeF = new System.Drawing.SizeF(82.94F, 24F);
            this.txt出生体重.StylePriority.UseBorders = false;
            this.txt出生体重.StylePriority.UsePadding = false;
            this.txt出生体重.StylePriority.UseTextAlignment = false;
            this.txt出生体重.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Text = "kg";
            this.xrTableCell56.Weight = 0.72165362317364934D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Text = "目前体重";
            this.xrTableCell57.Weight = 0.66321414907323706D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt目前体重});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.83773561491853543D;
            // 
            // txt目前体重
            // 
            this.txt目前体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt目前体重.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt目前体重.Name = "txt目前体重";
            this.txt目前体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt目前体重.SizeF = new System.Drawing.SizeF(87.5F, 24F);
            this.txt目前体重.StylePriority.UseBorders = false;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "cm";
            this.xrTableCell53.Weight = 0.48272882999009026D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Text = "出生身长";
            this.xrTableCell58.Weight = 0.75791787441986713D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生身长});
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.63946198491459416D;
            // 
            // txt出生身长
            // 
            this.txt出生身长.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt出生身长.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 3.814697E-05F);
            this.txt出生身长.Name = "txt出生身长";
            this.txt出生身长.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt出生身长.SizeF = new System.Drawing.SizeF(66.79F, 24F);
            this.txt出生身长.StylePriority.UseBorders = false;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Text = "cm";
            this.xrTableCell54.Weight = 0.61752587131184289D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 0.40749886061820623D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Text = "喂养方式 1 纯母乳 2 混合 3 人工";
            this.xrTableCell62.Weight = 2.5822217351880536D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt喂养方式});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 0.35084433623474143D;
            // 
            // txt喂养方式
            // 
            this.txt喂养方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt喂养方式.LocationFloat = new DevExpress.Utils.PointFloat(6.65F, 4F);
            this.txt喂养方式.Name = "txt喂养方式";
            this.txt喂养方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt喂养方式.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt喂养方式.StylePriority.UseBorders = false;
            this.txt喂养方式.StylePriority.UsePadding = false;
            this.txt喂养方式.StylePriority.UseTextAlignment = false;
            this.txt喂养方式.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Text = "*吃奶量";
            this.xrTableCell65.Weight = 0.66321414907323706D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt吃奶量});
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 0.83773561491853554D;
            // 
            // txt吃奶量
            // 
            this.txt吃奶量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt吃奶量.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 7.629395E-06F);
            this.txt吃奶量.Name = "txt吃奶量";
            this.txt吃奶量.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt吃奶量.SizeF = new System.Drawing.SizeF(87.5F, 24F);
            this.txt吃奶量.StylePriority.UseBorders = false;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "ml/次";
            this.xrTableCell67.Weight = 0.48272941434830891D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Text = "吃奶次数";
            this.xrTableCell68.Weight = 0.75791787441986713D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt吃奶次数});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.63946198491459416D;
            // 
            // txt吃奶次数
            // 
            this.txt吃奶次数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt吃奶次数.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt吃奶次数.Name = "txt吃奶次数";
            this.txt吃奶次数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt吃奶次数.SizeF = new System.Drawing.SizeF(66.79F, 24F);
            this.txt吃奶次数.StylePriority.UseBorders = false;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "次/日";
            this.xrTableCell70.Weight = 0.6175252869536243D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 0.40749886061820623D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Text = "*呕吐  1 无  2 有";
            this.xrTableCell63.Weight = 2.582221607359696D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt呕吐});
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.35084490515758893D;
            // 
            // txt呕吐
            // 
            this.txt呕吐.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt呕吐.LocationFloat = new DevExpress.Utils.PointFloat(6.65F, 4F);
            this.txt呕吐.Name = "txt呕吐";
            this.txt呕吐.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt呕吐.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt呕吐.StylePriority.UseBorders = false;
            this.txt呕吐.StylePriority.UsePadding = false;
            this.txt呕吐.StylePriority.UseTextAlignment = false;
            this.txt呕吐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Text = "*大便  1 糊状  2 稀";
            this.xrTableCell73.Weight = 1.7742435815644622D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt大便});
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.20943515568113019D;
            // 
            // txt大便
            // 
            this.txt大便.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt大便.LocationFloat = new DevExpress.Utils.PointFloat(6.875038F, 7.208347F);
            this.txt大便.Name = "txt大便";
            this.txt大便.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt大便.SizeF = new System.Drawing.SizeF(9.375122F, 7.791649F);
            this.txt大便.StylePriority.UseBorders = false;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "*大便次数";
            this.xrTableCell75.Weight = 0.75791787441986713D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt大便次数});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.63946140055637557D;
            // 
            // txt大便次数
            // 
            this.txt大便次数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt大便次数.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt大便次数.Name = "txt大便次数";
            this.txt大便次数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt大便次数.SizeF = new System.Drawing.SizeF(66.79F, 24F);
            this.txt大便次数.StylePriority.UseBorders = false;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Text = "次/日";
            this.xrTableCell77.Weight = 0.61752587131184289D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell71,
            this.xrTableCell78,
            this.xrTableCell85,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.40749886061820623D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "体温";
            this.xrTableCell84.Weight = 0.53814582006542377D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt体温});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 1.4058009030656564D;
            // 
            // txt体温
            // 
            this.txt体温.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt体温.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt体温.Name = "txt体温";
            this.txt体温.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt体温.SizeF = new System.Drawing.SizeF(146.52F, 24F);
            this.txt体温.StylePriority.UseBorders = false;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "℃";
            this.xrTableCell78.Weight = 0.9891197893862046D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Text = "脉率";
            this.xrTableCell85.Weight = 0.39844258855444692D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脉率});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 1.0127500424112754D;
            // 
            // txt脉率
            // 
            this.txt脉率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt脉率.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt脉率.Name = "txt脉率";
            this.txt脉率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt脉率.SizeF = new System.Drawing.SizeF(102.03F, 24F);
            this.txt脉率.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Text = "次/分钟";
            this.xrTableCell80.Weight = 0.57248610627987007D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Text = "呼吸频率";
            this.xrTableCell81.Weight = 0.75791787441986713D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt呼吸频率});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.63946140055637557D;
            // 
            // txt呼吸频率
            // 
            this.txt呼吸频率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt呼吸频率.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.txt呼吸频率.Name = "txt呼吸频率";
            this.txt呼吸频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt呼吸频率.SizeF = new System.Drawing.SizeF(66.79F, 24F);
            this.txt呼吸频率.StylePriority.UseBorders = false;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Text = "次/分钟";
            this.xrTableCell83.Weight = 0.61752587131184289D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell89,
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 0.40749886061820623D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = "面色 1 红润  2 黄染  3 其他";
            this.xrTableCell86.Weight = 2.2114124826521655D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt面色});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 1.002894958392512D;
            // 
            // txt面色
            // 
            this.txt面色.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt面色.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt面色.Name = "txt面色";
            this.txt面色.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt面色.SizeF = new System.Drawing.SizeF(104.75F, 24F);
            this.txt面色.StylePriority.UseBorders = false;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Text = "黄疸部位 1 面部 2 躯干 3 四肢 4 手足";
            this.xrTableCell91.Weight = 3.004076701555344D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt黄疸});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.71326625345094219D;
            // 
            // txt黄疸
            // 
            this.txt黄疸.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt黄疸.LocationFloat = new DevExpress.Utils.PointFloat(51.20919F, 3.999964F);
            this.txt黄疸.Name = "txt黄疸";
            this.txt黄疸.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt黄疸.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt黄疸.StylePriority.UseBorders = false;
            this.txt黄疸.StylePriority.UsePadding = false;
            this.txt黄疸.StylePriority.UseTextAlignment = false;
            this.txt黄疸.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell90,
            this.xrTableCell93});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 0.40749886061820623D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "前囟";
            this.xrTableCell94.Weight = 0.38695410562323884D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt前囟1});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.75734233105969184D;
            // 
            // txt前囟1
            // 
            this.txt前囟1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt前囟1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt前囟1.Name = "txt前囟1";
            this.txt前囟1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt前囟1.SizeF = new System.Drawing.SizeF(79.1F, 24F);
            this.txt前囟1.StylePriority.UseBorders = false;
            this.txt前囟1.StylePriority.UseTextAlignment = false;
            this.txt前囟1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "cm x";
            this.xrTableCell88.Weight = 0.47184854355389239D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt前囟2});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.82750696265367685D;
            // 
            // txt前囟2
            // 
            this.txt前囟2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt前囟2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt前囟2.Name = "txt前囟2";
            this.txt前囟2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt前囟2.SizeF = new System.Drawing.SizeF(80.24F, 24F);
            this.txt前囟2.StylePriority.UseBorders = false;
            this.txt前囟2.StylePriority.UseTextAlignment = false;
            this.txt前囟2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Text = "cm   1 正常 2 膨隆 3 凹陷 4 其他";
            this.xrTableCell96.Weight = 2.5827639448253166D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt前囟其他});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 1.6595130098078337D;
            // 
            // txt前囟其他
            // 
            this.txt前囟其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt前囟其他.LocationFloat = new DevExpress.Utils.PointFloat(10F, 1.525879E-05F);
            this.txt前囟其他.Name = "txt前囟其他";
            this.txt前囟其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt前囟其他.SizeF = new System.Drawing.SizeF(160.71F, 24F);
            this.txt前囟其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt四肢活动度});
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.24572149852731393D;
            // 
            // txt四肢活动度
            // 
            this.txt四肢活动度.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt四肢活动度.CanGrow = false;
            this.txt四肢活动度.LocationFloat = new DevExpress.Utils.PointFloat(2.375031F, 3.499921F);
            this.txt四肢活动度.Name = "txt四肢活动度";
            this.txt四肢活动度.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt四肢活动度.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt四肢活动度.StylePriority.UseBorders = false;
            this.txt四肢活动度.StylePriority.UsePadding = false;
            this.txt四肢活动度.StylePriority.UseTextAlignment = false;
            this.txt四肢活动度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell103});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 0.40749886061820623D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.Text = "眼外观   1 未见异常 2 异常";
            this.xrTableCell99.Weight = 2.189077388693248D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt眼外观异常});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Weight = 0.78109591202210582D;
            // 
            // txt眼外观异常
            // 
            this.txt眼外观异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt眼外观异常.LocationFloat = new DevExpress.Utils.PointFloat(1.583984F, 0F);
            this.txt眼外观异常.Name = "txt眼外观异常";
            this.txt眼外观异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt眼外观异常.SizeF = new System.Drawing.SizeF(80F, 23.5F);
            this.txt眼外观异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt眼外观});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.24413376299906847D;
            // 
            // txt眼外观
            // 
            this.txt眼外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt眼外观.CanGrow = false;
            this.txt眼外观.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt眼外观.Name = "txt眼外观";
            this.txt眼外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt眼外观.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt眼外观.StylePriority.UseBorders = false;
            this.txt眼外观.StylePriority.UsePadding = false;
            this.txt眼外观.StylePriority.UseTextAlignment = false;
            this.txt眼外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Text = "四肢活动度 1 未见异常 2 异常";
            this.xrTableCell97.Weight = 2.34466897461357D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt四肢活动度异常});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 1.1496920770299051D;
            // 
            // txt四肢活动度异常
            // 
            this.txt四肢活动度异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt四肢活动度异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt四肢活动度异常.Name = "txt四肢活动度异常";
            this.txt四肢活动度异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt四肢活动度异常.SizeF = new System.Drawing.SizeF(117.71F, 24F);
            this.txt四肢活动度异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt前囟状态});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.22298228069306725D;
            // 
            // txt前囟状态
            // 
            this.txt前囟状态.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt前囟状态.CanGrow = false;
            this.txt前囟状态.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.500112F);
            this.txt前囟状态.Name = "txt前囟状态";
            this.txt前囟状态.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt前囟状态.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt前囟状态.StylePriority.UseBorders = false;
            this.txt前囟状态.StylePriority.UsePadding = false;
            this.txt前囟状态.StylePriority.UseTextAlignment = false;
            this.txt前囟状态.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.40749886061820623D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Text = "耳外观   1 未见异常 2 异常";
            this.xrTableCell102.Weight = 2.189077388693248D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt耳外观异常});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 0.78109532766381939D;
            // 
            // txt耳外观异常
            // 
            this.txt耳外观异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt耳外观异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt耳外观异常.Name = "txt耳外观异常";
            this.txt耳外观异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt耳外观异常.SizeF = new System.Drawing.SizeF(81.58398F, 23.49994F);
            this.txt耳外观异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt耳外观});
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.24413434735735481D;
            // 
            // txt耳外观
            // 
            this.txt耳外观.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt耳外观.CanGrow = false;
            this.txt耳外观.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt耳外观.Name = "txt耳外观";
            this.txt耳外观.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt耳外观.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt耳外观.StylePriority.UseBorders = false;
            this.txt耳外观.StylePriority.UsePadding = false;
            this.txt耳外观.StylePriority.UseTextAlignment = false;
            this.txt耳外观.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "颈部包块   1 无       2 有";
            this.xrTableCell106.Weight = 2.34466897461357D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt颈部包块有});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 1.1270014447708527D;
            // 
            // txt颈部包块有
            // 
            this.txt颈部包块有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt颈部包块有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt颈部包块有.Name = "txt颈部包块有";
            this.txt颈部包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt颈部包块有.SizeF = new System.Drawing.SizeF(117.71F, 24F);
            this.txt颈部包块有.StylePriority.UseBorders = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt颈部包块});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.24567291295211957D;
            // 
            // txt颈部包块
            // 
            this.txt颈部包块.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt颈部包块.CanGrow = false;
            this.txt颈部包块.LocationFloat = new DevExpress.Utils.PointFloat(2.370008F, 3.499889F);
            this.txt颈部包块.Name = "txt颈部包块";
            this.txt颈部包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt颈部包块.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt颈部包块.StylePriority.UseBorders = false;
            this.txt颈部包块.StylePriority.UsePadding = false;
            this.txt颈部包块.StylePriority.UseTextAlignment = false;
            this.txt颈部包块.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 0.40749886061820623D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Text = "鼻       1 未见异常 2 异常";
            this.xrTableCell109.Weight = 2.189077388693248D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt鼻异常});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.78109532766381939D;
            // 
            // txt鼻异常
            // 
            this.txt鼻异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt鼻异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt鼻异常.Name = "txt鼻异常";
            this.txt鼻异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt鼻异常.SizeF = new System.Drawing.SizeF(81.58398F, 23.5F);
            this.txt鼻异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt鼻});
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 0.24413434735735481D;
            // 
            // txt鼻
            // 
            this.txt鼻.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt鼻.CanGrow = false;
            this.txt鼻.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt鼻.Name = "txt鼻";
            this.txt鼻.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt鼻.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt鼻.StylePriority.UseBorders = false;
            this.txt鼻.StylePriority.UsePadding = false;
            this.txt鼻.StylePriority.UseTextAlignment = false;
            this.txt鼻.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Text = "皮肤  1 未见异常 2 湿疹 3 糜烂 4 其他";
            this.xrTableCell112.Weight = 3.0040763192458115D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt皮肤其他});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.46759410013861114D;
            // 
            // txt皮肤其他
            // 
            this.txt皮肤其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt皮肤其他.CanGrow = false;
            this.txt皮肤其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt皮肤其他.Name = "txt皮肤其他";
            this.txt皮肤其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt皮肤其他.SizeF = new System.Drawing.SizeF(48.83F, 24F);
            this.txt皮肤其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt皮肤});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.24567291295211957D;
            // 
            // txt皮肤
            // 
            this.txt皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt皮肤.CanGrow = false;
            this.txt皮肤.LocationFloat = new DevExpress.Utils.PointFloat(2.370008F, 3.500016F);
            this.txt皮肤.Name = "txt皮肤";
            this.txt皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt皮肤.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt皮肤.StylePriority.UseBorders = false;
            this.txt皮肤.StylePriority.UsePadding = false;
            this.txt皮肤.StylePriority.UseTextAlignment = false;
            this.txt皮肤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 0.40749886061820623D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Text = "口腔     1 未见异常 2 异常";
            this.xrTableCell115.Weight = 2.189077388693248D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt口腔异常});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.78109532766381939D;
            // 
            // txt口腔异常
            // 
            this.txt口腔异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt口腔异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt口腔异常.Name = "txt口腔异常";
            this.txt口腔异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt口腔异常.SizeF = new System.Drawing.SizeF(81.58398F, 23.91663F);
            this.txt口腔异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt口腔});
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.24413434735735481D;
            // 
            // txt口腔
            // 
            this.txt口腔.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt口腔.CanGrow = false;
            this.txt口腔.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt口腔.Name = "txt口腔";
            this.txt口腔.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt口腔.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt口腔.StylePriority.UseBorders = false;
            this.txt口腔.StylePriority.UsePadding = false;
            this.txt口腔.StylePriority.UseTextAlignment = false;
            this.txt口腔.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "肛门     1 未见异常 2 异常";
            this.xrTableCell118.Weight = 2.34466897461357D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肛门异常});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 1.1270014447708527D;
            // 
            // txt肛门异常
            // 
            this.txt肛门异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt肛门异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt肛门异常.Name = "txt肛门异常";
            this.txt肛门异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt肛门异常.SizeF = new System.Drawing.SizeF(117.71F, 24F);
            this.txt肛门异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt肛门});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.24567291295211957D;
            // 
            // txt肛门
            // 
            this.txt肛门.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肛门.CanGrow = false;
            this.txt肛门.LocationFloat = new DevExpress.Utils.PointFloat(2.37F, 3.5F);
            this.txt肛门.Name = "txt肛门";
            this.txt肛门.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt肛门.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt肛门.StylePriority.UseBorders = false;
            this.txt肛门.StylePriority.UsePadding = false;
            this.txt肛门.StylePriority.UseTextAlignment = false;
            this.txt肛门.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell126});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.40749886061820623D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Text = "心肺听诊 1 未见异常 2 异常";
            this.xrTableCell121.Weight = 2.189077388693248D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心肺听诊异常});
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.78109532766381939D;
            // 
            // txt心肺听诊异常
            // 
            this.txt心肺听诊异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt心肺听诊异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt心肺听诊异常.Name = "txt心肺听诊异常";
            this.txt心肺听诊异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt心肺听诊异常.SizeF = new System.Drawing.SizeF(81.58398F, 23.50006F);
            this.txt心肺听诊异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心肺听诊});
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.24413434735735481D;
            // 
            // txt心肺听诊
            // 
            this.txt心肺听诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心肺听诊.CanGrow = false;
            this.txt心肺听诊.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt心肺听诊.Name = "txt心肺听诊";
            this.txt心肺听诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt心肺听诊.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt心肺听诊.StylePriority.UseBorders = false;
            this.txt心肺听诊.StylePriority.UsePadding = false;
            this.txt心肺听诊.StylePriority.UseTextAlignment = false;
            this.txt心肺听诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "外生殖器 1 未见异常 2 异常";
            this.xrTableCell124.Weight = 2.34466897461357D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt外生殖器异常});
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 1.1270014447708527D;
            // 
            // txt外生殖器异常
            // 
            this.txt外生殖器异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外生殖器异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt外生殖器异常.Name = "txt外生殖器异常";
            this.txt外生殖器异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外生殖器异常.SizeF = new System.Drawing.SizeF(117.7084F, 23.50006F);
            this.txt外生殖器异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell126.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt外生殖器});
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Weight = 0.24567291295211957D;
            // 
            // txt外生殖器
            // 
            this.txt外生殖器.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt外生殖器.CanGrow = false;
            this.txt外生殖器.LocationFloat = new DevExpress.Utils.PointFloat(2.37F, 3.5F);
            this.txt外生殖器.Name = "txt外生殖器";
            this.txt外生殖器.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt外生殖器.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt外生殖器.StylePriority.UseBorders = false;
            this.txt外生殖器.StylePriority.UsePadding = false;
            this.txt外生殖器.StylePriority.UseTextAlignment = false;
            this.txt外生殖器.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.40749886061820623D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Text = "腹部触诊 1 未见异常 2 异常";
            this.xrTableCell127.Weight = 2.189077388693248D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt腹部触诊异常});
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.78109532766381939D;
            // 
            // txt腹部触诊异常
            // 
            this.txt腹部触诊异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt腹部触诊异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt腹部触诊异常.Name = "txt腹部触诊异常";
            this.txt腹部触诊异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt腹部触诊异常.SizeF = new System.Drawing.SizeF(81.58401F, 26F);
            this.txt腹部触诊异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt腹部触诊});
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 0.24413434735735484D;
            // 
            // txt腹部触诊
            // 
            this.txt腹部触诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt腹部触诊.CanGrow = false;
            this.txt腹部触诊.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3.5F);
            this.txt腹部触诊.Name = "txt腹部触诊";
            this.txt腹部触诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt腹部触诊.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt腹部触诊.StylePriority.UseBorders = false;
            this.txt腹部触诊.StylePriority.UsePadding = false;
            this.txt腹部触诊.StylePriority.UseTextAlignment = false;
            this.txt腹部触诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.Text = "脊柱     1 未见异常 2 异常";
            this.xrTableCell130.Weight = 2.34466897461357D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脊柱异常});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 1.1269546961079446D;
            // 
            // txt脊柱异常
            // 
            this.txt脊柱异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt脊柱异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.txt脊柱异常.Name = "txt脊柱异常";
            this.txt脊柱异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt脊柱异常.SizeF = new System.Drawing.SizeF(117.7084F, 26F);
            this.txt脊柱异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脊柱});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.24571966161502773D;
            // 
            // txt脊柱
            // 
            this.txt脊柱.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脊柱.CanGrow = false;
            this.txt脊柱.LocationFloat = new DevExpress.Utils.PointFloat(2.37F, 3.5F);
            this.txt脊柱.Name = "txt脊柱";
            this.txt脊柱.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脊柱.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt脊柱.StylePriority.UseBorders = false;
            this.txt脊柱.StylePriority.UsePadding = false;
            this.txt脊柱.StylePriority.UseTextAlignment = false;
            this.txt脊柱.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell135,
            this.xrTableCell136});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 0.40749886061820623D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Text = "脐带     1 未脱 2 脱落 3 脐部有渗出 4 其他";
            this.xrTableCell133.Weight = 3.4965507075612878D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脐带其他});
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 3.1893798009831911D;
            // 
            // txt脐带其他
            // 
            this.txt脐带其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt脐带其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt脐带其他.Name = "txt脐带其他";
            this.txt脐带其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt脐带其他.SizeF = new System.Drawing.SizeF(294.29F, 24F);
            this.txt脐带其他.StylePriority.UseBorders = false;
            this.txt脐带其他.StylePriority.UseTextAlignment = false;
            this.txt脐带其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt脐带});
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.Weight = 0.24571988750648527D;
            // 
            // txt脐带
            // 
            this.txt脐带.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt脐带.CanGrow = false;
            this.txt脐带.LocationFloat = new DevExpress.Utils.PointFloat(2.37F, 3.5F);
            this.txt脐带.Name = "txt脐带";
            this.txt脐带.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt脐带.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt脐带.StylePriority.UseBorders = false;
            this.txt脐带.StylePriority.UsePadding = false;
            this.txt脐带.StylePriority.UseTextAlignment = false;
            this.txt脐带.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell134,
            this.xrTableCell138});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.40749886061820628D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Text = "转诊建议\r\n";
            this.xrTableCell137.Weight = 1.0033043796145165D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "1 有  2 无";
            this.xrTableCell134.Weight = 5.68262555593809D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Weight = 0.24572046049835822D;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.40749886061820628D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Text = "原因：";
            this.xrTableCell139.Weight = 1.0033045987488483D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt原因});
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 5.6826247524454718D;
            // 
            // txt原因
            // 
            this.txt原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt原因.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.txt原因.Name = "txt原因";
            this.txt原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt原因.SizeF = new System.Drawing.SizeF(274.62F, 24F);
            this.txt原因.StylePriority.UseBorders = false;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell141.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt转诊建议});
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.24572104485664456D;
            // 
            // txt转诊建议
            // 
            this.txt转诊建议.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt转诊建议.CanGrow = false;
            this.txt转诊建议.LocationFloat = new DevExpress.Utils.PointFloat(2.38F, 3.5F);
            this.txt转诊建议.Name = "txt转诊建议";
            this.txt转诊建议.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt转诊建议.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt转诊建议.StylePriority.UseBorders = false;
            this.txt转诊建议.StylePriority.UsePadding = false;
            this.txt转诊建议.StylePriority.UseTextAlignment = false;
            this.txt转诊建议.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 0.40749886061820628D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Text = "机构及科室：";
            this.xrTableCell142.Weight = 1.0033044526592936D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt机构及科室});
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 5.682673400272793D;
            // 
            // txt机构及科室
            // 
            this.txt机构及科室.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt机构及科室.LocationFloat = new DevExpress.Utils.PointFloat(2.288818E-05F, 0F);
            this.txt机构及科室.Name = "txt机构及科室";
            this.txt机构及科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt机构及科室.SizeF = new System.Drawing.SizeF(274.62F, 24F);
            this.txt机构及科室.StylePriority.UseBorders = false;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.24567254311887735D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell21,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell7,
            this.xrTableCell18,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell23,
            this.xrTableCell153});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.StylePriority.UseBorders = false;
            this.xrTableRow28.Weight = 0.40749886061820628D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UsePadding = false;
            this.xrTableCell145.Text = "指导 1 喂养指导 2 发育指导 3 防病指导 4 预防指导 5 口腔保健指导";
            this.xrTableCell145.Weight = 5.0407772314382644D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt喂养});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.21063077323229107D;
            // 
            // txt喂养
            // 
            this.txt喂养.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt喂养.CanGrow = false;
            this.txt喂养.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.txt喂养.Name = "txt喂养";
            this.txt喂养.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt喂养.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt喂养.StylePriority.UseBorders = false;
            this.txt喂养.StylePriority.UsePadding = false;
            this.txt喂养.StylePriority.UseTextAlignment = false;
            this.txt喂养.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "/";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell13.Weight = 0.21063077323229112D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt发育});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 0.21063077323229112D;
            // 
            // txt发育
            // 
            this.txt发育.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt发育.CanGrow = false;
            this.txt发育.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.txt发育.Name = "txt发育";
            this.txt发育.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt发育.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt发育.StylePriority.UseBorders = false;
            this.txt发育.StylePriority.UsePadding = false;
            this.txt发育.StylePriority.UseTextAlignment = false;
            this.txt发育.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "/";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell7.Weight = 0.2106307732322911D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt防病});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.21063077323229107D;
            // 
            // txt防病
            // 
            this.txt防病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt防病.CanGrow = false;
            this.txt防病.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.txt防病.Name = "txt防病";
            this.txt防病.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt防病.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt防病.StylePriority.UseBorders = false;
            this.txt防病.StylePriority.UsePadding = false;
            this.txt防病.StylePriority.UseTextAlignment = false;
            this.txt防病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "/";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell10.Weight = 0.21063077323229107D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt预防});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.21063077323229112D;
            // 
            // txt预防
            // 
            this.txt预防.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt预防.CanGrow = false;
            this.txt预防.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.txt预防.Name = "txt预防";
            this.txt预防.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt预防.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt预防.StylePriority.UseBorders = false;
            this.txt预防.StylePriority.UsePadding = false;
            this.txt预防.StylePriority.UseTextAlignment = false;
            this.txt预防.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "/";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell23.Weight = 0.19626958184088073D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt口腔保健});
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.22018817014578121D;
            // 
            // txt口腔保健
            // 
            this.txt口腔保健.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt口腔保健.CanGrow = false;
            this.txt口腔保健.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.5F);
            this.txt口腔保健.Name = "txt口腔保健";
            this.txt口腔保健.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txt口腔保健.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt口腔保健.StylePriority.UseBorders = false;
            this.txt口腔保健.StylePriority.UsePadding = false;
            this.txt口腔保健.StylePriority.UseTextAlignment = false;
            this.txt口腔保健.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell157,
            this.xrTableCell171,
            this.xrTableCell164});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.Weight = 0.40749886061820628D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Text = "本次访视日期";
            this.xrTableCell155.Weight = 1.0033046170100568D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt本次访视日期});
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 2.3608049143518479D;
            // 
            // txt本次访视日期
            // 
            this.txt本次访视日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt本次访视日期.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 1.525879E-05F);
            this.txt本次访视日期.Name = "txt本次访视日期";
            this.txt本次访视日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt本次访视日期.SizeF = new System.Drawing.SizeF(246.58F, 24F);
            this.txt本次访视日期.StylePriority.UseBorders = false;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.Text = "下次随该地点";
            this.xrTableCell171.Weight = 1.0699059769699053D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell164.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt下次随访地点});
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Weight = 2.4976348877191539D;
            // 
            // txt下次随访地点
            // 
            this.txt下次随访地点.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt下次随访地点.LocationFloat = new DevExpress.Utils.PointFloat(9.999969F, 1.525879E-05F);
            this.txt下次随访地点.Name = "txt下次随访地点";
            this.txt下次随访地点.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt下次随访地点.SizeF = new System.Drawing.SizeF(248.75F, 24F);
            this.txt下次随访地点.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell172,
            this.xrTableCell170});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 0.40749886061820628D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Text = "下次随访日期";
            this.xrTableCell162.Weight = 1.0033046170100568D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt下次随访日期});
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 2.3608049143518479D;
            // 
            // txt下次随访日期
            // 
            this.txt下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 1.525879E-05F);
            this.txt下次随访日期.Name = "txt下次随访日期";
            this.txt下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt下次随访日期.SizeF = new System.Drawing.SizeF(246.58F, 24F);
            this.txt下次随访日期.StylePriority.UseBorders = false;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Text = "随访医生签名";
            this.xrTableCell172.Weight = 1.0699068535073071D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell170.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访医生签名});
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Weight = 2.4976340111817521D;
            // 
            // txt随访医生签名
            // 
            this.txt随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(10F, 1.525879E-05F);
            this.txt随访医生签名.Name = "txt随访医生签名";
            this.txt随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt随访医生签名.SizeF = new System.Drawing.SizeF(248.75F, 24F);
            this.txt随访医生签名.StylePriority.UseBorders = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 19F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 23F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report新生儿家庭访视记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(64, 39, 19, 23);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell txt身份证号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell txt家庭住址;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell txt父亲姓名1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell txt父出生日期;
        private DevExpress.XtraReports.UI.XRTableCell txt父电话;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell txt父亲职业;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell txt母亲姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell txt母亲职业;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell txt母电话;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell txt母出生日期;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRLabel txt孕周;
        private DevExpress.XtraReports.UI.XRLabel txt妊娠其他;
        private DevExpress.XtraReports.UI.XRLabel txt妊娠情况;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRLabel txt助产机构;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRLabel txt出生情况其他;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRLabel txt新生儿窒息;
        private DevExpress.XtraReports.UI.XRLabel txt有畸形;
        private DevExpress.XtraReports.UI.XRLabel txt畸形;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRLabel txt听力;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRLabel txt其他代谢病;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRLabel txt出生体重;
        private DevExpress.XtraReports.UI.XRLabel txt目前体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRLabel txt出生身长;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRLabel txt喂养方式;
        private DevExpress.XtraReports.UI.XRLabel txt吃奶量;
        private DevExpress.XtraReports.UI.XRLabel txt吃奶次数;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel txt呕吐;
        private DevExpress.XtraReports.UI.XRLabel txt大便;
        private DevExpress.XtraReports.UI.XRLabel txt大便次数;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRLabel txt体温;
        private DevExpress.XtraReports.UI.XRLabel txt脉率;
        private DevExpress.XtraReports.UI.XRLabel txt呼吸频率;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRLabel txt面色;
        private DevExpress.XtraReports.UI.XRLabel txt黄疸;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRLabel txt前囟1;
        private DevExpress.XtraReports.UI.XRLabel txt前囟2;
        private DevExpress.XtraReports.UI.XRLabel txt前囟其他;
        private DevExpress.XtraReports.UI.XRLabel txt前囟状态;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRLabel txt眼外观异常;
        private DevExpress.XtraReports.UI.XRLabel txt四肢活动度异常;
        private DevExpress.XtraReports.UI.XRLabel txt四肢活动度;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRLabel txt眼外观;
        private DevExpress.XtraReports.UI.XRLabel txt耳外观异常;
        private DevExpress.XtraReports.UI.XRLabel txt耳外观;
        private DevExpress.XtraReports.UI.XRLabel txt颈部包块有;
        private DevExpress.XtraReports.UI.XRLabel txt颈部包块;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRLabel txt鼻异常;
        private DevExpress.XtraReports.UI.XRLabel txt鼻;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRLabel txt皮肤其他;
        private DevExpress.XtraReports.UI.XRLabel txt皮肤;
        private DevExpress.XtraReports.UI.XRLabel txt口腔异常;
        private DevExpress.XtraReports.UI.XRLabel txt口腔;
        private DevExpress.XtraReports.UI.XRLabel txt肛门异常;
        private DevExpress.XtraReports.UI.XRLabel txt肛门;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRLabel txt心肺听诊异常;
        private DevExpress.XtraReports.UI.XRLabel txt心肺听诊;
        private DevExpress.XtraReports.UI.XRLabel txt外生殖器异常;
        private DevExpress.XtraReports.UI.XRLabel txt外生殖器;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRLabel txt腹部触诊异常;
        private DevExpress.XtraReports.UI.XRLabel txt腹部触诊;
        private DevExpress.XtraReports.UI.XRLabel txt脊柱异常;
        private DevExpress.XtraReports.UI.XRLabel txt脊柱;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRLabel txt脐带其他;
        private DevExpress.XtraReports.UI.XRLabel txt脐带;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRLabel txt原因;
        private DevExpress.XtraReports.UI.XRLabel txt机构及科室;
        private DevExpress.XtraReports.UI.XRLabel txt转诊建议;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRLabel txt本次访视日期;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRLabel txt下次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRLabel txt下次随访地点;
        private DevExpress.XtraReports.UI.XRLabel txt随访医生签名;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel txt父亲姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRLabel txt姓别;
        private DevExpress.XtraReports.UI.XRLabel txt喂养;
        private DevExpress.XtraReports.UI.XRLabel txt发育;
        private DevExpress.XtraReports.UI.XRLabel txt防病;
        private DevExpress.XtraReports.UI.XRLabel txt预防;
        private DevExpress.XtraReports.UI.XRLabel txt口腔保健;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRLabel txt疾病甲低;
        private DevExpress.XtraReports.UI.XRLabel txt疾病苯丙酮;
        private DevExpress.XtraReports.UI.XRLabel txt疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRLabel txt出生情况1;
        private DevExpress.XtraReports.UI.XRLabel txt出生情况2;
    }
}
