﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class UC儿童健康检查_30月_显示 : UserControlBase
    {
        private string m_grdabh = null;
        private string m_prgid = null;

        private DataSet m_ds30月 = null;
        private bll儿童_健康检查_30月 m_bll30月 = new bll儿童_健康检查_30月();
        public UC儿童健康检查_30月_显示(string grdabh)
        {
            InitializeComponent();
            m_grdabh = grdabh;

            BindData();
        }

        #region 为控件绑定数据
        private void BindData()
        {
            BindDataForComboBox("hw_item", comboBoxEdit体重);

            BindDataForComboBox("hw_item", comboBoxEdit身长);

            BindDataForComboBox("ywyc", comboBoxEdit步态);

            BindDataForComboBox("ywyc", comboBoxEdit皮肤);

            //BindDataForComboBox("qx_qianxin", comboBoxEdit前囟);

            //BindDataForComboBox("yw_youwu", comboBoxEdit颈部包块);

            BindDataForComboBox("ywyc", comboBoxEdit眼外观);

            BindDataForComboBox("ywyc", comboBoxEdit耳外观);

            BindDataForComboBox("ywyc", comboBoxEdit心肺);

            BindDataForComboBox("ywyc", comboBoxEdit腹部);

            //BindDataForComboBox("jd_jidai", comboBoxEdit脐部);

            BindDataForComboBox("ywyc", comboBoxEdit四肢);

            //BindDataForComboBox("glbtz", comboBoxEdit可疑佝偻);

            //BindDataForComboBox("ywyc", comboBoxEdit肛门外生殖器);

            //BindDataForComboBox("myfypg", comboBoxEdit发育评估);

            //BindDataForComboBox("mylchbqk", comboBoxEdit两次随访间患病情况);

            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);

            BindDataForComboBox("ywyc", comboBoxEdit胸部);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }
        #endregion

        private void UC儿童健康检查_30月_显示_Load(object sender, EventArgs e)
        {
            ucTxtLbl体重.Txt1.Properties.ReadOnly = true;
            ucTxtLbl身长.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl头围.Txt1.Properties.ReadOnly = true;
            //ucTxtLblTxtLbl前囟.Txt1.Properties.ReadOnly = true;
            //ucTxtLblTxtLbl前囟.Txt2.Properties.ReadOnly = true;
            ucTxtLbl血红蛋白值.Txt1.Properties.ReadOnly = true;
            ucTxtLbl户外活动.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl服用维生素.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊原因.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊机构.Txt1.Properties.ReadOnly = true;
            //ucTxtLbl出牙数.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl出牙龋齿.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl出牙龋齿.Txt2.Properties.ReadOnly = true;
            ucLblTxt联系人.Txt1.Properties.ReadOnly = true;
            ucLblTxt联系方式.Txt1.Properties.ReadOnly = true;
            ucLblTxt结果.Txt1.Properties.ReadOnly = true;
            

            m_ds30月 = m_bll30月.GetBusinessByGrdabh(m_grdabh, true);

            m_prgid = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.所属机构].ToString();

            textEdit儿童档案号.Text = m_ds30月.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();

            string str姓名 = m_ds30月.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.姓名].ToString();
            textEdit儿童姓名.Text = DESEncrypt.DES解密(str姓名);
            textEdit身份证号.Text = m_ds30月.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.身份证号].ToString();

            string str性别 = m_ds30月.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.性别].ToString();
            DataRow[] dr性别 = DataDictCache.Cache.t性别.Select("P_CODE='" + str性别 + "'");
            if (dr性别.Length > 0)
            {
                textEdit性别.Text = dr性别[0]["P_DESC"].ToString();
            }
            textEdit出生日期.Text = m_ds30月.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.出生日期].ToString();

            textEdit卡号.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.卡号].ToString();
            ucTxtLbl体重.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.体重].ToString();

            string str体重选项 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.体重选项].ToString();
            util.ControlsHelper.SetComboxData(str体重选项, comboBoxEdit体重);

            ucTxtLbl身长.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.身长].ToString();
            string str身长选项 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.身长选项].ToString();
            util.ControlsHelper.SetComboxData(str身长选项, comboBoxEdit身长);

            //ucTxtLbl头围.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.头围].ToString();

            string str步态 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.步态].ToString();
            util.ControlsHelper.SetComboxData(str步态, comboBoxEdit步态);

            string str皮肤 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.皮肤].ToString();
            util.ControlsHelper.SetComboxData(str皮肤, comboBoxEdit皮肤);

            //string str前囟 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.前囟].ToString();
            //util.ControlsHelper.SetComboxData(str前囟, comboBoxEdit前囟);

            //ucTxtLblTxtLbl前囟.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.前囟值].ToString();
            //ucTxtLblTxtLbl前囟.Txt2.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.后囟值].ToString();

            //ucTxtLbl出牙数.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.出牙数].ToString();
            ucTxtLblTxtLbl出牙龋齿.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.牙齿数目].ToString();
            ucTxtLblTxtLbl出牙龋齿.Txt2.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.龋齿数].ToString();
            //string str颈部包块 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.颈部包块].ToString();
            //util.ControlsHelper.SetComboxData(str颈部包块, comboBoxEdit颈部包块);

            string str眼外观 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.眼部].ToString();
            util.ControlsHelper.SetComboxData(str眼外观, comboBoxEdit眼外观);

            string str耳外观 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.耳部].ToString();
            util.ControlsHelper.SetComboxData(str耳外观, comboBoxEdit耳外观);

            string str心肺 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.心肺].ToString();
            util.ControlsHelper.SetComboxData(str心肺, comboBoxEdit心肺);

            string str腹部 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.腹部].ToString();
            util.ControlsHelper.SetComboxData(str腹部, comboBoxEdit腹部);

            //string str脐部 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.脐部].ToString();
            //util.ControlsHelper.SetComboxData(str脐部, comboBoxEdit脐部);

            string str四肢 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.四肢].ToString();
            util.ControlsHelper.SetComboxData(str四肢, comboBoxEdit四肢);

            //string str可疑佝偻 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.佝偻病体征].ToString();
            //util.ControlsHelper.SetComboxData(str可疑佝偻, comboBoxEdit可疑佝偻);

            //string str佝偻病症状 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.佝偻病症状].ToString();
            //util.ControlsHelper.SetComboxData(str佝偻病症状, comboBoxEdit佝偻病症状);

            //string str肛门 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.生殖器].ToString();
            //util.ControlsHelper.SetComboxData(str肛门, comboBoxEdit肛门外生殖器);

            ucTxtLbl血红蛋白值.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.血红蛋白值].ToString();

            ucTxtLbl户外活动.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.户外活动].ToString();
            //ucTxtLbl服用维生素.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.服用维生素].ToString();

            //string str听力 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.佝偻病体征].ToString();
            //util.ControlsHelper.SetComboxData(str听力, comboBoxEdit听力);

            //string str发育评估 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.发育评估].ToString();
            //util.ControlsHelper.SetComboxData(str发育评估, comboBoxEdit发育评估);

            string str面色 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.面色].ToString();
            char[] c分隔符 = { ',' };
            string[] str面色Value = str面色.Split(c分隔符);
            for (int index = 0; index < str面色Value.Length; index++)
            {
                //if (str面色Value[index].Equals("1"))
                //{
                //    checkEdit面色未检.Checked = true;
                //    break;
                //}
                //else 
                if (str面色Value[index].Equals("2"))
                {
                    checkEdit面色红润.Checked = true;
                }
                //else if (str面色Value[index].Equals("3"))
                //{
                //    checkEdit面色黄染.Checked = true;
                //}
                else if (str面色Value[index].Equals("99"))
                {
                    checkEdit面色其他.Checked = true;
                }
            }

            //string str两次随访间患病情况 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.两次随访间患病情况].ToString();
            //util.ControlsHelper.SetComboxData(str两次随访间患病情况, comboBoxEdit两次随访间患病情况);

            string str转诊 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.转诊状况].ToString();
            util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);

            ucLblTxt转诊原因.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.转诊原因].ToString();
            ucLblTxt转诊机构.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.转诊机构].ToString();

            string str指导 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.指导].ToString();
            string[] str指导Values = str指导.Split(c分隔符);
            for (int index = 0; index < str指导Values.Length; index++)
            {
                switch (str指导Values[index])
                {
                    case "1":
                        checkEdit指导合理膳食.Checked = true;
                        break;
                    case "2":
                        checkEdit指导预防意外.Checked = true;
                        break;
                    case "3":
                        checkEdit指导生长发育.Checked = true;
                        break;
                    case "4":
                        checkEdit指导口腔保健.Checked = true;
                        break;
                    case "99":
                        checkEdit指导疾病预防.Checked = true;
                        break;
                    case "100":
                        checkEdit指导其他.Checked = true;
                        textEdit指导其他.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.EXSEZDQT].ToString();
                        break;
                    default:
                        break;
                }
            }

            string str中医Temp = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.中医药管理].ToString();
            string[] str中医 = str中医Temp.Split(c分隔符);
            for (int index = 0; index < str中医.Length; index++)
            {
                switch (str中医[index])
                {
                    case "1":
                        checkEdit中医饮食.Checked = true;
                        break;
                    case "2":
                        checkEdit中医起居.Checked = true;
                        break;
                    case "3":
                        checkEdit传授摩捏.Checked = true;
                        break;
                    case "4":
                        checkEdit中医其他.Checked = true;
                        textEdit中医其他.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.中医药管理其他].ToString();
                        break;
                }
            }

            #region 新版本添加
            string str胸部 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.胸部].ToString();
            util.ControlsHelper.SetComboxData(str胸部, comboBoxEdit胸部);
            #region 发育评估
            string str发育评估 = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.发育评估].ToString();
            string[] strs发育评估 = str发育评估.Split(c分隔符);
            for (int index = 0; index < strs发育评估.Length; index++)
            {
                switch (strs发育评估[index])
                {
                    case "1":
                        chk发育评估无.Checked = true;
                        break;
                    case "2":
                        chk发育评估1.Checked = true;
                        break;
                    case "3":
                        chk发育评估2.Checked = true;
                        break;
                    case "4":
                        chk发育评估3.Checked = true;
                        break;
                    case "5":
                        chk发育评估4.Checked = true;
                        break;
                }
            }
            #endregion
            #region   患病情况
            string str两次随访间患病情况 = SetFlowLayoutValues("lcsfhbqk", m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.两次随访间患病情况].ToString());
            string result = string.Empty;
            if (!string.IsNullOrEmpty(str两次随访间患病情况))
            {
                if (str两次随访间患病情况 == "无")
                {
                    result += "无";
                }
                else
                {
                    string[] a = str两次随访间患病情况.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (a[i] == "肺炎")
                        {
                            result += a[i] + "：" + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.肺炎].ToString() + "次，   ";
                        }
                        else if (a[i] == "腹泻")
                        {
                            result += a[i] + "：" + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.腹泻].ToString() + "次，   ";
                        }
                        else if (a[i] == "外伤")
                        {
                            result += a[i] + "：" + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.外伤].ToString() + "次，   ";
                        }
                        else if (a[i] == "其他")
                        {
                            result += a[i] + "：" + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.患病其他].ToString();
                        }
                        else
                        {
                            result += a[i];
                        }
                    }
                }
            }

            this.textEdit患病情况.Text = result;
            #endregion
            ucLblTxt联系人.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.联系人].ToString();
            ucLblTxt联系方式.Txt1.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.联系方式].ToString();
            string str结果 = m_bll30月.ReturnDis字典显示("sfdw", m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.结果].ToString());
            ucLblTxt结果.Txt1.Text = str结果;
            textEdit家长签名.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.家长签名].ToString();
            #endregion

            textEdit其他.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.其他].ToString();
            dateEdit下次随访日期.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.下次随访日期].ToString();
            textEdit随访医生.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.随访医生].ToString();
            dateEdit随访日期.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.发生时间].ToString();


            textEdit录入时间.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.创建时间].ToString();
            textEdit录入人.Text = _bllUser.Return用户名称(m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.创建人].ToString());
            textEdit创建机构.Text = _bll机构信息.Return机构名称(m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.创建机构].ToString());
            textEdit当前所属机构.Text = _bll机构信息.Return机构名称(m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.所属机构].ToString());
            textEdit最近修改人.Text = _bllUser.Return用户名称(m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.修改人].ToString());
            textEdit最近修改时间.Text = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.修改时间].ToString();
            
            #region 字体变色
            //Set考核项颜色_new(layoutControl1, labelControl考核项);
            ChangeColorForLayoutItem();
            #endregion

            labelControl考核项.Text = "考核项：20     缺项："
                + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.缺项].ToString()
                + " 完整度："
                + m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.完整度].ToString()
                + "% ";
        }

        private void ChangeColorForLayoutItem()
        {
            if (string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit体重.Text))
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci体重.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLbl身长.Txt1.Text) || string.IsNullOrWhiteSpace(comboBoxEdit身长.Text))
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci身长.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            //if (string.IsNullOrWhiteSpace(ucTxtLbl头围.Txt1.Text))
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci头围.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            if (//checkEdit面色未检.Checked || 
                checkEdit面色红润.Checked
                //|| checkEdit面色黄染.Checked 
                || checkEdit面色其他.Checked)
            {
                lciFace.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lciFace.AppearanceItemCaption.ForeColor = Color.Red;
            }

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);
            //SetComboBoxEditItemColor(comboBoxEdit口腔, lci口腔);
            SetComboBoxEditItemColor(comboBoxEdit皮肤, lci皮肤);

            //if (string.IsNullOrWhiteSpace(comboBoxEdit前囟.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt1.Text)
            //    || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl前囟.Txt2.Text))
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lci前囟.AppearanceItemCaption.ForeColor = Color.Blue;
            //}


            //SetComboBoxEditItemColor(comboBoxEdit颈部包块, lci颈部包块);
            SetComboBoxEditItemColor(comboBoxEdit眼外观, lciEye);
            SetComboBoxEditItemColor(comboBoxEdit耳外观, lciEar);
            SetComboBoxEditItemColor(comboBoxEdit心肺, lciHeart);

            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt1.Text) || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl出牙龋齿.Txt2.Text))
            {
                lci出牙龋齿.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lci出牙龋齿.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLbl血红蛋白值.Txt1.Text))
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXhdb.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            SetComboBoxEditItemColor(comboBoxEdit腹部, lciBelly);
            //SetComboBoxEditItemColor(comboBoxEdit脐部, lciNavel);
            SetComboBoxEditItemColor(comboBoxEdit四肢, lciFours);
            //SetComboBoxEditItemColor(comboBoxEdit佝偻病症状, lciGlbzz);
            //SetComboBoxEditItemColor(comboBoxEdit可疑佝偻, lciGlbtz);
            //SetComboBoxEditItemColor(comboBoxEdit肛门外生殖器, lciSzq);


            if (string.IsNullOrWhiteSpace(ucTxtLbl户外活动.Txt1.Text))
            {
                lciHwhd.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciHwhd.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            //SetComboBoxEditItemColor(comboBoxEdit听力, lciListening);

            //if (string.IsNullOrWhiteSpace(ucTxtLbl服用维生素.Txt1.Text))
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Red;
            //}
            //else
            //{
            //    lciFywssd.AppearanceItemCaption.ForeColor = Color.Blue;
            //}

            //SetComboBoxEditItemColor(comboBoxEdit发育评估, lciFypg);

            SetComboBoxEditItemColor(comboBoxEdit步态, lciBt);

            //SetComboBoxEditItemColor(comboBoxEdit两次随访间患病情况, lciLcsfjhbqk);

            if (string.IsNullOrWhiteSpace(comboBoxEdit转诊.Text))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (comboBoxEdit转诊.Text == "有" && (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text)
                 || string.IsNullOrWhiteSpace(ucLblTxt联系人.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt联系方式.Txt1.Text)
                 || string.IsNullOrWhiteSpace(ucLblTxt结果.Txt1.Text)
                ))
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciZz.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if(checkEdit指导其他.Checked && string.IsNullOrWhiteSpace(textEdit指导其他.Text))
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else if (checkEdit指导合理膳食.Checked || checkEdit指导生长发育.Checked
                || checkEdit指导疾病预防.Checked || checkEdit指导预防意外.Checked
                || checkEdit指导口腔保健.Checked)
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lciZd.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(textEdit其他.Text))
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciQt.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciXcsfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfys.AppearanceItemCaption.ForeColor = Color.Blue;
            }

            if (string.IsNullOrWhiteSpace(dateEdit随访日期.Text))
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lciSfrq.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            #region 新版本添加
            SetComboBoxEditItemColor(comboBoxEdit胸部, lbl胸部);

            if (chk发育评估无.Checked || chk发育评估1.Checked || chk发育评估2.Checked
                || chk发育评估3.Checked || chk发育评估4.Checked)
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl发育评估.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (!string.IsNullOrEmpty(textEdit患病情况.Text))
            {
                lbl患病情况.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                lbl患病情况.AppearanceItemCaption.ForeColor = Color.Red;
            }

            if (string.IsNullOrWhiteSpace(textEdit家长签名.Text))
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                lbl家长签名.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            #endregion
        }

        private void SetComboBoxEditItemColor(DevExpress.XtraEditors.ComboBoxEdit combo, DevExpress.XtraLayout.LayoutControlItem item)
        {
            if (string.IsNullOrWhiteSpace(combo.Text))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        //private void comboBoxEdit前囟_TextChanged(object sender, EventArgs e)
        //{
        //    string value = util.ControlsHelper.GetComboxKey(comboBoxEdit前囟);
        //    if (value == "99")
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = true;
        //    }
        //    else
        //    {
        //        ucTxtLblTxtLbl前囟.Enabled = false;
        //    }
        //}

        private void comboBoxEdit转诊_TextChanged(object sender, EventArgs e)
        {
            string value = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);

            //1是“有”的编码
            if (value != null && value.Equals("1"))
            {
                ucLblTxt转诊原因.Enabled = true;
                ucLblTxt转诊机构.Enabled = true;
                ucLblTxt联系人.Enabled = true;
                ucLblTxt联系方式.Enabled = true;
                ucLblTxt结果.Enabled = true;
            }
            else
            {
                ucLblTxt转诊原因.Enabled = false;
                ucLblTxt转诊原因.Txt1.Text = "";
                ucLblTxt转诊机构.Enabled = false;
                ucLblTxt转诊机构.Txt1.Text = "";
                ucLblTxt联系人.Enabled = false;
                ucLblTxt联系方式.Enabled = false;
                ucLblTxt结果.Enabled = false;
                ucLblTxt联系人.Txt1.Text = "";
                ucLblTxt联系方式.Txt1.Text = "";
                ucLblTxt结果.Txt1.Text = "";
            }
        }

        private void sbtnUpdate_Click(object sender, EventArgs e)
        {
            bool isallow = DataDictCache.Cache.IsAllow跨机构修改();
            if (!isallow)
            {
                if (m_prgid != Loginer.CurrentUser.所属机构)
                {
                    Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }

            UC儿童健康检查_30月 ctl = new UC儿童健康检查_30月(m_grdabh, UpdateType.Modify, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }

        private void sbtnDelete_Click(object sender, EventArgs e)
        {
            if (m_prgid != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                return;
            }

            if (Msg.AskQuestion("您是否确实要删除儿童健康检查记录表（30月）吗？"))
            { }
            else
            {
                //不删除
                return;
            }

            string strID = m_ds30月.Tables[tb_儿童_健康检查_30月.__TableName].Rows[0][tb_儿童_健康检查_30月.ID].ToString();
            bool bRet = m_bll30月.Delete(strID);

            string strCurrentServerDate = m_bll30月.ServiceDateTime;
            try
            {
                string strBirth = textEdit出生日期.Text;

                DateTime dt出生日期 = Convert.ToDateTime(strBirth);
                DateTime dt更新时间 = Convert.ToDateTime(strCurrentServerDate);
                int month = (dt更新时间.Year - dt出生日期.Year) * 12 + (dt更新时间.Month - dt出生日期.Month);

                if (month < 30 || month >= 36)
                {
                }
                else
                {
                    string strXcsfsj = dt出生日期.AddMonths(6).ToString("yyyy-MM-dd");
                    m_bll30月.UpdateRelatedInfo(m_grdabh, strXcsfsj);
                }
            }
            catch(Exception ex)
            {
                Msg.ShowException(ex);
            }

            if(bRet)
            {
                Msg.ShowInformation("删除成功！");
                UC儿童基本信息_显示 ctrl = new UC儿童基本信息_显示(Common.frm个人健康 as frm个人健康, m_grdabh);
                ctrl.Dock = DockStyle.Fill;
                ShowControl(ctrl);
            }
            else
            {
                Msg.ShowInformation("删除失败。");
            }
        }

        private void checkEdit中医其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit中医其他.Checked)
            {
                textEdit中医其他.Enabled = true;
            }
            else
            {
                textEdit中医其他.Enabled = true;
                textEdit中医其他.Text = "";
            }
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit指导其他.Checked)
            {
                textEdit指导其他.Enabled = true;
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            string _docNo = this.textEdit儿童档案号.Text.Trim();
            if (!string.IsNullOrEmpty(_docNo))
            {
                report1_2岁儿童健康检查记录表 report = new report1_2岁儿童健康检查记录表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }
    }
}
