﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class report1_2岁儿童健康检查记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        
        string docNo;
        DataSet _ds12月;
        DataSet _ds18月;
        DataSet _ds24月;
        DataSet _ds30月;
        ////bll老年人随访 _bll = new bll老年人随访();
        bll儿童_健康检查_12月 _bll12月 = new bll儿童_健康检查_12月();
        bll儿童_健康检查_18月 _bll18月 = new bll儿童_健康检查_18月();
        bll儿童_健康检查_24月 _bll24月 = new bll儿童_健康检查_24月();
        bll儿童_健康检查_30月 _bll30月 = new bll儿童_健康检查_30月();

        public report1_2岁儿童健康检查记录表()
        {
            InitializeComponent();
        }

        public report1_2岁儿童健康检查记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds12月 = _bll12月.GetInfoByFangshi(_docNo, true);
            _ds18月 = _bll18月.GetInfoByFangshi(_docNo, true);
            _ds24月 = _bll24月.GetInfoByFangshi(_docNo, true);
            _ds30月 = _bll30月.GetInfoByFangshi(_docNo, true);

            DoBindingDataSource(_ds12月,_ds18月,_ds24月,_ds30月);
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="_ds12月"></param>
        /// <param name="_ds18月"></param>
        /// <param name="_ds24月"></param>
        /// <param name="_ds30月"></param>
        private void DoBindingDataSource(DataSet _ds12月,DataSet _ds18月,DataSet _ds24月,DataSet _ds30月)
        {
            //throw new NotImplementedException();
            DataTable dt12月 = _ds12月.Tables[Models.tb_儿童_健康检查_12月.__TableName];
            if (dt12月 == null || dt12月.Rows.Count == 0) return;

            DataTable dt18月 = _ds18月.Tables[Models.tb_儿童_健康检查_18月.__TableName];


            DataTable dt24月 = _ds24月.Tables[Models.tb_儿童_健康检查_24月.__TableName];


            DataTable dt30月 = _ds30月.Tables[Models.tb_儿童_健康检查_30月.__TableName];


            DataTable dt健康档案 = _ds12月.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;
            //12月绑定数据
            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt12月随访.Text = Convert.ToDateTime(dt12月.Rows[0][tb_儿童_健康检查_12月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt12月体重.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.体重].ToString();
            this.txt12月身长.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.身长].ToString();

            this.txt12月面色.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.面色].ToString();
            if (txt12月面色.Text == "2")
            {
                this.txt12月面色.Text = "1";
            }
            this.txt12月皮肤.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.皮肤].ToString();
            this.txt12月闭合.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.前囟].ToString();

            if (txt12月闭合.Text == "99")
            {
                this.txt12月闭合.Text = "2";
            }

            this.txt12月眼外观.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.眼部].ToString();
            this.txt12月耳外观.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.耳部].ToString();
            this.txt12月听力.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.听力].ToString();
            this.txt12月出牙数.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.出牙数].ToString();
            this.txt12月龋齿数.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.龋齿数].ToString();
            this.txt12月心肺.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.心肺].ToString();
            this.txt12月腹部.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.腹部].ToString();
            this.txt12月四肢.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.四肢].ToString();
            
            this.txt12月佝偻病.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.佝偻病体征].ToString();
            if (txt12月佝偻病.Text == "1")
            {
                this.txt12月佝偻病.Text = "";
            }
            else if (txt12月佝偻病.Text == "10")
            {
                this.txt12月佝偻病.Text = "1";
            }
            else if (txt12月佝偻病.Text == "11")
            {
                this.txt12月佝偻病.Text = "2";
            }
            this.txt12月发育.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.发育评估].ToString();
            this.txt12月两次随访.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.两次随访间患病情况].ToString();
            
            string txt12月指导 = dt12月.Rows[0][tb_儿童_健康检查_12月.指导].ToString();
            string[] arr12月 = txt12月指导.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr12月.Length; i++)
            {
                switch (arr12月[i])
                {
                    case "1":
                        this.txt12月科学.Text = arr12月[i];
                        break;
                    case "5":
                        this.txt12月生长.Text = "2";
                        break;
                    case "6":
                        this.txt12月疾病.Text = "3";
                        break;
                    case "7":
                        this.txt12月预防.Text = "4";
                        break;
                    case "8":
                        this.txt12月口腔保健.Text = "5";
                        break;
                    case "100":
                        this.txt12月低盐.Text = "6";
                        break;
                    default:
                        break;
                }
            }

            

            this.txt12月前囟.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.前囟值].ToString();
            this.txt12月后囟.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.后囟值].ToString();
            //this.txt12月血红.Text = 12满月.Rows[0][tb_儿童_健康检查21满月.血红蛋白值].ToString();
            this.txt12月户外.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.户外活动].ToString();
            this.txt12月维生素.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.服用维生素].ToString();
            this.txt12月其他.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.其他].ToString();
            this.txt12月转诊建议.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.转诊状况].ToString();
            this.txt12月转诊原因.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.转诊原因].ToString();
            this.txt12月转诊机构.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.转诊机构].ToString();
            this.txt12月服务其他.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.中医其他].ToString();
            this.txt12月下次随访.Text = Convert.ToDateTime(dt12月.Rows[0][tb_儿童_健康检查_12月.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt12月随访医生.Text = dt12月.Rows[0][tb_儿童_健康检查_12月.随访医生].ToString();
            //18月绑定数据
            if (dt18月 != null && dt18月.Rows.Count > 0)
            {
                this.txt18月随访.Text = Convert.ToDateTime(dt18月.Rows[0][tb_儿童_健康检查_18月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt18月体重.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.体重].ToString();
                this.txt18月身长.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.身长].ToString();

                this.txt18月面色.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.面色].ToString();
                if (txt18月面色.Text == "2")
                {
                    this.txt18月面色.Text = "1";
                }
                this.txt18月皮肤.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.皮肤].ToString();
                this.txt18月闭合.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.前囟].ToString();

                if (txt18月闭合.Text == "99")
                {
                    this.txt18月闭合.Text = "2";
                }

                this.txt18月眼外观.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.眼部].ToString();
                this.txt18月耳外观.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.耳部].ToString();
                this.txt18月出牙数.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.出牙数].ToString();
                this.txt18月龋齿数.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.龋齿数].ToString();
                this.txt18月心肺.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.心肺].ToString();
                this.txt18月腹部.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.腹部].ToString();
                this.txt18月四肢.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.四肢].ToString();
                this.txt18月步大.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.步态].ToString();
                this.txt18月佝偻病.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.佝偻病体征].ToString();
                if (txt18月佝偻病.Text == "1")
                {
                    this.txt18月佝偻病.Text = "";
                }
                else if (txt18月佝偻病.Text == "10")
                {
                    this.txt18月佝偻病.Text = "1";
                }
                else if (txt18月佝偻病.Text == "11")
                {
                    this.txt18月佝偻病.Text = "2";
                }
                this.txt18月发育.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.发育评估].ToString();
                this.txt18月两次随访.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.两次随访间患病情况].ToString();
                
                string txt18月指导 = dt18月.Rows[0][tb_儿童_健康检查_18月.指导].ToString();
                string[] arr18月 = txt18月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr18月.Length; i++)
                {
                    switch (arr18月[i])
                    {
                        case "1":
                            this.txt18月科学.Text = arr18月[i];
                            break;
                        case "5":
                            this.txt18月生长.Text = "2";
                            break;
                        case "6":
                            this.txt18月疾病.Text = "3";
                            break;
                        case "7":
                            this.txt18月预防.Text = "4";
                            break;
                        case "8":
                            this.txt18月口腔保健.Text = "5";
                            break;
                        case "100":
                            this.txt18月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt18月前囟.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.前囟值].ToString();
                this.txt18月后囟.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.后囟值].ToString();
                this.txt18月血红.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.血红蛋白].ToString();
                this.txt18月户外.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.户外活动].ToString();
                this.txt18月维生素.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.服用维生素].ToString();
                this.txt18月其他.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.其他].ToString();
                this.txt18月转诊建议.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.转诊状况].ToString();
                this.txt18月转诊原因.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.转诊原因].ToString();
                this.txt18月转诊机构.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.转诊机构].ToString();
                this.txt18月服务其他.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.中医药管理其他].ToString();
                this.txt18月下次随访.Text = Convert.ToDateTime(dt18月.Rows[0][tb_儿童_健康检查_18月.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt18月随访医生.Text = dt18月.Rows[0][tb_儿童_健康检查_18月.随访医生].ToString();
            }
            //24月绑定数据
            if (dt24月 != null && dt24月.Rows.Count > 0)
            {
                this.txt24月随访.Text = Convert.ToDateTime(dt24月.Rows[0][tb_儿童_健康检查_24月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt24月体重.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.体重].ToString();
                this.txt24月身长.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.身长].ToString();

                this.txt24月面色.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.面色].ToString();
                if (txt24月面色.Text == "2")
                {
                    this.txt24月面色.Text = "1";
                }
                this.txt24月皮肤.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.皮肤].ToString();
                this.txt24月闭合.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.前囟].ToString();

                if (txt24月闭合.Text == "99")
                {
                    this.txt24月闭合.Text = "2";
                }

                this.txt24月眼外观.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.眼部].ToString();
                this.txt24月耳外观.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.耳部].ToString();
                this.txt24月听力.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.听力].ToString();
                this.txt24月出牙数.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.出牙数].ToString();
                this.txt24月龋齿数.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.龋齿数].ToString();
                this.txt24月心肺.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.心肺].ToString();
                this.txt24月腹部.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.腹部].ToString();
                this.txt24月四肢.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.四肢].ToString();
                this.txt24月步大.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.步态].ToString();
                this.txt24月佝偻病.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.佝偻病体征].ToString();
                if (txt24月佝偻病.Text == "1")
                {
                    this.txt24月佝偻病.Text = "";
                }
                else if (txt24月佝偻病.Text == "10")
                {
                    this.txt24月佝偻病.Text = "1";
                }
                else if (txt24月佝偻病.Text == "11")
                {
                    this.txt24月佝偻病.Text = "2";
                }
                this.txt24月发育.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.发育评估].ToString();
                this.txt24月两次随访.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.两次随访间患病情况].ToString();

                string txt24月指导 = dt24月.Rows[0][tb_儿童_健康检查_24月.指导].ToString();
                string[] arr24月 = txt24月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr24月.Length; i++)
                {
                    switch (arr24月[i])
                    {
                        case "1":
                            this.txt24月合理.Text = arr24月[i];
                            break;
                        case "2":
                            this.txt24月生长.Text = "2";
                            break;
                        case "3":
                            this.txt24月疾病.Text = "3";
                            break;
                        case "4":
                            this.txt24月预防.Text = "4";
                            break;
                        case "99":
                            this.txt24月口腔保健.Text = "5";
                            break;
                        case "100":
                            this.txt24月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt24月前囟.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.前囟值].ToString();
                this.txt24月后囟.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.后囟值].ToString();
                //this.txt24月血红.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.血红蛋白].ToString();
                this.txt24月户外.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.户外活动].ToString();
                this.txt24月维生素.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.服用维生素].ToString();
                this.txt24月其他.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.其他].ToString();
                this.txt24月转诊建议.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.转诊状况].ToString();
                this.txt24月转诊原因.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.转诊原因].ToString();
                this.txt24月转诊机构.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.转诊机构].ToString();
                this.txt24月服务其他.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.中医药管理其他].ToString();
                this.txt24月下次随访.Text = Convert.ToDateTime(dt24月.Rows[0][tb_儿童_健康检查_24月.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt24月随访医生.Text = dt24月.Rows[0][tb_儿童_健康检查_24月.随访医生].ToString();
            }
            //30月绑定数据
            if (dt30月 != null && dt30月.Rows.Count > 0)
            {
                this.txt30月随访.Text = Convert.ToDateTime(dt30月.Rows[0][tb_儿童_健康检查_30月.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt30月体重.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.体重].ToString();
                this.txt30月身长.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.身长].ToString();

                this.txt30月面色.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.面色].ToString();
                if (txt30月面色.Text == "2")
                {
                    this.txt30月面色.Text = "1";
                }
                this.txt30月皮肤.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.皮肤].ToString();
                
                this.txt30月眼外观.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.眼部].ToString();
                this.txt30月耳外观.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.耳部].ToString();
                this.txt30月出牙数.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.牙齿数目].ToString();
                this.txt30月龋齿数.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.龋齿数].ToString();
                this.txt30月心肺.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.心肺].ToString();
                this.txt30月腹部.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.腹部].ToString();
                this.txt30月四肢.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.四肢].ToString();
                this.txt30月步大.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.步态].ToString();
                
                this.txt30月两次随访.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.两次随访间患病情况].ToString();

                string txt30月指导 = dt30月.Rows[0][tb_儿童_健康检查_30月.指导].ToString();
                string[] arr30月 = txt30月指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr30月.Length; i++)
                {
                    switch (arr30月[i])
                    {
                        case "1":
                            this.txt30月合理.Text = arr30月[i];
                            break;
                        case "2":
                            this.txt30月生长.Text = "2";
                            break;
                        case "3":
                            this.txt30月疾病.Text = "3";
                            break;
                        case "4":
                            this.txt30月预防.Text = "4";
                            break;
                        case "99":
                            this.txt30月口腔保健.Text = "5";
                            break;
                        case "100":
                            this.txt30月低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt30月血红.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.血红蛋白值].ToString();
                //this.txt30月户外.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.户外活动].ToString();
                this.txt30月户外.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.户外活动].ToString();
                this.txt30月其他.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.其他].ToString();
                this.txt30月转诊建议.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.转诊状况].ToString();
                this.txt30月转诊原因.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.转诊原因].ToString();
                this.txt30月转诊机构.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.转诊机构].ToString();
                this.txt30月服务其他.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.中医药管理其他].ToString();
                this.txt30月下次随访.Text = Convert.ToDateTime(dt30月.Rows[0][tb_儿童_健康检查_30月.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt30月随访医生.Text = dt30月.Rows[0][tb_儿童_健康检查_30月.随访医生].ToString();
            }
        }
    }
}
