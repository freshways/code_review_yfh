﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.儿童健康信息
{
    public partial class report3_6岁儿童健康检查记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        DataSet _ds3岁;
        DataSet _ds4岁;
        DataSet _ds5岁;
        DataSet _ds6岁;
        //bll老年人随访 _bll = new bll老年人随访();
        bll儿童_健康检查_3岁 _bll3岁 = new bll儿童_健康检查_3岁();
        bll儿童_健康检查_4岁 _bll4岁 = new bll儿童_健康检查_4岁();
        bll儿童_健康检查_5岁 _bll5岁 = new bll儿童_健康检查_5岁();
        bll儿童_健康检查_6岁 _bll6岁 = new bll儿童_健康检查_6岁();

        public report3_6岁儿童健康检查记录表()
        {
            InitializeComponent();
        }

        public report3_6岁儿童健康检查记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds3岁 = _bll3岁.GetInfoByFangshi(_docNo, true);
            _ds4岁 = _bll4岁.GetInfoByFangshi(_docNo, true);
            _ds5岁 = _bll5岁.GetInfoByFangshi(_docNo, true);
            _ds6岁 = _bll6岁.GetInfoByFangshi(_docNo, true);

            DoBindingDataSource(_ds3岁);
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="_ds3岁"></param>
        private void DoBindingDataSource(DataSet _ds3岁)
        {
            //throw new NotImplementedException();
            DataTable dt3岁 = _ds3岁.Tables[Models.tb_儿童_健康检查_3岁.__TableName];
            if (dt3岁 == null || dt3岁.Rows.Count == 0) return;

            DataTable dt4岁 = _ds4岁.Tables[Models.tb_儿童_健康检查_4岁.__TableName];


            DataTable dt5岁 = _ds5岁.Tables[Models.tb_儿童_健康检查_5岁.__TableName];


            DataTable dt6岁 = _ds6岁.Tables[Models.tb_儿童_健康检查_6岁.__TableName];


            DataTable dt健康档案 = _ds3岁.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;
            //3岁绑定数据
            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt3岁随访.Text = Convert.ToDateTime(dt3岁.Rows[0][tb_儿童_健康检查_3岁.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt3岁体重.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.体重].ToString();
            this.txt3岁身长.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.身高].ToString();
            this.txt3岁体格发育.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.体格发育评价].ToString();
            this.txt3岁听力.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.听力].ToString();
            this.txt3岁牙齿数目.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.牙齿数目].ToString();
            this.txt3岁龋齿数.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.龋齿数].ToString();
            this.txt3岁心肺.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.心肺].ToString();
            this.txt3岁腹部.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.腹部].ToString();

            string txt3岁指导 = dt3岁.Rows[0][tb_儿童_健康检查_3岁.指导].ToString();
            string[] arr3岁 = txt3岁指导.Split(',');
            //对指导做出循环判断
            for (int i = 0; i < arr3岁.Length; i++)
            {
                switch (arr3岁[i])
                {
                    case "1":
                        this.txt3岁合理.Text = arr3岁[i];
                        break;
                    case "2":
                        this.txt3岁预防.Text = "4";
                        
                        break;
                    case "3":
                        this.txt3岁生长.Text = "2";
                        
                        break;
                    case "4":
                        this.txt3岁口腔保健.Text = "5";
                        break;
                    case "99":
                        this.txt3岁疾病.Text = "3";
                        break;
                    case "100":
                        this.txt3岁低盐.Text = "6";
                        break;
                    default:
                        break;
                }
            }

            this.txt3岁血红.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.血红蛋白值].ToString();
            this.txt3岁其他.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.其他].ToString();
            this.txt3岁肺炎.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.患病情况肺炎].ToString();
            this.txt3岁腹泻.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.患病情况腹泻].ToString();
            this.txt3岁外伤.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.患病情况外伤].ToString();
            this.txt3岁两次其他.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.患病情况其他].ToString();
            this.txt3岁转诊建议.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.转诊状况].ToString();
            this.txt3岁转诊原因.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.转诊原因].ToString();
            this.txt3岁转诊机构.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.转诊机构].ToString();
            this.txt3岁服务其他.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.中医药管理其他].ToString();
            this.txt3岁下次随访.Text = Convert.ToDateTime(dt3岁.Rows[0][tb_儿童_健康检查_3岁.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt3岁随访医生.Text = dt3岁.Rows[0][tb_儿童_健康检查_3岁.随访医生].ToString();
            //4岁绑定数据
            if (dt4岁 != null && dt4岁.Rows.Count > 0)
            {
                this.txt4岁随访.Text = Convert.ToDateTime(dt4岁.Rows[0][tb_儿童_健康检查_4岁.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt4岁体重.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.体重].ToString();
                this.txt4岁身长.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.身高].ToString();
                this.txt4岁体格发育.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.体格发育评价].ToString();
                this.txt4岁牙齿数目.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.牙齿数目].ToString();
                this.txt4岁龋齿数.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.龋齿数].ToString();
                this.txt4岁心肺.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.心肺].ToString();
                this.txt4岁腹部.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.腹部].ToString();

                string txt4岁指导 = dt4岁.Rows[0][tb_儿童_健康检查_4岁.指导].ToString();
                string[] arr4岁 = txt4岁指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr4岁.Length; i++)
                {
                    switch (arr4岁[i])
                    {
                        case "1":
                            this.txt4岁合理.Text = arr4岁[i];
                            break;
                        case "2":
                            this.txt4岁预防.Text = "4";

                            break;
                        case "3":
                            this.txt4岁生长.Text = "2";

                            break;
                        case "4":
                            this.txt4岁口腔保健.Text = "5";
                            break;
                        case "99":
                            this.txt4岁疾病.Text = "3";
                            break;
                        case "100":
                            this.txt4岁低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt4岁视力.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.视力].ToString();
                this.txt4岁血红.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.血红蛋白值].ToString();
                this.txt4岁其他.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.其他].ToString();
                this.txt4岁肺炎.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.患病情况肺炎].ToString();
                this.txt4岁腹泻.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.患病情况腹泻].ToString();
                this.txt4岁外伤.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.患病情况外伤].ToString();
                this.txt4岁两次其他.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.患病情况其他].ToString();
                this.txt4岁转诊建议.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.转诊状况].ToString();
                this.txt4岁转诊原因.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.转诊原因].ToString();
                this.txt4岁转诊机构.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.转诊机构].ToString();
                
                this.txt4岁下次随访.Text = Convert.ToDateTime(dt4岁.Rows[0][tb_儿童_健康检查_4岁.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt4岁随访医生.Text = dt4岁.Rows[0][tb_儿童_健康检查_4岁.随访医生].ToString();
            }
            //5岁绑定数据
            if (dt5岁 != null && dt5岁.Rows.Count > 0)
            {
                this.txt5岁随访.Text = Convert.ToDateTime(dt5岁.Rows[0][tb_儿童_健康检查_5岁.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt5岁体重.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.体重].ToString();
                this.txt5岁身长.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.身高].ToString();
                this.txt5岁体格发育.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.体格发育评价].ToString();
                this.txt5岁牙齿数目.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.牙齿数目].ToString();
                this.txt5岁龋齿数.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.龋齿数].ToString();
                this.txt5岁心肺.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.心肺].ToString();
                this.txt5岁腹部.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.腹部].ToString();

                string txt5岁指导 = dt5岁.Rows[0][tb_儿童_健康检查_5岁.指导].ToString();
                string[] arr5岁 = txt5岁指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr5岁.Length; i++)
                {
                    switch (arr5岁[i])
                    {
                        case "1":
                            this.txt5岁合理.Text = arr5岁[i];
                            break;
                        case "2":
                            this.txt5岁预防.Text = "4";

                            break;
                        case "3":
                            this.txt5岁生长.Text = "2";

                            break;
                        case "4":
                            this.txt5岁口腔保健.Text = "5";
                            break;
                        case "99":
                            this.txt5岁疾病.Text = "3";
                            break;
                        case "100":
                            this.txt5岁低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt5岁视力.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.视力].ToString();
                this.txt5岁血红.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.血红蛋白值].ToString();
                this.txt5岁其他.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.其他].ToString();
                this.txt5岁肺炎.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.患病情况肺炎].ToString();
                this.txt5岁腹泻.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.患病情况腹泻].ToString();
                this.txt5岁外伤.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.患病情况外伤].ToString();
                this.txt5岁两次其他.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.患病情况其他].ToString();
                this.txt5岁转诊建议.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.转诊状况].ToString();
                this.txt5岁转诊原因.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.转诊原因].ToString();
                this.txt5岁转诊机构.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.转诊机构].ToString();                        
                this.txt5岁下次随访.Text = Convert.ToDateTime(dt5岁.Rows[0][tb_儿童_健康检查_5岁.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt5岁随访医生.Text = dt5岁.Rows[0][tb_儿童_健康检查_5岁.随访医生].ToString();
            }
            //6岁绑定数据
            if (dt6岁 != null && dt6岁.Rows.Count > 0)
            {
                this.txt6岁随访.Text = Convert.ToDateTime(dt6岁.Rows[0][tb_儿童_健康检查_6岁.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt6岁体重.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.体重].ToString();
                this.txt6岁身长.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.身高].ToString();
                this.txt6岁体格发育.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.体格发育评价].ToString();
                this.txt6岁牙齿数目.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.牙齿数目].ToString();
                this.txt6岁龋齿数.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.龋齿数].ToString();
                this.txt6岁心肺.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.心肺].ToString();
                this.txt6岁腹部.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.腹部].ToString();

                string txt6岁指导 = dt6岁.Rows[0][tb_儿童_健康检查_6岁.指导].ToString();
                string[] arr6岁 = txt6岁指导.Split(',');
                //对指导做出循环判断
                for (int i = 0; i < arr6岁.Length; i++)
                {
                    switch (arr6岁[i])
                    {
                        case "1":
                            this.txt6岁合理.Text = arr6岁[i];
                            break;
                        case "2":
                            this.txt6岁预防.Text = "4";

                            break;
                        case "3":
                            this.txt6岁生长.Text = "2";

                            break;
                        case "4":
                            this.txt6岁口腔保健.Text = "5";
                            break;
                        case "99":
                            this.txt6岁疾病.Text = "3";
                            break;
                        case "100":
                            this.txt6岁低盐.Text = "6";
                            break;
                        default:
                            break;
                    }
                }

                this.txt6岁视力.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.视力].ToString();
                this.txt6岁血红.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.血红蛋白值].ToString();
                this.txt6岁其他.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.其他].ToString();
                this.txt6岁肺炎.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.患病情况肺炎].ToString();
                this.txt6岁腹泻.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.患病情况腹泻].ToString();
                this.txt6岁外伤.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.患病情况外伤].ToString();
                this.txt6岁两次其他.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.患病情况其他].ToString();
                this.txt6岁转诊建议.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.转诊状况].ToString();
                this.txt6岁转诊原因.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.转诊原因].ToString();
                this.txt6岁转诊机构.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.转诊机构].ToString();
                //this.txt6岁下次随访.Text = Convert.ToDateTime(dt6岁.Rows[0][tb_儿童_健康检查_6岁.下次随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
                this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();
                this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();
                this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();                //this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();
                this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();            // this.txt6岁随访医生.Text = dt6岁.Rows[0][tb_儿童_健康检查_6岁.随访医生].ToString();
            }
        }

    }
}
