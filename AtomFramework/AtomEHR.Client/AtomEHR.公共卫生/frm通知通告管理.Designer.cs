﻿namespace AtomEHR.公共卫生
{
    partial class frm通知通告管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm通知通告管理));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_标题查询 = new DevExpress.XtraEditors.TextEdit();
            this.DateEdit_创建时间查询1 = new DevExpress.XtraEditors.DateEdit();
            this.DateEdit_创建时间查询2 = new DevExpress.XtraEditors.DateEdit();
            this.LookUp_创建人查询 = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUp_创建机构查询 = new DevExpress.XtraEditors.LookUpEdit();
            this.ComboBoxEdit_置顶 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pagerControl_分页 = new TActionProject.PagerControl();
            this.dataGridControl_查询 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gridView_查询 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_标题 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn_创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_创建机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_是否置顶 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_GUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.sBut_查询 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_新建公告 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl_选项卡 = new DevExpress.XtraTab.XtraTabControl();
            this.Page_查询 = new DevExpress.XtraTab.XtraTabPage();
            this.Page_详情 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit_内容详情 = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.listView_附件详情 = new System.Windows.Forms.ListView();
            this.imageList_图标 = new System.Windows.Forms.ImageList(this.components);
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit修改机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.textEdit_标题详情 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.sBut_下载附件 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_修改 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_删除 = new DevExpress.XtraEditors.SimpleButton();
            this.Page_编辑 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit_内容编辑 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_标题编辑 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.sBut_保存 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_选择附件 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_删除附件 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit_是否置顶编辑 = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.listView_附件编辑 = new System.Windows.Forms.ListView();
            this.checkEdit_是否置顶详情 = new DevExpress.XtraEditors.CheckEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题查询.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUp_创建人查询.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUp_创建机构查询.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_置顶.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridControl_查询)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_查询)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl_选项卡)).BeginInit();
            this.xtraTabControl_选项卡.SuspendLayout();
            this.Page_查询.SuspendLayout();
            this.Page_详情.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_内容详情.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题详情.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.Page_编辑.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_内容编辑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题编辑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_是否置顶编辑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_是否置顶详情.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.xtraTabControl_选项卡);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlSummary.Size = new System.Drawing.Size(938, 595);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tcBusiness.Size = new System.Drawing.Size(938, 595);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit_标题查询);
            this.layoutControl1.Controls.Add(this.DateEdit_创建时间查询1);
            this.layoutControl1.Controls.Add(this.DateEdit_创建时间查询2);
            this.layoutControl1.Controls.Add(this.LookUp_创建人查询);
            this.layoutControl1.Controls.Add(this.LookUp_创建机构查询);
            this.layoutControl1.Controls.Add(this.ComboBoxEdit_置顶);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(922, 88);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit_标题查询
            // 
            this.textEdit_标题查询.Location = new System.Drawing.Point(302, 32);
            this.textEdit_标题查询.Name = "textEdit_标题查询";
            this.textEdit_标题查询.Size = new System.Drawing.Size(608, 20);
            this.textEdit_标题查询.StyleController = this.layoutControl1;
            this.textEdit_标题查询.TabIndex = 10;
            // 
            // DateEdit_创建时间查询1
            // 
            this.DateEdit_创建时间查询1.EditValue = null;
            this.DateEdit_创建时间查询1.Location = new System.Drawing.Point(302, 56);
            this.DateEdit_创建时间查询1.Name = "DateEdit_创建时间查询1";
            this.DateEdit_创建时间查询1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_创建时间查询1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_创建时间查询1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.DateEdit_创建时间查询1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.DateEdit_创建时间查询1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateEdit_创建时间查询1.Size = new System.Drawing.Size(181, 20);
            this.DateEdit_创建时间查询1.StyleController = this.layoutControl1;
            this.DateEdit_创建时间查询1.TabIndex = 6;
            // 
            // DateEdit_创建时间查询2
            // 
            this.DateEdit_创建时间查询2.EditValue = null;
            this.DateEdit_创建时间查询2.Location = new System.Drawing.Point(522, 56);
            this.DateEdit_创建时间查询2.Name = "DateEdit_创建时间查询2";
            this.DateEdit_创建时间查询2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_创建时间查询2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_创建时间查询2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.DateEdit_创建时间查询2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.DateEdit_创建时间查询2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateEdit_创建时间查询2.Size = new System.Drawing.Size(181, 20);
            this.DateEdit_创建时间查询2.StyleController = this.layoutControl1;
            this.DateEdit_创建时间查询2.TabIndex = 7;
            // 
            // LookUp_创建人查询
            // 
            this.LookUp_创建人查询.Location = new System.Drawing.Point(77, 56);
            this.LookUp_创建人查询.Name = "LookUp_创建人查询";
            this.LookUp_创建人查询.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUp_创建人查询.Properties.NullText = "";
            this.LookUp_创建人查询.Size = new System.Drawing.Size(156, 20);
            this.LookUp_创建人查询.StyleController = this.layoutControl1;
            this.LookUp_创建人查询.TabIndex = 8;
            // 
            // LookUp_创建机构查询
            // 
            this.LookUp_创建机构查询.Location = new System.Drawing.Point(77, 32);
            this.LookUp_创建机构查询.Name = "LookUp_创建机构查询";
            this.LookUp_创建机构查询.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUp_创建机构查询.Properties.NullText = "";
            this.LookUp_创建机构查询.Size = new System.Drawing.Size(156, 20);
            this.LookUp_创建机构查询.StyleController = this.layoutControl1;
            this.LookUp_创建机构查询.TabIndex = 4;
            // 
            // ComboBoxEdit_置顶
            // 
            this.ComboBoxEdit_置顶.Location = new System.Drawing.Point(772, 56);
            this.ComboBoxEdit_置顶.Name = "ComboBoxEdit_置顶";
            this.ComboBoxEdit_置顶.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_置顶.Size = new System.Drawing.Size(138, 20);
            this.ComboBoxEdit_置顶.StyleController = this.layoutControl1;
            this.ComboBoxEdit_置顶.TabIndex = 11;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "查询条件";
            this.layoutControlGroup1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem14,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(922, 88);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.DateEdit_创建时间查询2;
            this.layoutControlItem4.CustomizationFormText = "~";
            this.layoutControlItem4.Location = new System.Drawing.Point(475, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem4.Text = "~";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem1.Control = this.LookUp_创建机构查询;
            this.layoutControlItem1.CustomizationFormText = "创建机构:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem1.Text = "创建机构:";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem14.Control = this.textEdit_标题查询;
            this.layoutControlItem14.CustomizationFormText = "标题:";
            this.layoutControlItem14.Location = new System.Drawing.Point(225, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(677, 24);
            this.layoutControlItem14.Text = "标题:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem3.Control = this.DateEdit_创建时间查询1;
            this.layoutControlItem3.CustomizationFormText = "创建时间:";
            this.layoutControlItem3.Location = new System.Drawing.Point(225, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem3.Text = "创建时间:";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.Control = this.LookUp_创建人查询;
            this.layoutControlItem5.CustomizationFormText = "创建人:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem5.Text = "创建人:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.ComboBoxEdit_置顶;
            this.layoutControlItem2.CustomizationFormText = "是否置顶:";
            this.layoutControlItem2.Location = new System.Drawing.Point(695, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem2.Text = "是否置顶:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // pagerControl_分页
            // 
            this.pagerControl_分页.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl_分页.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl_分页.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl_分页.JumpText = "跳转";
            this.pagerControl_分页.Location = new System.Drawing.Point(2, 521);
            this.pagerControl_分页.Margin = new System.Windows.Forms.Padding(4);
            this.pagerControl_分页.MaximumSize = new System.Drawing.Size(0, 4849);
            this.pagerControl_分页.Name = "pagerControl_分页";
            this.pagerControl_分页.PageIndex = 1;
            this.pagerControl_分页.PageSize = 5;
            this.pagerControl_分页.paramters = null;
            this.pagerControl_分页.RecordCount = 0;
            this.pagerControl_分页.Size = new System.Drawing.Size(922, 37);
            this.pagerControl_分页.SqlQuery = null;
            this.pagerControl_分页.TabIndex = 135;
            this.pagerControl_分页.OnPageChanged += new System.EventHandler(this.pagerControl_分页_OnPageChanged);
            // 
            // dataGridControl_查询
            // 
            this.dataGridControl_查询.AllowBandedGridColumnSort = false;
            this.dataGridControl_查询.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridControl_查询.IsBestFitColumns = true;
            this.dataGridControl_查询.Location = new System.Drawing.Point(2, 118);
            this.dataGridControl_查询.MainView = this.gridView_查询;
            this.dataGridControl_查询.Name = "dataGridControl_查询";
            this.dataGridControl_查询.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.dataGridControl_查询.ShowContextMenu = false;
            this.dataGridControl_查询.Size = new System.Drawing.Size(922, 403);
            this.dataGridControl_查询.StrWhere = "";
            this.dataGridControl_查询.TabIndex = 136;
            this.dataGridControl_查询.UseCheckBox = false;
            this.dataGridControl_查询.View = "";
            this.dataGridControl_查询.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_查询});
            // 
            // gridView_查询
            // 
            this.gridView_查询.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView_查询.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView_查询.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView_查询.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.DetailTip.Options.UseFont = true;
            this.gridView_查询.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.Empty.Options.UseFont = true;
            this.gridView_查询.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.EvenRow.Options.UseFont = true;
            this.gridView_查询.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView_查询.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView_查询.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.FixedLine.Options.UseFont = true;
            this.gridView_查询.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView_查询.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView_查询.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gridView_查询.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView_查询.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.GroupButton.Options.UseFont = true;
            this.gridView_查询.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView_查询.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView_查询.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.GroupRow.Options.UseFont = true;
            this.gridView_查询.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView_查询.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView_查询.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.HorzLine.Options.UseFont = true;
            this.gridView_查询.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.OddRow.Options.UseFont = true;
            this.gridView_查询.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.Preview.Options.UseFont = true;
            this.gridView_查询.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.Row.Options.UseFont = true;
            this.gridView_查询.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView_查询.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView_查询.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView_查询.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.VertLine.Options.UseFont = true;
            this.gridView_查询.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gridView_查询.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView_查询.ColumnPanelRowHeight = 30;
            this.gridView_查询.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_创建时间,
            this.gridColumn_标题,
            this.gridColumn_创建人,
            this.gridColumn_创建机构,
            this.gridColumn_是否置顶,
            this.gridColumn_GUID});
            this.gridView_查询.GridControl = this.dataGridControl_查询;
            this.gridView_查询.GroupPanelText = "DragColumn";
            this.gridView_查询.Name = "gridView_查询";
            this.gridView_查询.OptionsView.ColumnAutoWidth = false;
            this.gridView_查询.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView_查询.OptionsView.EnableAppearanceOddRow = true;
            this.gridView_查询.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn_创建时间
            // 
            this.gridColumn_创建时间.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn_创建时间.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn_创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn_创建时间.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建时间.Caption = "创建时间";
            this.gridColumn_创建时间.FieldName = "创建时间";
            this.gridColumn_创建时间.Name = "gridColumn_创建时间";
            this.gridColumn_创建时间.OptionsColumn.ReadOnly = true;
            this.gridColumn_创建时间.Visible = true;
            this.gridColumn_创建时间.VisibleIndex = 0;
            // 
            // gridColumn_标题
            // 
            this.gridColumn_标题.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn_标题.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn_标题.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn_标题.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_标题.Caption = "标题";
            this.gridColumn_标题.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn_标题.FieldName = "标题";
            this.gridColumn_标题.Name = "gridColumn_标题";
            this.gridColumn_标题.OptionsColumn.ReadOnly = true;
            this.gridColumn_标题.Visible = true;
            this.gridColumn_标题.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn_创建人
            // 
            this.gridColumn_创建人.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn_创建人.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn_创建人.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn_创建人.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建人.Caption = "创建人";
            this.gridColumn_创建人.FieldName = "创建人";
            this.gridColumn_创建人.Name = "gridColumn_创建人";
            this.gridColumn_创建人.OptionsColumn.ReadOnly = true;
            this.gridColumn_创建人.Visible = true;
            this.gridColumn_创建人.VisibleIndex = 2;
            // 
            // gridColumn_创建机构
            // 
            this.gridColumn_创建机构.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn_创建机构.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn_创建机构.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_创建机构.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn_创建机构.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_创建机构.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_创建机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_创建机构.Caption = "创建机构";
            this.gridColumn_创建机构.FieldName = "创建机构";
            this.gridColumn_创建机构.Name = "gridColumn_创建机构";
            this.gridColumn_创建机构.OptionsColumn.ReadOnly = true;
            this.gridColumn_创建机构.Visible = true;
            this.gridColumn_创建机构.VisibleIndex = 3;
            // 
            // gridColumn_是否置顶
            // 
            this.gridColumn_是否置顶.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gridColumn_是否置顶.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn_是否置顶.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_是否置顶.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_是否置顶.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn_是否置顶.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_是否置顶.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_是否置顶.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_是否置顶.Caption = "是否置顶";
            this.gridColumn_是否置顶.FieldName = "是否置顶";
            this.gridColumn_是否置顶.Name = "gridColumn_是否置顶";
            this.gridColumn_是否置顶.OptionsColumn.ReadOnly = true;
            this.gridColumn_是否置顶.Visible = true;
            this.gridColumn_是否置顶.VisibleIndex = 4;
            // 
            // gridColumn_GUID
            // 
            this.gridColumn_GUID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn_GUID.AppearanceHeader.Options.UseFont = true;
            this.gridColumn_GUID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_GUID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_GUID.Caption = "GUID";
            this.gridColumn_GUID.Name = "gridColumn_GUID";
            this.gridColumn_GUID.OptionsColumn.ReadOnly = true;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.sBut_查询);
            this.flowLayoutPanel2.Controls.Add(this.sBut_新建公告);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(2, 90);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(922, 28);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // sBut_查询
            // 
            this.sBut_查询.Image = ((System.Drawing.Image)(resources.GetObject("sBut_查询.Image")));
            this.sBut_查询.Location = new System.Drawing.Point(3, 3);
            this.sBut_查询.Name = "sBut_查询";
            this.sBut_查询.Size = new System.Drawing.Size(75, 23);
            this.sBut_查询.TabIndex = 1;
            this.sBut_查询.Text = "查询";
            this.sBut_查询.Click += new System.EventHandler(this.sBut_查询_Click);
            // 
            // sBut_新建公告
            // 
            this.sBut_新建公告.Image = ((System.Drawing.Image)(resources.GetObject("sBut_新建公告.Image")));
            this.sBut_新建公告.Location = new System.Drawing.Point(84, 3);
            this.sBut_新建公告.Name = "sBut_新建公告";
            this.sBut_新建公告.Size = new System.Drawing.Size(75, 23);
            this.sBut_新建公告.TabIndex = 0;
            this.sBut_新建公告.Text = "新建公告";
            this.sBut_新建公告.Visible = false;
            this.sBut_新建公告.Click += new System.EventHandler(this.sBut_新建公告_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dataGridControl_查询);
            this.panelControl1.Controls.Add(this.pagerControl_分页);
            this.panelControl1.Controls.Add(this.flowLayoutPanel2);
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(926, 560);
            this.panelControl1.TabIndex = 137;
            // 
            // xtraTabControl_选项卡
            // 
            this.xtraTabControl_选项卡.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl_选项卡.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl_选项卡.Name = "xtraTabControl_选项卡";
            this.xtraTabControl_选项卡.SelectedTabPage = this.Page_查询;
            this.xtraTabControl_选项卡.Size = new System.Drawing.Size(932, 589);
            this.xtraTabControl_选项卡.TabIndex = 138;
            this.xtraTabControl_选项卡.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Page_查询,
            this.Page_详情,
            this.Page_编辑});
            // 
            // Page_查询
            // 
            this.Page_查询.Controls.Add(this.panelControl1);
            this.Page_查询.Name = "Page_查询";
            this.Page_查询.Size = new System.Drawing.Size(926, 560);
            this.Page_查询.Text = "公告查询";
            // 
            // Page_详情
            // 
            this.Page_详情.Controls.Add(this.memoEdit_内容详情);
            this.Page_详情.Controls.Add(this.panelControl8);
            this.Page_详情.Controls.Add(this.panelControl7);
            this.Page_详情.Controls.Add(this.panelControl6);
            this.Page_详情.Controls.Add(this.panelControl5);
            this.Page_详情.Name = "Page_详情";
            this.Page_详情.PageVisible = false;
            this.Page_详情.Size = new System.Drawing.Size(926, 560);
            this.Page_详情.Text = "公告详情";
            // 
            // memoEdit_内容详情
            // 
            this.memoEdit_内容详情.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_内容详情.Location = new System.Drawing.Point(0, 69);
            this.memoEdit_内容详情.Name = "memoEdit_内容详情";
            this.memoEdit_内容详情.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_内容详情.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_内容详情.Properties.ReadOnly = true;
            this.memoEdit_内容详情.Size = new System.Drawing.Size(926, 315);
            this.memoEdit_内容详情.TabIndex = 5;
            this.memoEdit_内容详情.UseOptimizedRendering = true;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.label2);
            this.panelControl8.Controls.Add(this.listView_附件详情);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl8.Location = new System.Drawing.Point(0, 384);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(926, 80);
            this.panelControl8.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(863, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "附件区";
            // 
            // listView_附件详情
            // 
            this.listView_附件详情.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_附件详情.LargeImageList = this.imageList_图标;
            this.listView_附件详情.Location = new System.Drawing.Point(2, 2);
            this.listView_附件详情.Name = "listView_附件详情";
            this.listView_附件详情.Size = new System.Drawing.Size(922, 76);
            this.listView_附件详情.TabIndex = 0;
            this.listView_附件详情.UseCompatibleStateImageBehavior = false;
            // 
            // imageList_图标
            // 
            this.imageList_图标.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_图标.ImageStream")));
            this.imageList_图标.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_图标.Images.SetKeyName(0, "Unknown");
            this.imageList_图标.Images.SetKeyName(1, "Folder");
            this.imageList_图标.Images.SetKeyName(2, "File");
            this.imageList_图标.Images.SetKeyName(3, "Link");
            this.imageList_图标.Images.SetKeyName(4, "JPG");
            this.imageList_图标.Images.SetKeyName(5, "PNG");
            this.imageList_图标.Images.SetKeyName(6, "PDF");
            this.imageList_图标.Images.SetKeyName(7, "TXT");
            this.imageList_图标.Images.SetKeyName(8, "EXE");
            this.imageList_图标.Images.SetKeyName(9, "ZIP");
            this.imageList_图标.Images.SetKeyName(10, "RAR");
            this.imageList_图标.Images.SetKeyName(11, "Word");
            this.imageList_图标.Images.SetKeyName(12, "Excel");
            this.imageList_图标.Images.SetKeyName(13, "PowerPoint");
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.layoutControl2);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(0, 464);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(926, 96);
            this.panelControl7.TabIndex = 2;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEdit_修改人);
            this.layoutControl2.Controls.Add(this.textEdit_创建人);
            this.layoutControl2.Controls.Add(this.textEdit修改机构);
            this.layoutControl2.Controls.Add(this.textEdit_创建机构);
            this.layoutControl2.Controls.Add(this.textEdit_修改时间);
            this.layoutControl2.Controls.Add(this.textEdit_创建时间);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(922, 92);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEdit_修改人
            // 
            this.textEdit_修改人.Location = new System.Drawing.Point(580, 60);
            this.textEdit_修改人.Name = "textEdit_修改人";
            this.textEdit_修改人.Properties.ReadOnly = true;
            this.textEdit_修改人.Size = new System.Drawing.Size(330, 20);
            this.textEdit_修改人.StyleController = this.layoutControl2;
            this.textEdit_修改人.TabIndex = 9;
            // 
            // textEdit_创建人
            // 
            this.textEdit_创建人.Location = new System.Drawing.Point(129, 60);
            this.textEdit_创建人.Name = "textEdit_创建人";
            this.textEdit_创建人.Properties.ReadOnly = true;
            this.textEdit_创建人.Size = new System.Drawing.Size(330, 20);
            this.textEdit_创建人.StyleController = this.layoutControl2;
            this.textEdit_创建人.TabIndex = 8;
            // 
            // textEdit修改机构
            // 
            this.textEdit修改机构.Location = new System.Drawing.Point(580, 36);
            this.textEdit修改机构.Name = "textEdit修改机构";
            this.textEdit修改机构.Properties.ReadOnly = true;
            this.textEdit修改机构.Size = new System.Drawing.Size(330, 20);
            this.textEdit修改机构.StyleController = this.layoutControl2;
            this.textEdit修改机构.TabIndex = 7;
            // 
            // textEdit_创建机构
            // 
            this.textEdit_创建机构.Location = new System.Drawing.Point(129, 36);
            this.textEdit_创建机构.Name = "textEdit_创建机构";
            this.textEdit_创建机构.Properties.ReadOnly = true;
            this.textEdit_创建机构.Size = new System.Drawing.Size(330, 20);
            this.textEdit_创建机构.StyleController = this.layoutControl2;
            this.textEdit_创建机构.TabIndex = 6;
            // 
            // textEdit_修改时间
            // 
            this.textEdit_修改时间.Location = new System.Drawing.Point(580, 12);
            this.textEdit_修改时间.Name = "textEdit_修改时间";
            this.textEdit_修改时间.Properties.ReadOnly = true;
            this.textEdit_修改时间.Size = new System.Drawing.Size(330, 20);
            this.textEdit_修改时间.StyleController = this.layoutControl2;
            this.textEdit_修改时间.TabIndex = 5;
            // 
            // textEdit_创建时间
            // 
            this.textEdit_创建时间.Location = new System.Drawing.Point(129, 12);
            this.textEdit_创建时间.Name = "textEdit_创建时间";
            this.textEdit_创建时间.Properties.ReadOnly = true;
            this.textEdit_创建时间.Size = new System.Drawing.Size(330, 20);
            this.textEdit_创建时间.StyleController = this.layoutControl2;
            this.textEdit_创建时间.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(922, 92);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit_创建时间;
            this.layoutControlItem8.CustomizationFormText = "创建时间:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem8.Text = "创建时间:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.textEdit_修改时间;
            this.layoutControlItem9.CustomizationFormText = "修改时间:";
            this.layoutControlItem9.Location = new System.Drawing.Point(451, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem9.Text = "修改时间:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit_创建机构;
            this.layoutControlItem10.CustomizationFormText = "创建机构:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem10.Text = "创建机构:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit修改机构;
            this.layoutControlItem11.CustomizationFormText = "修改机构:";
            this.layoutControlItem11.Location = new System.Drawing.Point(451, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem11.Text = "修改机构:";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit_创建人;
            this.layoutControlItem12.CustomizationFormText = "创建人:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem12.Text = "创建人:";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit_修改人;
            this.layoutControlItem13.CustomizationFormText = "修改人:";
            this.layoutControlItem13.Location = new System.Drawing.Point(451, 48);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem13.Text = "修改人:";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(112, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.textEdit_标题详情);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 32);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(926, 37);
            this.panelControl6.TabIndex = 1;
            // 
            // textEdit_标题详情
            // 
            this.textEdit_标题详情.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit_标题详情.EditValue = "";
            this.textEdit_标题详情.Location = new System.Drawing.Point(2, 2);
            this.textEdit_标题详情.Name = "textEdit_标题详情";
            this.textEdit_标题详情.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit_标题详情.Properties.Appearance.Options.UseFont = true;
            this.textEdit_标题详情.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_标题详情.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit_标题详情.Properties.ReadOnly = true;
            this.textEdit_标题详情.Size = new System.Drawing.Size(922, 32);
            this.textEdit_标题详情.TabIndex = 0;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.flowLayoutPanel3);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(926, 32);
            this.panelControl5.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.sBut_下载附件);
            this.flowLayoutPanel3.Controls.Add(this.sBut_修改);
            this.flowLayoutPanel3.Controls.Add(this.sBut_删除);
            this.flowLayoutPanel3.Controls.Add(this.checkEdit_是否置顶详情);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(922, 28);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // sBut_下载附件
            // 
            this.sBut_下载附件.Image = ((System.Drawing.Image)(resources.GetObject("sBut_下载附件.Image")));
            this.sBut_下载附件.Location = new System.Drawing.Point(3, 3);
            this.sBut_下载附件.Name = "sBut_下载附件";
            this.sBut_下载附件.Size = new System.Drawing.Size(75, 23);
            this.sBut_下载附件.TabIndex = 2;
            this.sBut_下载附件.Text = "下载附件";
            this.sBut_下载附件.Click += new System.EventHandler(this.sBut_下载附件_Click);
            // 
            // sBut_修改
            // 
            this.sBut_修改.Image = ((System.Drawing.Image)(resources.GetObject("sBut_修改.Image")));
            this.sBut_修改.Location = new System.Drawing.Point(84, 3);
            this.sBut_修改.Name = "sBut_修改";
            this.sBut_修改.Size = new System.Drawing.Size(75, 23);
            this.sBut_修改.TabIndex = 0;
            this.sBut_修改.Text = "修改";
            this.sBut_修改.Visible = false;
            this.sBut_修改.Click += new System.EventHandler(this.sBut_修改_Click);
            // 
            // sBut_删除
            // 
            this.sBut_删除.Image = ((System.Drawing.Image)(resources.GetObject("sBut_删除.Image")));
            this.sBut_删除.Location = new System.Drawing.Point(165, 3);
            this.sBut_删除.Name = "sBut_删除";
            this.sBut_删除.Size = new System.Drawing.Size(75, 23);
            this.sBut_删除.TabIndex = 1;
            this.sBut_删除.Text = "删除";
            this.sBut_删除.Visible = false;
            this.sBut_删除.Click += new System.EventHandler(this.sBut_删除_Click);
            // 
            // Page_编辑
            // 
            this.Page_编辑.Controls.Add(this.panelControl2);
            this.Page_编辑.Name = "Page_编辑";
            this.Page_编辑.PageVisible = false;
            this.Page_编辑.Size = new System.Drawing.Size(926, 560);
            this.Page_编辑.Text = "公告编辑";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.memoEdit_内容编辑);
            this.panelControl2.Controls.Add(this.layoutControl3);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(926, 560);
            this.panelControl2.TabIndex = 0;
            // 
            // memoEdit_内容编辑
            // 
            this.memoEdit_内容编辑.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit_内容编辑.Location = new System.Drawing.Point(2, 92);
            this.memoEdit_内容编辑.Name = "memoEdit_内容编辑";
            this.memoEdit_内容编辑.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit_内容编辑.Properties.Appearance.Options.UseFont = true;
            this.memoEdit_内容编辑.Properties.MaxLength = 4000;
            this.memoEdit_内容编辑.Properties.NullValuePrompt = "4000字以内";
            this.memoEdit_内容编辑.Properties.NullValuePromptShowForEmptyValue = true;
            this.memoEdit_内容编辑.Size = new System.Drawing.Size(922, 386);
            this.memoEdit_内容编辑.TabIndex = 7;
            this.memoEdit_内容编辑.UseOptimizedRendering = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textEdit_标题编辑);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl3.Location = new System.Drawing.Point(2, 36);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(922, 56);
            this.layoutControl3.TabIndex = 6;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textEdit_标题编辑
            // 
            this.textEdit_标题编辑.EditValue = "";
            this.textEdit_标题编辑.Location = new System.Drawing.Point(97, 12);
            this.textEdit_标题编辑.Name = "textEdit_标题编辑";
            this.textEdit_标题编辑.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit_标题编辑.Properties.Appearance.Options.UseFont = true;
            this.textEdit_标题编辑.Properties.MaxLength = 50;
            this.textEdit_标题编辑.Properties.NullValuePrompt = "50字以内";
            this.textEdit_标题编辑.Properties.NullValuePromptShowForEmptyValue = true;
            this.textEdit_标题编辑.Size = new System.Drawing.Size(813, 32);
            this.textEdit_标题编辑.StyleController = this.layoutControl3;
            this.textEdit_标题编辑.TabIndex = 4;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(922, 56);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit_标题编辑;
            this.layoutControlItem7.CustomizationFormText = "标题:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(902, 36);
            this.layoutControlItem7.Text = "标题:";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.flowLayoutPanel1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(922, 34);
            this.panelControl4.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.sBut_保存);
            this.flowLayoutPanel1.Controls.Add(this.sBut_选择附件);
            this.flowLayoutPanel1.Controls.Add(this.sBut_删除附件);
            this.flowLayoutPanel1.Controls.Add(this.checkEdit_是否置顶编辑);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(918, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // sBut_保存
            // 
            this.sBut_保存.Image = ((System.Drawing.Image)(resources.GetObject("sBut_保存.Image")));
            this.sBut_保存.Location = new System.Drawing.Point(3, 3);
            this.sBut_保存.Name = "sBut_保存";
            this.sBut_保存.Size = new System.Drawing.Size(75, 23);
            this.sBut_保存.TabIndex = 0;
            this.sBut_保存.Text = "保存";
            this.sBut_保存.Click += new System.EventHandler(this.sBut_保存_Click);
            // 
            // sBut_选择附件
            // 
            this.sBut_选择附件.Image = ((System.Drawing.Image)(resources.GetObject("sBut_选择附件.Image")));
            this.sBut_选择附件.Location = new System.Drawing.Point(84, 3);
            this.sBut_选择附件.Name = "sBut_选择附件";
            this.sBut_选择附件.Size = new System.Drawing.Size(75, 23);
            this.sBut_选择附件.TabIndex = 3;
            this.sBut_选择附件.Text = "选择附件";
            this.sBut_选择附件.Click += new System.EventHandler(this.sBut_选择附件_Click);
            // 
            // sBut_删除附件
            // 
            this.sBut_删除附件.Location = new System.Drawing.Point(165, 3);
            this.sBut_删除附件.Name = "sBut_删除附件";
            this.sBut_删除附件.Size = new System.Drawing.Size(75, 23);
            this.sBut_删除附件.TabIndex = 5;
            this.sBut_删除附件.Text = "删除附件";
            this.sBut_删除附件.Click += new System.EventHandler(this.sBut_删除附件_Click);
            // 
            // checkEdit_是否置顶编辑
            // 
            this.checkEdit_是否置顶编辑.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_是否置顶编辑.Location = new System.Drawing.Point(246, 7);
            this.checkEdit_是否置顶编辑.Name = "checkEdit_是否置顶编辑";
            this.checkEdit_是否置顶编辑.Properties.Caption = "是否置顶";
            this.checkEdit_是否置顶编辑.Size = new System.Drawing.Size(75, 19);
            this.checkEdit_是否置顶编辑.TabIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.label1);
            this.panelControl3.Controls.Add(this.listView_附件编辑);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 478);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(922, 80);
            this.panelControl3.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(860, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "附件区";
            // 
            // listView_附件编辑
            // 
            this.listView_附件编辑.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_附件编辑.LargeImageList = this.imageList_图标;
            this.listView_附件编辑.Location = new System.Drawing.Point(2, 2);
            this.listView_附件编辑.Name = "listView_附件编辑";
            this.listView_附件编辑.Size = new System.Drawing.Size(918, 76);
            this.listView_附件编辑.TabIndex = 1;
            this.listView_附件编辑.UseCompatibleStateImageBehavior = false;
            // 
            // checkEdit_是否置顶详情
            // 
            this.checkEdit_是否置顶详情.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_是否置顶详情.Location = new System.Drawing.Point(246, 7);
            this.checkEdit_是否置顶详情.Name = "checkEdit_是否置顶详情";
            this.checkEdit_是否置顶详情.Properties.Caption = "是否置顶";
            this.checkEdit_是否置顶详情.Properties.ReadOnly = true;
            this.checkEdit_是否置顶详情.Size = new System.Drawing.Size(75, 19);
            this.checkEdit_是否置顶详情.TabIndex = 5;
            // 
            // frm通知通告管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 621);
            this.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this.Name = "frm通知通告管理";
            this.Text = "通知通告管理";
            this.Load += new System.EventHandler(this.frm通知通告管理_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题查询.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_创建时间查询2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUp_创建人查询.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUp_创建机构查询.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_置顶.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridControl_查询)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_查询)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl_选项卡)).EndInit();
            this.xtraTabControl_选项卡.ResumeLayout(false);
            this.Page_查询.ResumeLayout(false);
            this.Page_详情.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_内容详情.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题详情.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.Page_编辑.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_内容编辑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_标题编辑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_是否置顶编辑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_是否置顶详情.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private Library.UserControls.DataGridControl dataGridControl_查询;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_查询;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_标题;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_创建人;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_创建机构;
        private TActionProject.PagerControl pagerControl_分页;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_GUID;
        private DevExpress.XtraEditors.DateEdit DateEdit_创建时间查询1;
        private DevExpress.XtraEditors.DateEdit DateEdit_创建时间查询2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton sBut_查询;
        private DevExpress.XtraEditors.SimpleButton sBut_新建公告;
        private DevExpress.XtraEditors.LookUpEdit LookUp_创建人查询;
        private DevExpress.XtraEditors.LookUpEdit LookUp_创建机构查询;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl_选项卡;
        private DevExpress.XtraTab.XtraTabPage Page_查询;
        private DevExpress.XtraTab.XtraTabPage Page_编辑;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.ListView listView_附件编辑;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton sBut_保存;
        private DevExpress.XtraEditors.SimpleButton sBut_选择附件;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.TextEdit textEdit_标题编辑;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.MemoEdit memoEdit_内容编辑;
        private DevExpress.XtraTab.XtraTabPage Page_详情;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.SimpleButton sBut_修改;
        private DevExpress.XtraEditors.SimpleButton sBut_删除;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.TextEdit textEdit_标题详情;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEdit_修改人;
        private DevExpress.XtraEditors.TextEdit textEdit_创建人;
        private DevExpress.XtraEditors.TextEdit textEdit修改机构;
        private DevExpress.XtraEditors.TextEdit textEdit_创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit_修改时间;
        private DevExpress.XtraEditors.TextEdit textEdit_创建时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit textEdit_标题查询;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_置顶;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_是否置顶;
        private System.Windows.Forms.ImageList imageList_图标;
        private DevExpress.XtraEditors.SimpleButton sBut_下载附件;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.ListView listView_附件详情;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.MemoEdit memoEdit_内容详情;
        private DevExpress.XtraEditors.CheckEdit checkEdit_是否置顶编辑;
        private DevExpress.XtraEditors.SimpleButton sBut_删除附件;
        private DevExpress.XtraEditors.CheckEdit checkEdit_是否置顶详情;
    }
}