﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;
using System.Data;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.ReportFrm.Report报告单
{
    public enum Rep_Name
    {
        血常规,
        血生化
    }

    public partial class Report_化验室报告单 : DevExpress.XtraReports.UI.XtraReport
    {
        string str_recordNum;
        string str_idNum;
        string str_examinationDate;
        Rep_Name rn;
        public Report_化验室报告单(string str_recordNum, string str_idNum, string str_examinationDate, Rep_Name rn)
        {
            InitializeComponent();

            this.str_recordNum = str_recordNum;
            this.str_idNum = str_idNum;
            this.str_examinationDate = str_examinationDate;
            this.rn = rn;

            Binding();
        }

        private void Binding()
        {
            bll获取化验信息.Bean_所属医院 be = bll获取化验信息.Get_所属医院();

            DataTable dt = new DataTable();

            switch (rn.ToString())
            {
                case "血常规":
                    dt = bll获取化验信息.Get_血常规(str_idNum, str_examinationDate);
                    break;
                case "血生化":
                    dt = bll获取化验信息.Get_血生化(str_idNum, str_examinationDate);
                    break;
                default:
                    break;
            }

            if (dt.Rows.Count > 0)
            {
                DataSource = dt;

                xrLabel_卫生院.Text = be.str_机构名称;
                xrLabel_报告单名称.Text = rn.ToString() + "检查体检报告单";

                xtc_姓名.Text = dt.Rows[0]["姓名"].ToString();
                xtc_性别.Text = dt.Rows[0]["性别"].ToString();
                xtc_年龄.Text = dt.Rows[0]["年龄"].ToString();
                xtc_身份证号.Text = str_idNum;
                xtc_个人档案编号.Text = str_recordNum;

                xtc_Code.DataBindings.Add("Text", null, "编码");
                xtc_项目名称.DataBindings.Add("Text", null, "检验项目");
                xtc_结果值.DataBindings.Add("Text", null, "结果");
                xtc_提示.DataBindings.Add("Text", null, "标记");
                xtc_单位.DataBindings.Add("Text", null, "单位");
                xtc_参考范围.DataBindings.Add("Text", null, "参考值");

                xtc_检查日期.Text = dt.Rows[0]["检验日期"].ToString();
                xtc_检验医师.Text = dt.Rows[0]["检验医师"].ToString();
                xtc_审核者.Text = dt.Rows[0]["审核者"].ToString();
            }
        }
    }
}
