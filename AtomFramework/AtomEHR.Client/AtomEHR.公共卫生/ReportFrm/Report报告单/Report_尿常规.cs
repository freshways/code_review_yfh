﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.ReportFrm.Report报告单
{
    public partial class Report_尿常规 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report_尿常规()
        {
            InitializeComponent();
        }

        public Report_尿常规(string str_recordNum,string str_Data)
        {
            InitializeComponent();

            bll获取化验信息.Bean_所属医院 be = bll获取化验信息.Get_所属医院();

            bll健康档案 bll_健康档案 = new bll健康档案();
            bll健康体检 bll_健康体检 = new bll健康体检();
            DataRow dr_健康档案 = bll_健康档案.Get_健康档案封面(str_recordNum).Tables[tb_健康档案.__TableName].Rows[0];
            DataRow dr_健康体检 = bll_健康体检.Get_One体检(str_recordNum,str_Data);

            xrLabel_卫生院.Text = be.str_机构名称;
            xtc_姓名.Text = dr_健康档案[tb_健康档案.姓名].ToString();
            xtc_性别.Text = dr_健康档案[tb_健康档案.性别].ToString();
            int age = 0;
            try
            {
                TimeSpan sp = DateTime.Now.Subtract(Convert.ToDateTime(dr_健康档案[tb_健康档案.出生日期]));
                age = sp.Days / 365;
            }
            catch
            {

            }
            xtc_年龄.Text = age.ToString();
            xtc_身份证号.Text = dr_健康档案[tb_健康档案.身份证号].ToString();
            xtc_个人档案编号.Text = str_recordNum;
            xtc_尿蛋白.Text = dr_健康体检[tb_健康体检.尿蛋白].ToString();
            xtc_尿糖.Text = dr_健康体检[tb_健康体检.尿糖].ToString();
            xtc_尿酮体.Text = dr_健康体检[tb_健康体检.尿酮体].ToString();
            xtc_尿潜血.Text = dr_健康体检[tb_健康体检.尿潜血].ToString();
            xtc_检查日期.Text = str_Data;
            xtc_检验医师.Text = "";
            try
            {
                bll医生信息 bll手签 = new bll医生信息();
                //string RGID = ds体检.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
                DataTable dt = bll手签.GetSummaryData(false);

                System.Drawing.Image img = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='辅助检查' ")[0]["s手签"]);
                this.xrPictureBox检查医生.Image = img;

            }
            catch (Exception ex)
            {
            }
        }

    }
}
