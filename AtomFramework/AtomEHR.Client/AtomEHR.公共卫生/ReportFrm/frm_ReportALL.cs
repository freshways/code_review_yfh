﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using RP = AtomEHR.公共卫生.Module.个人健康;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using AtomEHR.公共卫生.ReportFrm.Report报告单;

namespace AtomEHR.公共卫生.ReportFrm
{
    /// <summary>
    /// WXF 统合打印功能
    /// </summary>
    public partial class frm_ReportALL : frmBase
    {
        string str_recordNum = string.Empty;    //个人档案号
        string str_idNum = string.Empty;        //身份证号
        
        DataRow dr_健康档案;
        DataTable dt_既往病史;
        DataRow dr_健康状态;
        Dictionary<string, bool> dic_Classify = new Dictionary<string, bool>();
        List<short> ls_PrintOrder = new List<short>();

        public frm_ReportALL(string str_recordNum)
        {
            InitializeComponent();

            bll健康档案 bll_健康档案 = new bll健康档案();
            DataSet ds_健康档案 = bll_健康档案.Get_基本信息And既往史疾病And健康状态(str_recordNum);
            dr_健康档案 = ds_健康档案.Tables[tb_健康档案.__TableName].Rows[0];
            dt_既往病史 = ds_健康档案.Tables[tb_健康档案_既往病史.__TableName];
            dr_健康状态 = ds_健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0];

            this.str_recordNum = str_recordNum;
            this.str_idNum = dr_健康档案[tb_健康档案.身份证号].ToString();
        }

        #region Load事件
        private void frm_ReportALL_Load(object sender, EventArgs e)
        {
            this.lab_姓名.Text = dr_健康档案[tb_健康档案.姓名].ToString();
            this.lab_身份证号.Text = this.str_idNum;
            this.txt个人档案编号.Text = str_recordNum;

            RecodeClassify();
            List<ListViewItem> ls_lvi = new List<ListViewItem>();
            if (dic_Classify["yun"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "yun";
                ls_lvi.Add(lvi);
                //---------------------------------//
            }

            if (dic_Classify["you"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "you";
                ls_lvi.Add(lvi);
                //---------------------------------//
            }

            if (dic_Classify["lao"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "lao";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bll老年人随访 bll_老年人自理能力评估 = new bll老年人随访();
                bll老年人中医药特征管理 bll_老年人中医药特征管理 = new bll老年人中医药特征管理();
                DataTable dt_老年人自理能力评估 = bll_老年人自理能力评估.Get_发生时间(str_recordNum);
                DataTable dt_老年人中医药特征管理 = bll_老年人中医药特征管理.Get_发生时间(str_recordNum);
                Binding_ComboBoxEdit(dt_老年人自理能力评估, ce_自理能力评估, cbe_自理能力评估, tb_老年人随访.随访日期);
                Binding_ComboBoxEdit(dt_老年人中医药特征管理, ce_中医药管理, cbe_中医药管理, tb_老年人中医药特征管理.发生时间);
            }

            if (dic_Classify["gao"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "gao";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bllMXB高血压随访表 bll_高血压随访 = new bllMXB高血压随访表();
                DataTable dt_高血压随访 = bll_高血压随访.Get_发生时间(str_recordNum);
                Binding_CheckedComboBoxEdit(dt_高血压随访, ce_高血压, ccbe_高血压, tb_MXB高血压随访表.发生时间);
            }

            if (dic_Classify["tang"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "tang";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bllMXB糖尿病随访表 bll_糖尿病随访 = new bllMXB糖尿病随访表();
                DataTable dt_糖尿病随访 = bll_糖尿病随访.Get_发生时间(str_recordNum);
                Binding_CheckedComboBoxEdit(dt_糖尿病随访, ce_糖尿病, ccbe_糖尿病, tb_MXB糖尿病随访表.发生时间);
            }

            if (dic_Classify["nao"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "nao";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bllMXB脑卒中随访表 bll_脑卒中随访 = new bllMXB脑卒中随访表();
                DataTable dt_脑卒中随访 = bll_脑卒中随访.Get_发生时间(str_recordNum);
                Binding_CheckedComboBoxEdit(dt_脑卒中随访, ce_脑卒中, ccbe_脑卒中, tb_MXB脑卒中随访表.发生时间);
            }

            if (dic_Classify["guan"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "guan";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bllMXB冠心病随访表 bll_冠心病随访 = new bllMXB冠心病随访表();
                DataTable dt_冠心病随访 = bll_冠心病随访.Get_发生时间(str_recordNum);
                Binding_CheckedComboBoxEdit(dt_冠心病随访, ce_冠心病, ccbe_冠心病, tb_MXB冠心病随访表.发生时间);
            }

            if (dic_Classify["can"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "can";
                ls_lvi.Add(lvi);
                //---------------------------------//
            }

            if (dic_Classify["jing"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "jing";
                ls_lvi.Add(lvi);
                //---------------------------------//
                bll精神疾病信息补充表 bll_精神障碍补充 = new bll精神疾病信息补充表();
                bll精神病随访记录 bll_精神障碍随访 = new bll精神病随访记录();
                DataTable dt_精神障碍补充 = bll_精神障碍补充.Get_发生时间(str_recordNum);
                DataTable dt_精神障碍随访 = bll_精神障碍随访.Get_发生时间(str_recordNum);
                Binding_ComboBoxEdit(dt_精神障碍补充, ce_精神障碍补充, cbe_精神障碍补充, tb_精神疾病信息补充表.检查日期);
                Binding_ComboBoxEdit(dt_精神障碍随访, ce_精神障碍随访, cbe_精神障碍随访, tb_精神病随访记录.随访日期);
            }

            if (dic_Classify["fei"])
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageKey = "fei";
                ls_lvi.Add(lvi);
                //---------------------------------//
            }
            ListView_Load(ls_lvi);

            //体检表
            bll健康体检 bll_健康体检 = new bll健康体检();
            DataTable dt_健康体检_日期 = bll_健康体检.Get_发生时间(str_recordNum);
            if (dt_健康体检_日期.Rows.Count > 0)
            {
                Binding_ComboBoxEdit(dt_健康体检_日期, ce_健康体检, cbe_健康体检, tb_健康体检.体检日期);

                ce_回执单封面.Enabled = true;
                ce_体检结果及指导.Enabled = true;
                ce_尿常规.Enabled = true;
                ce_心电图.Enabled = true;
                ce_B超正常.Enabled = true;
            }

            //化验室
            DataTable dt_化验信息列表 = bll获取化验信息.Get化验信息列表(str_idNum);
            if (dt_化验信息列表.Rows.Count > 0)
            {
                Binding_ComboBoxEdit(dt_化验信息列表, ce_血常规, cbe_血常规, "化验日期");
                Binding_ComboBoxEdit(dt_化验信息列表, ce_血生化, cbe_血生化, "化验日期");
            }
        }
        #endregion

        #region Button
        private void sBut_预览打印_Click(object sender, EventArgs e)
        {
            ReportPrintTool rpt = new ReportPrintTool(Package_Report());
            rpt.ShowPreviewDialog();
        }

        private void sBut_默认_Click(object sender, EventArgs e)
        {
            ls_PrintOrder.Clear();
            ce_健康体检.Checked = false;
            ce_个人基本信息.Checked = false;
            ce_健康档案封面.Checked = false;
            ce_回执单封面.Checked = false;
            ce_体检结果及指导.Checked = false;
            ce_复核更新记录.Checked = false;
            ce_血常规.Checked = false;
            ce_血生化.Checked = false;
            ce_尿常规.Checked = false;
            ce_心电图.Checked = false;
            ce_B超正常.Checked = false;
            ce_高血压.Checked = false;
            ce_糖尿病.Checked = false;
            ce_脑卒中.Checked = false;
            ce_冠心病.Checked = false;
            ce_自理能力评估.Checked = false;
            ce_中医药管理.Checked = false;
            ce_精神障碍补充.Checked = false;
            ce_精神障碍随访.Checked = false;
            chk高危人群干预随访.Checked = false;
            chk高血压随访服务.Checked = false;
        }

        private void sBut_体检回执单_Click(object sender, EventArgs e)
        {
            ce_回执单封面.Checked = true;
            ce_体检结果及指导.Checked = true;
            ce_血常规.Checked = true;
            ce_血生化.Checked = true;
            ce_尿常规.Checked = true;
            ce_心电图.Checked = true;
            ce_B超正常.Checked = true;

            ls_PrintOrder.Add(4);
            ls_PrintOrder.Add(5);
            ls_PrintOrder.Add(7);
            ls_PrintOrder.Add(8);
            ls_PrintOrder.Add(9);
            ls_PrintOrder.Add(10);
            ls_PrintOrder.Add(11);
        }

        private void sBut_B超报告维护_Click(object sender, EventArgs e)
        {
            DataRow drs = new bll健康体检().Get_One体检(str_recordNum, cbe_健康体检.Text);
            if (drs.Table.Rows.Count >= 1)
            {
                Form frm = new Form();
                AtomEHR.公共卫生.Module.个人健康.体检就诊信息.UC健康体检_B超 uc = new AtomEHR.公共卫生.Module.个人健康.体检就诊信息.UC健康体检_B超(
                    this.lab_姓名.Text, this.txt个人档案编号.Text
                    ,drs[tb_健康体检.B超].ToString()
                    ,drs[tb_健康体检.B超其他].ToString()
                    , this.cbe_健康体检.Text
                    , AtomEHR.Common.UpdateType.Modify);
                frm.Controls.Add(uc);
                uc.Dock = DockStyle.Fill;
                frm.WindowState = FormWindowState.Normal;
                frm.Size = new System.Drawing.Size(750, 690);
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowIcon = false;
                frm.MaximizeBox = false;
                //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.Text = "B超报告编辑";
                frm.Show();
            }
        }

        #endregion

        #region 自定义方法

        /// <summary>
        /// 档案分类(孕,幼,老,高,糖,脑,冠,残,精,肺)
        /// </summary>
        private void RecodeClassify()
        {
            dic_Classify.Add("yun", false);
            dic_Classify.Add("you", false);
            dic_Classify.Add("lao", false);
            dic_Classify.Add("gao", false);
            dic_Classify.Add("tang", false);
            dic_Classify.Add("nao", false);
            dic_Classify.Add("guan", false);
            dic_Classify.Add("can", false);
            dic_Classify.Add("jing", false);
            dic_Classify.Add("fei", false);


            int i_Age = DateTime.Now.Year - Convert.ToDateTime(dr_健康档案[tb_健康档案.出生日期]).Year;

            if (dr_健康档案[tb_健康档案.怀孕情况].ToString().Equals("已孕未生产") || dr_健康档案[tb_健康档案.怀孕情况].ToString().Equals("已生产随访期内"))
            {
                dic_Classify["yun"] = true;
            }

            if (i_Age <= 6)
            {
                dic_Classify["you"] = true;
            }

            if (i_Age >= 65)
            {
                dic_Classify["lao"] = true;
            }

            if (!string.IsNullOrEmpty(dr_健康状态[tb_健康档案_健康状态.残疾情况].ToString()))
            {
                dic_Classify["can"] = true;
            }

            foreach (DataRow item in dt_既往病史.Rows)
            {
                string[] strs_疾病 = item[tb_健康档案_既往病史.疾病名称].ToString().Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (Array.IndexOf(strs_疾病, "2") >= 0)
                {
                    dic_Classify["gao"] = true;
                }

                if (Array.IndexOf(strs_疾病, "3") >= 0)
                {
                    dic_Classify["tang"] = true;
                }

                if (Array.IndexOf(strs_疾病, "7") >= 0)
                {
                    dic_Classify["nao"] = true;
                }

                if (Array.IndexOf(strs_疾病, "4") >= 0)
                {
                    dic_Classify["guan"] = true;
                }

                if (Array.IndexOf(strs_疾病, "8") >= 0)
                {
                    dic_Classify["jing"] = true;
                }

                if (Array.IndexOf(strs_疾病, "5") >= 0)
                {
                    dic_Classify["fei"] = true;
                }
            }
        }

        /// <summary>
        /// 展示档案分类图标
        /// </summary>
        private void ListView_Load(List<ListViewItem> ls_lvi)
        {
            this.listView_重点人群分类.View = View.LargeIcon;
            this.listView_重点人群分类.LargeImageList = this.imageList_重点人群分类;
            this.listView_重点人群分类.Scrollable = false;

            this.listView_重点人群分类.BeginUpdate();
            foreach (ListViewItem item in ls_lvi)
            {
                this.listView_重点人群分类.Items.Add(item);
            }
            this.listView_重点人群分类.EndUpdate();
        }

        /// <summary>
        /// 给CheckedComboBoxEdit控件赋值
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="ce"></param>
        /// <param name="cbe"></param>
        /// <param name="TextName"></param>
        private void Binding_ComboBoxEdit(DataTable dt, CheckEdit ce, ComboBoxEdit cbe, string TextName)
        {
            if (dt.Rows.Count > 0)
            {
                ce.Enabled = true;
                cbe.Enabled = true;
                foreach (DataRow item in dt.Rows)
                {
                    cbe.Properties.Items.Add(item[TextName].ToString());
                }
                cbe.Text = dt.Rows[0][TextName].ToString();
            }
        }

        /// <summary>
        /// 给CheckedComboBoxEdit控件赋值,默认选中前4个选项
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="ce"></param>
        /// <param name="ccbe"></param>
        /// <param name="TextName"></param>
        private void Binding_CheckedComboBoxEdit(DataTable dt, CheckEdit ce, CheckedComboBoxEdit ccbe, string TextName)
        {
            if (dt.Rows.Count > 0)
            {
                ce.Enabled = true;
                ccbe.Enabled = true;
                ccbe.Properties.SelectAllItemVisible = false;
                ccbe.Properties.DataSource = dt;
                ccbe.Properties.DisplayMember = TextName;
                List<string> ls_Default = new List<string>();
                if (dt.Rows.Count >= 4)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        string str_Default = dt.Rows[i][TextName].ToString();
                        if (!string.IsNullOrEmpty(str_Default))
                        {
                            ls_Default.Add(str_Default);
                        }
                    }
                }
                else
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ls_Default.Add(item[TextName].ToString());
                    }
                }
                ccbe.EditValue = string.Join(", ", ls_Default);
            }
        }

        /// <summary>
        /// ******在这里新增打印页******
        /// 汇总需要打印的XtraReport
        /// 返回打印页集合(Dictionary<short, XtraReport>)
        /// </summary>
        /// <returns></returns>
        private Dictionary<short, XtraReport> Collect_Report()
        {
            Dictionary<short, XtraReport> dic_Rep = new Dictionary<short, XtraReport>();
            
            //Key=1
            if (ce_健康档案封面.Checked)
            {
                RP.家庭成员健康信息.report居民健康档案封面 rep_居民健康档案封面 = new RP.家庭成员健康信息.report居民健康档案封面(str_recordNum);
                rep_居民健康档案封面.CreateDocument();
                dic_Rep.Add(1, rep_居民健康档案封面);
            }

            //Key=2
            if (ce_个人基本信息.Checked)
            {
                RP.家庭成员健康信息.report个人基本信息表 rep_个人基本信息 = new RP.家庭成员健康信息.report个人基本信息表(str_recordNum);
                rep_个人基本信息.CreateDocument();
                dic_Rep.Add(2, rep_个人基本信息);
            }

            //Key=3
            if (ce_健康体检.Checked)
            {
                string str_CreateTime = new bll健康体检().Get_One体检(str_recordNum, cbe_健康体检.Text)[tb_健康体检.创建时间].ToString();
                //为了避免重复查询，先把要显示的数据提前取出来
                DataSet ds体检 = new bll健康体检().GetReportDataByKey(str_recordNum, str_CreateTime);
                //扩展方法，默认参数，不影响其他调用
                RP.体检就诊信息.report健康体检表A4_1_2017 rep_健康体检1 = new RP.体检就诊信息.report健康体检表A4_1_2017(str_recordNum, str_CreateTime, ds体检);
                RP.体检就诊信息.report健康体检表A4_2_2017 rep_健康体检2 = new RP.体检就诊信息.report健康体检表A4_2_2017(str_recordNum, str_CreateTime, ds体检);
                RP.体检就诊信息.report健康体检表A4_3_2017 rep_健康体检3 = new RP.体检就诊信息.report健康体检表A4_3_2017(str_recordNum, str_CreateTime, ds体检);
                RP.体检就诊信息.report健康体检表A4_4_2017 rep_健康体检4 = new RP.体检就诊信息.report健康体检表A4_4_2017(str_recordNum, str_CreateTime, ds体检);
                rep_健康体检1.CreateDocument();
                rep_健康体检2.CreateDocument();
                rep_健康体检3.CreateDocument();
                rep_健康体检4.CreateDocument();
                rep_健康体检1.Pages.AddRange(rep_健康体检2.Pages);
                rep_健康体检1.Pages.AddRange(rep_健康体检3.Pages);
                rep_健康体检1.Pages.AddRange(rep_健康体检4.Pages);
                dic_Rep.Add(3, rep_健康体检1);
            }
            
            //Key=4
            if (ce_回执单封面.Checked)
            {
                AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4.回执封面1A4 rep_回执单封面 = new Module.健康档案.体检回执单.回执A4.回执封面1A4(str_recordNum);
                rep_回执单封面.CreateDocument();
                dic_Rep.Add(4, rep_回执单封面);
            }

            //Key=5
            if (ce_体检结果及指导.Checked)
            {
                AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4.report异常结果及健康指导 rep_结果指导 = new Module.健康档案.体检回执单.回执A4.report异常结果及健康指导(str_recordNum, cbe_健康体检.Text, cbe_中医药管理.Text);
                rep_结果指导.CreateDocument();
                dic_Rep.Add(5, rep_结果指导);
            }

            //Key=6
            if (ce_复核更新记录.Checked)
            {
                Report修改记录 rep_复核更新记录 = new Report修改记录(str_recordNum, Convert.ToDateTime("1900-01-01"), DateTime.Now);
                rep_复核更新记录.CreateDocument();
                dic_Rep.Add(6, rep_复核更新记录);
            }

            //Key=7
            if (ce_血常规.Checked)
            {
                Report_化验室报告单 rep_血常规 = new Report_化验室报告单(str_recordNum, str_idNum, cbe_血常规.Text, Rep_Name.血常规);
                rep_血常规.CreateDocument();
                dic_Rep.Add(7, rep_血常规);
            }

            //Key=8
            if (ce_血生化.Checked)
            {
                Report_化验室报告单 rep_血生化 = new Report_化验室报告单(str_recordNum, str_idNum, cbe_血生化.Text, Rep_Name.血生化);
                rep_血生化.CreateDocument();
                dic_Rep.Add(8, rep_血生化);
            }

            //Key=9
            if (ce_尿常规.Checked)
            {
                Report_尿常规 rep_尿常规 = new Report_尿常规(str_recordNum, cbe_健康体检.Text);
                rep_尿常规.CreateDocument();
                dic_Rep.Add(9, rep_尿常规);
            }

            //Key=10
            if (ce_心电图.Checked)
            {
                AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4.回执心电 rep_心电图 = new Module.健康档案.体检回执单.回执A4.回执心电(str_recordNum, cbe_健康体检.Text);
                rep_心电图.CreateDocument();
                dic_Rep.Add(10, rep_心电图);
            }

            //Key=11
            if (ce_B超正常.Checked)
            {
                //if ("1".Equals(new bll健康体检().Get_One体检(str_recordNum, cbe_健康体检.Text)[tb_健康体检.B超].ToString()))
                //{
                   
                //}
                AtomEHR.公共卫生.Module.健康档案.体检回执单.回执A4.回执B超 rep_B超正常 = new Module.健康档案.体检回执单.回执A4.回执B超(str_recordNum, cbe_健康体检.Text);
                rep_B超正常.CreateDocument();
                dic_Rep.Add(11, rep_B超正常);
            }

            //Key=12
            if (ce_高血压.Checked)
            {
                RP.慢性病患者健康信息.report高血压患者随访服务记录表_2017 rep_高血压 = new RP.慢性病患者健康信息.report高血压患者随访服务记录表_2017(str_recordNum, ccbe_高血压.Text.Split(',').ToList<string>());
                rep_高血压.CreateDocument();
                dic_Rep.Add(12, rep_高血压);
            }

            //Key=13
            if (ce_糖尿病.Checked)
            {
                RP.慢性病患者健康信息.report2型糖尿病患者随访服务记录表_2017 rep_糖尿病 = new RP.慢性病患者健康信息.report2型糖尿病患者随访服务记录表_2017(str_recordNum, ccbe_糖尿病.Text.Split(',').ToList<string>());
                rep_糖尿病.CreateDocument();
                dic_Rep.Add(13, rep_糖尿病);
            }

            //Key=14
            if (ce_脑卒中.Checked)
            {
                RP.慢性病患者健康信息.report脑卒中患者随访服务记录表 rep_脑卒中 = new RP.慢性病患者健康信息.report脑卒中患者随访服务记录表(str_recordNum, ccbe_脑卒中.Text.Split(',').ToList<string>());
                rep_脑卒中.CreateDocument();
                dic_Rep.Add(14, rep_脑卒中);
            }

            //Key=15
            if (ce_冠心病.Checked)
            {
                RP.慢性病患者健康信息.report冠心病患者随访服务记录表 rep_冠心病 = new RP.慢性病患者健康信息.report冠心病患者随访服务记录表(str_recordNum, ccbe_冠心病.Text.Split(',').ToList<string>());
                rep_冠心病.CreateDocument();
                dic_Rep.Add(15, rep_冠心病);
            }

            //Key=16
            if (ce_自理能力评估.Checked)
            {
                RP.老年人健康管理.report老年人生活自理能力评估表 rep_自理能力评估 = new RP.老年人健康管理.report老年人生活自理能力评估表(str_recordNum, cbe_自理能力评估.Text);
                rep_自理能力评估.CreateDocument();
                dic_Rep.Add(16, rep_自理能力评估);
            }

            //Key=17
            if (ce_中医药管理.Checked)
            {
                //string str_CreateTime = new bll老年人中医药特征管理().Get_One中医药管理(str_recordNum, cbe_中医药管理.Text)[tb_老年人中医药特征管理.创建时间].ToString();

                RP.老年人健康管理.report老年人中医药健康管理 rep_中医药管理1 = new RP.老年人健康管理.report老年人中医药健康管理(str_recordNum, cbe_中医药管理.Text);
                RP.老年人健康管理.report老年人中医药健康管理反面 rep_中医药管理2 = new RP.老年人健康管理.report老年人中医药健康管理反面(str_recordNum, cbe_中医药管理.Text);
                rep_中医药管理1.CreateDocument();
                rep_中医药管理2.CreateDocument();
                rep_中医药管理1.Pages.AddRange(rep_中医药管理2.Pages);
                dic_Rep.Add(17, rep_中医药管理1);
            }

            //Key=18
            if (ce_精神障碍补充.Checked)
            {
                RP.重性精神疾病患者管理.report重性精神疾病患者个人信息补充表 rep_精神障碍补充 = new RP.重性精神疾病患者管理.report重性精神疾病患者个人信息补充表(str_recordNum);
                rep_精神障碍补充.CreateDocument();
                dic_Rep.Add(18, rep_精神障碍补充);
            }

            //Key=19
            if (ce_精神障碍随访.Checked)
            {
                RP.重性精神疾病患者管理.report重性精神疾病患者随访服务记录表 rep_精神障碍随访 = new RP.重性精神疾病患者管理.report重性精神疾病患者随访服务记录表(str_recordNum, cbe_精神障碍随访.Text);
                rep_精神障碍随访.CreateDocument();
                dic_Rep.Add(19, rep_精神障碍随访);
            }

            //Key=20
            if (chk高危人群干预随访.Checked)
            {
                RP.慢性病患者健康信息.report高血压高危人群干预调查与随访记录表 rep_高危人群干预调查 = new RP.慢性病患者健康信息.report高血压高危人群干预调查与随访记录表();
                rep_高危人群干预调查.CreateDocument();
                dic_Rep.Add(20, rep_高危人群干预调查);
            }

            //Key=21
            if (chk高血压随访服务.Checked)
            {
                RP.慢性病患者健康信息.report高血压患者随访服务记录表二 rep_高血压患者随访服务 = new RP.慢性病患者健康信息.report高血压患者随访服务记录表二();
                rep_高血压患者随访服务.CreateDocument();
                dic_Rep.Add(21, rep_高血压患者随访服务);
            }

            return dic_Rep;
        }

        /// <summary>
        /// 根据排列顺序(ls_PrintOrder)对打印页进行排序
        /// 返回有序的打印页列表(ls_Rep)
        /// </summary>
        /// <returns></returns>
        private List<XtraReport> Order_Report()
        {
            Dictionary<short, XtraReport> dic_Rep = Collect_Report();
            List<XtraReport> ls_Rep = new List<XtraReport>();

            if (ls_PrintOrder.Count() == 0)
            {
                foreach (XtraReport item in dic_Rep.Values)
                {
                    ls_Rep.Add(item);
                }
            }
            else
            {
                foreach (short item in ls_PrintOrder)
                {
                    if (dic_Rep.ContainsKey(item))
                    {
                        ls_Rep.Add(dic_Rep[item]);
                    }
                }
            }

            return ls_Rep;
        }

        /// <summary>
        /// 把有序的打印页(若干个XtraReport)打包成一个XtraReport
        /// 返回组合好的XtraReport
        /// </summary>
        /// <returns></returns>
        private XtraReport Package_Report()
        {
            List<XtraReport> ls_Rep = Order_Report();

            A4_Docker rep_A4 = new A4_Docker();
            rep_A4.CreateDocument();

            foreach (XtraReport item in ls_Rep)
            {
                rep_A4.Pages.AddRange(item.Pages);
            }

            rep_A4.PrintingSystem.ContinuousPageNumbering = true;

            return rep_A4;
        }

        /// <summary>
        /// 从CheckedComboBoxEdit控件的EditValueChanging事件中传入ChangingEventArgs,
        /// 限制CheckedComboBoxEdit控件的多选框最多勾选4个,
        /// 勾选超过4个会回到之前的选中状态
        /// </summary>
        /// <param name="e"></param>
        private void CheckedFor4(DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            string str_newValue = e.NewValue.ToString();

            if (!string.IsNullOrEmpty(str_newValue))
            {
                if (str_newValue.Split(',').Count() > 4)
                {
                    e.NewValue = e.OldValue;
                    XtraMessageBox.Show("一次最多只能打印四次慢病随访,请重新选择!");
                }
            }
        }

        #endregion

        #region 事件

        private void ccbe_高血压_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckedFor4(e);
        }

        private void ccbe_糖尿病_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckedFor4(e);
        }

        private void ccbe_脑卒中_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckedFor4(e);
        }

        private void ccbe_冠心病_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckedFor4(e);
        }

        #endregion
    }
}
