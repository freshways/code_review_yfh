﻿namespace AtomEHR.公共卫生.ReportFrm
{
    partial class frm_ReportALL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ReportALL));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.sBut_默认 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_体检回执单 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_B超报告维护 = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lab_姓名 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lab_身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.listView_重点人群分类 = new System.Windows.Forms.ListView();
            this.imageList_重点人群分类 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chk高血压随访服务 = new DevExpress.XtraEditors.CheckEdit();
            this.chk高危人群干预随访 = new DevExpress.XtraEditors.CheckEdit();
            this.checkedComboBoxEdit2 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbe_高危人群干预 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ce_精神障碍随访 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_中医药管理 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_精神障碍补充 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_自理能力评估 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_血生化 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_血常规 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_健康体检 = new DevExpress.XtraEditors.CheckEdit();
            this.cbe_精神障碍随访 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbe_精神障碍补充 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbe_自理能力评估 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbe_中医药管理 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ce_回执单封面 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_体检结果及指导 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_尿常规 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_心电图 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_B超正常 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.ce_个人基本信息 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_健康档案封面 = new DevExpress.XtraEditors.CheckEdit();
            this.ce_复核更新记录 = new DevExpress.XtraEditors.CheckEdit();
            this.cbe_血生化 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbe_血常规 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ccbe_高血压 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ccbe_糖尿病 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ccbe_脑卒中 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ccbe_冠心病 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cbe_健康体检 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.sBut_预览打印 = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压随访服务.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高危人群干预随访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_高危人群干预.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_精神障碍随访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_中医药管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_精神障碍补充.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_自理能力评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_血生化.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_血常规.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_健康体检.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_精神障碍随访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_精神障碍补充.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_自理能力评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_中医药管理.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ce_回执单封面.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_体检结果及指导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_尿常规.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_心电图.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_B超正常.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ce_个人基本信息.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_健康档案封面.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_复核更新记录.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_血生化.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_血常规.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_健康体检.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanel5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 367);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(878, 50);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "打印模板";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.sBut_默认);
            this.flowLayoutPanel5.Controls.Add(this.sBut_体检回执单);
            this.flowLayoutPanel5.Controls.Add(this.sBut_B超报告维护);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 18);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(872, 29);
            this.flowLayoutPanel5.TabIndex = 0;
            // 
            // sBut_默认
            // 
            this.sBut_默认.Location = new System.Drawing.Point(3, 3);
            this.sBut_默认.Name = "sBut_默认";
            this.sBut_默认.Size = new System.Drawing.Size(75, 23);
            this.sBut_默认.TabIndex = 1;
            this.sBut_默认.Text = "默认";
            this.sBut_默认.Click += new System.EventHandler(this.sBut_默认_Click);
            // 
            // sBut_体检回执单
            // 
            this.sBut_体检回执单.Location = new System.Drawing.Point(84, 3);
            this.sBut_体检回执单.Name = "sBut_体检回执单";
            this.sBut_体检回执单.Size = new System.Drawing.Size(75, 23);
            this.sBut_体检回执单.TabIndex = 0;
            this.sBut_体检回执单.Text = "体检回执单";
            this.sBut_体检回执单.Click += new System.EventHandler(this.sBut_体检回执单_Click);
            // 
            // sBut_B超报告维护
            // 
            this.sBut_B超报告维护.Location = new System.Drawing.Point(165, 3);
            this.sBut_B超报告维护.Name = "sBut_B超报告维护";
            this.sBut_B超报告维护.Size = new System.Drawing.Size(75, 23);
            this.sBut_B超报告维护.TabIndex = 2;
            this.sBut_B超报告维护.Text = "B超报告维护";
            this.sBut_B超报告维护.Click += new System.EventHandler(this.sBut_B超报告维护_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel2);
            this.groupBox2.Controls.Add(this.listView_重点人群分类);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(878, 78);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "档案信息";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelControl2);
            this.flowLayoutPanel2.Controls.Add(this.lab_姓名);
            this.flowLayoutPanel2.Controls.Add(this.labelControl5);
            this.flowLayoutPanel2.Controls.Add(this.lab_身份证号);
            this.flowLayoutPanel2.Controls.Add(this.labelControl3);
            this.flowLayoutPanel2.Controls.Add(this.txt个人档案编号);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 18);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(872, 30);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(3, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(38, 19);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "姓名:";
            // 
            // lab_姓名
            // 
            this.lab_姓名.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_姓名.Location = new System.Drawing.Point(47, 3);
            this.lab_姓名.Margin = new System.Windows.Forms.Padding(3, 3, 30, 3);
            this.lab_姓名.Name = "lab_姓名";
            this.lab_姓名.Size = new System.Drawing.Size(0, 19);
            this.lab_姓名.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(80, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(70, 19);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "身份证号:";
            // 
            // lab_身份证号
            // 
            this.lab_身份证号.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_身份证号.Location = new System.Drawing.Point(156, 3);
            this.lab_身份证号.Margin = new System.Windows.Forms.Padding(3, 3, 30, 3);
            this.lab_身份证号.Name = "lab_身份证号";
            this.lab_身份证号.Size = new System.Drawing.Size(0, 19);
            this.lab_身份证号.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(189, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 19);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "档案号:";
            // 
            // txt个人档案编号
            // 
            this.txt个人档案编号.Location = new System.Drawing.Point(249, 3);
            this.txt个人档案编号.Name = "txt个人档案编号";
            this.txt个人档案编号.Properties.ReadOnly = true;
            this.txt个人档案编号.Size = new System.Drawing.Size(151, 20);
            this.txt个人档案编号.TabIndex = 6;
            // 
            // listView_重点人群分类
            // 
            this.listView_重点人群分类.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listView_重点人群分类.Enabled = false;
            this.listView_重点人群分类.LargeImageList = this.imageList_重点人群分类;
            this.listView_重点人群分类.Location = new System.Drawing.Point(3, 48);
            this.listView_重点人群分类.Name = "listView_重点人群分类";
            this.listView_重点人群分类.Size = new System.Drawing.Size(872, 27);
            this.listView_重点人群分类.TabIndex = 0;
            this.listView_重点人群分类.UseCompatibleStateImageBehavior = false;
            this.listView_重点人群分类.View = System.Windows.Forms.View.SmallIcon;
            // 
            // imageList_重点人群分类
            // 
            this.imageList_重点人群分类.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_重点人群分类.ImageStream")));
            this.imageList_重点人群分类.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_重点人群分类.Images.SetKeyName(0, "yun");
            this.imageList_重点人群分类.Images.SetKeyName(1, "you");
            this.imageList_重点人群分类.Images.SetKeyName(2, "lao");
            this.imageList_重点人群分类.Images.SetKeyName(3, "gao");
            this.imageList_重点人群分类.Images.SetKeyName(4, "tang");
            this.imageList_重点人群分类.Images.SetKeyName(5, "nao");
            this.imageList_重点人群分类.Images.SetKeyName(6, "guan");
            this.imageList_重点人群分类.Images.SetKeyName(7, "can");
            this.imageList_重点人群分类.Images.SetKeyName(8, "jing");
            this.imageList_重点人群分类.Images.SetKeyName(9, "fei");
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.layoutControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 87);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(878, 274);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "打印组合";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chk高血压随访服务);
            this.layoutControl1.Controls.Add(this.chk高危人群干预随访);
            this.layoutControl1.Controls.Add(this.checkedComboBoxEdit2);
            this.layoutControl1.Controls.Add(this.cbe_高危人群干预);
            this.layoutControl1.Controls.Add(this.ce_精神障碍随访);
            this.layoutControl1.Controls.Add(this.ce_中医药管理);
            this.layoutControl1.Controls.Add(this.ce_精神障碍补充);
            this.layoutControl1.Controls.Add(this.ce_自理能力评估);
            this.layoutControl1.Controls.Add(this.ce_冠心病);
            this.layoutControl1.Controls.Add(this.ce_糖尿病);
            this.layoutControl1.Controls.Add(this.ce_脑卒中);
            this.layoutControl1.Controls.Add(this.ce_血生化);
            this.layoutControl1.Controls.Add(this.ce_高血压);
            this.layoutControl1.Controls.Add(this.ce_血常规);
            this.layoutControl1.Controls.Add(this.ce_健康体检);
            this.layoutControl1.Controls.Add(this.cbe_精神障碍随访);
            this.layoutControl1.Controls.Add(this.cbe_精神障碍补充);
            this.layoutControl1.Controls.Add(this.cbe_自理能力评估);
            this.layoutControl1.Controls.Add(this.cbe_中医药管理);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.cbe_血生化);
            this.layoutControl1.Controls.Add(this.cbe_血常规);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.ccbe_高血压);
            this.layoutControl1.Controls.Add(this.ccbe_糖尿病);
            this.layoutControl1.Controls.Add(this.ccbe_脑卒中);
            this.layoutControl1.Controls.Add(this.ccbe_冠心病);
            this.layoutControl1.Controls.Add(this.cbe_健康体检);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(3, 18);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(872, 253);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chk高血压随访服务
            // 
            this.chk高血压随访服务.Location = new System.Drawing.Point(438, 156);
            this.chk高血压随访服务.Name = "chk高血压随访服务";
            this.chk高血压随访服务.Properties.Caption = "";
            this.chk高血压随访服务.Size = new System.Drawing.Size(19, 19);
            this.chk高血压随访服务.StyleController = this.layoutControl1;
            this.chk高血压随访服务.TabIndex = 32;
            // 
            // chk高危人群干预随访
            // 
            this.chk高危人群干预随访.Location = new System.Drawing.Point(12, 156);
            this.chk高危人群干预随访.Name = "chk高危人群干预随访";
            this.chk高危人群干预随访.Properties.Caption = "";
            this.chk高危人群干预随访.Size = new System.Drawing.Size(19, 19);
            this.chk高危人群干预随访.StyleController = this.layoutControl1;
            this.chk高危人群干预随访.TabIndex = 31;
            // 
            // checkedComboBoxEdit2
            // 
            this.checkedComboBoxEdit2.Location = new System.Drawing.Point(668, 156);
            this.checkedComboBoxEdit2.Name = "checkedComboBoxEdit2";
            this.checkedComboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit2.Size = new System.Drawing.Size(192, 20);
            this.checkedComboBoxEdit2.StyleController = this.layoutControl1;
            this.checkedComboBoxEdit2.TabIndex = 30;
            // 
            // cbe_高危人群干预
            // 
            this.cbe_高危人群干预.Location = new System.Drawing.Point(242, 156);
            this.cbe_高危人群干预.Name = "cbe_高危人群干预";
            this.cbe_高危人群干预.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_高危人群干预.Size = new System.Drawing.Size(192, 20);
            this.cbe_高危人群干预.StyleController = this.layoutControl1;
            this.cbe_高危人群干预.TabIndex = 29;
            // 
            // ce_精神障碍随访
            // 
            this.ce_精神障碍随访.Enabled = false;
            this.ce_精神障碍随访.Location = new System.Drawing.Point(438, 132);
            this.ce_精神障碍随访.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_精神障碍随访.Name = "ce_精神障碍随访";
            this.ce_精神障碍随访.Properties.Caption = "";
            this.ce_精神障碍随访.Size = new System.Drawing.Size(19, 19);
            this.ce_精神障碍随访.StyleController = this.layoutControl1;
            this.ce_精神障碍随访.TabIndex = 27;
            // 
            // ce_中医药管理
            // 
            this.ce_中医药管理.Enabled = false;
            this.ce_中医药管理.Location = new System.Drawing.Point(438, 108);
            this.ce_中医药管理.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_中医药管理.Name = "ce_中医药管理";
            this.ce_中医药管理.Properties.Caption = "";
            this.ce_中医药管理.Size = new System.Drawing.Size(19, 19);
            this.ce_中医药管理.StyleController = this.layoutControl1;
            this.ce_中医药管理.TabIndex = 27;
            // 
            // ce_精神障碍补充
            // 
            this.ce_精神障碍补充.Enabled = false;
            this.ce_精神障碍补充.Location = new System.Drawing.Point(12, 132);
            this.ce_精神障碍补充.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_精神障碍补充.Name = "ce_精神障碍补充";
            this.ce_精神障碍补充.Properties.Caption = "";
            this.ce_精神障碍补充.Size = new System.Drawing.Size(19, 19);
            this.ce_精神障碍补充.StyleController = this.layoutControl1;
            this.ce_精神障碍补充.TabIndex = 27;
            // 
            // ce_自理能力评估
            // 
            this.ce_自理能力评估.Enabled = false;
            this.ce_自理能力评估.Location = new System.Drawing.Point(12, 108);
            this.ce_自理能力评估.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_自理能力评估.Name = "ce_自理能力评估";
            this.ce_自理能力评估.Properties.Caption = "";
            this.ce_自理能力评估.Size = new System.Drawing.Size(19, 19);
            this.ce_自理能力评估.StyleController = this.layoutControl1;
            this.ce_自理能力评估.TabIndex = 28;
            // 
            // ce_冠心病
            // 
            this.ce_冠心病.Enabled = false;
            this.ce_冠心病.Location = new System.Drawing.Point(438, 84);
            this.ce_冠心病.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_冠心病.Name = "ce_冠心病";
            this.ce_冠心病.Properties.Caption = "";
            this.ce_冠心病.Size = new System.Drawing.Size(19, 19);
            this.ce_冠心病.StyleController = this.layoutControl1;
            this.ce_冠心病.TabIndex = 26;
            // 
            // ce_糖尿病
            // 
            this.ce_糖尿病.Enabled = false;
            this.ce_糖尿病.Location = new System.Drawing.Point(438, 60);
            this.ce_糖尿病.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_糖尿病.Name = "ce_糖尿病";
            this.ce_糖尿病.Properties.Caption = "";
            this.ce_糖尿病.Size = new System.Drawing.Size(19, 19);
            this.ce_糖尿病.StyleController = this.layoutControl1;
            this.ce_糖尿病.TabIndex = 26;
            // 
            // ce_脑卒中
            // 
            this.ce_脑卒中.Enabled = false;
            this.ce_脑卒中.Location = new System.Drawing.Point(12, 84);
            this.ce_脑卒中.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_脑卒中.Name = "ce_脑卒中";
            this.ce_脑卒中.Properties.Caption = "";
            this.ce_脑卒中.Size = new System.Drawing.Size(19, 19);
            this.ce_脑卒中.StyleController = this.layoutControl1;
            this.ce_脑卒中.TabIndex = 26;
            // 
            // ce_血生化
            // 
            this.ce_血生化.Enabled = false;
            this.ce_血生化.Location = new System.Drawing.Point(225, 36);
            this.ce_血生化.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_血生化.Name = "ce_血生化";
            this.ce_血生化.Properties.Caption = "";
            this.ce_血生化.Size = new System.Drawing.Size(19, 19);
            this.ce_血生化.StyleController = this.layoutControl1;
            this.ce_血生化.TabIndex = 26;
            // 
            // ce_高血压
            // 
            this.ce_高血压.Enabled = false;
            this.ce_高血压.Location = new System.Drawing.Point(12, 60);
            this.ce_高血压.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_高血压.Name = "ce_高血压";
            this.ce_高血压.Properties.Caption = "";
            this.ce_高血压.Size = new System.Drawing.Size(19, 19);
            this.ce_高血压.StyleController = this.layoutControl1;
            this.ce_高血压.TabIndex = 27;
            // 
            // ce_血常规
            // 
            this.ce_血常规.Enabled = false;
            this.ce_血常规.Location = new System.Drawing.Point(12, 36);
            this.ce_血常规.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_血常规.Name = "ce_血常规";
            this.ce_血常规.Properties.Caption = "";
            this.ce_血常规.Size = new System.Drawing.Size(19, 19);
            this.ce_血常规.StyleController = this.layoutControl1;
            this.ce_血常规.TabIndex = 26;
            // 
            // ce_健康体检
            // 
            this.ce_健康体检.Enabled = false;
            this.ce_健康体检.Location = new System.Drawing.Point(12, 12);
            this.ce_健康体检.MaximumSize = new System.Drawing.Size(19, 0);
            this.ce_健康体检.Name = "ce_健康体检";
            this.ce_健康体检.Properties.Caption = "";
            this.ce_健康体检.Size = new System.Drawing.Size(19, 19);
            this.ce_健康体检.StyleController = this.layoutControl1;
            this.ce_健康体检.TabIndex = 25;
            // 
            // cbe_精神障碍随访
            // 
            this.cbe_精神障碍随访.Enabled = false;
            this.cbe_精神障碍随访.Location = new System.Drawing.Point(646, 132);
            this.cbe_精神障碍随访.Name = "cbe_精神障碍随访";
            this.cbe_精神障碍随访.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_精神障碍随访.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_精神障碍随访.Size = new System.Drawing.Size(214, 20);
            this.cbe_精神障碍随访.StyleController = this.layoutControl1;
            this.cbe_精神障碍随访.TabIndex = 24;
            // 
            // cbe_精神障碍补充
            // 
            this.cbe_精神障碍补充.Enabled = false;
            this.cbe_精神障碍补充.Location = new System.Drawing.Point(217, 132);
            this.cbe_精神障碍补充.Name = "cbe_精神障碍补充";
            this.cbe_精神障碍补充.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_精神障碍补充.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_精神障碍补充.Size = new System.Drawing.Size(217, 20);
            this.cbe_精神障碍补充.StyleController = this.layoutControl1;
            this.cbe_精神障碍补充.TabIndex = 23;
            // 
            // cbe_自理能力评估
            // 
            this.cbe_自理能力评估.Enabled = false;
            this.cbe_自理能力评估.Location = new System.Drawing.Point(217, 108);
            this.cbe_自理能力评估.Name = "cbe_自理能力评估";
            this.cbe_自理能力评估.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_自理能力评估.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_自理能力评估.Size = new System.Drawing.Size(217, 20);
            this.cbe_自理能力评估.StyleController = this.layoutControl1;
            this.cbe_自理能力评估.TabIndex = 22;
            // 
            // cbe_中医药管理
            // 
            this.cbe_中医药管理.Enabled = false;
            this.cbe_中医药管理.Location = new System.Drawing.Point(646, 108);
            this.cbe_中医药管理.Name = "cbe_中医药管理";
            this.cbe_中医药管理.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_中医药管理.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_中医药管理.Size = new System.Drawing.Size(214, 20);
            this.cbe_中医药管理.StyleController = this.layoutControl1;
            this.cbe_中医药管理.TabIndex = 21;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.ce_回执单封面);
            this.flowLayoutPanel1.Controls.Add(this.ce_体检结果及指导);
            this.flowLayoutPanel1.Controls.Add(this.ce_尿常规);
            this.flowLayoutPanel1.Controls.Add(this.ce_心电图);
            this.flowLayoutPanel1.Controls.Add(this.ce_B超正常);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(225, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(635, 20);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // ce_回执单封面
            // 
            this.ce_回执单封面.Enabled = false;
            this.ce_回执单封面.Location = new System.Drawing.Point(3, 3);
            this.ce_回执单封面.Name = "ce_回执单封面";
            this.ce_回执单封面.Properties.Caption = "体检回执单封面";
            this.ce_回执单封面.Size = new System.Drawing.Size(120, 19);
            this.ce_回执单封面.TabIndex = 2;
            // 
            // ce_体检结果及指导
            // 
            this.ce_体检结果及指导.Enabled = false;
            this.ce_体检结果及指导.Location = new System.Drawing.Point(129, 3);
            this.ce_体检结果及指导.Name = "ce_体检结果及指导";
            this.ce_体检结果及指导.Properties.Caption = "体检结果及指导";
            this.ce_体检结果及指导.Size = new System.Drawing.Size(128, 19);
            this.ce_体检结果及指导.TabIndex = 3;
            // 
            // ce_尿常规
            // 
            this.ce_尿常规.Enabled = false;
            this.ce_尿常规.Location = new System.Drawing.Point(263, 3);
            this.ce_尿常规.Name = "ce_尿常规";
            this.ce_尿常规.Properties.Caption = "尿常规报告单";
            this.ce_尿常规.Size = new System.Drawing.Size(116, 19);
            this.ce_尿常规.TabIndex = 0;
            // 
            // ce_心电图
            // 
            this.ce_心电图.Enabled = false;
            this.ce_心电图.Location = new System.Drawing.Point(385, 3);
            this.ce_心电图.Name = "ce_心电图";
            this.ce_心电图.Properties.Caption = "心电图报告单";
            this.ce_心电图.Size = new System.Drawing.Size(116, 19);
            this.ce_心电图.TabIndex = 1;
            // 
            // ce_B超正常
            // 
            this.ce_B超正常.Enabled = false;
            this.ce_B超正常.Location = new System.Drawing.Point(507, 3);
            this.ce_B超正常.Name = "ce_B超正常";
            this.ce_B超正常.Properties.Caption = "B超报告单";
            this.ce_B超正常.Size = new System.Drawing.Size(116, 19);
            this.ce_B超正常.TabIndex = 2;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.ce_个人基本信息);
            this.flowLayoutPanel4.Controls.Add(this.ce_健康档案封面);
            this.flowLayoutPanel4.Controls.Add(this.ce_复核更新记录);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(438, 36);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(422, 20);
            this.flowLayoutPanel4.TabIndex = 17;
            // 
            // ce_个人基本信息
            // 
            this.ce_个人基本信息.Location = new System.Drawing.Point(3, 3);
            this.ce_个人基本信息.Name = "ce_个人基本信息";
            this.ce_个人基本信息.Properties.Caption = "个人基本信息表";
            this.ce_个人基本信息.Size = new System.Drawing.Size(121, 19);
            this.ce_个人基本信息.TabIndex = 0;
            // 
            // ce_健康档案封面
            // 
            this.ce_健康档案封面.Location = new System.Drawing.Point(130, 3);
            this.ce_健康档案封面.Name = "ce_健康档案封面";
            this.ce_健康档案封面.Properties.Caption = "居民健康档案封面";
            this.ce_健康档案封面.Size = new System.Drawing.Size(128, 19);
            this.ce_健康档案封面.TabIndex = 1;
            // 
            // ce_复核更新记录
            // 
            this.ce_复核更新记录.Location = new System.Drawing.Point(264, 3);
            this.ce_复核更新记录.Name = "ce_复核更新记录";
            this.ce_复核更新记录.Properties.Caption = "基本信息复核更新记录";
            this.ce_复核更新记录.Size = new System.Drawing.Size(146, 19);
            this.ce_复核更新记录.TabIndex = 3;
            // 
            // cbe_血生化
            // 
            this.cbe_血生化.Enabled = false;
            this.cbe_血生化.Location = new System.Drawing.Point(334, 36);
            this.cbe_血生化.Name = "cbe_血生化";
            this.cbe_血生化.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_血生化.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_血生化.Size = new System.Drawing.Size(100, 20);
            this.cbe_血生化.StyleController = this.layoutControl1;
            this.cbe_血生化.TabIndex = 16;
            // 
            // cbe_血常规
            // 
            this.cbe_血常规.EditValue = "";
            this.cbe_血常规.Enabled = false;
            this.cbe_血常规.Location = new System.Drawing.Point(121, 36);
            this.cbe_血常规.Name = "cbe_血常规";
            this.cbe_血常规.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_血常规.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_血常规.Size = new System.Drawing.Size(100, 20);
            this.cbe_血常规.StyleController = this.layoutControl1;
            this.cbe_血常规.TabIndex = 15;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(12, 180);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(848, 61);
            this.flowLayoutPanel3.TabIndex = 14;
            // 
            // ccbe_高血压
            // 
            this.ccbe_高血压.EditValue = "";
            this.ccbe_高血压.Enabled = false;
            this.ccbe_高血压.Location = new System.Drawing.Point(121, 60);
            this.ccbe_高血压.Name = "ccbe_高血压";
            this.ccbe_高血压.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbe_高血压.Size = new System.Drawing.Size(313, 20);
            this.ccbe_高血压.StyleController = this.layoutControl1;
            this.ccbe_高血压.TabIndex = 9;
            this.ccbe_高血压.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ccbe_高血压_EditValueChanging);
            // 
            // ccbe_糖尿病
            // 
            this.ccbe_糖尿病.EditValue = "";
            this.ccbe_糖尿病.Enabled = false;
            this.ccbe_糖尿病.Location = new System.Drawing.Point(547, 60);
            this.ccbe_糖尿病.Name = "ccbe_糖尿病";
            this.ccbe_糖尿病.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbe_糖尿病.Size = new System.Drawing.Size(313, 20);
            this.ccbe_糖尿病.StyleController = this.layoutControl1;
            this.ccbe_糖尿病.TabIndex = 7;
            this.ccbe_糖尿病.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ccbe_糖尿病_EditValueChanging);
            // 
            // ccbe_脑卒中
            // 
            this.ccbe_脑卒中.EditValue = "";
            this.ccbe_脑卒中.Enabled = false;
            this.ccbe_脑卒中.Location = new System.Drawing.Point(121, 84);
            this.ccbe_脑卒中.Name = "ccbe_脑卒中";
            this.ccbe_脑卒中.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbe_脑卒中.Size = new System.Drawing.Size(313, 20);
            this.ccbe_脑卒中.StyleController = this.layoutControl1;
            this.ccbe_脑卒中.TabIndex = 8;
            this.ccbe_脑卒中.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ccbe_脑卒中_EditValueChanging);
            // 
            // ccbe_冠心病
            // 
            this.ccbe_冠心病.EditValue = "";
            this.ccbe_冠心病.Enabled = false;
            this.ccbe_冠心病.Location = new System.Drawing.Point(547, 84);
            this.ccbe_冠心病.Name = "ccbe_冠心病";
            this.ccbe_冠心病.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ccbe_冠心病.Size = new System.Drawing.Size(313, 20);
            this.ccbe_冠心病.StyleController = this.layoutControl1;
            this.ccbe_冠心病.TabIndex = 10;
            this.ccbe_冠心病.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ccbe_冠心病_EditValueChanging);
            // 
            // cbe_健康体检
            // 
            this.cbe_健康体检.EditValue = "";
            this.cbe_健康体检.Enabled = false;
            this.cbe_健康体检.Location = new System.Drawing.Point(121, 12);
            this.cbe_健康体检.Name = "cbe_健康体检";
            this.cbe_健康体检.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbe_健康体检.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbe_健康体检.Size = new System.Drawing.Size(100, 20);
            this.cbe_健康体检.StyleController = this.layoutControl1;
            this.cbe_健康体检.TabIndex = 18;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem2,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(872, 253);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem6.Control = this.ccbe_高血压;
            this.layoutControlItem6.CustomizationFormText = "高血压患者随访";
            this.layoutControlItem6.Location = new System.Drawing.Point(23, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem6.Text = "高血压患者随访";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem6.TextToControlDistance = 0;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem4.Control = this.ccbe_糖尿病;
            this.layoutControlItem4.CustomizationFormText = "糖尿病患者随访";
            this.layoutControlItem4.Location = new System.Drawing.Point(449, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem4.Text = "糖尿病患者随访";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem4.TextToControlDistance = 0;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.flowLayoutPanel3;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(852, 65);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.cbe_血常规;
            this.layoutControlItem8.CustomizationFormText = "血常规报告单";
            this.layoutControlItem8.Location = new System.Drawing.Point(23, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem8.Text = "血常规报告单";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(86, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.cbe_血生化;
            this.layoutControlItem9.CustomizationFormText = "血生化报告单";
            this.layoutControlItem9.Location = new System.Drawing.Point(236, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem9.Text = "血生化报告单";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(86, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.flowLayoutPanel4;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(426, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(426, 24);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem11.Control = this.cbe_健康体检;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(23, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem11.Text = "健康体检表";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(86, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.flowLayoutPanel1;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(213, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(639, 24);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.Control = this.ccbe_脑卒中;
            this.layoutControlItem5.CustomizationFormText = "脑卒中患者随访";
            this.layoutControlItem5.Location = new System.Drawing.Point(23, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem5.Text = "脑卒中患者随访";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem5.TextToControlDistance = 0;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.ccbe_冠心病;
            this.layoutControlItem7.CustomizationFormText = "冠心病患者随访";
            this.layoutControlItem7.Location = new System.Drawing.Point(449, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem7.Text = "冠心病患者随访";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(86, 14);
            this.layoutControlItem7.TextToControlDistance = 0;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem12.Control = this.cbe_中医药管理;
            this.layoutControlItem12.CustomizationFormText = "老年人中医药健康管理服务记录表";
            this.layoutControlItem12.Location = new System.Drawing.Point(449, 96);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem12.Text = "老年人中医药健康管理服务记录表";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(180, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem13.Control = this.cbe_自理能力评估;
            this.layoutControlItem13.CustomizationFormText = "老年人生活自理能力评估表";
            this.layoutControlItem13.Location = new System.Drawing.Point(23, 96);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem13.Text = "老年人生活自理能力评估表";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(182, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.cbe_精神障碍补充;
            this.layoutControlItem2.CustomizationFormText = "严重精神障碍患者个人信息补充表";
            this.layoutControlItem2.Location = new System.Drawing.Point(23, 120);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem2.Text = "严重精神障碍患者个人信息补充表";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(182, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem14.Control = this.cbe_精神障碍随访;
            this.layoutControlItem14.CustomizationFormText = "严重精神障碍患者随访服务记录表";
            this.layoutControlItem14.Location = new System.Drawing.Point(449, 120);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem14.Text = "严重精神障碍患者随访服务记录表";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(180, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.ce_健康体检;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.ce_血常规;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.ce_血生化;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(213, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.ce_高血压;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.ce_脑卒中;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.ce_糖尿病;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(426, 48);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.ce_冠心病;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(426, 72);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.ce_自理能力评估;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.ce_精神障碍补充;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.ce_中医药管理;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(426, 96);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.ce_精神障碍随访;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(426, 120);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.cbe_高危人群干预;
            this.layoutControlItem26.CustomizationFormText = "高血压高危人群干预调查与随访记录表";
            this.layoutControlItem26.Location = new System.Drawing.Point(23, 144);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem26.Text = "高血压高危人群干预调查与随访记录表";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(204, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.checkedComboBoxEdit2;
            this.layoutControlItem27.CustomizationFormText = "高血压患者随访服务记录表(二)";
            this.layoutControlItem27.Location = new System.Drawing.Point(449, 144);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem27.Text = "高血压患者随访服务记录表(二)";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(204, 14);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.chk高危人群干预随访;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(23, 23);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(23, 23);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.chk高血压随访服务;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(426, 144);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(23, 23);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(23, 23);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(23, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelControl1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 423);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(878, 78);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "打印说明";
            // 
            // labelControl1
            // 
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelControl1.Location = new System.Drawing.Point(3, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(424, 42);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "显示为禁用状态的为此档案无此类数据,可结合档案信息中的重点人群概览来操作\r\n随访表默认为选择最新的数据\r\n体检结果与指导需配合体检日期与中医药健康管理随访日期来使" +
    "用";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sBut_预览打印);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 507);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(878, 51);
            this.panel1.TabIndex = 6;
            // 
            // sBut_预览打印
            // 
            this.sBut_预览打印.Image = ((System.Drawing.Image)(resources.GetObject("sBut_预览打印.Image")));
            this.sBut_预览打印.Location = new System.Drawing.Point(763, 12);
            this.sBut_预览打印.Name = "sBut_预览打印";
            this.sBut_预览打印.Size = new System.Drawing.Size(80, 30);
            this.sBut_预览打印.TabIndex = 1;
            this.sBut_预览打印.Text = "预览打印";
            this.sBut_预览打印.Click += new System.EventHandler(this.sBut_预览打印_Click);
            // 
            // frm_ReportALL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_ReportALL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "打印总览";
            this.Load += new System.EventHandler(this.frm_ReportALL_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压随访服务.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk高危人群干预随访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_高危人群干预.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_精神障碍随访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_中医药管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_精神障碍补充.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_自理能力评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_血生化.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_血常规.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_健康体检.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_精神障碍随访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_精神障碍补充.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_自理能力评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_中医药管理.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ce_回执单封面.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_体检结果及指导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_尿常规.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_心电图.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_B超正常.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ce_个人基本信息.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_健康档案封面.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ce_复核更新记录.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_血生化.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_血常规.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ccbe_冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbe_健康体检.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton sBut_预览打印;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_血生化;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_血常规;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit ce_尿常规;
        private DevExpress.XtraEditors.CheckEdit ce_心电图;
        private DevExpress.XtraEditors.CheckEdit ce_B超正常;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.CheckEdit ce_个人基本信息;
        private DevExpress.XtraEditors.CheckEdit ce_健康档案封面;
        private DevExpress.XtraEditors.CheckEdit ce_复核更新记录;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbe_高血压;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbe_糖尿病;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbe_脑卒中;
        private DevExpress.XtraEditors.CheckedComboBoxEdit ccbe_冠心病;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_自理能力评估;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_中医药管理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_精神障碍随访;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_精神障碍补充;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.SimpleButton sBut_体检回执单;
        private System.Windows.Forms.ListView listView_重点人群分类;
        private System.Windows.Forms.ImageList imageList_重点人群分类;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lab_姓名;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lab_身份证号;
        private DevExpress.XtraEditors.CheckEdit ce_冠心病;
        private DevExpress.XtraEditors.CheckEdit ce_糖尿病;
        private DevExpress.XtraEditors.CheckEdit ce_脑卒中;
        private DevExpress.XtraEditors.CheckEdit ce_血生化;
        private DevExpress.XtraEditors.CheckEdit ce_高血压;
        private DevExpress.XtraEditors.CheckEdit ce_血常规;
        private DevExpress.XtraEditors.CheckEdit ce_健康体检;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.CheckEdit ce_精神障碍随访;
        private DevExpress.XtraEditors.CheckEdit ce_中医药管理;
        private DevExpress.XtraEditors.CheckEdit ce_精神障碍补充;
        private DevExpress.XtraEditors.CheckEdit ce_自理能力评估;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.SimpleButton sBut_默认;
        private DevExpress.XtraEditors.ComboBoxEdit cbe_健康体检;
        private DevExpress.XtraEditors.CheckEdit ce_回执单封面;
        private DevExpress.XtraEditors.CheckEdit ce_体检结果及指导;
        private DevExpress.XtraEditors.CheckEdit chk高血压随访服务;
        private DevExpress.XtraEditors.CheckEdit chk高危人群干预随访;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit2;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cbe_高危人群干预;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.SimpleButton sBut_B超报告维护;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt个人档案编号;
    }
}