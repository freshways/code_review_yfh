﻿namespace AtomEHR.公共卫生
{
    partial class frm工作提醒
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit个人信息 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucDtpToDtp1 = new AtomEHR.Library.UserControls.UCDtpToDtp();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tab = new DevExpress.XtraTab.XtraTabControl();
            this.tab儿童 = new DevExpress.XtraTab.XtraTabPage();
            this.gc儿童 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv儿童 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link档案编号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab孕产妇 = new DevExpress.XtraTab.XtraTabPage();
            this.gc孕产妇 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv孕产妇 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab老年人 = new DevExpress.XtraTab.XtraTabPage();
            this.gc老年人 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv老年人 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab高血压 = new DevExpress.XtraTab.XtraTabPage();
            this.gc高血压 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv高血压 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab糖尿病 = new DevExpress.XtraTab.XtraTabPage();
            this.gc糖尿病 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv糖尿病 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab精神病 = new DevExpress.XtraTab.XtraTabPage();
            this.gc精神病 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv精神病 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab冠心病 = new DevExpress.XtraTab.XtraTabPage();
            this.gc冠心病 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv冠心病 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab脑卒中 = new DevExpress.XtraTab.XtraTabPage();
            this.gc脑卒中 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv脑卒中 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tab肢体残 = new DevExpress.XtraTab.XtraTabPage();
            this.tab智力残 = new DevExpress.XtraTab.XtraTabPage();
            this.tab视力残 = new DevExpress.XtraTab.XtraTabPage();
            this.tab听力残疾 = new DevExpress.XtraTab.XtraTabPage();
            this.tab健康体检 = new DevExpress.XtraTab.XtraTabPage();
            this.gc健康体检 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv健康体检 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link健康体检 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit个人信息.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab)).BeginInit();
            this.tab.SuspendLayout();
            this.tab儿童.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc儿童)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv儿童)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案编号)).BeginInit();
            this.tab孕产妇.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc孕产妇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv孕产妇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.tab老年人.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            this.tab高血压.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc高血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv高血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            this.tab糖尿病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc糖尿病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv糖尿病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            this.tab精神病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc精神病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv精神病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).BeginInit();
            this.tab冠心病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc冠心病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv冠心病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            this.tab脑卒中.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc脑卒中)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv脑卒中)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).BeginInit();
            this.tab健康体检.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc健康体检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv健康体检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link健康体检)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.comboBoxEdit个人信息);
            this.layoutControl1.Controls.Add(this.ucDtpToDtp1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(922, 59);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(806, 26);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(114, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "查询";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(382, 26);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Size = new System.Drawing.Size(420, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 8;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(82, 26);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Size = new System.Drawing.Size(216, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 7;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(570, 2);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(350, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 6;
            // 
            // comboBoxEdit个人信息
            // 
            this.comboBoxEdit个人信息.EditValue = "请选择";
            this.comboBoxEdit个人信息.Location = new System.Drawing.Point(382, 2);
            this.comboBoxEdit个人信息.Name = "comboBoxEdit个人信息";
            this.comboBoxEdit个人信息.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit个人信息.Properties.Items.AddRange(new object[] {
            "请选择",
            "姓名",
            "档案号",
            "身份证"});
            this.comboBoxEdit个人信息.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit个人信息.Size = new System.Drawing.Size(184, 20);
            this.comboBoxEdit个人信息.StyleController = this.layoutControl1;
            this.comboBoxEdit个人信息.TabIndex = 5;
            // 
            // ucDtpToDtp1
            // 
            this.ucDtpToDtp1.Dte1Size = new System.Drawing.Size(100, 20);
            this.ucDtpToDtp1.Dte2Size = new System.Drawing.Size(100, 20);
            this.ucDtpToDtp1.Location = new System.Drawing.Point(82, 2);
            this.ucDtpToDtp1.Margin = new System.Windows.Forms.Padding(0);
            this.ucDtpToDtp1.Name = "ucDtpToDtp1";
            this.ucDtpToDtp1.Size = new System.Drawing.Size(216, 20);
            this.ucDtpToDtp1.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(922, 59);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ucDtpToDtp1;
            this.layoutControlItem1.CustomizationFormText = "查询时间段";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(300, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "查询时间段：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.comboBoxEdit个人信息;
            this.layoutControlItem2.CustomizationFormText = "个人信息：";
            this.layoutControlItem2.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(268, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "个人信息：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(568, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(354, 24);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.comboBoxEdit镇;
            this.layoutControlItem4.CustomizationFormText = "街道：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(300, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(300, 35);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "街道：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(75, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.comboBoxEdit村;
            this.layoutControlItem5.CustomizationFormText = "居委会：";
            this.layoutControlItem5.Location = new System.Drawing.Point(300, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(504, 35);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "居委会：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(804, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(118, 35);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // tab
            // 
            this.tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab.Location = new System.Drawing.Point(0, 59);
            this.tab.Name = "tab";
            this.tab.SelectedTabPage = this.tab儿童;
            this.tab.Size = new System.Drawing.Size(922, 443);
            this.tab.TabIndex = 1;
            this.tab.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab儿童,
            this.tab孕产妇,
            this.tab老年人,
            this.tab高血压,
            this.tab糖尿病,
            this.tab精神病,
            this.tab冠心病,
            this.tab脑卒中,
            this.tab肢体残,
            this.tab智力残,
            this.tab视力残,
            this.tab听力残疾,
            this.tab健康体检});
            this.tab.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tab_SelectedPageChanged);
            this.tab.TabIndexChanged += new System.EventHandler(this.tab_TabIndexChanged);
            // 
            // tab儿童
            // 
            this.tab儿童.Controls.Add(this.gc儿童);
            this.tab儿童.Name = "tab儿童";
            this.tab儿童.Size = new System.Drawing.Size(916, 414);
            this.tab儿童.Tag = "儿童";
            this.tab儿童.Text = "儿童";
            // 
            // gc儿童
            // 
            this.gc儿童.AllowBandedGridColumnSort = false;
            this.gc儿童.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc儿童.IsBestFitColumns = true;
            this.gc儿童.Location = new System.Drawing.Point(0, 0);
            this.gc儿童.MainView = this.gv儿童;
            this.gc儿童.Name = "gc儿童";
            this.gc儿童.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link档案编号});
            this.gc儿童.ShowContextMenu = false;
            this.gc儿童.Size = new System.Drawing.Size(916, 414);
            this.gc儿童.StrWhere = "";
            this.gc儿童.TabIndex = 0;
            this.gc儿童.UseCheckBox = false;
            this.gc儿童.View = "";
            this.gc儿童.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv儿童});
            // 
            // gv儿童
            // 
            this.gv儿童.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv儿童.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv儿童.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col档案号,
            this.gridColumn2,
            this.gridColumn63,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gv儿童.GridControl = this.gc儿童;
            this.gv儿童.Name = "gv儿童";
            this.gv儿童.OptionsView.ColumnAutoWidth = false;
            this.gv儿童.OptionsView.EnableAppearanceEvenRow = true;
            this.gv儿童.OptionsView.EnableAppearanceOddRow = true;
            this.gv儿童.OptionsView.ShowGroupPanel = false;
            // 
            // col档案号
            // 
            this.col档案号.Caption = "档案编号";
            this.col档案号.ColumnEdit = this.link档案编号;
            this.col档案号.FieldName = "个人档案编号";
            this.col档案号.Name = "col档案号";
            this.col档案号.OptionsColumn.ReadOnly = true;
            this.col档案号.Visible = true;
            this.col档案号.VisibleIndex = 0;
            // 
            // link档案编号
            // 
            this.link档案编号.AutoHeight = false;
            this.link档案编号.Name = "link档案编号";
            this.link档案编号.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "姓名";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "出生日期";
            this.gridColumn63.FieldName = "出生日期";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "性别";
            this.gridColumn3.FieldName = "性别";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "联系电话";
            this.gridColumn4.FieldName = "联系人电话";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "居住地址";
            this.gridColumn5.FieldName = "居住地址";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "随访时间";
            this.gridColumn6.FieldName = "下次随访时间";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "距离天数";
            this.gridColumn7.FieldName = "距离天数";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // tab孕产妇
            // 
            this.tab孕产妇.Controls.Add(this.gc孕产妇);
            this.tab孕产妇.Name = "tab孕产妇";
            this.tab孕产妇.Size = new System.Drawing.Size(916, 414);
            this.tab孕产妇.Tag = "孕产妇";
            this.tab孕产妇.Text = "孕产妇";
            // 
            // gc孕产妇
            // 
            this.gc孕产妇.AllowBandedGridColumnSort = false;
            this.gc孕产妇.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc孕产妇.IsBestFitColumns = true;
            this.gc孕产妇.Location = new System.Drawing.Point(0, 0);
            this.gc孕产妇.MainView = this.gv孕产妇;
            this.gc孕产妇.Name = "gc孕产妇";
            this.gc孕产妇.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc孕产妇.ShowContextMenu = false;
            this.gc孕产妇.Size = new System.Drawing.Size(916, 414);
            this.gc孕产妇.StrWhere = "";
            this.gc孕产妇.TabIndex = 1;
            this.gc孕产妇.UseCheckBox = false;
            this.gc孕产妇.View = "";
            this.gc孕产妇.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv孕产妇});
            // 
            // gv孕产妇
            // 
            this.gv孕产妇.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv孕产妇.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv孕产妇.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gv孕产妇.GridControl = this.gc孕产妇;
            this.gv孕产妇.Name = "gv孕产妇";
            this.gv孕产妇.OptionsView.ColumnAutoWidth = false;
            this.gv孕产妇.OptionsView.EnableAppearanceEvenRow = true;
            this.gv孕产妇.OptionsView.EnableAppearanceOddRow = true;
            this.gv孕产妇.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "档案编号";
            this.gridColumn1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "姓名";
            this.gridColumn8.FieldName = "姓名";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "性别";
            this.gridColumn9.FieldName = "性别";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "联系电话";
            this.gridColumn10.FieldName = "联系人电话";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "居住地址";
            this.gridColumn11.FieldName = "居住地址";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "随访时间";
            this.gridColumn12.FieldName = "下次随访时间";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "距离天数";
            this.gridColumn13.FieldName = "距离天数";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            // 
            // tab老年人
            // 
            this.tab老年人.Controls.Add(this.gc老年人);
            this.tab老年人.Name = "tab老年人";
            this.tab老年人.Size = new System.Drawing.Size(916, 414);
            this.tab老年人.Tag = "老年人";
            this.tab老年人.Text = "老年人";
            // 
            // gc老年人
            // 
            this.gc老年人.AllowBandedGridColumnSort = false;
            this.gc老年人.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc老年人.IsBestFitColumns = true;
            this.gc老年人.Location = new System.Drawing.Point(0, 0);
            this.gc老年人.MainView = this.gv老年人;
            this.gc老年人.Name = "gc老年人";
            this.gc老年人.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit2});
            this.gc老年人.ShowContextMenu = false;
            this.gc老年人.Size = new System.Drawing.Size(916, 414);
            this.gc老年人.StrWhere = "";
            this.gc老年人.TabIndex = 2;
            this.gc老年人.UseCheckBox = false;
            this.gc老年人.View = "";
            this.gc老年人.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv老年人});
            // 
            // gv老年人
            // 
            this.gv老年人.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv老年人.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv老年人.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20});
            this.gv老年人.GridControl = this.gc老年人;
            this.gv老年人.Name = "gv老年人";
            this.gv老年人.OptionsView.ColumnAutoWidth = false;
            this.gv老年人.OptionsView.EnableAppearanceEvenRow = true;
            this.gv老年人.OptionsView.EnableAppearanceOddRow = true;
            this.gv老年人.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "档案编号";
            this.gridColumn14.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn14.FieldName = "个人档案编号";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "姓名";
            this.gridColumn15.FieldName = "姓名";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 1;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "性别";
            this.gridColumn16.FieldName = "性别";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "联系电话";
            this.gridColumn17.FieldName = "联系人电话";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "居住地址";
            this.gridColumn18.FieldName = "居住地址";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "随访时间";
            this.gridColumn19.FieldName = "下次随访时间";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 5;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "距离天数";
            this.gridColumn20.FieldName = "距离天数";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 6;
            // 
            // tab高血压
            // 
            this.tab高血压.Controls.Add(this.gc高血压);
            this.tab高血压.Name = "tab高血压";
            this.tab高血压.Size = new System.Drawing.Size(916, 414);
            this.tab高血压.Tag = "高血压";
            this.tab高血压.Text = "高血压";
            // 
            // gc高血压
            // 
            this.gc高血压.AllowBandedGridColumnSort = false;
            this.gc高血压.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc高血压.IsBestFitColumns = true;
            this.gc高血压.Location = new System.Drawing.Point(0, 0);
            this.gc高血压.MainView = this.gv高血压;
            this.gc高血压.Name = "gc高血压";
            this.gc高血压.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit3});
            this.gc高血压.ShowContextMenu = false;
            this.gc高血压.Size = new System.Drawing.Size(916, 414);
            this.gc高血压.StrWhere = "";
            this.gc高血压.TabIndex = 3;
            this.gc高血压.UseCheckBox = false;
            this.gc高血压.View = "";
            this.gc高血压.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv高血压});
            // 
            // gv高血压
            // 
            this.gv高血压.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv高血压.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv高血压.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27});
            this.gv高血压.GridControl = this.gc高血压;
            this.gv高血压.Name = "gv高血压";
            this.gv高血压.OptionsView.ColumnAutoWidth = false;
            this.gv高血压.OptionsView.EnableAppearanceEvenRow = true;
            this.gv高血压.OptionsView.EnableAppearanceOddRow = true;
            this.gv高血压.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "档案编号";
            this.gridColumn21.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn21.FieldName = "个人档案编号";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "姓名";
            this.gridColumn22.FieldName = "姓名";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "性别";
            this.gridColumn23.FieldName = "性别";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 2;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "联系电话";
            this.gridColumn24.FieldName = "联系人电话";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "居住地址";
            this.gridColumn25.FieldName = "居住地址";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 4;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "随访时间";
            this.gridColumn26.FieldName = "下次随访时间";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 5;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "距离天数";
            this.gridColumn27.FieldName = "距离天数";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 6;
            // 
            // tab糖尿病
            // 
            this.tab糖尿病.Controls.Add(this.gc糖尿病);
            this.tab糖尿病.Name = "tab糖尿病";
            this.tab糖尿病.Size = new System.Drawing.Size(916, 414);
            this.tab糖尿病.Tag = "糖尿病";
            this.tab糖尿病.Text = "糖尿病";
            // 
            // gc糖尿病
            // 
            this.gc糖尿病.AllowBandedGridColumnSort = false;
            this.gc糖尿病.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc糖尿病.IsBestFitColumns = true;
            this.gc糖尿病.Location = new System.Drawing.Point(0, 0);
            this.gc糖尿病.MainView = this.gv糖尿病;
            this.gc糖尿病.Name = "gc糖尿病";
            this.gc糖尿病.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit4});
            this.gc糖尿病.ShowContextMenu = false;
            this.gc糖尿病.Size = new System.Drawing.Size(916, 414);
            this.gc糖尿病.StrWhere = "";
            this.gc糖尿病.TabIndex = 4;
            this.gc糖尿病.UseCheckBox = false;
            this.gc糖尿病.View = "";
            this.gc糖尿病.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv糖尿病});
            // 
            // gv糖尿病
            // 
            this.gv糖尿病.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv糖尿病.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv糖尿病.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34});
            this.gv糖尿病.GridControl = this.gc糖尿病;
            this.gv糖尿病.Name = "gv糖尿病";
            this.gv糖尿病.OptionsView.ColumnAutoWidth = false;
            this.gv糖尿病.OptionsView.EnableAppearanceEvenRow = true;
            this.gv糖尿病.OptionsView.EnableAppearanceOddRow = true;
            this.gv糖尿病.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "档案编号";
            this.gridColumn28.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.gridColumn28.FieldName = "个人档案编号";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "姓名";
            this.gridColumn29.FieldName = "姓名";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 1;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "性别";
            this.gridColumn30.FieldName = "性别";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 2;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "联系电话";
            this.gridColumn31.FieldName = "联系人电话";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 3;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "居住地址";
            this.gridColumn32.FieldName = "居住地址";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 4;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "随访时间";
            this.gridColumn33.FieldName = "下次随访时间";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 5;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "距离天数";
            this.gridColumn34.FieldName = "距离天数";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 6;
            // 
            // tab精神病
            // 
            this.tab精神病.Controls.Add(this.gc精神病);
            this.tab精神病.Name = "tab精神病";
            this.tab精神病.Size = new System.Drawing.Size(916, 414);
            this.tab精神病.Tag = "精神病";
            this.tab精神病.Text = "精神病";
            // 
            // gc精神病
            // 
            this.gc精神病.AllowBandedGridColumnSort = false;
            this.gc精神病.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc精神病.IsBestFitColumns = true;
            this.gc精神病.Location = new System.Drawing.Point(0, 0);
            this.gc精神病.MainView = this.gv精神病;
            this.gc精神病.Name = "gc精神病";
            this.gc精神病.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit5});
            this.gc精神病.ShowContextMenu = false;
            this.gc精神病.Size = new System.Drawing.Size(916, 414);
            this.gc精神病.StrWhere = "";
            this.gc精神病.TabIndex = 5;
            this.gc精神病.UseCheckBox = false;
            this.gc精神病.View = "";
            this.gc精神病.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv精神病});
            // 
            // gv精神病
            // 
            this.gv精神病.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv精神病.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv精神病.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41});
            this.gv精神病.GridControl = this.gc精神病;
            this.gv精神病.Name = "gv精神病";
            this.gv精神病.OptionsView.ColumnAutoWidth = false;
            this.gv精神病.OptionsView.EnableAppearanceEvenRow = true;
            this.gv精神病.OptionsView.EnableAppearanceOddRow = true;
            this.gv精神病.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "档案编号";
            this.gridColumn35.ColumnEdit = this.repositoryItemHyperLinkEdit5;
            this.gridColumn35.FieldName = "个人档案编号";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit5
            // 
            this.repositoryItemHyperLinkEdit5.AutoHeight = false;
            this.repositoryItemHyperLinkEdit5.Name = "repositoryItemHyperLinkEdit5";
            this.repositoryItemHyperLinkEdit5.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "姓名";
            this.gridColumn36.FieldName = "姓名";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 1;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "性别";
            this.gridColumn37.FieldName = "性别";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 2;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "联系电话";
            this.gridColumn38.FieldName = "联系人电话";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 3;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "居住地址";
            this.gridColumn39.FieldName = "居住地址";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 4;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "随访时间";
            this.gridColumn40.FieldName = "下次随访时间";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 5;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "距离天数";
            this.gridColumn41.FieldName = "距离天数";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 6;
            // 
            // tab冠心病
            // 
            this.tab冠心病.Controls.Add(this.gc冠心病);
            this.tab冠心病.Name = "tab冠心病";
            this.tab冠心病.Size = new System.Drawing.Size(916, 414);
            this.tab冠心病.Tag = "冠心病";
            this.tab冠心病.Text = "冠心病";
            // 
            // gc冠心病
            // 
            this.gc冠心病.AllowBandedGridColumnSort = false;
            this.gc冠心病.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc冠心病.IsBestFitColumns = true;
            this.gc冠心病.Location = new System.Drawing.Point(0, 0);
            this.gc冠心病.MainView = this.gv冠心病;
            this.gc冠心病.Name = "gc冠心病";
            this.gc冠心病.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit6});
            this.gc冠心病.ShowContextMenu = false;
            this.gc冠心病.Size = new System.Drawing.Size(916, 414);
            this.gc冠心病.StrWhere = "";
            this.gc冠心病.TabIndex = 6;
            this.gc冠心病.UseCheckBox = false;
            this.gc冠心病.View = "";
            this.gc冠心病.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv冠心病});
            // 
            // gv冠心病
            // 
            this.gv冠心病.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv冠心病.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv冠心病.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48});
            this.gv冠心病.GridControl = this.gc冠心病;
            this.gv冠心病.Name = "gv冠心病";
            this.gv冠心病.OptionsView.ColumnAutoWidth = false;
            this.gv冠心病.OptionsView.EnableAppearanceEvenRow = true;
            this.gv冠心病.OptionsView.EnableAppearanceOddRow = true;
            this.gv冠心病.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "档案编号";
            this.gridColumn42.ColumnEdit = this.repositoryItemHyperLinkEdit6;
            this.gridColumn42.FieldName = "个人档案编号";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "姓名";
            this.gridColumn43.FieldName = "姓名";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 1;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "性别";
            this.gridColumn44.FieldName = "性别";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 2;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "联系电话";
            this.gridColumn45.FieldName = "联系人电话";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 3;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "居住地址";
            this.gridColumn46.FieldName = "居住地址";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 4;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "随访时间";
            this.gridColumn47.FieldName = "下次随访时间";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 5;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "距离天数";
            this.gridColumn48.FieldName = "距离天数";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 6;
            // 
            // tab脑卒中
            // 
            this.tab脑卒中.Controls.Add(this.gc脑卒中);
            this.tab脑卒中.Name = "tab脑卒中";
            this.tab脑卒中.Size = new System.Drawing.Size(916, 414);
            this.tab脑卒中.Tag = "脑卒中";
            this.tab脑卒中.Text = "脑卒中";
            // 
            // gc脑卒中
            // 
            this.gc脑卒中.AllowBandedGridColumnSort = false;
            this.gc脑卒中.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc脑卒中.IsBestFitColumns = true;
            this.gc脑卒中.Location = new System.Drawing.Point(0, 0);
            this.gc脑卒中.MainView = this.gv脑卒中;
            this.gc脑卒中.Name = "gc脑卒中";
            this.gc脑卒中.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit7});
            this.gc脑卒中.ShowContextMenu = false;
            this.gc脑卒中.Size = new System.Drawing.Size(916, 414);
            this.gc脑卒中.StrWhere = "";
            this.gc脑卒中.TabIndex = 6;
            this.gc脑卒中.UseCheckBox = false;
            this.gc脑卒中.View = "";
            this.gc脑卒中.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv脑卒中});
            // 
            // gv脑卒中
            // 
            this.gv脑卒中.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv脑卒中.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv脑卒中.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55});
            this.gv脑卒中.GridControl = this.gc脑卒中;
            this.gv脑卒中.Name = "gv脑卒中";
            this.gv脑卒中.OptionsView.ColumnAutoWidth = false;
            this.gv脑卒中.OptionsView.EnableAppearanceEvenRow = true;
            this.gv脑卒中.OptionsView.EnableAppearanceOddRow = true;
            this.gv脑卒中.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "档案编号";
            this.gridColumn49.ColumnEdit = this.repositoryItemHyperLinkEdit7;
            this.gridColumn49.FieldName = "个人档案编号";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit7
            // 
            this.repositoryItemHyperLinkEdit7.AutoHeight = false;
            this.repositoryItemHyperLinkEdit7.Name = "repositoryItemHyperLinkEdit7";
            this.repositoryItemHyperLinkEdit7.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "姓名";
            this.gridColumn50.FieldName = "姓名";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 1;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "性别";
            this.gridColumn51.FieldName = "性别";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 2;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "联系电话";
            this.gridColumn52.FieldName = "联系人电话";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 3;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "居住地址";
            this.gridColumn53.FieldName = "居住地址";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 4;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "随访时间";
            this.gridColumn54.FieldName = "下次随访时间";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 5;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "距离天数";
            this.gridColumn55.FieldName = "距离天数";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 6;
            // 
            // tab肢体残
            // 
            this.tab肢体残.Name = "tab肢体残";
            this.tab肢体残.PageVisible = false;
            this.tab肢体残.Size = new System.Drawing.Size(916, 414);
            this.tab肢体残.Tag = "肢体残";
            this.tab肢体残.Text = "肢体残";
            // 
            // tab智力残
            // 
            this.tab智力残.Name = "tab智力残";
            this.tab智力残.PageVisible = false;
            this.tab智力残.Size = new System.Drawing.Size(916, 414);
            this.tab智力残.Tag = "智力残";
            this.tab智力残.Text = "智力残";
            // 
            // tab视力残
            // 
            this.tab视力残.Name = "tab视力残";
            this.tab视力残.PageVisible = false;
            this.tab视力残.Size = new System.Drawing.Size(916, 414);
            this.tab视力残.Tag = "视力残";
            this.tab视力残.Text = "视力残";
            // 
            // tab听力残疾
            // 
            this.tab听力残疾.Name = "tab听力残疾";
            this.tab听力残疾.PageVisible = false;
            this.tab听力残疾.Size = new System.Drawing.Size(916, 414);
            this.tab听力残疾.Tag = "听力言语残";
            this.tab听力残疾.Text = "听力言语残";
            // 
            // tab健康体检
            // 
            this.tab健康体检.Controls.Add(this.gc健康体检);
            this.tab健康体检.Name = "tab健康体检";
            this.tab健康体检.PageVisible = false;
            this.tab健康体检.Size = new System.Drawing.Size(916, 414);
            this.tab健康体检.Tag = "健康体检";
            this.tab健康体检.Text = "健康体检";
            // 
            // gc健康体检
            // 
            this.gc健康体检.AllowBandedGridColumnSort = false;
            this.gc健康体检.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc健康体检.IsBestFitColumns = true;
            this.gc健康体检.Location = new System.Drawing.Point(0, 0);
            this.gc健康体检.MainView = this.gv健康体检;
            this.gc健康体检.Name = "gc健康体检";
            this.gc健康体检.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link健康体检});
            this.gc健康体检.ShowContextMenu = false;
            this.gc健康体检.Size = new System.Drawing.Size(916, 414);
            this.gc健康体检.StrWhere = "";
            this.gc健康体检.TabIndex = 1;
            this.gc健康体检.UseCheckBox = false;
            this.gc健康体检.View = "";
            this.gc健康体检.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv健康体检});
            // 
            // gv健康体检
            // 
            this.gv健康体检.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gv健康体检.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv健康体检.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62});
            this.gv健康体检.GridControl = this.gc健康体检;
            this.gv健康体检.Name = "gv健康体检";
            this.gv健康体检.OptionsView.ColumnAutoWidth = false;
            this.gv健康体检.OptionsView.EnableAppearanceEvenRow = true;
            this.gv健康体检.OptionsView.EnableAppearanceOddRow = true;
            this.gv健康体检.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "档案编号";
            this.gridColumn56.ColumnEdit = this.link健康体检;
            this.gridColumn56.FieldName = "个人档案编号";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 0;
            // 
            // link健康体检
            // 
            this.link健康体检.AutoHeight = false;
            this.link健康体检.Name = "link健康体检";
            this.link健康体检.Click += new System.EventHandler(this.link档案编号_Click);
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "姓名";
            this.gridColumn57.FieldName = "姓名";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 1;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "性别";
            this.gridColumn58.FieldName = "性别";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 2;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "联系电话";
            this.gridColumn59.FieldName = "联系人电话";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 3;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "居住地址";
            this.gridColumn60.FieldName = "居住地址";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 4;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "随访时间";
            this.gridColumn61.FieldName = "下次随访时间";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 5;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "距离天数";
            this.gridColumn62.FieldName = "距离天数";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 6;
            // 
            // frm工作提醒
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 502);
            this.Controls.Add(this.tab);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm工作提醒";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "工作提醒";
            this.Load += new System.EventHandler(this.frm工作提醒_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit个人信息.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tab)).EndInit();
            this.tab.ResumeLayout(false);
            this.tab儿童.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc儿童)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv儿童)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link档案编号)).EndInit();
            this.tab孕产妇.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc孕产妇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv孕产妇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.tab老年人.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc老年人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv老年人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            this.tab高血压.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc高血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv高血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            this.tab糖尿病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc糖尿病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv糖尿病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            this.tab精神病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc精神病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv精神病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).EndInit();
            this.tab冠心病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc冠心病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv冠心病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            this.tab脑卒中.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc脑卒中)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv脑卒中)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).EndInit();
            this.tab健康体检.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc健康体检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv健康体检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link健康体检)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private Library.UserControls.UCDtpToDtp ucDtpToDtp1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraTab.XtraTabControl tab;
        private DevExpress.XtraTab.XtraTabPage tab儿童;
        private DevExpress.XtraTab.XtraTabPage tab孕产妇;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit个人信息;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraTab.XtraTabPage tab老年人;
        private DevExpress.XtraTab.XtraTabPage tab高血压;
        private DevExpress.XtraTab.XtraTabPage tab糖尿病;
        private DevExpress.XtraTab.XtraTabPage tab精神病;
        private DevExpress.XtraTab.XtraTabPage tab冠心病;
        private DevExpress.XtraTab.XtraTabPage tab脑卒中;
        private DevExpress.XtraTab.XtraTabPage tab肢体残;
        private DevExpress.XtraTab.XtraTabPage tab智力残;
        private DevExpress.XtraTab.XtraTabPage tab视力残;
        private DevExpress.XtraTab.XtraTabPage tab听力残疾;
        private DevExpress.XtraTab.XtraTabPage tab健康体检;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private Library.UserControls.DataGridControl gc儿童;
        private DevExpress.XtraGrid.Views.Grid.GridView gv儿童;
        private DevExpress.XtraGrid.Columns.GridColumn col档案号;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private Library.UserControls.DataGridControl gc孕产妇;
        private DevExpress.XtraGrid.Views.Grid.GridView gv孕产妇;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private Library.UserControls.DataGridControl gc老年人;
        private DevExpress.XtraGrid.Views.Grid.GridView gv老年人;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private Library.UserControls.DataGridControl gc高血压;
        private DevExpress.XtraGrid.Views.Grid.GridView gv高血压;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private Library.UserControls.DataGridControl gc糖尿病;
        private DevExpress.XtraGrid.Views.Grid.GridView gv糖尿病;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private Library.UserControls.DataGridControl gc精神病;
        private DevExpress.XtraGrid.Views.Grid.GridView gv精神病;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private Library.UserControls.DataGridControl gc冠心病;
        private DevExpress.XtraGrid.Views.Grid.GridView gv冠心病;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private Library.UserControls.DataGridControl gc脑卒中;
        private DevExpress.XtraGrid.Views.Grid.GridView gv脑卒中;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private Library.UserControls.DataGridControl gc健康体检;
        private DevExpress.XtraGrid.Views.Grid.GridView gv健康体检;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link健康体检;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;

    }
}