﻿namespace AtomEHR.公共卫生
{
    partial class frm公共卫生主窗体
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm公共卫生主窗体));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu辖区健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem个人健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem家庭健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem健康体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem接诊记录 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem会诊记录 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem双向转诊 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem体检回执单 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu区域健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem区域健康档案 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu转档管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem转档申请 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem转档查询 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem批量转档 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu基本公共卫生 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu重点人群服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.儿童健康管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童基本信息 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem新生儿家庭访视记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表满月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表3月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表6月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表8月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表12月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表18月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表24月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表30月 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表3岁 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表4岁 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表5岁 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童健康检查记录表6岁 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem儿童入托检查表 = new System.Windows.Forms.ToolStripMenuItem();
            this.妇女健康管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem更年期保健检查表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem妇女保健检查表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem妇女分级管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇健康管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem孕产妇基本信息 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem第一次产前随访服务记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem第二次产前随访服务记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem第三次产前随访服务记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem第四次产前随访服务记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem第五次产前随访服务记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem产后访视记录 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem产后42天健康检查 = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人健康管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem老年人生理自理能力评估表 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem老年人中医药健康管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.残疾人健康管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.残疾人随访服务表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu疾病防控服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu疾病防控 = new System.Windows.Forms.ToolStripMenuItem();
            this.预防接种服务ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu慢性疾病患者管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.高血压患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem高血压患者管理卡 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem高血压患者随访服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.糖尿病患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem糖尿病患者管理卡 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem糖尿病患者随访服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.脑卒中患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem脑卒中患者管理卡 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem脑卒中患者随访服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.冠心病患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem冠心病患者管理卡 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem冠心病患者随访服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem高血压高危干预 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem高血压分级管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu重性精神疾病患者管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem重症精神疾病患者个人信息 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem重症精神疾病患者随访服务表 = new System.Windows.Forms.ToolStripMenuItem();
            this.肺结核患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.患病期间第一次随访ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.患病其他后续随访ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.艾滋病患者管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.个案随访表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.健康教育ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.卫生监督协管ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工作提醒ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu统计分析 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.工作量统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem居民健康统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem个人基本信息统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem健康体检表信息统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem健康体检表统计详细 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem接诊记录表统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem老年人统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem健康人统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem慢病统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem健康人未体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem健康人已体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem高血压未体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem高血压已体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem糖尿病未体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem糖尿病已体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem老年人未体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem老年人已体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem重症精神疾病未体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem重症精神疾病已体检 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem档案复核统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem贫困人口统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.辖区居民管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.高高血压患者管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.糖尿病患者管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.脑卒中管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.冠心病管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.疾病统计分析ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.基本公共卫生ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工作量统计ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康管理统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童基本信息统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem新生儿家庭访视记录统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表满月统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表3月龄统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表6月龄 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表8月龄统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表12月龄统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表18月龄统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表24月龄统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表30月龄 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表3岁统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表4岁统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表5岁统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem儿童健康检查记录表6岁统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.妇女健康管理统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇健康管理ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产妇基本信息统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产1次随访 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产2次随访 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产3次随访 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产4次随访 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem孕产5次随访 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem产后访视记录随访统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem产后42天访视记录随访统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人健康管理统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem老年人随访表统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.重症精神疾病患者管理统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem精神疾病补充表统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem精神疾病随访表统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.慢性病ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem高血压患者管理卡统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem高血压患者随访记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem糖尿病患者管理卡统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem糖尿病患者随访记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem脑卒中患者管理卡统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem脑卒中患者随访记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem冠心病患者管理卡统计 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem冠心病患者随访记录表 = new System.Windows.Forms.ToolStripMenuItem();
            this.残疾人健康管理统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.残疾人康复服务信息统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.居民健康档案统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.个人基本信息统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案复核统计ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.档案动态使用率统计ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.档案重复数据统计ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.管理率统计分析ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.贫困人口体检统计ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.公卫统计汇总ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人统计查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人健康体检统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人月报表ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.慢病统计查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.高血压患者血压控制率ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.糖尿病患者血糖控制率ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.管理率统计分析ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.高血压高危人群达标率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇统计查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇统计分析ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.早孕建册率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.妇保院工作量统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.儿童统计查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.岁儿童统计分析ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.岁儿童系统管理率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.妇保院数据统计儿童ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.常用统计功能ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.岁儿童统计分析ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.岁儿童管理率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇早孕建册率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇统计分析ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.老年人月报表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.慢性病人群统计分析ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.管理率统计分析ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.糖尿病患者血糖控制率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.高血压患者血压控制率ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hIS慢性病查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案动态使用率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案重复数据统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.妇保院数据统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案复核统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.贫困人口体检统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wis联合统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.儿童系统管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕产妇系统管理率统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wis孕妇信息查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wis儿童信息查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.出生数据统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.孕情数据统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.医疗信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.诊疗记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.化验结果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.影像结果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.天士力手表数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu协同办公 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu通知通告管理 = new System.Windows.Forms.ToolStripMenuItem();
            this.解密姓名ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTab工作提醒 = new DevExpress.XtraTab.XtraTabPage();
            this.gc工作提醒 = new DevExpress.XtraGrid.GridControl();
            this.gv工作提醒 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col重点人群 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本季度随访 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col今日随访 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col明日随访 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本周随访 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col未体检 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col已体检 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link未随访 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link今日随访 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link明日随访 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link本周随访 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link未体检 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.link已体检 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTab工作报表 = new DevExpress.XtraTab.XtraTabPage();
            this.link精神疾病随访表统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link重型精神病补充表统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link糖尿病管理卡统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link糖尿病随访统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link高血压随访统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link老年人随访统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link高血压管理卡统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link孕产妇基本信息统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.hyperLinkEdit2 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link儿童基本信息统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.link个人基本信息统计 = new DevExpress.XtraEditors.HyperLinkEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gc待转出档案 = new DevExpress.XtraGrid.GridControl();
            this.gv带转出档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link个人档案编号 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn同意 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn拒绝 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gc自动转出档案 = new DevExpress.XtraGrid.GridControl();
            this.gv自动转出档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gc自动转入档案 = new DevExpress.XtraGrid.GridControl();
            this.gv自动转入档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.linkLabel健康档案 = new System.Windows.Forms.LinkLabel();
            this.linkLabel个人体检 = new System.Windows.Forms.LinkLabel();
            this.linkLabel高血压随访表 = new System.Windows.Forms.LinkLabel();
            this.linkLabel糖尿病随访表 = new System.Windows.Forms.LinkLabel();
            this.linkLabel脑卒中随访表 = new System.Windows.Forms.LinkLabel();
            this.linkLabel冠心病随访表 = new System.Windows.Forms.LinkLabel();
            this.linkLabel新建户主档案 = new System.Windows.Forms.LinkLabel();
            this.linkLabel儿童基本信息 = new System.Windows.Forms.LinkLabel();
            this.linkLabel孕妇基本信息 = new System.Windows.Forms.LinkLabel();
            this.linkLabel老年人生理自理能力评估表 = new System.Windows.Forms.LinkLabel();
            this.linkLabel精神疾病患者随访 = new System.Windows.Forms.LinkLabel();
            this.linkLable转档查询 = new System.Windows.Forms.LinkLabel();
            this.linkLabel转档申请 = new System.Windows.Forms.LinkLabel();
            this.linkLabel高血压分级管理 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTab工作提醒.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc工作提醒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv工作提醒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link未随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link今日随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link明日随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link本周随访)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link未体检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link已体检)).BeginInit();
            this.xtraTab工作报表.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.link精神疾病随访表统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link重型精神病补充表统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link糖尿病管理卡统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link糖尿病随访统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link高血压随访统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link老年人随访统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link高血压管理卡统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link孕产妇基本信息统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link儿童基本信息统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人基本信息统计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc待转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv带转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人档案编号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn同意)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn拒绝)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc自动转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv自动转出档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc自动转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv自动转入档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilReports
            // 
            this.ilReports.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilReports.ImageStream")));
            this.ilReports.Images.SetKeyName(0, "16_ArrayWhite.bmp");
            // 
            // pnlContainer
            // 
            this.pnlContainer.Controls.Add(this.tableLayoutPanel1);
            this.pnlContainer.Controls.Add(this.menuStrip1);
            this.pnlContainer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlContainer.Size = new System.Drawing.Size(990, 471);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu健康档案,
            this.Menu基本公共卫生,
            this.Menu统计分析,
            this.医疗信息ToolStripMenuItem,
            this.Menu协同办公});
            this.menuStrip1.Location = new System.Drawing.Point(2, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(986, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // Menu健康档案
            // 
            this.Menu健康档案.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu辖区健康档案,
            this.Menu区域健康档案,
            this.Menu转档管理});
            this.Menu健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.wall;
            this.Menu健康档案.Name = "Menu健康档案";
            this.Menu健康档案.Size = new System.Drawing.Size(88, 24);
            this.Menu健康档案.Text = "健康档案";
            // 
            // Menu辖区健康档案
            // 
            this.Menu辖区健康档案.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem个人健康档案,
            this.menuItem家庭健康档案,
            this.menuItem健康体检,
            this.menuItem接诊记录,
            this.menuItem会诊记录,
            this.menuItem双向转诊,
            this.menuItem体检回执单});
            this.Menu辖区健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.wallet;
            this.Menu辖区健康档案.Name = "Menu辖区健康档案";
            this.Menu辖区健康档案.Size = new System.Drawing.Size(148, 22);
            this.Menu辖区健康档案.Text = "辖区健康档案";
            // 
            // menuItem个人健康档案
            // 
            this.menuItem个人健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.user;
            this.menuItem个人健康档案.Name = "menuItem个人健康档案";
            this.menuItem个人健康档案.Size = new System.Drawing.Size(148, 22);
            this.menuItem个人健康档案.Text = "个人健康档案";
            this.menuItem个人健康档案.Click += new System.EventHandler(this.menuItem个人健康档案_Click);
            // 
            // menuItem家庭健康档案
            // 
            this.menuItem家庭健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.users;
            this.menuItem家庭健康档案.Name = "menuItem家庭健康档案";
            this.menuItem家庭健康档案.Size = new System.Drawing.Size(148, 22);
            this.menuItem家庭健康档案.Text = "家庭健康档案";
            this.menuItem家庭健康档案.Click += new System.EventHandler(this.Menu家庭健康档案_Click);
            // 
            // menuItem健康体检
            // 
            this.menuItem健康体检.Image = global::AtomEHR.公共卫生.Properties.Resources.umbrella;
            this.menuItem健康体检.Name = "menuItem健康体检";
            this.menuItem健康体检.Size = new System.Drawing.Size(148, 22);
            this.menuItem健康体检.Text = "健康体检";
            this.menuItem健康体检.Click += new System.EventHandler(this.Menu健康体检_Click);
            // 
            // menuItem接诊记录
            // 
            this.menuItem接诊记录.Image = global::AtomEHR.公共卫生.Properties.Resources.arrow_right;
            this.menuItem接诊记录.Name = "menuItem接诊记录";
            this.menuItem接诊记录.Size = new System.Drawing.Size(148, 22);
            this.menuItem接诊记录.Text = "接诊记录";
            this.menuItem接诊记录.Click += new System.EventHandler(this.Menu接诊记录_Click);
            // 
            // menuItem会诊记录
            // 
            this.menuItem会诊记录.Image = global::AtomEHR.公共卫生.Properties.Resources.auction;
            this.menuItem会诊记录.Name = "menuItem会诊记录";
            this.menuItem会诊记录.Size = new System.Drawing.Size(148, 22);
            this.menuItem会诊记录.Text = "会诊记录";
            this.menuItem会诊记录.Click += new System.EventHandler(this.Menu会诊记录_Click);
            // 
            // menuItem双向转诊
            // 
            this.menuItem双向转诊.Image = global::AtomEHR.公共卫生.Properties.Resources.archive;
            this.menuItem双向转诊.Name = "menuItem双向转诊";
            this.menuItem双向转诊.Size = new System.Drawing.Size(148, 22);
            this.menuItem双向转诊.Text = "双向转诊";
            this.menuItem双向转诊.Click += new System.EventHandler(this.Menu双向转诊_Click);
            // 
            // menuItem体检回执单
            // 
            this.menuItem体检回执单.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem体检回执单.Name = "menuItem体检回执单";
            this.menuItem体检回执单.Size = new System.Drawing.Size(148, 22);
            this.menuItem体检回执单.Text = "体检回执单";
            this.menuItem体检回执单.Click += new System.EventHandler(this.menuItem体检回执单_Click);
            // 
            // Menu区域健康档案
            // 
            this.Menu区域健康档案.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem区域健康档案});
            this.Menu区域健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.address_book;
            this.Menu区域健康档案.Name = "Menu区域健康档案";
            this.Menu区域健康档案.Size = new System.Drawing.Size(148, 22);
            this.Menu区域健康档案.Text = "区域健康档案";
            // 
            // menuItem区域健康档案
            // 
            this.menuItem区域健康档案.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem区域健康档案.Name = "menuItem区域健康档案";
            this.menuItem区域健康档案.Size = new System.Drawing.Size(148, 22);
            this.menuItem区域健康档案.Text = "区域健康档案";
            this.menuItem区域健康档案.Click += new System.EventHandler(this.menuItem区域健康档案_Click);
            // 
            // Menu转档管理
            // 
            this.Menu转档管理.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem转档申请,
            this.menuItem转档查询,
            this.menuItem批量转档});
            this.Menu转档管理.Image = global::AtomEHR.公共卫生.Properties.Resources.application;
            this.Menu转档管理.Name = "Menu转档管理";
            this.Menu转档管理.Size = new System.Drawing.Size(148, 22);
            this.Menu转档管理.Text = "转档管理";
            // 
            // menuItem转档申请
            // 
            this.menuItem转档申请.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem转档申请.Name = "menuItem转档申请";
            this.menuItem转档申请.Size = new System.Drawing.Size(124, 22);
            this.menuItem转档申请.Text = "转档申请";
            this.menuItem转档申请.Click += new System.EventHandler(this.Menu转档申请_Click);
            // 
            // menuItem转档查询
            // 
            this.menuItem转档查询.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem转档查询.Name = "menuItem转档查询";
            this.menuItem转档查询.Size = new System.Drawing.Size(124, 22);
            this.menuItem转档查询.Text = "转档查询";
            this.menuItem转档查询.Click += new System.EventHandler(this.Menu转档查询_Click);
            // 
            // menuItem批量转档
            // 
            this.menuItem批量转档.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem批量转档.Name = "menuItem批量转档";
            this.menuItem批量转档.Size = new System.Drawing.Size(124, 22);
            this.menuItem批量转档.Text = "批量转档";
            this.menuItem批量转档.Click += new System.EventHandler(this.menuItem批量转档_Click);
            // 
            // Menu基本公共卫生
            // 
            this.Menu基本公共卫生.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu重点人群服务,
            this.Menu疾病防控服务,
            this.健康教育ToolStripMenuItem,
            this.卫生监督协管ToolStripMenuItem,
            this.工作提醒ToolStripMenuItem});
            this.Menu基本公共卫生.Image = global::AtomEHR.公共卫生.Properties.Resources.documents;
            this.Menu基本公共卫生.Name = "Menu基本公共卫生";
            this.Menu基本公共卫生.Size = new System.Drawing.Size(112, 24);
            this.Menu基本公共卫生.Text = "基本公共卫生";
            // 
            // Menu重点人群服务
            // 
            this.Menu重点人群服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.儿童健康管理ToolStripMenuItem,
            this.妇女健康管理ToolStripMenuItem,
            this.老年人健康管理ToolStripMenuItem,
            this.残疾人健康管理ToolStripMenuItem});
            this.Menu重点人群服务.Image = global::AtomEHR.公共卫生.Properties.Resources.documents;
            this.Menu重点人群服务.Name = "Menu重点人群服务";
            this.Menu重点人群服务.Size = new System.Drawing.Size(148, 22);
            this.Menu重点人群服务.Text = "重点人群服务";
            // 
            // 儿童健康管理ToolStripMenuItem
            // 
            this.儿童健康管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem儿童基本信息,
            this.MenuItem新生儿家庭访视记录表,
            this.MenuItem儿童健康检查记录表满月,
            this.menuItem儿童健康检查记录表3月,
            this.menuItem儿童健康检查记录表6月,
            this.menuItem儿童健康检查记录表8月,
            this.menuItem儿童健康检查记录表12月,
            this.menuItem儿童健康检查记录表18月,
            this.menuItem儿童健康检查记录表24月,
            this.menuItem儿童健康检查记录表30月,
            this.menuItem儿童健康检查记录表3岁,
            this.menuItem儿童健康检查记录表4岁,
            this.menuItem儿童健康检查记录表5岁,
            this.menuItem儿童健康检查记录表6岁,
            this.menuItem儿童入托检查表});
            this.儿童健康管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.儿童健康管理ToolStripMenuItem.Name = "儿童健康管理ToolStripMenuItem";
            this.儿童健康管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.儿童健康管理ToolStripMenuItem.Text = "儿童健康管理";
            // 
            // MenuItem儿童基本信息
            // 
            this.MenuItem儿童基本信息.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.MenuItem儿童基本信息.Name = "MenuItem儿童基本信息";
            this.MenuItem儿童基本信息.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童基本信息.Text = "儿童基本信息";
            this.MenuItem儿童基本信息.Click += new System.EventHandler(this.MenuItem儿童基本信息_Click);
            // 
            // MenuItem新生儿家庭访视记录表
            // 
            this.MenuItem新生儿家庭访视记录表.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem新生儿家庭访视记录表.Image")));
            this.MenuItem新生儿家庭访视记录表.Name = "MenuItem新生儿家庭访视记录表";
            this.MenuItem新生儿家庭访视记录表.Size = new System.Drawing.Size(230, 22);
            this.MenuItem新生儿家庭访视记录表.Text = "新生儿家庭访视记录表";
            this.MenuItem新生儿家庭访视记录表.Click += new System.EventHandler(this.MenuItem新生儿家庭访视记录表_Click);
            // 
            // MenuItem儿童健康检查记录表满月
            // 
            this.MenuItem儿童健康检查记录表满月.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem儿童健康检查记录表满月.Image")));
            this.MenuItem儿童健康检查记录表满月.Name = "MenuItem儿童健康检查记录表满月";
            this.MenuItem儿童健康检查记录表满月.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表满月.Text = "儿童健康检查记录表(满月)";
            this.MenuItem儿童健康检查记录表满月.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表满月_Click);
            // 
            // menuItem儿童健康检查记录表3月
            // 
            this.menuItem儿童健康检查记录表3月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表3月.Image")));
            this.menuItem儿童健康检查记录表3月.Name = "menuItem儿童健康检查记录表3月";
            this.menuItem儿童健康检查记录表3月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表3月.Text = "儿童健康检查记录表(3月龄)";
            this.menuItem儿童健康检查记录表3月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表3月_Click);
            // 
            // menuItem儿童健康检查记录表6月
            // 
            this.menuItem儿童健康检查记录表6月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表6月.Image")));
            this.menuItem儿童健康检查记录表6月.Name = "menuItem儿童健康检查记录表6月";
            this.menuItem儿童健康检查记录表6月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表6月.Text = "儿童健康检查记录表(6月龄)";
            this.menuItem儿童健康检查记录表6月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表6月_Click);
            // 
            // menuItem儿童健康检查记录表8月
            // 
            this.menuItem儿童健康检查记录表8月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表8月.Image")));
            this.menuItem儿童健康检查记录表8月.Name = "menuItem儿童健康检查记录表8月";
            this.menuItem儿童健康检查记录表8月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表8月.Text = "儿童健康检查记录表(8月龄)";
            this.menuItem儿童健康检查记录表8月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表8月_Click);
            // 
            // menuItem儿童健康检查记录表12月
            // 
            this.menuItem儿童健康检查记录表12月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表12月.Image")));
            this.menuItem儿童健康检查记录表12月.Name = "menuItem儿童健康检查记录表12月";
            this.menuItem儿童健康检查记录表12月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表12月.Text = "儿童健康检查记录表(12月龄)";
            this.menuItem儿童健康检查记录表12月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表12月_Click);
            // 
            // menuItem儿童健康检查记录表18月
            // 
            this.menuItem儿童健康检查记录表18月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表18月.Image")));
            this.menuItem儿童健康检查记录表18月.Name = "menuItem儿童健康检查记录表18月";
            this.menuItem儿童健康检查记录表18月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表18月.Text = "儿童健康检查记录表(18月龄)";
            this.menuItem儿童健康检查记录表18月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表18月_Click);
            // 
            // menuItem儿童健康检查记录表24月
            // 
            this.menuItem儿童健康检查记录表24月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表24月.Image")));
            this.menuItem儿童健康检查记录表24月.Name = "menuItem儿童健康检查记录表24月";
            this.menuItem儿童健康检查记录表24月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表24月.Text = "儿童健康检查记录表(24月龄)";
            this.menuItem儿童健康检查记录表24月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表24月_Click);
            // 
            // menuItem儿童健康检查记录表30月
            // 
            this.menuItem儿童健康检查记录表30月.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表30月.Image")));
            this.menuItem儿童健康检查记录表30月.Name = "menuItem儿童健康检查记录表30月";
            this.menuItem儿童健康检查记录表30月.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表30月.Text = "儿童健康检查记录表(30月龄)";
            this.menuItem儿童健康检查记录表30月.Click += new System.EventHandler(this.menuItem儿童健康检查记录表30月_Click);
            // 
            // menuItem儿童健康检查记录表3岁
            // 
            this.menuItem儿童健康检查记录表3岁.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表3岁.Image")));
            this.menuItem儿童健康检查记录表3岁.Name = "menuItem儿童健康检查记录表3岁";
            this.menuItem儿童健康检查记录表3岁.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表3岁.Text = "儿童健康检查记录表(3岁)";
            this.menuItem儿童健康检查记录表3岁.Click += new System.EventHandler(this.menuItem儿童健康检查记录表3岁_Click);
            // 
            // menuItem儿童健康检查记录表4岁
            // 
            this.menuItem儿童健康检查记录表4岁.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表4岁.Image")));
            this.menuItem儿童健康检查记录表4岁.Name = "menuItem儿童健康检查记录表4岁";
            this.menuItem儿童健康检查记录表4岁.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表4岁.Text = "儿童健康检查记录表(4岁)";
            this.menuItem儿童健康检查记录表4岁.Click += new System.EventHandler(this.menuItem儿童健康检查记录表4岁_Click);
            // 
            // menuItem儿童健康检查记录表5岁
            // 
            this.menuItem儿童健康检查记录表5岁.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表5岁.Image")));
            this.menuItem儿童健康检查记录表5岁.Name = "menuItem儿童健康检查记录表5岁";
            this.menuItem儿童健康检查记录表5岁.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表5岁.Text = "儿童健康检查记录表(5岁)";
            this.menuItem儿童健康检查记录表5岁.Click += new System.EventHandler(this.menuItem儿童健康检查记录表5岁_Click);
            // 
            // menuItem儿童健康检查记录表6岁
            // 
            this.menuItem儿童健康检查记录表6岁.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童健康检查记录表6岁.Image")));
            this.menuItem儿童健康检查记录表6岁.Name = "menuItem儿童健康检查记录表6岁";
            this.menuItem儿童健康检查记录表6岁.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童健康检查记录表6岁.Text = "儿童健康检查记录表(6岁)";
            this.menuItem儿童健康检查记录表6岁.Click += new System.EventHandler(this.menuItem儿童健康检查记录表6岁_Click);
            // 
            // menuItem儿童入托检查表
            // 
            this.menuItem儿童入托检查表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem儿童入托检查表.Image")));
            this.menuItem儿童入托检查表.Name = "menuItem儿童入托检查表";
            this.menuItem儿童入托检查表.Size = new System.Drawing.Size(230, 22);
            this.menuItem儿童入托检查表.Text = "儿童入托检查表";
            this.menuItem儿童入托检查表.Click += new System.EventHandler(this.menuItem儿童入托检查表_Click);
            // 
            // 妇女健康管理ToolStripMenuItem
            // 
            this.妇女健康管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem更年期保健检查表,
            this.menuItem妇女保健检查表,
            this.menuItem妇女分级管理,
            this.孕产妇健康管理ToolStripMenuItem});
            this.妇女健康管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.妇女健康管理ToolStripMenuItem.Name = "妇女健康管理ToolStripMenuItem";
            this.妇女健康管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.妇女健康管理ToolStripMenuItem.Text = "妇女健康管理";
            // 
            // menuItem更年期保健检查表
            // 
            this.menuItem更年期保健检查表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem更年期保健检查表.Image")));
            this.menuItem更年期保健检查表.Name = "menuItem更年期保健检查表";
            this.menuItem更年期保健检查表.Size = new System.Drawing.Size(172, 22);
            this.menuItem更年期保健检查表.Text = "更年期保健检查表";
            this.menuItem更年期保健检查表.Click += new System.EventHandler(this.menuItem更年期保健检查表_Click);
            // 
            // menuItem妇女保健检查表
            // 
            this.menuItem妇女保健检查表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem妇女保健检查表.Image")));
            this.menuItem妇女保健检查表.Name = "menuItem妇女保健检查表";
            this.menuItem妇女保健检查表.Size = new System.Drawing.Size(172, 22);
            this.menuItem妇女保健检查表.Text = "妇女保健检查表";
            this.menuItem妇女保健检查表.Click += new System.EventHandler(this.menuItem妇女保健检查表_Click);
            // 
            // menuItem妇女分级管理
            // 
            this.menuItem妇女分级管理.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.menuItem妇女分级管理.Name = "menuItem妇女分级管理";
            this.menuItem妇女分级管理.Size = new System.Drawing.Size(172, 22);
            this.menuItem妇女分级管理.Text = "妇女分级管理";
            this.menuItem妇女分级管理.Click += new System.EventHandler(this.menuItem妇女分级管理_Click);
            // 
            // 孕产妇健康管理ToolStripMenuItem
            // 
            this.孕产妇健康管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem孕产妇基本信息,
            this.menuItem第一次产前随访服务记录表,
            this.menuItem第二次产前随访服务记录表,
            this.menuItem第三次产前随访服务记录表,
            this.menuItem第四次产前随访服务记录表,
            this.menuItem第五次产前随访服务记录表,
            this.menuItem产后访视记录,
            this.menuItem产后42天健康检查});
            this.孕产妇健康管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.孕产妇健康管理ToolStripMenuItem.Name = "孕产妇健康管理ToolStripMenuItem";
            this.孕产妇健康管理ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.孕产妇健康管理ToolStripMenuItem.Text = "孕产妇健康管理";
            // 
            // menuItem孕产妇基本信息
            // 
            this.menuItem孕产妇基本信息.Image = ((System.Drawing.Image)(resources.GetObject("menuItem孕产妇基本信息.Image")));
            this.menuItem孕产妇基本信息.Name = "menuItem孕产妇基本信息";
            this.menuItem孕产妇基本信息.Size = new System.Drawing.Size(220, 22);
            this.menuItem孕产妇基本信息.Text = "孕产妇基本信息";
            this.menuItem孕产妇基本信息.Click += new System.EventHandler(this.menuItem孕产妇基本信息_Click);
            // 
            // menuItem第一次产前随访服务记录表
            // 
            this.menuItem第一次产前随访服务记录表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem第一次产前随访服务记录表.Image")));
            this.menuItem第一次产前随访服务记录表.Name = "menuItem第一次产前随访服务记录表";
            this.menuItem第一次产前随访服务记录表.Size = new System.Drawing.Size(220, 22);
            this.menuItem第一次产前随访服务记录表.Text = "第一次产前随访服务记录表";
            this.menuItem第一次产前随访服务记录表.Click += new System.EventHandler(this.menuItem第一次产前随访服务记录表_Click);
            // 
            // menuItem第二次产前随访服务记录表
            // 
            this.menuItem第二次产前随访服务记录表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem第二次产前随访服务记录表.Image")));
            this.menuItem第二次产前随访服务记录表.Name = "menuItem第二次产前随访服务记录表";
            this.menuItem第二次产前随访服务记录表.Size = new System.Drawing.Size(220, 22);
            this.menuItem第二次产前随访服务记录表.Text = "第二次产前随访服务记录表";
            this.menuItem第二次产前随访服务记录表.Click += new System.EventHandler(this.menuItem第二次产前随访服务记录表_Click);
            // 
            // menuItem第三次产前随访服务记录表
            // 
            this.menuItem第三次产前随访服务记录表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem第三次产前随访服务记录表.Image")));
            this.menuItem第三次产前随访服务记录表.Name = "menuItem第三次产前随访服务记录表";
            this.menuItem第三次产前随访服务记录表.Size = new System.Drawing.Size(220, 22);
            this.menuItem第三次产前随访服务记录表.Text = "第三次产前随访服务记录表";
            this.menuItem第三次产前随访服务记录表.Click += new System.EventHandler(this.menuItem第三次产前随访服务记录表_Click);
            // 
            // menuItem第四次产前随访服务记录表
            // 
            this.menuItem第四次产前随访服务记录表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem第四次产前随访服务记录表.Image")));
            this.menuItem第四次产前随访服务记录表.Name = "menuItem第四次产前随访服务记录表";
            this.menuItem第四次产前随访服务记录表.Size = new System.Drawing.Size(220, 22);
            this.menuItem第四次产前随访服务记录表.Text = "第四次产前随访服务记录表";
            this.menuItem第四次产前随访服务记录表.Click += new System.EventHandler(this.menuItem第四次产前随访服务记录表_Click);
            // 
            // menuItem第五次产前随访服务记录表
            // 
            this.menuItem第五次产前随访服务记录表.Image = ((System.Drawing.Image)(resources.GetObject("menuItem第五次产前随访服务记录表.Image")));
            this.menuItem第五次产前随访服务记录表.Name = "menuItem第五次产前随访服务记录表";
            this.menuItem第五次产前随访服务记录表.Size = new System.Drawing.Size(220, 22);
            this.menuItem第五次产前随访服务记录表.Text = "第五次产前随访服务记录表";
            this.menuItem第五次产前随访服务记录表.Click += new System.EventHandler(this.menuItem第五次产前随访服务记录表_Click);
            // 
            // menuItem产后访视记录
            // 
            this.menuItem产后访视记录.Image = ((System.Drawing.Image)(resources.GetObject("menuItem产后访视记录.Image")));
            this.menuItem产后访视记录.Name = "menuItem产后访视记录";
            this.menuItem产后访视记录.Size = new System.Drawing.Size(220, 22);
            this.menuItem产后访视记录.Text = "产后访视记录";
            this.menuItem产后访视记录.Click += new System.EventHandler(this.menuItem产后访视记录_Click);
            // 
            // menuItem产后42天健康检查
            // 
            this.menuItem产后42天健康检查.Image = ((System.Drawing.Image)(resources.GetObject("menuItem产后42天健康检查.Image")));
            this.menuItem产后42天健康检查.Name = "menuItem产后42天健康检查";
            this.menuItem产后42天健康检查.Size = new System.Drawing.Size(220, 22);
            this.menuItem产后42天健康检查.Text = "产后42天健康检查";
            this.menuItem产后42天健康检查.Click += new System.EventHandler(this.menuItem产后42天健康检查_Click);
            // 
            // 老年人健康管理ToolStripMenuItem
            // 
            this.老年人健康管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem老年人生理自理能力评估表,
            this.menuItem老年人中医药健康管理});
            this.老年人健康管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.老年人健康管理ToolStripMenuItem.Name = "老年人健康管理ToolStripMenuItem";
            this.老年人健康管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.老年人健康管理ToolStripMenuItem.Text = "老年人健康管理";
            // 
            // menuItem老年人生理自理能力评估表
            // 
            this.menuItem老年人生理自理能力评估表.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem老年人生理自理能力评估表.Name = "menuItem老年人生理自理能力评估表";
            this.menuItem老年人生理自理能力评估表.Size = new System.Drawing.Size(220, 22);
            this.menuItem老年人生理自理能力评估表.Text = "老年人生理自理能力评估表";
            this.menuItem老年人生理自理能力评估表.Click += new System.EventHandler(this.menuItem老年人生理自理能力评估表_Click);
            // 
            // menuItem老年人中医药健康管理
            // 
            this.menuItem老年人中医药健康管理.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem老年人中医药健康管理.Name = "menuItem老年人中医药健康管理";
            this.menuItem老年人中医药健康管理.Size = new System.Drawing.Size(220, 22);
            this.menuItem老年人中医药健康管理.Text = "老年人中医药健康管理";
            this.menuItem老年人中医药健康管理.Click += new System.EventHandler(this.menuItem老年人中医药健康管理_Click);
            // 
            // 残疾人健康管理ToolStripMenuItem
            // 
            this.残疾人健康管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.残疾人随访服务表ToolStripMenuItem});
            this.残疾人健康管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.残疾人健康管理ToolStripMenuItem.Name = "残疾人健康管理ToolStripMenuItem";
            this.残疾人健康管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.残疾人健康管理ToolStripMenuItem.Text = "残疾人健康管理";
            // 
            // 残疾人随访服务表ToolStripMenuItem
            // 
            this.残疾人随访服务表ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.残疾人随访服务表ToolStripMenuItem.Name = "残疾人随访服务表ToolStripMenuItem";
            this.残疾人随访服务表ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.残疾人随访服务表ToolStripMenuItem.Text = "残疾人随访服务表";
            this.残疾人随访服务表ToolStripMenuItem.Click += new System.EventHandler(this.残疾人随访服务表ToolStripMenuItem_Click);
            // 
            // Menu疾病防控服务
            // 
            this.Menu疾病防控服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu疾病防控,
            this.Menu慢性疾病患者管理,
            this.Menu重性精神疾病患者管理,
            this.肺结核患者管理ToolStripMenuItem,
            this.艾滋病患者管理ToolStripMenuItem});
            this.Menu疾病防控服务.Image = global::AtomEHR.公共卫生.Properties.Resources.documents;
            this.Menu疾病防控服务.Name = "Menu疾病防控服务";
            this.Menu疾病防控服务.Size = new System.Drawing.Size(148, 22);
            this.Menu疾病防控服务.Text = "疾病防控服务";
            // 
            // Menu疾病防控
            // 
            this.Menu疾病防控.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.预防接种服务ToolStripMenuItem});
            this.Menu疾病防控.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.Menu疾病防控.Name = "Menu疾病防控";
            this.Menu疾病防控.Size = new System.Drawing.Size(196, 22);
            this.Menu疾病防控.Text = "疾病防控服务";
            // 
            // 预防接种服务ToolStripMenuItem
            // 
            this.预防接种服务ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.预防接种服务ToolStripMenuItem.Name = "预防接种服务ToolStripMenuItem";
            this.预防接种服务ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.预防接种服务ToolStripMenuItem.Text = "预防接种服务";
            // 
            // Menu慢性疾病患者管理
            // 
            this.Menu慢性疾病患者管理.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.高血压患者管理ToolStripMenuItem,
            this.糖尿病患者管理ToolStripMenuItem,
            this.脑卒中患者管理ToolStripMenuItem,
            this.冠心病患者管理ToolStripMenuItem,
            this.menuItem高血压高危干预,
            this.menuItem高血压分级管理});
            this.Menu慢性疾病患者管理.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.Menu慢性疾病患者管理.Name = "Menu慢性疾病患者管理";
            this.Menu慢性疾病患者管理.Size = new System.Drawing.Size(196, 22);
            this.Menu慢性疾病患者管理.Text = "慢性疾病患者管理";
            // 
            // 高血压患者管理ToolStripMenuItem
            // 
            this.高血压患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem高血压患者管理卡,
            this.menuItem高血压患者随访服务});
            this.高血压患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.高血压患者管理ToolStripMenuItem.Name = "高血压患者管理ToolStripMenuItem";
            this.高血压患者管理ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.高血压患者管理ToolStripMenuItem.Text = "高血压患者管理";
            // 
            // menuItem高血压患者管理卡
            // 
            this.menuItem高血压患者管理卡.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem高血压患者管理卡.Name = "menuItem高血压患者管理卡";
            this.menuItem高血压患者管理卡.Size = new System.Drawing.Size(220, 22);
            this.menuItem高血压患者管理卡.Text = "高血压患者管理卡";
            this.menuItem高血压患者管理卡.Click += new System.EventHandler(this.menuItem高血压患者管理卡_Click);
            // 
            // menuItem高血压患者随访服务
            // 
            this.menuItem高血压患者随访服务.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem高血压患者随访服务.Name = "menuItem高血压患者随访服务";
            this.menuItem高血压患者随访服务.Size = new System.Drawing.Size(220, 22);
            this.menuItem高血压患者随访服务.Text = "高血压患者随访服务记录表";
            this.menuItem高血压患者随访服务.Click += new System.EventHandler(this.menuItem高血压患者随访服务_Click);
            // 
            // 糖尿病患者管理ToolStripMenuItem
            // 
            this.糖尿病患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem糖尿病患者管理卡,
            this.menuItem糖尿病患者随访服务});
            this.糖尿病患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.糖尿病患者管理ToolStripMenuItem.Name = "糖尿病患者管理ToolStripMenuItem";
            this.糖尿病患者管理ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.糖尿病患者管理ToolStripMenuItem.Text = "糖尿病患者管理";
            // 
            // menuItem糖尿病患者管理卡
            // 
            this.menuItem糖尿病患者管理卡.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem糖尿病患者管理卡.Name = "menuItem糖尿病患者管理卡";
            this.menuItem糖尿病患者管理卡.Size = new System.Drawing.Size(220, 22);
            this.menuItem糖尿病患者管理卡.Text = "糖尿病患者管理卡";
            this.menuItem糖尿病患者管理卡.Click += new System.EventHandler(this.糖尿病患者管理卡ToolStripMenuItem_Click);
            // 
            // menuItem糖尿病患者随访服务
            // 
            this.menuItem糖尿病患者随访服务.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem糖尿病患者随访服务.Name = "menuItem糖尿病患者随访服务";
            this.menuItem糖尿病患者随访服务.Size = new System.Drawing.Size(220, 22);
            this.menuItem糖尿病患者随访服务.Text = "糖尿病患者随访服务记录表";
            this.menuItem糖尿病患者随访服务.Click += new System.EventHandler(this.menuItem糖尿病患者随访服务_Click);
            // 
            // 脑卒中患者管理ToolStripMenuItem
            // 
            this.脑卒中患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem脑卒中患者管理卡,
            this.menuItem脑卒中患者随访服务});
            this.脑卒中患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.脑卒中患者管理ToolStripMenuItem.Name = "脑卒中患者管理ToolStripMenuItem";
            this.脑卒中患者管理ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.脑卒中患者管理ToolStripMenuItem.Text = "脑卒中患者管理*";
            // 
            // menuItem脑卒中患者管理卡
            // 
            this.menuItem脑卒中患者管理卡.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem脑卒中患者管理卡.Name = "menuItem脑卒中患者管理卡";
            this.menuItem脑卒中患者管理卡.Size = new System.Drawing.Size(213, 22);
            this.menuItem脑卒中患者管理卡.Text = "脑卒中患者管理卡*";
            this.menuItem脑卒中患者管理卡.Click += new System.EventHandler(this.menuItem脑卒中患者管理卡_Click);
            // 
            // menuItem脑卒中患者随访服务
            // 
            this.menuItem脑卒中患者随访服务.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem脑卒中患者随访服务.Name = "menuItem脑卒中患者随访服务";
            this.menuItem脑卒中患者随访服务.Size = new System.Drawing.Size(213, 22);
            this.menuItem脑卒中患者随访服务.Text = "脑卒中患者随访服务记录*";
            this.menuItem脑卒中患者随访服务.Click += new System.EventHandler(this.menuItem脑卒中患者随访服务_Click);
            // 
            // 冠心病患者管理ToolStripMenuItem
            // 
            this.冠心病患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem冠心病患者管理卡,
            this.menuItem冠心病患者随访服务});
            this.冠心病患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.冠心病患者管理ToolStripMenuItem.Name = "冠心病患者管理ToolStripMenuItem";
            this.冠心病患者管理ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.冠心病患者管理ToolStripMenuItem.Text = "冠心病患者管理*";
            // 
            // menuItem冠心病患者管理卡
            // 
            this.menuItem冠心病患者管理卡.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem冠心病患者管理卡.Name = "menuItem冠心病患者管理卡";
            this.menuItem冠心病患者管理卡.Size = new System.Drawing.Size(208, 22);
            this.menuItem冠心病患者管理卡.Text = "冠心病患者管理卡";
            this.menuItem冠心病患者管理卡.Click += new System.EventHandler(this.menuItem冠心病患者管理卡_Click);
            // 
            // menuItem冠心病患者随访服务
            // 
            this.menuItem冠心病患者随访服务.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.menuItem冠心病患者随访服务.Name = "menuItem冠心病患者随访服务";
            this.menuItem冠心病患者随访服务.Size = new System.Drawing.Size(208, 22);
            this.menuItem冠心病患者随访服务.Text = "冠心病患者随访服务记录";
            this.menuItem冠心病患者随访服务.Click += new System.EventHandler(this.menuItem冠心病患者随访服务_Click);
            // 
            // menuItem高血压高危干预
            // 
            this.menuItem高血压高危干预.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.menuItem高血压高危干预.Name = "menuItem高血压高危干预";
            this.menuItem高血压高危干预.Size = new System.Drawing.Size(184, 22);
            this.menuItem高血压高危干预.Text = "高血压高危干预管理";
            this.menuItem高血压高危干预.Click += new System.EventHandler(this.menuItem高血压高危干预_Click);
            // 
            // menuItem高血压分级管理
            // 
            this.menuItem高血压分级管理.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.menuItem高血压分级管理.Name = "menuItem高血压分级管理";
            this.menuItem高血压分级管理.Size = new System.Drawing.Size(184, 22);
            this.menuItem高血压分级管理.Text = "高血压分级管理";
            this.menuItem高血压分级管理.Click += new System.EventHandler(this.menuItem高血压分级管理_Click);
            // 
            // Menu重性精神疾病患者管理
            // 
            this.Menu重性精神疾病患者管理.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem重症精神疾病患者个人信息,
            this.menuItem重症精神疾病患者随访服务表});
            this.Menu重性精神疾病患者管理.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.Menu重性精神疾病患者管理.Name = "Menu重性精神疾病患者管理";
            this.Menu重性精神疾病患者管理.Size = new System.Drawing.Size(196, 22);
            this.Menu重性精神疾病患者管理.Text = "重性精神疾病患者管理";
            // 
            // menuItem重症精神疾病患者个人信息
            // 
            this.menuItem重症精神疾病患者个人信息.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem重症精神疾病患者个人信息.Name = "menuItem重症精神疾病患者个人信息";
            this.menuItem重症精神疾病患者个人信息.Size = new System.Drawing.Size(232, 22);
            this.menuItem重症精神疾病患者个人信息.Text = "重症精神疾病患者基本信息表";
            this.menuItem重症精神疾病患者个人信息.Click += new System.EventHandler(this.menuItem重症精神疾病患者个人信息_Click);
            // 
            // menuItem重症精神疾病患者随访服务表
            // 
            this.menuItem重症精神疾病患者随访服务表.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.menuItem重症精神疾病患者随访服务表.Name = "menuItem重症精神疾病患者随访服务表";
            this.menuItem重症精神疾病患者随访服务表.Size = new System.Drawing.Size(232, 22);
            this.menuItem重症精神疾病患者随访服务表.Text = "重症精神疾病患者随访服务表";
            this.menuItem重症精神疾病患者随访服务表.Click += new System.EventHandler(this.menuItem重症精神疾病患者随访服务表_Click);
            // 
            // 肺结核患者管理ToolStripMenuItem
            // 
            this.肺结核患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.患病期间第一次随访ToolStripMenuItem,
            this.患病其他后续随访ToolStripMenuItem});
            this.肺结核患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.肺结核患者管理ToolStripMenuItem.Name = "肺结核患者管理ToolStripMenuItem";
            this.肺结核患者管理ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.肺结核患者管理ToolStripMenuItem.Text = "肺结核患者管理";
            // 
            // 患病期间第一次随访ToolStripMenuItem
            // 
            this.患病期间第一次随访ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.患病期间第一次随访ToolStripMenuItem.Name = "患病期间第一次随访ToolStripMenuItem";
            this.患病期间第一次随访ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.患病期间第一次随访ToolStripMenuItem.Text = "患病期间第一次随访";
            this.患病期间第一次随访ToolStripMenuItem.Click += new System.EventHandler(this.患病期间第一次随访ToolStripMenuItem_Click);
            // 
            // 患病其他后续随访ToolStripMenuItem
            // 
            this.患病其他后续随访ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.患病其他后续随访ToolStripMenuItem.Name = "患病其他后续随访ToolStripMenuItem";
            this.患病其他后续随访ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.患病其他后续随访ToolStripMenuItem.Text = "患病期间后续随访";
            this.患病其他后续随访ToolStripMenuItem.Click += new System.EventHandler(this.患病其他后续随访ToolStripMenuItem_Click);
            // 
            // 艾滋病患者管理ToolStripMenuItem
            // 
            this.艾滋病患者管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.个案随访表ToolStripMenuItem});
            this.艾滋病患者管理ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.艾滋病患者管理ToolStripMenuItem.Name = "艾滋病患者管理ToolStripMenuItem";
            this.艾滋病患者管理ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.艾滋病患者管理ToolStripMenuItem.Text = "艾滋病患者管理";
            // 
            // 个案随访表ToolStripMenuItem
            // 
            this.个案随访表ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document_edit;
            this.个案随访表ToolStripMenuItem.Name = "个案随访表ToolStripMenuItem";
            this.个案随访表ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.个案随访表ToolStripMenuItem.Text = "个案随访表";
            this.个案随访表ToolStripMenuItem.Click += new System.EventHandler(this.个案随访表ToolStripMenuItem_Click);
            // 
            // 健康教育ToolStripMenuItem
            // 
            this.健康教育ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("健康教育ToolStripMenuItem.Image")));
            this.健康教育ToolStripMenuItem.Name = "健康教育ToolStripMenuItem";
            this.健康教育ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.健康教育ToolStripMenuItem.Text = "健康教育";
            // 
            // 卫生监督协管ToolStripMenuItem
            // 
            this.卫生监督协管ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("卫生监督协管ToolStripMenuItem.Image")));
            this.卫生监督协管ToolStripMenuItem.Name = "卫生监督协管ToolStripMenuItem";
            this.卫生监督协管ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.卫生监督协管ToolStripMenuItem.Text = "卫生监督协管";
            // 
            // 工作提醒ToolStripMenuItem
            // 
            this.工作提醒ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("工作提醒ToolStripMenuItem.Image")));
            this.工作提醒ToolStripMenuItem.Name = "工作提醒ToolStripMenuItem";
            this.工作提醒ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.工作提醒ToolStripMenuItem.Text = "工作提醒";
            // 
            // Menu统计分析
            // 
            this.Menu统计分析.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.基本公共卫生ToolStripMenuItem,
            this.常用统计功能ToolStripMenuItem,
            this.wis联合统计ToolStripMenuItem});
            this.Menu统计分析.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.Menu统计分析.Name = "Menu统计分析";
            this.Menu统计分析.Size = new System.Drawing.Size(88, 24);
            this.Menu统计分析.Text = "统计分析";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.工作量统计ToolStripMenuItem});
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(156, 26);
            this.toolStripMenuItem3.Text = "健康档案";
            // 
            // 工作量统计ToolStripMenuItem
            // 
            this.工作量统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem居民健康统计,
            this.管理率统计ToolStripMenuItem});
            this.工作量统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.工作量统计ToolStripMenuItem.Name = "工作量统计ToolStripMenuItem";
            this.工作量统计ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.工作量统计ToolStripMenuItem.Text = "工作量统计";
            // 
            // MenuItem居民健康统计
            // 
            this.MenuItem居民健康统计.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem个人基本信息统计,
            this.MenuItem健康体检表信息统计,
            this.MenuItem健康体检表统计详细,
            this.MenuItem接诊记录表统计,
            this.MenuItem老年人统计,
            this.MenuItem健康人统计,
            this.MenuItem慢病统计,
            this.MenuItem健康人未体检,
            this.MenuItem健康人已体检,
            this.MenuItem高血压未体检,
            this.MenuItem高血压已体检,
            this.MenuItem糖尿病未体检,
            this.MenuItem糖尿病已体检,
            this.MenuItem老年人未体检,
            this.MenuItem老年人已体检,
            this.MenuItem重症精神疾病未体检,
            this.MenuItem重症精神疾病已体检,
            this.MenuItem档案复核统计,
            this.MenuItem贫困人口统计});
            this.MenuItem居民健康统计.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.MenuItem居民健康统计.Name = "MenuItem居民健康统计";
            this.MenuItem居民健康统计.Size = new System.Drawing.Size(156, 26);
            this.MenuItem居民健康统计.Text = "居民健康统计";
            // 
            // MenuItem个人基本信息统计
            // 
            this.MenuItem个人基本信息统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem个人基本信息统计.Name = "MenuItem个人基本信息统计";
            this.MenuItem个人基本信息统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem个人基本信息统计.Text = "个人基本信息统计";
            this.MenuItem个人基本信息统计.Click += new System.EventHandler(this.MenuItem个人基本信息统计_Click);
            // 
            // MenuItem健康体检表信息统计
            // 
            this.MenuItem健康体检表信息统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem健康体检表信息统计.Name = "MenuItem健康体检表信息统计";
            this.MenuItem健康体检表信息统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem健康体检表信息统计.Text = "健康体检表统计";
            this.MenuItem健康体检表信息统计.Click += new System.EventHandler(this.MenuItem健康体检表信息统计_Click);
            // 
            // MenuItem健康体检表统计详细
            // 
            this.MenuItem健康体检表统计详细.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem健康体检表统计详细.Name = "MenuItem健康体检表统计详细";
            this.MenuItem健康体检表统计详细.Size = new System.Drawing.Size(188, 26);
            this.MenuItem健康体检表统计详细.Text = "健康体检表统计详细";
            this.MenuItem健康体检表统计详细.Click += new System.EventHandler(this.MenuItem健康体检表统计详细_Click);
            // 
            // MenuItem接诊记录表统计
            // 
            this.MenuItem接诊记录表统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem接诊记录表统计.Name = "MenuItem接诊记录表统计";
            this.MenuItem接诊记录表统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem接诊记录表统计.Text = "接诊记录表统计";
            this.MenuItem接诊记录表统计.Click += new System.EventHandler(this.MenuItem接诊记录表统计_Click);
            // 
            // MenuItem老年人统计
            // 
            this.MenuItem老年人统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem老年人统计.Name = "MenuItem老年人统计";
            this.MenuItem老年人统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem老年人统计.Text = "老年人体检统计";
            this.MenuItem老年人统计.Click += new System.EventHandler(this.MenuItem老年人统计_Click);
            // 
            // MenuItem健康人统计
            // 
            this.MenuItem健康人统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem健康人统计.Name = "MenuItem健康人统计";
            this.MenuItem健康人统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem健康人统计.Text = "健康人体检统计";
            this.MenuItem健康人统计.Click += new System.EventHandler(this.MenuItem健康人统计_Click);
            // 
            // MenuItem慢病统计
            // 
            this.MenuItem慢病统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem慢病统计.Name = "MenuItem慢病统计";
            this.MenuItem慢病统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem慢病统计.Text = "慢病体检统计";
            this.MenuItem慢病统计.Click += new System.EventHandler(this.MenuItem慢病统计_Click);
            // 
            // MenuItem健康人未体检
            // 
            this.MenuItem健康人未体检.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem健康人未体检.Name = "MenuItem健康人未体检";
            this.MenuItem健康人未体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem健康人未体检.Text = "健康人未体检";
            this.MenuItem健康人未体检.Click += new System.EventHandler(this.MenuItem健康人未体检_Click);
            // 
            // MenuItem健康人已体检
            // 
            this.MenuItem健康人已体检.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem健康人已体检.Name = "MenuItem健康人已体检";
            this.MenuItem健康人已体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem健康人已体检.Text = "健康人已体检";
            this.MenuItem健康人已体检.Click += new System.EventHandler(this.MenuItem健康人已体检_Click);
            // 
            // MenuItem高血压未体检
            // 
            this.MenuItem高血压未体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem高血压未体检.Image")));
            this.MenuItem高血压未体检.Name = "MenuItem高血压未体检";
            this.MenuItem高血压未体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem高血压未体检.Text = "高血压未体检";
            this.MenuItem高血压未体检.Click += new System.EventHandler(this.MenuItem高血压未体检_Click);
            // 
            // MenuItem高血压已体检
            // 
            this.MenuItem高血压已体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem高血压已体检.Image")));
            this.MenuItem高血压已体检.Name = "MenuItem高血压已体检";
            this.MenuItem高血压已体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem高血压已体检.Text = "高血压已体检";
            this.MenuItem高血压已体检.Click += new System.EventHandler(this.MenuItem高血压已体检_Click);
            // 
            // MenuItem糖尿病未体检
            // 
            this.MenuItem糖尿病未体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem糖尿病未体检.Image")));
            this.MenuItem糖尿病未体检.Name = "MenuItem糖尿病未体检";
            this.MenuItem糖尿病未体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem糖尿病未体检.Text = "糖尿病未体检";
            this.MenuItem糖尿病未体检.Click += new System.EventHandler(this.MenuItem糖尿病未体检_Click);
            // 
            // MenuItem糖尿病已体检
            // 
            this.MenuItem糖尿病已体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem糖尿病已体检.Image")));
            this.MenuItem糖尿病已体检.Name = "MenuItem糖尿病已体检";
            this.MenuItem糖尿病已体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem糖尿病已体检.Text = "糖尿病已体检";
            this.MenuItem糖尿病已体检.Click += new System.EventHandler(this.MenuItem糖尿病已体检_Click);
            // 
            // MenuItem老年人未体检
            // 
            this.MenuItem老年人未体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem老年人未体检.Image")));
            this.MenuItem老年人未体检.Name = "MenuItem老年人未体检";
            this.MenuItem老年人未体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem老年人未体检.Text = "老年人未体检";
            this.MenuItem老年人未体检.Click += new System.EventHandler(this.MenuItem老年人未体检_Click);
            // 
            // MenuItem老年人已体检
            // 
            this.MenuItem老年人已体检.Image = ((System.Drawing.Image)(resources.GetObject("MenuItem老年人已体检.Image")));
            this.MenuItem老年人已体检.Name = "MenuItem老年人已体检";
            this.MenuItem老年人已体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem老年人已体检.Text = "老年人已体检";
            this.MenuItem老年人已体检.Click += new System.EventHandler(this.MenuItem老年人已体检_Click);
            // 
            // MenuItem重症精神疾病未体检
            // 
            this.MenuItem重症精神疾病未体检.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem重症精神疾病未体检.Name = "MenuItem重症精神疾病未体检";
            this.MenuItem重症精神疾病未体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem重症精神疾病未体检.Text = "重症精神疾病未体检";
            this.MenuItem重症精神疾病未体检.Click += new System.EventHandler(this.MenuItem重症精神疾病未体检_Click);
            // 
            // MenuItem重症精神疾病已体检
            // 
            this.MenuItem重症精神疾病已体检.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem重症精神疾病已体检.Name = "MenuItem重症精神疾病已体检";
            this.MenuItem重症精神疾病已体检.Size = new System.Drawing.Size(188, 26);
            this.MenuItem重症精神疾病已体检.Text = "重症精神疾病已体检";
            this.MenuItem重症精神疾病已体检.Click += new System.EventHandler(this.MenuItem重症精神疾病已体检_Click);
            // 
            // MenuItem档案复核统计
            // 
            this.MenuItem档案复核统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem档案复核统计.Name = "MenuItem档案复核统计";
            this.MenuItem档案复核统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem档案复核统计.Text = "档案复核统计";
            this.MenuItem档案复核统计.Click += new System.EventHandler(this.MenuItem档案复核统计_Click);
            // 
            // MenuItem贫困人口统计
            // 
            this.MenuItem贫困人口统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem贫困人口统计.Name = "MenuItem贫困人口统计";
            this.MenuItem贫困人口统计.Size = new System.Drawing.Size(188, 26);
            this.MenuItem贫困人口统计.Text = "贫困人口体检统计";
            this.MenuItem贫困人口统计.Click += new System.EventHandler(this.MenuItem贫困人口统计_Click);
            // 
            // 管理率统计ToolStripMenuItem
            // 
            this.管理率统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.辖区居民管理率统计ToolStripMenuItem,
            this.老年人管理率统计ToolStripMenuItem,
            this.高高血压患者管理率统计ToolStripMenuItem,
            this.糖尿病患者管理率统计ToolStripMenuItem,
            this.脑卒中管理率统计ToolStripMenuItem,
            this.冠心病管理率统计ToolStripMenuItem,
            this.疾病统计分析ToolStripMenuItem});
            this.管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.管理率统计ToolStripMenuItem.Name = "管理率统计ToolStripMenuItem";
            this.管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.管理率统计ToolStripMenuItem.Text = "管理率统计";
            // 
            // 辖区居民管理率统计ToolStripMenuItem
            // 
            this.辖区居民管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.辖区居民管理率统计ToolStripMenuItem.Name = "辖区居民管理率统计ToolStripMenuItem";
            this.辖区居民管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.辖区居民管理率统计ToolStripMenuItem.Text = "辖区居民管理率统计";
            this.辖区居民管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.辖区居民管理率统计ToolStripMenuItem_Click);
            // 
            // 老年人管理率统计ToolStripMenuItem
            // 
            this.老年人管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.老年人管理率统计ToolStripMenuItem.Name = "老年人管理率统计ToolStripMenuItem";
            this.老年人管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.老年人管理率统计ToolStripMenuItem.Text = "老年人管理率统计";
            this.老年人管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.老年人管理率统计ToolStripMenuItem_Click);
            // 
            // 高高血压患者管理率统计ToolStripMenuItem
            // 
            this.高高血压患者管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.高高血压患者管理率统计ToolStripMenuItem.Name = "高高血压患者管理率统计ToolStripMenuItem";
            this.高高血压患者管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.高高血压患者管理率统计ToolStripMenuItem.Text = "高血压管理率统计";
            this.高高血压患者管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.高血压管理率统计ToolStripMenuItem_Click);
            // 
            // 糖尿病患者管理率统计ToolStripMenuItem
            // 
            this.糖尿病患者管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.糖尿病患者管理率统计ToolStripMenuItem.Name = "糖尿病患者管理率统计ToolStripMenuItem";
            this.糖尿病患者管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.糖尿病患者管理率统计ToolStripMenuItem.Text = "糖尿病管理率统计";
            this.糖尿病患者管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.糖尿病管理率统计ToolStripMenuItem_Click);
            // 
            // 脑卒中管理率统计ToolStripMenuItem
            // 
            this.脑卒中管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.脑卒中管理率统计ToolStripMenuItem.Name = "脑卒中管理率统计ToolStripMenuItem";
            this.脑卒中管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.脑卒中管理率统计ToolStripMenuItem.Text = "脑卒中管理率统计";
            this.脑卒中管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.脑卒中管理率统计ToolStripMenuItem_Click);
            // 
            // 冠心病管理率统计ToolStripMenuItem
            // 
            this.冠心病管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.冠心病管理率统计ToolStripMenuItem.Name = "冠心病管理率统计ToolStripMenuItem";
            this.冠心病管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.冠心病管理率统计ToolStripMenuItem.Text = "冠心病管理率统计";
            this.冠心病管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.冠心病管理率统计ToolStripMenuItem_Click);
            // 
            // 疾病统计分析ToolStripMenuItem
            // 
            this.疾病统计分析ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.疾病统计分析ToolStripMenuItem.Name = "疾病统计分析ToolStripMenuItem";
            this.疾病统计分析ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.疾病统计分析ToolStripMenuItem.Text = "疾病统计分析";
            this.疾病统计分析ToolStripMenuItem.Click += new System.EventHandler(this.疾病统计分析ToolStripMenuItem_Click);
            // 
            // 基本公共卫生ToolStripMenuItem
            // 
            this.基本公共卫生ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.工作量统计ToolStripMenuItem1,
            this.居民健康档案统计ToolStripMenuItem,
            this.老年人统计查询ToolStripMenuItem,
            this.慢病统计查询ToolStripMenuItem,
            this.孕产妇统计查询ToolStripMenuItem,
            this.儿童统计查询ToolStripMenuItem});
            this.基本公共卫生ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("基本公共卫生ToolStripMenuItem.Image")));
            this.基本公共卫生ToolStripMenuItem.Name = "基本公共卫生ToolStripMenuItem";
            this.基本公共卫生ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.基本公共卫生ToolStripMenuItem.Text = "基本公共卫生";
            // 
            // 工作量统计ToolStripMenuItem1
            // 
            this.工作量统计ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem儿童健康管理统计,
            this.妇女健康管理统计ToolStripMenuItem,
            this.老年人健康管理统计ToolStripMenuItem,
            this.重症精神疾病患者管理统计ToolStripMenuItem,
            this.慢性病ToolStripMenuItem,
            this.残疾人健康管理统计ToolStripMenuItem});
            this.工作量统计ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.工作量统计ToolStripMenuItem1.Name = "工作量统计ToolStripMenuItem1";
            this.工作量统计ToolStripMenuItem1.Size = new System.Drawing.Size(183, 26);
            this.工作量统计ToolStripMenuItem1.Text = "工作量统计";
            // 
            // MenuItem儿童健康管理统计
            // 
            this.MenuItem儿童健康管理统计.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem儿童基本信息统计,
            this.MenuItem新生儿家庭访视记录统计,
            this.MenuItem儿童健康检查记录表满月统计,
            this.MenuItem儿童健康检查记录表3月龄统计,
            this.MenuItem儿童健康检查记录表6月龄,
            this.MenuItem儿童健康检查记录表8月龄统计,
            this.MenuItem儿童健康检查记录表12月龄统计,
            this.MenuItem儿童健康检查记录表18月龄统计,
            this.MenuItem儿童健康检查记录表24月龄统计,
            this.MenuItem儿童健康检查记录表30月龄,
            this.MenuItem儿童健康检查记录表3岁统计,
            this.MenuItem儿童健康检查记录表4岁统计,
            this.MenuItem儿童健康检查记录表5岁统计,
            this.MenuItem儿童健康检查记录表6岁统计});
            this.MenuItem儿童健康管理统计.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.MenuItem儿童健康管理统计.Name = "MenuItem儿童健康管理统计";
            this.MenuItem儿童健康管理统计.Size = new System.Drawing.Size(224, 26);
            this.MenuItem儿童健康管理统计.Text = "儿童健康管理统计";
            // 
            // MenuItem儿童基本信息统计
            // 
            this.MenuItem儿童基本信息统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童基本信息统计.Name = "MenuItem儿童基本信息统计";
            this.MenuItem儿童基本信息统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童基本信息统计.Text = "儿童基本信息";
            this.MenuItem儿童基本信息统计.Click += new System.EventHandler(this.MenuItem儿童基本信息统计_Click);
            // 
            // MenuItem新生儿家庭访视记录统计
            // 
            this.MenuItem新生儿家庭访视记录统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem新生儿家庭访视记录统计.Name = "MenuItem新生儿家庭访视记录统计";
            this.MenuItem新生儿家庭访视记录统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem新生儿家庭访视记录统计.Text = "新生儿家庭访视记录表";
            this.MenuItem新生儿家庭访视记录统计.Click += new System.EventHandler(this.MenuItem新生儿家庭访视记录统计_Click);
            // 
            // MenuItem儿童健康检查记录表满月统计
            // 
            this.MenuItem儿童健康检查记录表满月统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表满月统计.Name = "MenuItem儿童健康检查记录表满月统计";
            this.MenuItem儿童健康检查记录表满月统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表满月统计.Text = "儿童健康检查记录表(满月)";
            this.MenuItem儿童健康检查记录表满月统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表满月统计_Click);
            // 
            // MenuItem儿童健康检查记录表3月龄统计
            // 
            this.MenuItem儿童健康检查记录表3月龄统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表3月龄统计.Name = "MenuItem儿童健康检查记录表3月龄统计";
            this.MenuItem儿童健康检查记录表3月龄统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表3月龄统计.Text = "儿童健康检查记录表(3月龄)";
            this.MenuItem儿童健康检查记录表3月龄统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表3月龄统计_Click);
            // 
            // MenuItem儿童健康检查记录表6月龄
            // 
            this.MenuItem儿童健康检查记录表6月龄.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表6月龄.Name = "MenuItem儿童健康检查记录表6月龄";
            this.MenuItem儿童健康检查记录表6月龄.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表6月龄.Text = "儿童健康检查记录表(6月龄)";
            this.MenuItem儿童健康检查记录表6月龄.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表6月龄_Click);
            // 
            // MenuItem儿童健康检查记录表8月龄统计
            // 
            this.MenuItem儿童健康检查记录表8月龄统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表8月龄统计.Name = "MenuItem儿童健康检查记录表8月龄统计";
            this.MenuItem儿童健康检查记录表8月龄统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表8月龄统计.Text = "儿童健康检查记录表(8月龄)";
            this.MenuItem儿童健康检查记录表8月龄统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表8月龄统计_Click);
            // 
            // MenuItem儿童健康检查记录表12月龄统计
            // 
            this.MenuItem儿童健康检查记录表12月龄统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表12月龄统计.Name = "MenuItem儿童健康检查记录表12月龄统计";
            this.MenuItem儿童健康检查记录表12月龄统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表12月龄统计.Text = "儿童健康检查记录表(12月龄)";
            this.MenuItem儿童健康检查记录表12月龄统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表12月龄统计_Click);
            // 
            // MenuItem儿童健康检查记录表18月龄统计
            // 
            this.MenuItem儿童健康检查记录表18月龄统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表18月龄统计.Name = "MenuItem儿童健康检查记录表18月龄统计";
            this.MenuItem儿童健康检查记录表18月龄统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表18月龄统计.Text = "儿童健康检查记录表(18月龄)";
            this.MenuItem儿童健康检查记录表18月龄统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表18月龄统计_Click);
            // 
            // MenuItem儿童健康检查记录表24月龄统计
            // 
            this.MenuItem儿童健康检查记录表24月龄统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表24月龄统计.Name = "MenuItem儿童健康检查记录表24月龄统计";
            this.MenuItem儿童健康检查记录表24月龄统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表24月龄统计.Text = "儿童健康检查记录表(24月龄)";
            this.MenuItem儿童健康检查记录表24月龄统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表24月龄统计_Click);
            // 
            // MenuItem儿童健康检查记录表30月龄
            // 
            this.MenuItem儿童健康检查记录表30月龄.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表30月龄.Name = "MenuItem儿童健康检查记录表30月龄";
            this.MenuItem儿童健康检查记录表30月龄.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表30月龄.Text = "儿童健康检查记录表(30月龄)";
            this.MenuItem儿童健康检查记录表30月龄.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表30月龄_Click);
            // 
            // MenuItem儿童健康检查记录表3岁统计
            // 
            this.MenuItem儿童健康检查记录表3岁统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表3岁统计.Name = "MenuItem儿童健康检查记录表3岁统计";
            this.MenuItem儿童健康检查记录表3岁统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表3岁统计.Text = "儿童健康检查记录表(3岁)";
            this.MenuItem儿童健康检查记录表3岁统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表3岁统计_Click);
            // 
            // MenuItem儿童健康检查记录表4岁统计
            // 
            this.MenuItem儿童健康检查记录表4岁统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表4岁统计.Name = "MenuItem儿童健康检查记录表4岁统计";
            this.MenuItem儿童健康检查记录表4岁统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表4岁统计.Text = "儿童健康检查记录表(4岁)";
            this.MenuItem儿童健康检查记录表4岁统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表4岁统计_Click);
            // 
            // MenuItem儿童健康检查记录表5岁统计
            // 
            this.MenuItem儿童健康检查记录表5岁统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表5岁统计.Name = "MenuItem儿童健康检查记录表5岁统计";
            this.MenuItem儿童健康检查记录表5岁统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表5岁统计.Text = "儿童健康检查记录表(5岁)";
            this.MenuItem儿童健康检查记录表5岁统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表5岁统计_Click);
            // 
            // MenuItem儿童健康检查记录表6岁统计
            // 
            this.MenuItem儿童健康检查记录表6岁统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem儿童健康检查记录表6岁统计.Name = "MenuItem儿童健康检查记录表6岁统计";
            this.MenuItem儿童健康检查记录表6岁统计.Size = new System.Drawing.Size(230, 22);
            this.MenuItem儿童健康检查记录表6岁统计.Text = "儿童健康检查记录表(6岁)";
            this.MenuItem儿童健康检查记录表6岁统计.Click += new System.EventHandler(this.MenuItem儿童健康检查记录表6岁统计_Click);
            // 
            // 妇女健康管理统计ToolStripMenuItem
            // 
            this.妇女健康管理统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.孕产妇健康管理ToolStripMenuItem1});
            this.妇女健康管理统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.妇女健康管理统计ToolStripMenuItem.Name = "妇女健康管理统计ToolStripMenuItem";
            this.妇女健康管理统计ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.妇女健康管理统计ToolStripMenuItem.Text = "妇女健康管理统计";
            // 
            // 孕产妇健康管理ToolStripMenuItem1
            // 
            this.孕产妇健康管理ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem孕产妇基本信息统计,
            this.MenuItem孕产1次随访,
            this.MenuItem孕产2次随访,
            this.MenuItem孕产3次随访,
            this.MenuItem孕产4次随访,
            this.MenuItem孕产5次随访,
            this.MenuItem产后访视记录随访统计,
            this.MenuItem产后42天访视记录随访统计});
            this.孕产妇健康管理ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.孕产妇健康管理ToolStripMenuItem1.Name = "孕产妇健康管理ToolStripMenuItem1";
            this.孕产妇健康管理ToolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            this.孕产妇健康管理ToolStripMenuItem1.Text = "孕产妇健康管理";
            // 
            // MenuItem孕产妇基本信息统计
            // 
            this.MenuItem孕产妇基本信息统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产妇基本信息统计.Name = "MenuItem孕产妇基本信息统计";
            this.MenuItem孕产妇基本信息统计.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产妇基本信息统计.Text = "孕产妇基本信息";
            this.MenuItem孕产妇基本信息统计.Click += new System.EventHandler(this.MenuItem孕产妇基本信息统计_Click);
            // 
            // MenuItem孕产1次随访
            // 
            this.MenuItem孕产1次随访.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产1次随访.Name = "MenuItem孕产1次随访";
            this.MenuItem孕产1次随访.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产1次随访.Text = "第1次产前随访服务记录表";
            this.MenuItem孕产1次随访.Click += new System.EventHandler(this.MenuItem孕产1次随访_Click);
            // 
            // MenuItem孕产2次随访
            // 
            this.MenuItem孕产2次随访.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产2次随访.Name = "MenuItem孕产2次随访";
            this.MenuItem孕产2次随访.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产2次随访.Text = "第2次产前随访服务记录表";
            this.MenuItem孕产2次随访.Click += new System.EventHandler(this.MenuItem孕产2次随访_Click);
            // 
            // MenuItem孕产3次随访
            // 
            this.MenuItem孕产3次随访.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产3次随访.Name = "MenuItem孕产3次随访";
            this.MenuItem孕产3次随访.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产3次随访.Text = "第3次产前随访服务记录表";
            this.MenuItem孕产3次随访.Click += new System.EventHandler(this.MenuItem孕产3次随访_Click);
            // 
            // MenuItem孕产4次随访
            // 
            this.MenuItem孕产4次随访.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产4次随访.Name = "MenuItem孕产4次随访";
            this.MenuItem孕产4次随访.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产4次随访.Text = "第4次产前随访服务记录表";
            this.MenuItem孕产4次随访.Click += new System.EventHandler(this.MenuItem孕产4次随访_Click);
            // 
            // MenuItem孕产5次随访
            // 
            this.MenuItem孕产5次随访.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem孕产5次随访.Name = "MenuItem孕产5次随访";
            this.MenuItem孕产5次随访.Size = new System.Drawing.Size(215, 22);
            this.MenuItem孕产5次随访.Text = "第5次产前随访服务记录表";
            this.MenuItem孕产5次随访.Click += new System.EventHandler(this.MenuItem孕产5次随访_Click);
            // 
            // MenuItem产后访视记录随访统计
            // 
            this.MenuItem产后访视记录随访统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem产后访视记录随访统计.Name = "MenuItem产后访视记录随访统计";
            this.MenuItem产后访视记录随访统计.Size = new System.Drawing.Size(215, 22);
            this.MenuItem产后访视记录随访统计.Text = "产后访视记录表统计";
            this.MenuItem产后访视记录随访统计.Click += new System.EventHandler(this.MenuItem产后访视记录随访统计_Click);
            // 
            // MenuItem产后42天访视记录随访统计
            // 
            this.MenuItem产后42天访视记录随访统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem产后42天访视记录随访统计.Name = "MenuItem产后42天访视记录随访统计";
            this.MenuItem产后42天访视记录随访统计.Size = new System.Drawing.Size(215, 22);
            this.MenuItem产后42天访视记录随访统计.Text = "产后42天访视记录表统计";
            this.MenuItem产后42天访视记录随访统计.Click += new System.EventHandler(this.MenuItem产后42天访视记录随访统计_Click);
            // 
            // 老年人健康管理统计ToolStripMenuItem
            // 
            this.老年人健康管理统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem老年人随访表统计});
            this.老年人健康管理统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.老年人健康管理统计ToolStripMenuItem.Name = "老年人健康管理统计ToolStripMenuItem";
            this.老年人健康管理统计ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.老年人健康管理统计ToolStripMenuItem.Text = "老年人健康管理统计";
            // 
            // MenuItem老年人随访表统计
            // 
            this.MenuItem老年人随访表统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem老年人随访表统计.Name = "MenuItem老年人随访表统计";
            this.MenuItem老年人随访表统计.Size = new System.Drawing.Size(172, 22);
            this.MenuItem老年人随访表统计.Text = "老年人随访表统计";
            this.MenuItem老年人随访表统计.Click += new System.EventHandler(this.MenuItem老年人随访表统计_Click);
            // 
            // 重症精神疾病患者管理统计ToolStripMenuItem
            // 
            this.重症精神疾病患者管理统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem精神疾病补充表统计,
            this.MenuItem精神疾病随访表统计});
            this.重症精神疾病患者管理统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.重症精神疾病患者管理统计ToolStripMenuItem.Name = "重症精神疾病患者管理统计ToolStripMenuItem";
            this.重症精神疾病患者管理统计ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.重症精神疾病患者管理统计ToolStripMenuItem.Text = "重症精神疾病患者管理统计";
            // 
            // MenuItem精神疾病补充表统计
            // 
            this.MenuItem精神疾病补充表统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem精神疾病补充表统计.Name = "MenuItem精神疾病补充表统计";
            this.MenuItem精神疾病补充表统计.Size = new System.Drawing.Size(184, 22);
            this.MenuItem精神疾病补充表统计.Text = "精神疾病补充表统计";
            this.MenuItem精神疾病补充表统计.Click += new System.EventHandler(this.MenuItem精神疾病补充表统计_Click);
            // 
            // MenuItem精神疾病随访表统计
            // 
            this.MenuItem精神疾病随访表统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem精神疾病随访表统计.Name = "MenuItem精神疾病随访表统计";
            this.MenuItem精神疾病随访表统计.Size = new System.Drawing.Size(184, 22);
            this.MenuItem精神疾病随访表统计.Text = "精神疾病随访表统计";
            this.MenuItem精神疾病随访表统计.Click += new System.EventHandler(this.MenuItem精神疾病随访表统计_Click);
            // 
            // 慢性病ToolStripMenuItem
            // 
            this.慢性病ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem高血压患者管理卡统计,
            this.MenuItem高血压患者随访记录表,
            this.MenuItem糖尿病患者管理卡统计,
            this.MenuItem糖尿病患者随访记录表,
            this.MenuItem脑卒中患者管理卡统计,
            this.MenuItem脑卒中患者随访记录表,
            this.MenuItem冠心病患者管理卡统计,
            this.MenuItem冠心病患者随访记录表});
            this.慢性病ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.慢性病ToolStripMenuItem.Name = "慢性病ToolStripMenuItem";
            this.慢性病ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.慢性病ToolStripMenuItem.Text = "慢性病健康患者管理统计";
            // 
            // MenuItem高血压患者管理卡统计
            // 
            this.MenuItem高血压患者管理卡统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem高血压患者管理卡统计.Name = "MenuItem高血压患者管理卡统计";
            this.MenuItem高血压患者管理卡统计.Size = new System.Drawing.Size(196, 22);
            this.MenuItem高血压患者管理卡统计.Text = "高血压患者管理卡";
            this.MenuItem高血压患者管理卡统计.Click += new System.EventHandler(this.MenuItem高血压患者管理卡统计_Click);
            // 
            // MenuItem高血压患者随访记录表
            // 
            this.MenuItem高血压患者随访记录表.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem高血压患者随访记录表.Name = "MenuItem高血压患者随访记录表";
            this.MenuItem高血压患者随访记录表.Size = new System.Drawing.Size(196, 22);
            this.MenuItem高血压患者随访记录表.Text = "高血压患者随访记录表";
            this.MenuItem高血压患者随访记录表.Click += new System.EventHandler(this.MenuItem高血压患者随访记录表_Click);
            // 
            // MenuItem糖尿病患者管理卡统计
            // 
            this.MenuItem糖尿病患者管理卡统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem糖尿病患者管理卡统计.Name = "MenuItem糖尿病患者管理卡统计";
            this.MenuItem糖尿病患者管理卡统计.Size = new System.Drawing.Size(196, 22);
            this.MenuItem糖尿病患者管理卡统计.Text = "糖尿病患者管理卡";
            this.MenuItem糖尿病患者管理卡统计.Click += new System.EventHandler(this.MenuItem糖尿病患者管理卡统计_Click);
            // 
            // MenuItem糖尿病患者随访记录表
            // 
            this.MenuItem糖尿病患者随访记录表.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem糖尿病患者随访记录表.Name = "MenuItem糖尿病患者随访记录表";
            this.MenuItem糖尿病患者随访记录表.Size = new System.Drawing.Size(196, 22);
            this.MenuItem糖尿病患者随访记录表.Text = "糖尿病患者随访记录表";
            this.MenuItem糖尿病患者随访记录表.Click += new System.EventHandler(this.MenuItem糖尿病患者随访记录表_Click);
            // 
            // MenuItem脑卒中患者管理卡统计
            // 
            this.MenuItem脑卒中患者管理卡统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem脑卒中患者管理卡统计.Name = "MenuItem脑卒中患者管理卡统计";
            this.MenuItem脑卒中患者管理卡统计.Size = new System.Drawing.Size(196, 22);
            this.MenuItem脑卒中患者管理卡统计.Text = "脑卒中患者管理卡";
            this.MenuItem脑卒中患者管理卡统计.Click += new System.EventHandler(this.MenuItem脑卒中患者管理卡统计_Click);
            // 
            // MenuItem脑卒中患者随访记录表
            // 
            this.MenuItem脑卒中患者随访记录表.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem脑卒中患者随访记录表.Name = "MenuItem脑卒中患者随访记录表";
            this.MenuItem脑卒中患者随访记录表.Size = new System.Drawing.Size(196, 22);
            this.MenuItem脑卒中患者随访记录表.Text = "脑卒中患者随访记录表";
            this.MenuItem脑卒中患者随访记录表.Click += new System.EventHandler(this.MenuItem脑卒中患者随访记录表_Click);
            // 
            // MenuItem冠心病患者管理卡统计
            // 
            this.MenuItem冠心病患者管理卡统计.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem冠心病患者管理卡统计.Name = "MenuItem冠心病患者管理卡统计";
            this.MenuItem冠心病患者管理卡统计.Size = new System.Drawing.Size(196, 22);
            this.MenuItem冠心病患者管理卡统计.Text = "冠心病患者管理卡";
            this.MenuItem冠心病患者管理卡统计.Click += new System.EventHandler(this.MenuItem冠心病患者管理卡统计_Click);
            // 
            // MenuItem冠心病患者随访记录表
            // 
            this.MenuItem冠心病患者随访记录表.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.MenuItem冠心病患者随访记录表.Name = "MenuItem冠心病患者随访记录表";
            this.MenuItem冠心病患者随访记录表.Size = new System.Drawing.Size(196, 22);
            this.MenuItem冠心病患者随访记录表.Text = "冠心病患者随访记录表";
            this.MenuItem冠心病患者随访记录表.Click += new System.EventHandler(this.MenuItem冠心病患者随访记录表_Click);
            // 
            // 残疾人健康管理统计ToolStripMenuItem
            // 
            this.残疾人健康管理统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.残疾人康复服务信息统计ToolStripMenuItem});
            this.残疾人健康管理统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.残疾人健康管理统计ToolStripMenuItem.Name = "残疾人健康管理统计ToolStripMenuItem";
            this.残疾人健康管理统计ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.残疾人健康管理统计ToolStripMenuItem.Text = "残疾人健康管理统计";
            // 
            // 残疾人康复服务信息统计ToolStripMenuItem
            // 
            this.残疾人康复服务信息统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.残疾人康复服务信息统计ToolStripMenuItem.Name = "残疾人康复服务信息统计ToolStripMenuItem";
            this.残疾人康复服务信息统计ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.残疾人康复服务信息统计ToolStripMenuItem.Text = "残疾人康复服务信息统计";
            this.残疾人康复服务信息统计ToolStripMenuItem.Click += new System.EventHandler(this.残疾人康复服务信息统计ToolStripMenuItem_Click);
            // 
            // 居民健康档案统计ToolStripMenuItem
            // 
            this.居民健康档案统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.个人基本信息统计ToolStripMenuItem,
            this.档案复核统计ToolStripMenuItem1,
            this.档案动态使用率统计ToolStripMenuItem1,
            this.档案重复数据统计ToolStripMenuItem1,
            this.管理率统计分析ToolStripMenuItem1,
            this.贫困人口体检统计ToolStripMenuItem1,
            this.公卫统计汇总ToolStripMenuItem});
            this.居民健康档案统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.居民健康档案统计ToolStripMenuItem.Name = "居民健康档案统计ToolStripMenuItem";
            this.居民健康档案统计ToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.居民健康档案统计ToolStripMenuItem.Text = "居民健康档案统计";
            // 
            // 个人基本信息统计ToolStripMenuItem
            // 
            this.个人基本信息统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.个人基本信息统计ToolStripMenuItem.Name = "个人基本信息统计ToolStripMenuItem";
            this.个人基本信息统计ToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.个人基本信息统计ToolStripMenuItem.Text = "个人基本信息统计";
            this.个人基本信息统计ToolStripMenuItem.Click += new System.EventHandler(this.MenuItem个人基本信息统计_Click);
            // 
            // 档案复核统计ToolStripMenuItem1
            // 
            this.档案复核统计ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.档案复核统计ToolStripMenuItem1.Name = "档案复核统计ToolStripMenuItem1";
            this.档案复核统计ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.档案复核统计ToolStripMenuItem1.Text = "档案复核统计";
            this.档案复核统计ToolStripMenuItem1.Click += new System.EventHandler(this.MenuItem档案复核统计_Click);
            // 
            // 档案动态使用率统计ToolStripMenuItem1
            // 
            this.档案动态使用率统计ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.档案动态使用率统计ToolStripMenuItem1.Name = "档案动态使用率统计ToolStripMenuItem1";
            this.档案动态使用率统计ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.档案动态使用率统计ToolStripMenuItem1.Text = "档案动态使用率统计";
            this.档案动态使用率统计ToolStripMenuItem1.Click += new System.EventHandler(this.档案动态使用率统计ToolStripMenuItem_Click);
            // 
            // 档案重复数据统计ToolStripMenuItem1
            // 
            this.档案重复数据统计ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.档案重复数据统计ToolStripMenuItem1.Name = "档案重复数据统计ToolStripMenuItem1";
            this.档案重复数据统计ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.档案重复数据统计ToolStripMenuItem1.Text = "档案重复数据统计";
            this.档案重复数据统计ToolStripMenuItem1.Click += new System.EventHandler(this.档案重复数据统计ToolStripMenuItem_Click);
            // 
            // 管理率统计分析ToolStripMenuItem1
            // 
            this.管理率统计分析ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.管理率统计分析ToolStripMenuItem1.Name = "管理率统计分析ToolStripMenuItem1";
            this.管理率统计分析ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.管理率统计分析ToolStripMenuItem1.Text = "管理率统计分析";
            this.管理率统计分析ToolStripMenuItem1.Click += new System.EventHandler(this.管理率统计分析ToolStripMenuItem_Click);
            // 
            // 贫困人口体检统计ToolStripMenuItem1
            // 
            this.贫困人口体检统计ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.贫困人口体检统计ToolStripMenuItem1.Name = "贫困人口体检统计ToolStripMenuItem1";
            this.贫困人口体检统计ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.贫困人口体检统计ToolStripMenuItem1.Text = "贫困人口体检统计";
            this.贫困人口体检统计ToolStripMenuItem1.Click += new System.EventHandler(this.贫困人口体检统计ToolStripMenuItem_Click);
            // 
            // 公卫统计汇总ToolStripMenuItem
            // 
            this.公卫统计汇总ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.公卫统计汇总ToolStripMenuItem.Name = "公卫统计汇总ToolStripMenuItem";
            this.公卫统计汇总ToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.公卫统计汇总ToolStripMenuItem.Text = "公卫统计汇总";
            this.公卫统计汇总ToolStripMenuItem.Click += new System.EventHandler(this.公卫统计汇总ToolStripMenuItem_Click);
            // 
            // 老年人统计查询ToolStripMenuItem
            // 
            this.老年人统计查询ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.老年人健康体检统计ToolStripMenuItem,
            this.老年人月报表ToolStripMenuItem1});
            this.老年人统计查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.老年人统计查询ToolStripMenuItem.Name = "老年人统计查询ToolStripMenuItem";
            this.老年人统计查询ToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.老年人统计查询ToolStripMenuItem.Text = "老年人统计查询";
            // 
            // 老年人健康体检统计ToolStripMenuItem
            // 
            this.老年人健康体检统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.老年人健康体检统计ToolStripMenuItem.Name = "老年人健康体检统计ToolStripMenuItem";
            this.老年人健康体检统计ToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.老年人健康体检统计ToolStripMenuItem.Text = "老年人健康体检统计";
            this.老年人健康体检统计ToolStripMenuItem.Click += new System.EventHandler(this.MenuItem老年人统计_Click);
            // 
            // 老年人月报表ToolStripMenuItem1
            // 
            this.老年人月报表ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.老年人月报表ToolStripMenuItem1.Name = "老年人月报表ToolStripMenuItem1";
            this.老年人月报表ToolStripMenuItem1.Size = new System.Drawing.Size(188, 26);
            this.老年人月报表ToolStripMenuItem1.Text = "老年人月报表";
            this.老年人月报表ToolStripMenuItem1.Click += new System.EventHandler(this.老年人月报表ToolStripMenuItem_Click);
            // 
            // 慢病统计查询ToolStripMenuItem
            // 
            this.慢病统计查询ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.高血压患者血压控制率ToolStripMenuItem1,
            this.糖尿病患者血糖控制率ToolStripMenuItem1,
            this.管理率统计分析ToolStripMenuItem2,
            this.高血压高危人群达标率ToolStripMenuItem});
            this.慢病统计查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.慢病统计查询ToolStripMenuItem.Name = "慢病统计查询ToolStripMenuItem";
            this.慢病统计查询ToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.慢病统计查询ToolStripMenuItem.Text = "慢性病统计查询";
            // 
            // 高血压患者血压控制率ToolStripMenuItem1
            // 
            this.高血压患者血压控制率ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.高血压患者血压控制率ToolStripMenuItem1.Name = "高血压患者血压控制率ToolStripMenuItem1";
            this.高血压患者血压控制率ToolStripMenuItem1.Size = new System.Drawing.Size(200, 26);
            this.高血压患者血压控制率ToolStripMenuItem1.Text = "高血压患者血压控制率";
            this.高血压患者血压控制率ToolStripMenuItem1.Click += new System.EventHandler(this.高血压患者血压控制率ToolStripMenuItem_Click);
            // 
            // 糖尿病患者血糖控制率ToolStripMenuItem1
            // 
            this.糖尿病患者血糖控制率ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.糖尿病患者血糖控制率ToolStripMenuItem1.Name = "糖尿病患者血糖控制率ToolStripMenuItem1";
            this.糖尿病患者血糖控制率ToolStripMenuItem1.Size = new System.Drawing.Size(200, 26);
            this.糖尿病患者血糖控制率ToolStripMenuItem1.Text = "糖尿病患者血糖控制率";
            this.糖尿病患者血糖控制率ToolStripMenuItem1.Click += new System.EventHandler(this.糖尿病患者血糖控制率ToolStripMenuItem_Click);
            // 
            // 管理率统计分析ToolStripMenuItem2
            // 
            this.管理率统计分析ToolStripMenuItem2.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.管理率统计分析ToolStripMenuItem2.Name = "管理率统计分析ToolStripMenuItem2";
            this.管理率统计分析ToolStripMenuItem2.Size = new System.Drawing.Size(200, 26);
            this.管理率统计分析ToolStripMenuItem2.Text = "管理率统计分析";
            this.管理率统计分析ToolStripMenuItem2.Click += new System.EventHandler(this.管理率统计分析ToolStripMenuItem_Click);
            // 
            // 高血压高危人群达标率ToolStripMenuItem
            // 
            this.高血压高危人群达标率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.高血压高危人群达标率ToolStripMenuItem.Name = "高血压高危人群达标率ToolStripMenuItem";
            this.高血压高危人群达标率ToolStripMenuItem.Size = new System.Drawing.Size(200, 26);
            this.高血压高危人群达标率ToolStripMenuItem.Text = "高血压高危人群达标率";
            this.高血压高危人群达标率ToolStripMenuItem.Click += new System.EventHandler(this.高血压高危人群达标率ToolStripMenuItem_Click);
            // 
            // 孕产妇统计查询ToolStripMenuItem
            // 
            this.孕产妇统计查询ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.孕产妇统计分析ToolStripMenuItem1,
            this.早孕建册率统计ToolStripMenuItem,
            this.妇保院工作量统计ToolStripMenuItem});
            this.孕产妇统计查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.孕产妇统计查询ToolStripMenuItem.Name = "孕产妇统计查询ToolStripMenuItem";
            this.孕产妇统计查询ToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.孕产妇统计查询ToolStripMenuItem.Text = "孕产妇统计查询";
            // 
            // 孕产妇统计分析ToolStripMenuItem1
            // 
            this.孕产妇统计分析ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.孕产妇统计分析ToolStripMenuItem1.Name = "孕产妇统计分析ToolStripMenuItem1";
            this.孕产妇统计分析ToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.孕产妇统计分析ToolStripMenuItem1.Text = "孕产妇统计分析";
            this.孕产妇统计分析ToolStripMenuItem1.Click += new System.EventHandler(this.孕产妇统计分析ToolStripMenuItem_Click);
            // 
            // 早孕建册率统计ToolStripMenuItem
            // 
            this.早孕建册率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.早孕建册率统计ToolStripMenuItem.Name = "早孕建册率统计ToolStripMenuItem";
            this.早孕建册率统计ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.早孕建册率统计ToolStripMenuItem.Text = "早孕建册率统计";
            this.早孕建册率统计ToolStripMenuItem.Click += new System.EventHandler(this.孕产妇早孕建册率ToolStripMenuItem_Click);
            // 
            // 妇保院工作量统计ToolStripMenuItem
            // 
            this.妇保院工作量统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.妇保院工作量统计ToolStripMenuItem.Name = "妇保院工作量统计ToolStripMenuItem";
            this.妇保院工作量统计ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.妇保院工作量统计ToolStripMenuItem.Text = "妇保院工作量统计";
            this.妇保院工作量统计ToolStripMenuItem.Click += new System.EventHandler(this.妇保院数据统计ToolStripMenuItem_Click);
            // 
            // 儿童统计查询ToolStripMenuItem
            // 
            this.儿童统计查询ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.岁儿童统计分析ToolStripMenuItem1,
            this.岁儿童系统管理率ToolStripMenuItem,
            this.妇保院数据统计儿童ToolStripMenuItem});
            this.儿童统计查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.folder;
            this.儿童统计查询ToolStripMenuItem.Name = "儿童统计查询ToolStripMenuItem";
            this.儿童统计查询ToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.儿童统计查询ToolStripMenuItem.Text = "0-6岁儿童统计查询";
            // 
            // 岁儿童统计分析ToolStripMenuItem1
            // 
            this.岁儿童统计分析ToolStripMenuItem1.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.岁儿童统计分析ToolStripMenuItem1.Name = "岁儿童统计分析ToolStripMenuItem1";
            this.岁儿童统计分析ToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.岁儿童统计分析ToolStripMenuItem1.Text = "0-6岁儿童统计分析";
            this.岁儿童统计分析ToolStripMenuItem1.Click += new System.EventHandler(this.岁儿童统计分析ToolStripMenuItem_Click);
            // 
            // 岁儿童系统管理率ToolStripMenuItem
            // 
            this.岁儿童系统管理率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.岁儿童系统管理率ToolStripMenuItem.Name = "岁儿童系统管理率ToolStripMenuItem";
            this.岁儿童系统管理率ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.岁儿童系统管理率ToolStripMenuItem.Text = "0-6岁儿童系统管理率";
            this.岁儿童系统管理率ToolStripMenuItem.Click += new System.EventHandler(this.岁儿童管理率ToolStripMenuItem_Click);
            // 
            // 妇保院数据统计儿童ToolStripMenuItem
            // 
            this.妇保院数据统计儿童ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_bar;
            this.妇保院数据统计儿童ToolStripMenuItem.Name = "妇保院数据统计儿童ToolStripMenuItem";
            this.妇保院数据统计儿童ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.妇保院数据统计儿童ToolStripMenuItem.Text = "妇保院工作量统计儿童";
            this.妇保院数据统计儿童ToolStripMenuItem.Click += new System.EventHandler(this.妇保院数据统计儿童ToolStripMenuItem_Click);
            // 
            // 常用统计功能ToolStripMenuItem
            // 
            this.常用统计功能ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.岁儿童统计分析ToolStripMenuItem,
            this.岁儿童管理率ToolStripMenuItem,
            this.孕产妇早孕建册率ToolStripMenuItem,
            this.孕产妇统计分析ToolStripMenuItem,
            this.老年人月报表ToolStripMenuItem,
            this.慢性病人群统计分析ToolStripMenuItem,
            this.管理率统计分析ToolStripMenuItem,
            this.糖尿病患者血糖控制率ToolStripMenuItem,
            this.高血压患者血压控制率ToolStripMenuItem,
            this.hIS慢性病查询ToolStripMenuItem,
            this.档案动态使用率统计ToolStripMenuItem,
            this.档案重复数据统计ToolStripMenuItem,
            this.妇保院数据统计ToolStripMenuItem,
            this.档案复核统计ToolStripMenuItem,
            this.贫困人口体检统计ToolStripMenuItem});
            this.常用统计功能ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.常用统计功能ToolStripMenuItem.Name = "常用统计功能ToolStripMenuItem";
            this.常用统计功能ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.常用统计功能ToolStripMenuItem.Text = "常用统计功能";
            // 
            // 岁儿童统计分析ToolStripMenuItem
            // 
            this.岁儿童统计分析ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.岁儿童统计分析ToolStripMenuItem.Name = "岁儿童统计分析ToolStripMenuItem";
            this.岁儿童统计分析ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.岁儿童统计分析ToolStripMenuItem.Text = "0-6岁儿童统计分析";
            this.岁儿童统计分析ToolStripMenuItem.Click += new System.EventHandler(this.岁儿童统计分析ToolStripMenuItem_Click);
            // 
            // 岁儿童管理率ToolStripMenuItem
            // 
            this.岁儿童管理率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.岁儿童管理率ToolStripMenuItem.Name = "岁儿童管理率ToolStripMenuItem";
            this.岁儿童管理率ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.岁儿童管理率ToolStripMenuItem.Text = "0-6岁儿童系统管理率";
            this.岁儿童管理率ToolStripMenuItem.Click += new System.EventHandler(this.岁儿童管理率ToolStripMenuItem_Click);
            // 
            // 孕产妇早孕建册率ToolStripMenuItem
            // 
            this.孕产妇早孕建册率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.孕产妇早孕建册率ToolStripMenuItem.Name = "孕产妇早孕建册率ToolStripMenuItem";
            this.孕产妇早孕建册率ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.孕产妇早孕建册率ToolStripMenuItem.Text = "孕产妇早孕建册/系统管理率";
            this.孕产妇早孕建册率ToolStripMenuItem.Click += new System.EventHandler(this.孕产妇早孕建册率ToolStripMenuItem_Click);
            // 
            // 孕产妇统计分析ToolStripMenuItem
            // 
            this.孕产妇统计分析ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.孕产妇统计分析ToolStripMenuItem.Name = "孕产妇统计分析ToolStripMenuItem";
            this.孕产妇统计分析ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.孕产妇统计分析ToolStripMenuItem.Text = "孕产妇统计分析";
            this.孕产妇统计分析ToolStripMenuItem.Click += new System.EventHandler(this.孕产妇统计分析ToolStripMenuItem_Click);
            // 
            // 老年人月报表ToolStripMenuItem
            // 
            this.老年人月报表ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.老年人月报表ToolStripMenuItem.Name = "老年人月报表ToolStripMenuItem";
            this.老年人月报表ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.老年人月报表ToolStripMenuItem.Text = "老年人月报表";
            this.老年人月报表ToolStripMenuItem.Click += new System.EventHandler(this.老年人月报表ToolStripMenuItem_Click);
            // 
            // 慢性病人群统计分析ToolStripMenuItem
            // 
            this.慢性病人群统计分析ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.慢性病人群统计分析ToolStripMenuItem.Name = "慢性病人群统计分析ToolStripMenuItem";
            this.慢性病人群统计分析ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.慢性病人群统计分析ToolStripMenuItem.Text = "慢性病人群统计分析";
            this.慢性病人群统计分析ToolStripMenuItem.Click += new System.EventHandler(this.慢性病人群统计分析ToolStripMenuItem_Click);
            // 
            // 管理率统计分析ToolStripMenuItem
            // 
            this.管理率统计分析ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.管理率统计分析ToolStripMenuItem.Name = "管理率统计分析ToolStripMenuItem";
            this.管理率统计分析ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.管理率统计分析ToolStripMenuItem.Text = "管理率统计分析";
            this.管理率统计分析ToolStripMenuItem.Click += new System.EventHandler(this.管理率统计分析ToolStripMenuItem_Click);
            // 
            // 糖尿病患者血糖控制率ToolStripMenuItem
            // 
            this.糖尿病患者血糖控制率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.糖尿病患者血糖控制率ToolStripMenuItem.Name = "糖尿病患者血糖控制率ToolStripMenuItem";
            this.糖尿病患者血糖控制率ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.糖尿病患者血糖控制率ToolStripMenuItem.Text = "糖尿病患者血糖控制率";
            this.糖尿病患者血糖控制率ToolStripMenuItem.Click += new System.EventHandler(this.糖尿病患者血糖控制率ToolStripMenuItem_Click);
            // 
            // 高血压患者血压控制率ToolStripMenuItem
            // 
            this.高血压患者血压控制率ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.高血压患者血压控制率ToolStripMenuItem.Name = "高血压患者血压控制率ToolStripMenuItem";
            this.高血压患者血压控制率ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.高血压患者血压控制率ToolStripMenuItem.Text = "高血压患者血压控制率";
            this.高血压患者血压控制率ToolStripMenuItem.Click += new System.EventHandler(this.高血压患者血压控制率ToolStripMenuItem_Click);
            // 
            // hIS慢性病查询ToolStripMenuItem
            // 
            this.hIS慢性病查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.hIS慢性病查询ToolStripMenuItem.Name = "hIS慢性病查询ToolStripMenuItem";
            this.hIS慢性病查询ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.hIS慢性病查询ToolStripMenuItem.Text = "HIS慢性病查询";
            this.hIS慢性病查询ToolStripMenuItem.Click += new System.EventHandler(this.hIS慢性病查询ToolStripMenuItem_Click);
            // 
            // 档案动态使用率统计ToolStripMenuItem
            // 
            this.档案动态使用率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.档案动态使用率统计ToolStripMenuItem.Name = "档案动态使用率统计ToolStripMenuItem";
            this.档案动态使用率统计ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.档案动态使用率统计ToolStripMenuItem.Text = "档案动态使用率统计";
            this.档案动态使用率统计ToolStripMenuItem.Click += new System.EventHandler(this.档案动态使用率统计ToolStripMenuItem_Click);
            // 
            // 档案重复数据统计ToolStripMenuItem
            // 
            this.档案重复数据统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.档案重复数据统计ToolStripMenuItem.Name = "档案重复数据统计ToolStripMenuItem";
            this.档案重复数据统计ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.档案重复数据统计ToolStripMenuItem.Text = "档案重复数据统计";
            this.档案重复数据统计ToolStripMenuItem.Click += new System.EventHandler(this.档案重复数据统计ToolStripMenuItem_Click);
            // 
            // 妇保院数据统计ToolStripMenuItem
            // 
            this.妇保院数据统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.妇保院数据统计ToolStripMenuItem.Name = "妇保院数据统计ToolStripMenuItem";
            this.妇保院数据统计ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.妇保院数据统计ToolStripMenuItem.Text = "妇保院工作量统计";
            this.妇保院数据统计ToolStripMenuItem.Click += new System.EventHandler(this.妇保院数据统计ToolStripMenuItem_Click);
            // 
            // 档案复核统计ToolStripMenuItem
            // 
            this.档案复核统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.档案复核统计ToolStripMenuItem.Name = "档案复核统计ToolStripMenuItem";
            this.档案复核统计ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.档案复核统计ToolStripMenuItem.Text = "档案复核统计";
            this.档案复核统计ToolStripMenuItem.Click += new System.EventHandler(this.MenuItem档案复核统计_Click);
            // 
            // 贫困人口体检统计ToolStripMenuItem
            // 
            this.贫困人口体检统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.briefcase;
            this.贫困人口体检统计ToolStripMenuItem.Name = "贫困人口体检统计ToolStripMenuItem";
            this.贫困人口体检统计ToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.贫困人口体检统计ToolStripMenuItem.Text = "贫困人口体检统计";
            this.贫困人口体检统计ToolStripMenuItem.Click += new System.EventHandler(this.贫困人口体检统计ToolStripMenuItem_Click);
            // 
            // wis联合统计ToolStripMenuItem
            // 
            this.wis联合统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.儿童系统管理率统计ToolStripMenuItem,
            this.孕产妇系统管理率统计ToolStripMenuItem,
            this.wis孕妇信息查询ToolStripMenuItem,
            this.wis儿童信息查询ToolStripMenuItem,
            this.出生数据统计ToolStripMenuItem,
            this.孕情数据统计ToolStripMenuItem});
            this.wis联合统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.chart_pie;
            this.wis联合统计ToolStripMenuItem.Name = "wis联合统计ToolStripMenuItem";
            this.wis联合统计ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.wis联合统计ToolStripMenuItem.Text = "Wis联合统计";
            // 
            // 儿童系统管理率统计ToolStripMenuItem
            // 
            this.儿童系统管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.儿童系统管理率统计ToolStripMenuItem.Name = "儿童系统管理率统计ToolStripMenuItem";
            this.儿童系统管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.儿童系统管理率统计ToolStripMenuItem.Text = "儿童系统管理率统计";
            this.儿童系统管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.儿童系统管理率统计ToolStripMenuItem_Click);
            // 
            // 孕产妇系统管理率统计ToolStripMenuItem
            // 
            this.孕产妇系统管理率统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.孕产妇系统管理率统计ToolStripMenuItem.Name = "孕产妇系统管理率统计ToolStripMenuItem";
            this.孕产妇系统管理率统计ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.孕产妇系统管理率统计ToolStripMenuItem.Text = "孕产妇系统管理率统计";
            this.孕产妇系统管理率统计ToolStripMenuItem.Click += new System.EventHandler(this.孕产妇系统管理率统计ToolStripMenuItem_Click);
            // 
            // wis孕妇信息查询ToolStripMenuItem
            // 
            this.wis孕妇信息查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.wis孕妇信息查询ToolStripMenuItem.Name = "wis孕妇信息查询ToolStripMenuItem";
            this.wis孕妇信息查询ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.wis孕妇信息查询ToolStripMenuItem.Text = "Wis孕妇信息查询";
            this.wis孕妇信息查询ToolStripMenuItem.Click += new System.EventHandler(this.wis孕妇信息查询ToolStripMenuItem_Click);
            // 
            // wis儿童信息查询ToolStripMenuItem
            // 
            this.wis儿童信息查询ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.wis儿童信息查询ToolStripMenuItem.Name = "wis儿童信息查询ToolStripMenuItem";
            this.wis儿童信息查询ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.wis儿童信息查询ToolStripMenuItem.Text = "Wis儿童信息查询";
            this.wis儿童信息查询ToolStripMenuItem.Click += new System.EventHandler(this.wis儿童信息查询ToolStripMenuItem_Click);
            // 
            // 出生数据统计ToolStripMenuItem
            // 
            this.出生数据统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.出生数据统计ToolStripMenuItem.Name = "出生数据统计ToolStripMenuItem";
            this.出生数据统计ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.出生数据统计ToolStripMenuItem.Text = "出生数据统计";
            this.出生数据统计ToolStripMenuItem.Click += new System.EventHandler(this.出生数据统计ToolStripMenuItem_Click);
            // 
            // 孕情数据统计ToolStripMenuItem
            // 
            this.孕情数据统计ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.孕情数据统计ToolStripMenuItem.Name = "孕情数据统计ToolStripMenuItem";
            this.孕情数据统计ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.孕情数据统计ToolStripMenuItem.Text = "孕情数据统计";
            this.孕情数据统计ToolStripMenuItem.Click += new System.EventHandler(this.孕情数据统计ToolStripMenuItem_Click);
            // 
            // 医疗信息ToolStripMenuItem
            // 
            this.医疗信息ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.诊疗记录ToolStripMenuItem,
            this.化验结果ToolStripMenuItem,
            this.影像结果ToolStripMenuItem,
            this.天士力手表数据ToolStripMenuItem});
            this.医疗信息ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.user_card;
            this.医疗信息ToolStripMenuItem.Name = "医疗信息ToolStripMenuItem";
            this.医疗信息ToolStripMenuItem.Size = new System.Drawing.Size(88, 24);
            this.医疗信息ToolStripMenuItem.Text = "医疗信息";
            // 
            // 诊疗记录ToolStripMenuItem
            // 
            this.诊疗记录ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.toggle_expand_alt;
            this.诊疗记录ToolStripMenuItem.Name = "诊疗记录ToolStripMenuItem";
            this.诊疗记录ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.诊疗记录ToolStripMenuItem.Text = "诊疗记录";
            this.诊疗记录ToolStripMenuItem.Click += new System.EventHandler(this.诊疗记录ToolStripMenuItem_Click);
            // 
            // 化验结果ToolStripMenuItem
            // 
            this.化验结果ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.application;
            this.化验结果ToolStripMenuItem.Name = "化验结果ToolStripMenuItem";
            this.化验结果ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.化验结果ToolStripMenuItem.Text = "化验结果";
            this.化验结果ToolStripMenuItem.Click += new System.EventHandler(this.化验结果ToolStripMenuItem_Click);
            // 
            // 影像结果ToolStripMenuItem
            // 
            this.影像结果ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.archive;
            this.影像结果ToolStripMenuItem.Name = "影像结果ToolStripMenuItem";
            this.影像结果ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.影像结果ToolStripMenuItem.Text = "影像结果";
            this.影像结果ToolStripMenuItem.Click += new System.EventHandler(this.影像结果ToolStripMenuItem_Click);
            // 
            // 天士力手表数据ToolStripMenuItem
            // 
            this.天士力手表数据ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.archive;
            this.天士力手表数据ToolStripMenuItem.Name = "天士力手表数据ToolStripMenuItem";
            this.天士力手表数据ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.天士力手表数据ToolStripMenuItem.Text = "天士力手表数据";
            this.天士力手表数据ToolStripMenuItem.Click += new System.EventHandler(this.天士力手表数据ToolStripMenuItem_Click);
            // 
            // Menu协同办公
            // 
            this.Menu协同办公.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu通知通告管理,
            this.解密姓名ToolStripMenuItem});
            this.Menu协同办公.Image = global::AtomEHR.公共卫生.Properties.Resources.monitor;
            this.Menu协同办公.Name = "Menu协同办公";
            this.Menu协同办公.Size = new System.Drawing.Size(88, 24);
            this.Menu协同办公.Text = "协同办公";
            // 
            // Menu通知通告管理
            // 
            this.Menu通知通告管理.Image = global::AtomEHR.公共卫生.Properties.Resources.document;
            this.Menu通知通告管理.Name = "Menu通知通告管理";
            this.Menu通知通告管理.Size = new System.Drawing.Size(148, 22);
            this.Menu通知通告管理.Text = "通知通告管理";
            // 
            // 解密姓名ToolStripMenuItem
            // 
            this.解密姓名ToolStripMenuItem.Image = global::AtomEHR.公共卫生.Properties.Resources.safe;
            this.解密姓名ToolStripMenuItem.Name = "解密姓名ToolStripMenuItem";
            this.解密姓名ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.解密姓名ToolStripMenuItem.Text = "解密姓名";
            this.解密姓名ToolStripMenuItem.Click += new System.EventHandler(this.解密姓名ToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.35829F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.64171F));
            this.tableLayoutPanel1.Controls.Add(this.groupControl4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupControl3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupControl2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 30);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.17773F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.82227F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 261F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(986, 439);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.xtraTabControl2);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(391, 227);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(592, 209);
            this.groupControl4.TabIndex = 8;
            this.groupControl4.Text = "工作提醒/工作报表 ";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(2, 22);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTab工作提醒;
            this.xtraTabControl2.Size = new System.Drawing.Size(588, 185);
            this.xtraTabControl2.TabIndex = 1;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTab工作提醒,
            this.xtraTab工作报表});
            // 
            // xtraTab工作提醒
            // 
            this.xtraTab工作提醒.Controls.Add(this.gc工作提醒);
            this.xtraTab工作提醒.Name = "xtraTab工作提醒";
            this.xtraTab工作提醒.Size = new System.Drawing.Size(582, 156);
            this.xtraTab工作提醒.Text = "工作提醒";
            // 
            // gc工作提醒
            // 
            this.gc工作提醒.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc工作提醒.Location = new System.Drawing.Point(0, 0);
            this.gc工作提醒.MainView = this.gv工作提醒;
            this.gc工作提醒.Name = "gc工作提醒";
            this.gc工作提醒.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link未随访,
            this.link今日随访,
            this.link明日随访,
            this.link本周随访,
            this.link未体检,
            this.link已体检});
            this.gc工作提醒.Size = new System.Drawing.Size(582, 156);
            this.gc工作提醒.TabIndex = 0;
            this.gc工作提醒.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv工作提醒});
            // 
            // gv工作提醒
            // 
            this.gv工作提醒.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col重点人群,
            this.col本季度随访,
            this.col今日随访,
            this.col明日随访,
            this.col本周随访,
            this.col未体检,
            this.col已体检});
            this.gv工作提醒.GridControl = this.gc工作提醒;
            this.gv工作提醒.Name = "gv工作提醒";
            this.gv工作提醒.OptionsView.ShowGroupPanel = false;
            this.gv工作提醒.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gv工作提醒_CustomRowCellEdit);
            // 
            // col重点人群
            // 
            this.col重点人群.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.col重点人群.AppearanceCell.Options.UseBackColor = true;
            this.col重点人群.AppearanceCell.Options.UseTextOptions = true;
            this.col重点人群.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col重点人群.AppearanceHeader.Options.UseTextOptions = true;
            this.col重点人群.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col重点人群.Caption = "重点人群";
            this.col重点人群.FieldName = "随访对象";
            this.col重点人群.Name = "col重点人群";
            this.col重点人群.OptionsColumn.ReadOnly = true;
            this.col重点人群.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "随访对象", "合计")});
            this.col重点人群.Visible = true;
            this.col重点人群.VisibleIndex = 0;
            // 
            // col本季度随访
            // 
            this.col本季度随访.AppearanceCell.Options.UseTextOptions = true;
            this.col本季度随访.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本季度随访.AppearanceHeader.Options.UseTextOptions = true;
            this.col本季度随访.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本季度随访.Caption = "本季度随访";
            this.col本季度随访.FieldName = "本季度随访";
            this.col本季度随访.Name = "col本季度随访";
            this.col本季度随访.OptionsColumn.ReadOnly = true;
            this.col本季度随访.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "未随访", "{0:n0}")});
            this.col本季度随访.Visible = true;
            this.col本季度随访.VisibleIndex = 1;
            // 
            // col今日随访
            // 
            this.col今日随访.AppearanceCell.Options.UseTextOptions = true;
            this.col今日随访.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col今日随访.AppearanceHeader.Options.UseTextOptions = true;
            this.col今日随访.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col今日随访.Caption = "今日随访";
            this.col今日随访.FieldName = "今日随访";
            this.col今日随访.Name = "col今日随访";
            this.col今日随访.OptionsColumn.ReadOnly = true;
            this.col今日随访.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "今日随访", "{0:n0}")});
            this.col今日随访.Visible = true;
            this.col今日随访.VisibleIndex = 2;
            // 
            // col明日随访
            // 
            this.col明日随访.AppearanceCell.Options.UseTextOptions = true;
            this.col明日随访.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col明日随访.AppearanceHeader.Options.UseTextOptions = true;
            this.col明日随访.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col明日随访.Caption = "明日随访";
            this.col明日随访.FieldName = "明日随访";
            this.col明日随访.Name = "col明日随访";
            this.col明日随访.OptionsColumn.ReadOnly = true;
            this.col明日随访.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "明日随访", "{0:n0}")});
            this.col明日随访.Visible = true;
            this.col明日随访.VisibleIndex = 3;
            // 
            // col本周随访
            // 
            this.col本周随访.AppearanceCell.Options.UseTextOptions = true;
            this.col本周随访.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本周随访.AppearanceHeader.Options.UseTextOptions = true;
            this.col本周随访.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本周随访.Caption = "本周随访";
            this.col本周随访.FieldName = "本周随访";
            this.col本周随访.Name = "col本周随访";
            this.col本周随访.OptionsColumn.ReadOnly = true;
            this.col本周随访.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "本周随访", "{0:n0}")});
            this.col本周随访.Visible = true;
            this.col本周随访.VisibleIndex = 4;
            // 
            // col未体检
            // 
            this.col未体检.AppearanceCell.Options.UseTextOptions = true;
            this.col未体检.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col未体检.AppearanceHeader.Options.UseTextOptions = true;
            this.col未体检.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col未体检.Caption = "未体检";
            this.col未体检.FieldName = "未体检";
            this.col未体检.Name = "col未体检";
            this.col未体检.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "未体检", "{0:n0}")});
            // 
            // col已体检
            // 
            this.col已体检.AppearanceCell.Options.UseTextOptions = true;
            this.col已体检.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col已体检.AppearanceHeader.Options.UseTextOptions = true;
            this.col已体检.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col已体检.Caption = "已体检";
            this.col已体检.FieldName = "已体检";
            this.col已体检.Name = "col已体检";
            this.col已体检.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "已体检", "{0:n0}")});
            // 
            // link未随访
            // 
            this.link未随访.AutoHeight = false;
            this.link未随访.Name = "link未随访";
            this.link未随访.Click += new System.EventHandler(this.link未随访_Click);
            // 
            // link今日随访
            // 
            this.link今日随访.AutoHeight = false;
            this.link今日随访.Name = "link今日随访";
            this.link今日随访.Click += new System.EventHandler(this.link今日随访_Click);
            // 
            // link明日随访
            // 
            this.link明日随访.AutoHeight = false;
            this.link明日随访.Name = "link明日随访";
            this.link明日随访.Click += new System.EventHandler(this.link明日随访_Click);
            // 
            // link本周随访
            // 
            this.link本周随访.AutoHeight = false;
            this.link本周随访.Name = "link本周随访";
            this.link本周随访.Click += new System.EventHandler(this.link本周随访_Click);
            // 
            // link未体检
            // 
            this.link未体检.AutoHeight = false;
            this.link未体检.Name = "link未体检";
            this.link未体检.Click += new System.EventHandler(this.link未体检_Click);
            // 
            // link已体检
            // 
            this.link已体检.AutoHeight = false;
            this.link已体检.Name = "link已体检";
            this.link已体检.Click += new System.EventHandler(this.link已体检_Click);
            // 
            // xtraTab工作报表
            // 
            this.xtraTab工作报表.Controls.Add(this.link精神疾病随访表统计);
            this.xtraTab工作报表.Controls.Add(this.link重型精神病补充表统计);
            this.xtraTab工作报表.Controls.Add(this.link糖尿病管理卡统计);
            this.xtraTab工作报表.Controls.Add(this.link糖尿病随访统计);
            this.xtraTab工作报表.Controls.Add(this.link高血压随访统计);
            this.xtraTab工作报表.Controls.Add(this.link老年人随访统计);
            this.xtraTab工作报表.Controls.Add(this.link高血压管理卡统计);
            this.xtraTab工作报表.Controls.Add(this.link孕产妇基本信息统计);
            this.xtraTab工作报表.Controls.Add(this.hyperLinkEdit2);
            this.xtraTab工作报表.Controls.Add(this.link儿童基本信息统计);
            this.xtraTab工作报表.Controls.Add(this.link个人基本信息统计);
            this.xtraTab工作报表.Name = "xtraTab工作报表";
            this.xtraTab工作报表.Size = new System.Drawing.Size(396, 133);
            this.xtraTab工作报表.Text = "工作报表";
            // 
            // link精神疾病随访表统计
            // 
            this.link精神疾病随访表统计.EditValue = "精神疾病随访表统计";
            this.link精神疾病随访表统计.Location = new System.Drawing.Point(147, 93);
            this.link精神疾病随访表统计.Name = "link精神疾病随访表统计";
            this.link精神疾病随访表统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link精神疾病随访表统计.Size = new System.Drawing.Size(114, 20);
            this.link精神疾病随访表统计.TabIndex = 10;
            this.link精神疾病随访表统计.Click += new System.EventHandler(this.link精神疾病随访表统计_Click);
            // 
            // link重型精神病补充表统计
            // 
            this.link重型精神病补充表统计.EditValue = "重型精神病补充表统计";
            this.link重型精神病补充表统计.Location = new System.Drawing.Point(13, 93);
            this.link重型精神病补充表统计.Name = "link重型精神病补充表统计";
            this.link重型精神病补充表统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link重型精神病补充表统计.Size = new System.Drawing.Size(128, 20);
            this.link重型精神病补充表统计.TabIndex = 9;
            this.link重型精神病补充表统计.Click += new System.EventHandler(this.link重型精神病补充表统计_Click);
            // 
            // link糖尿病管理卡统计
            // 
            this.link糖尿病管理卡统计.EditValue = "糖尿病管理卡统计";
            this.link糖尿病管理卡统计.Location = new System.Drawing.Point(147, 67);
            this.link糖尿病管理卡统计.Name = "link糖尿病管理卡统计";
            this.link糖尿病管理卡统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link糖尿病管理卡统计.Size = new System.Drawing.Size(114, 20);
            this.link糖尿病管理卡统计.TabIndex = 8;
            this.link糖尿病管理卡统计.Click += new System.EventHandler(this.link糖尿病管理卡统计_Click);
            // 
            // link糖尿病随访统计
            // 
            this.link糖尿病随访统计.EditValue = "糖尿病随访统计";
            this.link糖尿病随访统计.Location = new System.Drawing.Point(267, 67);
            this.link糖尿病随访统计.Name = "link糖尿病随访统计";
            this.link糖尿病随访统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link糖尿病随访统计.Size = new System.Drawing.Size(114, 20);
            this.link糖尿病随访统计.TabIndex = 7;
            this.link糖尿病随访统计.Click += new System.EventHandler(this.link糖尿病随访统计_Click);
            // 
            // link高血压随访统计
            // 
            this.link高血压随访统计.EditValue = "高血压随访统计";
            this.link高血压随访统计.Location = new System.Drawing.Point(13, 67);
            this.link高血压随访统计.Name = "link高血压随访统计";
            this.link高血压随访统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link高血压随访统计.Size = new System.Drawing.Size(128, 20);
            this.link高血压随访统计.TabIndex = 6;
            this.link高血压随访统计.Click += new System.EventHandler(this.link高血压随访统计_Click);
            // 
            // link老年人随访统计
            // 
            this.link老年人随访统计.EditValue = "老年人随访统计";
            this.link老年人随访统计.Location = new System.Drawing.Point(147, 41);
            this.link老年人随访统计.Name = "link老年人随访统计";
            this.link老年人随访统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link老年人随访统计.Size = new System.Drawing.Size(114, 20);
            this.link老年人随访统计.TabIndex = 5;
            this.link老年人随访统计.Click += new System.EventHandler(this.link老年人随访统计_Click);
            // 
            // link高血压管理卡统计
            // 
            this.link高血压管理卡统计.EditValue = "高血压管理卡统计";
            this.link高血压管理卡统计.Location = new System.Drawing.Point(267, 41);
            this.link高血压管理卡统计.Name = "link高血压管理卡统计";
            this.link高血压管理卡统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link高血压管理卡统计.Size = new System.Drawing.Size(114, 20);
            this.link高血压管理卡统计.TabIndex = 4;
            this.link高血压管理卡统计.Click += new System.EventHandler(this.link高血压管理卡统计_Click);
            // 
            // link孕产妇基本信息统计
            // 
            this.link孕产妇基本信息统计.EditValue = "孕产妇基本信息统计";
            this.link孕产妇基本信息统计.Location = new System.Drawing.Point(13, 41);
            this.link孕产妇基本信息统计.Name = "link孕产妇基本信息统计";
            this.link孕产妇基本信息统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link孕产妇基本信息统计.Size = new System.Drawing.Size(128, 20);
            this.link孕产妇基本信息统计.TabIndex = 3;
            this.link孕产妇基本信息统计.Click += new System.EventHandler(this.link孕产妇基本信息统计_Click);
            // 
            // hyperLinkEdit2
            // 
            this.hyperLinkEdit2.EditValue = "健康体检统计";
            this.hyperLinkEdit2.Location = new System.Drawing.Point(147, 15);
            this.hyperLinkEdit2.Name = "hyperLinkEdit2";
            this.hyperLinkEdit2.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.hyperLinkEdit2.Size = new System.Drawing.Size(114, 20);
            this.hyperLinkEdit2.TabIndex = 2;
            this.hyperLinkEdit2.Click += new System.EventHandler(this.健康体检表统计_Click);
            // 
            // link儿童基本信息统计
            // 
            this.link儿童基本信息统计.EditValue = "儿童基本信息统计";
            this.link儿童基本信息统计.Location = new System.Drawing.Point(267, 15);
            this.link儿童基本信息统计.Name = "link儿童基本信息统计";
            this.link儿童基本信息统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link儿童基本信息统计.Size = new System.Drawing.Size(114, 20);
            this.link儿童基本信息统计.TabIndex = 1;
            this.link儿童基本信息统计.Click += new System.EventHandler(this.儿童基本信息_Click);
            // 
            // link个人基本信息统计
            // 
            this.link个人基本信息统计.EditValue = "个人基本信息统计";
            this.link个人基本信息统计.Location = new System.Drawing.Point(13, 15);
            this.link个人基本信息统计.Name = "link个人基本信息统计";
            this.link个人基本信息统计.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.link个人基本信息统计.Size = new System.Drawing.Size(128, 20);
            this.link个人基本信息统计.TabIndex = 0;
            this.link个人基本信息统计.Click += new System.EventHandler(this.link个人基本信息统计_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.xtraTabControl1);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(391, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(592, 218);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "即时消息";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 22);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(588, 194);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            this.xtraTabControl1.Click += new System.EventHandler(this.xtraTabControl1_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gc待转出档案);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(582, 165);
            this.xtraTabPage2.Text = "待转出转档";
            // 
            // gc待转出档案
            // 
            this.gc待转出档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc待转出档案.Location = new System.Drawing.Point(0, 0);
            this.gc待转出档案.MainView = this.gv带转出档案;
            this.gc待转出档案.Name = "gc待转出档案";
            this.gc待转出档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btn同意,
            this.btn拒绝,
            this.link个人档案编号});
            this.gc待转出档案.Size = new System.Drawing.Size(582, 165);
            this.gc待转出档案.TabIndex = 1;
            this.gc待转出档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv带转出档案});
            // 
            // gv带转出档案
            // 
            this.gv带转出档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn6,
            this.gridColumn8,
            this.gridColumn7,
            this.gridColumn2,
            this.gridColumn24,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn22,
            this.gridColumn23});
            this.gv带转出档案.GridControl = this.gc待转出档案;
            this.gv带转出档案.Name = "gv带转出档案";
            this.gv带转出档案.OptionsView.ColumnAutoWidth = false;
            this.gv带转出档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "个人档案编号";
            this.gridColumn1.ColumnEdit = this.link个人档案编号;
            this.gridColumn1.FieldName = "个人档案编号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 81;
            // 
            // link个人档案编号
            // 
            this.link个人档案编号.AutoHeight = false;
            this.link个人档案编号.Name = "link个人档案编号";
            this.link个人档案编号.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "姓名";
            this.gridColumn6.FieldName = "姓名";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "性别";
            this.gridColumn8.FieldName = "性别";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "当前所属机构";
            this.gridColumn7.FieldName = "所属机构";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 81;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "转入机构";
            this.gridColumn2.FieldName = "转入机构";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 4;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "转档原因";
            this.gridColumn24.FieldName = "备注";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 5;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "转入详细地址";
            this.gridColumn4.FieldName = "转入详细地址";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 6;
            this.gridColumn4.Width = 81;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "创建人";
            this.gridColumn5.FieldName = "创建人";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "同意";
            this.gridColumn22.ColumnEdit = this.btn同意;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 7;
            this.gridColumn22.Width = 67;
            // 
            // btn同意
            // 
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn同意.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "同意", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btn同意.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn同意.Name = "btn同意";
            this.btn同意.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn同意.Click += new System.EventHandler(this.btn同意_Click);
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "拒绝";
            this.gridColumn23.ColumnEdit = this.btn拒绝;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 8;
            this.gridColumn23.Width = 69;
            // 
            // btn拒绝
            // 
            this.btn拒绝.AutoHeight = false;
            this.btn拒绝.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "拒绝", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btn拒绝.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn拒绝.Name = "btn拒绝";
            this.btn拒绝.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn拒绝.Click += new System.EventHandler(this.btn拒绝_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(396, 142);
            this.xtraTabPage1.Text = "通知公告";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gc自动转出档案);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(396, 142);
            this.xtraTabPage3.Text = "自动转出档案";
            // 
            // gc自动转出档案
            // 
            this.gc自动转出档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc自动转出档案.Location = new System.Drawing.Point(0, 0);
            this.gc自动转出档案.MainView = this.gv自动转出档案;
            this.gc自动转出档案.Name = "gc自动转出档案";
            this.gc自动转出档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc自动转出档案.Size = new System.Drawing.Size(396, 142);
            this.gc自动转出档案.TabIndex = 2;
            this.gc自动转出档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv自动转出档案});
            // 
            // gv自动转出档案
            // 
            this.gv自动转出档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gv自动转出档案.GridControl = this.gc自动转出档案;
            this.gv自动转出档案.Name = "gv自动转出档案";
            this.gv自动转出档案.OptionsView.ColumnAutoWidth = false;
            this.gv自动转出档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "个人档案编号";
            this.gridColumn3.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn3.FieldName = "个人档案编号";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "姓名";
            this.gridColumn9.FieldName = "姓名";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "性别";
            this.gridColumn10.FieldName = "性别";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "当前所属机构";
            this.gridColumn11.FieldName = "所属机构";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "转出机构";
            this.gridColumn12.FieldName = "转出机构";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "转入详细地址";
            this.gridColumn13.FieldName = "转入详细地址";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "创建人";
            this.gridColumn14.FieldName = "创建人";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gc自动转入档案);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(396, 142);
            this.xtraTabPage4.Text = "自动转入档案";
            // 
            // gc自动转入档案
            // 
            this.gc自动转入档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc自动转入档案.Location = new System.Drawing.Point(0, 0);
            this.gc自动转入档案.MainView = this.gv自动转入档案;
            this.gc自动转入档案.Name = "gc自动转入档案";
            this.gc自动转入档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit2});
            this.gc自动转入档案.Size = new System.Drawing.Size(396, 142);
            this.gc自动转入档案.TabIndex = 2;
            this.gc自动转入档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv自动转入档案});
            // 
            // gv自动转入档案
            // 
            this.gv自动转入档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21});
            this.gv自动转入档案.GridControl = this.gc自动转入档案;
            this.gv自动转入档案.Name = "gv自动转入档案";
            this.gv自动转入档案.OptionsView.ColumnAutoWidth = false;
            this.gv自动转入档案.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "个人档案编号";
            this.gridColumn15.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn15.FieldName = "个人档案编号";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.link个人档案编号_Click);
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "姓名";
            this.gridColumn16.FieldName = "姓名";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "性别";
            this.gridColumn17.FieldName = "性别";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "当前所属机构";
            this.gridColumn18.FieldName = "所属机构";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 3;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "转出机构";
            this.gridColumn19.FieldName = "转出机构";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 4;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "转入详细地址";
            this.gridColumn20.FieldName = "转入详细地址";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 5;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "创建人";
            this.gridColumn21.FieldName = "创建人";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 6;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.linkLabel2);
            this.groupControl2.Controls.Add(this.linkLabel3);
            this.groupControl2.Controls.Add(this.linkLabel1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(3, 227);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(382, 209);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "友情链接";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(180, 53);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(97, 14);
            this.linkLabel2.TabIndex = 0;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "常见问题汇总>>";
            this.linkLabel2.Click += new System.EventHandler(this.linkLabel2_Click);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(31, 113);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(109, 14);
            this.linkLabel3.TabIndex = 0;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "完善个人身份证>>";
            this.linkLabel3.Click += new System.EventHandler(this.linkLabel3_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(31, 53);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(97, 14);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "沂水卫生论坛>>";
            this.linkLabel1.Click += new System.EventHandler(this.linkLabel1_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.flowLayoutPanel1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(382, 218);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "快捷方式";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.linkLabel健康档案);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel个人体检);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel高血压随访表);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel糖尿病随访表);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel脑卒中随访表);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel冠心病随访表);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel新建户主档案);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel儿童基本信息);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel孕妇基本信息);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel老年人生理自理能力评估表);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel精神疾病患者随访);
            this.flowLayoutPanel1.Controls.Add(this.linkLable转档查询);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel转档申请);
            this.flowLayoutPanel1.Controls.Add(this.linkLabel高血压分级管理);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(378, 194);
            this.flowLayoutPanel1.TabIndex = 79;
            // 
            // linkLabel健康档案
            // 
            this.linkLabel健康档案.AutoSize = true;
            this.linkLabel健康档案.Location = new System.Drawing.Point(3, 5);
            this.linkLabel健康档案.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel健康档案.Name = "linkLabel健康档案";
            this.linkLabel健康档案.Size = new System.Drawing.Size(73, 14);
            this.linkLabel健康档案.TabIndex = 67;
            this.linkLabel健康档案.TabStop = true;
            this.linkLabel健康档案.Text = ">>健康档案";
            this.linkLabel健康档案.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel健康档案_LinkClicked);
            // 
            // linkLabel个人体检
            // 
            this.linkLabel个人体检.AutoSize = true;
            this.linkLabel个人体检.Location = new System.Drawing.Point(82, 5);
            this.linkLabel个人体检.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel个人体检.Name = "linkLabel个人体检";
            this.linkLabel个人体检.Size = new System.Drawing.Size(73, 14);
            this.linkLabel个人体检.TabIndex = 74;
            this.linkLabel个人体检.TabStop = true;
            this.linkLabel个人体检.Text = ">>个人体检";
            this.linkLabel个人体检.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel个人体检_LinkClicked);
            // 
            // linkLabel高血压随访表
            // 
            this.linkLabel高血压随访表.AutoSize = true;
            this.linkLabel高血压随访表.Location = new System.Drawing.Point(161, 5);
            this.linkLabel高血压随访表.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel高血压随访表.Name = "linkLabel高血压随访表";
            this.linkLabel高血压随访表.Size = new System.Drawing.Size(97, 14);
            this.linkLabel高血压随访表.TabIndex = 71;
            this.linkLabel高血压随访表.TabStop = true;
            this.linkLabel高血压随访表.Text = ">>高血压随访表";
            this.linkLabel高血压随访表.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel高血压随访表_LinkClicked);
            // 
            // linkLabel糖尿病随访表
            // 
            this.linkLabel糖尿病随访表.AutoSize = true;
            this.linkLabel糖尿病随访表.Location = new System.Drawing.Point(264, 5);
            this.linkLabel糖尿病随访表.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel糖尿病随访表.Name = "linkLabel糖尿病随访表";
            this.linkLabel糖尿病随访表.Size = new System.Drawing.Size(97, 14);
            this.linkLabel糖尿病随访表.TabIndex = 70;
            this.linkLabel糖尿病随访表.TabStop = true;
            this.linkLabel糖尿病随访表.Text = ">>糖尿病随访表";
            this.linkLabel糖尿病随访表.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel糖尿病随访表_LinkClicked);
            // 
            // linkLabel脑卒中随访表
            // 
            this.linkLabel脑卒中随访表.AutoSize = true;
            this.linkLabel脑卒中随访表.Location = new System.Drawing.Point(3, 29);
            this.linkLabel脑卒中随访表.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel脑卒中随访表.Name = "linkLabel脑卒中随访表";
            this.linkLabel脑卒中随访表.Size = new System.Drawing.Size(97, 14);
            this.linkLabel脑卒中随访表.TabIndex = 79;
            this.linkLabel脑卒中随访表.TabStop = true;
            this.linkLabel脑卒中随访表.Text = ">>脑卒中随访表";
            this.linkLabel脑卒中随访表.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel脑卒中随访表_LinkClicked);
            // 
            // linkLabel冠心病随访表
            // 
            this.linkLabel冠心病随访表.AutoSize = true;
            this.linkLabel冠心病随访表.Location = new System.Drawing.Point(106, 29);
            this.linkLabel冠心病随访表.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel冠心病随访表.Name = "linkLabel冠心病随访表";
            this.linkLabel冠心病随访表.Size = new System.Drawing.Size(97, 14);
            this.linkLabel冠心病随访表.TabIndex = 80;
            this.linkLabel冠心病随访表.TabStop = true;
            this.linkLabel冠心病随访表.Text = ">>冠心病随访表";
            this.linkLabel冠心病随访表.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel冠心病随访表_LinkClicked);
            // 
            // linkLabel新建户主档案
            // 
            this.linkLabel新建户主档案.AutoSize = true;
            this.linkLabel新建户主档案.Location = new System.Drawing.Point(209, 29);
            this.linkLabel新建户主档案.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel新建户主档案.Name = "linkLabel新建户主档案";
            this.linkLabel新建户主档案.Size = new System.Drawing.Size(97, 14);
            this.linkLabel新建户主档案.TabIndex = 75;
            this.linkLabel新建户主档案.TabStop = true;
            this.linkLabel新建户主档案.Text = ">>新建户主档案";
            this.linkLabel新建户主档案.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel新建户主档案_LinkClicked);
            // 
            // linkLabel儿童基本信息
            // 
            this.linkLabel儿童基本信息.AutoSize = true;
            this.linkLabel儿童基本信息.Location = new System.Drawing.Point(3, 53);
            this.linkLabel儿童基本信息.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel儿童基本信息.Name = "linkLabel儿童基本信息";
            this.linkLabel儿童基本信息.Size = new System.Drawing.Size(97, 14);
            this.linkLabel儿童基本信息.TabIndex = 78;
            this.linkLabel儿童基本信息.TabStop = true;
            this.linkLabel儿童基本信息.Text = ">>儿童基本信息";
            this.linkLabel儿童基本信息.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel儿童基本信息_LinkClicked);
            // 
            // linkLabel孕妇基本信息
            // 
            this.linkLabel孕妇基本信息.AutoSize = true;
            this.linkLabel孕妇基本信息.Location = new System.Drawing.Point(106, 53);
            this.linkLabel孕妇基本信息.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel孕妇基本信息.Name = "linkLabel孕妇基本信息";
            this.linkLabel孕妇基本信息.Size = new System.Drawing.Size(97, 14);
            this.linkLabel孕妇基本信息.TabIndex = 77;
            this.linkLabel孕妇基本信息.TabStop = true;
            this.linkLabel孕妇基本信息.Text = ">>孕妇基本信息";
            this.linkLabel孕妇基本信息.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel孕妇基本信息_LinkClicked);
            // 
            // linkLabel老年人生理自理能力评估表
            // 
            this.linkLabel老年人生理自理能力评估表.AutoSize = true;
            this.linkLabel老年人生理自理能力评估表.Location = new System.Drawing.Point(3, 77);
            this.linkLabel老年人生理自理能力评估表.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel老年人生理自理能力评估表.Name = "linkLabel老年人生理自理能力评估表";
            this.linkLabel老年人生理自理能力评估表.Size = new System.Drawing.Size(169, 14);
            this.linkLabel老年人生理自理能力评估表.TabIndex = 76;
            this.linkLabel老年人生理自理能力评估表.TabStop = true;
            this.linkLabel老年人生理自理能力评估表.Text = ">>老年人生理自理能力评估表";
            this.linkLabel老年人生理自理能力评估表.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel老年人生理自理能力评估表_LinkClicked);
            // 
            // linkLabel精神疾病患者随访
            // 
            this.linkLabel精神疾病患者随访.AutoSize = true;
            this.linkLabel精神疾病患者随访.Location = new System.Drawing.Point(178, 77);
            this.linkLabel精神疾病患者随访.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel精神疾病患者随访.Name = "linkLabel精神疾病患者随访";
            this.linkLabel精神疾病患者随访.Size = new System.Drawing.Size(121, 14);
            this.linkLabel精神疾病患者随访.TabIndex = 69;
            this.linkLabel精神疾病患者随访.TabStop = true;
            this.linkLabel精神疾病患者随访.Text = ">>精神疾病患者随访";
            this.linkLabel精神疾病患者随访.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel精神疾病患者随访_LinkClicked);
            // 
            // linkLable转档查询
            // 
            this.linkLable转档查询.AutoSize = true;
            this.linkLable转档查询.Location = new System.Drawing.Point(3, 101);
            this.linkLable转档查询.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLable转档查询.Name = "linkLable转档查询";
            this.linkLable转档查询.Size = new System.Drawing.Size(73, 14);
            this.linkLable转档查询.TabIndex = 72;
            this.linkLable转档查询.TabStop = true;
            this.linkLable转档查询.Text = ">>转档查询";
            this.linkLable转档查询.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLable转档查询_LinkClicked);
            // 
            // linkLabel转档申请
            // 
            this.linkLabel转档申请.AutoSize = true;
            this.linkLabel转档申请.Location = new System.Drawing.Point(82, 101);
            this.linkLabel转档申请.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel转档申请.Name = "linkLabel转档申请";
            this.linkLabel转档申请.Size = new System.Drawing.Size(73, 14);
            this.linkLabel转档申请.TabIndex = 73;
            this.linkLabel转档申请.TabStop = true;
            this.linkLabel转档申请.Text = ">>转档申请";
            this.linkLabel转档申请.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel转档申请_LinkClicked);
            // 
            // linkLabel高血压分级管理
            // 
            this.linkLabel高血压分级管理.AutoSize = true;
            this.linkLabel高血压分级管理.Location = new System.Drawing.Point(161, 101);
            this.linkLabel高血压分级管理.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.linkLabel高血压分级管理.Name = "linkLabel高血压分级管理";
            this.linkLabel高血压分级管理.Size = new System.Drawing.Size(59, 14);
            this.linkLabel高血压分级管理.TabIndex = 81;
            this.linkLabel高血压分级管理.TabStop = true;
            this.linkLabel高血压分级管理.Text = ">>GXYFJ";
            this.linkLabel高血压分级管理.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel高血压分级管理_LinkClicked);
            // 
            // frm公共卫生主窗体
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(990, 471);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm公共卫生主窗体";
            this.Load += new System.EventHandler(this.frm公共卫生主窗体_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTab工作提醒.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc工作提醒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv工作提醒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link未随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link今日随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link明日随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link本周随访)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link未体检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link已体检)).EndInit();
            this.xtraTab工作报表.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.link精神疾病随访表统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link重型精神病补充表统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link糖尿病管理卡统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link糖尿病随访统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link高血压随访统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link老年人随访统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link高血压管理卡统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link孕产妇基本信息统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link儿童基本信息统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人基本信息统计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc待转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv带转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link个人档案编号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn同意)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn拒绝)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc自动转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv自动转出档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc自动转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv自动转入档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu协同办公;
        private System.Windows.Forms.ToolStripMenuItem Menu辖区健康档案;
        private System.Windows.Forms.ToolStripMenuItem menuItem个人健康档案;
        private System.Windows.Forms.ToolStripMenuItem menuItem家庭健康档案;
        private System.Windows.Forms.ToolStripMenuItem menuItem健康体检;
        private System.Windows.Forms.ToolStripMenuItem menuItem接诊记录;
        private System.Windows.Forms.ToolStripMenuItem menuItem会诊记录;
        private System.Windows.Forms.ToolStripMenuItem menuItem双向转诊;
        private System.Windows.Forms.ToolStripMenuItem Menu区域健康档案;
        private System.Windows.Forms.ToolStripMenuItem menuItem区域健康档案;
        private System.Windows.Forms.ToolStripMenuItem Menu转档管理;
        private System.Windows.Forms.ToolStripMenuItem menuItem转档申请;
        private System.Windows.Forms.ToolStripMenuItem menuItem转档查询;
        private System.Windows.Forms.ToolStripMenuItem Menu基本公共卫生;
        private System.Windows.Forms.ToolStripMenuItem Menu重点人群服务;
        private System.Windows.Forms.ToolStripMenuItem 儿童健康管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童基本信息;
        private System.Windows.Forms.ToolStripMenuItem MenuItem新生儿家庭访视记录表;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表满月;
        private System.Windows.Forms.ToolStripMenuItem 妇女健康管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem妇女保健检查表;
        private System.Windows.Forms.ToolStripMenuItem menuItem更年期保健检查表;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇健康管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem孕产妇基本信息;
        private System.Windows.Forms.ToolStripMenuItem menuItem第一次产前随访服务记录表;
        private System.Windows.Forms.ToolStripMenuItem 老年人健康管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem老年人生理自理能力评估表;
        private System.Windows.Forms.ToolStripMenuItem menuItem老年人中医药健康管理;
        private System.Windows.Forms.ToolStripMenuItem Menu疾病防控服务;
        private System.Windows.Forms.ToolStripMenuItem Menu疾病防控;
        private System.Windows.Forms.ToolStripMenuItem 预防接种服务ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menu慢性疾病患者管理;
        private System.Windows.Forms.ToolStripMenuItem 高血压患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem高血压患者管理卡;
        private System.Windows.Forms.ToolStripMenuItem menuItem高血压患者随访服务;
        private System.Windows.Forms.ToolStripMenuItem 糖尿病患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 健康教育ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 卫生监督协管ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 工作提醒ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menu统计分析;
        private System.Windows.Forms.ToolStripMenuItem Menu健康档案;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表3月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表6月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表8月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表12月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表18月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表24月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表30月;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表3岁;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表4岁;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表5岁;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童健康检查记录表6岁;
        private System.Windows.Forms.ToolStripMenuItem menuItem儿童入托检查表;
        private System.Windows.Forms.ToolStripMenuItem menuItem第二次产前随访服务记录表;
        private System.Windows.Forms.ToolStripMenuItem menuItem第三次产前随访服务记录表;
        private System.Windows.Forms.ToolStripMenuItem menuItem第四次产前随访服务记录表;
        private System.Windows.Forms.ToolStripMenuItem menuItem第五次产前随访服务记录表;
        private System.Windows.Forms.ToolStripMenuItem menuItem产后访视记录;
        private System.Windows.Forms.ToolStripMenuItem menuItem产后42天健康检查;
        private System.Windows.Forms.ToolStripMenuItem 冠心病患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem冠心病患者管理卡;
        private System.Windows.Forms.ToolStripMenuItem menuItem冠心病患者随访服务;
        private System.Windows.Forms.ToolStripMenuItem menuItem糖尿病患者管理卡;
        private System.Windows.Forms.ToolStripMenuItem menuItem糖尿病患者随访服务;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem Menu通知通告管理;
        private System.Windows.Forms.ToolStripMenuItem 脑卒中患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem脑卒中患者管理卡;
        private System.Windows.Forms.ToolStripMenuItem menuItem脑卒中患者随访服务;
        private System.Windows.Forms.ToolStripMenuItem 工作量统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 基本公共卫生ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 工作量统计ToolStripMenuItem1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.ToolStripMenuItem Menu重性精神疾病患者管理;
        private System.Windows.Forms.ToolStripMenuItem menuItem重症精神疾病患者个人信息;
        private System.Windows.Forms.ToolStripMenuItem menuItem重症精神疾病患者随访服务表;
        private DevExpress.XtraGrid.GridControl gc工作提醒;
        private DevExpress.XtraGrid.Views.Grid.GridView gv工作提醒;
        private DevExpress.XtraGrid.Columns.GridColumn col重点人群;
        private DevExpress.XtraGrid.Columns.GridColumn col本季度随访;
        private DevExpress.XtraGrid.Columns.GridColumn col今日随访;
        private DevExpress.XtraGrid.Columns.GridColumn col明日随访;
        private DevExpress.XtraGrid.Columns.GridColumn col本周随访;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link未随访;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link今日随访;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link明日随访;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link本周随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem居民健康统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem个人基本信息统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem健康体检表信息统计;
        private System.Windows.Forms.ToolStripMenuItem 妇女健康管理统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇健康管理ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产妇基本信息统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产1次随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem产后访视记录随访统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem产后42天访视记录随访统计;
        private System.Windows.Forms.ToolStripMenuItem 老年人健康管理统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem老年人随访表统计;
        private System.Windows.Forms.ToolStripMenuItem 重症精神疾病患者管理统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem精神疾病补充表统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem精神疾病随访表统计;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTab工作提醒;
        private DevExpress.XtraTab.XtraTabPage xtraTab工作报表;
        private DevExpress.XtraEditors.HyperLinkEdit link个人基本信息统计;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEdit2;
        private DevExpress.XtraEditors.HyperLinkEdit link儿童基本信息统计;
        private DevExpress.XtraEditors.HyperLinkEdit link重型精神病补充表统计;
        private DevExpress.XtraEditors.HyperLinkEdit link糖尿病管理卡统计;
        private DevExpress.XtraEditors.HyperLinkEdit link糖尿病随访统计;
        private DevExpress.XtraEditors.HyperLinkEdit link高血压随访统计;
        private DevExpress.XtraEditors.HyperLinkEdit link老年人随访统计;
        private DevExpress.XtraEditors.HyperLinkEdit link高血压管理卡统计;
        private DevExpress.XtraEditors.HyperLinkEdit link孕产妇基本信息统计;
        private System.Windows.Forms.ToolStripMenuItem 慢性病ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem高血压患者管理卡统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem高血压患者随访记录表;

        private System.Windows.Forms.ToolStripMenuItem MenuItem糖尿病患者管理卡统计;

        private System.Windows.Forms.LinkLabel linkLabel健康档案;
        private System.Windows.Forms.LinkLabel linkLabel儿童基本信息;
        private System.Windows.Forms.LinkLabel linkLabel孕妇基本信息;
        private System.Windows.Forms.LinkLabel linkLabel老年人生理自理能力评估表;
        private System.Windows.Forms.LinkLabel linkLabel新建户主档案;
        private System.Windows.Forms.LinkLabel linkLabel个人体检;
        private System.Windows.Forms.LinkLabel linkLabel转档申请;
        private System.Windows.Forms.LinkLabel linkLable转档查询;
        private System.Windows.Forms.LinkLabel linkLabel高血压随访表;
        private System.Windows.Forms.LinkLabel linkLabel糖尿病随访表;
        private System.Windows.Forms.LinkLabel linkLabel精神疾病患者随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem糖尿病患者随访记录表;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产2次随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产3次随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产4次随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem孕产5次随访;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康管理统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童基本信息统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem新生儿家庭访视记录统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表满月统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表3月龄统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表6月龄;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表8月龄统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表12月龄统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表18月龄统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表24月龄统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表30月龄;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表3岁统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表4岁统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表5岁统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem儿童健康检查记录表6岁统计;
        private DevExpress.XtraEditors.HyperLinkEdit link精神疾病随访表统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem脑卒中患者管理卡统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem脑卒中患者随访记录表;
        private System.Windows.Forms.ToolStripMenuItem MenuItem冠心病患者管理卡统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem冠心病患者随访记录表;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraGrid.Columns.GridColumn col未体检;
        private DevExpress.XtraGrid.Columns.GridColumn col已体检;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link未体检;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link已体检;
        private System.Windows.Forms.ToolStripMenuItem menuItem批量转档;
        private System.Windows.Forms.ToolStripMenuItem MenuItem健康体检表统计详细;
        private System.Windows.Forms.ToolStripMenuItem MenuItem接诊记录表统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem辖区居民管理率统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem老年人管理率统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem高血压管理率统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem糖尿病管理率统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem脑卒中管理率统计;
        //private System.Windows.Forms.ToolStripMenuItem MenuItem冠心病管理率统计;
        private System.Windows.Forms.ToolStripMenuItem 解密姓名ToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel脑卒中随访表;
        private System.Windows.Forms.LinkLabel linkLabel冠心病随访表;
        private DevExpress.XtraGrid.GridControl gc待转出档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv带转出档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gc自动转出档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv自动转出档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gc自动转入档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv自动转入档案;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private System.Windows.Forms.ToolStripMenuItem MenuItem健康人未体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem健康人已体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem高血压未体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem高血压已体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem糖尿病未体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem糖尿病已体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem老年人未体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem老年人已体检;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn同意;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn拒绝;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private System.Windows.Forms.ToolStripMenuItem menuItem体检回执单;
        private System.Windows.Forms.ToolStripMenuItem 医疗信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 诊疗记录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 化验结果ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 影像结果ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem老年人统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem健康人统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem慢病统计;
        private System.Windows.Forms.ToolStripMenuItem 常用统计功能ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 岁儿童统计分析ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇统计分析ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 老年人月报表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 残疾人健康管理统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 残疾人康复服务信息统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 残疾人健康管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 残疾人随访服务表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menultem残疾人随访服务表;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.ToolStripMenuItem 慢性病人群统计分析ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 辖区居民管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 老年人管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 高高血压患者管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 糖尿病患者管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 脑卒中管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 冠心病管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wis联合统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 儿童系统管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇系统管理率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理率统计分析ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wis孕妇信息查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wis儿童信息查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 糖尿病患者血糖控制率ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 高血压患者血压控制率ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 岁儿童管理率ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇早孕建册率ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 出生数据统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hIS慢性病查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕情数据统计ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 肺结核患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 患病期间第一次随访ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem重症精神疾病未体检;
        private System.Windows.Forms.ToolStripMenuItem MenuItem重症精神疾病已体检;
        private System.Windows.Forms.ToolStripMenuItem 患病其他后续随访ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 艾滋病患者管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 个案随访表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItem档案复核统计;
        private System.Windows.Forms.ToolStripMenuItem MenuItem贫困人口统计;
        private System.Windows.Forms.ToolStripMenuItem 天士力手表数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案动态使用率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem妇女分级管理;
        private System.Windows.Forms.ToolStripMenuItem 档案重复数据统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 疾病统计分析ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem高血压高危干预;
        private System.Windows.Forms.ToolStripMenuItem 妇保院数据统计ToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel高血压分级管理;
        private System.Windows.Forms.ToolStripMenuItem 档案复核统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 贫困人口体检统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 居民健康档案统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 个人基本信息统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案复核统计ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 档案动态使用率统计ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 档案重复数据统计ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 管理率统计分析ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 贫困人口体检统计ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 老年人统计查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 老年人健康体检统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 老年人月报表ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 慢病统计查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 高血压患者血压控制率ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 糖尿病患者血糖控制率ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇统计查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 孕产妇统计分析ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 儿童统计查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 岁儿童统计分析ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 妇保院工作量统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItem高血压分级管理;
        private System.Windows.Forms.ToolStripMenuItem 公卫统计汇总ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 早孕建册率统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 岁儿童系统管理率ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 妇保院数据统计儿童ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理率统计分析ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 高血压高危人群达标率ToolStripMenuItem;
    }
}
