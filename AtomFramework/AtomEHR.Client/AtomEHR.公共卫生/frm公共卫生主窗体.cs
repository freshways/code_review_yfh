﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Interfaces;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module;
using AtomEHR.公共卫生.Module.个人健康;
using AtomEHR.公共卫生.Module.基本公共卫生.儿童健康管理;
using AtomEHR.公共卫生.Module.基本公共卫生.妇女健康管理;
using AtomEHR.公共卫生.Module.基本公共卫生.疾病防控服务;
using AtomEHR.公共卫生.Module.基本公共卫生.老年人健康管理;
using AtomEHR.公共卫生.Module.基本公共卫生.重性精神疾病患者管理;
using AtomEHR.公共卫生.Module.基本公共卫生.残疾人健康管理;
using AtomEHR.公共卫生.Module.健康档案.辖区健康档案;
using AtomEHR.公共卫生.Module.健康档案.转档管理;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using NetDimension.NanUI;
//using AtomEHR.公共卫生.Module.统计分析.重症精神病;

namespace AtomEHR.公共卫生
{
    public partial class frm公共卫生主窗体 : AtomEHR.Library.frmModuleBase
    {
        AtomEHR.Business.bll转档申请 bll转档申请 = new Business.bll转档申请();
        public frm公共卫生主窗体()
        {
            InitializeComponent();
            _ModuleID = ModuleID.公共卫生; //设置模块编号
            _ModuleName = ModuleNames.公共卫生;//设置模块名称
            //MenuItemMain.Text = ModuleNames.公共卫生; //与AssemblyModuleEntry.ModuleName定义相同
            this.MainMenuStrip = this.menuStrip1;
            foreach (ToolStripItem tl in menuStrip1.Items)
            {
                SetMenuTag(tl);
            }
            //SetMenuTag();
            BindGZTX();//绑定工作提醒Grid
            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                Bind待转出();//绑定待转出
                Bind自动转出();
                Bind自动转入();
            }
            //try
            //{
            //    //BindGZTX();
            //    BackgroundWorker back = new BackgroundWorker();
            //    back.DoWork += back_DoWork;
            //    back.RunWorkerCompleted += back_RunWorkerCompleted;
            //    back.RunWorkerAsync();
            //}
            //catch (Exception ex)
            //{
            //    Msg.Warning(ex.Message.ToString());
            //}
        }

        private void back_DoWork(object sender, DoWorkEventArgs e)
        {
            //throw new NotImplementedException();
            BindGZTX();//绑定工作提醒Grid
        }

        private void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Bind自动转出()
        {
            string _strWhere = string.Empty;
            string pgrid = Loginer.CurrentUser.所属机构.ToString();

            if (pgrid.Length < 7)
            {
                _strWhere += " AND  转出机构 like '" + pgrid + "%' ";
            }
            if (pgrid.Length == 12)
            {
                _strWhere += " AND (转出机构 = '" + pgrid + "'  OR (len(转出机构)=15 and substring(转出机构,1,7)+'1'+substring(转出机构,9,7)  like '" + pgrid + "%'))";
            }
            if (pgrid.Length == 15)
            {
                _strWhere += " AND 转出机构 = '" + pgrid + "' ";
            }

            _strWhere += " AND 类型 = '3' AND 转档方式 = '1' ";
            //this.gc自动转出档案.StrWhere = _strWhere;
            //this.gc自动转出档案.View = "vw_转档";
            DataTable ds = bll转档申请.Get全部数据("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");
            if (ds.Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Rows.Count; i++)
                {
                    ds.Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Rows[i]["姓名"].ToString());
                    ds.Rows[i]["性别"] = bll转档申请.ReturnDis字典显示("xb_xingbie", ds.Rows[i]["性别"].ToString());
                    ds.Rows[i]["转出机构"] = bll转档申请.Return机构名称(ds.Rows[i]["转出机构"].ToString());
                    ds.Rows[i]["转入机构"] = bll转档申请.Return机构名称(ds.Rows[i]["转入机构"].ToString());
                    ds.Rows[i]["所属机构"] = bll转档申请.Return机构名称(ds.Rows[i]["所属机构"].ToString());
                    ds.Rows[i]["所属机构审核人"] = bll转档申请.Return用户名称(ds.Rows[i]["所属机构审核人"].ToString());
                    ds.Rows[i]["创建人"] = bll转档申请.Return用户名称(ds.Rows[i]["创建人"].ToString());
                }
            }
            this.gc自动转出档案.DataSource = ds;
            this.gv自动转出档案.BestFitColumns();
        }

        private void Bind自动转入()
        {
            string _strWhere = string.Empty;
            string pgrid = Loginer.CurrentUser.所属机构.ToString();

            if (pgrid.Length < 7)
            {
                _strWhere += " AND  转入机构 like '" + pgrid + "%' ";
            }
            if (pgrid.Length == 12)
            {
                _strWhere += " AND ( 转入机构 = '" + pgrid + "'  OR (len( 转入机构)=15 and  substring( 转入机构,1,7)+'1'+substring( 转入机构,9,7)  like '" + pgrid + "%'))";
            }
            if (pgrid.Length == 15)
            {
                _strWhere += " AND  转入机构 = '" + pgrid + "' ";
            }

            _strWhere += " AND 类型 = '3' AND 转档方式 = '1' ";
            //this.gc自动转出档案.StrWhere = _strWhere;
            //this.gc自动转出档案.View = "vw_转档";
            DataTable ds = bll转档申请.Get全部数据("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");
            if (ds.Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Rows.Count; i++)
                {
                    ds.Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Rows[i]["姓名"].ToString());
                    ds.Rows[i]["性别"] = bll转档申请.ReturnDis字典显示("xb_xingbie", ds.Rows[i]["性别"].ToString());
                    ds.Rows[i]["转出机构"] = bll转档申请.Return机构名称(ds.Rows[i]["转出机构"].ToString());
                    ds.Rows[i]["转入机构"] = bll转档申请.Return机构名称(ds.Rows[i]["转入机构"].ToString());
                    ds.Rows[i]["所属机构"] = bll转档申请.Return机构名称(ds.Rows[i]["所属机构"].ToString());
                    ds.Rows[i]["所属机构审核人"] = bll转档申请.Return用户名称(ds.Rows[i]["所属机构审核人"].ToString());
                    ds.Rows[i]["创建人"] = bll转档申请.Return用户名称(ds.Rows[i]["创建人"].ToString());
                }
            }
            this.gc自动转入档案.DataSource = ds;
            this.gv自动转入档案.BestFitColumns();
        }

        private void Bind待转出()
        {
            string pgrid = Loginer.CurrentUser.所属机构.ToString();
            string _strWhere = string.Empty;
            //this.pager待转出档案.InitControl();

            if (pgrid.Length < 7)
            {
                _strWhere += " AND  转出机构 like '" + pgrid + "%' ";
            }
            if (pgrid.Length == 12)
            {
                _strWhere += " AND ( 转出机构 = '" + pgrid + "'  OR (len( 转出机构)=15 and substring( 转出机构,1,7)+'1'+substring( 转出机构,9,7)  like '" + pgrid + "%'))";
            }
            if (pgrid.Length == 15)
            {
                _strWhere += " AND  转出机构 = '" + pgrid + "' ";
            }

            _strWhere += " AND (类型 = '0' or  类型 = '1' )";

            DataTable ds = bll转档申请.Get全部数据("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");
            // this.pager待转出档案.GetQueryResultNew("vw_转档", "*", _strWhere, "HAPPENTIME", "DESC");

            if (ds.Rows.Count >= 0)
            {
                for (int i = 0; i < ds.Rows.Count; i++)
                {
                    ds.Rows[i]["姓名"] = util.DESEncrypt.DES解密(ds.Rows[i]["姓名"].ToString());
                    ds.Rows[i]["性别"] = bll转档申请.ReturnDis字典显示("xb_xingbie", ds.Rows[i]["性别"].ToString());
                    ds.Rows[i]["转入机构"] = bll转档申请.Return机构名称(ds.Rows[i]["转入机构"].ToString());
                    ds.Rows[i]["所属机构"] = bll转档申请.Return机构名称(ds.Rows[i]["所属机构"].ToString());
                    ds.Rows[i]["创建人"] = bll转档申请.Return用户名称(ds.Rows[i]["创建人"].ToString());
                    ds.Rows[i]["类型"] = "待所属机构审核";
                }
            }

            this.gc待转出档案.DataSource = ds;
            this.gv带转出档案.BestFitColumns();
        }

        private void frm公共卫生主窗体_Load(object sender, EventArgs e)
        {
            //窗体load事件不加载
        }
        /// <summary>
        /// 重写该方法，返回模块主菜单对象.
        /// </summary>
        /// <returns></returns>
        public override MenuStrip GetModuleMenu()
        {
            return this.menuStrip1;
        }
        /// <summary>
        /// 设置菜单标记。初始化窗体的可用权限
        /// 请参考MenuItemTag类定义
        /// </summary>
        private void SetMenuTag(ToolStripItem tl)
        {
            tl.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            if (tl is ToolStripMenuItem)
                foreach (ToolStripItem tls in ((ToolStripMenuItem)tl).DropDownItems)
                {
                    SetMenuTag(tls);
                }

            ////协同办公
            //Menu协同办公.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //Menu通知通告管理.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            ////健康档案
            //Menu健康档案.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //Menu辖区健康档案.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem个人健康档案.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem家庭健康档案.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem健康体检.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem接诊记录.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem会诊记录.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem双向转诊.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.公共卫生, AuthorityCategory.NONE);

            //Menu区域健康档案.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem区域健康档案.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);

            //Menu转档管理.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem转档申请.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem转档查询.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            ////基本公共卫生
            //Menu基本公共卫生.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //Menu重点人群服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //Menu疾病防控服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //Menu慢性疾病患者管理.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem高血压患者管理卡.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //this.menuItem高血压患者随访服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem糖尿病患者管理卡.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //this.menuItem糖尿病患者随访服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem脑卒中患者管理卡.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //this.menuItem脑卒中患者随访服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //menuItem冠心病患者管理卡.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            //this.menuItem冠心病患者随访服务.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            ////统计分析
            //Menu统计分析.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.公共卫生, AuthorityCategory.NONE);
            ////MenuItemBusEdit.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.TestModel, AuthorityCategory.BUSINESS_ACTION_VALUE);

            ////儿童健康管理

        }

        /// <summary>
        /// 设置模块主界面的权限
        /// </summary>        
        public override void SetSecurity(object securityInfo)
        {
            base.SetSecurity(securityInfo);

            if (securityInfo is ToolStrip)
            {
                //设置按钮权限
                //btnEdit.Enabled = MenuItemEdit.Enabled;
                //btnBusEdit.Enabled = MenuItemBusEdit.Enabled;
            }
        }

        private void MenuItemEdit_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(FrmEdit), MenuItemEdit);
        }

        private void MenuItemBusEdit_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(FrmBusEdit), MenuItemBusEdit);
        }

        private void menuItem个人健康档案_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm个人健康档案), menuItem个人健康档案);
        }

        private void Menu家庭健康档案_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm家庭健康档案), menuItem家庭健康档案);
        }

        private void Menu健康体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表), menuItem健康体检);
        }

        private void Menu接诊记录_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm接诊记录), menuItem接诊记录);
        }

        private void Menu会诊记录_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm会诊记录), menuItem会诊记录);
        }

        private void Menu双向转诊_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm双向会诊), menuItem双向转诊);
        }

        private void menuItem区域健康档案_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm区域健康档案), menuItem区域健康档案);
        }

        #region 转档管理
        private void Menu转档申请_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm转档申请), menuItem转档申请);
        }

        private void Menu转档查询_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm转档查询), menuItem转档查询);
        }
        private void menuItem批量转档_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm批量转档), menuItem批量转档);
        }
        #endregion

        #region 儿童

        private void MenuItem儿童基本信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童基本信息), MenuItem儿童基本信息);
        }

        private void MenuItem新生儿家庭访视记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm新生儿家庭访视记录表), MenuItem新生儿家庭访视记录表);
        }

        private void MenuItem儿童健康检查记录表满月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_满月), MenuItem儿童健康检查记录表满月);
        }

        private void menuItem儿童健康检查记录表3月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_3月龄), menuItem儿童健康检查记录表3月);
        }

        private void menuItem儿童健康检查记录表6月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_6月龄), menuItem儿童健康检查记录表6月);
        }

        private void menuItem儿童健康检查记录表8月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_8月龄), menuItem儿童健康检查记录表8月);
        }

        private void menuItem儿童健康检查记录表12月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_12月龄), menuItem儿童健康检查记录表12月);

        }

        private void menuItem儿童健康检查记录表18月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_18月龄), menuItem儿童健康检查记录表18月);

        }

        private void menuItem儿童健康检查记录表24月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_24月龄), menuItem儿童健康检查记录表24月);
        }

        private void menuItem儿童健康检查记录表30月_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_30月龄), menuItem儿童健康检查记录表30月);

        }

        private void menuItem儿童健康检查记录表3岁_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_3岁), menuItem儿童健康检查记录表3岁);

        }

        private void menuItem儿童健康检查记录表4岁_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_4岁), menuItem儿童健康检查记录表4岁);

        }

        private void menuItem儿童健康检查记录表5岁_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_5岁), menuItem儿童健康检查记录表5岁);

        }

        private void menuItem儿童健康检查记录表6岁_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查记录表_6岁), menuItem儿童健康检查记录表6岁);

        }

        private void menuItem儿童入托检查表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童入托检查表), menuItem儿童入托检查表);

        }
        #endregion

        #region 孕妇
        private void menuItem孕产妇基本信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm孕产妇基本信息), menuItem孕产妇基本信息);
        }

        private void menuItem第一次产前随访服务记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第一次产前随访记录), menuItem第一次产前随访服务记录表);

        }

        private void menuItem第二次产前随访服务记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第二次产前随访记录), menuItem第二次产前随访服务记录表);
        }

        private void menuItem第三次产前随访服务记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第三次产前随访记录), menuItem第三次产前随访服务记录表);

        }

        private void menuItem第四次产前随访服务记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第四次产前随访记录), menuItem第四次产前随访服务记录表);

        }

        private void menuItem第五次产前随访服务记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第五次产前随访记录), menuItem第五次产前随访服务记录表);

        }

        private void menuItem产后访视记录_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm产后访视情况), menuItem产后访视记录);

        }

        private void menuItem产后42天健康检查_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm产后42天检查), menuItem产后42天健康检查);

        }
        #endregion

        #region 慢性病

        private void menuItem高血压患者管理卡_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压患者管理卡), menuItem高血压患者管理卡);
        }
        private void menuItem高血压患者随访服务_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压患者随访记录), menuItem高血压患者随访服务);
        }

        private void 糖尿病患者管理卡ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病患者管理卡), menuItem糖尿病患者管理卡);
        }

        private void menuItem糖尿病患者随访服务_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病患者随访记录), menuItem糖尿病患者随访服务);
        }

        private void menuItem脑卒中患者管理卡_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm脑卒中患者管理卡), menuItem脑卒中患者管理卡);
        }

        private void menuItem脑卒中患者随访服务_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm脑卒中患者随访记录), menuItem脑卒中患者随访服务);
        }

        private void menuItem冠心病患者管理卡_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm冠心病患者管理卡), menuItem冠心病患者管理卡);
        }

        private void menuItem冠心病患者随访服务_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm冠心病患者随访记录), menuItem冠心病患者随访服务);
        }
        private void menuItem高血压高危干预_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压高危干预), menuItem高血压高危干预);
        }
        
        private void menuItem高血压分级管理_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压分级管理), menuItem高血压分级管理);
        }
        #endregion

        private void linkLabel1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("iexplore.exe", "http://192.168.10.100:83");
        }

        private void linkLabel2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("iexplore.exe", "http://192.168.10.100:83/showtopic-15471.aspx");
        }

        private void linkLabel3_Click(object sender, EventArgs e)
        {
            frm个人资料完善 frm = new frm个人资料完善();
            frm.ShowDialog();
        }

        #region 工作提醒
        private void BindGZTX()
        {
            string prgid = Loginer.CurrentUser.所属机构;
            if (prgid.Length > 6)//不是卫生局的账号登录
            {
                DataSet ds = new Business.bllCom().Get工作提醒("", "", "");
                this.gc工作提醒.DataSource = ds.Tables[0];
                this.xtraTabControl2.SelectedTabPage = xtraTab工作提醒;
            }
            else //卫生局的账号登录   372323
            {
                xtraTab工作提醒.PageVisible = false;
                this.xtraTabControl2.SelectedTabPage = xtraTab工作报表;
            }
        }
        private void link未随访_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            //wxf 2018年8月29日 16:43:05 修改未随访为本季度随访
            frm工作提醒 frm = new frm工作提醒(type, "本季度随访");
            frm.ShowDialog();
        }
        private void link今日随访_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            frm工作提醒 frm = new frm工作提醒(type, "今日随访");
            frm.ShowDialog();
        }
        private void link明日随访_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            frm工作提醒 frm = new frm工作提醒(type, "明日随访");
            frm.ShowDialog();
        }
        private void link本周随访_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            frm工作提醒 frm = new frm工作提醒(type, "本周随访");
            frm.ShowDialog();
        }

        private void link未体检_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            frm工作提醒 frm = new frm工作提醒(type, "未体检");
            frm.ShowDialog();
        }

        private void link已体检_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv工作提醒.GetFocusedDataRow();
            string type = row["随访对象"].ToString();
            //string dateType = row.Table.Columns["未随访"].ColumnName;
            frm工作提醒 frm = new frm工作提醒(type, "未体检");
            frm.ShowDialog();
        }

        #endregion

        #region MenuItem

        private void menuItem老年人生理自理能力评估表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人随访服务记录表), menuItem老年人生理自理能力评估表);

        }

        private void menuItem老年人中医药健康管理_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人中医药健康管理), menuItem老年人中医药健康管理);
        }
        private void menuItem妇女保健检查表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm妇女保健检查表), menuItem妇女保健检查表);
        }

        private void menuItem妇女分级管理_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm妇女分级管理), menuItem妇女分级管理);
        }

        private void menuItem更年期保健检查表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm更年期保健检查表), menuItem更年期保健检查表);
        }

        private void menuItem重症精神疾病患者个人信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神病患者基本信息表), menuItem重症精神疾病患者个人信息);
        }

        private void menuItem重症精神疾病患者随访服务表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神病患者随访服务表), menuItem重症精神疾病患者随访服务表);
        }
        private void menuItem残疾人随访服务表_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm残疾人随访服务表), menuItem残疾人随访服务表);
        }
        private void MenuItem个人基本信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm个人基本信息统计), MenuItem个人基本信息统计);
        }

        private void MenuItem健康体检表信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表统计), MenuItem健康体检表信息统计);
        }


        #region 儿童

        private void MenuItem儿童基本信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmChildBaseInfoStat), MenuItem儿童基本信息统计);
        }

        private void MenuItem新生儿家庭访视记录统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm新生儿访视统计), MenuItem新生儿家庭访视记录统计);
        }

        private void MenuItem儿童健康检查记录表满月统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童满月统计), MenuItem儿童健康检查记录表满月统计);
        }

        private void MenuItem儿童健康检查记录表3月龄统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童3月统计), MenuItem儿童健康检查记录表3月龄统计);
        }

        private void MenuItem儿童健康检查记录表6月龄_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童6月统计), MenuItem儿童健康检查记录表6月龄);
        }

        private void MenuItem儿童健康检查记录表8月龄统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童8月统计), MenuItem儿童健康检查记录表8月龄统计);
        }

        private void MenuItem儿童健康检查记录表12月龄统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童12月统计), MenuItem儿童健康检查记录表12月龄统计);
        }

        private void MenuItem儿童健康检查记录表18月龄统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童18月统计), MenuItem儿童健康检查记录表18月龄统计);
        }

        private void MenuItem儿童健康检查记录表24月龄统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童24月统计), MenuItem儿童健康检查记录表24月龄统计);
        }

        private void MenuItem儿童健康检查记录表30月龄_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童30月统计), MenuItem儿童健康检查记录表30月龄);
        }

        private void MenuItem儿童健康检查记录表3岁统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童3岁统计), MenuItem儿童健康检查记录表3岁统计);
        }

        private void MenuItem儿童健康检查记录表4岁统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童4岁统计), MenuItem儿童健康检查记录表4岁统计);
        }

        private void MenuItem儿童健康检查记录表5岁统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童5岁统计), MenuItem儿童健康检查记录表5岁统计);
        }

        private void MenuItem儿童健康检查记录表6岁统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童6岁统计), MenuItem儿童健康检查记录表6岁统计);
        }

        #endregion

        #region 孕妇
        private void MenuItem孕产1次随访_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第1次产前随访服务记录表统计), MenuItem孕产1次随访);
        }

        private void MenuItem孕产2次随访_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第2次产前随访统计), MenuItem孕产1次随访);
        }

        private void MenuItem孕产3次随访_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第3次产前随访统计), MenuItem孕产3次随访);
        }

        private void MenuItem孕产4次随访_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第4次产前随访统计), MenuItem孕产4次随访);
        }

        private void MenuItem孕产5次随访_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm第5次产前随访统计), MenuItem孕产5次随访);
        }

        private void MenuItem孕产妇基本信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm残疾人康复服务信息统计), MenuItem孕产妇基本信息统计);
        }

        private void MenuItem产后访视记录随访统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm产后访视记录表统计), MenuItem产后访视记录随访统计);
        }

        private void MenuItem产后42天访视记录随访统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm产后42天访视记录表统计), MenuItem产后42天访视记录随访统计);
        }
        #endregion

        private void MenuItem老年人随访表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人生活自理能力评估表统计), MenuItem老年人随访表统计);
        }

        private void MenuItem精神疾病补充表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神疾病补充表统计), MenuItem精神疾病补充表统计);
        }

        private void MenuItem精神疾病随访表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神疾病随访表统计), MenuItem精神疾病随访表统计);
        }

        #region 慢性病
        private void MenuItem高血压患者管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压管理卡统计), MenuItem高血压患者管理卡统计);
        }

        private void MenuItem高血压患者随访记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压患者随访记录表), MenuItem高血压患者随访记录表);
        }

        private void MenuItem糖尿病患者管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病管理卡统计), MenuItem糖尿病患者管理卡统计);
        }

        private void MenuItem糖尿病患者随访记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病患者随访记录表), MenuItem糖尿病患者随访记录表);
        }

        private void MenuItem脑卒中患者管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm脑卒中管理卡统计), MenuItem脑卒中患者管理卡统计);
        }

        private void MenuItem脑卒中患者随访记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm脑卒中患者随访记录表), MenuItem脑卒中患者随访记录表);
        }

        private void MenuItem冠心病患者管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm冠心病管理卡统计), MenuItem冠心病患者管理卡统计);
        }

        private void MenuItem冠心病患者随访记录表_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm冠心病患者随访记录表), MenuItem冠心病患者随访记录表);
        }
        #endregion

        #endregion

        #region 快捷键
        private void linkLabel健康档案_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem个人健康档案_Click(null, null);
        }
        private void linkLabel个人体检_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Menu健康体检_Click(null, null);
        }
        private void linkLabel转档申请_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Menu转档申请_Click(null, null);
        }
        private void linkLable转档查询_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Menu转档查询_Click(null, null);
        }
        private void linkLabel新建户主档案_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frm个人基本信息表 frm = new frm个人基本信息表();
            //frm个人基本信息表 frm = new frm个人基本信息表();
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                frm.Close();
                Msg.ShowInformation("保存成功!");
            }
        }
        private void linkLabel儿童基本信息_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuItem儿童基本信息_Click(null, null);
        }
        private void linkLabel孕妇基本信息_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem孕产妇基本信息_Click(null, null);
        }
        private void linkLabel老年人生理自理能力评估表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem老年人生理自理能力评估表_Click(null, null);
        }
        private void linkLabel高血压随访表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem高血压患者随访服务_Click(null, null);
        }

        private void linkLabel糖尿病随访表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem糖尿病患者随访服务_Click(null, null);
        }


        private void linkLabel脑卒中随访表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem脑卒中患者随访服务_Click(null, null);
        }

        private void linkLabel冠心病随访表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem冠心病患者随访服务_Click(null, null);
        }

        private void linkLabel精神疾病患者随访_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem重症精神疾病患者随访服务表_Click(null, null);
        }

        private void linkLabel残疾人随访服务表_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            menuItem残疾人随访服务表_Click(null, null);
        }

        #endregion

        #region 统计模块

        private void link个人基本信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm个人基本信息统计), MenuItem个人基本信息统计);
        }
        private void 健康体检表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表统计), MenuItem健康体检表信息统计);
        }
        private void MenuItem健康体检表统计详细_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表统计详细), MenuItem健康体检表统计详细);
        }

        private void MenuItem接诊记录表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm接诊记录统计), MenuItem接诊记录表统计);
        }


        private void 儿童基本信息_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frmChildBaseInfoStat), MenuItem儿童基本信息统计);
        }

        private void link孕产妇基本信息统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm残疾人康复服务信息统计), MenuItem孕产妇基本信息统计);
        }

        private void link老年人随访统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人生活自理能力评估表统计), MenuItem老年人随访表统计);
        }

        #region 慢性病
        private void link高血压管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压管理卡统计), MenuItem高血压患者管理卡统计);
        }

        private void link高血压随访统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压患者随访记录表), MenuItem高血压患者随访记录表);
        }

        private void link糖尿病管理卡统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病管理卡统计), MenuItem糖尿病患者管理卡统计);
        }

        private void link糖尿病随访统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病患者随访记录表), MenuItem糖尿病患者随访记录表);
        }
        #endregion

        private void link重型精神病补充表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神疾病补充表统计), MenuItem精神疾病补充表统计);
        }

        private void link精神疾病随访表统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm精神疾病随访表统计), MenuItem精神疾病随访表统计);
        }

        #endregion

        private void gv工作提醒_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            DataRowView row = gv工作提醒.GetRow(e.RowHandle) as DataRowView;
            if (row == null) return;
            //string zongshu = row["总数"].ToString();
            //string hegeshu = row["合格数"].ToString();
            //string buhegeshu = row["不合格数"].ToString();
            if (e.Column == col本季度随访)
            {
                string count = row["本季度随访"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link未随访_Click;
                    e.RepositoryItem = link;
                }
            }

            if (e.Column == col今日随访)
            {
                string count = row["今日随访"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link今日随访_Click;
                    e.RepositoryItem = link;
                }
            }
            if (e.Column == col明日随访)
            {
                string count = row["明日随访"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link明日随访_Click;
                    e.RepositoryItem = link;
                }
            }
            if (e.Column == col本周随访)
            {
                string count = row["本周随访"].ToString();
                if (!string.IsNullOrEmpty(count) && count != "0")
                {
                    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                    link.Click += link本周随访_Click;
                    e.RepositoryItem = link;
                }
            }
            if (e.Column == col未体检)
            {
                //string count = row["未体检"].ToString();
                //if (!string.IsNullOrEmpty(count) && count != "0")
                //{
                //    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                //    link.Click += link未体检_Click;
                //    e.RepositoryItem = link;
                //}
            }
            if (e.Column == col已体检)
            {
                //string count = row["已体检"].ToString();
                //if (!string.IsNullOrEmpty(count) && count != "0")
                //{
                //    RepositoryItemHyperLinkEdit link = new RepositoryItemHyperLinkEdit();
                //    link.Click += link已体检_Click;
                //    e.RepositoryItem = link;
                //}
            }
        }

        private void 解密姓名ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm姓名解密), 解密姓名ToolStripMenuItem);
        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }

        private void MenuItem健康人未体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康人未体检), MenuItem健康人未体检);
        }

        private void MenuItem健康人已体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康人已体检), MenuItem健康人已体检);
        }

        private void MenuItem老年人已体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人已体检), MenuItem老年人已体检);
        }

        private void MenuItem老年人未体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人未体检), MenuItem老年人未体检);
        }

        private void MenuItem糖尿病已体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病已体检), MenuItem糖尿病已体检);
        }

        private void MenuItem糖尿病未体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病未体检), MenuItem糖尿病未体检);
        }

        private void MenuItem高血压已体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压已体检), MenuItem高血压已体检);
        }

        private void MenuItem高血压未体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压未体检), MenuItem高血压未体检);
        }

        private void menuItem体检回执单_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm体检回执单), menuItem体检回执单);
        }

        private void link个人档案编号_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.HyperLinkEdit edit = (DevExpress.XtraEditors.HyperLinkEdit)sender;
            if (edit == null) return;
            DevExpress.XtraGrid.GridControl gc = edit.Parent as DevExpress.XtraGrid.GridControl;
            if (gc == null) return;

            DevExpress.XtraGrid.Views.Grid.GridView view = gc.Views[0] as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view == null) return;
            DataRow focusRow = view.GetFocusedDataRow();
            if (focusRow == null) return;
            string 个人档案编号 = focusRow["个人档案编号"] as string;
            string 家庭档案编号 = focusRow["家庭档案编号1"] as string;
            string date = focusRow["创建时间"].ToString();
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, date);
            frm.Show();
        }

        private void btn同意_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv带转出档案.GetFocusedDataRow();
            if (row == null) return;
            string id = row["ID"].ToString();
            string jtdabh = row["家庭档案编号"].ToString();
            string docNO = row["个人档案编号"].ToString();
            string yhzgx = new bll健康档案().GetYHZGX(docNO);
            int jtcysl = 0;//家庭成员数量
            string dqjg = Loginer.CurrentUser.所属机构;
            if (!string.IsNullOrEmpty(jtdabh))
            {
                bll家庭档案 _bll家庭档案 = new bll家庭档案();
                jtcysl = _bll家庭档案.countHomeCy(jtdabh);//获取家庭成员数量
                if (dqjg.Length > 7)
                {
                    if (!string.IsNullOrEmpty(yhzgx) && jtcysl > 1)
                    {
                        if (Msg.AskQuestion("此居民家庭中含有其他成员！【确定】将把整个家庭的档案转出，【取消】将把该居民从家庭中注销并转出(该居民若为户主除外)。"))
                        {
                            SaveResult result = bll转档申请.updateConvertTopassed(id, yhzgx, jtdabh, "2");
                            if (result.Success)
                            {
                                Msg.ShowInformation("转档成功！");
                                //btn待转出档案_Click(null, null);
                                Bind待转出();
                            }
                        }
                        else
                        {
                            if (yhzgx == "1")//为户主，不能进行转档
                            {
                                Msg.ShowInformation("此居民是【户主】,且家庭中有其他成员，请先到家庭基本信息表中修改其【户主关系】为‘非户主’后再对其进行转档!");
                                return;
                            }
                            else
                            {
                                SaveResult result = bll转档申请.updateConvertTopassed(id, yhzgx, jtdabh, "1");
                                if (result.Success)
                                {
                                    Msg.ShowInformation("转档成功！");
                                    //btn待转出档案_Click(null, null);
                                    Bind待转出();
                                }
                            }
                        }
                    }
                    else
                    {
                        SaveResult result = bll转档申请.updateConvertTopassed(id, "", "", "1");
                        if (result.Success)
                        {
                            Msg.ShowInformation("转档成功！");
                            //btn待转出档案_Click(null, null);
                            Bind待转出();
                        }
                    }
                }
                else
                {
                    SaveResult result = bll转档申请.updateConvertTopassed(id, "", jtdabh, "");
                    if (result.Success)
                    {
                        Msg.ShowInformation("转档成功！");
                        //btn待转出档案_Click(null, null);
                        Bind待转出();
                    }
                }
            }
            else
            {
                SaveResult result = bll转档申请.updateConvertTopassed(id, "", jtdabh, "1");
                if (result.Success)
                {
                    //serrchConvert('waitRollout');
                    Msg.ShowInformation("转档成功！");
                    //btn待转出档案_Click(null, null);
                    Bind待转出();
                }
            }
        }

        private void btn拒绝_Click(object sender, EventArgs e)
        {

            DataTable dt = this.gc待转出档案.DataSource as DataTable;
            DataRow row = this.gv带转出档案.GetFocusedDataRow();
            if (row == null) return;
            frm驳回原因 frm = new frm驳回原因();
            frm.ShowDialog();
            if (frm.DialogResult != System.Windows.Forms.DialogResult.OK) return;

            string jjyy = string.Empty;
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                jjyy = frm._remark;
            else
                return;
            string id = row["ID"].ToString();
            string type = string.Empty;
            string wsjshr = string.Empty;
            string ssjgshr = string.Empty;
            string pRgid = Loginer.CurrentUser.所属机构;
            if (pRgid.Length <= 6)
            {
                type = "4";
                wsjshr = Loginer.CurrentUser.用户编码;
            }
            else
            {
                type = "2";
                ssjgshr = Loginer.CurrentUser.用户编码;
            }
            DataSet ds = bll转档申请.GetBusinessByKey(id, true);
            if (ds.Tables[Models.tb_转档申请.__TableName].Rows.Count == 1)
            {
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.拒绝原因] = jjyy;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.类型] = type;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.所属机构审核人] = ssjgshr;
                ds.Tables[tb_转档申请.__TableName].Rows[0][tb_转档申请.卫生局审核人] = wsjshr;
            }
            SaveResult result = bll转档申请.Save(ds);
            if (result.Success)
            {
                Msg.ShowInformation("驳回成功！");
                Bind待转出();
                //btn待转出档案_Click(null, null);
            }
            else
            {
                //Msg.ShowInformation("操作失败，请联系管理员"+result.e);
            }
        }

        private void 诊疗记录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.医疗信息.frm诊疗记录), 诊疗记录ToolStripMenuItem);
        }

        private void 化验结果ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.医疗信息.frm化验结果), 化验结果ToolStripMenuItem);
        }

        private void 影像结果ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.医疗信息.frm影像结果), 影像结果ToolStripMenuItem);
        }

        //private void menuItem公共卫生字典配置_Click(object sender, EventArgs e)
        //{
        //    MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm公共字典定义), menuItem公共卫生字典配置);
        //}

        private void MenuItem慢病统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表_慢病), MenuItem慢病统计);
        }

        private void MenuItem健康人统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表_健康人), MenuItem健康人统计);
        }

        private void MenuItem老年人统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表_老年人), MenuItem老年人统计);
        }

        private void 岁儿童统计分析ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm儿童健康检查登记簿), 岁儿童统计分析ToolStripMenuItem);
        }

        private void 孕产妇统计分析ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm孕产妇系统管理情况登记簿), 孕产妇统计分析ToolStripMenuItem);
        }

        private void 老年人月报表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm沂水县老年人体检月报表), 老年人月报表ToolStripMenuItem);
        }

        private void 残疾人随访服务表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm残疾人随访服务表), Menultem残疾人随访服务表);
        }

        private void 残疾人康复服务信息统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm残疾人康复服务信息统计), 残疾人康复服务信息统计ToolStripMenuItem);
        }

		private void 慢性病人群统计分析ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm慢性病人群分析), 慢性病人群统计分析ToolStripMenuItem);
        }
		#region 管理率统计
        private void 辖区居民管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm辖区居民管理率统计), MenuItem辖区居民管理率统计);
        }
        

        private void 老年人管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm老年人管理率统计), MenuItem老年人管理率统计);
        }

        private void 高血压管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高血压管理率统计), MenuItem高血压管理率统计);
        }

        private void 糖尿病管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm糖尿病管理率统计), MenuItem糖尿病管理率统计);
        }

        private void 脑卒中管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm脑卒中管理率统计), MenuItem脑卒中管理率统计);
        }

        private void 冠心病管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm冠心病管理率统计), MenuItem冠心病管理率统计);
        }
        #endregion

        private void 儿童系统管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm儿童公卫管理率), 儿童系统管理率统计ToolStripMenuItem);
        }

        private void 孕产妇系统管理率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm孕产妇公卫管理率), 孕产妇系统管理率统计ToolStripMenuItem);
        }

        private void 管理率统计分析ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm管理率统计), 管理率统计分析ToolStripMenuItem);
        }

        private void wis孕妇信息查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.WIS联合统计.FrmWis孕产妇信息查询), wis孕妇信息查询ToolStripMenuItem);
        }

        private void wis儿童信息查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.WIS联合统计.FrmWis儿童信息查询), wis儿童信息查询ToolStripMenuItem);
        }

        private void 糖尿病患者血糖控制率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.常用统计功能.frm糖尿病控制率统计), 糖尿病患者血糖控制率ToolStripMenuItem);
        }

        private void 高血压患者血压控制率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.常用统计功能.frm高血压控制率统计), 高血压患者血压控制率ToolStripMenuItem);
        }

        private void 岁儿童管理率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.常用统计功能.frm0_6岁儿童系统管理率), 岁儿童管理率ToolStripMenuItem);
        }

        private void 孕产妇早孕建册率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.常用统计功能.frm孕产妇早孕建册率), 孕产妇早孕建册率ToolStripMenuItem);
        }

        private void 出生数据统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.WIS联合统计.frm出生数据统计), 孕产妇早孕建册率ToolStripMenuItem);
        }

        private void hIS慢性病查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.常用统计功能.frmHIS慢性病查询), hIS慢性病查询ToolStripMenuItem);            
        }

        private void 孕情数据统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.统计分析.WIS联合统计.Frm孕情数据统计), 孕情数据统计ToolStripMenuItem);        
        }

        private void 患病期间第一次随访ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm肺结核第一次随访), 患病期间第一次随访ToolStripMenuItem);
        }

        private void MenuItem重症精神疾病未体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm重症精神疾病未体检), MenuItem重症精神疾病未体检);
        }

        private void MenuItem重症精神疾病已体检_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm重症精神疾病已体检), MenuItem重症精神疾病已体检);
        }
		
		 private void 患病其他后续随访ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm肺结核后续随访), 患病其他后续随访ToolStripMenuItem);
        }

        private void 个案随访表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm个案随访表), 个案随访表ToolStripMenuItem);
        }

        private void MenuItem档案复核统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm档案复核升级统计), MenuItem档案复核统计);
        }

        private void MenuItem贫困人口统计_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表_贫困人口), MenuItem贫困人口统计);
        }

        private void 天士力手表数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Module.医疗信息.frm天士力手表数据), 天士力手表数据ToolStripMenuItem);
        }

        private void 档案动态使用率统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm档案动态使用率统计), 档案动态使用率统计ToolStripMenuItem);
        }

        private void 档案重复数据统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm档案重复数统计), 档案重复数据统计ToolStripMenuItem);
        }

        private void 疾病统计分析ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm疾病人群统计分析), 疾病统计分析ToolStripMenuItem);
        }

        private void 妇保院数据统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm妇保院工作量统计), 妇保院数据统计ToolStripMenuItem);
        }

        private void 妇保院数据统计儿童ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm妇保院工作量统计_儿童), 妇保院数据统计儿童ToolStripMenuItem);
        }

        private void linkLabel高血压分级管理_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WebForm frm = new WebForm();
            frm.Show();
        }

        private void 贫困人口体检统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康体检表_贫困人口), 贫困人口体检统计ToolStripMenuItem);
        }

        private void 公卫统计汇总ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm健康档案汇总统计), 公卫统计汇总ToolStripMenuItem);
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm公卫报表统计汇总), 公卫统计汇总ToolStripMenuItem);
        }

        private void 高血压高危人群达标率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(frm高危人群达标率统计), 高血压高危人群达标率ToolStripMenuItem);            
        }


    }
}
