﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Business.Security;

namespace AtomEHR.公共卫生
{
    public partial class frm个人资料完善 : frmBaseDialog
    {
        public frm个人资料完善()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textEdit1.Text) || this.textEdit1.Text.Length != 18)
            {
                Msg.ShowInformation("请填写正确身份证号！");
                return;
            }
            bool success = new bllUser().Modify身份证Direct(Loginer.CurrentUser.Account, this.textEdit1.Text);
            if (success)
            {
                Msg.ShowInformation("修改成功！");
                this.Close();
            }
            else
            {
                Msg.Warning("修改出错！");
            }
        }
    }
}
