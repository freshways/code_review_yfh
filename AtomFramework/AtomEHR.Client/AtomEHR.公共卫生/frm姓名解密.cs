﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Business;

namespace AtomEHR.公共卫生
{
    public partial class frm姓名解密 : AtomEHR.Library.frmBaseBusinessForm
    {
        AtomEHR.Business.bllCom com = new Business.bllCom();
        public frm姓名解密()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.textEdit表名.Text = "tb_健康档案";
            string _strWhere = " where 1=1 and len(姓名)>10 ";
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit21.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    _strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }

            DataTable table = com.Get姓名(this.textEdit表名.Text.Trim(), _strWhere);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                table.Rows[i]["解密姓名"] = util.DESEncrypt.DES解密(table.Rows[i]["加密姓名"].ToString());
            }

            this.gridControl1.DataSource = table;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DataTable table = this.gridControl1.DataSource as DataTable;
            if (table == null || table.Rows.Count == 0) return;
            SaveResult result = com.Get解密(table, this.textEdit表名.Text.Trim());
        }

        private void frm姓名解密_Load(object sender, EventArgs e)
        {
            //为“所属机构"绑定信息
            try
            {
                bll机构信息 bll机构 = new bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            else { treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false; }
        }
    }
}
