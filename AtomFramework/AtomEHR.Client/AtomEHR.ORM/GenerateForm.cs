﻿///*************************************************************************/
///*
///* 文件名            : GenerateForm.cs    
///*
///* 程序说明        : 自动生成数据层的代码
///* 非原创作者    :  ATOM 
///* 
///* Copyright ©  2015
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtomEHR.ORM
{
    /// <summary>
    ///  生成Form代码使用的参数基类
    /// </summary>
    public class ParamsForm
    {
        //protected string _ConcretelyName;
        protected string _BLL_Name;
        protected string _ORM_Name;
        protected string _Form_Name;
        protected string _Form_Namespace;
        protected string[] _UsingNamespace;

        //public string ConcretelyName { get { return _ConcretelyName; } set { _ConcretelyName = value; } }
        public string BLL_Name { get { return _BLL_Name; } set { _BLL_Name = value; } }
        public string ORM_Name { get { return _ORM_Name; } set { _ORM_Name = value; } }
        public string Form_Name { get { return _Form_Name; } set { _Form_Name = value; } }
        public string Form_Namespace { get { return _Form_Namespace; } set { _Form_Namespace = value; } }
        public string[] UsingNamespace { get { return _UsingNamespace; } set { _UsingNamespace = value; } }
    }

    /// <summary>
    /// 生成数据字典Form的参数类
    /// </summary>
    public class Params4DataDictForm : ParamsForm { }

    /// <summary>
    /// 生成业务单据Form的参数类
    /// </summary>
    public class Params4BusinessForm : ParamsForm { }

    /// <summary>
    /// 自动生成数据层的代码
    /// </summary>
    public class GenerateForm
    {
        /// <summary>
        /// 生成数据字典Form
        /// </summary>
        public string GenerateDataDictForm(Params4DataDictForm param)
        {
            StringBuilder builder = new StringBuilder();

            //.Net Framework的名字空间
            builder.AppendLine("using System;");
            builder.AppendLine("using System.ComponentModel;");
            builder.AppendLine("using System.Data;");

            //引用的自定义名字空间
            foreach (string space in param.UsingNamespace)
                builder.AppendLine("using " + space + ";");

            //生成单元注释部分
            CreateComment(builder, param.Form_Name);

            builder.AppendLine("namespace " + param.Form_Namespace); //当前Form所在的名字空间
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine("    /// " + param.Form_Name + "字典界面");//
            builder.AppendLine("    /// </summary>");
            builder.AppendLine("    public partial class " + param.Form_Name + " : AtomEHR.Library.frmBaseDataDictionary"); //基类
            builder.AppendLine("    {");
            builder.AppendLine("         /// <summary>");
            builder.AppendLine("         /// 构造器");
            builder.AppendLine("         /// </summary>");
            builder.AppendLine("         /// <param name=\"loginer\">当前登录用户</param>");
            builder.AppendLine("         public " + param.Form_Name + "(Loginer loginer): base(loginer)"); //构造器
            builder.AppendLine("         {");
            builder.AppendLine("             _KeyName = " + param.ORM_Name + ".__KeyName; //主键字段");
            builder.AppendLine("             _TableName = " + param.ORM_Name + ".__TableName;//表名");
            builder.AppendLine("             _ModelType = typeof(" + param.ORM_Name + ");");
            builder.AppendLine("         }");
            builder.AppendLine("");
            builder.AppendLine("         /// <summary>");
            builder.AppendLine("         /// 根据表名获取该表的SQL命令生成器");
            builder.AppendLine("         /// </summary>");
            builder.AppendLine("         /// <param name=\"tableName\">表名</param>");
            builder.AppendLine("         /// <returns></returns>");
            builder.AppendLine("         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)");
            builder.AppendLine("         {");
            builder.AppendLine("           Type ORM = null;");
            builder.AppendLine("           if (tableName == " + param.ORM_Name + ".__TableName) ORM = typeof(" + param.ORM_Name + ");");
            builder.AppendLine("           if (ORM == null) throw new Exception(tableName + \"表没有ORM模型！\");");
            //builder.AppendLine("           return new GenerateSqlCmdByTableFields(ORM);");
            builder.AppendLine("           return new GenerateSqlCmdByObjectClass(ORM);");
            builder.AppendLine("         }");
            builder.AppendLine("");
            builder.AppendLine("     }");
            builder.AppendLine("}");

            return builder.ToString();
        }


        /// <summary>
        /// 生成业务单据的数据层
        /// </summary>
        /// <param name="param">参数</param>
        /// <returns></returns>
        public string GenerateBusinessForm(Params4BusinessForm param)
        {
            StringBuilder builder = new StringBuilder();

            //.Net Framework的名字空间
            builder.AppendLine("using System;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using System.ComponentModel;");
            builder.AppendLine("using System.Data;");
            builder.AppendLine("using System.Drawing;");
            builder.AppendLine("using System.Text;");

            //引用的自定义名字空间
            foreach (string space in param.UsingNamespace)
                builder.AppendLine("using " + space + ";");

            //生成单元注释部分
            CreateComment(builder, param.Form_Name);

            //当前业务层所在的名字空间
            builder.AppendLine("namespace " + param.Form_Namespace);
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine("    /// " + param.Form_Name + "业务界面");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine("    public partial class " + param.Form_Name + " : AtomEHR.Library.frmBaseBusinessForm");
            builder.AppendLine("    {");
            builder.AppendLine("         public " + param.Form_Name + "()");
            builder.AppendLine("         {");
            builder.AppendLine("             InitializeComponent();");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("         private void " + param.Form_Name + "_Load(object sender, EventArgs e)"); 
            builder.AppendLine("         {");
            builder.AppendLine("             InitializeForm();");
            builder.AppendLine("         }");
            builder.AppendLine("");
            builder.AppendLine("         /// <summary>");
            builder.AppendLine("         ///初始化窗体");
            builder.AppendLine("         /// </summary>");
            builder.AppendLine("         protected override void InitializeForm()");
            builder.AppendLine("         {");
            builder.AppendLine("             _BLL = new " + param.BLL_Name + "();// 业务逻辑层实例");
            builder.AppendLine("             _SummaryView = new DevGridView(gvSummary);");
            builder.AppendLine("             //_ActiveEditor = textEdit;//请根据默认焦点控件调整本行的代码");
            builder.AppendLine("            _DetailGroupControl = panel1;");
            builder.AppendLine("             base.InitializeForm(); //这行代码放到初始化变量后最好");
            builder.AppendLine("             frmGridCustomize.RegisterGrid(gvSummary);");
            builder.AppendLine("             DevStyle.SetGridControlLayout(gcSummary, false);//表格设置 ");
            builder.AppendLine("             DevStyle.SetSummaryGridViewLayout(gvSummary);");
            builder.AppendLine("");
            builder.AppendLine("             BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.");
            builder.AppendLine("             BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);");
            builder.AppendLine("");
            builder.AppendLine("             //txt_DocDateTo.DateTime = DateTime.Today; //查询条件截止日期");
            builder.AppendLine("");
            builder.AppendLine("             //DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.Account);//绑定弹窗控件");
            builder.AppendLine("");
            builder.AppendLine("             _BLL.GetBusinessByKey(\" - \", true);//加载一个空的业务对象");
            builder.AppendLine("");
            builder.AppendLine("             ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage ");
            builder.AppendLine("         }");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          /// 重写保存");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <param name=\"sender\">自定义按钮接口</param>");
            builder.AppendLine("          public override void DoSave(IButtonInfo sender)// 保存数据");
            builder.AppendLine("          {");
            builder.AppendLine("              this.UpdateLastControl();//更新最后一个输入框的数据");
            builder.AppendLine("");
            builder.AppendLine("              if (!ValidatingSummaryData(_BLL.CurrentBusiness.Tables[" + param.ORM_Name + ".__TableName])) return; //检查主表数据合法性");
            builder.AppendLine("              //if (!ValidatingDetailData(_BLL.CurrentBusiness.Tables[" + param.ORM_Name + "s.__TableName])) return;//检查子表数据合法性");
            builder.AppendLine("");
            builder.AppendLine("              if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志");
            builder.AppendLine("");
            builder.AppendLine("              DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据");
            builder.AppendLine("");
            builder.AppendLine("             SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法");
            builder.AppendLine("");
            builder.AppendLine("              if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．");
            builder.AppendLine("              {");
            builder.AppendLine("                  if (_UpdateType == UpdateType.Add) _BLL.DataBindRow[" + param.ORM_Name + ".__KeyName] = result.DocNo; //更新单据号码");
            builder.AppendLine("                  //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人");
            builder.AppendLine("");
            builder.AppendLine("                  this.UpdateSummaryRow(_BLL.DataBindRow);//刷新表格内当前记录的缓存数据.");
            builder.AppendLine("                  this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据");
            builder.AppendLine("                  if (_UpdateType == UpdateType.Add) gvSummary.MoveLast(); //如是新增单据,移动到取后一条记录.");
            builder.AppendLine("                  base.DoSave(sender); //调用基类的方法. 此行代码应放较后位置.");
            builder.AppendLine("                  Msg.ShowInformation(\"保存成功!\");");
            builder.AppendLine("              }");
            builder.AppendLine("              else //无查询条件不返回数据");
            builder.AppendLine("                  Msg.Warning(\"保存失败!\");");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("          #region 主从表数据输入合法性检查");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          /// 检查主表数据");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <param name=\"summary\"></param>");
            builder.AppendLine("          /// <returns></returns>");
            builder.AppendLine("          private bool ValidatingSummaryData(DataTable summary)");
            builder.AppendLine("          {");
            builder.AppendLine("              //编写验证条件");
            builder.AppendLine("              //if (string.IsNullOrEmpty(ConvertEx.ToString(txt主键.Text)))");
            builder.AppendLine("              //{");
            builder.AppendLine("                    //Msg.Warning(\"主键不能为空!\");");
            builder.AppendLine("                    //txt主键.Focus();");
            builder.AppendLine("                    //return false;");
            builder.AppendLine("              //}");
            builder.AppendLine("              return true;");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          ///检查子表数据");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <param name=\"summary\"></param>");
            builder.AppendLine("          /// <returns></returns>");
            builder.AppendLine("          private bool ValidatingDetailData(DataTable summary)");
            builder.AppendLine("          {");
            builder.AppendLine("              //编写验证条件");
            builder.AppendLine("              //if (string.IsNullOrEmpty(ConvertEx.ToString(txt子表字段.Text)))");
            builder.AppendLine("              //{");
            builder.AppendLine("                    //Msg.Warning(\"子表字段格式不正确!\");");
            builder.AppendLine("                    //txt子表字段.Focus();");
            builder.AppendLine("                    //return false;");
            builder.AppendLine("              //}");
            builder.AppendLine("              return true;");
            builder.AppendLine("          }");
            builder.AppendLine("          #endregion");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          //按钮状态改变时触发的事件");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          protected override void ButtonStateChanged(UpdateType currentState)");
            builder.AppendLine("          {");
            builder.AppendLine("              //设置编辑明细页面的控件");
            builder.AppendLine("              //this.SetDetailEditorsAccessable(xtraTabPage1, this.DataChanged); //明细控件");
            builder.AppendLine("              this.SetDetailEditorsAccessable(panel1, this.DataChanged);");
            builder.AppendLine("              //txt锁定字段.Enabled = false; ");
            builder.AppendLine("              //lblStatus.Text = this.UpdateTypeName; //修改状态");
            builder.AppendLine("              //this.SetEditorEnable(txt弹窗字段, false, true);//禁用");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          //绑定主表控件内容");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <param name=\"dataSource\"></param>");
            builder.AppendLine("          protected override void DoBindingSummaryEditor(DataTable dataSource)");
            builder.AppendLine("          {");
            builder.AppendLine("              if (dataSource == null) return;");
            builder.AppendLine("              //根据页面内容修改绑定");
            builder.AppendLine("              //DataBinder.BindingTextEdit(txt字段, dataSource, " + param.ORM_Name + ".字段);");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          /// 绑定子表控件内容");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <param name=\"dataSource\"></param>");
            builder.AppendLine("          protected override void DoBindingDetailEditor(DataSet dataSource)");
            builder.AppendLine("          {");
            builder.AppendLine("              //绑定明细.根据子表orm命名修改 ");
            builder.AppendLine("              //DataTable Detail = dataSource.Tables[" + param.ORM_Name + "s.__TableName];");
            builder.AppendLine("              //if (Detail == null || Detail.Rows.Count == 0) { Detail.Rows.Add(); }");
            builder.AppendLine("              //DataBinder.BindingTextEdit(txt子表字段, Detail, " + param.ORM_Name + "s.参数1);");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("          /// <summary>");
            builder.AppendLine("          /// 绑定列表数据");
            builder.AppendLine("          /// </summary>");
            builder.AppendLine("          /// <returns></returns>");
            builder.AppendLine("          protected override bool DoSearchSummary()");
            builder.AppendLine("          {");
            builder.AppendLine("              DataTable dt = (_BLL as " + param.BLL_Name + ").GetSummaryByParam(txt_DocNoFrom.Text, txt_DocNoTo.Text, txt_DocDateFrom.DateTime, txt_DocDateTo.DateTime);");
            builder.AppendLine("              DoBindingSummaryGrid(dt); //绑定主表的Grid");
            builder.AppendLine("              ShowSummaryPage(true); //显示Summary页面. ");
            builder.AppendLine("              return dt != null && dt.Rows.Count > 0;");
            builder.AppendLine("          }");
            builder.AppendLine("");
            builder.AppendLine("     }");
            builder.AppendLine("}");
            builder.AppendLine("");

            return builder.ToString();
        }

        /// <summary>
        /// 生成注释部分
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="concretelyName"></param>
        private void CreateComment(StringBuilder builder, string concretelyName)
        {
            builder.AppendLine("");
            builder.AppendLine("/*==========================================");
            builder.AppendLine(" *   程序说明: " + concretelyName + "的业务页面");
            builder.AppendLine(" *   作者姓名: ATOM");
            builder.AppendLine(" *   创建日期: " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
            builder.AppendLine(" *   最后修改: " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
            builder.AppendLine(" *   ");
            builder.AppendLine(" *   注: 本代码由[代码生成器]自动生成");
            builder.AppendLine(" *   版权所有 Copyright © . 2015");
            builder.AppendLine(" *==========================================*/");
            builder.AppendLine("");
        }
    }
    
}
