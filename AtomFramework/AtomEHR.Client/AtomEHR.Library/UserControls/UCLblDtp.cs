﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UCLblDtp : UserControl
    {
        public UCLblDtp()
        {
            InitializeComponent();
        }
        #region MyRegion
        [Category("Customer")]
        public DateEdit Dtp1
        {
            get { return this.dtp1; }
            set { this.dtp1 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }

        [Category("Custom Size")]
        public Size Dtp1Size
        {
            get { return this.dtp1.Size; }
            set { this.dtp1.Size = value; }
        }
        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
         [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        #endregion
    }
}
