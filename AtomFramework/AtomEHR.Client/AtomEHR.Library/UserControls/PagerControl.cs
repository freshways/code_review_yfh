﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Common;

namespace TActionProject
{
    public partial class PagerControl : UserControl
    {
        #region 构造函数

        public PagerControl()
        {
            InitializeComponent();
        }

        #endregion

        #region 分页字段和属性

        private int pageIndex = 1;
        /// <summary>
        /// 当前页面
        /// </summary>
        public virtual int PageIndex
        {
            get { return pageIndex; }
            set { pageIndex = value; }
        }

        private int pageSize = 100;
        /// <summary>
        /// 每页记录数
        /// </summary>
        public virtual int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        private int recordCount = 0;
        /// <summary>
        /// 总记录数
        /// </summary>
        public virtual int RecordCount
        {
            get { return recordCount; }
            set { recordCount = value; }
        }

        private int pageCount = 0;
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount
        {
            get
            {
                if (pageSize != 0)
                {
                    pageCount = GetPageCount();
                }
                return pageCount;
            }
        }

        private string jumpText;
        /// <summary>
        /// 跳转按钮文本
        /// </summary>
        public string JumpText
        {
            get
            {
                if (string.IsNullOrEmpty(jumpText))
                {
                    jumpText = "跳转";
                }
                return jumpText;
            }
            set { jumpText = value; }
        }
        /// <summary>
        /// 查询语句
        /// </summary>
        public string SqlQuery
        {
            get;
            set;
        }
        public SqlParameter[] paramters
        {
            get;
            set;
        }
        #endregion

        #region 页码变化触发事件

        public event EventHandler OnPageChanged;

        #endregion

        #region 分页及相关事件功能实现

        private void SetFormCtrEnabled()
        {
            lnkFirst.Enabled = true;
            lnkPrev.Enabled = true;
            lnkNext.Enabled = true;
            lnkLast.Enabled = true;
            btnGo.Enabled = true;
        }

        /// <summary>
        /// 计算总页数
        /// </summary>
        /// <returns></returns>
        private int GetPageCount()
        {
            if (PageSize == 0)
            {
                return 0;
            }
            int pageCount = RecordCount / PageSize;
            if (RecordCount % PageSize == 0)
            {
                pageCount = RecordCount / PageSize;
            }
            else
            {
                pageCount = RecordCount / PageSize + 1;
            }
            return pageCount;
        }
        /// <summary>
        /// 外部调用
        /// </summary>
        public void DrawControl()
        {
            DrawControl(false);
        }
        /// <summary>
        /// 页面控件呈现
        /// </summary>
        private void DrawControl(bool callEvent)
        {
            btnGo.Text = JumpText;
            lblCurrentPage.Text = PageIndex.ToString();
            lblPageCount.Text = PageCount.ToString();
            lblTotalCount.Text = RecordCount.ToString();
            txtPageSize.Text = PageSize.ToString();

            if (callEvent && OnPageChanged != null)
            {
                OnPageChanged(this, null);//当前分页数字改变时，触发委托事件
            }
            SetFormCtrEnabled();
            if (PageCount == 1)//有且仅有一页
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
                lnkNext.Enabled = false;
                lnkLast.Enabled = false;
                btnGo.Enabled = false;
            }
            else if (PageIndex == 1)//第一页
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
            }
            else if (PageIndex == PageCount)//最后一页
            {
                lnkNext.Enabled = false;
                lnkLast.Enabled = false;
            }
        }

        #endregion

        #region 相关控件事件

        private void lnkFirst_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PageIndex = 1;
            DrawControl(true);
        }

        private void lnkPrev_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PageIndex = Math.Max(1, PageIndex - 1);
            DrawControl(true);
        }

        private void lnkNext_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PageIndex = Math.Min(PageCount, PageIndex + 1);
            DrawControl(true);
        }

        private void lnkLast_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PageIndex = PageCount;
            DrawControl(true);
        }

        /// <summary>
        /// enter键功能
        /// </summary>
        private void txtPageNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                int num = 0;
                if (int.TryParse(txtPageNum.Text.Trim(), out num) && num > 0)
                {
                    if (num > PageCount)
                    {
                        txtPageNum.Text = PageCount.ToString();
                    }
                }
                else
                { Msg.ShowInformation("无效页数"); return; }
                btnGo_Click(sender, e);
            }
        }

        /// <summary>
        /// 跳转页数限制
        /// </summary>
        private void txtPageNum_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGo_Click(object sender, EventArgs e)
        {
            int num = 0;
            if (int.TryParse(txtPageNum.Text.Trim(), out num) && num > 0)
            {
                PageIndex = num;
                DrawControl(true);
            }
        }

        #endregion
        bool isTextChanged = false;
        /// <summary>
        /// 分页属性改变了。
        /// </summary>
        private void txtPageSize_TextChanged(object sender, EventArgs e)
        {
            int num = 0;
            if (!int.TryParse(txtPageSize.Text.Trim(), out num) || num <= 0)
            {
                num = 100;
                txtPageSize.Text = "100";
            }
            else
            {
                isTextChanged = true;
            }
            pageSize = num;
        }
        /// <summary>
        /// 光标离开分页属性
        private void txtPageSize_Leave(object sender, EventArgs e)
        {
            if (isTextChanged)
            {
                // isTextChanged = false;
                //lnkFirst_LinkClicked(null, null);
            }
        }

        private void txtPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.)
            //{

            //}
        }

        private void txtPageSize_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (isTextChanged)
                {
                    isTextChanged = false;
                    lnkFirst_LinkClicked(null, null);
                }
            }
        }

        /// <summary>
        /// 返回Grid的数据源
        /// edited：王茂国
        /// 2015-04-19
        /// </summary>
        /// <param name="sqlSource">传入sql查询语句</param>
        /// <returns></returns>
        public DataSet GetQueryResult(string tableName, string strWhere)
        {
            strWhere = "1=1 " + strWhere; //处理存储过程里面没有默认条件问题
            if (!string.IsNullOrEmpty(tableName))
            {
                DataSet table = new bllBase().Get分页数据(tableName, "*", strWhere, "ID", this.PageSize, this.PageIndex, out this.recordCount,"DESC");
                return table;
                //string sqlDataSource = string.Format("SELECT  TOP {0}* FROM ( SELECT A.*, ROWNUM RN FROM ({0}) A WHERE ROWNUM <={1} ) WHERE RN >={2}", sqlSource, (this.PageIndex) * this.PageSize, (this.PageIndex - 1) * this.PageSize + 1);
                //DataTable source = DbHelperOra.Query(sqlDataSource, paramters).Tables[0];
                //return source;
            }
            else
                return null;
        }

        /// <summary>
        /// 返回Grid的数据源
        /// 2015-10-13 16:49:32 yufh 重载
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public DataSet GetQueryResult(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            strWhere = "1=1 " + strWhere; //处理存储过程里面没有默认条件问题
            if (!string.IsNullOrEmpty(tableName))
            {
                DataSet table = new bllBase().Get分页数据(tableName, Fields, strWhere, SortExpression, this.PageSize, this.PageIndex, out this.recordCount, SortDire);
                return table;
            }
            else
                return null;
        }

        /// <summary>
        /// 返回Grid的数据源
        /// 2015-12-25 10:55:17 yufh 
        /// </summary>
        /// <param name="tableName">表名/多表join on a = b </param>
        /// <param name="Fields">查询字段,多表时指定查询字段</param>
        /// <param name="strWhere">查询条件， 由于添加了多表-所以在添加条件的时候注意</param>
        /// <param name="SortExpression">排序字段，由于添加了多表-所以在添加条件的时候注意</param>
        /// <param name="SortDire">排序类型，默认DESC</param>
        /// <returns></returns>
        public DataSet GetQueryResultNew(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            strWhere = "1=1 " + strWhere; //处理存储过程里面没有默认条件问题
            if (!string.IsNullOrEmpty(tableName))
            {
                DataSet table = new bllBase().Get分页数据New(tableName, Fields, strWhere, SortExpression, this.PageSize, this.PageIndex, out this.recordCount, SortDire);
                return table;
            }
            else
                return null;
        }
        /// <summary>
        /// wxf 2018年10月13日 16:30:52
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="Fields"></param>
        /// <param name="strWhere"></param>
        /// <param name="SortExpression"></param>
        /// <param name="SortDire"></param>
        /// <returns></returns>
        public DataSet GetQueryResultNew_New(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            strWhere = "1=1 " + strWhere; //处理存储过程里面没有默认条件问题
            if (!string.IsNullOrEmpty(tableName))
            {
                DataSet table = new bllBase().Get分页数据New_New(tableName, Fields, strWhere, SortExpression, this.PageSize, this.PageIndex, out this.recordCount, SortDire);
                return table;
            }
            else
                return null;
        }
        /// <summary>
        /// 初始化控件
        /// </summary>
        public void InitControl()
        {
            this.PageIndex = 1;
            this.PageSize = 15;
        }

    }
}