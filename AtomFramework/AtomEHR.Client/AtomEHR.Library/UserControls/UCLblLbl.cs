﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UCLblLbl : UserControl
    {
        public UCLblLbl()
        {
            InitializeComponent();
        }
        #region MyRegion
        [Category("Customer")]
        public LabelControl Lbl2
        {
            get { return this.lbl2; }
            set { this.lbl2 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }

        [Category("Custom Size")]
        public Size Lbl2Size
        {
            get { return this.lbl2.Size; }
            set { this.lbl2.Size = value; }
        }
        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
         [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        #endregion
    }
}
