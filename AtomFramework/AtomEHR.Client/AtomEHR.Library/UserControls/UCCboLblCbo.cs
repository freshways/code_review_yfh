﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UCCboLblCbo : UserControl
    {
        public UCCboLblCbo()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Customer")]
        public ComboBoxEdit Cbo1
        {
            get { return this.cbo1; }
            set { this.cbo1 = value; }
        }
        [Category("Customer")]
        public ComboBoxEdit Cbo2
        {
            get { return this.cbo2; }
            set { this.cbo2 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }
  
        [Category("Custom Size")]
        public Size Cbo1Size
        {
            get { return this.cbo1.Size; }
            set { this.cbo1.Size = value; }
        }
        [Category("Custom Size")]
        public Size Cbo2Size
        {
            get { return this.cbo2.Size; }
            set { this.cbo2.Size = value; }
        }
        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
      
        [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        #endregion
    }
}
