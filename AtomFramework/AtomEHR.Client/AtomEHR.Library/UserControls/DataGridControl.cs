﻿using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.Library.UserControls
{
    public partial class DataGridControl : DevExpress.XtraGrid.GridControl
    {
        private BarManager gridBarManager;
        private Bar bar1;
        private Bar bar2;
        private Bar bar3;
        private DevExpress.XtraBars.PopupMenu gridPopupMenu;
        private BaseEdit inplaceEditor;
        private bool _isOnLoaded = false;
        private string _strWhere = string.Empty;

        public string StrWhere
        {
            get { return _strWhere; }
            set { _strWhere = value; }
        }
        private string _view = string.Empty;

        public string View
        {
            get { return _view; }
            set { _view = value; }
        }

        private bool _isBestFitColumns = true;

        public bool IsBestFitColumns
        {
            get { return _isBestFitColumns; }
            set { _isBestFitColumns = value; }
        }

        private bool _allowBandedGridColumnSort = false;

        public bool AllowBandedGridColumnSort
        {
            get { return _allowBandedGridColumnSort; }
            set { _allowBandedGridColumnSort = value; }
        }

        int _dropDownRowCount = 50;


        private bool showContextMenu = false;
        /// <summary>
        /// 在Grid中点击右键，是否会显示“ConvertToExcel2007”的菜单。
        /// 默认值是true ，即显示
        /// </summary>
        public bool ShowContextMenu
        {
            get { return showContextMenu; }
            set { showContextMenu = value; }
        }

        private bool useCheckBox = false;
        /// <summary>
        /// Grid显示最左边的选择列  默认false
        /// 默认值是true ，即显示
        /// </summary>
        public bool UseCheckBox
        {
            get { return useCheckBox; }
            set { useCheckBox = value; }
        }
        private GridView GridView
        {
            get
            {
                SetGridViewAllowMouseWheel(base.MainView as GridView, false);
                return base.MainView as GridView;
            }
        }

        public void SetGridViewAllowMouseWheel(GridView grv, bool allow)
        {
            if (grv != null)
            {
                foreach (GridColumn gcol in grv.Columns)
                {
                    if (gcol.ColumnEdit != null)
                        (gcol.ColumnEdit as RepositoryItem).AllowMouseWheel = allow;
                }
            }
        }

        public DataGridControl()
        {
            InitializeComponent();
            //InitGridPopupMenu();//鼠标右键点击弹出的功能按钮，暂时注释

            //this.ShowRowIndex = true;

            this.MouseClick += DataGridControl_MouseClick;
            this.Load += DataGridControl_Load;
            this.MouseDoubleClick += DataGridControl_MouseDoubleClick;
            // this.gridPopupMenu.Popup += gridPopupMenu_Popup;
            this.DataSourceChanged += new EventHandler(DataGridControl_DataSourceChanged);
            this.ProcessGridKey += new KeyEventHandler(DataGridControl_ProcessGridKey);
        }



        void DataGridControl_DataSourceChanged(object sender, EventArgs e)
        {
            if (this.MainView is BandedGridView)
                return;
            //if (this.MainView is GridView && this.IsBestFitColumns)
            //{
            //    GridView grv = this.MainView as GridView;
            //    grv.BestFitColumns();
            //}

        }
        void DataGridControl_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (this.MainView is GridView)
            {
                if (e.KeyValue == 37)
                {
                    GridView gv = this.MainView as GridView;
                    gv.LeftCoord -= 20;
                }
                else if (e.KeyValue == 39)
                {
                    GridView gv = this.MainView as GridView;
                    gv.LeftCoord += 20;
                }
            }
        }


        private void SetBandBestWidth(GridBand band)
        {
            Graphics graphics = this.CreateGraphics();
            SizeF bandCaptionSize = graphics.MeasureString(band.Caption, band.AppearanceHeader.Font);
            int minWidth = (int)bandCaptionSize.Width + 5;
            band.Width = band.Width >= minWidth ? band.Width : minWidth;
            if (band.HasChildren)
            {
                foreach (GridBand childBand in band.Children)
                {
                    SetBandBestWidth(childBand);
                }
            }
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            if (!this.DesignMode && !_isOnLoaded && this.GridView != null)
            {
                _isOnLoaded = true;

                this.GridView.ShownEditor += GridView_ShownEditor;
                this.GridView.HiddenEditor += GridView_HiddenEditor;
                //Grid加载时，设置Grid中的Combox输出模式为displayText 
                //add by wang.maoguo  2014-7-24  
                SetGridComboExportMode();
            }
        }

        void GridView_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += inplaceEditor_DoubleClick;
        }

        void GridView_HiddenEditor(object sender, EventArgs e)
        {
            if (inplaceEditor != null)
            {
                inplaceEditor.DoubleClick -= inplaceEditor_DoubleClick;
                inplaceEditor = null;
            }
        }

        void inplaceEditor_DoubleClick(object sender, EventArgs e)
        {
            BaseEdit editor = (BaseEdit)sender;
            GridControl grid = (GridControl)editor.Parent;
            Point pt = grid.PointToClient(Control.MousePosition);
            GridView view = (GridView)grid.FocusedView;
            CopyCellValueToClipBoard(view, pt);
        }

        private static void CopyCellValueToClipBoard(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow && info.InRowCell)
            {
                //try
                //{
                string str = view.GetRowCellValue(info.RowHandle, info.Column) as string;
                if (view.GetRowCellValue(info.RowHandle, info.Column) != null
                    && view.GetRowCellValue(info.RowHandle, info.Column).ToString() != String.Empty)
                {
                    Clipboard.SetText(view.GetRowCellValue(info.RowHandle, info.Column).ToString());
                }
                //}
                //catch (Exception ex)
                //{
                //}
            }
        }

        void gridPopupMenu_Popup(object sender, EventArgs e)
        {
            UnSubscribePopupMenuEvents();
            SubscribePopupMenuEvents();
        }

        private void SubscribePopupMenuEvents()
        {
            ((PopupControl)this.gridPopupMenu).IPopup.CustomControl.MouseLeave += new EventHandler(PopupMenuForm_MouseLeave);
            ((PopupControl)this.gridPopupMenu).IPopup.CustomControl.FindForm().MouseLeave += new EventHandler(PopupMenuForm_MouseLeave);
        }

        private void UnSubscribePopupMenuEvents()
        {
            ((PopupControl)this.gridPopupMenu).IPopup.CustomControl.MouseLeave -= new EventHandler(PopupMenuForm_MouseLeave);
            ((PopupControl)this.gridPopupMenu).IPopup.CustomControl.FindForm().MouseLeave -= new EventHandler(PopupMenuForm_MouseLeave);
        }

        bool IsCursorOnMenu(PopupMenu menu)
        {
            return ((PopupControl)this.gridPopupMenu).IPopup.CustomControl.FindForm().Bounds.Contains(Control.MousePosition);
        }

        void PopupMenuForm_MouseLeave(object sender, EventArgs e)
        {
            if (!IsCursorOnMenu(this.gridPopupMenu))
                this.gridPopupMenu.HidePopup();
        }

        void DataGridControl_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.MainView is GridView)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    Point pt = GridView.GridControl.PointToClient(Control.MousePosition);
                    CopyCellValueToClipBoard(this.GridView, pt);
                }
            }
        }

        public void InitGridPopupMenu()
        {
            this.gridPopupMenu = new DevExpress.XtraBars.PopupMenu();
            this.gridBarManager = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            // 
            // gridPopupMenu
            // 
            this.gridPopupMenu.AllowRibbonQATMenu = false;
            this.gridPopupMenu.Manager = this.gridBarManager;
            this.gridPopupMenu.Name = "gridPopupMenu";
            // 
            // gridBarManager
            // 
            this.gridBarManager.MaxItemId = 0;
            // 
            // bar1
            // 
            this.bar1.DockCol = 0;
            // 
            // bar2
            // 
            this.bar2.DockCol = 0;
            // 
            // bar3
            // 
            this.bar3.DockCol = 0;

            string[] menuString = new string[] {
            "ConvertToExcelWithOrigianlData" 
            ,"ConvertToExcel" 
            ,"ConvertToExcelWithViewData"            
            ,"ConvertToExcel2007"            
            ,"ConvertToMHT"
            ,"ConvertToText"
            ,"ConvertToPDF"
        };
            CreateContextMenu(menuString);
        }

        private void CreateContextMenu(string[] menuString)
        {
            gridPopupMenu.ClearLinks();

            foreach (string menuName in menuString)
            {
                BarButtonItem gridBarButtonItem = new BarButtonItem(gridBarManager, menuName);

                gridBarButtonItem.Tag = menuName;

                gridBarButtonItem.ItemClick -= new ItemClickEventHandler(gridBarButtonItem_ItemClick);

                gridBarButtonItem.ItemClick += new ItemClickEventHandler(gridBarButtonItem_ItemClick);

                gridPopupMenu.ItemLinks.Add(gridBarButtonItem);

            }
        }

        void gridBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {

            //    string[] menuString = new string[] {
            //    "ConvertToExcelWithOrigianlData" 
            //    ,"ConvertToExcel" 
            //    ,"ConvertToExcelWithViewData"            
            //    ,"ConvertToExcel2007"            
            //    ,"ConvertToMHT"
            //    ,"ConvertToText"
            //    ,"ConvertToPDF"
            //};

            string menu = (sender as BarManager).PressedLink.Item.Tag.ToString();
            SaveFileDialog sfd = new SaveFileDialog();
            switch (menu)
            {
                case "ConvertToExcelWithOrigianlData":
                    sfd.Filter = "Microsoft Excel|*.xls;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        //XtraGridHelper.ExcelExport(this.GridView, null);
                        //SetGridComboExportMode();
                        this.GridView.ExportToXls(sfd.FileName.ToString(), true);
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                    break;
                case "ConvertToExcel":
                    sfd.Filter = "Microsoft Excel|*.xls;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToXls(sfd.FileName.ToString(), true);
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }

                    break;
                case "ConvertToExcelWithViewData":
                    sfd.Filter = "Microsoft Excel|*.xls;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToXls(sfd.FileName.ToString(), true);
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }

                    break;
                case "ConvertToExcel2007":
                    sfd.Filter = "Microsoft Excel|*.xlsx;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToXlsx(sfd.FileName.ToString());
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }

                    break;
                case "ConvertToMHT":
                    sfd.Filter = "Web Documents |*.mht;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToMht(sfd.FileName.ToString());
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                    break;
                case "ConvertToText":
                    sfd.Filter = "Text Documents|*.txt;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToText(sfd.FileName.ToString());
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                    break;
                case "ConvertToPDF":
                    sfd.Filter = "PDF|*.pdf;";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        this.GridView.ExportToPdf(sfd.FileName.ToString());
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                    break;
                default:
                    break;
            }

        }
        /// <summary>
        /// 设置Grid中的comboxEdit的输出模式为DisplayText
        /// add by wangmaoguo  2014-7-24
        /// </summary>
        private void SetGridComboExportMode()
        {
            for (int i = 0; i < GridView.Columns.Count; i++)
            {
                if (GridView.Columns[i].ColumnEdit != null && GridView.Columns[i].ColumnEdit.GetType() == typeof(DevExpress.XtraEditors.Repository.RepositoryItemComboBox))
                {
                    GridView.Columns[i].ColumnEdit.ExportMode = ExportMode.DisplayText;
                }
            }
        }

        void DataGridControl_MouseClick(object sender, MouseEventArgs e)
        {
            //if (ShowContextMenu)
            //{
            //    if (e.Button == System.Windows.Forms.MouseButtons.Right)
            //    {
            //        gridPopupMenu.ShowPopup(Cursor.Position);
            //    }
            //    else
            //    {
            //        gridPopupMenu.HidePopup();
            //    }
            //}

            if (this.MainView is BandedGridView && this.AllowBandedGridColumnSort)
            {

                BandedGridView bgv = this.MainView as BandedGridView;
                BandedGridHitInfo hitInfo = bgv.CalcHitInfo(e.X, e.Y) as BandedGridHitInfo;
                if (hitInfo != null && hitInfo.InBandPanel && hitInfo.Band != null)// 不判断hitInfo.Band！=null点击序号列时会报错
                {
                    foreach (BandedGridColumn column in bgv.Columns)
                    {
                        if (!hitInfo.Band.HasChildren && column.OwnerBand != hitInfo.Band)
                        {
                            column.SortOrder = DevExpress.Data.ColumnSortOrder.None;
                        }

                    }

                    foreach (BandedGridColumn column in bgv.Columns)
                    {
                        if (!hitInfo.Band.HasChildren && column.OwnerBand == hitInfo.Band)
                        {
                            if (column.SortOrder == DevExpress.Data.ColumnSortOrder.Descending)
                            {
                                column.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                            }
                            else
                            {
                                column.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                            }
                            return;
                        }
                    }
                }
            }

        }


        private void DataGridControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (this.MainView is DevExpress.XtraGrid.Views.Card.CardView)
                {
                    DevExpress.XtraGrid.Views.Card.CardView cardView = this.MainView as DevExpress.XtraGrid.Views.Card.CardView;
                    this.Font = new System.Drawing.Font("宋体", 9.0F);
                    foreach (DevExpress.Utils.AppearanceObject appearaceObject in cardView.Appearance)
                    {
                        appearaceObject.Font = new System.Drawing.Font("宋体", 9.0F);
                    }
                    foreach (DevExpress.XtraGrid.Columns.GridColumn column in cardView.Columns)
                    {
                        if (column.OptionsColumn.AllowEdit == true)
                        {
                            //column.AppearanceCell.BackColor = System.Drawing.Color.White;
                        }

                    }
                    foreach (DevExpress.XtraEditors.Repository.RepositoryItem repositoryItem in this.RepositoryItems)
                    {

                        if (repositoryItem.GetType() == typeof(RepositoryItemTextEdit))
                        {
                            RepositoryItemTextEdit txt = repositoryItem as RepositoryItemTextEdit;
                            txt.Click += new EventHandler(txt_Click);
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemComboBox))
                        {
                            RepositoryItemComboBox cbo = repositoryItem as RepositoryItemComboBox;
                            cbo.DropDownRows = _dropDownRowCount;
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemDateEdit))
                        {
                            RepositoryItemDateEdit dtp = repositoryItem as RepositoryItemDateEdit;
                            dtp.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(dtp_ParseEditValue);
                        }
                    }
                }
                else if (this.MainView is DevExpress.XtraGrid.Views.Layout.LayoutView)
                {
                    DevExpress.XtraGrid.Views.Layout.LayoutView layoutView = this.MainView as DevExpress.XtraGrid.Views.Layout.LayoutView;
                    this.Font = new System.Drawing.Font("宋体", 9.0F);
                    foreach (DevExpress.Utils.AppearanceObject appearaceObject in layoutView.Appearance)
                    {
                        appearaceObject.Font = new System.Drawing.Font("宋体", 9.0F);
                    }
                    foreach (DevExpress.XtraGrid.Columns.GridColumn column in layoutView.Columns)
                    {
                        if (column.OptionsColumn.AllowEdit == true)
                        {
                            //  column.AppearanceCell.BackColor = System.Drawing.Color.White;
                        }
                    }
                    foreach (DevExpress.XtraEditors.Repository.RepositoryItem repositoryItem in this.RepositoryItems)
                    {
                        if (repositoryItem.GetType() == typeof(RepositoryItemTextEdit))
                        {
                            RepositoryItemTextEdit txt = repositoryItem as RepositoryItemTextEdit;
                            txt.Click += new EventHandler(txt_Click);
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemComboBox))
                        {
                            RepositoryItemComboBox cbo = repositoryItem as RepositoryItemComboBox;
                            cbo.DropDownRows = _dropDownRowCount;
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemDateEdit))
                        {
                            RepositoryItemDateEdit dtp = repositoryItem as RepositoryItemDateEdit;
                            dtp.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(dtp_ParseEditValue);
                        }
                    }
                }
                else if (this.MainView is DevExpress.XtraGrid.Views.Grid.GridView)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gridView = this.MainView as DevExpress.XtraGrid.Views.Grid.GridView;

                    //gridView.OptionsView.EnableAppearanceEvenRow = true;
                    //gridView.OptionsView.EnableAppearanceOddRow = true;
                    gridView.ColumnPanelRowHeight = 30;
                    this.Font = new System.Drawing.Font("宋体", 9.0F);
                    foreach (DevExpress.Utils.AppearanceObject appearaceObject in gridView.Appearance)
                    {
                        appearaceObject.Font = new System.Drawing.Font("宋体", 9.0F);
                    }

                    //设置合计的字体
                    gridView.Appearance.FooterPanel.Font = new System.Drawing.Font(gridView.Appearance.FooterPanel.Font, FontStyle.Bold);


                    //gridView.GroupPanelText = Utility.ControlsHelper.GetResourceString("GridGroupPanelText");
                    gridView.OptionsView.ShowGroupPanel = false;
                    gridView.GroupPanelText = "DragColumn";
                    foreach (DevExpress.XtraGrid.Columns.GridColumn column in gridView.Columns)
                    {
                        if (column.OptionsColumn.AllowEdit == true)
                        {
                            //column.AppearanceCell.BackColor = System.Drawing.Color.White;
                        }
                        column.AppearanceHeader.Font = new Font(column.AppearanceHeader.Font, FontStyle.Bold);
                    }

                    if (gridView is BandedGridView)
                    {
                        BandedGridView bandGridView = gridView as BandedGridView;
                        bandGridView.BandPanelRowHeight = 30;
                        foreach (GridBand band in bandGridView.Bands)
                        {
                            band.AppearanceHeader.Font = new Font(band.AppearanceHeader.Font, FontStyle.Bold);
                            SetChildrenBand(band);
                        }
                    }

                    foreach (DevExpress.XtraEditors.Repository.RepositoryItem repositoryItem in this.RepositoryItems)
                    {
                        //added by kuangyongsheng for the grid textEdit  2013-7-29 9:46:20
                        //when the grid textEdit focused ,select all of the text content
                        if (repositoryItem.GetType() == typeof(RepositoryItemTextEdit))
                        {
                            RepositoryItemTextEdit txt = repositoryItem as RepositoryItemTextEdit;
                            txt.Click += new EventHandler(txt_Click);
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemComboBox))
                        {
                            RepositoryItemComboBox cbo = repositoryItem as RepositoryItemComboBox;
                            cbo.DropDownRows = _dropDownRowCount;
                        }
                        else if (repositoryItem.GetType() == typeof(RepositoryItemDateEdit))
                        {
                            RepositoryItemDateEdit dtp = repositoryItem as RepositoryItemDateEdit;
                            dtp.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(dtp_ParseEditValue);
                        }

                    }
                    //if (this.ShowRowIndex && gridView.OptionsView.ShowIndicator)
                    SetRowNumberIndicator(gridView);//显示行号
                    if (this.useCheckBox) SetUseCheck(gridView);//显示单选列
                    //if (this._isBestFitColumns && gridView.GetType() == typeof(GridView))
                    //    gridView.BestFitColumns();
                }
            }
        }
        private void SetChildrenBand(GridBand band)
        {
            if (band.HasChildren)
            {
                foreach (GridBand childBand in band.Children)
                {
                    childBand.AppearanceHeader.Font = new Font(childBand.AppearanceHeader.Font, FontStyle.Bold);
                    SetChildrenBand(childBand);
                }
            }
        }

        //added by kuangyongsheng for the grid textEdit  2013-7-29 9:46:20
        //when the grid textEdit focused ,select all of the text content
        private void txt_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit txt = sender as DevExpress.XtraEditors.TextEdit;
            txt.SelectAll();
            txt.Leave += new EventHandler(txt_Leave);
            txt.Click -= new EventHandler(txt_Click);
        }
        private void txt_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit txt = sender as DevExpress.XtraEditors.TextEdit;
            txt.Click += new EventHandler(txt_Click);
        }
        private void dtp_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            try
            {
                object newValue = e.Value;
                if (newValue == null
                    || newValue.ToString().Trim().Length < 8)
                {
                    e.Value = null;
                    return;
                }
                if (newValue.ToString().Length == 8)
                {
                    e.Value = DateTime.ParseExact(newValue.ToString(), "yyyyMMdd", null).ToString("yyyy-MM-dd");
                }
                else if (newValue.ToString().Length == 10)
                {
                    e.Value = Convert.ToDateTime(newValue).ToString("yyyy-MM-dd");
                }
                else
                {
                    e.Value = Convert.ToDateTime(newValue).ToString("yyyy-MM-dd");
                }
            }
            catch (Exception)
            {
                e.Value = null;
            }

        }
        #region Column Handling

        public new void SetAllColumnEditable(bool editable)
        {
            GridColumnCollection columnCollection = null;

            if (this.MainView is DevExpress.XtraGrid.Views.Card.CardView)
            {
                DevExpress.XtraGrid.Views.Card.CardView cardView = this.MainView as DevExpress.XtraGrid.Views.Card.CardView;

                columnCollection = cardView.Columns;
            }
            else if (this.MainView is DevExpress.XtraGrid.Views.Layout.LayoutView)
            {
                DevExpress.XtraGrid.Views.Layout.LayoutView layoutView = this.MainView as DevExpress.XtraGrid.Views.Layout.LayoutView;
                columnCollection = layoutView.Columns;
            }
            else if (this.MainView is DevExpress.XtraGrid.Views.Grid.GridView)
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridView = this.MainView as DevExpress.XtraGrid.Views.Grid.GridView;
                columnCollection = gridView.Columns;
            }
            if (columnCollection == null) return;
            global::DevExpress.Data.UnboundColumnType oldUnboundColumnType = global::DevExpress.Data.UnboundColumnType.Bound;
            foreach (GridColumn col in columnCollection)
            {
                oldUnboundColumnType = col.UnboundType;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = global::DevExpress.Data.UnboundColumnType.Bound;
                col.OptionsColumn.AllowEdit = editable;
                col.OptionsColumn.AllowFocus = editable;
                col.OptionsColumn.ReadOnly = !editable;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = oldUnboundColumnType;
            }
        }

        public new void SetAllColumnAllowMerge(DefaultBoolean allowMerge)
        {
            GridColumnCollection columnCollection = this.GridView.Columns;

            if (columnCollection == null) return;

            foreach (GridColumn col in columnCollection)
            {
                col.OptionsColumn.AllowMerge = allowMerge;
            }
            this.GridView.FocusedColumn = this.GridView.VisibleColumns[0];
        }

        public new void SetColumnEditable(bool editable, params string[] fieldNames)
        {
            if (fieldNames == null || fieldNames.Length == 0) return;

            GridView gridView = this.GridView;
            GridColumn col = null;
            global::DevExpress.Data.UnboundColumnType oldUnboundColumnType = global::DevExpress.Data.UnboundColumnType.Bound;
            foreach (string fieldName in fieldNames)
            {
                col = gridView.Columns[fieldName];
                if (col != null)
                {
                    oldUnboundColumnType = col.UnboundType;
                    if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = global::DevExpress.Data.UnboundColumnType.Bound;
                    col.OptionsColumn.AllowEdit = editable;
                    col.OptionsColumn.AllowFocus = editable;
                    col.OptionsColumn.ReadOnly = !editable;
                    if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = oldUnboundColumnType;
                }
            }
            gridView.FocusedColumn = gridView.VisibleColumns[0];
        }

        public new void SetColumnEditableByColumn(bool editable, params GridColumn[] columns)
        {
            if (columns == null || columns.Length == 0) return;

            GridView gridView = this.GridView;
            if (gridView == null) return;
            global::DevExpress.Data.UnboundColumnType oldUnboundColumnType = global::DevExpress.Data.UnboundColumnType.Bound;
            foreach (GridColumn col in columns)
            {
                oldUnboundColumnType = col.UnboundType;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = global::DevExpress.Data.UnboundColumnType.Bound;
                col.OptionsColumn.AllowEdit = editable;
                col.OptionsColumn.AllowFocus = editable;
                col.OptionsColumn.ReadOnly = !editable;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = oldUnboundColumnType;
            }
            gridView.FocusedColumn = gridView.VisibleColumns[0];
        }

        public new void SetColumnEditable(bool editable, bool useReadOnlyColor, params string[] fieldNames)
        {
            if (fieldNames == null || fieldNames.Length == 0) return;

            GridView gridView = this.GridView;
            GridColumn col = null;
            global::DevExpress.Data.UnboundColumnType oldUnboundColumnType = global::DevExpress.Data.UnboundColumnType.Bound;
            foreach (string fieldName in fieldNames)
            {
                col = gridView.Columns[fieldName];
                if (col != null)
                {
                    oldUnboundColumnType = col.UnboundType;
                    if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = global::DevExpress.Data.UnboundColumnType.Bound;
                    col.OptionsColumn.AllowEdit = editable;
                    col.OptionsColumn.AllowFocus = editable;
                    col.OptionsColumn.ReadOnly = !editable;
                    if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = oldUnboundColumnType;
                }
            }
            gridView.FocusedColumn = gridView.VisibleColumns[0];
        }

        public new void SetColumnEditableByColumn(bool editable, bool useReadOnlyColor, params GridColumn[] columns)
        {
            if (columns == null || columns.Length == 0) return;

            GridView gridView = this.GridView;
            global::DevExpress.Data.UnboundColumnType oldUnboundColumnType = global::DevExpress.Data.UnboundColumnType.Bound;
            foreach (GridColumn col in columns)
            {
                oldUnboundColumnType = col.UnboundType;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = global::DevExpress.Data.UnboundColumnType.Bound;
                col.OptionsColumn.AllowEdit = editable;
                col.OptionsColumn.AllowFocus = editable;
                col.OptionsColumn.ReadOnly = !editable;
                if (oldUnboundColumnType != global::DevExpress.Data.UnboundColumnType.Bound) col.UnboundType = oldUnboundColumnType;
            }
            gridView.FocusedColumn = gridView.VisibleColumns[0];
        }

        #endregion

        public static void SetRowNumberIndicator(GridView gridView)
        {
            gridView.BeginUpdate();
            gridView.OptionsView.ShowIndicator = true;
            gridView.CustomDrawRowIndicator += new RowIndicatorCustomDrawEventHandler(
                delegate(object sender, RowIndicatorCustomDrawEventArgs e)
                {
                    if (e.RowHandle >= 0)
                    {
                        int rowNumber = e.RowHandle + 1;
                        e.Info.DisplayText = rowNumber.ToString();
                    }
                }
            );

            if (gridView.RowCount >= 1)
            {
                gridView.IndicatorWidth = 22 + gridView.DataRowCount.ToString().Length * ((int)gridView.Appearance.HeaderPanel.Font.Size);
            }

            gridView.RowCountChanged += new EventHandler(
                delegate(object sender, EventArgs e)
                {
                    gridView.IndicatorWidth = 22 + gridView.DataRowCount.ToString().Length * ((int)gridView.Appearance.HeaderPanel.Font.Size);
                }
            );

            gridView.EndUpdate();
        }
        private void SetUseCheck(DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            gridView.BeginUpdate();
            gridView.OptionsSelection.MultiSelect = true;
            gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            for (int i = 0; i < gridView.VisibleColumns.Count; i++)
            {
                if (gridView.VisibleColumns[i].Caption == "Selection")
                {
                    gridView.VisibleColumns[i].Width = 40;
                    break;
                }
            }


            gridView.EndUpdate();
        }

        //public bool IsRowChecked(DataGridControl gridControl, int focusedRowHandle)
        //{
        //    GridColumn col = gridControl.GridView.Columns.ColumnByFieldName("SelectAllCheckEditField");
        //    bool isChecked = false;
        //    if (col != null) // 행선택 체크 박스 컬럼
        //    {
        //        foreach (int i in gridControl.GetCheckedRows())
        //        {
        //            if (i == focusedRowHandle) isChecked = true;
        //            break;
        //        }
        //    }
        //    return isChecked;
        //}

        public void ColumnMove(GridColumn preColumn, GridColumn movingColumn)
        {
            GridView grid = this.MainView as GridView;
            if (grid == null)
                return;
            int preIndex = preColumn == null ? 0 : preColumn.AbsoluteIndex + 1;
            grid.Columns.Remove(movingColumn);
            grid.Columns.Insert(preIndex, movingColumn);
            movingColumn.VisibleIndex = preColumn == null ? 0 : preColumn.VisibleIndex + 1;
        }
        //public DataGridControl(IContainer container)
        //{
        //    container.Add(this);

        //    InitializeComponent();
        //}
    }
}
