﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UC所属机构 : UserControl
    {
        public UC所属机构()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Custome")]
        public TreeListLookUpEdit Tree1
        {
            get { return tLkp1; }
            set { tLkp1 = value; }
        }

        [Category("Custome Size")]
        public Size Tree1Size
        {
            get { return this.tLkp1.Size; }
            set { this.tLkp1.Size = value; }
        }

        [Category("Custome")]
        public CheckEdit Chk1
        {
            get { return chk1; }
            set { chk1 = value; }
        }

        [Category("Custome Size")]
        public Size Chk1Size
        {
            get { return this.chk1.Size; }
            set { this.chk1.Size = value; }
        }
        #endregion

        #region Events
        private void UC所属机构_Load(object sender, EventArgs e)
        {

        }
        #endregion


    }
}
