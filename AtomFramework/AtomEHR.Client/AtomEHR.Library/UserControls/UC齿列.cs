﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.Library.UserControls
{
    public partial class UC齿列 : UserControl
    {
        public UC齿列()
        {
            InitializeComponent();
        }

        private void UC齿列_Load(object sender, EventArgs e)
        {
            this.chk正常.Checked = true;
            SetControlEditable(true);
        }

        private void chk正常_EditValueChanged(object sender, EventArgs e)
        {
            bool index = this.chk正常.Checked;
            SetControlEditable(index);
        }
        /// <summary>
        /// 设置控件的可用性
        /// </summary>
        /// <param name="index"></param>
        private void SetControlEditable(bool index)
        {
            this.chk缺齿.Checked = index ? !index : index;
            this.chk缺齿.Enabled = !index;
            this.txt缺齿1.Enabled = this.chk缺齿.Enabled && this.chk缺齿.Checked;
            this.txt缺齿2.Enabled = this.chk缺齿.Enabled && this.chk缺齿.Checked;
            this.txt缺齿3.Enabled = this.chk缺齿.Enabled && this.chk缺齿.Checked;
            this.txt缺齿4.Enabled = this.chk缺齿.Enabled && this.chk缺齿.Checked;

            this.chk龋齿.Checked = index ? !index : index;
            this.chk龋齿.Enabled = !index;
            this.txt龋齿1.Enabled = this.chk龋齿.Enabled && this.chk龋齿.Checked;
            this.txt龋齿2.Enabled = this.chk龋齿.Enabled && this.chk龋齿.Checked;
            this.txt龋齿3.Enabled = this.chk龋齿.Enabled && this.chk龋齿.Checked;
            this.txt龋齿4.Enabled = this.chk龋齿.Enabled && this.chk龋齿.Checked;

            this.chk义齿.Checked = index ? !index : index;
            this.chk义齿.Enabled = !index;
            this.txt义齿1.Enabled = this.chk义齿.Enabled && this.chk义齿.Checked;
            this.txt义齿2.Enabled = this.chk义齿.Enabled && this.chk义齿.Checked;
            this.txt义齿3.Enabled = this.chk义齿.Enabled && this.chk义齿.Checked;
            this.txt义齿4.Enabled = this.chk义齿.Enabled && this.chk义齿.Checked;

            this.chk其他.Checked = index ? !index : index;
            this.chk其他.Enabled = !index;
            this.txt其他.Enabled = this.chk其他.Enabled && this.chk其他.Checked;
        }

        private void chk缺齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk缺齿.Checked;
            this.txt缺齿1.Enabled = index;
            this.txt缺齿2.Enabled = index;
            this.txt缺齿3.Enabled = index;
            this.txt缺齿4.Enabled = index;

            //this.txt缺齿1.Text = index ? "" : this.txt缺齿1.Text;
            //this.txt缺齿2.Text = index ? "" : this.txt缺齿2.Text;
            //this.txt缺齿3.Text = index ? "" : this.txt缺齿3.Text;
            //this.txt缺齿4.Text = index ? "" : this.txt缺齿4.Text;

        }

        private void chk龋齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk龋齿.Checked;
            this.txt龋齿1.Enabled = index;
            this.txt龋齿2.Enabled = index;
            this.txt龋齿3.Enabled = index;
            this.txt龋齿4.Enabled = index;

            //this.txt龋齿1.Text = index ? "" : this.txt龋齿1.Text;
            //this.txt龋齿2.Text = index ? "" : this.txt龋齿2.Text;
            //this.txt龋齿3.Text = index ? "" : this.txt龋齿3.Text;
            //this.txt龋齿4.Text = index ? "" : this.txt龋齿4.Text;

        }

        private void chk义齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk义齿.Checked;
            this.txt义齿1.Enabled = index;
            this.txt义齿2.Enabled = index;
            this.txt义齿3.Enabled = index;
            this.txt义齿4.Enabled = index;

            //this.txt义齿1.Text = index ? "" : this.txt义齿1.Text;
            //this.txt义齿2.Text = index ? "" : this.txt义齿2.Text;
            //this.txt义齿3.Text = index ? "" : this.txt义齿3.Text;
            //this.txt义齿4.Text = index ? "" : this.txt义齿4.Text;

        }

        private void chk其他_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk其他.Checked;
            //this.txt其他.Text = index ? "" : this.txt其他.Text;
            this.txt其他.Enabled = index;
        }


        public Dictionary<string, string> GetResult()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string 齿列 = string.Empty;
            if (chk正常.Checked) 齿列 = "1";
            if (chk缺齿.Checked) 齿列 += ",2";
            if (chk龋齿.Checked) 齿列 += ",3";
            if (chk义齿.Checked) 齿列 += ",4";
            if (chk其他.Checked) 齿列 += ",5";
            result.Add("齿列", 齿列);

            bool index = this.chk正常.Checked;
            bool _index;
            _index = this.chk缺齿.Checked;
            result.Add("缺齿", this.txt缺齿1.Text.Trim() + "@" + this.txt缺齿2.Text.Trim() + "@" + this.txt缺齿3.Text.Trim() + "@" + this.txt缺齿4.Text.Trim());
            result.Add("龋齿", this.txt龋齿1.Text.Trim() + "@" + this.txt龋齿2.Text.Trim() + "@" + this.txt龋齿3.Text.Trim() + "@" + this.txt龋齿4.Text.Trim());
            result.Add("义齿", this.txt义齿1.Text.Trim() + "@" + this.txt义齿2.Text.Trim() + "@" + this.txt义齿3.Text.Trim() + "@" + this.txt义齿4.Text.Trim());
            result.Add("其他", this.txt其他.Text.Trim());
            return result;
        }

        private void chk缺齿_EnabledChanged(object sender, EventArgs e)
        {
            bool index = this.chk缺齿.Checked;
            this.txt缺齿1.Enabled = index;
            this.txt缺齿2.Enabled = index;
            this.txt缺齿3.Enabled = index;
            this.txt缺齿4.Enabled = index;

            //this.txt缺齿1.Text = index ? "" : this.txt缺齿1.Text;
            //this.txt缺齿2.Text = index ? "" : this.txt缺齿2.Text;
            //this.txt缺齿3.Text = index ? "" : this.txt缺齿3.Text;
            //this.txt缺齿4.Text = index ? "" : this.txt缺齿4.Text;
        }

        private void chk正常_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
