﻿namespace AtomEHR.Library.UserControls
{
    partial class UC职业病危害接触史
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radion总 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学 = new DevExpress.XtraEditors.TextEdit();
            this.txt放射2 = new DevExpress.XtraEditors.TextEdit();
            this.radio放射 = new DevExpress.XtraEditors.RadioGroup();
            this.txt放射 = new DevExpress.XtraEditors.TextEdit();
            this.txt粉尘2 = new DevExpress.XtraEditors.TextEdit();
            this.txt其他2 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学2 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理2 = new DevExpress.XtraEditors.TextEdit();
            this.radio其他 = new DevExpress.XtraEditors.RadioGroup();
            this.radio化学 = new DevExpress.XtraEditors.RadioGroup();
            this.radio粉尘 = new DevExpress.XtraEditors.RadioGroup();
            this.txt粉尘 = new DevExpress.XtraEditors.TextEdit();
            this.radio物理 = new DevExpress.XtraEditors.RadioGroup();
            this.txt物理 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt工种 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt从业时间 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.radion总.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // radion总
            // 
            this.radion总.EditValue = "1";
            this.radion总.Location = new System.Drawing.Point(3, 0);
            this.radion总.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radion总.Name = "radion总";
            this.radion总.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radion总.Size = new System.Drawing.Size(87, 20);
            this.radion总.TabIndex = 0;
            this.radion总.SelectedIndexChanged += new System.EventHandler(this.radion总_SelectedIndexChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt其他);
            this.layoutControl1.Controls.Add(this.txt化学);
            this.layoutControl1.Controls.Add(this.txt放射2);
            this.layoutControl1.Controls.Add(this.radio放射);
            this.layoutControl1.Controls.Add(this.txt放射);
            this.layoutControl1.Controls.Add(this.txt粉尘2);
            this.layoutControl1.Controls.Add(this.txt其他2);
            this.layoutControl1.Controls.Add(this.txt化学2);
            this.layoutControl1.Controls.Add(this.txt物理2);
            this.layoutControl1.Controls.Add(this.radio其他);
            this.layoutControl1.Controls.Add(this.radio化学);
            this.layoutControl1.Controls.Add(this.radio粉尘);
            this.layoutControl1.Controls.Add(this.txt粉尘);
            this.layoutControl1.Controls.Add(this.radio物理);
            this.layoutControl1.Controls.Add(this.txt物理);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(552, 184);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt其他
            // 
            this.txt其他.Location = new System.Drawing.Point(60, 154);
            this.txt其他.Name = "txt其他";
            this.txt其他.Size = new System.Drawing.Size(115, 20);
            this.txt其他.StyleController = this.layoutControl1;
            this.txt其他.TabIndex = 13;
            // 
            // txt化学
            // 
            this.txt化学.Location = new System.Drawing.Point(60, 129);
            this.txt化学.Name = "txt化学";
            this.txt化学.Size = new System.Drawing.Size(115, 20);
            this.txt化学.StyleController = this.layoutControl1;
            this.txt化学.TabIndex = 12;
            // 
            // txt放射2
            // 
            this.txt放射2.Location = new System.Drawing.Point(314, 79);
            this.txt放射2.Name = "txt放射2";
            this.txt放射2.Size = new System.Drawing.Size(229, 20);
            this.txt放射2.StyleController = this.layoutControl1;
            this.txt放射2.TabIndex = 8;
            // 
            // radio放射
            // 
            this.radio放射.EditValue = "1";
            this.radio放射.Location = new System.Drawing.Point(232, 79);
            this.radio放射.Name = "radio放射";
            this.radio放射.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio放射.Size = new System.Drawing.Size(78, 21);
            this.radio放射.StyleController = this.layoutControl1;
            this.radio放射.TabIndex = 7;
            this.radio放射.SelectedIndexChanged += new System.EventHandler(this.radio_SelectedIndexChanged);
            // 
            // txt放射
            // 
            this.txt放射.Location = new System.Drawing.Point(60, 79);
            this.txt放射.Name = "txt放射";
            this.txt放射.Size = new System.Drawing.Size(115, 20);
            this.txt放射.StyleController = this.layoutControl1;
            this.txt放射.TabIndex = 8;
            // 
            // txt粉尘2
            // 
            this.txt粉尘2.Location = new System.Drawing.Point(314, 54);
            this.txt粉尘2.Name = "txt粉尘2";
            this.txt粉尘2.Size = new System.Drawing.Size(229, 20);
            this.txt粉尘2.StyleController = this.layoutControl1;
            this.txt粉尘2.TabIndex = 7;
            // 
            // txt其他2
            // 
            this.txt其他2.Location = new System.Drawing.Point(314, 154);
            this.txt其他2.Name = "txt其他2";
            this.txt其他2.Size = new System.Drawing.Size(229, 20);
            this.txt其他2.StyleController = this.layoutControl1;
            this.txt其他2.TabIndex = 11;
            // 
            // txt化学2
            // 
            this.txt化学2.Location = new System.Drawing.Point(314, 129);
            this.txt化学2.Name = "txt化学2";
            this.txt化学2.Size = new System.Drawing.Size(229, 20);
            this.txt化学2.StyleController = this.layoutControl1;
            this.txt化学2.TabIndex = 10;
            // 
            // txt物理2
            // 
            this.txt物理2.Location = new System.Drawing.Point(314, 104);
            this.txt物理2.Name = "txt物理2";
            this.txt物理2.Size = new System.Drawing.Size(229, 20);
            this.txt物理2.StyleController = this.layoutControl1;
            this.txt物理2.TabIndex = 9;
            // 
            // radio其他
            // 
            this.radio其他.EditValue = "1";
            this.radio其他.Location = new System.Drawing.Point(232, 154);
            this.radio其他.Name = "radio其他";
            this.radio其他.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio其他.Size = new System.Drawing.Size(78, 21);
            this.radio其他.StyleController = this.layoutControl1;
            this.radio其他.TabIndex = 15;
            this.radio其他.SelectedIndexChanged += new System.EventHandler(this.radio_SelectedIndexChanged);
            // 
            // radio化学
            // 
            this.radio化学.EditValue = "1";
            this.radio化学.Location = new System.Drawing.Point(232, 129);
            this.radio化学.Name = "radio化学";
            this.radio化学.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio化学.Size = new System.Drawing.Size(78, 21);
            this.radio化学.StyleController = this.layoutControl1;
            this.radio化学.TabIndex = 14;
            this.radio化学.SelectedIndexChanged += new System.EventHandler(this.radio_SelectedIndexChanged);
            // 
            // radio粉尘
            // 
            this.radio粉尘.EditValue = "1";
            this.radio粉尘.Location = new System.Drawing.Point(232, 54);
            this.radio粉尘.Name = "radio粉尘";
            this.radio粉尘.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio粉尘.Size = new System.Drawing.Size(78, 21);
            this.radio粉尘.StyleController = this.layoutControl1;
            this.radio粉尘.TabIndex = 6;
            this.radio粉尘.SelectedIndexChanged += new System.EventHandler(this.radio_SelectedIndexChanged);
            // 
            // txt粉尘
            // 
            this.txt粉尘.Location = new System.Drawing.Point(60, 54);
            this.txt粉尘.Name = "txt粉尘";
            this.txt粉尘.Size = new System.Drawing.Size(115, 20);
            this.txt粉尘.StyleController = this.layoutControl1;
            this.txt粉尘.TabIndex = 5;
            // 
            // radio物理
            // 
            this.radio物理.EditValue = "1";
            this.radio物理.Location = new System.Drawing.Point(232, 104);
            this.radio物理.Name = "radio物理";
            this.radio物理.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio物理.Size = new System.Drawing.Size(78, 21);
            this.radio物理.StyleController = this.layoutControl1;
            this.radio物理.TabIndex = 10;
            this.radio物理.SelectedIndexChanged += new System.EventHandler(this.radio_SelectedIndexChanged);
            // 
            // txt物理
            // 
            this.txt物理.Location = new System.Drawing.Point(60, 104);
            this.txt物理.Name = "txt物理";
            this.txt物理.Size = new System.Drawing.Size(115, 20);
            this.txt物理.StyleController = this.layoutControl1;
            this.txt物理.TabIndex = 11;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.radion总);
            this.flowLayoutPanel1.Controls.Add(this.labelControl1);
            this.flowLayoutPanel1.Controls.Add(this.txt工种);
            this.flowLayoutPanel1.Controls.Add(this.labelControl2);
            this.flowLayoutPanel1.Controls.Add(this.txt从业时间);
            this.flowLayoutPanel1.Controls.Add(this.labelControl3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(544, 21);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(96, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "（工种";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // txt工种
            // 
            this.txt工种.Location = new System.Drawing.Point(138, 0);
            this.txt工种.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt工种.Name = "txt工种";
            this.txt工种.Size = new System.Drawing.Size(100, 20);
            this.txt工种.TabIndex = 2;
            this.txt工种.EditValueChanged += new System.EventHandler(this.txt工种_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(244, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "从业时间";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // txt从业时间
            // 
            this.txt从业时间.Location = new System.Drawing.Point(298, 0);
            this.txt从业时间.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt从业时间.Name = "txt从业时间";
            this.txt从业时间.Size = new System.Drawing.Size(100, 20);
            this.txt从业时间.TabIndex = 4;
            this.txt从业时间.EditValueChanged += new System.EventHandler(this.txt从业时间_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(404, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 14);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "年）";
            this.labelControl3.Click += new System.EventHandler(this.labelControl3_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(552, 184);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.flowLayoutPanel1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(548, 25);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "毒物种类";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem11,
            this.layoutControlItem14,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem20,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem19,
            this.layoutControlItem22});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 25);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(548, 155);
            this.layoutControlGroup2.Text = "毒物种类";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt粉尘;
            this.layoutControlItem2.CustomizationFormText = "粉尘";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(170, 25);
            this.layoutControlItem2.Text = "粉尘";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt放射;
            this.layoutControlItem11.CustomizationFormText = "放射物质";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(170, 25);
            this.layoutControlItem11.Text = "放射物质";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt物理;
            this.layoutControlItem14.CustomizationFormText = "物理因素";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(170, 25);
            this.layoutControlItem14.Text = "物理因素";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt化学;
            this.layoutControlItem16.CustomizationFormText = "化学物质";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 75);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(170, 25);
            this.layoutControlItem16.Text = "化学物质";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt其他;
            this.layoutControlItem17.CustomizationFormText = "其他";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 100);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(170, 25);
            this.layoutControlItem17.Text = "其他";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.radio粉尘;
            this.layoutControlItem3.CustomizationFormText = "防护措施";
            this.layoutControlItem3.Location = new System.Drawing.Point(170, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(107, 25);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(135, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "防护措施";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt粉尘2;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(305, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.radio放射;
            this.layoutControlItem12.CustomizationFormText = "防护措施";
            this.layoutControlItem12.Location = new System.Drawing.Point(170, 25);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(107, 25);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(135, 25);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "防护措施";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt放射2;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(305, 25);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.radio物理;
            this.layoutControlItem15.CustomizationFormText = "防护措施";
            this.layoutControlItem15.Location = new System.Drawing.Point(170, 50);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(107, 25);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(135, 25);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "防护措施";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txt物理2;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(305, 50);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.radio化学;
            this.layoutControlItem18.CustomizationFormText = "防护措施";
            this.layoutControlItem18.Location = new System.Drawing.Point(170, 75);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(106, 25);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(135, 25);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "防护措施";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txt化学2;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(305, 75);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.radio其他;
            this.layoutControlItem19.CustomizationFormText = "防护措施";
            this.layoutControlItem19.Location = new System.Drawing.Point(170, 100);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(109, 25);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(135, 25);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "防护措施";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txt其他2;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(305, 100);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt粉尘;
            this.layoutControlItem5.CustomizationFormText = "粉尘";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 254);
            this.layoutControlItem5.Name = "layoutControlItem2";
            this.layoutControlItem5.Size = new System.Drawing.Size(262, 29);
            this.layoutControlItem5.Text = "粉尘";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.radio粉尘;
            this.layoutControlItem6.CustomizationFormText = "防护措施";
            this.layoutControlItem6.Location = new System.Drawing.Point(262, 254);
            this.layoutControlItem6.Name = "layoutControlItem3";
            this.layoutControlItem6.Size = new System.Drawing.Size(131, 29);
            this.layoutControlItem6.Text = "防护措施";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt粉尘2;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem7.Location = new System.Drawing.Point(393, 254);
            this.layoutControlItem7.Name = "layoutControlItem4";
            this.layoutControlItem7.Size = new System.Drawing.Size(131, 29);
            this.layoutControlItem7.Text = "layoutControlItem4";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt粉尘;
            this.layoutControlItem8.CustomizationFormText = "粉尘";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 254);
            this.layoutControlItem8.Name = "layoutControlItem2";
            this.layoutControlItem8.Size = new System.Drawing.Size(262, 29);
            this.layoutControlItem8.Text = "粉尘";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.radio粉尘;
            this.layoutControlItem9.CustomizationFormText = "防护措施";
            this.layoutControlItem9.Location = new System.Drawing.Point(262, 254);
            this.layoutControlItem9.Name = "layoutControlItem3";
            this.layoutControlItem9.Size = new System.Drawing.Size(131, 29);
            this.layoutControlItem9.Text = "防护措施";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt粉尘2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem10.Location = new System.Drawing.Point(393, 254);
            this.layoutControlItem10.Name = "layoutControlItem4";
            this.layoutControlItem10.Size = new System.Drawing.Size(131, 29);
            this.layoutControlItem10.Text = "layoutControlItem4";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // UC职业病危害接触史
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UC职业病危害接触史";
            this.Size = new System.Drawing.Size(552, 184);
            this.Load += new System.EventHandler(this.UC职业病危害接触史_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radion总.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.RadioGroup radion总;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt工种;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt从业时间;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.RadioGroup radio粉尘;
        private DevExpress.XtraEditors.TextEdit txt粉尘;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txt粉尘2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txt其他;
        private DevExpress.XtraEditors.TextEdit txt化学;
        private DevExpress.XtraEditors.TextEdit txt放射2;
        private DevExpress.XtraEditors.RadioGroup radio放射;
        private DevExpress.XtraEditors.TextEdit txt放射;
        private DevExpress.XtraEditors.RadioGroup radio物理;
        private DevExpress.XtraEditors.TextEdit txt物理;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.RadioGroup radio其他;
        private DevExpress.XtraEditors.RadioGroup radio化学;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.TextEdit txt其他2;
        private DevExpress.XtraEditors.TextEdit txt化学2;
        private DevExpress.XtraEditors.TextEdit txt物理2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
    }
}
