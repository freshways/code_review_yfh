﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UC职业病危害接触史 : UserControl
    {
        private string editValue;

        public string EditValue
        {
            get { return editValue; }
            set { this.radion总.EditValue = value; }
        }

        public UC职业病危害接触史()
        {
            InitializeComponent();
        }

        private void UC职业病危害接触史_Load(object sender, EventArgs e)
        {
            this.radion总.EditValue = false;
        }

        private void radion总_SelectedIndexChanged(object sender, EventArgs e)
        {
            Boolean index = this.radion总.EditValue == null ? false : Convert.ToBoolean(this.radion总.EditValue);
            SetControlEditable(index);
        }

        private void SetControlEditable(bool p)
        {
            this.txt工种.Enabled = p;
            this.txt从业时间.Enabled = p;

            this.txt粉尘.Enabled = p;
            this.radio粉尘.Enabled = p;
            //this.radio粉尘.EditValue = p ? "1" : "0";
            this.txt粉尘2.Enabled = p;

            this.txt放射.Enabled = p;
            this.radio放射.Enabled = p;
            //this.radio放射.EditValue = p ? "1" : "0";
            this.txt放射2.Enabled = p;

            this.txt物理.Enabled = p;
            this.radio物理.Enabled = p;
            //this.radio物理.EditValue = p ? "1" : "0";
            this.txt物理2.Enabled = p;

            this.txt化学.Enabled = p;
            this.radio化学.Enabled = p;
            //this.radio化学.EditValue = p ? "1" : "0";
            this.txt化学2.Enabled = p;

            this.txt其他.Enabled = p;
            this.radio其他.Enabled = p;
            //this.radio其他.EditValue = p ? "1" : "0";
            this.txt其他2.Enabled = p;


        }

        private void radio_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioGroup radio = (RadioGroup)sender;
            if (radio == null) return;
            bool index = radio.EditValue == null ? false : Convert.ToBoolean(radio.EditValue);

            string radioName = radio.Name;
            string targetName = string.Empty;
            if (!string.IsNullOrEmpty(radioName))
            {
                targetName = "txt" + radio.Name.Substring(5) + "2";
            }
            Control[] control = this.Controls.Find(targetName, true);
            if (control.Length == 1)
            {
                control[0].Enabled = index;
            }
        }
        /// <summary>
        /// 获取结果集，返回一个字典
        /// </summary>
        /// <returns>字典</returns>
        public Dictionary<string, string> GetResult()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string index = this.radion总.EditValue.ToString();
            result.Add("有无职业病", index);
            if (index == "3")//有
            {
                result.Add("具体职业", this.txt工种.Text.Trim());
                result.Add("从业时间", this.txt从业时间.Text.Trim());

                string 粉尘防护措施有无 = this.radio粉尘.EditValue.ToString();
                result.Add("粉尘", this.txt粉尘.Text.Trim());
                result.Add("粉尘防护措施有无", 粉尘防护措施有无);
                result.Add("粉尘防护措施", 粉尘防护措施有无 == "3" ? this.txt粉尘2.Text.Trim() : "");

                string 放射物质防护措施有无 = this.radio放射.EditValue.ToString();
                result.Add("放射物质", this.txt放射.Text.Trim());
                result.Add("放射物质防护措施有无", 放射物质防护措施有无);
                result.Add("放射物质防护措施", 放射物质防护措施有无 == "3" ? this.txt放射2.Text.Trim() : "");

                string 物理因素防护措施有无 = this.radio物理.EditValue.ToString();
                result.Add("物理因素", this.txt物理.Text.Trim());
                result.Add("物理因素防护措施有无", 物理因素防护措施有无);
                result.Add("物理因素防护措施", 物理因素防护措施有无 == "3" ? this.txt物理2.Text.Trim() : "");

                string 化学物质防护措施有无 = this.radio化学.EditValue.ToString();
                result.Add("化学物质", this.txt化学.Text.Trim());
                result.Add("化学物质防护措施有无", 化学物质防护措施有无);
                result.Add("化学物质防护措施", 化学物质防护措施有无 == "3" ? this.txt化学2.Text.Trim() : "");

                string 其他防护措施有无 = this.radio其他.EditValue.ToString();
                result.Add("其他", this.txt其他.Text.Trim());
                result.Add("其他防护措施有无", 其他防护措施有无);
                result.Add("其他防护措施", 其他防护措施有无 == "3" ? this.txt其他2.Text.Trim() : "");

                //_index = this.radio放射.EditValue == null ? false : Convert.ToBoolean(this.radio放射.EditValue);
                //result.Add("放射物质", index ? this.txt放射.Text.Trim() : "");
                //result.Add("是否放射物质防护措施", index && _index);
                //result.Add("放射物质防护措施", index && _index ? this.txt放射2.Text.Trim() : "");

                //_index = this.radio物理.EditValue == null ? false : Convert.ToBoolean(this.radio物理.EditValue);
                //result.Add("物理", index ? this.txt物理.Text.Trim() : "");
                //result.Add("是否物理防护措施", index && _index);
                //result.Add("粉尘物理措施", index && _index ? this.txt物理2.Text.Trim() : "");

                //_index = this.radio化学.EditValue == null ? false : Convert.ToBoolean(this.radio化学.EditValue);
                //result.Add("化学", index ? this.txt化学.Text.Trim() : "");
                //result.Add("是否化学防护措施", index && _index);
                //result.Add("化学防护措施", index && _index ? this.txt化学2.Text.Trim() : "");

                //_index = this.radio其他.EditValue == null ? false : Convert.ToBoolean(this.radio其他.EditValue);
                //result.Add("其他", index ? this.txt其他.Text.Trim() : "");
                //result.Add("是否其他防护措施", index && _index);
                //result.Add("其他防护措施", index && _index ? this.txt其他2.Text.Trim() : "");
            }


            return result;
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void txt工种_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }

        private void txt从业时间_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl3_Click(object sender, EventArgs e)
        {

        }
    }
}
