﻿namespace AtomEHR.Library.UserControls
{
    partial class UC齿列
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chk义齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk缺齿 = new DevExpress.XtraEditors.CheckEdit();
            this.txt缺齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿4 = new DevExpress.XtraEditors.TextEdit();
            this.chk龋齿 = new DevExpress.XtraEditors.CheckEdit();
            this.txt龋齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt其他 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.chk正常 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk义齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk缺齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk龋齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.56885F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.54839F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.43011F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.39785F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.62302F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.93905F));
            this.tableLayoutPanel1.Controls.Add(this.chk其他, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.chk义齿, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.chk缺齿, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿4, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.chk龋齿, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿3, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿2, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿4, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt其他, 4, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 25);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.86207F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.27586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.13793F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.86207F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(410, 117);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chk其他
            // 
            this.chk其他.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk其他.Location = new System.Drawing.Point(207, 58);
            this.chk其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.chk其他.Properties.Appearance.Options.UseBackColor = true;
            this.chk其他.Properties.Appearance.Options.UseTextOptions = true;
            this.chk其他.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk其他.Properties.AutoHeight = false;
            this.chk其他.Properties.Caption = "其他";
            this.tableLayoutPanel1.SetRowSpan(this.chk其他, 2);
            this.chk其他.Size = new System.Drawing.Size(66, 58);
            this.chk其他.TabIndex = 15;
            this.chk其他.Tag = "5";
            this.chk其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // chk义齿
            // 
            this.chk义齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk义齿.Location = new System.Drawing.Point(1, 58);
            this.chk义齿.Margin = new System.Windows.Forms.Padding(0);
            this.chk义齿.Name = "chk义齿";
            this.chk义齿.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.chk义齿.Properties.Appearance.Options.UseBackColor = true;
            this.chk义齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk义齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk义齿.Properties.AutoHeight = false;
            this.chk义齿.Properties.Caption = "义齿(假牙)";
            this.tableLayoutPanel1.SetRowSpan(this.chk义齿, 2);
            this.chk义齿.Size = new System.Drawing.Size(46, 58);
            this.chk义齿.TabIndex = 10;
            this.chk义齿.Tag = "4";
            this.chk义齿.CheckedChanged += new System.EventHandler(this.chk义齿_CheckedChanged);
            // 
            // chk缺齿
            // 
            this.chk缺齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk缺齿.Location = new System.Drawing.Point(1, 1);
            this.chk缺齿.Margin = new System.Windows.Forms.Padding(0);
            this.chk缺齿.Name = "chk缺齿";
            this.chk缺齿.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.chk缺齿.Properties.Appearance.Options.UseBackColor = true;
            this.chk缺齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk缺齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk缺齿.Properties.AutoHeight = false;
            this.chk缺齿.Properties.Caption = "缺齿";
            this.tableLayoutPanel1.SetRowSpan(this.chk缺齿, 2);
            this.chk缺齿.Size = new System.Drawing.Size(46, 56);
            this.chk缺齿.TabIndex = 0;
            this.chk缺齿.Tag = "2";
            this.chk缺齿.CheckedChanged += new System.EventHandler(this.chk缺齿_CheckedChanged);
            this.chk缺齿.EnabledChanged += new System.EventHandler(this.chk缺齿_EnabledChanged);
            // 
            // txt缺齿1
            // 
            this.txt缺齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿1.Location = new System.Drawing.Point(51, 4);
            this.txt缺齿1.Name = "txt缺齿1";
            this.txt缺齿1.Size = new System.Drawing.Size(69, 20);
            this.txt缺齿1.TabIndex = 1;
            // 
            // txt缺齿3
            // 
            this.txt缺齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿3.Location = new System.Drawing.Point(51, 34);
            this.txt缺齿3.Name = "txt缺齿3";
            this.txt缺齿3.Size = new System.Drawing.Size(69, 20);
            this.txt缺齿3.TabIndex = 2;
            // 
            // txt缺齿2
            // 
            this.txt缺齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿2.Location = new System.Drawing.Point(127, 4);
            this.txt缺齿2.Name = "txt缺齿2";
            this.txt缺齿2.Size = new System.Drawing.Size(76, 20);
            this.txt缺齿2.TabIndex = 3;
            // 
            // txt缺齿4
            // 
            this.txt缺齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿4.Location = new System.Drawing.Point(127, 34);
            this.txt缺齿4.Name = "txt缺齿4";
            this.txt缺齿4.Size = new System.Drawing.Size(76, 20);
            this.txt缺齿4.TabIndex = 4;
            // 
            // chk龋齿
            // 
            this.chk龋齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk龋齿.Location = new System.Drawing.Point(207, 1);
            this.chk龋齿.Margin = new System.Windows.Forms.Padding(0);
            this.chk龋齿.Name = "chk龋齿";
            this.chk龋齿.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.chk龋齿.Properties.Appearance.Options.UseBackColor = true;
            this.chk龋齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk龋齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk龋齿.Properties.AutoHeight = false;
            this.chk龋齿.Properties.Caption = "龋齿";
            this.tableLayoutPanel1.SetRowSpan(this.chk龋齿, 2);
            this.chk龋齿.Size = new System.Drawing.Size(66, 56);
            this.chk龋齿.TabIndex = 5;
            this.chk龋齿.Tag = "3";
            this.chk龋齿.CheckedChanged += new System.EventHandler(this.chk龋齿_CheckedChanged);
            // 
            // txt龋齿1
            // 
            this.txt龋齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿1.Location = new System.Drawing.Point(277, 4);
            this.txt龋齿1.Name = "txt龋齿1";
            this.txt龋齿1.Size = new System.Drawing.Size(69, 20);
            this.txt龋齿1.TabIndex = 6;
            // 
            // txt龋齿3
            // 
            this.txt龋齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿3.Location = new System.Drawing.Point(277, 34);
            this.txt龋齿3.Name = "txt龋齿3";
            this.txt龋齿3.Size = new System.Drawing.Size(69, 20);
            this.txt龋齿3.TabIndex = 7;
            // 
            // txt龋齿2
            // 
            this.txt龋齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿2.Location = new System.Drawing.Point(353, 4);
            this.txt龋齿2.Name = "txt龋齿2";
            this.txt龋齿2.Size = new System.Drawing.Size(53, 20);
            this.txt龋齿2.TabIndex = 8;
            // 
            // txt龋齿4
            // 
            this.txt龋齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿4.Location = new System.Drawing.Point(353, 34);
            this.txt龋齿4.Name = "txt龋齿4";
            this.txt龋齿4.Size = new System.Drawing.Size(53, 20);
            this.txt龋齿4.TabIndex = 9;
            // 
            // txt义齿1
            // 
            this.txt义齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿1.Location = new System.Drawing.Point(51, 61);
            this.txt义齿1.Name = "txt义齿1";
            this.txt义齿1.Size = new System.Drawing.Size(69, 20);
            this.txt义齿1.TabIndex = 11;
            // 
            // txt义齿2
            // 
            this.txt义齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿2.Location = new System.Drawing.Point(127, 61);
            this.txt义齿2.Name = "txt义齿2";
            this.txt义齿2.Size = new System.Drawing.Size(76, 20);
            this.txt义齿2.TabIndex = 13;
            // 
            // txt义齿3
            // 
            this.txt义齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿3.Location = new System.Drawing.Point(51, 89);
            this.txt义齿3.Name = "txt义齿3";
            this.txt义齿3.Size = new System.Drawing.Size(69, 20);
            this.txt义齿3.TabIndex = 16;
            // 
            // txt义齿4
            // 
            this.txt义齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿4.Location = new System.Drawing.Point(127, 89);
            this.txt义齿4.Name = "txt义齿4";
            this.txt义齿4.Size = new System.Drawing.Size(76, 20);
            this.txt义齿4.TabIndex = 17;
            // 
            // txt其他
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txt其他, 2);
            this.txt其他.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt其他.Location = new System.Drawing.Point(277, 81);
            this.txt其他.Margin = new System.Windows.Forms.Padding(3, 23, 3, 3);
            this.txt其他.Name = "txt其他";
            this.tableLayoutPanel1.SetRowSpan(this.txt其他, 2);
            this.txt其他.Size = new System.Drawing.Size(129, 20);
            this.txt其他.TabIndex = 18;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.chk正常);
            this.layoutControl2.Controls.Add(this.tableLayoutPanel1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(414, 144);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // chk正常
            // 
            this.chk正常.EditValue = true;
            this.chk正常.Location = new System.Drawing.Point(2, 2);
            this.chk正常.Name = "chk正常";
            this.chk正常.Properties.Caption = "正常";
            this.chk正常.Size = new System.Drawing.Size(410, 19);
            this.chk正常.StyleController = this.layoutControl2;
            this.chk正常.TabIndex = 4;
            this.chk正常.Tag = "1";
            this.chk正常.CheckedChanged += new System.EventHandler(this.chk正常_EditValueChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(414, 144);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chk正常;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(414, 23);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.tableLayoutPanel1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(104, 120);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(414, 121);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // UC齿列
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl2);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UC齿列";
            this.Size = new System.Drawing.Size(414, 144);
            this.Load += new System.EventHandler(this.UC齿列_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk义齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk缺齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk龋齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.CheckEdit chk缺齿;
        private DevExpress.XtraEditors.TextEdit txt缺齿1;
        private DevExpress.XtraEditors.TextEdit txt缺齿3;
        private DevExpress.XtraEditors.TextEdit txt缺齿2;
        private DevExpress.XtraEditors.TextEdit txt缺齿4;
        private DevExpress.XtraEditors.CheckEdit chk龋齿;
        private DevExpress.XtraEditors.CheckEdit chk义齿;
        private DevExpress.XtraEditors.TextEdit txt龋齿1;
        private DevExpress.XtraEditors.TextEdit txt龋齿3;
        private DevExpress.XtraEditors.TextEdit txt龋齿2;
        private DevExpress.XtraEditors.TextEdit txt龋齿4;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private DevExpress.XtraEditors.TextEdit txt义齿1;
        private DevExpress.XtraEditors.TextEdit txt义齿2;
        private DevExpress.XtraEditors.TextEdit txt义齿3;
        private DevExpress.XtraEditors.TextEdit txt义齿4;
        private DevExpress.XtraEditors.TextEdit txt其他;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.CheckEdit chk正常;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
