﻿namespace AtomEHR.Library.UserControls
{
    partial class UCCboLblCbo
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbo1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.cbo1);
            this.flowLayoutPanel1.Controls.Add(this.lbl1);
            this.flowLayoutPanel1.Controls.Add(this.cbo2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(224, 20);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // lbl1
            // 
            this.lbl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl1.Location = new System.Drawing.Point(103, 3);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(18, 14);
            this.lbl1.TabIndex = 1;
            this.lbl1.Text = "lbl1";
            // 
            // cbo1
            // 
            this.cbo1.Location = new System.Drawing.Point(0, 0);
            this.cbo1.Margin = new System.Windows.Forms.Padding(0);
            this.cbo1.Name = "cbo1";
            this.cbo1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo1.Size = new System.Drawing.Size(100, 20);
            this.cbo1.TabIndex = 0;
            // 
            // cbo2
            // 
            this.cbo2.Location = new System.Drawing.Point(124, 0);
            this.cbo2.Margin = new System.Windows.Forms.Padding(0);
            this.cbo2.Name = "cbo2";
            this.cbo2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo2.Size = new System.Drawing.Size(100, 20);
            this.cbo2.TabIndex = 2;
            // 
            // UCCboLblCbo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UCCboLblCbo";
            this.Size = new System.Drawing.Size(224, 20);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lbl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo2;
    }
}
