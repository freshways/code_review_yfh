﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.Library.UserControls
{
    public partial class UCTxtToTxt : UserControl
    {
        public UCTxtToTxt()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Custom")]
        public TextEdit Txt1
        {
            get { return this.txt1; }
            set
            {
                this.txt1 = value;
            }
        }
        [Category("Custom Size")]
        public Size Txt1Size
        {
            get { return this.txt1.Size; }
            set { this.txt1.Size = value; }
        }
        [Category("Custom")]
        public TextEdit Txt2
        {
            get { return this.txt2; }
            set
            {
                this.txt2 = value;
            }
        }

        [Category("Custom Size")]
        public Size Txt2Size
        {
            get { return this.txt2.Size; }
            set { this.txt2.Size = value; }
        }
        #endregion
    }
}
