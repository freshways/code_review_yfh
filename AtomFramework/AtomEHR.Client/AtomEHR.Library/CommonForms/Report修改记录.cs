﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business.BLL_Business;

namespace AtomEHR.Library
{
    public partial class Report修改记录 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report修改记录()
        {
            InitializeComponent();
        }

        public Report修改记录(DataTable dt)
        {
            InitializeComponent();

            Binding(dt);
        }


        //Begin WXF 2018-11-07 | 18:34
        //统合打印用 
        public Report修改记录(string str_recordNum, DateTime DateFrom, DateTime DateTo)
        {
            InitializeComponent();

            DataTable dt = bllBusinessLog.SearchLog("", "tb_健康档案", DateFrom, DateTo, str_recordNum).Tables[0];//Loginer.CurrentUser.Account
            Binding(dt);
        }

        private void Binding(DataTable dt)
        {
            this.DataSource = dt;
            xrTableCell时间.DataBindings.Add("Text", dt, "LogDate");
            xrTableCell复核方式.DataBindings.Add("Text", dt, "OPType");
            xrTableCell复核内容.DataBindings.Add("Text", dt, "EditContent");
            xrTableCell复核人.DataBindings.Add("Text", dt, "LogUser");
        }
        //End

    }
}
