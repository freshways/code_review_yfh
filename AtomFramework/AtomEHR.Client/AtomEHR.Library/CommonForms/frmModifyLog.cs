﻿///*************************************************************************/
///*
///* 文件名    ：frmModifyLog.cs                              
///* 程序说明  : 修改历史记录查询窗体
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Core.Log;
using AtomEHR.Library;
using AtomEHR.Business.BLL_Business;

namespace AtomEHR.Library
{
    /// <summary>
    /// 修改历史记录查询窗体
    /// </summary>
    public partial class frmModifyLog : AtomEHR.Library.frmBaseChild
    {
        //最后一次搜索结果
        private DataSet _LastSearch = null;

        public frmModifyLog()
        {
            InitializeComponent();
        }

        //移动主表记录自动显示明细记录
        private void gvSummary_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if ((_LastSearch != null) && (e.FocusedRowHandle >= 0))
            {
                string GUID32 = ConvertEx.ToString(gvSummary.GetDataRow(e.FocusedRowHandle)["GUID32"]);
                DataView detail = _LastSearch.Tables[1].DefaultView;
                detail.RowFilter = "GUID32='" + GUID32 + "'";
                gcDetail.DataSource = detail;
            }
        }
        string KeyValue = string.Empty;
        //执行查询
        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (cbBusiness.ItemIndex >= 0)
            {
                DataRowView row = (DataRowView)cbBusiness.Properties.GetDataSourceRowByKeyValue(cbBusiness.EditValue);

                string tableName = ConvertEx.ToString(row.Row["Table1"]);
                _LastSearch = bllBusinessLog.SearchLog("", tableName, txtDateFrom.DateTime, txtDateTo.DateTime, KeyValue);//Loginer.CurrentUser.Account
                gcSummary.DataSource = _LastSearch.Tables[0];
                gvSummary_FocusedRowChanged(gvSummary, new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(-1, gvSummary.FocusedRowHandle));
            }
            else
            {
                Msg.Warning("请选择单据!");
                cbBusiness.Focus();
                cbBusiness.ShowPopup();
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            txtDateFrom.EditValue = null;
            txtDateTo.EditValue = null;

            cbBusiness.Focus();
            cbBusiness.ShowPopup();
        }

        private void frmModifyLog_Load(object sender, EventArgs e)
        {
            this.InitButtons();

            //txtDateFrom.EditValue = DateTime.Now.ToString("yyyy-MM-01");
            txtDateTo.DateTime = DateTime.Today;

            DataBinder.BindingLookupEditDataSource(cbBusiness, DataDictCache.Cache.BusinessTables, "FormCaption", "FormName");
            //cbBusiness.ItemIndex = 0;
            //cbBusiness.EditValue = "tb_健康档案";
        }

        public void ShowModifyLog(string formName,string keyvalue)
        {
            cbBusiness.Text = formName;
            KeyValue = keyvalue;
            btnQuery.PerformClick();
        }

        private void chkRowHeight_CheckedChanged(object sender, EventArgs e)
        {
            gvSummary.OptionsView.RowAutoHeight = chkRowHeight.Checked;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataTable dtprint = gcSummary.DataSource as DataTable;
            if (dtprint == null || dtprint.Rows.Count <= 0) return;
            Report修改记录 report = new Report修改记录(dtprint);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(report);
            tool.ShowPreviewDialog();
        }
    }
}
