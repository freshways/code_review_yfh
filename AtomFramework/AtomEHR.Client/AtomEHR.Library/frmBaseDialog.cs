﻿///*************************************************************************/
///*
///* 文件名    ：frmBaseDialog.cs                              
///* 程序说明  : 对话框窗体基类
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.Library
{
    /// <summary>
    ///  对话框窗体基类. 
    /// </summary>
    public partial class frmBaseDialog : frmBase
    {
        public frmBaseDialog()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //
        }
    }
}
