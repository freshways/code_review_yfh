﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using AtomEHR.WebServiceReference.WCF_CommonService;
using AtomEHR.WebServiceReference.WCF_DataDictService;
using AtomEHR.WebServiceReference.WCF_SystemSecurityService;
using AtomEHR.Common;
using AtomEHR.WebServiceReference.WCF_AccountModuleService;
using AtomEHR.WebServiceReference.WS_QY;
using AtomEHR.WebServiceReference.WS_HIS;
namespace AtomEHR.WebServiceReference
{

    /// <summary>
    /// WCF服务客户端实例工厂
    /// </summary>
    public class SoapClientFactory
    {

        /// <summary>
        /// 创建公共服务的SOAP Client对象
        /// </summary>
        /// <returns></returns>
        public static CommonServiceClient CreateCommonServiceClient()
        {
            WSHttpBinding BINDING = new WSHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "WSHttpBinding_ICommonService");//从配置文件(app.config)读取配置。            
            string endpoint = SoapClientConfig.GetSoapRemoteAddress("WSHttpBinding_ICommonService");
            return new CommonServiceClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

        /// <summary>
        /// 创建数据字典的WCF客户端实例
        /// </summary>
        /// <returns></returns>
        public static DataDictServiceClient CreateDataDictClient()
        {
            WSHttpBinding BINDING = new WSHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "WSHttpBinding_IDataDictService");//从配置文件(app.config)读取配置。

            string endpoint = SoapClientConfig.GetSoapRemoteAddress("WSHttpBinding_IDataDictService");
            return new DataDictServiceClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

        /// <summary>
        /// 创建系统安全管理的WCF客户端实例
        /// </summary>
        /// <returns></returns>
        public static SystemSecurityServiceClient CreateSecurityClient()
        {
            WSHttpBinding BINDING = new WSHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "WSHttpBinding_ISystemSecurityService");//从配置文件(app.config)读取配置。

            string endpoint = SoapClientConfig.GetSoapRemoteAddress("WSHttpBinding_ISystemSecurityService");
            return new SystemSecurityServiceClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

        /// <summary>
        /// 创建财务模块的WCF客户端实例
        /// </summary>
        /// <returns></returns>
        public static AccountModuleServiceClient CreateAccountModuleServiceClient()
        {
            WSHttpBinding BINDING = new WSHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "WSHttpBinding_IAccountModuleService");//从配置文件(app.config)读取配置。

            string endpoint = SoapClientConfig.GetSoapRemoteAddress("WSHttpBinding_IAccountModuleService");
            return new AccountModuleServiceClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

        /// <summary>
        /// 创建外部接口客户端实例
        /// </summary>
        /// <returns></returns>
        public static JeecgWServiceIClient CreateQYYSServiceClient()
        {
            BasicHttpBinding BINDING = new BasicHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "JeecgWServiceImplServiceSoapBinding");//从配置文件(app.config)读取配置。

            string endpoint = SoapClientConfig.GetSoapRemoteAddress("JeecgWServiceImplPort");
            return new JeecgWServiceIClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

        /// <summary>
        /// 创建外部接口客户端实例
        /// </summary>
        /// <returns></returns>
        public static HISServiceClient CreateQYYSNameServiceClient()
        {
            BasicHttpBinding BINDING = new BasicHttpBinding();
            SoapClientConfig.ReadBindingConfig(BINDING, "BasicHttpBinding_IHISService");//从配置文件(app.config)读取配置。

            string endpoint = SoapClientConfig.GetSoapRemoteAddress("BasicHttpBinding_IHISService");
            return new HISServiceClient(BINDING, new EndpointAddress(endpoint));//构建WCF客户端实例
        }

    }
}
