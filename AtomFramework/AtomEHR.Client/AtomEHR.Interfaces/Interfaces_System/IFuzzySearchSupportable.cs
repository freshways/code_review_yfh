﻿///*************************************************************************/
///*
///* 文件名    ：IFuzzySearchSupportable.cs                                
///* 程序说明  : 支持模糊查询的接口,用于frmFuzzySearch窗体
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AtomEHR.Interfaces
{
    /// <summary>
    /// 支持模糊查询的接口,用于frmFuzzySearch窗体
    /// </summary>
    public interface IFuzzySearchSupportable
    {
        string FuzzySearchName { get; }
        DataTable FuzzySearch(string content);
    }
}
