///*************************************************************************/
///*
///* 文件名    ：IMsg.cs                                
///* 程序说明  : 消息显示接口
///* 原创作者  ：ATOM 
///* 
///* Copyright ?  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace AtomEHR.Core
{
    /// <summary>
    /// 消息显示接口
    /// </summary>
    public interface IMsg
    {
        /// <summary>
        /// 显示消息
        /// </summary>
        /// <param name="msg"></param>
        void UpdateMessage(string msg);
    }
}
