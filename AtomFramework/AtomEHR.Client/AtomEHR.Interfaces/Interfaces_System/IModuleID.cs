﻿///*************************************************************************/
///*
///* 文件名        : ModuleID.cs                                
///* 程序说明    : 模块编号和名称定义
///* 原创作者    : ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

namespace AtomEHR.Interfaces
{
    /// <summary>
    /// 模块编号.
    /// </summary>
    public enum ModuleID
    {
        //用于排序
        None = 0,
        DataDictionary = 1,
        SystemManage = 2,
        公共卫生 = 3,
        签约服务 = 4
    }

    /// <summary>
    /// 模块名称.
    /// </summary>
    public class ModuleNames
    {
        public const string DataDictionary = "数据字典";//
        public const string SystemManage = "系统管理";
        public const string 公共卫生 = "协同办公|健康档案|基本公共卫生|统计分析";
        public const string 签约服务 = "签约服务管理";
    }
}
