///*************************************************************************/
///*
///* 文件名    ：IObserver.cs                                
///* 程序说明  : 观察者接口
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace AtomEHR.Interfaces
{
    /// <summary>
    /// 观察者接口
    /// </summary>
    public interface IObserver
    {
        /// <summary>
        /// 发送通知
        /// </summary>
        void Notify();
    }

}
