﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AtomEHR.Interfaces.Interfaces_Bridge
{
    /// <summary>
    /// 共同数据层的桥接接口
    /// </summary>
    public interface IBridge_CommonData
    {
        DataTable GetSystemDataSet();
        DataTable GetTableFields(string tableName);
        DataTable GetBusinessTables();
        DataTable GetModules();
        DataTable GetTableFieldsDef(string tableName, bool onlyDisplayField);

        DataTable GetDB4Backup();

        DataTable GetBackupHistory(int topList);

        bool BackupDatabase(string DBNAME, string BKPATH);

        bool RestoreDatabase(string BKFILE, string DBNAME);

        string GetDataSN(string dataCode, bool asHeader);
        
        DataTable GetVersion();

        DataTable GetLoginPersonalInfo(string IDCard);//添加，和his融合，实现单点登陆

        string GetQYYS(string IDCrad);//yufh 2017年9月20日 08点48分 添加 ，获取签约医生

        string GetQYYSName(string IDCrad);//yufh 2017年9月20日 08点48分 添加 ，获取签约医生

        string SendWxMessage(string template_id, string idcard, string type, string key_first, string key_remark, string date, string[] key_keyword);//Yufh 2018年4月17日 添加 微信推送接口
    }
}
