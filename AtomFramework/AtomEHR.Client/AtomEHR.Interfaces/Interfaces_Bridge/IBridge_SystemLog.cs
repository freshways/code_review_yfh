﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AtomEHR.Interfaces.Interfaces_Bridge
{
    public interface IBridge_EditLogHistory
    {
        DataSet SearchLog(string logUser, string tableName, DateTime dateFrom, DateTime dateTo, string KeyValue);
        DataTable GetLogFieldDef(string tableName);
        string[] GetTracedFields(string tableName);
        bool SaveFieldDef(DataTable data);
        void WriteLog(string logID, DataTable originalData, DataTable changes, string tableName, string keyFieldName, bool isMaster);
        void WriteLog(string DocNo, string TableName, string KeyFieldName);
    }
}
