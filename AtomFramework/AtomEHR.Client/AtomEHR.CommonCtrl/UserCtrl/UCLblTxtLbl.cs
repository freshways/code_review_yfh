﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.CommonCtrl.UserCtrl
{
    public partial class UCLblTxtLbl : UserControl
    {
        public UCLblTxtLbl()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Customer")]
        public TextEdit Txt1
        {
            get { return this.txt1; }
            set { this.txt1 = value; }
        }

        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl2
        {
            get { return this.lbl2; }
            set { this.lbl2 = value; }
        }
        [Category("Custom Size")]
        public Size Txt1Size
        {
            get { return this.txt1.Size; }
            set { this.txt1.Size = value; }
        }

        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
        [Category("Customer")]
        public Size Lbl2Size
        {
            get { return this.lbl2.Size; }
            set { this.lbl2.Size = value; }
        }
        [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        [Category("Customer")]
        public string Lbl2Text
        {
            get { return this.lbl2.Text; }
            set
            {
                this.lbl2.Text = value;
            }
        }
        #endregion
    }
}
