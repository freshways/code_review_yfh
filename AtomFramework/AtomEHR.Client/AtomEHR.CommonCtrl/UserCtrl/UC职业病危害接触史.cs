﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.CommonCtrl.UserCtrl
{
    public partial class UC职业病危害接触史 : UserControl
    {
        public UC职业病危害接触史()
        {
            InitializeComponent();
        }

        private void UC职业病危害接触史_Load(object sender, EventArgs e)
        {
            this.radion总.EditValue = false;
        }

        private void radion总_SelectedIndexChanged(object sender, EventArgs e)
        {
            Boolean index = this.radion总.EditValue == null ? false : Convert.ToBoolean(this.radion总.EditValue);
            SetControlEditable(index);
        }

        private void SetControlEditable(bool p)
        {
            this.txt工种.Enabled = p;
            this.txt从业时间.Enabled = p;

            this.txt粉尘.Enabled = p;
            this.radio粉尘.Enabled = p;
            //this.radio粉尘.EditValue = p ? "1" : "0";
            this.txt粉尘2.Enabled = p;

            this.txt放射.Enabled = p;
            this.radio放射.Enabled = p;
            //this.radio放射.EditValue = p ? "1" : "0";
            this.txt放射2.Enabled = p;

            this.txt物理.Enabled = p;
            this.radio物理.Enabled = p;
            //this.radio物理.EditValue = p ? "1" : "0";
            this.txt物理2.Enabled = p;

            this.txt化学.Enabled = p;
            this.radio化学.Enabled = p;
            //this.radio化学.EditValue = p ? "1" : "0";
            this.txt化学2.Enabled = p;

            this.txt其他.Enabled = p;
            this.radio其他.Enabled = p;
            //this.radio其他.EditValue = p ? "1" : "0";
            this.txt其他2.Enabled = p;


        }

        private void radio_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioGroup radio = (RadioGroup)sender;
            if (radio == null) return;
            bool index = radio.EditValue == null ? false : Convert.ToBoolean(radio.EditValue);

            string radioName = radio.Name;
            string targetName = string.Empty;
            if (!string.IsNullOrEmpty(radioName))
            {
                targetName = "txt" + radio.Name.Substring(5) + "2";
            }
            Control[] control = this.Controls.Find(targetName, true);
            if (control.Length == 1)
            {
                control[0].Enabled = index;
            }
        }
        /// <summary>
        /// 获取结果集，返回一个字典
        /// </summary>
        /// <returns>字典</returns>
        public Dictionary<string, object> GetResult()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            bool index = this.radion总.EditValue == null ? false : Convert.ToBoolean(this.radion总.EditValue);
            bool _index = false;
            result.Add("是否存在职业病危害因素接触史", index);

            result.Add("工种", index ? this.txt工种.Text.Trim() : "");
            result.Add("从业时间", index ? this.txt从业时间.Text.Trim() : "");

            _index = this.radio粉尘.EditValue == null ? false : Convert.ToBoolean(this.radio粉尘.EditValue);
            result.Add("粉尘", index ? this.txt粉尘.Text.Trim() : "");
            result.Add("是否粉尘防护措施", index && _index);
            result.Add("粉尘防护措施", index && _index ? this.txt粉尘2.Text.Trim() : "");

            _index = this.radio放射.EditValue == null ? false : Convert.ToBoolean(this.radio放射.EditValue);
            result.Add("放射物质", index ? this.txt放射.Text.Trim() : "");
            result.Add("是否放射物质防护措施", index && _index);
            result.Add("放射物质防护措施", index && _index ? this.txt放射2.Text.Trim() : "");

            _index = this.radio物理.EditValue == null ? false : Convert.ToBoolean(this.radio物理.EditValue);
            result.Add("物理", index ? this.txt物理.Text.Trim() : "");
            result.Add("是否物理防护措施", index && _index);
            result.Add("粉尘物理措施", index && _index ? this.txt物理2.Text.Trim() : "");

            _index = this.radio化学.EditValue == null ? false : Convert.ToBoolean(this.radio化学.EditValue);
            result.Add("化学", index ? this.txt化学.Text.Trim() : "");
            result.Add("是否化学防护措施", index && _index);
            result.Add("化学防护措施", index && _index ? this.txt化学2.Text.Trim() : "");

            _index = this.radio其他.EditValue == null ? false : Convert.ToBoolean(this.radio其他.EditValue);
            result.Add("其他", index ? this.txt其他.Text.Trim() : "");
            result.Add("是否其他防护措施", index && _index);
            result.Add("其他防护措施", index && _index ? this.txt其他2.Text.Trim() : "");

            return result;
        }
    }
}
