﻿namespace AtomEHR.CommonCtrl.UserCtrl
{
    partial class UC所属机构
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tLkp1 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.chk1 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.tLkp1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tLkp1
            // 
            this.tLkp1.Location = new System.Drawing.Point(0, 0);
            this.tLkp1.Margin = new System.Windows.Forms.Padding(0);
            this.tLkp1.MaximumSize = new System.Drawing.Size(160, 20);
            this.tLkp1.MinimumSize = new System.Drawing.Size(160, 20);
            this.tLkp1.Name = "tLkp1";
            this.tLkp1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tLkp1.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.tLkp1.Size = new System.Drawing.Size(160, 20);
            this.tLkp1.TabIndex = 0;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // chk1
            // 
            this.chk1.EditValue = true;
            this.chk1.Location = new System.Drawing.Point(160, 0);
            this.chk1.Margin = new System.Windows.Forms.Padding(0);
            this.chk1.MaximumSize = new System.Drawing.Size(90, 19);
            this.chk1.MinimumSize = new System.Drawing.Size(90, 19);
            this.chk1.Name = "chk1";
            this.chk1.Properties.Caption = "含下属机构";
            this.chk1.Size = new System.Drawing.Size(90, 19);
            this.chk1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.tLkp1);
            this.flowLayoutPanel1.Controls.Add(this.chk1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(335, 22);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // UC所属机构
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UC所属机构";
            this.Size = new System.Drawing.Size(287, 22);
            this.Load += new System.EventHandler(this.UC所属机构_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tLkp1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TreeListLookUpEdit tLkp1;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.CheckEdit chk1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}
