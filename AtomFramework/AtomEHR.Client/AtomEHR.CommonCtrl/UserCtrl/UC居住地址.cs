﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace AtomEHR.CommonCtrl.UserCtrl
{
    public partial class UC居住地址 : UserControl
    {
        public UC居住地址()
        {
            InitializeComponent();
        }

        #region memeber(s) && property(s)
        [Category("Custom")]
        public ComboBoxEdit Cbo1
        {
            get
            {
                return cbo1;
            }
            set
            {
                cbo1 = value;
            }
        }
        [Category("Custom Size")]
        public Size Cbo1Size
        {
            get { return cbo1.Size; }
            set
            {
                cbo1.Size = value;
            }
        }
        [Category("Custom")]
        public ComboBoxEdit Cbo2
        {
            get
            {
                return cbo2;
            }
            set
            {
                cbo2 = value;
            }
        }
        [Category("Custom Size")]
        public Size Cbo2Size
        {
            get { return cbo2.Size; }
            set
            {
                cbo2.Size = value;
            }
        }
        [Category("Custom")]
        public TextEdit Txt1
        {
            get
            {
                return txt1;
            }
            set
            {
                txt1 = value;
            }
        }
        [Category("Custom Size")]
        public Size Txt1Size
        {
            get { return txt1.Size; }
            set
            {
                txt1.Size = value;
            }
        }
        #endregion
    }
}
