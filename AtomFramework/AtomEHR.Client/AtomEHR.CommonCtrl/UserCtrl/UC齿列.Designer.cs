﻿namespace AtomEHR.CommonCtrl.UserCtrl
{
    partial class UC齿列
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chk其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chk义齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk缺齿 = new DevExpress.XtraEditors.CheckEdit();
            this.txt缺齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿4 = new DevExpress.XtraEditors.TextEdit();
            this.chk龋齿 = new DevExpress.XtraEditors.CheckEdit();
            this.txt龋齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt其他 = new DevExpress.XtraEditors.TextEdit();
            this.chk正常 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk义齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk缺齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk龋齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.chk正常);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(507, 155);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tableLayoutPanel1);
            this.panelControl1.Location = new System.Drawing.Point(4, 27);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(499, 124);
            this.panelControl1.TabIndex = 5;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.chk其他, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.chk义齿, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.chk缺齿, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt缺齿4, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.chk龋齿, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿3, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿2, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿4, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt义齿4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt其他, 4, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(495, 120);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chk其他
            // 
            this.chk其他.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk其他.Location = new System.Drawing.Point(241, 56);
            this.chk其他.Name = "chk其他";
            this.chk其他.Properties.Appearance.Options.UseTextOptions = true;
            this.chk其他.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk其他.Properties.AutoHeight = false;
            this.chk其他.Properties.Caption = "其他";
            this.tableLayoutPanel1.SetRowSpan(this.chk其他, 2);
            this.chk其他.Size = new System.Drawing.Size(33, 61);
            this.chk其他.TabIndex = 15;
            this.chk其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // chk义齿
            // 
            this.chk义齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk义齿.Location = new System.Drawing.Point(3, 56);
            this.chk义齿.Name = "chk义齿";
            this.chk义齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk义齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk义齿.Properties.AutoHeight = false;
            this.chk义齿.Properties.Caption = "义齿(假牙)";
            this.tableLayoutPanel1.SetRowSpan(this.chk义齿, 2);
            this.chk义齿.Size = new System.Drawing.Size(39, 61);
            this.chk义齿.TabIndex = 10;
            this.chk义齿.CheckedChanged += new System.EventHandler(this.chk义齿_CheckedChanged);
            // 
            // chk缺齿
            // 
            this.chk缺齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk缺齿.Location = new System.Drawing.Point(3, 3);
            this.chk缺齿.Name = "chk缺齿";
            this.chk缺齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk缺齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk缺齿.Properties.AutoHeight = false;
            this.chk缺齿.Properties.Caption = "缺齿";
            this.tableLayoutPanel1.SetRowSpan(this.chk缺齿, 2);
            this.chk缺齿.Size = new System.Drawing.Size(39, 47);
            this.chk缺齿.TabIndex = 0;
            this.chk缺齿.CheckedChanged += new System.EventHandler(this.chk缺齿_CheckedChanged);
            this.chk缺齿.EnabledChanged += new System.EventHandler(this.chk缺齿_EnabledChanged);
            // 
            // txt缺齿1
            // 
            this.txt缺齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿1.Location = new System.Drawing.Point(48, 3);
            this.txt缺齿1.Name = "txt缺齿1";
            this.txt缺齿1.Size = new System.Drawing.Size(91, 20);
            this.txt缺齿1.TabIndex = 1;
            // 
            // txt缺齿3
            // 
            this.txt缺齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿3.Location = new System.Drawing.Point(48, 29);
            this.txt缺齿3.Name = "txt缺齿3";
            this.txt缺齿3.Size = new System.Drawing.Size(91, 20);
            this.txt缺齿3.TabIndex = 2;
            // 
            // txt缺齿2
            // 
            this.txt缺齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿2.Location = new System.Drawing.Point(145, 3);
            this.txt缺齿2.Name = "txt缺齿2";
            this.txt缺齿2.Size = new System.Drawing.Size(90, 20);
            this.txt缺齿2.TabIndex = 3;
            // 
            // txt缺齿4
            // 
            this.txt缺齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt缺齿4.Location = new System.Drawing.Point(145, 29);
            this.txt缺齿4.Name = "txt缺齿4";
            this.txt缺齿4.Size = new System.Drawing.Size(90, 20);
            this.txt缺齿4.TabIndex = 4;
            // 
            // chk龋齿
            // 
            this.chk龋齿.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk龋齿.Location = new System.Drawing.Point(241, 3);
            this.chk龋齿.Name = "chk龋齿";
            this.chk龋齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk龋齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk龋齿.Properties.AutoHeight = false;
            this.chk龋齿.Properties.Caption = "龋齿";
            this.tableLayoutPanel1.SetRowSpan(this.chk龋齿, 2);
            this.chk龋齿.Size = new System.Drawing.Size(33, 47);
            this.chk龋齿.TabIndex = 5;
            this.chk龋齿.CheckedChanged += new System.EventHandler(this.chk龋齿_CheckedChanged);
            // 
            // txt龋齿1
            // 
            this.txt龋齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿1.Location = new System.Drawing.Point(280, 3);
            this.txt龋齿1.Name = "txt龋齿1";
            this.txt龋齿1.Size = new System.Drawing.Size(101, 20);
            this.txt龋齿1.TabIndex = 6;
            // 
            // txt龋齿3
            // 
            this.txt龋齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿3.Location = new System.Drawing.Point(280, 29);
            this.txt龋齿3.Name = "txt龋齿3";
            this.txt龋齿3.Size = new System.Drawing.Size(101, 20);
            this.txt龋齿3.TabIndex = 7;
            // 
            // txt龋齿2
            // 
            this.txt龋齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿2.Location = new System.Drawing.Point(387, 3);
            this.txt龋齿2.Name = "txt龋齿2";
            this.txt龋齿2.Size = new System.Drawing.Size(105, 20);
            this.txt龋齿2.TabIndex = 8;
            // 
            // txt龋齿4
            // 
            this.txt龋齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt龋齿4.Location = new System.Drawing.Point(387, 29);
            this.txt龋齿4.Name = "txt龋齿4";
            this.txt龋齿4.Size = new System.Drawing.Size(105, 20);
            this.txt龋齿4.TabIndex = 9;
            // 
            // txt义齿1
            // 
            this.txt义齿1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿1.Location = new System.Drawing.Point(48, 56);
            this.txt义齿1.Name = "txt义齿1";
            this.txt义齿1.Size = new System.Drawing.Size(91, 20);
            this.txt义齿1.TabIndex = 11;
            // 
            // txt义齿2
            // 
            this.txt义齿2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿2.Location = new System.Drawing.Point(145, 56);
            this.txt义齿2.Name = "txt义齿2";
            this.txt义齿2.Size = new System.Drawing.Size(90, 20);
            this.txt义齿2.TabIndex = 13;
            // 
            // txt义齿3
            // 
            this.txt义齿3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿3.Location = new System.Drawing.Point(48, 83);
            this.txt义齿3.Name = "txt义齿3";
            this.txt义齿3.Size = new System.Drawing.Size(91, 20);
            this.txt义齿3.TabIndex = 16;
            // 
            // txt义齿4
            // 
            this.txt义齿4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt义齿4.Location = new System.Drawing.Point(145, 83);
            this.txt义齿4.Name = "txt义齿4";
            this.txt义齿4.Size = new System.Drawing.Size(90, 20);
            this.txt义齿4.TabIndex = 17;
            // 
            // txt其他
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txt其他, 2);
            this.txt其他.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt其他.Location = new System.Drawing.Point(280, 76);
            this.txt其他.Margin = new System.Windows.Forms.Padding(3, 23, 3, 3);
            this.txt其他.Name = "txt其他";
            this.tableLayoutPanel1.SetRowSpan(this.txt其他, 2);
            this.txt其他.Size = new System.Drawing.Size(212, 20);
            this.txt其他.TabIndex = 18;
            // 
            // chk正常
            // 
            this.chk正常.EditValue = true;
            this.chk正常.Location = new System.Drawing.Point(4, 4);
            this.chk正常.Name = "chk正常";
            this.chk正常.Properties.Caption = "正常";
            this.chk正常.Size = new System.Drawing.Size(499, 19);
            this.chk正常.StyleController = this.layoutControl1;
            this.chk正常.TabIndex = 4;
            this.chk正常.CheckedChanged += new System.EventHandler(this.chk正常_CheckedChanged);
            this.chk正常.EditValueChanged += new System.EventHandler(this.chk正常_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(507, 155);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chk正常;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(503, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(503, 128);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // UC齿列
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "UC齿列";
            this.Size = new System.Drawing.Size(507, 155);
            this.Load += new System.EventHandler(this.UC齿列_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk义齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk缺齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk龋齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit chk正常;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.CheckEdit chk缺齿;
        private DevExpress.XtraEditors.TextEdit txt缺齿1;
        private DevExpress.XtraEditors.TextEdit txt缺齿3;
        private DevExpress.XtraEditors.TextEdit txt缺齿2;
        private DevExpress.XtraEditors.TextEdit txt缺齿4;
        private DevExpress.XtraEditors.CheckEdit chk龋齿;
        private DevExpress.XtraEditors.CheckEdit chk义齿;
        private DevExpress.XtraEditors.TextEdit txt龋齿1;
        private DevExpress.XtraEditors.TextEdit txt龋齿3;
        private DevExpress.XtraEditors.TextEdit txt龋齿2;
        private DevExpress.XtraEditors.TextEdit txt龋齿4;
        private DevExpress.XtraEditors.CheckEdit chk其他;
        private DevExpress.XtraEditors.TextEdit txt义齿1;
        private DevExpress.XtraEditors.TextEdit txt义齿2;
        private DevExpress.XtraEditors.TextEdit txt义齿3;
        private DevExpress.XtraEditors.TextEdit txt义齿4;
        private DevExpress.XtraEditors.TextEdit txt其他;
    }
}
