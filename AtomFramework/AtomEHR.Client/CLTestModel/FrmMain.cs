﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Interfaces;
using AtomEHR.Common;
using AtomEHR.Library;
using DevExpress.XtraEditors;

namespace CLTestModel
{
    public partial class FrmMain : AtomEHR.Library.frmModuleBase
    {
        public FrmMain()
        {
            InitializeComponent();

            _ModuleID = ModuleID.TestModel; //设置模块编号
            _ModuleName = ModuleNames.TestModel;//设置模块名称
            MenuItemMain.Text = ModuleNames.TestModel; //与AssemblyModuleEntry.ModuleName定义相同

            this.MainMenuStrip = this.menuStrip1;

            SetMenuTag();
        }
        /// <summary>
        /// 重写该方法，返回模块主菜单对象.
        /// </summary>
        /// <returns></returns>
        public override MenuStrip GetModuleMenu()
        {
            return this.menuStrip1;
        }
        /// <summary>
        /// 设置菜单标记。初始化窗体的可用权限
        /// 请参考MenuItemTag类定义
        /// </summary>
        private void SetMenuTag()
        {
            MenuItemMain.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.TestModel, AuthorityCategory.NONE);
            
            MenuItemBusEdit.Tag = new MenuItemTag(MenuType.DataForm, (int)ModuleID.TestModel, AuthorityCategory.BUSINESS_ACTION_VALUE);
        }

        /// <summary>
        /// 设置模块主界面的权限
        /// </summary>        
        public override void SetSecurity(object securityInfo)
        {
            base.SetSecurity(securityInfo);

            if (securityInfo is ToolStrip)
            {
                //设置按钮权限
                btnBusEdit.Enabled = MenuItemBusEdit.Enabled;
            }
        }


        private void MenuItemBusEdit_Click(object sender, EventArgs e)
        {
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(FrmBusEdit), MenuItemBusEdit);
        }

    }
}
