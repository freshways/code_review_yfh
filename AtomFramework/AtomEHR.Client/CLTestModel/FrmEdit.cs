﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;

namespace CLTestModel
{
    public partial class FrmEdit : AtomEHR.Library.frmBaseDataDictionary
    {
        private bllTest _DataProxy = new bllTest();//用户业务类
        public FrmEdit()
        {
            InitializeComponent();
        }

        private void FrmEdit_Load(object sender, EventArgs e)
        {
            InitializeForm();
        }

        protected override void InitializeForm()
        {
            _SummaryView = new DevGridView(gvSummary);//给成员变量赋值.每个业务窗体必需给这个变量赋值.
            _ActiveEditor = txt编码;
            _DetailGroupControl = gcDetailEditor;
            _BLL = new bllTest();

            base.InitializeForm();
        }
        protected override void ButtonStateChanged(UpdateType currentState)
        {
            base.ButtonStateChanged(currentState);
            txt编码.Enabled = UpdateType.Add == currentState;
        }

        // 检查主表数据是否完整或合法
        protected override bool ValidatingData()
        {
            if (txt编码.Text == "")
            {
                Msg.Warning("编码不能为空！");
                txt编码.Focus();
                return false;
            }

            if (txt名称.Text == "")
            {
                Msg.Warning("名称不能为空！");
                txt名称.Focus();
                return false;
            }
            if (_UpdateType == UpdateType.Add)
            {
                if (_BLL.CheckNoExists(txt编码.Text))
                {
                    Msg.Warning("编码：‘" + txt编码.Text + "’已存在!");
                    txt编码.Focus();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 绑定输入框
        /// </summary>
        /// <param name="summary"></param>
        protected override void DoBindingSummaryEditor(DataTable summary)
        {
            try
            {
                if (summary == null) return;
                DataBinder.BindingTextEdit(txt编码, summary, tb_Test._编码);
                DataBinder.BindingTextEdit(txt名称, summary, tb_Test._名称);
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }
    }
}
