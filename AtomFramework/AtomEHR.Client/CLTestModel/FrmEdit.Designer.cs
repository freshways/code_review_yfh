﻿namespace CLTestModel
{
    partial class FrmEdit
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDetailEditor = new DevExpress.XtraEditors.GroupControl();
            this.txt名称 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt编码 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txt_DocDateTo = new DevExpress.XtraEditors.DateEdit();
            this.txt_DocNoTo = new DevExpress.XtraEditors.TextEdit();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txt_DocDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txt_DocNoFrom = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).BeginInit();
            this.gcDetailEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoFrom.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Size = new System.Drawing.Size(797, 326);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Size = new System.Drawing.Size(803, 355);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(803, 355);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.gcDetailEditor);
            this.tpDetail.Size = new System.Drawing.Size(797, 326);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(803, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(625, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(428, 2);
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.Location = new System.Drawing.Point(0, 56);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.Size = new System.Drawing.Size(797, 270);
            this.gcSummary.TabIndex = 0;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnID,
            this.gridColumn编码,
            this.gridColumn名称});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnID
            // 
            this.gridColumnID.Caption = "ID";
            this.gridColumnID.FieldName = "ID";
            this.gridColumnID.Name = "gridColumnID";
            this.gridColumnID.Visible = true;
            this.gridColumnID.VisibleIndex = 0;
            // 
            // gridColumn编码
            // 
            this.gridColumn编码.Caption = "编码";
            this.gridColumn编码.FieldName = "编码";
            this.gridColumn编码.Name = "gridColumn编码";
            this.gridColumn编码.Visible = true;
            this.gridColumn编码.VisibleIndex = 1;
            // 
            // gridColumn名称
            // 
            this.gridColumn名称.Caption = "名称";
            this.gridColumn名称.FieldName = "名称";
            this.gridColumn名称.Name = "gridColumn名称";
            this.gridColumn名称.Visible = true;
            this.gridColumn名称.VisibleIndex = 2;
            // 
            // gcDetailEditor
            // 
            this.gcDetailEditor.Controls.Add(this.txt名称);
            this.gcDetailEditor.Controls.Add(this.labelControl4);
            this.gcDetailEditor.Controls.Add(this.txt编码);
            this.gcDetailEditor.Controls.Add(this.labelControl11);
            this.gcDetailEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDetailEditor.Location = new System.Drawing.Point(0, 0);
            this.gcDetailEditor.Name = "gcDetailEditor";
            this.gcDetailEditor.Size = new System.Drawing.Size(797, 326);
            this.gcDetailEditor.TabIndex = 29;
            // 
            // txt名称
            // 
            this.txt名称.Location = new System.Drawing.Point(97, 96);
            this.txt名称.Name = "txt名称";
            this.txt名称.Size = new System.Drawing.Size(205, 20);
            this.txt名称.TabIndex = 21;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(48, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 18;
            this.labelControl4.Text = "编码：";
            // 
            // txt编码
            // 
            this.txt编码.Location = new System.Drawing.Point(97, 69);
            this.txt编码.Name = "txt编码";
            this.txt编码.Size = new System.Drawing.Size(205, 20);
            this.txt编码.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(48, 99);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(36, 14);
            this.labelControl11.TabIndex = 20;
            this.labelControl11.Text = "名称：";
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.btnEmpty);
            this.gcFindGroup.Controls.Add(this.btnQuery);
            this.gcFindGroup.Controls.Add(this.labelControl23);
            this.gcFindGroup.Controls.Add(this.labelControl22);
            this.gcFindGroup.Controls.Add(this.txt_DocDateTo);
            this.gcFindGroup.Controls.Add(this.txt_DocNoTo);
            this.gcFindGroup.Controls.Add(this.pictureBox3);
            this.gcFindGroup.Controls.Add(this.txt_DocDateFrom);
            this.gcFindGroup.Controls.Add(this.labelControl8);
            this.gcFindGroup.Controls.Add(this.txt_DocNoFrom);
            this.gcFindGroup.Controls.Add(this.labelControl5);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(797, 56);
            this.gcFindGroup.TabIndex = 20;
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(511, 9);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(62, 41);
            this.btnEmpty.TabIndex = 5;
            this.btnEmpty.Text = "清空(&E)";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(446, 8);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(59, 42);
            this.btnQuery.TabIndex = 4;
            this.btnQuery.Text = "查询(&S)";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(111, 32);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(24, 14);
            this.labelControl23.TabIndex = 13;
            this.labelControl23.Text = "至：";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(296, 32);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(24, 14);
            this.labelControl22.TabIndex = 12;
            this.labelControl22.Text = "至：";
            // 
            // txt_DocDateTo
            // 
            this.txt_DocDateTo.EditValue = null;
            this.txt_DocDateTo.Location = new System.Drawing.Point(327, 29);
            this.txt_DocDateTo.Name = "txt_DocDateTo";
            this.txt_DocDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_DocDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt_DocDateTo.Size = new System.Drawing.Size(100, 20);
            this.txt_DocDateTo.TabIndex = 3;
            // 
            // txt_DocNoTo
            // 
            this.txt_DocNoTo.Location = new System.Drawing.Point(142, 29);
            this.txt_DocNoTo.Name = "txt_DocNoTo";
            this.txt_DocNoTo.Size = new System.Drawing.Size(100, 20);
            this.txt_DocNoTo.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(5, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // txt_DocDateFrom
            // 
            this.txt_DocDateFrom.EditValue = null;
            this.txt_DocDateFrom.Location = new System.Drawing.Point(327, 5);
            this.txt_DocDateFrom.Name = "txt_DocDateFrom";
            this.txt_DocDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_DocDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt_DocDateFrom.Size = new System.Drawing.Size(100, 20);
            this.txt_DocDateFrom.TabIndex = 2;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(260, 8);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "支票日期：";
            // 
            // txt_DocNoFrom
            // 
            this.txt_DocNoFrom.Location = new System.Drawing.Point(142, 5);
            this.txt_DocNoFrom.Name = "txt_DocNoFrom";
            this.txt_DocNoFrom.Size = new System.Drawing.Size(100, 20);
            this.txt_DocNoFrom.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(75, 7);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "付款号码：";
            // 
            // FrmEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(803, 381);
            this.Name = "FrmEdit";
            this.Load += new System.EventHandler(this.FrmEdit_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDetailEditor)).EndInit();
            this.gcDetailEditor.ResumeLayout(false);
            this.gcDetailEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt编码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            this.gcFindGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoFrom.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraEditors.GroupControl gcDetailEditor;
        private DevExpress.XtraEditors.TextEdit txt名称;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txt编码;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn编码;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn名称;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.DateEdit txt_DocDateTo;
        private DevExpress.XtraEditors.TextEdit txt_DocNoTo;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.DateEdit txt_DocDateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txt_DocNoFrom;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}
