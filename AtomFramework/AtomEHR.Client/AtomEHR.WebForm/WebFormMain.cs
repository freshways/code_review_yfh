﻿using NetDimension.NanUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.WebForm
{
    /// <summary>
    /// 带边框模式
    /// </summary>
    public partial class WebFormMain : WinFormium
    {
        public WebFormMain(string URL, string Title = "", int Width = 800, int Hight = 500)
            : base(URL)
        {
            InitializeComponent();
            this.Text = Title;
            this.Size = new Size(Width, Hight);
            this.StartPosition = FormStartPosition.CenterScreen;
        }
    }
}
