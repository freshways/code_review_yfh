﻿using NetDimension.NanUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.WebForm
{
    /// <summary>
    /// 无边框
    /// </summary>
    public partial class WebFormMain2 : Formium
    {
        public WebFormMain2(string URL, int Width = 800, int Hight = 500)
            : base(URL)
        {
            InitializeComponent();
            this.Size = new Size(Width, Hight);
            this.StartPosition = FormStartPosition.CenterParent;
        }
    }
}
