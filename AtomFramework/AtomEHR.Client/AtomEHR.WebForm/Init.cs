﻿using NetDimension.NanUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtomEHR.WebForm
{
    public class Init
    {
        public static bool InitState = false;
        public static void InitCef()
        {
            //初始化浏览器
            InitState = Bootstrap.Load(settings =>
            {
                //禁用日志
                settings.LogSeverity = Chromium.CfxLogSeverity.Disable;

                //指定中文为当前CEF环境的默认语言
                settings.AcceptLanguageList = "zh-CN";
                settings.Locale = "zh-CN";
            }, commandLine =>
            {
                //在启动参数中添加disable-web-security开关，禁用跨域安全检测
                commandLine.AppendSwitch("disable-web-security");

            });
        }

        public static void ShowForm(string URL)
        {
            if (!Init.InitState)
            {
                //始化CEF
                Init.InitCef();
            }
            WebFormMain frm = new WebFormMain(URL);
            frm.Show();
        }
    }
}
