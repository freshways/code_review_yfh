﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_家庭档案的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015-08-10 10:51:16
     *   最后修改: 2015-08-10 10:51:16
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_家庭档案,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_家庭档案", "家庭档案编号", true)]
    public sealed class tb_家庭档案
    {
        public static string __TableName = "tb_家庭档案";

        public static string __KeyName = "家庭档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, false, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 18, false, true, true, false, true)]
        public static string 家庭档案编号 = "家庭档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string 住房类型 = "住房类型";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 住房面积 = "住房面积";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 厕所类型 = "厕所类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 厕所类型其他 = "厕所类型其他";

        [ORM_FieldAttribute(SqlDbType.Decimal, 9, false, true, false, false, false)]
        public static string 人均收入 = "人均收入";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否低保户 = "是否低保户";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 吃油量 = "吃油量";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 吃盐量 = "吃盐量";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 建档时间 = "建档时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建时间 = "创建时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改时间 = "修改时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建机构 = "创建机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 18, false, true, false, false, false)]
        public static string D_JTDABH_NEW = "D_JTDABH_NEW";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string 缺少项 = "缺少项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 完整度 = "完整度";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 街道 = "街道";
    }
}