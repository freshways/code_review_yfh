﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_通知通告表_附件的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-10-11 03:26:29
 *   最后修改: 2018-10-11 03:26:29
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_通知通告表_附件,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_通知通告表_附件", "GUID", true)]
    public sealed class tb_通知通告表_附件
    {
        public static string __TableName ="tb_通知通告表_附件";

        public static string __KeyName = "GUID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,32,false,true,true,false,false)]
        public static string GUID = "GUID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,32,false,true,false,false,false)]
        public static string Map_GUID = "Map_GUID"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,100,false,true,false,false,false)]
        public static string fileName = "fileName"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string fileType = "fileType"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string fileSize = "fileSize"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,4000,false,true,false,false,false)]
        public static string filePath = "filePath"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string createTime = "createTime"; 

    }
}