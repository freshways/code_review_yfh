﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_健康档案的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/08/17 03:58:04
     *   最后修改: 2015/08/17 03:58:04
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康档案,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康档案", "个人档案编号", false)]
    public sealed class tb_健康档案
    {
        public static string __TableName = "tb_健康档案";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, false, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, true, false, true)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 18, false, true, false, true, true)]
        public static string 家庭档案编号 = "家庭档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string 姓名 = "姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 身份证号 = "身份证号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 150, false, true, false, false, false)]
        public static string 拼音简码 = "拼音简码";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 与户主关系 = "与户主关系";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 工作单位 = "工作单位";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 本人电话 = "本人电话";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 邮箱 = "邮箱";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 省 = "省";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 市 = "市";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 区 = "区";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 街道 = "街道";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 居委会 = "居委会";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 居住地址 = "居住地址";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属片区 = "所属片区";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 常住类型 = "常住类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 性别 = "性别";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 出生日期 = "出生日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 民族 = "民族";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 文化程度 = "文化程度";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 职业 = "职业";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 婚姻状况 = "婚姻状况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 医疗费支付类型 = "医疗费支付类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 医疗保险号 = "医疗保险号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 新农合号 = "新农合号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 调查时间 = "调查时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建时间 = "创建时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改时间 = "修改时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建机构 = "创建机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string PASSWORD = "PASSWORD";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string D_ZHUXIAO = "D_ZHUXIAO";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 联系人姓名 = "联系人姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 联系人电话 = "联系人电话";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 医疗费用支付类型其他 = "医疗费用支付类型其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 怀孕情况 = "怀孕情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 孕次 = "孕次";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 产次 = "产次";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string 缺项 = "缺项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 档案类别 = "档案类别";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 档案状态 = "档案状态";

        [ORM_FieldAttribute(SqlDbType.VarChar, 1, false, true, false, false, false)]
        public static string D_DAZTYY = "D_DAZTYY";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 证件类型 = "证件类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string D_ZJHQT = "D_ZJHQT";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 厨房排气设施 = "厨房排气设施";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 燃料类型 = "燃料类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 饮水 = "饮水";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 厕所 = "厕所";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 禽畜栏 = "禽畜栏";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 完整度 = "完整度";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string D_GRDABH_17 = "D_GRDABH_17";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 20, false, true, false, false, false)]
        public static string D_GRDABH_SHOW = "D_GRDABH_SHOW";
        //2017-02-08 16:11:54 yufh 添加 ，原来 D_GRDABH_SHOW 字段不稳定
        [ORM_FieldAttribute(SqlDbType.NVarChar, 20, false, true, false, false, false)]
        public static string 档案位置 = "档案位置";
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 劳动强度 = "劳动强度";
        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否流动人口 = "是否流动人口";
        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否签约 = "是否签约";
        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否贫困人口 = "是否贫困人口";
        //2017-02-08 11:39:03 yufh 添加 沙沟
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 外出情况 = "外出情况";
        [ORM_FieldAttribute(SqlDbType.NVarChar, 50, false, true, false, false, false)]
        public static string 外出地址 = "外出地址";
        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string 户主姓名 = "户主姓名";
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 户主身份证号 = "户主身份证号";
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 家庭人口数 = "家庭人口数";
        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 家庭结构 = "家庭结构";
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 居住情况 = "居住情况";

        //[ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        //public static string 职工医疗保险卡号 = "职工医疗保险卡号";

        //[ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        //public static string 居民医疗保险卡号 = "居民医疗保险卡号";

        [ORM_FieldAttribute(SqlDbType.VarChar, -1, false, true, false, false, false)]
        public static string 本人或家属签字 = "本人或家属签字";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 签字时间 = "签字时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 贫困救助卡号 = "贫困救助卡号";
        //孙局长让加了给泉庄用，没有指纹仪
        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 复核标记 = "复核标记";
        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 复核备注 = "复核备注";

        //孙局长让加的，
        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 二次复核 = "二次复核";
        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 二次复核时间 = "二次复核时间";

    }
}