﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_档案摘要的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-07-09 10:52:42
 *   最后修改: 2015-07-09 10:52:42
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_档案摘要,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_档案摘要", "档案号", true)]
    public sealed class tb_档案摘要
    {
        public static string __TableName ="tb_档案摘要";

        public static string __KeyName = "档案号";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,false,false,false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, true, false, true)]
        public static string 档案号 = "档案号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 姓名 = "姓名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 性别 = "性别"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 生日 = "生日"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,11,false,true,false,false,false)]
        public static string 手机号 = "手机号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 居住地址 = "居住地址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

    }
}