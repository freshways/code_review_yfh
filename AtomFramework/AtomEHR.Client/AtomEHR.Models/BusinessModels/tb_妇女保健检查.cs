﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_妇女保健检查的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/10/06 11:24:06
 *   最后修改: 2015/10/06 11:24:06
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_妇女保健检查,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_妇女保健检查", "ID", true)]
    public sealed class tb_妇女保健检查
    {
        public static string __TableName ="tb_妇女保健检查";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 检查次数 = "检查次数"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 现有症状 = "现有症状"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 曾患妇科病 = "曾患妇科病"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 曾做手术 = "曾做手术"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 初经年龄 = "初经年龄"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 持续天数 = "持续天数"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 间隔天数 = "间隔天数"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 绝经年龄 = "绝经年龄"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 结扎对象 = "结扎对象"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 结扎医院 = "结扎医院"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 避孕工具 = "避孕工具"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 药物名称 = "药物名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 未孕 = "未孕"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 孕次 = "孕次"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 产次 = "产次"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 自然流产 = "自然流产"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 人工流产 = "人工流产"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 中孕引产 = "中孕引产"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 不孕年数 = "不孕年数"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 男方原因 = "男方原因"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 女方原因 = "女方原因"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 血压 = "血压"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重 = "体重"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 身高 = "身高"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 乳房 = "乳房"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 结节 = "结节"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 压痛 = "压痛"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 乳房左侧 = "乳房左侧"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 乳房右侧 = "乳房右侧"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 心 = "心"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 肺 = "肺"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 透胸 = "透胸"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 外阴 = "外阴"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 阴道 = "阴道"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 白带 = "白带"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 白带性质 = "白带性质"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 子宫颈 = "子宫颈"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 子宫体活动 = "子宫体活动"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 子宫脱垂 = "子宫脱垂"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 子宫体大小 = "子宫体大小"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 左卵巢 = "左卵巢"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 右卵巢 = "右卵巢"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 附件 = "附件"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 阴道异常情况 = "阴道异常情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 白带涂片清洁度 = "白带涂片清洁度"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 酸碱度PH值 = "酸碱度PH值"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 滴虫 = "滴虫"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 霉菌 = "霉菌"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 化检结果 = "化检结果"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 复查结果 = "复查结果"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 印象 = "印象"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 处理意见 = "处理意见"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 治疗情况 = "治疗情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 医师签名 = "医师签名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 痛经 = "痛经"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 末次月经 = "末次月经"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 宫颈刮片 = "宫颈刮片"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 巴氏 = "巴氏"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 结扎日期 = "结扎日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 检查日期 = "检查日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 卡号 = "卡号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 血压2 = "血压2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,6,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

    }
}