﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_MXB冠心病随访表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-12-02 04:22:01
 *   最后修改: 2015-12-02 04:22:01
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB冠心病随访表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB冠心病随访表", "ID", true)]
    public sealed class tb_MXB冠心病随访表
    {
        public static string __TableName ="tb_MXB冠心病随访表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 姓名 = "姓名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 性别 = "性别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,18,false,true,false,false,false)]
        public static string 身份证号 = "身份证号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 出生日期 = "出生日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 民族 = "民族"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 文化程度 = "文化程度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 常住类型 = "常住类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 邮箱 = "邮箱"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 工作单位 = "工作单位"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 婚姻状况 = "婚姻状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 职业 = "职业"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 联系电话 = "联系电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 省 = "省"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 市 = "市"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 区 = "区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 街道 = "街道"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 居委会 = "居委会"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属片区 = "所属片区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 居住地址 = "居住地址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 医疗费支付类型 = "医疗费支付类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 医疗保险号 = "医疗保险号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 新农合号 = "新农合号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_JDJG = "D_JDJG"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_JDSJ = "D_JDSJ"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_JDR = "D_JDR"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 吸烟数量 = "吸烟数量"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 饮酒数量 = "饮酒数量"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 运动频率 = "运动频率"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 运动持续时间 = "运动持续时间"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 身高 = "身高"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重 = "体重"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string D_BMI = "D_BMI"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 收缩压 = "收缩压"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 舒张压 = "舒张压"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 空腹血糖 = "空腹血糖"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 高密度蛋白 = "高密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 低密度蛋白 = "低密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 甘油三脂 = "甘油三脂"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 总胆固醇 = "总胆固醇"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 心率 = "M_XL"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访方式 = "随访方式"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访方式其他 = "随访方式其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 冠心病类型 = "冠心病类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状 = "目前症状"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状其他 = "目前症状其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 心电图检查 = "心电图检查"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 心电图运动负荷 = "心电图运动负荷"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 心脏彩超 = "心脏彩超"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 冠状动脉造影 = "冠状动脉造影"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 心肌酶学 = "心肌酶学"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 服药依从性 = "服药依从性"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string G_YYMC1 = "G_YYMC1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_YF1 = "G_YF1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string G_YYMC2 = "G_YYMC2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_YF2 = "G_YF2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string G_YYMC3 = "G_YYMC3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_YF3 = "G_YF3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 特殊治疗 = "特殊治疗"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 非药物治疗措施 = "非药物治疗措施"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 本次随访分类 = "本次随访分类"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,450,false,true,false,false,false)]
        public static string 随访医生建议 = "随访医生建议"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访医生 = "随访医生"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 发生时间 = "发生时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string D_PYJM = "D_PYJM"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 下次随访时间 = "下次随访时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 此次随访分类不良反应 = "此次随访分类不良反应"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 此次随访分类并发症 = "此次随访分类并发症"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 降压药 = "降压药"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 随访次数 = "随访次数"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 慢病并发症 = "慢病并发症";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 体重2 = "体重2";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string D_BMI2 = "D_BMI2";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 吸烟数量2 = "吸烟数量2";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 饮酒数量2 = "饮酒数量2";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 运动频率2 = "运动频率2";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 运动持续时间2 = "运动持续时间2";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 摄盐情况 = "摄盐情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 摄盐情况2 = "摄盐情况2";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 心理调整 = "心理调整";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 遵医行为 = "遵医行为";

        [ORM_FieldAttribute(SqlDbType.VarChar, 150, false, true, false, false, false)]
        public static string 辅助检查 = "辅助检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 药物副作用 = "药物副作用";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 副作用详述 = "副作用详述"; 

    }
}