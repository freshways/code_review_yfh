﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_健康档案_个人健康特征的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/08/19 03:28:51
     *   最后修改: 2015/08/19 03:28:51
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康档案_个人健康特征,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康档案_个人健康特征", "个人档案编号", false)]
    public sealed class tb_健康档案_个人健康特征
    {
        public static string __TableName = "tb_健康档案_个人健康特征";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, false, false, true)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, true, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, true, false)]
        public static string 家庭档案编号 = "家庭档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 与户主关系 = "与户主关系";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人基本信息表 = "个人基本信息表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 家庭档案 = "家庭档案";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 健康体检 = "健康体检";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 接诊记录 = "接诊记录";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 会诊记录 = "会诊记录";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童基本信息 = "儿童基本信息";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 新生儿随访记录 = "新生儿随访记录";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表满月 = "儿童健康检查记录表满月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表3月 = "儿童健康检查记录表3月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表6月 = "儿童健康检查记录表6月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表8月 = "儿童健康检查记录表8月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表12月 = "儿童健康检查记录表12月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表18月 = "儿童健康检查记录表18月";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表2岁 = "儿童健康检查记录表2岁";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表2岁半 = "儿童健康检查记录表2岁半";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表3岁 = "儿童健康检查记录表3岁";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表4岁 = "儿童健康检查记录表4岁";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表5岁 = "儿童健康检查记录表5岁";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童健康检查记录表6岁 = "儿童健康检查记录表6岁";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 儿童入托信息表 = "儿童入托信息表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 妇女保健检查 = "妇女保健检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 妇女更年期保健检查 = "妇女更年期保健检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 孕产妇基本信息表 = "孕产妇基本信息表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 第一次产前检查表 = "第一次产前检查表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 第二次产前检查表 = "第二次产前检查表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 第三次产前检查表 = "第三次产前检查表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 第四次产前检查表 = "第四次产前检查表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 第五次产前检查表 = "第五次产前检查表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 孕产妇产后访视 = "孕产妇产后访视";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 孕产妇产后42天 = "孕产妇产后42天";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 老年人随访 = "老年人随访";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 高血压管理卡 = "高血压管理卡";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 高血压随访表 = "高血压随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 糖尿病管理卡 = "糖尿病管理卡";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 糖尿病随访表 = "糖尿病随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 脑卒中管理卡 = "脑卒中管理卡";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 脑卒中随访表 = "脑卒中随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 冠心病管理卡 = "冠心病管理卡";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 冠心病随访表 = "冠心病随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 精神疾病信息补充表 = "精神疾病信息补充表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 精神疾病随访表 = "精神疾病随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 听力言语残疾随访表 = "听力言语残疾随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 肢体残疾随访表 = "肢体残疾随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 智力残疾随访表 = "智力残疾随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 视力残疾随访表 = "视力残疾随访表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 残疾人康复服务随访记录表 = "残疾人康复服务随访记录表";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否高血压 = "是否高血压";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否糖尿病 = "是否糖尿病";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否冠心病 = "是否冠心病";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否脑卒中 = "是否脑卒中";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否空腹血糖 = "是否空腹血糖";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否血脂 = "是否血脂";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否餐后血糖 = "是否餐后血糖";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否肥胖 = "是否肥胖";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否重度吸烟 = "是否重度吸烟";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否超重肥胖 = "是否超重肥胖";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否肿瘤 = "是否肿瘤";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2, false, true, false, false, false)]
        public static string 是否慢阻肺 = "是否慢阻肺";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

        public static string oldTableName = "T_DA_JKDA_GRJKTZ";

    }
}