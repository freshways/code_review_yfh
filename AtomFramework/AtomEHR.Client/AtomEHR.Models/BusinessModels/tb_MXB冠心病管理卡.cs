﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_冠心病管理卡的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/08/19 11:38:01
     *   最后修改: 2015/08/19 11:38:01
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_冠心病管理卡,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB冠心病管理卡", "个人档案编号", true)]
    public sealed class tb_MXB冠心病管理卡
    {
        public static string __TableName = "tb_MXB冠心病管理卡";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 管理卡编号 = "管理卡编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, true, true)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 家族史 = "家族史";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 吸烟情况 = "吸烟情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 饮酒情况 = "饮酒情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 运动频率 = "运动频率";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 生活自理能力 = "生活自理能力";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 身高 = "身高";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 体重 = "体重";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string BMI = "BMI";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 收缩压 = "收缩压";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 舒张压 = "舒张压";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 空腹血糖 = "空腹血糖";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 高密度蛋白 = "高密度蛋白";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 低密度蛋白 = "低密度蛋白";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 甘油三酯 = "甘油三酯";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 腰围 = "腰围";

        [ORM_FieldAttribute(SqlDbType.Decimal, 5, false, true, false, false, false)]
        public static string 胆固醇 = "胆固醇";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 体检日期 = "体检日期";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 心率 = "心率";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 管理组别 = "管理组别";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 病例来源 = "病例来源";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 病例来源其他 = "病例来源其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 确诊日期 = "确诊日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 确诊医院 = "确诊医院";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 冠心病类型 = "冠心病类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 目前症状 = "目前症状";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 目前症状其他 = "目前症状其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人病史 = "个人病史";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 心电图检查 = "心电图检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 心电图运动负荷 = "心电图运动负荷";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 心脏彩超检查 = "心脏彩超检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 冠状动脉造影 = "冠状动脉造影";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 心肌酶学检查 = "心肌酶学检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 其他检查结果 = "其他检查结果";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 药物名称1 = "药物名称1";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 用法1 = "用法1";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 药物名称2 = "药物名称2";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 用法2 = "用法2";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, false, false)]
        public static string 药物名称3 = "药物名称3";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 用法3 = "用法3";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 服药依从性 = "服药依从性";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 特殊治疗 = "特殊治疗";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 非药物治疗措施 = "非药物治疗措施";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 终止管理 = "终止管理";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 终止管理日期 = "终止管理日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 终止理由 = "终止理由";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string G_SFYY = "G_SFYY";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 下次随访时间 = "下次随访时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建机构 = "创建机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建时间 = "创建时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改时间 = "修改时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 发生时间 = "发生时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 缺项 = "缺项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string G_JYY = "G_JYY";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 完整度 = "完整度";

    }
}