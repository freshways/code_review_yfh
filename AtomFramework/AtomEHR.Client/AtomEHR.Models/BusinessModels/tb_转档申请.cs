﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_转档申请的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/09/28 11:04:48
     *   最后修改: 2015/09/28 11:04:48
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_转档申请,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_转档申请", "ID", true)]
    public sealed class tb_转档申请
    {
        public static string __TableName = "tb_转档申请";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 转入机构 = "转入机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 转出机构 = "转出机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 转入居委会 = "转入居委会";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string HAPPENTIME = "HAPPENTIME";

        [ORM_FieldAttribute(SqlDbType.VarChar, 1, false, true, false, false, false)]
        public static string 类型 = "类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 家庭档案编号 = "家庭档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 转入详细地址 = "转入详细地址";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 转出居委会 = "转出居委会";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 转出详细地址 = "转出详细地址";

        [ORM_FieldAttribute(SqlDbType.VarChar, 200, false, true, false, false, false)]
        public static string 备注 = "备注";

        [ORM_FieldAttribute(SqlDbType.VarChar, 200, false, true, false, false, false)]
        public static string 拒绝原因 = "拒绝原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构审核人 = "所属机构审核人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 卫生局审核人 = "卫生局审核人";

        //[ORM_FieldAttribute(SqlDbType.VarChar, 1, false, true, false, false, false)]
        //public static string 转档类型 = "转档类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 1, false, true, false, false, false)]
        public static string 转档方式 = "转档方式";
    }
}