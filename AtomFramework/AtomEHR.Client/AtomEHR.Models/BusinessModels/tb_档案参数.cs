﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_档案参数的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-07-09 02:45:46
 *   最后修改: 2015-07-09 02:45:46
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_档案参数,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_档案参数", "档案号", false)]
    public sealed class tb_档案参数
    {
        public static string __TableName ="tb_档案参数";

        public static string __KeyName = "档案号";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,false,false,false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, true, true, false)]
        public static string 档案号 = "档案号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 参数1 = "参数1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 参数2 = "参数2"; 

    }
}