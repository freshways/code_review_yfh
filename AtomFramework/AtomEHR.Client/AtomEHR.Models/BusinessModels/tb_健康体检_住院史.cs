﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_健康体检_住院史的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/09/10 04:40:03
     *   最后修改: 2015/09/10 04:40:03
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康体检_住院史,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康体检_住院史", "ID", false)]
    public sealed class tb_健康体检_住院史
    {
        public static string __TableName = "tb_健康体检_住院史";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 类型 = "类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 入院日期 = "入院日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 出院日期 = "出院日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 200, false, true, false, false, false)]
        public static string 原因 = "原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 医疗机构名称 = "医疗机构名称";

        [ORM_FieldAttribute(SqlDbType.VarChar, 40, false, true, false, false, false)]
        public static string 病案号 = "病案号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 40, false, true, false, false, false)]
        public static string 创建日期 = "创建日期";

    }
}