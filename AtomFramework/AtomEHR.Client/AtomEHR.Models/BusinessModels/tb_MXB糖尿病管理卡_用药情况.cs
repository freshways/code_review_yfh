﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_MXB糖尿病管理卡_用药情况的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-09-09 08:07:17
 *   最后修改: 2015-09-09 08:07:17
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB糖尿病管理卡_用药情况,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB糖尿病管理卡_用药情况", "Y_ID", false)]
    public sealed class tb_MXB糖尿病管理卡_用药情况
    {
        public static string __TableName ="tb_MXB糖尿病管理卡_用药情况";

        public static string __KeyName = "Y_ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string Y_ID = "Y_ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,true,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 药物名称 = "药物名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 用法 = "用法"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,40,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

    }
}