﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_健康体检_非免疫规划预防接种史的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/09/10 04:41:48
 *   最后修改: 2015/09/10 04:41:48
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康体检_非免疫规划预防接种史,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康体检_非免疫规划预防接种史", "ID", false)]
    public sealed class tb_健康体检_非免疫规划预防接种史
    {
        public static string __TableName ="tb_健康体检_非免疫规划预防接种史";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 接种名称 = "接种名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 接种日期 = "接种日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,40,false,true,false,false,false)]
        public static string 接种机构 = "接种机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,40,false,true,false,false,false)]
        public static string 创建日期 = "创建日期"; 

    }
}