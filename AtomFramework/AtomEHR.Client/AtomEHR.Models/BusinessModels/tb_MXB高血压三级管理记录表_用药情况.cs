﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_高血压三级管理记录表_用药情况的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2019-03-19 02:49:02
 *   最后修改: 2019-03-19 02:49:02
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_高血压三级管理记录表_用药情况,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_高血压三级管理记录表_用药情况", "Y_ID", false)]
    public sealed class tb_MXB高血压三级管理记录表_用药情况
    {
        public static string __TableName ="tb_高血压三级管理记录表_用药情况";

        public static string __KeyName = "Y_ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string Y_ID = "Y_ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 药物名称 = "药物名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 用法 = "用法"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 用量 = "用量"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 服药依从性 = "服药依从性"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

    }
}