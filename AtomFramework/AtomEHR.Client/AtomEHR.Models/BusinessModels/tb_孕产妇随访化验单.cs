﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_孕产妇随访化验单的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-12-19 10:37:32
 *   最后修改: 2018-12-19 10:37:32
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_孕产妇随访化验单,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_孕产妇随访化验单", "ID", true)]
    public sealed class tb_孕产妇随访化验单
    {
        public static string __TableName ="tb_孕产妇随访化验单";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 档案号 = "档案号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,36,false,true,false,false,false)]
        public static string PICID = "PICID"; 

        [ORM_FieldAttribute(SqlDbType.VarBinary,-1,false,true,false,false,false)]
        public static string PICBYTE = "PICBYTE"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string valflag = "valflag"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string createuser = "createuser";

        [ORM_FieldAttribute(SqlDbType.DateTime, 8, false, true, false, false, false)]
        public static string createtime = "createtime"; 

    }
}