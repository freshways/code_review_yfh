﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_残疾人康复服务随访记录表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2016-08-10 08:25:38
 *   最后修改: 2016-08-10 08:25:38
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_残疾人康复服务随访记录表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_残疾人康复服务随访记录表", "ID", true)]
    public sealed class tb_残疾人康复服务随访记录表
    {
        public static string __TableName ="tb_残疾人康复服务随访记录表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 主要残疾 = "主要残疾"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 多重残疾 = "多重残疾"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 是否多重残疾 = "是否多重残疾"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 残疾程度 = "残疾程度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访日期 = "随访日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 随访方式 = "随访方式"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 血压高压 = "血压高压"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 血压低压 = "血压低压"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重 = "体重"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 心率1 = "心率1";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 心率2= "心率2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,2000,false,true,false,false,false)]
        public static string 体检其他 = "体检其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 康复服务情况 = "康复服务情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2000, false, true, false, false, false)]
        public static string 康复服务情况其他 = "康复服务情况其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2000, false, true, false, false, false)]
        public static string 转介原因 = "转介原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 2000, false, true, false, false, false)]
        public static string 转介去向 = "转介去向"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 功能训练每月次1 = "功能训练每月次1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 功能训练每次分钟1 = "功能训练每次分钟1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 功能训练每月次2 = "功能训练每月次2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 功能训练每次分钟2 = "功能训练每次分钟2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 训练场地 = "训练场地"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 训练评估分数 = "训练评估分数"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 康复目标 = "康复目标"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,2000,false,true,false,false,false)]
        public static string 康复目标其他 = "康复目标其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 训练效果 = "训练效果"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 遵医行为 = "遵医行为"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 此次随访分类 = "此次随访分类"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 下次随访日期 = "下次随访日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 服务对象或家属 = "服务对象或家属"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访医生 = "随访医生"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string 缺项="缺项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 完整度="完整度";

    }
}