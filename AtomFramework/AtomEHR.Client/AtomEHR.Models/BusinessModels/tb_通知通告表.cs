﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_通知通告表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-10-09 11:26:29
 *   最后修改: 2018-10-09 11:26:29
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_通知通告表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_通知通告表", "GUID", true)]
    public sealed class tb_通知通告表
    {
        public static string __TableName ="tb_通知通告表";

        public static string __KeyName = "GUID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,32,false,true,true,false,false)]
        public static string GUID = "GUID"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,100,false,true,false,false,false)]
        public static string headline = "headline"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,-1,false,true,false,false,false)]
        public static string value = "value"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string setTop = "setTop"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string createOrg = "createOrg"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string updateOrg = "updateOrg"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string createTime = "createTime"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string updateTime = "updateTime"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string creator = "creator"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string modifier = "modifier"; 

    }
}