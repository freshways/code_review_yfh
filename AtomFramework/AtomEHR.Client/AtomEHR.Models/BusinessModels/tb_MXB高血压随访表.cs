﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_MXB高血压随访表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-09-09 08:04:42
 *   最后修改: 2015-09-09 08:04:42
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB高血压随访表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB高血压随访表", "ID", true)]
    public sealed class tb_MXB高血压随访表
    {
        public static string __TableName ="tb_MXB高血压随访表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, true, true)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 姓名 = "姓名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 性别 = "性别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,18,false,true,false,false,false)]
        public static string 身份证号 = "身份证号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 出生日期 = "出生日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 民族 = "民族"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 文化程度 = "文化程度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 常住类型 = "常住类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 邮箱 = "邮箱"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 工作单位 = "工作单位"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 婚姻状况 = "婚姻状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 职业 = "职业"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 联系电话 = "联系电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 省 = "省"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 市 = "市"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 区 = "区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 街道 = "街道"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 居委会 = "居委会"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属片区 = "所属片区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 居住地址 = "居住地址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 医疗费支付类型 = "医疗费支付类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 医疗保险号 = "医疗保险号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 新农合号 = "新农合号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 建档机构 = "建档机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 建档时间 = "建档时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 建档人 = "建档人"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 吸烟数量 = "吸烟数量"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 饮酒数量 = "饮酒数量"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 运动频率 = "运动频率"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 运动持续时间 = "运动持续时间"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 身高 = "身高"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重 = "体重"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string BMI = "BMI"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 腰围 = "腰围"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 收缩压 = "收缩压"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 舒张压 = "舒张压"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 空腹血糖 = "空腹血糖"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 餐后血糖 = "餐后血糖"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 高密度蛋白 = "高密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 低密度蛋白 = "低密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 甘油三脂 = "甘油三脂"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 总胆固醇 = "总胆固醇"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string EKG = "EKG"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访方式 = "随访方式"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访方式其他 = "随访方式其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状 = "目前症状"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状其他 = "目前症状其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_BFZQK = "G_BFZQK"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 降压药 = "降压药"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 药物名称1 = "药物名称1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 使用方法1 = "使用方法1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 药物名称2 = "药物名称2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 使用方法2 = "使用方法2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 药物名称3 = "药物名称3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 使用方法3 = "使用方法3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 服药依从性 = "服药依从性"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 药物副作用 = "药物副作用"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_ZZYY = "G_ZZYY"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 转诊科别 = "转诊科别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 转诊原因 = "转诊原因"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_FYYZLCS = "G_FYYZLCS"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 本次随访分类 = "本次随访分类"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,450,false,true,false,false,false)]
        public static string 随访医生建议 = "随访医生建议"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 随访医生 = "随访医生"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 发生时间 = "发生时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 副作用详述 = "副作用详述"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_BFZQKQT = "G_BFZQKQT"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string D_PYJM = "D_PYJM"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 下次随访时间 = "下次随访时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 心理调整 = "心理调整"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,2,false,true,false,false,false)]
        public static string 遵医行为 = "遵医行为"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 摄盐情况 = "摄盐情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 体征其他 = "体征其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_YYQT = "G_YYQT"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string G_SYQT = "G_SYQT"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 辅助检查 = "辅助检查"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string G_DXL = "G_DXL"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 心率 = "心率"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 慢病并发症 = "慢病并发症"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 转诊情况 = "转诊情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string SFCS = "SFCS"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重2 = "体重2"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string BMI2 = "BMI2"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 吸烟数量2 = "吸烟数量2"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 饮酒数量2 = "饮酒数量2"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 运动频率2 = "运动频率2"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 运动持续时间2 = "运动持续时间2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 摄盐情况2 = "摄盐情况2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 用药调整意见 = "用药调整意见"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 转诊结果 = "转诊结果"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 下一步管理措施 = "下一步管理措施"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 转诊联系人 = "转诊联系人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 转诊联系电话 = "转诊联系电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,-1,false,true,false,false,false)]
        public static string 居民签名 = "居民签名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 备注 = "备注"; 

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 访视情况 = "访视情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 失访原因 = "失访原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 死亡日期 = "死亡日期";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 60, false, true, false, false, false)]
        public static string 死亡原因 = "死亡原因"; 
    }
}