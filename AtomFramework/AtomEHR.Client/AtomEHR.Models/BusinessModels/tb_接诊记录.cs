﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_接诊记录的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-08-26 02:28:40
 *   最后修改: 2015-08-26 02:28:40
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_接诊记录,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_接诊记录", "ID", true)]
    public sealed class tb_接诊记录
    {
        public static string __TableName ="tb_接诊记录";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, true, true)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 接诊医生 = "接诊医生"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 主观资料 = "主观资料"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 客观资料 = "客观资料"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 评估 = "评估"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 处置计划 = "处置计划"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string PRESENTREGION = "PRESENTREGION"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 接诊时间 = "接诊时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,6,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

    }
}