﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_个案随访表的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2017-09-28 03:32:47
     *   最后修改: 2017-09-28 03:32:47
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_个案随访表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_个案随访表", "ID", true)]
    public sealed class tb_个案随访表
    {
        public static string __TableName = "tb_个案随访表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 25, false, true, false, false, false)]
        public static string 卡片编号 = "卡片编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string 患者姓名 = "患者姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 患儿家长姓名 = "患儿家长姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 18, false, true, false, false, false)]
        public static string 身份证号 = "身份证号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 性别 = "性别";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 联系电话 = "联系电话";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 200, false, true, false, false, false)]
        public static string 居住地址 = "居住地址";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 随访次数 = "随访次数";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否羁押 = "是否羁押";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 失访原因 = "失访原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 查无此人 = "查无此人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 病程阶段 = "病程阶段";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 艾滋病确诊日期 = "艾滋病确诊日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否已死亡 = "是否已死亡";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 死亡日期 = "死亡日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 主要死因 = "主要死因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否为母婴传播病例 = "是否为母婴传播病例";

        [ORM_FieldAttribute(SqlDbType.VarChar, 25, false, true, false, false, false)]
        public static string 阳性母亲卡片编号 = "阳性母亲卡片编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否有配偶 = "是否有配偶";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 配偶变更及性行为情况 = "配偶变更及性行为情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 配偶感染状况 = "配偶感染状况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 检测日期 = "检测日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 25, false, true, false, false, false)]
        public static string 配偶感染状况为阳性卡片编号 = "配偶感染状况为阳性卡片编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否有固定性伴 = "是否有固定性伴";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 异性固定性伴个数 = "异性固定性伴个数";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 异性固定性伴检测人数 = "异性固定性伴检测人数";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 同性固定性伴个数 = "同性固定性伴个数";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 同性固定性伴检测人数 = "同性固定性伴检测人数";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 过去3个月性行为人数 = "过去3个月性行为人数";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否每次性行为时都用安全套 = "是否每次性行为时都用安全套";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否每次与固定性伴性行为时都用安全套 = "是否每次与固定性伴性行为时都用安全套";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否咳嗽咳痰 = "是否咳嗽咳痰";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否咳痰带血 = "是否咳痰带血";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否反复发热 = "是否反复发热";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否夜间出汗 = "是否夜间出汗";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否异常体重下降 = "是否异常体重下降";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否容易疲劳 = "是否容易疲劳";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否淋巴结肿大 = "是否淋巴结肿大";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否接受过结核病检查 = "是否接受过结核病检查";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 结核病检查结果 = "结核病检查结果";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 是否接受免费艾滋病抗病毒治疗 = "是否接受免费艾滋病抗病毒治疗";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 抗病毒治疗编号 = "抗病毒治疗编号";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string 做过CD4检测次数 = "做过CD4检测次数";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, true, false, false, false)]
        public static string CD4检测结果个数 = "CD4检测结果个数";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string CD4检测日期 = "CD4检测日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string CD4检测单位 = "CD4检测单位";

        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string 随访实施单位 = "随访实施单位";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 随访人员 = "随访人员";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 随访日期 = "随访日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 300, false, true, false, false, false)]
        public static string 备注 = "备注";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 缺项 = "缺项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 完整度 = "完整度";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建机构 = "创建机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建时间 = "创建时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改时间 = "修改时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

    }
}