﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_精神病_用药情况的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/10/31 02:41:28
 *   最后修改: 2015/10/31 02:41:28
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_精神病_用药情况,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_精神病_用药情况", "ID", true)]
    public sealed class tb_精神病_用药情况
    {
        public static string __TableName ="tb_精神病_用药情况";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,true,false,false,false)]
        public static string C_JRID = "C_JRID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 药物名称 = "药物名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 剂量 = "剂量"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 用法 = "用法"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string YONGFA = "YONGFA"; 

    }
}