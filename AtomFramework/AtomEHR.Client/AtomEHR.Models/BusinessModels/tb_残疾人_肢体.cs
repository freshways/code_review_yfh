﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_残疾人_听力的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/11/25 02:58:51
     *   最后修改: 2015/11/25 02:58:51
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_残疾人_听力,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_残疾人_肢体", "ID", true)]
    public sealed class tb_残疾人_肢体
    {
        public static string __TableName = "tb_残疾人_肢体";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 致残时间 = "致残时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 60, false, true, false, false, false)]
        public static string 残疾证号 = "残疾证号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 致残原因 = "致残原因";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 致残原因其他 = "致残原因其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 残疾程度 = "残疾程度";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 持续时间 = "持续时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 其他伴随残疾 = "其他伴随残疾";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 智力残疾 = "智力残疾";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 肢体残疾 = "肢体残疾";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人自理 = "个人自理";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 监护人姓名 = "监护人姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 监护人电话 = "监护人电话";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 盲校 = "盲校";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 盲校年级 = "盲校年级";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 聋校 = "聋校";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 聋校年纪 = "聋校年纪";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 其他特殊学校 = "其他特殊学校";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 其他特殊学校年级 = "其他特殊学校年级";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 就业情况 = "就业情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 就业情况其他 = "就业情况其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 150, false, true, false, false, false)]
        public static string 收入来源 = "收入来源";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 收入来源其他 = "收入来源其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 平均收入 = "平均收入";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 劳动能力 = "劳动能力";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 劳动技能 = "劳动技能";

        [ORM_FieldAttribute(SqlDbType.VarChar, 150, false, true, false, false, false)]
        public static string 能力来源 = "能力来源";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 能力来源其他 = "能力来源其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 40, false, true, false, false, false)]
        public static string 伤残部位 = "伤残部位";

        [ORM_FieldAttribute(SqlDbType.VarChar, 80, false, true, false, false, false)]
        public static string 康复治疗情况 = "康复治疗情况";

        [ORM_FieldAttribute(SqlDbType.VarChar, 80, false, true, false, false, false)]
        public static string 康复需求 = "康复需求";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 随访医生 = "随访医生";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 下次随访日期 = "下次随访日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 随访时间 = "随访时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建机构 = "创建机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建日期 = "创建日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改日期 = "修改日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 修改人 = "修改人";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 所属机构 = "所属机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string FIELD2 = "FIELD2";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string FIELD3 = "FIELD3";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string FIELD4 = "FIELD4";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string FIELD5 = "FIELD5";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string 缺项 = "缺项";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 随访次数 = "随访次数";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 完整度 = "完整度";

    }
}