﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_CJR_精神疾病信息补充表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/10/18 09:59:15
 *   最后修改: 2015/10/18 09:59:15
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_CJR_精神疾病信息补充表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_精神疾病信息补充表", "个人档案编号", true)]
    public sealed class tb_精神疾病信息补充表
    {
        public static string __TableName ="tb_精神疾病信息补充表";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,true,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 监护人姓名 = "监护人姓名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 与患者关系 = "与患者关系"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 监护人住址 = "监护人住址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 监护人电话 = "监护人电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 联系人电话 = "联系人电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 初次发病时间 = "初次发病时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 既往主要症状 = "既往主要症状"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 既往主要症状其他 = "既往主要症状其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 门诊状况 = "门诊状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 住院次数 = "住院次数"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 诊断 = "诊断"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 确诊医院 = "确诊医院"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 确诊日期 = "确诊日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 治疗效果 = "治疗效果"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 对家庭和社会的影响 = "对家庭和社会的影响"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 轻度滋事 = "轻度滋事"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 肇事 = "肇事"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 肇祸 = "肇祸"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 自伤 = "自伤"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 自杀未遂 = "自杀未遂"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 关锁情况 = "关锁情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 医生签名 = "医生签名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 检查日期 = "检查日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 知情同意 = "知情同意"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 签字时间 = "签字时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 签字 = "签字"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 首次抗精神病药治疗时间 = "首次抗精神病药治疗时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 经济状况 = "经济状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,500,false,true,false,false,false)]
        public static string 医生意见 = "医生意见"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 下次随访时间 = "下次随访时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 姓名 = "姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 30, false, true, false, true, false)]
        public static string 家庭档案编号 = "家庭档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,40,false,true,false,false,false)]
        public static string 拼音解码 = "拼音解码"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 身份证号 = "身份证号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 工作地址 = "工作地址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 联系电话 = "联系电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 省 = "省"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 市 = "市"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 区 = "区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 街道 = "街道"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 居委会 = "居委会"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 居住地址 = "居住地址"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属片区 = "所属片区"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 常住类型 = "常住类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 性别 = "性别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 出生日期 = "出生日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 民族 = "民族"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 文化程度 = "文化程度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 职业 = "职业"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 婚姻状况 = "婚姻状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 医疗支付类型 = "医疗支付类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 新农合号 = "新农合号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 档案状态 = "档案状态"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_JZZK = "D_JZZK"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string D_YLFZFLX = "D_YLFZFLX"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_GRDABH_17 = "D_GRDABH_17"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_GRDABH_SHOW = "D_GRDABH_SHOW"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 户别 = "户别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 就业情况 = "就业情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 危险行为_其他危害 = "危险行为_其他危害"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 患者签字 = "患者签字"; 

    }
}