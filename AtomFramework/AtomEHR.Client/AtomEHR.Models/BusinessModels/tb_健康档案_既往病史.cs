﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_健康档案_既往病史的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2015/08/24 11:42:48
     *   最后修改: 2015/08/24 11:42:48
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康档案_既往病史,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康档案_既往病史", "ID", false)]
    public sealed class tb_健康档案_既往病史
    {
        public static string __TableName = "tb_健康档案_既往病史";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, true, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string 疾病名称 = "疾病名称";

        [ORM_FieldAttribute(SqlDbType.VarChar, 500, false, true, false, false, false)]
        public static string D_JBBM = "D_JBBM";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 日期 = "日期";

        [ORM_FieldAttribute(SqlDbType.NChar, 40, false, true, false, false, false)]
        public static string D_ZDDW = "D_ZDDW";

        [ORM_FieldAttribute(SqlDbType.VarChar, 100, false, true, false, false, false)]
        public static string D_CLQK = "D_CLQK";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string D_ZGQK = "D_ZGQK";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 疾病名称其他 = "疾病名称其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 10, false, true, false, false, false)]
        public static string 疾病类型 = "疾病类型";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 恶性肿瘤 = "恶性肿瘤";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 疾病其他 = "疾病其他";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 职业病其他 = "职业病其他";
    }
}