﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_参数列表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/09/10 10:54:00
 *   最后修改: 2015/09/10 10:54:00
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_参数列表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_参数列表", "ID", true)]
    public sealed class tb_参数列表
    {
        public static string __TableName ="tb_参数列表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 别名 = "别名"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,200,false,true,false,false,false)]
        public static string 名称 = "名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 键值 = "键值"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 备注 = "备注"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string P_RGID = "P_RGID"; 

    }
}