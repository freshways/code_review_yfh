﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_MXB高血压高危人群干预调查与随访记录表_个人膳食的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-11-12 03:11:05
 *   最后修改: 2018-11-12 03:11:05
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB高血压高危人群干预调查与随访记录表_个人膳食,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB高血压高危人群干预调查与随访记录表_个人膳食", "ID", true)]
    public sealed class tb_MXB高血压高危人群干预调查与随访记录表_个人膳食
    {
        public static string __TableName ="tb_MXB高血压高危人群干预调查与随访记录表_个人膳食";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 食物名称 = "食物名称"; 

        [ORM_FieldAttribute(SqlDbType.Char,1,false,true,false,false,false)]
        public static string 是否吃 = "是否吃"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 次_天 = "次_天"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 次_周 = "次_周"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 次_月 = "次_月"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 平均每次食用量 = "平均每次食用量"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

    }
}