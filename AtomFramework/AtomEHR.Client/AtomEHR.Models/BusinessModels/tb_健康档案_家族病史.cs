﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_健康档案_家族病史的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/08/25 08:38:36
 *   最后修改: 2015/08/25 08:38:36
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_健康档案_家族病史,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_健康档案_家族病史", "ID", false)]
    public sealed class tb_健康档案_家族病史
    {
        public static string __TableName ="tb_健康档案_家族病史";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,true,false)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 家族关系 = "家族关系"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 家族病史 = "家族病史"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,500,false,true,false,false,false)]
        public static string D_JBBM = "D_JBBM"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 其他疾病 = "其他疾病"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string D_QTJBBM = "D_QTJBBM"; 

         [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 家族关系名称 = "家族关系名称"; 
        
    }
}