﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


    /*==========================================
     *   程序说明: tb_MXB高血压随访表_用药调整的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2017-09-07 12:02:02
     *   最后修改: 2017-09-07 12:02:02
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB高血压随访表_用药调整,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB高血压随访表_用药调整", "Y_ID", true)]
    public sealed class tb_MXB高血压随访表_用药调整
    {
        public static string __TableName = "tb_MXB高血压随访表_用药调整";

        public static string __KeyName = "Y_ID";

        [ORM_FieldAttribute(SqlDbType.BigInt, 8, false, false, true, false, false)]
        public static string Y_ID = "Y_ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 个人档案编号 = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 药物名称 = "药物名称";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string 用法 = "用法";

        [ORM_FieldAttribute(SqlDbType.VarChar, 40, false, true, false, false, false)]
        public static string 创建时间 = "创建时间";

    }
}
