﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_MXB糖尿病管理卡的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-09-09 08:07:02
 *   最后修改: 2015-09-09 08:07:02
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_MXB糖尿病管理卡,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_MXB糖尿病管理卡", "个人档案编号", true)]
    public sealed class tb_MXB糖尿病管理卡
    {
        public static string __TableName ="tb_MXB糖尿病管理卡";

        public static string __KeyName = "个人档案编号";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 管理卡编号 = "管理卡编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,true,true,true)]
        public static string 个人档案编号 = "个人档案编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 家族史 = "家族史"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 吸烟情况 = "吸烟情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 饮酒情况 = "饮酒情况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 锻炼频率 = "锻炼频率"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 生活自理能力 = "生活自理能力"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 身高 = "身高"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 体重 = "体重"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string BMI = "BMI"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 腰围 = "腰围"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 收缩压 = "收缩压"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 舒张压 = "舒张压"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 空腹血糖 = "空腹血糖"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 餐后血糖 = "餐后血糖"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 高密度蛋白 = "高密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 低密度蛋白 = "低密度蛋白"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 甘油三脂 = "甘油三脂"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 总胆固醇 = "总胆固醇"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 体检日期 = "体检日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 管理组别 = "管理组别"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 病例来源 = "病例来源"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 病例来源其他 = "病例来源其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状 = "目前症状"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 目前症状其他 = "目前症状其他"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 糖尿病类型 = "糖尿病类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 确诊时间 = "确诊时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 确诊单位 = "确诊单位"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 肾脏病变 = "肾脏病变"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 神经病变 = "神经病变"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 心脏病变 = "心脏病变"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 视网膜病变 = "视网膜病变"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 足部病变 = "足部病变"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 脑血管病变 = "脑血管病变"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 糖化血红蛋白 = "糖化血红蛋白"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string T_YYMC1 = "T_YYMC1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string T_YF1 = "T_YF1"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string T_YYMC2 = "T_YYMC2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string T_YF2 = "T_YF2"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string T_YYMC3 = "T_YYMC3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string T_YF3 = "T_YF3"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 终止管理日期 = "终止管理日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 终止理由 = "终止理由"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string T_SFYY = "T_SFYY"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string T_XCSFSJ = "T_XCSFSJ"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建机构 = "创建机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 发生时间 = "发生时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string T_BFZQK = "T_BFZQK"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,5,false,true,false,false,false)]
        public static string 胰岛素用量 = "胰岛素用量"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 胰岛素 = "胰岛素"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 口服降糖药 = "口服降糖药"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 尿微量蛋白 = "尿微量蛋白"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 终止管理 = "终止管理"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 缺项 = "缺项"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 完整度 = "完整度"; 

    }
}