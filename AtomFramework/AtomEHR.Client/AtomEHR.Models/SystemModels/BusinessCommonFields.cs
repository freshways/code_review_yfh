﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtomEHR.Models
{
    /// <summary>
    /// 业务单据标准字段
    /// </summary>
    public class BusinessCommonFields
    {
        public const string CreationDate = "CreationDate";
        public const string CreatedBy = "CreatedBy";
        public const string LastUpdateDate = "LastUpdateDate";
        public const string LastUpdatedBy = "LastUpdatedBy";
        public const string FlagApp = "FlagApp";
        public const string AppUser = "AppUser";
        public const string AppDate = "AppDate";
        public const string s创建人= "创建人";
        public const string s创建时间 = "创建时间";
        public const string s修改人 = "修改人";
        public const string s修改时间 = "修改时间";
    }

    /// <summary>
    /// 业务单据明细表标准字段
    /// </summary>
    public class BusinessDetailCommonFields
    {
        public const string CreationDate = "CreationDate";
        public const string CreatedBy = "CreatedBy";
        public const string LastUpdateDate = "LastUpdateDate";
        public const string LastUpdatedBy = "LastUpdatedBy";
    }

}
