﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;


/*==========================================
 *   程序说明: tb_机构信息的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015-07-15 11:19:13
 *   最后修改: 2015-07-15 11:19:13
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Models
{
    ///<summary>
    /// ORM模型, 数据表:tb_机构信息,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_机构信息", "机构编号", true)]
    public sealed class tb_机构信息
    {
    #region 所有字段的局部变量定义
        private Int64 F_ID;
        private string F_机构编号;
        private string F_机构名称;
        private string F_P_RGID_MARKER;
        private string F_负责人;
        private string F_联系人;
        private string F_联系电话;
        private string F_上级机构;
        private string F_机构级别;
        private string F_创建人;
        private DateTime F_创建时间;
        private string F_状态;
        private string F_P_QYDW;
    #endregion

        public static string __TableName ="tb_机构信息";

        public static string __KeyName = "机构编号";

    #region 所有字段名常量
        public const string _ID = "ID"; 
        public const string _机构编号 = "机构编号"; 
        public const string _机构名称 = "机构名称"; 
        public const string _P_RGID_MARKER = "P_RGID_MARKER"; 
        public const string _负责人 = "负责人"; 
        public const string _联系人 = "联系人"; 
        public const string _联系电话 = "联系电话";
        public const string _上级机构 = "上级机构"; 
        public const string _机构级别 = "机构级别"; 
        public const string _创建人 = "创建人"; 
        public const string _创建时间 = "创建时间"; 
        public const string _状态 = "状态"; 
        public const string _P_QYDW = "P_QYDW"; 
    #endregion

    #region 所有字段属性
        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,false,false,false)]
        public Int64  ID{get{return F_ID;}set{F_ID=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,true,true,true,false,true)]
        public string  机构编号{get{return F_机构编号;}set{F_机构编号=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public string  机构名称{get{return F_机构名称;}set{F_机构名称=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  P_RGID_MARKER{get{return F_P_RGID_MARKER;}set{F_P_RGID_MARKER=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  负责人{get{return F_负责人;}set{F_负责人=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  联系人{get{return F_联系人;}set{F_联系人=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  联系电话{get{return F_联系电话;}set{F_联系电话=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public string 上级机构 { get { return F_上级机构; } set { F_上级机构 = value; } }

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  机构级别{get{return F_机构级别;}set{F_机构级别=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  创建人{get{return F_创建人;}set{F_创建人=value;}}

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public DateTime  创建时间{get{return F_创建时间;}set{F_创建时间=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public string  状态{get{return F_状态;}set{F_状态=value;}}

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public string  P_QYDW{get{return F_P_QYDW;}set{F_P_QYDW=value;}}

    #endregion
    }
}