﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;


    /*==========================================
     *   程序说明: tb_医生信息的ORM模型
     *   作者姓名: ATOM
     *   创建日期: 2018-11-02 04:59:07
     *   最后修改: 2018-11-02 04:59:07
     *   
     *   注: 本代码由[代码生成器]自动生成
     *   版权所有 Copyright © . 2015
     *==========================================*/
	 
namespace AtomEHR.Models
{
    ///<summary>
    /// ORM模型, 数据表:tb_医生信息,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_医生信息", "b编码", true)]
    public sealed class tb_医生信息
    {
        public static string __TableName = "tb_医生信息";

        public static string __KeyName = "b编码";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, false, false, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, true, false, false)]
        public static string b编码 = "b编码";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string x医生姓名 = "x医生姓名";

        [ORM_FieldAttribute(SqlDbType.VarChar, 5, false, true, false, false, false)]
        public static string x性别 = "x性别";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string c出生日期 = "c出生日期";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string z职务 = "z职务";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string k科室 = "k科室";

        [ORM_FieldAttribute(SqlDbType.VarChar, 18, false, true, false, false, false)]
        public static string s身份证号 = "s身份证号";

        [ORM_FieldAttribute(SqlDbType.VarChar, 50, false, true, false, false, false)]
        public static string c查体模块 = "c查体模块";

        [ORM_FieldAttribute(SqlDbType.Bit, 1, false, true, false, false, false)]
        public static string s是否停用 = "s是否停用";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string s所属机构 = "s所属机构";

        [ORM_FieldAttribute(SqlDbType.DateTime, 8, false, true, false, false, false)]
        public static string c创建时间 = "c创建时间";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string c创建人 = "c创建人";

        [ORM_FieldAttribute(SqlDbType.Image, -1, false, true, false, false, false)]
        public static string s手签 = "s手签";

    }
}