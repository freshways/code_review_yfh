﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_B超报告模板的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2019-04-12 11:52:39
 *   最后修改: 2019-04-12 11:52:39
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_B超报告模板,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_B超报告模板", "ID", true)]
    public sealed class tb_B超报告模板
    {
        public static string __TableName ="tb_B超报告模板";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, false, true, false, false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 检查部位 = "检查部位"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,500,false,true,false,false,false)]
        public static string 超声提示 = "超声提示"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,2000,false,true,false,false,false)]
        public static string 超声所见 = "超声所见"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,200,false,true,false,false,false)]
        public static string 备注 = "备注"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建日期 = "创建日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,2,false,true,false,false,false)]
        public static string 是否停用 = "是否停用"; 

    }
}