﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_常用字典的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-11-07 04:02:11
 *   最后修改: 2018-11-07 04:02:11
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_常用字典,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_常用字典", "ID", true)]
    public sealed class tb_常用字典
    {
        public static string __TableName ="tb_常用字典";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string P_FUN_CODE = "P_FUN_CODE"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,80,false,true,false,false,false)]
        public static string P_FUN_DESC = "P_FUN_DESC"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string P_CODE = "P_CODE"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string P_DESC_CODE = "P_DESC_CODE"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,80,false,true,false,false,false)]
        public static string P_DESC = "P_DESC"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,80,false,true,false,false,false)]
        public static string P_BEIZHU = "P_BEIZHU"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string P_FLAG = "P_FLAG"; 

    }
}