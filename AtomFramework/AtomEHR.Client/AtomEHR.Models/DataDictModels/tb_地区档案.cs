﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;


/*==========================================
 *   程序说明: tb_地区档案的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2015/09/21 08:20:45
 *   最后修改: 2015/09/21 08:20:45
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Models
{
    ///<summary>
    /// ORM模型, 数据表:tb_地区档案,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_地区档案", "地区编码", true)]
    public sealed class tb_地区档案
    {
        public static string __TableName ="tb_地区档案";

        public static string __KeyName = "地区编码";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,18,false,true,true,false,false)]
        public static string 地区编码 = "地区编码"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 地区名称 = "地区名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 上级编码 = "上级编码"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,550,false,true,false,false,false)]
        public static string 备注 = "备注";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 200, false, true, false, false, false)]
        public static string 详细地区名称 = "详细地区名称";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 20, false, true, false, false, false)]
        public static string 地区人口 = "地区人口";

        [ORM_FieldAttribute(SqlDbType.NVarChar, 20, false, true, false, false, false)]
        public static string 是否城市 = "是否城市";

        [ORM_FieldAttribute(SqlDbType.VarChar, 20, false, true, false, false, false)]
        public static string 创建人 = "创建人";

        [ORM_FieldAttribute(SqlDbType.DateTime, 8, false, true, false, false, false)]
        public static string 创建时间 = "创建时间"; 

    }
}