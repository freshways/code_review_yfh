﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Interfaces;

namespace AtomEHR.DataDictionary
{
    /// <summary>
    /// FrmBusEdit业务界面
    /// </summary>
    public partial class FrmBusEdit : AtomEHR.Library.frmBaseBusinessForm
    {
        public FrmBusEdit()
        {
            InitializeComponent();
        }

        private void FrmBusEdit_Load(object sender, EventArgs e)
        {
            InitializeForm();
        }
        /// <summary>
        /// 初始化窗体
        /// </summary>
        protected override void InitializeForm()
        {
            _BLL = new bll档案摘要();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);
            _ActiveEditor = txt姓名;
            _DetailGroupControl = panel1;

            base.InitializeForm(); //这行代码放到初始化变量后最好

            frmGridCustomize.RegisterGrid(gvSummary);
            DevStyle.SetGridControlLayout(gcSummary, false);//表格设置   
            DevStyle.SetSummaryGridViewLayout(gvSummary);

            BindingSummaryNavigator(controlNavigatorSummary, gcSummary); //Summary导航条.
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);
        

            txt_DocDateTo.DateTime = DateTime.Today; //查询条件截止日期

            DataBinder.BindingLookupEditDataSource(txt创建人, DataDictCache.Cache.User, TUser.UserName, TUser.Account);//绑定弹窗控件

            _BLL.GetBusinessByKey("-", true);//加载一个空的业务对象.

            ShowSummaryPage(true); //一切初始化完毕后显示SummaryPage        
        }


        //public override void DoEdit(IButtonInfo sender)
        //{
        //    //base.DoEdit(sender);
        //    this._UpdateType = UpdateType.Modify;
        //    this.AssertFocusedRow();
        //    DataRow row = _SummaryView.GetDataRow(_SummaryView.FocusedRowHandle);
        //    if (_BLL.IsOwnerChange(row) == false)
        //    {
        //        Msg.Warning("您不能修改他人创建的单据！\r\n\r\n ***本单据已应用修改策略*** ");
        //        return;
        //    }
        //    string docNo = _SummaryView.GetDataRow(_SummaryView.FocusedRowHandle)[tb_档案摘要.__KeyName].ToString();

        //    FrmBusEditAdd frm = new FrmBusEditAdd(docNo, _UpdateType);
        //    frm.ShowDialog();
        //} 

        /// <summary>
        /// 重写保存
        /// </summary>
        /// <param name="sender">自定义按钮接口</param>
        public override void DoSave(IButtonInfo sender)// 保存数据
        {
            this.UpdateLastControl();//更新最后一个输入框的数据

            if (!ValidatingSummaryData(_BLL.CurrentBusiness.Tables[tb_档案摘要.__TableName])) return; //检查主表数据合法性
            //if (!ValidatingDetailData(_BLL.CurrentBusiness.Tables[tb_档案参数.__TableName])) return;

            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                if (_UpdateType == UpdateType.Add) _BLL.DataBindRow[tb_档案摘要.__KeyName] = result.DocNo; //更新单据号码
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                this.UpdateSummaryRow(_BLL.DataBindRow);//刷新表格内当前记录的缓存数据.
                this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                if (_UpdateType == UpdateType.Add) gvSummary.MoveLast(); //如是新增单据,移动到取后一条记录.
                base.DoSave(sender); //调用基类的方法. 此行代码应放较后位置.                    
                Msg.ShowInformation("保存成功!");
            }
            else
                Msg.Warning("保存失败!");
        }

        #region 主从表数据输入合法性检查

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData(DataTable summary)
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt姓名.Text)))
            {
                Msg.Warning("姓名不能为空!");
                txt姓名.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 检查子表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingDetailData(DataTable summary)
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt姓名.Text)))
            {
                Msg.Warning("姓名不能为空!");
                txt姓名.Focus();
                return false;
            }
            return true;
        }

        #endregion

        protected override void ButtonStateChanged(UpdateType currentState)// 按钮状态改变时触发的事件
        {
            //设置编辑明细页面的控件
            //this.SetDetailEditorsAccessable(xtraTabPage1, this.DataChanged);
            this.SetDetailEditorsAccessable(panel1, this.DataChanged);

            txt档案号.Enabled = false;
            txt创建人.Enabled = false;
            txt创建日期.Enabled = false;
            lblStatus.Text = this.UpdateTypeName;

            //this.SetEditorEnable(txt机构, false, true);//禁用客户名称
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            DataBinder.BindingTextEdit(txt档案号, dataSource, tb_档案摘要.档案号);
            DataBinder.BindingTextEdit(txt姓名, dataSource, tb_档案摘要.姓名);
            DataBinder.BindingTextEdit(txt性别, dataSource, tb_档案摘要.性别);
            DataBinder.BindingTextEdit(txt生日, dataSource, tb_档案摘要.生日);
            DataBinder.BindingTextEdit(txt手机号, dataSource, tb_档案摘要.手机号);
            DataBinder.BindingTextEdit(txt居住地址, dataSource, tb_档案摘要.居住地址);
            DataBinder.BindingTextEdit(txt机构, dataSource, tb_档案摘要.所属机构);
            DataBinder.BindingTextEdit(txt创建人, dataSource, tb_档案摘要.创建人);
            DataBinder.BindingTextEdit(txt创建日期, dataSource, tb_档案摘要.创建时间);
        }

        /// <summary>
        ///  绑定子表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingDetailEditor(DataSet dataSource)
        {
            //绑定明细. 
            DataTable Detail = dataSource.Tables[tb_档案参数.__TableName];
            if (Detail == null || Detail.Rows.Count == 0) { Detail.Rows.Add(); }
            DataBinder.BindingTextEdit(txt参数1, Detail, tb_档案参数.参数1);
            DataBinder.BindingTextEdit(txt参数2, Detail, tb_档案参数.参数2);
        }

        /// <summary>
        /// 绑定列表数据
        /// </summary>
        /// <returns></returns>
        protected override bool DoSearchSummary()
        {
            DataTable dt = (_BLL as bll档案摘要).GetSummaryByParam(txt_DocNoFrom.Text, txt_DocNoTo.Text, txt_DocDateFrom.DateTime, txt_DocDateTo.DateTime);
            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.                         
            return dt != null && dt.Rows.Count > 0;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {

        }        

    }
}
