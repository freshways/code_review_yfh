﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using AtomEHR.Common;
using AtomEHR.Interfaces;
using AtomEHR.Library;

namespace AtomEHR.DataDictionary
{
    //继承框架的子窗体
    public partial class frmTestChild : AtomEHR.Library.frmBaseChild
    {
        public frmTestChild()
        {
            InitializeComponent();
        }
        private IToolbarRegister _MdiToolbar = null;
        private void frmTestChild_Load(object sender, EventArgs e)
        {
            this.InitButtons();
            this._MdiToolbar = new DevBarRegister(this, this.bar1);
            this.RegisterMdiButtons();
        }
        /// <summary>
        /// 注册MDI主窗体的按钮
        /// </summary>
        public void RegisterMdiButtons()
        {
            this.MdiToolbar.RegisteButton(this.MdiButtons);
        }
        /// <summary>
        /// MDI主窗体的工具条按钮注册接口
        /// </summary>
        public IToolbarRegister MdiToolbar { get { return _MdiToolbar; } set { _MdiToolbar = value; } }

        /// <summary>
        /// 主窗体的按钮
        /// </summary>
        public IList MdiButtons
        {
            get
            {
                IList btns = new ArrayList();

                btns.Add(this.MdiToolbar.CreateButton("btnHelp", "帮助",
                     Globals.LoadBitmap("24_Help.png"), new Size(57, 28), this.DoHelp));
                btns.Add(this.MdiToolbar.CreateButton("btnClose", "关闭",
                    Globals.LoadBitmap("24_Exit.png"), new Size(57, 28), this.DoClose));

                return btns;
            }
        }

        public override void InitButtons()
        {
            base.InitButtons();
            
            bool salaryRight = (Loginer.CurrentUser.IsAdmin()) || ((ButtonAuthority.EX_01 & this.FormAuthorities) == ButtonAuthority.EX_01);

            if (salaryRight)
            {
                //创建“查看工资”按钮
                IButtonInfo btnViewSalary = this.ToolbarRegister.CreateButton("btnViewSalary", "写面值卡",
                    Globals.LoadBitmap("24_DesignReport.ico"), new Size(57, 28), this.DoViewSalary);

                //在Toolbar上显示
                this._buttons.AddRange(new IButtonInfo[] { btnViewSalary });
            }
        }

        public void DoViewSalary(AtomEHR.Interfaces.IButtonInfo sender)
        {
            Msg.ShowInformation("打开窗体");
        }

     
    }
}
