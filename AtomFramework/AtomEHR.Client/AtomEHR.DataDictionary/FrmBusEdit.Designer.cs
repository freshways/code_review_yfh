﻿namespace AtomEHR.DataDictionary
{
    partial class FrmBusEdit
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txt_DocDateTo = new DevExpress.XtraEditors.DateEdit();
            this.txt_DocNoTo = new DevExpress.XtraEditors.TextEdit();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txt_DocDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txt_DocNoFrom = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生日 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col手机号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col制单人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col制单日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txt参数2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txt参数1 = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt机构 = new AtomEHR.Library.UserControls.ucDT_CustomerEditor();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl档案号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txt手机号 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.txt生日 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt创建日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txt创建人 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt参数2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt参数1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手机号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生日.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生日.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tpSummary.Size = new System.Drawing.Size(1081, 492);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 33);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.pnlSummary.Size = new System.Drawing.Size(1087, 498);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tcBusiness.Size = new System.Drawing.Size(1087, 498);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Controls.Add(this.panel1);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tpDetail.Size = new System.Drawing.Size(1219, 987);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.gcNavigator.Size = new System.Drawing.Size(1087, 33);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(855, 5);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.controlNavigatorSummary.Size = new System.Drawing.Size(230, 23);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(598, 5);
            this.lblAboutInfo.Size = new System.Drawing.Size(257, 23);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.btnEmpty);
            this.gcFindGroup.Controls.Add(this.btnQuery);
            this.gcFindGroup.Controls.Add(this.labelControl23);
            this.gcFindGroup.Controls.Add(this.labelControl22);
            this.gcFindGroup.Controls.Add(this.txt_DocDateTo);
            this.gcFindGroup.Controls.Add(this.txt_DocNoTo);
            this.gcFindGroup.Controls.Add(this.pictureBox3);
            this.gcFindGroup.Controls.Add(this.txt_DocDateFrom);
            this.gcFindGroup.Controls.Add(this.labelControl8);
            this.gcFindGroup.Controls.Add(this.txt_DocNoFrom);
            this.gcFindGroup.Controls.Add(this.labelControl5);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(1081, 72);
            this.gcFindGroup.TabIndex = 20;
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(598, 10);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(71, 53);
            this.btnEmpty.TabIndex = 5;
            this.btnEmpty.Text = "清空(&E)";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(523, 9);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(67, 54);
            this.btnQuery.TabIndex = 4;
            this.btnQuery.Text = "查询(&S)";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(127, 41);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(30, 18);
            this.labelControl23.TabIndex = 13;
            this.labelControl23.Text = "至：";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(338, 41);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(30, 18);
            this.labelControl22.TabIndex = 12;
            this.labelControl22.Text = "至：";
            // 
            // txt_DocDateTo
            // 
            this.txt_DocDateTo.EditValue = null;
            this.txt_DocDateTo.Location = new System.Drawing.Point(374, 37);
            this.txt_DocDateTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_DocDateTo.Name = "txt_DocDateTo";
            this.txt_DocDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_DocDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt_DocDateTo.Size = new System.Drawing.Size(114, 24);
            this.txt_DocDateTo.TabIndex = 3;
            // 
            // txt_DocNoTo
            // 
            this.txt_DocNoTo.Location = new System.Drawing.Point(162, 37);
            this.txt_DocNoTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_DocNoTo.Name = "txt_DocNoTo";
            this.txt_DocNoTo.Size = new System.Drawing.Size(114, 24);
            this.txt_DocNoTo.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(6, 4);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // txt_DocDateFrom
            // 
            this.txt_DocDateFrom.EditValue = null;
            this.txt_DocDateFrom.Location = new System.Drawing.Point(374, 6);
            this.txt_DocDateFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_DocDateFrom.Name = "txt_DocDateFrom";
            this.txt_DocDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_DocDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt_DocDateFrom.Size = new System.Drawing.Size(114, 24);
            this.txt_DocDateFrom.TabIndex = 2;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(297, 10);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(75, 18);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "创建日期：";
            // 
            // txt_DocNoFrom
            // 
            this.txt_DocNoFrom.Location = new System.Drawing.Point(162, 6);
            this.txt_DocNoFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_DocNoFrom.Name = "txt_DocNoFrom";
            this.txt_DocNoFrom.Size = new System.Drawing.Size(114, 24);
            this.txt_DocNoFrom.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(86, 9);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 18);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "档案号码：";
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcSummary.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSummary.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.Location = new System.Drawing.Point(0, 72);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit11});
            this.gcSummary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gcSummary.Size = new System.Drawing.Size(1081, 420);
            this.gcSummary.TabIndex = 21;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col档案号,
            this.col姓名,
            this.col性别,
            this.col生日,
            this.col手机号,
            this.col居住地址,
            this.col所属机构,
            this.col制单人,
            this.col制单日期});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowFooter = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // col档案号
            // 
            this.col档案号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col档案号.AppearanceHeader.Options.UseFont = true;
            this.col档案号.Caption = "档案号";
            this.col档案号.FieldName = "档案号";
            this.col档案号.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.col档案号.Name = "col档案号";
            this.col档案号.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.col档案号.Visible = true;
            this.col档案号.VisibleIndex = 0;
            this.col档案号.Width = 150;
            // 
            // col姓名
            // 
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            this.col姓名.Width = 112;
            // 
            // col性别
            // 
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            this.col性别.Width = 80;
            // 
            // col生日
            // 
            this.col生日.Caption = "生日";
            this.col生日.FieldName = "生日";
            this.col生日.Name = "col生日";
            this.col生日.Visible = true;
            this.col生日.VisibleIndex = 3;
            this.col生日.Width = 100;
            // 
            // col手机号
            // 
            this.col手机号.Caption = "手机号";
            this.col手机号.FieldName = "手机号";
            this.col手机号.Name = "col手机号";
            this.col手机号.Visible = true;
            this.col手机号.VisibleIndex = 4;
            this.col手机号.Width = 120;
            // 
            // col居住地址
            // 
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 5;
            this.col居住地址.Width = 230;
            // 
            // col所属机构
            // 
            this.col所属机构.Caption = "所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 6;
            this.col所属机构.Width = 79;
            // 
            // col制单人
            // 
            this.col制单人.Caption = "制单人";
            this.col制单人.FieldName = "制单人";
            this.col制单人.Name = "col制单人";
            this.col制单人.Visible = true;
            this.col制单人.VisibleIndex = 7;
            this.col制单人.Width = 89;
            // 
            // col制单日期
            // 
            this.col制单日期.Caption = "制单日期";
            this.col制单日期.FieldName = "制单日期";
            this.col制单日期.Name = "col制单日期";
            this.col制单日期.Visible = true;
            this.col制单日期.VisibleIndex = 8;
            this.col制单日期.Width = 93;
            // 
            // repositoryItemLookUpEdit11
            // 
            this.repositoryItemLookUpEdit11.AutoHeight = false;
            this.repositoryItemLookUpEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit11.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CustomerNativeName", "CustomerNativeName")});
            this.repositoryItemLookUpEdit11.Name = "repositoryItemLookUpEdit11";
            this.repositoryItemLookUpEdit11.NullText = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.xtraTabControl1);
            this.panel1.Controls.Add(this.txt姓名);
            this.panel1.Controls.Add(this.txt机构);
            this.panel1.Controls.Add(this.lblStatus);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.labelControl13);
            this.panel1.Controls.Add(this.txt档案号);
            this.panel1.Controls.Add(this.labelControl档案号);
            this.panel1.Controls.Add(this.labelControl31);
            this.panel1.Controls.Add(this.txt手机号);
            this.panel1.Controls.Add(this.txt居住地址);
            this.panel1.Controls.Add(this.labelControl7);
            this.panel1.Controls.Add(this.labelControl24);
            this.panel1.Controls.Add(this.labelControl10);
            this.panel1.Controls.Add(this.labelControl9);
            this.panel1.Controls.Add(this.labelControl4);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.labelControl29);
            this.panel1.Controls.Add(this.txt生日);
            this.panel1.Controls.Add(this.labelControl6);
            this.panel1.Controls.Add(this.txt创建日期);
            this.panel1.Controls.Add(this.labelControl11);
            this.panel1.Controls.Add(this.txt创建人);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.labelControl3);
            this.panel1.Controls.Add(this.txt性别);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1219, 987);
            this.panel1.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(16, 256);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(878, 192);
            this.xtraTabControl1.TabIndex = 386;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelControl15);
            this.xtraTabPage1.Controls.Add(this.txt参数2);
            this.xtraTabPage1.Controls.Add(this.labelControl14);
            this.xtraTabPage1.Controls.Add(this.txt参数1);
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(872, 159);
            this.xtraTabPage1.Text = "其他信息";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(259, 32);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(53, 18);
            this.labelControl15.TabIndex = 230;
            this.labelControl15.Text = "参数2：";
            // 
            // txt参数2
            // 
            this.txt参数2.EditValue = "";
            this.txt参数2.Location = new System.Drawing.Point(334, 31);
            this.txt参数2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt参数2.Name = "txt参数2";
            this.txt参数2.Size = new System.Drawing.Size(141, 24);
            this.txt参数2.TabIndex = 3;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(15, 32);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(53, 18);
            this.labelControl14.TabIndex = 230;
            this.labelControl14.Text = "参数1：";
            // 
            // txt参数1
            // 
            this.txt参数1.EditValue = "";
            this.txt参数1.Location = new System.Drawing.Point(74, 31);
            this.txt参数1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt参数1.Name = "txt参数1";
            this.txt参数1.Size = new System.Drawing.Size(141, 24);
            this.txt参数1.TabIndex = 3;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(872, 159);
            this.xtraTabPage2.Text = "其他信息";
            // 
            // txt姓名
            // 
            this.txt姓名.EditValue = "";
            this.txt姓名.Location = new System.Drawing.Point(395, 77);
            this.txt姓名.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(195, 24);
            this.txt姓名.TabIndex = 385;
            // 
            // txt机构
            // 
            this.txt机构.CustomerAttributeCodes = ",SPL,";
            this.txt机构.CustomerNameControl = null;
            this.txt机构.CustomerNameField = "NativeName";
            this.txt机构.Location = new System.Drawing.Point(91, 216);
            this.txt机构.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt机构.Name = "txt机构";
            this.txt机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt机构.Size = new System.Drawing.Size(195, 24);
            this.txt机构.TabIndex = 384;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(489, 23);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(102, 22);
            this.lblStatus.TabIndex = 383;
            this.lblStatus.Text = "(操作状态)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(414, 24);
            this.label1.TabIndex = 382;
            this.label1.Text = "个人基本信息(Electronic Health Records)";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(5, 4);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 381;
            this.pictureBox2.TabStop = false;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl13.Location = new System.Drawing.Point(593, 81);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(8, 18);
            this.labelControl13.TabIndex = 288;
            this.labelControl13.Text = "*";
            // 
            // txt档案号
            // 
            this.txt档案号.EditValue = "371321";
            this.txt档案号.Location = new System.Drawing.Point(91, 77);
            this.txt档案号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(195, 24);
            this.txt档案号.TabIndex = 0;
            // 
            // labelControl档案号
            // 
            this.labelControl档案号.Location = new System.Drawing.Point(26, 81);
            this.labelControl档案号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl档案号.Name = "labelControl档案号";
            this.labelControl档案号.Size = new System.Drawing.Size(60, 18);
            this.labelControl档案号.TabIndex = 287;
            this.labelControl档案号.Text = "档案号：";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl31.Location = new System.Drawing.Point(290, 81);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(8, 18);
            this.labelControl31.TabIndex = 1;
            this.labelControl31.Text = "*";
            // 
            // txt手机号
            // 
            this.txt手机号.EditValue = "";
            this.txt手机号.Location = new System.Drawing.Point(91, 170);
            this.txt手机号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt手机号.Name = "txt手机号";
            this.txt手机号.Size = new System.Drawing.Size(195, 24);
            this.txt手机号.TabIndex = 3;
            // 
            // txt居住地址
            // 
            this.txt居住地址.EditValue = "";
            this.txt居住地址.Location = new System.Drawing.Point(395, 170);
            this.txt居住地址.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Size = new System.Drawing.Size(443, 24);
            this.txt居住地址.TabIndex = 3;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(619, 81);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(45, 18);
            this.labelControl7.TabIndex = 267;
            this.labelControl7.Text = "性别：";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(321, 81);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(45, 18);
            this.labelControl24.TabIndex = 239;
            this.labelControl24.Text = "姓名：";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(619, 125);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(53, 18);
            this.labelControl10.TabIndex = 257;
            this.labelControl10.Text = "缺项：0";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(801, 127);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(99, 18);
            this.labelControl9.TabIndex = 257;
            this.labelControl9.Text = "完整度：100%";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(321, 127);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(105, 18);
            this.labelControl4.TabIndex = 257;
            this.labelControl4.Text = "档案状态：活动";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 126);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(75, 18);
            this.labelControl1.TabIndex = 257;
            this.labelControl1.Text = "出生日期：";
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(619, 220);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(75, 18);
            this.labelControl29.TabIndex = 257;
            this.labelControl29.Text = "创建日期：";
            // 
            // txt生日
            // 
            this.txt生日.EditValue = new System.DateTime(2010, 10, 29, 18, 35, 50, 843);
            this.txt生日.Location = new System.Drawing.Point(91, 123);
            this.txt生日.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt生日.Name = "txt生日";
            this.txt生日.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt生日.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt生日.Size = new System.Drawing.Size(195, 24);
            this.txt生日.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(13, 216);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 18);
            this.labelControl6.TabIndex = 265;
            this.labelControl6.Text = "所属机构：";
            // 
            // txt创建日期
            // 
            this.txt创建日期.EditValue = new System.DateTime(2010, 10, 29, 18, 35, 50, 843);
            this.txt创建日期.Location = new System.Drawing.Point(698, 216);
            this.txt创建日期.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt创建日期.Name = "txt创建日期";
            this.txt创建日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt创建日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt创建日期.Size = new System.Drawing.Size(195, 24);
            this.txt创建日期.TabIndex = 5;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(26, 171);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 18);
            this.labelControl11.TabIndex = 230;
            this.labelControl11.Text = "手机号：";
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(395, 217);
            this.txt创建人.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt创建人.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Account", 80, "用户帐号"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName", 100, "用户名")});
            this.txt创建人.Properties.NullText = "";
            this.txt创建人.Properties.PopupWidth = 180;
            this.txt创建人.Size = new System.Drawing.Size(195, 24);
            this.txt创建人.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(321, 171);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 18);
            this.labelControl2.TabIndex = 230;
            this.labelControl2.Text = "居住地址：";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(321, 220);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 231;
            this.labelControl3.Text = "创建人：";
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(698, 77);
            this.txt性别.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt性别.Properties.ImmediatePopup = true;
            this.txt性别.Properties.Items.AddRange(new object[] {
            "男",
            "女",
            "   "});
            this.txt性别.Properties.PopupFormSize = new System.Drawing.Size(480, 0);
            this.txt性别.Properties.PopupSizeable = true;
            this.txt性别.Size = new System.Drawing.Size(195, 24);
            this.txt性别.TabIndex = 11;
            // 
            // FrmBusEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.ClientSize = new System.Drawing.Size(1087, 531);
            this.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.Name = "FrmBusEdit";
            this.Load += new System.EventHandler(this.FrmBusEdit_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            this.gcFindGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_DocNoFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt参数2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt参数1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手机号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生日.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生日.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.DateEdit txt_DocDateTo;
        private DevExpress.XtraEditors.TextEdit txt_DocNoTo;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.DateEdit txt_DocDateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txt_DocNoFrom;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn col档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col生日;
        private DevExpress.XtraGrid.Columns.GridColumn col手机号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col制单日期;
        private DevExpress.XtraGrid.Columns.GridColumn col制单人;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit11;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private AtomEHR.Library.UserControls.ucDT_CustomerEditor txt机构;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.LabelControl labelControl档案号;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.DateEdit txt创建日期;
        private DevExpress.XtraEditors.LookUpEdit txt创建人;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txt参数2;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txt参数1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.TextEdit txt手机号;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit txt生日;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit txt性别;
    }
}
