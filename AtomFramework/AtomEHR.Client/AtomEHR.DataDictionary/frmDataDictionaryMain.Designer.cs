namespace AtomEHR.DataDictionary
{
    partial class frmDataDictionaryMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataDictionaryMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuMainDataDict = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCommonDataDict = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTestChild = new System.Windows.Forms.ToolStripMenuItem();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCommonDataDict = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilReports
            // 
            this.ilReports.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilReports.ImageStream")));
            this.ilReports.Images.SetKeyName(0, "16_ArrayWhite.bmp");
            // 
            // pnlContainer
            // 
            this.pnlContainer.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnlContainer.Appearance.Options.UseBackColor = true;
            this.pnlContainer.Controls.Add(this.panelControl3);
            this.pnlContainer.Controls.Add(this.btnCommonDataDict);
            this.pnlContainer.Size = new System.Drawing.Size(811, 569);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMainDataDict});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(811, 25);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuMainDataDict
            // 
            this.menuMainDataDict.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCommonDataDict,
            this.menuItemTestChild});
            this.menuMainDataDict.Image = global::AtomEHR.DataDictionary.Properties.Resources.anchor;
            this.menuMainDataDict.Name = "menuMainDataDict";
            this.menuMainDataDict.Size = new System.Drawing.Size(84, 21);
            this.menuMainDataDict.Text = "数据字典";
            // 
            // menuCommonDataDict
            // 
            this.menuCommonDataDict.Image = global::AtomEHR.DataDictionary.Properties.Resources.application;
            this.menuCommonDataDict.Name = "menuCommonDataDict";
            this.menuCommonDataDict.Size = new System.Drawing.Size(172, 22);
            this.menuCommonDataDict.Text = "公共数据字典定义";
            this.menuCommonDataDict.Click += new System.EventHandler(this.menuCommonDataDict_Click);
            // 
            // menuItemTestChild
            // 
            this.menuItemTestChild.Name = "menuItemTestChild";
            this.menuItemTestChild.Size = new System.Drawing.Size(172, 22);
            this.menuItemTestChild.Text = "测试窗体";
            this.menuItemTestChild.Click += new System.EventHandler(this.menuItemTestChild_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.label4);
            this.panelControl3.Controls.Add(this.label5);
            this.panelControl3.Location = new System.Drawing.Point(163, 67);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(254, 73);
            this.panelControl3.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 28);
            this.label4.TabIndex = 27;
            this.label4.Text = "公共字典数据定义。以类型分类，\r\n定义多种基础资料。";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.OrangeRed;
            this.label5.Location = new System.Drawing.Point(3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "公共字典";
            // 
            // btnCommonDataDict
            // 
            this.btnCommonDataDict.Enabled = false;
            this.btnCommonDataDict.Image = global::AtomEHR.DataDictionary.Properties.Resources._48_GiftExchange;
            this.btnCommonDataDict.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnCommonDataDict.Location = new System.Drawing.Point(61, 56);
            this.btnCommonDataDict.Name = "btnCommonDataDict";
            this.btnCommonDataDict.Size = new System.Drawing.Size(84, 84);
            this.btnCommonDataDict.TabIndex = 49;
            this.btnCommonDataDict.Text = "公共字典";
            this.btnCommonDataDict.Click += new System.EventHandler(this.menuCommonDataDict_Click);
            // 
            // frmDataDictionaryMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(811, 569);
            this.Controls.Add(this.menuStrip1);
            this.Name = "frmDataDictionaryMain";
            this.Text = "数据字典定义";
            this.Controls.SetChildIndex(this.pnlContainer, 0);
            this.Controls.SetChildIndex(this.menuStrip1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuMainDataDict;
        private DevExpress.XtraEditors.SimpleButton btnCommonDataDict;
        private System.Windows.Forms.ToolStripMenuItem menuCommonDataDict;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem menuItemTestChild;
    }
}
