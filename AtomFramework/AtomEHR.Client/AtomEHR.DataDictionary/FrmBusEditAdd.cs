﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.DataDictionary
{
    public partial class FrmBusEditAdd : frmBase
    {
        public FrmBusEditAdd()
        {
            InitializeComponent();
        }
        DataSet _ds;
        UpdateType _UpdateType = UpdateType.None;
        public FrmBusEditAdd(string docNo, UpdateType currentType)
        {
            InitializeComponent();

            _ds = _Bll.GetBusinessByKey(docNo, true);
            _UpdateType = currentType;
            DoBindingSummaryEditor(_ds.Tables[0]);
        }

        bll档案摘要 _Bll = new bll档案摘要();

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        void DoBindingSummaryEditor(DataTable dataSource)
        {

            if (dataSource == null) return;

            DataBinder.BindingTextEdit(textEdit1, dataSource, tb_档案摘要.姓名);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet ds = _Bll.CreateSaveData(_ds, _UpdateType);
            SaveResult result = _Bll.Save(ds);
            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                Msg.AskQuestion("成功");
            }
        }
    }
}
