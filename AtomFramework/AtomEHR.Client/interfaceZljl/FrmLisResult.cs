﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace interfaceZljl
{
    public partial class FrmLisResult : Form
    {
        private string str姓名;
        private string str身份证号;
        public FrmLisResult()
        {
            InitializeComponent();
        }

        public FrmLisResult(string xm, string sfzh)
        {
            InitializeComponent();

            str姓名 = xm;
            str身份证号 = sfzh;
        }

        private void FrmLisResult_Load(object sender, EventArgs e)
        {
            this.Text = str姓名 + "的近期化验结果：";

            try
            {
                this.gridControlMain.DataSource = BindLisResult(str身份证号);
                this.gridViewMain.BestFitColumns();
                //this.splitContainerControl1.Panel1.Width = this.gridControlMain.Width;
            }
            catch(Exception ex)
            {
                MessageBox.Show("显示化验结果时出现异常.异常信息："+ex.Message);
            }
        }

        private DataTable BindLisResult(string sfzh)
        {

            DataTable dtList = null;
            try
            {
                string conStr = Config.lisStr;
                string sql = "Pro_GetLisResult";

                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@sfzh", sfzh);

                dtList = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.StoredProcedure, sql, sqlparams).Tables[0];
            }
            catch (Exception ex)
            {
                dtList = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return dtList;
        }

        private DataTable BindLisResultDetail(string yyid, string fjyid)
        {
            DataTable dtDetail = null;
            try
            {
                string conStr = Config.lisStr;
                string sql = "Pro_GetLisResultDetail";

                SqlParameter[] sqlparams = new SqlParameter[2];
                sqlparams[0] = new SqlParameter("@deptid", yyid);
                sqlparams[1] = new SqlParameter("@fjy_id", fjyid);

                dtDetail = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.StoredProcedure, sql, sqlparams).Tables[0];
            }
            catch (Exception ex)
            {
                dtDetail = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return dtDetail;
        }

        private void gridViewMain_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            DataRow row = this.gridViewMain.GetFocusedDataRow();

            if(row==null)
            { return; }

            string fjyid = row["fjyid"].ToString();
            string[] strs = fjyid.Split('_');

            if(strs.Length==2)
            {
                DataTable dtdetail = BindLisResultDetail(strs[0], strs[1]);

                this.gridControlDetail.DataSource = dtdetail;
            }
        }

        private void gridViewMain_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0)
                {
                    e.Info.DisplayText = (e.RowHandle + 1).ToString();
                }
            }
        }
    }
}
