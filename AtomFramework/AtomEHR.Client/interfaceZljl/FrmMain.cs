﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;

namespace interfaceZljl
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void btn医院住院_Click(object sender, EventArgs e)
        {
            ////医院住院病人信息
            //Report.XtraReportCybrPic rep = new Report.XtraReportCybrPic();
            //ReportPrintTool tool = new ReportPrintTool(rep);
            //tool.ShowPreviewDialog();
        }

        private void btn医院门诊_Click(object sender, EventArgs e)
        {
            //Report.XtraReportMenZhen rep = new Report.XtraReportMenZhen();
            //ReportPrintTool tool = new ReportPrintTool(rep);
            //tool.ShowPreviewDialog();
        }

        private void btn卫生室门诊_Click(object sender, EventArgs e)
        {
            //Report.Frm卫生室门诊 frm = new Report.Frm卫生室门诊();
            //frm.Show();
        }

        private void btn就医信息_Click(object sender, EventArgs e)
        {
            Report.XtraReport信息检索 rep = new Report.XtraReport信息检索();
            ReportPrintTool tool = new ReportPrintTool(rep);
            tool.ShowPreviewDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report.FrmZljl frm = new Report.FrmZljl();

            //设置报表
            //frm.SetReport("372827193606234040", 10);
            //frm.ShowDialog();

            frm.SetReport("372827196212292131", 10);
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Report.XtraReport信息检索 rep = new Report.XtraReport信息检索();
            ReportPrintTool tool = new ReportPrintTool(rep);

            tool.AutoShowParametersPanel = false;
            //rep.sfzh.Visible = false;
            //rep.jyCount.Visible = false;
            rep.sfzh.Value = "372827193606234040";
            rep.jyCount.Value = 10;
            //rep.PrintingSystem.ExecCommand(PrintingSystemCommand.SubmitParameters);

            rep.sfzh.Visible = false;
            rep.jyCount.Visible = false;

            rep.CreateDocument();
            //rep.PrintingSystem.ExecCommand(PrintingSystemCommand.SubmitParameters);
            

            tool.ShowPreviewDialog();
        }

        
    }
}
