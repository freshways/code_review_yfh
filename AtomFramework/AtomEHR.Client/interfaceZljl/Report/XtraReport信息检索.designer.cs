﻿namespace interfaceZljl.Report
{
    partial class XtraReport信息检索
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell序号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医院 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell病人姓名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell类型 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell疾病名称 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell就医时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell详细信息 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelError = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel民族 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel家庭住址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel身份证号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel出生日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.sfzh = new DevExpress.XtraReports.Parameters.Parameter();
            this.jyCount = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(37.08331F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(767.0001F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell序号,
            this.xrTableCell医院,
            this.xrTableCell病人姓名,
            this.xrTableCell类型,
            this.xrTableCell疾病名称,
            this.xrTableCell就医时间,
            this.xrTableCell详细信息});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell序号
            // 
            this.xrTableCell序号.Name = "xrTableCell序号";
            this.xrTableCell序号.StylePriority.UseTextAlignment = false;
            this.xrTableCell序号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell序号.Weight = 0.39999996666077609D;
            this.xrTableCell序号.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell序号_BeforePrint);
            // 
            // xrTableCell医院
            // 
            this.xrTableCell医院.Name = "xrTableCell医院";
            this.xrTableCell医院.Weight = 1.5799998727605504D;
            // 
            // xrTableCell病人姓名
            // 
            this.xrTableCell病人姓名.Name = "xrTableCell病人姓名";
            this.xrTableCell病人姓名.Weight = 0.5241663098491004D;
            // 
            // xrTableCell类型
            // 
            this.xrTableCell类型.Name = "xrTableCell类型";
            this.xrTableCell类型.Weight = 0.79166773814724245D;
            // 
            // xrTableCell疾病名称
            // 
            this.xrTableCell疾病名称.Name = "xrTableCell疾病名称";
            this.xrTableCell疾病名称.Weight = 1.7395818839142576D;
            // 
            // xrTableCell就医时间
            // 
            this.xrTableCell就医时间.Name = "xrTableCell就医时间";
            this.xrTableCell就医时间.Weight = 1.2964592286680727D;
            // 
            // xrTableCell详细信息
            // 
            this.xrTableCell详细信息.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell详细信息.ForeColor = System.Drawing.Color.Blue;
            this.xrTableCell详细信息.Name = "xrTableCell详细信息";
            this.xrTableCell详细信息.StylePriority.UseFont = false;
            this.xrTableCell详细信息.StylePriority.UseForeColor = false;
            this.xrTableCell详细信息.Text = "查看详细信息";
            this.xrTableCell详细信息.Weight = 1.3381256103515624D;
            this.xrTableCell详细信息.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrTableCell详细信息_PreviewClick);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelError,
            this.xrLabel民族,
            this.xrLabel3,
            this.xrTable1,
            this.xrLabel家庭住址,
            this.xrLabel10,
            this.xrLabel身份证号,
            this.xrLabel8,
            this.xrLabel出生日期,
            this.xrLabel6,
            this.xrLabel性别,
            this.xrLabel4,
            this.xrLabel姓名,
            this.xrLabel2,
            this.xrPictureBox1,
            this.xrLabel1});
            this.ReportHeader.HeightF = 230.2083F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelError
            // 
            this.xrLabelError.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabelError.LocationFloat = new DevExpress.Utils.PointFloat(578.125F, 74.99994F);
            this.xrLabelError.Name = "xrLabelError";
            this.xrLabelError.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelError.SizeF = new System.Drawing.SizeF(190.6249F, 115F);
            this.xrLabelError.StylePriority.UseFont = false;
            this.xrLabelError.StylePriority.UseTextAlignment = false;
            this.xrLabelError.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabelError.Visible = false;
            // 
            // xrLabel民族
            // 
            this.xrLabel民族.LocationFloat = new DevExpress.Utils.PointFloat(366.6667F, 97.99989F);
            this.xrLabel民族.Name = "xrLabel民族";
            this.xrLabel民族.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel民族.SizeF = new System.Drawing.SizeF(195.8333F, 23F);
            this.xrLabel民族.StylePriority.UseTextAlignment = false;
            this.xrLabel民族.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(292.7083F, 97.99989F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(73.95834F, 23F);
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "民      族：";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(37.08344F, 205.2083F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(767F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell1,
            this.xrTableCell6,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "序号";
            this.xrTableCell11.Weight = 0.1564537207492793D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "医院(卫生室)名称";
            this.xrTableCell1.Weight = 0.61799218228774078D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "姓名";
            this.xrTableCell6.Weight = 0.20501952189976422D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "就诊类型";
            this.xrTableCell2.Weight = 0.30964818305422154D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "疾病名称";
            this.xrTableCell3.Weight = 0.68041039259213054D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "就诊时间/入院时间";
            this.xrTableCell4.Weight = 0.50708932889020741D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "查看详细信息";
            this.xrTableCell5.Weight = 0.52338667052665622D;
            // 
            // xrLabel家庭住址
            // 
            this.xrLabel家庭住址.LocationFloat = new DevExpress.Utils.PointFloat(236.4583F, 166.9999F);
            this.xrLabel家庭住址.Name = "xrLabel家庭住址";
            this.xrLabel家庭住址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel家庭住址.SizeF = new System.Drawing.SizeF(326.0417F, 23.00001F);
            this.xrLabel家庭住址.StylePriority.UseTextAlignment = false;
            this.xrLabel家庭住址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(155.2083F, 120.9999F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(81.24994F, 23F);
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "出生日期：";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel身份证号
            // 
            this.xrLabel身份证号.LocationFloat = new DevExpress.Utils.PointFloat(236.4583F, 143.9999F);
            this.xrLabel身份证号.Name = "xrLabel身份证号";
            this.xrLabel身份证号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel身份证号.SizeF = new System.Drawing.SizeF(326.0417F, 23F);
            this.xrLabel身份证号.StylePriority.UseTextAlignment = false;
            this.xrLabel身份证号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(155.2082F, 166.9999F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(81.24998F, 23F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "家庭住址：";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel出生日期
            // 
            this.xrLabel出生日期.LocationFloat = new DevExpress.Utils.PointFloat(236.4583F, 120.9999F);
            this.xrLabel出生日期.Name = "xrLabel出生日期";
            this.xrLabel出生日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel出生日期.SizeF = new System.Drawing.SizeF(326.0417F, 23F);
            this.xrLabel出生日期.StylePriority.UseTextAlignment = false;
            this.xrLabel出生日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(155.2082F, 143.9999F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(81.24999F, 23F);
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "身份证号：";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(236.4583F, 97.99995F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(56.25003F, 23F);
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(155.2082F, 97.99995F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(81.25009F, 23F);
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "性         别：";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(236.4583F, 74.99994F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(104.1667F, 23F);
            this.xrLabel姓名.StylePriority.UseFont = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(155.2083F, 74.99994F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(81.24996F, 23F);
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓         名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(37.08331F, 74.99994F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(107.7083F, 115.0001F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(287.5F, 33.33332F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(207.2917F, 32.75001F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "诊疗记录";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 46.95832F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "第 {0} 页";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(328.125F, 10.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // sfzh
            // 
            this.sfzh.Description = "身份证号：";
            this.sfzh.Name = "sfzh";
            // 
            // jyCount
            // 
            this.jyCount.Description = "就医记录数量：";
            this.jyCount.Name = "jyCount";
            this.jyCount.Type = typeof(short);
            this.jyCount.ValueInfo = "10";
            // 
            // XtraReport信息检索
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.sfzh,
            this.jyCount});
            this.Version = "13.2";
            this.ParametersRequestSubmit += new System.EventHandler<DevExpress.XtraReports.Parameters.ParametersRequestEventArgs>(this.XtraReport信息检索_ParametersRequestSubmit);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel身份证号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel出生日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel家庭住址;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医院;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell类型;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell疾病名称;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell就医时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell详细信息;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel民族;
        private DevExpress.XtraReports.UI.XRLabel xrLabelError;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell序号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell病人姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        public DevExpress.XtraReports.Parameters.Parameter sfzh;
        public DevExpress.XtraReports.Parameters.Parameter jyCount;
    }
}
