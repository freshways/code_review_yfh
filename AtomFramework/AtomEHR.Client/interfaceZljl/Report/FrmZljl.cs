﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace interfaceZljl.Report
{
    public partial class FrmZljl : Form
    {
        //public string cardid = "";
        //public int count = 10;
        public FrmZljl()
        {
            InitializeComponent();
        }

        public void SetReport(string strcardid, int count)
        {
            if (strcardid == null  || !(Regex.IsMatch(strcardid, @"(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)"))
                || count < 0)
            {
                MessageBox.Show("请检查您输入的身份证号和需要查询的记录数量","提示",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                return;
            }

            DataTable dtCardInfo = Get身份信息(strcardid);
            DataTable dtZljl = Get诊疗记录(strcardid, count.ToString());

            XtraReport信息检索 rep = new XtraReport信息检索(strcardid, dtCardInfo, dtZljl);
            documentViewer1.DocumentSource = rep;
            rep.sfzh.Visible = false;
            rep.jyCount.Visible = false;
            rep.CreateDocument();
        }

        private DataTable Get身份信息(string cardid)
        {
            DataTable dtCardInfo = null;
            try
            {
                //根据ID查询身份证信息，从PubDB数据库中取身份证信息（PubDB数据库中的身份证信息是最全的）
                string conStr = GetPubDbConStr();
                string sql = @"SELECT TOP 1 [name],[sex],[nation],[birthday],[address],[cardId],[picture]
                                        FROM [pubDB].[dbo].[pubSfzhInfo]
                                        where cardId = @cardid";

                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@cardid", cardid);

                dtCardInfo = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql, sqlparams).Tables[0];
            }
            catch (Exception ex)
            {
                dtCardInfo = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return dtCardInfo;
        }

        private DataTable Get诊疗记录(string cardid, string count)
        {
            DataTable dtDetaiInfo = null;
            try
            {
                string conStr = GetHisConStr();
                //string sql = "uSp就医信息检索";
                string sql = "uSp就医信息检索Contain2709";

                SqlParameter[] sqlparams = new SqlParameter[2];
                sqlparams[0] = new SqlParameter("@cardid", cardid);
                sqlparams[1] = new SqlParameter("@topnum", count);

                dtDetaiInfo = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.StoredProcedure, sql, sqlparams).Tables[0];
            }
            catch (Exception ex)
            {
                dtDetaiInfo = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return dtDetaiInfo;
        }

        private string GetPubDbConStr()
        {
            string conStr = Config.PubDBConStr;
            return conStr;
        }

        private string GetHisConStr()
        {
            string conStr = Config.hisStr;
            return conStr;
        }

    }
}
