﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using DevExpress.XtraPrinting;
using System.Windows.Forms;

namespace interfaceZljl.Report.NewDetailReport
{
    public partial class XtraReport临时医嘱 : DevExpress.XtraReports.UI.XtraReport
    {
        //ClassPerson住院病人 _person;
        //public XtraReport临时医嘱(ClassPerson住院病人 person)
        //{
        //    InitializeComponent();
        //    _person = person;
        //    xrLabel医院名称.Text = TYK.zdInfo.sDwmc;
        //    dt临时医嘱YsImageTableAdapter1.FillYsImage(emr1.dt临时医嘱YsImage, Convert.ToInt32(person.sZYID));
        //    string s执行时间 = "0";
        //    for (int i = 0; i < emr1.dt临时医嘱YsImage.Rows.Count - 1; i++)
        //    {
        //        if (emr1.dt临时医嘱YsImage.Rows[i]["执行时间"].ToString() == s执行时间)
        //        {
        //            emr1.dt临时医嘱YsImage.Rows[i]["执行时间"] = "..";
        //        }
        //        else
        //        {
        //            if (s执行时间 != "0")
        //            {
        //                emr1.dt临时医嘱YsImage.Rows[i - 1]["执行时间"] = s执行时间;//连续相同的执行时间最后一行显示执行时间
        //            }
        //            s执行时间 = emr1.dt临时医嘱YsImage.Rows[i]["执行时间"].ToString();
        //        }
        //    }
        //    //DataTable dt已执行医嘱 = tykClass.Db.autoDb.GetNewDataTable(emr1.dt临时医嘱YsImage, "真正执行时间 is not null");
        //    //DataTable dt执行时间点 = dt已执行医嘱.DefaultView.ToTable(true, new string[] { "真正执行时间" });
        //    //foreach (DataRow row时间点 in dt执行时间点.Rows)
        //    //{
        //    //    string s时间点 = row时间点[0].ToString();
        //    //    string sMaxID = emr1.dt临时医嘱YsImage.Compute("max(ID)", "真正执行时间='" + s时间点 + "'").ToString();
        //    //    string sMaxID = emr1.dt临时医嘱YsImage.Compute("max(ID)", "真正执行时间='" + s时间点 + "'").ToString();
        //    //    DataRow[] rows同时间点 = emr1.dt临时医嘱YsImage.Select("");
        //    //}


        //    xrLabel姓名.Text = "姓名:" + person.S姓名;
        //    xrLabel科别.Text = "科别:" + person.S科室名称;
        //    xrLabel床号.Text = "床号:" + person.S床位名称;
        //    xrLabel病历号.Text = "病历号:" + person.s住院号码;
        //    //string SQL =
        //    //    "update ys住院医嘱 set [已打印标志]=1 where 医嘱类型='临时医嘱' and ZYID=" + person.sZYID;
        //    //tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
        //}

        public XtraReport临时医嘱(string str医院名称, DataTable dt病人信息, DataTable dt临时医嘱)
        {
            InitializeComponent();

            if (str医院名称 != null)
            {
                xrLabel医院名称.Text = str医院名称;
            }
            if (dt病人信息 != null && dt病人信息.Rows.Count > 0)
            {
                xrLabel姓名.Text = "姓名:" + dt病人信息.Rows[0]["病人姓名"].ToString();
                xrLabel科别.Text = "科别:" + dt病人信息.Rows[0]["科室"].ToString();
                xrLabel床号.Text = "床号:" + dt病人信息.Rows[0]["病床"].ToString();
                xrLabel病历号.Text = "病历号:" + dt病人信息.Rows[0]["住院号码"].ToString();
            }

            if (dt临时医嘱 != null && dt临时医嘱.Rows.Count > 0)
            {

                string s执行时间 = "0";
                for (int i = 0; i < dt临时医嘱.Rows.Count - 1; i++)
                {
                    if (dt临时医嘱.Rows[i]["执行时间"].ToString() == s执行时间)
                    {
                        dt临时医嘱.Rows[i]["执行时间"] = "..";
                    }
                    else
                    {
                        if (s执行时间 != "0")
                        {
                            dt临时医嘱.Rows[i - 1]["执行时间"] = s执行时间;//连续相同的执行时间最后一行显示执行时间
                        }
                        s执行时间 = dt临时医嘱.Rows[i]["执行时间"].ToString();
                    }
                }
                this.DataSource = null;
                this.DataMember = null;
                this.DataAdapter = null;
                this.DataSource = dt临时医嘱;
            }
        }

        private void XtraReport临时医嘱_FillEmptySpace(object sender, BandEventArgs e)
        {

            XRTable xrt医嘱内容 = xrTable医嘱内容;
            int i行高 = xrt医嘱内容.Rows[xrt医嘱内容.Rows.Count - 1].Height;

            XRTable xrTable = new XRTable();
            xrTable.Size = new Size(xrt医嘱内容.Width, e.Band.Height - 1);
            xrTable.BorderWidth = xrt医嘱内容.BorderWidth;
            xrTable.Location = xrt医嘱内容.Location;
            xrTable.BackColor = xrt医嘱内容.BackColor;

            int i空行数 = e.Band.Height / i行高;

            XRTableRow[] xrRow新空行 = new XRTableRow[i空行数];

            if (i空行数 > 0)
            {

                for (int i = 0; i < i空行数; i++)
                {
                    xrRow新空行[i] = new XRTableRow();
                    xrRow新空行[i].Size = new Size(xrt医嘱内容.Width, i行高);
                    xrRow新空行[i].Location = new Point(xrt医嘱内容.Location.X, i * i行高);
                    xrRow新空行[i].BorderWidth = 1;
                    xrRow新空行[i].BorderColor = xrt医嘱内容.Rows[xrt医嘱内容.Rows.Count - 1].BorderColor;
                    //CreateCell
                    XRTableRow row内容末行 = xrt医嘱内容.Rows[xrt医嘱内容.Rows.Count - 1];//内容末行
                    //if (i == (i空行数 - 1))
                    //{
                    //    CreateCellArray(xrRow新空行[i], row内容末行, true);
                    //}
                    //else
                    //{
                    CreateCellArray(xrRow新空行[i], row内容末行);
                    //}

                }
                xrTable.Rows.AddRange(xrRow新空行);
                e.Band.Controls.Add(xrTable);
            }

        }


        /// <summary>
        /// CreateCell
        /// </summary>
        /// <param name="xrRow新行">Current Row</param>
        /// <param name="xrRow内容末行">Row Template</param>
        private void CreateCellArray(XRTableRow xrRow新行, XRTableRow xrRow内容末行)
        {
            int Xmargin = 0;
            for (int i = 0; i < xrRow内容末行.Cells.Count; i++)
            {
                XRTableCell xrcell = new XRTableCell();
                xrcell.Borders = xrRow内容末行.Cells[i].Borders;
                xrcell.WidthF = xrRow内容末行.Cells[i].WidthF;
                xrcell.BackColor = xrRow内容末行.Cells[i].BackColor;
                xrcell.HeightF = xrRow内容末行.Cells[i].HeightF;
                xrcell.FormattingRules.Add(this.formattingRule表头);
                if (i != 0)
                {
                    xrcell.Location = new Point(Convert.ToInt32(Xmargin + xrRow内容末行.Cells[i].WidthF), 0);
                }
                else
                {
                    xrcell.Location = new Point(0, 0);
                }
                xrRow新行.Cells.Add(xrcell);
            }
        }

        #region 格式控制

        string s开嘱日期;
        string s开嘱时间;
        string s执行时间;
        string s用法;
        string s组别;
        bool b横线 = true;
        string s组别符号 = "";
        private void xrTable医嘱内容_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTable table = (XRTable)sender;
            s组别 = table.Rows[0].Cells[4].Text;//
            s组别符号 = table.Rows[0].Cells[4].Text;//组别符号
        }



        private void xrTableCell用法_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (s组别符号 == "┘" || s组别符号 == "]" || s组别符号 == "")
            {
                cell.ForeColor = Color.Black;
            }
            else
            {
                cell.ForeColor = Color.White;
            }
        }

        private void xrTableCell开嘱日期_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == s开嘱日期)
            {
                cell.Text = "..";
            }
            else
            {
                cell.ForeColor = Color.Black;
                s开嘱日期 = cell.Text;
            }
        }


        private void xrTableCell开嘱时间_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == s开嘱时间)
            {
                cell.Text = "..";
            }
            else
            {
                cell.ForeColor = Color.Black;
                s开嘱时间 = cell.Text;
            }
        }
        #endregion

        private void xrTableCell执行时间_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell cell = (XRTableCell)sender;
            //if (cell.Text == s执行时间)
            //{
            //    cell.Text = "..";
            //}
            //else
            //{
            //    cell.ForeColor = Color.Black;
            //    s执行时间 = cell.Text;
            //}
        }

        private void xrTableCell医师_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (xrTableCell开嘱时间.Text == "..")
            {
                xrTableCell医师.Text = "..";
            }
            else
            {
                xrTableCell医师.Text = "";
            }
        }

        private void xrTableCell执行护士_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (xrTableCell开嘱时间.Text == "..")
            {
                xrTableCell执行护士.Text = "..";
            }
            else
            {
                xrTableCell执行护士.Text = "";
            }
        }

        private void XtraReport临时医嘱_ParametersRequestSubmit(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {

        }

        private void XtraReport临时医嘱_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    string sql = "update zy病人信息 set 临时医嘱续打时间点='" + DateTime.Now.ToString() + "' where zyid=" + _person.sZYID;
            //    tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, sql);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }





    }
}
