﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.IO;

namespace interfaceZljl.Report.NewDetailReport
{
    public partial class XtraReport门诊处方分组 : DevExpress.XtraReports.UI.XtraReport
    {
        private static int cfidlen = 13;
        //public XtraReport门诊处方分组(ClassYBJK.person _mzPerson, System.Drawing.Image _image)
        public XtraReport门诊处方分组(string yyid, string str医院名称, ClassYBJK.person _mzPerson, DataTable dt明细)
        {
            InitializeComponent();
            xrLabelCfid.Text = "处方号：" + _mzPerson.S处方号;
            xrLabel姓名.Text = "患者姓名:" + _mzPerson.S姓名;
            xrLabel科室.Text = "科室：" + _mzPerson.S科室;
            xrLabel临床诊断.Text = "临床诊断:" + _mzPerson.S临床诊断;
            //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
            xrLabel年龄.Text = "年龄: " + _mzPerson.S年龄.ToString();
            xrLabel性别.Text = "性别: " + _mzPerson.S性别;
            xrLabel医生.Text = "医生: " + _mzPerson.s医生姓名;
            //xrLabel总金额.Text = "总金额: " + _mzPerson.Dec总金额.ToString();
            xrLabel日期.Text = "日期:" + _mzPerson.Dtime日期.ToShortDateString();
            xrLabelTitle.Text = TYK.zdInfo.sDwmc;
            //xrPictureBox医生签名.Image = _image;
            //foreach (DataRow row in dataSetYsgzz1.ys门诊处方.Rows)
            //{

            //}
        }

        public XtraReport门诊处方分组(string yyid, string str医院名称, DataTable dt病人信息, DataTable dt处方)
        {
            InitializeComponent();

            if ((yyid != null) && (yyid.Equals("2709") || yyid.Equals("2719")))
            {
                xrLabel13.Text = "门 诊 处 方 笺";
            }
            else
            {
                xrLabel13.Text = "门 诊 用 药 记 录";
            }

            if (str医院名称 != null)
            {
                xrLabelTitle.Text = str医院名称;
            }

            #region 设定病人信息
            if (dt病人信息 != null && dt病人信息.Rows.Count > 0)
            {
                //设置病人信息
                //xrLabelCfid.Text = chufang.Rows[0]["CFID"].ToString();
                xrLabel姓名.Text = "患者姓名：" + dt病人信息.Rows[0]["姓名"].ToString();
                xrLabel科室.Text = "科室：" + dt病人信息.Rows[0]["科室名称"].ToString();
                xrLabel临床诊断.Text = "诊断：" + dt病人信息.Rows[0]["临床诊断"].ToString();
                //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
                xrLabel年龄.Text = "年龄：" + dt病人信息.Rows[0]["年龄"].ToString() + dt病人信息.Rows[0]["年龄单位"].ToString();
                xrLabel性别.Text = "性别：" + dt病人信息.Rows[0]["性别"].ToString();
                xrLabel医生.Text = "医生：" + dt病人信息.Rows[0]["医生"].ToString();
                //xrLabel总金额.Text = "总金额: " + dt病人信息.Rows[0]["总金额"].ToString();
                
                //xrLabel日期.Text = "日期:" + dt病人信息.Rows[0]["开方时间"].ToString();
                string str开方时间 = dt病人信息.Rows[0]["开方时间"].ToString();
                string[] arrStr = str开方时间.Split(' ');
                if (arrStr.Length >0)
                {
                    xrLabel日期.Text = "日期：" + arrStr[0];
                }
                else
                {
                    xrLabel日期.Text = "日期：" + str开方时间;
                }
                
                string strCfid = dt病人信息.Rows[0]["cfid"].ToString();
                if (strCfid.Length >= cfidlen)
                {
                    xrLabelCfid.Text = "门诊号：" + strCfid.Substring(9, 4);
                }
                else
                {
                    xrLabelCfid.Text = "门诊号：" + strCfid;
                }

                ////手签图片设定
                //try
                //{
                //    byte[] arr = Convert.FromBase64String(dt病人信息.Rows[0]["手签照片"].ToString());
                //    MemoryStream ms = new MemoryStream(arr);
                //    Bitmap bmp = new Bitmap(ms);
                //    xrPictureBox医生签名.Image = bmp;
                //    ms.Close();
                //}
                //catch
                //{
                //    xrPictureBox医生签名.Image = null;
                //}  
            }
            #endregion

            if (dt处方 != null && dt处方.Rows.Count > 0)
            {
                //xrLabelCfid.Text = "处方编号：" + dt处方.Rows[0]["CFID"].ToString().Substring(9, 4);
                this.DataSource = null;
                this.DataMember = null;
                this.DataAdapter = null;
                this.DataSource = dt处方;
            }
        }

    }
}
