﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace interfaceZljl.Report
{
    public partial class XtraReport信息检索 : DevExpress.XtraReports.UI.XtraReport
    {
        int index = 0;//用来显示序号的变量

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="cardID"></param>
        /// <param name="dtCardInfo">身份证信息DataTable</param>
        /// <param name="dt就医记录">诊疗记录DataTable</param>
        public XtraReport信息检索(string cardID, DataTable dtCardInfo, DataTable dt就医记录)
        {
            InitializeComponent();

            #region 设定身份证信息
            if (dtCardInfo != null && dtCardInfo.Rows.Count > 0)
            {
                xrLabel姓名.Text = dtCardInfo.Rows[0]["name"].ToString();
                xrLabel性别.Text = dtCardInfo.Rows[0]["sex"].ToString();
                xrLabel民族.Text = dtCardInfo.Rows[0]["nation"].ToString();
                xrLabel出生日期.Text = dtCardInfo.Rows[0]["birthday"].ToString();
                xrLabel家庭住址.Text = dtCardInfo.Rows[0]["address"].ToString();
                xrLabel身份证号.Text = dtCardInfo.Rows[0]["cardid"].ToString();
                //xrPictureBox1.Image = Convert.FromBase64String(dt.Rows[0]["picture"].ToString());
                xrPictureBox1.DataBindings.Add("Image", dtCardInfo, "picture");
            }
            else
            {
                xrLabel身份证号.Text = cardID;
                xrLabelError.Text = "未找到身份证信息\n";
                xrLabelError.Visible = true;
            }
            #endregion

            #region 绑定就医记录信息
            if (dt就医记录 != null && dt就医记录.Rows.Count > 0)
            {
                //xrTableCell序号.DataBindings.Add("Text", null, "");
                xrTableCell医院.DataBindings.Add("Text", null, "医院名称");
                xrTableCell病人姓名.DataBindings.Add("Text", null, "病人姓名");
                xrTableCell类型.DataBindings.Add("Text", null, "类型");
                xrTableCell疾病名称.DataBindings.Add("Text", null, "疾病名称");
                xrTableCell就医时间.DataBindings.Add("Text", null, "时间");
                //后期需要对这个单元格进行字段绑定
                //string strFormat = @"javascript:window.showModalDialog('DetailInfo.aspx?params=" + cardID + "_{0}" + "','详细信息','dialogHeight:600px;dialogWidth:940px;scroll:yes;resizable:yes;center:yes;location:no;'); ";
                //xrTableCell详细信息.DataBindings.Add("NavigateUrl", null, "Params", strFormat);
                xrTableCell详细信息.DataBindings.Add("Tag", null, "Params");

                this.DataSource = dt就医记录;
            }
            else
            {
                xrLabelError.Visible = true;
                xrLabelError.Text += "未找到就医记录";
            }
            #endregion
        }

        /// <summary>
        /// 详细信息的单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xrTableCell详细信息_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            //Tag中存储的数据格式为 aaa_bbb_ccc
            //其中，aaa是医院id或村卫生室id，bbb的值有三种：mz/zy/wssmz，即医院门诊、医院住院、卫生室门诊
            //ccc是门诊id、住院id或卫生室门诊单据编码
            //从下面的语句中获取XRTableCell里已经绑定好的Tag的值
            object obj = e.Brick.Value;
            if (obj == null)
            {
                XtraMessageBox.Show("非常抱歉，没有您需要的数据", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            string param = obj.ToString();
            ShowDetailInfo(param);
        }

        public void ShowDetailInfo(string param)
        {
            string[] arrStr = param.Split('_');
            if (arrStr.Length != 3)
            {
                XtraMessageBox.Show("对不起，获取门诊住院信息失败。", "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (arrStr[1].Equals("mz")) //就医类型是医院门诊的情况时,显示医院门诊处方
            {
                //DetailReport.XtraReport门诊处方 rep = new DetailReport.XtraReport门诊处方(arrStr[0], arrStr[2]);
                //ReportPrintTool tool = new ReportPrintTool(rep);
                //tool.ShowPreviewDialog();
                Show医院门诊处方(arrStr[0], arrStr[2]);
            }
            else if (arrStr[1].Equals("zy"))//就医类型是医院住院的情况时，显示住院信息（包括长期医嘱、临时医嘱、病历信息）
            {
                //Frm住院信息 frm = new Frm住院信息(arrStr[0], arrStr[2]);
                //frm.ShowDialog();
                Show医院住院信息(arrStr[0], arrStr[2]);
            }
            else if (arrStr[1].Equals("wssmz")) //就医类型是卫生室门诊的情况时，显示卫生室门诊处方
            {
                //DetailReport.XtraReport卫生室门诊处方 wssrep = new DetailReport.XtraReport卫生室门诊处方(arrStr[2]);
                //ReportPrintTool tool = new ReportPrintTool(wssrep);
                //tool.ShowPreviewDialog();
                Show卫生室处方(arrStr[0], arrStr[2]);
            }
            else
            {
                XtraMessageBox.Show("对不起，获取门诊住院信息失败。", "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        


        public XtraReport信息检索()
        {
            InitializeComponent();

            xrTableCell医院.DataBindings.Add("Text", null, "医院名称");
            xrTableCell病人姓名.DataBindings.Add("Text", null, "病人姓名");
            xrTableCell类型.DataBindings.Add("Text", null, "类型");
            xrTableCell疾病名称.DataBindings.Add("Text", null, "疾病名称");
            xrTableCell就医时间.DataBindings.Add("Text", null, "时间");

            //后期需要对"xrTableCell详细信息"单元格进行字段绑定
            //Params字段的数据格式为aaa_bbb_ccc
            //其中，aaa是医院id或村卫生室id，bbb的值有三种：mz/zy/wssmz，即医院门诊、医院住院、卫生室门诊
            //ccc是门诊id、住院id或卫生室门诊单据编码
            xrTableCell详细信息.DataBindings.Add("Tag", null, "Params");
        }

        //Web 版的Report中，此事件无效
        private void XtraReport信息检索_ParametersRequestSubmit(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {
            string cardid = (sfzh.Value == null) ? null : sfzh.Value.ToString();
            string count = (jyCount.Value == null) ? null : jyCount.Value.ToString();

            //初始化XrLabelError控件的状态
            xrLabelError.Visible = false;
            xrLabelError.Text = "";

            //初始化序号的值
            index = 0;//序号重新设置为0

            if (cardid != null && count != null && Regex.IsMatch(cardid, @"(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)")
                 && Regex.IsMatch(count, @"^\d+$"))
            {
                Set身份证信息(cardid);
                Set就医记录(cardid,count);
            }
            else
            {
                XtraMessageBox.Show("请检查您输入的身份证号、就医记录数量", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void Set身份证信息(string cardid)
        {
            DataTable dtCardInfo = null;
            try
            {
                //根据ID查询身份证信息，从PubDB数据库中取身份证信息（PubDB数据库中的身份证信息是最全的）
                string conStr = GetPubDbConStr();
                string sql = @"SELECT TOP 1 [name],[sex],[nation],[birthday],[address],[cardId],[picture]
                                        FROM [pubDB].[dbo].[pubSfzhInfo]
                                        where cardId = @cardid";

                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@cardid", cardid);

                dtCardInfo = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql, sqlparams).Tables[0];
            }
            catch(Exception ex)
            {
                dtCardInfo = null;
                XtraMessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            //设定身份证信息
            if (dtCardInfo != null && dtCardInfo.Rows.Count > 0)
            {
                xrLabel姓名.Text = dtCardInfo.Rows[0]["name"].ToString();
                xrLabel性别.Text = dtCardInfo.Rows[0]["sex"].ToString();
                xrLabel民族.Text = dtCardInfo.Rows[0]["nation"].ToString();
                xrLabel出生日期.Text = dtCardInfo.Rows[0]["birthday"].ToString();
                xrLabel家庭住址.Text = dtCardInfo.Rows[0]["address"].ToString();
                xrLabel身份证号.Text = dtCardInfo.Rows[0]["cardid"].ToString();
                xrPictureBox1.DataBindings.Add("Image", dtCardInfo, "picture");
            }
            else
            {
                xrLabel姓名.Text = "";
                xrLabel性别.Text = "";
                xrLabel民族.Text = "";
                xrLabel出生日期.Text ="";
                xrLabel家庭住址.Text = "";
                xrLabel身份证号.Text = "";
                //xrPictureBox1.DataBindings.Add("Image", null, "picture");
                if (xrPictureBox1.DataBindings.Count > 0)
                {
                    xrPictureBox1.DataBindings.RemoveAt(0);
                }

                xrLabel身份证号.Text = cardid;
                xrLabelError.Text = "未找到身份证信息\n";
                xrLabelError.Visible = true;
            }
        }

        private void Set就医记录(string cardid, string count)
        {
            DataTable dtDetaiInfo = null;
            try
            {
                string conStr = GetHisConStr();
                string sql = "uSp就医信息检索";

                SqlParameter[] sqlparams = new SqlParameter[2];
                sqlparams[0] = new SqlParameter("@cardid", cardid);
                sqlparams[1] = new SqlParameter("@topnum", count);

                dtDetaiInfo = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.StoredProcedure, sql, sqlparams).Tables[0];
            }
            catch(Exception ex)
            {
                dtDetaiInfo = null;
                XtraMessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            //绑定就医记录信息
            if (dtDetaiInfo != null && dtDetaiInfo.Rows.Count > 0)
            {
                #region 已经注释掉的代码
                //xrTableCell医院.DataBindings.Add("Text", null, "医院名称");
                //xrTableCell病人姓名.DataBindings.Add("Text", null, "病人姓名");
                //xrTableCell类型.DataBindings.Add("Text", null, "类型");
                //xrTableCell疾病名称.DataBindings.Add("Text", null, "疾病名称");
                //xrTableCell就医时间.DataBindings.Add("Text", null, "时间");
                ////后期需要对这个单元格进行字段绑定
                ////string strFormat = @"javascript:window.showModalDialog('DetailInfo.aspx?params=" + cardID + "_{0}" + "','详细信息','dialogHeight:600px;dialogWidth:940px;scroll:yes;resizable:yes;center:yes;location:no;'); ";
                //xrTableCell详细信息.DataBindings.Add("Tag", null, "Params");
                #endregion

                this.DataSource = dtDetaiInfo;
            }
            else
            {
                this.DataSource = null;
                xrLabelError.Visible = true;
                xrLabelError.Text += "未找到就医记录";
            }
        }

        private void xrTableCell序号_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            cell.Text = (++index).ToString();
        }

        private string GetPubDbConStr()
        {
            string conStr = Config.PubDBConStr;
            return conStr;
        }

        private string GetYthConStr()
        {
            string conStr = Config.ythstr;
            return conStr;
        }

        private string GetHisConStr()
        {
            string conStr = Config.hisStr;
            return conStr;
        }

        private void Show医院住院信息(string yyid, string zyid)
        {
            Frm住院信息 frm = new Frm住院信息(yyid, zyid);
            frm.ShowDialog();
        }

        private string Get医院名称(string yyid)
        {
            string conStr = GetHisConStr();
            string deptname = "";
            if (yyid.Equals("2709") || yyid.Equals("109"))
            {
                deptname = "沂水县第二人民医院";
            }
            else
            {
                try
                {
                    string sql = @"select top 1 deptname from CHIS2701.dbo.中心院列表 where deptid = " + yyid;

                    DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql).Tables[0];
                    deptname = dt.Rows[0][0].ToString();
                }
                catch
                {
                    deptname = "医院名称";
                }
            }
            return deptname;
        }

        private void Show医院门诊处方(string yyid, string mzid)
        {
            //DetailReport.XtraReport门诊处方 rep = new DetailReport.XtraReport门诊处方(yyid, mzid);
            //ReportPrintTool tool = new ReportPrintTool(rep);
            //tool.ShowPreviewDialog();


            string str医院名称 = Get医院名称(yyid);
            DataTable dt门诊病人 = Get门诊病人(yyid, mzid);
            DataTable dt处方 = Get门诊处方(yyid, mzid);

            if ((dt门诊病人 != null) && (dt处方 !=null) &&(dt门诊病人.Rows.Count >0) && (dt处方.Rows.Count >0))
            {
                NewDetailReport.XtraReport门诊处方分组 rep = new NewDetailReport.XtraReport门诊处方分组(yyid, str医院名称, dt门诊病人, dt处方);
                ReportPrintTool tool = new ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                XtraMessageBox.Show("没有与该门诊记录对应的用药记录","提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        #region 获取医院门诊信息
        private DataTable Get门诊病人(string yyid, string mzid)
        {
            string conStr = GetHisConStr();
            DataTable dt病人信息 = null;

            #region 注释掉的代码
//            try
//            {
//                //验证门诊摘要表中，是否存在“门诊诊断”“年龄单位”
//                string sql验证 = "use CHIS" + yyid + @";
//select '门诊诊断' 项目, COUNT(*) 次数 from syscolumns where id=object_id('[MF门诊摘要]') and name='门诊诊断'
//union all 
//select '年龄单位' 项目, COUNT(*) 次数 from syscolumns where id=object_id('[MF门诊摘要]') and name='年龄单位'";
//                DataTable dt验证 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql验证).Tables[0];

//                //用于查询门诊病人的信息
//                string sql2 = "SELECT top 1 [MZID],[病人姓名],[性别]";
//                if (dt验证.Rows[0]["次数"].ToString() == "1")
//                {
//                    sql2 += ",门诊诊断 诊断,[录入时间],";
//                }
//                else
//                {
//                    sql2 += ",临床诊断 诊断,[录入时间],";
//                }

//                if (dt验证.Rows[1]["次数"].ToString() == "1")
//                {
//                    sql2 += "cast(isnull([年龄],0) as varchar(20)) + isnull([年龄单位],'') 年龄,";
//                }
//                else
//                {
//                    sql2 += "cast(isnull([年龄],0) as varchar(20)) 年龄,";
//                }
//                sql2 += @"[总费用],bb.用户名 医生姓名, bb.科室名称
//FROM [CHIS" + yyid + @"].dbo.[MF门诊摘要] aa
//	left outer join [CHIS" + yyid + @"].dbo.pubUser bb on aa.医生编码 = bb.用户编码
//where aa.MZID = '" + mzid + "'";
//                dt病人信息 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql2).Tables[0];

//            }
//            catch
//            {
//                dt病人信息 = null;
            //            }
            #endregion

            try
            {
                //string sqlCmd摘要 = //"select aa.cfid,bb.病人姓名 姓名,bb.性别,bb.年龄,bb.年龄单位, " +
                //            "use CHIS" + yyid + ";\r\n" +
                //            "select top 1 aa.cfid,bb.病人姓名 姓名,bb.性别,bb.年龄,bb.年龄单位, " +
                //            " bb.地址,cc.用户名 医生,aa.医生编码,aa.零售价金额 总金额,cc.科室名称,bb.临床诊断,aa.开方时间,cc.手签照片 " +
                //            " from mf处方摘要 aa left outer join mf门诊摘要 bb " +
                //            " on aa.mzid=bb.mzid left outer join pubUser cc on aa.医生编码=cc.用户编码" +
                //    //" where aa.cfid=" + _s处方号;
                //            " where bb.MZID=" + mzid;
                string sqlCmd摘要 = "";
                if (yyid == "2709")
                {
                    sqlCmd摘要 = "select top 1 aa.cfid,bb.病人姓名 姓名,bb.性别,bb.年龄,bb.年龄单位, " +
                                " bb.地址,cc.用户名 医生,aa.医生编码,aa.零售价金额 总金额,cc.科室名称,bb.临床诊断,aa.开方时间,cc.手签照片 " +
                                " from MZYY.CHIS2709.dbo.mf处方摘要 aa left outer join MZYY.CHIS2709.dbo.mf门诊摘要 bb " +
                                " on aa.mzid=bb.mzid left outer join pubUser cc on aa.医生编码=cc.用户编码" +
                                " where bb.MZID=" + mzid;
                }
                else
                {
                    sqlCmd摘要 = //"select aa.cfid,bb.病人姓名 姓名,bb.性别,bb.年龄,bb.年龄单位, " +
                                "use CHIS" + yyid + ";\r\n" +
                                "select top 1 aa.cfid,bb.病人姓名 姓名,bb.性别,bb.年龄,bb.年龄单位, " +
                                " bb.地址,cc.用户名 医生,aa.医生编码,aa.零售价金额 总金额,cc.科室名称,bb.临床诊断,aa.开方时间,cc.手签照片 " +
                                " from mf处方摘要 aa left outer join mf门诊摘要 bb " +
                                " on aa.mzid=bb.mzid left outer join pubUser cc on aa.医生编码=cc.用户编码" +
                        //" where aa.cfid=" + _s处方号;
                                " where bb.MZID=" + mzid;
                }
                dt病人信息 = TYK.Class.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sqlCmd摘要).Tables[0];
            }
            catch
            {
                dt病人信息 = null;
            }
            return dt病人信息;
        }

        //获取处方时，按照“一个门诊只有一个处方”来处理的
        private DataTable Get门诊处方(string yyid, string mzid)
        {
            string conStr = GetHisConStr();
            DataTable dt明细 = null;
            try
            {
                //string SQL明细 = "use CHIS" + yyid + ";\r\n" +
                //    "SELECT aa.组别,yp.财务分类,aa.药品名称 项目名称," + "\r\n" +
                //    "       aa.药房规格 规格," + "\r\n" +
                //    "       aa.药房单位 单位," + "\r\n" +
                //    "       [dbo].[ClearZero](剂量数量) 剂量数量," + "\r\n" +
                //    "       aa.剂量单位," + "\r\n" +
                //    "       频次," + "\r\n" +
                //    "       用法,[dbo].[ClearZero](处方总量) 数量,零售价 单价,aa.零售价金额 金额" + "\r\n" +//,case when 用法 is not null then '用法' else '' end 用法提示
                //    "  FROM MF处方明细 aa" + "\r\n" +
                //    "       LEFT OUTER JOIN MF处方摘要 bb ON aa.CFID = bb.CFID left outer join yk药品信息 yp on aa.药品序号=yp.药品序号 " + "\r\n" +
                //    //" WHERE bb.CFID =" + _s处方号 + " and yp.财务分类<>'卫生材料' order by aa.id ";
                //    " WHERE bb.MZID =" + mzid + " and yp.财务分类<>'卫生材料' order by aa.id ";
                string SQL明细 = "";
                if(yyid=="2709")
                {
                    SQL明细 = "SELECT aa.组别,yp.财务分类,aa.药品名称 项目名称," + "\r\n" +
                    "       aa.药房规格 规格," + "\r\n" +
                    "       aa.药房单位 单位," + "\r\n" +
                    "       [dbo].[ClearZero](剂量数量) 剂量数量," + "\r\n" +
                    "       aa.剂量单位," + "\r\n" +
                    "       频次," + "\r\n" +
                    "       用法,[dbo].[ClearZero](处方总量) 数量,零售价 单价,aa.零售价金额 金额" + "\r\n" +
                    "  FROM MZYY.CHIS2709.dbo.MF处方明细 aa" + "\r\n" +
                    "       LEFT OUTER JOIN MZYY.CHIS2709.dbo.MF处方摘要 bb ON aa.CFID = bb.CFID left outer join yk药品信息 yp on aa.药品序号=yp.药品序号 " + "\r\n" +
                    " WHERE bb.MZID =" + mzid + " and yp.财务分类<>'卫生材料' order by aa.id ";
                }
                else
                {
                    SQL明细 = "use CHIS" + yyid + ";\r\n" +
                    "SELECT aa.组别,yp.财务分类,aa.药品名称 项目名称," + "\r\n" +
                    "       aa.药房规格 规格," + "\r\n" +
                    "       aa.药房单位 单位," + "\r\n" +
                    "       [dbo].[ClearZero](剂量数量) 剂量数量," + "\r\n" +
                    "       aa.剂量单位," + "\r\n" +
                    "       频次," + "\r\n" +
                    "       用法,[dbo].[ClearZero](处方总量) 数量,零售价 单价,aa.零售价金额 金额" + "\r\n" +//,case when 用法 is not null then '用法' else '' end 用法提示
                    "  FROM MF处方明细 aa" + "\r\n" +
                    "       LEFT OUTER JOIN MF处方摘要 bb ON aa.CFID = bb.CFID left outer join yk药品信息 yp on aa.药品序号=yp.药品序号 " + "\r\n" +
                        //" WHERE bb.CFID =" + _s处方号 + " and yp.财务分类<>'卫生材料' order by aa.id ";
                    " WHERE bb.MZID =" + mzid + " and yp.财务分类<>'卫生材料' order by aa.id ";
                }


                dt明细 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, SQL明细).Tables[0];
            }
            catch
            {
                dt明细 = null;
            }
            return dt明细;
        }

        #endregion 


        private void Show卫生室处方(string wssid, string djbm)
        {
            //DetailReport.XtraReport卫生室门诊处方 wssrep = new DetailReport.XtraReport卫生室门诊处方(djbm);
            //ReportPrintTool tool = new ReportPrintTool(wssrep);
            //tool.ShowPreviewDialog();
            DataTable dt病人 = Get卫生室病人信息(djbm);
            DataTable dt处方 = Get卫生室门诊处方(djbm);
            string str卫生室名称 = dt病人.Rows[0]["卫生室名称"].ToString();

            if ((dt病人 != null) && (dt处方 != null) && (dt病人.Rows.Count > 0) && (dt处方.Rows.Count > 0))
            {
                NewDetailReport.XtraReport卫生室门诊处方 rep = new NewDetailReport.XtraReport卫生室门诊处方("wss", str卫生室名称, dt病人, dt处方);
                ReportPrintTool tool = new ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                XtraMessageBox.Show("没有与该门诊记录对应的用药记录", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        #region 获取卫生室门诊


        private DataTable Get卫生室病人信息(string djbm)
        {
            DataTable dt = null;
            try
            {
                string conStr = GetYthConStr();
                string sqlStr = @"SELECT TOP 1 aa.[F_DJBM] cfid,aa.[F_NAME] 姓名,'' 性别,'' 年龄,'' 年龄单位
				, '' 地址,aa.[F_ZZYS] 医生,'' 医生编码, [F_MZYYF] 总金额
				,'' 科室名称, aa.[F_JBID] 临床诊断
				,aa.[F_JZRQ] 开方时间,'' 手签照片
				,bb.[RGNAME] 卫生室名称
  FROM [yth].[dbo].[BC_MZYLBC] aa
  left outer join yth.dbo.ZD_REGION bb on aa.F_YLJG = bb.RGID
  where F_DJBM =@djbm";

                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@djbm", djbm);

                dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sqlStr, sqlparams).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        private DataTable Get卫生室门诊处方(string djid)
        {
            DataTable dt = null;
            try
            {
                string conStr = GetYthConStr();
                string sql = @"SELECT '' 组别, '' 财务分类
	  --, [F_DJBM]  单据编码
      ,[F_MC]   项目名称      
      ,[F_RULE]  规格
      ,[F_UNIT]  单位
      --,F_LBMC
      ,'' 剂量数量
      ,'' 剂量单位,'' 频次,''用法
      ,F_ZL 数量, F_DJ 单价, F_ZJE 金额
  FROM [yth].[dbo].[BC_FYMXB]
  where F_LBMC in('西药','中草药') and F_DJBM = @djbm";
                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@djbm", djid);
                dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql, sqlparams).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        #endregion
    }
}
