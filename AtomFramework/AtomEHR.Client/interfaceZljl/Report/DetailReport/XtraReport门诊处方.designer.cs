﻿namespace interfaceZljl.Report.DetailReport
{
    partial class XtraReport门诊处方
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel频次 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel剂量单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel数量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel规格 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel项目名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel剂量数量 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel临床诊断 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCfid = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel发药人 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel总金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel频次,
            this.xrLabel用法,
            this.xrLabel剂量单位,
            this.xrLabel数量,
            this.xrLabel单位,
            this.xrLabel规格,
            this.xrLabel项目名称,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLabel14,
            this.xrLabel剂量数量});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 132.7151F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel频次
            // 
            this.xrLabel频次.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "频次")});
            this.xrLabel频次.Dpi = 254F;
            this.xrLabel频次.LocationFloat = new DevExpress.Utils.PointFloat(695.0937F, 71.64922F);
            this.xrLabel频次.Name = "xrLabel频次";
            this.xrLabel频次.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel频次.SizeF = new System.Drawing.SizeF(206.077F, 58.42004F);
            this.xrLabel频次.StylePriority.UseTextAlignment = false;
            this.xrLabel频次.Text = "xrLabel频次";
            this.xrLabel频次.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel用法
            // 
            this.xrLabel用法.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "用法")});
            this.xrLabel用法.Dpi = 254F;
            this.xrLabel用法.LocationFloat = new DevExpress.Utils.PointFloat(474.8741F, 71.64922F);
            this.xrLabel用法.Name = "xrLabel用法";
            this.xrLabel用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel用法.SizeF = new System.Drawing.SizeF(220.2196F, 58.42004F);
            this.xrLabel用法.StylePriority.UseTextAlignment = false;
            this.xrLabel用法.Text = "xrLabel用法";
            this.xrLabel用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel剂量单位
            // 
            this.xrLabel剂量单位.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "剂量单位")});
            this.xrLabel剂量单位.Dpi = 254F;
            this.xrLabel剂量单位.LocationFloat = new DevExpress.Utils.PointFloat(298.0266F, 71.64922F);
            this.xrLabel剂量单位.Name = "xrLabel剂量单位";
            this.xrLabel剂量单位.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel剂量单位.SizeF = new System.Drawing.SizeF(176.8475F, 58.42004F);
            this.xrLabel剂量单位.StylePriority.UseTextAlignment = false;
            this.xrLabel剂量单位.Text = "xrLabel剂量单位";
            this.xrLabel剂量单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel数量
            // 
            this.xrLabel数量.CanGrow = false;
            this.xrLabel数量.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "数量")});
            this.xrLabel数量.Dpi = 254F;
            this.xrLabel数量.LocationFloat = new DevExpress.Utils.PointFloat(748.0104F, 13.22917F);
            this.xrLabel数量.Name = "xrLabel数量";
            this.xrLabel数量.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel数量.SizeF = new System.Drawing.SizeF(150.5143F, 58.41998F);
            this.xrLabel数量.Text = "xrLabel数量";
            // 
            // xrLabel单位
            // 
            this.xrLabel单位.CanGrow = false;
            this.xrLabel单位.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "单位")});
            this.xrLabel单位.Dpi = 254F;
            this.xrLabel单位.LocationFloat = new DevExpress.Utils.PointFloat(536.5751F, 13.22917F);
            this.xrLabel单位.Name = "xrLabel单位";
            this.xrLabel单位.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel单位.SizeF = new System.Drawing.SizeF(182.3311F, 58.41999F);
            this.xrLabel单位.Text = "xrLabel单位";
            // 
            // xrLabel规格
            // 
            this.xrLabel规格.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "规格")});
            this.xrLabel规格.Dpi = 254F;
            this.xrLabel规格.LocationFloat = new DevExpress.Utils.PointFloat(298.0266F, 13.22917F);
            this.xrLabel规格.Name = "xrLabel规格";
            this.xrLabel规格.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel规格.SizeF = new System.Drawing.SizeF(209.4443F, 58.41998F);
            this.xrLabel规格.StylePriority.UseTextAlignment = false;
            this.xrLabel规格.Text = "xrLabel规格";
            this.xrLabel规格.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel规格.WordWrap = false;
            // 
            // xrLabel项目名称
            // 
            this.xrLabel项目名称.CanGrow = false;
            this.xrLabel项目名称.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "项目名称")});
            this.xrLabel项目名称.Dpi = 254F;
            this.xrLabel项目名称.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 13.22917F);
            this.xrLabel项目名称.Name = "xrLabel项目名称";
            this.xrLabel项目名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel项目名称.SizeF = new System.Drawing.SizeF(262.0433F, 58.42001F);
            this.xrLabel项目名称.Text = "xrLabel项目名称";
            this.xrLabel项目名称.WordWrap = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(507.4709F, 13.22917F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(29.10419F, 58.42F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "/";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(718.9062F, 13.22917F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(29.10419F, 58.42F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "×";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 71.64922F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(101.0709F, 58.42001F);
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "用法";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel剂量数量
            // 
            this.xrLabel剂量数量.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "剂量数量")});
            this.xrLabel剂量数量.Dpi = 254F;
            this.xrLabel剂量数量.LocationFloat = new DevExpress.Utils.PointFloat(137.0542F, 71.64922F);
            this.xrLabel剂量数量.Name = "xrLabel剂量数量";
            this.xrLabel剂量数量.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel剂量数量.SizeF = new System.Drawing.SizeF(159.1734F, 58.42004F);
            this.xrLabel剂量数量.StylePriority.UseTextAlignment = false;
            this.xrLabel剂量数量.Text = "xrLabel剂量数量";
            this.xrLabel剂量数量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.xrLine4,
            this.xrLabel日期,
            this.xrLine2,
            this.xrLabel临床诊断,
            this.xrLabel年龄,
            this.xrLabel性别,
            this.xrLabel姓名,
            this.xrLine1,
            this.xrLabel科室,
            this.xrLabelCfid,
            this.xrLabelTitle});
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 396F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 115.9108F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(862.5416F, 58.42F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "门 诊 用 药 记 录";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 254F;
            this.xrLine4.LineWidth = 3;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(30.6916F, 370.0166F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(867.8334F, 5.080017F);
            // 
            // xrLabel日期
            // 
            this.xrLabel日期.Dpi = 254F;
            this.xrLabel日期.LocationFloat = new DevExpress.Utils.PointFloat(631.825F, 174.3308F);
            this.xrLabel日期.Name = "xrLabel日期";
            this.xrLabel日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel日期.SizeF = new System.Drawing.SizeF(266.6998F, 58.42001F);
            this.xrLabel日期.StylePriority.UseTextAlignment = false;
            this.xrLabel日期.Text = "日期:";
            this.xrLabel日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel日期.WordWrap = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(30.6916F, 300.3781F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(867.8334F, 5.079987F);
            // 
            // xrLabel临床诊断
            // 
            this.xrLabel临床诊断.Dpi = 254F;
            this.xrLabel临床诊断.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 311.5966F);
            this.xrLabel临床诊断.Name = "xrLabel临床诊断";
            this.xrLabel临床诊断.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel临床诊断.SizeF = new System.Drawing.SizeF(862.5416F, 58.41998F);
            this.xrLabel临床诊断.StylePriority.UseTextAlignment = false;
            this.xrLabel临床诊断.Text = "诊断:";
            this.xrLabel临床诊断.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel临床诊断.WordWrap = false;
            // 
            // xrLabel年龄
            // 
            this.xrLabel年龄.Dpi = 254F;
            this.xrLabel年龄.LocationFloat = new DevExpress.Utils.PointFloat(501.9482F, 241.9582F);
            this.xrLabel年龄.Name = "xrLabel年龄";
            this.xrLabel年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel年龄.SizeF = new System.Drawing.SizeF(232.8333F, 58.41997F);
            this.xrLabel年龄.StylePriority.UseTextAlignment = false;
            this.xrLabel年龄.Text = "年龄:";
            this.xrLabel年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.Dpi = 254F;
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(737.1291F, 241.9582F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(161.3957F, 58.42F);
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.Text = "性别:";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Dpi = 254F;
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 241.9582F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(465.9648F, 58.41997F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "患者姓名:";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel姓名.WordWrap = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(30.6916F, 232.9626F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(870.4791F, 5.080002F);
            // 
            // xrLabel科室
            // 
            this.xrLabel科室.Dpi = 254F;
            this.xrLabel科室.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 174.3308F);
            this.xrLabel科室.Name = "xrLabel科室";
            this.xrLabel科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel科室.SizeF = new System.Drawing.SizeF(289.3484F, 58.42F);
            this.xrLabel科室.StylePriority.UseTextAlignment = false;
            this.xrLabel科室.Text = "科室:";
            this.xrLabel科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel科室.WordWrap = false;
            // 
            // xrLabelCfid
            // 
            this.xrLabelCfid.Dpi = 254F;
            this.xrLabelCfid.LocationFloat = new DevExpress.Utils.PointFloat(354.4356F, 174.3308F);
            this.xrLabelCfid.Name = "xrLabelCfid";
            this.xrLabelCfid.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCfid.SizeF = new System.Drawing.SizeF(277.3894F, 58.42001F);
            this.xrLabelCfid.StylePriority.UseTextAlignment = false;
            this.xrLabelCfid.Text = "门诊号:";
            this.xrLabelCfid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabelCfid.WordWrap = false;
            // 
            // xrLabelTitle
            // 
            this.xrLabelTitle.Dpi = 254F;
            this.xrLabelTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabelTitle.LocationFloat = new DevExpress.Utils.PointFloat(35.98327F, 56.75001F);
            this.xrLabelTitle.Name = "xrLabelTitle";
            this.xrLabelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTitle.SizeF = new System.Drawing.SizeF(862.5416F, 58.42F);
            this.xrLabelTitle.StylePriority.UseFont = false;
            this.xrLabelTitle.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle.Text = "医院名称";
            this.xrLabelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 25F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel发药人,
            this.xrLabel总金额,
            this.xrLabel医生,
            this.xrLine3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 172.6458F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel发药人
            // 
            this.xrLabel发药人.Dpi = 254F;
            this.xrLabel发药人.LocationFloat = new DevExpress.Utils.PointFloat(109.855F, 101.6001F);
            this.xrLabel发药人.Name = "xrLabel发药人";
            this.xrLabel发药人.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel发药人.SizeF = new System.Drawing.SizeF(326.6018F, 58.42001F);
            this.xrLabel发药人.Text = "发药人：";
            this.xrLabel发药人.WordWrap = false;
            // 
            // xrLabel总金额
            // 
            this.xrLabel总金额.Dpi = 254F;
            this.xrLabel总金额.LocationFloat = new DevExpress.Utils.PointFloat(573.6166F, 25.29416F);
            this.xrLabel总金额.Name = "xrLabel总金额";
            this.xrLabel总金额.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel总金额.SizeF = new System.Drawing.SizeF(306.9168F, 58.42001F);
            this.xrLabel总金额.Text = "总金额";
            this.xrLabel总金额.WordWrap = false;
            // 
            // xrLabel医生
            // 
            this.xrLabel医生.Dpi = 254F;
            this.xrLabel医生.LocationFloat = new DevExpress.Utils.PointFloat(109.855F, 25.29416F);
            this.xrLabel医生.Name = "xrLabel医生";
            this.xrLabel医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel医生.SizeF = new System.Drawing.SizeF(326.6018F, 58.42001F);
            this.xrLabel医生.Text = "医生:";
            this.xrLabel医生.WordWrap = false;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LineWidth = 3;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(12.69998F, 2.540025F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(867.8334F, 5.08F);
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // XtraReport门诊处方
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter});
            this.Dpi = 254F;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(0, 8, 396, 25);
            this.PageHeight = 2101;
            this.PageWidth = 947;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel临床诊断;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCfid;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel发药人;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医生;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel日期;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel频次;
        private DevExpress.XtraReports.UI.XRLabel xrLabel用法;
        private DevExpress.XtraReports.UI.XRLabel xrLabel剂量单位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel剂量数量;
        private DevExpress.XtraReports.UI.XRLabel xrLabel数量;
        private DevExpress.XtraReports.UI.XRLabel xrLabel单位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel规格;
        private DevExpress.XtraReports.UI.XRLabel xrLabel项目名称;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
    }
}
