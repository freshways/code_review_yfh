﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Data.SqlClient;

namespace interfaceZljl.Report.DetailReport
{
    public partial class XtraReport卫生室门诊处方 : DevExpress.XtraReports.UI.XtraReport
    {
        //Web版本使用的构造函数
        public XtraReport卫生室门诊处方(DataTable dt病人信息, DataTable dt处方)
        {
            InitializeComponent();
            //xrLabelCfid.Text = "处方号：" + _mzPerson.S处方号;
            //xrLabel姓名.Text = "患者姓名:" + _mzPerson.S姓名;
            //xrLabel科室.Text = "科室：" + _mzPerson.S科室;
            //xrLabel临床诊断.Text = "临床诊断:" + _mzPerson.S临床诊断;
            //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
            //xrLabel年龄.Text = "年龄: " + _mzPerson.S年龄.ToString();
            //xrLabel性别.Text = "性别: " + _mzPerson.S性别;
            //xrLabel医生.Text = "医生: " + _mzPerson.s医生姓名;
            //xrLabel总金额.Text = "总金额: " + _mzPerson.Dec总金额.ToString();
            //xrLabel日期.Text = "日期:" + _mzPerson.Dtime日期.ToShortDateString();
            //xrLabelTitle.Text = TYK.zdInfo.iDwid == 109 ? "沂水县第二人民医院" : TYK.zdInfo.sDwmc;
            //xrPictureBox医生签名.Image = _image;

            if (dt病人信息 != null && dt病人信息.Rows.Count > 0)
            {
                //设置病人信息
                xrLabelTitle.Text = dt病人信息.Rows[0]["RGNAME"].ToString();
                xrLabel单据编码.Text = "单据编码：" + dt病人信息.Rows[0]["F_DJBM"].ToString();
                xrLabel姓名.Text = "患者姓名："+dt病人信息.Rows[0]["F_NAME"].ToString();
                xrLabel日期.Text = "日期：" + dt病人信息.Rows[0]["F_JZRQ"].ToString();
                xrLabel疾病名称.Text = "疾病名称：" + dt病人信息.Rows[0]["F_JBID"].ToString();
                xrLabel医生.Text = "医生："+ dt病人信息.Rows[0]["F_ZZYS"].ToString();
            }
            else
            {
                xrLabel姓名.Text = "患者姓名:"+"未找到病人信息";
            }

            if(dt处方!=null && dt处方.Rows.Count > 0)
            {
                //设置
                this.DataSource = dt处方;
            }
            else
            {
                xrLabel3.Text = "未找到处方信息";
            }
        }

        public XtraReport卫生室门诊处方(string djbm)
        {
            InitializeComponent();

            Bind病人信息(djbm);
            Bind处方信息(djbm);
        }

        private void Bind病人信息(string djbm)
        {
            DataTable dt病人信息 = Get病人信息(djbm);

            if (dt病人信息 != null && dt病人信息.Rows.Count > 0)
            {
                //设置病人信息
                xrLabelTitle.Text = dt病人信息.Rows[0]["RGNAME"].ToString();
                xrLabel单据编码.Text = "单据编码：" + dt病人信息.Rows[0]["F_DJBM"].ToString();
                xrLabel姓名.Text = "患者姓名：" + dt病人信息.Rows[0]["F_NAME"].ToString();
                xrLabel日期.Text = "日期：" + dt病人信息.Rows[0]["F_JZRQ"].ToString();
                xrLabel疾病名称.Text = "疾病名称：" + dt病人信息.Rows[0]["F_JBID"].ToString();
                xrLabel医生.Text = "医生：" + dt病人信息.Rows[0]["F_ZZYS"].ToString();
            }
            else
            {
                xrLabel姓名.Text = "患者姓名:" + "未找到病人信息";
            }
        }

        private void Bind处方信息(string djbm)
        {
            DataTable dt处方 = Get卫生室门诊处方(djbm);
            if (dt处方 != null && dt处方.Rows.Count > 0)
            {
                //设置
                this.DataSource = dt处方;
            }
            else
            {
                xrLabel3.Text = "未找到处方信息";
            }
        }


        private DataTable Get病人信息(string djid)
        {
            DataTable dt = null;
            try
            {
                string conStr = GetYthConStr();
                string sqlStr = @"SELECT TOP 1 aa.[F_DJBM],aa.[F_NAME],aa.[F_JZRQ],aa.[F_JBID],aa.[F_ZZYS],bb.[RGNAME]
  FROM [yth].[dbo].[BC_MZYLBC] aa
  left outer join yth.dbo.ZD_REGION bb on aa.F_YLJG = bb.RGID
  where F_DJBM = @djbm";

                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@djbm", djid);

                dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sqlStr, sqlparams).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }


        private DataTable Get卫生室门诊处方(string djid)
        {
            DataTable dt = null;
            try
            {
                string conStr = GetYthConStr();
                string sql = @"SELECT [F_DJBM]  单据编码
      ,[F_MC]   项目名称
      ,[F_ZL] 数量
      ,[F_UNIT]  单位
      ,[F_RULE]  规格
  FROM [yth].[dbo].[BC_FYMXB]
  where F_LBMC in('西药','中草药') and F_DJBM = @djbm";
                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@djbm", djid);
                dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql, sqlparams).Tables[0];
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        private string GetYthConStr()
        {
            string conStr = Config.ythstr;
            return conStr;
        }
    }
}
