﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace interfaceZljl.Report.DetailReport
{
    public partial class XtraReport门诊处方 : DevExpress.XtraReports.UI.XtraReport
    {
        //public XtraReport门诊处方(ClassYBJK.person _mzPerson, System.Drawing.Image _image)
        //{
        //    InitializeComponent();
        //    xrLabelCfid.Text = "处方号：" + _mzPerson.S处方号;
        //    xrLabel姓名.Text = "患者姓名:" + _mzPerson.S姓名;
        //    xrLabel科室.Text = "科室：" + _mzPerson.S科室;
        //    xrLabel临床诊断.Text = "临床诊断:" + _mzPerson.S临床诊断;
        //    xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
        //    xrLabel年龄.Text = "年龄: " + _mzPerson.S年龄.ToString();
        //    xrLabel性别.Text = "性别: " + _mzPerson.S性别;
        //    xrLabel医生.Text = "医生: " + _mzPerson.s医生姓名;
        //    xrLabel总金额.Text = "总金额: " + _mzPerson.Dec总金额.ToString();
        //    xrLabel日期.Text = "日期:" + _mzPerson.Dtime日期.ToShortDateString();
        //    xrLabelTitle.Text = TYK.zdInfo.iDwid == 109 ? "沂水县第二人民医院" : TYK.zdInfo.sDwmc;
        //    xrPictureBox医生签名.Image = _image;
        //}

        
        /// <summary>
        /// Web版使用了次构造函数
        /// </summary>
        /// <param name="str医院名称"></param>
        /// <param name="dt病人信息"></param>
        /// <param name="dt处方"></param>
        public XtraReport门诊处方(string str医院名称, DataTable dt病人信息, DataTable dt处方)
        {
            InitializeComponent();
            if (str医院名称 != null)
            {
                xrLabelTitle.Text = str医院名称;
            }

            if (dt病人信息 != null && dt病人信息.Rows.Count > 0)
            {
                //设置病人信息
                //xrLabelCfid.Text = chufang.Rows[0]["CFID"].ToString();
                xrLabel姓名.Text = "姓名：" + dt病人信息.Rows[0]["病人姓名"].ToString();
                xrLabel科室.Text = "科室：" + dt病人信息.Rows[0]["科室名称"].ToString();
                xrLabel临床诊断.Text = "诊断：" + dt病人信息.Rows[0]["诊断"].ToString();
                //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
                xrLabel年龄.Text = "年龄: " + dt病人信息.Rows[0]["年龄"].ToString();
                xrLabel性别.Text = "性别: " + dt病人信息.Rows[0]["性别"].ToString();
                xrLabel医生.Text = "医生: " + dt病人信息.Rows[0]["医生姓名"].ToString();
                xrLabel总金额.Text = "总金额: " + dt病人信息.Rows[0]["总费用"].ToString();
                xrLabel日期.Text = "日期:" + dt病人信息.Rows[0]["CreateTime"].ToString();
            }

            if (dt处方 != null && dt处方.Rows.Count > 0)
            {
                xrLabelCfid.Text = "处方编号：" + dt处方.Rows[0]["CFID"].ToString().Substring(9, 4);
                this.DataSource = dt处方;
            }
        }

        //此构造函数暂时未使用
        public XtraReport门诊处方(DataTable bingren, DataTable chufang)
        {
            InitializeComponent();
            xrLabelCfid.Text = chufang.Rows[0]["CFID"].ToString();
            xrLabel姓名.Text = bingren.Rows[0]["病人姓名"].ToString();
            xrLabel科室.Text = bingren.Rows[0]["科室名称"].ToString();
            xrLabel临床诊断.Text = bingren.Rows[0]["诊断"].ToString();
            //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
            xrLabel年龄.Text = "年龄: " + bingren.Rows[0]["年龄"].ToString();
            xrLabel性别.Text = "性别: " + bingren.Rows[0]["性别"].ToString();
            xrLabel医生.Text = "医生: " + bingren.Rows[0]["医生姓名"].ToString();
            xrLabel总金额.Text = "总金额: " + bingren.Rows[0]["总费用"].ToString();
            xrLabel日期.Text = "日期:" + bingren.Rows[0]["录入时间"].ToString();;
            xrLabelTitle.Text = bingren.Rows[0]["单位编码"].ToString()== "109" ? "沂水县第二人民医院" : bingren.Rows[0]["医院名称"].ToString();
            //xrPictureBox医生签名.Image = _image;

            this.DataSource = chufang;

        }


        //WinForm版使用了此构造函数
        public XtraReport门诊处方(string yyid, string mzid)
        {
            InitializeComponent();
            BindDataToReportHead(yyid, mzid);
        }
        
        private void BindDataToReportHead(string yyid, string mzid)
        {
            string conStr = GetHisConn();

            #region 查询处方信息,并绑定报表中
            try
            {
                //查询处方信息，并并绑定到报表中
                string sql处方 =
                      "SELECT aa.CFID,药品名称 项目名称," + "\r\n" +
                      "       药房规格 规格," + "\r\n" +
                      "       药房单位 单位," + "\r\n" +
                      "       剂量数量," + "\r\n" +
                      "       剂量单位," + "\r\n" +
                      "       频次," + "\r\n" +
                    //"       用法,[dbo].[ClearZero](处方总量) 数量,case when 用法 is not null then '用法' else '' end 用法提示" + "\r\n" +
                      "       用法,处方总量 数量,case when 用法 is not null then '用法' else '' end 用法提示" + "\r\n" +
                      "  FROM [CHIS" + yyid + "].dbo.MF处方明细 aa" + "\r\n" +
                      "       LEFT OUTER JOIN [CHIS" + yyid + "].dbo.MF处方摘要 bb ON aa.CFID = bb.CFID" + "\r\n" +
                      " WHERE bb.作废标记 = 0 and bb.MZID =" + mzid;
                DataTable dt处方 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql处方).Tables[0];

                this.DataSource = dt处方;

                xrLabelCfid.Text = dt处方.Rows[0]["CFID"].ToString();
            }
            catch
            {

            }
            #endregion

            #region 设置医院名称
            string deptname = "";
            if(yyid.Equals("2709") || yyid.Equals("109"))
            {
                deptname = "沂水县第二人民医院";
            }
            else
            {
                try
                {
                    string sql = @"select top 1 deptname from CHIS2701.dbo.中心院列表 where deptid = " + yyid;
                    
                    DataTable dt = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql).Tables[0];
                    deptname = dt.Rows[0][0].ToString();
                }
                catch
                {
                    deptname = "医院名称";
                }
            }
            xrLabelTitle.Text = deptname;

            if ((yyid != null) && (yyid.Equals("2709") || yyid.Equals("2719")))
            {
                xrLabel13.Text = "门 诊 处 方 笺";
            }
            else
            {
                xrLabel13.Text = "门 诊 用 药 记 录";
            }

            #endregion

            #region 设置病人信息
            try
            {
                //验证门诊摘要表中，是否存在“门诊诊断”“年龄单位”
                string sql验证 = "use CHIS" + yyid + @";
select '门诊诊断' 项目, COUNT(*) 次数 from syscolumns where id=object_id('[MF门诊摘要]') and name='门诊诊断'
union all 
select '年龄单位' 项目, COUNT(*) 次数 from syscolumns where id=object_id('[MF门诊摘要]') and name='年龄单位'";
                DataTable dt验证 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql验证).Tables[0];

                //用于查询门诊病人的信息
                string sql2 = "SELECT top 1 [MZID],[病人姓名],[性别]";
                if (dt验证.Rows[0]["次数"].ToString() == "1")
                {
                    sql2 += ",门诊诊断 诊断,[录入时间],";
                }
                else
                {
                    sql2 += ",临床诊断 诊断,[录入时间],";
                }

                if (dt验证.Rows[1]["次数"].ToString() == "1")
                {
                    sql2 += "cast(isnull([年龄],0) as varchar(20)) + isnull([年龄单位],'') 年龄,";
                }
                else
                {
                    sql2 += "cast(isnull([年龄],0) as varchar(20)) 年龄,";
                }
                sql2 += @"[总费用],bb.用户名 医生姓名, bb.科室名称
FROM [CHIS" + yyid + @"].dbo.[MF门诊摘要] aa
	left outer join [CHIS" + yyid + @"].dbo.pubUser bb on aa.医生编码 = bb.用户编码
where aa.MZID = '" + mzid + "'";
                DataTable dt病人信息 = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.Text, sql2).Tables[0];

                //设置病人信息
                //xrLabelCfid.Text = chufang.Rows[0]["CFID"].ToString();
                xrLabel姓名.Text = "姓名：" + dt病人信息.Rows[0]["病人姓名"].ToString();
                xrLabel科室.Text = "科室名称：" + dt病人信息.Rows[0]["科室名称"].ToString();
                xrLabel临床诊断.Text = "诊断:" + dt病人信息.Rows[0]["诊断"].ToString();
                //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
                xrLabel年龄.Text = "年龄: " + dt病人信息.Rows[0]["年龄"].ToString();
                xrLabel性别.Text = "性别: " + dt病人信息.Rows[0]["性别"].ToString();
                xrLabel医生.Text = "医生: " + dt病人信息.Rows[0]["医生姓名"].ToString();
                xrLabel总金额.Text = "总金额: " + dt病人信息.Rows[0]["总费用"].ToString();
                xrLabel日期.Text = "日期:" + dt病人信息.Rows[0]["录入时间"].ToString();
            }
            catch
            {
                //设置病人信息
                //xrLabelCfid.Text = chufang.Rows[0]["CFID"].ToString();
                xrLabel姓名.Text = "姓名：";
                xrLabel科室.Text = "科室名称：";
                xrLabel临床诊断.Text = "诊断：";
                //xrLabel发药人.Text = "发药人: ";// +TYK.zdInfo.UserInfo.UserName;
                xrLabel年龄.Text = "年龄：";
                xrLabel性别.Text = "性别：";
                xrLabel医生.Text = "医生：";
                xrLabel总金额.Text = "总金额：";
                xrLabel日期.Text = "日期：";
            }
            #endregion
        }

        private string GetHisConn()
        {
            string conStr = Config.hisStr;
            return conStr;
        }
    }
}
