﻿namespace AtomEHR.签约服务
{
    partial class frm签约服务主窗体
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm签约服务主窗体));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuMain签约服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCommonDataDict = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTestChild = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilReports
            // 
            this.ilReports.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilReports.ImageStream")));
            this.ilReports.Images.SetKeyName(0, "16_ArrayWhite.bmp");
            // 
            // pnlContainer
            // 
            this.pnlContainer.Controls.Add(this.menuStrip1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMain签约服务});
            this.menuStrip1.Location = new System.Drawing.Point(2, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(678, 25);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // menuMain签约服务
            // 
            this.menuMain签约服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCommonDataDict,
            this.menuItemTestChild});
            this.menuMain签约服务.Name = "menuMain签约服务";
            this.menuMain签约服务.Size = new System.Drawing.Size(68, 21);
            this.menuMain签约服务.Text = "签约服务";
            // 
            // menuCommonDataDict
            // 
            this.menuCommonDataDict.Name = "menuCommonDataDict";
            this.menuCommonDataDict.Size = new System.Drawing.Size(152, 22);
            this.menuCommonDataDict.Text = "签约管理";
            // 
            // menuItemTestChild
            // 
            this.menuItemTestChild.Name = "menuItemTestChild";
            this.menuItemTestChild.Size = new System.Drawing.Size(152, 22);
            this.menuItemTestChild.Text = "测试窗体";
            // 
            // frm签约服务主窗体
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(682, 397);
            this.Name = "frm签约服务主窗体";
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuMain签约服务;
        private System.Windows.Forms.ToolStripMenuItem menuCommonDataDict;
        private System.Windows.Forms.ToolStripMenuItem menuItemTestChild;
    }
}
