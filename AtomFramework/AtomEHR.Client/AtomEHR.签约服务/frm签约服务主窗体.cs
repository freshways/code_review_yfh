﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Interfaces;

namespace AtomEHR.签约服务
{
    public partial class frm签约服务主窗体 : AtomEHR.Library.frmModuleBase
    {
        public frm签约服务主窗体()
        {
            InitializeComponent();

            _ModuleID = ModuleID.签约服务; //设置模块编号
            _ModuleName = ModuleNames.签约服务;//设置模块名称
            menuMain签约服务.Text = ModuleNames.签约服务; //与AssemblyModuleEntry.ModuleName定义相同

            this.MainMenuStrip = this.menuStrip1;

            //this.SetMenuTag();
        }

        /// <summary>
        /// 重写该方法，返回模块主菜单对象.
        /// </summary>
        /// <returns></returns>
        public override MenuStrip GetModuleMenu()
        {
            return this.menuStrip1;
        }

        /// <summary>
        /// 设置菜单标记。初始化窗体的可用权限
        /// 请参考MenuItemTag类定义
        /// </summary>
        private void SetMenuTag(ToolStripItem tl)
        {
            tl.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.签约服务, AuthorityCategory.NONE);
            if (tl is ToolStripMenuItem)
                foreach (ToolStripItem tls in ((ToolStripMenuItem)tl).DropDownItems)
                {
                    SetMenuTag(tls);
                }

        }

        /// <summary>
        /// 设置模块主界面的权限
        /// </summary>        
        public override void SetSecurity(object securityInfo)
        {
            base.SetSecurity(securityInfo);

            if (securityInfo is ToolStrip)
            {
                //设置按钮权限
                //btnEdit.Enabled = MenuItemEdit.Enabled;
                //btnBusEdit.Enabled = MenuItemBusEdit.Enabled;
            }
        }
    }
}
