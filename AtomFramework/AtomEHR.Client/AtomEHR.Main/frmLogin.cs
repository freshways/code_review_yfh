
///*************************************************************************/
///*
///* 文件名    ：frmLogin.cs    
///*
///* 程序说明  : 登录窗体
///*              登录成功后加载MDI主窗体，同时加载进度在登录窗体内显示
///
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Business.Security;
using AtomEHR.Core;
using AtomEHR.Interfaces;
using System.IO;
using AtomEHR.Models;
using AtomEHR.Bridge;
using System.Net;

namespace AtomEHR.Main
{
    /// <summary>
    /// 登录窗体
    /// </summary>
    public partial class frmLogin : frmBase
    {
        private ILoginAuthorization _CurrentAuthorization = null;//当前授权模式
        private bool _Version = true; //是否允许不升级登陆

        /// <summary>
        /// 私有构造器
        /// </summary>
        private frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            InitLoginWindow(); //初始化登陆窗体
            this.lab版本.Text = "版本:" + VersionCheck.GetVersion();
        }

        /// <summary>
        /// 显示登陆窗体.(公共静态方法)
        /// </summary>        
        public static bool Login(string IDcard)
        {
            if (string.IsNullOrEmpty(IDcard))
            {
                frmLogin form = new frmLogin();
                DialogResult result = form.ShowDialog();
                bool ret = (result == DialogResult.OK) && (SystemAuthentication.Current != null);
                return ret;
            }
            else
            {//此处是单点登陆功能，传入一个唯一标识，开始验证登陆
                try
                {
                    SystemConfig.ReadSettings(); //读取用户自定义设置

                    DevExpress.UserSkins.BonusSkins.Register();//注册Dev酷皮肤
                    DevExpress.Skins.SkinManager.EnableFormSkins();//启用窗体支持换肤特性

                    frmLogin form = new frmLogin();
                    if (!BridgeFactory.InitializeBridge())
                    {
                        Msg.Warning("初始化失败!");
                        return false;
                    }

                    //第一步： 传入的身份证号不为空，去数据库检索此身份证号。
                    DataTable dtLogin = CommonData.GetLoginPersonalInfo(IDcard);
                    if (dtLogin == null || dtLogin.Rows.Count <= 0)
                    {
                        Msg.Warning("登录失败，未找到用户!\n" + IDcard);
                        return false;
                    }
                    //第二步： 获得此身份证号对应的用户名密码，初始化登陆参数。
                    string userID = dtLogin.Rows[0][TUser.Account].ToString();
                    string password = dtLogin.Rows[0][TUser.Password].ToString();
                    string dataSetID = "YSDB"; //txtDataset.EditValue.ToString();//帐套编号
                    string dataSetDB = "AtomEHR.YSDB";

                    LoginUser loginUser = new LoginUser(userID, password, dataSetID, dataSetDB, VersionCheck.GetVersion(),"");
                    form._CurrentAuthorization = new LoginSystemAuth();
                    //第三步： 调用登陆验证方法。
                    if (form._CurrentAuthorization.Login(loginUser)) //调用登录策略
                    {
                        SystemAuthentication.Current = form._CurrentAuthorization; //授权成功, 保存当前授权模式

                        Program.MainForm = new frmMain();//登录成功创建主窗体    
                        Program.MainForm.InitUserInterface();
                        Program.MainForm.Show();
                    }
                }
                catch(Exception  ex)
                {
                    Msg.Warning("登录失败!失败原因:\n"+ex.Message);
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 初始化登陆窗体, 创建登录策略
        /// </summary>
        private void InitLoginWindow()
        {
            this.BindingDataSet();//绑定帐套数据源
            this.BindingVersion();//获取限定版本信息，判断是否强制升级
            this.ReadLoginInfo(); //读取保存的登录信息
            this.Text = SystemConfig.CurrentConfig.ApplicationName + " - [" + BridgeFactory.BridgeType.ToString() + "]";//后台连接:

            SystemAuthentication.Current = null;

            LoginAuthType authType = (LoginAuthType)SystemConfig.CurrentConfig.LoginAuthType; //系统设置
            if (authType == LoginAuthType.LocalSystemAuth)
            {
                _CurrentAuthorization = new LoginSystemAuth();
                btnLogin.Text = "登录 (&L)";
            }
            else if (authType == LoginAuthType.NovellUserAuth)
            {
                _CurrentAuthorization = new LoginNovellAuth(txtUser, lblLoadingInfo);
                btnLogin.Text = "Novell自动登录";

                this.txtPwd.Enabled = false;
                this.txtUser.Enabled = false;
                this.Visible = true;
                this.Update();//自动显示窗体

                btnLogin.PerformClick();//直接登录系统
            }
            else if (authType == LoginAuthType.WindowsDomainAuth)
            {
                _CurrentAuthorization = new LoginWindowsDomainAuth(txtUser, lblLoadingInfo);
                btnLogin.Text = "域用户自动登录";

                this.txtPwd.Enabled = false;
                this.txtUser.Enabled = false;
                this.Visible = true;
                this.Update();//自动显示窗体

                btnLogin.PerformClick();//直接登录系统
            }
            else //不明类型,禁止登录
            {
                pcInputArea.Enabled = false;
                btnLogin.Enabled = false;
            }

        }

        private void ReadLoginInfo()
        {
            //存在用户配置文件，自动加载登录信息
            string cfgINI = Application.StartupPath + BridgeFactory.INI_CFG;
            if (File.Exists(cfgINI))
            {
                IniFile ini = new IniFile(cfgINI);
                txtUser.Text = ini.IniReadValue("LoginWindow", "User");
                txtDataset.EditValue = ini.IniReadValue("LoginWindow", "DataSetID");
                txtPwd.Text = CEncoder.Decode(ini.IniReadValue("LoginWindow", "Password"));
                chkSaveLoginInfo.Checked = ini.IniReadValue("LoginWindow", "SaveLogin") == "Y";
            }
        }

        private void SaveLoginInfo()
        {
            //存在用户配置文件，自动加载登录信息
            string cfgINI = Application.StartupPath + BridgeFactory.INI_CFG;

            IniFile ini = new IniFile(cfgINI);
            ini.IniWriteValue("LoginWindow", "User", txtUser.Text);
            ini.IniWriteValue("LoginWindow", "DataSetID", txtDataset.EditValue.ToString());
            ini.IniWriteValue("LoginWindow", "Password", CEncoder.Encode(txtPwd.Text));
            ini.IniWriteValue("LoginWindow", "SaveLogin", chkSaveLoginInfo.Checked ? "Y" : "N");
        }

        private void BindingDataSet()
        {
            DataTable data = CommonData.GetSystemDataSet();
            DataBinder.BindingLookupEditDataSource(txtDataset, data, "DataSetName", "DataSetID");
        }

        /// <summary>
        /// 2015-11-09 15:32:49 yufh 添加-获取限定版本信息
        /// </summary>
        private void BindingVersion()
        {
            DataTable data = CommonData.GetVersion();
            if (data == null || data.Rows.Count <= 0) return;
            if (Convert.ToBoolean(data.Rows[0]["强制升级"]))
            {
                Int32 olvs = Convert.ToInt32(VersionCheck.GetVersion().Replace(".", ""));
                Int32 newvs = Convert.ToInt32(data.Rows[0]["限定版本"].ToString().Replace(".", ""));
                if (newvs > olvs)
                {
                    UpdatePic_Click(null, null);
                    _Version = false;
                }
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_Version) { Msg.Warning("请升级后再进行登陆！"); return; }
                this.Cursor = Cursors.WaitCursor;
                this.SetButtonEnable(false);
                this.Update();//必须
                this.ShowLoginInfo("正在验证用户名及密码");

                bllUser.ValidateLogin(txtUser.Text, txtPwd.Text);//检查登录信息

                string userID = txtUser.Text;
                string password = CEncoder.Encode(txtPwd.Text);/*常规加密*/
                string dataSetID = txtDataset.EditValue.ToString();//帐套编号
                string dataSetDB = GetDataSetDBName();

                string hostName = Dns.GetHostName();   //获取本机名
                IPHostEntry localhost = Dns.GetHostByName(hostName);    //方法已过期，可以获取IPv4的地址
                string ip = localhost.AddressList[0].ToString() + "(" + hostName + ")";
                LoginUser loginUser = new LoginUser(userID, password, dataSetID, dataSetDB, lab版本.Text, ip);

                if (_CurrentAuthorization.Login(loginUser)) //调用登录策略
                {
                    if (chkSaveLoginInfo.Checked) this.SaveLoginInfo();//跟据选项保存登录信息                    
                    SystemAuthentication.Current = _CurrentAuthorization; //授权成功, 保存当前授权模式
                    Program.MainForm = new frmMain();//登录成功创建主窗体                    
                    Program.MainForm.InitUserInterface(new LoadStatus(lblLoadingInfo));
                    this.DialogResult = DialogResult.OK; //成功
                    this.Close(); //关闭登陆窗体
                }
                else
                {
                    this.ShowLoginInfo("登录失败，请检查用户名和密码!");
                    Msg.Warning("登录失败，请检查用户名和密码!");
                }
            }
            catch (CustomException ex)
            {
                this.SetButtonEnable(true);
                this.ShowLoginInfo(ex.Message);
                Msg.Warning(ex.Message);
            }
            catch
            {
                this.SetButtonEnable(true);
                this.ShowLoginInfo("登录失败，请检查用户名和密码!");
                Msg.ShowError("登录失败，请检查用户名和密码!");
            }

            this.Cursor = Cursors.Default;
        }

        private string GetDataSetDBName()
        {
            DataRowView view = (DataRowView)txtDataset.Properties.GetDataSourceRowByKeyValue(txtDataset.EditValue);
            return ConvertEx.ToString(view.Row["DBName"]);
        }


        private void ShowLoginInfo(string info)
        {
            lblLoadingInfo.Text = info;
            lblLoadingInfo.Update();
        }

        private void SetButtonEnable(bool value)
        {
            btnLogin.Enabled = value;
            btnCancel.Enabled = value;
            btnLogin.Update();
            btnCancel.Update();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Msg.AskQuestion("确定要退出系统吗？")) Application.Exit();
        }

        private void btnModifyPwd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoginUser user = new LoginUser();
            user.Account = txtUser.Text;
            user.DataSetID = txtDataset.EditValue.ToString();
            user.DataSetDBName = GetDataSetDBName();
            frmModifyPwd.Execute(user, ModifyPwdType.LoginWindowDirect);
        }

        #region 扩展
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                if (this.ActiveControl.Parent == txtPwd || this.ActiveControl == btnLogin)
                    this.btnLogin.PerformClick();
                else
                    SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }
        
        /// <summary>
        /// 2015-10-20 16:43:30 yufh 添加升级功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdatePic_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                Msg.ShowInformation("升级工具丢失，请联系软件维护人员处理故障!");
                return;
            }
            Process.Start("uplistdo.exe");
        }
        
        #endregion

    }
}