using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using AtomEHR.Common;
using System.Diagnostics;
using AtomEHR.Business;
using DevExpress.UserSkins;
using DevExpress.Skins;
using AtomEHR.Core;
using AtomEHR.Bridge;
using SharpRaven;
using SharpRaven.Data;
using NetDimension.NanUI;

/************************************************************************* 
 * 程序说明: 程序入口
 * 注意初始化程序的代码顺序!!! 
 * 作者：ATOM
 **************************************************************************/

namespace AtomEHR.Main
{
    static class Program
    {
        [STAThread]
        static void Main(string[] arge)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(new frmTest());

            #region 根据操作系统是否初始化CEF
            //获取操作系统信息
            System.OperatingSystem osInfo = System.Environment.OSVersion;

            //获取操作系统ID
            System.PlatformID platformID = osInfo.Platform;

            //获取主版本号
            int versionMajor = osInfo.Version.Major;

            //获取副版本号
            int versionMinor = osInfo.Version.Minor;

            //指定CEF架构和文件目录结构，并初始化CEF
            if (versionMajor >= 6)
            {
                try
                {//为了不影响主线程，添加异常判断，初始化失败的直接跳过不做处理
                    Bootstrap.Load(settings =>
                    {
                        //禁用日志
                        settings.LogSeverity = Chromium.CfxLogSeverity.Disable;

                        //指定中文为当前CEF环境的默认语言
                        settings.AcceptLanguageList = "zh-CN";
                        settings.Locale = "zh-CN";
                    }, commandLine =>
                    {
                        //在启动参数中添加disable-web-security开关，禁用跨域安全检测
                        commandLine.AppendSwitch("disable-web-security");

                    });
                }
                catch
                {
                }
            }
            #endregion

            #region MyRegion

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);//捕获系统所产生的异常。
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);

            //Program.CheckInstance();//检查程序是否运行多实例
            SystemConfig.ReadSettings(); //读取用户自定义设置
            //检查程序是否运行 CheckInstanc() 方法无效，单独写才有效
            Boolean createdNew; //返回是否赋予了使用线程的互斥体初始所属权
            Mutex instance = new Mutex(true, Globals.DEF_PROGRAM_NAME, out createdNew); //同步基元变量
            if (createdNew) //首次使用互斥体
            {
                instance.ReleaseMutex();
            }
            else
            {
                if (!SystemConfig.CurrentConfig.AllowRunMultiInstance)
                {
                    Msg.Warning("已经启动了一个程序，请先退出！");
                    Application.Exit();
                    return;
                }
            }

            if (false == BridgeFactory.InitializeBridge())//初始化桥接功能
            {
                Application.Exit();
                return;
            }

            BonusSkins.Register();//注册Dev酷皮肤
            //OfficeSkins.Register();////注册Office样式的皮肤
            SkinManager.EnableFormSkins();//启用窗体支持换肤特性
            //arge = new string[] { "123456789012345678" };
            //注意：先打开登陆窗体,登陆成功后正式运行程序(MDI主窗体)
            if (arge.Length > 0)
            {
                frmLogin.Login(arge[0]);
                Application.Run();
            }
            else if (frmLogin.Login(null))
            {
                Program.MainForm.Show();
                Application.Run();
            }
            else//登录失败,退出程序
                Application.Exit();

            #endregion
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            if (SystemAuthentication.Current != null)
                SystemAuthentication.Current.Logout(); //登出
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            var ravenClient = new RavenClient("http://cab7f574a9c04e3cba733731e70ff7ac@192.168.10.26:9000/12");
            try
            {
                ravenClient.Capture(new SentryEvent(e.Exception));
            }
            catch (Exception)
            {
            }
            Msg.ShowException(e.Exception);//处理系统异常
        }

        private static frmMain _mainForm = null;

        /// <summary>
        /// MDI主窗体
        /// </summary>        
        public static frmMain MainForm { get { return _mainForm; } set { _mainForm = value; } }

        /// <summary>
        ///检查程序是否运行多实例
        /// </summary>
        public static void CheckInstance()
        {
            Boolean createdNew; //返回是否赋予了使用线程的互斥体初始所属权
            Mutex instance = new Mutex(true, Globals.DEF_PROGRAM_NAME, out createdNew); //同步基元变量
            if (createdNew) //首次使用互斥体
            {
                instance.ReleaseMutex();
            }
            else
            {
                if (!SystemConfig.CurrentConfig.AllowRunMultiInstance)
                {
                    Msg.Warning("已经启动了一个程序，请先退出！");
                    Application.Exit();
                    return;
                }
            }
        }
    }
}