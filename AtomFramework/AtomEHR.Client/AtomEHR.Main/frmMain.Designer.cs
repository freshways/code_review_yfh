namespace AtomEHR.Main
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ilModule = new System.Windows.Forms.ImageList(this.components);
            this.ilModuleIcon = new System.Windows.Forms.ImageList(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barMainMenu = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnAbout = new DevExpress.XtraBars.BarButtonItem();
            this.menuDeveloper = new DevExpress.XtraBars.BarButtonItem();
            this.barMdiChildrenListItem1 = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.btnSetSkin = new DevExpress.XtraBars.BarSubItem();
            this.btnUploadFiles = new DevExpress.XtraBars.BarSubItem();
            this.btnExit = new DevExpress.XtraBars.BarSubItem();
            this.barToolButtons = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lab标题 = new DevExpress.XtraEditors.LabelControl();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.btnLogout = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefreshCache = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.btnDoHelp = new DevExpress.XtraBars.BarButtonItem();
            this.btnConnStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.menuStripCloseForm = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuCloseThis = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCloseAll = new System.Windows.Forms.ToolStripMenuItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            this.menuStripCloseForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilModule
            // 
            this.ilModule.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilModule.ImageStream")));
            this.ilModule.TransparentColor = System.Drawing.Color.Transparent;
            this.ilModule.Images.SetKeyName(0, "32_Stock.ico");
            // 
            // ilModuleIcon
            // 
            this.ilModuleIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilModuleIcon.ImageStream")));
            this.ilModuleIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ilModuleIcon.Images.SetKeyName(0, "m1.ico");
            this.ilModuleIcon.Images.SetKeyName(1, "submodule.ico");
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.xtraTabbedMdiManager1_MouseUp);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "16_CustomerQuery.ico");
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMainMenu,
            this.barToolButtons});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this.panelControl1;
            this.barManager1.Images = this.imageList1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barMdiChildrenListItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.btnLogout,
            this.btnAbout,
            this.btnSetSkin,
            this.menuDeveloper,
            this.btnRefreshCache,
            this.barStaticItem2,
            this.btnDoHelp,
            this.btnConnStatus,
            this.barButtonItem2,
            this.barButtonItem3,
            this.btnExit,
            this.btnUploadFiles});
            this.barManager1.MainMenu = this.barMainMenu;
            this.barManager1.MaxItemId = 22;
            // 
            // barMainMenu
            // 
            this.barMainMenu.BarName = "Main menu";
            this.barMainMenu.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.barMainMenu.DockCol = 0;
            this.barMainMenu.DockRow = 0;
            this.barMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barMdiChildrenListItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSetSkin),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnUploadFiles),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExit)});
            this.barMainMenu.OptionsBar.AllowQuickCustomization = false;
            this.barMainMenu.OptionsBar.DrawDragBorder = false;
            this.barMainMenu.OptionsBar.UseWholeRow = true;
            this.barMainMenu.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "关于";
            this.barSubItem1.Glyph = global::AtomEHR.Main.Properties.Resources.cslogo16;
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAbout),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuDeveloper)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnAbout
            // 
            this.btnAbout.Caption = "关于C/S开发框架";
            this.btnAbout.Glyph = global::AtomEHR.Main.Properties.Resources.cslogo16;
            this.btnAbout.Id = 7;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAbout_ItemClick);
            // 
            // menuDeveloper
            // 
            this.menuDeveloper.Caption = "开发人员";
            this.menuDeveloper.Glyph = global::AtomEHR.Main.Properties.Resources._2009011340768021;
            this.menuDeveloper.Id = 11;
            this.menuDeveloper.Name = "menuDeveloper";
            // 
            // barMdiChildrenListItem1
            // 
            this.barMdiChildrenListItem1.Caption = "窗口";
            this.barMdiChildrenListItem1.Glyph = global::AtomEHR.Main.Properties.Resources.childforms1;
            this.barMdiChildrenListItem1.Id = 1;
            this.barMdiChildrenListItem1.Name = "barMdiChildrenListItem1";
            this.barMdiChildrenListItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnSetSkin
            // 
            this.btnSetSkin.Caption = "设置皮肤";
            this.btnSetSkin.Glyph = global::AtomEHR.Main.Properties.Resources.skin16;
            this.btnSetSkin.Id = 10;
            this.btnSetSkin.Name = "btnSetSkin";
            this.btnSetSkin.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnUploadFiles
            // 
            this.btnUploadFiles.Caption = "检查升级";
            this.btnUploadFiles.Glyph = ((System.Drawing.Image)(resources.GetObject("btnUploadFiles.Glyph")));
            this.btnUploadFiles.Id = 21;
            this.btnUploadFiles.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnUploadFiles.LargeGlyph")));
            this.btnUploadFiles.Name = "btnUploadFiles";
            this.btnUploadFiles.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnUploadFiles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUploadFiles_ItemClick);
            // 
            // btnExit
            // 
            this.btnExit.Caption = "退出系统";
            this.btnExit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExit.Glyph")));
            this.btnExit.Id = 20;
            this.btnExit.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnExit.LargeGlyph")));
            this.btnExit.Name = "btnExit";
            this.btnExit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // barToolButtons
            // 
            this.barToolButtons.BarName = "Tools";
            this.barToolButtons.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barToolButtons.DockCol = 0;
            this.barToolButtons.DockRow = 0;
            this.barToolButtons.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barToolButtons.OptionsBar.AllowQuickCustomization = false;
            this.barToolButtons.OptionsBar.DisableClose = true;
            this.barToolButtons.OptionsBar.DisableCustomization = true;
            this.barToolButtons.OptionsBar.DrawDragBorder = false;
            this.barToolButtons.OptionsBar.UseWholeRow = true;
            this.barToolButtons.Text = "Tools";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(2, 2);
            this.barDockControlTop.Size = new System.Drawing.Size(897, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(2, 76);
            this.barDockControlBottom.Size = new System.Drawing.Size(897, 29);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(2, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 50);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(899, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 50);
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(142)))), ((int)(((byte)(244)))));
            this.panelControl1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(124)))), ((int)(((byte)(215)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl1.ContentImage")));
            this.panelControl1.ContentImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.panelControl1.Controls.Add(this.lab标题);
            this.panelControl1.Controls.Add(this.barDockControlLeft);
            this.panelControl1.Controls.Add(this.barDockControlRight);
            this.panelControl1.Controls.Add(this.barDockControlBottom);
            this.panelControl1.Controls.Add(this.barDockControlTop);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(901, 107);
            this.panelControl1.TabIndex = 11;
            // 
            // lab标题
            // 
            this.lab标题.Appearance.Font = new System.Drawing.Font("微软雅黑", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab标题.Appearance.ForeColor = System.Drawing.Color.White;
            this.lab标题.Location = new System.Drawing.Point(78, 37);
            this.lab标题.Name = "lab标题";
            this.lab标题.Size = new System.Drawing.Size(300, 33);
            this.lab标题.TabIndex = 4;
            this.lab标题.Text = "阳光软件基本公共卫生系统";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "关于C/S结构系统框架";
            this.barSubItem2.Id = 2;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "技术支持：临沂市阳光科技有限公司";
            this.barSubItem3.Id = 3;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // btnLogout
            // 
            this.btnLogout.Caption = "登出(Logout)";
            this.btnLogout.Id = 5;
            this.btnLogout.ImageIndex = 0;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLogout_ItemClick);
            // 
            // btnRefreshCache
            // 
            this.btnRefreshCache.Caption = "更新缓存数据";
            this.btnRefreshCache.Id = 13;
            this.btnRefreshCache.Name = "btnRefreshCache";
            this.btnRefreshCache.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefreshCache_ItemClick);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "帐套";
            this.barStaticItem2.Id = 14;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnDoHelp
            // 
            this.btnDoHelp.Caption = "帮助";
            this.btnDoHelp.Id = 15;
            this.btnDoHelp.Name = "btnDoHelp";
            // 
            // btnConnStatus
            // 
            this.btnConnStatus.Caption = "连接状态";
            this.btnConnStatus.Id = 16;
            this.btnConnStatus.Name = "btnConnStatus";
            this.btnConnStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "后退";
            this.barButtonItem2.Id = 18;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "前进";
            this.barButtonItem3.Id = 19;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
            this.barManager2.MaxItemId = 2;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLogout, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnStatus, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshCache),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDoHelp, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "          技术支持：临沂市阳光科技有限公司          ";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(901, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 660);
            this.barDockControl2.Size = new System.Drawing.Size(901, 27);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 660);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(901, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 660);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = null;
            this.navBarControl1.ContentButtonHint = null;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl1.Location = new System.Drawing.Point(0, 107);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 255;
            this.navBarControl1.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControl1.Size = new System.Drawing.Size(255, 553);
            this.navBarControl1.StoreDefaultPaintStyleName = true;
            this.navBarControl1.TabIndex = 0;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // menuStripCloseForm
            // 
            this.menuStripCloseForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripCloseForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCloseThis,
            this.menuCloseAll});
            this.menuStripCloseForm.Name = "menuStripCloseForm";
            this.menuStripCloseForm.Size = new System.Drawing.Size(173, 48);
            // 
            // menuCloseThis
            // 
            this.menuCloseThis.Name = "menuCloseThis";
            this.menuCloseThis.Size = new System.Drawing.Size(172, 22);
            this.menuCloseThis.Text = "关闭当前窗体";
            this.menuCloseThis.Click += new System.EventHandler(this.menuCloseThis_Click);
            // 
            // menuCloseAll
            // 
            this.menuCloseAll.Name = "menuCloseAll";
            this.menuCloseAll.Size = new System.Drawing.Size(172, 22);
            this.menuCloseAll.Text = "除此之外全部关闭";
            this.menuCloseAll.Click += new System.EventHandler(this.menuCloseAll_Click);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(255, 107);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 553);
            this.splitterControl1.TabIndex = 16;
            this.splitterControl1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(901, 687);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.navBarControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = ".Net快速开发框架 - 主窗体";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            this.menuStripCloseForm.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        protected System.Windows.Forms.ImageList ilModuleIcon;
        private System.Windows.Forms.ImageList ilModule;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barToolButtons;
        private DevExpress.XtraBars.Bar barMainMenu;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarMdiChildrenListItem barMdiChildrenListItem1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem btnLogout;
        private DevExpress.XtraBars.BarButtonItem btnAbout;
        private DevExpress.XtraBars.BarSubItem btnSetSkin;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraBars.BarButtonItem menuDeveloper;
        private DevExpress.XtraBars.BarButtonItem btnRefreshCache;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarButtonItem btnDoHelp;
        private DevExpress.XtraBars.BarStaticItem btnConnStatus;
        private System.Windows.Forms.ContextMenuStrip menuStripCloseForm;
        private System.Windows.Forms.ToolStripMenuItem menuCloseThis;
        private System.Windows.Forms.ToolStripMenuItem menuCloseAll;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraBars.BarSubItem btnExit;
        private DevExpress.XtraBars.BarSubItem btnUploadFiles;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.LabelControl lab标题;
    }
}

