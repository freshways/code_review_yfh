﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_System;

namespace AtomEHR.Server.DataAccess
{
    public class dal慢性病统计分析
    {
        Loginer _Loginer = new Loginer();
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal慢性病统计分析(Loginer loginer)
        {
            _Loginer = loginer;
        }

        public DataTable GetMain(string age)
        {
            string strSql = @"select * from (select '高血压' 类型,count(1) 人数
from tb_MXB高血压管理卡 where isnull(终止管理,2)=2
and  not exists(select distinct * from (
select 个人档案编号 from tb_MXB糖尿病管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡 )a where a.个人档案编号 = tb_MXB高血压管理卡.个人档案编号)
 and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '糖尿病' 类型,count(1) 人数 
from tb_MXB糖尿病管理卡 where isnull(终止管理,2)=2
and  not exists(select distinct * from (
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡  )a where a.个人档案编号 = tb_MXB糖尿病管理卡.个人档案编号)
 and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '冠心病' 类型,count(1) 人数 
from tb_MXB冠心病管理卡 where isnull(终止管理,2)=2
and  not exists(select distinct * from (
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB糖尿病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡)a where a.个人档案编号 = tb_MXB冠心病管理卡.个人档案编号)
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '脑卒中' 类型,count(1) 人数 
from tb_MXB脑卒中管理卡 where isnull(终止管理,2)=2
and  not exists(select distinct * from (
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB冠心病管理卡)a where a.个人档案编号 = tb_MXB脑卒中管理卡.个人档案编号)
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
) a where 1=1";

            switch (age)
            {
                case "65岁及以上老年人":
                    strSql = string.Format(strSql, ">=65");
                    break;
                case "65岁以下人群":
                    strSql = string.Format(strSql, "<65");
                    break;
                default:
                    strSql = string.Format(strSql, ">0");
                    break;
            }

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#慢性病");
        }

        public DataTable Get高血压人群分析(string types,string age)
        {
            string where = string.Empty;
            string[] mbType = types.Split(';');
            foreach (string s in mbType)
            {
                where += "'" + s.Trim() + "',";
            }
            where = where.Substring(0, where.Length - 1);

            string SQL = @"select * from (select '高血压' 类型,count(1) 人数
from tb_MXB高血压管理卡 where isnull(终止管理,2)=2 and 个人档案编号 not in (
select 个人档案编号 from tb_MXB糖尿病管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡 ) 
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '糖尿病' 类型,count(1) 人数 from tb_MXB高血压管理卡 
where isnull(终止管理,2)=2 and 个人档案编号 in(select 个人档案编号 from tb_MXB糖尿病管理卡  )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '冠心病' 类型,count(1) 人数 from tb_MXB高血压管理卡 
where isnull(终止管理,2)=2 and 个人档案编号 in(select 个人档案编号 from tb_MXB冠心病管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '脑卒中' 类型,count(1) 人数 from tb_MXB高血压管理卡 
where isnull(终止管理,2)=2  and 个人档案编号 in(select 个人档案编号 from tb_MXB脑卒中管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
) a  where 1=1 ";
            if (!string.IsNullOrEmpty(where))
                SQL += "and 类型 in(" + where + ")";

            switch (age)
            {
                case "65岁以上老年人":                    
                    SQL = string.Format(SQL, ">=65");
                    break;
                case "65岁以下人群":
                    SQL = string.Format(SQL, "<65");
                    break;
                default:
                    SQL = string.Format(SQL, ">0");
                    break;
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);
            

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#高血压");
        }

        public DataTable Get糖尿病人群分析(string types,string age)
        {
            string where = string.Empty;
            string[] mbType = types.Split(';');
            foreach (string s in mbType)
            {
                where += "'" + s.Trim() + "',";
            }
            where = where.Substring(0, where.Length - 1);

            string SQL = @"select * from (select '高血压' 类型,count(1) 人数 
from tb_MXB糖尿病管理卡 where isnull(终止管理,2)=2 
and 个人档案编号  in (select 个人档案编号 from tb_MXB高血压管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all
select '糖尿病' 类型,count(1) 人数 
from tb_MXB糖尿病管理卡 where isnull(终止管理,2)=2 and 个人档案编号 not in(
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '冠心病' 类型,count(1) 人数 
from tb_MXB糖尿病管理卡 where isnull(终止管理,2)=2 and 个人档案编号 in(select 个人档案编号 from tb_MXB冠心病管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '脑卒中' 类型,count(1) 人数 
from tb_MXB糖尿病管理卡 where isnull(终止管理,2)=2  and 个人档案编号 in(select 个人档案编号 from tb_MXB脑卒中管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
) a  where 1=1 ";
            if (!string.IsNullOrEmpty(where))
                SQL += "and 类型 in(" + where + ")";
            switch (age)
            {
                case "65岁以上老年人":
                    SQL = string.Format(SQL, ">=65");
                    break;
                case "65岁以下人群":
                    SQL = string.Format(SQL, "<65");
                    break;
                default:
                    SQL = string.Format(SQL, ">0");
                    break;
            }

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#糖尿病");
        }

        public DataTable Get冠心病人群分析(string types,string age)
        {
            string where = string.Empty;
            string[] mbType = types.Split(';');
            foreach (string s in mbType)
            {
                where += "'" + s.Trim() + "',";
            }
            where = where.Substring(0, where.Length - 1);

            string SQL = @"select * from (select '高血压' 类型,count(1) 人数 
from tb_MXB冠心病管理卡 where isnull(终止管理,2)=2 and 个人档案编号  in(select 个人档案编号 from tb_MXB高血压管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '糖尿病' 类型,count(1) 人数 from tb_MXB冠心病管理卡 where isnull(终止管理,2)=2 
and 个人档案编号  in(select 个人档案编号 from tb_MXB糖尿病管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '冠心病' 类型,count(1) 人数 from tb_MXB冠心病管理卡 where isnull(终止管理,2)=2 and 个人档案编号 not in(
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB糖尿病管理卡 union 
select 个人档案编号 from tb_MXB脑卒中管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '脑卒中' 类型,count(1) 人数 from tb_MXB冠心病管理卡 where isnull(终止管理,2)=2  
and 个人档案编号 in(select 个人档案编号 from tb_MXB脑卒中管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
) a  where 1=1 ";
            if (!string.IsNullOrEmpty(where))
                SQL += "and 类型 in(" + where + ")";

            switch (age)
            {
                case "65岁以上老年人":
                    SQL = string.Format(SQL, ">=65");
                    break;
                case "65岁以下人群":
                    SQL = string.Format(SQL, "<65");
                    break;
                default:
                    SQL = string.Format(SQL, ">0");
                    break;
            }

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#冠心病");
        }

        public DataTable Get脑卒中人群分析(string types,string age)
        {
            string where = string.Empty;
            string[] mbType = types.Split(';');
            foreach (string s in mbType)
            {
                where += "'" + s.Trim() + "',";
            }
            where = where.Substring(0, where.Length - 1);

            string SQL = @"select * from (select '高血压' 类型,count(1) 人数 
from tb_MXB脑卒中管理卡 where isnull(终止管理,2)=2 and 个人档案编号  in (select 个人档案编号 from tb_MXB高血压管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '糖尿病' 类型,count(1) 人数 
from tb_MXB脑卒中管理卡 where isnull(终止管理,2)=2 and 个人档案编号 in(
select 个人档案编号 from tb_MXB糖尿病管理卡  )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '冠心病' 类型,count(1) 人数 
from tb_MXB脑卒中管理卡 where isnull(终止管理,2)=2 and 个人档案编号 in(select 个人档案编号 from tb_MXB冠心病管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
union all 
select '脑卒中' 类型,count(1) 人数 
from tb_MXB脑卒中管理卡 where isnull(终止管理,2)=2  and 个人档案编号 not in(
select 个人档案编号 from tb_MXB高血压管理卡 union select 个人档案编号 from tb_MXB冠心病管理卡 union 
select 个人档案编号 from tb_MXB冠心病管理卡 )
and 个人档案编号 in(select 个人档案编号 from tb_健康档案 where datediff(year,出生日期,getdate()){0})
) a where 1=1 ";
            if (!string.IsNullOrEmpty(where))
                SQL += "and 类型 in(" + where + ")";

            switch (age)
            {
                case "65岁以上老年人":
                    SQL = string.Format(SQL, ">=65");
                    break;
                case "65岁以下人群":
                    SQL = string.Format(SQL, "<65");
                    break;
                default:
                    SQL = string.Format(SQL, ">0");
                    break;
            }

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#脑卒中");
        }

        public DataTable GetDataTable(string SQL)
        {
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#ls");
        }

    }
}
