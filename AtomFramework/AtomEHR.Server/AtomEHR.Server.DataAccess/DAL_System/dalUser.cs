///*************************************************************************/
///*
///* 文件名    ：dalUser.cs                                
///* 程序说明  : 用户数据字典的DAL层
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Interfaces;
using AtomEHR.Server.DataAccess.DAL_Base;

namespace AtomEHR.Server.DataAccess.DAL_System
{
    /// <summary>
    /// 用户数据字典的DAL层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(TUser), true)]
    public class dalUser : dalBaseDataDict, IBridge_User
    {
        public dalUser(Loginer loginer)
            : base(loginer)
        {
            _KeyName = TUser.__KeyName; //主键字段
            _TableName = TUser.__TableName;//表名
            _ModelType = typeof(TUser);
        }

        /// <summary>
        /// 跟据表名创建SQL命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;

            if (tableName == TUser.__TableName) ORM = typeof(TUser);

            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");

            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 获取所有用户列表
        /// </summary>
        /// <returns></returns>
        public DataTable GetUsers()
        {            
            string sql = "select * from [tb_MyUser] where 1=1 ";
            if (Loginer.CurrentUser.所属机构.Length == 12)
            {
                sql += " and ( [所属机构] = @RGID or [所属机构] in (select 机构编号 from tb_机构信息 where 上级机构=@RGID ) ) ";
            }
            else
            {
                sql += " and [所属机构] like '" + Loginer.CurrentUser.所属机构 + "%'";
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RGID", SqlDbType.VarChar, Loginer.CurrentUser.所属机构);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_MyUser");
        }

        public DataTable GetUsersRegion(string RGID,string strWhere)
        {
            string sql = "select * from [tb_MyUser] where 1=1  ";
            if (RGID.Length == 12)
            { 
                sql += "and ( [所属机构] = @RGID or [所属机构] in (select 机构编号 from tb_机构信息 where 上级机构=@RGID ) ) "; 
            }
            else 
            {
                sql += " and [所属机构] like '" + RGID + "%'";
            }
            if (!string.IsNullOrEmpty(strWhere))
                sql += strWhere;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RGID", SqlDbType.VarChar, RGID);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_MyUser");
        }

        /// <summary>
        /// 检查用户是否存在
        /// </summary>
        /// <param name="userid">用户编号</param>
        /// <returns></returns>
        public bool ExistsUser(string account)
        {
            string sql = "select count(*) from [tb_MyUser] where [Account]=@Account";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@Account", SqlDbType.VarChar, account);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return int.Parse(o.ToString()) > 0;
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="account">登录帐号</param>
        public void Logout(string account)
        {
            SqlCommandBase cmd = SqlBuilder.BuildSqlProcedure("sp_Logout");
            cmd.AddParam("@Account", SqlDbType.VarChar, account);
            DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="userID">登录帐号</param>
        /// <returns></returns>
        public DataTable Login(LoginUser loginUser, char LoginUserType)
        {
            SqlCommandBase cmd = SqlBuilder.BuildSqlProcedure("sp_Login");
            cmd.AddParam("@Account", SqlDbType.VarChar, loginUser.Account);
            cmd.AddParam("@Password", SqlDbType.VarChar, loginUser.Password);
            cmd.AddParam("@DataSetID", SqlDbType.VarChar, loginUser.DataSetID);
            cmd.AddParam("@LoginUserType", SqlDbType.Char, LoginUserType);
            cmd.AddParam("@LoginIP", SqlDbType.VarChar, loginUser.IP);
            DataSet ds = DataProvider.Instance.GetDataSet(loginUser.DataSetDBName, cmd.SqlCommand);
            if (ds.Tables.Count == 2)
            {
                string error = ConvertEx.ToString(ds.Tables[1].Rows[0][0]);
                if (error.Trim() != string.Empty) throw new CustomException(error); //抛出异常

                return ds.Tables[0];
            }
            return null;
        }

        /// <summary>
        /// 跟据Novell网帐号获取系统帐号
        /// </summary>
        /// <param name="novellAccount">Novell网帐号</param>
        /// <returns></returns>
        public DataTable GetUserByNovellID(string novellAccount)
        {
            string sql = "select * from [tb_MyUser] where [NovellAccount]=@novellAccount";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@novellAccount", SqlDbType.VarChar, novellAccount);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
            return dt;
        }

        /// <summary>
        /// 获取当前用户的系统权限
        /// </summary>
        /// <param name="user">当前用户</param>
        /// <returns></returns>
        public DataTable GetUserAuthorities(Loginer user)
        {
            SqlProcedure sp = SqlBuilder.BuildSqlProcedure("sp_MyGetAuthorities");
            sp.AddParam("@UserOrGroup", SqlDbType.VarChar, user.Account);
            sp.AddParam("@Type", SqlDbType.Int, 1);
            DataTable dt = DataProvider.Instance.GetTable(user.DBName, sp.SqlCommand, TUserRole.__TableName);
            return dt;
        }

        /// <summary>
        /// 获取用户所属组
        /// </summary>
        /// <param name="account">当前用户</param>
        /// <returns></returns>
        public DataTable GetUserGroups(string account)
        {
            string SQL = "SELECT * FROM tb_MyUserGroup WHERE GroupCode IN (SELECT GroupCode FROM tb_MyUserGroupRe WHERE Account=@Account)";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);
            cmd.AddParam("@Account", SqlDbType.VarChar, account);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUserGroup.__TableName);
            return dt;
        }

        /// <summary>
        /// 获取用户数据
        /// </summary>
        /// <param name="account">帐号</param>
        /// <returns></returns>
        public DataTable GetUser(string account)
        {
            string sql = "select * from [tb_MyUser] where [Account]=@Account";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@Account", SqlDbType.VarChar, account);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
            return dt;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="account">帐号</param>
        /// <returns></returns>
        public bool DeleteUser(string s_用户编码)
        {
            string sql = "Delete [tb_MyUser] where [" + TUser.用户编码 + "]=@用户编码";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@用户编码", SqlDbType.VarChar, s_用户编码);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="account">帐号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public bool ModifyPassword(string account, string pwd)
        {
            return ModifyPwdDirect(account, pwd, _Loginer.DBName);
            //string sql = "update tb_MyUser set password=@password where account=@account";
            //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            //cmd.AddParam("password", SqlDbType.VarChar, pwd);
            //cmd.AddParam("account", SqlDbType.VarChar, account);
            //object o = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            //return int.Parse(o.ToString()) != 0;
        }

        public DataTable GetUserDirect(string account, string DBName)
        {
            string sql = "select * from [tb_MyUser] where [Account]=@Account";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@Account", SqlDbType.VarChar, account);
            DataTable dt = DataProvider.Instance.GetTable(DBName, cmd.SqlCommand, TUser.__TableName);
            return dt;
        }

        public bool ModifyPwdDirect(string account, string pwd, string DBName)
        {
            string sql = "update tb_MyUser set password=@password where account=@account";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("password", SqlDbType.VarChar, pwd);
            cmd.AddParam("account", SqlDbType.VarChar, account);
            object o = DataProvider.Instance.ExecuteNoQuery(DBName, cmd.SqlCommand);
            return int.Parse(o.ToString()) != 0;
        }

        /// <summary>
        /// 修改用户身份证
        /// </summary>
        /// <param name="account">用户编码</param>
        /// <param name="sfz">身份证</param>
        /// <returns></returns>
        public bool ModifySFZDirect(string account, string sfz)
        {
            string sql = "update tb_MyUser set 身份证=@身份证 where account=@account";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("身份证", SqlDbType.VarChar, sfz);
            cmd.AddParam("account", SqlDbType.VarChar, account);
            object o = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return int.Parse(o.ToString()) != 0;
        }


        public DataSet GetUserReportData(DateTime createDateFrom, DateTime createDateTo)
        {
            StringBuilder sb = new StringBuilder("select * from [tb_MyUser] where 1=1 ");

            if (createDateFrom.Year > 1901)
                sb.Append(" AND CONVERT(VARCHAR,[CreateTime],112)>='" + createDateFrom.ToString("yyyyMMdd") + "'");

            if (createDateTo.Year > 1901)
                sb.Append(" AND CONVERT(VARCHAR,[CreateTime],112)<='" + createDateTo.ToString("yyyyMMdd") + "'");

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
            return DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
        }

        //根据当前用户ID,获取与其相同机构的用户名和“ISID”主键(正常情况下应该使用“用户编码”字段)
        public DataTable GetUserNameAndIDBy机构用户(string rgid)
        {
            string sql = "SELECT '' [用户编码],'' [isid],'请选择' [UserName], '' [所属机构] union all SELECT [用户编码],[isid],[UserName]+'|'+Account,[所属机构] FROM [tb_MyUser] where 所属机构 =@id";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@id", SqlDbType.NVarChar, rgid);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
            return dt;
        }

        public string GetUserNameBy用户编号(string 用户编码)
        {
            string sql = "select UserName FROM [tb_MyUser] where 用户编码=@用户编号 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@用户编号", SqlDbType.VarChar, 用户编码);
            object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            if(obj!=null)
            {
                return obj.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 根据机构获取新的用户编号
        /// 2015-11-19 16:39:28 yufh添加
        /// </summary>
        /// <param name="RegionID">机构ID</param>
        /// <returns></returns>
        public string GetNew用户编号(string RegionID)
        {

            SqlProcedure sp = SqlBuilder.BuildSqlProcedure("USP_GETUSERNO");
            sp.AddParam("@RegionID", SqlDbType.VarChar, RegionID);
            string result= DataProvider.Instance.ExecuteScalar(_Loginer.DBName, sp.SqlCommand).ToString();
            return result;
//            DataTable dt = DataProvider.Instance.GetTable(user.DBName, sp.SqlCommand, TUserRole.__TableName);
//            return dt;


//            string sql = @"select @RegionID+
//	                            stuff('0000',len('0000')-len(count(用户编码))+1,
//	                            len(count(用户编码)),isnull(max(substring(用户编码,len(用户编码)-3,4)),0)+1)
//	                            from tb_MyUser 
//	                            where substring(用户编码,1,len(用户编码)-4) = @RegionID "; 
//            //所属机构 2016-04-07 11:55:22 yufh 调整，解决用户调整了所属机构后编码生成重复
//            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
//            cmd.AddParam("@RegionID", SqlDbType.VarChar, RegionID);
//            object 用户编号 = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
//            return 用户编号 != null ? 用户编号.ToString() : "";
        }


        #region InterFaceClass 2016-03-02 10:22:50 yufh 添加
        public bool InterFaceUserExSql(string SQL)
        {
            if (string.IsNullOrEmpty(SQL)) return false;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        public DataTable InterFaceGetUserMapingList(string User)
        {
            string sql = "SELECT * FROM InterFace_用户对照 WHERE 对应用户id=@id";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@id", SqlDbType.VarChar, User);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "用户对照");
            return dt;
        }

        public DataTable InterFaceGetUserList(string User)
        {
            string sql = "select USERID as 编码,USERNAME as 名称,REGION as 机构  from tbUser ";
            if (!string.IsNullOrEmpty(User))
                sql += "where REGION like '" + User + "%' ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            string conn = "Server=192.168.10.100;Database=yth;User ID=yggsuser;Password=yggsuser;Connection TimeOut=180;";
            cmd.SqlCommand.Connection = new System.Data.SqlClient.SqlConnection(conn);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "外围用户列表");
            return dt;
        } 
        #endregion

        public DataTable GetGWUserListByRGID(string rgid, string name)
        {
            string sql = @"SELECT top 50 [用户编码],[UserName],bb.机构名称 所属机构名称
                         FROM [dbo].[tb_MyUser] aa
                         join dbo.tb_机构信息 bb on aa.所属机构 = bb.机构编号
                         where IsLocked =0 and (bb.机构编号=@rgid or bb.上级机构=@rgid) and UserName like '%'+@name+'%' ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@rgid", SqlDbType.VarChar, rgid);
            cmd.AddParam("@name", SqlDbType.VarChar, name);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand,"GWUSER");
            return dt;
        }

        //通知通告用
        public DataTable Get_卫生院管理员成员()
        {
            string strSql = @"select b.UserName as 用户名称,b.用户编码 from tb_MyUserGroupRe a left join tb_MyUser b on a.Account = b.Account where a.GroupCode='admin_Region'";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
        }

        public DataTable Get_用户名称(string UserCode)
        {
            string strSql = @"select UserName from tb_MyUser where 用户编码=@UserCode";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
            cmd.AddParam("@UserCode", SqlDbType.VarChar, UserCode);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
        }

        public DataTable GetwxUser(string jgbh)
        {
            string sql = "SELECT [Account],[用户编码],[UserName] FROM [tb_MyUser] where 所属机构=@jgbh and IsLocked='0'";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@jgbh", SqlDbType.NVarChar, jgbh);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
            return dt;
        }
    }
}
