﻿///*************************************************************************/
///*
///* 文件名    ：DocNoTool.cs                                
///* 程序说明  : 单据号码管理工具
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using AtomEHR.Common;
using System.Data;

namespace AtomEHR.Server.DataAccess.DAL_System
{
    /// <summary>
    /// 单据号码管理工具
    /// </summary>
    public class DocNoTool
    {
        /// <summary>
        /// 在同一事务内生成单号
        /// </summary>
        /// <param name="tran">当前事务</param>
        /// <param name="DocNoName">单据名称</param>
        /// <returns></returns>
        public static string GetNumber(SqlTransaction tran, string DocNoName)
        {
            SqlCommand cmd = new SqlCommand("sp_GetNo '" + DocNoName + "'", tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }

        public static string GetDataSN(SqlTransaction tran, string dataCode, bool asHeader)
        {
            string SQL = "sp_GetDataSN '" + dataCode + "','" + (asHeader ? "Y" : "N") + "'";
            SqlCommand cmd = new SqlCommand(SQL, tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }
        /// <summary>
        /// 获取工作提醒
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="sdate"></param>
        /// <param name="edate"></param>
        /// <returns></returns>
        public static DataSet Get工作提醒(SqlConnection conn, string sdate, string edate, string strWhere)
        {
            string prgid = Loginer.CurrentUser.所属机构;
            //SqlCommand sqlCommand = new SqlCommand("sp_Get工作提醒_NEW", conn);
            SqlCommand sqlCommand = new SqlCommand("sp_Get工作提醒_NEW2018", conn);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.Add(new SqlParameter("@PRGID", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@PRGID"].Value = prgid;
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);

            return dataSet;
        }

        public static DataSet Get工作提醒详细(SqlConnection conn, string sdate, string edate, string strWhere, string type)
        {
            string prgid = Loginer.CurrentUser.所属机构;
            //SqlCommand sqlCommand = new SqlCommand("sp_Get工作提醒详细", conn);
            SqlCommand sqlCommand = new SqlCommand("sp_Get工作提醒详细2018", conn);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //表名
            sqlCommand.Parameters.Add(new SqlParameter("@BEGINDATE", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@BEGINDATE"].Value = sdate;
            //查询字段、默认*或指定要查询的字段
            sqlCommand.Parameters.Add(new SqlParameter("@ENDDATE", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@ENDDATE"].Value = edate;
            //查询条件
            sqlCommand.Parameters.Add(new SqlParameter("@PRGID", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@PRGID"].Value = prgid;

            sqlCommand.Parameters.Add(new SqlParameter("@STRWHERE", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@STRWHERE"].Value = strWhere;

            sqlCommand.Parameters.Add(new SqlParameter("@TYPE", System.Data.SqlDbType.VarChar));
            sqlCommand.Parameters["@TYPE"].Value = type;

            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);

            return dataSet;
        }
        public static string GetDataSN(SqlConnection conn, string dataCode, bool asHeader)
        {
            string SQL = "sp_GetDataSN '" + dataCode + "','" + (asHeader ? "Y" : "N") + "'";
            SqlCommand cmd = new SqlCommand(SQL, conn);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }

        public static string Get家庭档案编号(SqlConnection conn, string dataCode, bool asHeader)
        {
            string SQL = "sp_GetDataSN '" + dataCode + "','" + (asHeader ? "Y" : "N") + "'";
            SqlCommand cmd = new SqlCommand(SQL, conn);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="pageSize">页/大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="count">返回记录总数</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public static DataSet Get分页数据(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                //sqlConn.Open();
                //string sqlDataSource = string.Format("SelectBase {0},{1},{2},{3}", this.PageIndex, this.PageSize, tableName, strWhere);
                SqlCommand sqlCommand = new SqlCommand("GetRecordByPage", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //表名
                sqlCommand.Parameters.Add(new SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@TableName"].Value = tableName;
                //查询字段、默认*或指定要查询的字段
                sqlCommand.Parameters.Add(new SqlParameter("@SelectField", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SelectField"].Value = Fields;
                //查询条件
                sqlCommand.Parameters.Add(new SqlParameter("@WhereConditional", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@WhereConditional"].Value = strWhere;
                //排序索引字段名
                sqlCommand.Parameters.Add(new SqlParameter("@SortExpression", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortExpression"].Value = SortExpression;
                //页大小
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageSize"].Value = pageSize;
                //页码
                sqlCommand.Parameters.Add(new SqlParameter("@PageIndex", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageIndex"].Value = pageIndex;
                //返回记录总数
                sqlCommand.Parameters.Add(new SqlParameter("@RecordCount", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
                //设置排序类型
                sqlCommand.Parameters.Add(new SqlParameter("@SortDire", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortDire"].Value = SortDire;

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);

                // 读取返回值及输出参数的值  
                count = (int)sqlCommand.Parameters["@RecordCount"].Value;
                return dataSet;
            }

            //DataTable table = DataProvider.Instance.GetTable(Loginer.CurrentUser.DBName, DocNoName, "");
            //return table;
        }

        /// <summary>
        /// 获取分页数据,2015-12-25 10:59:01 Yfuh添加
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="pageSize">页/大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="count">返回记录总数</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public static DataSet Get分页数据New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("GetRecordByPageNew", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //表名
                sqlCommand.Parameters.Add(new SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@TableName"].Value = tableName;
                //查询字段、默认*或指定要查询的字段
                sqlCommand.Parameters.Add(new SqlParameter("@SelectField", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SelectField"].Value = Fields;
                //查询条件
                sqlCommand.Parameters.Add(new SqlParameter("@WhereConditional", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@WhereConditional"].Value = strWhere;
                //排序索引字段名
                sqlCommand.Parameters.Add(new SqlParameter("@SortExpression", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortExpression"].Value = SortExpression;
                //页大小
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageSize"].Value = pageSize;
                //页码
                sqlCommand.Parameters.Add(new SqlParameter("@PageIndex", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageIndex"].Value = pageIndex;
                //返回记录总数
                sqlCommand.Parameters.Add(new SqlParameter("@RecordCount", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
                //设置排序类型
                sqlCommand.Parameters.Add(new SqlParameter("@SortDire", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortDire"].Value = SortDire;

                //延长等待时间
                sqlCommand.CommandTimeout = 40;

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);

                // 读取返回值及输出参数的值  
                count = (int)sqlCommand.Parameters["@RecordCount"].Value;
                return dataSet;
            }
        }

        public static DataSet Get分页数据New_New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("GetRecordByPageNew_New", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //表名
                sqlCommand.Parameters.Add(new SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@TableName"].Value = tableName;
                //查询字段、默认*或指定要查询的字段
                sqlCommand.Parameters.Add(new SqlParameter("@SelectField", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SelectField"].Value = Fields;
                //查询条件
                sqlCommand.Parameters.Add(new SqlParameter("@WhereConditional", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@WhereConditional"].Value = strWhere;
                //排序索引字段名
                sqlCommand.Parameters.Add(new SqlParameter("@SortExpression", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortExpression"].Value = SortExpression;
                //页大小
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageSize"].Value = pageSize;
                //页码
                sqlCommand.Parameters.Add(new SqlParameter("@PageIndex", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@PageIndex"].Value = pageIndex;
                //返回记录总数
                sqlCommand.Parameters.Add(new SqlParameter("@RecordCount", System.Data.SqlDbType.Int));
                sqlCommand.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
                //设置排序类型
                sqlCommand.Parameters.Add(new SqlParameter("@SortDire", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SortDire"].Value = SortDire;

                //延长等待时间
                sqlCommand.CommandTimeout = 40;

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);

                // 读取返回值及输出参数的值  
                count = (int)sqlCommand.Parameters["@RecordCount"].Value;
                return dataSet;
            }
        }

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <param name="tableName">表名/视图</param>
        /// <param name="Fields">查询字段,可默认*或指定查询字段</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="SortExpression">排序字段</param>
        /// <param name="SortDire">排序类型</param>
        /// <returns></returns>
        public static DataTable Get全部数据(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                string sql = string.Format("SELECT {0} FROM {1} WHERE 1=1 {2} ORDER BY {3} {4}", Fields, tableName, strWhere, SortExpression, SortDire);

                //sqlConn.Open();
                //string sqlDataSource = string.Format("SelectBase {0},{1},{2},{3}", this.PageIndex, this.PageSize, tableName, strWhere);
                SqlCommand sqlCommand = new SqlCommand(sql, sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                return dataSet.Tables[0];
            }

            //DataTable table = DataProvider.Instance.GetTable(Loginer.CurrentUser.DBName, DocNoName, "");
            //return table;
        }


        public static DataSet Get体检详细数据(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string SearchBy,string spName)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand(spName, sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@机构", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@机构"].Value = DocNo机构;
                sqlCommand.Parameters.Add(new SqlParameter("@DateFrom", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@DateFrom"].Value = DateFrom;
                sqlCommand.Parameters.Add(new SqlParameter("@DateTo", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@DateTo"].Value = DateTo;
                sqlCommand.Parameters.Add(new SqlParameter("@完整度", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@完整度"].Value = DocNo完整度;
                sqlCommand.Parameters.Add(new SqlParameter("@SearchBy", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SearchBy"].Value = SearchBy;
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                return dataSet;
            }
        }

        public static string Get高血压管理卡(SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand("select isnull(COUNT(管理卡编号),0)+1 from tb_MXB高血压管理卡 ", tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }

        public static string Get糖尿病管理卡(SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand("select isnull(COUNT(管理卡编号),0)+1 from tb_MXB糖尿病管理卡 ", tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }
        public static string Get脑卒中管理卡(SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand("select isnull(COUNT(管理卡编号),0)+1 from tb_MXB脑卒中管理卡 ", tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }
        public static string Get冠心病管理卡(SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand("select isnull(COUNT(管理卡编号),0)+1 from tb_MXB冠心病管理卡 ", tran.Connection, tran);
            object no = cmd.ExecuteScalar();
            return ConvertEx.ToString(no);
        }

        internal static DataSet Get体检详细数据_老年人未体检(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string SearchBy)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("USP_GET体检详细_老年人未体检", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@机构", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@机构"].Value = DocNo机构;
                sqlCommand.Parameters.Add(new SqlParameter("@DateFrom", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@DateFrom"].Value = DateFrom;
                sqlCommand.Parameters.Add(new SqlParameter("@DateTo", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@DateTo"].Value = DateTo;
                sqlCommand.Parameters.Add(new SqlParameter("@完整度", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@完整度"].Value = DocNo完整度;
                sqlCommand.Parameters.Add(new SqlParameter("@SearchBy", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@SearchBy"].Value = SearchBy;
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                return dataSet;
            }
        }
    }
}
