﻿///*************************************************************************/
///*
///* 文件名    ：dalCommon.cs                                
///* 程序说明  : 公共数据层
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using AtomEHR.Interfaces.Interfaces_Bridge;
using System.Data;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_Base;
using System.Data.SqlClient;

namespace AtomEHR.Server.DataAccess.DAL_System
{
    public class dalCommon : dalBase, IBridge_CommonData
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="user">当前用户登录信息</param>
        public dalCommon(Loginer user) : base(user) { }

        public bool TestConnection()
        {
            string sql = "SELECT COUNT(*) FROM tb_DataSet";

            //取系统数据库的帐套数据表
            object o = DataProvider.Instance.ExecuteScalar(Globals.DEF_SYSTEM_DB, sql);
            return true;
        }

        public DataTable GetSystemDataSet()
        {
            string sql = "SELECT * FROM tb_DataSet";

            //取系统数据库的帐套数据表
            return DataProvider.Instance.GetTable(Globals.DEF_SYSTEM_DB, sql, "tb_DataSet");
        }

        /// <summary>
        /// 获取物理表的字段
        /// </summary>
        /// <param name="tableName">物理表名</param>
        /// <returns></returns>
        public DataTable GetTableFields(string tableName)
        {
            SqlProcedure sp = SqlBuilder.BuildSqlProcedure("sp_sys_GetTableFieldType");
            sp.AddParam("@TableName", SqlDbType.VarChar, 50, tableName);
            return DataProvider.Instance.GetTable(_Loginer.DBName, sp.SqlCommand, "tb_TableFieldTypeData");
        }

        /// <summary>
        /// 获取业务单据名称定义
        /// </summary>
        /// <returns></returns>
        public DataTable GetBusinessTables()
        {
            string SQL = "SELECT * FROM sys_BusinessTables ORDER BY ModuleID,SortID";
            return DataProvider.Instance.GetTable(_Loginer.DBName, SQL, "sys_BusinessTables");
        }

        /// <summary>
        /// 获取模块名称定义
        /// </summary>
        /// <returns></returns>
        public DataTable GetModules()
        {
            return DataProvider.Instance.GetTable(_Loginer.DBName, "SELECT * FROM sys_Modules", "sys_Modules");
        }

        /// <summary>
        /// 获取表的所有字段
        /// </summary>
        /// <param name="tableName">表名</param>
        public DataTable GetTableFieldsDef(string tableName, bool onlyDisplayField)
        {
            string flag = onlyDisplayField ? "N" : "Y";
            string sql = "sp_sys_GetTableFieldsForPicker '" + tableName + "','" + flag + "' ";
            return DataProvider.Instance.GetTable(_Loginer.DBName, sql, "Fields");
        }


        public DataTable GetDB4Backup()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT DBName,DataSetName FROM dbo.tb_DataSet");
            sb.AppendLine("UNION ");
            sb.AppendLine("SELECT 'AtomEHR.System','系统数据库'");

            //取系统数据库里的资料表
            return DataProvider.Instance.GetTable(Globals.DEF_SYSTEM_DB, sb.ToString(), "DB");
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="DBNAME">数据名称。如：CSFramework22.Normal</param>
        /// <param name="BKPATH">备份路径。如：C:\</param>
        public bool BackupDatabase(string DBNAME, string BKPATH)
        {
            SqlProcedure sp = SqlBuilder.BuildSqlProcedure("p_BackupDB");
            sp.AddParam("@DBNAME", SqlDbType.NVarChar, 100, DBNAME);
            sp.AddParam("@BKPATH", SqlDbType.NVarChar, 260, BKPATH);
            DataProvider.Instance.ExecuteNoQuery(Globals.DEF_MASTER_DB, sp.SqlCommand);
            return true;
        }

        /// <summary>
        /// 还原数据库
        /// </summary>
        /// <param name="BKFILE">备份的文件。如：C:\CSFramework22.Normal_2010.bak</param>
        /// <param name="DBNAME">数据名称。如：CSFramework22.Normal</param>
        public bool RestoreDatabase(string BKFILE, string DBNAME)
        {
            SqlProcedure sp = SqlBuilder.BuildSqlProcedure("p_RestoreDB");
            sp.AddParam("@BKFILE", SqlDbType.NVarChar, 1000, BKFILE);
            sp.AddParam("@DBNAME", SqlDbType.NVarChar, 100, DBNAME);
            DataProvider.Instance.ExecuteNoQuery(Globals.DEF_MASTER_DB, sp.SqlCommand);
            return true;
        }

        /// <summary>
        /// 获取备份历史记录
        /// </summary>
        /// <param name="topList">返回的记录数</param>
        /// <returns></returns>
        public DataTable GetBackupHistory(int topList)
        {
            string sql = "SELECT TOP " + topList.ToString() + " * FROM sys_BackupHistory ORDER BY BackupTime DESC ";
            return DataProvider.Instance.GetTable(Globals.DEF_MASTER_DB, sql, "sys_BackupHistory");
        }

        public string GetDataSN(string dataCode, bool asHeader)
        {
            SqlConnection conn = DataProvider.Instance.CreateConnection(_Loginer.DBName);
            string no = DocNoTool.GetDataSN(conn, dataCode, asHeader);
            conn.Close();
            return no;
        }
                
        public DataTable GetVersion()
        {
            string sql = "SELECT * FROM tb_Version";

            //取系统数据库的版本信息数据表
            return DataProvider.Instance.GetTable(Globals.DEF_SYSTEM_DB, sql, "tb_Version");
        }

        public DataTable GetLoginPersonalInfo(string IDCard)
        {
            string sql = "SELECT * FROM tb_MyUser where IsLocked=0 and 身份证=@身份证";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@身份证",SqlDbType.VarChar, IDCard);
            //取系统数据库的版本信息数据表
            return DataProvider.Instance.GetTable("AtomEHR.YSDB", cmd.SqlCommand, "tb_MyUser");
        }

        public DataTable GetInterfaceConn(string RGID)
        {
            string sql = @" select * from tb_InterfaceConnSet where Remark='LISCONN' 
                                    and (RGID = substring(@RGID,1,7)+'1'+substring(@RGID,9,4) or RGID=@RGID) ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RGID", SqlDbType.VarChar, RGID.Trim());
            DataTable dt = DataProvider.Instance.GetTable(Globals.DEF_SYSTEM_DB, cmd.SqlCommand, "#lis连接");
            return dt;
        }

        public string GetQYYS(string IDCrad) { return ""; }

        public string GetQYYSName(string IDCrad) { return ""; }

        public string SendWxMessage(string template_id, string idcard, string type, string key_first, string key_remark, string date, string[] key_keyword) { return ""; }
											 
    }
}
