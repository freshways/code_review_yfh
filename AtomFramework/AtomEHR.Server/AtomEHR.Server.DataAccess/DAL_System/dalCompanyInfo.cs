﻿///*************************************************************************/
///*
///* 文件名    ：dalCommonService.cs                                
///* 程序说明  : 公共数据层
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///*
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess.DAL_Base;

namespace AtomEHR.Server.DataAccess.DAL_System
{
    /// <summary>
    /// 公司资料设置
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_CompanyInfo),true)]
    public class dalCompanyInfo : dalBaseDataDict
    {
        public dalCompanyInfo(Loginer loginer)
            : base(loginer)
        {
            _TableName = tb_CompanyInfo.__TableName;
            _KeyName = tb_CompanyInfo.__KeyName;
            _ModelType = typeof(tb_CompanyInfo);
        }
    }
}
