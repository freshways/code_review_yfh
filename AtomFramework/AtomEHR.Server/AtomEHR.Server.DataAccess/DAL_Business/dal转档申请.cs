﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 转档申请的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/28 11:05:15
 *   最后修改: 2015/09/28 11:05:15
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal转档申请
    /// </summary>
    public class dal转档申请 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal转档申请(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_转档申请.__KeyName; //主表的主键字段
            _SummaryTableName = tb_转档申请.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_转档申请.__TableName) ORM = typeof(tb_转档申请);
            //if (tableName == tb_转档申请s.__TableName) ORM = typeof(tb_转档申请s);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_转档申请.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_转档申请.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_转档申请.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_转档申请]    where [" + tb_转档申请.__KeyName + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_转档申请s]   where [" + tb_转档申请.__KeyName + "]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_转档申请.__TableName;
            //ds.Tables[1].TableName =tb_转档申请s.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 查询正处于转档状态 的信息
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public DataTable GetZDXX(string docNo)
        {
            string sql = @"select * from tb_转档申请 where 个人档案编号 = '" + docNo + "' and (类型 = '0' or 类型 = '1') ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_转档申请.__TableName);
            return dt;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        ///根据id删除即可
        /// </summary>
        public bool Delete(string docNo)
        {
            string sql2 = "delete [tb_转档申请] where [" + tb_转档申请.__KeyName + "]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "转档申请");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        /// <summary>
        /// 查询 已转入/已转出。。。。。档案
        /// </summary>
        /// <param name="_param"></param>
        /// <returns></returns>
        public DataTable GetDataByParams(Dictionary<string, string> _param)
        {
            //_param.Add("status", "alreadyRollin");
            //_param.Add("包含下属机构", this.checkEdit1.Checked ? "1" : "0");
            //_param.Add("机构", this.cbo机构.EditValue.ToString());
            string sql = @"select * from tb_转档申请 zd inner join tb_健康档案 rkx on zd.个人档案编号 = rkx.个人档案编号";
            string 包含下属机构 = _param["包含下属机构"];
            string pgrid = _param["机构"];
            string status = _param["status"];
            if (status == "alreadyRollin" || status == "waitRollin" || status == "reject")
            {
                if (包含下属机构 == "1")//包含下属机构
                {
                    if (pgrid.Length < 7)
                    {
                        sql += " AND zd.转入机构 like '" + pgrid + "%' ";
                    }
                    if (pgrid.Length == 12)
                    {
                        sql += " AND (zd.转入机构 = '" + pgrid + "'  OR (len(zd.转入机构)=15 and  substring(zd.转入机构,1,7)+'1'+substring(zd.转入机构,9,7)  like '" + pgrid + "%'))";
                    }
                    if (pgrid.Length == 15)
                    {
                        sql += " AND zd.转入机构 = '" + pgrid + "' ";
                    }
                }
                else
                {
                    sql += " AND zd.转入机构 = '" + pgrid + "' ";
                }

                if ("alreadyRollin" == status)
                {
                    sql += " AND 类型 = '3' ";
                }
                if ("waitRollin" == status)
                {
                    sql += " AND (类型 = '0' or  类型 = '1' )";
                }
                if ("reject" == status)
                {
                    sql += " AND (类型 = '2' or  类型 = '4' )";
                }
            }
            else if (status == "alreadyRollout" || status == "waitRollout")
            {
                if (包含下属机构 == "1")//包含下属机构
                {
                    if (pgrid.Length < 7)
                    {
                        sql += " AND zd.转出机构 like '" + pgrid + "%' ";
                    }
                    if (pgrid.Length == 12)
                    {
                        sql += " AND (zd.转出机构 = '" + pgrid + "'  OR (len(zd.转出机构)=15 and substring(zd.转出机构,1,7)+'1'+substring(zd.转出机构,9,7)  like '" + pgrid + "%'))";
                    }
                    if (pgrid.Length == 15)
                    {
                        sql += " AND zd.转出机构 = '" + pgrid + "' ";
                    }
                }
                else
                {
                    sql += " AND zd.转出机构 = '" + pgrid + "' ";
                }

                if ("alreadyRollout" == status)
                {
                    sql += " AND 类型 = '3' ";
                }
                if ("waitRollout" == status)
                {
                    sql += " AND (类型 = '0' or  类型 = '1' )";
                }

            }
            sql += "  order by zd.HAPPENTIME desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_转档申请.__TableName);
            return dt;
        }

        public bool UpdateByPrimaryKeySelective(string id, string jjyy)
        {

            //TzdZdsq tzdZdsq = new TzdZdsq();
            //tzdZdsq.setId(Integer.valueOf(Integer.parseInt(id)));
            //tzdZdsq.setJjyy(jjyy);
            //if (user.getpRgid().length() <= 6)
            //{
            //    tzdZdsq.setType("4");
            //    tzdZdsq.setWsjshr(user.getpUserid());
            //}
            //else
            //{
            //    tzdZdsq.setType("2");
            //    tzdZdsq.setSsjgshr(user.getpUserid());
            //}
            //int m = this.tzdZdsqMapper.updateByPrimaryKeySelective(tzdZdsq);
            //return m;
            //string type = string.Empty;
            //string Wsjshr = string.Empty;
            //string Ssjgshr = string.Empty;
            //string pRgid = Loginer.CurrentUser.所属机构;
            //if (pRgid.Length <= 6)
            //{
            //    type = "4";
            //    Wsjshr = Loginer.CurrentUser.用户编码;
            //}
            //else
            //{
            //    type = "2";
            //    Ssjgshr = Loginer.CurrentUser.用户编码;
            //}


            //string sql2 = "delete [tb_转档申请] where [" + tb_转档申请.__KeyName + "]=@DocNo1 ";
            //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            //return i != 0;
            return true;
        }



        public SaveResult updateConvertTopassed(string id, string yhzgx, string jtdabh, string only)
        {
            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();
            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            try
            {
                string sfsh = "2";//TODO: 是否需要卫生局进行审核，此字段在原程序中 默认设置为“2”，即不需要卫生局审核
                DataSet zdsq = this.GetBusinessByKey(id);
                String[] tableInfos = GetTable.getGetTableInfoName();
                String[] tables = GetTable.getGetTableName();
                string sql = string.Empty;
                if (_Loginer.所属机构.Length <= 6)
                {
                    String ol = zdsq.Tables[0].Rows[0][tb_转档申请.卫生局审核人].ToString();
                    sql = " update tb_转档申请 set 类型 = '3',转档方式 = '0' ,HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120), 卫生局审核人 = '" + _Loginer.用户编码 + "' where id = '" + id + "'; ";
                    //zdsq.Tables[0].Rows[0][tb_转档申请.类型] = "3";
                    //zdsq.Tables[0].Rows[0][tb_转档申请.卫生局审核人] = _Loginer.用户编码;
                    if (ol == "2")
                    {
                        DataTable jtcyList = this.GetjtcyByJTDABH(jtdabh);
                        if (jtcyList != null && jtcyList.Rows.Count > 0)
                        {
                            for (int i = 0; i < jtcyList.Rows.Count; i++)
                            {
                                string sfzx = "no";
                                string grdabh = jtcyList.Rows[i]["个人档案编号"].ToString();
                                string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                                string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                                string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                                string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                                string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                                string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                                string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                                for (int j = 0; j < tables.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tables[j]))
                                    {
                                        sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                        if (!string.IsNullOrEmpty(pZcrgid))
                                        {
                                            sql += " and 所属机构 = '" + pZcrgid + "' ";
                                        }
                                        if (tables[j] == "tb_家庭档案")
                                        {
                                            sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                        }
                                        else
                                        {
                                            sql += " and 个人档案编号 = '" + grdabh + "' ";
                                        }
                                    }
                                }
                                for (int j = 0; j < tableInfos.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tableInfos[j]))
                                    {
                                        sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                    }
                                }

                                sql += string.Format(@"update tb_转档申请 set 类型 = '4',
		                            拒绝原因 = '此居民已转离原机构！', HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120), 卫生局审核人 =
		                            '{0}' where 个人档案编号 ='{1}' and (类型 = '0' or 类型 = '1')", _Loginer.用户编码, grdabh);
                            }
                        }
                        else
                        {
                            #region MyRegion
                            string sfzx = "no";
                            string grdabh = zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString();
                            string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                            string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                            string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                            string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                            string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                            string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                            string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                            for (int j = 0; j < tables.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tables[j]))
                                {
                                    sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                    if (!string.IsNullOrEmpty(pZcrgid))
                                    {
                                        sql += " and 所属机构 = '" + pZcrgid + "' ";
                                    }
                                    if (tables[j] == "tb_家庭档案")
                                    {
                                        sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                    }
                                    else
                                    {
                                        sql += " and 个人档案编号 = '" + grdabh + "' ";
                                    }
                                }
                            }
                            for (int j = 0; j < tableInfos.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tableInfos[j]))
                                {
                                    sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                }
                            }
                            #endregion
                        }
                    }
                    else if (ol == "1")
                    {
                        DataTable table = this.selectRkxzlByGrdabh(zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString());
                        DataTable jtcyList = this.GetjtcyByJTDABH(jtdabh);
                        if (jtcyList != null && jtcyList.Rows.Count > 0)
                        {
                            for (int i = 0; i < jtcyList.Rows.Count; i++)
                            {
                                string sfzx = "zx";
                                string grdabh = jtcyList.Rows[i]["个人档案编号"].ToString();
                                string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                                string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                                string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                                string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                                string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                                string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                                string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                                for (int j = 0; j < tables.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tables[j]) && tables[j] != "tb_家庭档案")
                                    {
                                        sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                        if (!string.IsNullOrEmpty(pZcrgid))
                                        {
                                            sql += " and 所属机构 = '" + pZcrgid + "' ";
                                        }
                                        if (tables[j] == "tb_家庭档案")
                                        {
                                            sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                        }
                                        else
                                        {
                                            sql += " and 个人档案编号 = '" + grdabh + "' ";
                                        }
                                    }
                                }
                                for (int j = 0; j < tableInfos.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tableInfos[j]))
                                    {
                                        sql += string.Format(@"  update {0} set 所属机构 = '{1}' ", tableInfos[j], pZrrgid);
                                        if (tableInfos[j] == "tb_健康档案")
                                        {
                                            sql += " ,与户主关系 = '' , 家庭档案编号  = '' ";
                                        }
                                        sql += string.Format(@",市 = '{0}',区='{1}',街道='{2}',居委会='{3}',居住地址='{4}'
                                                                              where 个人档案编号 = '{5}'   ", pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                    }
                                }

                                sql += string.Format(@"update tb_转档申请 set 类型 = '4',
		                            拒绝原因 = '此居民已转离原机构！',HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120),  卫生局审核人 =
		                            '{0}' where 个人档案编号 ='{1}' and (类型 = '0' or 类型 = '1')", _Loginer.用户编码, grdabh);
                            }
                        }
                        else
                        {
                            #region MyRegion
                            string sfzx = "no";
                            string grdabh = zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString();
                            string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                            string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                            string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                            string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                            string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                            string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                            string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                            for (int j = 0; j < tables.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tables[j]))
                                {
                                    sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                    if (!string.IsNullOrEmpty(pZcrgid))
                                    {
                                        sql += " and 所属机构 = '" + pZcrgid + "' ";
                                    }
                                    if (tables[j] == "tb_家庭档案")
                                    {
                                        sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                    }
                                    else
                                    {
                                        sql += " and 个人档案编号 = '" + grdabh + "' ";
                                    }
                                }
                            }
                            for (int j = 0; j < tableInfos.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tableInfos[j]))
                                {
                                    sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                }
                            }
                            #endregion
                        }
                    }
                }
                //默认   不需要卫生局审核
                else if (sfsh == "2")
                {
                    sql = " update tb_转档申请 set 类型 = '3',转档方式 = '0' , HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120), 所属机构审核人 = '" + _Loginer.用户编码 + "' where id = '" + id + "'; ";
                    if (only == "2")  //家庭成员全部转档
                    {
                        DataTable jtcyList = this.GetjtcyByJTDABH(jtdabh);
                        if (jtcyList != null && jtcyList.Rows.Count > 0)
                        {
                            for (int i = 0; i < jtcyList.Rows.Count; i++)//for循环 家庭成员中的 每一个 成员 进行转档
                            {
                                string sfzx = "no";
                                string grdabh = jtcyList.Rows[i]["个人档案编号"].ToString();
                                string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                                string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                                string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                                string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                                string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                                string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                                string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                                for (int j = 0; j < tables.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tables[j]))
                                    {
                                        sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                        if (!string.IsNullOrEmpty(pZcrgid))
                                        {
                                            sql += " and 所属机构 = '" + pZcrgid + "' ";
                                        }
                                        if (tables[j] == "tb_家庭档案")
                                        {
                                            sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                        }
                                        else
                                        {
                                            sql += " and 个人档案编号 = '" + grdabh + "' ";
                                        }
                                    }
                                }
                                for (int j = 0; j < tableInfos.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tableInfos[j]))
                                    {
                                        sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                    }
                                }

                                sql += string.Format(@"update tb_转档申请 set 类型 = '4',
		                            拒绝原因 = '此居民已转离原机构！', HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120),卫生局审核人 =
		                            '{0}' where 个人档案编号 ='{1}' and (类型 = '0' or 类型 = '1')", _Loginer.用户编码, grdabh);
                            }
                        }
                        else
                        {
                            #region MyRegion
                            string sfzx = "no";
                            string grdabh = zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString();
                            string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                            string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                            string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                            string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                            string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                            string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                            string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                            for (int j = 0; j < tables.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tables[j]))
                                {
                                    sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                    if (!string.IsNullOrEmpty(pZcrgid))
                                    {
                                        sql += " and 所属机构 = '" + pZcrgid + "' ";
                                    }
                                    if (tables[j] == "tb_家庭档案")
                                    {
                                        sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                    }
                                    else
                                    {
                                        sql += " and 个人档案编号 = '" + grdabh + "' ";
                                    }
                                }
                            }
                            for (int j = 0; j < tableInfos.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tableInfos[j]))
                                {
                                    sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                }
                            }
                            #endregion
                        }
                    }
                    //只转档当前人
                    else if (only == "1")
                    {
                        DataTable table = this.selectRkxzlByGrdabh(zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString());
                        DataTable jtcyList = this.GetjtcyByJTDABH(jtdabh);
                        if (jtcyList != null && jtcyList.Rows.Count > 0)
                        {
                            if ("1".Equals(yhzgx))  //判断是否是户主，户主不能进行转档
                            {
                                mResult.Description = "此居民是‘户主’，且家庭中有其他成员，请先到家庭基本信息表中修改其‘户主关系’为‘非户主’后再对其进行转档！";
                            }
                            else
                            {
                                string sfzx = "zx";
                                string grdabh = zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString();
                                string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                                string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                                string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                                string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                                string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                                string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                                string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                                for (int j = 0; j < tables.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tables[j]) && tables[j] != "tb_家庭档案")
                                    {
                                        sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                        if (!string.IsNullOrEmpty(pZcrgid))
                                        {
                                            sql += " and 所属机构 = '" + pZcrgid + "' ";
                                        }
                                        if (tables[j] == "tb_家庭档案")
                                        {
                                            sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                        }
                                        else
                                        {
                                            sql += " and 个人档案编号 = '" + grdabh + "' ";
                                        }
                                    }
                                }
                                for (int j = 0; j < tableInfos.Length; j++)
                                {
                                    if (!string.IsNullOrEmpty(tableInfos[j]))
                                    {
                                        sql += string.Format(@"  update {0} set 所属机构 = '{1}' ", tableInfos[j], pZrrgid);
                                        if (tableInfos[j] == "tb_健康档案")
                                        {
                                            sql += " ,与户主关系 = '' , 家庭档案编号  = '' ";
                                        }
                                        sql += string.Format(@",市 = '{0}',区='{1}',街道='{2}',居委会='{3}',居住地址='{4}'
                                                                              where 个人档案编号 = '{5}'   ", pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                    }
                                }

                                sql += string.Format(@"update tb_转档申请 set 类型 = '4',
		                            拒绝原因 = '此居民已转离原机构！',HAPPENTIME = CONVERT( VARCHAR, GETDATE() ,120),  卫生局审核人 =
		                            '{0}' where 个人档案编号 ='{1}' and (类型 = '0' or 类型 = '1')", _Loginer.用户编码, grdabh);
                            }
                        }
                        else
                        {
                            #region MyRegion
                            string sfzx = "no";
                            string grdabh = zdsq.Tables[0].Rows[0][tb_转档申请.个人档案编号].ToString();
                            string pZrshi = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 4);
                            string pZrqu = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 6);
                            string pZrjd = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString().Substring(0, 8);
                            string pZrjwh = zdsq.Tables[0].Rows[0][tb_转档申请.转入居委会].ToString();
                            string pZrxxdz = zdsq.Tables[0].Rows[0][tb_转档申请.转入详细地址].ToString();
                            string pZrrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转入机构].ToString();
                            string pZcrgid = zdsq.Tables[0].Rows[0][tb_转档申请.转出机构].ToString();
                            for (int j = 0; j < tables.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tables[j]))
                                {
                                    sql += string.Format("  update {0} set 所属机构 = '{1}' where 1=1  ", tables[j], pZrrgid);
                                    if (!string.IsNullOrEmpty(pZcrgid))
                                    {
                                        sql += " and 所属机构 = '" + pZcrgid + "' ";
                                    }
                                    if (tables[j] == "tb_家庭档案")
                                    {
                                        sql += " and 家庭档案编号 = '" + jtdabh + "' ";
                                    }
                                    else
                                    {
                                        sql += " and 个人档案编号 = '" + grdabh + "' ";
                                    }
                                }
                            }
                            for (int j = 0; j < tableInfos.Length; j++)
                            {
                                if (!string.IsNullOrEmpty(tableInfos[j]))
                                {
                                    sql += string.Format(@"  update {0} set 所属机构 = '{1}' ,市 = '{2}',区='{3}',街道='{4}',居委会='{5}',居住地址='{6}'
                                        where 个人档案编号 = '{7}'   ", tableInfos[j], pZrrgid, pZrshi, pZrqu, pZrjd, pZrjwh, pZrxxdz, grdabh);
                                }
                            }
                            #endregion
                        }
                    }
                }
                else//需要卫生局批准  不做现在
                {
                    //                    int n = 0;
                    //tzdZdsq.setType("1");
                    //tzdZdsq.setSsjgshr(user.getpUserid());
                    //if ("1".equals(yhzgx)) {
                    //  TdaJkdaRkxzl rkxzl = new TdaJkdaRkxzl();
                    //  rkxzl.setdGrdabh(zdsq.getdGrdabh());
                    //  rkxzl = (TdaJkdaRkxzl)this.tzdZdsqMapper.selectRkxzlByGrdabh(rkxzl).get(0);
                    //  List jtcyList = this.tzdZdsqMapper
                    //    .selectJtcy(rkxzl.getdJtdabh());
                    //  if (jtcyList.size() > 1) {
                    //    reply = "此居民是‘户主’，且家庭中有其他成员，请先到家庭基本信息表中修改其‘户主关系’为‘非户主’后再对其进行转档！";
                    //  } else {
                    //    tzdZdsq.setWsjshr(only);
                    //    n = this.tzdZdsqMapper.updateByPrimaryKeySelective(tzdZdsq);
                    //  }
                    //} else {
                    //  tzdZdsq.setWsjshr(only);
                    //  n = this.tzdZdsqMapper.updateByPrimaryKeySelective(tzdZdsq);
                    //}
                    //if (n == 1)
                    //  reply = "已将申请发送至卫生局，请等待卫生局审核！";
                    //else if ("".equals(reply)) {
                    //  reply = "审核失败
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.SqlCommand.CommandTimeout = 0;
                int k = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
                //int k = DataProvider.Instance.ExecuteNoQuery(_CurrentTrans, sql);
                //return i != 0;
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务  
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      

                mResult.SetException(ex); //保存结果设置异常消息

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
        }

        private DataTable selectRkxzlByGrdabh(string p)
        {
            string sql = "SELECT * FROM tb_健康档案 WHERE " + tb_健康档案.__KeyName + "='" + p + "'";
            DataTable table = DataProvider.Instance.GetTable(_Loginer.DBName, sql, tb_健康档案.__TableName);
            return table;
        }

        private DataTable GetjtcyByJTDABH(string jtdabh)
        {
            string sql = "SELECT * FROM tb_健康档案 WHERE " + tb_健康档案.家庭档案编号 + "='" + jtdabh + "'";
            DataTable table = DataProvider.Instance.GetTable(_Loginer.DBName, sql, tb_健康档案.__TableName);
            return table;
        }


        public SaveResult ConvertForRgid(string zcrgid, string zcjd, string zcjwh, string zrrgid, string zrjd, string zrjwh,string creUser,string jzdz)
        {
            if(string.IsNullOrEmpty(zcrgid))
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n转出机构不能为空！");

            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();
            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            try
            {
                String[] tableinfonames = GetTable.getGetTableInfoName();
                String[] tablenames = GetTable.getGetTableName();

                string sql = string.Empty;
                #region Detail

                for (int i = 0; i < tablenames.Length; i++)
                {
                    if (!string.IsNullOrEmpty(tablenames[i]))
                    {
                        sql += " update " + tablenames[i] + " set 所属机构 = '" + zrrgid + "' where 1=1 ";
                        if (tablenames[i] == "tb_家庭档案")
                        {
                            sql += " and 家庭档案编号 in (select 家庭档案编号 from tb_健康档案 where 1=1 ";
                            if (!string.IsNullOrEmpty(zcrgid))
                            {
                                sql += "  and  所属机构 = '" + zcrgid + "' ";
                            }
                            if (!string.IsNullOrEmpty(zcjd))
                            {
                                sql += "  and  街道 = '" + zcjd + "' ";
                            }
                            if (!string.IsNullOrEmpty(zcjwh))
                            {
                                sql += "  and  居委会 = '" + zcjwh + "' ";
                            }
                            if (!string.IsNullOrEmpty(jzdz))
                            {
                                sql += " and 居住地址 LIKE '%" + jzdz + "%'  ";
                            }
                            if (!string.IsNullOrEmpty(creUser))
                            {
                                sql += "  and (修改人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' ) or 创建人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' )) ";
                            }
                            sql += ")  ";
                        }
                        else
                        {
                            sql += " and 个人档案编号 in (select 个人档案编号 from tb_健康档案 where 1=1 ";
                            if (!string.IsNullOrEmpty(zcrgid))
                            {
                                sql += "  and  所属机构 = '" + zcrgid + "' ";
                            }
                            if (!string.IsNullOrEmpty(zcjd))
                            {
                                sql += "  and  街道 = '" + zcjd + "' ";
                            }
                            if (!string.IsNullOrEmpty(zcjwh))
                            {
                                sql += "  and  居委会 = '" + zcjwh + "' ";
                            }
                            if (!string.IsNullOrEmpty(jzdz))
                            {
                                sql += " and 居住地址 LIKE '%" + jzdz + "%'  ";
                            }
                            if (!string.IsNullOrEmpty(creUser))
                            {
                                sql += "  and (修改人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' ) or 创建人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' )) ";
                            }
                            sql += ") ";
                        }
                        //if (!string.IsNullOrEmpty(zcrgid))
                        //{
                        //    sql += " and 所属机构 = '" + zcrgid + "' ) \r\n";
                        //}
                    }

                }

                #endregion
                #region Info

                for (int i = 0; i < tableinfonames.Length; i++)
                {
                    if (!string.IsNullOrEmpty(tableinfonames[i]))
                    {

                        sql += "update " + tableinfonames[i] + " set 所属机构 = '" + zrrgid + "',所属片区=''";
                        if (!string.IsNullOrEmpty(zrjd))
                        {
                            sql += ",街道 = '" + zrjd + "'";
                        }
                        if (!string.IsNullOrEmpty(zrjwh))
                        {
                            sql += ",居委会 = '" + zrjwh + "'";
                        }
                        if (!string.IsNullOrEmpty(zcrgid))
                        {
                            sql += " where 1=1   and 个人档案编号 in (select 个人档案编号 from tb_健康档案 where  所属机构 = '" + zcrgid + "' ";
                        }
                        if (!string.IsNullOrEmpty(zcjd))
                        {
                            sql += "  and  街道 = '" + zcjd + "' ";
                        }
                        if (!string.IsNullOrEmpty(zcjwh))
                        {
                            sql += "  and  居委会 = '" + zcjwh + "' ";
                        }
                        if (!string.IsNullOrEmpty(jzdz))
                        {
                            sql += " and 居住地址 LIKE '%" + jzdz + "%'  ";
                        }
                        if (!string.IsNullOrEmpty(creUser))
                        {
                            sql += "  and (修改人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' ) or 创建人 in(select 用户编码 from tb_Myuser where 所属机构='" + zrrgid + "' )) ";
                        }
                        sql += ") \r\n";
                    }
                }

                #endregion

                sql += @"insert into tb_批量转档 (转出机构, 转出街道,       转出居委会, 转入机构, 转入街道, 
      转入居委会, 操作人, 转档时间)    values ('" + zcrgid + "', '" + zcjd + "','" + zcjwh + "', '" + zrrgid + "', '" + zrjd + "', '" + zrjwh + "', '" + _Loginer.Account + "', '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.SqlCommand.CommandTimeout = 0;
                int k = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
                //_CurrentTrans.Commit
                //DataProvider.Instance.ExecuteNoQuery(_CurrentTrans, sql);
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务  
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      
                mResult.SetException(ex); //保存结果设置异常消息
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
            return mResult; //返回保存结果
        }
    }
}

