﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 通知通告表的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018-09-29 04:22:02
 *   最后修改: 2018-09-29 04:22:02
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal通知通告表
    /// </summary>
    public class dal通知通告表 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal通知通告表(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_通知通告表.__KeyName; //主表的主键字段
            _SummaryTableName = tb_通知通告表.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_通知通告表.__TableName) ORM = typeof(tb_通知通告表);
            //if (tableName == tb_通知通告表s.__TableName) ORM = typeof(tb_通知通告表s);//如有明细表请调整本行的代码
            if (tableName == tb_通知通告表_附件.__TableName) ORM = typeof(tb_通知通告表_附件);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_通知通告表.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_通知通告表.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 传入where条件查询主表数据
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public DataTable GetSummaryByParam(string docNo)//wxf 2018年9月29日 17:17:13
        {
            string query = "select * from " + _SummaryTableName + " where 1=1 " + docNo;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
            return dt;

        }

        public DataTable MyGetSummaryByParam(string docNo)//wxf 2018年9月29日 17:17:13
        {
            string query = @"select
                             a.ID as ID
                             ,a.GUID as GUID
                             ,a.headline as 标题
                             ,a.value as 内容
                             ,CONVERT(varchar(20),a.createTime,20) as 创建时间
                             ,CONVERT(varchar(20),a.updateTime,20) as 修改时间
                             ,f.P_DESC as 是否置顶
                             ,b.机构名称 as 创建机构
                             ,c.机构名称 as 修改机构
                             ,d.UserName as 创建人
                             ,e.UserName as 修改人
                             from " + _SummaryTableName + @" a 
                             left join tb_机构信息 b on a.createOrg = b.机构编号
                             left join tb_机构信息 c on a.updateOrg = c.机构编号
                             left join tb_MyUser d on a.creator = d.用户编码
                             left join tb_MyUser e on a.modifier = e.用户编码
                             left join (select P_DESC,P_CODE from tb_常用字典 where P_FUN_CODE='sfzd') f on a.setTop=f.P_CODE
                             where 1=1 " + docNo + @" order by a.createTime desc";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
            return dt;
        }

        public DataTable Get_详情(string docNo)
        {
            string query = @"select
                             a.ID as ID
                             ,a.GUID as GUID
                             ,a.headline as 标题
                             ,a.value as 内容
                             ,a.createTime as 创建时间
                             ,a.updateTime as 修改时间
                             ,f.P_DESC as 是否置顶
                             ,b.机构名称 as 创建机构
                             ,c.机构名称 as 修改机构
                             ,d.UserName as 创建人
                             ,e.UserName as 修改人
                             from " + _SummaryTableName + @" a 
                             left join tb_机构信息 b on a.createOrg = b.机构编号
                             left join tb_机构信息 c on a.updateOrg = c.机构编号
                             left join tb_MyUser d on a.creator = d.用户编码
                             left join tb_MyUser e on a.modifier = e.用户编码
                             left join (select P_DESC,P_CODE from tb_常用字典 where P_FUN_CODE='sfzd') f on a.setTop=f.P_CODE
                             where a.GUID='" + docNo + "'";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
            return dt;
        }

        
        public DataTable Show_详情(string orgID)
        {
            string query = @"select GUID,headline,value from tb_通知通告表 where createOrg='" + orgID + "' and setTop='1'";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
            return dt;
        }

        public DataTable Get_置顶(string orgID)
        {
            string query = @"select GUID from tb_通知通告表 where createOrg='" + orgID + "' and setTop='1'";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_通知通告表.__TableName);
            return dt;
        }

        public int Set_不置顶(string GUID)
        {
            string query = @"update tb_通知通告表 set setTop='0' where GUID='"+GUID+"'";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i;
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_通知通告表]    where [" + tb_通知通告表.__KeyName + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_通知通告表_附件]   where [" + tb_通知通告表_附件.__KeyName + "]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_通知通告表.__TableName;
            //ds.Tables[1].TableName = tb_通知通告表_附件.__TableName;//子表
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            //string sql1 = "delete [tb_通知通告表s] where ["+tb_通知通告表.__KeyName+"]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_通知通告表] where [" + tb_通知通告表.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "通知通告表");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

    }
}

