﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtomEHR.Server.DataAccess
{
    public class GetTable
    {
        public static String[] getGetTableName()
        {
            String[] name = new String[53];

            name[0] = "tb_精神病随访记录";// "T_Cjr_Jingshen";
            name[1] = "tb_MXB糖尿病随访表";
            name[2] = "tb_残疾人_视力";
            name[3] = "tb_残疾人_听力";
            name[4] = "tb_残疾人_智力";
            name[5] = "tb_残疾人_肢体";
            name[6] = "tb_健康档案_个人健康特征";
            name[7] = "tb_健康体检";
            name[8] = "tb_精神病随访记录";//"T_JK_GXY_JKTJ";//无 
            name[9] = "";//"T_JK_LNR_JKTJ";//无
            name[10] = "";//".T_JK_TNB_JKTJ";//无
            name[11] = "";//".tb_预防接种";
            name[12] = "";//".T_YF_ZSJL";
            name[13] = "";//".T_YF_ZSJLHZ";
            name[14] = "";//".T_YF_ZYXX";
            name[15] = "";//".H_ZL_HZD";
            name[16] = "";// ".H_ZL_ZCD";
            name[17] = "tb_MXB冠心病随访表";
            name[18] = "tb_MXB高血压随访表";
            name[19] = "tb_家庭档案";
            name[20] = ".tb_MXB脑卒中随访表";
            name[21] = "";// ".T_EB_CSQKJL";
            name[22] = "tb_儿童_健康检查_12月";
            name[23] = "tb_儿童_健康检查_18月";
            name[24] = "tb_儿童_健康检查_24月";
            name[25] = "tb_儿童_健康检查_30月";
            name[26] = "tb_儿童_健康检查_3岁";
            name[27] = "tb_儿童_健康检查_3月";
            name[28] = "tb_儿童_健康检查_满月";
            name[29] = "tb_儿童_健康检查_4岁";
            name[30] = "tb_儿童_健康检查_5岁";
            name[31] = "tb_儿童_健康检查_6岁";
            name[32] = "tb_儿童_健康检查_6月";
            name[33] = "tb_儿童_健康检查_8月";
            name[34] = "";// ".T_EB_JKJC_XSE"; 不用
            name[35] = "tb_儿童_入托检查";
            name[36] = "tb_儿童新生儿访视记录";
            name[37] = "";// ".T_EB_XSEJBSC";不用
            name[38] = "tb_孕妇_产后42天检查记录";
            name[39] = "tb_孕妇_产后访视情况";
            name[40] = "tb_MXB糖尿病管理卡";
            name[41] = "tb_孕妇_产前随访2次";
            name[42] = "tb_孕妇_产前随访3次";
            name[43] = "tb_孕妇_产前随访4次";
            name[44] = "tb_孕妇_产前随访5次";
            name[45] = "tb_MXB脑卒中管理卡";
            name[46] = "tb_妇女保健检查";
            name[47] = "tb_妇女更年期保健检查";
            name[48] = "tb_孕妇_产前随访1次";
            name[49] = "tb_MXB冠心病管理卡";
            name[50] = "tb_MXB高血压管理卡";
            name[51] = "tb_老年人随访";
            name[52] = "tb_老年人中医药特征管理";
            return name;
        }
        public static String[] getGetDeleteTableName()
        {
            String[] name = new String[54];

            name[0] = "tb_精神病随访记录";
            name[1] = "tb_MXB糖尿病随访表";
            name[2] = "tb_残疾人_视力";
            name[3] = "tb_残疾人_听力";
            name[4] = "tb_残疾人_智力";
            name[5] = "tb_残疾人_肢体";
            name[6] = "tb_健康档案_个人健康特征";
            name[7] = "tb_健康体检";
            name[8] = "";// "T_JK_GXY_JKTJ";
            name[9] = "";// "T_JK_LNR_JKTJ";
            name[10] = "";// "T_JK_TNB_JKTJ";
            name[11] = "";// "T_JK_YFJZ";
            name[12] = "";//"T_YF_ZSJL";
            name[13] = "";//"T_YF_ZSJLHZ";
            name[14] = "";//"T_YF_ZYXX";
            name[15] = "";//"H_ZL_HZD";
            name[16] = "";//"H_ZL_ZCD";
            name[17] = "tb_MXB冠心病随访表";
            name[18] = "tb_MXB高血压随访表";
            name[19] = "tb_家庭档案";
            name[20] = "tb_MXB脑卒中随访表";
            name[21] = "";// "T_EB_CSQKJL";
            name[22] = "tb_儿童_健康检查_12月";
            name[23] = "tb_儿童_健康检查_18月";
            name[24] = "tb_儿童_健康检查_24月";
            name[25] = "tb_儿童_健康检查_30月";
            name[26] = "tb_儿童_健康检查_3岁";
            name[27] = "tb_儿童_健康检查_3月";
            name[28] = "tb_儿童_健康检查_满月";
            name[29] = "tb_儿童_健康检查_4岁";
            name[30] = "tb_儿童_健康检查_5岁";
            name[31] = "tb_儿童_健康检查_6岁";
            name[32] = "tb_儿童_健康检查_6月";
            name[33] = "tb_儿童_健康检查_8月";
            name[34] = "";// "T_EB_JKJC_XSE";
            name[35] = "tb_儿童_入托检查";
            name[36] = "tb_儿童新生儿访视记录";
            name[37] = "";// "T_EB_XSEJBSC";
            name[38] = "tb_孕妇_产后42天检查记录";
            name[39] =  "tb_孕妇_产后访视情况";
            name[40] =  "tb_MXB糖尿病管理卡";
            name[41] =  "tb_孕妇_产前随访2次";
            name[42] =  "tb_孕妇_产前随访3次";
            name[43] =  "tb_孕妇_产前随访4次";
            name[44] =  "tb_孕妇_产前随访5次";
            name[45] = "tb_MXB脑卒中管理卡";
            name[46] = "tb_妇女保健检查";
            name[47] = "tb_妇女更年期保健检查";
            name[48] = "tb_孕妇_产前随访1次";
            name[49] = "tb_MXB冠心病管理卡";
            name[50] = "tb_MXB高血压管理卡";
            name[51] = "tb_老年人随访";
            name[52] = "tb_转档申请";
            name[53] = "tb_老年人中医药特征管理";
            return name;
        }
        public static String[] getGetTableInfoName()
        {
            String[] name2 = new String[9];
            /*
                         name2[0] = "DB2ADMIN.T_Cjr_Info";
            name2[1] = "DB2ADMIN.T_Da_Jkda_Rkxzl";
            name2[2] = "DB2ADMIN.T_Cjr_Jsjbxxbcb";
            name2[3] = "DB2ADMIN.T_EB_ETJBXX";
            name2[4] = "DB2ADMIN.T_FB_FUNVINFO";
            name2[5] = "DB2ADMIN.T_FB_YUNINFO";
            name2[6] = "DB2ADMIN.T_JG_MBBASEINFO";
            name2[7] = "DB2ADMIN.T_LNR_INFO";
            name2[8] = "DB2ADMIN.T_JK_INFO";
             */
            name2[0] = "tb_残疾人_基本信息";
            name2[1] = "";// "tb_健康体检";
            name2[2] = "tb_精神疾病信息补充表";
            name2[3] = "tb_儿童基本信息";
            name2[4] = "tb_妇女基本信息";
            name2[5] = "tb_孕妇基本信息";
            name2[6] = "tb_MXB慢病基础信息表";
            name2[7] = "tb_老年人基本信息";
            name2[8] = "tb_健康档案";//个人健康档案 一定要放到最后，！2019年2月13日 前提条件从此表查询，如果在此之前被修改了，后面的都修改不成功

            return name2;
        }
    }
}
