﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 孕妇基本信息的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/30 04:58:10
 *   最后修改: 2015/09/30 04:58:10
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal孕妇基本信息
    /// </summary>
    public class dal孕妇基本信息 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal孕妇基本信息(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_孕妇基本信息.__KeyName; //主表的主键字段
            _SummaryTableName = tb_孕妇基本信息.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_孕妇基本信息.__TableName) ORM = typeof(tb_孕妇基本信息);
            if (tableName == tb_健康档案.__TableName) ORM = typeof(tb_健康档案);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_孕妇基本信息.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_孕妇基本信息.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_孕妇基本信息.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_孕妇基本信息]    where [" + tb_孕妇基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.__KeyName + "]=@DocNo2 "; //个人档案
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_孕妇基本信息.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;//个人档案
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_孕妇基本信息s] where [" + tb_孕妇基本信息.__KeyName + "]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_孕妇基本信息] where [" + tb_孕妇基本信息.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "孕妇基本信息");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportDataBYLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇基本信息 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇基本信息 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )	
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )	
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )	
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            #endregion
            return ds;
        }
        public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.建档时间 >= (@DateFrom) and jktj.建档时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.建档时间 >= (@DateFrom)  and jktj.建档时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.建档时间 >= (@DateFrom)  and jktj.建档时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.建档时间 >= (@DateFrom)  and jktj.建档时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇基本信息 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇基本信息 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
 ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.建档时间 >= (@DateFrom)
                                        and jktj.建档时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.建档时间 >= (@DateFrom)
                                        and jktj.建档时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_孕妇基本信息  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.建档时间 >= (@DateFrom)
                                        and jktj.建档时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            #endregion
            return ds;
        }
        #region 新增的方法 add by wjz 20151022
        public DataSet GetBusinessByGrdabh(string grdabh)
        {
            string sql1 = " select * from [tb_孕妇基本信息]    where [" + tb_孕妇基本信息.__KeyName + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_孕妇基本信息s]   where [" + tb_孕妇基本信息.__KeyName + "]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdabh.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_孕妇基本信息.__TableName;
            //ds.Tables[1].TableName =tb_孕妇基本信息s.__TableName;//子表
            return ds;
        }

        public void UpdateInfoByGrdabh(string grdabh, string sfflag, string xcsfsj)
        {
            //删除单据:从子表开始删除!!!
            string sql2 = "update [tb_孕妇基本信息] set [" + tb_孕妇基本信息.SFFLAG + "]=@sfflag, [" + tb_孕妇基本信息.下次随访时间 + "]=@xcsfsj where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            cmd.AddParam("@sfflag", SqlDbType.VarChar, sfflag.Trim());
            cmd.AddParam("@xcsfsj", SqlDbType.VarChar, xcsfsj.Trim());
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdabh.Trim());
            DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
        }
        #endregion

        //孕产妇系统管理情况登记
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prgid"></param>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <returns></returns>
        public DataSet GetYunChanFuInfo(string prgid, string sDate, string eDate, string p4, string p5)
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("USP_孕产妇系统管理情况", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@prgid"].Value = prgid;
                sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@sDate"].Value = sDate;
                sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@eDate"].Value = eDate;
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                adapter.Fill(dataSet);
            }
            return dataSet;

            #region OldField
            //            string sql = string.Empty;
            //            DataSet ds = null;
            //            #region 查询语句
            //            sql = @"
            //            select
            //a.个人档案编号,a.姓名 as '孕产妇姓名',a.户口所在地,a.居住地址 as '住址',a.年龄,a.孕次,a.产次,
            //b.末次月经,b.预产期,a.建档时间 as '建册时间',
            //b.填表日期+' '+CAST( b.孕周 as varchar(2))+' '+b.孕周天 as '8周',
            //c.发生时间+' '+CAST( c.孕周 as varchar(2))+' '+c.孕周天 as '16周',
            //d.发生时间+' '+convert(varchar, d.孕周 ,2 )+' '+d.孕周天 as '24周',
            //e.发生时间+' '+CAST( e.孕周 as varchar(2))+' '+e.孕周天 as '32周',
            //f.发生时间+' '+CAST( f.孕周 as varchar(2))+' '+f.孕周天 as '38周',
            //tt.随访日期 as '产后访视次数',
            //(select top 1 出生日期 from tb_儿童基本信息 where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '分娩时间',
            //(select top 1 tb_儿童新生儿访视记录.出生孕周 from tb_儿童基本信息 
            //		left  join tb_儿童新生儿访视记录  on tb_儿童基本信息.个人档案编号=tb_儿童新生儿访视记录.个人档案编号
            // where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '孕周',
            //(select top 1 tb_儿童新生儿访视记录.助产机构名称 from tb_儿童基本信息 
            //		left  join tb_儿童新生儿访视记录  on tb_儿童基本信息.个人档案编号=tb_儿童新生儿访视记录.个人档案编号
            // where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '分娩地点',
            //(select top 1 性别 from tb_儿童基本信息 where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '性别',
            //(select top 1 tb_儿童新生儿访视记录.Apgar评分1 from tb_儿童基本信息 
            //		left  join tb_儿童新生儿访视记录  on tb_儿童基本信息.个人档案编号=tb_儿童新生儿访视记录.个人档案编号
            // where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '阿氏评分1分钟',
            //(select top 1 tb_儿童新生儿访视记录.出生身长 from tb_儿童基本信息 
            //		left  join tb_儿童新生儿访视记录  on tb_儿童基本信息.个人档案编号=tb_儿童新生儿访视记录.个人档案编号
            // where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '出生身高',
            //(select top 1 tb_儿童新生儿访视记录.新生儿出生体重 from tb_儿童基本信息 
            //		left  join tb_儿童新生儿访视记录  on tb_儿童基本信息.个人档案编号=tb_儿童新生儿访视记录.个人档案编号
            // where tb_儿童基本信息.母亲身份证号 = aa.身份证号
            //	order by tb_儿童基本信息.出生日期 desc 
            //) as '出生体重',
            //b.死胎,b.死产,b.新生儿死亡,
            //m.随访日期 as '产后42天检查'
            //from tb_孕妇_产后访视情况 tt 
            //LEFT JOIN tb_健康档案 aa on tt.个人档案编号 = aa.个人档案编号 --and tt.孕次 = aa.孕次
            //left join  tb_孕妇基本信息 a  on aa.个人档案编号 = a.个人档案编号 and aa.孕次 = a.孕次
            //left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
            //left  join tb_孕妇_产前随访2次 c on aa.个人档案编号=c.个人档案编号 and aa.孕次 = c.孕次
            //left  join tb_孕妇_产前随访3次 d on aa.个人档案编号=d.个人档案编号 and aa.孕次 = d.孕次
            //left  join tb_孕妇_产前随访4次 e  on aa.个人档案编号=e.个人档案编号 and aa.孕次 = e.孕次
            //left  join tb_孕妇_产前随访5次 f on aa.个人档案编号=f.个人档案编号 and aa.孕次 = f.孕次
            //left  join tb_孕妇_产后42天检查记录 m  on aa.个人档案编号=m.个人档案编号 and aa.产次 = m.产次
            //where (len(aa.所属机构)>=15 and substring(aa.所属机构,1,7)+'1'+substring(aa.所属机构,9,7) like @prgid +'%'
            //       or aa.所属机构 like @prgid+ '%')
            //and  tt.随访日期 > @sDate and tt.随访日期 < @eDate
            //            ";

            //            #endregion

            //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            //cmd.AddParam("@prgid", SqlDbType.VarChar, prgid);
            //cmd.AddParam("@sDate", SqlDbType.VarChar, sDate);
            //cmd.AddParam("@eDate", SqlDbType.VarChar, eDate);
            ////cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
            //ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            //return ds;
            #endregion
        }

        public DataSet GetYunChanFuInfoNew(string prgid, string fmBDate, string fmEDate,string chsfDate1, string chsfDate2, string name, string sfzh, string dah, string yzbegin, string yzend)
        {
            DataSet dataSet = new DataSet();

            StringBuilder sqlsb = new StringBuilder();
            sqlsb.Append(@"select distinct 
	aa.家庭档案编号,
	aa.个人档案编号,aa.姓名 as '孕产妇姓名',a.户口所在地,aa.居住地址 as '住址',a.年龄,a.孕次,a.产次,
	b.末次月经,b.预产期,a.建档时间 as '建册时间',
	b.填表日期+' '+cast( ISNULL(b.孕周,'') as varchar(2))+' + '+ISNULL(b.孕周天,'')+' + ' + ISNULL(b.总体评估其他,'') as '8周',
	c.发生时间+' '+cast( ISNULL(c.孕周,'') as varchar(2))+' + '+isnull(c.孕周天,'')+' + ' + ISNULL(c.主诉,'') as '16周',
	d.发生时间+' '+convert(varchar, ISNULL(d.孕周,'') ,2 )+' + '+ISNULL(d.孕周天,'') +' + ' + ISNULL(d.主诉,'') as '24周',
	e.发生时间+' '+cast( ISNULL(e.孕周,'') as varchar(2))+' + '+ISNULL(e.孕周天,'')+' + ' + ISNULL(e.主诉,'') as '32周',
	f.发生时间+' '+cast( ISNULL(f.孕周,'') as varchar(2))+' + '+ISNULL(f.孕周天,'')+' + ' + ISNULL(f.主诉,'') as '38周',
	tt.随访日期 as '产后访视次数',
	tt.分娩日期 as '分娩时间',
	cc.出生孕周 as '孕周',
	cc.助产机构名称 as '分娩地点',
	(select  P_DESC from tb_常用字典 where P_FUN_DESC = '性别' and P_CODE =  bb.性别 )as '性别',
	cc.Apgar评分1 as '阿氏评分1分钟',
	cc.出生身长 as '出生身高',
	cc.新生儿出生体重 as '出生体重',
	b.死胎,b.死产,b.新生儿死亡,
	m.随访日期 as '产后42天检查'
from tb_孕妇_产后访视情况 tt  
	inner join tb_健康档案 aa on aa.个人档案编号 = tt.个人档案编号 and aa.产次 = tt.产次 and aa.档案状态='1'
	left join tb_孕妇_产后42天检查记录 m  on aa.个人档案编号=m.个人档案编号 and aa.产次 = m.产次
	left join tb_孕妇基本信息 a  on aa.个人档案编号 = a.个人档案编号 --and aa.孕次 = a.孕次
	left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
	left join tb_孕妇_产前随访2次 c on aa.个人档案编号=c.个人档案编号 and aa.孕次 = c.孕次
	left join tb_孕妇_产前随访3次 d on aa.个人档案编号=d.个人档案编号 and aa.孕次 = d.孕次
	left join tb_孕妇_产前随访4次 e  on aa.个人档案编号=e.个人档案编号 and aa.孕次 = e.孕次
	left join tb_孕妇_产前随访5次 f on aa.个人档案编号=f.个人档案编号 and aa.孕次 = f.孕次
	left join tb_儿童基本信息 bb on aa.身份证号 = bb.母亲身份证号 and isnull(bb.母亲身份证号,'') <> ''
	and bb.出生日期 = tt.分娩日期
	left join tb_儿童新生儿访视记录 cc on bb.个人档案编号 = cc.个人档案编号	
where (len(aa.所属机构)>=15 and substring(aa.所属机构,1,7)+'1'+substring(aa.所属机构,9,7) like @prgid +'%'	or aa.所属机构 like @prgid+ '%')
	and isnull(a.个人档案编号,'') <> '' and (aa.档案状态='1'  or isnull(tt.失访情况,'')='1')");


            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(new SqlParameter("@prgid", prgid));
            if (!string.IsNullOrWhiteSpace(fmBDate))
            {
                paramlist.Add(new SqlParameter("@fmBDate", fmBDate));
                sqlsb.Append(" and convert(date,tt.分娩日期) >=@fmBDate ");
            }

            if (!string.IsNullOrWhiteSpace(fmEDate))
            {
                paramlist.Add(new SqlParameter("@fmEDate", fmEDate));
                sqlsb.Append(" and convert(date,tt.分娩日期) < @fmEDate ");
            }

            if (!string.IsNullOrWhiteSpace(chsfDate1))
            {
                paramlist.Add(new SqlParameter("@chsfDate1", chsfDate1));
                sqlsb.Append(" and convert(date, tt.随访日期) >= @chsfDate1 ");
            }

            if (!string.IsNullOrWhiteSpace(chsfDate2))
            {
                paramlist.Add(new SqlParameter("@chsfDate2", chsfDate2));
                sqlsb.Append(" and convert(date,tt.随访日期) < @chsfDate2 ");
            }

            if(!string.IsNullOrWhiteSpace(name))
            {
                paramlist.Add(new SqlParameter("@name", name));
                sqlsb.Append(" and aa.姓名 like '%'+@name+'%' ");
            }

            if (!string.IsNullOrWhiteSpace(sfzh))
            {
                paramlist.Add(new SqlParameter("@sfzh", sfzh));
                sqlsb.Append(" and aa.身份证号=@sfzh ");
            }

            if (!string.IsNullOrWhiteSpace(dah))
            {
                paramlist.Add(new SqlParameter("@dah", dah));
                sqlsb.Append(" and aa.个人档案编号=@dah ");
            }

            if(!string.IsNullOrWhiteSpace(yzbegin) && !string.IsNullOrWhiteSpace(yzend))
            {
                sqlsb.Append(" and b.孕周<>'' ");
            }

            if(!string.IsNullOrWhiteSpace(yzbegin))
            {
                paramlist.Add(new SqlParameter("@yzbegin", yzbegin));
                sqlsb.Append(" and cast(b.孕周 as int) >= @yzbegin ");
            }

            if (!string.IsNullOrWhiteSpace(yzend))
            {
                paramlist.Add(new SqlParameter("@yzend", yzend));
                sqlsb.Append(" and cast(b.孕周 as int) < @yzend ");
            }


            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                //SqlCommand sqlCommand = new SqlCommand("USP_孕产妇系统管理情况New", sqlConn);
                //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.NVarChar));
                //sqlCommand.Parameters["@prgid"].Value = prgid;
                //sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.NVarChar));
                //sqlCommand.Parameters["@sDate"].Value = sDate;
                //sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.NVarChar));
                //sqlCommand.Parameters["@eDate"].Value = eDate;

                //sqlCommand.Parameters.Add(new SqlParameter("@name", System.Data.SqlDbType.NVarChar));
                //sqlCommand.Parameters["@name"].Value = name;

                //sqlCommand.Parameters.Add(new SqlParameter("@sfzh", System.Data.SqlDbType.VarChar));
                //sqlCommand.Parameters["@sfzh"].Value = sfzh;

                //sqlCommand.Parameters.Add(new SqlParameter("@dah", System.Data.SqlDbType.VarChar));
                //sqlCommand.Parameters["@dah"].Value = dah;

                //sqlCommand.Parameters.Add(new SqlParameter("@yzbegin", System.Data.SqlDbType.VarChar));
                //sqlCommand.Parameters["@yzbegin"].Value = yzbegin;

                //sqlCommand.Parameters.Add(new SqlParameter("@yzend", System.Data.SqlDbType.VarChar));
                //sqlCommand.Parameters["@yzend"].Value = yzend;

                SqlCommand sqlCommand = sqlConn.CreateCommand();
                sqlCommand.CommandText = sqlsb.ToString();
                sqlCommand.Parameters.AddRange(paramlist.ToArray());

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                adapter.Fill(dataSet);
            }
            return dataSet;
        }

        
        /// <summary>
        /// 2019年4月22日 yufh 修改调整
        /// </summary>
        /// <param name="prgid"></param>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        public DataSet GetYunFuInfo(string prgid, string sDate, string eDate, string SearchBy, string selectType)
        {
            string sql = string.Empty;
            DataSet dataSet = new DataSet();
            if (selectType == "1")
            {
                using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
                {
                    SqlCommand sqlCommand = new SqlCommand("USP_孕产妇系统管理率", sqlConn);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.VarChar));
                    sqlCommand.Parameters["@prgid"].Value = prgid;
                    sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.VarChar));
                    sqlCommand.Parameters["@sDate"].Value = sDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.VarChar));
                    sqlCommand.Parameters["@eDate"].Value = eDate;
                    sqlCommand.Parameters.Add(new SqlParameter("@SearchBy", System.Data.SqlDbType.VarChar));
                    sqlCommand.Parameters["@SearchBy"].Value = SearchBy;
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.CommandTimeout = 0;
                    adapter.Fill(dataSet);
                }
            }
            else if (selectType == "2")
            {
                sql = @"select a.ID,  a.个人档案编号,a.家庭档案编号,a.姓名
	,a.性别 ,a.出生日期,a.身份证号,a.街道,a.居委会,a.居住地址
	,aa.联系人电话,aa.联系人姓名,aa.本人电话
	,(select 机构名称 from tb_机构信息 where 机构编号 = a.所属机构)所属机构
	,(select UserName from tb_MyUser where 用户编码 = a.创建人) 创建人,a.创建时间 
from vw_孕妇产后访视情况表 a 
left join tb_健康档案 aa on aa.个人档案编号 = a.个人档案编号 and aa.产次 = a.产次 
left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
where a.档案状态='1' 
	and isnull(a.失访情况,'')<>'1'
	and (len(a.所属机构)>=15 and substring(a.所属机构,1,7)+'1'+substring(a.所属机构,9,7) like @机构+'%'
		or a.所属机构 like  @机构+'%')";
                if (SearchBy == "1")
                {
                    sql += @"  and a.随访日期 >=@DateFrom and a.随访日期 <=@DateTo ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"  and a.分娩日期 >=@DateFrom and a.分娩日期 <=@DateTo ";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, prgid);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, sDate);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, eDate);
                dataSet = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                dataSet.Tables[0].TableName = "#统计";
            }
            else if (selectType == "3")
            {
                sql = @"select a.ID,  a.个人档案编号,a.家庭档案编号,a.姓名
	,a.性别 ,a.出生日期,a.身份证号,a.街道,a.居委会,a.居住地址
	,aa.联系人电话,aa.联系人姓名,aa.本人电话
	,(select 机构名称 from tb_机构信息 where 机构编号 = a.所属机构)所属机构
	,(select UserName from tb_MyUser where 用户编码 = a.创建人) 创建人,a.创建时间 
from vw_孕妇产后访视情况表 a 
left join tb_健康档案 aa on aa.个人档案编号 = a.个人档案编号 and aa.产次 = a.产次 
left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
where a.档案状态='1' 
	and isnull(a.失访情况,'')<>'1' and (b.孕周>4 and b.孕周<13)
	and (len(a.所属机构)>=15 and substring(a.所属机构,1,7)+'1'+substring(a.所属机构,9,7) like @机构+'%'
		or a.所属机构 like  @机构+'%')";
                if (SearchBy == "1")
                {
                    sql += @"  and a.随访日期 >=@DateFrom and a.随访日期 <=@DateTo ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"  and a.分娩日期 >=@DateFrom and a.分娩日期 <=@DateTo ";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, prgid);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, sDate);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, eDate);
                dataSet = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                dataSet.Tables[0].TableName = "#统计";
            }
            return dataSet;
        }

    }
}

