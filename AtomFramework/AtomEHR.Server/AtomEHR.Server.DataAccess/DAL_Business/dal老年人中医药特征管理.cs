﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*      
 *   程序说明: 老年人中医药特征管理的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/23 08:00:37
 *   最后修改: 2015/09/23 08:00:37
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *      */

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal老年人中医药特征管理
    /// </summary>
    public class dal老年人中医药特征管理 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal老年人中医药特征管理(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_老年人中医药特征管理.__KeyName; //主表的主键字段
            _SummaryTableName = tb_老年人中医药特征管理.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_老年人中医药特征管理.__TableName) ORM = typeof(tb_老年人中医药特征管理);
            if (tableName == tb_老年人基本信息.__TableName) ORM = typeof(tb_老年人基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_健康体检.__TableName) ORM = typeof(tb_健康体检);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_老年人中医药特征管理.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_老年人中医药特征管理.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_老年人中医药特征管理.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// 需要更新
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人中医药特征管理]    where 1=2 ";
            string sql3 = " select  top 1 * from tb_健康体检   where [" + tb_健康体检.个人档案编号 + "]=@DocNo2 order by  体检日期 desc";

            //string sql2 = " select * from [tb_老年人随访s]   where ["+tb_老年人随访.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人中医药特征管理.__TableName;//子表
            ds.Tables[2].TableName = tb_健康体检.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataSet GetOneDataByKeys(string docNo, string id)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人中医药特征管理]   where [" + tb_老年人中医药特征管理.__KeyName + "]=@DocNo2 "; //子表
            string sql3 = " select top 1 * from tb_健康体检   where [" + tb_健康体检.个人档案编号 + "]=@DocNo3 and 体检日期 like ''+  (select substring(发生时间,1,4) from [dbo].[tb_老年人中医药特征管理] where ID = @DocNo4 )+'%'  order by  体检日期 desc ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, id.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人中医药特征管理.__TableName;//子表
            ds.Tables[2].TableName = tb_健康体检.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取全部业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetAllDataByKey(string docNo)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人中医药特征管理]   where [" + tb_老年人中医药特征管理.个人档案编号 + "]=@DocNo2 "; //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人中医药特征管理.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取全部业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetAllDataValueByKey(string docNo)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = @" SELECT ID,
									SUBSTRING(发生时间,1,4) AS 年份,
                                    个人档案编号,
                                    发生时间 AS 随访时间,
                                    CASE WHEN 气虚质辨识 = '1' THEN 
                                    '得分：'+CAST(气虚质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  气虚质辨识 = '2' THEN  
                                    '得分：'+CAST(气虚质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(气虚质得分 AS VARCHAR) 
                                    END
                                    AS 气虚质,
                                    CASE WHEN 阳虚质辨识 = '1' THEN 
                                    '得分：'+CAST(阳虚质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  阳虚质辨识 = '2' THEN  
                                    '得分：'+CAST(阳虚质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(阳虚质得分 AS VARCHAR) 
                                    END
                                    AS 阳虚质,
                                    CASE WHEN 阴虚质辨识 = '1' THEN 
                                    '得分：'+CAST(阴虚质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  阴虚质辨识 = '2' THEN  
                                    '得分：'+CAST(阴虚质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(阴虚质得分 AS VARCHAR) 
                                    END
                                    AS 阴虚质,
                                    CASE WHEN 气虚质辨识 = '1' THEN 
                                    '得分：'+CAST(气虚质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  气虚质辨识 = '2' THEN  
                                    '得分：'+CAST(气虚质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(气虚质得分 AS VARCHAR) 
                                    END
                                    AS 气虚质,
                                    CASE WHEN 痰湿质辨识 = '1' THEN 
                                    '得分：'+CAST(痰湿质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  痰湿质辨识 = '2' THEN  
                                    '得分：'+CAST(痰湿质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(痰湿质得分 AS VARCHAR) 
                                    END
                                    AS 痰湿质,
                                    CASE WHEN 湿热质辨识 = '1' THEN 
                                    '得分：'+CAST(湿热质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  湿热质辨识 = '2' THEN  
                                    '得分：'+CAST(湿热质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(湿热质得分 AS VARCHAR) 
                                    END
                                    AS 湿热质,
                                    CASE WHEN 血瘀质辨识 = '1' THEN 
                                    '得分：'+CAST(血瘀质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  血瘀质辨识 = '2' THEN  
                                    '得分：'+CAST(血瘀质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(血瘀质得分 AS VARCHAR) 
                                    END
                                    AS 血瘀质,
                                    CASE WHEN 气郁质辨识 = '1' THEN 
                                    '得分：'+CAST(气郁质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  气郁质辨识 = '2' THEN  
                                    '得分：'+CAST(气郁质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(气郁质得分 AS VARCHAR) 
                                    END
                                    AS 气郁质,
                                    CASE WHEN 特禀质辨识 = '1' THEN 
                                    '得分：'+CAST(特禀质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  特禀质辨识 = '2' THEN  
                                    '得分：'+CAST(特禀质得分 AS VARCHAR)  + '       辨识：倾向是'   
                                    ELSE 
                                    '得分：'+CAST(特禀质得分 AS VARCHAR) 
                                    END
                                    AS 特禀质,
                                    CASE WHEN 平和质辨识 = '1' THEN 
                                    '得分：'+CAST(平和质得分 AS VARCHAR)  + '       辨识：是'   
                                    WHEN  平和质辨识 = '2' THEN  
                                    '得分：'+CAST(平和质得分 AS VARCHAR)  + '       辨识：基本是'   
                                    ELSE 
                                    '得分：'+CAST(平和质得分 AS VARCHAR) 
                                    END
                                    AS 平和质
                                    FROM [TB_老年人中医药特征管理]   
   where [" + tb_老年人中医药特征管理.个人档案编号 + "]=@DocNo2 "; //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人中医药特征管理.__TableName;//子表
            return ds;
        }
        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string id)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_老年人中医药特征管理] where [" + tb_老年人中医药特征管理.__KeyName + "]=@DocNo1";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, id.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "老年人中医药特征管理");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }
        /// <summary>
        /// 根据条件查询数据
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public DataTable GetDataByPrarm(Dictionary<string, string> dict)
        {
            //dict.Add("机构", this.cbo所属机构.EditValue.ToString());
            //dict.Add("包含下属机构", this.checkEdit1.Checked ? "1" : "2");
            //dict.Add("姓名", this.txt姓名.Text.Trim());
            //dict.Add("身份证号", this.txt身份证号.Text.Trim());
            //dict.Add("录入时间1", this.uC录入时间.Dte1.Text.Trim());
            //dict.Add("录入时间2", this.uC录入时间.Dte2.Text.Trim());
            //dict.Add("个人档案编号", this.txt档案编号.Text.Trim());
            //dict.Add("档案状态", util.ControlsHelper.GetComboxKey(cbo档案状态));
            //dict.Add("出生日期1", this.uC出生日期.Dte1.Text.Trim());
            //dict.Add("出生日期2", this.uC出生日期.Dte2.Text.Trim());
            //dict.Add("年 龄1", this.uc年龄区间.Txt1.Text.Trim());
            //dict.Add("年 龄2", this.uc年龄区间.Txt2.Text.Trim());
            //dict.Add("录入人", this.cbo录入人.EditValue.ToString());
            //dict.Add("镇", util.ControlsHelper.GetComboxKey(cbo镇));
            //dict.Add("居委会", util.ControlsHelper.GetComboxKey(cbo居委会));
            //dict.Add("详细地址", this.txt详细地址.Text.Trim());
            //dict.Add("选项", this.cbo人数.SelectedIndex.ToString());
            //dict.Add("是否做中医体质", (this.cbo是否做中医.SelectedIndex + 1).ToString());
            DataTable dt = null;
            if (dict["是否做中医体质"] == "1")//已做
            {
                if (dict["选项"] == "1")//人数
                {
                    dt = this.SearchlistPagenum(dict);
                }
                dt = this.SearchlistPage(dict);
            }
            return dt;
        }

        private DataTable SearchlistPage(Dictionary<string, string> dict)
        {
            #region 拼sql

            string sql = @"SELECT [ID]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[联系电话]
                                              ,[省]
                                              ,[市]
                                              ,[区]
                                              ,[街道]
                                              ,[居委会]
                                              ,[居住地址]
                                              ,[出生日期]
                                              ,[所属机构]
                                              ,[创建时间]
                                              ,[创建人]
                                          FROM [tb_老年人基本信息] INNER JOIN  ";
            if (!string.IsNullOrEmpty(dict["姓名"]))
            {
                sql += " AND 姓名 = '" + dict["姓名"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["出生日期1"]))
            {
                sql += " AND 出生日期 >= '" + dict["出生日期1"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["出生日期2"]))
            {
                sql += " AND 出生日期 <= '" + dict["出生日期2"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["镇"]))
            {
                sql += " AND 街道 = '" + dict["镇"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["居委会"]))
            {
                sql += " AND 居委会 = '" + dict["居委会"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["详细地址"]))
            {
                sql += " AND 居住地址 LIKE '%" + dict["居住地址"] + "%' ";
            }
            if (!string.IsNullOrEmpty(dict["身份证号"]))
            {
                sql += " AND 身份证号 = '" + dict["身份证号"] + "' ";
            }
            sql += " AND 个人档案编号 IN ( select 个人档案编号 from tb_老年人中医药特征管理  where 1=1 ";
            if (!string.IsNullOrEmpty(dict["录入时间1"]))
            {
                sql += " AND 创建时间 >= '" + dict["录入时间1"] + " 00:00:00 ' ";
            }
            if (!string.IsNullOrEmpty(dict["录入时间2"]))
            {
                sql += " AND 创建时间 <= '" + dict["录入时间2"] + " 00:00:00 ' ";
            }
            string pgrid = _Loginer.所属机构;
            if (!string.IsNullOrEmpty(dict["包含下属机构"]) && dict["包含下属机构"] == "2")
            {
                if (pgrid.Length < 7)
                {
                    sql += " AND 所属机构 like '" + dict["机构"] + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    sql += " AND 所属机构 = '" + dict["机构"] + "'  OR (length(所属机构)=15 and 所属机构 like '" + dict["机构"] + "%'";
                }
                if (pgrid.Length == 15)
                {
                    sql += " AND 所属机构 = '" + dict["机构"] + "' ";
                }
            }
            else
            {
                sql += " AND 所属机构 = '" + dict["机构"] + "' ";
            }

            if (!string.IsNullOrEmpty(dict["录入人"]))
            {
                sql += " AND 创建人 = '" + dict["录入人"] + "' ";
            }
            sql += "group by 个人档案编号 )";

            #endregion

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_老年人信息");
        }

        private DataTable SearchlistPagenum(Dictionary<string, string> dict)
        {
            #region 拼sql

            string sql = @"SELECT [ID]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[联系电话]
                                              ,[省]
                                              ,[市]
                                              ,[区]
                                              ,[街道]
                                              ,[居委会]
                                              ,[居住地址]
                                              ,[出生日期]
                                              ,[所属机构]
                                              ,[创建时间]
                                              ,[创建人]
                                          FROM [tb_老年人基本信息] WHERE 1 =1 ";
            if (!string.IsNullOrEmpty(dict["姓名"]))
            {
                sql += " AND 姓名 = '" + dict["姓名"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["出生日期1"]))
            {
                sql += " AND 出生日期 >= '" + dict["出生日期1"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["出生日期2"]))
            {
                sql += " AND 出生日期 <= '" + dict["出生日期2"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["镇"]))
            {
                sql += " AND 街道 = '" + dict["镇"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["居委会"]))
            {
                sql += " AND 居委会 = '" + dict["居委会"] + "' ";
            }
            if (!string.IsNullOrEmpty(dict["居住地址"]))
            {
                sql += " AND 居住地址 LIKE '%" + dict["居住地址"] + "%' ";
            }
            if (!string.IsNullOrEmpty(dict["身份证号"]))
            {
                sql += " AND 身份证号 = '" + dict["身份证号"] + "' ";
            }
            sql += " AND 个人档案编号 IN ( select 个人档案编号 from tb_老年人中医药特征管理  where 1=1 ";
            if (!string.IsNullOrEmpty(dict["录入时间1"]))
            {
                sql += " AND 创建时间 >= '" + dict["录入时间1"] + " 00:00:00 ' ";
            }
            if (!string.IsNullOrEmpty(dict["录入时间2"]))
            {
                sql += " AND 创建时间 <= '" + dict["录入时间2"] + " 00:00:00 ' ";
            }
            string pgrid = _Loginer.所属机构;
            if (!string.IsNullOrEmpty(dict["包含下属机构"]) && dict["包含下属机构"] == "2")
            {
                if (pgrid.Length < 7)
                {
                    sql += " AND 所属机构 like '" + dict["机构"] + "%' ";
                }
                if (pgrid.Length == 12)
                {
                    sql += " AND 所属机构 = '" + dict["机构"] + "'  OR (length(所属机构)=15 and 所属机构 like '" + dict["机构"] + "%'";
                }
                if (pgrid.Length == 15)
                {
                    sql += " AND 所属机构 = '" + dict["机构"] + "' ";
                }
            }
            else
            {
                sql += " AND 所属机构 = '" + dict["机构"] + "' ";
            }

            if (!string.IsNullOrEmpty(dict["录入人"]))
            {
                sql += " AND 创建人 = '" + dict["录入人"] + "' ";
            }
            sql += "group by 个人档案编号 )";

            #endregion

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_老年人信息");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public bool update体检信息(string year,string sql)
        {
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        /// 根据条件查询数据
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public DataSet GetInfoByOldZYY(string docNo,string _date)
        {
            string sql1 = " select * from tb_老年人基本信息 where [" + tb_老年人基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from tb_老年人中医药特征管理  where [" + tb_老年人中医药特征管理.个人档案编号 + "]=@DocNo2 and 发生时间=@DocNo4"; //子表
            string sql3 = " select * from tb_健康档案   where [" + tb_健康档案.个人档案编号 + "]=@DocNo3 ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, _date.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人中医药特征管理.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案.__TableName;
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:26
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            string sql = " select 发生时间 from [tb_老年人中医药特征管理]   where [" + tb_老年人中医药特征管理.个人档案编号 + "]=@DocNo1 order by 发生时间 desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_老年人中医药特征管理.__TableName);
            return dt;
        }
        //End


        //Begin WXF 2018-11-07 | 17:39
        //根据档案号与随访日期返回一条数据 
        public DataRow Get_One中医药管理(string grdah, string str_CreateDT)
        {
            string sql = " select top 1 * from [tb_老年人中医药特征管理]   where [" + tb_老年人中医药特征管理.个人档案编号 + "]=@DocNo1 and [" + tb_老年人中医药特征管理.发生时间 + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, str_CreateDT.Trim());
            DataRow dr = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康体检.__TableName).Rows[0];
            return dr;
        }
        //End
											 
    }
}

