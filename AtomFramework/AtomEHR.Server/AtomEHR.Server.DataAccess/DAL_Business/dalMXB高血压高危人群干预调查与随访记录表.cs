﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: MXB高血压高危人群干预调查与随访记录表的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018-11-12 03:10:35
 *   最后修改: 2018-11-12 03:10:35
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dalMXB高血压高危人群干预调查与随访记录表
    /// </summary>
    public class dalMXB高血压高危人群干预调查与随访记录表 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalMXB高血压高危人群干预调查与随访记录表(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_MXB高血压高危人群干预调查与随访记录表.__KeyName; //主表的主键字段
             _SummaryTableName = tb_MXB高血压高危人群干预调查与随访记录表.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_MXB高血压高危人群干预调查与随访记录表.__TableName) ORM = typeof(tb_MXB高血压高危人群干预调查与随访记录表);
             if (tableName == tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.__TableName) ORM = typeof(tb_MXB高血压高危人群干预调查与随访记录表_个人膳食);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_MXB高血压高危人群干预调查与随访记录表.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_MXB高血压高危人群干预调查与随访记录表.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_MXB高血压高危人群干预调查与随访记录表.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_MXB高血压高危人群干预调查与随访记录表]    where ["+tb_MXB高血压高危人群干预调查与随访记录表.个人档案编号+"]=@DocNo1 ";
              string sql2 = " select * from [tb_MXB高血压高危人群干预调查与随访记录表_个人膳食]   where [" + tb_MXB高血压高危人群干预调查与随访记录表.个人档案编号 + "]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB高血压高危人群干预调查与随访记录表.__TableName;
              ds.Tables[1].TableName = tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.__TableName;//子表
              return ds;
          }

          /// <summary>
          /// 获取一个病人的一条表单数据
          /// </summary>
          /// <param name="ID"></param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByID(string ID)
          {
              string sql1 = " select * from [tb_MXB高血压高危人群干预调查与随访记录表]    where [" + tb_MXB高血压高危人群干预调查与随访记录表.ID + "]=@ID1 ";
              string sql2 = " select a.* from  [tb_MXB高血压高危人群干预调查与随访记录表_个人膳食]  a  where exists(select 1 from [tb_MXB高血压高危人群干预调查与随访记录表] b  where b.ID = @ID2 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间)";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@ID1", SqlDbType.VarChar, ID.Trim());
              cmd.AddParam("@ID2", SqlDbType.VarChar, ID.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB高血压高危人群干预调查与随访记录表.__TableName;
              ds.Tables[1].TableName = tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              string sql1 = "delete a from  [tb_MXB高血压高危人群干预调查与随访记录表_个人膳食]  a  where exists(select 1 from tb_MXB高血压高危人群干预调查与随访记录表 b  where b.ID = @DocNo1 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间)  ";//删除子表
              string sql2 = "delete [tb_MXB高血压高危人群干预调查与随访记录表] where ["+tb_MXB高血压高危人群干预调查与随访记录表.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "MXB高血压高危人群干预调查与随访记录表");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

        /// <summary>
        /// 获取打印数据
        /// </summary>
        /// <param name="docNo"></param>
        /// <param name="year"></param>
        /// <returns></returns>
          public DataSet GetReportData(string docNo, List<string> year)
          {
              string dates = string.Empty;
              if (year != null)
              {
                  for (int i = 0; i < year.Count; i++)
                  {
                      dates = dates + "'" + year[i].Trim() + "'" + ",";
                  }
              }
              if (dates.Length > 1)
              {
                  dates = dates.Substring(0, dates.Length - 1);
              }
              string sql1 = " select * from [tb_MXB高血压高危人群干预调查与随访记录表] where [个人档案编号]=@docNo and 随访日期 in (" + dates + ") order by 随访日期 asc";
              string sql2 = " select * from [tb_MXB高血压高危人群干预调查与随访记录表_个人膳食] a where exists(select 1 from tb_MXB高血压高危人群干预调查与随访记录表 b  where b.个人档案编号=@docNo and 随访日期 in (" + dates + ") and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间) ";
              string sql3 = " select * from [tb_健康档案] where[" + tb_健康档案.个人档案编号 + "]=@DocNo2";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
              cmd.AddParam("@docNo", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB高血压高危人群干预调查与随访记录表.__TableName;
              ds.Tables[1].TableName = tb_MXB高血压高危人群干预调查与随访记录表_个人膳食.__TableName;
              ds.Tables[2].TableName = tb_健康档案.__TableName;
              return ds;
          }

          /// <summary>
          /// 2019年7月22日 yufh 高血压高危人群随访达标率
          /// </summary>
          /// <param name="prgid"></param>
          /// <param name="sDate"></param>
          /// <param name="eDate"></param>
          /// <returns></returns>
          public DataSet GetReportDataForDBL(string prgid, string sDate, string eDate, string SearchBy, string selectType)
          {
              string sql = string.Empty;
              DataSet dataSet = new DataSet();
              if (selectType == "1")
              {
                  using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
                  {
                      SqlCommand sqlCommand = new SqlCommand("USP_高血压高危人群达标率", sqlConn);
                      sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                      sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.VarChar));
                      sqlCommand.Parameters["@prgid"].Value = prgid;
                      sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.VarChar));
                      sqlCommand.Parameters["@sDate"].Value = sDate;
                      sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.VarChar));
                      sqlCommand.Parameters["@eDate"].Value = eDate;
                      sqlCommand.Parameters.Add(new SqlParameter("@SearchBy", System.Data.SqlDbType.VarChar));
                      sqlCommand.Parameters["@SearchBy"].Value = SearchBy;
                      SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                      sqlCommand.CommandTimeout = 0;
                      adapter.Fill(dataSet);
                  }
              }
              else if (selectType == "2")
              {
                  sql = @"select a.ID,  a.个人档案编号,a.家庭档案编号,a.姓名
	,a.性别 ,a.出生日期,a.身份证号,a.街道,a.居委会,a.居住地址
	,aa.联系人电话,aa.联系人姓名,aa.本人电话
	,(select 机构名称 from tb_机构信息 where 机构编号 = a.所属机构)所属机构
	,(select UserName from tb_MyUser where 用户编码 = a.创建人) 创建人,a.创建时间 
from vw_孕妇产后访视情况表 a 
left join tb_健康档案 aa on aa.个人档案编号 = a.个人档案编号 and aa.产次 = a.产次 
left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
where a.档案状态='1' 
	and isnull(a.失访情况,'')<>'1'
	and (len(a.所属机构)>=15 and substring(a.所属机构,1,7)+'1'+substring(a.所属机构,9,7) like @机构+'%'
		or a.所属机构 like  @机构+'%')";
                  if (SearchBy == "1")
                  {
                      sql += @"  and a.随访日期 >=@DateFrom and a.随访日期 <=@DateTo ";
                  }
                  else if (SearchBy == "2")
                  {
                      sql += @"  and a.分娩日期 >=@DateFrom and a.分娩日期 <=@DateTo ";
                  }
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, prgid);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, sDate);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, eDate);
                  dataSet = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  dataSet.Tables[0].TableName = "#统计";
              }
              else if (selectType == "3")
              {
                  sql = @"select a.ID,  a.个人档案编号,a.家庭档案编号,a.姓名
	,a.性别 ,a.出生日期,a.身份证号,a.街道,a.居委会,a.居住地址
	,aa.联系人电话,aa.联系人姓名,aa.本人电话
	,(select 机构名称 from tb_机构信息 where 机构编号 = a.所属机构)所属机构
	,(select UserName from tb_MyUser where 用户编码 = a.创建人) 创建人,a.创建时间 
from vw_孕妇产后访视情况表 a 
left join tb_健康档案 aa on aa.个人档案编号 = a.个人档案编号 and aa.产次 = a.产次 
left join tb_孕妇_产前随访1次 b on aa.个人档案编号=b.个人档案编号 and aa.孕次 = b.孕次
where a.档案状态='1' 
	and isnull(a.失访情况,'')<>'1' and (b.孕周>4 and b.孕周<13)
	and (len(a.所属机构)>=15 and substring(a.所属机构,1,7)+'1'+substring(a.所属机构,9,7) like @机构+'%'
		or a.所属机构 like  @机构+'%')";
                  if (SearchBy == "1")
                  {
                      sql += @"  and a.随访日期 >=@DateFrom and a.随访日期 <=@DateTo ";
                  }
                  else if (SearchBy == "2")
                  {
                      sql += @"  and a.分娩日期 >=@DateFrom and a.分娩日期 <=@DateTo ";
                  }
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, prgid);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, sDate);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, eDate);
                  dataSet = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  dataSet.Tables[0].TableName = "#统计";
              }
              return dataSet;
          }

     }
}

