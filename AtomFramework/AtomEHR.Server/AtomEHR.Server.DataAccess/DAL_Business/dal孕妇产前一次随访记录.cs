﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 孕妇产前一次随访记录的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/10/16 02:20:22
 *   最后修改: 2015/10/16 02:20:22
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal孕妇产前一次随访记录
    /// </summary>
    public class dal孕妇产前一次随访记录 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal孕妇产前一次随访记录(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_孕妇_产前随访1次.__KeyName; //主表的主键字段
            _SummaryTableName = tb_孕妇_产前随访1次.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_孕妇_产前随访1次.__TableName) ORM = typeof(tb_孕妇_产前随访1次);
            if (tableName == tb_孕妇基本信息.__TableName) ORM = typeof(tb_孕妇基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_孕妇产前一次随访记录.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_孕妇产前一次随访记录.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_孕妇_产前随访1次.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_孕妇_产前随访1次]    where 1=2 ";
            string sql2 = " select * from tb_孕妇基本信息   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo2 ";
            string sql4 = " select * from tb_健康档案   where [" + tb_健康档案.个人档案编号 + "]=@DocNo3 ";

            //string sql2 = " select * from [tb_老年人随访s]   where ["+tb_老年人随访.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_孕妇_产前随访1次.__TableName;
            ds.Tables[1].TableName = tb_孕妇基本信息.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            ds.Tables[3].TableName = tb_健康档案.__TableName;//子表
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_孕妇_产前随访1次] where [" + tb_孕妇_产前随访1次.__KeyName + "]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }
        public override bool CheckNoExists(string keyValue)
        {
            string sql = string.Format("SELECT COUNT(*) C FROM [{0}] WHERE [个人档案编号]=@KEY", _SummaryTableName, _SummaryKeyName);
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, keyValue);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        /// 查询本孕妇 是否已经做了此孕次的此次随访
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="yunCi">孕次</param>
        /// <returns></returns>
        public bool CheckExistsByYunCi(string docNo, string yunCi)
        {
            string sql = "  SELECT COUNT(*) C FROM [tb_孕妇_产前随访1次]   WHERE [个人档案编号]=@docNo AND 孕次 = @yunCi";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@docNo", SqlDbType.VarChar, docNo);
            cmd.AddParam("@yunCi", SqlDbType.VarChar, yunCi);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "孕妇产前一次随访记录");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportDataBYLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	                                 tab1.rgid 区域编码,tab1.机构名称 区域名称,
                                isnull(tab1.总数,0) 总数,isnull(tab2.合格数,0) 合格数,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数,convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
                                isnull(tab7.平均完整度,0) 平均完整度,isnull(tab9.转入档案数,0) 转入档案数,isnull(tab8.转出档案数,0) 转出档案数,
                                isnull(tab6.非活动档案数,0) 非活动档案数
                            from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访1次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访1次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访1次 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访1次 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 order by tab1.rgid
";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#健康体检信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            #endregion
            return ds;
        }

        public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	                                 tab1.rgid 区域编码,tab1.机构名称 区域名称,
                                isnull(tab1.总数,0) 总数,isnull(tab2.合格数,0) 合格数,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数,convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
                                isnull(tab7.平均完整度,0) 平均完整度,isnull(tab9.转入档案数,0) 转入档案数,isnull(tab8.转出档案数,0) 转出档案数,
                                isnull(tab6.非活动档案数,0) 非活动档案数
                            from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.填表日期 >= (@DateFrom) and jktj.填表日期 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.填表日期 >= (@DateFrom)  and jktj.填表日期 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访1次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.填表日期 >= (@DateFrom)  and jktj.填表日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访1次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.填表日期 >= (@DateFrom)  and jktj.填表日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访1次 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访1次 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 order by tab1.rgid
";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#健康体检信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.填表日期 >= (@DateFrom)
                                        and jktj.填表日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.填表日期 >= (@DateFrom)
                                        and jktj.填表日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_孕妇_产前随访1次 jktj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = jktj.个人档案编号
                                        and jktj.填表日期 >= (@DateFrom)
                                        and jktj.填表日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            #endregion
            return ds;
        }
        public DataSet GetAllDataByKey(string _docNo)
        {
            string sql1 = " select * from tb_孕妇基本信息   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from tb_孕妇_产前随访1次   where [" + tb_孕妇_产前随访1次.个人档案编号 + "]=@DocNo2 order by 填表日期 desc"; //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_孕妇基本信息.__TableName;
            ds.Tables[1].TableName = tb_孕妇_产前随访1次.__TableName;//子表
            return ds;
        }

        //add 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
        public DataSet GetAllDataByKeyNew(string _docNo)
        {
            string sql1 = " select * from tb_健康档案   where [" + tb_健康档案.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from tb_孕妇_产前随访1次   where [" + tb_孕妇_产前随访1次.个人档案编号 + "]=@DocNo2 order by 填表日期 desc"; //子表
            string sql3 = " select * from tb_孕妇基本信息 where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;
            ds.Tables[1].TableName = tb_孕妇_产前随访1次.__TableName;//子表
            ds.Tables[2].TableName = tb_孕妇基本信息.__TableName;//子表
            return ds;
        }
        //add 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △

        public DataSet GetOneDataByKey(string docNo, string id)
        {
            string sql1 = " select * from tb_孕妇基本信息   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from [tb_孕妇_产前随访1次]   where [" + tb_孕妇_产前随访1次.ID + "]=@DocNo2 "; //子表
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 ";
            string sql4 = " select * from tb_健康档案   where [" + tb_健康档案.__KeyName + "]=@DocNo4 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_孕妇基本信息.__TableName;
            ds.Tables[1].TableName = tb_孕妇_产前随访1次.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            ds.Tables[3].TableName = tb_健康档案.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取孕妇1次随访信息
        /// </summary>
        /// <param name="_docNo"></param>
        /// <returns></returns>
        public DataSet GetInfoSuiFang(string _docNo)
        {
            //string sql1 = " select * from tb_孕妇基本信息   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql1 = " select * from [tb_孕妇_产前随访1次]   where [" + tb_孕妇_产前随访1次.个人档案编号 + "]=@DocNo1 "; //子表
            string sql2 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2 ";
            string sql3 = " select * from tb_健康档案   where [" + tb_健康档案.个人档案编号 + "]=@DocNo3 ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
            //cmd.AddParam("@DocNo4", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            //ds.Tables[0].TableName = tb_孕妇基本信息.__TableName;
            ds.Tables[0].TableName = tb_孕妇_产前随访1次.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案.__TableName;//子表
            return ds;
        }

    }
}

