﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: MXB高血压随访表二的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-07-04 03:30:27
 *   最后修改: 2019-07-04 03:30:27
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dalMXB高血压随访表二
    /// </summary>
    public class dalMXB高血压随访表二 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalMXB高血压随访表二(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_MXB高血压随访表二.__KeyName; //主表的主键字段
             _SummaryTableName = tb_MXB高血压随访表二.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_MXB高血压随访表二.__TableName) ORM = typeof(tb_MXB高血压随访表二);
             if (tableName == tb_MXB高血压随访表二_个人膳食.__TableName) ORM = typeof(tb_MXB高血压随访表二_个人膳食);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_MXB高血压随访表二.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_MXB高血压随访表二.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_MXB高血压随访表二.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_MXB高血压随访表二]    where ["+tb_MXB高血压随访表二.个人档案编号+"]=@DocNo1 ";
              string sql2 = " select * from [tb_MXB高血压随访表二_个人膳食]   where [" + tb_MXB高血压随访表二_个人膳食.个人档案编号 + "]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB高血压随访表二.__TableName;
              ds.Tables[1].TableName = tb_MXB高血压随访表二_个人膳食.__TableName;//子表
              return ds;
          }

          /// <summary>
          /// 获取一个病人的一条表单数据
          /// </summary>
          /// <param name="ID"></param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByID(string ID)
          {
              string sql1 = " select * from [tb_MXB高血压随访表二]    where [" + tb_MXB高血压随访表二.ID + "]=@ID1 ";
              string sql2 = " select a.* from  [tb_MXB高血压随访表二_个人膳食]  a  where exists(select 1 from [tb_MXB高血压随访表二] b  where b.ID = @ID2 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间)";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@ID1", SqlDbType.VarChar, ID.Trim());
              cmd.AddParam("@ID2", SqlDbType.VarChar, ID.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB高血压随访表二.__TableName;
              ds.Tables[1].TableName = tb_MXB高血压随访表二_个人膳食.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              string sql1 = "delete a from  [tb_MXB高血压随访表二_个人膳食]  a  where exists(select 1 from tb_MXB高血压随访表二 b  where b.ID = @DocNo1 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间) ";//删除子表
              string sql2 = "delete [tb_MXB高血压随访表二] where ["+tb_MXB高血压随访表二.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "MXB高血压随访表二");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

     }
}

