﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 档案摘要的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-09 10:54:46
 *   最后修改: 2015-07-09 10:54:46
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal档案摘要
    /// </summary>
    public class dal档案摘要 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dal档案摘要(Loginer loginer)
             : base(loginer)
         {
             _SummaryKeyName = tb_档案摘要.__KeyName; //主表的主键字段
             _SummaryTableName = tb_档案摘要.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_档案摘要.__TableName) ORM = typeof(tb_档案摘要);
             if (tableName == tb_档案参数.__TableName) ORM = typeof(tb_档案参数);
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL
              //
              if (docNoFrom != string.Empty) sql.Append(" and " + tb_档案摘要.档案号 + ">='" + docNoFrom + "'");
              if (docNoTo != string.Empty) sql.Append(" and " + tb_档案摘要.档案号 + "<='" + docNoTo + "'");

              if (docDateFrom > DateTime.MinValue)
                  sql.Append(" and Convert(varchar," + tb_档案摘要.创建时间 + ",112) >='" + ConvertEx.ToCharYYYYMMDD(docDateFrom) + "'");

              if (docDateTo > DateTime.MinValue)
                  sql.Append(" and Convert(varchar," + tb_档案摘要.创建时间 + ",112) <='" + ConvertEx.ToCharYYYYMMDD(docDateTo) + "'");
             
              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_档案摘要.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  //return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_档案摘要]    where [" + tb_档案摘要.__KeyName + "]=@DocNo1 ";
              string sql2 = " select * from [tb_档案参数]    where [" + tb_档案参数.__KeyName + "]=@DocNo2  "; //明细表排序
              
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_档案摘要.__TableName;
              ds.Tables[1].TableName = tb_档案参数.__TableName;//明细表
              return ds;
          }

          /// <summary>
          ///删除一张单据!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据!!!
              string sql1 = "delete [tb_档案摘要] where [" + tb_档案摘要.__KeyName + "]=@DocNo1 ";
              string sql2 = "delete [tb_档案参数] where [" + tb_档案参数.__KeyName + "]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public override SaveResult Update(DataSet data)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "DA");
              return docNo;
          }


     }
}

