﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 妇女保健检查的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/10/06 11:24:33
 *   最后修改: 2015/10/06 11:24:33
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal妇女保健检查
    /// </summary>
    public class dal妇女保健检查 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal妇女保健检查(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_妇女保健检查.__KeyName; //主表的主键字段
            _SummaryTableName = tb_妇女保健检查.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_妇女保健检查.__TableName) ORM = typeof(tb_妇女保健检查);
            if (tableName == tb_妇女基本信息.__TableName) ORM = typeof(tb_妇女基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_既往病史.__TableName) ORM = typeof(tb_健康档案_既往病史);//如有明细表请调整本行的代码
            if (tableName == tb_MXB高血压管理卡.__TableName) ORM = typeof(tb_MXB高血压管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_MXB慢病基础信息表.__TableName) ORM = typeof(tb_MXB慢病基础信息表);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_妇女保健检查.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_妇女保健检查.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_妇女保健检查.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_妇女保健检查]    where [" + tb_妇女保健检查.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_妇女基本信息]    where [" + tb_妇女基本信息.个人档案编号 + "]=@DocNo2 ";
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 ";
            string sql4 = " select * from [tb_MXB高血压管理卡]    where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo4 ";
            string sql5 = " select * from [tb_MXB慢病基础信息表]    where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo5 ";
            string sql6 = " select * from [tb_健康档案_既往病史]    where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo6 ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_妇女保健检查.__TableName;
            ds.Tables[1].TableName = tb_妇女基本信息.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            ds.Tables[3].TableName = tb_MXB高血压管理卡.__TableName;//子表
            ds.Tables[4].TableName = tb_MXB慢病基础信息表.__TableName;//子表
            ds.Tables[5].TableName = tb_健康档案_既往病史.__TableName;//子表
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_妇女保健检查s] where [" + tb_妇女保健检查.__KeyName + "]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_妇女保健检查] where [" + tb_妇女保健检查.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "妇女保健检查");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }


        public DataSet GetAllDataByKey(string _docNo)
        {
            string sql1 = " select * from tb_健康档案   where [" + tb_健康档案.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_妇女保健检查]   where [" + tb_妇女保健检查.个人档案编号 + "]=@DocNo2 order by 检查日期 desc"; //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;
            ds.Tables[1].TableName = tb_妇女保健检查.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        ///  <param name="id">id主键</param>
        /// <returns></returns>
        public DataSet GetOneDataByKey(string docNo, string id)
        {
            string sql1 = " select * from tb_妇女基本信息   where [" + tb_妇女基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_妇女保健检查]   where [" + tb_妇女保健检查.__KeyName + "]=@DocNo2 "; //子表
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 ";
            string sql4 = " select * from [tb_MXB高血压管理卡]    where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo4 ";
            string sql5 = " select * from [tb_MXB慢病基础信息表]    where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo5 ";
            string sql6 = " select * from [tb_健康档案_既往病史]    where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo6 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_妇女基本信息.__TableName;
            ds.Tables[1].TableName = tb_妇女保健检查.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            ds.Tables[3].TableName = tb_MXB高血压管理卡.__TableName;//子表
            ds.Tables[4].TableName = tb_MXB慢病基础信息表.__TableName;//子表
            ds.Tables[5].TableName = tb_健康档案_既往病史.__TableName;//子表
            return ds;
        }

        public override SaveResult Update(DataSet data)
        {
            //对个人健康档案保存进行单独处理...重写保存方法              

            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            string m家庭编号 = string.Empty;
            string m个人编号 = string.Empty;


            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            string 高血压管理卡 = "";
            if (data.Tables.Contains(tb_MXB高血压管理卡.__TableName))
            {
                高血压管理卡 = DAL_System.DocNoTool.Get高血压管理卡(_CurrentTrans);
            }
            try
            {
                //更新所有资料表
                foreach (DataTable dt in data.Tables)
                {
                    //仅处理有作修改的资料表
                    if (dt.GetChanges() == null) continue;

                    //根据资料表名获取SQL命令生成器
                    IGenerateSqlCommand gen = this.CreateSqlGenerator(dt.TableName);
                    if (gen == null) continue; //当前数据层无法识别的SQL命令生成器，则不更新当前资料表

                    //本次更新是更新主表,取主表的主键用于设置明细表的外键
                    //if (gen.IsSummary() && dt.TableName == tb_家庭档案.__TableName)
                    //{
                    //    UpdateSummaryKey(_CurrentTrans, dt, _UpdateSummaryKeyMode, gen.GetPrimaryFieldName(),
                    //        ref m家庭编号, gen.GetDocNoFieldName(), ref m个人编号);

                    //    mResult.GUID = m家庭编号;
                    //    mResult.DocNo = m个人编号;
                    //}
                    //else
                    //{
                    //更新明细表的外键,对健康档案生成两个键值进行重写pan
                    if (dt.TableName == tb_健康档案.__TableName || dt.TableName == tb_健康档案_个人健康特征.__TableName || dt.TableName == tb_MXB慢病基础信息表.__TableName || dt.TableName == tb_儿童基本信息.__TableName || dt.TableName == tb_老年人基本信息.__TableName || dt.TableName == tb_妇女基本信息.__TableName)
                    {
                        if (_UpdateSummaryKeyMode == UpdateKeyMode.DocumentNoAndGuid)
                            UpdateDetailKey(dt, gen.GetForeignFieldName(), m家庭编号);
                    }
                    //赋值个人档案编号
                    foreach (DataRow row in dt.Rows)
                    {
                        //仅新增记录才需要更新外键，注意状态的使用
                        if (row.RowState == DataRowState.Added)
                        {
                            if (dt.TableName == tb_MXB高血压管理卡.__TableName && row.Table.Columns.Contains(tb_MXB高血压管理卡.__KeyName))
                            {
                                row[tb_MXB高血压管理卡.管理卡编号] = 高血压管理卡;
                            }
                        }
                    }
                    //使用基于ADO构架的SqlClient组件更新数据
                    SqlDataAdapter adp = new SqlDataAdapter();
                    adp.RowUpdating += new SqlRowUpdatingEventHandler(OnAdapterRowUpdating);
                    adp.UpdateCommand = GetUpdateCommand(gen, _CurrentTrans);
                    adp.InsertCommand = GetInsertCommand(gen, _CurrentTrans);
                    adp.DeleteCommand = GetDeleteCommand(gen, _CurrentTrans);
                    adp.Update(dt);
                }
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      
                mResult.SetException(ex); //保存结果设置异常消息
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
            return mResult; //返回保存结果
        }

        public override bool CheckNoExists(string keyValue)
        {
            string sql = string.Format("SELECT COUNT(*) C FROM [{0}] WHERE [个人档案编号]=@KEY", _SummaryTableName);
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, keyValue);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        /// 判断检查日期是否重复
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool CheckNoExistsByDate(string docNo, string date)
        {
            string sql = string.Format("SELECT COUNT(*) C FROM [{0}] WHERE [个人档案编号] = @DOCNO AND [检查日期]=@KEY", _SummaryTableName);
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DOCNO", SqlDbType.VarChar, docNo);
            cmd.AddParam("@KEY", SqlDbType.VarChar, date);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        /// 根据ID 删除单条记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteByID(string id)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_妇女保健检查] where [ID]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, id.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }
    }
}

