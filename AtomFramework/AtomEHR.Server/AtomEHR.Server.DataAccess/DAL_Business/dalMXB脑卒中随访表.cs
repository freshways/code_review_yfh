﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: MXB脑卒中随访表的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015-12-02 04:24:07
 *   最后修改: 2015-12-02 04:24:07
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dalMXB脑卒中随访表
    /// </summary>
    public class dalMXB脑卒中随访表 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalMXB脑卒中随访表(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_MXB脑卒中随访表.__KeyName; //主表的主键字段
             _SummaryTableName = tb_MXB脑卒中随访表.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_MXB脑卒中随访表.__TableName) ORM = typeof(tb_MXB脑卒中随访表);
             if (tableName == tb_MXB脑卒中随访表_用药情况.__TableName) ORM = typeof(tb_MXB脑卒中随访表_用药情况);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_MXB脑卒中随访表.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_MXB脑卒中随访表.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_MXB脑卒中随访表.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_MXB脑卒中随访表]    where [" + tb_MXB脑卒中随访表.个人档案编号 + "]=@DocNo1 order by 发生时间 desc ";
              string sql2 = " select * from [tb_MXB脑卒中随访表_用药情况]   where [" + tb_MXB脑卒中随访表_用药情况.个人档案编号 + "]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB脑卒中随访表.__TableName;
              ds.Tables[1].TableName = tb_MXB脑卒中随访表_用药情况.__TableName;//子表
              return ds;
          }

          /// <summary>
          /// 获取一个病人的一条表单数据
          /// </summary>
          /// <param name="ID"></param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByID(string ID)
          {
              string sql1 = " select * from [tb_MXB脑卒中随访表]    where [" + tb_MXB脑卒中随访表.ID + "]=@ID1 ";
              string sql2 = "select a.* from  [tb_MXB脑卒中随访表_用药情况]  a  where exists(select 1 from [tb_MXB脑卒中随访表] b  where b.ID = @ID2 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间)";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@ID1", SqlDbType.VarChar, ID.Trim());
              cmd.AddParam("@ID2", SqlDbType.VarChar, ID.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB脑卒中随访表.__TableName;
              ds.Tables[1].TableName = tb_MXB脑卒中随访表_用药情况.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              string sql1 = "delete a from  [tb_MXB脑卒中随访表_用药情况]  a  where exists(select 1 from tb_MXB脑卒中随访表 b  where b.ID = @DocNo1 and a.个人档案编号 = b.个人档案编号 and a.创建时间 = b.创建时间)  ";//删除子表
              string sql2 = "delete [tb_MXB脑卒中随访表] where [" + tb_MXB脑卒中随访表.ID + "]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          /// 更新个人健康特征-完整度
          /// </summary>
          /// <param name="s个人档案编号"></param>
          public void Update个人健康特征(string s个人档案编号)
          {
              string sql = @"update tb_健康档案_个人健康特征 set 脑卒中随访表=(select top 1 缺项+','+完整度 from tb_MXB脑卒中随访表 
                                    where 个人档案编号=tb_健康档案_个人健康特征.个人档案编号
                                    order by 发生时间 desc) where 个人档案编号 = @DocNo ";
              string sql2 = @"update tb_MXB慢病基础信息表 set 脑卒中随访时间=(select top 1 下次随访时间 from tb_MXB脑卒中随访表 
                                    where 个人档案编号=tb_MXB慢病基础信息表.个人档案编号
                                    order by 发生时间 desc) where 个人档案编号 = @DocNo ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql + sql2);
              cmd.AddParam("@DocNo", SqlDbType.VarChar, s个人档案编号.Trim());
              DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "MXB脑卒中随访表");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type, string DateType)
          {
              string sql = string.Empty;
              DataSet ds = null;
              #region 查询语句

              if (Type == "1")
              {
                  sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_MXB脑卒中随访表  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj." + DateType + @" >= (@DateFrom) and jktj." + DateType + @" <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_MXB脑卒中随访表  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj." + DateType + @" >= (@DateFrom)  and jktj." + DateType + @" <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_MXB脑卒中随访表  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj." + DateType + @" >= (@DateFrom)  and jktj." + DateType + @" <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_MXB脑卒中随访表  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj." + DateType + @" >= (@DateFrom)  and jktj." + DateType + @" <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_MXB脑卒中随访表 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_MXB脑卒中随访表 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#统计";
              }
              else if (Type == "2")
              {
                  sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_MXB脑卒中随访表  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj." + DateType + @" >= (@DateFrom)
                                        and jktj." + DateType + @" <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#统计";
              }
              else if (Type == "3")//合格
              {
                  sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_MXB脑卒中随访表  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj." + DateType + @" >= (@DateFrom)
                                        and jktj." + DateType + @" <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#统计";
              }
              else if (Type == "4")//不合格
              {
                  sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_MXB脑卒中随访表  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj." + DateType + @" >= (@DateFrom)
                                        and jktj." + DateType + @" <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#统计";
              }
              #endregion
              return ds;
          }

         /// <summary>
         /// 查询脑卒中随访表
         /// </summary>
         /// <param name="docNo"></param>
         /// <param name="year"></param>
         /// <returns></returns>
          public DataSet GetInfoByDate(string docNo, string year)
          {
              string sql1 = "select 发生时间 from [tb_MXB脑卒中随访表] where [" + tb_MXB脑卒中随访表.个人档案编号 + "]=@docNo order by 发生时间 desc";//substring(发生时间,1,4) as '年'  :截取年
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@docNo", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB脑卒中随访表.__TableName;
              return ds; 
          }

          public DataSet GetInfoByNZZ(string docNo, List<string> year)
          {
              string dates = string.Empty;
              if (year != null)
              {
                  for (int i = 0; i < year.Count; i++)
                  {
                      dates = dates + "'" + year[i].Trim() + "'" + ",";
                  }
              }
              if (dates.Length > 1)
              {
                  dates = dates.Substring(0, dates.Length - 1);
              }
              string sql1 = " select * from [tb_MXB脑卒中随访表]    where [" + tb_MXB脑卒中随访表.个人档案编号 + "]=@docNo and 发生时间 in (" + dates + ") order by 发生时间 asc";
              string sql2 = " select * from [tb_健康档案] where[" + tb_健康档案.个人档案编号 + "]=@DocNo2";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@docNo", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@year", SqlDbType.VarChar, "");
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB脑卒中随访表.__TableName;
              ds.Tables[1].TableName = tb_健康档案.__TableName;
              return ds;
          }
         /// <summary>
         /// 脑卒中用药情况
         /// </summary>
         /// <param name="docNo"></param>
         /// <param name="year"></param>
         /// <returns></returns>
          public DataSet GetInfoByYY(string docNo, string year)
          {
              string sql1 = "select * from [tb_MXB脑卒中随访表_用药情况]    where [" + tb_MXB脑卒中随访表_用药情况.个人档案编号 + "]=@DocNo1 and 创建时间 = @DocNo2";
              //string sql2 = " select * from  [tb_MXB脑卒中随访表_用药情况]    where [" + tb_MXB脑卒中随访表_用药情况.创建时间 + "]=@DocNo2";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, year.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_MXB脑卒中随访表_用药情况.__TableName;
              return ds;
          }
        //高血压患者
          public bool Check添加高血压随访(string s个人档案编号, string s随访日期)
          {
              string sql1 = " select 1 from [tb_MXB高血压管理卡]   where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo1 ";
              string sql2 = " select 1 from [tb_MXB高血压随访表]   where [" + tb_MXB高血压随访表.个人档案编号 + "]=@DocNo1 and convert(datetime,[" + tb_MXB高血压随访表.发生时间 + "])=@DocNo2 ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, s个人档案编号.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, s随访日期.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)//判断是否是高血压
              {
                  if (ds.Tables[1] == null || ds.Tables[1].Rows.Count == 0)
                      return true; //如果指定的随访日期不存在则返回true 添加新的随访
              }
              return false;
          }

          //糖尿病患者
          public bool Check添加糖尿病随访(string s个人档案编号, string s随访日期)
          {
              string sql1 = " select 1 from [tb_MXB糖尿病管理卡]   where [" + tb_MXB糖尿病管理卡.个人档案编号 + "]=@DocNo1 ";
              string sql2 = " select 1 from [tb_MXB糖尿病随访表]   where [" + tb_MXB糖尿病随访表.个人档案编号 + "]=@DocNo1 and convert(datetime,[" + tb_MXB糖尿病随访表.发生时间 + "])=@DocNo2 ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, s个人档案编号.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, s随访日期.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)//判断是否是糖尿病
              {
                  if (ds.Tables[1] == null || ds.Tables[1].Rows.Count == 0)
                      return true; //如果指定的随访日期不存在则返回true 添加新的随访
              }
              return false;
          }

          //冠心病患者
          public bool Check添加冠心病随访(string s个人档案编号, string s随访日期)
          {
              string sql1 = " select 1 from [tb_MXB冠心病管理卡]   where [" + tb_MXB冠心病管理卡.个人档案编号 + "]=@DocNo1 ";
              string sql2 = " select 1 from [tb_MXB冠心病随访表]   where [" + tb_MXB冠心病随访表.个人档案编号 + "]=@DocNo1 and convert(datetime,[" + tb_MXB冠心病随访表.发生时间 + "])=@DocNo2 ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, s个人档案编号.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, s随访日期.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)//判断是否是冠心病
              {
                  if (ds.Tables[1] == null || ds.Tables[1].Rows.Count == 0)
                      return true; //如果指定的随访日期不存在则返回true 添加新的随访
              }
              return false;
          }

          //Begin WXF 2018-11-02 | 11:06
          //根据档案号返回所有发生时间 
          public DataTable Get_发生时间(string grdah)
          {
              string sql = " select 发生时间 from [tb_MXB脑卒中随访表]   where [" + tb_MXB脑卒中随访表.个人档案编号 + "]=@DocNo1 order by 发生时间 desc";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
              DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_MXB脑卒中随访表.__TableName);
              return dt;
          }
        //End
    }
}

