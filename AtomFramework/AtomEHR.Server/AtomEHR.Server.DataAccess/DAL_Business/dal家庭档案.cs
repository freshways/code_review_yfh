﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 家庭档案的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/08/20 11:07:24
 *   最后修改: 2015/08/20 11:07:24
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal家庭档案
    /// </summary>
    public class dal家庭档案 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal家庭档案(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_家庭档案.__KeyName; //主表的主键字段
            _SummaryTableName = tb_家庭档案.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_家庭档案.__TableName) ORM = typeof(tb_家庭档案);
            if (tableName == tb_健康档案.__TableName) ORM = typeof(tb_健康档案);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_家庭档案.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_家庭档案.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_家庭档案.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }
        /// <summary>
        /// 根据家庭档案编号  获取家庭成员信息
        /// 家庭健康信息页面  专用
        /// </summary>
        /// <param name="FamilyNO">家庭档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByFamilyNO(string FamilyNO)
        {
            string sql1 = " select * from [tb_家庭档案]   where [" + tb_家庭档案.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.家庭档案编号 + "]=@DocNo2";      //子表
            string sql3 = " select * from tb_健康档案_个人健康特征 where 个人档案编号 in (select 个人档案编号 from tb_健康档案 where 家庭档案编号 = @DocNo3)";      //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, FamilyNO.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, FamilyNO.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, FamilyNO.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_家庭档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            return ds;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_家庭档案]    where [" + tb_家庭档案.__KeyName + "]=@DocNo1 ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_家庭档案.__TableName;
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_家庭档案] where [" + tb_家庭档案.__KeyName + "]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        /// 用户注销，更新与户主关系和家庭档案编号字段为''即可
        /// </summary>
        /// <param name="个人档案编号">个人档案编号</param>
        /// <param name="与户主关系">与户主关系</param>
        /// <param name="与户主关系">家庭成员个数</param>
        /// <returns></returns>
        public bool fun注销(string 个人档案编号, string 家庭档案编号, string 与户主关系, int 家庭成员数量)
        {
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            try
            {
                string sql = @"update tb_健康档案  set 与户主关系 ='',家庭档案编号='' where 个人档案编号='" + 个人档案编号 + "' ";
                if (与户主关系 == "1")//户主
                {
                    sql += "  update tb_健康档案_个人健康特征  set 与户主关系 ='" + 与户主关系 + "',家庭档案编号='' where 个人档案编号='" + 个人档案编号 + "'  ";
                } if (家庭成员数量 == 1)//家庭成员中只有一个人
                {
                    sql += " delete from tb_家庭档案   where 家庭档案编号='" + 家庭档案编号 + "'";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);

                int i = DataProvider.Instance.ExecuteNoQuery(this._CurrentTrans, sql);
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       
                return i != 0;

            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction(); //提交事务       

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
        }
        /// <summary>
        /// 家庭成员转出功能
        /// </summary>
        /// <param name="yhzgx">与户主关系</param>
        /// <param name="jtdabh">转入的家庭档案编号</param>
        /// <param name="yjtdabh">原家庭档案编号</param>
        /// <param name="grdabh">个人档案编号</param>
        /// <returns></returns>
        public bool fun转出(string yhzgx, string jtdabh, string yjtdabh, string grdabh)
        {
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            try
            {
                string sql = string.Empty;
                int i = this.countHomeCy(yjtdabh);//获取原家庭中的成员个数
                if (i == 1)//只有一个人的话 要删除 原家庭档案
                {
                    sql += "delete tb_家庭档案 where 家庭档案编号 = '" + yjtdabh + "' ";
                }
                //更新
                //tb_健康档案_个人健康特征
                sql += "   update tb_健康档案_个人健康特征 set 与户主关系 ='" + yhzgx + "',家庭档案='' where 个人档案编号='" + grdabh + "'";//
                //update db2admin.t_da_jkda_rkxzl set d_jtdabh=#{jtdabh},d_yhzgx=#{yhzgx} where d_grdabh=#{grdabh}
                sql += "   update tb_健康档案 set 与户主关系 ='" + yhzgx + "',家庭档案编号='" + jtdabh + "' where 个人档案编号='" + grdabh + "'";//
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);

                int x = DataProvider.Instance.ExecuteNoQuery(this._CurrentTrans, sql);
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       
                return x != 0;
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction(); //提交事务       

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
        }
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }
        protected override void UpdateSummaryKey(SqlTransaction tran, DataTable summary, UpdateKeyMode model, string keyFieldName, ref string GUID, string docNoFieldName, ref string docNo)
        {
            DataRow row = summary.Rows[0]; //取主表第一条记录

            //如果未指定单号更新类型则取旧的单号.
            if ((row.RowState != DataRowState.Added) || (model == UpdateKeyMode.None)) //取旧的单号
            {
                docNo = row[docNoFieldName].ToString();
                return;
            }

            //新增记录,更新主键的值
            if (row.RowState == DataRowState.Added)
            {
                //注意状态的使用,只有在新增状态下才更新单号
                if (model == UpdateKeyMode.OnlyGuid)
                {
                    if (keyFieldName == "") throw new Exception("没有设定主键,检查类模型参数定义！");
                    GUID = Guid.NewGuid().ToString().Replace("-", "");
                    row[keyFieldName] = GUID;
                }

                if (model == UpdateKeyMode.OnlyDocumentNo)
                {
                    if (docNoFieldName == "") throw new Exception("没有设定单号主键,检查类模型参数定义！");
                    docNo = GetNumber(tran); //调用模板方法获取单据号码

                    if (keyFieldName == "") throw new Exception("没有设定主键,检查类模型参数定义！");
                    if (docNoFieldName == "") throw new Exception("没有设定单号主键,检查类模型参数定义！");

                    DataTable dt = Get家庭档案编号(tran, row[tb_健康档案.居委会].ToString(), row[tb_健康档案.家庭档案编号].ToString()); //调用模板方法获取单据号码;
                    GUID = dt.Rows[0][0].ToString();
                    docNo = dt.Rows[0][1].ToString();
                    row[keyFieldName] = docNo;
                    row[docNoFieldName] = GUID;

                    row[docNoFieldName] = docNo;
                }
            }
        }
        /// <summary>
        ///获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "家庭档案");
            return docNo;
        }
        /// <summary>
        ///获取单据流水号码
        /// </summary>
        private DataTable Get家庭档案编号(SqlTransaction tran, String s街道编号, String s家庭档案号)
        {
            //if(_UpdateType.)
            string query = "sp_Get家庭编号 @s所属机构='" + _Loginer.所属机构 + "' ,@s街道编号='" + s街道编号 + "' ,@s家庭档案编号='" + s家庭档案号 + "' ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable docNo = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "家庭档案编号");
            return docNo;
        }
        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        /// <summary>
        /// 家庭转出 页面查询方法
        /// </summary>
        /// <param name="flag">查询条件cbo</param>
        /// <param name="txt">查询条件txt</param>
        /// <param name="rgid">所属机构</param>
        /// <param name="_jtdabh">家庭档案编号</param>
        /// <returns></returns>
        public DataTable selectRkxzlByHzxx(string flag, string txt, string rgid, string _jtdabh)
        {
            string query = @" select  a.个人档案编号
                                        ,'2' as 与户主关系
		                                ,a.家庭档案编号
		                                ,a.姓名
		                                ,(select b.P_DESC from tb_常用字典 b where  b.P_CODE = a.性别 and b.P_FUN_CODE = 'xb_xingbie') 性别
		                                ,(select 地区名称 from [dbo].[tb_地区档案]  where 地区编码 = a.居委会)居委会
		                                ,a.居住地址
		                                ,a.出生日期
		                                ,a.身份证号
                                 from tb_健康档案 a where 1=1 
                                ";
            if (flag == "1")//姓名
            {
                query += " AND (A.姓名 = '" + Common.DESEncrypt.DES加密(txt) + "' or  A.姓名 = '" + txt + "' )";
            }
            if (flag == "2")//档案号
            {
                query += " AND A.个人档案编号 = '" + txt + "'";
            }
            if (flag == "3")//身份证号
            {
                query += " AND A.身份证号 = '" + txt + "'";
            }
            if (!string.IsNullOrEmpty(rgid))
            {
                query += " AND A.所属机构 = '" + rgid + "'";
            }
            if (!string.IsNullOrEmpty(_jtdabh))
            {
                query += " AND A.家庭档案编号 <> '" + _jtdabh + "'  and 与户主关系='1'";
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "家庭档案编号");
            return dt;
        }
        /// <summary>
        /// 方法重写
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="_jtdabh">家庭档案编号</param>
        /// <returns></returns>
        public DataTable selectRkxzlByHzxx(string where, string _jtdabh)
        {
            string query = @" select  a.个人档案编号
                                        ,'2' as 与户主关系
		                                ,a.家庭档案编号
		                                ,a.姓名
		                                ,(select b.P_DESC from tb_常用字典 b where  b.P_CODE = a.性别 and b.P_FUN_CODE = 'xb_xingbie') 性别
		                                ,(select 地区名称 from [dbo].[tb_地区档案]  where 地区编码 = a.居委会)居委会
		                                ,a.居住地址
		                                ,a.出生日期
		                                ,a.身份证号
                                 from tb_健康档案 a where 1=1 
                                ";

            if (!string.IsNullOrEmpty(where))
            {
                query += where;
            }
            if (!string.IsNullOrEmpty(_jtdabh))
            {
                query += " AND A.家庭档案编号 <> '" + _jtdabh + "'  and 与户主关系='1'";
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "家庭档案编号");
            return dt;
        }
        /// <summary>
        /// 查询一个家庭中的成员个数
        /// </summary>
        /// <param name="jtdabh">家庭档案编号</param>
        /// <returns></returns>
        public int countHomeCy(string jtdabh)
        {
            string query = "select count(1) from tb_健康档案 where 家庭档案编号 ='" + jtdabh + "'";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return ConvertEx.ToInt(o);
        }
        /// <summary>
        /// 转入家庭成员  页面查询按钮
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public DataTable selectFreeJtcy(string strWhere)
        {
            string query = @" select  a.个人档案编号
		                                ,a.家庭档案编号
		                                ,a.姓名
		                                ,(select b.P_DESC from tb_常用字典 b where  b.P_CODE = a.性别 and b.P_FUN_CODE = 'xb_xingbie') 性别
		                                ,(select 地区名称 from [dbo].[tb_地区档案]  where 地区编码 = a.居委会)居委会
                                        ,(select 地区名称 from [dbo].[tb_地区档案]  where 地区编码 = a.街道)街道
                ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE 机构级别 <>'1'  and 机构编号 =[所属机构])[所属机构] 
                                        ,a.本人电话
		                                ,a.居住地址
		                                ,a.出生日期
		                                ,a.身份证号
                                        ,a.[创建人]
			                            ,a.[创建时间]
                                 from tb_健康档案 a where 1=1 
                              and (与户主关系 = '' or a.家庭档案编号='') " + strWhere + " order by 创建时间 desc ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "家庭档案编号");
            return dt;
        }

        public bool addJtcy(string grdabh, string _jtdabh, string yhzgx)
        {
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            try
            {
                string serverDate = new DAL_Base.dalBaseBusiness(Loginer.CurrentUser).GetServiceDateTime();
                string sql = string.Empty;
                sql += " update tb_健康档案 set 家庭档案编号='" + _jtdabh + "',与户主关系='" + yhzgx + "' where 个人档案编号='" + grdabh + "'"; SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                sql += " update tb_家庭档案 set 修改人='" + Loginer.CurrentUser.用户编码 + "',修改时间='" + serverDate + "' where 家庭档案编号='" + _jtdabh + "'";
                int x = DataProvider.Instance.ExecuteNoQuery(this._CurrentTrans, sql);
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       
                return x != 0;
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction(); //提交事务       

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
        }


        /// <summary>
        /// 从数据库中获取家庭档案信息
        /// </summary>
        /// <param name="_docNo"></param>
        /// <returns></returns>
        public DataSet GetInfoByFamily(string _docNo)
        {

            string sql1 = " select * from [tb_家庭档案]    where [" + tb_家庭档案.家庭档案编号 + "]=@DocNo1";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.家庭档案编号 + "]=@DocNo2 order by 与户主关系 asc"; //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_家庭档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;
            return ds;
        }
    }
}

