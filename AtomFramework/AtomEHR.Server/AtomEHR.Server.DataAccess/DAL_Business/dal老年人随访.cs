﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 老年人随访的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/22 08:56:18
 *   最后修改: 2015/09/22 08:56:18
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal老年人随访
    /// </summary>
    public class dal老年人随访 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal老年人随访(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_老年人随访.__KeyName; //主表的主键字段
            _SummaryTableName = tb_老年人随访.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_老年人随访.__TableName) ORM = typeof(tb_老年人随访);
            if (tableName == tb_老年人基本信息.__TableName) ORM = typeof(tb_老年人基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_老年人随访.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_老年人随访.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_老年人随访.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人随访]    where 1=2 ";
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo2 ";

            //string sql2 = " select * from [tb_老年人随访s]   where ["+tb_老年人随访.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人随访.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        ///  <param name="id">id主键</param>
        /// <returns></returns>
        public System.Data.DataSet GetOneDataByKey(string docNo, string id)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人随访]   where [" + tb_老年人随访.__KeyName + "]=@DocNo2 "; //子表
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人随访.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 获取全部业务单据的数据
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        ///  <param name="id">id主键</param>
        /// <returns></returns>
        public System.Data.DataSet GetAllDataByKey(string docNo)
        {
            string sql1 = " select * from tb_老年人基本信息   where [" + tb_老年人基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_老年人随访]   where [" + tb_老年人随访.个人档案编号 + "]=@DocNo2  order by 随访日期 desc"; //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人基本信息.__TableName;
            ds.Tables[1].TableName = tb_老年人随访.__TableName;//子表
            return ds;
        }
        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_老年人随访s] where [" + tb_老年人随访.__KeyName + "]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_老年人随访] where [" + tb_老年人随访.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }
        /// <summary>
        /// 判断是否存在 此随访日期 的随访
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="happenTime">随访日期</param>
        /// <returns></returns>
        public bool isExists(string docNo, string happenTime)
        {
            string sql1 = "select count(1) from tb_老年人随访  where [" + tb_老年人随访.__KeyName + "]=@DocNo1 and " + tb_老年人随访.随访日期 + "= @DocNo2";//删除子表
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, happenTime.Trim());
            object i = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return ConvertEx.ToInt(i) != 0;
        }
        public override bool CheckNoExists(string docNo)
        {
            string sql = "SELECT COUNT(*) C FROM [tb_老年人随访] WHERE [个人档案编号]=@KEY";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, docNo);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        /// 获取本年度随访次数
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="happenTime">随访时间（年）</param>
        /// <returns></returns>
        public int getbndsfcs(string docNo, string happenTime)
        {
            string sql1 = "select max(随访次数) from tb_老年人随访  where [个人档案编号]=@DocNo1 and " + tb_老年人随访.随访日期 + " like '%" + happenTime + "%'";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            object i = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return ConvertEx.ToInt(i);
        }
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }
        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "老年人随访");
            return docNo;
        }
        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportDataByLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            #endregion
            return ds;
        }
        public DataSet GetReportDataByDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.随访日期 >= (@DateFrom) and jktj.随访日期 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.随访日期 >= (@DateFrom)  and jktj.随访日期 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.随访日期 >= (@DateFrom)  and jktj.随访日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.随访日期 >= (@DateFrom)  and jktj.随访日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.随访日期 >= (@DateFrom)
                                        and jktj.随访日期 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.随访日期 >= (@DateFrom)
                                        and jktj.随访日期 <= (@DateTo)				
                                       and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.随访日期 >= (@DateFrom)
                                        and jktj.随访日期 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            #endregion
            return ds;
        }
        /// <summary>
        /// 删除随访记录
        /// </summary>
        /// <param name="_id"></param>
        /// <param name="_docNo"></param>
        /// <param name="happenYear"></param>
        /// <returns></returns>
        public SaveResult fun删除随访(string _id, string _docNo, string happenYear)
        {
            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            string mGUID = string.Empty;
            string mDocNo = string.Empty;

            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();
            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");

            try
            {
                string sql1 = "delete tb_老年人随访 where id = @id  ";
                string sql2 = @" update tb_健康档案_个人健康特征 set 老年人随访=(select top 1 缺项+','+完整度 from tb_老年人随访   where 个人档案编号=tb_健康档案_个人健康特征.个人档案编号
                                    order by 随访日期 desc) where 个人档案编号 = @DocNo";
                string sql3 = @" update tb_老年人基本信息 set 下次随访时间=(select top 1 下次随访日期 from tb_老年人随访
                                    where 个人档案编号=tb_老年人随访.个人档案编号 order by 随访日期 desc) where 个人档案编号 = @DocNo";

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
                cmd.AddParam("@id", SqlDbType.VarChar, _id.Trim());
                cmd.AddParam("@DocNo", SqlDbType.VarChar, _docNo.Trim());
                DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      
                mResult.SetException(ex); //保存结果设置异常消息
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
            return mResult;
            //return true;
        }
        /// <summary>
        /// 获取老年人随访的基本信息
        /// </summary>
        /// <param name="_docNo"></param>
        /// <returns></returns>
        public DataSet GetInfoByFangshi(string _docNo, string _date)
        {

            string sql1 = " select * from [tb_老年人随访]    where [" + tb_老年人随访.个人档案编号 + "]=@DocNo1 and 随访日期=@DocNo4 ";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo2 "; //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, _date.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_老年人随访.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;
            return ds;
        }


        /// <summary>
        /// 老年人管理率
        /// </summary>
        /// <param name="DocNo机构"></param>
        /// <param name="DocNo完整度"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public DataSet GetReportDataOfGLL(string DocNo机构, string DocNo完整度, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
    sum(a.城镇人口数)+sum(a.农村人口数) as 总人口数, 
    sum(a.建档总数) as 建档总数,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
    --case when sum(a.总数)=0 then 0 else sum(a.总数)*3.8*0.1221*100.0/sum(a.总数) end 占总人口比率,
    case when sum(a.建档总数)=0 then 0 else (sum(a.城镇人口数)+sum(a.农村人口数))*0.38*0.1221*100.0/sum(a.建档总数) end 健康管理率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select
    sum(a.城镇人口数) as 城镇人口数,
	sum(a.农村人口数) as 农村人口数, 
    sum(a.建档总数) as 建档总数,  
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
        convert(int,isnull(tab1.城镇人口数,0)) as 城镇人口数,
        convert(int,isnull(tab1.农村人口数,0)) as 农村人口数,
        isnull(tab1.建档总数,0) 建档总数,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数,people.农村人口数,people.城镇人口数,a.建档总数 
	                            from tb_机构信息 region
                                left join(
                                    select 机构编号, case ISNULL(农村人口数,'') when '' then '0' else 农村人口数 end 农村人口数,
                                    case ISNULL(城镇人口数,'') when '' then '0' else 城镇人口数 end 城镇人口数
                                    from tb_地区人口 
                                ) as people on people.机构编号 = region.机构编号          
	                            left join (
		                            select jktj.所属机构, 
                                    count(jktj.所属机构) as 总数, 
                                    count(distinct jktj.个人档案编号) as 建档总数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            --and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                    --and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            --and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_老年人随访  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            --and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_老年人随访 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "2")
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc  ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.创建机构, jktj.所属机构,jktj.创建人,jktj.创建时间,jktj.ID  from  tb_健康档案  jkjc   ,tb_老年人随访  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                         and (
		                                (len(@机构)>6 and len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jktj.所属机构)>=12 and jktj.所属机构 like @机构 +'%')
		                                or(jktj.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#老年人随访";
            }
            #endregion
            return ds;
        }

        //Begin WXF 2018-11-01 | 19:26
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            string sql = " select 随访日期 from [tb_老年人随访]   where [" + tb_老年人随访.个人档案编号 + "]=@DocNo1 order by 随访日期 desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_老年人随访.__TableName);
            return dt;
        }
        //End


        //Begin WXF 2018-11-26 | 16:24
        //获取指定年份的评估数据(一年一条) 
        public DataTable Get_yearData(string recordNum, string year)
        {
            string sql = "select top 1 * from tb_老年人随访 where 个人档案编号='" + recordNum + "' and 随访日期 like '" + year + "%' order by 随访日期 desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_老年人随访.__TableName);
        }
        //End

    }
}

