﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 健康档案的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-30 04:21:13
 *   最后修改: 2015-07-30 04:21:13
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal健康档案
    /// </summary>
    public class dal健康档案 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal健康档案(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_健康档案.__KeyName; //主表的主键字段
            _SummaryTableName = tb_健康档案.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.DocumentNoAndGuid;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_健康档案.__TableName) ORM = typeof(tb_健康档案);
            if (tableName == tb_家庭档案.__TableName) ORM = typeof(tb_家庭档案);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_健康状态.__TableName) ORM = typeof(tb_健康档案_健康状态);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_既往病史.__TableName) ORM = typeof(tb_健康档案_既往病史);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_家族病史.__TableName) ORM = typeof(tb_健康档案_家族病史);//如有明细表请调整本行的代码
            if (tableName == tb_MXB慢病基础信息表.__TableName) ORM = typeof(tb_MXB慢病基础信息表);//如有明细表请调整本行的代码
            if (tableName == tb_MXB高血压管理卡.__TableName) ORM = typeof(tb_MXB高血压管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_MXB糖尿病管理卡.__TableName) ORM = typeof(tb_MXB糖尿病管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_儿童基本信息.__TableName) ORM = typeof(tb_儿童基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_老年人基本信息.__TableName) ORM = typeof(tb_老年人基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_妇女基本信息.__TableName) ORM = typeof(tb_妇女基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_孕妇基本信息.__TableName) ORM = typeof(tb_孕妇基本信息);//如有明细表请调整本行的代码
            if (tableName == tb_MXB冠心病管理卡.__TableName) ORM = typeof(tb_MXB冠心病管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_MXB脑卒中管理卡.__TableName) ORM = typeof(tb_MXB脑卒中管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_精神疾病信息补充表.__TableName) ORM = typeof(tb_精神疾病信息补充表);//如有明细表请调整本行的代码
            if (tableName == tb_残疾人_基本信息.__TableName) ORM = typeof(tb_残疾人_基本信息);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_健康档案.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_健康档案.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康档案.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 查询功能，传入条件自己编辑查询语句
        /// </summary>
        public DataTable GetSummaryByParam(string strWhere)
        {
            string query = @"SELECT  [个人档案编号]   
                                    ,[家庭档案编号]
                                 ,[姓名]
			                     ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xb_xingbie' AND B.P_CODE = [性别])[性别]  
			                     ,[出生日期]  
			                     ,[身份证号] 
			                     ,[居住地址]
			                     ,[本人电话]
			                     ,[联系人电话] 
			                     ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE 机构级别 <>'1'  and 机构编号 =[所属机构])[所属机构] 
			                     ,[创建人]
			                     ,[创建时间]
                                 ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='yhzgx' AND B.P_CODE =[与户主关系])[与户主关系]
			                     ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rkx_dazt' AND B.P_CODE = [档案状态])[档案状态]   from "
                + _SummaryTableName + " where 1=1 " + strWhere;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康档案.__TableName);
            return dt;
        }

        /// <summary>
        /// 查询功能，传入条件自己编辑查询语句
        /// </summary>
        public DataTable Get家庭档案(string strWhere)
        {
            string query = @"select a.家庭档案编号
		        ,(select p_desc from tb_常用字典 where P_FUN_CODE = 'zflx' and P_CODE = a.住房类型) 住房类型
		        ,a.住房面积
		        ,(select p_desc from tb_常用字典 where P_FUN_CODE = 'cslx' and P_CODE = a.厕所类型) 厕所类型
		        ,a.人均收入
		        ,(select p_desc from tb_常用字典 where P_FUN_CODE = 'yw_youwu' and P_CODE = a.是否低保户) 是否低保户
		        ,(select 地区名称 from [dbo].[tb_地区档案]  where 地区编码 = b.区)
			        +(select 地区名称 from [dbo].[tb_地区档案] where 地区编码 = b.街道) +b.居住地址
		            as 居住地址
		            ,(select 机构名称 from [dbo].[tb_机构信息]  where 机构编号 = a.所属机构) 所属机构
		            ,a.创建人
                    ,b.姓名
                    ,b.个人档案编号
		            ,a.创建时间 from tb_家庭档案  a left join tb_健康档案 b on a.家庭档案编号 = b.家庭档案编号  where  b.与户主关系='1' "
                + strWhere;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康档案.__TableName);
            return dt;
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// 获取原始的个人档案信息
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetRowInfoByKey(string grdah)
        {
            string sql1 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo1";      //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;

            return ds;
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// 获取此家庭的所有人员健康信息
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_家庭档案]   where [" + tb_家庭档案.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo2";      //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3";      //子表
            string sql4 = " select * from [tb_健康档案_健康状态]   where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo4";      //子表
            string sql5 = " select * from [tb_健康档案_既往病史]   where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo5";      //子表
            string sql6 = " select * from [tb_健康档案_家族病史]   where [" + tb_健康档案_家族病史.个人档案编号 + "]=@DocNo6";      //子表
            string sql7 = " select * from [tb_儿童基本信息]   where [" + tb_儿童基本信息.个人档案编号 + "]=@DocNo7";      //子表
            string sql8 = " select * from [tb_MXB高血压管理卡]   where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo8";      //子表
            string sql9 = " select * from [tb_MXB糖尿病管理卡]   where [" + tb_MXB糖尿病管理卡.个人档案编号 + "]=@DocNo9";      //子表
            string sql10 = " select * from [tb_MXB慢病基础信息表]   where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo10";      //子表
            string sql11 = " select * from [tb_老年人基本信息]   where [" + tb_老年人基本信息.个人档案编号 + "]=@DocNo11";      //子表
            string sql12 = " select * from [tb_妇女基本信息]   where [" + tb_妇女基本信息.个人档案编号 + "]=@DocNo12";      //子表
            string sql13 = " select * from [tb_孕妇基本信息]   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo13";      //子表
            string sql14 = " select * from [tb_MXB脑卒中管理卡]   where [" + tb_MXB脑卒中管理卡.个人档案编号 + "]=@DocNo14";      //子表
            string sql15 = " select * from [tb_MXB冠心病管理卡]   where [" + tb_MXB冠心病管理卡.个人档案编号 + "]=@DocNo15";      //子表
            string sql16 = " select * from [tb_精神疾病信息补充表]   where [" + tb_精神疾病信息补充表.个人档案编号 + "]=@DocNo16";      //子表
            string sql17 = " select * from [tb_残疾人_基本信息]   where [" + tb_残疾人_基本信息.个人档案编号 + "]=@DocNo17";      //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9 + sql10 + sql11 + sql12 + sql13 + sql14 + sql15 + sql16 + sql17);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo7", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo8", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo9", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo10", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo11", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo12", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo13", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo14", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo15", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo16", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo17", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_家庭档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            ds.Tables[3].TableName = tb_健康档案_健康状态.__TableName;   //子表
            ds.Tables[4].TableName = tb_健康档案_既往病史.__TableName;   //子表
            ds.Tables[5].TableName = tb_健康档案_家族病史.__TableName;   //子表
            ds.Tables[6].TableName = tb_儿童基本信息.__TableName;   //子表
            ds.Tables[7].TableName = tb_MXB高血压管理卡.__TableName;   //子表
            ds.Tables[8].TableName = tb_MXB糖尿病管理卡.__TableName;   //子表
            ds.Tables[9].TableName = tb_MXB慢病基础信息表.__TableName;   //子表
            ds.Tables[10].TableName = tb_老年人基本信息.__TableName;   //子表
            ds.Tables[11].TableName = tb_妇女基本信息.__TableName;   //子表
            ds.Tables[12].TableName = tb_孕妇基本信息.__TableName;   //子表
            ds.Tables[13].TableName = tb_MXB脑卒中管理卡.__TableName;   //子表
            ds.Tables[14].TableName = tb_MXB冠心病管理卡.__TableName;   //子表
            ds.Tables[15].TableName = tb_精神疾病信息补充表.__TableName;   //子表
            ds.Tables[16].TableName = tb_残疾人_基本信息.__TableName;   //子表
            return ds;
        }
        /// <summary>
        /// 根据家庭档案编号  获取家庭成员信息
        /// 家庭健康信息页面  专用
        /// </summary>
        /// <param name="FamilyNO">家庭档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByFamilyNO(string FamilyNO)
        {
            string sql1 = " select * from [tb_家庭档案]   where [" + tb_家庭档案.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.家庭档案编号 + "]=@DocNo2";      //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征] where [" + tb_健康档案_个人健康特征.家庭档案编号 + "]=@DocNo3";//子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, FamilyNO.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, FamilyNO.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, FamilyNO.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_家庭档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            return ds;
        }
        /// <summary>
        /// 获取此家庭的所有人员健康信息
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetAllUserByKey(string familyNo, string docNo)
        {
            string sql1;
            if (!string.IsNullOrEmpty(familyNo))  //存在家庭档案编号，则根据家庭档案编号进行查询  ，查询出所有家庭档案编号下的成员
            {
                sql1 = @" SELECT  [ID]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[拼音简码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='yhzgx' AND B.P_CODE =[与户主关系])[与户主关系]
                                                ,[与户主关系] AS [与户主关系编码]
                                              ,[工作单位]
                                              ,[本人电话]
                                              ,[邮箱]
                                              ,[省]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [市])[市]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [区])[区]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [街道])[街道]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [居委会])[居委会]
                                                ,[市] AS 市编码,[区] as [区编码],[街道] as [街道编码],[居委会] as [居委会编码]
                                              ,[居住地址]
                                              ,[所属片区]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='jzzk' AND B.P_CODE =[常住类型])[常住类型],[常住类型]  as [常住类型编码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xb_xingbie' AND B.P_CODE =[性别])[性别],[性别] as [性别编码]
                                              ,[出生日期]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='mz_minzu' AND B.P_CODE =[民族])[民族],[民族] as [民族编码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='whcd' AND B.P_CODE =[文化程度])[文化程度],[文化程度] as [文化程度编码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zy_zhiye' AND B.P_CODE =[职业])[职业],[职业] as [职业编码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='hyzk' AND B.P_CODE =[婚姻状况])[婚姻状况],[婚姻状况] as [婚姻状况编码]
	  	                                        ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ylfzflx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [医疗费支付类型] +',')>0
		                                          FOR XML PATH('')) as '医疗费支付类型',医疗费支付类型 as 医疗费支付类编码
                                              ,职工医疗保险卡号
                                              ,居民医疗保险卡号
                                              ,贫困救助卡号
                                              ,[医疗保险号]
                                              ,[新农合号]
                                              ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE  机构编号 =[所属机构])[所属机构] ,[所属机构]  as [所属机构编码] 
                                              ,[调查时间]
                                              ,[创建时间]
                                              ,[修改时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE  机构编号 =[创建机构])[创建机构] ,[创建机构]  as [创建机构编码] 
                                              ,[PASSWORD]
                                              ,[D_ZHUXIAO]
                                              ,[联系人姓名]
                                              ,[联系人电话]
                                              ,[医疗费用支付类型其他]
                                              ,[怀孕情况]
                                              ,[孕次]
                                              ,[产次]
                                              ,[缺项]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='dalb' AND B.P_CODE = [档案类别])[档案类别]  ,[档案类别]  as [档案类别编码]  
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rkx_dazt' AND B.P_CODE = [档案状态])[档案状态]  , [档案状态]   as [档案状态编码]  
                                              ,[D_DAZTYY]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zjlb' AND B.P_CODE = [证件类型])[证件类型],[证件类型] as [证件类型编码]
                                              ,[D_ZJHQT]
	                                          ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cf_pqsb' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [厨房排气设施] +',')>0
		                                          FOR XML PATH('')) as '厨房排气设施'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ranliao_lx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 燃料类型 +',')>0
		                                          FOR XML PATH('')) as '燃料类型'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='yinshui' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 饮水 +',')>0
		                                          FOR XML PATH('')) as '饮水'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='qinxulan' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 禽畜栏 +',')>0
		                                          FOR XML PATH('')) as '禽畜栏'
                                                ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cesuo' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 厕所 +',')>0
		                                          FOR XML PATH('')) as '厕所'
                                              ,[完整度]
                                              ,[D_GRDABH_17]
                                              ,[D_GRDABH_SHOW]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='ldqd' AND B.P_CODE = [劳动强度])[劳动强度]  ,[劳动强度]   as [劳动强度编码]  ,档案位置
                                              --,(select top 1 isnull(身高,0) from tb_健康体检 where tb_健康档案.个人档案编号 = tb_健康体检.个人档案编号  order by tb_健康体检.ID desc) as 身高
                                              ,(select top 1 isnull(身高,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号   order by tj.体检日期 desc) as 身高
                                              ,(select top 1 isnull(体重,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号   order by tj.体检日期 desc) as 体重
                                              ,(select top 1 isnull(体重指数,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 体重指数
                                              ,(select top 1 isnull(腰围,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 腰围
                                              ,(select top 1 isnull(血压左侧1,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血压左侧1
                                              ,(select top 1 isnull(血压左侧2,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血压左侧2
                                              ,(select top 1 isnull(空腹血糖,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 空腹血糖
                                              ,(select top 1 isnull(血清高密度脂蛋白胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血清高密度脂蛋白胆固醇
                                              ,(select top 1 isnull(血清低密度脂蛋白胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血清低密度脂蛋白胆固醇
                                              ,(select top 1 isnull(甘油三酯,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 甘油三酯
                                              ,(select top 1 isnull(总胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 总胆固醇
                                              ,(select top 1 isnull(体检日期,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 体检日期
                                              ,(select top 1 isnull(吸烟状况,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 吸烟状况
                                              ,(select top 1 isnull(饮酒频率,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 饮酒频率
                                              ,(select top 1 isnull(锻炼频率,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 锻炼频率
                                              ,(select top 1 isnull(老年人自理评估,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 老年人自理评估
                                              ,(select top 1 isnull(尿微量白蛋白,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as  尿微量白蛋白
                                              ,(select top 1 isnull(糖化血红蛋白,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as  糖化血红蛋白
                                                    from [tb_健康档案]   where 家庭档案编号 = '" + familyNo + "' order by 与户主关系 asc"; //查询整个家庭档案中所有成员的数据
            }
            else  //不存在家庭档案编号， 则根据个人档案编号进行查询  ， 只查询其档案数据
            {
                sql1 = @" SELECT  [ID],[证件类型] as [证件类型编码]  , [档案状态]   as [档案状态编码]  ,[档案类别]  as [档案类别编码]  
,[创建机构]  as [创建机构编码] ,[所属机构]  as [所属机构编码] ,医疗费支付类型 as 医疗费支付类编码
,[婚姻状况] as [婚姻状况编码],[职业] as [职业编码],[文化程度] as [文化程度编码],[民族] as [民族编码]
,[居委会] as [居委会编码],[性别] as [性别编码],[常住类型]  as [常住类型编码]
,[市] AS 市编码,[区] as [区编码],[街道] as [街道编码],[与户主关系] AS [与户主关系编码]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[拼音简码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='yhzgx' AND B.P_CODE =[与户主关系])[与户主关系]
                                              ,[工作单位]
                                              ,[本人电话]
                                              ,[邮箱]
                                              ,[省]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [市])[市]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [区])[区]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [街道])[街道]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [居委会])[居委会]
                                              ,[居住地址]
                                              ,[所属片区]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='jzzk' AND B.P_CODE =[常住类型])[常住类型]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xb_xingbie' AND B.P_CODE =[性别])[性别]
                                              ,[出生日期]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='mz_minzu' AND B.P_CODE =[民族])[民族]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='whcd' AND B.P_CODE =[文化程度])[文化程度]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zy_zhiye' AND B.P_CODE =[职业])[职业]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='hyzk' AND B.P_CODE =[婚姻状况])[婚姻状况]
	  	                                        ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ylfzflx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [医疗费支付类型] +',')>0
		                                          FOR XML PATH('')) as '医疗费支付类型'
                                              ,[医疗保险号]
                                              ,[新农合号]
                                              ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE 机构编号 =[所属机构])[所属机构] 
                                              ,[调查时间]
                                              ,[创建时间]
                                              ,[修改时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE 机构编号 =[创建机构])[创建机构] 
                                              ,[PASSWORD]
                                              ,[D_ZHUXIAO]
                                              ,[联系人姓名]
                                              ,[联系人电话]
                                              ,[医疗费用支付类型其他]
                                              ,[怀孕情况]
                                              ,[孕次]
                                              ,[产次]
                                              ,[缺项]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='dalb' AND B.P_CODE = [档案类别])[档案类别]  
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rkx_dazt' AND B.P_CODE = [档案状态])[档案状态]  
                                              ,[D_DAZTYY]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zjlb' AND B.P_CODE = [证件类型])[证件类型]
                                              ,[D_ZJHQT]
	                                          ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cf_pqsb' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [厨房排气设施] +',')>0
		                                          FOR XML PATH('')) as '厨房排气设施'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ranliao_lx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 燃料类型 +',')>0
		                                          FOR XML PATH('')) as '燃料类型'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='yinshui' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 饮水 +',')>0
		                                          FOR XML PATH('')) as '饮水'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='qinxulan' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 禽畜栏 +',')>0
		                                          FOR XML PATH('')) as '禽畜栏'
                                                ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cesuo' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 厕所 +',')>0
		                                          FOR XML PATH('')) as '厕所'
                                              ,[完整度]
                                              ,[D_GRDABH_17]
                                              ,[D_GRDABH_SHOW]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='ldqd' AND B.P_CODE = [劳动强度])[劳动强度]   ,[劳动强度]   as [劳动强度编码]   ,档案位置
                                              --,(select top 1 isnull(身高,0) from tb_健康体检 where tb_健康档案.个人档案编号 = tb_健康体检.个人档案编号  order by tb_健康体检.ID desc) as 身高
                                              ,(select top 1 isnull(身高,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号   order by tj.体检日期 desc) as 身高
                                              ,(select top 1 isnull(体重,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号   order by tj.体检日期 desc) as 体重
                                              ,(select top 1 isnull(体重指数,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 体重指数
                                              ,(select top 1 isnull(腰围,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 腰围
                                              ,(select top 1 isnull(血压左侧1,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血压左侧1
                                              ,(select top 1 isnull(血压左侧2,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血压左侧2
                                              ,(select top 1 isnull(空腹血糖,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 空腹血糖
                                              ,(select top 1 isnull(血清高密度脂蛋白胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血清高密度脂蛋白胆固醇
                                              ,(select top 1 isnull(血清低密度脂蛋白胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 血清低密度脂蛋白胆固醇
                                              ,(select top 1 isnull(甘油三酯,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 甘油三酯
                                              ,(select top 1 isnull(总胆固醇,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 总胆固醇
                                              ,(select top 1 isnull(体检日期,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 体检日期
                                              ,(select top 1 isnull(吸烟状况,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 吸烟状况
                                              ,(select top 1 isnull(饮酒频率,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 饮酒频率
                                              ,(select top 1 isnull(锻炼频率,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 锻炼频率
                                              ,(select top 1 isnull(老年人自理评估,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as 老年人自理评估
                                              ,(select top 1 isnull(尿微量白蛋白,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as  尿微量白蛋白
                                              ,(select top 1 isnull(糖化血红蛋白,0) from tb_健康体检 tj where tj.个人档案编号 = tb_健康档案.个人档案编号 order by tj.体检日期 desc) as  糖化血红蛋白
                                                    from [tb_健康档案]   where 个人档案编号 = '" + docNo + "' "; //查询整个家庭档案中所有成员的数据
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息页面用，不需要生成  家庭档案编号
        /// </summary>
        /// <param name="docNo"></param>
        /// <returns></returns>
        public System.Data.DataSet GetNewUserByKey(string docNo)
        {
            string sql1 = " select * from [tb_健康档案]   where [" + tb_健康档案.家庭档案编号 + "]=@DocNo1";      //子表
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2";      //子表
            string sql3 = " select * from [tb_健康档案_健康状态]   where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo3";      //子表
            string sql4 = " select * from [tb_健康档案_既往病史]   where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo4";      //子表
            string sql5 = " select * from [tb_健康档案_家族病史]   where [" + tb_健康档案_家族病史.个人档案编号 + "]=@DocNo5";      //子表
            string sql6 = " select * from [tb_儿童基本信息]   where [" + tb_儿童基本信息.个人档案编号 + "]=@DocNo6";      //子表
            string sql7 = " select * from [tb_MXB高血压管理卡]   where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo7";      //子表
            string sql8 = " select * from [tb_MXB糖尿病管理卡]   where [" + tb_MXB糖尿病管理卡.个人档案编号 + "]=@DocNo8";      //子表
            string sql9 = " select * from [tb_MXB慢病基础信息表]   where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo9";      //子表
            string sql10 = " select * from [tb_老年人基本信息]   where [" + tb_老年人基本信息.个人档案编号 + "]=@DocNo10";      //子表
            string sql11 = " select * from [tb_妇女基本信息]   where [" + tb_妇女基本信息.个人档案编号 + "]=@DocNo11";      //子表
            string sql12 = " select * from [tb_孕妇基本信息]   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo12";      //子表
            string sql13 = " select * from [tb_MXB脑卒中管理卡]   where [" + tb_MXB脑卒中管理卡.个人档案编号 + "]=@DocNo13";      //子表
            string sql14 = " select * from [tb_MXB冠心病管理卡]   where [" + tb_MXB冠心病管理卡.个人档案编号 + "]=@DocNo14";      //子表
            string sql15 = " select * from [tb_精神疾病信息补充表]   where [" + tb_精神疾病信息补充表.个人档案编号 + "]=@DocNo15";      //子表
            string sql16 = " select * from [tb_残疾人_基本信息]   where [" + tb_残疾人_基本信息.个人档案编号 + "]=@DocNo16";      //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9 + sql10 + sql11 + sql12 + sql13 + sql14 + sql15 + sql16);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo7", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo8", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo9", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo10", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo11", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo12", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo13", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo14", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo15", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo16", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_健康状态.__TableName;   //子表
            ds.Tables[3].TableName = tb_健康档案_既往病史.__TableName;   //子表
            ds.Tables[4].TableName = tb_健康档案_家族病史.__TableName;   //子表
            ds.Tables[5].TableName = tb_儿童基本信息.__TableName;   //子表
            ds.Tables[6].TableName = tb_MXB高血压管理卡.__TableName;   //子表
            ds.Tables[7].TableName = tb_MXB糖尿病管理卡.__TableName;   //子表
            ds.Tables[8].TableName = tb_MXB慢病基础信息表.__TableName;   //子表
            ds.Tables[9].TableName = tb_老年人基本信息.__TableName;   //子表
            ds.Tables[10].TableName = tb_妇女基本信息.__TableName;   //子表
            ds.Tables[11].TableName = tb_孕妇基本信息.__TableName;   //子表
            ds.Tables[12].TableName = tb_MXB脑卒中管理卡.__TableName;   //子表
            ds.Tables[13].TableName = tb_MXB冠心病管理卡.__TableName;   //子表
            ds.Tables[14].TableName = tb_精神疾病信息补充表.__TableName;   //子表
            ds.Tables[15].TableName = tb_残疾人_基本信息.__TableName;   //子表
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息页面_显示
        ///  用，不需要生成  家庭档案编号
        ///  直接查询出对应Code的值，绑定后显示
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetOneUserByKeyForShow(string docNo)
        {
            string sql1 = @"  SELECT  [ID]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[拼音简码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='yhzgx' AND B.P_CODE =[与户主关系])[与户主关系]
                                              ,[工作单位]
                                              ,[本人电话]
                                              ,[邮箱]
                                              ,[省]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [市])[市]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [区])[区]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [街道])[街道]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [居委会])[居委会]
                                              ,[居住地址]
                                              ,[所属片区]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='jzzk' AND B.P_CODE =[常住类型])[常住类型]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xb_xingbie' AND B.P_CODE =[性别])[性别]
                                              ,[出生日期]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='mz_minzu' AND B.P_CODE =[民族])[民族]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='whcd' AND B.P_CODE =[文化程度])[文化程度]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zy_zhiye' AND B.P_CODE =[职业])[职业]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='hyzk' AND B.P_CODE =[婚姻状况])[婚姻状况]
	  	                                        ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ylfzflx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [医疗费支付类型] +',')>0
		                                          FOR XML PATH('')) as '医疗费支付类型'
                                              ,职工医疗保险卡号
                                              ,居民医疗保险卡号
                                              ,贫困救助卡号
                                              ,[医疗保险号]
                                              ,[新农合号]
                                                ,所属机构 
                                              ,[调查时间]
                                              ,[创建时间]
                                              ,[修改时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,创建机构
                                              ,[PASSWORD]
                                              ,[D_ZHUXIAO]
                                              ,[联系人姓名]
                                              ,[联系人电话]
                                              ,[医疗费用支付类型其他]
                                              ,[怀孕情况]
                                              ,[孕次]
                                              ,[产次]
                                              ,[缺项]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='dalb' AND B.P_CODE = [档案类别])[档案类别]  
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rkx_dazt' AND B.P_CODE = [档案状态])[档案状态]  
                                              ,[D_DAZTYY]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zjlb' AND B.P_CODE = [证件类型])[证件类型]
                                              ,[D_ZJHQT]
	                                          ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cf_pqsb' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [厨房排气设施] +',')>0
		                                          FOR XML PATH('')) as '厨房排气设施'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ranliao_lx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 燃料类型 +',')>0
		                                          FOR XML PATH('')) as '燃料类型'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='yinshui' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 饮水 +',')>0
		                                          FOR XML PATH('')) as '饮水'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='qinxulan' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 禽畜栏 +',')>0
		                                          FOR XML PATH('')) as '禽畜栏'
                                                ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cesuo' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 厕所 +',')>0
		                                          FOR XML PATH('')) as '厕所'
                                              ,[完整度]
                                              ,[D_GRDABH_17]
                                              ,[D_GRDABH_SHOW]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='ldqd' AND B.P_CODE = [劳动强度])[劳动强度],是否流动人口,是否签约,是否贫困人口,外出情况,外出地址 ,档案位置
                                              ,户主姓名
                                              ,户主身份证号
                                              ,家庭人口数
                                              ,家庭结构
                                              ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='lnr_jzqk' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [居住情况] +',')>0
		                                          FOR XML PATH('')) as '居住情况'
                                               ,本人或家属签字
                                               ,签字时间,复核标记,复核备注,二次复核,二次复核时间
                                          FROM [tb_健康档案]    where [" + tb_健康档案.__KeyName + "]=@DocNo1";      //子表
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2";      //子表
            string sql3 = @" SELECT  TOP 1  [ID]
                                              ,[个人档案编号]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xuexing' AND B.P_CODE =[血型])[血型]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rhxx' AND B.P_CODE =[RH])[RH]
                                              ,[过敏史有无]
	                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='gms' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 药物过敏史 +',')>0
		                                          FOR XML PATH('')) as '药物过敏史'
                                              ,[过敏史其他]
                                              ,[暴露史]
                                              ,[D_YBLS]
                                              ,[暴露史化学品]
                                              ,[暴露史毒物]
                                              ,[暴露史射线]
                                              ,[遗传病史有无]
                                              ,[遗传病史]
                                              ,[有无残疾]
                                              ,[有无疾病]
                                              ,[有无既往史]
	                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cjjk' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 残疾情况 +',')>0
		                                          FOR XML PATH('')) as '残疾情况'
                                              ,[调查时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,[创建时间]
                                              ,[残疾其他]
                                          FROM[tb_健康档案_健康状态]   where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo3 ORDER BY ID DESC";      //子表
            string sql4 = " select * from [tb_健康档案_既往病史] where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo4";//既往史
            string sql5 = " select * from [tb_健康档案_家族病史] where [" + tb_健康档案_家族病史.个人档案编号 + "]=@DocNo5";//家族病史

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_健康状态.__TableName;   //子表
            ds.Tables[3].TableName = tb_健康档案_既往病史.__TableName;   //子表
            ds.Tables[4].TableName = tb_健康档案_家族病史.__TableName;   //子表
            return ds;
        }
        /// <summary>
        ///  UC个人基本信息页面_显示，只查询Code,修改页面用
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public System.Data.DataSet GetOneUserCodeByKey(string docNo)
        {
            string sql1 = @"  SELECT  *   FROM [tb_健康档案]    where [" + tb_健康档案.__KeyName + "]=@DocNo1";      //子表
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2";      //子表
            string sql3 = @" SELECT TOP 1  *  FROM[tb_健康档案_健康状态]   where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo3 ORDER BY ID DESC";      //子表
            string sql4 = " select * from [tb_健康档案_既往病史] where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo4";//既往史
            string sql5 = " select * from [tb_健康档案_家族病史] where [" + tb_健康档案_家族病史.个人档案编号 + "]=@DocNo5";//家族病史
            string sql6 = " select * from [tb_儿童基本信息]   where [" + tb_儿童基本信息.个人档案编号 + "]=@DocNo6";      //子表
            string sql7 = " select * from [tb_MXB高血压管理卡]   where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo7";      //子表
            string sql8 = " select * from [tb_MXB糖尿病管理卡]   where [" + tb_MXB糖尿病管理卡.个人档案编号 + "]=@DocNo8";      //子表
            string sql9 = " select * from [tb_MXB慢病基础信息表]   where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo9";      //子表
            string sql10 = " select * from [tb_老年人基本信息]   where [" + tb_老年人基本信息.个人档案编号 + "]=@DocNo10";      //子表
            string sql11 = " select * from tb_妇女基本信息   where [" + tb_妇女基本信息.个人档案编号 + "]=@DocNo11";      //子表
            string sql12 = " select * from tb_孕妇基本信息   where [" + tb_孕妇基本信息.个人档案编号 + "]=@DocNo12";      //子表
            string sql13 = " select * from tb_MXB冠心病管理卡   where [" + tb_MXB冠心病管理卡.个人档案编号 + "]=@DocNo13";      //子表
            string sql14 = " select * from tb_MXB脑卒中管理卡   where [" + tb_MXB脑卒中管理卡.个人档案编号 + "]=@DocNo14";      //子表
            string sql15 = " select * from tb_精神疾病信息补充表   where [" + tb_精神疾病信息补充表.个人档案编号 + "]=@DocNo15";      //子表
            string sql16 = " select * from tb_残疾人_基本信息   where [" + tb_残疾人_基本信息.个人档案编号 + "]=@DocNo16";      //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9 + sql10 + sql11 + sql12 + sql13 + sql14 + sql15 + sql16);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo7", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo8", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo9", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo10", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo11", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo12", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo13", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo14", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo15", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo16", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;   //子表
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;   //子表
            ds.Tables[2].TableName = tb_健康档案_健康状态.__TableName;   //子表
            ds.Tables[3].TableName = tb_健康档案_既往病史.__TableName;   //子表
            ds.Tables[4].TableName = tb_健康档案_家族病史.__TableName;   //子表
            ds.Tables[5].TableName = tb_儿童基本信息.__TableName;   //子表
            ds.Tables[6].TableName = tb_MXB高血压管理卡.__TableName;   //子表
            ds.Tables[7].TableName = tb_MXB糖尿病管理卡.__TableName;   //子表
            ds.Tables[8].TableName = tb_MXB慢病基础信息表.__TableName;   //子表
            ds.Tables[9].TableName = tb_老年人基本信息.__TableName;   //子表
            ds.Tables[10].TableName = tb_妇女基本信息.__TableName;   //子表
            ds.Tables[11].TableName = tb_孕妇基本信息.__TableName;   //子表
            ds.Tables[12].TableName = tb_MXB冠心病管理卡.__TableName;   //子表
            ds.Tables[13].TableName = tb_MXB脑卒中管理卡.__TableName;   //子表
            ds.Tables[14].TableName = tb_精神疾病信息补充表.__TableName;   //子表
            ds.Tables[15].TableName = tb_残疾人_基本信息.__TableName;   //子表
            return ds;
        }
        /// <summary>
        /// 根据机构、街道和居委会查询总人数
        /// </summary>
        /// <param name="rgid">所属机构</param>
        /// <param name="jd">街道</param>
        /// <param name="jwh">居委会</param>
        /// <returns></returns>
        public int SelectCountForJg(string rgid, string jd, string jwh, string creUser)
        {
            //int count = 0;
            string sql = "select count(*) from tb_健康档案 where 所属机构 ='" + rgid + "'";
            if (!string.IsNullOrEmpty(jd))
            {
                sql += " and 街道 = '" + jd + "'";
            }
            if (!string.IsNullOrEmpty(jwh))
            {
                sql += " and 居委会 = '" + jwh + "'";
            }
            if (!string.IsNullOrEmpty(creUser))
            {
                sql += "  and (修改人 in(select 用户编码 from tb_Myuser where 所属机构='" + creUser + "' ) or 创建人 in(select 用户编码 from tb_Myuser where 所属机构='" + creUser + "' )) ";
            }
            return Convert.ToInt32(DataProvider.Instance.ExecuteScalar(_Loginer.DBName, sql));
            //return count;
        }
        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public SaveResult DeleteByDocNo(string docNo)
        {
            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();
            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            try
            {
                string sql = string.Empty;
                string[] tables = GetTable.getGetDeleteTableName();
                string[] tableInfos = GetTable.getGetTableInfoName();
                for (int j = 0; j < tableInfos.Length; j++)
                {
                    if (!string.IsNullOrEmpty(tableInfos[j]))
                    {
                        sql += string.Format(@"  delete {0} where 个人档案编号 = '{1}'   ", tableInfos[j], docNo);
                    }
                }
                for (int j = 0; j < tables.Length; j++)
                {
                    if (!string.IsNullOrEmpty(tables[j]))
                    {
                        if (tables[j] != "tb_家庭档案")
                        {
                            sql += string.Format("  delete {0}  where 个人档案编号 = '{1}' ", tables[j], docNo);
                        }
                    }
                }
                sql += string.Format("  delete tb_健康档案_健康状态  where 个人档案编号 = '{0}' ", docNo); //单独删除健康状态
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);//+ sql1
                int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务  
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      

                mResult.SetException(ex); //保存结果设置异常消息

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
        }
        public bool Delete(string docNo)
        {
            return true;
        }
        /// <summary>
        //保存数据
        /// </summary>
        public override SaveResult Update(DataSet data)
        {
            //对个人健康档案保存进行单独处理...重写保存方法              

            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            string m家庭编号 = string.Empty;
            string m个人编号 = string.Empty;


            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            string 高血压管理卡 = "";
            string 糖尿病管理卡 = "";
            string 脑卒中管理卡 = "";
            string 冠心病管理卡 = "";
            if (data.Tables.Contains(tb_MXB高血压管理卡.__TableName))
            {
                高血压管理卡 = DAL_System.DocNoTool.Get高血压管理卡(_CurrentTrans);
            }
            if (data.Tables.Contains(tb_MXB糖尿病管理卡.__TableName))
            {
                糖尿病管理卡 = DAL_System.DocNoTool.Get糖尿病管理卡(_CurrentTrans);
            }
            if (data.Tables.Contains(tb_MXB脑卒中管理卡.__TableName))
            {
                脑卒中管理卡 = DAL_System.DocNoTool.Get脑卒中管理卡(_CurrentTrans);
            }
            if (data.Tables.Contains(tb_MXB冠心病管理卡.__TableName))
            {
                冠心病管理卡 = DAL_System.DocNoTool.Get冠心病管理卡(_CurrentTrans);
            }
            try
            {
                //更新所有资料表
                foreach (DataTable dt in data.Tables)
                {
                    //仅处理有作修改的资料表
                    if (dt.GetChanges() == null) continue;

                    //根据资料表名获取SQL命令生成器
                    IGenerateSqlCommand gen = this.CreateSqlGenerator(dt.TableName);
                    if (gen == null) continue; //当前数据层无法识别的SQL命令生成器，则不更新当前资料表

                    //本次更新是更新主表,取主表的主键用于设置明细表的外键
                    if (gen.IsSummary() && dt.TableName == tb_家庭档案.__TableName)
                    {
                        UpdateSummaryKey(_CurrentTrans, dt, _UpdateSummaryKeyMode, gen.GetPrimaryFieldName(),
                            ref m家庭编号, gen.GetDocNoFieldName(), ref m个人编号);

                        mResult.GUID = m家庭编号;
                        mResult.DocNo = m个人编号;
                    }
                    else
                    {
                        //更新明细表的外键,对健康档案生成两个键值进行重写pan
                        if (dt.TableName == tb_健康档案.__TableName || dt.TableName == tb_健康档案_个人健康特征.__TableName || dt.TableName == tb_MXB慢病基础信息表.__TableName || dt.TableName == tb_儿童基本信息.__TableName || dt.TableName == tb_老年人基本信息.__TableName || dt.TableName == tb_妇女基本信息.__TableName || dt.TableName == tb_孕妇基本信息.__TableName || dt.TableName == tb_精神疾病信息补充表.__TableName || dt.TableName == tb_残疾人_基本信息.__TableName)
                        {
                            if (_UpdateSummaryKeyMode == UpdateKeyMode.DocumentNoAndGuid)
                                UpdateDetailKey(dt, gen.GetForeignFieldName(), m家庭编号);
                        }
                        //赋值个人档案编号
                        foreach (DataRow row in dt.Rows)
                        {
                            //仅新增记录才需要更新外键，注意状态的使用
                            if (row.RowState == DataRowState.Added)
                            {
                                if (dt.TableName == tb_健康档案.__TableName && row.Table.Columns.Contains(tb_健康档案.__KeyName)) row[tb_健康档案.__KeyName] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_个人健康特征.__TableName && row.Table.Columns.Contains(tb_健康档案_个人健康特征.__KeyName)) row[tb_健康档案_个人健康特征.__KeyName] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_健康状态.__TableName && row.Table.Columns.Contains(tb_健康档案_健康状态.__KeyName)) row[tb_健康档案_健康状态.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_既往病史.__TableName && row.Table.Columns.Contains(tb_健康档案_既往病史.__KeyName)) row[tb_健康档案_既往病史.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_家族病史.__TableName && row.Table.Columns.Contains(tb_健康档案_家族病史.__KeyName)) row[tb_健康档案_家族病史.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB慢病基础信息表.__TableName && row.Table.Columns.Contains(tb_MXB慢病基础信息表.__KeyName)) row[tb_MXB慢病基础信息表.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB高血压管理卡.__TableName && row.Table.Columns.Contains(tb_MXB高血压管理卡.__KeyName))
                                {
                                    row[tb_MXB高血压管理卡.个人档案编号] = mResult.DocNo;
                                    row[tb_MXB高血压管理卡.管理卡编号] = 高血压管理卡;
                                }
                                if (dt.TableName == tb_MXB糖尿病管理卡.__TableName && row.Table.Columns.Contains(tb_MXB糖尿病管理卡.__KeyName))
                                {
                                    row[tb_MXB糖尿病管理卡.个人档案编号] = mResult.DocNo;
                                    row[tb_MXB糖尿病管理卡.管理卡编号] = 糖尿病管理卡;
                                }
                                if (dt.TableName == tb_MXB脑卒中管理卡.__TableName && row.Table.Columns.Contains(tb_MXB脑卒中管理卡.__KeyName))
                                {
                                    row[tb_MXB脑卒中管理卡.个人档案编号] = mResult.DocNo;
                                    row[tb_MXB脑卒中管理卡.管理卡编号] = 脑卒中管理卡;
                                }
                                if (dt.TableName == tb_MXB冠心病管理卡.__TableName && row.Table.Columns.Contains(tb_MXB冠心病管理卡.__KeyName))
                                {
                                    row[tb_MXB冠心病管理卡.个人档案编号] = mResult.DocNo;
                                    row[tb_MXB冠心病管理卡.管理卡编号] = 冠心病管理卡;
                                }
                                if (dt.TableName == tb_儿童基本信息.__TableName && row.Table.Columns.Contains(tb_儿童基本信息.__KeyName)) row[tb_儿童基本信息.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_老年人基本信息.__TableName && row.Table.Columns.Contains(tb_老年人基本信息.__KeyName)) row[tb_老年人基本信息.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_妇女基本信息.__TableName && row.Table.Columns.Contains(tb_妇女基本信息.__KeyName)) row[tb_妇女基本信息.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_孕妇基本信息.__TableName && row.Table.Columns.Contains(tb_孕妇基本信息.__KeyName)) row[tb_孕妇基本信息.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_精神疾病信息补充表.__TableName && row.Table.Columns.Contains(tb_精神疾病信息补充表.__KeyName)) row[tb_精神疾病信息补充表.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_残疾人_基本信息.__TableName && row.Table.Columns.Contains(tb_残疾人_基本信息.__KeyName)) row[tb_残疾人_基本信息.个人档案编号] = mResult.DocNo;
                            }
                        }
                    }

                    //使用基于ADO构架的SqlClient组件更新数据
                    SqlDataAdapter adp = new SqlDataAdapter();
                    adp.RowUpdating += new SqlRowUpdatingEventHandler(OnAdapterRowUpdating);
                    adp.UpdateCommand = GetUpdateCommand(gen, _CurrentTrans);
                    adp.InsertCommand = GetInsertCommand(gen, _CurrentTrans);
                    adp.DeleteCommand = GetDeleteCommand(gen, _CurrentTrans);
                    adp.Update(dt);
                }

                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       

            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      
                mResult.SetException(ex); //保存结果设置异常消息
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
            //调用基类的方法保存数据
            //return base.Update(data);
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult UpdateOneData(DataSet data)
        {
            //对个人健康档案保存进行单独处理...重写保存方法              

            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            string m家庭编号 = string.Empty;
            string m个人编号 = string.Empty;

            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");

            try
            {
                //更新所有资料表
                foreach (DataTable dt in data.Tables)
                {
                    //仅处理有作修改的资料表
                    if (dt.GetChanges() == null) continue;

                    //根据资料表名获取SQL命令生成器
                    IGenerateSqlCommand gen = this.CreateSqlGenerator(dt.TableName);
                    if (gen == null) continue; //当前数据层无法识别的SQL命令生成器，则不更新当前资料表
                    if (dt.TableName == tb_MXB高血压管理卡.__TableName && dt.Rows.Count != 0)
                    {
                        string GXYGLKH = DocNoTool.Get高血压管理卡(_CurrentTrans);
                        dt.Rows[0][tb_MXB高血压管理卡.管理卡编号] = GXYGLKH;
                    }
                    if (dt.TableName == tb_MXB糖尿病管理卡.__TableName && dt.Rows.Count != 0)
                    {
                        string TNBGLKH = DocNoTool.Get糖尿病管理卡(_CurrentTrans);
                        dt.Rows[0][tb_MXB糖尿病管理卡.管理卡编号] = TNBGLKH;
                    }
                    if (dt.TableName == tb_MXB脑卒中管理卡.__TableName && dt.Rows.Count != 0)
                    {
                        string NZZGLKH = DocNoTool.Get脑卒中管理卡(_CurrentTrans);
                        dt.Rows[0][tb_MXB脑卒中管理卡.管理卡编号] = NZZGLKH;
                    }
                    if (dt.TableName == tb_MXB冠心病管理卡.__TableName && dt.Rows.Count != 0)
                    {
                        string GXBGLKH = DocNoTool.Get冠心病管理卡(_CurrentTrans);
                        dt.Rows[0][tb_MXB冠心病管理卡.管理卡编号] = GXBGLKH;
                    }
                    //本次更新是更新主表,取主表的主键用于设置明细表的外键
                    if (dt.TableName == tb_健康档案.__TableName)
                    {
                        UpdateSummaryKey(_CurrentTrans, dt, _UpdateSummaryKeyMode, gen.GetPrimaryFieldName(),
                            ref m家庭编号, gen.GetDocNoFieldName(), ref m个人编号);

                        mResult.GUID = m家庭编号;
                        mResult.DocNo = m个人编号;
                    }
                    else
                    {
                        //更新明细表的外键,对健康档案生成两个键值进行重写pan
                        //if (_UpdateSummaryKeyMode == UpdateKeyMode.DocumentNoAndGuid)
                        //    UpdateDetailKey(dt, gen.GetForeignFieldName(), m家庭编号);
                        //赋值个人档案编号
                        foreach (DataRow row in dt.Rows)
                        {
                            //仅新增记录才需要更新外键，注意状态的使用
                            if (row.RowState == DataRowState.Added)
                            {
                                if (dt.TableName == tb_家庭档案.__TableName && row.Table.Columns.Contains(tb_家庭档案.__KeyName))
                                {
                                    row[tb_健康档案.__KeyName] = mResult.DocNo;
                                }
                                if (dt.TableName == tb_健康档案.__TableName && row.Table.Columns.Contains(tb_健康档案.__KeyName)) row[tb_健康档案.__KeyName] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_个人健康特征.__TableName && row.Table.Columns.Contains(tb_健康档案_个人健康特征.__KeyName)) row[tb_健康档案_个人健康特征.__KeyName] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_健康状态.__TableName && row.Table.Columns.Contains(tb_健康档案_健康状态.__KeyName)) row[tb_健康档案_健康状态.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_既往病史.__TableName && row.Table.Columns.Contains(tb_健康档案_既往病史.__KeyName)) row[tb_健康档案_既往病史.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_健康档案_家族病史.__TableName && row.Table.Columns.Contains(tb_健康档案_家族病史.__KeyName)) row[tb_健康档案_家族病史.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB慢病基础信息表.__TableName && row.Table.Columns.Contains(tb_MXB慢病基础信息表.__KeyName))
                                {
                                    row[tb_MXB慢病基础信息表.个人档案编号] = mResult.DocNo;
                                    row[tb_MXB慢病基础信息表.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_MXB高血压管理卡.__TableName && row.Table.Columns.Contains(tb_MXB高血压管理卡.__KeyName)) row[tb_MXB高血压管理卡.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB糖尿病管理卡.__TableName && row.Table.Columns.Contains(tb_MXB糖尿病管理卡.__KeyName)) row[tb_MXB糖尿病管理卡.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB脑卒中管理卡.__TableName && row.Table.Columns.Contains(tb_MXB脑卒中管理卡.__KeyName)) row[tb_MXB脑卒中管理卡.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_MXB冠心病管理卡.__TableName && row.Table.Columns.Contains(tb_MXB冠心病管理卡.__KeyName)) row[tb_MXB冠心病管理卡.个人档案编号] = mResult.DocNo;
                                if (dt.TableName == tb_儿童基本信息.__TableName && row.Table.Columns.Contains(tb_儿童基本信息.__KeyName))
                                {
                                    row[tb_儿童基本信息.个人档案编号] = mResult.DocNo;
                                    row[tb_儿童基本信息.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_老年人基本信息.__TableName && row.Table.Columns.Contains(tb_老年人基本信息.__KeyName))
                                {
                                    row[tb_老年人基本信息.个人档案编号] = mResult.DocNo;
                                    row[tb_老年人基本信息.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_妇女基本信息.__TableName && row.Table.Columns.Contains(tb_妇女基本信息.__KeyName))
                                {
                                    row[tb_妇女基本信息.个人档案编号] = mResult.DocNo;
                                    row[tb_妇女基本信息.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_孕妇基本信息.__TableName && row.Table.Columns.Contains(tb_孕妇基本信息.__KeyName))
                                {
                                    row[tb_孕妇基本信息.个人档案编号] = mResult.DocNo;
                                    row[tb_孕妇基本信息.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_精神疾病信息补充表.__TableName && row.Table.Columns.Contains(tb_精神疾病信息补充表.__KeyName))
                                {
                                    row[tb_精神疾病信息补充表.个人档案编号] = mResult.DocNo;
                                    row[tb_精神疾病信息补充表.家庭档案编号] = mResult.GUID;
                                }
                                if (dt.TableName == tb_残疾人_基本信息.__TableName && row.Table.Columns.Contains(tb_残疾人_基本信息.__KeyName))
                                {
                                    row[tb_残疾人_基本信息.个人档案编号] = mResult.DocNo;
                                    row[tb_残疾人_基本信息.家庭档案编号] = mResult.GUID;
                                }
                            }
                        }
                    }

                    //使用基于ADO构架的SqlClient组件更新数据
                    SqlDataAdapter adp = new SqlDataAdapter();
                    adp.RowUpdating += new SqlRowUpdatingEventHandler(OnAdapterRowUpdating);
                    adp.UpdateCommand = GetUpdateCommand(gen, _CurrentTrans);
                    adp.InsertCommand = GetInsertCommand(gen, _CurrentTrans);
                    adp.DeleteCommand = GetDeleteCommand(gen, _CurrentTrans);
                    adp.Update(dt);
                }

                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       

            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      

                mResult.SetException(ex); //保存结果设置异常消息

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
            //调用基类的方法保存数据
            //return base.Update(data);
        }
        /// <summary>
        /// 更新主表主键
        /// </summary>
        /// <param name="tran">事务</param>
        /// <param name="summary">主表</param>
        /// <param name="model">主键更新类型</param>
        /// <param name="keyFieldName">主键字段名称</param>
        /// <param name="GUID">生成32位GUID主键值</param>
        /// <param name="docNoFieldName">单据号码的字段名</param>
        /// <param name="docNo">单据号码</param>
        protected override void UpdateSummaryKey(SqlTransaction tran, DataTable summary, UpdateKeyMode model,
             string keyFieldName, ref string GUID, string docNoFieldName, ref string docNo)
        {
            DataRow row = summary.Rows[0]; //取主表第一条记录

            //如果未指定单号更新类型则取旧的单号.
            if ((row.RowState != DataRowState.Added) || (model == UpdateKeyMode.None)) //取旧的单号
            {
                GUID = row[docNoFieldName].ToString();//家庭档案编号
                docNo = row[keyFieldName].ToString();//个人档案编号
                return;
            }

            //新增记录,更新主键的值
            if (row.RowState == DataRowState.Added)
            {
                //2015-08-08 09:49:28 Yufh添加整理,处理两个主键
                if (model == UpdateKeyMode.DocumentNoAndGuid)
                {
                    if (keyFieldName == "") throw new Exception("没有设定主键,检查类模型参数定义！");
                    if (docNoFieldName == "") throw new Exception("没有设定单号主键,检查类模型参数定义！");

                    DataTable dt = GetGuidAndNumber(tran, row[tb_家庭档案.街道].ToString(), row[tb_家庭档案.家庭档案编号].ToString()); //调用模板方法获取单据号码;
                    GUID = dt.Rows[0][0].ToString();//家庭档案编号
                    docNo = dt.Rows[0][1].ToString();//个人档案编号
                    row[keyFieldName] = docNo;
                    row[docNoFieldName] = GUID;
                }
            }
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "DA");
            return docNo;
        }

        /// <summary>
        ///获取单据流水号码
        /// </summary>
        private DataTable GetGuidAndNumber(SqlTransaction tran, String s街道编号, String s家庭档案号)
        {
            //if(_UpdateType.)
            string query = "sp_Get家庭编号 @s所属机构='" + _Loginer.所属机构 + "' ,@s街道编号='" + s街道编号 + "' ,@s家庭档案编号='" + s家庭档案号 + "' ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable docNo = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "家庭档案编号");
            return docNo;
        }


        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// type: 默认是为1  根据机构编号进行查询
        ///          2：查询总档案数
        ///          3：查询合格档案数
        ///          4：查询不合格档案数
        /// <returns></returns>
        public DataSet GetReportDataBYCJSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region MyRegion
                sql = @"select 
	                        所属上级机构 as 区域编码,
	                        c.机构名称 as 区域名称,
	                        sum(a.总数) as 总数,
	                        sum(a.合格数) as 合格数 ,
	                        sum(a.总数)-sum(a.合格数) as 不合格数,
	                        case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	                        avg(a.平均完整度) 平均完整度 ,
	                        sum(a.城镇人口数)as 城镇人口数,
	                        sum(a.农村人口数)as 农村人口数,
	                        sum(a.城镇人口数)+sum(a.农村人口数) as 总人口数,
	                        sum(a.转入档案数)as 转入档案数,
	                        sum(a.转出档案数)as 转出档案数,
                            sum(a.未录入有效证件数)as 未录入有效证件数,
                            sum(a.城镇档案数)as 城镇档案数,
                            sum(a.总数)  - sum(a.城镇档案数)  as 农村档案数,
                            case when sum(a.总数) = 0 then 0 else  sum(a.城镇档案数)*100.0/sum(a.总数) end as 城镇档案率,
                            case when sum(a.总数) = 0 then 0 else ( sum(a.总数)-sum(a.城镇档案数))*100.0/sum(a.总数) end as 农村档案率,
                            case when (sum(a.总数 )=0 or (sum(a.城镇人口数)+sum(a.农村人口数))=0) then 0 else sum(a.总数)*100.0/(sum(a.城镇人口数)+sum(a.农村人口数)) end as 健康管理率,
                            sum(a.更新数) as 更新数 ,
                            sum(a.非活动档案数) as 非活动档案数
	
                         from (
                        select   
	                        sum(a.城镇人口数)城镇人口数,
	                        sum(a.农村人口数)农村人口数,
	                        sum(a.总数) 总数,
	                        sum(a.合格数) 合格数 ,
	                        avg(a.平均完整度) 平均完整度 ,
	                        sum(a.转入档案数)转入档案数,
                            sum(a.转出档案数)转出档案数,
                            sum(a.未录入有效证件数)未录入有效证件数,
                            sum(a.城镇档案数)城镇档案数,
                            sum(a.更新数) as 更新数 ,
                            sum(a.非活动档案数) as 非活动档案数,
	                        case when   (len(@机构) = 6 and len(a.区域编码) >6) then substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		                        else a.区域编码 end as 所属上级机构
                            from(
                        select  --*
                                tab1.rgid 区域编码
                                ,tab1.机构名称 区域名称
                                ,convert(int,isnull(tab1.城镇人口数,0))城镇人口数
                                ,convert(int,isnull(tab1.农村人口数,0))农村人口数
                                --,convert(int,tab1.城镇人口数)+convert(int,tab1.农村人口数) 总人口数
                                ,isnull(tab1.总数,0)总数 
                                ,isnull(tab2.合格数,0)合格数
                                --,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数
                                --,convert(dec(18,2),case when tab2.合格数 is null then tab2.合格数 else tab2.合格数*100.0/tab1.总数 end) 合格率
                                ,isnull(tab7.平均完整度 ,0)平均完整度
                                ,isnull(tab9.转入档案数,0)转入档案数
                                ,isnull(tab8.转出档案数,0)转出档案数
                                ,isnull(tab3.未录入有效证件数,0)未录入有效证件数
                                ,isnull(tab5.城镇档案数,0)城镇档案数
                                --,isnull(tab1.总数,0) - isnull(tab5.城镇档案数,0) 农村档案数
                                ,'0' 城镇档案率
                                ,'0' 农村档案率
                                ,isnull(tab4.更新数,0)更新数 
                                ,isnull(tab6.非活动档案数,0)非活动档案数
                            from(
                                select  region.机构编号 as rgid,region.机构名称,a.总数,people.农村人口数,people.城镇人口数 
                                from tb_机构信息 region 
                                left join(
                                    select 机构编号, case ISNULL(农村人口数,'') when '' then '0' else 农村人口数 end 农村人口数,
                                    case ISNULL(城镇人口数,'') when '' then '0' else 城镇人口数 end 城镇人口数
                                    from tb_地区人口 
                                ) as people on people.机构编号 = region.机构编号   
                                left join (
                                    select  jkjc.所属机构, count(jkjc.所属机构) as 总数 
          
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1'  
                                        and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)				
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) 
                                         like @机构 +'%'  )
                                        or jkjc.所属机构 like @机构 +'%'  )
                                        group by jkjc.所属机构 
                                    ) a on region.机构编号=a.所属机构 
                                    where 
                                        (region.状态 = '' or region.状态 is null) 
                                        and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
                                        or region.机构编号 like @机构 +'%')		
                                     ) as tab1 
                                left join (
                                    select   jkjc.所属机构, count(jkjc.所属机构) as 合格数 
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1' 
                                                and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                        or jkjc.所属机构 like @机构 +'%')  
                                        and cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3)) >=@完整度
                                        group by jkjc.所属机构					
                                ) as tab2 on tab1.rgid = tab2.所属机构 
                                left join (
                                    select 
                                        jkjc.所属机构, count(jkjc.所属机构) as 未录入有效证件数 				
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1' 
                                        and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)				
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                        or jkjc.所属机构 like @机构 +'%')  
                                        and ((jkjc.身份证号='' or jkjc.身份证号 is null) and (jkjc.D_ZJHQT='' or jkjc.D_ZJHQT is null)) 
                                    group by jkjc.所属机构 		
                                    ) as tab3 on tab1.rgid = tab3.所属机构 
                                  left join (
                                        select  jkjc.所属机构, count(jkjc.所属机构) as 城镇档案数 
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)					
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            and (jkjc.档案类别='' or jkjc.档案类别 is null or jkjc.档案类别 = '1') 
                                        group by jkjc.所属机构 			
                                    ) as tab5 on tab1.rgid = tab5.所属机构 
                                    left join (
                                        select   jkjc.所属机构 , count(jkjc.所属机构) as 更新数 
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <=  (@DateTo)	
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            and jkjc.创建时间!=jkjc.修改时间 
                                        group by jkjc.所属机构 			
                                    ) as tab4 on tab1.rgid=tab4.所属机构 
                                    left join (
                                        select  
                                            jkjc.所属机构 , count(jkjc.所属机构) as 非活动档案数  
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态 = '2' 
                                            and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                                            or jkjc.所属机构 like @机构 +'%') 
                                            group by jkjc.所属机构 
                                    ) as tab6 on tab1.rgid = tab6.所属机构
                                    left join (
                                        select 
                                            jkjc.所属机构 , 
                                            AVG(cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3))) 平均完整度
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            group by jkjc.所属机构 
                                    ) as tab7 on tab1.rgid = tab7.所属机构
                                    left join (
                                        select 
                                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
                                        from  tb_转档申请  jkjc  , tb_健康档案  da 
                                        where da.档案状态='1' 
                                            and jkjc.个人档案编号 = da.个人档案编号 
                                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
                                            or jkjc.转出机构 like @机构 +'%')  
                                            group by jkjc.转出机构 
                                    ) as tab8 on tab1.rgid = tab8.转出机构
                                    left join (
                                        select 
                                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
                                        from  tb_转档申请  jkjc  , tb_健康档案  da 
                                        where da.档案状态='1'  
                                            and jkjc.个人档案编号 = da.个人档案编号 
                                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
                                            or jkjc.转入机构 like @机构 +'%')  
                                            group by jkjc.转入机构 
                                    ) as tab9 on tab1.rgid = tab9.转入机构
                                    --order by tab1.rgid 
		                          ) a 
		                          group by a.区域编码
	                        ) a left join  tb_机构信息   c
                                            on c.机构编号 = a.所属上级机构
                            where a.所属上级机构 <> '371323'
	                        group by a.所属上级机构	 ,c. 机构名称
		                          order by 所属上级机构";
                #endregion

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "2")
            {

                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.创建时间 >= (@DateFrom)
                                        and jkjc.创建时间 <= (@DateTo)				
                                        and (
		(len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		or(jkjc.所属机构 = @机构)
 ) ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.创建时间 >= (@DateFrom)
                                        and jkjc.创建时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jkjc.完整度,'')  
                                        when '' then '0' else jkjc.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.创建时间 >= (@DateFrom)
                                        and jkjc.创建时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
                                 )
                                        and cast(case ISNULL(jkjc.完整度,'')  
                                        when '' then '0' else jkjc.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            #endregion
            return ds;
        }
        public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region 代码

                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.城镇人口数)as 城镇人口数,
	sum(a.农村人口数)as 农村人口数,
	sum(a.城镇人口数)+sum(a.农村人口数) as 总人口数,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    sum(a.未录入有效证件数)as 未录入有效证件数,
    sum(a.城镇档案数)as 城镇档案数,
    sum(a.总数)  - sum(a.城镇档案数)  as 农村档案数,
    case when sum(a.总数) = 0 then 0 else  sum(a.城镇档案数)*100.0/sum(a.总数) end as 城镇档案率,
    case when sum(a.总数) = 0 then 0 else ( sum(a.总数)-sum(a.城镇档案数))*100.0/sum(a.总数) end as 农村档案率,
    case when (sum(a.总数 )=0 or (sum(a.城镇人口数)+sum(a.农村人口数))=0) then 0 else sum(a.总数)*100.0/(sum(a.城镇人口数)+sum(a.农村人口数)) end as 健康管理率,
    sum(a.更新数) as 更新数 ,
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.城镇人口数)城镇人口数,
	sum(a.农村人口数)农村人口数,
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    sum(a.未录入有效证件数)未录入有效证件数,
    sum(a.城镇档案数)城镇档案数,
    sum(a.更新数) as 更新数 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
    from(
select  --*
        tab1.rgid 区域编码
        ,tab1.机构名称 区域名称
        ,convert(int,isnull(tab1.城镇人口数,0))城镇人口数
        ,convert(int,isnull(tab1.农村人口数,0))农村人口数
        --,convert(int,tab1.城镇人口数)+convert(int,tab1.农村人口数) 总人口数
        ,isnull(tab1.总数,0)总数 
        ,isnull(tab2.合格数,0)合格数
        --,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数
        --,convert(dec(18,2),case when tab2.合格数 is null then tab2.合格数 else tab2.合格数*100.0/tab1.总数 end) 合格率
        ,isnull(tab7.平均完整度 ,0)平均完整度
        ,isnull(tab9.转入档案数,0)转入档案数
        ,isnull(tab8.转出档案数,0)转出档案数
        ,isnull(tab3.未录入有效证件数,0)未录入有效证件数
        ,isnull(tab5.城镇档案数,0)城镇档案数
        --,isnull(tab1.总数,0) - isnull(tab5.城镇档案数,0) 农村档案数
        ,'0' 城镇档案率
        ,'0' 农村档案率
        ,isnull(tab4.更新数,0)更新数 
        ,isnull(tab6.非活动档案数,0)非活动档案数
    from(
        select  region.机构编号 as rgid,region.机构名称,a.总数,people.农村人口数,people.城镇人口数 
        from tb_机构信息 region 
        left join(
            select 机构编号, case ISNULL(农村人口数,'') when '' then '0' else 农村人口数 end 农村人口数,
            case ISNULL(城镇人口数,'') when '' then '0' else 城镇人口数 end 城镇人口数
            from tb_地区人口 
        ) as people on people.机构编号 = region.机构编号   
        left join (
            select  jkjc.所属机构, count(jkjc.所属机构) as 总数 
          
            from  tb_健康档案  jkjc  
            where jkjc.档案状态='1'  
                and jkjc.调查时间 >= (@DateFrom) and jkjc.调查时间 <= (@DateTo)				
                and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) 
                 like @机构 +'%'  )
                or jkjc.所属机构 like @机构 +'%'  )
                group by jkjc.所属机构 
            ) a on region.机构编号=a.所属机构 
            where 
                (region.状态 = '' or region.状态 is null) 
                and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
                or region.机构编号 like @机构 +'%')		
             ) as tab1 
        left join (
            select   jkjc.所属机构, count(jkjc.所属机构) as 合格数 
            from  tb_健康档案  jkjc  
            where jkjc.档案状态='1' 
                        and jkjc.调查时间 >= (@DateFrom)  and jkjc.调查时间 <= (@DateTo)			
                and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                or jkjc.所属机构 like @机构 +'%')  
                and cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3)) >=@完整度
                group by jkjc.所属机构					
        ) as tab2 on tab1.rgid = tab2.所属机构 
        left join (
            select 
                jkjc.所属机构, count(jkjc.所属机构) as 未录入有效证件数 				
            from  tb_健康档案  jkjc  
            where jkjc.档案状态='1' 
                and jkjc.调查时间 >= (@DateFrom) and jkjc.调查时间 <= (@DateTo)				
                and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                or jkjc.所属机构 like @机构 +'%')  
                and ((jkjc.身份证号='' or jkjc.身份证号 is null) and (jkjc.D_ZJHQT='' or jkjc.D_ZJHQT is null)) 
            group by jkjc.所属机构 		
            ) as tab3 on tab1.rgid = tab3.所属机构 
          left join (
                select  jkjc.所属机构, count(jkjc.所属机构) as 城镇档案数 
                from  tb_健康档案  jkjc  
                where jkjc.档案状态='1' 
                    and jkjc.调查时间 >= (@DateFrom) and jkjc.调查时间 <= (@DateTo)					
                    and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                    or jkjc.所属机构 like @机构 +'%')  
                    and (jkjc.档案类别='' or jkjc.档案类别 is null or jkjc.档案类别 = '1') 
                group by jkjc.所属机构 			
            ) as tab5 on tab1.rgid = tab5.所属机构 
            left join (
                select   jkjc.所属机构 , count(jkjc.所属机构) as 更新数 
                from  tb_健康档案  jkjc  
                where jkjc.档案状态='1' 
                    and jkjc.调查时间 >= (@DateFrom) and jkjc.调查时间 <=  (@DateTo)	
                    and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                    or jkjc.所属机构 like @机构 +'%')  
                    and jkjc.创建时间!=jkjc.修改时间 
                group by jkjc.所属机构 			
            ) as tab4 on tab1.rgid=tab4.所属机构 
            left join (
                select  
                    jkjc.所属机构 , count(jkjc.所属机构) as 非活动档案数  
                from  tb_健康档案  jkjc  
                where jkjc.档案状态 = '2' 
                    and jkjc.调查时间 >= (@DateFrom)  and jkjc.调查时间 <= (@DateTo)			
                    and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                    or jkjc.所属机构 like @机构 +'%') 
                    group by jkjc.所属机构 
            ) as tab6 on tab1.rgid = tab6.所属机构
            left join (
                select 
                    jkjc.所属机构 , 
                    AVG(cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3))) 平均完整度
                from  tb_健康档案  jkjc  
                where jkjc.档案状态='1' 
                    and jkjc.调查时间 >= (@DateFrom)  and jkjc.调查时间 <= (@DateTo)			
                    and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                    or jkjc.所属机构 like @机构 +'%')  
                    group by jkjc.所属机构 
            ) as tab7 on tab1.rgid = tab7.所属机构
            left join (
                select 
                    jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
                from  tb_转档申请  jkjc  , tb_健康档案  da 
                where da.档案状态='1' 
                    and jkjc.个人档案编号 = da.个人档案编号 
                    and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
                    or jkjc.转出机构 like @机构 +'%')  
                    group by jkjc.转出机构 
            ) as tab8 on tab1.rgid = tab8.转出机构
            left join (
                select 
                    jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
                from  tb_转档申请  jkjc  , tb_健康档案  da 
                where da.档案状态='1'  
                    and jkjc.个人档案编号 = da.个人档案编号 
                    and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
                    or jkjc.转入机构 like @机构 +'%')  
                    group by jkjc.转入机构 
            ) as tab9 on tab1.rgid = tab9.转入机构
            --order by tab1.rgid 
		  ) a 
		  group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
                    where a.所属上级机构 <> '371323'
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 ";

                #endregion
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "2")
            {

                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.调查时间 >= (@DateFrom)
                                        and jkjc.调查时间 <= (@DateTo)				
                                     and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.调查时间 >= (@DateFrom)
                                        and jkjc.调查时间 <= (@DateTo)				
                                       and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jkjc.完整度,'')  
                                        when '' then '0' else jkjc.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        and jkjc.调查时间 >= (@DateFrom)
                                        and jkjc.调查时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jkjc.完整度,'')  
                                        when '' then '0' else jkjc.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            #endregion
            return ds;
        }
        /// <summary>
        /// 判断身份证号是否已经注册
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public override bool CheckNoExists(string keyValue)
        {
            string sql = "SELECT COUNT(*) C FROM " + tb_健康档案.__TableName + " WHERE 身份证号 =@KEY";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, keyValue);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        ///根据身份证号查询用户信息 
        /// </summary>
        /// <param name="id">身份证号</param>
        /// <returns></returns>
        public DataTable GetBusinessByID(string id)
        {
            string sql = "SELECT * FROM " + tb_健康档案.__TableName + " WHERE 身份证号 =@KEY";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, id);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康档案.__TableName);
        }
        /// <summary>
        /// 根据个人档案编号查询 与户主关系
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <returns></returns>
        public string GetYHZGX(string docNo)
        {
            string yhzgx = string.Empty;
            string sql = "select 与户主关系 from tb_健康档案 where 个人档案编号 = @KEY";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, docNo);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return ConvertEx.ToString(o);
        }

        /// <summary>
        /// 获取个人基本信息数据
        /// </summary>
        /// <param name="_docNo"></param>
        /// <returns></returns>
        public DataSet GetInfoByXinxi(string _docNo)
        {
            string sql1 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2 "; //子表
            string sql3 = " select * from [tb_健康档案_既往病史]   where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo3 "; //子表
            string sql4 = " select * from [tb_健康档案_家族病史]   where [" + tb_健康档案_家族病史.个人档案编号 + "]=@DocNo4 "; //子表
            string sql5 = " select * from [tb_健康档案_健康状态]   where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo5 "; //子表
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;
            ds.Tables[2].TableName = tb_健康档案_既往病史.__TableName;
            ds.Tables[3].TableName = tb_健康档案_家族病史.__TableName;
            ds.Tables[4].TableName = tb_健康档案_健康状态.__TableName;
            return ds;
        }
		
		public string GetAddressByDAH(string dah)
        {
            string yhzgx = string.Empty;
            string sql = @"SELECT BB.地区名称 +
	  CC.地区名称+
	  DD.地区名称+
	  EE.地区名称 +' '+ISNULL([居住地址],'')
  FROM [dbo].[tb_健康档案] AA
  LEFT OUTER JOIN dbo.tb_地区档案 BB ON AA.[省] = BB.地区编码
  LEFT OUTER JOIN dbo.tb_地区档案 CC ON AA.[市] = CC.地区编码
  LEFT OUTER JOIN dbo.tb_地区档案 DD ON AA.[区] = DD.地区编码
  LEFT OUTER JOIN dbo.tb_地区档案 EE ON AA.[街道] = EE.地区编码
  where 个人档案编号=@dah ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return ConvertEx.ToString(o);
        }

        /// <summary>
        /// 档案管理率获取
        /// </summary>
        /// <param name="DocNo机构"></param>
        /// <param name="DocNo完整度"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public DataSet GetReportDataOfGLL(string DocNo机构, string DocNo完整度, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region MyRegion
                sql = @"select 
	                        所属上级机构 as 区域编码,
	                        c.机构名称 as 区域名称,
	                        sum(a.总数) as 总数,
	                        sum(a.合格数) as 合格数 ,
	                        sum(a.总数)-sum(a.合格数) as 不合格数,
	                        case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	                        avg(a.平均完整度) 平均完整度 ,
	                        sum(a.城镇人口数)as 城镇人口数,
	                        sum(a.农村人口数)as 农村人口数,
	                        sum(a.城镇人口数)+sum(a.农村人口数) as 总人口数,
	                        sum(a.转入档案数)as 转入档案数,
	                        sum(a.转出档案数)as 转出档案数,
                            sum(a.未录入有效证件数)as 未录入有效证件数,
                            sum(a.城镇档案数)as 城镇档案数,
                            sum(a.总数)  - sum(a.城镇档案数)  as 农村档案数,
                            --  case when sum(a.城镇人口数)=0 then sum(a.城镇档案数) else sum(a.城镇人口数) end as 城镇人口数,    
                            case when sum(a.总数) = 0 then 0 else sum(a.城镇档案数)*100.0/sum(a.总数) end as 城镇档案率,
                            case when sum(a.总数) = 0 then 0 else ( sum(a.总数)-sum(a.城镇档案数))*100.0/sum(a.农村人口数) end as 农村档案率,
                            case when sum(a.总数)=0 then 0 else sum(a.总数)*100.0/(sum(a.城镇人口数)+sum(a.农村人口数)) end as 健康管理率,
                            sum(a.更新数) as 更新数 ,
                            sum(a.非活动档案数) as 非活动档案数
	
                         from (
                        select
	                        sum(a.城镇人口数)城镇人口数,
	                        sum(a.农村人口数)农村人口数,
	                        sum(a.总数) 总数,
	                        sum(a.合格数) 合格数 ,
	                        avg(a.平均完整度) 平均完整度 ,
	                        sum(a.转入档案数)转入档案数,
                            sum(a.转出档案数)转出档案数,
                            sum(a.未录入有效证件数)未录入有效证件数,
                            sum(a.城镇档案数)城镇档案数,
                            sum(a.更新数) as 更新数 ,
                            sum(a.非活动档案数) as 非活动档案数,
	                        case when   (len(@机构) = 6 and len(a.区域编码) >6) then substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		                        else a.区域编码 end as 所属上级机构
                            from(
                        select  --*
                                tab1.rgid 区域编码
                                ,tab1.机构名称 区域名称
                                ,convert(int,isnull(tab1.城镇人口数,0))城镇人口数
                                ,convert(int,isnull(tab1.农村人口数,0))农村人口数
                                --,convert(int,tab1.城镇人口数)+convert(int,tab1.农村人口数) 总人口数
                                ,isnull(tab1.总数,0)总数 
                                ,isnull(tab2.合格数,0)合格数
                                --,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数
                                --,convert(dec(18,2),case when tab2.合格数 is null then tab2.合格数 else tab2.合格数*100.0/tab1.总数 end) 合格率
                                ,isnull(tab7.平均完整度 ,0)平均完整度
                                ,isnull(tab9.转入档案数,0)转入档案数
                                ,isnull(tab8.转出档案数,0)转出档案数
                                ,isnull(tab3.未录入有效证件数,0)未录入有效证件数
                                ,isnull(tab5.城镇档案数,0)城镇档案数
                                --,isnull(tab1.总数,0) - isnull(tab5.城镇档案数,0) 农村档案数
                                ,'0' 城镇档案率
                                ,'0' 农村档案率
                                ,isnull(tab4.更新数,0)更新数 
                                ,isnull(tab6.非活动档案数,0)非活动档案数
                            from(
                                select  region.机构编号 as rgid,region.机构名称,a.总数,people.农村人口数,people.城镇人口数 
                                from tb_机构信息 region 
                                left join(
                                    select 机构编号, case ISNULL(农村人口数,'') when '' then '0' else 农村人口数 end 农村人口数,
                                    case ISNULL(城镇人口数,'') when '' then '0' else 城镇人口数 end 城镇人口数
                                    from tb_地区人口 
                                ) as people on people.机构编号 = region.机构编号   
                                left join (
                                    select  jkjc.所属机构, count(jkjc.所属机构) as 总数 
          
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1'  
                                        --and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)				
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) 
                                         like @机构 +'%'  )
                                        or jkjc.所属机构 like @机构 +'%'  )
                                        group by jkjc.所属机构 
                                    ) a on region.机构编号=a.所属机构 
                                    where 
                                        (region.状态 = '' or region.状态 is null) 
                                        and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
                                        or region.机构编号 like @机构 +'%')		
                                     ) as tab1 
                                left join (
                                    select   jkjc.所属机构, count(jkjc.所属机构) as 合格数 
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1' 
                                        --and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                        or jkjc.所属机构 like @机构 +'%')  
                                        and cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3)) >=@完整度
                                        group by jkjc.所属机构					
                                ) as tab2 on tab1.rgid = tab2.所属机构 
                                left join (
                                    select 
                                        jkjc.所属机构, count(jkjc.所属机构) as 未录入有效证件数 				
                                    from  tb_健康档案  jkjc  
                                    where jkjc.档案状态='1' 
                                        --and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)				
                                        and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                        or jkjc.所属机构 like @机构 +'%')  
                                        and ((jkjc.身份证号='' or jkjc.身份证号 is null) and (jkjc.D_ZJHQT='' or jkjc.D_ZJHQT is null)) 
                                    group by jkjc.所属机构 		
                                    ) as tab3 on tab1.rgid = tab3.所属机构 
                                  left join (
                                        select  jkjc.所属机构, count(jkjc.所属机构) as 城镇档案数 
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            --and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <= (@DateTo)					
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            and (jkjc.档案类别='' or jkjc.档案类别 is null or jkjc.档案类别 = '1') 
                                        group by jkjc.所属机构 			
                                    ) as tab5 on tab1.rgid = tab5.所属机构 
                                    left join (
                                        select   jkjc.所属机构 , count(jkjc.所属机构) as 更新数 
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            --and jkjc.创建时间 >= (@DateFrom) and jkjc.创建时间 <=  (@DateTo)	
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            --and jkjc.创建时间!=jkjc.修改时间 
                                        group by jkjc.所属机构 			
                                    ) as tab4 on tab1.rgid=tab4.所属机构 
                                    left join (
                                        select  
                                            jkjc.所属机构 , count(jkjc.所属机构) as 非活动档案数  
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态 = '2' 
                                            --and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                                            or jkjc.所属机构 like @机构 +'%') 
                                            group by jkjc.所属机构 
                                    ) as tab6 on tab1.rgid = tab6.所属机构
                                    left join (
                                        select 
                                            jkjc.所属机构 , 
                                            AVG(cast(case ISNULL(jkjc.完整度,'') when '' then '0' else jkjc.完整度 end as decimal(8,3))) 平均完整度
                                        from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1' 
                                            --and jkjc.创建时间 >= (@DateFrom)  and jkjc.创建时间 <= (@DateTo)			
                                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%'  ) 
                                            or jkjc.所属机构 like @机构 +'%')  
                                            group by jkjc.所属机构 
                                    ) as tab7 on tab1.rgid = tab7.所属机构
                                    left join (
                                        select 
                                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
                                        from  tb_转档申请  jkjc  , tb_健康档案  da 
                                        where da.档案状态='1' 
                                            and jkjc.个人档案编号 = da.个人档案编号 
                                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
                                            or jkjc.转出机构 like @机构 +'%')  
                                            group by jkjc.转出机构 
                                    ) as tab8 on tab1.rgid = tab8.转出机构
                                    left join (
                                        select 
                                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
                                        from  tb_转档申请  jkjc  , tb_健康档案  da 
                                        where da.档案状态='1'  
                                            and jkjc.个人档案编号 = da.个人档案编号 
                                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
                                            or jkjc.转入机构 like @机构 +'%')  
                                            group by jkjc.转入机构 
                                    ) as tab9 on tab1.rgid = tab9.转入机构
                                    --order by tab1.rgid 
		                          ) a 
		                          group by a.区域编码
	                        ) a left join  tb_机构信息   c
                                            on c.机构编号 = a.所属上级机构
                            where a.所属上级机构 <> '371323'
	                        group by a.所属上级机构	 ,c. 机构名称
		                          order by 所属上级机构";
                #endregion

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                //cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                //cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            else if (Type == "2")
            {

                sql = @"select * from  tb_健康档案  jkjc  
                                        where jkjc.档案状态='1'  
                                        --and jkjc.创建时间 >= (@DateFrom)
                                        --and jkjc.创建时间 <= (@DateTo)				
                                        and (
		(len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		or(jkjc.所属机构 = @机构)
 ) ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                //cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                //cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#个人基本信息统计";
            }
            #endregion
            return ds;
        }

        //Begin WXF 2018-11-01 | 17:05
        //新增既往史疾病查询,用于档案分类判断 
        public System.Data.DataSet Get_基本信息And既往史疾病And健康状态(string grdah)
        {
            string sql1 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo1";      //子表
            string sql2 = " select * from [tb_健康档案_既往病史]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo2 and 疾病类型='疾病'";      //子表
            string sql3 = " select * from [tb_健康档案_健康状态]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo3";      //子表

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);

            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, grdah.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

            ds.Tables[0].TableName = tb_健康档案.__TableName;
            ds.Tables[1].TableName = tb_健康档案_既往病史.__TableName;
            ds.Tables[2].TableName = tb_健康档案_健康状态.__TableName;

            return ds;
        }
        //End


        //Begin WXF 2018-11-04 | 00:30
        //获取居民健康档案封面信息 
        public DataSet Get_健康档案封面(string str_recordNum)
        {
            string sql1 = @"select 个人档案编号
                           ,姓名
                           ,(select P_DESC from tb_常用字典 where P_FUN_CODE='xb_xingbie' and P_CODE=性别) as 性别
                           ,身份证号
                           ,出生日期 
                           ,(select 地区名称 from tb_地区档案 where 地区编码=省) as 省
                           ,(select 地区名称 from tb_地区档案 where 地区编码=市) as 市
                           ,(select 地区名称 from tb_地区档案 where 地区编码=区) as 区
                           ,(select 地区名称 from tb_地区档案 where 地区编码=街道) as 街道
                           ,(select 地区名称 from tb_地区档案 where 地区编码=居委会) as 居委会
                           ,居住地址
                           ,本人电话
                           ,(select 机构名称 from tb_机构信息 where 机构编号=所属机构) as 所属机构
                           ,(select 机构名称 from tb_机构信息 where 机构编号=创建机构) as 创建机构
                           ,(select UserName from tb_MyUser where 用户编码=isnull(修改人,创建人)) as 创建人
                           ,CONVERT(date,创建时间) as 创建时间
                           from tb_健康档案 where 个人档案编号=@DocNo1
                           ";

            string sql2 = @"select top 1 FIELD2,体检日期 from tb_健康体检 where 个人档案编号=@DocNo2 order by 体检日期 desc ";
            string sql3 = @"select top 1 图像 from tb_健康体检_图像采集 where 个人档案编号=@DocNo2 order by ID desc ";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, str_recordNum.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, str_recordNum.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

            ds.Tables[0].TableName = tb_健康档案.__TableName;
            ds.Tables[1].TableName = tb_健康体检.__TableName;
            ds.Tables[2].TableName = "tb_健康体检_图像采集";

            return ds;
        }
        //End

        public bool Is孕产妇(string dah)
        {
            string sql = "  select count(1) from dbo.tb_健康档案 where 个人档案编号=@dah and ([怀孕情况]='已孕未生产' or [怀孕情况]='已生产随访期内'or [怀孕情况]='已生产(随访期内)')";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah.Trim());
            object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return obj.ToString() == "0" ? false : true;
        }

        public DataSet Get重点人群档案号ByDah(List<string> dahlist)
        {
            StringBuilder sbWhere = new StringBuilder();

            for (int index = 0; index < dahlist.Count; index++)
            {
                sbWhere.Append("@dah" + index.ToString() + ",");
            }

            string strwhere = sbWhere.ToString().Trim(',');

            string sql = @"
SELECT distinct 个人档案编号 FROM [dbo].[tb_健康档案_既往病史] WHERE 个人档案编号 in (" + strwhere + @") and 疾病名称='9' 
select distinct 个人档案编号 from tb_MXB高血压管理卡 where 个人档案编号 in (" + strwhere + @") and  isnull(tb_MXB高血压管理卡.终止管理,'')<>'1'
select distinct 个人档案编号 from tb_MXB糖尿病管理卡 where 个人档案编号 in (" + strwhere + @") and isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1'
select distinct 个人档案编号 from tb_MXB冠心病管理卡 where 个人档案编号 in (" + strwhere + @") and isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1'
select distinct 个人档案编号 from tb_MXB脑卒中管理卡 where 个人档案编号 in (" + strwhere + @") and isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1'
select distinct 个人档案编号 from tb_精神疾病信息补充表";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);

            for (int index = 0; index < dahlist.Count; index++)
            {
                cmd.AddParam("@dah" + index.ToString(), SqlDbType.VarChar, dahlist[index]);
            }

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = "tb_健康档案_既往病史";
            ds.Tables[1].TableName = "tb_MXB高血压管理卡";
            ds.Tables[2].TableName = "tb_MXB糖尿病管理卡";
            ds.Tables[3].TableName = "tb_MXB冠心病管理卡";
            ds.Tables[4].TableName = "tb_MXB脑卒中管理卡";
            ds.Tables[5].TableName = "tb_精神疾病信息补充表";
            return ds;
        }

    }
}

