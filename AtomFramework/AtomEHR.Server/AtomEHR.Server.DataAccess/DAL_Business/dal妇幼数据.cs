﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 妇幼数据的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/21 11:45:47
 *   最后修改: 2015/09/21 11:45:47
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal妇幼数据
    /// </summary>
    public class dal妇幼数据 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dal妇幼数据(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_妇幼数据.__KeyName; //主表的主键字段
             _SummaryTableName = tb_妇幼数据.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_妇幼数据.__TableName) ORM = typeof(tb_妇幼数据);
             //if (tableName == tb_妇幼数据s.__TableName) ORM = typeof(tb_妇幼数据s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_妇幼数据.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_妇幼数据.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_妇幼数据.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_妇幼数据]    where ["+tb_妇幼数据.__KeyName+"]=@DocNo1 ";
              string sql2 = " select * from [tb_妇幼数据s]   where ["+tb_妇幼数据.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_妇幼数据.__TableName;
              //ds.Tables[1].TableName =tb_妇幼数据s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_妇幼数据s] where ["+tb_妇幼数据.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_妇幼数据] where ["+tb_妇幼数据.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

        /// <summary>
        /// 删除妇幼数据
        /// </summary>
        /// <param name="biaodannum"></param>
        /// <param name="csrq"></param>
        /// <param name="lrr"></param>
        /// <param name="lrsj"></param>
        /// <param name="name"></param>
        /// <param name="sfzh"></param>
        /// <param name="sjly"></param>
        /// <param name="jcrgid"></param>
        /// <returns></returns>
        public bool Delete(string biaodannum, string csrq, string lrr, string lrsj, 
                            string name, string sfzh, string sjly, string jcrgid)
        {
            string sql = @"delete from [tb_妇幼数据] where " + tb_妇幼数据.表单号 + @"=@biaodanhao
	   		and "+tb_妇幼数据.出生日期+@" = @csrq
	   		and "+tb_妇幼数据.录入人+@" = @lrr
	   		and "+tb_妇幼数据.录入时间+@" = @lrsj
	   		and "+tb_妇幼数据.姓名+@" = @name
	   		and "+tb_妇幼数据.身份证号+@" = @sfzh
	   		and "+tb_妇幼数据.SJLY+@" = @sjly
	   		and "+tb_妇幼数据.JCRGID+" = @jcrgid";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@biaodanhao", SqlDbType.VarChar, biaodannum.Trim());
            cmd.AddParam("@csrq",SqlDbType.VarChar, csrq);
            cmd.AddParam("@lrr", SqlDbType.VarChar, lrr);
            cmd.AddParam("@lrsj", SqlDbType.VarChar, lrsj);
            cmd.AddParam("@name", SqlDbType.VarChar, name);
            cmd.AddParam("@sfzh", SqlDbType.VarChar, sfzh);
            cmd.AddParam("@sjly", SqlDbType.VarChar, sjly);
            cmd.AddParam("@jcrgid", SqlDbType.VarChar, jcrgid);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "妇幼数据");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          /// <summary>
          /// 需要先插入儿童基本信息，然后在调用此方法,注意使用默认值的几个参数
          /// </summary>
          /// <param name="卡号"></param>
          /// <param name="姓名"></param>
          /// <param name="性别"></param>
          /// <param name="出生日期"></param>
          /// <param name="身份证号"></param>
          /// <param name="身份">使用默认值“儿童”</param>
          /// <param name="YUNCC"></param>
          /// <param name="表单号">表单号使用默认值“e0”</param>
          /// <param name="母亲姓名"></param>
          /// <param name="母亲身份证号"></param>
          /// <param name="JCRGID"></param>
          /// <param name="JCRGNAME"></param>
          /// <param name="FCYYRGID"></param>
          /// <param name="录入人"></param>
          /// <param name="录入时间"></param>
          /// <param name="SJTQFSQK">使用默认值“0”</param>
          /// <param name="SJLY">使用默认值“zhonglian”</param>
          public bool InsertData(string 卡号, string 姓名, string 性别, string 出生日期, string 身份证号,  string 母亲姓名, 
                                 string 母亲身份证号, string JCRGID, string JCRGNAME, string FCYYRGID, string 录入人, string 录入时间
                                // ,string 身份,string YUNCC, string 表单号,string SJTQFSQK, string SJLY
                                )
          {
              string sql = @"INSERT INTO [dbo].[tb_妇幼数据]([卡号],[姓名],[性别],[出生日期],[身份证号],[身份],
            [YUNCC],[表单号],[母亲姓名],[母亲身份证号],[JCRGID],[JCRGNAME],[FCYYRGID],[录入人],[录入时间],[SJTQFSQK],[SJLY])
            VALUES(@卡号,@姓名,@性别,@出生日期,@身份证号,@身份,@YUNCC,@表单号,@母亲姓名,@母亲身份证号,
                    @JCRGID,@JCRGNAME,@FCYYRGID,@录入人,@录入时间,@SJTQFSQK,@SJLY)";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@卡号", SqlDbType.VarChar, 卡号.Trim());
              cmd.AddParam("@姓名", SqlDbType.VarChar, 姓名.Trim());
              cmd.AddParam("@性别", SqlDbType.VarChar, 性别.Trim());
              cmd.AddParam("@出生日期", SqlDbType.VarChar, 出生日期.Trim());
              cmd.AddParam("@身份证号", SqlDbType.VarChar, 身份证号.Trim());
              cmd.AddParam("@身份", SqlDbType.VarChar, "儿童");
              cmd.AddParam("@YUNCC", SqlDbType.VarChar, "");
              cmd.AddParam("@表单号", SqlDbType.VarChar, "e0");
              cmd.AddParam("@母亲姓名", SqlDbType.VarChar, 母亲姓名.Trim());
              cmd.AddParam("@母亲身份证号", SqlDbType.VarChar, 母亲身份证号.Trim());
              cmd.AddParam("@JCRGID", SqlDbType.VarChar, JCRGID.Trim());
              cmd.AddParam("@JCRGNAME", SqlDbType.VarChar, JCRGNAME.Trim());
              cmd.AddParam("@FCYYRGID", SqlDbType.VarChar, FCYYRGID.Trim());
              cmd.AddParam("@录入人", SqlDbType.VarChar, 录入人.Trim());
              cmd.AddParam("@录入时间", SqlDbType.VarChar, 录入时间.Trim());
              cmd.AddParam("@SJTQFSQK", SqlDbType.VarChar, "0");
              cmd.AddParam("@SJLY", SqlDbType.VarChar, "zhonglian");              
              int i = DataProvider.Instance.ExecuteSQL(_Loginer.DBName, cmd.SqlCommand);

              return i != 0;

          }

     }
}

