﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 儿童基本信息的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/02 01:27:45
 *   最后修改: 2015/09/02 01:27:45
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal儿童基本信息
    /// </summary>
    public class dal儿童基本信息 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal儿童基本信息(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_儿童基本信息.__KeyName; //主表的主键字段
            _SummaryTableName = tb_儿童基本信息.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_儿童基本信息.__TableName)
            {
                ORM = typeof(tb_儿童基本信息);
            }
            else if (tableName == tb_健康档案_个人健康特征.__TableName)
            {
                ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            }

            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_儿童基本信息.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_儿童基本信息.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_儿童基本信息.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                //return null;
            }
        }


        /// <summary>
        ///查询数据
        /// </summary>
        public DataTable GetSummaryByParam(string str机构编码, bool b含下属机构, string str姓名, string str性别,
                                           string str出生日期开始, string str出生日期截止, string str档案状态, string str录入人,
                                           string str录入时间Begin, string str录入时间End, string str档案编号, string str身份证号,
                                           string str是否合格,
                                           string str镇, string str村, string str村地址)
        {
            StringBuilder sql = new StringBuilder();

            sql.Append(@"SELECT aa.ID,个人档案编号,姓名,家庭档案编号, ff.P_DESC 性别,出生日期,母亲姓名,母亲身份证号,
bb.地区名称+' '+cc.地区名称 +' '+居住地址  居住地址,
aa.联系电话,dd.机构名称 当前机构,ee.UserName 录入人,aa.创建日期,aa.所属机构
 FROM [tb_儿童基本信息] aa left outer join [tb_地区档案] bb on aa.街道 = bb.地区编码
 left outer join [tb_地区档案] cc on aa.居委会 = cc.地区编码 
 left outer join tb_机构信息 dd on aa.所属机构 = dd.机构编号
 left outer join tb_MyUser ee on aa.创建人 = ee.用户编码
 left outer join tb_常用字典 ff on aa.性别 = FF.P_CODE AND ff.P_FUN_CODE = 'xb_xingbie'
where 1=1 ");
            List<SqlParameter> paramList = new List<SqlParameter>();

            //所属机构
            if (!b含下属机构)
            {
                sql.Append(" and 所属机构 = @prgid ");
                paramList.Add(new SqlParameter("@prgid", str机构编码));
            }
            else if (b含下属机构 && str机构编码.Length != 12)
            {
                sql.Append(" and 所属机构 like @prgid+'%' ");
                paramList.Add(new SqlParameter("@prgid", str机构编码));
            }
            else if (b含下属机构 && str机构编码.Length == 12)
            {
                sql.Append(" and ( 所属机构=@prgid or substring(所属机构,1,7)+'1'+substring(所属机构,9,7) like @prgid+'%' ) ");
                paramList.Add(new SqlParameter("@prgid", str机构编码));
            }

            //姓名
            if (string.IsNullOrWhiteSpace(str姓名))
            { }
            else
            {
                sql.Append(" and (姓名 = @xm ) ");
                paramList.Add(new SqlParameter("@xm", str姓名));
                //paramList.Add(new SqlParameter("@xm2",DESEncrypt.DES加密(str姓名)));
            }

            //性别
            if (string.IsNullOrWhiteSpace(str性别))
            { }
            else
            {
                sql.Append(" and 性别=@xb ");
                paramList.Add(new SqlParameter("@xb", str性别));
            }

            //街道--镇
            if (string.IsNullOrWhiteSpace(str镇))
            { }
            else
            {
                sql.Append(" and 街道 = @jd ");
                paramList.Add(new SqlParameter("@jd", str镇));
            }

            //居委会
            if (string.IsNullOrWhiteSpace(str村))
            { }
            else
            {
                sql.Append(" and 居委会 = @jwh ");
                paramList.Add(new SqlParameter("@jwh", str村));
            }

            //居住地址
            if (string.IsNullOrWhiteSpace(str村地址))
            { }
            else
            {
                sql.Append(" and 居住地址 like @xxdz+'%' ");
                paramList.Add(new SqlParameter("@xxdz", str村地址));
            }

            //身份证号
            if (string.IsNullOrWhiteSpace(str身份证号))
            { }
            else
            {
                sql.Append(" and 身份证号 = @sfzh ");
                paramList.Add(new SqlParameter("@sfzh", str身份证号));
            }

            //个人档案编号
            if (string.IsNullOrWhiteSpace(str档案编号))
            { }
            else
            {
                sql.Append(" and 个人档案编号=@grdabh ");
                paramList.Add(new SqlParameter("@grdabh", str档案编号));
            }

            //录入人
            if (string.IsNullOrWhiteSpace(str录入人))
            { }
            else
            {
                sql.Append(" and aa.创建人=@cjr ");
                paramList.Add(new SqlParameter("@cjr", str录入人));
            }

            //档案状态
            if (string.IsNullOrWhiteSpace(str档案状态))
            { }
            else
            {
                sql.Append(" and 档案状态=@dazt");
                paramList.Add(new SqlParameter("@dazt", str档案状态));
            }

            //出生日期
            if (!(string.IsNullOrWhiteSpace(str出生日期开始)) && !(string.IsNullOrWhiteSpace(str出生日期截止)))
            {
                sql.Append(" and 出生日期 between @csrq1 and @csrq2 ");
                paramList.Add(new SqlParameter("@csrq1", str出生日期开始));
                paramList.Add(new SqlParameter("@csrq2", str出生日期截止));
            }
            else if (!(string.IsNullOrWhiteSpace(str出生日期开始)) && (string.IsNullOrWhiteSpace(str出生日期截止)))
            {
                sql.Append(" and 出生日期 >= @csrq1 ");
                paramList.Add(new SqlParameter("@csrq1", str出生日期开始));
            }
            else if ((string.IsNullOrWhiteSpace(str出生日期开始)) && !(string.IsNullOrWhiteSpace(str出生日期截止)))
            {
                sql.Append(" and 出生日期 <= @csrq2 ");
                paramList.Add(new SqlParameter("@csrq2", str出生日期截止));
            }

            //创建时间
            if (!(string.IsNullOrWhiteSpace(str录入时间Begin)) && !(string.IsNullOrWhiteSpace(str录入时间End)))
            {
                sql.Append(" and 创建日期 between @createtime1+' 00:00:00' and @createtime2+' 23:59:59' ");
                paramList.Add(new SqlParameter("@createtime1", str录入时间Begin));
                paramList.Add(new SqlParameter("@createtime2", str录入时间End));
            }
            else if (!(string.IsNullOrWhiteSpace(str录入时间Begin)) && (string.IsNullOrWhiteSpace(str录入时间End)))
            {
                sql.Append(" and 创建日期 >= @createtime1+' 00:00:00' ");
                paramList.Add(new SqlParameter("@createtime1", str录入时间Begin));
                //paramList.Add(new SqlParameter("@createtime2", str录入时间End));
            }
            else if ((string.IsNullOrWhiteSpace(str录入时间Begin)) && !(string.IsNullOrWhiteSpace(str录入时间End)))
            {
                sql.Append(" and 创建日期 <= @createtime2+' 23:59:59' ");
                //paramList.Add(new SqlParameter("@createtime1", str录入时间Begin));
                paramList.Add(new SqlParameter("@createtime2", str录入时间End));
            }

            //是否活动
            if (str是否合格 != null && str是否合格.Equals("1"))//是
            {
                sql.Append(" and 缺项='0' ");
            }
            else if (str是否合格 != null && str是否合格.Equals("2"))//否
            {
                sql.Append(" and (缺项!='0' or 缺项 is null) ");
            }

            sql.Append(" order by 创建日期 desc ");

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql.ToString());

            for (int index = 0; index < paramList.Count; index++)
            {
                cmd.AddParam(paramList[index].ParameterName, SqlDbType.VarChar, paramList[index].Value);
            }

            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_儿童基本信息.__TableName);

            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    for (int index = 0; index < dt.Rows.Count; index++)
            //    {
            //        dt.Rows[index]["姓名"] = DESEncrypt.DES解密(dt.Rows[index]["姓名"].ToString());
            //    }
            //}
            return dt;
        }


        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_儿童基本信息]    where [" + tb_儿童基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_儿童基本信息.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByID(string docNo)
        {
            string sql1 = " select aa.*, bb.[UserName] from [tb_儿童基本信息] aa left outer join tb_MyUser bb on aa.[创建人]=bb.[用户编码]  where [" + tb_儿童基本信息.ID + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_儿童基本信息s]   where ["+tb_儿童基本信息.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_儿童基本信息.__TableName;
            //ds.Tables[1].TableName =tb_儿童基本信息s.__TableName;//子表
            return ds;
        }

        /// <summary>
        ///获取儿童基本信息,用于显示
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet Get儿童基本信息ByKey(string docNo)
        {
            string sql1 = @" SELECT aa.ID,aa.卡号,aa.个人档案编号,aa.姓名,aa.身份证号,aa.缺项,aa.完整度,
ff.P_DESC 性别,--aa.性别
aa.出生日期,aa.联系电话,
 gg.地区名称+hh.地区名称+ii.地区名称+bb.地区名称+cc.地区名称+居住地址  居住地址,
 aa.父亲姓名,aa.父亲工作单位,aa.父亲联系电话,aa.父亲出生日期,
 aa.母亲姓名,aa.母亲工作单位,aa.母亲联系电话,aa.母亲出生日期,
 aa.邮政编码,aa.母亲身份证号,aa.建档单位,aa.建档单位电话,
 aa.托幼机构,aa.托幼机构电话,
 aa.HAPPENTIME,aa.创建日期,aa.修改日期,
 kk.机构名称 创建机构,--aa.创建机构,
ee.UserName 录入人,--aa.创建人
jj.UserName 修改人,--aa.修改人
dd.机构名称 当前机构,aa.所属机构
 FROM [tb_儿童基本信息] aa  
 left outer join [tb_地区档案] gg on aa.省 = gg.地区编码
 left outer join tb_地区档案 hh on aa.市 = hh.地区编码
 left outer join tb_地区档案 ii on aa.区 = ii.地区编码
 left outer join [tb_地区档案] bb on aa.街道 = bb.地区编码
 left outer join [tb_地区档案] cc on aa.居委会 = cc.地区编码 
 left outer join tb_机构信息 dd on aa.所属机构 = dd.机构编号
 left outer join tb_MyUser ee on aa.创建人 = ee.用户编码
 left outer join tb_MyUser jj on aa.修改人 = jj.用户编码
 left outer join tb_机构信息 kk on aa.创建机构 = kk.机构编号
 left outer join tb_常用字典 ff on aa.性别 = FF.P_CODE AND ff.P_FUN_CODE = 'xb_xingbie'
where aa.个人档案编号=@grdabh ";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@grdabh", SqlDbType.VarChar, docNo.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_儿童基本信息.__TableName;

            //if(ds.Tables[0].Rows.Count > 0)
            //{
            //    ds.Tables[0].Rows[0]["姓名"] = DESEncrypt.DES解密(ds.Tables[0].Rows[0]["姓名"].ToString());
            //}

            return ds;
        }

        public System.Data.DataSet Get儿童基本信息ByGrdabh(string grdabh)
        {
            string sql1 = @" SELECT aa.ID,aa.卡号,aa.个人档案编号,aa.姓名,aa.身份证号,aa.缺项,aa.完整度,
ff.P_DESC 性别,--aa.性别
aa.出生日期,aa.联系电话,
 gg.地区名称+hh.地区名称+ii.地区名称+bb.地区名称+cc.地区名称+居住地址  居住地址,
 aa.父亲姓名,aa.父亲工作单位,aa.父亲联系电话,aa.父亲出生日期,
 aa.母亲姓名,aa.母亲工作单位,aa.母亲联系电话,aa.母亲出生日期,
 aa.邮政编码,aa.母亲身份证号,aa.建档单位,aa.建档单位电话,
 aa.托幼机构,aa.托幼机构电话,
 aa.HAPPENTIME,aa.创建日期,aa.修改日期,
 kk.机构名称 创建机构,--aa.创建机构,
ee.UserName 录入人,--aa.创建人
jj.UserName 修改人,--aa.修改人
dd.机构名称 当前机构,aa.所属机构
 FROM [tb_儿童基本信息] aa  
 left outer join [tb_地区档案] gg on aa.省 = gg.地区编码
 left outer join tb_地区档案 hh on aa.市 = hh.地区编码
 left outer join tb_地区档案 ii on aa.区 = ii.地区编码
 left outer join [tb_地区档案] bb on aa.街道 = bb.地区编码
 left outer join [tb_地区档案] cc on aa.居委会 = cc.地区编码 
 left outer join tb_机构信息 dd on aa.所属机构 = dd.机构编号
 left outer join tb_MyUser ee on aa.创建人 = ee.用户编码
 left outer join tb_MyUser jj on aa.修改人 = jj.用户编码
 left outer join tb_机构信息 kk on aa.创建机构 = kk.机构编号
 left outer join tb_常用字典 ff on aa.性别 = FF.P_CODE AND ff.P_FUN_CODE = 'xb_xingbie'
where aa.个人档案编号=@grdabh ";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@grdabh", SqlDbType.VarChar, grdabh.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_儿童基本信息.__TableName;

            //if(ds.Tables[0].Rows.Count > 0)
            //{
            //    ds.Tables[0].Rows[0]["姓名"] = DESEncrypt.DES解密(ds.Tables[0].Rows[0]["姓名"].ToString());
            //}

            return ds;
        }



        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            //string sql1 = "delete [tb_儿童基本信息s] where ["+tb_儿童基本信息.__KeyName+"]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_儿童基本信息] where [" + tb_儿童基本信息.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }
        DataSet ds = null;
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            ds = data;
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            //string docNo = DocNoTool.GetNumber(tran, "儿童基本信息");
            string docNo = ds.Tables[tb_儿童基本信息.__TableName].Rows[0][tb_儿童基本信息.个人档案编号].ToString();
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }


        /// <summary>
        /// 业务单据的保存数据方法
        /// </summary>
        public override SaveResult Update(DataSet data)
        {
            /***********************************************************************************
             *  数据更新注意事项:             
             *
             *  1. 参数:data 是业务数据所有更新的数据表. DataSet第1张表为主表，
             *     第2,3,...n张表为明细表或其它相关数据。
             * 
             *  2. 主表的主关键字可能是系统自动生成或用户输入，必须在保存前检查唯一性. 
             *     当_UpdateSummaryKeyMode=UpdateKeyMode.None为用户自行输入单号.
             * 
             *  3. 获取主表主键的值，调用UpdateDetailKey()方法更新所有明细表的外键值
             *     (更新外键：与主表建立关联的字段)                                                                    
             *     
             ************************************************************************************/

            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            string mGUID = string.Empty;
            string mDocNo = string.Empty;

            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");

            try
            {
                //更新所有资料表
                foreach (DataTable dt in data.Tables)
                {
                    //仅处理有作修改的资料表
                    if (dt.GetChanges() == null) continue;

                    //根据资料表名获取SQL命令生成器
                    IGenerateSqlCommand gen = this.CreateSqlGenerator(dt.TableName);
                    if (gen == null) continue; //当前数据层无法识别的SQL命令生成器，则不更新当前资料表

                    ////本次更新是更新主表,取主表的主键用于设置明细表的外键
                    //if (gen.IsSummary())
                    //{
                    //    UpdateSummaryKey(_CurrentTrans, dt, _UpdateSummaryKeyMode, gen.GetPrimaryFieldName(),
                    //        ref mGUID, gen.GetDocNoFieldName(), ref mDocNo);

                    //    mResult.GUID = mGUID;
                    //    mResult.DocNo = mDocNo;
                    //}
                    //else
                    //{
                    //    //更新明细表的外键
                    //    if (_UpdateSummaryKeyMode == UpdateKeyMode.OnlyDocumentNo) //单号
                    //        UpdateDetailKey(dt, gen.GetForeignFieldName(), mDocNo);
                    //    else if (_UpdateSummaryKeyMode == UpdateKeyMode.OnlyGuid) //GUID
                    //        UpdateDetailKey(dt, gen.GetForeignFieldName(), mGUID);
                    //    else if (_UpdateSummaryKeyMode == UpdateKeyMode.None)
                    //        UpdateDetailKey(dt, gen.GetForeignFieldName(), mDocNo);//不指定更新类型预设为用户输入单号
                    //}

                    //使用基于ADO构架的SqlClient组件更新数据
                    SqlDataAdapter adp = new SqlDataAdapter();
                    adp.RowUpdating += new SqlRowUpdatingEventHandler(OnAdapterRowUpdating);
                    adp.UpdateCommand = GetUpdateCommand(gen, _CurrentTrans);
                    adp.InsertCommand = GetInsertCommand(gen, _CurrentTrans);
                    adp.DeleteCommand = GetDeleteCommand(gen, _CurrentTrans);
                    adp.Update(dt);
                }

                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       

            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      

                mResult.SetException(ex); //保存结果设置异常消息

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
        }

        #region 新增的代码
        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region sql

                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_儿童基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.创建日期 >= (@DateFrom) and jktj.创建日期 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_儿童基本信息  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.创建日期 >= (@DateFrom)  and jktj.创建日期 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_儿童基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.创建日期 >= (@DateFrom)  and jktj.创建日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_儿童基本信息  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.创建日期 >= (@DateFrom)  and jktj.创建日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_儿童基本信息 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_儿童基本信息 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构
		                        )     a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
where a.所属上级机构 <> '371323'
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";

                #endregion
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#健康体检信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select * from  tb_健康档案  jkjc  ,tb_儿童基本信息 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建日期 >= (@DateFrom)
                                        and tj.创建日期 <= (@DateTo)	
                                        and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )			
                                                                             ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#儿童基本信息";
            }
            else if (Type == "3")//合格
            {
                sql = @"select * from  tb_健康档案  jkjc  ,tb_儿童基本信息 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建日期 >= (@DateFrom)
                                        and tj.创建日期 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )	
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#儿童基本信息";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select * from  tb_健康档案  jkjc  ,tb_儿童基本信息 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建日期 >= (@DateFrom)
                                        and tj.创建日期 <= (@DateTo)				
                                       and (
		                                (len(@机构)>6 and len(tj.所属机构)>=15 and substring(tj.所属机构,1,7)+'1'+substring(tj.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(tj.所属机构)>=12 and tj.所属机构 like @机构 +'%')
		                                or(tj.所属机构 = @机构)
		                                )	
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#儿童基本信息";
            }
            #endregion
            return ds;
        }

        #endregion

        /// <summary>
        /// 0-6岁儿童健康检查登记薄
        /// </summary>
        /// <param name="prgid"></param>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <returns></returns>
        public DataSet GetChildInfo(string prgid, string sDate, string eDate, string p4, string p5)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("USP_儿童健康检查登记簿", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@prgid"].Value = prgid;
                sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.NVarChar));
                sqlCommand.Parameters["@sDate"].Value = sDate;
                sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.NVarChar, 4000));
                sqlCommand.Parameters["@eDate"].Value = eDate;
                //sqlCommand.Parameters.Add(new SqlParameter("@TYPE", System.Data.SqlDbType.NVarChar));
                //sqlCommand.Parameters["@TYPE"].Value = type;
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                adapter.Fill(dataSet);
            }
            return dataSet;
            #region oldfield
            //            string sql = string.Empty;
//            DataSet ds = null;
//            #region 查询语句
////            sql = @"select m.居住地址 as '村庄',m.姓名,
////(select P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = m.性别)  as 性别,
////m.出生日期,m.创建日期 as '建册日期', 
////a.发生时间 as '一次',b.发生时间 as '二次',c.发生时间 as '三次',d.发生时间 as '四次',d.体重选项 as '评价',
////e.发生时间 as '一次',k.发生时间 as '二次',k.体重选项 as '评价',
////f.发生时间 as '一次',l.发生时间 as '二次',l.体重选项 as '评价',
////g.发生时间 as '一次',g.体重选项 as '评价',
////h.发生时间 as '一次',h.体重选项 as '评价',
////i.发生时间 as '一次',i.体重选项 as '评价',
////j.发生时间 as '一次',j.体重选项 as '评价'
////from tb_儿童新生儿访视记录 main 
////left join tb_儿童基本信息 m on main.个人档案编号=m.个人档案编号
////left join tb_健康档案 aa on main.个人档案编号=aa.个人档案编号
////left join tb_儿童_健康检查_满月 a on main.个人档案编号=a.个人档案编号
////left join tb_儿童_健康检查_3月 b on main.个人档案编号=b.个人档案编号
////left join tb_儿童_健康检查_6月 c on main.个人档案编号=c.个人档案编号
////left join tb_儿童_健康检查_8月 d on main.个人档案编号=d.个人档案编号
////left join tb_儿童_健康检查_12月 e on main.个人档案编号=e.个人档案编号
////left join tb_儿童_健康检查_18月 k on main.个人档案编号=k.个人档案编号
////left join tb_儿童_健康检查_24月 f on main.个人档案编号=f.个人档案编号
////left join tb_儿童_健康检查_30月 l on main.个人档案编号=l.个人档案编号
////left join tb_儿童_健康检查_3岁 g on main.个人档案编号=g.个人档案编号
////left join tb_儿童_健康检查_4岁 h on main.个人档案编号=h.个人档案编号
////left join tb_儿童_健康检查_5岁 i on main.个人档案编号=i.个人档案编号
////left join tb_儿童_健康检查_6岁 j on main.个人档案编号=j.个人档案编号
////where (len(aa.所属机构)>=15 and substring(aa.所属机构,1,7)+'1'+substring(aa.所属机构,9,7) like @prgid +'%'
////       or aa.所属机构 like @prgid +'%')
////and main.发生时间>@sDate 
////and main.发生时间<@eDate";
//            #endregion
//            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
//            cmd.AddParam("@prgid", SqlDbType.VarChar, prgid);
//            cmd.AddParam("@sDate", SqlDbType.VarChar, sDate);
//            cmd.AddParam("@eDate", SqlDbType.VarChar, eDate);
//            ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            //            return ds;
            #endregion
        }

        public DataSet GetChildInfoNew(string prgid, string sDate, string eDate, string name, string sfzh, string dah)
        {
            DataSet dataSet = new DataSet();
            StringBuilder sql = new StringBuilder();
            sql.Append(@"select aa.家庭档案编号, aa.个人档案编号, m.居住地址 as '村庄',aa.姓名,
(select P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = m.性别)  as 性别,
m.母亲联系电话, m.父亲联系电话, m.母亲姓名,m.母亲身份证号,
m.出生日期,--m.HAPPENTIME as '建册日期', 
main.[发生时间]  as '建册日期', 
case when a.指导其他 like'%外出%' then a.指导其他 else a.发生时间 end '0岁检查一次',
case when b.指导其他 like'%外出%' then b.指导其他 else b.发生时间 end '0岁检查二次',
case when c.指导其他 like'%外出%' then c.指导其他 else c.发生时间 end '0岁检查三次',
case when d.指导其他 like'%外出%' then d.指导其他 else d.发生时间 end '0岁检查四次',
d.体重选项 as '0岁检查评价',
case when e.EXSEZDQT like'%外出%' then e.EXSEZDQT else e.发生时间 end '1岁检查一次',
case when k.EXSEZDQT like'%外出%' then k.EXSEZDQT else k.发生时间 end '1岁检查二次',
k.体重选项 as '1岁检查评价',
case when f.指导其他 like'%外出%' then f.指导其他 else f.发生时间 end '2岁检查一次',
case when l.EXSEZDQT like'%外出%' then l.EXSEZDQT else l.发生时间 end '2岁检查二次',
l.体重选项 as '2岁检查评价',
case when g.E_XSEZDQT like'%外出%' then g.E_XSEZDQT else g.发生时间 end '3岁检查一次',
g.体重选项 as '3岁检查评价',
case when h.E_XSEZDQT like'%外出%' then h.E_XSEZDQT else h.发生时间 end '4岁检查一次',
h.体重选项 as '4岁检查评价',
case when i.E_XSEZDQT like'%外出%' then i.E_XSEZDQT else i.发生时间 end '5岁检查一次',
i.体重选项 as '5岁检查评价',
case when j.E_XSEZDQT like'%外出%' then j.E_XSEZDQT else j.发生时间 end '6岁检查一次',
j.体重选项 as '6岁检查评价'
from tb_健康档案 aa 
left join tb_儿童基本信息 m on aa.个人档案编号=m.个人档案编号
left join tb_儿童新生儿访视记录 main on main.个人档案编号=aa.个人档案编号
left join tb_儿童_健康检查_满月 a on aa.个人档案编号=a.个人档案编号
left join tb_儿童_健康检查_3月 b on aa.个人档案编号=b.个人档案编号
left join tb_儿童_健康检查_6月 c on aa.个人档案编号=c.个人档案编号
left join tb_儿童_健康检查_8月 d on aa.个人档案编号=d.个人档案编号
left join tb_儿童_健康检查_12月 e on aa.个人档案编号=e.个人档案编号
left join tb_儿童_健康检查_18月 k on aa.个人档案编号=k.个人档案编号
left join tb_儿童_健康检查_24月 f on aa.个人档案编号=f.个人档案编号
left join tb_儿童_健康检查_30月 l on aa.个人档案编号=l.个人档案编号
left join tb_儿童_健康检查_3岁 g on aa.个人档案编号=g.个人档案编号
left join tb_儿童_健康检查_4岁 h on aa.个人档案编号=h.个人档案编号
left join tb_儿童_健康检查_5岁 i on aa.个人档案编号=i.个人档案编号
left join tb_儿童_健康检查_6岁 j on aa.个人档案编号=j.个人档案编号
where (len(aa.所属机构)>=15 and substring(aa.所属机构,1,7)+'1'+substring(aa.所属机构,9,7) like @prgid +'%' or aa.所属机构 like @prgid +'%')
and aa.出生日期>=@sDate and aa.出生日期<=@eDate
and aa.档案状态='1' ");
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(new SqlParameter("@prgid", prgid));
            paramlist.Add(new SqlParameter("@sDate", sDate));
            paramlist.Add(new SqlParameter("@eDate", eDate));
            if (string.IsNullOrWhiteSpace(name))
            { }
            else
            {
                sql.Append(" and aa.姓名 like '%'+@name+'%'");
                paramlist.Add(new SqlParameter("@name", name));
            }

            if(!string.IsNullOrWhiteSpace(sfzh))
            {
                sql.Append(" and aa.身份证号 = @sfzh");
                paramlist.Add(new SqlParameter("@sfzh", sfzh));
            }

            if (!string.IsNullOrWhiteSpace(dah))
            {
                sql.Append(" and aa.个人档案编号 = @dah");
                paramlist.Add(new SqlParameter("@dah", dah));
            }

            sql.Append(" order by aa.出生日期");
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = sqlConn.CreateCommand();
                sqlCommand.CommandText = sql.ToString();
                sqlCommand.Parameters.AddRange(paramlist.ToArray());

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                adapter.Fill(dataSet);
            }
            return dataSet;
        }

        /// <summary>
        /// 2019年5月9日 yufh 调整
        /// </summary>
        /// <param name="rgid">机构ID</param>
        /// <param name="sDate">开始日期</param>
        /// <param name="eDate">结束日期</param>
        /// <param name="SearchBy">日期类型1出生日期2随访日期</param>
        /// <param name="selectType">查询类型1汇总2明细</param>
        /// <returns></returns>
        public DataSet GetChildGLL(string rgid, string sDate, string eDate, string SearchBy, string selectType)
        {
            string sql = string.Empty;
            DataSet dataSet = new DataSet();
            switch (selectType)
            {
                case"1":
                    using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
                    {
                        SqlCommand sqlCommand = new SqlCommand("USP_儿童系统管理率", sqlConn);
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@prgid", System.Data.SqlDbType.NVarChar));
                        sqlCommand.Parameters["@prgid"].Value = rgid;
                        sqlCommand.Parameters.Add(new SqlParameter("@sDate", System.Data.SqlDbType.NVarChar));
                        sqlCommand.Parameters["@sDate"].Value = sDate;
                        sqlCommand.Parameters.Add(new SqlParameter("@eDate", System.Data.SqlDbType.NVarChar));
                        sqlCommand.Parameters["@eDate"].Value = eDate;
                        SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                        sqlCommand.CommandTimeout = 0;
                        adapter.Fill(dataSet);
                    }
                    break;
                case "2": //新生儿访视数
                    sql = @"select a.ID,  a.个人档案编号,a.家庭档案编号,a.姓名
	,a.性别 ,a.出生日期,a.身份证号,a.街道,a.居委会,a.居住地址
	,aa.联系人电话,aa.联系人姓名,aa.本人电话
	,(select 机构名称 from tb_机构信息 where 机构编号 = a.所属机构)所属机构
	,(select UserName from tb_MyUser where 用户编码 = a.创建人) 创建人,a.创建时间 
    from vw_儿童新生儿访视 a 
    left join tb_健康档案 aa on aa.个人档案编号 = a.个人档案编号  
    left join tb_儿童基本信息 b on aa.个人档案编号 = b.个人档案编号 
    where a.档案状态='1' 
	and (len(a.所属机构)>=15 and substring(a.所属机构,1,7)+'1'+substring(a.所属机构,9,7) like @机构+'%'
		or a.所属机构 like  @机构+'%')";
                if (SearchBy == "1")
                {
                    sql += @"  and a.随访时间 >=@DateFrom and a.随访时间 <=@DateTo ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"  and a.出生日期 >=@DateFrom and a.出生日期 <=@DateTo ";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, rgid);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, sDate);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, eDate);
                dataSet = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                dataSet.Tables[0].TableName = "#统计";
                    break;
                default:
                    break;
            }
            
            return dataSet;
        }

        public DataSet GetInfoByFangshi(string _docNo)
        {
            //throw new NotImplementedException();
            string sql1 = " select * from [tb_儿童基本信息]    where [" + tb_儿童基本信息.个人档案编号 + "]=@DocNo1";
            string sql2 = " select * from [tb_健康档案]   where [" + tb_健康档案.个人档案编号 + "]=@DocNo2 "; //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_儿童基本信息.__TableName;
            ds.Tables[1].TableName = tb_健康档案.__TableName;
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;
            return ds;
        }

        public DataTable GetBaseData(string sex)
        {
            string sql = "SELECT * FROM [dbo].[tb_儿童发育基础数据] where sex=@sex order by id desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@sex", SqlDbType.VarChar, sex.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public DataTable GetGrownData(string dah)
        {
            string sql = @"select 0 [Month], 出生身长 CM, [新生儿出生体重] KG from dbo.tb_儿童新生儿访视记录 WHERE 个人档案编号=@dah
union all 
select 3 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_3月] WHERE 个人档案编号=@dah
union all 
select 6 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_6月] WHERE 个人档案编号=@dah
union all 
select 8 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_8月] WHERE 个人档案编号=@dah
union all 
select 12 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_12月] WHERE 个人档案编号=@dah
union all 
select 18 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_18月] WHERE 个人档案编号=@dah
union all 
select 24 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_24月] WHERE 个人档案编号=@dah
union all 
select 30 [Month], 身长 CM, 体重 KG from dbo.[tb_儿童_健康检查_30月] WHERE 个人档案编号=@dah
union all 
select 36 [Month], [身高] CM, 体重 KG from dbo.[tb_儿童_健康检查_3岁] WHERE 个人档案编号=@dah
union all 
select 48 [Month], [身高] CM, 体重 KG from dbo.[tb_儿童_健康检查_4岁] WHERE 个人档案编号=@dah
union all 
select 60 [Month], [身高] CM, 体重 KG from dbo.[tb_儿童_健康检查_5岁] WHERE 个人档案编号=@dah
union all 
select 72 [Month], [身高] CM, 体重 KG from dbo.[tb_儿童_健康检查_6岁] WHERE 个人档案编号=@dah";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public DataSet Get妇保院工作量(string name, string sfzh, string dah, string createUserNo, string sfrq1, string sfrq2, string csrq1, string csrq2)
        {
            StringBuilder sbSFRQ = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(sfrq1))
            {
                sbSFRQ.Append(" and convert(datetime,[发生时间])>=@sfrq1 ");
            }
            if (!string.IsNullOrWhiteSpace(sfrq2))
            {
                sbSFRQ.Append(" and convert(datetime,[发生时间])< @sfrq2 ");
            }

            StringBuilder sbCSRQ = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(csrq1))
            {
                sbCSRQ.Append(" and convert(datetime, b.[出生日期]) >=@csrq1 ");
            }
            if (!string.IsNullOrWhiteSpace(csrq2))
            {
                sbCSRQ.Append(" and convert(datetime, b.[出生日期]) <@csrq2 ");
            }

            StringBuilder sbWhere = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(dah))
            {
                sbWhere.Append(" and a.个人档案编号=@dah ");
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                sbWhere.Append(" and b.姓名 like @name+'%' ");
            }

            if (!string.IsNullOrWhiteSpace(sfzh))
            {
                sbWhere.Append(" and b.身份证号=@sfzh ");
            }

            string sql1 = @"select a.*,b.姓名,b.出生日期,b.身份证号,b.居住地址,b.联系人电话,所属机构名称 所属机构,居委会 
from (
select 个人档案编号,创建人,'满月' 随访次序,发生时间 随访时间 
from tb_儿童_健康检查_满月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'3月' 随访次,发生时间 
from tb_儿童_健康检查_3月  where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'6月' 随访次,发生时间 
from tb_儿童_健康检查_6月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'8月' 随访次,发生时间 
from tb_儿童_健康检查_8月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'12月' 随访次,发生时间 
from tb_儿童_健康检查_12月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'18月' 随访次,发生时间 
from tb_儿童_健康检查_18月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'24月' 随访次,发生时间 
from tb_儿童_健康检查_24月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'30月' 随访次,发生时间 
from tb_儿童_健康检查_30月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'3岁' 随访次,发生时间 
from tb_儿童_健康检查_3岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'4岁' 随访次,发生时间 
from tb_儿童_健康检查_4岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'5岁' 随访次,发生时间 
from tb_儿童_健康检查_5岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'6岁' 随访次,发生时间 
from tb_儿童_健康检查_6岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
) a 
left join View_个人信息表 b on a.个人档案编号 = b.个人档案编号
where 1=1 " + sbWhere.ToString() + sbCSRQ.ToString();


            string sql2 = @"select a.个人档案编号,b.姓名,MAX(b.出生日期) 出生日期,MAX(b.身份证号) 身份证号,MAX(b.居住地址) 居住地址
,MAX(b.联系人电话) 联系电话,COUNT(b.姓名) 随访次数
,MAX(a.满月随访时间) 满月随访时间,MAX(a.[3月随访时间]) [3月随访时间]
,MAX(a.[6月随访时间]) [6月随访时间],MAX(a.[8月随访时间]) [8月随访时间]
,MAX(a.[12月随访时间]) [12月随访时间],MAX(a.[18月随访时间]) [18月随访时间]
,MAX(a.[24月随访时间]) [24月随访时间],MAX(a.[30月随访时间]) [30月随访时间]
,MAX(a.[3岁随访时间]) [3岁随访时间],MAX(a.[4岁随访时间]) [4岁随访时间]
,MAX(a.[5岁随访时间]) [5岁随访时间],MAX(a.[6岁随访时间]) [6岁随访时间]
,MAX(所属机构名称) 所属机构,MAX(b.居委会) 居委会 
from (
select 个人档案编号,创建人,发生时间 as 满月随访时间,'' '3月随访时间','' '6月随访时间', '' '8月随访时间'
	,'' '12月随访时间' ,'' '18月随访时间','' '24月随访时间','' '30月随访时间','' '3岁随访时间'
	,'' '4岁随访时间' ,'' '5岁随访时间' ,'' '6岁随访时间' 
from tb_儿童_健康检查_满月  where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'',发生时间,'','','','','','','','','',''
from tb_儿童_健康检查_3月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','',发生时间,'','','','','','','','',''
from tb_儿童_健康检查_6月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','',发生时间,'','','','','','','',''
from tb_儿童_健康检查_8月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','',发生时间,'','','','','','',''
from tb_儿童_健康检查_12月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','',发生时间,'','','','','','' 
from tb_儿童_健康检查_18月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','',发生时间,'','','','','' 
from tb_儿童_健康检查_24月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','','',发生时间,'','','',''
from tb_儿童_健康检查_30月 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','','','',发生时间,'','',''
from tb_儿童_健康检查_3岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','','','','',发生时间,'','' 
from tb_儿童_健康检查_4岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','','','','','',发生时间,''
from tb_儿童_健康检查_5岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
union all
select 个人档案编号,创建人,'','','','','','','','','','','',发生时间
from tb_儿童_健康检查_6岁 where 创建人=@createUserNo  and isdate([发生时间])=1 " + sbSFRQ.ToString() + @"
) a 
left join View_个人信息表 b on a.个人档案编号 = b.个人档案编号
where 1=1" + sbWhere.ToString() + sbCSRQ.ToString() + " group by a.个人档案编号,a.创建人,b.姓名 ";


            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);

            cmd.AddParam("@createUserNo", SqlDbType.VarChar, createUserNo);

            if (!string.IsNullOrWhiteSpace(sfrq1))
            {
                cmd.AddParam("@sfrq1", SqlDbType.VarChar, sfrq1);
            }
            if (!string.IsNullOrWhiteSpace(sfrq2))
            {
                cmd.AddParam("@sfrq2", SqlDbType.VarChar, sfrq2);
            }

            if (!string.IsNullOrWhiteSpace(csrq1))
            {
                cmd.AddParam("@csrq1", SqlDbType.VarChar, csrq1);
            }

            if (!string.IsNullOrWhiteSpace(csrq2))
            {
                cmd.AddParam("@csrq2", SqlDbType.VarChar, csrq2);
            }

            if (!string.IsNullOrWhiteSpace(dah))
            {
                cmd.AddParam("@dah", SqlDbType.VarChar, dah);
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                cmd.AddParam("@name", SqlDbType.VarChar, name);
            }

            if (!string.IsNullOrWhiteSpace(sfzh))
            {
                cmd.AddParam("@sfzh", SqlDbType.VarChar, sfzh);
            }

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds;
        }
    }
}

