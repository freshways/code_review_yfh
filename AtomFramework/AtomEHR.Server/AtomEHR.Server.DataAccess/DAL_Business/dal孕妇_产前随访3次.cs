﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 孕妇_产前随访3次的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/10/26 05:24:04
 *   最后修改: 2015/10/26 05:24:04
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal孕妇_产前随访3次
    /// </summary>
    public class dal孕妇_产前随访3次 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dal孕妇_产前随访3次(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_孕妇_产前随访3次.__KeyName; //主表的主键字段
             _SummaryTableName = tb_孕妇_产前随访3次.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_孕妇_产前随访3次.__TableName) ORM = typeof(tb_孕妇_产前随访3次);
             //if (tableName == tb_孕妇_产前随访3次s.__TableName) ORM = typeof(tb_孕妇_产前随访3次s);//如有明细表请调整本行的代码
             else if (tableName == tb_健康档案.__TableName) ORM = typeof(tb_健康档案);
             else if (tableName == tb_孕妇基本信息.__TableName) ORM = typeof(tb_孕妇基本信息);
             else if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);

             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_孕妇_产前随访3次.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_孕妇_产前随访3次.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_孕妇_产前随访3次.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_孕妇_产前随访3次]    where ["+tb_孕妇_产前随访3次.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_孕妇_产前随访3次s]   where ["+tb_孕妇_产前随访3次.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_孕妇_产前随访3次.__TableName;
              //ds.Tables[1].TableName =tb_孕妇_产前随访3次s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_孕妇_产前随访3次s] where ["+tb_孕妇_产前随访3次.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_孕妇_产前随访3次] where ["+tb_孕妇_产前随访3次.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "孕妇_产前随访3次");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          #region 新增的方法
          /// <summary>
          /// 根据个人档案编号获取孕妇第二次随访记录
          /// </summary>
          /// <param name="grdabh"></param>
          /// <returns></returns>
          public DataSet GetShowInfos(string grdabh)
          {
              string sql1 = @"select aa.ID,aa.[个人档案编号], aa.[孕周], aa.[体重], aa.[收缩压], aa.[宫底高度], aa.[腹围]
, aa.[尿蛋白], aa.[随访医生], ii.机构名称 [创建机构], gg.UserName [创建人], hh.UserName [修改人], aa.所属机构, jj.机构名称 [所属机构名称]
, aa.[创建时间], aa.[修改时间], aa.[发生时间], aa.[卡号], aa.[主诉]
, aa.[舒张压], aa.[血红蛋白], aa.[其他检查], aa.[分类], aa.[分类异常], aa.[指导]
, aa.[转诊], aa.[转诊原因], aa.[转诊机构], aa.[下次随访日期], aa.[缺项], aa.[孕次]
, aa.[TX_TYPE], aa.[胎位], aa.[指导其他], aa.[完整度], aa.[胎心率], aa.[孕周天]
, bb.[姓名],bb.[联系人电话] as 联系电话,bb.[出生日期],bb.[身份证号], kk.P_DESC [居住状况],
cc.地区名称 +' '+ dd.地区名称 +' '+ ee.地区名称 +' ' + ff.地区名称+ '' +bb.[居住地址] 居住地址
,aa.随访方式,aa.随访方式其他,aa.产前检查机构名称,aa.联系人,aa.联系方式,aa.结果,aa.居民签名
,aa.[怀孕状态],aa.[流引产日期],aa.[流引产孕周],aa.[流引产原因],aa.[流引产医院],aa.SFID
from [tb_孕妇_产前随访3次] aa inner join tb_健康档案 bb   on aa.个人档案编号=bb.个人档案编号 
	left outer join tb_地区档案 cc on bb.省 = cc.地区编码
	left outer join tb_地区档案 dd on bb.市 = dd.地区编码
	left outer join tb_地区档案 ee on bb.街道 = ee.地区编码
	left outer join tb_地区档案 ff on bb.居委会 = ff.地区编码
	left outer join tb_MyUser gg on aa.创建人 = gg.用户编码
	left outer join tb_MyUser hh on aa.修改人 = hh.用户编码
	left outer join tb_机构信息 ii on aa.创建机构 = ii.机构编号
	left outer join tb_机构信息 jj on aa.所属机构 = jj.机构编号
    LEFT OUTER JOIN tb_常用字典 kk on kk.P_FUN_CODE = 'jzzk' and bb.常住类型 = kk.P_CODE
 where aa.[" + tb_孕妇_产前随访3次.个人档案编号 + "]=@DocNo1  order by aa.[" + tb_孕妇_产前随访3次.发生时间 + "] desc ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdabh.Trim());

              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_孕妇_产前随访3次.__TableName;
              return ds;
          }
          public void Update个人体征(string grdabh)
          {
              string sql2 = "update [tb_健康档案_个人健康特征] set [" + tb_健康档案_个人健康特征.第二次产前检查表 + "]='未建' where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, grdabh.Trim());
              DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
          }

          public System.Data.DataSet GetBusinessByGrdabhAndID(string grdabh, string id)
          {
              string sql1 = " select * from [tb_孕妇_产前随访3次]    where [" + tb_孕妇_产前随访3次.个人档案编号 + "]=@grdabh  and [" + tb_孕妇_产前随访3次.ID + "]=@id ";
              string sql2 = " select * from [tb_健康档案] where [" + tb_健康档案.个人档案编号 + "]=@grdabh ";
              string sql3 = " select * from [tb_孕妇基本信息] where [" + tb_孕妇基本信息.个人档案编号 + "]=@grdabh ";
              string sql4 = " select * from [tb_健康档案_个人健康特征] where [" + tb_健康档案_个人健康特征.个人档案编号 + "] = @grdabh ";
              //高血压、慢性病、既往病史、健康状况等四个表也需要更新，但由于时间限制，暂时不实现这块的功能。

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4);
              cmd.AddParam("@grdabh", SqlDbType.VarChar, grdabh.Trim());
              cmd.AddParam("@id", SqlDbType.BigInt, id);

              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_孕妇_产前随访3次.__TableName;    //新增或修改这个表的记录
              ds.Tables[1].TableName = tb_健康档案.__TableName;//档案信息只是使用，不对其数据进行修改
              ds.Tables[2].TableName = tb_孕妇基本信息.__TableName;  //修改sfflag、下次随访时间两个字段
              ds.Tables[3].TableName = tb_健康档案_个人健康特征.__TableName;//暂时只更新“孕妇_产前随访3次”的信息，应该更新高血压、高血压随访表、高血压管理卡

              return ds;
          }

          /// <summary>
          /// 根据个人档案编号，孕次查询随访记录是否存在, 存在返回true，不存在返回false
          /// </summary>
          /// <param name="grdabh"></param>
          /// <param name="str孕次"></param>
          /// <returns>存在返回true，不存在返回false</returns>
          public bool Check随访记录(string grdabh, string str孕次)
          {
              bool ret = false;
              string sql1 = " select count(*) from [tb_孕妇_产前随访3次] where [" + tb_孕妇_产前随访3次.个人档案编号 + "]=@grdabh  and [" + tb_孕妇_产前随访3次.孕次 + "]=@孕次 ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@grdabh", SqlDbType.VarChar, grdabh.Trim());
              cmd.AddParam("@孕次", SqlDbType.VarChar, str孕次);

              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

              if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
              {
                  ret = false;
              }
              else
              {
                  string strCount = ds.Tables[0].Rows[0][0].ToString();
                  try
                  {
                      int count = Convert.ToInt32(strCount);
                      if (count >= 1)
                      {
                          ret = true;
                      }
                      else
                      {
                          ret = false;
                      }
                  }
                  catch
                  {
                      ret = false;
                  }
              }
              return ret;
          }

          /// <summary>
          /// 根据档案号检查记录是否存在
          /// </summary>
          /// <param name="grdabh"></param>
          /// <returns></returns>
          public bool Check记录(string grdabh)
          {
              bool ret = false;
              string sql1 = " select count(*) from [tb_孕妇_产前随访3次] where [" + tb_孕妇_产前随访3次.个人档案编号 + "]=@grdabh ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@grdabh", SqlDbType.VarChar, grdabh.Trim());

              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

              if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
              {
                  ret = false;
              }
              else
              {
                  string strCount = ds.Tables[0].Rows[0][0].ToString();
                  try
                  {
                      int count = Convert.ToInt32(strCount);
                      if (count >= 1)
                      {
                          ret = true;
                      }
                      else
                      {
                          ret = false;
                      }
                  }
                  catch
                  {
                      ret = false;
                  }
              }
              return ret;
          }

          public DataSet GetNextDateByGrdabhAndYc(string grdabh, string yunCi)
          {
              string sql1 = " select " + tb_孕妇_产前随访1次.下次随访日期 + " from tb_孕妇_产前随访1次   where [" + tb_孕妇_产前随访1次.个人档案编号 + "]=@grdabh and [" + tb_孕妇_产前随访1次.孕次 + "]=@yunci"; //子表
              string sql2 = " select " + tb_孕妇_产前随访2次.下次随访日期 + " from tb_孕妇_产前随访2次   where [" + tb_孕妇_产前随访2次.个人档案编号 + "]=@grdabh and [" + tb_孕妇_产前随访2次.孕次 + "]=@yunci"; //子表

              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
              cmd.AddParam("@grdabh", SqlDbType.VarChar, grdabh.Trim());
              cmd.AddParam("@yunci", SqlDbType.VarChar, yunCi.Trim());

              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

              return ds;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportDataBYLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
          {
              string sql = string.Empty;
              DataSet ds = null;
              #region 查询语句

              if (Type == "1")
              {
                  sql = @"select 
tab1.rgid 区域编码,tab1.机构名称 区域名称,
isnull(tab1.总数,0) 总数,isnull(tab2.合格数,0) 合格数,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数,convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
isnull(tab7.平均完整度,0) 平均完整度,isnull(tab9.转入档案数,0) 转入档案数,isnull(tab8.转出档案数,0) 转出档案数,
isnull(tab6.非活动档案数,0) 非活动档案数
                            from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访3次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访3次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访3次 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访3次 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 order by tab1.rgid
";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#健康体检信息统计";
              }
              else if (Type == "2")
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建时间 >= (@DateFrom)
                                        and tj.创建时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              else if (Type == "3")//合格
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建时间 >= (@DateFrom)
                                        and tj.创建时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              else if (Type == "4")//不合格
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.创建时间 >= (@DateFrom)
                                        and tj.创建时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              #endregion
              return ds;
          }
          public DataSet GetReportDataBYDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
          {
              string sql = string.Empty;
              DataSet ds = null;
              #region 查询语句

              if (Type == "1")
              {
                  sql = @"select 
tab1.rgid 区域编码,tab1.机构名称 区域名称,
isnull(tab1.总数,0) 总数,isnull(tab2.合格数,0) 合格数,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数,convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
isnull(tab7.平均完整度,0) 平均完整度,isnull(tab9.转入档案数,0) 转入档案数,isnull(tab8.转出档案数,0) 转出档案数,
isnull(tab6.非活动档案数,0) 非活动档案数
                            from (
	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.发生时间 >= (@DateFrom) and jktj.发生时间 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.发生时间 >= (@DateFrom)  and jktj.发生时间 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访3次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.发生时间 >= (@DateFrom)  and jktj.发生时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_孕妇_产前随访3次  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.发生时间 >= (@DateFrom)  and jktj.发生时间 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访3次 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_孕妇_产前随访3次 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 order by tab1.rgid
";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#健康体检信息统计";
              }
              else if (Type == "2")
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.发生时间 >= (@DateFrom)
                                        and tj.发生时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              else if (Type == "3")//合格
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.发生时间 >= (@DateFrom)
                                        and tj.发生时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              else if (Type == "4")//不合格
              {
                  sql = @"select * from  tb_健康档案  jkjc  ,tb_孕妇_产前随访3次 tj
                                        where jkjc.档案状态='1'  and jkjc.个人档案编号 = tj.个人档案编号
                                        and tj.发生时间 >= (@DateFrom)
                                        and tj.发生时间 <= (@DateTo)				
                                        and ((len(tj.所属机构)>=15 and tj.所属机构 like @机构 +'%') 
                                        or tj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(tj.完整度,'')  
                                        when '' then '0' else tj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                  cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                  cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                  cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                  cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                  ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                  ds.Tables[0].TableName = "#个人基本信息统计";
              }
              #endregion
              return ds;
          }
          #endregion

        /// <summary>
        /// 获取孕妇3次随访信息
        /// </summary>
        /// <param name="_docNo"></param>
        /// <returns></returns>
          public DataSet GetInfoSuiFang3(string _docNo)
          {
              string sql1 = " select * from [tb_孕妇_产前随访3次]    where [" + tb_孕妇_产前随访3次.个人档案编号 + "]=@DocNo1 ";
              string sql2 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo2 ";
              string sql3 = " select * from tb_健康档案   where [" + tb_健康档案.个人档案编号 + "]=@DocNo3 ";
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, _docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, _docNo.Trim());
              cmd.AddParam("@DocNo3", SqlDbType.VarChar, _docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_孕妇_产前随访3次.__TableName;
              ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;//子表
              ds.Tables[2].TableName = tb_健康档案.__TableName;//子表
              return ds;
          }
     }
}

