﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 残疾人_听力的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/11/25 03:00:24
 *   最后修改: 2015/11/25 03:00:24
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal残疾人_听力
    /// </summary>
    public class dal残疾人_肢体 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal残疾人_肢体(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_残疾人_肢体.__KeyName; //主表的主键字段
            _SummaryTableName = tb_残疾人_肢体.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_残疾人_肢体.__TableName) ORM = typeof(tb_残疾人_肢体);
            if (tableName == tb_残疾人_基本信息.__TableName) ORM = typeof(tb_残疾人_基本信息);
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);
            //if (tableName == tb_残疾人_听力s.__TableName) ORM = typeof(tb_残疾人_听力s);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_残疾人_听力.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_残疾人_听力.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_残疾人_听力.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_残疾人_肢体]    where 1=2 ";
            string sql2 = " select * from [tb_残疾人_基本信息]   where [" + tb_残疾人_基本信息.__KeyName + "]=@DocNo2 "; //子表
            string sql3 = " select * from [tb_健康档案_个人健康特征]   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_残疾人_肢体.__TableName;
            ds.Tables[1].TableName = tb_残疾人_基本信息.__TableName;
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;
            //ds.Tables[1].TableName =tb_残疾人_听力s.__TableName;//子表
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_残疾人_肢体] where [" + tb_残疾人_肢体.__KeyName + "]=@DocNo1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        public bool Delete(string docNo, string id)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_残疾人_肢体] where [" + tb_残疾人_听力.__KeyName + "]=@DocNo1 ";
            string sql2 = @"update [tb_健康档案_个人健康特征] set 肢体残疾随访表 = '未建'
            where [" + tb_健康档案_个人健康特征.个人档案编号 + "] not in (select 个人档案编号 from [tb_残疾人_肢体] ) and 个人档案编号 = @DocNo2";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        public override bool CheckNoExists(string keyValue)
        {
            string sql = string.Format("SELECT COUNT(*) C FROM [tb_残疾人_肢体] WHERE [个人档案编号]=@KEY", _SummaryTableName, _SummaryKeyName);
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@KEY", SqlDbType.VarChar, keyValue);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "tb_残疾人_肢体");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        public System.Data.DataSet GetAllDataByKey(string docNo)
        {
            string sql1 = " select * from tb_残疾人_基本信息   where [" + tb_残疾人_基本信息.个人档案编号 + "]=@DocNo1 ";
            string sql2 = " select * from [tb_残疾人_肢体]   where [" + tb_残疾人_听力.个人档案编号 + "]=@DocNo2 "; //子表
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_残疾人_基本信息.__TableName;
            ds.Tables[1].TableName = tb_残疾人_听力.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }

        public System.Data.DataSet GetDataByKey(string docNo)
        {
            string sql1 = " select * from tb_残疾人_基本信息   where [" + tb_残疾人_基本信息.__KeyName + "]=@DocNo1 ";
            string sql2 = " select * from [tb_残疾人_肢体]   where [" + tb_残疾人_听力.个人档案编号 + "]=@DocNo2 "; //子表
            //string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.__KeyName + "]=@DocNo3 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_残疾人_基本信息.__TableName;
            ds.Tables[1].TableName = tb_残疾人_肢体.__TableName;//子表
            //ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }

        public DataSet GetOneDataByKey(string docNo, string id)
        {
            string sql1 = " select * from [tb_残疾人_肢体]   where [" + tb_残疾人_听力.ID + "]=@DocNo2 ";
            string sql2 = " select * from tb_残疾人_基本信息   where [" + tb_残疾人_基本信息.个人档案编号 + "]=@DocNo1 "; //子表
            string sql3 = " select * from tb_健康档案_个人健康特征   where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, id.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_残疾人_肢体.__TableName;
            ds.Tables[1].TableName = tb_残疾人_基本信息.__TableName;//子表
            ds.Tables[2].TableName = tb_健康档案_个人健康特征.__TableName;//子表
            return ds;
        }
    }
}

