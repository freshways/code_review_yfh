﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 健康体检的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/08/20 08:49:12
 *   最后修改: 2015/08/20 08:49:12
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// dal健康体检
    /// </summary>
    public class dal健康体检 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal健康体检(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_健康体检.__KeyName; //主表的主键字段
            _SummaryTableName = tb_健康体检.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_健康体检.__TableName) ORM = typeof(tb_健康体检);
            if (tableName == tb_健康档案_个人健康特征.__TableName) ORM = typeof(tb_健康档案_个人健康特征);//如有明细表请调整本行的代码
            if (tableName == "住院史") ORM = typeof(tb_健康体检_住院史);//如有明细表请调整本行的代码
            if (tableName == "家庭病床史") ORM = typeof(tb_健康体检_住院史);//如有明细表请调整本行的代码
            if (tableName == tb_健康体检_用药情况表.__TableName) ORM = typeof(tb_健康体检_用药情况表);//如有明细表请调整本行的代码
            if (tableName == tb_健康体检_非免疫规划预防接种史.__TableName) ORM = typeof(tb_健康体检_非免疫规划预防接种史);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_既往病史.__TableName) ORM = typeof(tb_健康档案_既往病史);//如有明细表请调整本行的代码
            if (tableName == tb_MXB糖尿病管理卡.__TableName) ORM = typeof(tb_MXB糖尿病管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_MXB慢病基础信息表.__TableName) ORM = typeof(tb_MXB慢病基础信息表);//如有明细表请调整本行的代码
            if (tableName == tb_MXB高血压管理卡.__TableName) ORM = typeof(tb_MXB高血压管理卡);//如有明细表请调整本行的代码
            if (tableName == tb_健康档案_健康状态.__TableName) ORM = typeof(tb_健康档案_健康状态);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_健康体检.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_健康体检.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康体检.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                return null;
            }
        }
        public DataTable GetSummaryByParam(string strWhere)
        {
            string query = @"SELECT  a.个人档案编号  
                        ,b.家庭档案编号  
                        ,b.[姓名]
			            ,(SELECT P_DESC FROM tb_常用字典  WHERE P_FUN_CODE='xb_xingbie' AND P_CODE = b.[性别])[性别]  
			            ,b.[出生日期]  
			            ,b.[身份证号] 
			            ,b.[居住地址]
			            ,b.[本人电话]
			            ,b.[联系人电话] 
			            ,(SELECT 机构名称 FROM dbo.tb_机构信息 WHERE 机构级别 <>'1'  and 机构编号 =b.[所属机构])[所属机构] 
			            ,a.[创建人]
			            ,a.[创建时间]   from "
                     + _SummaryTableName +
                     @" a inner join tb_健康档案 b on a.个人档案编号 = b.个人档案编号 where 1=1 " + strWhere;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康档案.__TableName);
            return dt;
        }
        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {

            string sql1 = " select * from [tb_健康体检]     where 1= 2";
            string sql2 = " select * from [tb_健康档案_个人健康特征]    where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo1 ";
            string sql3 = " select * from [tb_健康体检_住院史]   where 1= 2 ";
            string sql4 = " select * from [tb_健康体检_住院史]    where 1= 2 ";
            string sql5 = " select * from [tb_健康体检_用药情况表]   where 1= 2 ";
            string sql6 = " select * from [tb_健康体检_非免疫规划预防接种史]   where 1= 2  ";
            string sql7 = " select * from [tb_MXB高血压管理卡]   where  个人档案编号 = '" + docNo + "'";
            string sql8 = " select * from [tb_MXB糖尿病管理卡]   where 个人档案编号 = '" + docNo + "'";
            string sql9 = " select * from [tb_MXB慢病基础信息表]   where 个人档案编号 = '" + docNo + "'";
            string sql10 = " select * from [tb_健康档案_既往病史]    where 个人档案编号 = '" + docNo + "'";
            string sql11 = " select * from [tb_健康档案_健康状态]    where 个人档案编号 = '" + docNo + "'";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9 + sql10 + sql11);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康体检.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;
            ds.Tables[2].TableName = "住院史";
            ds.Tables[3].TableName = "家庭病床史";
            ds.Tables[4].TableName = tb_健康体检_用药情况表.__TableName;
            ds.Tables[5].TableName = tb_健康体检_非免疫规划预防接种史.__TableName;
            ds.Tables[6].TableName = tb_MXB高血压管理卡.__TableName;
            ds.Tables[7].TableName = tb_MXB糖尿病管理卡.__TableName;
            ds.Tables[8].TableName = tb_MXB慢病基础信息表.__TableName;
            ds.Tables[9].TableName = tb_健康档案_既往病史.__TableName;
            ds.Tables[10].TableName = tb_健康档案_健康状态.__TableName;
            //ds.Tables[1].TableName =tb_健康体检s.__TableName;//子表
            return ds;
        }
        /// <summary>
        /// 查询一条体检记录
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="date">创建日期</param>
        /// <returns></returns>
        public DataSet GetOneDataByKey(string docNo)
        {
            string query = @"SELECT *
                                          FROM [dbo].[View_健康体检表] a
                                          where a.个人档案编号 = @DocNo1 order by a.创建时间 desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            DataSet dt = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return dt;
        }
        /// <summary>
        /// 获取单条的个人体检信息数据，为code类型的
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="date">创建时间</param>
        /// <returns></returns>
        public System.Data.DataSet GetUserValueByKey(string docNo, string date, bool resetCurrent)
        {
            string sql1 = " select * from [tb_健康体检] where [" + tb_健康体检.个人档案编号 + "]=@DocNo1 and 创建时间 = @DocNo2 order by ID desc ";
            string sql2 = " select * from [tb_健康档案_个人健康特征] where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo3 ";
            string sql3 = " select * from [tb_健康体检_住院史] where [" + tb_健康体检_住院史.个人档案编号 + "]=@DocNo4 and DATEDIFF(day,创建日期 ,@DocNo5)=0 and 类型 = '1'";
            string sql4 = " select * from [tb_健康体检_住院史] where [" + tb_健康体检_住院史.个人档案编号 + "]=@DocNo6 and DATEDIFF(day,创建日期, @DocNo7)=0 and 类型 = '2'";
            string sql5 = " select * from [tb_健康体检_用药情况表]    where [" + tb_健康体检_用药情况表.个人档案编号 + "]=@DocNo8 and DATEDIFF(day,创建时间 ,@DocNo9)=0 "; //2017-06-03 12:45:05 yufh 调整：住院、用药同一天的都可显示
            string sql6 = " select * from [tb_健康体检_非免疫规划预防接种史]    where [" + tb_健康体检_非免疫规划预防接种史.个人档案编号 + "]=@DocNo10 and DATEDIFF(day,创建日期,@DocNo11)=0 ";
            string sql7 = " select * from [tb_MXB高血压管理卡]    where [" + tb_MXB高血压管理卡.个人档案编号 + "]=@DocNo12 ";
            string sql8 = " select * from [tb_MXB糖尿病管理卡]    where [" + tb_MXB糖尿病管理卡.个人档案编号 + "]=@DocNo13 ";
            string sql9 = " select * from [tb_MXB慢病基础信息表]    where [" + tb_MXB慢病基础信息表.个人档案编号 + "]=@DocNo14 ";
            string sql10 = " select * from [tb_健康档案_既往病史]    where [" + tb_健康档案_既往病史.个人档案编号 + "]=@DocNo15 ";
            string sql11 = " select * from [tb_健康档案_健康状态]    where [" + tb_健康档案_健康状态.个人档案编号 + "]=@DocNo16 ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9 + sql10 + sql11);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, date.Trim());
            cmd.AddParam("@DocNo3", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo4", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo5", SqlDbType.VarChar, date.Trim());
            cmd.AddParam("@DocNo6", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo7", SqlDbType.VarChar, date.Trim());
            cmd.AddParam("@DocNo8", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo9", SqlDbType.VarChar, date.Trim());
            cmd.AddParam("@DocNo10", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo11", SqlDbType.VarChar, date.Trim());
            cmd.AddParam("@DocNo12", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo13", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo14", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo15", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo16", SqlDbType.VarChar, docNo.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康体检.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;
            ds.Tables[2].TableName = "住院史";
            ds.Tables[3].TableName = "家庭病床史";
            ds.Tables[4].TableName = tb_健康体检_用药情况表.__TableName;
            ds.Tables[5].TableName = tb_健康体检_非免疫规划预防接种史.__TableName;
            ds.Tables[6].TableName = tb_MXB高血压管理卡.__TableName;
            ds.Tables[7].TableName = tb_MXB糖尿病管理卡.__TableName;
            ds.Tables[8].TableName = tb_MXB慢病基础信息表.__TableName;
            ds.Tables[9].TableName = tb_健康档案_既往病史.__TableName;
            ds.Tables[10].TableName = tb_健康档案_健康状态.__TableName;

            return ds;
        }
        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool DeleteBYDocNoAndDate(string docNo, string date,string _ID)
        {
            //删除单据:从子表开始删除!!!
            string sql1 = "delete [tb_健康体检_用药情况表] where [" + tb_健康体检_用药情况表.个人档案编号 + "]='" + docNo + "' and DATEDIFF(day,创建时间 ,'" + date + "')=0 ";//删除子表
            string sql2 = "delete [tb_健康体检_住院史] where [" + tb_健康体检_住院史.个人档案编号 + "]='" + docNo + "' and 创建日期 = '" + date + "' ";//删除子表
            string sql3 = "delete [tb_健康体检_非免疫规划预防接种史] where [" + tb_健康体检_非免疫规划预防接种史.个人档案编号 + "]='" + docNo + "' and 创建日期 = '" + date + "' ";//删除子表
            string sql4 = "delete [tb_健康体检] where [" + tb_健康体检.ID + "]='" + _ID + "' ";//删除子表
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4);
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }
        public override SaveResult Update(DataSet data)
        {
            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果

            //string mGUID = string.Empty;
            //string mDocNo = string.Empty;

            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();

            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");

            try
            {
                //更新所有资料表
                foreach (DataTable dt in data.Tables)
                {
                    //仅处理有作修改的资料表
                    if (dt.GetChanges() == null) continue;

                    //根据资料表名获取SQL命令生成器
                    IGenerateSqlCommand gen = this.CreateSqlGenerator(dt.TableName);
                    if (gen == null) continue; //当前数据层无法识别的SQL命令生成器，则不更新当前资料表

                    //使用基于ADO构架的SqlClient组件更新数据
                    SqlDataAdapter adp = new SqlDataAdapter();
                    adp.RowUpdating += new SqlRowUpdatingEventHandler(OnAdapterRowUpdating);
                    adp.UpdateCommand = GetUpdateCommand(gen, _CurrentTrans);
                    adp.InsertCommand = GetInsertCommand(gen, _CurrentTrans);
                    adp.DeleteCommand = GetDeleteCommand(gen, _CurrentTrans);
                    adp.Update(dt);
                }

                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务       

            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      

                mResult.SetException(ex); //保存结果设置异常消息

                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }

            return mResult; //返回保存结果
        }
        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "健康体检");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportDataByLRSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region MyRegion

                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
         select  region.机构编号 as rgid,region.机构名称,a.总数  
         from tb_机构信息 region 
         left join (
             select  jktj.所属机构, count(jktj.所属机构) as 总数 
             from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
             where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                 and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
                 and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
                 or jktj.所属机构 like @机构 +'%'  )
                 group by jktj.所属机构 
             ) a on region.机构编号=a.所属机构 
             where 
                 (region.状态 = '' or region.状态 is null) 
                 and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
                 or region.机构编号 like @机构 +'%')		
              ) as tab1 
         left join (
             select   jktj.所属机构, count(jktj.所属机构) as 合格数 
             from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
             where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
                         and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
                 and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
                 or jktj.所属机构 like @机构 +'%')  
                 and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                 group by jktj.所属机构					
         ) as tab2 on tab1.rgid = tab2.所属机构 
             left join (
                 select  
                     jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
                 from  tb_健康档案  jkjc   ,tb_健康体检  jktj
                 where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
                     and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
                     and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
                     or jktj.所属机构 like @机构 +'%') 
                     group by jktj.所属机构 
             ) as tab6 on tab1.rgid = tab6.所属机构
             left join (
                 select 
                     jktj.所属机构 , 
                     AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
                 from  tb_健康档案  jkjc   ,tb_健康体检  jktj
                 where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
                     and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
                     and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
                     or jktj.所属机构 like @机构 +'%')  
                     group by jktj.所属机构 
             ) as tab7 on tab1.rgid = tab7.所属机构
             left join (
                 select 
                     jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
                 from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_健康体检 tj
                 where da.档案状态='1' 
                     and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
                     and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
                     or jkjc.转出机构 like @机构 +'%')  
                     group by jkjc.转出机构 
             ) as tab8 on tab1.rgid = tab8.转出机构
             left join (
                 select 
                     jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
                 from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_健康体检 tj
                 where da.档案状态='1'  
                     and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
                     and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
                     or jkjc.转入机构 like @机构 +'%')  
                     group by jkjc.转入机构 
             ) as tab9 on tab1.rgid = tab9.转入机构 
             ) a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
where a.所属上级机构 <> '371323'
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";

                #endregion
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

                ds.Tables[0].TableName = "#产前一次随访信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                       and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                      and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            #endregion
            return ds;
        }
        public DataSet GetReportDataByDCSJ(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region MyRegion

                sql = @"select 
	所属上级机构 as 区域编码,
	c.机构名称 as 区域名称,
	sum(a.总数) as 总数,
	sum(a.合格数) as 合格数 ,
	sum(a.总数)-sum(a.合格数) as 不合格数,
	case when sum(a.总数)= 0 then 0 else  convert(dec(18,2),sum(a.合格数)*100.0/sum(a.总数) ) end as  合格率,
	avg(a.平均完整度) 平均完整度 ,
	sum(a.转入档案数)as 转入档案数,
	sum(a.转出档案数)as 转出档案数,
    --sum(a.更新数) as 更新数 
    sum(a.非活动档案数) as 非活动档案数
	
 from (
select   
	sum(a.总数) 总数,
	sum(a.合格数) 合格数 ,
	sum(a.转入档案数)转入档案数,
    sum(a.转出档案数)转出档案数,
    avg(a.平均完整度) 平均完整度 ,
    sum(a.非活动档案数) as 非活动档案数,
	case when  (len(@机构) = 6 and len(a.区域编码) >6) then   substring(a.区域编码,1,7)+'1'+substring(a.区域编码,9,4)
		else a.区域编码 end as 所属上级机构
	--a.区域编码  as 所属上级机构
    from(
select 
       tab1.rgid 区域编码,tab1.机构名称 区域名称,
         isnull(tab1.总数,0) 总数,
         isnull(tab2.合格数,0) 合格数,
         --convert(dec(18,2),case when tab2.合格数 is null then 0 else tab2.合格数*100.0/tab1.总数 end) 合格率,
         isnull(tab7.平均完整度,0) 平均完整度,
         isnull(tab6.非活动档案数,0) 非活动档案数,
         isnull(tab9.转入档案数,0) 转入档案数,
         isnull(tab8.转出档案数,0) 转出档案数
     from (
         select  region.机构编号 as rgid,region.机构名称,a.总数  
	                            from tb_机构信息 region 
	                            left join (
		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
		                            from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
			                            and jktj.体检日期 >= (@DateFrom) and jktj.体检日期 <= (@DateTo)				
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
			                            or jktj.所属机构 like @机构 +'%'  )
			                            group by jktj.所属机构 
		                            ) a on region.机构编号=a.所属机构 
		                            where 
			                            (region.状态 = '' or region.状态 is null) 
			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
			                            or region.机构编号 like @机构 +'%')		
		                             ) as tab1 
	                            left join (
		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
		                            from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
					                            and jktj.体检日期 >= (@DateFrom)  and jktj.体检日期 <= (@DateTo)			
			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
			                            or jktj.所属机构 like @机构 +'%')  
			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
			                            group by jktj.所属机构					
	                            ) as tab2 on tab1.rgid = tab2.所属机构 
		                            left join (
			                            select  
				                            jktj.所属机构 , count(jktj.所属机构) as 非活动档案数  
			                            from  tb_健康档案  jkjc   ,tb_健康体检  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态 = '2' 
				                            and jktj.体检日期 >= (@DateFrom)  and jktj.体检日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%') 
				                            group by jktj.所属机构 
		                            ) as tab6 on tab1.rgid = tab6.所属机构
		                            left join (
			                            select 
				                            jktj.所属机构 , 
				                            AVG(cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3))) 平均完整度
			                            from  tb_健康档案  jkjc   ,tb_健康体检  jktj
			                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
				                            and jktj.体检日期 >= (@DateFrom)  and jktj.体检日期 <= (@DateTo)			
				                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%'  ) 
				                            or jktj.所属机构 like @机构 +'%')  
				                            group by jktj.所属机构 
		                            ) as tab7 on tab1.rgid = tab7.所属机构
		                            left join (
			                            select 
				                            jkjc.转出机构, count(jkjc.转出机构) as 转出档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_健康体检 tj
			                            where da.档案状态='1' 
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转出机构)>=15 and substring(jkjc.转出机构,1,7)+'1'+substring(jkjc.转出机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转出机构 like @机构 +'%')  
				                            group by jkjc.转出机构 
		                            ) as tab8 on tab1.rgid = tab8.转出机构
		                            left join (
			                            select 
				                            jkjc.转入机构, count(jkjc.转入机构) as 转入档案数 
			                            from  tb_转档申请  jkjc  , tb_健康档案  da ,tb_健康体检 tj
			                            where da.档案状态='1'  
				                            and jkjc.个人档案编号 = da.个人档案编号 and jkjc.个人档案编号 = tj.个人档案编号
				                            and ((len(jkjc.转入机构)>=15 and substring(jkjc.转入机构,1,7)+'1'+substring(jkjc.转入机构,9,7) like @机构 +'%' ) 
				                            or jkjc.转入机构 like @机构 +'%')  
				                            group by jkjc.转入机构 
		                            ) as tab9 on tab1.rgid = tab9.转入机构 
             ) a group by a.区域编码
	) a left join  tb_机构信息   c
                    on c.机构编号 = a.所属上级机构
            where a.所属上级机构 <> '371323'
	group by a.所属上级机构	 ,c. 机构名称
		  order by 所属上级机构 
";

                #endregion
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#产前一次随访信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                        and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,jkjc.性别,jkjc.出生日期,jkjc.身份证号,
jkjc.联系人电话,jktj.所属机构,jktj.创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                                        and jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                       and (
		                                (len(@机构)>6 and len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%') 
		                                or (len(@机构)>6 and len(jkjc.所属机构)>=12 and jkjc.所属机构 like @机构 +'%')
		                                or(jkjc.所属机构 = @机构)
		                                )
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                        ";
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            #endregion
            return ds;
        }
        public DataSet GetReportDataDetail(string DocNo机构, string DocNo完整度, string DateFrom, string DateTo, string Type, string searchType, string SearchBy)
        {
            string sql = string.Empty;
            DataSet ds = null;
            #region 查询语句

            if (Type == "1")
            {
                #region MyRegion

                //                sql = @"select 
                //	                          tab1.rgid 区域编码,tab1.机构名称 区域名称,
                //                                isnull(tab1.总数,0) 总数,isnull(tab2.合格数,0) 合格数,isnull(tab1.总数,0)-isnull(tab2.合格数,0) 不合格数,
                //								isnull(tab10.老年人已体检,0) 老年人已体检,
                //								isnull(tab11.老年人未体检,0)老年人未体检,
                //								isnull(tab12.高血压已体检,0)高血压已体检,
                //								isnull(tab13.高血压未体检,0)高血压未体检,
                //								isnull(tab14.糖尿病已体检,0)糖尿病已体检,
                //								isnull(tab15.糖尿病未体检,0)糖尿病未体检
                //                            from (
                //	                            select  region.机构编号 as rgid,region.机构名称,a.总数  
                //	                            from tb_机构信息 region 
                //	                            left join (
                //		                            select  jktj.所属机构, count(jktj.所属机构) as 总数 
                //		                            from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
                //		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  
                //			                            and jktj.创建时间 >= (@DateFrom) and jktj.创建时间 <= (@DateTo)				
                //			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7)  like @机构 +'%'  )
                //			                            or jktj.所属机构 like @机构 +'%'  )
                //			                            group by jktj.所属机构 
                //		                            ) a on region.机构编号=a.所属机构 
                //		                            where 
                //			                            (region.状态 = '' or region.状态 is null) 
                //			                            and ((len(region.机构编号)>=15 and substring(region.机构编号,1,7)+'1'+substring(region.机构编号,9,7) like @机构 +'%' ) 
                //			                            or region.机构编号 like @机构 +'%')		
                //		                             ) as tab1 
                //	                            left join (
                //		                            select   jktj.所属机构, count(jktj.所属机构) as 合格数 
                //		                            from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
                //		                            where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1' 
                //					                            and jktj.创建时间 >= (@DateFrom)  and jktj.创建时间 <= (@DateTo)			
                //			                            and ((len(jktj.所属机构)>=15 and substring(jktj.所属机构,1,7)+'1'+substring(jktj.所属机构,9,7) like @机构 +'%' ) 
                //			                            or jktj.所属机构 like @机构 +'%')  
                //			                            and cast(case ISNULL(jktj.完整度,'') when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                //			                            group by jktj.所属机构					
                //	                            ) as tab2 on tab1.rgid = tab2.所属机构 
                //									      left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 老年人已体检 
                //			                            from  tb_老年人基本信息  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab10 on tab1.rgid = tab10.所属机构 
                //											      left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 老年人未体检 
                //			                            from  tb_老年人基本信息  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab11 on tab1.rgid = tab11.所属机构 
                //									  left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 高血压已体检 
                //			                            from  tb_MXB高血压管理卡  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab12 on tab1.rgid = tab12.所属机构 
                //											      left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 高血压未体检 
                //			                            from  tb_MXB高血压管理卡  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab13 on tab1.rgid = tab13.所属机构 
                //											  left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 糖尿病已体检 
                //			                            from  tb_MXB糖尿病管理卡  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab14 on tab1.rgid = tab14.所属机构 
                //											      left join (
                //			                            select 
                //				                            jkjc.所属机构, count(1) as 糖尿病未体检 
                //			                            from  tb_MXB糖尿病管理卡  jkjc  , tb_健康档案  da
                //			                            where jkjc.个人档案编号 = da.个人档案编号 
                //										and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo))
                //										and da.档案状态='1'  
                //				                            and ((len(jkjc.所属机构)>=15 and substring(jkjc.所属机构,1,7)+'1'+substring(jkjc.所属机构,9,7) like @机构 +'%' ) 
                //				                         or jkjc.所属机构 like @机构 +'%')  
                //				                            group by jkjc.所属机构 
                //		                            ) as tab15 on tab1.rgid = tab15.所属机构 
                //									order by tab1.rgid
                //";

                #endregion

                //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                //cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                //cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                //cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //DataTable table = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "");
                //ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                switch (searchType)
                {
                    //查询全部数据
                    case "1":
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_NEW");
                        break;
                    case "2"://老年人
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_老年人");
                        break;
                    case "3"://健康人
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_健康人");
                        break;
                    case "4"://慢病
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_慢病");
                        break;
                    case "5"://贫困人口 2018年5月9日 yufh 添加
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_贫困人口");
                        break;
                    default:
                        ds = DocNoTool.Get体检详细数据(DocNo机构, DocNo完整度, DateFrom, DateTo, SearchBy, "USP_GET体检详细_NEW");
                        break;
                }

                //ds.Tables[0].TableName = "#产前一次随访信息统计";
            }
            else if (Type == "2")
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,(select P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = jkjc.性别)性别,jkjc.出生日期,jkjc.身份证号,jkjc.街道,jkjc.居委会,jkjc.居住地址,
jkjc.联系人电话,jkjc.联系人姓名,jkjc.本人电话,(select 机构名称 from tb_机构信息 where 机构编号 = jktj.所属机构)所属机构,
(select UserName from tb_MyUser where 用户编码 = jktj.创建人) 创建人,jktj.创建时间 from  tb_健康档案  jkjc  ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  ";
                if (SearchBy == "1")
                {
                    sql += @"  and  jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                              order by jktj.创建时间 desc ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"  and  jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
  order by jktj.创建时间 desc";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "3")//合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,(select P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = jkjc.性别)性别,jkjc.出生日期,jkjc.身份证号,jkjc.街道,jkjc.居委会,jkjc.居住地址,
jkjc.联系人电话,jkjc.联系人姓名,jkjc.本人电话,(select 机构名称 from tb_机构信息 where 机构编号 = jktj.所属机构)所属机构,
(select UserName from tb_MyUser where 用户编码 = jktj.创建人) 创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  ";
                if (SearchBy == "1")
                {
                    sql += @"    and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                                                                order by jktj.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @"    and jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) >=@完整度
                                                                                order by jktj.体检日期 desc";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "4")//不合格
            {
                sql = @"select jktj.ID,  jkjc.个人档案编号,jkjc.家庭档案编号,jkjc.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = jkjc.性别)性别,jkjc.出生日期,jkjc.身份证号,
						jkjc.街道,jkjc.居委会,jkjc.居住地址,
jkjc.联系人电话,jkjc.联系人姓名,jkjc.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = jktj.所属机构)所属机构,
(select top 1 UserName from tb_MyUser where 用户编码 = jktj.创建人) 创建人,jktj.创建时间 from  tb_健康档案  jkjc   ,tb_健康体检  jktj  
                                        where jkjc.个人档案编号 = jktj.个人档案编号 and jkjc.档案状态='1'  ";
                if (SearchBy == "1")
                {
                    sql += @"and jktj.创建时间 >= (@DateFrom)
                                        and jktj.创建时间 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                                                                order by jktj.创建时间 desc";
                }
                else if (SearchBy == "2")
                {

                    sql += @"and jktj.体检日期 >= (@DateFrom)
                                        and jktj.体检日期 <= (@DateTo)				
                                        and ((len(jktj.所属机构)>=15 and jktj.所属机构 like @机构 +'%') 
                                        or jktj.所属机构 like @机构 +'%')
                                        and cast(case ISNULL(jktj.完整度,'')  
                                        when '' then '0' else jktj.完整度 end as decimal(8,3)) <@完整度
                                                                                order by jktj.体检日期 desc";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "5")//老年人已体检
            {
                sql = @"      select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,
(select top 1 UserName from tb_MyUser where 用户编码 = tj.创建人) 创建人,tj.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_健康体检 tj left join  tb_健康档案  da  on tj.个人档案编号 = da.个人档案编号
                          where 1=1 ";
                if (SearchBy == "1")
                {
                    sql += @"and tj.创建时间 >= (@DateFrom)  and tj.创建时间 <= (@DateTo)	
                        and da.档案状态='1'  
                        and isnull(da.出生日期,'') <> ''  and da.出生日期 <= convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31' 
                        and da.所属机构 = @机构 
                                                                              order by tj.创建时间 desc  ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"and tj.体检日期 >= (@DateFrom)  and tj.体检日期 <= (@DateTo)	
                        and da.档案状态='1'  
                        and isnull(da.出生日期,'') <> ''  and da.出生日期 <= convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                         and da.所属机构 = @机构  
                                        order by tj.体检日期 desc";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "6")//老年人未体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,
(select top 1 UserName from tb_MyUser where 用户编码 = da.创建人) 创建人,da.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_健康档案  da";
                if (SearchBy == "1")
                {
                    sql += @"  where da.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                        and da.档案状态='1'  
                        and isnull(da.出生日期,'') <> ''  and da.出生日期 <= convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'  
                        and da.所属机构 = @机构  
                        order by da.创建时间 desc  ";
                }
                else if (SearchBy == "2")
                {
                    sql += @"  where da.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 体检日期 >= (@DateFrom)  and 体检日期 <= (@DateTo)		 )
                        and da.档案状态='1'  
                        and isnull(da.出生日期,'') <> ''  and da.出生日期 <= convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                          and da.所属机构 = @机构 
                        order by da.创建时间 desc  ";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "7")//高血压已体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,
(select top 1 UserName from tb_MyUser where 用户编码 =tj.创建人) 创建人,tj.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_健康体检 tj  left join  tb_健康档案  da on tj.个人档案编号 = da.个人档案编号 
                          where 1=1 and da.档案状态='1'   
						  and exists (select top 1 个人档案编号 from tb_MXB高血压管理卡 where tb_MXB高血压管理卡.个人档案编号 = da.个人档案编号) 
                          and exists (select 机构编号 from tb_机构信息 jg where da.所属机构 = jg.机构编号 and 机构编号=@机构)  ";
                if (DocNo完整度.Equals("合格"))
                {
                    sql += @" and (isnull(血压右侧1,'')<>'' and isnull(血压右侧2,'')<>'' and isnull(脑血管疾病,'')<>'' and isnull(肾脏疾病,'')<>'' and isnull(心脏疾病,'')<>'' 
	  and isnull(眼部疾病,'')<>'' and isnull(神经系统疾病,'')<>'' and isnull(其他系统疾病,'')<>'' and isnull(健康评价,'')<>'' and isnull(危险因素控制,'')<>'' 
	  and isnull(吸烟状况,'')<>'' and isnull(饮酒频率,'')<>'' and isnull(锻炼频率,'')<>'' and isnull(饮食习惯,'')<>'' and isnull(体温,0)<>0 
	  and isnull(脉搏,0)<>0 and isnull(呼吸,0)<>0 and isnull(身高,0)<>0 and isnull(体重,0)<>0 and isnull(腰围,0)<>0 and isnull(皮肤,'')<>'' 
	  and isnull(淋巴结,'')<>'' and isnull(心率,0)<>0 and isnull(心律,'')<>'' and isnull(杂音,'')<>'' and isnull(桶状胸,'')<>'' and isnull(呼吸音,'')<>'' 
	  and isnull(罗音,'')<>'' and isnull(压痛,'')<>'' and isnull(包块,'')<>'' and isnull(肝大,'')<>'' and isnull(脾大,'')<>'' and isnull(浊音,'')<>'' 
	  and isnull(口唇,'')<>'' and isnull(齿列,'')<>'' and isnull(咽部,'')<>'' and isnull(右眼视力,0)<>0 and isnull(左眼视力,0)<>0 and isnull(听力,'')<>'' 
	  and isnull(运动功能,'')<>'')  ";
                }
                else if (DocNo完整度.Equals("不合格"))
                {
                    sql += @" and (isnull(血压右侧1,'')='' or isnull(血压右侧2,'')='' or isnull(脑血管疾病,'')='' or isnull(肾脏疾病,'')='' or isnull(心脏疾病,'')='' 
	  or isnull(眼部疾病,'')='' or isnull(神经系统疾病,'')='' or isnull(其他系统疾病,'')='' or isnull(健康评价,'')='' or isnull(危险因素控制,'')='' 
	  or isnull(吸烟状况,'')='' or isnull(饮酒频率,'')='' or isnull(锻炼频率,'')='' or isnull(饮食习惯,'')='' or isnull(体温,0)=0  
	  or isnull(脉搏,0)=0 or isnull(呼吸,0)=0 or isnull(身高,0)=0 or isnull(体重,0)=0 or isnull(腰围,0)=0 or isnull(皮肤,'')='' 
	  or isnull(淋巴结,'')='' or isnull(心率,0)=0 or isnull(心律,'')='' or isnull(杂音,'')='' or isnull(桶状胸,'')='' or isnull(呼吸音,'')='' 
	  or isnull(罗音,'')='' or isnull(压痛,'')='' or isnull(包块,'')='' or isnull(肝大,'')='' or isnull(脾大,'')='' or isnull(浊音,'')='' 
	  or isnull(口唇,'')='' or isnull(齿列,'')='' or isnull(咽部,'')='' or isnull(右眼视力,0)=0 or isnull(左眼视力,0)=0 or isnull(听力,'')='' 
	  or isnull(运动功能,'')='')  ";
                }
                if (SearchBy == "1")
                {
                    sql += @" and  tj.创建时间 >= (@DateFrom)  and tj.创建时间 <= (@DateTo) order by tj.创建时间 desc      ";
                }
                else if (SearchBy == "2")
                {
                    sql += @" and  tj.体检日期 >= (@DateFrom)  and tj.体检日期 <= (@DateTo) order by tj.体检日期 desc";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "8")//高血压未体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,
                     (select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                      da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,
                      (select top 1 UserName from tb_MyUser where 用户编码 = da.创建人) 创建人,da.创建时间,
	                    da.居住地址
                          from  tb_MXB高血压管理卡  jkjc  , tb_健康档案  da ";
                if (SearchBy == "1")
                {
                    sql += @"      where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                        and da.档案状态='1'  
                             and da.所属机构 = @机构  
                                        order by da.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @"      where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 体检日期 >= (@DateFrom)  and 体检日期 <= (@DateTo)		 )
                        and da.档案状态='1'  
                             and da.所属机构 = @机构   
                                       order by da.创建时间 desc ";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "9")//糖尿病已体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1 UserName from tb_MyUser where 用户编码 = tj.创建人) 创建人,tj.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_健康体检 tj  left join  tb_健康档案  da on tj.个人档案编号 = da.个人档案编号";
                if (SearchBy == "1")
                {
                    sql += @" where  tj.创建时间 >= (@DateFrom)  and tj.创建时间 <= (@DateTo)
						   and exists (select 个人档案编号 from tb_MXB糖尿病管理卡 where tb_MXB糖尿病管理卡.个人档案编号 = da.个人档案编号)
                        and da.档案状态='1'  
                         and da.所属机构 = @机构   
                                        order by  tj.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where  tj.体检日期 >= (@DateFrom)  and tj.体检日期 <= (@DateTo)
						   and exists (select 个人档案编号 from tb_MXB糖尿病管理卡 where tb_MXB糖尿病管理卡.个人档案编号 = da.个人档案编号)
                        and da.档案状态='1'  
                         and da.所属机构 = @机构 
                                        order by  tj.体检日期  desc ";
                }


                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "10")//糖尿病未体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称  from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1 UserName from tb_MyUser where 用户编码 = da.创建人) 创建人,da.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_MXB糖尿病管理卡  jkjc  , tb_健康档案  da";
                if (SearchBy == "1")
                {
                    sql += @" where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                        and da.档案状态='1'  
                            and da.所属机构 = @机构 
                                        order by da.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 体检日期 >= (@DateFrom)  and 体检日期 <= (@DateTo)		 )
                        and da.档案状态='1'  
                            and da.所属机构 = @机构  
                                         order by da.创建时间 desc ";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "11")//健康人已体检
            {
                sql = @"  select 
                da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1 UserName from tb_MyUser where 用户编码 = tj.创建人) 创建人,tj.创建时间,
						da.街道,da.居委会,da.居住地址
            from tb_健康体检 tj left join   tb_健康档案  da on tj.个人档案编号 = da.个人档案编号";
                if (SearchBy == "1")
                {
                    sql += @" where da.档案状态='1'  
				and tj.创建时间 >= (@DateFrom)  and tj.创建时间 <= (@DateTo)
                 and da.所属机构 = @机构 
					and  not exists (select 个人档案编号 from [dbo].[tb_健康档案_既往病史] 
				where [tb_健康档案_既往病史].个人档案编号 = da.个人档案编号 and  疾病类型 = '疾病'
				)
            and isnull(da.出生日期,'') <> ''   and da.出生日期 > convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                                        order by tj.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where da.档案状态='1'  
				and tj.体检日期 >= (@DateFrom)  and tj.体检日期 <= (@DateTo)
                  and da.所属机构 = @机构 
					and  not exists (select 个人档案编号 from [dbo].[tb_健康档案_既往病史] 
				where [tb_健康档案_既往病史].个人档案编号 = da.个人档案编号 and  疾病类型 = '疾病'
				)
            and isnull(da.出生日期,'') <> ''  and da.出生日期 > convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                                        order by tj.体检日期 desc ";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "12")//健康人未体检
            {
                sql = @"  select 
                da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1  P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1  UserName from tb_MyUser where 用户编码 = da.创建人) 创建人,da.创建时间,
						da.街道,da.居委会,da.居住地址
            from   tb_健康档案  da ";
                if (SearchBy == "1")
                {
                    sql += @" where da.档案状态='1'  
                 and da.所属机构 = @机构 
					and  not exists (select 个人档案编号 from [dbo].[tb_健康档案_既往病史] 
				where [tb_健康档案_既往病史].个人档案编号 = da.个人档案编号 and  疾病类型 = '疾病'
				) and  da.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
            --and  da.创建时间>= (@DateFrom) and da.创建时间<= (@DateTo)
            and isnull(da.出生日期,'') <> ''   and da.出生日期 > convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                                        order by da.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where da.档案状态='1'  
                  and da.所属机构 = @机构 
					and  not exists (select 个人档案编号 from [dbo].[tb_健康档案_既往病史] 
				where [tb_健康档案_既往病史].个人档案编号 = da.个人档案编号 and  疾病类型 = '疾病'
				) and  da.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 体检日期 >= (@DateFrom)  and 体检日期 <= (@DateTo)		 )
            --and da.创建时间>= (@DateFrom) and da.创建时间<= (@DateTo)
            and isnull(da.出生日期,'') <> ''  and da.出生日期 > convert(varchar(10),year(dateadd(yy,-65,getdate())),120)+'-12-31'
                                        order by da.创建时间 desc ";
                }
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                cmd.SqlCommand.CommandTimeout = 0;
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "13")//重症精神疾病未体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称  from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1 UserName from tb_MyUser where 用户编码 = da.创建人) 创建人,da.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_精神疾病信息补充表  jkjc  , tb_健康档案  da";
                if (SearchBy == "1")
                {
                    sql += @" where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 创建时间 >= (@DateFrom)  and 创建时间 <= (@DateTo)		 )
                        and da.档案状态='1'  
                            and da.所属机构 = @机构 
                                        order by da.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where jkjc.个人档案编号 = da.个人档案编号 
                          and jkjc.个人档案编号 not in (select 个人档案编号 from tb_健康体检 where 体检日期 >= (@DateFrom)  and 体检日期 <= (@DateTo)		 )
                        and da.档案状态='1'  
                            and da.所属机构 = @机构  
                                         order by da.创建时间 desc ";
                }

                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            else if (Type == "14")//重症精神疾病已体检
            {
                sql = @" select     da.ID,  da.个人档案编号,da.家庭档案编号,da.姓名,(select top 1 P_DESC from tb_常用字典 where p_fun_code = 'xb_xingbie' and p_code = da.性别)性别,da.出生日期,da.身份证号,
                        da.联系人电话,da.联系人姓名,da.本人电话,(select top 1 机构名称 from tb_机构信息 where 机构编号 = da.所属机构)所属机构,(select top 1 UserName from tb_MyUser where 用户编码 = tj.创建人) 创建人,tj.创建时间,
						da.街道,da.居委会,da.居住地址
                          from  tb_健康体检 tj  left join  tb_健康档案  da on tj.个人档案编号 = da.个人档案编号";
                if (SearchBy == "1")
                {
                    sql += @" where  tj.创建时间 >= (@DateFrom)  and tj.创建时间 <= (@DateTo)
						   and exists (select 个人档案编号 from tb_精神疾病信息补充表 where tb_精神疾病信息补充表.个人档案编号 = da.个人档案编号)
                        and da.档案状态='1'  
                         and da.所属机构 = @机构   
                                        order by  tj.创建时间 desc";
                }
                else if (SearchBy == "2")
                {
                    sql += @" where  tj.体检日期 >= (@DateFrom)  and tj.体检日期 <= (@DateTo)
						   and exists (select 个人档案编号 from tb_精神疾病信息补充表 where tb_精神疾病信息补充表.个人档案编号 = da.个人档案编号)
                        and da.档案状态='1'  
                         and da.所属机构 = @机构 
                                        order by  tj.体检日期  desc ";
                }


                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                cmd.AddParam("@机构", SqlDbType.VarChar, DocNo机构);
                cmd.AddParam("@DateFrom", SqlDbType.VarChar, DateFrom);
                cmd.AddParam("@DateTo", SqlDbType.VarChar, DateTo);
                //cmd.AddParam("@完整度", SqlDbType.VarChar, DocNo完整度);
                ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
                ds.Tables[0].TableName = "#统计";
            }
            #endregion
            return ds;
        }
        /// <summary>
        /// 打印体检报表用的方法
        /// </summary>
        /// <param name="docNo">个人档案编号</param>
        /// <param name="date">体检时间</param>
        /// <returns></returns>
        public System.Data.DataSet GetReportDataByKey(string docNo, string date)
        {
            string sql1 = " select * from [tb_健康体检]    where [" + tb_健康体检.个人档案编号 + "]=@DocNo1 and 创建时间 = @DocNo2 ";
            string sql2 = " select * from [tb_健康档案_个人健康特征]    where [" + tb_健康档案_个人健康特征.个人档案编号 + "]=@DocNo1 ";
            string sql3 = " select * from [tb_健康体检_住院史] where [" + tb_健康体检_住院史.个人档案编号 + "]=@DocNo1 and 创建日期 = @DocNo2 and 类型 = '1'";
            string sql4 = " select * from [tb_健康体检_住院史] where [" + tb_健康体检_住院史.个人档案编号 + "]=@DocNo1 and 创建日期 = @DocNo2 and 类型 = '2'";
            string sql5 = " select * from [tb_健康体检_用药情况表] where [" + tb_健康体检_用药情况表.个人档案编号 + "]=@DocNo1 and DATEDIFF(day,创建时间 ,@DocNo2)=0";
            string sql6 = " select * from [tb_健康体检_非免疫规划预防接种史] where [" + tb_健康体检_非免疫规划预防接种史.个人档案编号 + "]=@DocNo1 and 创建日期 = @DocNo2";
            string sql7 = " select * from [tb_健康档案] where [" + tb_健康档案.个人档案编号 + "]=@DocNo1";
            string sql8 = " select * from [tb_健康档案_健康状态] where [" + tb_健康档案.个人档案编号 + "]=@DocNo1";
            string sql9 = " select * from tb_健康体检_结果反馈 where 反馈类型='tb_健康体检' and PID=(select ID from [tb_健康体检] where [" + tb_健康体检.个人档案编号 + "]=@DocNo1 and 创建时间 = @DocNo2) ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1 + sql2 + sql3 + sql4 + sql5 + sql6 + sql7 + sql8 + sql9);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, date.Trim());

            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_健康体检.__TableName;
            ds.Tables[1].TableName = tb_健康档案_个人健康特征.__TableName;
            ds.Tables[2].TableName = "住院史";
            ds.Tables[3].TableName = "家庭病床史";
            ds.Tables[4].TableName = tb_健康体检_用药情况表.__TableName;
            ds.Tables[5].TableName = tb_健康体检_非免疫规划预防接种史.__TableName;
            ds.Tables[6].TableName = tb_健康档案.__TableName;
            ds.Tables[7].TableName = tb_健康档案_健康状态.__TableName;
            ds.Tables[8].TableName = "tb_健康体检_结果反馈";
            //ds.Tables[8].TableName = tb_MXB慢病基础信息表.__TableName;
            //ds.Tables[9].TableName = tb_健康档案_既往病史.__TableName;

            return ds;
        }

        /// <summary>
        /// 获取复核情况
        /// </summary>
        /// <param name="DocNo机构"></param>
        /// <returns></returns>
        public DataSet GetReportDataByFH(string DocNo机构, string DateFrom, string DateTo)
        {
            using (SqlConnection sqlConn = DataProvider.Instance.CreateConnection(Loginer.CurrentUser.DBName))
            {
                SqlCommand sqlCommand = new SqlCommand("USP_GET复核升级统计", sqlConn);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@机构", System.Data.SqlDbType.VarChar));
                sqlCommand.Parameters["@机构"].Value = DocNo机构;
                sqlCommand.Parameters.Add(new SqlParameter("@DateFrom", System.Data.SqlDbType.VarChar));
                sqlCommand.Parameters["@DateFrom"].Value = DateFrom;
                sqlCommand.Parameters.Add(new SqlParameter("@DateTo", System.Data.SqlDbType.VarChar));
                sqlCommand.Parameters["@DateTo"].Value = DateTo;
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                sqlCommand.CommandTimeout = 0;
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                return dataSet;
            }
        }

        //Begin WXF 2018-11-01 | 19:26
        //根据档案号返回所有发生时间 
        public DataTable Get_发生时间(string grdah)
        {
            string sql = " select 体检日期 from [tb_健康体检]   where [" + tb_健康体检.个人档案编号 + "]=@DocNo1 order by 体检日期 desc";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康体检.__TableName);
            return dt;
        }
        //End


        //Begin WXF 2018-11-04 | 02:20
        //根据档案号与体检日期获取一条数据
        public DataRow Get_One体检(string grdah, string str_CreateDT)
        {
            string sql = " select top 1 * from [tb_健康体检]   where [" + tb_健康体检.个人档案编号 + "]=@DocNo1 and [" + tb_健康体检.体检日期 + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, str_CreateDT.Trim());
            DataRow dr = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_健康体检.__TableName).Rows[0];
            return dr;
        }
        //End
        public DataTable Get_图像采集(string grdah, string str_date)
        {
            string sql = " select top 1 图像 from tb_健康体检_图像采集 where 个人档案编号=@DocNo1 and 体检日期=@DocNo2 order by ID desc ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, str_date.Trim());
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_健康体检_图像采集");
            return dt;
        }
        public DataTable Get_B超报告(string grdah, string str_date)
        {
            string sql = " select top 1 * from tb_健康体检_B超 where 个人档案编号=@DocNo1 and 检查日期=@DocNo2 order by ID desc ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, grdah.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, str_date.Trim());
            DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_健康体检_B超");
            return dt;
        }

    }
}

