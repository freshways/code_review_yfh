///*************************************************************************/
///*
///* 文件名    ：dalBase.cs                                 
///* 程序说明  : 数据层基类
///* 原创作者  ：ATOM 
///* 
///* Copyright ©  2015 GGBond
///**************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AtomEHR.ORM;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_System;

namespace AtomEHR.Server.DataAccess.DAL_Base
{
    /// <summary>
    /// 数据访问层基类定义
    /// </summary>
    public class dalBase
    {
        /// <summary>
        /// 当前登录的用户信息
        /// </summary>
        protected Loginer _Loginer = null;

        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录的用户</param>
        public dalBase(Loginer loginer)
        {
            _Loginer = loginer;
        }

        /// <summary>
        /// 生成删除命令
        /// </summary>
        /// <param name="gen">SQL 生成器</param>
        /// <param name="tran">事务</param>
        /// <returns>返回SqlCommand对象</returns>
        protected SqlCommand GetDeleteCommand(IGenerateSqlCommand gen, SqlTransaction tran)
        {
            return gen.GetDeleteCommand(tran);
        }

        /// <summary>
        /// 生成资料更新命令
        /// </summary>
        /// <param name="gen">SQL 生成器</param>
        /// <param name="tran">事务</param>
        /// <returns>返回SqlCommand对象</returns>
        protected SqlCommand GetUpdateCommand(IGenerateSqlCommand gen, SqlTransaction tran)
        {
            return gen.GetUpdateCommand(tran);
        }

        /// <summary>
        /// 生成插入记录命令
        /// </summary>
        /// <param name="gen">SQL 生成器</param>
        /// <param name="tran">事务</param>
        /// <returns>返回SqlCommand对象</returns>
        protected SqlCommand GetInsertCommand(IGenerateSqlCommand gen, SqlTransaction tran)
        {
            return gen.GetInsertCommand(tran);
        }

        /// <summary>
        /// 日期类型需要转换
        /// </summary>
        protected void OnAdapterRowUpdating(object sender, SqlRowUpdatingEventArgs e)
        {
            foreach (SqlParameter p in e.Command.Parameters)
            {
                if (p.SqlDbType != SqlDbType.DateTime) continue;
                if (string.IsNullOrEmpty(p.Value.ToString()))
                { p.Value = DBNull.Value; continue; }
                if (DateTime.Parse(p.Value.ToString()).ToString("yyyyMMdd") == DateTime.MinValue.ToString("yyyyMMdd"))
                { p.Value = DBNull.Value; continue; }
            }
        }

        /// <summary>
        /// DataTable转换为DataSet
        /// </summary>
        /// <param name="table">DataTable</param>
        /// <returns></returns>
        public static DataSet TableToDataSet(DataTable table)
        {
            if (table.DataSet != null)
                return table.DataSet;
            else
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(table);
                return ds;
            }
        }
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual DataSet Get分页数据(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return DocNoTool.Get分页数据(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }

        /// <summary>
        /// 查询分页数据,Yfuh添加
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual DataSet Get分页数据New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return DocNoTool.Get分页数据New(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }

        /// <summary>
        /// wxf 2018年10月13日 16:28:54
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="Fields"></param>
        /// <param name="strWhere"></param>
        /// <param name="SortExpression"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <param name="SortDire"></param>
        /// <returns></returns>
        public virtual DataSet Get分页数据New_New(String tableName, String Fields, String strWhere, String SortExpression, Int32 pageSize, Int32 pageIndex, out Int32 count, String SortDire)
        {
            return DocNoTool.Get分页数据New_New(tableName, Fields, strWhere, SortExpression, pageSize, pageIndex, out count, SortDire);
        }
        /// <summary>
        /// 查询全部数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="Fields"></param>
        /// <param name="strWhere"></param>
        /// <param name="SortExpression"></param>
        /// <param name="SortDire"></param>
        /// <returns></returns>
        public virtual DataTable Get全部数据(String tableName, String Fields, String strWhere, String SortExpression, String SortDire)
        {
            DataTable dt = DocNoTool.Get全部数据(tableName, Fields, strWhere, SortExpression, SortDire);
            return dt;
        }
        #region 事务控制

        /// <summary>
        /// 用户自己启用事务控制模式，在保存数据前设置此参数。
        /// </summary>
        protected bool _UserManualControlTrans = false;

        /// <summary>
        /// 当前启用的事务
        /// </summary>
        protected SqlTransaction _CurrentTrans = null;

        /// <summary>
        /// 启用事务控制
        /// </summary>
        protected virtual void BeginTransaction()
        {
            SqlConnection conn = DataProvider.Instance.CreateConnection(_Loginer.DBName);
            _CurrentTrans = conn.BeginTransaction();
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        protected virtual void CommitTransaction()
        {
            try
            {
                if (_CurrentTrans != null)
                {
                    _CurrentTrans.Commit();
                    _CurrentTrans = null;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 事务回滚
        /// </summary>
        protected virtual void RollbackTransaction()
        {
            try
            {
                if (_CurrentTrans != null)
                {
                    _CurrentTrans.Rollback();
                    _CurrentTrans = null;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }

}
