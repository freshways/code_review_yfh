﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.Common;

namespace AtomEHR.Server.DataAccess
{
    public class Interface_获取化验信息
    {
        public static DataTable Get化验室检查信息(SqlConnection conn, string s身份证号, string s化验日期)
        {
            //changed by wjz 20170409 调整读取化验结果的功能 ▽
            //string SQL = @"select fitem_code,fitem_name,fitem_unit,fitem_ref,fitem_badge,forder_by,fvalue,fname_j  ,sfzh,fjy_date
            //                    from vw_InterFace_Result  where  sfzh=@身份证号 and  fjy_date=@化验日期 ";
            string SQL = @"select fitem_code,fitem_name,fitem_unit,fitem_ref,fitem_badge,forder_by,fvalue,fname_j  ,sfzh,fjy_date
                                from vw_InterFace_Result  where sfzh!='' AND sfzh=@身份证号 and  fjy_date=@化验日期 ";
            //changed by wjz 20170409 调整读取化验结果的功能 △
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@身份证号", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@化验日期", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@身份证号"].Value = s身份证号;
                cmd.Parameters["@化验日期"].Value = s化验日期;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("#LISResult");
                adapter.Fill(dt);
                return dt;
            }
        }

        public static DataTable Get化验室检查信息列表(SqlConnection conn, string s身份证号)
        {
            string SQL = @"select * from vw_InterFace_ResultList where 身份证=@身份证号  order by 化验日期 desc";
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@身份证号", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@身份证号"].Value = s身份证号;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                return dt;
            }
        }

        //add by wjz 20170409 调整读取化验结果的功能 ▽
        public static DataTable Get化验室检查信息列表(SqlConnection conn, string s身份证号, string s档案号)
        {
            string SQL = @"select * from vw_InterFace_ResultList where 身份证!='' AND (身份证=@身份证号 or 身份证=@档案号)  order by 化验日期 desc";
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@身份证号", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@档案号", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@身份证号"].Value = ChangeSFZH(s身份证号);
                cmd.Parameters["@档案号"].Value = s档案号;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataSet = new DataTable();
                adapter.Fill(dataSet);
                return dataSet;
            }
        }

        //15位身份证号转18位。返回是否转换成功，true:转换成功，false：转换失败
        private static string ChangeSFZH(string oldsfzh)
        {
            string temp = "";
            string newsfzh = oldsfzh;

            try
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(oldsfzh, @"^\d{15}$"))
                {
                    temp = oldsfzh.Substring(0, 6) + "19" + oldsfzh.Substring(6);

                    int[] i系数 = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
                    int[] iSFZH = new int[17];
                    for (int index = 0; index < 17; index++)
                    {
                        iSFZH[index] = temp[index] - 48;
                    }

                    //将17位数字分别和系数相乘，然后结果相加
                    int sum = 0;
                    for (int index = 0; index < 17; index++)
                    {
                        sum += iSFZH[index] * i系数[index];
                    }

                    //用加出来和除以11，取余数
                    int mod = sum % 11;
                    char[] c检验码 = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };

                    newsfzh = temp + c检验码[mod];
                }
            }
            catch (Exception ex)
            {
                newsfzh = oldsfzh;
            }

            return newsfzh;
        }
        //add by wjz 20170409 调整读取化验结果的功能 △

        //yufh 2017-06-01 22:09:32 添加 体检回执单用
        public static DataTable GetLIs数据(SqlConnection conn, string 项目, string 条码, string 体检日期)
        {
            string sql = @"select * from View_GWCT where 化验仪器 =@项目 and sfzh = @条码 and 日期 =@体检日期 ";

            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@项目", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@条码", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@体检日期", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@项目"].Value = 项目;
                cmd.Parameters["@条码"].Value = 条码;
                cmd.Parameters["@体检日期"].Value = 体检日期;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataSet = new DataTable();
                adapter.Fill(dataSet);

                return dataSet;
            }
        }

        //Begin WXF 2018-11-06 | 15:08
        //获取血常规
        public static DataTable Get_血常规(SqlConnection conn, string str_idNum, string str_examineDate)
        {
            string sql = @" select * from vw_血常规 where 身份证号=@idNum and 检验日期=@examineDate ";

            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@idNum", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@examineDate", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@idNum"].Value = str_idNum;
                cmd.Parameters["@examineDate"].Value = str_examineDate;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                return dt;
            }
        }

        public static DataTable Get_血生化(SqlConnection conn, string str_idNum, string str_examineDate)
        {
            string sql = @" select * from vw_生化 where 身份证号=@idNum and 检验日期=@examineDate ";

            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@idNum", System.Data.SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@examineDate", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@idNum"].Value = str_idNum;
                cmd.Parameters["@examineDate"].Value = str_examineDate;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                return dt;
            }
        }

        //End


    }
}
