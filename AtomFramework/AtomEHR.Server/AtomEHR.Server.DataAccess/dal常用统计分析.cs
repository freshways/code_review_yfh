﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_System;
using System.Data.SqlClient;

namespace AtomEHR.Server.DataAccess
{
    public class dal常用统计分析
    {
        Loginer _Loginer = new Loginer();
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal常用统计分析(Loginer loginer)
        {
            _Loginer = loginer;
        }

        public DataSet Get重复统计(string RGID, string begindate, string enddate,string ctype)
        {
            string sql = "USP_手机号重复统计";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@prgid", RGID));
            cmd.Parameters.Add(new SqlParameter("@sDate", begindate));
            cmd.Parameters.Add(new SqlParameter("@eDate", enddate));
            cmd.Parameters.Add(new SqlParameter("@ctype", ctype));

            DataSet dtData = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, cmd);

            return dtData;
        }

        public DataSet Get疾病人群统计分析(string RGID, string begindate, string enddate)
        {
            string sql = "USP_GET疾病统计分析";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@机构", RGID));
            cmd.Parameters.Add(new SqlParameter("@DateFrom", begindate));
            cmd.Parameters.Add(new SqlParameter("@DateTo", enddate));
            cmd.Parameters.Add(new SqlParameter("@完整度", "100"));
            cmd.Parameters.Add(new SqlParameter("@SearchBy", "1"));

            DataSet dtData = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, cmd);

            return dtData;
        }

        public DataTable GetDataTable(string SQL)
        {
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(SQL);

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "#ls");
        }

    }
}
