﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.ORM;
using AtomEHR.Interfaces;
using AtomEHR.Server.DataAccess.DAL_Base;

namespace AtomEHR.Server.DataAccess.DAL_DataDict
{
    [DefaultORM_UpdateMode(typeof(tb_AccountIDs), true)]
    public class dalAccountIDs : dalBaseDataDict
    {
        public dalAccountIDs(Loginer loginer)
            : base(loginer)
        {
            _KeyName = tb_AccountIDs.__KeyName; //主键字段
            _TableName = tb_AccountIDs.__TableName;//表名
            _ModelType = typeof(tb_AccountIDs);
        }
    }
}
