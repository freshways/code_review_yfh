﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: IDCard_AreaCode_Old的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018-12-06 05:47:04
 *   最后修改: 2018-12-06 05:47:04
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_IDCard_AreaCode_Old的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_IDCard_AreaCode_Old), true)]
    public class dalIDCard_AreaCode_Old : dalBaseDataDict
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dalIDCard_AreaCode_Old(Loginer loginer)
            : base(loginer)
        {
            _KeyName = tb_IDCard_AreaCode_Old.__KeyName; //主键字段
            _TableName = tb_IDCard_AreaCode_Old.__TableName;//表名
            _ModelType = typeof(tb_IDCard_AreaCode_Old);
        }

        /// <summary>
        /// 根据表名获取该表的SQL命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_IDCard_AreaCode_Old.__TableName) ORM = typeof(tb_IDCard_AreaCode_Old);
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByObjectClass(ORM);
        }

        //Begin WXF 2018-12-06 | 19:41
        //@@UpdateValue 
        /// <summary>
        /// 根据地区编码获取地区详细行政区划信息
        /// </summary>
        /// <param name="AreaCode"></param>
        /// <returns></returns>
        public DataTable GetAreaInfo(string AreaCode)
        {
            string sql = @"select * from " + tb_IDCard_AreaCode_Old.__TableName + " where 编码=@AreaCode";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@AreaCode", SqlDbType.NChar, AreaCode);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_IDCard_AreaCode_Old.__TableName);
        }
        //End
    }
}
