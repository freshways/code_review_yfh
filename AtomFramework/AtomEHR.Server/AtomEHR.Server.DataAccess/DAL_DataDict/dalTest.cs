﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: Test的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-08 03:48:46
 *   最后修改: 2015-07-08 03:48:46
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_Test的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_Test), true)]
    public class dalTest : dalBaseDataDict
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dalTest(Loginer loginer)
            : base(loginer)
        {
            _KeyName = tb_Test.__KeyName; //主键字段
            _TableName = tb_Test.__TableName;//表名
            _ModelType = typeof(tb_Test);
        }

        /// <summary>
        /// 根据表名获取该表的SQL命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_Test.__TableName) ORM = typeof(tb_Test);
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByObjectClass(ORM);
        }

    }
}
