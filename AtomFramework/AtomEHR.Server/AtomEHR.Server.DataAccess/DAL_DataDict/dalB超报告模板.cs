﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: B超报告模板的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-04-12 12:06:35
 *   最后修改: 2019-04-12 12:06:35
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_B超报告模板的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_B超报告模板), true)]
    public class dalB超报告模板 : dalBaseDataDict
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalB超报告模板(Loginer loginer): base(loginer)
         {
             _KeyName = tb_B超报告模板.__KeyName; //主键字段
             _TableName = tb_B超报告模板.__TableName;//表名
             _ModelType = typeof(tb_B超报告模板);
         }

         /// <summary>
         /// 根据表名获取该表的SQL命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
           Type ORM = null;
           if (tableName == tb_B超报告模板.__TableName) ORM = typeof(tb_B超报告模板);
           if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
           return new GenerateSqlCmdByTableFields(ORM);
         }

         public override DataTable GetSummaryData()
         {
             string sql = "SELECT * FROM tb_B超报告模板  ";
             SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
             //cmd.AddParam("@RGID", SqlDbType.VarChar, Loginer.CurrentUser.所属机构);
             return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_B超报告模板");
         }

         /// <summary>
         /// 通用筛选接口方法
         /// </summary>
         /// <param name="attributeCodes"></param>
         /// <returns></returns>
         public DataTable F模糊查询(string attributeCodes)
         {
             StringBuilder sb = new StringBuilder();
             sb.Append("SELECT TOP 100 ID,检查部位,超声提示,超声所见,备注,所属机构,创建人,创建日期,是否停用 FROM tb_B超报告模板 where 1=1");
             sb.Append(" and (超声提示 like'" + attributeCodes + "%') ");
             sb.Append(" and (所属机构 in(select 机构编号 from fn_GetRGID('" + Loginer.CurrentUser.所属机构 + "')) or isnull(所属机构,'')='') ");

             SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());

             return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_B超报告模板.__TableName);
         }

     }
}
