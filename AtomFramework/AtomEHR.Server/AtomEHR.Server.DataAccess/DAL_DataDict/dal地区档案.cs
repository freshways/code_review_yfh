﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 地区档案的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015/09/21 08:22:03
 *   最后修改: 2015/09/21 08:22:03
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_地区档案的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_地区档案), true)]
    public class dal地区档案 : dalBaseDataDict
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dal地区档案(Loginer loginer)
             : base(loginer)
         {
            _KeyName = tb_地区档案.__KeyName; //主键字段
            _TableName = tb_地区档案.__TableName;//表名
            _ModelType = typeof(tb_地区档案);
         }

         /// <summary>
         /// 根据表名获取该表的SQL命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_地区档案.__TableName) ORM = typeof(tb_地区档案);
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM); //注意：这个方法有两种，根据生成的模板选择（和GenerateSqlCmdByObjectClass注意区分）2017-03-07 12:53:45 yufh 被坑后标记 如果是静态类就用这个，如果是实体类就用GenerateSqlCmdByObjectClass
         }

         public override DataTable GetSummaryData()
         {
             string sql = "SELECT * FROM tb_地区档案 WHERE 地区编码 like @RGID+'%' ";
             SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
             cmd.AddParam("@RGID", SqlDbType.VarChar, Loginer.CurrentUser.单位代码);
             return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_地区档案");
         }

         /// <summary>
         /// 根据地区获取新的用户编号
         /// 2015-12-29 19:54:42 yufh添加
         /// </summary>
         /// <param name="RegionID">地区ID</param>
         /// <returns></returns>
         public string GetNew地区编码(string RegionID)
         {
             string sql = "";
             if (RegionID.Length >= 12)
             {
                 sql = @"select substring(@RegionID,1,7)+'2'+substring(@RegionID,9,7)+
	                            stuff('000',len('000')-len(count(机构编号))+1,
	                            len(count(机构编号)),isnull(max(substring(机构编号,len(机构编号)-1,2)),0)+1)
	                            from tb_机构信息 
	                            where 上级机构 = @RegionID ";
             }
             else
             {
                 sql = @"select @RegionID+'B1'+
	                            stuff('0000',len('0000')-len(count(机构编号))+1,
	                            len(count(机构编号)),isnull(max(substring(机构编号,len(机构编号)-1,2)),0)+1)
	                            from tb_机构信息 
	                            where 上级机构 = @RegionID ";
             }
             SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
             cmd.AddParam("@RegionID", SqlDbType.VarChar, RegionID);
             object 用户编号 = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
             return 用户编号 != null ? 用户编号.ToString() : "";
         }

         /// <summary>
         /// 自定义查询方法
         /// </summary>
         public DataTable SearchBy(string CustomerFrom, string CustomerTo, string Name, string Attribute)
         {
             StringBuilder sb = new StringBuilder();
             sb.Append(@"SELECT * FROM tb_地区档案 a WHERE 1=1 ");

             if (String.IsNullOrEmpty(CustomerFrom) == false)
                 sb.Append(" AND 地区编码>='" + CustomerFrom + "'");

             if (String.IsNullOrEmpty(CustomerTo) == false)
                 sb.Append(" AND 地区编码<='" + CustomerTo + "'");

             if (String.IsNullOrEmpty(Name) == false)
                 sb.Append(" AND (地区名称 LIKE '%" + Name + "%' OR 地区名称 LIKE '%" + Name + "%')");

             SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
             return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_地区档案.__TableName);

         }

        /// <summary>
        /// 根据地区ID获取地区名称,例如根据省id,获取省的名称
        /// </summary>
        /// <param name="地区ID"></param>
        /// <returns></returns>
          public string Get名称By地区ID(string 地区ID)
          {
              string sql2 = "select 地区名称 From [tb_地区档案] where [" + tb_地区档案.地区编码 + "]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, 地区ID.Trim());
              object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              if (obj != null)
              {
                  return obj.ToString();
              }
              else
              {
                  return "";
              }
          }

     }
}

