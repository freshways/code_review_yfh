﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 常用字典的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018-11-07 06:21:25
 *   最后修改: 2018-11-07 06:21:25
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_常用字典的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_常用字典), true)]
    public class dal常用字典 : dalBaseDataDict
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dal常用字典(Loginer loginer): base(loginer)
         {
             _KeyName = tb_常用字典.__KeyName; //主键字段
             _TableName = tb_常用字典.__TableName;//表名
             _ModelType = typeof(tb_常用字典);
         }

         /// <summary>
         /// 根据表名获取该表的SQL命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
           Type ORM = null;
           if (tableName == tb_常用字典.__TableName) ORM = typeof(tb_常用字典);
           if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
           return new GenerateSqlCmdByObjectClass(ORM);
         }

         //Begin WXF 2018-11-07 | 16:18
         //翻译常用字典
         public string DicMap(string P_FUN_CODE, string P_CODE)
         {
             string sql = " select * from [tb_常用字典]    where [" + tb_常用字典.P_FUN_CODE + "]='" + P_FUN_CODE + "' and [" + tb_常用字典.P_CODE + "]='" + P_CODE + "' ";

             DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, sql, tb_常用字典.__TableName);
             return dt.Rows[0][tb_常用字典.P_DESC].ToString();
         }
        //End

     }
}
