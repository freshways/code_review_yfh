﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 医生信息的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018-11-02 04:46:01
 *   最后修改: 2018-11-02 04:46:01
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
    /// <summary>
    /// tb_医生信息的数据访问层
    /// </summary>
    [DefaultORM_UpdateMode(typeof(tb_医生信息), true)]
    public class dal医生信息 : dalBaseDataDict
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dal医生信息(Loginer loginer)
            : base(loginer)
        {
            _KeyName = tb_医生信息.__KeyName; //主键字段
            _TableName = tb_医生信息.__TableName;//表名
            _ModelType = typeof(tb_医生信息);
        }

        /// <summary>
        /// 根据表名获取该表的SQL命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_医生信息.__TableName) ORM = typeof(tb_医生信息);
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        public override DataTable GetSummaryData()
        {
            string sql = "SELECT top 100 * FROM tb_医生信息 WHERE (s所属机构 = @RGID OR SUBSTRING(s所属机构,1,7)+'1'+SUBSTRING(s所属机构,9,7) LIKE @RGID+'%') ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RGID", SqlDbType.VarChar, Loginer.CurrentUser.所属机构);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_医生信息");
        }

        /// <summary>
        /// 根据地区获取新的编号
        /// 2015-12-29 19:54:42 yufh添加
        /// </summary>
        /// <param name="RegionID">地区ID</param>
        /// <returns></returns>
        public string GetNew医生编码(string RegionID)
        {
            string sql = "";
            if (RegionID.Length >= 12)
            {
                sql = @"select substring(@RegionID,1,7)+'2'+substring(@RegionID,9,7)+
	                            stuff('000',len('000')-len(count(b编码))+1,
	                            len(count(b编码)),isnull(max(substring(b编码,len(b编码)-1,2)),0)+1)
	                            from tb_医生信息 
	                            where s所属机构 = @RegionID ";
            }
            else
            {
                sql = @"select @RegionID+'B1'+ stuff('0000',len('0000')-len(count(b编码))+1,
	                            len(count(b编码)),isnull(max(substring(b编码,len(b编码)-1,2)),0)+1)
	                            from tb_医生信息 
	                            where s所属机构 = @RegionID ";
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RegionID", SqlDbType.VarChar, RegionID);
            object 医生编号 = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return 医生编号 != null ? 医生编号.ToString() : "";
        }

        /// <summary>
        /// 自定义查询方法
        /// </summary>
        public DataTable SearchBy(string _RGID, string _科室, string _医生姓名, string _查体模块)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT * FROM tb_医生信息 a WHERE 1=1 ");

            if (String.IsNullOrEmpty(_RGID) == false)
                sb.Append(" AND " + tb_医生信息.s所属机构 + "='" + _RGID + "'");

            if (String.IsNullOrEmpty(_科室) == false)
                sb.Append(" AND " + tb_医生信息.k科室 + "='" + _科室 + "'");

            if (String.IsNullOrEmpty(_医生姓名) == false)
                sb.Append(" AND (x医生姓名 LIKE '%" + _医生姓名 + "%')");

            if (String.IsNullOrEmpty(_查体模块) == false)
                sb.Append(" AND c查体模块='" + _查体模块 + "'");

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_医生信息.__TableName);
        }

        public DataTable Get手签By机构(string RGID)
        {
            string sql = "SELECT * FROM tb_医生信息 WHERE (s所属机构 = @RGID OR SUBSTRING(s所属机构,1,7)+'1'+SUBSTRING(s所属机构,9,7) LIKE @RGID+'%') ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@RGID", SqlDbType.VarChar, RGID);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_医生信息");
        }

        //Begin WXF 2018-12-24 | 17:25
        //医师签字绑定用 
        public DataTable LookUp医师签字(string Org)
        {
            string sql = "select b编码,x医生姓名 from tb_医生信息 where s所属机构 in(select 机构编号 from tb_机构信息 where 机构编号=@Org or 上级机构=Org)";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@Org", SqlDbType.VarChar, Org);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_医生信息.__TableName);
        }

        public string GetBmp手签(string code)
        {
            string sql = "select s手签 from tb_医生信息 where b编码=@code";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@code", SqlDbType.VarChar, code);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_医生信息.__TableName).Rows[0][tb_医生信息.s手签].ToString();
        }
        //End

        //Begin WXF 2019-01-22 | 14:33
        //公共查询窗口模糊查询
        public DataTable F模糊查询(string attributeCodes)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select b编码 as 编码,x医生姓名 as 医生姓名,x性别 as 性别,c出生日期 as 出生日期,z职务 as 职务,k科室 as 科室,s身份证号 as 身份证号 from tb_医生信息 where 1=1 ");
            sb.Append(" and (s所属机构 like'" + Loginer.CurrentUser.所属机构 + "%' or s所属机构 in(select 机构编号 from tb_机构信息 where 上级机构='" + Loginer.CurrentUser.所属机构 + "')) ");
            sb.Append(" and (b编码 like '%" + attributeCodes + "%' or x医生姓名 like '%" + attributeCodes + "%') ");

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());

            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
        }
        //End

    }
}
