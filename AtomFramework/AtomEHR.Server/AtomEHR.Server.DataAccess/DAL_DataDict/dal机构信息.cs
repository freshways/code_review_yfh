﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: 机构信息的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2015-07-15 11:20:59
 *   最后修改: 2015-07-15 11:20:59
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.Server.DataAccess
{
	/// <summary>
	/// tb_机构信息的数据访问层
	/// </summary>
	[DefaultORM_UpdateMode(typeof(tb_机构信息), true)]
	public class dal机构信息 : dalBaseDataDict
	{
		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="loginer">当前登录用户</param>
		public dal机构信息(Loginer loginer)
			: base(loginer)
		{
			_KeyName = tb_机构信息.__KeyName; //主键字段
			_TableName = tb_机构信息.__TableName;//表名
			_ModelType = typeof(tb_机构信息);
		}

		/// <summary>
		/// 根据表名获取该表的SQL命令生成器
		/// </summary>
		/// <param name="tableName">表名</param>
		/// <returns></returns>
		protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
		{
			Type ORM = null;
			if (tableName == tb_机构信息.__TableName) ORM = typeof(tb_机构信息);
			if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
			return new GenerateSqlCmdByObjectClass(ORM);
		}

		public override DataTable GetSummaryData()
		{
			string sql = "SELECT * FROM tb_机构信息 WHERE ( [机构编号] = @RGID OR SUBSTRING([机构编号],1,7)+'1'+SUBSTRING([机构编号],9,7) LIKE @RGID+'%') ";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
			cmd.AddParam("@RGID", SqlDbType.VarChar, Loginer.CurrentUser.所属机构);
			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "tb_机构信息");
		}

		/// <summary>
		/// 根据机构获取新的用户编号
		/// 2015-12-29 19:54:42 yufh添加
		/// </summary>
		/// <param name="RegionID">机构ID</param>
		/// <returns></returns>
		public string GetNew机构编号(string RegionID)
		{
			string sql = "";
			if (RegionID.Length >= 12)
			{
				sql = @"select substring(@RegionID,1,7)+'2'+substring(@RegionID,9,7)+
								stuff('000',len('000')-len(count(机构编号))+1,
								len(count(机构编号)),isnull(max(substring(机构编号,len(机构编号)-1,2)),0)+1)
								from tb_机构信息 
								where 上级机构 = @RegionID ";
			}
			else
			{
				sql = @"select @RegionID+'B1'+
								stuff('0000',len('0000')-len(count(机构编号))+1,
								len(count(机构编号)),isnull(max(substring(机构编号,len(机构编号)-1,2)),0)+1)
								from tb_机构信息 
								where 上级机构 = @RegionID ";
			}
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
			cmd.AddParam("@RegionID", SqlDbType.VarChar, RegionID);
            object 用户编号 = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return 用户编号 != null ? 用户编号.ToString() : "";
		}

		/// <summary>
		/// 自定义查询方法
		/// </summary>
		public DataTable SearchBy(string CustomerFrom, string CustomerTo, string Name, string Attribute)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(@"SELECT a.*,b.NativeName 机构级别名称 FROM tb_机构信息 a 
			 left join tb_CommonDataDict b on a.机构级别 = b.DataCode and b.DataType='20' WHERE 1=1 ");

			if (String.IsNullOrEmpty(CustomerFrom) == false)
				sb.Append(" AND 机构编号>='" + CustomerFrom + "'");

			if (String.IsNullOrEmpty(CustomerTo) == false)
				sb.Append(" AND 机构编号<='" + CustomerTo + "'");

			if (String.IsNullOrEmpty(Name) == false)
				sb.Append(" AND (机构名称 LIKE '%" + Name + "%' OR 机构名称 LIKE '%" + Name + "%')");

			if (String.IsNullOrEmpty(Attribute) == false)
				sb.Append(" AND 机构类型('," + Attribute + ",',AttributeCodes)>0 ");

			if (_Loginer.FlagAdmin != "Y")
				sb.Append(" AND (机构编号 ='" + _Loginer.所属机构 + "' or 上级机构='" + _Loginer.所属机构 + "') ");

			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);

		}

		public DataTable F模糊查询(string attributeCodes)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("select 机构编号,机构名称,负责人,联系人,联系电话 from tb_机构信息 where 1=1 ");
			sb.Append(" and (机构编号 like'" + Loginer.CurrentUser.所属机构 + "%' or 机构编号 in(select 机构编号 from tb_机构信息 where 上级机构='" + Loginer.CurrentUser.所属机构 + "')) ");
			sb.Append("and (机构编号 like '%" + attributeCodes + "%' or 机构名称 like '%" + attributeCodes + "%') ");

			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());

			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
		}

		//根据机构编号获取此机构下的所有子机构（含孙机构等，一直递归向下）
		//public DataTable Get机构树(string str机构编号)
		public DataTable Get机构树()
		{
			string str机构编号 = _Loginer.所属机构;
			string strSql = @";with f as
									(
									select 机构编号, 机构名称, 上级机构  from tb_机构信息 where 机构编号=@机构编号
									union all
									select a.机构编号, a.机构名称,a.上级机构 from tb_机构信息 a, f where a.上级机构=f.机构编号
									)
									select 机构编号, 机构名称, 上级机构 from tb_机构信息 where 机构编号 in(select 机构编号 from f)
									order by 上级机构, 机构编号";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@机构编号", SqlDbType.VarChar, str机构编号);

			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
		}
		public DataTable Get机构树(string rgid = "")
		{
			string str机构编号 = string.IsNullOrEmpty(rgid) ? _Loginer.所属机构 : rgid;
			string strSql = @";with f as
									(
									select 机构编号, 机构名称, 上级机构  from tb_机构信息 where 机构编号=@机构编号
									union all
									select a.机构编号, a.机构名称,a.上级机构 from tb_机构信息 a, f where a.上级机构=f.机构编号
									)
									select 机构编号, 机构名称, 上级机构 from tb_机构信息 where 机构编号 in(select 机构编号 from f)
									order by 上级机构, 机构编号";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@机构编号", SqlDbType.VarChar, str机构编号);

			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
		}

		public DataTable Get机构(string rgid)
		{
			string strSql = @"select 机构编号, 机构名称 from tb_机构信息 where 机构编号!='371321C10001' and 机构编号!='371321C10002' and 上级机构 =@jgbh
							  order by 机构编号";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@jgbh", SqlDbType.VarChar, rgid);

			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
		}

		public string Get上级机构(string rgid)
		{
			string strSql = @"select 上级机构 from tb_机构信息 where 机构编号=@jgbh ";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@jgbh", SqlDbType.VarChar, rgid);
			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName).Rows[0][0].ToString();
		}

		/// <summary>
		/// 根据子机构编号获取该子机构所在乡镇医院的编码
		/// </summary>
		/// <param name="rgid"></param>
		/// <returns>如果是县级机构，则返回6位编码，否则返回所属医院编码</returns>
		public string Get所在乡镇编码(string rgid)
		{
			string str机构编号 = string.IsNullOrEmpty(rgid) ? _Loginer.所属机构 : rgid;

			if (str机构编号.Length <= 6)
			{
				return str机构编号;
			}

			string strSql = @";with f as
  (
	select 机构编号, 上级机构  from tb_机构信息 where 机构编号=@机构编号
	 union all
	select a.机构编号, a.上级机构 from tb_机构信息 a, f where a.机构编号=f.上级机构
  )
  select 机构编号
  from tb_机构信息 
  where 机构编号 in(select 机构编号 from f) and 上级机构=@卫计局编号
  order by 上级机构, 机构编号";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@机构编号", SqlDbType.VarChar, str机构编号);
			cmd.AddParam("@卫计局编号", SqlDbType.VarChar, str机构编号.Substring(0, 6));

			DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
			if (dt != null && dt.Rows.Count > 0)
			{
				return dt.Rows[0]["机构编号"].ToString();
			}
			else
			{
				return str机构编号;
			}
		}

		//通知通告用 
		public DataTable Get_卫生院名称(string rgid)
		{
			string strSql = @"select 机构编号,机构名称 from tb_机构信息 where 上级机构=@rgid or 机构编号=@rgid";
			SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
			cmd.AddParam("@rgid", SqlDbType.VarChar, rgid);
			return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
		}

        public DataTable Get_机构名称(string rgid)
        {
            string strSql = @"select 机构名称 from tb_机构信息 where 机构编号=@rgid";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(strSql);
            cmd.AddParam("@rgid", SqlDbType.VarChar, rgid);
            return DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_机构信息.__TableName);
        }
    }
}
