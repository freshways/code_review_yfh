﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtomEHR.Interfaces.Interfaces_Bridge;
using System.Data;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.Common;
using AtomEHR.Server.DataAccess.DAL_Base;
using System.Data.SqlClient;



namespace AtomEHR.Server.DataAccess
{
    public class dalCom : dalBaseBusiness
    {
        public dalCom(Loginer loginer)
            : base(loginer)
        { }
        /// <summary>
        /// 从服务器中获取系统时间
        /// </summary>
        /// <returns></returns>
        public string GetDateTimeFromDBServer()
        {
            string date = null;
            string query = "select convert(varchar(20), getdate(), 120)";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dtDate = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, "获取系统时间");

            if (dtDate != null && dtDate.Rows.Count > 0)
            {
                date = dtDate.Rows[0][0].ToString();
            }
            return date;
        }

        //计算年龄
        //日期的格式必须限定为yyyy-MM-dd HH:mm:ss
        public int GetAgeByBirthDay(string strBirthday)
        {
            int returnAge = 0;
            char[] cArr = { '-', ' ', ':' };
            string[] strBirthdayArr = strBirthday.Split(cArr);

            int birthYear = Convert.ToInt32(strBirthdayArr[0]);
            int birthMonth = Convert.ToInt32(strBirthdayArr[1]);
            int birthDay = Convert.ToInt32(strBirthdayArr[2]);

            string date = "";
            try
            {
                date = GetDateTimeFromDBServer();
            }
            catch (Exception ex)
            {
                date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }

            int nowYear = Convert.ToInt32(date.Substring(0, 4));
            int nowMonth = Convert.ToInt32(date.Substring(5, 2));
            int nowDay = Convert.ToInt32(date.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;

                if (ageDiff > 0)
                {
                    if (nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;
                        if (dayDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;
                        if (monthDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                }
                else
                {
                    returnAge = -1;
                }
            }
            return returnAge;
        }

        //计算年龄
        //日期的格式必须限定为yyyy-MM-dd HH:mm:ss
        public int GetAgeByBirthDay(string strBirthday, string beginDate)
        {
            int returnAge = 0;
            char[] cArr = { '-', ' ', ':' };
            string[] strBirthdayArr = strBirthday.Split(cArr);

            int birthYear = Convert.ToInt32(strBirthdayArr[0]);
            int birthMonth = Convert.ToInt32(strBirthdayArr[1]);
            int birthDay = Convert.ToInt32(strBirthdayArr[2]);

            int nowYear = Convert.ToInt32(beginDate.Substring(0, 4));
            int nowMonth = Convert.ToInt32(beginDate.Substring(5, 2));
            int nowDay = Convert.ToInt32(beginDate.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;

                if (ageDiff > 0)
                {
                    if (nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;
                        if (dayDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;
                        if (monthDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                }
                else
                {
                    returnAge = -1;
                }
            }
            return returnAge;
        }


        public DataSet Get工作提醒(string sdate, string edate, string strWhere)
        {
            SqlConnection conn = DataProvider.Instance.CreateConnection(_Loginer.DBName);
            return DocNoTool.Get工作提醒(conn, sdate, edate, strWhere);
        }

        public DataSet Get工作提醒详细(string sdate, string edate, string strWhere, string type)
        {
            SqlConnection conn = DataProvider.Instance.CreateConnection(_Loginer.DBName);
            return DocNoTool.Get工作提醒详细(conn, sdate, edate, strWhere, type);
        }

        public DataTable Get姓名(string tableName ,string where="")
        {
            //TODO: XIUGAI XINGING 
            string query = "select 个人档案编号 ,姓名 as 加密姓名 ,''  as 解密姓名 from  " + tableName + " " + where;
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataTable dtDate = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tableName);
            return dtDate;
            //if (dtDate != null && dtDate.Rows.Count > 0)
            //{
            //    date = dtDate.Rows[0][0].ToString();
            //}
            //return date;
        }

        public SaveResult Get解密(DataTable table, string tableName)
        {
            SaveResult mResult = SaveResult.CreateDefault(); //预设保存结果
            //非用户手动事务模式，预设启用事务
            if (_UserManualControlTrans == false) this.BeginTransaction();
            if (_CurrentTrans == null) throw new Exception("用户手动控制事务模式下，但您没有启用事务！");
            try
            {
                string sql = string.Empty;
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    string dabh = table.Rows[i]["个人档案编号"].ToString();
                    string 解密姓名 = table.Rows[i]["解密姓名"].ToString();
                    string 加密姓名 = table.Rows[i]["加密姓名"].ToString();
                    sql = string.Format("update {0} set 姓名 = '{1}' where 个人档案编号 = '{2}'  ", tableName, 解密姓名, dabh);

                    SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
                    //int k = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
                    //_CurrentTrans.Commit
                    DataProvider.Instance.ExecuteNoQuery(_CurrentTrans, sql);
                }
                
                if (_UserManualControlTrans == false) this.CommitTransaction(); //提交事务  
            }
            catch (Exception ex)
            {
                if (_UserManualControlTrans == false) this.RollbackTransaction();//回滚事务      
                mResult.SetException(ex); //保存结果设置异常消息
                throw new Exception("更新数据发生错误！Event:Update()\r\n\r\n" + ex.Message);
            }
            return mResult; //返回保存结果
        }

        public DataSet GetLogData(string strWhere)
        {
            string query = @"SELECT a.[isid]
                                  ,[LogUser]
                                  ,b.username	as 操作人
                                    ,c.机构名称
                                  ,[LogDate]	as 操作时间
                                  ,substring([KeyFieldName],0,
										case when charindex('&',[KeyFieldName])=0 then len([KeyFieldName])+1
											else charindex('&',[KeyFieldName])
											end 
                                  )	 as 操作对象
                                  ,'删除 ' + case tablename when   'tb_MXB冠心病管理卡' then '冠心病管理卡，发生时间：' 
                                  + substring([KeyFieldName],charindex('&',[KeyFieldName])+1,len([KeyFieldName])) 
			                            when 'tb_MXB脑卒中管理卡' then '脑卒中管理卡，发生时间：' 
                                  + substring([KeyFieldName],charindex('&',[KeyFieldName])+1,len([KeyFieldName])) 
			                            when 'tb_健康档案' then '个人健康档案'
			                            when 'tb_MXB高血压管理卡' then '高血压管理卡，发生时间：' 
                                  + substring([KeyFieldName],charindex('&',[KeyFieldName])+1,len([KeyFieldName])) 
			                            when 'tb_MXB糖尿病管理卡' then '糖尿病管理卡，发生时间：' 
                                  + substring([KeyFieldName],charindex('&',[KeyFieldName])+1,len([KeyFieldName])) 
			                            end as 操作内容
                                  ,[DocNo]
                              FROM [AtomEHR.YSDB].[dbo].[tb_Log] a
                                left join tb_myuser b on a.loguser = b.用户编码
                                left join dbo.tb_机构信息 c on b.所属机构 = c.机构编号
                             where 1=1  " + strWhere;
            //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
            DataSet dtDate = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, query);
            return dtDate;
        }

        public DataSet Get管理率By机构ID(string jgid)
        {
            string sql = "GET管理率By机构编号";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@机构",jgid));

            DataSet dtData = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, cmd);

            return dtData;
        }

        public DataSet Get动态管理率(string RGID,string begindate, string enddate)
        {
            string sql = "GET动态使用率";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@机构", RGID));
            cmd.Parameters.Add(new SqlParameter("@begin", begindate));
            cmd.Parameters.Add(new SqlParameter("@end", enddate));

            DataSet dtData = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, cmd);

            return dtData;
        }

        public DataSet Get档案汇总统计(string RGID, string begindate, string enddate)
        {
            string sql = "USP_GET档案汇总统计2";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@机构", RGID));
            cmd.Parameters.Add(new SqlParameter("@begin", begindate));
            cmd.Parameters.Add(new SqlParameter("@end", enddate));

            DataSet dtData = DataProvider.Instance.GetDataSet(Loginer.CurrentUser.DBName, cmd);

            return dtData;
        }
    }
}
