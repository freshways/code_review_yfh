﻿namespace DXTempLate
{
    partial class XtraFormUserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormUserInfo));
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.memoEditAddress = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtTel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditBM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleBtnImg = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditRemark = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxXL = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxZW = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditBirthday = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditSex = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleBtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxXL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditSex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.memoEditAddress);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.txtEmail);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.txtPhone);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.txtTel);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 324);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(573, 206);
            this.groupControl2.TabIndex = 8;
            this.groupControl2.Text = "联系方式";
            // 
            // memoEditAddress
            // 
            this.memoEditAddress.Location = new System.Drawing.Point(88, 142);
            this.memoEditAddress.Name = "memoEditAddress";
            this.memoEditAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoEditAddress.Properties.Appearance.Options.UseFont = true;
            this.memoEditAddress.Size = new System.Drawing.Size(473, 56);
            this.memoEditAddress.TabIndex = 24;
            this.memoEditAddress.UseOptimizedRendering = true;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(10, 142);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 20;
            this.labelControl13.Text = "联系地址：";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(88, 105);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(249, 26);
            this.txtEmail.TabIndex = 19;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(10, 110);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 14);
            this.labelControl12.TabIndex = 18;
            this.labelControl12.Text = "电子邮件：";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(88, 66);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtPhone.Properties.Appearance.Options.UseFont = true;
            this.txtPhone.Size = new System.Drawing.Size(249, 26);
            this.txtPhone.TabIndex = 17;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(34, 71);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(36, 14);
            this.labelControl11.TabIndex = 16;
            this.labelControl11.Text = "手机：";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(88, 27);
            this.txtTel.Name = "txtTel";
            this.txtTel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTel.Properties.Appearance.Options.UseFont = true;
            this.txtTel.Size = new System.Drawing.Size(249, 26);
            this.txtTel.TabIndex = 15;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(10, 33);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 14;
            this.labelControl10.Text = "办公电话：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(34, 216);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 27;
            this.labelControl7.Text = "部门：";
            // 
            // comboBoxEditBM
            // 
            this.comboBoxEditBM.EditValue = "";
            this.comboBoxEditBM.Location = new System.Drawing.Point(88, 213);
            this.comboBoxEditBM.Name = "comboBoxEditBM";
            this.comboBoxEditBM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxEditBM.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditBM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBM.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditBM.Size = new System.Drawing.Size(249, 26);
            this.comboBoxEditBM.TabIndex = 26;
            // 
            // simpleBtnImg
            // 
            this.simpleBtnImg.Location = new System.Drawing.Point(442, 209);
            this.simpleBtnImg.Name = "simpleBtnImg";
            this.simpleBtnImg.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnImg.TabIndex = 25;
            this.simpleBtnImg.Text = "浏览";
            this.simpleBtnImg.Click += new System.EventHandler(this.simpleBtnImg_Click);
            // 
            // memoEditRemark
            // 
            this.memoEditRemark.Location = new System.Drawing.Point(88, 249);
            this.memoEditRemark.Name = "memoEditRemark";
            this.memoEditRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoEditRemark.Properties.Appearance.Options.UseFont = true;
            this.memoEditRemark.Size = new System.Drawing.Size(473, 64);
            this.memoEditRemark.TabIndex = 23;
            this.memoEditRemark.UseOptimizedRendering = true;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(34, 248);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "备注：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(34, 183);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 20;
            this.labelControl6.Text = "职务：";
            // 
            // comboBoxXL
            // 
            this.comboBoxXL.EditValue = "";
            this.comboBoxXL.Location = new System.Drawing.Point(88, 142);
            this.comboBoxXL.Name = "comboBoxXL";
            this.comboBoxXL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxXL.Properties.Appearance.Options.UseFont = true;
            this.comboBoxXL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxXL.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxXL.Size = new System.Drawing.Size(249, 26);
            this.comboBoxXL.TabIndex = 19;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(34, 148);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 18;
            this.labelControl5.Text = "学历：";
            // 
            // comboBoxZW
            // 
            this.comboBoxZW.EditValue = "";
            this.comboBoxZW.Location = new System.Drawing.Point(88, 178);
            this.comboBoxZW.Name = "comboBoxZW";
            this.comboBoxZW.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxZW.Properties.Appearance.Options.UseFont = true;
            this.comboBoxZW.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxZW.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxZW.Size = new System.Drawing.Size(249, 26);
            this.comboBoxZW.TabIndex = 21;
            // 
            // dateEditBirthday
            // 
            this.dateEditBirthday.EditValue = null;
            this.dateEditBirthday.Location = new System.Drawing.Point(88, 106);
            this.dateEditBirthday.Name = "dateEditBirthday";
            this.dateEditBirthday.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dateEditBirthday.Properties.Appearance.Options.UseFont = true;
            this.dateEditBirthday.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditBirthday.Size = new System.Drawing.Size(249, 26);
            this.dateEditBirthday.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(10, 111);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "入职日期：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(34, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "性别：";
            // 
            // comboBoxEditSex
            // 
            this.comboBoxEditSex.EditValue = "";
            this.comboBoxEditSex.Location = new System.Drawing.Point(88, 69);
            this.comboBoxEditSex.Name = "comboBoxEditSex";
            this.comboBoxEditSex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxEditSex.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditSex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditSex.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditSex.Size = new System.Drawing.Size(249, 26);
            this.comboBoxEditSex.TabIndex = 14;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(88, 33);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtUserName.Properties.Appearance.Options.UseFont = true;
            this.txtUserName.Size = new System.Drawing.Size(249, 26);
            this.txtUserName.TabIndex = 13;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.comboBoxEditBM);
            this.groupControl1.Controls.Add(this.simpleBtnImg);
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Controls.Add(this.memoEditRemark);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.comboBoxZW);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.comboBoxXL);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.dateEditBirthday);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.comboBoxEditSex);
            this.groupControl1.Controls.Add(this.txtUserName);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(573, 324);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "基本资料";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::DXTempLate.Properties.Resources.People;
            this.pictureEdit1.Location = new System.Drawing.Point(387, 29);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(174, 178);
            this.pictureEdit1.TabIndex = 24;
            this.pictureEdit1.ToolTip = "可进行上传用户照片";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(22, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "用户名：";
            // 
            // simpleBtnCancel
            // 
            this.simpleBtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleBtnCancel.Location = new System.Drawing.Point(486, 541);
            this.simpleBtnCancel.Name = "simpleBtnCancel";
            this.simpleBtnCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnCancel.TabIndex = 6;
            this.simpleBtnCancel.Text = "取消";
            // 
            // simpleBtnOK
            // 
            this.simpleBtnOK.Location = new System.Drawing.Point(405, 541);
            this.simpleBtnOK.Name = "simpleBtnOK";
            this.simpleBtnOK.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnOK.TabIndex = 5;
            this.simpleBtnOK.Text = "保存";
            this.simpleBtnOK.Click += new System.EventHandler(this.simpleBtnOK_Click);
            // 
            // XtraFormUserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 568);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.simpleBtnCancel);
            this.Controls.Add(this.simpleBtnOK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XtraFormUserInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户信息";
            this.Load += new System.EventHandler(this.XtraFormUserInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxXL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxZW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditSex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.MemoEdit memoEditAddress;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtTel;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBM;
        private DevExpress.XtraEditors.SimpleButton simpleBtnImg;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEditRemark;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxXL;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxZW;
        private DevExpress.XtraEditors.DateEdit dateEditBirthday;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditSex;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleBtnCancel;
        private DevExpress.XtraEditors.SimpleButton simpleBtnOK;
    }
}