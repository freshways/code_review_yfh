﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;

namespace DXTempLate.Controls
{
    public partial class XUControlSysDic : DevExpress.XtraEditors.XtraUserControl
    {
        private SqliteClass oraClass = null;
        private PublicClass pubClass = null;
        private int mCount;
        private int mCurRow;
        private System.Collections.Generic.IList<clsGridViewSysCode> mSysCodeArrList = null;

        public XUControlSysDic()
        {
            InitializeComponent();


            mSysCodeArrList = new List<clsGridViewSysCode>();
            oraClass = new SqliteClass();
            pubClass = new PublicClass();
        }

        private static XUControlSysDic _ItSelf;
        public static XUControlSysDic ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControlSysDic();
                }
                return _ItSelf;
            }
        }

        private void XUControlSysDic_Load(object sender, EventArgs e)
        {
            BindDataList();
        }

        /// <summary>
        /// 绑定数据列表
        /// </summary>
        /// <param name="strTypeName">编码类型名称</param>
        private void BindDataList(string strTypeName = "")
        {
            QueryParam qp = new QueryParam();
            System.Data.DataSet datSet = new DataSet();
            System.Data.DataTable datTable = new DataTable();
            qp.TableName = "SYS_DIC";
            qp.Orderfld = "TypeCode,Sort,ID";
            qp.OrderType = 2;
            qp.ReturnFields = "*";
            if (strTypeName != string.Empty)
            {
                qp.Where = " WHERE TYPENAME='" + strTypeName + "'";
            }
            int RecordCount = 0;
            datSet = oraClass.QueryTableInfo(qp, out RecordCount, false);
            mSysCodeArrList.Clear();//清除数据
            if (datSet.Tables[0].Rows.Count > 0)
            {
                mCount = RecordCount;
                datTable = datSet.Tables[0];
                for (int i = 0; i < datTable.Rows.Count; i++)
                {
                    //增加一行
                    mSysCodeArrList.Add(new clsGridViewSysCode());
                    this.gridControlSysCode.DataSource = mSysCodeArrList;
                    this.gridControlSysCode.Refresh();

                    this.gridViewSysCode.SetRowCellValue(i, "XZ", false);
                    this.gridViewSysCode.SetRowCellValue(i, "XH", i + 1);
                    this.gridViewSysCode.SetRowCellValue(i, "ID", datTable.Rows[i]["ID"].ToString());
                    this.gridViewSysCode.SetRowCellValue(i, "TYPECODE", datTable.Rows[i]["TYPECODE"].ToString());
                    if (datTable.Rows[i]["TYPENAME"] != System.DBNull.Value)
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "TYPENAME", datTable.Rows[i]["TYPENAME"].ToString());
                    }
                    else
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "TYPENAME", "");
                    }
                    this.gridViewSysCode.SetRowCellValue(i, "NAME", datTable.Rows[i]["NAME"].ToString());
                    if (datTable.Rows[i]["CODE"] != System.DBNull.Value)
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "CODE", datTable.Rows[i]["CODE"].ToString());
                    }
                    else
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "CODE", "");
                    }
                    if (datTable.Rows[i]["SORT"] != System.DBNull.Value)
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "SORT", datTable.Rows[i]["SORT"].ToString());
                    }
                    else
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "SORT", "");
                    }

                    if (datTable.Rows[i]["REMARK"] != System.DBNull.Value)
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "REMARK", datTable.Rows[i]["REMARK"].ToString());
                    }
                    else
                    {
                        this.gridViewSysCode.SetRowCellValue(i, "REMARK", "");
                    }
                }
                this.gridViewSysCode.BestFitColumns();
            }
            this.gridControlSysCode.RefreshDataSource();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (CheckFormValue() == false)
            {
                return;
            }
            Entity_SysDic en = new Entity_SysDic();
            en.ID = pubClass.GetMaxID("SYS_DIC") + 1;
            en.NAME = txtName.Text.Trim();
            en.TYPECODE = Convert.ToInt32(txtTypeCode.Text.Trim());
            en.TYPENAME = txtTypeName.Text.Trim();
            en.SORTNUM = Convert.ToInt32(this.txtSortNum.Text.Trim());
            en.CODE = Convert.ToInt32(this.txtNameCode.Text.Trim());
            en.REMARK = this.memoEdit1.Text.Trim();

            if (oraClass.UpdateSysDic(en) == true)
            {
                AddRow(en);
                XtraMessageBox.Show("添加成功!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void AddRow(Entity_SysDic en)
        {
            clsGridViewSysCode cls = new clsGridViewSysCode();
            cls.XZ = false;
            cls.XH = Convert.ToString(mCount + 1);
            cls.ID = en.ID.ToString();
            cls.NAME = en.NAME;
            cls.SORT = en.SORTNUM.ToString();
            cls.TYPECODE = en.TYPECODE.ToString();
            cls.TYPENAME = en.TYPENAME;
            cls.CODE = en.CODE.ToString();
            cls.REMARK = en.REMARK;
            mCount++;
            mSysCodeArrList.Add(cls);
            this.gridControlSysCode.DataSource = mSysCodeArrList;
            this.gridControlSysCode.RefreshDataSource();
        }
        /// <summary>
        /// 检查界面输入值
        /// </summary>
        private bool CheckFormValue()
        {
            bool bFlag = true;
            //类型名称
            if (this.txtTypeName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("类型名称不能为空!", "系统提示");
                bFlag = false;
                return bFlag;
            }
            //类型编码
            if (this.txtTypeCode.Text.Trim() != string.Empty)
            {
                if (pubClass.InStrint(this.txtTypeCode.Text.Trim()) == false)
                {
                    MessageBox.Show("类型编码必须为数字!", "系统提示");
                    bFlag = false;
                    return bFlag;
                }
            }
            else
            {
                MessageBox.Show("类型编码不能为空!", "系统提示");
                bFlag = false;
                return bFlag;
            }

            if (this.txtName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("编码名称不能为空!", "系统提示");
                bFlag = false;
                return bFlag;
            }

            if (this.txtNameCode.Text.Trim() != string.Empty)
            {
                if (pubClass.InStrint(this.txtNameCode.Text.Trim()) == false)
                {
                    MessageBox.Show("编码必须为数字!", "系统提示");
                    bFlag = false;
                    return bFlag;
                }
            }
            else
            {
                MessageBox.Show("编码不能为空!", "系统提示");
                bFlag = false;
                return bFlag;
            }

            if (this.txtSortNum.Text.Trim() != string.Empty)
            {
                if (pubClass.InStrint(this.txtSortNum.Text.Trim()) == false)
                {
                    MessageBox.Show("编码排序为数字!", "系统提示");
                    bFlag = false;
                    return bFlag;
                }
            }
            else
            {
                MessageBox.Show("编码排序为空!", "系统提示");
                bFlag = false;
                return bFlag;
            }
            return bFlag;

        }

        private void simpleBtnSave_Click(object sender, EventArgs e)
        {
            if (CheckFormValue() == false)
            {
                return;
            }
            //判断值是否输入正确
            Entity_SysDic en = new Entity_SysDic();
            en.ID = Convert.ToInt32(txtID.Text);
            en.NAME = txtName.Text.Trim();
            en.TYPECODE = Convert.ToInt32(txtTypeCode.Text.Trim());
            en.TYPENAME = txtTypeName.Text.Trim();
            en.SORTNUM = Convert.ToInt32(this.txtSortNum.Text.Trim());
            en.CODE = Convert.ToInt32(this.txtNameCode.Text.Trim());
            en.REMARK = this.memoEdit1.Text.Trim();
            if (oraClass.UpdateSysDic(en) == true)
            {
                XtraMessageBox.Show("修改成功!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //更新列表的值
                if (mCurRow >= 0)
                {
                    clsGridViewSysCode cls = new clsGridViewSysCode();
                    cls.XZ = true;
                    cls.ID = en.ID.ToString();
                    cls.NAME = en.NAME;
                    cls.SORT = en.SORTNUM.ToString();
                    cls.TYPECODE = en.TYPECODE.ToString();
                    cls.TYPENAME = en.TYPENAME;
                    cls.CODE = en.CODE.ToString();
                    cls.REMARK = en.REMARK.ToString();

                    this.gridViewSysCode.SetRowCellValue(mCurRow, "NAME", cls.NAME);
                    this.gridViewSysCode.SetRowCellValue(mCurRow, "CODE", cls.CODE);
                    this.gridViewSysCode.SetRowCellValue(mCurRow, "TYPECODE", cls.TYPECODE);
                    this.gridViewSysCode.SetRowCellValue(mCurRow, "TYPENAME", cls.TYPENAME);
                    this.gridViewSysCode.SetRowCellValue(mCurRow, "SORT", cls.SORT);
                    this.gridViewSysCode.SetRowCellValue(mCurRow, "REMARK", cls.REMARK);

                    this.gridControlSysCode.RefreshDataSource();
                }

            }
        }

        private void simpleBtnDelete_Click(object sender, EventArgs e)
        {
            int nID = 0;
            string strName = "";
            for (int nRow = 0; nRow < this.gridViewSysCode.RowCount; nRow++)
            {
                if (Convert.ToBoolean(gridViewSysCode.GetRowCellValue(nRow, "XZ")) == true)
                {
                    nID = Convert.ToInt32(gridViewSysCode.GetRowCellValue(nRow, "ID").ToString());
                    strName = gridViewSysCode.GetRowCellValue(nRow, "NAME").ToString();
                }
            }
            if (nID == 0)
            {
                XtraMessageBox.Show("请选择你要删除的列表!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (XtraMessageBox.Show("你确定要编码名称[" + strName + "]删除吗？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (pubClass.DeleteByID(nID, "SYS_Dic") == true)
                {
                    XtraMessageBox.Show("删除成功!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BindDataList();
                }
            }
        }

        private void gridControlSysCode_MouseDown(object sender, MouseEventArgs e)
        {
            int nCurRow = 0;
            FormClass formClass = new FormClass();
            nCurRow = formClass.XGridViewMouseDown(this.gridViewSysCode, e, 0, "XZ");
            mCurRow = nCurRow;          
            try
            {
                this.txtID.Text = gridViewSysCode.GetRowCellValue(nCurRow, "ID").ToString();
                this.txtTypeName.Text = gridViewSysCode.GetRowCellValue(nCurRow, "TYPENAME").ToString();
            }
            catch { }

            try
            {
                this.txtTypeCode.Text = gridViewSysCode.GetRowCellValue(nCurRow, "TYPECODE").ToString();
                this.txtName.Text = gridViewSysCode.GetRowCellValue(nCurRow, "NAME").ToString();
                this.txtNameCode.Text = gridViewSysCode.GetRowCellValue(nCurRow, "CODE").ToString();
                this.txtSortNum.Text = gridViewSysCode.GetRowCellValue(nCurRow, "SORT").ToString();
                this.memoEdit1.Text = gridViewSysCode.GetRowCellValue(nCurRow, "REMARK").ToString();
            }
            catch { }
        }

    }

    public class clsGridViewSysCode
    {
        private bool _XZ;
        private string _XH;
        private string _ID;
        private string _TYPECODE;
        private string _TYPENAME;
        private string _NAME;
        private string _CODE;
        private string _SORT;
        private string _REMARK;

        public bool XZ
        {
            get { return _XZ; }
            set { _XZ = value; }
        }
        public string XH
        {
            get { return _XH; }
            set { _XH = value; }
        }
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public string TYPECODE
        {
            get { return _TYPECODE; }
            set { _TYPECODE = value; }
        }
        public string TYPENAME
        {
            get { return _TYPENAME; }
            set { _TYPENAME = value; }
        }
        public string NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }
        public string CODE
        {
            get { return _CODE; }
            set { _CODE = value; }
        }
        public string SORT
        {
            get { return _SORT; }
            set { _SORT = value; }
        }
        public string REMARK
        {
            get { return _REMARK; }
            set { _REMARK = value; }
        }
    }
}
