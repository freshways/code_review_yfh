﻿namespace DXTempLate.Controls
{
    partial class XUControlSysDic
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUControlSysDic));
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtID = new System.Windows.Forms.TextBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.txtTypeCode = new DevExpress.XtraEditors.TextEdit();
            this.simpleBtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTypeName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtSortNum = new DevExpress.XtraEditors.TextEdit();
            this.txtNameCode = new DevExpress.XtraEditors.TextEdit();
            this.gridControlSysCode = new DevExpress.XtraGrid.GridControl();
            this.gridViewSysCode = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.XZ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYPENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TYPECODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSortNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSysCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSysCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(471, 28);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoEdit1.Size = new System.Drawing.Size(300, 89);
            this.memoEdit1.TabIndex = 32;
            this.memoEdit1.UseOptimizedRendering = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(413, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 31;
            this.labelControl1.Text = "描述说明：";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(387, 62);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(35, 22);
            this.txtID.TabIndex = 30;
            this.txtID.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(551, 158);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(69, 23);
            this.simpleButton1.TabIndex = 29;
            this.simpleButton1.Text = "添加";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // txtTypeCode
            // 
            this.txtTypeCode.Location = new System.Drawing.Point(115, 59);
            this.txtTypeCode.Name = "txtTypeCode";
            this.txtTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTypeCode.Properties.Appearance.Options.UseFont = true;
            this.txtTypeCode.Size = new System.Drawing.Size(266, 26);
            this.txtTypeCode.TabIndex = 28;
            // 
            // simpleBtnDelete
            // 
            this.simpleBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnDelete.Image")));
            this.simpleBtnDelete.Location = new System.Drawing.Point(700, 158);
            this.simpleBtnDelete.Name = "simpleBtnDelete";
            this.simpleBtnDelete.Size = new System.Drawing.Size(69, 23);
            this.simpleBtnDelete.TabIndex = 27;
            this.simpleBtnDelete.Text = "删除";
            this.simpleBtnDelete.Click += new System.EventHandler(this.simpleBtnDelete_Click);
            // 
            // simpleBtnSave
            // 
            this.simpleBtnSave.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnSave.Image")));
            this.simpleBtnSave.Location = new System.Drawing.Point(626, 158);
            this.simpleBtnSave.Name = "simpleBtnSave";
            this.simpleBtnSave.Size = new System.Drawing.Size(68, 23);
            this.simpleBtnSave.TabIndex = 26;
            this.simpleBtnSave.Text = "修改";
            this.simpleBtnSave.Click += new System.EventHandler(this.simpleBtnSave_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(37, 62);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 25;
            this.labelControl6.Text = "类型编码：";
            // 
            // txtTypeName
            // 
            this.txtTypeName.Location = new System.Drawing.Point(115, 28);
            this.txtTypeName.Name = "txtTypeName";
            this.txtTypeName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtTypeName.Properties.Appearance.Options.UseFont = true;
            this.txtTypeName.Size = new System.Drawing.Size(266, 26);
            this.txtTypeName.TabIndex = 24;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(37, 128);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 23;
            this.labelControl5.Text = "编码代码：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(37, 160);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "编码排序：";
            // 
            // txtSortNum
            // 
            this.txtSortNum.Location = new System.Drawing.Point(115, 155);
            this.txtSortNum.Name = "txtSortNum";
            this.txtSortNum.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSortNum.Properties.Appearance.Options.UseFont = true;
            this.txtSortNum.Size = new System.Drawing.Size(266, 26);
            this.txtSortNum.TabIndex = 21;
            // 
            // txtNameCode
            // 
            this.txtNameCode.Location = new System.Drawing.Point(115, 123);
            this.txtNameCode.Name = "txtNameCode";
            this.txtNameCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtNameCode.Properties.Appearance.Options.UseFont = true;
            this.txtNameCode.Size = new System.Drawing.Size(266, 26);
            this.txtNameCode.TabIndex = 20;
            // 
            // gridControlSysCode
            // 
            this.gridControlSysCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSysCode.Location = new System.Drawing.Point(2, 22);
            this.gridControlSysCode.MainView = this.gridViewSysCode;
            this.gridControlSysCode.Name = "gridControlSysCode";
            this.gridControlSysCode.Size = new System.Drawing.Size(777, 309);
            this.gridControlSysCode.TabIndex = 5;
            this.gridControlSysCode.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSysCode});
            this.gridControlSysCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridControlSysCode_MouseDown);
            // 
            // gridViewSysCode
            // 
            this.gridViewSysCode.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.XZ,
            this.gridColumn1,
            this.ID,
            this.TYPENAME,
            this.TYPECODE,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn2});
            this.gridViewSysCode.GridControl = this.gridControlSysCode;
            this.gridViewSysCode.Name = "gridViewSysCode";
            this.gridViewSysCode.OptionsView.ShowGroupPanel = false;
            this.gridViewSysCode.OptionsView.ShowIndicator = false;
            // 
            // XZ
            // 
            this.XZ.Caption = "选择";
            this.XZ.FieldName = "XZ";
            this.XZ.Name = "XZ";
            this.XZ.OptionsColumn.FixedWidth = true;
            this.XZ.Visible = true;
            this.XZ.VisibleIndex = 0;
            this.XZ.Width = 35;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "序号";
            this.gridColumn1.FieldName = "XH";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 40;
            // 
            // ID
            // 
            this.ID.Caption = "ID";
            this.ID.FieldName = "ID";
            this.ID.Name = "ID";
            this.ID.OptionsColumn.AllowEdit = false;
            this.ID.Width = 40;
            // 
            // TYPENAME
            // 
            this.TYPENAME.Caption = "类型名称";
            this.TYPENAME.FieldName = "TYPENAME";
            this.TYPENAME.Name = "TYPENAME";
            this.TYPENAME.OptionsColumn.AllowEdit = false;
            this.TYPENAME.OptionsColumn.FixedWidth = true;
            this.TYPENAME.Visible = true;
            this.TYPENAME.VisibleIndex = 2;
            this.TYPENAME.Width = 90;
            // 
            // TYPECODE
            // 
            this.TYPECODE.Caption = "类型编码";
            this.TYPECODE.FieldName = "TYPECODE";
            this.TYPECODE.Name = "TYPECODE";
            this.TYPECODE.OptionsColumn.AllowEdit = false;
            this.TYPECODE.OptionsColumn.FixedWidth = true;
            this.TYPECODE.Visible = true;
            this.TYPECODE.VisibleIndex = 3;
            this.TYPECODE.Width = 50;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "编码名称";
            this.gridColumn3.FieldName = "NAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 90;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "编码代码";
            this.gridColumn4.FieldName = "CODE";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 50;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "排序";
            this.gridColumn5.FieldName = "SORT";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            this.gridColumn5.Width = 40;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "说明";
            this.gridColumn2.FieldName = "REMARK";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 7;
            this.gridColumn2.Width = 180;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControlSysCode);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(781, 333);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "数据列表";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(115, 91);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtName.Properties.Appearance.Options.UseFont = true;
            this.txtName.Size = new System.Drawing.Size(266, 26);
            this.txtName.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(37, 94);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "编码名称：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(37, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "类型名称：";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.memoEdit1);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtID);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.txtTypeCode);
            this.groupControl1.Controls.Add(this.simpleBtnDelete);
            this.groupControl1.Controls.Add(this.simpleBtnSave);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtTypeName);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtSortNum);
            this.groupControl1.Controls.Add(this.txtNameCode);
            this.groupControl1.Controls.Add(this.txtName);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 333);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(781, 189);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "编辑信息";
            // 
            // XUControlSysDic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "XUControlSysDic";
            this.Size = new System.Drawing.Size(781, 522);
            this.Load += new System.EventHandler(this.XUControlSysDic_Load);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSortNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSysCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSysCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn XZ;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TextBox txtID;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit txtTypeCode;
        private DevExpress.XtraEditors.SimpleButton simpleBtnDelete;
        private DevExpress.XtraEditors.SimpleButton simpleBtnSave;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraGrid.Columns.GridColumn ID;
        private DevExpress.XtraGrid.Columns.GridColumn TYPENAME;
        private DevExpress.XtraGrid.Columns.GridColumn TYPECODE;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit txtTypeName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtSortNum;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.TextEdit txtNameCode;
        private DevExpress.XtraGrid.GridControl gridControlSysCode;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSysCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
    }
}
