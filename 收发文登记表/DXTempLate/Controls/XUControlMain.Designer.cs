﻿namespace DXTempLate.Controls
{
    partial class XUControlMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtSumMoney = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtSumMoneyOUT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtSumMoneyIN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoneyOUT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoneyIN.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.txtSumMoney);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtSumMoneyOUT);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtSumMoneyIN);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(137, 89);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(599, 273);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "文件批办概况";
            // 
            // txtSumMoney
            // 
            this.txtSumMoney.Location = new System.Drawing.Point(403, 118);
            this.txtSumMoney.Name = "txtSumMoney";
            this.txtSumMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtSumMoney.Properties.Appearance.Options.UseFont = true;
            this.txtSumMoney.Properties.ReadOnly = true;
            this.txtSumMoney.Size = new System.Drawing.Size(175, 40);
            this.txtSumMoney.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(436, 79);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 33);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "未处理";
            // 
            // txtSumMoneyOUT
            // 
            this.txtSumMoneyOUT.Location = new System.Drawing.Point(210, 118);
            this.txtSumMoneyOUT.Name = "txtSumMoneyOUT";
            this.txtSumMoneyOUT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtSumMoneyOUT.Properties.Appearance.Options.UseFont = true;
            this.txtSumMoneyOUT.Properties.ReadOnly = true;
            this.txtSumMoneyOUT.Size = new System.Drawing.Size(175, 40);
            this.txtSumMoneyOUT.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Location = new System.Drawing.Point(243, 79);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(84, 33);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "总处理";
            // 
            // txtSumMoneyIN
            // 
            this.txtSumMoneyIN.Location = new System.Drawing.Point(16, 118);
            this.txtSumMoneyIN.Name = "txtSumMoneyIN";
            this.txtSumMoneyIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.txtSumMoneyIN.Properties.Appearance.Options.UseFont = true;
            this.txtSumMoneyIN.Properties.ReadOnly = true;
            this.txtSumMoneyIN.Size = new System.Drawing.Size(175, 40);
            this.txtSumMoneyIN.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Location = new System.Drawing.Point(50, 79);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "总收文";
            // 
            // XUControlMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "XUControlMain";
            this.Size = new System.Drawing.Size(742, 489);
            this.Load += new System.EventHandler(this.XUControlMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoneyOUT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumMoneyIN.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSumMoney;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtSumMoneyOUT;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtSumMoneyIN;
    }
}
