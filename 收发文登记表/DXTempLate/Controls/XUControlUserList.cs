﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DXTempLate.Controls
{
    public partial class XUControlUserList : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControlUserList()
        {
            InitializeComponent();
        }
        private static XUControlUserList _ItSelf;
        public static XUControlUserList ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControlUserList();
                }
                return _ItSelf;
            }
        }

        private void XUControlUserList_Load(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void BindDataList()
        {
            System.Data.DataTable datTable = new DataTable();
            #region 创建表头
            DataColumn dc1 = new DataColumn("选择", typeof(Boolean));
            DataColumn dc2 = new DataColumn("ID", typeof(int));
            DataColumn dc3 = new DataColumn("序号", typeof(int));
            DataColumn dc4 = new DataColumn("用户名");
            DataColumn dc5 = new DataColumn("性别");
            DataColumn dc6 = new DataColumn("入职日期");
            DataColumn dc7 = new DataColumn("职务");
            DataColumn dc8 = new DataColumn("学历");
            DataColumn dc9 = new DataColumn("联系电话");

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);
            datTable.Columns.Add(dc9); 
            #endregion

            #region 绑定
            PublicClass pubClass = new PublicClass();
            SqliteClass sqliteClass = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqliteClass.GetAllTablesID("Sys_Employee");
            if (listid.Count > 0)
            {
                for (int i = 0; i < listid.Count; i++)
                {
                    Entity_Sys_Employee objUserinfo = new Entity_Sys_Employee();
                    objUserinfo = sqliteClass.GetSys_Employee(listid[i]);

                    DataRow datRow = datTable.NewRow();//创建新行
                    datRow[0] = false;
                    datRow[1] = listid[i];
                    datRow[2] = i + 1;
                    datRow[3] = objUserinfo.Name;
                    datRow[4] = pubClass.GetNameByCode(objUserinfo.SEX, 2);
                    datRow[5] = objUserinfo.BirtyDay.ToShortDateString();
                    datRow[6] = pubClass.GetNameByCode(objUserinfo.ZW, 4);
                    datRow[7] = pubClass.GetNameByCode(objUserinfo.XL, 3);
                    datRow[8] = objUserinfo.Photo;

                    datTable.Rows.Add(datRow);
                }
                //DataTable按照排序字段进行升序排列
                datTable.DefaultView.Sort = "ID asc";
                DataTable dtT = datTable.DefaultView.ToTable();
                datTable.Clear();
                datTable = dtT;

                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource(); 
            #endregion

            #region Grid样式
                //设置列宽
                this.gridView1.Columns[0].Width = 40;
                this.gridView1.Columns[2].Width = 40;
                this.gridView1.Columns[3].Width = 100;
                this.gridView1.Columns[4].Width = 80;
                this.gridView1.Columns[5].Width = 80;
                this.gridView1.Columns[6].Width = 80;
                this.gridView1.Columns[7].Width = 80;
                this.gridView1.Columns[8].Width = 80;

                //隐藏列
                this.gridView1.Columns[1].Visible = false;

                //设置列不允许编辑
                this.gridView1.Columns[2].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[3].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[4].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[5].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[6].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[7].OptionsColumn.AllowEdit = false;
                this.gridView1.Columns[8].OptionsColumn.AllowEdit = false; 
                #endregion
            }
            else
            {
                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();
            }
        }


        #region ButtonClick
        //全选
        private void simpleBSelAll_Click(object sender, EventArgs e)
        {
            FormClass frmClass = new FormClass();
            if (this.simpleBSelAll.Text == "全选")
            {
                frmClass.SelCurRowCheckAllAndUnAll(this.gridView1, 0);
                this.simpleBSelAll.Text = "全不选";
            }
            else
            {
                frmClass.SelCurRowCheckAllAndUnAll(this.gridView1, 1);
                this.simpleBSelAll.Text = "全选";
            }
        }

        //添加用户
        private void simpleBtnAdd_Click(object sender, EventArgs e)
        {
            XtraFormUserInfo Xfrm = new XtraFormUserInfo();
            if (Xfrm.ShowDialog() == DialogResult.OK)
            {
                this.BindDataList();
            }
        }

        //详细信息
        private void simpleBtnUserInfo_Click(object sender, EventArgs e)
        {
            int nGridViewId = GetRowViewCheckID();
            if (nGridViewId == 0) return;

            XtraFormUserInfo Xfrm = new XtraFormUserInfo(nGridViewId);
            if (Xfrm.ShowDialog() == DialogResult.OK)
            {
                this.BindDataList();
            }
        }

        private int GetRowViewCheckID()
        {
            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    break;
                }
            }
            return nRowId;
        }

        //删除
        private void simpleBtnDelete_Click(object sender, EventArgs e)
        {
            List<int> listID = new List<int>();

            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    listID.Add(nRowId);
                }
            }
            if (listID.Count == 0) return;

            if (XtraMessageBox.Show("你确定要删除该用户吗?", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                PublicClass pubClass = new PublicClass();
                for (int i = 0; i < listID.Count; i++)
                {
                    if (!pubClass.DeleteByID(listID[i], "Sys_Employee"))
                    {
                        XtraMessageBox.Show("删除成功", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                this.BindDataList();
            }
        } 
        #endregion

        #region GridEvent
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            FormClass frmClass = new FormClass();
            int nCurRow = 0;
            nCurRow = frmClass.XGridViewMouseDown(this.gridView1, e, 0);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            int nCurRow = 0;
            GridHitInfo hi = this.gridView1.CalcHitInfo(this.gridControl1.PointToClient(MousePosition));
            if (hi.InRow)
            {
                nCurRow = hi.RowHandle;
            }
            if (nCurRow < 0) return;

            int nGridViewId = Convert.ToInt32(this.gridView1.GetRowCellValue(nCurRow, "ID"));

            XtraFormUserInfo user = new XtraFormUserInfo(nGridViewId, true);
            user.ShowDialog();
        }

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                string str = "暂无人员数据!";
                Font f = new Font("宋体", 10, FontStyle.Bold);
                Rectangle r = new Rectangle(e.Bounds.Left + 25, e.Bounds.Top + 5, e.Bounds.Width - 5, e.Bounds.Height - 5);
                e.Graphics.DrawString(str, f, Brushes.Black, r);
            }
        }
        
        #endregion
    }
}
