﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DXTempLate.Class;

namespace DXTempLate.Controls
{
    public partial class XUControlMain : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControlMain()
        {
            InitializeComponent();
        }

        private static XUControlMain _ItSelf;
        public static XUControlMain ItSelf
        {
            get
            {
                //if (_ItSelf == null || _ItSelf.IsDisposed)
                //{
                    _ItSelf = new XUControlMain();
                //}
                return _ItSelf;
            }
        }

        private void XUControlMain_Load(object sender, EventArgs e)
        {
            SqliteClass sqlite =new SqliteClass();
            double dtSumMoneyIN = sqlite.GetSumMoney("Sys_MoneyIn");
            double dtSumMoneyOUT = sqlite.GetSumMoney("Sys_MoneyOut");
            double dtSumMoney = dtSumMoneyIN - dtSumMoneyOUT;

            this.txtSumMoneyIN.Text = Math.Round(dtSumMoneyIN,2).ToString();
            this.txtSumMoneyOUT.Text = Math.Round(dtSumMoneyOUT,2).ToString();
            this.txtSumMoney.Text =  Math.Round(dtSumMoney,2).ToString();

        }
    }
}
