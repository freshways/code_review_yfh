﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DXTempLate.Class
{
    class EntityClass
    {
    }

    /// <summary>
    /// 数据字典实体类
    /// </summary>
    public class Entity_SysDic
    {
        private int _ID;
        private int _TYPECODE;
        private string _TYPENAME;
        private string _NAME;
        private int _CODE;
        private int _SORTNUM;
        private string _REMARK;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public int TYPECODE
        {
            get { return _TYPECODE; }
            set { _TYPECODE = value; }
        }
        public string TYPENAME
        {
            get { return _TYPENAME; }
            set { _TYPENAME = value; }
        }
        public string NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }
        public int CODE
        {
            get { return _CODE; }
            set { _CODE = value; }
        }
        public int SORTNUM
        {
            get { return _SORTNUM; }
            set { _SORTNUM = value; }
        }
        public string REMARK
        {
            get { return _REMARK; }
            set { _REMARK = value; }
        }
    }


    /// <summary>
    /// 用户表实体类
    /// </summary>
    public class Entity_Sys_Employee
    {
        private int _ID;
        private string _Name;
        private string _Password;
        private int _LoginType;
        private int _SEX;
        private DateTime _BirtyDay;
        private int _ZW;
        private int _XL;
        private int _BM;
        private string _Photo;
        private string _SJ;
        private string _Email;
        private string _Address;
        private string _Remark;
        private string _ImgName;

        private string _SkinName;

        public Entity_Sys_Employee()
        {
            _ID = 0;
            _Name = "";
            _Password = "";
            _LoginType = 0;
            _SEX = 0;
            _BirtyDay = Convert.ToDateTime("1000-1-1");
            _ZW = 0;
            _XL = 0;
            _BM = 0;
            _Photo = "";
            _SJ = "";
            _Email = "";
            _Address = "";
            _Remark = "";
            _ImgName = "";
            _SkinName = "";
        }

        /// <summary>
        /// 皮肤名称
        /// </summary>
        public string SkinName
        {
            get { return _SkinName; }
            set { _SkinName = value; }
        }

        public int XL
        {
            get { return _XL; }
            set { _XL = value; }
        }


        public int BM
        {
            get { return _BM; }
            set { _BM = value; }
        }


        public string Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }


        public string SJ
        {
            get { return _SJ; }
            set { _SJ = value; }
        }


        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }


        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }


        public string ImgName
        {
            get { return _ImgName; }
            set { _ImgName = value; }
        }

        /// <summary>
        /// 用户ID
        /// </summary>
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        /// <summary>
        /// 登陆类型
        /// </summary>
        public int LoginType
        {
            get { return _LoginType; }
            set { _LoginType = value; }
        }

        /// <summary>
        /// 性别
        /// </summary>
        public int SEX
        {
            get { return _SEX; }
            set { _SEX = value; }
        }
        /// <summary>
        /// 职务
        /// </summary>
        public int ZW
        {
            get { return _ZW; }
            set { _ZW = value; }
        }
        /// <summary>
        ///  生日
        /// </summary>
        public DateTime BirtyDay
        {
            get { return _BirtyDay; }
            set { _BirtyDay = value; }
        }
    }


    /// <summary>
    /// 收文登记表
    /// </summary>
    public class Entity_Sys_收文登记表
    {
        public int ID  { get; set; }

        public DateTime 收到时间  { get; set; }
        public string 来文机关 { get; set; }
        public string 文号 { get; set; }
        public string 文件名称  { get; set; }
        public string 批办领导 { get; set; }
        public int 收到文件数 { get; set; }
        public int 打印状态  { get; set; }
        public string 备注  { get; set; }
        public string 创建人  { get; set; }
        public DateTime 创建时间  { get; set; }
        public string 修改人 { get; set; }
        public DateTime 修改时间 { get; set; }

        public int SSUserID  { get; set; }        
    }

}
