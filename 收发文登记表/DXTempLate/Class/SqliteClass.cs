﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace DXTempLate.Class
{
    /// <summary>
    /// 数据库函数处理类
    /// </summary>
    public class SqliteClass
    {
        public SqliteClass()
        {

        }
        private DBClass dbConstring;

        #region 通用查询数据集函数
        /// <summary>
        /// 查询返回DataSet任何表的信息，包含分页功能 （通用函数）
        /// </summary>
        /// <param name="qp">参数实体类(包括：SQL语句，翻页参数)</param>
        /// <param name="RecordCount">输出函数：查询的总记录数</param>
        /// <param name="bPagination">是否分页</param>
        /// <returns></returns>
        public System.Data.DataSet QueryTableInfo(QueryParam qp, out int RecordCount, bool bPagination)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection oraConn = new SQLiteConnection(dbConstring.m_ConnString);

            int TotalRecordForPageIndex = qp.PageIndex * qp.PageSize;
            int FirstRecordForPageIndex = (qp.PageIndex - 1) * qp.PageSize;

            //定义排序功能
            string strOrderBy;
            if (qp.OrderType == 1)
            {
                strOrderBy = " ORDER BY " + qp.Orderfld + " DESC";
            }
            else
            {
                strOrderBy = " ORDER BY " + qp.Orderfld + " ASC";
            }
            StringBuilder sb = new StringBuilder();
            if (bPagination)//True为分页，False不分页
            {
                sb.AppendFormat("SELECT * FROM (SELECT A.*, ROWNUM RN FROM (SELECT {0} FROM {1} {2} {3}) A WHERE ROWNUM <= {4})WHERE RN > {5} ", qp.ReturnFields, qp.TableName.ToUpper(), qp.Where, strOrderBy, TotalRecordForPageIndex, FirstRecordForPageIndex);
            }
            else
            {
                //判断条件和排序字段是否为空
                if (qp.Where != null && qp.Orderfld != null)
                {
                    sb.AppendFormat("SELECT {0} FROM {1} {2} {3}", qp.ReturnFields, qp.TableName.ToUpper(), qp.Where, strOrderBy);
                }
                else
                {
                    if (qp.Where == null && qp.Orderfld != null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1} {2}", qp.ReturnFields, qp.TableName.ToUpper(), strOrderBy);
                    }
                    else if (qp.Where != null && qp.Orderfld == null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1} {2}", qp.ReturnFields, qp.TableName.ToUpper(), qp.Where);
                    }
                    else if (qp.Where == null && qp.Orderfld == null)
                    {
                        sb.AppendFormat("SELECT {0} FROM {1}", qp.ReturnFields, qp.TableName.ToUpper());
                    }
                }
            }

            System.Data.DataSet datSet = null;
            datSet = new DataSet();
            oraConn.Open();//打开数据库连接

            SQLiteCommand oraCommand = null;
            oraCommand = new SQLiteCommand("", oraConn);
            RecordCount = 0;
            try
            {
                datSet = dbConstring.GetDataSetBySql(sb.ToString(), qp.TableName);
                oraCommand.CommandText = String.Format("SELECT COUNT(*) FROM {0} {1}", qp.TableName, qp.Where);
                RecordCount = Convert.ToInt32(oraCommand.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            finally
            {
                oraCommand.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }

            return datSet;

        }
        #endregion

        /// <summary>
        /// 检查用户名和密码是否正确     
        /// </summary>
        /// <param name="strUserName">用户名称</param>
        /// <param name="UserPassword">用户密码</param>
        /// <returns></returns>
        public bool CheckUserLogin(string strUserName, string UserPassword,out int nUserID)
        {
            bool bCheck = false;
            if (strUserName == "" && UserPassword == "")
            {
                nUserID = 0;
                return bCheck;
            }
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT ID,NAME,PASSWORD FROM SYS_USER WHERE NAME='" + strUserName + "' AND PASSWORD='" + UserPassword + "'";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);
            int nID=0;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                if (datRead.Read())
                {
                    nID =Convert.ToInt32(datRead["ID"].ToString());
                    bCheck = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("执行数据库SQL语句错误：" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            nUserID = nID;
            return bCheck;

        }

        /// <summary>
        /// 修改用户的用户密码
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        public bool UpdateUserPwd(string strUserName, string NewPassword)
        {
            bool bCheck = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "UPDATE SYS_USER SET PASSWORD='" + NewPassword + "' WHERE NAME='" + strUserName + "'";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);

            try
            {
                conn.Open();
                int num = -1;
                num = comm.ExecuteNonQuery();
                if (num != -1)
                {
                    bCheck = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("执行数据库SQL语句错误：" + ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return bCheck;
        }


        /// <summary>
        /// 获得表中ID集合
        /// </summary>
        /// <param name="strTableName">表名</param>
        /// <returns></returns>
        public List<int> GetAllTablesID(string strTableName, string strSortField = "")
        {
            List<int> listId = new List<int>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT ID FROM " + strTableName;
            if (strSortField != "")
            {
                strSql = strSql + " order by " + strSortField + " desc,ID desc";
            }
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listId.Add(Convert.ToInt32(datRead["ID"]));
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }
            return listId;
        }

        public List<int> GetAllTablesIDByField(string strTableName, string strField, int nFieldValue)
        {
            List<int> listId = new List<int>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT ID FROM " + strTableName + " WHERE " + strField + "=" + nFieldValue;
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listId.Add(Convert.ToInt32(datRead["ID"]));
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }
            return listId;
        }
        
        /// <summary>
        /// 添加、修改字典编码表信息
        /// </summary>
        /// <param name="en">字典编码表实体类</param>
        /// <returns></returns>
        public bool UpdateSysDic(Entity_SysDic en)
        {
            bool bBool = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "SYS_DIC";
            strSql = "SELECT * FROM " + strTableName + " WHERE ID=" + en.ID;
            System.Data.SQLite.SQLiteConnection oraConn = null;
            System.Data.SQLite.SQLiteDataAdapter datAdapter = null;
            System.Data.DataSet datSet = new DataSet();

            oraConn = new SQLiteConnection(dbConstring.m_ConnString);
            oraConn.Open();//打开数据连接
            datAdapter = new SQLiteDataAdapter(strSql, oraConn);
            SQLiteCommandBuilder oraCmmdBuilder = new SQLiteCommandBuilder(datAdapter);
            datAdapter.Fill(datSet, strTableName);
            System.Data.DataTable datTable = null;
            datTable = datSet.Tables[0];
            System.Data.DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                datRow["ID"] = en.ID;
            }
            if (en.NAME != string.Empty)
            {
                datRow["NAME"] = en.NAME;
            }
            else
            {
                datRow["NAME"] = System.DBNull.Value;
            }

            if (en.CODE > 0)
            {
                datRow["CODE"] = en.CODE;
            }
            else
            {
                datRow["CODE"] = System.DBNull.Value;
            }
            if (en.TYPECODE > 0)
            {
                datRow["TYPECODE"] = en.TYPECODE;
            }
            else
            {
                datRow["TYPECODE"] = System.DBNull.Value;
            }
            if (en.TYPENAME != string.Empty)
            {
                datRow["TYPENAME"] = en.TYPENAME;
            }
            else
            {
                datRow["TYPENAME"] = System.DBNull.Value;
            }

            if (en.SORTNUM > 0)
            {
                datRow["SORT"] = en.SORTNUM;
            }
            else
            {
                datRow["SORT"] = System.DBNull.Value;
            }

            if (en.REMARK != string.Empty)
            {
                datRow["REMARK"] = en.REMARK;
            }
            else
            {
                datRow["REMARK"] = System.DBNull.Value;
            }

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                datAdapter.Update(datSet, strTableName);
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //关闭连接
                datSet = null;
                datTable = null;
                datRow = null;
                datAdapter.Dispose();
                oraCmmdBuilder.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }
            return bBool;
        }


        # region 用户表
        /// <summary>
        /// 添加、修改用户表信息
        /// </summary>
        /// <param name="objUsersModel"></param>
        /// <returns></returns>
        public bool UpdateSys_Employee(Entity_Sys_Employee objUsersModel)
        {
            bool bTrue = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strTableName = "Sys_Employee";
            string strSql = "SELECT * FROM " + strTableName + " WHERE ID=" + objUsersModel.ID;

            System.Data.SQLite.SQLiteConnection oraConn = null;
            System.Data.SQLite.SQLiteDataAdapter datAdapter = null;
            System.Data.DataSet datSet = new DataSet();

            oraConn = new SQLiteConnection(dbConstring.m_ConnString);
            oraConn.Open();//打开数据连接
            datAdapter = new SQLiteDataAdapter(strSql, oraConn);
            SQLiteCommandBuilder oraCmmdBuilder = new SQLiteCommandBuilder(datAdapter);
            datAdapter.Fill(datSet, strTableName);
            System.Data.DataTable datTable = null;
            datTable = datSet.Tables[0];
            System.Data.DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                datRow["ID"] = objUsersModel.ID;
            }
            if (objUsersModel.BirtyDay != null)
            {
                datRow["BirtyDay"] = objUsersModel.BirtyDay;
            }
            else
            {
                datRow["BirtyDay"] = System.DBNull.Value;
            }

            if (objUsersModel.BM > 0)
            {
                datRow["BM"] = objUsersModel.BM;
            }
            else
            {
                datRow["BM"] = System.DBNull.Value;
            }

            if (objUsersModel.Email != string.Empty)
            {
                datRow["Email"] = objUsersModel.Email;
            }
            else
            {
                datRow["Email"] = System.DBNull.Value;
            }

            if (objUsersModel.ImgName != string.Empty)
            {
                datRow["ImgName"] = objUsersModel.ImgName;
            }

            if (objUsersModel.LoginType > 0)
            {
                datRow["LoginType"] = objUsersModel.LoginType;
            }

            if (objUsersModel.Name != string.Empty)
            {
                datRow["NAME"] = objUsersModel.Name;
            }

            if (objUsersModel.Password != string.Empty)
            {
                datRow["Password"] = objUsersModel.Password;
            }

            if (objUsersModel.Photo != string.Empty)
            {
                datRow["Photo"] = objUsersModel.Photo;
            }
            else
            {
                datRow["Photo"] = System.DBNull.Value;
            }
            if (objUsersModel.Remark != string.Empty)
            {
                datRow["Remark"] = objUsersModel.Remark;
            }
            else
            {
                datRow["Remark"] = System.DBNull.Value;
            }

            if (objUsersModel.SEX > 0)
            {
                datRow["SEX"] = objUsersModel.SEX;
            }
            else
            {
                datRow["SEX"] = System.DBNull.Value;
            }
            if (objUsersModel.SJ != string.Empty)
            {
                datRow["SJ"] = objUsersModel.SJ;
            }
            else
            {
                datRow["SJ"] = System.DBNull.Value;
            }
            if (objUsersModel.XL > 0)
            {
                datRow["XL"] = objUsersModel.XL;
            }
            else
            {
                datRow["XL"] = System.DBNull.Value;
            }
            if (objUsersModel.ZW > 0)
            {
                datRow["ZW"] = objUsersModel.ZW;
            }
            else
            {
                datRow["ZW"] = System.DBNull.Value;
            }
            if (objUsersModel.Address != string.Empty)
            {
                datRow["Address"] = objUsersModel.Address;
            }
            else
            {
                datRow["Address"] = System.DBNull.Value;
            }

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                datAdapter.Update(datSet, strTableName);
                bTrue = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //关闭连接
                datSet = null;
                datTable = null;
                datRow = null;
                datAdapter.Dispose();
                oraCmmdBuilder.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }
            return bTrue;
        }

        /// <summary>
        /// 根据用户Id 获取用户信息
        /// </summary>
        /// <param name="nId"></param>
        /// <returns></returns>
        public Entity_Sys_Employee GetSys_Employee(int nId = 0)
        {
            Entity_Sys_Employee objModel = new Entity_Sys_Employee();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "";
            if (nId > 0)
            {
                strSql = "SELECT * FROM Sys_Employee WHERE ID=" + nId;
            }
            else
            {
                strSql = "SELECT * FROM Sys_Employee ";
            }
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;

            try
            {
                conn.Open();//打开连接
                datRead = comm.ExecuteReader();
                if (datRead.Read())
                {
                    objModel.ID = nId;
                    if (datRead["Address"] != System.DBNull.Value)
                    {
                        objModel.Address = datRead["Address"].ToString();
                    }
                    else
                    {
                        objModel.Address = "";
                    }

                    if (datRead["BirtyDay"] != System.DBNull.Value)
                    {
                        objModel.BirtyDay = Convert.ToDateTime(datRead["BirtyDay"]);
                    }
                    else
                    {
                        objModel.BirtyDay = Convert.ToDateTime("1000-1-1"); ;
                    }

                    if (datRead["BM"] != System.DBNull.Value)
                    {
                        objModel.BM = Convert.ToInt32(datRead["BM"].ToString());
                    }
                    else
                    {
                        objModel.BM = 0;
                    }
                    if (datRead["Email"] != System.DBNull.Value)
                    {
                        objModel.Email = datRead["Email"].ToString();
                    }
                    else
                    {
                        objModel.Email = "";
                    }

                    if (datRead["ImgName"] != System.DBNull.Value)
                    {
                        objModel.ImgName = datRead["ImgName"].ToString();
                    }
                    else
                    {
                        objModel.ImgName = "";
                    }

                    if (datRead["LoginType"] != System.DBNull.Value)
                    {
                        objModel.LoginType = Convert.ToInt32(datRead["LoginType"].ToString());
                    }
                    else
                    {
                        objModel.LoginType = 0;
                    }

                    if (datRead["Name"] != System.DBNull.Value)
                    {
                        objModel.Name = datRead["Name"].ToString();
                    }
                    else
                    {
                        objModel.Name = "";
                    }

                    if (datRead["Password"] != System.DBNull.Value)
                    {
                        objModel.Password = datRead["Password"].ToString();
                    }
                    else
                    {
                        objModel.Password = "";
                    }
                    if (datRead["Photo"] != System.DBNull.Value)
                    {
                        objModel.Photo = datRead["Photo"].ToString();
                    }
                    else
                    {
                        objModel.Photo = "";
                    }
                    if (datRead["Remark"] != System.DBNull.Value)
                    {
                        objModel.Remark = datRead["Remark"].ToString();
                    }
                    else
                    {
                        objModel.Remark = "";
                    }
                    if (datRead["SJ"] != System.DBNull.Value)
                    {
                        objModel.SJ = datRead["SJ"].ToString();
                    }
                    else
                    {
                        objModel.SJ = "";
                    }
                    if (datRead["SEX"] != System.DBNull.Value)
                    {
                        objModel.SEX = Convert.ToInt32(datRead["SEX"].ToString());
                    }
                    else
                    {
                        objModel.SEX = 0;
                    }
                    if (datRead["XL"] != System.DBNull.Value)
                    {
                        objModel.XL = Convert.ToInt32(datRead["XL"].ToString());
                    }
                    else
                    {
                        objModel.XL = 0;
                    }
                    if (datRead["ZW"] != System.DBNull.Value)
                    {
                        objModel.ZW = Convert.ToInt32(datRead["ZW"].ToString());
                    }
                    else
                    {
                        objModel.ZW = 0;
                    }
                    if (datRead["SkinName"] != System.DBNull.Value)
                    {
                        objModel.SkinName = datRead["SkinName"].ToString();
                    }
                    else
                    {
                        objModel.SkinName = "";
                    }

                }
            }
            catch { }
            finally
            {
                conn.Close();
                conn.Dispose();
                comm.Dispose();
            }
            return objModel;
        }


        #endregion


        /// <summary>
        /// 统计营销总收入、总支出
        /// </summary>
        /// <param name="nType">0营销收入，1营销支出</param>
        /// <param name="nYear">年份</param>
        /// <param name="nMonth">月份</param>
        /// <returns></returns>
        public double GetSumGSMoney(int nType, int nYear = 0, int nMonth = 0, string strTableName = "Sys_MoneyIn")
        {
            double dtSum = 0;
            string strSql = "";
            strSql = "SELECT SUM(Money) AS moneysum FROM " + strTableName + " WHERE 1=1";

            if (nType == 0)
            { //营销收入
                if (nYear > 0)
                {
                    string dtStartTime = "";
                    string dtEndTime = "";
                    if (nMonth > 0)
                    {
                        dtStartTime = nYear + "-" + nMonth + "-01";

                        DateTime dtNowDate = Convert.ToDateTime(dtStartTime);
                        int MonthDays = DateTime.DaysInMonth(dtNowDate.Year, dtNowDate.Month);
                        dtEndTime = nYear + "-" + nMonth + "-" + MonthDays;
                    }
                    else
                    {
                        dtStartTime = nYear + "-01-01";
                        dtEndTime = nYear + "-12-31";
                    }
                    strSql = strSql + "  and  InDate>='" + dtStartTime + "'";
                    strSql = strSql + "  and InDate<='" + dtEndTime + "'";
                }
            }
            else
            { 
               //营销支出
                if (nYear > 0)
                {
                    string dtStartTime = "";
                    string dtEndTime = "";
                    if (nMonth > 0)
                    {
                        dtStartTime = nYear + "-" + nMonth + "-01";

                        DateTime dtNowDate = Convert.ToDateTime(dtStartTime);
                        int MonthDays = DateTime.DaysInMonth(dtNowDate.Year, dtNowDate.Month);
                        dtEndTime = nYear + "-" + nMonth + "-" + MonthDays;
                    }
                    else
                    {
                        dtStartTime = nYear + "-01-01";
                        dtEndTime = nYear + "-12-31";
                    }
                    strSql = strSql + "  and  OutDate>='" + dtStartTime + "'";
                    strSql = strSql + "  and OutDate<='" + dtEndTime + "'";
                }
            }
           

            dbConstring = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);

            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    if (datRead["moneysum"] == System.DBNull.Value)
                    {
                        dtSum = 0;
                    }
                    else
                    {
                        dtSum = Convert.ToDouble(datRead["moneysum"]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("执行数据库SQL语句错误：" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dtSum;
        }


        #region 工作单
        /// <summary>
        /// 获得收文登记表-来文机关
        /// </summary>
        /// <returns></returns>
        public List<string> GetAll来文机关()
        {
            List<string> listUser = new List<string>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT distinct 来文机关 FROM Sys_收文登记表";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listUser.Add(datRead["来文机关"].ToString());
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }

            return listUser;
        }

        /// <summary>
        /// 获得收文登记表-批办领导
        /// </summary>
        /// <returns></returns>
        public List<string> GetAll批办领导()
        {
            List<string> listUser = new List<string>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT distinct 批办领导 FROM Sys_收文登记表";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listUser.Add(datRead["批办领导"].ToString());
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }

            return listUser;
        }
        
        /// <summary>
        /// 添加、修改收文管理
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public bool UpdateSys_收文登记表(Entity_Sys_收文登记表 en)
        {
            bool bBool = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "Sys_收文登记表";
            strSql = "SELECT * FROM " + strTableName + " WHERE ID=" + en.ID;
            SQLiteConnection oraConn = null;
            SQLiteDataAdapter datAdapter = null;
            System.Data.DataSet datSet = new DataSet();

            oraConn = new SQLiteConnection(dbConstring.m_ConnString);
            oraConn.Open();//打开数据连接
            datAdapter = new SQLiteDataAdapter(strSql, oraConn);
            SQLiteCommandBuilder oraCmmdBuilder = new SQLiteCommandBuilder(datAdapter);
            datAdapter.Fill(datSet, strTableName);
            System.Data.DataTable datTable = null;
            datTable = datSet.Tables[0];
            System.Data.DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }

            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                datRow["ID"] = en.ID;
            }
            if (en.收到时间 != Convert.ToDateTime("1000-1-1"))
            {
                datRow["收到时间"] = en.收到时间;
            }
            else
            {
                datRow["收到时间"] = System.DBNull.Value;
            }
            if (en.来文机关 != string.Empty)
            {
                datRow["来文机关"] = en.来文机关;
            }
            else
            {
                datRow["来文机关"] = System.DBNull.Value;
            }

            if (en.文号 != string.Empty)
            {
                datRow["文号"] = en.文号;
            }
            else
            {
                datRow["文号"] = System.DBNull.Value;
            }
            if (en.文件名称 != string.Empty)
            {
                datRow["文件名称"] = en.文件名称;
            }
            else
            {
                datRow["文件名称"] = System.DBNull.Value;
            }
            if (en.批办领导 != string.Empty)
            {
                datRow["批办领导"] = en.批办领导;
            }
            else
            {
                datRow["批办领导"] = System.DBNull.Value;
            }


            if (en.收到文件数 >0)
            {
                datRow["收到文件数"] = en.收到文件数;
            }
            else
            {
                datRow["收到文件数"] = System.DBNull.Value;
            }

            if (en.打印状态 >= 0)
            {
                datRow["打印状态"] = en.打印状态;
            }
            else
            {
                datRow["打印状态"] = System.DBNull.Value;
            }

            if (en.备注 != string.Empty)
            {
                datRow["备注"] = en.备注;
            }
            else
            {
                datRow["备注"] = System.DBNull.Value;
            }

            if (en.创建人 != string.Empty)
            {
                datRow["创建人"] = en.创建人;
            }
            else
            {
                datRow["创建人"] = System.DBNull.Value;
            }
            if (en.创建时间 != Convert.ToDateTime("1000-1-1"))
            {
                datRow["创建时间"] = en.创建时间;
            }
            else
            {
                datRow["创建时间"] = System.DBNull.Value;
            }

            if (en.修改时间 != Convert.ToDateTime("1000-1-1"))
            {
                datRow["修改时间"] = en.修改时间;
            }
            else
            {
                datRow["修改时间"] = System.DBNull.Value;
            }
            if (en.修改人 != string.Empty)
            {
                datRow["修改人"] = en.修改人;
            }
            else
            {
                datRow["修改人"] = System.DBNull.Value;
            }
            if (en.SSUserID > 0)
            {
                datRow["SSUserID"] = en.SSUserID;
            }
            else
            {
                datRow["SSUserID"] = System.DBNull.Value;
            }

            

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }

            try
            {
                datAdapter.Update(datSet, strTableName);
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //关闭连接
                datSet = null;
                datTable = null;
                datRow = null;
                datAdapter.Dispose();
                oraCmmdBuilder.Dispose();
                oraConn.Dispose();
                oraConn.Close();
            }
            return bBool;
        }

        /// <summary>
        /// 获取一条收文管理
        /// </summary>
        /// <param name="nId"></param>
        /// <returns></returns>
        public Entity_Sys_收文登记表 GetSys_收文登记表ByID(int nId)
        {
            Entity_Sys_收文登记表 objModel = new Entity_Sys_收文登记表();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "";
            if (nId > 0)
            {
                strSql = "SELECT * FROM Sys_收文登记表 WHERE ID=" + nId;
            }
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;

            try
            {
                conn.Open();//打开连接
                datRead = comm.ExecuteReader();
                if (datRead.Read())
                {
                    objModel.ID = nId;

                    if (datRead["收到时间"] != System.DBNull.Value)
                    {
                        objModel.收到时间 = Convert.ToDateTime(datRead["收到时间"]);
                    }
                    else
                    {
                        objModel.收到时间 = Convert.ToDateTime("1000-1-1");
                    }

                    if (datRead["来文机关"] != System.DBNull.Value)
                    {
                        objModel.来文机关 = datRead["来文机关"].ToString();
                    }
                    else
                    {
                        objModel.来文机关 = "";
                    }

                    if (datRead["文号"] != System.DBNull.Value)
                    {
                        objModel.文号 = datRead["文号"].ToString();
                    }
                    else
                    {
                        objModel.文号 = "";
                    }

                    if (datRead["文件名称"] != System.DBNull.Value)
                    {
                        objModel.文件名称 = datRead["文件名称"].ToString();
                    }
                    else
                    {
                        objModel.文件名称 = "";
                    }

                    if (datRead["批办领导"] != System.DBNull.Value)
                    {
                        objModel.批办领导 = datRead["批办领导"].ToString();
                    }
                    else
                    {
                        objModel.批办领导 = "";
                    }

                    if (datRead["收到文件数"] != System.DBNull.Value)
                    {
                        objModel.收到文件数 = Convert.ToInt32(datRead["收到文件数"].ToString());
                    }
                    else
                    {
                        objModel.收到文件数 = 0;
                    }

                    if (datRead["打印状态"] != System.DBNull.Value)
                    {
                        objModel.打印状态 = Convert.ToInt32(datRead["打印状态"].ToString());
                    }
                    else
                    {
                        objModel.打印状态 = 0;
                    }
                    if (datRead["备注"] != System.DBNull.Value)
                    {
                        objModel.备注 = datRead["备注"].ToString();
                    }
                    else
                    {
                        objModel.备注 = "";
                    }

                    if (datRead["创建人"] != System.DBNull.Value)
                    {
                        objModel.创建人 = datRead["创建人"].ToString();
                    }
                    else
                    {
                        objModel.创建人 = "";
                    }

                    if (datRead["创建时间"] != System.DBNull.Value)
                    {
                        objModel.创建时间 = Convert.ToDateTime(datRead["创建时间"].ToString());
                    }
                    else
                    {
                        objModel.创建时间 = Convert.ToDateTime("1000-1-1");
                    }

                    if (datRead["修改人"] != System.DBNull.Value)
                    {
                        objModel.修改人 = datRead["修改人"].ToString();
                    }
                    else
                    {
                        objModel.修改人 = "";
                    }

                    if (datRead["修改时间"] != System.DBNull.Value)
                    {
                        objModel.修改时间 = Convert.ToDateTime(datRead["修改时间"].ToString());
                    }
                    else
                    {
                        objModel.修改时间 = Convert.ToDateTime("1000-1-1");
                    }


                    if (datRead["SSUserID"] != System.DBNull.Value)
                    {
                        objModel.SSUserID = Convert.ToInt32(datRead["SSUserID"].ToString());
                    }
                    else
                    {
                        objModel.SSUserID = 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                comm.Dispose();
            }
            return objModel;
        }

        public void UpdateSys_收文登记表打印状态(int id)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "update  Sys_收文登记表 set 打印状态=1 Where ID='" + id + "'";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            int datRead = 0;
            try
            {
                conn.Open();
                datRead = comm.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }
        }

        /// <summary> 
        /// 获得最大单号 - 暂无用
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public int GetDHXH(string strDate)
        {
            int nMaxID = 0;
            string strSql = "";
            strSql = "SELECT MAX(BH) AS Expr1 FROM Sys_收文登记表 WHERE (BH LIKE '" + strDate + "%')";
            dbConstring = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);

            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    if (datRead["Expr1"] == System.DBNull.Value)
                    {
                        nMaxID = 0;
                    }
                    else
                    {
                        nMaxID = Convert.ToInt32(datRead["Expr1"]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("执行数据库SQL语句错误：" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return nMaxID;
        }

        /// <summary>
        /// 获得收文列表
        /// </summary>
        /// <returns></returns>
        public List<int> GetGSGZDID(string d开始日期, string d结束日期, string str来文机关, string str批办领导, string str文件名称,string str文号)
        {
            List<int> listId = new List<int>();
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT ID FROM Sys_收文登记表";
            strSql = strSql + " WHERE 1=1";

            if (d开始日期 != "")
            {
                strSql = strSql + "  and  收到时间>='" + d开始日期 + "'";
            }

            if (d结束日期 != "")
            {
                strSql = strSql + "  and 收到时间<='" + d结束日期 + " 23:59:59'";
            }
            if (str来文机关 != "")
            {
                strSql = strSql + "  and 来文机关='" + str来文机关 + "'";
            }
            if (str批办领导 != "")
            {
                strSql = strSql + "  and 批办领导='" + str批办领导 + "'";
            }
            if (str文件名称 != "")
            {
                strSql = strSql + "  and 文件名称 like '%" + str文件名称 + "%'";
            }
            if (!string.IsNullOrEmpty(str文号))
            {
                strSql = strSql + "  and 文号 like '%" + str文号 + "%'";
            }
            strSql = strSql + " order by 收到时间 desc,ID desc";

            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    listId.Add(Convert.ToInt32(datRead["ID"]));
                }
            }
            catch (Exception ex) { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }
            return listId;
        }

        /// <summary>
        /// 根据工作单联系人，获取联系人的联系电话
        /// </summary>
        /// <returns></returns>
        public string GetXmlxdhByLxr(string strLxr = "")
        {
            string strXmlxdh = "";
            dbConstring = new DBClass(DbFilePath.FilePathName);
            string strSql = "SELECT distinct XMLXDH FROM Sys_收文登记表 Where KHLXR='" + strLxr + "'";
            SQLiteConnection conn = new SQLiteConnection(dbConstring.m_ConnString);
            SQLiteCommand comm = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = null;
            try
            {
                conn.Open();
                datRead = comm.ExecuteReader();
                while (datRead.Read())
                {
                    strXmlxdh = datRead["XMLXDH"].ToString();
                    break;
                }
            }
            catch { }
            finally
            {
                conn.Close();
                comm.Dispose();
            }

            return strXmlxdh;
        }

        #endregion

    }
}
