﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DXTempLate.Class
{
    /// <summary>
    /// 系统界面中常用的方法
    /// </summary>
    public class FormClass
    {
        /// <summary>
        /// 零时存放主界面的TreeView控件
        /// </summary>
        public static System.Windows.Forms.TreeView mTreeView;

        /// <summary>
        /// XGridView鼠标左键点击，获取点击当前行索引，并选中当前行的CheckBox
        /// </summary>
        /// <param name="XGridView">GridView列表</param>
        /// <param name="e">System.Windows.Forms.MouseEventArgs鼠标左键点击</param>
        /// <param name="nType">类型：0为单选，1为可多选</param>
        ///<param name="strFileName">选择字段名称（选择,XZ）</param>
        /// <returns>
        /// 当前行索引 nCurRow
        /// </returns>
        public int XGridViewMouseDown(DevExpress.XtraGrid.Views.Grid.GridView XGridView, System.Windows.Forms.MouseEventArgs e, int nType, string strFileName = "选择")
        {
            int nCurRow = 0;
            GridHitInfo hi = XGridView.CalcHitInfo(e.Location);
            if (hi.InRow)
            {
                nCurRow = hi.RowHandle;
            }
            if (nCurRow < 0) return nCurRow;

            //如果点击左键
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                //选择上CheckBox
                SetCurRowCheck(XGridView, nCurRow, nType, strFileName);
            }
            return nCurRow;
        }

        /// <summary>
        /// 控制列表的CheckBox
        /// 点击列表中行，选中CheckBox
        /// </summary>
        /// <param name="XGridView">GridView列表</param>
        /// <param name="nCurRow">当前行索引</param>
        /// <param name="nType">类型：0为单选，1为可多选</param>
        /// <param name="strFileName">字段名称（选择,XZ）</param>
        public void SetCurRowCheck(DevExpress.XtraGrid.Views.Grid.GridView XGridView, int nCurRow, int nType, string strFileName = "选择")
        {
            var _with1 = XGridView;
            for (int nRow = 0; nRow <= _with1.RowCount - 1; nRow++)
            {
                if (nRow == nCurRow)
                {
                    if (Convert.ToBoolean(XGridView.GetRowCellValue(nRow, strFileName)) == true)
                    {
                        _with1.SetRowCellValue(nRow, strFileName, false);
                    }
                    else
                    {
                        _with1.SetRowCellValue(nRow, strFileName, true);
                    }
                }
                else
                {
                    if (nType == 0)
                    {
                        _with1.SetRowCellValue(nRow, strFileName, false);
                    }
                }
            }
        }

        /// <summary>
        /// 全选或者全不选列表方法
        /// </summary>
        /// <param name="XGridView">列表名称</param>
        /// <param name="nType">类型：0为全选，1为全不选</param>
        public void SelCurRowCheckAllAndUnAll(DevExpress.XtraGrid.Views.Grid.GridView XGridView, int nType)
        {
            var _with1 = XGridView;
            for (int nRow = 0; nRow <= _with1.RowCount - 1; nRow++)
            {
                if (nType == 0)
                {
                    //全选
                    _with1.SetRowCellValue(nRow, "选择", true);
                }
                else
                {
                    //全不选
                    _with1.SetRowCellValue(nRow, "选择", false);
                }
            }
        }

        /// <summary>
        /// 绑定ComboBox列表的值
        /// 根据字典表
        /// </summary>
        /// <param name="comboBox">控件</param>
        /// <param name="mList">保存编码和名称的数组</param>
        /// <param name="nType">
        /// 1用户类型，2地雷种类,3性别，4学历，5职务，6部门
        /// </param>
        public void BindComboBox(DevExpress.XtraEditors.ComboBoxEdit combox, int nType, out Dictionary<int, string> mList)
        {
            mList = new Dictionary<int, string>();
            combox.Properties.Items.Clear();//先清除数据

            System.Data.DataSet datSet = new System.Data.DataSet();
            QueryParam qp = new QueryParam();
            qp.TableName = "SYS_DIC";
            qp.Orderfld = "SORT";
            qp.OrderType = 2;
            qp.Where = " WHERE TYPECODE=" + nType;
            qp.ReturnFields = "NAME,CODE,SORT";
            int nCount = 0;
            SqliteClass oraClass = new SqliteClass();
            datSet = oraClass.QueryTableInfo(qp, out nCount, false);
            if (nCount > 0)
            {
                for (int i = 0; i < datSet.Tables[0].Rows.Count; i++)
                {
                    combox.Properties.Items.Add(datSet.Tables[0].Rows[i]["NAME"].ToString());
                    mList.Add(Convert.ToInt32(datSet.Tables[0].Rows[i]["CODE"].ToString()), datSet.Tables[0].Rows[i]["NAME"].ToString());
                }
                //comboBox.SelectedIndex = 0;               
            }
        }

        /// <summary>
        /// 绑定ComboBox列表的值
        /// </summary>
        /// <param name="combox">DevExpress.XtraEditors.ComboBoxEdit</param>
        /// <param name="nType">1用户类型，2地雷种类,3性别，4学历，5职务，6部门</param>
        public void BindComboBox(DevExpress.XtraEditors.ComboBoxEdit combox, int nType)
        {
            combox.Properties.Items.Clear();
            System.Data.DataSet datSet = new DataSet();

            QueryParam qp = new QueryParam();
            qp.TableName = "SYS_DIC";
            qp.Orderfld = "SORT";
            qp.OrderType = 2;
            qp.Where = " WHERE TYPECODE=" + nType;
            qp.ReturnFields = "NAME,CODE,SORT";
            int nCount = 0;
            SqliteClass oraClass = new SqliteClass();
            datSet = oraClass.QueryTableInfo(qp, out nCount, false);
            if (nCount > 0)
            {
                for (int i = 0; i < datSet.Tables[0].Rows.Count; i++)
                {
                    combox.Properties.Items.Add(datSet.Tables[0].Rows[i]["NAME"]);
                }
                combox.SelectedIndex = 0;
            }

        }

        /// <summary>
        /// 绑定ComboBox列表的值
        /// 根据字典表
        /// </summary>
        /// <param name="comboBox">控件</param>       
        /// <param name="nType">
        /// 1数据等级，2文件类别，3权限类型，4角色类型，5性别，6职务,7日志类型
        /// </param>
        public void BindComboBox(System.Windows.Forms.ComboBox comboBox, int nType)
        {
            comboBox.Items.Clear();//先清除数据
            System.Data.DataSet datSet = new System.Data.DataSet();
            QueryParam qp = new QueryParam();
            qp.TableName = "SYS_DIC";
            qp.Orderfld = "SORT";
            qp.OrderType = 2;
            qp.Where = " WHERE TYPECODE=" + nType;
            qp.ReturnFields = "NAME,CODE,SORT";
            int nCount = 0;
            SqliteClass oraClass = new SqliteClass();
            datSet = oraClass.QueryTableInfo(qp, out nCount, false);
            if (nCount > 0)
            {
                for (int i = 0; i < datSet.Tables[0].Rows.Count; i++)
                {
                    comboBox.Items.Add(datSet.Tables[0].Rows[i]["NAME"].ToString());
                }
            }

        }


        /// <summary>
        /// 绑定当前用户表数据
        /// </summary>
        /// <param name="combox"> DevExpress.XtraEditors.ComboBoxEdit  </param>
        public void BindComBoxUser(DevExpress.XtraEditors.ComboBoxEdit combox)
        {
            SqliteClass sqliteClass = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqliteClass.GetAllTablesID("Sys_Employee");
            if (listid.Count > 0)
            {
                combox.Properties.Items.Clear();
                for (int i = 0; i < listid.Count; i++)
                {
                    //if (listid[i] == 1) continue;
                    Entity_Sys_Employee objinfo = new Entity_Sys_Employee();
                    objinfo = sqliteClass.GetSys_Employee(listid[i]);
                    combox.Properties.Items.Add(objinfo.Name);
                }
            }
        }
    }
}
