﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraSplashScreen;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars;

using DXTempLate.Controls;
using DXTempLate.Properties;
using DXTempLate.Class;

namespace DXTempLate
{
    public partial class FrmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmMain()
        {
            InitializeComponent();

            string strSkinName = Properties.Settings.Default.SkinName;//获得皮肤的值
            UserLookAndFeel.Default.SetSkinStyle(strSkinName);//默认系统界面皮肤

            SkinHelper.InitSkinGallery(ribbonGalleryBarItem1, true); //添加界面皮肤
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.Text = StaticClass.g_TitleName;

            this.panelControlMain.Dock = System.Windows.Forms.DockStyle.Fill;

            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUC_GSGZD.ItSelf);
            XUC_GSGZD.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        #region 工作管理
        //首页
        private void btn首页_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUC_GSGZD.ItSelf);
            XUC_GSGZD.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.panelControlMain.Controls.Add(XUControlMainMap.ItSelf);
            //XUControlMainMap.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //工作单录入
        private void btn收文登记_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUC_GSGZD.ItSelf);
            XUC_GSGZD.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }
        //工作单查询
        private void btn收文登记查询_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUC_GSGZDTJ.ItSelf);
            XUC_GSGZDTJ.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //工作量统计
        private void btn工作量统计_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUC_GZDTJMap.ItSelf);
            XUC_GZDTJMap.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        } 
        #endregion

        #region 系统管理
        //用户信息
        private void btn用户信息_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraFormUserInfo user = new XtraFormUserInfo(1, true);
            user.ShowDialog();
        }

        //人员维护
        private void btn人员维护_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlUserList.ItSelf);
            XUControlUserList.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //数据字典
        private void btn数据字典_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlSysDic.ItSelf);
            XUControlSysDic.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //密码修改
        private void btn密码修改_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlPassword.ItSelf);
            XUControlPassword.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //退出
        private void barBtomExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("你确认需要退出吗？", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Application.Exit();
            }
        } 
        #endregion

        #region 主题
        //主题
        private void ribbonGalleryBarItem1_Gallery_ItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            string caption = string.Empty;
            if (ribbonGalleryBarItem1.Gallery == null) return;
            caption = ribbonGalleryBarItem1.Gallery.GetCheckedItems()[0].Caption;//主题的描述
            caption = caption.Replace("主题：", "");

            if (XtraMessageBox.Show("你确定要保存当前皮肤吗?", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Properties.Settings.Default.SkinName = caption;
                Properties.Settings.Default.Save();
            }
        } 
        #endregion       

    }
}
