﻿namespace DXTempLate
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btn首页 = new DevExpress.XtraBars.BarButtonItem();
            this.btn人员维护 = new DevExpress.XtraBars.BarButtonItem();
            this.btn密码修改 = new DevExpress.XtraBars.BarButtonItem();
            this.btn用户信息 = new DevExpress.XtraBars.BarButtonItem();
            this.btn收文登记 = new DevExpress.XtraBars.BarButtonItem();
            this.btn收文登记查询 = new DevExpress.XtraBars.BarButtonItem();
            this.barBtomExit = new DevExpress.XtraBars.BarButtonItem();
            this.btn数据字典 = new DevExpress.XtraBars.BarButtonItem();
            this.btn工作量统计 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.panelControlMain = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Images = this.imageList1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonGalleryBarItem1,
            this.btn首页,
            this.btn人员维护,
            this.btn密码修改,
            this.btn用户信息,
            this.btn收文登记,
            this.btn收文登记查询,
            this.barBtomExit,
            this.btn数据字典,
            this.btn工作量统计});
            this.ribbonControl1.LargeImages = this.imageList1;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1457, 184);
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "首页.png");
            this.imageList1.Images.SetKeyName(1, "收入.png");
            this.imageList1.Images.SetKeyName(2, "支出.png");
            this.imageList1.Images.SetKeyName(3, "人员管理.png");
            this.imageList1.Images.SetKeyName(4, "密码修改.png");
            this.imageList1.Images.SetKeyName(5, "统计.png");
            this.imageList1.Images.SetKeyName(6, "月账单.png");
            this.imageList1.Images.SetKeyName(7, "关于.png");
            this.imageList1.Images.SetKeyName(8, "退出.png");
            this.imageList1.Images.SetKeyName(9, "系统日志.png");
            this.imageList1.Images.SetKeyName(10, "用户信息.png");
            this.imageList1.Images.SetKeyName(11, "字典编码.png");
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            // 
            // 
            // 
            this.ribbonGalleryBarItem1.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.ribbonGalleryBarItem1_Gallery_ItemClick);
            this.ribbonGalleryBarItem1.Id = 1;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // btn首页
            // 
            this.btn首页.Caption = "首页";
            this.btn首页.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn首页.Id = 6;
            this.btn首页.ImageIndex = 0;
            this.btn首页.LargeImageIndex = 0;
            this.btn首页.Name = "btn首页";
            this.btn首页.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn首页_ItemClick);
            // 
            // btn人员维护
            // 
            this.btn人员维护.Caption = "人员维护";
            this.btn人员维护.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn人员维护.Id = 10;
            this.btn人员维护.ImageIndex = 3;
            this.btn人员维护.LargeImageIndex = 3;
            this.btn人员维护.Name = "btn人员维护";
            this.btn人员维护.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn人员维护_ItemClick);
            // 
            // btn密码修改
            // 
            this.btn密码修改.Caption = "密码修改";
            this.btn密码修改.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn密码修改.Id = 11;
            this.btn密码修改.ImageIndex = 4;
            this.btn密码修改.LargeImageIndex = 4;
            this.btn密码修改.Name = "btn密码修改";
            this.btn密码修改.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn密码修改_ItemClick);
            // 
            // btn用户信息
            // 
            this.btn用户信息.Caption = "用户信息";
            this.btn用户信息.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn用户信息.Id = 13;
            this.btn用户信息.ImageIndex = 10;
            this.btn用户信息.LargeImageIndex = 10;
            this.btn用户信息.Name = "btn用户信息";
            this.btn用户信息.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btn用户信息.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn用户信息_ItemClick);
            // 
            // btn收文登记
            // 
            this.btn收文登记.Caption = "收文登记";
            this.btn收文登记.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn收文登记.Id = 14;
            this.btn收文登记.ImageIndex = 9;
            this.btn收文登记.LargeImageIndex = 9;
            this.btn收文登记.Name = "btn收文登记";
            this.btn收文登记.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn收文登记_ItemClick);
            // 
            // btn收文登记查询
            // 
            this.btn收文登记查询.Caption = "收文登记查询";
            this.btn收文登记查询.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn收文登记查询.Id = 15;
            this.btn收文登记查询.ImageIndex = 7;
            this.btn收文登记查询.LargeImageIndex = 7;
            this.btn收文登记查询.Name = "btn收文登记查询";
            this.btn收文登记查询.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn收文登记查询_ItemClick);
            // 
            // barBtomExit
            // 
            this.barBtomExit.Caption = "退出";
            this.barBtomExit.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barBtomExit.Id = 16;
            this.barBtomExit.ImageIndex = 8;
            this.barBtomExit.LargeImageIndex = 8;
            this.barBtomExit.Name = "barBtomExit";
            this.barBtomExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtomExit_ItemClick);
            // 
            // btn数据字典
            // 
            this.btn数据字典.Caption = "数据字典";
            this.btn数据字典.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn数据字典.Id = 17;
            this.btn数据字典.ImageIndex = 11;
            this.btn数据字典.LargeImageIndex = 11;
            this.btn数据字典.Name = "btn数据字典";
            this.btn数据字典.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn数据字典_ItemClick);
            // 
            // btn工作量统计
            // 
            this.btn工作量统计.Caption = "工作量统计";
            this.btn工作量统计.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn工作量统计.Id = 18;
            this.btn工作量统计.ImageIndex = 5;
            this.btn工作量统计.LargeImageIndex = 5;
            this.btn工作量统计.Name = "btn工作量统计";
            this.btn工作量统计.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn工作量统计_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4,
            this.ribbonPageGroup2,
            this.ribbonPageGroup5});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "主系统";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btn首页);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btn收文登记);
            this.ribbonPageGroup3.ItemLinks.Add(this.btn收文登记查询);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "工作单管理";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btn工作量统计);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "查询统计";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.ribbonGalleryBarItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "界面皮肤";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btn用户信息);
            this.ribbonPageGroup5.ItemLinks.Add(this.btn人员维护);
            this.ribbonPageGroup5.ItemLinks.Add(this.btn数据字典);
            this.ribbonPageGroup5.ItemLinks.Add(this.btn密码修改);
            this.ribbonPageGroup5.ItemLinks.Add(this.barBtomExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "系统管理";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.barDockControlTop.Size = new System.Drawing.Size(1457, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 1056);
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.barDockControlBottom.Size = new System.Drawing.Size(1457, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 1056);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1457, 0);
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 1056);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // panelControlMain
            // 
            this.panelControlMain.Location = new System.Drawing.Point(81, 295);
            this.panelControlMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelControlMain.Name = "panelControlMain";
            this.panelControlMain.Size = new System.Drawing.Size(1071, 509);
            this.panelControlMain.TabIndex = 5;
            // 
            // FrmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1457, 1079);
            this.Controls.Add(this.panelControlMain);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收文登记表-管理软件";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.BarButtonItem btn首页;
        private DevExpress.XtraBars.BarButtonItem btn人员维护;
        private DevExpress.XtraBars.BarButtonItem btn密码修改;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem btn用户信息;
        private DevExpress.XtraBars.BarButtonItem btn收文登记;
        private DevExpress.XtraBars.BarButtonItem btn收文登记查询;
        private DevExpress.XtraBars.BarButtonItem barBtomExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraEditors.PanelControl panelControlMain;
        private DevExpress.XtraBars.BarButtonItem btn数据字典;
        private DevExpress.XtraBars.BarButtonItem btn工作量统计;
    }
}

