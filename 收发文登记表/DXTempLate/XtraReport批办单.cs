﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace DXTempLate
{
    public partial class XtraReport批办单 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport批办单()
        {
            InitializeComponent();
        }

        public XtraReport批办单(string Title,string 机关, string 收文, string 时间, string 文件名, string 意见 = "请李局长阅示")
        {
            InitializeComponent();

            this.xrLabelTitle.Text = Title;
            this.xrTableCell机关.Text = 机关;
            this.xrTableCell收文.Text = string.Format(xrTableCell收文.Text,收文);
            this.xrTableCell时间.Text = 时间;
            this.xrTableCell文件名.Text = 文件名;
            this.xrTableCell意见.Text = 意见;
        }

    }
}
