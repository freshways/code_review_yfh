﻿namespace DXTempLate
{
    partial class XtraFormGSGZD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormGSGZD));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleBtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt收文数 = new DevExpress.XtraEditors.TextEdit();
            this.txt文号 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt批办领导 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblPeople = new DevExpress.XtraEditors.LabelControl();
            this.txt备注 = new DevExpress.XtraEditors.MemoEdit();
            this.txt文件名称 = new DevExpress.XtraEditors.MemoEdit();
            this.labelRemark = new DevExpress.XtraEditors.LabelControl();
            this.labelDecs = new DevExpress.XtraEditors.LabelControl();
            this.labelDate = new DevExpress.XtraEditors.LabelControl();
            this.txt收到时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt来文机关 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblDW = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt收文数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt批办领导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文件名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt收到时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt收到时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt来文机关.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.flowLayoutPanel1);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txt收文数);
            this.groupControl1.Controls.Add(this.txt文号);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txt批办领导);
            this.groupControl1.Controls.Add(this.lblPeople);
            this.groupControl1.Controls.Add(this.txt备注);
            this.groupControl1.Controls.Add(this.txt文件名称);
            this.groupControl1.Controls.Add(this.labelRemark);
            this.groupControl1.Controls.Add(this.labelDecs);
            this.groupControl1.Controls.Add(this.labelDate);
            this.groupControl1.Controls.Add(this.txt收到时间);
            this.groupControl1.Controls.Add(this.txt来文机关);
            this.groupControl1.Controls.Add(this.lblDW);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(394, 431);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "信息维护";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.simpleBtnCancel);
            this.flowLayoutPanel1.Controls.Add(this.simpleBtnOK);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 401);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(390, 28);
            this.flowLayoutPanel1.TabIndex = 44;
            // 
            // simpleBtnCancel
            // 
            this.simpleBtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleBtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnCancel.Image")));
            this.simpleBtnCancel.Location = new System.Drawing.Point(312, 3);
            this.simpleBtnCancel.Name = "simpleBtnCancel";
            this.simpleBtnCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnCancel.TabIndex = 17;
            this.simpleBtnCancel.Text = "取消&Esc";
            // 
            // simpleBtnOK
            // 
            this.simpleBtnOK.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnOK.Image")));
            this.simpleBtnOK.Location = new System.Drawing.Point(231, 3);
            this.simpleBtnOK.Name = "simpleBtnOK";
            this.simpleBtnOK.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnOK.TabIndex = 15;
            this.simpleBtnOK.Text = "保存&F5";
            this.simpleBtnOK.Click += new System.EventHandler(this.simpleBtnOK_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl6.Location = new System.Drawing.Point(22, 178);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(72, 16);
            this.labelControl6.TabIndex = 43;
            this.labelControl6.Text = "收文件数:";
            // 
            // txt收文数
            // 
            this.txt收文数.EditValue = "1";
            this.txt收文数.Location = new System.Drawing.Point(108, 173);
            this.txt收文数.Name = "txt收文数";
            this.txt收文数.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt收文数.Properties.Appearance.Options.UseFont = true;
            this.txt收文数.Size = new System.Drawing.Size(81, 26);
            this.txt收文数.TabIndex = 9;
            this.txt收文数.ToolTip = "营业额 【单位:元】";
            // 
            // txt文号
            // 
            this.txt文号.Location = new System.Drawing.Point(108, 97);
            this.txt文号.Name = "txt文号";
            this.txt文号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt文号.Properties.Appearance.Options.UseFont = true;
            this.txt文号.Size = new System.Drawing.Size(221, 26);
            this.txt文号.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl1.Location = new System.Drawing.Point(22, 100);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 16);
            this.labelControl1.TabIndex = 30;
            this.labelControl1.Text = "文    号:";
            // 
            // txt批办领导
            // 
            this.txt批办领导.Location = new System.Drawing.Point(108, 131);
            this.txt批办领导.Name = "txt批办领导";
            this.txt批办领导.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt批办领导.Properties.Appearance.Options.UseFont = true;
            this.txt批办领导.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt批办领导.Properties.DropDownRows = 15;
            this.txt批办领导.Size = new System.Drawing.Size(221, 26);
            this.txt批办领导.TabIndex = 7;
            // 
            // lblPeople
            // 
            this.lblPeople.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPeople.Location = new System.Drawing.Point(22, 68);
            this.lblPeople.Name = "lblPeople";
            this.lblPeople.Size = new System.Drawing.Size(72, 16);
            this.lblPeople.TabIndex = 28;
            this.lblPeople.Text = "来文机关:";
            // 
            // txt备注
            // 
            this.txt备注.EditValue = "请李局长阅示";
            this.txt备注.Location = new System.Drawing.Point(108, 331);
            this.txt备注.Name = "txt备注";
            this.txt备注.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt备注.Properties.Appearance.Options.UseFont = true;
            this.txt备注.Size = new System.Drawing.Size(265, 60);
            this.txt备注.TabIndex = 13;
            this.txt备注.UseOptimizedRendering = true;
            // 
            // txt文件名称
            // 
            this.txt文件名称.Location = new System.Drawing.Point(108, 210);
            this.txt文件名称.Name = "txt文件名称";
            this.txt文件名称.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt文件名称.Properties.Appearance.Options.UseFont = true;
            this.txt文件名称.Size = new System.Drawing.Size(265, 109);
            this.txt文件名称.TabIndex = 11;
            this.txt文件名称.UseOptimizedRendering = true;
            // 
            // labelRemark
            // 
            this.labelRemark.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelRemark.Location = new System.Drawing.Point(22, 342);
            this.labelRemark.Name = "labelRemark";
            this.labelRemark.Size = new System.Drawing.Size(72, 16);
            this.labelRemark.TabIndex = 23;
            this.labelRemark.Text = "备注信息:";
            // 
            // labelDecs
            // 
            this.labelDecs.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelDecs.Location = new System.Drawing.Point(22, 231);
            this.labelDecs.Name = "labelDecs";
            this.labelDecs.Size = new System.Drawing.Size(72, 16);
            this.labelDecs.TabIndex = 22;
            this.labelDecs.Text = "文件名称:";
            // 
            // labelDate
            // 
            this.labelDate.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelDate.Location = new System.Drawing.Point(22, 36);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(72, 16);
            this.labelDate.TabIndex = 19;
            this.labelDate.Text = "收到时间:";
            // 
            // txt收到时间
            // 
            this.txt收到时间.EditValue = "";
            this.txt收到时间.Location = new System.Drawing.Point(108, 33);
            this.txt收到时间.Name = "txt收到时间";
            this.txt收到时间.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt收到时间.Properties.Appearance.Options.UseFont = true;
            this.txt收到时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt收到时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt收到时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt收到时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt收到时间.Size = new System.Drawing.Size(221, 26);
            this.txt收到时间.TabIndex = 1;
            // 
            // txt来文机关
            // 
            this.txt来文机关.Location = new System.Drawing.Point(108, 65);
            this.txt来文机关.Name = "txt来文机关";
            this.txt来文机关.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt来文机关.Properties.Appearance.Options.UseFont = true;
            this.txt来文机关.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt来文机关.Properties.DropDownRows = 15;
            this.txt来文机关.Size = new System.Drawing.Size(221, 26);
            this.txt来文机关.TabIndex = 3;
            // 
            // lblDW
            // 
            this.lblDW.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblDW.Location = new System.Drawing.Point(22, 130);
            this.lblDW.Name = "lblDW";
            this.lblDW.Size = new System.Drawing.Size(72, 32);
            this.lblDW.TabIndex = 0;
            this.lblDW.Text = "批办领导/\r\n科室:";
            // 
            // XtraFormGSGZD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 431);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XtraFormGSGZD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收文登记表";
            this.Load += new System.EventHandler(this.XtraFormGSGZD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt收文数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt批办领导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文件名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt收到时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt收到时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt来文机关.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.ComboBoxEdit txt批办领导;
        private DevExpress.XtraEditors.LabelControl lblPeople;
        private DevExpress.XtraEditors.SimpleButton simpleBtnCancel;
        private DevExpress.XtraEditors.SimpleButton simpleBtnOK;
        private DevExpress.XtraEditors.MemoEdit txt备注;
        private DevExpress.XtraEditors.MemoEdit txt文件名称;
        private DevExpress.XtraEditors.LabelControl labelRemark;
        private DevExpress.XtraEditors.LabelControl labelDecs;
        private DevExpress.XtraEditors.LabelControl labelDate;
        private DevExpress.XtraEditors.DateEdit txt收到时间;
        private DevExpress.XtraEditors.ComboBoxEdit txt来文机关;
        private DevExpress.XtraEditors.LabelControl lblDW;
        private DevExpress.XtraEditors.TextEdit txt文号;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txt收文数;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}