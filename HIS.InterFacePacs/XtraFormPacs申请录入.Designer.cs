﻿namespace HIS.InterFacePacs
{
    partial class XtraFormPacs申请录入
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormPacs申请录入));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit家庭地址 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton查体 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton主诉 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit孕检 = new DevExpress.XtraEditors.CheckEdit();
            this.dateEdit末次月经 = new DevExpress.XtraEditors.DateEdit();
            this.searchLookUpEdit执行科室 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton诊断 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit诊断 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl编码 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit检查部位 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit主诉 = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit查体目的 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem孕检 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem末次 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem住址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.searchLookUpEdit1 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl名称 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家庭地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit孕检.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit末次月经.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit末次月经.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit检查部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem孕检)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem末次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 434);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 382);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(221, 46);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton确定;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(221, 382);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(121, 46);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(226, 387);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(117, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 4;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.searchLookUpEdit1);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit家庭地址);
            this.layoutControl1.Controls.Add(this.simpleButton查体);
            this.layoutControl1.Controls.Add(this.simpleButton主诉);
            this.layoutControl1.Controls.Add(this.checkEdit孕检);
            this.layoutControl1.Controls.Add(this.dateEdit末次月经);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit执行科室);
            this.layoutControl1.Controls.Add(this.simpleButton诊断);
            this.layoutControl1.Controls.Add(this.textEdit诊断);
            this.layoutControl1.Controls.Add(this.labelControl名称);
            this.layoutControl1.Controls.Add(this.labelControl编码);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.textEdit检查部位);
            this.layoutControl1.Controls.Add(this.textEdit主诉);
            this.layoutControl1.Controls.Add(this.textEdit查体目的);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(489, 327, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(472, 434);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(108, 355);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Size = new System.Drawing.Size(351, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 19;
            // 
            // textEdit家庭地址
            // 
            this.textEdit家庭地址.Enabled = false;
            this.textEdit家庭地址.Location = new System.Drawing.Point(108, 331);
            this.textEdit家庭地址.Name = "textEdit家庭地址";
            this.textEdit家庭地址.Size = new System.Drawing.Size(351, 20);
            this.textEdit家庭地址.StyleController = this.layoutControl1;
            this.textEdit家庭地址.TabIndex = 18;
            // 
            // simpleButton查体
            // 
            this.simpleButton查体.Location = new System.Drawing.Point(433, 158);
            this.simpleButton查体.Name = "simpleButton查体";
            this.simpleButton查体.Size = new System.Drawing.Size(26, 71);
            this.simpleButton查体.StyleController = this.layoutControl1;
            this.simpleButton查体.TabIndex = 17;
            this.simpleButton查体.Text = "...";
            this.simpleButton查体.Click += new System.EventHandler(this.simpleButton查体_Click);
            // 
            // simpleButton主诉
            // 
            this.simpleButton主诉.Location = new System.Drawing.Point(433, 83);
            this.simpleButton主诉.Name = "simpleButton主诉";
            this.simpleButton主诉.Size = new System.Drawing.Size(26, 71);
            this.simpleButton主诉.StyleController = this.layoutControl1;
            this.simpleButton主诉.TabIndex = 16;
            this.simpleButton主诉.Text = "...";
            this.simpleButton主诉.Click += new System.EventHandler(this.simpleButton主诉_Click);
            // 
            // checkEdit孕检
            // 
            this.checkEdit孕检.Location = new System.Drawing.Point(108, 307);
            this.checkEdit孕检.Name = "checkEdit孕检";
            this.checkEdit孕检.Properties.Caption = "是";
            this.checkEdit孕检.Size = new System.Drawing.Size(24, 19);
            this.checkEdit孕检.StyleController = this.layoutControl1;
            this.checkEdit孕检.TabIndex = 15;
            this.checkEdit孕检.CheckedChanged += new System.EventHandler(this.checkEdit孕检_CheckedChanged);
            // 
            // dateEdit末次月经
            // 
            this.dateEdit末次月经.EditValue = null;
            this.dateEdit末次月经.Enabled = false;
            this.dateEdit末次月经.Location = new System.Drawing.Point(215, 307);
            this.dateEdit末次月经.Name = "dateEdit末次月经";
            this.dateEdit末次月经.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit末次月经.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit末次月经.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit末次月经.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit末次月经.Size = new System.Drawing.Size(244, 20);
            this.dateEdit末次月经.StyleController = this.layoutControl1;
            this.dateEdit末次月经.TabIndex = 14;
            // 
            // searchLookUpEdit执行科室
            // 
            this.searchLookUpEdit执行科室.EditValue = "";
            this.searchLookUpEdit执行科室.Location = new System.Drawing.Point(108, 283);
            this.searchLookUpEdit执行科室.Name = "searchLookUpEdit执行科室";
            this.searchLookUpEdit执行科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit执行科室.Properties.NullText = "";
            this.searchLookUpEdit执行科室.Properties.View = this.gridView1;
            this.searchLookUpEdit执行科室.Size = new System.Drawing.Size(351, 20);
            this.searchLookUpEdit执行科室.StyleController = this.layoutControl1;
            this.searchLookUpEdit执行科室.TabIndex = 13;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton诊断
            // 
            this.simpleButton诊断.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton诊断.Image")));
            this.simpleButton诊断.Location = new System.Drawing.Point(433, 233);
            this.simpleButton诊断.Name = "simpleButton诊断";
            this.simpleButton诊断.Size = new System.Drawing.Size(26, 22);
            this.simpleButton诊断.StyleController = this.layoutControl1;
            this.simpleButton诊断.TabIndex = 12;
            this.simpleButton诊断.Click += new System.EventHandler(this.simpleButton诊断_Click);
            // 
            // textEdit诊断
            // 
            this.textEdit诊断.Location = new System.Drawing.Point(108, 233);
            this.textEdit诊断.Name = "textEdit诊断";
            this.textEdit诊断.Size = new System.Drawing.Size(321, 20);
            this.textEdit诊断.StyleController = this.layoutControl1;
            this.textEdit诊断.TabIndex = 11;
            // 
            // labelControl编码
            // 
            this.labelControl编码.Location = new System.Drawing.Point(112, 37);
            this.labelControl编码.Name = "labelControl编码";
            this.labelControl编码.Size = new System.Drawing.Size(347, 18);
            this.labelControl编码.StyleController = this.layoutControl1;
            this.labelControl编码.TabIndex = 8;
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.Enabled = false;
            this.simpleButton取消.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(347, 387);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(120, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 5;
            this.simpleButton取消.Text = "取消";
            this.simpleButton取消.Click += new System.EventHandler(this.simpleButton取消_Click);
            // 
            // textEdit检查部位
            // 
            this.textEdit检查部位.EditValue = "请选择...";
            this.textEdit检查部位.Location = new System.Drawing.Point(108, 259);
            this.textEdit检查部位.Name = "textEdit检查部位";
            this.textEdit检查部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit检查部位.Properties.DisplayMember = "部位名称";
            this.textEdit检查部位.Properties.NullText = "请选择部位...";
            this.textEdit检查部位.Properties.ValueMember = "部位编码";
            this.textEdit检查部位.Properties.View = this.searchLookUpEdit1View;
            this.textEdit检查部位.Size = new System.Drawing.Size(351, 20);
            this.textEdit检查部位.StyleController = this.layoutControl1;
            this.textEdit检查部位.TabIndex = 6;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // textEdit主诉
            // 
            this.textEdit主诉.Location = new System.Drawing.Point(108, 83);
            this.textEdit主诉.Name = "textEdit主诉";
            this.textEdit主诉.Size = new System.Drawing.Size(321, 71);
            this.textEdit主诉.StyleController = this.layoutControl1;
            this.textEdit主诉.TabIndex = 7;
            this.textEdit主诉.UseOptimizedRendering = true;
            // 
            // textEdit查体目的
            // 
            this.textEdit查体目的.Location = new System.Drawing.Point(108, 158);
            this.textEdit查体目的.Name = "textEdit查体目的";
            this.textEdit查体目的.Size = new System.Drawing.Size(321, 71);
            this.textEdit查体目的.StyleController = this.layoutControl1;
            this.textEdit查体目的.TabIndex = 10;
            this.textEdit查体目的.UseOptimizedRendering = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton取消;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(342, 382);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(124, 46);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "Pacs&检查申请单";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem11,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem10,
            this.layoutControlItem孕检,
            this.layoutControlItem末次,
            this.layoutControlItem住址,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(466, 382);
            this.layoutControlGroup2.Text = "Pacs&&检查申请单";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.labelControl编码;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(450, 22);
            this.layoutControlItem5.Text = "申请项目编码：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(94, 18);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem4.Control = this.textEdit主诉;
            this.layoutControlItem4.CustomizationFormText = "体征描述:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(101, 75);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(420, 75);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "主诉(病历摘要体征描述)：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton主诉;
            this.layoutControlItem11.CustomizationFormText = "...";
            this.layoutControlItem11.Location = new System.Drawing.Point(420, 46);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "...";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit查体目的;
            this.layoutControlItem7.CustomizationFormText = "查体";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 121);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(101, 75);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(420, 75);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "检查目的：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButton查体;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(420, 121);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit诊断;
            this.layoutControlItem8.CustomizationFormText = "诊断";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(420, 26);
            this.layoutControlItem8.Text = "临床诊断：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton诊断;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(420, 196);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit检查部位;
            this.layoutControlItem3.CustomizationFormText = "新组号:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(450, 24);
            this.layoutControlItem3.Text = "检查部位：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.searchLookUpEdit执行科室;
            this.layoutControlItem10.CustomizationFormText = "执行科室：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 246);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(450, 24);
            this.layoutControlItem10.Text = "执行科室：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem孕检
            // 
            this.layoutControlItem孕检.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem孕检.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem孕检.Control = this.checkEdit孕检;
            this.layoutControlItem孕检.CustomizationFormText = "孕产妇检查:";
            this.layoutControlItem孕检.Location = new System.Drawing.Point(0, 270);
            this.layoutControlItem孕检.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem孕检.MinSize = new System.Drawing.Size(1, 24);
            this.layoutControlItem孕检.Name = "layoutControlItem孕检";
            this.layoutControlItem孕检.Size = new System.Drawing.Size(123, 24);
            this.layoutControlItem孕检.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem孕检.Text = "孕妇检查：";
            this.layoutControlItem孕检.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem孕检.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem孕检.TextToControlDistance = 5;
            this.layoutControlItem孕检.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem末次
            // 
            this.layoutControlItem末次.Control = this.dateEdit末次月经;
            this.layoutControlItem末次.CustomizationFormText = "末次月经时间:";
            this.layoutControlItem末次.Location = new System.Drawing.Point(123, 270);
            this.layoutControlItem末次.Name = "layoutControlItem末次";
            this.layoutControlItem末次.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem末次.Text = "末次月经时间:";
            this.layoutControlItem末次.TextSize = new System.Drawing.Size(76, 14);
            this.layoutControlItem末次.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem住址
            // 
            this.layoutControlItem住址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem住址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem住址.Control = this.textEdit家庭地址;
            this.layoutControlItem住址.CustomizationFormText = "家庭住址：";
            this.layoutControlItem住址.Location = new System.Drawing.Point(0, 294);
            this.layoutControlItem住址.Name = "layoutControlItem住址";
            this.layoutControlItem住址.Size = new System.Drawing.Size(450, 24);
            this.layoutControlItem住址.Text = "家庭住址：";
            this.layoutControlItem住址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem住址.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem住址.TextToControlDistance = 5;
            this.layoutControlItem住址.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit身份证号;
            this.layoutControlItem13.CustomizationFormText = "身份证号：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 318);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(450, 24);
            this.layoutControlItem13.Text = "身份证号：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // searchLookUpEdit1
            // 
            this.searchLookUpEdit1.EditValue = "请选择...";
            this.searchLookUpEdit1.Location = new System.Drawing.Point(163, 59);
            this.searchLookUpEdit1.Name = "searchLookUpEdit1";
            this.searchLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit1.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit1.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit1.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit1.Properties.View = this.gridView2;
            this.searchLookUpEdit1.Size = new System.Drawing.Size(296, 20);
            this.searchLookUpEdit1.StyleController = this.layoutControl1;
            this.searchLookUpEdit1.TabIndex = 20;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.searchLookUpEdit1;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(150, 22);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.labelControl名称;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 22);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem6.Text = "申请项目名称：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(94, 18);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // labelControl名称
            // 
            this.labelControl名称.Location = new System.Drawing.Point(112, 59);
            this.labelControl名称.Name = "labelControl名称";
            this.labelControl名称.Size = new System.Drawing.Size(47, 18);
            this.labelControl名称.StyleController = this.layoutControl1;
            this.labelControl名称.TabIndex = 9;
            // 
            // XtraFormPacs申请录入
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 434);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormPacs申请录入";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pacs申请录入";
            this.Load += new System.EventHandler(this.XtraFormPacs申请录入_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家庭地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit孕检.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit末次月经.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit末次月经.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit检查部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem孕检)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem末次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl labelControl编码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SearchLookUpEdit textEdit检查部位;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.MemoEdit textEdit主诉;
        private DevExpress.XtraEditors.TextEdit textEdit诊断;
        private DevExpress.XtraEditors.MemoEdit textEdit查体目的;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton simpleButton诊断;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit执行科室;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.DateEdit dateEdit末次月经;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem末次;
        private DevExpress.XtraEditors.CheckEdit checkEdit孕检;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem孕检;
        private DevExpress.XtraEditors.SimpleButton simpleButton查体;
        private DevExpress.XtraEditors.SimpleButton simpleButton主诉;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEdit家庭地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem住址;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.LabelControl labelControl名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}