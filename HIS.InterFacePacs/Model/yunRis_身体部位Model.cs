﻿// litao@Copy Right 2006-2016
// 文件： yunRis_身体部位Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///yunRis_身体部位数据实体
	/// </summary>
	[DataContract]
	public partial class yunRis_身体部位Model:INotifyPropertyChanged
	{
	
		private decimal _iid;
		private string _设备类型 = String.Empty;
		private string _部位编码 = String.Empty;
		private string _部位名称 = String.Empty;
		private string _部位名称_eng = String.Empty;
		private string _his部位编码 = String.Empty;
		private string _his部位名称 = String.Empty;
		private string _cdept = String.Empty;
		private string _cae_title = String.Empty;
		private decimal _排序id = 0;
		private string _备注 = String.Empty;
		private decimal _父id = 0;
		private decimal _价格 = 0;
		private decimal _ibgcs = 0;
		private string _默认部位 = String.Empty;
		private string _医院编码 = String.Empty;
		private DateTime _createtime = Convert.ToDateTime("1900-01-01");
	
		///<summary>
		///
		///</summary>
		public yunRis_身体部位Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=iid></param>
		///<param name=设备类型></param>
		///<param name=部位编码></param>
		///<param name=部位名称></param>
		///<param name=部位名称_eng></param>
		///<param name=his部位编码></param>
		///<param name=his部位名称></param>
		///<param name=cdept></param>
		///<param name=cae_title></param>
		///<param name=排序id></param>
		///<param name=备注></param>
		///<param name=父id></param>
		///<param name=价格></param>
		///<param name=ibgcs></param>
		///<param name=默认部位></param>
		///<param name=医院编码>院区编码</param>
		///<param name=createtime></param>
		public yunRis_身体部位Model(decimal iid,string 设备类型,string 部位编码,string 部位名称,string 部位名称_eng,string his部位编码,string his部位名称,string cdept,string cae_title,decimal 排序id,string 备注,decimal 父id,decimal 价格,decimal ibgcs,string 默认部位,string 医院编码,DateTime createtime)
		{
			this._iid = iid;
			this._设备类型 = 设备类型;
			this._部位编码 = 部位编码;
			this._部位名称 = 部位名称;
			this._部位名称_eng = 部位名称_eng;
			this._his部位编码 = his部位编码;
			this._his部位名称 = his部位名称;
			this._cdept = cdept;
			this._cae_title = cae_title;
			this._排序id = 排序id;
			this._备注 = 备注;
			this._父id = 父id;
			this._价格 = 价格;
			this._ibgcs = ibgcs;
			this._默认部位 = 默认部位;
			this._医院编码 = 医院编码;
			this._createtime = createtime;
		}
        
	
		
		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "IID", IsRequired = false, Order = 0)]
		public decimal IID
		{
			get {return _iid;}
			set {
            _iid = value;
            PropertyChanged(this, new PropertyChangedEventArgs("IID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "设备类型", IsRequired = false, Order = 1)]
		public string 设备类型
		{
			get {return _设备类型;}
			set {
            _设备类型 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("设备类型"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位编码", IsRequired = false, Order = 2)]
		public string 部位编码
		{
			get {return _部位编码;}
			set {
            _部位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位名称", IsRequired = false, Order = 3)]
		public string 部位名称
		{
			get {return _部位名称;}
			set {
            _部位名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位名称_ENG", IsRequired = false, Order = 4)]
		public string 部位名称_ENG
		{
			get {return _部位名称_eng;}
			set {
            _部位名称_eng = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位名称_ENG"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "HIS部位编码", IsRequired = false, Order = 5)]
		public string HIS部位编码
		{
			get {return _his部位编码;}
			set {
            _his部位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("HIS部位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "HIS部位名称", IsRequired = false, Order = 6)]
		public string HIS部位名称
		{
			get {return _his部位名称;}
			set {
            _his部位名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("HIS部位名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "CDEPT", IsRequired = false, Order = 7)]
		public string CDEPT
		{
			get {return _cdept;}
			set {
            _cdept = value;
            PropertyChanged(this, new PropertyChangedEventArgs("CDEPT"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "CAE_TITLE", IsRequired = false, Order = 8)]
		public string CAE_TITLE
		{
			get {return _cae_title;}
			set {
            _cae_title = value;
            PropertyChanged(this, new PropertyChangedEventArgs("CAE_TITLE"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "排序ID", IsRequired = false, Order = 9)]
		public decimal 排序ID
		{
			get {return _排序id;}
			set {
            _排序id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("排序ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "备注", IsRequired = false, Order = 10)]
		public string 备注
		{
			get {return _备注;}
			set {
            _备注 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("备注"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "父ID", IsRequired = false, Order = 11)]
		public decimal 父ID
		{
			get {return _父id;}
			set {
            _父id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("父ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "价格", IsRequired = false, Order = 12)]
		public decimal 价格
		{
			get {return _价格;}
			set {
            _价格 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("价格"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "IBGCS", IsRequired = false, Order = 13)]
		public decimal IBGCS
		{
			get {return _ibgcs;}
			set {
            _ibgcs = value;
            PropertyChanged(this, new PropertyChangedEventArgs("IBGCS"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "默认部位", IsRequired = false, Order = 14)]
		public string 默认部位
		{
			get {return _默认部位;}
			set {
            _默认部位 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("默认部位"));
            }
		}

		///<summary>
		///院区编码
		///</summary>
		[DataMember(Name = "医院编码", IsRequired = false, Order = 15)]
		public string 医院编码
		{
			get {return _医院编码;}
			set {
            _医院编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("医院编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "createTime", IsRequired = false, Order = 16)]
		public DateTime createTime
		{
			get {return _createtime;}
			set {
            _createtime = value;
            PropertyChanged(this, new PropertyChangedEventArgs("createTime"));
            }
		}
        
         public bool ClearContext()
		{
            _iid = -1;
            _设备类型 = String.Empty;
            _部位编码 = String.Empty;
            _部位名称 = String.Empty;
            _部位名称_eng = String.Empty;
            _his部位编码 = String.Empty;
            _his部位名称 = String.Empty;
            _cdept = String.Empty;
            _cae_title = String.Empty;
            _排序id = 0;
            _备注 = String.Empty;
            _父id = 0;
            _价格 = 0;
            _ibgcs = 0;
            _默认部位 = String.Empty;
            _医院编码 = String.Empty;
            _createtime = Convert.ToDateTime("1900-01-01");
    	    return true;
		}
     
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
	
		
	}
}