﻿// litao@Copy Right 2006-2016
// 文件： Pacs_项目对照Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///Pacs_项目对照数据实体
	/// </summary>
	[DataContract]
	public partial class Pacs_项目对照Model:INotifyPropertyChanged
	{
	
		private int _id = -1;
		private string _医嘱编码 = String.Empty;
		private string _医嘱名称 = String.Empty;
		private string _医嘱名称拼音代码 = String.Empty;
		private int _收费编码 = 0;
		private string _收费名称 = String.Empty;
		private int _执行科室编码 = 0;
		private string _执行科室名称 = String.Empty;
		private string _设备编码 = String.Empty;
		private string _设备名称 = String.Empty;
		private int _分院编码 = 0;
		private string _部位编码 = String.Empty;
		private string _部位名称 = String.Empty;
		private string _胶片编码 = String.Empty;
		private string _胶片名称 = String.Empty;
		private int _胶片张数 = 0;
		private int _收费数量 = 0;
		private string _guid = String.Empty;
		private DateTime _createtime = Convert.ToDateTime("1900-01-01");
	
		///<summary>
		///
		///</summary>
		public Pacs_项目对照Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=医嘱编码></param>
		///<param name=医嘱名称></param>
		///<param name=医嘱名称拼音代码></param>
		///<param name=收费编码></param>
		///<param name=收费名称></param>
		///<param name=执行科室编码></param>
		///<param name=执行科室名称></param>
		///<param name=设备编码></param>
		///<param name=设备名称></param>
		///<param name=分院编码></param>
		///<param name=部位编码></param>
		///<param name=部位名称></param>
		///<param name=胶片编码></param>
		///<param name=胶片名称></param>
		///<param name=胶片张数></param>
		///<param name=收费数量></param>
		///<param name=guid></param>
		///<param name=createtime></param>
		public Pacs_项目对照Model(int id,string 医嘱编码,string 医嘱名称,string 医嘱名称拼音代码,int 收费编码,string 收费名称,int 执行科室编码,string 执行科室名称,string 设备编码,string 设备名称,int 分院编码,string 部位编码,string 部位名称,string 胶片编码,string 胶片名称,int 胶片张数,int 收费数量,string guid,DateTime createtime)
		{
			this._id = id;
			this._医嘱编码 = 医嘱编码;
			this._医嘱名称 = 医嘱名称;
			this._医嘱名称拼音代码 = 医嘱名称拼音代码;
			this._收费编码 = 收费编码;
			this._收费名称 = 收费名称;
			this._执行科室编码 = 执行科室编码;
			this._执行科室名称 = 执行科室名称;
			this._设备编码 = 设备编码;
			this._设备名称 = 设备名称;
			this._分院编码 = 分院编码;
			this._部位编码 = 部位编码;
			this._部位名称 = 部位名称;
			this._胶片编码 = 胶片编码;
			this._胶片名称 = 胶片名称;
			this._胶片张数 = 胶片张数;
			this._收费数量 = 收费数量;
			this._guid = guid;
			this._createtime = createtime;
		}
        
	
		
		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "医嘱编码", IsRequired = false, Order = 1)]
		public string 医嘱编码
		{
			get {return _医嘱编码;}
			set {
            _医嘱编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("医嘱编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "医嘱名称", IsRequired = false, Order = 2)]
		public string 医嘱名称
		{
			get {return _医嘱名称;}
			set {
            _医嘱名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("医嘱名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "医嘱名称拼音代码", IsRequired = false, Order = 3)]
		public string 医嘱名称拼音代码
		{
			get {return _医嘱名称拼音代码;}
			set {
            _医嘱名称拼音代码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("医嘱名称拼音代码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费编码", IsRequired = false, Order = 4)]
		public int 收费编码
		{
			get {return _收费编码;}
			set {
            _收费编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费名称", IsRequired = false, Order = 5)]
		public string 收费名称
		{
			get {return _收费名称;}
			set {
            _收费名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室编码", IsRequired = false, Order = 6)]
		public int 执行科室编码
		{
			get {return _执行科室编码;}
			set {
            _执行科室编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室名称", IsRequired = false, Order = 7)]
		public string 执行科室名称
		{
			get {return _执行科室名称;}
			set {
            _执行科室名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "设备编码", IsRequired = false, Order = 8)]
		public string 设备编码
		{
			get {return _设备编码;}
			set {
            _设备编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("设备编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "设备名称", IsRequired = false, Order = 9)]
		public string 设备名称
		{
			get {return _设备名称;}
			set {
            _设备名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("设备名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "分院编码", IsRequired = false, Order = 10)]
		public int 分院编码
		{
			get {return _分院编码;}
			set {
            _分院编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("分院编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位编码", IsRequired = false, Order = 11)]
		public string 部位编码
		{
			get {return _部位编码;}
			set {
            _部位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位名称", IsRequired = false, Order = 12)]
		public string 部位名称
		{
			get {return _部位名称;}
			set {
            _部位名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "胶片编码", IsRequired = false, Order = 13)]
		public string 胶片编码
		{
			get {return _胶片编码;}
			set {
            _胶片编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("胶片编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "胶片名称", IsRequired = false, Order = 14)]
		public string 胶片名称
		{
			get {return _胶片名称;}
			set {
            _胶片名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("胶片名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "胶片张数", IsRequired = false, Order = 15)]
		public int 胶片张数
		{
			get {return _胶片张数;}
			set {
            _胶片张数 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("胶片张数"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费数量", IsRequired = false, Order = 16)]
		public int 收费数量
		{
			get {return _收费数量;}
			set {
            _收费数量 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费数量"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "GUID", IsRequired = false, Order = 17)]
		public string GUID
		{
			get {return _guid;}
			set {
            _guid = value;
            PropertyChanged(this, new PropertyChangedEventArgs("GUID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "createTime", IsRequired = false, Order = 18)]
		public DateTime createTime
		{
			get {return _createtime;}
			set {
            _createtime = value;
            PropertyChanged(this, new PropertyChangedEventArgs("createTime"));
            }
		}
        
         public bool ClearContext()
		{
            _id = -1;
            _医嘱编码 = String.Empty;
            _医嘱名称 = String.Empty;
            _医嘱名称拼音代码 = String.Empty;
            _收费编码 = 0;
            _收费名称 = String.Empty;
            _执行科室编码 = 0;
            _执行科室名称 = String.Empty;
            _设备编码 = String.Empty;
            _设备名称 = String.Empty;
            _分院编码 = 0;
            _部位编码 = String.Empty;
            _部位名称 = String.Empty;
            _胶片编码 = String.Empty;
            _胶片名称 = String.Empty;
            _胶片张数 = 0;
            _收费数量 = 0;
            _guid = String.Empty;
            _createtime = Convert.ToDateTime("1900-01-01");
    	    return true;
		}
     
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
	
		
	}
}