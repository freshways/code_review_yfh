﻿// litao@Copy Right 2006-2016
// 文件： Pacs_申请模板Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-04-21
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///Pacs_申请模板数据实体
	/// </summary>
	[DataContract]
	public partial class Pacs_申请模板Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private string _名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _拼音代码 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _类别 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _是否禁用 = String.Empty;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public Pacs_申请模板Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=名称></param>
		///<param name=拼音代码></param>
		///<param name=类别></param>
		///<param name=是否禁用></param>
		public Pacs_申请模板Model(int id,string 名称,string 拼音代码,string 类别,string 是否禁用)
		{
			this._id = id;
			this._名称 = 名称;
			this._拼音代码 = 拼音代码;
			this._类别 = 类别;
			this._是否禁用 = 是否禁用;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "名称", IsRequired = false, Order = 1)]
		public string 名称
		{
			get {return _名称;}
			set {
            _名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "拼音代码", IsRequired = false, Order = 2)]
		public string 拼音代码
		{
			get {return _拼音代码;}
			set {
            _拼音代码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("拼音代码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "类别", IsRequired = false, Order = 3)]
		public string 类别
		{
			get {return _类别;}
			set {
            _类别 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("类别"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否禁用", IsRequired = false, Order = 4)]
		public string 是否禁用
		{
			get {return _是否禁用;}
			set {
            _是否禁用 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否禁用"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}