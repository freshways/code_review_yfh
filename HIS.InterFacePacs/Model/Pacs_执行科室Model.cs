﻿// litao@Copy Right 2006-2016
// 文件： Pacs_执行科室Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-04-20
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///Pacs_执行科室数据实体
	/// </summary>
	[DataContract]
	public partial class Pacs_执行科室Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private int _执行科室编码;
		///<summary>
		///
		///</summary>
		private string _执行科室名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _分院编码;
		///<summary>
		///
		///</summary>
		private int _单位编码;
		///<summary>
		///
		///</summary>
		private DateTime _create_time = DateTime.Now;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public Pacs_执行科室Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=执行科室编码></param>
		///<param name=执行科室名称></param>
		///<param name=分院编码></param>
		///<param name=单位编码></param>
		///<param name=create_time></param>
		public Pacs_执行科室Model(int id,int 执行科室编码,string 执行科室名称,int 分院编码,int 单位编码,DateTime create_time)
		{
			this._id = id;
			this._执行科室编码 = 执行科室编码;
			this._执行科室名称 = 执行科室名称;
			this._分院编码 = 分院编码;
			this._单位编码 = 单位编码;
			this._create_time = create_time;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室编码", IsRequired = false, Order = 1)]
		public int 执行科室编码
		{
			get {return _执行科室编码;}
			set {
            _执行科室编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室名称", IsRequired = false, Order = 2)]
		public string 执行科室名称
		{
			get {return _执行科室名称;}
			set {
            _执行科室名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "分院编码", IsRequired = false, Order = 3)]
		public int 分院编码
		{
			get {return _分院编码;}
			set {
            _分院编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("分院编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 4)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "Create_Time", IsRequired = false, Order = 5)]
		public DateTime Create_Time
		{
			get {return _create_time;}
			set {
            _create_time = value;
            PropertyChanged(this, new PropertyChangedEventArgs("Create_Time"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
